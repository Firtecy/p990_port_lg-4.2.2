.class public Lcom/android/server/connectivity/Tethering;
.super Landroid/net/INetworkManagementEventObserver$Stub;
.source "Tethering.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/connectivity/Tethering$EntitlementCheckService;,
        Lcom/android/server/connectivity/Tethering$TetherMasterSM;,
        Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;,
        Lcom/android/server/connectivity/Tethering$StateReceiver;
    }
.end annotation


# static fields
.field private static final DBG:Z = true

.field private static final DHCP_DEFAULT_RANGE:[Ljava/lang/String; = null

.field private static final DHCP_DEFAULT_SPR_HOTSPOT_RANGE:[Ljava/lang/String; = null

.field private static final DNS_DEFAULT_SERVER1:Ljava/lang/String; = "8.8.8.8"

.field private static final DNS_DEFAULT_SERVER2:Ljava/lang/String; = "8.8.4.4"

.field private static final DNS_DEFAULT_SERVER3:Ljava/lang/String; = "2001:4860:4860::8888"

.field private static final DNS_DEFAULT_SERVER4:Ljava/lang/String; = "2001:4860:4860::8844"

.field private static final DUN_TYPE:Ljava/lang/Integer; = null

.field private static final HIPRI_TYPE:Ljava/lang/Integer; = null

.field private static final LG_DATA_SPRINT_DHCP_PORT_FORWARDING_IP:Ljava/lang/String; = "192.168.42.100"

.field private static final MOBILE_TYPE:Ljava/lang/Integer; = null

.field private static final TAG:Ljava/lang/String; = "Tethering"

.field private static final TETHERING_TYPE:Ljava/lang/Integer; = null

.field private static final USB_NEAR_IFACE_ADDR:Ljava/lang/String; = "192.168.42.129"

.field private static final USB_PREFIX_LENGTH:I = 0x18

.field private static final VDBG:Z = true

.field private static mContext:Landroid/content/Context; = null

.field private static final sTetherWifiSta:[I = null

.field private static final sTetherWifiStaTmus:I = 0x2020292

.field private static final sTetherWifiSta_general:[I


# instance fields
.field private final TARGET_COUNTRY:Ljava/lang/String;

.field private final TARGET_OPERATOR:Ljava/lang/String;

.field cm:Landroid/net/ConnectivityManager;

.field public featureset:Ljava/lang/String;

.field private mAlarmSender:Landroid/app/PendingIntent;

.field mBinder:Landroid/os/IBinder;

.field private mBluetoothPan:Landroid/bluetooth/BluetoothPan;

.field private mBluetoothTethered:Z

.field private mBluetoothTetheredNotification:Landroid/app/Notification;

.field protected mCM:Lcom/android/internal/telephony/CommandsInterface;

.field private mConnMgr:Landroid/net/ConnectivityManager;

.field private final mConnService:Landroid/net/IConnectivityManager;

.field private mDefaultDnsServers:[Ljava/lang/String;

.field private mDefaultDnsV4v6Servers:[Ljava/lang/String;

.field private mDhcpRange:[Ljava/lang/String;

.field private mDnsV4v6Servers:[Ljava/lang/String;

.field private mIPv4UpStreamingDevice:Ljava/lang/String;

.field private mIPv6UpStreamingDevice:Ljava/lang/String;

.field private mIfaces:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;",
            ">;"
        }
    .end annotation
.end field

.field protected mLGfeature:Lcom/android/internal/telephony/LGfeature;

.field private mLooper:Landroid/os/Looper;

.field private mManager:Landroid/app/AlarmManager;

.field private mMultipleTetheredNotification:Landroid/app/Notification;

.field private final mNMService:Landroid/os/INetworkManagementService;

.field private mNcmEnabled:Z

.field private mPermissionErrorNotification:Landroid/app/Notification;

.field protected mPhone:Lcom/android/internal/telephony/Phone;

.field private mPreferredUpstreamMobileApn:I

.field private mProfileServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

.field private mPublicSync:Ljava/lang/Object;

.field private mRetryLimit:I

.field private mRndisEnabled:Z

.field private mStateReceiver:Landroid/content/BroadcastReceiver;

.field private final mStatsService:Landroid/net/INetworkStatsService;

.field private mTetherMasterSM:Lcom/android/internal/util/StateMachine;

.field private mTetherNetwork:Lcom/android/server/connectivity/TetherNetwork;

.field private mTetherableBluetoothRegexs:[Ljava/lang/String;

.field private mTetherableUsbRegexs:[Ljava/lang/String;

.field private mTetherableWifiRegexs:[Ljava/lang/String;

.field private mTetheredNotification:Landroid/app/Notification;

.field private mThread:Landroid/os/HandlerThread;

.field private mUSBTetheredNotification:Landroid/app/Notification;

.field private mUpstreamIfaceTypes:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mUsbTetherRequested:Z

.field private mUsbTethered:Z

.field private mWiFiTetheredNotification:Landroid/app/Notification;

.field mWifiManager:Landroid/net/wifi/IWifiManager;

.field private mWifiTethered:Z

.field private mtempWifiManager:Landroid/net/wifi/WifiManager;

.field multipleTethered:Z

.field sBluetoothTethered:Z

.field sUsbTethered:Z

.field sWifiTethered:Z

.field private usb0InterfaceAdd:Z


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x2

    #@2
    const/4 v5, 0x1

    #@3
    const/16 v4, 0x9

    #@5
    const/4 v3, 0x0

    #@6
    .line 154
    new-instance v0, Ljava/lang/Integer;

    #@8
    invoke-direct {v0, v3}, Ljava/lang/Integer;-><init>(I)V

    #@b
    sput-object v0, Lcom/android/server/connectivity/Tethering;->MOBILE_TYPE:Ljava/lang/Integer;

    #@d
    .line 155
    new-instance v0, Ljava/lang/Integer;

    #@f
    const/4 v1, 0x5

    #@10
    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(I)V

    #@13
    sput-object v0, Lcom/android/server/connectivity/Tethering;->HIPRI_TYPE:Ljava/lang/Integer;

    #@15
    .line 156
    new-instance v0, Ljava/lang/Integer;

    #@17
    invoke-direct {v0, v7}, Ljava/lang/Integer;-><init>(I)V

    #@1a
    sput-object v0, Lcom/android/server/connectivity/Tethering;->DUN_TYPE:Ljava/lang/Integer;

    #@1c
    .line 168
    new-instance v0, Ljava/lang/Integer;

    #@1e
    const/16 v1, 0x12

    #@20
    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(I)V

    #@23
    sput-object v0, Lcom/android/server/connectivity/Tethering;->TETHERING_TYPE:Ljava/lang/Integer;

    #@25
    .line 196
    const/16 v0, 0xe

    #@27
    new-array v0, v0, [Ljava/lang/String;

    #@29
    const-string v1, "192.168.42.2"

    #@2b
    aput-object v1, v0, v3

    #@2d
    const-string v1, "192.168.42.254"

    #@2f
    aput-object v1, v0, v5

    #@31
    const-string v1, "192.168.43.2"

    #@33
    aput-object v1, v0, v6

    #@35
    const/4 v1, 0x3

    #@36
    const-string v2, "192.168.43.254"

    #@38
    aput-object v2, v0, v1

    #@3a
    const-string v1, "192.168.44.2"

    #@3c
    aput-object v1, v0, v7

    #@3e
    const/4 v1, 0x5

    #@3f
    const-string v2, "192.168.44.254"

    #@41
    aput-object v2, v0, v1

    #@43
    const/4 v1, 0x6

    #@44
    const-string v2, "192.168.45.2"

    #@46
    aput-object v2, v0, v1

    #@48
    const/4 v1, 0x7

    #@49
    const-string v2, "192.168.45.254"

    #@4b
    aput-object v2, v0, v1

    #@4d
    const/16 v1, 0x8

    #@4f
    const-string v2, "192.168.46.2"

    #@51
    aput-object v2, v0, v1

    #@53
    const-string v1, "192.168.46.254"

    #@55
    aput-object v1, v0, v4

    #@57
    const/16 v1, 0xa

    #@59
    const-string v2, "192.168.47.2"

    #@5b
    aput-object v2, v0, v1

    #@5d
    const/16 v1, 0xb

    #@5f
    const-string v2, "192.168.47.254"

    #@61
    aput-object v2, v0, v1

    #@63
    const/16 v1, 0xc

    #@65
    const-string v2, "192.168.48.2"

    #@67
    aput-object v2, v0, v1

    #@69
    const/16 v1, 0xd

    #@6b
    const-string v2, "192.168.48.254"

    #@6d
    aput-object v2, v0, v1

    #@6f
    sput-object v0, Lcom/android/server/connectivity/Tethering;->DHCP_DEFAULT_RANGE:[Ljava/lang/String;

    #@71
    .line 203
    new-array v0, v6, [Ljava/lang/String;

    #@73
    const-string v1, "192.168.1.100"

    #@75
    aput-object v1, v0, v3

    #@77
    const-string v1, "192.168.1.254"

    #@79
    aput-object v1, v0, v5

    #@7b
    sput-object v0, Lcom/android/server/connectivity/Tethering;->DHCP_DEFAULT_SPR_HOTSPOT_RANGE:[Ljava/lang/String;

    #@7d
    .line 270
    new-array v0, v4, [I

    #@7f
    fill-array-data v0, :array_8c

    #@82
    sput-object v0, Lcom/android/server/connectivity/Tethering;->sTetherWifiSta:[I

    #@84
    .line 283
    new-array v0, v4, [I

    #@86
    fill-array-data v0, :array_a2

    #@89
    sput-object v0, Lcom/android/server/connectivity/Tethering;->sTetherWifiSta_general:[I

    #@8b
    return-void

    #@8c
    .line 270
    :array_8c
    .array-data 0x4
        0xfet 0x5t 0x2t 0x2t
        0xfft 0x5t 0x2t 0x2t
        0x0t 0x6t 0x2t 0x2t
        0x1t 0x6t 0x2t 0x2t
        0x2t 0x6t 0x2t 0x2t
        0x3t 0x6t 0x2t 0x2t
        0x4t 0x6t 0x2t 0x2t
        0x5t 0x6t 0x2t 0x2t
        0x6t 0x6t 0x2t 0x2t
    .end array-data

    #@a2
    .line 283
    :array_a2
    .array-data 0x4
        0xdct 0x5t 0x2t 0x2t
        0xddt 0x5t 0x2t 0x2t
        0xdet 0x5t 0x2t 0x2t
        0xdft 0x5t 0x2t 0x2t
        0xe0t 0x5t 0x2t 0x2t
        0xe1t 0x5t 0x2t 0x2t
        0xe2t 0x5t 0x2t 0x2t
        0xe3t 0x5t 0x2t 0x2t
        0xe4t 0x5t 0x2t 0x2t
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/INetworkManagementService;Landroid/net/INetworkStatsService;Landroid/net/IConnectivityManager;Landroid/os/Looper;)V
    .registers 15
    .parameter "context"
    .parameter "nmService"
    .parameter "statsService"
    .parameter "connService"
    .parameter "looper"

    #@0
    .prologue
    const/4 v8, 0x2

    #@1
    const/4 v7, 0x1

    #@2
    const/4 v5, 0x0

    #@3
    const/4 v6, 0x0

    #@4
    .line 318
    invoke-direct {p0}, Landroid/net/INetworkManagementEventObserver$Stub;-><init>()V

    #@7
    .line 164
    const-string v2, "ro.afwdata.LGfeatureset"

    #@9
    const-string v3, "none"

    #@b
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@e
    move-result-object v2

    #@f
    iput-object v2, p0, Lcom/android/server/connectivity/Tethering;->featureset:Ljava/lang/String;

    #@11
    .line 174
    const/4 v2, -0x1

    #@12
    iput v2, p0, Lcom/android/server/connectivity/Tethering;->mPreferredUpstreamMobileApn:I

    #@14
    .line 210
    new-instance v2, Lcom/android/server/connectivity/Tethering$1;

    #@16
    invoke-direct {v2, p0}, Lcom/android/server/connectivity/Tethering$1;-><init>(Lcom/android/server/connectivity/Tethering;)V

    #@19
    iput-object v2, p0, Lcom/android/server/connectivity/Tethering;->mProfileServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    #@1b
    .line 242
    iput-boolean v6, p0, Lcom/android/server/connectivity/Tethering;->usb0InterfaceAdd:Z

    #@1d
    .line 246
    iput-object v5, p0, Lcom/android/server/connectivity/Tethering;->mIPv4UpStreamingDevice:Ljava/lang/String;

    #@1f
    .line 247
    iput-object v5, p0, Lcom/android/server/connectivity/Tethering;->mIPv6UpStreamingDevice:Ljava/lang/String;

    #@21
    .line 251
    iput-boolean v6, p0, Lcom/android/server/connectivity/Tethering;->mUsbTethered:Z

    #@23
    .line 252
    iput-boolean v6, p0, Lcom/android/server/connectivity/Tethering;->mWifiTethered:Z

    #@25
    .line 253
    iput-boolean v6, p0, Lcom/android/server/connectivity/Tethering;->mBluetoothTethered:Z

    #@27
    .line 266
    iput-object v5, p0, Lcom/android/server/connectivity/Tethering;->mTetherNetwork:Lcom/android/server/connectivity/TetherNetwork;

    #@29
    .line 301
    iput-boolean v6, p0, Lcom/android/server/connectivity/Tethering;->multipleTethered:Z

    #@2b
    .line 302
    iput-boolean v6, p0, Lcom/android/server/connectivity/Tethering;->sWifiTethered:Z

    #@2d
    .line 303
    iput-boolean v6, p0, Lcom/android/server/connectivity/Tethering;->sUsbTethered:Z

    #@2f
    .line 304
    iput-boolean v6, p0, Lcom/android/server/connectivity/Tethering;->sBluetoothTethered:Z

    #@31
    .line 311
    const-string v2, "ro.build.target_operator"

    #@33
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@36
    move-result-object v2

    #@37
    iput-object v2, p0, Lcom/android/server/connectivity/Tethering;->TARGET_OPERATOR:Ljava/lang/String;

    #@39
    .line 312
    const-string v2, "ro.build.target_country"

    #@3b
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@3e
    move-result-object v2

    #@3f
    iput-object v2, p0, Lcom/android/server/connectivity/Tethering;->TARGET_COUNTRY:Ljava/lang/String;

    #@41
    .line 319
    sput-object p1, Lcom/android/server/connectivity/Tethering;->mContext:Landroid/content/Context;

    #@43
    .line 320
    iput-object p2, p0, Lcom/android/server/connectivity/Tethering;->mNMService:Landroid/os/INetworkManagementService;

    #@45
    .line 321
    iput-object p3, p0, Lcom/android/server/connectivity/Tethering;->mStatsService:Landroid/net/INetworkStatsService;

    #@47
    .line 322
    iput-object p4, p0, Lcom/android/server/connectivity/Tethering;->mConnService:Landroid/net/IConnectivityManager;

    #@49
    .line 323
    iput-object p5, p0, Lcom/android/server/connectivity/Tethering;->mLooper:Landroid/os/Looper;

    #@4b
    .line 325
    new-instance v2, Ljava/lang/Object;

    #@4d
    invoke-direct/range {v2 .. v2}, Ljava/lang/Object;-><init>()V

    #@50
    iput-object v2, p0, Lcom/android/server/connectivity/Tethering;->mPublicSync:Ljava/lang/Object;

    #@52
    .line 327
    new-instance v2, Ljava/util/HashMap;

    #@54
    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    #@57
    iput-object v2, p0, Lcom/android/server/connectivity/Tethering;->mIfaces:Ljava/util/HashMap;

    #@59
    .line 330
    new-instance v2, Landroid/os/HandlerThread;

    #@5b
    const-string v3, "Tethering"

    #@5d
    invoke-direct {v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    #@60
    iput-object v2, p0, Lcom/android/server/connectivity/Tethering;->mThread:Landroid/os/HandlerThread;

    #@62
    .line 331
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering;->mThread:Landroid/os/HandlerThread;

    #@64
    invoke-virtual {v2}, Landroid/os/HandlerThread;->start()V

    #@67
    .line 332
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering;->mThread:Landroid/os/HandlerThread;

    #@69
    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@6c
    move-result-object v2

    #@6d
    iput-object v2, p0, Lcom/android/server/connectivity/Tethering;->mLooper:Landroid/os/Looper;

    #@6f
    .line 333
    new-instance v2, Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@71
    const-string v3, "TetherMaster"

    #@73
    iget-object v4, p0, Lcom/android/server/connectivity/Tethering;->mLooper:Landroid/os/Looper;

    #@75
    invoke-direct {v2, p0, v3, v4}, Lcom/android/server/connectivity/Tethering$TetherMasterSM;-><init>(Lcom/android/server/connectivity/Tethering;Ljava/lang/String;Landroid/os/Looper;)V

    #@78
    iput-object v2, p0, Lcom/android/server/connectivity/Tethering;->mTetherMasterSM:Lcom/android/internal/util/StateMachine;

    #@7a
    .line 334
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering;->mTetherMasterSM:Lcom/android/internal/util/StateMachine;

    #@7c
    invoke-virtual {v2}, Lcom/android/internal/util/StateMachine;->start()V

    #@7f
    .line 336
    new-instance v2, Lcom/android/server/connectivity/Tethering$StateReceiver;

    #@81
    invoke-direct {v2, p0, v5}, Lcom/android/server/connectivity/Tethering$StateReceiver;-><init>(Lcom/android/server/connectivity/Tethering;Lcom/android/server/connectivity/Tethering$1;)V

    #@84
    iput-object v2, p0, Lcom/android/server/connectivity/Tethering;->mStateReceiver:Landroid/content/BroadcastReceiver;

    #@86
    .line 337
    new-instance v1, Landroid/content/IntentFilter;

    #@88
    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    #@8b
    .line 338
    .local v1, filter:Landroid/content/IntentFilter;
    const-string v2, "android.hardware.usb.action.USB_STATE"

    #@8d
    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@90
    .line 339
    const-string v2, "android.net.conn.CONNECTIVITY_CHANGE"

    #@92
    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@95
    .line 341
    const-string v2, "android.net.conn.CONNECTIVITY_CHANGE_IMMEDIATE"

    #@97
    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@9a
    .line 342
    sget-object v2, Lcom/android/server/connectivity/Tethering;->mContext:Landroid/content/Context;

    #@9c
    const-string v3, "wifi"

    #@9e
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@a1
    move-result-object v2

    #@a2
    check-cast v2, Landroid/net/wifi/WifiManager;

    #@a4
    iput-object v2, p0, Lcom/android/server/connectivity/Tethering;->mtempWifiManager:Landroid/net/wifi/WifiManager;

    #@a6
    .line 346
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering;->featureset:Ljava/lang/String;

    #@a8
    invoke-static {p1, v2}, Lcom/android/internal/telephony/LGfeature;->getInstance(Landroid/content/Context;Ljava/lang/String;)Lcom/android/internal/telephony/LGfeature;

    #@ab
    move-result-object v2

    #@ac
    iput-object v2, p0, Lcom/android/server/connectivity/Tethering;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@ae
    .line 348
    const-string v2, "Tethering"

    #@b0
    new-instance v3, Ljava/lang/StringBuilder;

    #@b2
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@b5
    const-string v4, "[Tethering.java]"

    #@b7
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v3

    #@bb
    iget-object v4, p0, Lcom/android/server/connectivity/Tethering;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@bd
    invoke-virtual {v4}, Lcom/android/internal/telephony/LGfeature;->toString()Ljava/lang/String;

    #@c0
    move-result-object v4

    #@c1
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c4
    move-result-object v3

    #@c5
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c8
    move-result-object v3

    #@c9
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@cc
    .line 350
    sget-object v2, Lcom/android/server/connectivity/Tethering;->mContext:Landroid/content/Context;

    #@ce
    iget-object v3, p0, Lcom/android/server/connectivity/Tethering;->mStateReceiver:Landroid/content/BroadcastReceiver;

    #@d0
    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@d3
    .line 353
    const-string v2, "com.lge.wifi.sap.WIFI_SAP_STATION_DISASSOC"

    #@d5
    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@d8
    .line 354
    const-string v2, "com.lge.wifi.sap.WIFI_SAP_STATION_ASSOC"

    #@da
    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@dd
    .line 357
    sget-object v2, Lcom/android/server/connectivity/Tethering;->mContext:Landroid/content/Context;

    #@df
    iget-object v3, p0, Lcom/android/server/connectivity/Tethering;->mStateReceiver:Landroid/content/BroadcastReceiver;

    #@e1
    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@e4
    .line 359
    new-instance v1, Landroid/content/IntentFilter;

    #@e6
    .end local v1           #filter:Landroid/content/IntentFilter;
    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    #@e9
    .line 360
    .restart local v1       #filter:Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.MEDIA_SHARED"

    #@eb
    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@ee
    .line 361
    const-string v2, "android.intent.action.MEDIA_UNSHARED"

    #@f0
    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@f3
    .line 362
    const-string v2, "file"

    #@f5
    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    #@f8
    .line 363
    sget-object v2, Lcom/android/server/connectivity/Tethering;->mContext:Landroid/content/Context;

    #@fa
    iget-object v3, p0, Lcom/android/server/connectivity/Tethering;->mStateReceiver:Landroid/content/BroadcastReceiver;

    #@fc
    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@ff
    .line 366
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getCountry()Ljava/lang/String;

    #@102
    move-result-object v2

    #@103
    const-string v3, "US"

    #@105
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@108
    move-result v2

    #@109
    if-eqz v2, :cond_12c

    #@10b
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->isWifiChameleonFeaturedCarrier()Z

    #@10e
    move-result v2

    #@10f
    if-nez v2, :cond_11d

    #@111
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getOperator()Ljava/lang/String;

    #@114
    move-result-object v2

    #@115
    const-string v3, "ACG"

    #@117
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11a
    move-result v2

    #@11b
    if-eqz v2, :cond_12c

    #@11d
    .line 369
    :cond_11d
    new-instance v0, Ljava/io/File;

    #@11f
    const-string v2, "/data/misc/TetherNetworkFree.conf"

    #@121
    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@124
    .line 370
    .local v0, TetherNetworkFree:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    #@127
    move-result v2

    #@128
    if-eqz v2, :cond_1b8

    #@12a
    .line 371
    iput-object v5, p0, Lcom/android/server/connectivity/Tethering;->mTetherNetwork:Lcom/android/server/connectivity/TetherNetwork;

    #@12c
    .line 377
    .end local v0           #TetherNetworkFree:Ljava/io/File;
    :cond_12c
    :goto_12c
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@12f
    move-result-object v2

    #@130
    const v3, 0x1070024

    #@133
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    #@136
    move-result-object v2

    #@137
    iput-object v2, p0, Lcom/android/server/connectivity/Tethering;->mDhcpRange:[Ljava/lang/String;

    #@139
    .line 379
    sget-object v2, Lcom/android/server/connectivity/Tethering;->mContext:Landroid/content/Context;

    #@13b
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@13e
    move-result-object v2

    #@13f
    const v3, 0x10e000b

    #@142
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    #@145
    move-result v2

    #@146
    iput v2, p0, Lcom/android/server/connectivity/Tethering;->mRetryLimit:I

    #@148
    .line 381
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering;->mDhcpRange:[Ljava/lang/String;

    #@14a
    array-length v2, v2

    #@14b
    if-eqz v2, :cond_154

    #@14d
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering;->mDhcpRange:[Ljava/lang/String;

    #@14f
    array-length v2, v2

    #@150
    rem-int/lit8 v2, v2, 0x2

    #@152
    if-ne v2, v7, :cond_158

    #@154
    .line 382
    :cond_154
    sget-object v2, Lcom/android/server/connectivity/Tethering;->DHCP_DEFAULT_RANGE:[Ljava/lang/String;

    #@156
    iput-object v2, p0, Lcom/android/server/connectivity/Tethering;->mDhcpRange:[Ljava/lang/String;

    #@158
    .line 386
    :cond_158
    invoke-virtual {p0}, Lcom/android/server/connectivity/Tethering;->updateConfiguration()V

    #@15b
    .line 389
    new-array v2, v8, [Ljava/lang/String;

    #@15d
    iput-object v2, p0, Lcom/android/server/connectivity/Tethering;->mDefaultDnsServers:[Ljava/lang/String;

    #@15f
    .line 390
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering;->mDefaultDnsServers:[Ljava/lang/String;

    #@161
    const-string v3, "8.8.8.8"

    #@163
    aput-object v3, v2, v6

    #@165
    .line 391
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering;->mDefaultDnsServers:[Ljava/lang/String;

    #@167
    const-string v3, "8.8.4.4"

    #@169
    aput-object v3, v2, v7

    #@16b
    .line 394
    const/4 v2, 0x4

    #@16c
    new-array v2, v2, [Ljava/lang/String;

    #@16e
    iput-object v2, p0, Lcom/android/server/connectivity/Tethering;->mDefaultDnsV4v6Servers:[Ljava/lang/String;

    #@170
    .line 395
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering;->mDefaultDnsV4v6Servers:[Ljava/lang/String;

    #@172
    const-string v3, "8.8.8.8"

    #@174
    aput-object v3, v2, v6

    #@176
    .line 396
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering;->mDefaultDnsV4v6Servers:[Ljava/lang/String;

    #@178
    const-string v3, "2001:4860:4860::8888"

    #@17a
    aput-object v3, v2, v7

    #@17c
    .line 397
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering;->mDefaultDnsV4v6Servers:[Ljava/lang/String;

    #@17e
    const-string v3, "8.8.4.4"

    #@180
    aput-object v3, v2, v8

    #@182
    .line 398
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering;->mDefaultDnsV4v6Servers:[Ljava/lang/String;

    #@184
    const/4 v3, 0x3

    #@185
    const-string v4, "2001:4860:4860::8844"

    #@187
    aput-object v4, v2, v3

    #@189
    .line 402
    sget-object v2, Lcom/android/server/connectivity/Tethering;->mContext:Landroid/content/Context;

    #@18b
    const-string v3, "alarm"

    #@18d
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@190
    move-result-object v2

    #@191
    check-cast v2, Landroid/app/AlarmManager;

    #@193
    iput-object v2, p0, Lcom/android/server/connectivity/Tethering;->mManager:Landroid/app/AlarmManager;

    #@195
    .line 403
    sget-object v2, Lcom/android/server/connectivity/Tethering;->mContext:Landroid/content/Context;

    #@197
    new-instance v3, Landroid/content/Intent;

    #@199
    sget-object v4, Lcom/android/server/connectivity/Tethering;->mContext:Landroid/content/Context;

    #@19b
    const-class v5, Lcom/android/server/connectivity/Tethering$EntitlementCheckService;

    #@19d
    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    #@1a0
    invoke-static {v2, v6, v3, v6}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@1a3
    move-result-object v2

    #@1a4
    iput-object v2, p0, Lcom/android/server/connectivity/Tethering;->mAlarmSender:Landroid/app/PendingIntent;

    #@1a6
    .line 407
    const-string v2, "VZW"

    #@1a8
    const-string v3, "ro.build.target_operator"

    #@1aa
    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@1ad
    move-result-object v3

    #@1ae
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1b1
    move-result v2

    #@1b2
    if-eqz v2, :cond_1b7

    #@1b4
    .line 408
    invoke-virtual {p0}, Lcom/android/server/connectivity/Tethering;->addUsbIfaces()V

    #@1b7
    .line 411
    :cond_1b7
    return-void

    #@1b8
    .line 373
    .restart local v0       #TetherNetworkFree:Ljava/io/File;
    :cond_1b8
    new-instance v2, Lcom/android/server/connectivity/TetherNetwork;

    #@1ba
    sget-object v3, Lcom/android/server/connectivity/Tethering;->mContext:Landroid/content/Context;

    #@1bc
    invoke-direct {v2, v3}, Lcom/android/server/connectivity/TetherNetwork;-><init>(Landroid/content/Context;)V

    #@1bf
    iput-object v2, p0, Lcom/android/server/connectivity/Tethering;->mTetherNetwork:Lcom/android/server/connectivity/TetherNetwork;

    #@1c1
    goto/16 :goto_12c
.end method

.method static synthetic access$000(Lcom/android/server/connectivity/Tethering;)Landroid/bluetooth/BluetoothPan;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 135
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mBluetoothPan:Landroid/bluetooth/BluetoothPan;

    #@2
    return-object v0
.end method

.method static synthetic access$002(Lcom/android/server/connectivity/Tethering;Landroid/bluetooth/BluetoothPan;)Landroid/bluetooth/BluetoothPan;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 135
    iput-object p1, p0, Lcom/android/server/connectivity/Tethering;->mBluetoothPan:Landroid/bluetooth/BluetoothPan;

    #@2
    return-object p1
.end method

.method static synthetic access$1000(Lcom/android/server/connectivity/Tethering;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 135
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->TARGET_OPERATOR:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$1100(Lcom/android/server/connectivity/Tethering;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 135
    iget-boolean v0, p0, Lcom/android/server/connectivity/Tethering;->mWifiTethered:Z

    #@2
    return v0
.end method

.method static synthetic access$1200()[I
    .registers 1

    #@0
    .prologue
    .line 135
    sget-object v0, Lcom/android/server/connectivity/Tethering;->sTetherWifiSta:[I

    #@2
    return-object v0
.end method

.method static synthetic access$1300(Lcom/android/server/connectivity/Tethering;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 135
    invoke-direct {p0, p1}, Lcom/android/server/connectivity/Tethering;->showWiFiTetheredNotification(I)V

    #@3
    return-void
.end method

.method static synthetic access$1400(Lcom/android/server/connectivity/Tethering;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 135
    invoke-direct {p0, p1}, Lcom/android/server/connectivity/Tethering;->showMaxClientReachedNotification(I)V

    #@3
    return-void
.end method

.method static synthetic access$1500(Lcom/android/server/connectivity/Tethering;Z)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 135
    invoke-direct {p0, p1}, Lcom/android/server/connectivity/Tethering;->configureUsbIface(Z)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1800(Lcom/android/server/connectivity/Tethering;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 135
    invoke-direct {p0}, Lcom/android/server/connectivity/Tethering;->resetAlarm()V

    #@3
    return-void
.end method

.method static synthetic access$1900(Lcom/android/server/connectivity/Tethering;)Lcom/android/server/connectivity/TetherNetwork;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 135
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mTetherNetwork:Lcom/android/server/connectivity/TetherNetwork;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Lcom/android/server/connectivity/Tethering;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 135
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mPublicSync:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$2000(Lcom/android/server/connectivity/Tethering;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 135
    invoke-direct {p0}, Lcom/android/server/connectivity/Tethering;->sendTetherStateChangedBroadcast()V

    #@3
    return-void
.end method

.method static synthetic access$300(Lcom/android/server/connectivity/Tethering;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 135
    iget-boolean v0, p0, Lcom/android/server/connectivity/Tethering;->mRndisEnabled:Z

    #@2
    return v0
.end method

.method static synthetic access$302(Lcom/android/server/connectivity/Tethering;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 135
    iput-boolean p1, p0, Lcom/android/server/connectivity/Tethering;->mRndisEnabled:Z

    #@2
    return p1
.end method

.method static synthetic access$3200(Lcom/android/server/connectivity/Tethering;)Landroid/os/INetworkManagementService;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 135
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mNMService:Landroid/os/INetworkManagementService;

    #@2
    return-object v0
.end method

.method static synthetic access$3400(Lcom/android/server/connectivity/Tethering;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 135
    invoke-direct {p0}, Lcom/android/server/connectivity/Tethering;->setAlarm()V

    #@3
    return-void
.end method

.method static synthetic access$3500(Lcom/android/server/connectivity/Tethering;)Landroid/net/INetworkStatsService;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 135
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mStatsService:Landroid/net/INetworkStatsService;

    #@2
    return-object v0
.end method

.method static synthetic access$3800(Lcom/android/server/connectivity/Tethering;)Landroid/net/IConnectivityManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 135
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mConnService:Landroid/net/IConnectivityManager;

    #@2
    return-object v0
.end method

.method static synthetic access$3900(Lcom/android/server/connectivity/Tethering;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 135
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mIPv4UpStreamingDevice:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$3902(Lcom/android/server/connectivity/Tethering;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 135
    iput-object p1, p0, Lcom/android/server/connectivity/Tethering;->mIPv4UpStreamingDevice:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$400(Lcom/android/server/connectivity/Tethering;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 135
    iget-boolean v0, p0, Lcom/android/server/connectivity/Tethering;->mNcmEnabled:Z

    #@2
    return v0
.end method

.method static synthetic access$4000(Lcom/android/server/connectivity/Tethering;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 135
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mIPv6UpStreamingDevice:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$4002(Lcom/android/server/connectivity/Tethering;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 135
    iput-object p1, p0, Lcom/android/server/connectivity/Tethering;->mIPv6UpStreamingDevice:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$402(Lcom/android/server/connectivity/Tethering;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 135
    iput-boolean p1, p0, Lcom/android/server/connectivity/Tethering;->mNcmEnabled:Z

    #@2
    return p1
.end method

.method static synthetic access$4800(Lcom/android/server/connectivity/Tethering;)[Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 135
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mDhcpRange:[Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$4802(Lcom/android/server/connectivity/Tethering;[Ljava/lang/String;)[Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 135
    iput-object p1, p0, Lcom/android/server/connectivity/Tethering;->mDhcpRange:[Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$4900()[Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 135
    sget-object v0, Lcom/android/server/connectivity/Tethering;->DHCP_DEFAULT_RANGE:[Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Lcom/android/server/connectivity/Tethering;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 135
    iget-boolean v0, p0, Lcom/android/server/connectivity/Tethering;->mUsbTetherRequested:Z

    #@2
    return v0
.end method

.method static synthetic access$5000()[Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 135
    sget-object v0, Lcom/android/server/connectivity/Tethering;->DHCP_DEFAULT_SPR_HOTSPOT_RANGE:[Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$502(Lcom/android/server/connectivity/Tethering;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 135
    iput-boolean p1, p0, Lcom/android/server/connectivity/Tethering;->mUsbTetherRequested:Z

    #@2
    return p1
.end method

.method static synthetic access$5300(Lcom/android/server/connectivity/Tethering;)[Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 135
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mDnsV4v6Servers:[Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$5302(Lcom/android/server/connectivity/Tethering;[Ljava/lang/String;)[Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 135
    iput-object p1, p0, Lcom/android/server/connectivity/Tethering;->mDnsV4v6Servers:[Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$5400(Lcom/android/server/connectivity/Tethering;)[Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 135
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mDefaultDnsV4v6Servers:[Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$5500(Lcom/android/server/connectivity/Tethering;)[Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 135
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mDefaultDnsServers:[Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$600(Lcom/android/server/connectivity/Tethering;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 135
    invoke-direct {p0, p1}, Lcom/android/server/connectivity/Tethering;->tetherUsb(Z)V

    #@3
    return-void
.end method

.method static synthetic access$6400(Lcom/android/server/connectivity/Tethering;)Ljava/util/Collection;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 135
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mUpstreamIfaceTypes:Ljava/util/Collection;

    #@2
    return-object v0
.end method

.method static synthetic access$6500(Lcom/android/server/connectivity/Tethering;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 135
    iget v0, p0, Lcom/android/server/connectivity/Tethering;->mPreferredUpstreamMobileApn:I

    #@2
    return v0
.end method

.method static synthetic access$6600(Lcom/android/server/connectivity/Tethering;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 135
    iget v0, p0, Lcom/android/server/connectivity/Tethering;->mRetryLimit:I

    #@2
    return v0
.end method

.method static synthetic access$700(Lcom/android/server/connectivity/Tethering;)Lcom/android/internal/util/StateMachine;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 135
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mTetherMasterSM:Lcom/android/internal/util/StateMachine;

    #@2
    return-object v0
.end method

.method static synthetic access$800(Lcom/android/server/connectivity/Tethering;)Landroid/net/wifi/WifiManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 135
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mtempWifiManager:Landroid/net/wifi/WifiManager;

    #@2
    return-object v0
.end method

.method static synthetic access$900()Landroid/content/Context;
    .registers 1

    #@0
    .prologue
    .line 135
    sget-object v0, Lcom/android/server/connectivity/Tethering;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method private clearTetheredNotification()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1041
    sget-object v1, Lcom/android/server/connectivity/Tethering;->mContext:Landroid/content/Context;

    #@3
    const-string v2, "notification"

    #@5
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Landroid/app/NotificationManager;

    #@b
    .line 1044
    .local v0, notificationManager:Landroid/app/NotificationManager;
    if-eqz v0, :cond_5d

    #@d
    .line 1045
    iget-boolean v1, p0, Lcom/android/server/connectivity/Tethering;->mUsbTethered:Z

    #@f
    if-nez v1, :cond_20

    #@11
    iget-object v1, p0, Lcom/android/server/connectivity/Tethering;->mUSBTetheredNotification:Landroid/app/Notification;

    #@13
    if-eqz v1, :cond_20

    #@15
    .line 1046
    iget-object v1, p0, Lcom/android/server/connectivity/Tethering;->mUSBTetheredNotification:Landroid/app/Notification;

    #@17
    iget v1, v1, Landroid/app/Notification;->icon:I

    #@19
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@1b
    invoke-virtual {v0, v3, v1, v2}, Landroid/app/NotificationManager;->cancelAsUser(Ljava/lang/String;ILandroid/os/UserHandle;)V

    #@1e
    .line 1047
    iput-object v3, p0, Lcom/android/server/connectivity/Tethering;->mUSBTetheredNotification:Landroid/app/Notification;

    #@20
    .line 1050
    :cond_20
    iget-boolean v1, p0, Lcom/android/server/connectivity/Tethering;->mWifiTethered:Z

    #@22
    if-nez v1, :cond_4a

    #@24
    iget-object v1, p0, Lcom/android/server/connectivity/Tethering;->mWiFiTetheredNotification:Landroid/app/Notification;

    #@26
    if-eqz v1, :cond_4a

    #@28
    .line 1051
    iget-object v1, p0, Lcom/android/server/connectivity/Tethering;->mWiFiTetheredNotification:Landroid/app/Notification;

    #@2a
    iget v1, v1, Landroid/app/Notification;->icon:I

    #@2c
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@2e
    invoke-virtual {v0, v3, v1, v2}, Landroid/app/NotificationManager;->cancelAsUser(Ljava/lang/String;ILandroid/os/UserHandle;)V

    #@31
    .line 1052
    iput-object v3, p0, Lcom/android/server/connectivity/Tethering;->mWiFiTetheredNotification:Landroid/app/Notification;

    #@33
    .line 1054
    iget-object v1, p0, Lcom/android/server/connectivity/Tethering;->TARGET_OPERATOR:Ljava/lang/String;

    #@35
    const-string v2, "ATT"

    #@37
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3a
    move-result v1

    #@3b
    if-eqz v1, :cond_4a

    #@3d
    sget-boolean v1, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@3f
    if-eqz v1, :cond_4a

    #@41
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->doesSupportHotspotList()Z

    #@44
    move-result v1

    #@45
    if-eqz v1, :cond_4a

    #@47
    .line 1055
    invoke-virtual {p0}, Lcom/android/server/connectivity/Tethering;->clearTetheredBlockNotification()V

    #@4a
    .line 1060
    :cond_4a
    iget-boolean v1, p0, Lcom/android/server/connectivity/Tethering;->mBluetoothTethered:Z

    #@4c
    if-nez v1, :cond_5d

    #@4e
    iget-object v1, p0, Lcom/android/server/connectivity/Tethering;->mBluetoothTetheredNotification:Landroid/app/Notification;

    #@50
    if-eqz v1, :cond_5d

    #@52
    .line 1061
    iget-object v1, p0, Lcom/android/server/connectivity/Tethering;->mBluetoothTetheredNotification:Landroid/app/Notification;

    #@54
    iget v1, v1, Landroid/app/Notification;->icon:I

    #@56
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@58
    invoke-virtual {v0, v3, v1, v2}, Landroid/app/NotificationManager;->cancelAsUser(Ljava/lang/String;ILandroid/os/UserHandle;)V

    #@5b
    .line 1062
    iput-object v3, p0, Lcom/android/server/connectivity/Tethering;->mBluetoothTetheredNotification:Landroid/app/Notification;

    #@5d
    .line 1065
    :cond_5d
    return-void
.end method

.method private configureUsbIface(Z)Z
    .registers 14
    .parameter "enabled"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 1211
    const-string v9, "Tethering"

    #@3
    new-instance v10, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v11, "configureUsbIface("

    #@a
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v10

    #@e
    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@11
    move-result-object v10

    #@12
    const-string v11, ")"

    #@14
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v10

    #@18
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v10

    #@1c
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 1214
    new-array v5, v8, [Ljava/lang/String;

    #@21
    .line 1216
    .local v5, ifaces:[Ljava/lang/String;
    :try_start_21
    iget-object v9, p0, Lcom/android/server/connectivity/Tethering;->mNMService:Landroid/os/INetworkManagementService;

    #@23
    invoke-interface {v9}, Landroid/os/INetworkManagementService;->listInterfaces()[Ljava/lang/String;
    :try_end_26
    .catch Ljava/lang/Exception; {:try_start_21 .. :try_end_26} :catch_5f

    #@26
    move-result-object v5

    #@27
    .line 1221
    move-object v1, v5

    #@28
    .local v1, arr$:[Ljava/lang/String;
    array-length v7, v1

    #@29
    .local v7, len$:I
    const/4 v3, 0x0

    #@2a
    .local v3, i$:I
    :goto_2a
    if-ge v3, v7, :cond_86

    #@2c
    aget-object v4, v1, v3

    #@2e
    .line 1222
    .local v4, iface:Ljava/lang/String;
    invoke-direct {p0, v4}, Lcom/android/server/connectivity/Tethering;->isUsb(Ljava/lang/String;)Z

    #@31
    move-result v9

    #@32
    if-eqz v9, :cond_5c

    #@34
    .line 1223
    const/4 v6, 0x0

    #@35
    .line 1225
    .local v6, ifcg:Landroid/net/InterfaceConfiguration;
    :try_start_35
    iget-object v9, p0, Lcom/android/server/connectivity/Tethering;->mNMService:Landroid/os/INetworkManagementService;

    #@37
    invoke-interface {v9, v4}, Landroid/os/INetworkManagementService;->getInterfaceConfig(Ljava/lang/String;)Landroid/net/InterfaceConfiguration;

    #@3a
    move-result-object v6

    #@3b
    .line 1226
    if-eqz v6, :cond_5c

    #@3d
    .line 1227
    const-string v9, "192.168.42.129"

    #@3f
    invoke-static {v9}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@42
    move-result-object v0

    #@43
    .line 1228
    .local v0, addr:Ljava/net/InetAddress;
    new-instance v9, Landroid/net/LinkAddress;

    #@45
    const/16 v10, 0x18

    #@47
    invoke-direct {v9, v0, v10}, Landroid/net/LinkAddress;-><init>(Ljava/net/InetAddress;I)V

    #@4a
    invoke-virtual {v6, v9}, Landroid/net/InterfaceConfiguration;->setLinkAddress(Landroid/net/LinkAddress;)V

    #@4d
    .line 1229
    if-eqz p1, :cond_68

    #@4f
    .line 1230
    invoke-virtual {v6}, Landroid/net/InterfaceConfiguration;->setInterfaceUp()V

    #@52
    .line 1234
    :goto_52
    const-string v9, "running"

    #@54
    invoke-virtual {v6, v9}, Landroid/net/InterfaceConfiguration;->clearFlag(Ljava/lang/String;)V

    #@57
    .line 1235
    iget-object v9, p0, Lcom/android/server/connectivity/Tethering;->mNMService:Landroid/os/INetworkManagementService;

    #@59
    invoke-interface {v9, v4, v6}, Landroid/os/INetworkManagementService;->setInterfaceConfig(Ljava/lang/String;Landroid/net/InterfaceConfiguration;)V
    :try_end_5c
    .catch Ljava/lang/Exception; {:try_start_35 .. :try_end_5c} :catch_6c

    #@5c
    .line 1221
    .end local v0           #addr:Ljava/net/InetAddress;
    .end local v6           #ifcg:Landroid/net/InterfaceConfiguration;
    :cond_5c
    add-int/lit8 v3, v3, 0x1

    #@5e
    goto :goto_2a

    #@5f
    .line 1217
    .end local v1           #arr$:[Ljava/lang/String;
    .end local v3           #i$:I
    .end local v4           #iface:Ljava/lang/String;
    .end local v7           #len$:I
    :catch_5f
    move-exception v2

    #@60
    .line 1218
    .local v2, e:Ljava/lang/Exception;
    const-string v9, "Tethering"

    #@62
    const-string v10, "Error listing Interfaces"

    #@64
    invoke-static {v9, v10, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@67
    .line 1244
    .end local v2           #e:Ljava/lang/Exception;
    :goto_67
    return v8

    #@68
    .line 1232
    .restart local v0       #addr:Ljava/net/InetAddress;
    .restart local v1       #arr$:[Ljava/lang/String;
    .restart local v3       #i$:I
    .restart local v4       #iface:Ljava/lang/String;
    .restart local v6       #ifcg:Landroid/net/InterfaceConfiguration;
    .restart local v7       #len$:I
    :cond_68
    :try_start_68
    invoke-virtual {v6}, Landroid/net/InterfaceConfiguration;->setInterfaceDown()V
    :try_end_6b
    .catch Ljava/lang/Exception; {:try_start_68 .. :try_end_6b} :catch_6c

    #@6b
    goto :goto_52

    #@6c
    .line 1237
    .end local v0           #addr:Ljava/net/InetAddress;
    :catch_6c
    move-exception v2

    #@6d
    .line 1238
    .restart local v2       #e:Ljava/lang/Exception;
    const-string v9, "Tethering"

    #@6f
    new-instance v10, Ljava/lang/StringBuilder;

    #@71
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@74
    const-string v11, "Error configuring interface "

    #@76
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v10

    #@7a
    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v10

    #@7e
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@81
    move-result-object v10

    #@82
    invoke-static {v9, v10, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@85
    goto :goto_67

    #@86
    .line 1244
    .end local v2           #e:Ljava/lang/Exception;
    .end local v4           #iface:Ljava/lang/String;
    .end local v6           #ifcg:Landroid/net/InterfaceConfiguration;
    :cond_86
    const/4 v8, 0x1

    #@87
    goto :goto_67
.end method

.method private forceClearTetheredNotification()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1069
    sget-object v1, Lcom/android/server/connectivity/Tethering;->mContext:Landroid/content/Context;

    #@3
    const-string v2, "notification"

    #@5
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Landroid/app/NotificationManager;

    #@b
    .line 1072
    .local v0, notificationManager:Landroid/app/NotificationManager;
    if-eqz v0, :cond_49

    #@d
    .line 1073
    iget-object v1, p0, Lcom/android/server/connectivity/Tethering;->mUSBTetheredNotification:Landroid/app/Notification;

    #@f
    if-eqz v1, :cond_1c

    #@11
    .line 1074
    iget-object v1, p0, Lcom/android/server/connectivity/Tethering;->mUSBTetheredNotification:Landroid/app/Notification;

    #@13
    iget v1, v1, Landroid/app/Notification;->icon:I

    #@15
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@17
    invoke-virtual {v0, v3, v1, v2}, Landroid/app/NotificationManager;->cancelAsUser(Ljava/lang/String;ILandroid/os/UserHandle;)V

    #@1a
    .line 1075
    iput-object v3, p0, Lcom/android/server/connectivity/Tethering;->mUSBTetheredNotification:Landroid/app/Notification;

    #@1c
    .line 1078
    :cond_1c
    iget-object v1, p0, Lcom/android/server/connectivity/Tethering;->mWiFiTetheredNotification:Landroid/app/Notification;

    #@1e
    if-eqz v1, :cond_2b

    #@20
    .line 1079
    iget-object v1, p0, Lcom/android/server/connectivity/Tethering;->mWiFiTetheredNotification:Landroid/app/Notification;

    #@22
    iget v1, v1, Landroid/app/Notification;->icon:I

    #@24
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@26
    invoke-virtual {v0, v3, v1, v2}, Landroid/app/NotificationManager;->cancelAsUser(Ljava/lang/String;ILandroid/os/UserHandle;)V

    #@29
    .line 1080
    iput-object v3, p0, Lcom/android/server/connectivity/Tethering;->mWiFiTetheredNotification:Landroid/app/Notification;

    #@2b
    .line 1083
    :cond_2b
    iget-object v1, p0, Lcom/android/server/connectivity/Tethering;->mBluetoothTetheredNotification:Landroid/app/Notification;

    #@2d
    if-eqz v1, :cond_3a

    #@2f
    .line 1084
    iget-object v1, p0, Lcom/android/server/connectivity/Tethering;->mBluetoothTetheredNotification:Landroid/app/Notification;

    #@31
    iget v1, v1, Landroid/app/Notification;->icon:I

    #@33
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@35
    invoke-virtual {v0, v3, v1, v2}, Landroid/app/NotificationManager;->cancelAsUser(Ljava/lang/String;ILandroid/os/UserHandle;)V

    #@38
    .line 1085
    iput-object v3, p0, Lcom/android/server/connectivity/Tethering;->mBluetoothTetheredNotification:Landroid/app/Notification;

    #@3a
    .line 1088
    :cond_3a
    iget-object v1, p0, Lcom/android/server/connectivity/Tethering;->mMultipleTetheredNotification:Landroid/app/Notification;

    #@3c
    if-eqz v1, :cond_49

    #@3e
    .line 1089
    iget-object v1, p0, Lcom/android/server/connectivity/Tethering;->mMultipleTetheredNotification:Landroid/app/Notification;

    #@40
    iget v1, v1, Landroid/app/Notification;->icon:I

    #@42
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@44
    invoke-virtual {v0, v3, v1, v2}, Landroid/app/NotificationManager;->cancelAsUser(Ljava/lang/String;ILandroid/os/UserHandle;)V

    #@47
    .line 1090
    iput-object v3, p0, Lcom/android/server/connectivity/Tethering;->mMultipleTetheredNotification:Landroid/app/Notification;

    #@49
    .line 1093
    :cond_49
    return-void
.end method

.method private isUsb(Ljava/lang/String;)Z
    .registers 8
    .parameter "iface"

    #@0
    .prologue
    .line 513
    iget-object v5, p0, Lcom/android/server/connectivity/Tethering;->mPublicSync:Ljava/lang/Object;

    #@2
    monitor-enter v5

    #@3
    .line 514
    :try_start_3
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mTetherableUsbRegexs:[Ljava/lang/String;

    #@5
    .local v0, arr$:[Ljava/lang/String;
    array-length v2, v0

    #@6
    .local v2, len$:I
    const/4 v1, 0x0

    #@7
    .local v1, i$:I
    :goto_7
    if-ge v1, v2, :cond_17

    #@9
    aget-object v3, v0, v1

    #@b
    .line 515
    .local v3, regex:Ljava/lang/String;
    invoke-virtual {p1, v3}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    #@e
    move-result v4

    #@f
    if-eqz v4, :cond_14

    #@11
    const/4 v4, 0x1

    #@12
    monitor-exit v5

    #@13
    .line 517
    .end local v3           #regex:Ljava/lang/String;
    :goto_13
    return v4

    #@14
    .line 514
    .restart local v3       #regex:Ljava/lang/String;
    :cond_14
    add-int/lit8 v1, v1, 0x1

    #@16
    goto :goto_7

    #@17
    .line 517
    .end local v3           #regex:Ljava/lang/String;
    :cond_17
    const/4 v4, 0x0

    #@18
    monitor-exit v5

    #@19
    goto :goto_13

    #@1a
    .line 518
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #i$:I
    .end local v2           #len$:I
    :catchall_1a
    move-exception v4

    #@1b
    monitor-exit v5
    :try_end_1c
    .catchall {:try_start_3 .. :try_end_1c} :catchall_1a

    #@1c
    throw v4
.end method

.method private resetAlarm()V
    .registers 3

    #@0
    .prologue
    .line 2915
    const-string v0, "Tethering"

    #@2
    const-string v1, "[EntitlementCheck] Tethering End   reset Alarm "

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 2916
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mManager:Landroid/app/AlarmManager;

    #@9
    iget-object v1, p0, Lcom/android/server/connectivity/Tethering;->mAlarmSender:Landroid/app/PendingIntent;

    #@b
    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    #@e
    .line 2917
    return-void
.end method

.method private sendTetherStateChangedBroadcast()V
    .registers 18

    #@0
    .prologue
    .line 733
    :try_start_0
    move-object/from16 v0, p0

    #@2
    iget-object v14, v0, Lcom/android/server/connectivity/Tethering;->mConnService:Landroid/net/IConnectivityManager;

    #@4
    invoke-interface {v14}, Landroid/net/IConnectivityManager;->isTetheringSupported()Z
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_b

    #@7
    move-result v14

    #@8
    if-nez v14, :cond_d

    #@a
    .line 837
    :goto_a
    return-void

    #@b
    .line 734
    :catch_b
    move-exception v5

    #@c
    .line 735
    .local v5, e:Landroid/os/RemoteException;
    goto :goto_a

    #@d
    .line 738
    .end local v5           #e:Landroid/os/RemoteException;
    :cond_d
    new-instance v2, Ljava/util/ArrayList;

    #@f
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@12
    .line 739
    .local v2, availableList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v1, Ljava/util/ArrayList;

    #@14
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@17
    .line 740
    .local v1, activeList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v6, Ljava/util/ArrayList;

    #@19
    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    #@1c
    .line 742
    .local v6, erroredList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v13, 0x0

    #@1d
    .line 743
    .local v13, wifiTethered:Z
    const/4 v12, 0x0

    #@1e
    .line 744
    .local v12, usbTethered:Z
    const/4 v3, 0x0

    #@1f
    .line 746
    .local v3, bluetoothTethered:Z
    move-object/from16 v0, p0

    #@21
    iget-object v15, v0, Lcom/android/server/connectivity/Tethering;->mPublicSync:Ljava/lang/Object;

    #@23
    monitor-enter v15

    #@24
    .line 747
    :try_start_24
    move-object/from16 v0, p0

    #@26
    iget-object v14, v0, Lcom/android/server/connectivity/Tethering;->mIfaces:Ljava/util/HashMap;

    #@28
    invoke-virtual {v14}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@2b
    move-result-object v9

    #@2c
    .line 748
    .local v9, ifaces:Ljava/util/Set;
    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@2f
    move-result-object v7

    #@30
    .local v7, i$:Ljava/util/Iterator;
    :cond_30
    :goto_30
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    #@33
    move-result v14

    #@34
    if-eqz v14, :cond_96

    #@36
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@39
    move-result-object v8

    #@3a
    .line 749
    .local v8, iface:Ljava/lang/Object;
    move-object/from16 v0, p0

    #@3c
    iget-object v14, v0, Lcom/android/server/connectivity/Tethering;->mIfaces:Ljava/util/HashMap;

    #@3e
    invoke-virtual {v14, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@41
    move-result-object v11

    #@42
    check-cast v11, Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;

    #@44
    .line 750
    .local v11, sm:Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;
    if-eqz v11, :cond_30

    #@46
    .line 751
    invoke-virtual {v11}, Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;->isErrored()Z

    #@49
    move-result v14

    #@4a
    if-eqz v14, :cond_55

    #@4c
    .line 752
    check-cast v8, Ljava/lang/String;

    #@4e
    .end local v8           #iface:Ljava/lang/Object;
    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@51
    goto :goto_30

    #@52
    .line 767
    .end local v7           #i$:Ljava/util/Iterator;
    .end local v9           #ifaces:Ljava/util/Set;
    .end local v11           #sm:Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;
    :catchall_52
    move-exception v14

    #@53
    monitor-exit v15
    :try_end_54
    .catchall {:try_start_24 .. :try_end_54} :catchall_52

    #@54
    throw v14

    #@55
    .line 753
    .restart local v7       #i$:Ljava/util/Iterator;
    .restart local v8       #iface:Ljava/lang/Object;
    .restart local v9       #ifaces:Ljava/util/Set;
    .restart local v11       #sm:Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;
    :cond_55
    :try_start_55
    invoke-virtual {v11}, Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;->isAvailable()Z

    #@58
    move-result v14

    #@59
    if-eqz v14, :cond_61

    #@5b
    .line 754
    check-cast v8, Ljava/lang/String;

    #@5d
    .end local v8           #iface:Ljava/lang/Object;
    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@60
    goto :goto_30

    #@61
    .line 755
    .restart local v8       #iface:Ljava/lang/Object;
    :cond_61
    invoke-virtual {v11}, Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;->isTethered()Z

    #@64
    move-result v14

    #@65
    if-eqz v14, :cond_30

    #@67
    .line 756
    move-object v0, v8

    #@68
    check-cast v0, Ljava/lang/String;

    #@6a
    move-object v14, v0

    #@6b
    move-object/from16 v0, p0

    #@6d
    invoke-direct {v0, v14}, Lcom/android/server/connectivity/Tethering;->isUsb(Ljava/lang/String;)Z

    #@70
    move-result v14

    #@71
    if-eqz v14, :cond_7a

    #@73
    .line 757
    const/4 v12, 0x1

    #@74
    .line 763
    :cond_74
    :goto_74
    check-cast v8, Ljava/lang/String;

    #@76
    .end local v8           #iface:Ljava/lang/Object;
    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@79
    goto :goto_30

    #@7a
    .line 758
    .restart local v8       #iface:Ljava/lang/Object;
    :cond_7a
    move-object v0, v8

    #@7b
    check-cast v0, Ljava/lang/String;

    #@7d
    move-object v14, v0

    #@7e
    move-object/from16 v0, p0

    #@80
    invoke-virtual {v0, v14}, Lcom/android/server/connectivity/Tethering;->isWifi(Ljava/lang/String;)Z

    #@83
    move-result v14

    #@84
    if-eqz v14, :cond_88

    #@86
    .line 759
    const/4 v13, 0x1

    #@87
    goto :goto_74

    #@88
    .line 760
    :cond_88
    move-object v0, v8

    #@89
    check-cast v0, Ljava/lang/String;

    #@8b
    move-object v14, v0

    #@8c
    move-object/from16 v0, p0

    #@8e
    invoke-virtual {v0, v14}, Lcom/android/server/connectivity/Tethering;->isBluetooth(Ljava/lang/String;)Z

    #@91
    move-result v14

    #@92
    if-eqz v14, :cond_74

    #@94
    .line 761
    const/4 v3, 0x1

    #@95
    goto :goto_74

    #@96
    .line 767
    .end local v8           #iface:Ljava/lang/Object;
    .end local v11           #sm:Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;
    :cond_96
    monitor-exit v15
    :try_end_97
    .catchall {:try_start_55 .. :try_end_97} :catchall_52

    #@97
    .line 769
    move-object/from16 v0, p0

    #@99
    iput-boolean v13, v0, Lcom/android/server/connectivity/Tethering;->mWifiTethered:Z

    #@9b
    .line 770
    move-object/from16 v0, p0

    #@9d
    iput-boolean v12, v0, Lcom/android/server/connectivity/Tethering;->mUsbTethered:Z

    #@9f
    .line 771
    move-object/from16 v0, p0

    #@a1
    iput-boolean v3, v0, Lcom/android/server/connectivity/Tethering;->mBluetoothTethered:Z

    #@a3
    .line 773
    new-instance v4, Landroid/content/Intent;

    #@a5
    const-string v14, "android.net.conn.TETHER_STATE_CHANGED"

    #@a7
    invoke-direct {v4, v14}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@aa
    .line 774
    .local v4, broadcast:Landroid/content/Intent;
    const/high16 v14, 0x2800

    #@ac
    invoke-virtual {v4, v14}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@af
    .line 776
    const-string v14, "availableArray"

    #@b1
    invoke-virtual {v4, v14, v2}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    #@b4
    .line 778
    const-string v14, "activeArray"

    #@b6
    invoke-virtual {v4, v14, v1}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    #@b9
    .line 779
    const-string v14, "erroredArray"

    #@bb
    invoke-virtual {v4, v14, v6}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    #@be
    .line 781
    sget-object v14, Lcom/android/server/connectivity/Tethering;->mContext:Landroid/content/Context;

    #@c0
    sget-object v15, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@c2
    invoke-virtual {v14, v4, v15}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@c5
    .line 783
    const-string v14, "Tethering"

    #@c7
    new-instance v15, Ljava/lang/StringBuilder;

    #@c9
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@cc
    const-string v16, "sendTetherStateChangedBroadcast "

    #@ce
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d1
    move-result-object v15

    #@d2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@d5
    move-result v16

    #@d6
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@d9
    move-result-object v15

    #@da
    const-string v16, ", "

    #@dc
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@df
    move-result-object v15

    #@e0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@e3
    move-result v16

    #@e4
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e7
    move-result-object v15

    #@e8
    const-string v16, ", "

    #@ea
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ed
    move-result-object v15

    #@ee
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@f1
    move-result v16

    #@f2
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f5
    move-result-object v15

    #@f6
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f9
    move-result-object v15

    #@fa
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@fd
    .line 788
    move-object/from16 v0, p0

    #@ff
    iget-object v14, v0, Lcom/android/server/connectivity/Tethering;->TARGET_OPERATOR:Ljava/lang/String;

    #@101
    const-string v15, "SPR"

    #@103
    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@106
    move-result v14

    #@107
    if-nez v14, :cond_121

    #@109
    move-object/from16 v0, p0

    #@10b
    iget-object v14, v0, Lcom/android/server/connectivity/Tethering;->TARGET_OPERATOR:Ljava/lang/String;

    #@10d
    const-string v15, "TMO"

    #@10f
    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@112
    move-result v14

    #@113
    if-eqz v14, :cond_143

    #@115
    move-object/from16 v0, p0

    #@117
    iget-object v14, v0, Lcom/android/server/connectivity/Tethering;->TARGET_COUNTRY:Ljava/lang/String;

    #@119
    const-string v15, "US"

    #@11b
    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11e
    move-result v14

    #@11f
    if-eqz v14, :cond_143

    #@121
    .line 790
    :cond_121
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@124
    move-result v14

    #@125
    const/4 v15, 0x1

    #@126
    if-le v14, v15, :cond_135

    #@128
    .line 791
    invoke-direct/range {p0 .. p0}, Lcom/android/server/connectivity/Tethering;->forceClearTetheredNotification()V

    #@12b
    .line 792
    invoke-direct/range {p0 .. p0}, Lcom/android/server/connectivity/Tethering;->showMultipleTetheredNotification()V

    #@12e
    .line 793
    const/4 v14, 0x1

    #@12f
    move-object/from16 v0, p0

    #@131
    iput-boolean v14, v0, Lcom/android/server/connectivity/Tethering;->multipleTethered:Z

    #@133
    goto/16 :goto_a

    #@135
    .line 796
    :cond_135
    move-object/from16 v0, p0

    #@137
    iget-boolean v14, v0, Lcom/android/server/connectivity/Tethering;->multipleTethered:Z

    #@139
    if-eqz v14, :cond_143

    #@13b
    .line 797
    invoke-direct/range {p0 .. p0}, Lcom/android/server/connectivity/Tethering;->forceClearTetheredNotification()V

    #@13e
    .line 798
    const/4 v14, 0x0

    #@13f
    move-object/from16 v0, p0

    #@141
    iput-boolean v14, v0, Lcom/android/server/connectivity/Tethering;->multipleTethered:Z

    #@143
    .line 806
    :cond_143
    if-eqz v12, :cond_18a

    #@145
    move-object/from16 v0, p0

    #@147
    iget-boolean v14, v0, Lcom/android/server/connectivity/Tethering;->mNcmEnabled:Z

    #@149
    if-nez v14, :cond_18a

    #@14b
    move-object/from16 v0, p0

    #@14d
    iget-object v14, v0, Lcom/android/server/connectivity/Tethering;->TARGET_OPERATOR:Ljava/lang/String;

    #@14f
    const-string v15, "VZW"

    #@151
    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@154
    move-result v14

    #@155
    if-nez v14, :cond_187

    #@157
    move-object/from16 v0, p0

    #@159
    iget-object v14, v0, Lcom/android/server/connectivity/Tethering;->TARGET_OPERATOR:Ljava/lang/String;

    #@15b
    const-string v15, "SPR"

    #@15d
    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@160
    move-result v14

    #@161
    if-nez v14, :cond_187

    #@163
    move-object/from16 v0, p0

    #@165
    iget-object v14, v0, Lcom/android/server/connectivity/Tethering;->TARGET_OPERATOR:Ljava/lang/String;

    #@167
    const-string v15, "TMO"

    #@169
    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@16c
    move-result v14

    #@16d
    if-eqz v14, :cond_17b

    #@16f
    move-object/from16 v0, p0

    #@171
    iget-object v14, v0, Lcom/android/server/connectivity/Tethering;->TARGET_COUNTRY:Ljava/lang/String;

    #@173
    const-string v15, "US"

    #@175
    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@178
    move-result v14

    #@179
    if-nez v14, :cond_187

    #@17b
    :cond_17b
    move-object/from16 v0, p0

    #@17d
    iget-object v14, v0, Lcom/android/server/connectivity/Tethering;->TARGET_OPERATOR:Ljava/lang/String;

    #@17f
    const-string v15, "DCM"

    #@181
    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@184
    move-result v14

    #@185
    if-eqz v14, :cond_18a

    #@187
    .line 810
    :cond_187
    invoke-direct/range {p0 .. p0}, Lcom/android/server/connectivity/Tethering;->showUSBTetheredNotification()V

    #@18a
    .line 814
    :cond_18a
    if-eqz v13, :cond_1d9

    #@18c
    move-object/from16 v0, p0

    #@18e
    iget-object v14, v0, Lcom/android/server/connectivity/Tethering;->TARGET_OPERATOR:Ljava/lang/String;

    #@190
    const-string v15, "VZW"

    #@192
    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@195
    move-result v14

    #@196
    if-nez v14, :cond_1d9

    #@198
    .line 815
    move-object/from16 v0, p0

    #@19a
    iget-object v14, v0, Lcom/android/server/connectivity/Tethering;->TARGET_OPERATOR:Ljava/lang/String;

    #@19c
    const-string v15, "TMO"

    #@19e
    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1a1
    move-result v14

    #@1a2
    if-nez v14, :cond_204

    #@1a4
    sget-boolean v14, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@1a6
    if-eqz v14, :cond_204

    #@1a8
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->doesSupportHotspotList()Z

    #@1ab
    move-result v14

    #@1ac
    if-eqz v14, :cond_204

    #@1ae
    .line 817
    move-object/from16 v0, p0

    #@1b0
    iput-boolean v13, v0, Lcom/android/server/connectivity/Tethering;->sWifiTethered:Z

    #@1b2
    .line 818
    invoke-static {}, Lcom/lge/wifi_iface/WifiIfaceManager;->getWifiSapIfaceIface()Lcom/lge/wifi_iface/WifiSapIfaceIface;

    #@1b5
    move-result-object v14

    #@1b6
    invoke-interface {v14}, Lcom/lge/wifi_iface/WifiSapIfaceIface;->getAllAssocMacListATT()Ljava/util/List;

    #@1b9
    move-result-object v14

    #@1ba
    invoke-interface {v14}, Ljava/util/List;->size()I

    #@1bd
    move-result v10

    #@1be
    .line 820
    .local v10, index:I
    if-lez v10, :cond_1e3

    #@1c0
    sget-object v14, Lcom/android/server/connectivity/Tethering;->mContext:Landroid/content/Context;

    #@1c2
    invoke-virtual {v14}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1c5
    move-result-object v14

    #@1c6
    const-string v15, "wifi_ap_current_max_client"

    #@1c8
    const/16 v16, 0x2

    #@1ca
    invoke-static/range {v14 .. v16}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@1cd
    move-result v14

    #@1ce
    if-ge v10, v14, :cond_1e3

    #@1d0
    .line 821
    sget-object v14, Lcom/android/server/connectivity/Tethering;->sTetherWifiSta:[I

    #@1d2
    aget v14, v14, v10

    #@1d4
    move-object/from16 v0, p0

    #@1d6
    invoke-direct {v0, v14}, Lcom/android/server/connectivity/Tethering;->showWiFiTetheredNotification(I)V

    #@1d9
    .line 833
    .end local v10           #index:I
    :cond_1d9
    :goto_1d9
    if-eqz v3, :cond_1de

    #@1db
    .line 834
    invoke-direct/range {p0 .. p0}, Lcom/android/server/connectivity/Tethering;->showBluetoothTetheredNotification()V

    #@1de
    .line 835
    :cond_1de
    invoke-direct/range {p0 .. p0}, Lcom/android/server/connectivity/Tethering;->clearTetheredNotification()V

    #@1e1
    goto/16 :goto_a

    #@1e3
    .line 823
    .restart local v10       #index:I
    :cond_1e3
    const-string v14, "Tethering"

    #@1e5
    new-instance v15, Ljava/lang/StringBuilder;

    #@1e7
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@1ea
    const-string v16, "sTetherWifiSta Index Out of Bound index = "

    #@1ec
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ef
    move-result-object v15

    #@1f0
    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f3
    move-result-object v15

    #@1f4
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f7
    move-result-object v15

    #@1f8
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1fb
    .line 824
    const v14, 0x20205fe

    #@1fe
    move-object/from16 v0, p0

    #@200
    invoke-direct {v0, v14}, Lcom/android/server/connectivity/Tethering;->showWiFiTetheredNotification(I)V

    #@203
    goto :goto_1d9

    #@204
    .line 826
    .end local v10           #index:I
    :cond_204
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getOperator()Ljava/lang/String;

    #@207
    move-result-object v14

    #@208
    const-string v15, "TMO"

    #@20a
    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@20d
    move-result v14

    #@20e
    if-eqz v14, :cond_225

    #@210
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getCountry()Ljava/lang/String;

    #@213
    move-result-object v14

    #@214
    const-string v15, "US"

    #@216
    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@219
    move-result v14

    #@21a
    if-eqz v14, :cond_225

    #@21c
    .line 827
    const v14, 0x2020292

    #@21f
    move-object/from16 v0, p0

    #@221
    invoke-direct {v0, v14}, Lcom/android/server/connectivity/Tethering;->showWiFiTetheredNotification(I)V

    #@224
    goto :goto_1d9

    #@225
    .line 829
    :cond_225
    const v14, 0x108056f

    #@228
    move-object/from16 v0, p0

    #@22a
    invoke-direct {v0, v14}, Lcom/android/server/connectivity/Tethering;->showWiFiTetheredNotification(I)V

    #@22d
    goto :goto_1d9
.end method

.method private setAlarm()V
    .registers 9

    #@0
    .prologue
    .line 2907
    const-string v0, "Tethering"

    #@2
    const-string v1, "[EntitlementCheck] Tethering Start\tset Alarm "

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 2908
    sget-object v0, Lcom/android/server/connectivity/Tethering;->mContext:Landroid/content/Context;

    #@9
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@c
    move-result-object v0

    #@d
    const-string v1, "tether_entitlement_check_interval"

    #@f
    const/16 v2, 0x5a0

    #@11
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@14
    move-result v7

    #@15
    .line 2909
    .local v7, intervalMin:I
    const v0, 0xea60

    #@18
    mul-int/2addr v0, v7

    #@19
    int-to-long v4, v0

    #@1a
    .line 2911
    .local v4, interval:J
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mManager:Landroid/app/AlarmManager;

    #@1c
    const/4 v1, 0x2

    #@1d
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@20
    move-result-wide v2

    #@21
    add-long/2addr v2, v4

    #@22
    iget-object v6, p0, Lcom/android/server/connectivity/Tethering;->mAlarmSender:Landroid/app/PendingIntent;

    #@24
    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V

    #@27
    .line 2912
    return-void
.end method

.method private showBluetoothTetheredNotification()V
    .registers 14

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 944
    sget-object v0, Lcom/android/server/connectivity/Tethering;->mContext:Landroid/content/Context;

    #@4
    const-string v3, "notification"

    #@6
    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@9
    move-result-object v7

    #@a
    check-cast v7, Landroid/app/NotificationManager;

    #@c
    .line 946
    .local v7, notificationManager:Landroid/app/NotificationManager;
    if-nez v7, :cond_f

    #@e
    .line 990
    :cond_e
    :goto_e
    return-void

    #@f
    .line 950
    :cond_f
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mBluetoothTetheredNotification:Landroid/app/Notification;

    #@11
    if-nez v0, :cond_e

    #@13
    .line 954
    new-instance v2, Landroid/content/Intent;

    #@15
    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    #@18
    .line 957
    .local v2, intent:Landroid/content/Intent;
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->TARGET_OPERATOR:Ljava/lang/String;

    #@1a
    const-string v3, "DCM"

    #@1c
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1f
    move-result v0

    #@20
    if-eqz v0, :cond_94

    #@22
    .line 958
    const-string v0, "com.android.settings"

    #@24
    const-string v3, "com.android.settings.Settings$WirelessSettingsActivity"

    #@26
    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@29
    .line 966
    :goto_29
    const v0, 0x10808000

    #@2c
    invoke-virtual {v2, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@2f
    .line 968
    sget-object v0, Lcom/android/server/connectivity/Tethering;->mContext:Landroid/content/Context;

    #@31
    sget-object v5, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    #@33
    move v3, v1

    #@34
    invoke-static/range {v0 .. v5}, Landroid/app/PendingIntent;->getActivityAsUser(Landroid/content/Context;ILandroid/content/Intent;ILandroid/os/Bundle;Landroid/os/UserHandle;)Landroid/app/PendingIntent;

    #@37
    move-result-object v8

    #@38
    .line 971
    .local v8, pi:Landroid/app/PendingIntent;
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@3b
    move-result-object v9

    #@3c
    .line 972
    .local v9, r:Landroid/content/res/Resources;
    const v0, 0x2090152

    #@3f
    invoke-virtual {v9, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@42
    move-result-object v10

    #@43
    .line 973
    .local v10, title:Ljava/lang/CharSequence;
    const v0, 0x10404be

    #@46
    invoke-virtual {v9, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@49
    move-result-object v6

    #@4a
    .line 976
    .local v6, message:Ljava/lang/CharSequence;
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->TARGET_OPERATOR:Ljava/lang/String;

    #@4c
    const-string v1, "VZW"

    #@4e
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@51
    move-result v0

    #@52
    if-eqz v0, :cond_5b

    #@54
    .line 977
    const v0, 0x20903b6

    #@57
    invoke-virtual {v9, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@5a
    move-result-object v6

    #@5b
    .line 981
    :cond_5b
    new-instance v0, Landroid/app/Notification;

    #@5d
    invoke-direct {v0}, Landroid/app/Notification;-><init>()V

    #@60
    iput-object v0, p0, Lcom/android/server/connectivity/Tethering;->mBluetoothTetheredNotification:Landroid/app/Notification;

    #@62
    .line 982
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mBluetoothTetheredNotification:Landroid/app/Notification;

    #@64
    const-wide/16 v11, 0x0

    #@66
    iput-wide v11, v0, Landroid/app/Notification;->when:J

    #@68
    .line 983
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mBluetoothTetheredNotification:Landroid/app/Notification;

    #@6a
    const v1, 0x108056c

    #@6d
    iput v1, v0, Landroid/app/Notification;->icon:I

    #@6f
    .line 984
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mBluetoothTetheredNotification:Landroid/app/Notification;

    #@71
    iget v1, v0, Landroid/app/Notification;->defaults:I

    #@73
    and-int/lit8 v1, v1, -0x2

    #@75
    iput v1, v0, Landroid/app/Notification;->defaults:I

    #@77
    .line 985
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mBluetoothTetheredNotification:Landroid/app/Notification;

    #@79
    const/4 v1, 0x2

    #@7a
    iput v1, v0, Landroid/app/Notification;->flags:I

    #@7c
    .line 986
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mBluetoothTetheredNotification:Landroid/app/Notification;

    #@7e
    iput-object v10, v0, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    #@80
    .line 987
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mBluetoothTetheredNotification:Landroid/app/Notification;

    #@82
    sget-object v1, Lcom/android/server/connectivity/Tethering;->mContext:Landroid/content/Context;

    #@84
    invoke-virtual {v0, v1, v10, v6, v8}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    #@87
    .line 989
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mBluetoothTetheredNotification:Landroid/app/Notification;

    #@89
    iget v0, v0, Landroid/app/Notification;->icon:I

    #@8b
    iget-object v1, p0, Lcom/android/server/connectivity/Tethering;->mBluetoothTetheredNotification:Landroid/app/Notification;

    #@8d
    sget-object v3, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@8f
    invoke-virtual {v7, v4, v0, v1, v3}, Landroid/app/NotificationManager;->notifyAsUser(Ljava/lang/String;ILandroid/app/Notification;Landroid/os/UserHandle;)V

    #@92
    goto/16 :goto_e

    #@94
    .line 962
    .end local v6           #message:Ljava/lang/CharSequence;
    .end local v8           #pi:Landroid/app/PendingIntent;
    .end local v9           #r:Landroid/content/res/Resources;
    .end local v10           #title:Ljava/lang/CharSequence;
    :cond_94
    const-string v0, "com.android.settings"

    #@96
    const-string v3, "com.android.settings.Settings$TetherNetworkSettingsActivity"

    #@98
    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@9b
    goto :goto_29
.end method

.method private showMaxClientReachedNotification(I)V
    .registers 11
    .parameter "icon"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 2863
    const-string v6, "Tethering"

    #@3
    const-string v7, "[Tethering]\tCreate New Notification !!!! "

    #@5
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 2864
    sget-object v6, Lcom/android/server/connectivity/Tethering;->mContext:Landroid/content/Context;

    #@a
    const-string v7, "notification"

    #@c
    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@f
    move-result-object v2

    #@10
    check-cast v2, Landroid/app/NotificationManager;

    #@12
    .line 2867
    .local v2, notificationManager:Landroid/app/NotificationManager;
    invoke-virtual {p0}, Lcom/android/server/connectivity/Tethering;->clearTetheredBlockNotification()V

    #@15
    .line 2869
    if-nez v2, :cond_18

    #@17
    .line 2902
    :goto_17
    return-void

    #@18
    .line 2873
    :cond_18
    iget-object v6, p0, Lcom/android/server/connectivity/Tethering;->mPermissionErrorNotification:Landroid/app/Notification;

    #@1a
    if-eqz v6, :cond_24

    #@1c
    .line 2874
    const-string v6, "Tethering"

    #@1e
    const-string v7, "[TetherSettings]  Notification Showed Already !!!! "

    #@20
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    goto :goto_17

    #@24
    .line 2878
    :cond_24
    new-instance v0, Landroid/content/Intent;

    #@26
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    #@29
    .line 2879
    .local v0, intent:Landroid/content/Intent;
    const-string v6, "com.android.settings"

    #@2b
    const-string v7, "com.android.settings.TetherSettings"

    #@2d
    invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@30
    .line 2880
    const/high16 v6, 0x4000

    #@32
    invoke-virtual {v0, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@35
    .line 2882
    sget-object v6, Lcom/android/server/connectivity/Tethering;->mContext:Landroid/content/Context;

    #@37
    invoke-static {v6, v8, v0, v8}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@3a
    move-result-object v3

    #@3b
    .line 2884
    .local v3, pi:Landroid/app/PendingIntent;
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@3e
    move-result-object v4

    #@3f
    .line 2887
    .local v4, r:Landroid/content/res/Resources;
    const v6, 0x2090380

    #@42
    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@45
    move-result-object v5

    #@46
    .line 2888
    .local v5, title:Ljava/lang/CharSequence;
    const v6, 0x2090381

    #@49
    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@4c
    move-result-object v1

    #@4d
    .line 2890
    .local v1, message:Ljava/lang/CharSequence;
    iget-object v6, p0, Lcom/android/server/connectivity/Tethering;->mPermissionErrorNotification:Landroid/app/Notification;

    #@4f
    if-nez v6, :cond_65

    #@51
    .line 2891
    const-string v6, "Tethering"

    #@53
    const-string v7, "[TetherSettings]  Create New Notification !!!! "

    #@55
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@58
    .line 2892
    new-instance v6, Landroid/app/Notification;

    #@5a
    invoke-direct {v6}, Landroid/app/Notification;-><init>()V

    #@5d
    iput-object v6, p0, Lcom/android/server/connectivity/Tethering;->mPermissionErrorNotification:Landroid/app/Notification;

    #@5f
    .line 2893
    iget-object v6, p0, Lcom/android/server/connectivity/Tethering;->mPermissionErrorNotification:Landroid/app/Notification;

    #@61
    const-wide/16 v7, 0x0

    #@63
    iput-wide v7, v6, Landroid/app/Notification;->when:J

    #@65
    .line 2895
    :cond_65
    iget-object v6, p0, Lcom/android/server/connectivity/Tethering;->mPermissionErrorNotification:Landroid/app/Notification;

    #@67
    iput p1, v6, Landroid/app/Notification;->icon:I

    #@69
    .line 2896
    iget-object v6, p0, Lcom/android/server/connectivity/Tethering;->mPermissionErrorNotification:Landroid/app/Notification;

    #@6b
    iget v7, v6, Landroid/app/Notification;->defaults:I

    #@6d
    and-int/lit8 v7, v7, -0x2

    #@6f
    iput v7, v6, Landroid/app/Notification;->defaults:I

    #@71
    .line 2897
    iget-object v6, p0, Lcom/android/server/connectivity/Tethering;->mPermissionErrorNotification:Landroid/app/Notification;

    #@73
    const/16 v7, 0x10

    #@75
    iput v7, v6, Landroid/app/Notification;->flags:I

    #@77
    .line 2898
    iget-object v6, p0, Lcom/android/server/connectivity/Tethering;->mPermissionErrorNotification:Landroid/app/Notification;

    #@79
    iput-object v5, v6, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    #@7b
    .line 2899
    iget-object v6, p0, Lcom/android/server/connectivity/Tethering;->mPermissionErrorNotification:Landroid/app/Notification;

    #@7d
    sget-object v7, Lcom/android/server/connectivity/Tethering;->mContext:Landroid/content/Context;

    #@7f
    invoke-virtual {v6, v7, v5, v1, v3}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    #@82
    .line 2901
    iget-object v6, p0, Lcom/android/server/connectivity/Tethering;->mPermissionErrorNotification:Landroid/app/Notification;

    #@84
    iget v6, v6, Landroid/app/Notification;->icon:I

    #@86
    iget-object v7, p0, Lcom/android/server/connectivity/Tethering;->mPermissionErrorNotification:Landroid/app/Notification;

    #@88
    invoke-virtual {v2, v6, v7}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    #@8b
    goto :goto_17
.end method

.method private showMultipleTetheredNotification()V
    .registers 14

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 842
    sget-object v0, Lcom/android/server/connectivity/Tethering;->mContext:Landroid/content/Context;

    #@4
    const-string v3, "notification"

    #@6
    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@9
    move-result-object v7

    #@a
    check-cast v7, Landroid/app/NotificationManager;

    #@c
    .line 844
    .local v7, notificationManager:Landroid/app/NotificationManager;
    if-nez v7, :cond_f

    #@e
    .line 873
    :cond_e
    :goto_e
    return-void

    #@f
    .line 848
    :cond_f
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mMultipleTetheredNotification:Landroid/app/Notification;

    #@11
    if-nez v0, :cond_e

    #@13
    .line 852
    new-instance v2, Landroid/content/Intent;

    #@15
    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    #@18
    .line 853
    .local v2, intent:Landroid/content/Intent;
    const-string v0, "com.android.settings"

    #@1a
    const-string v3, "com.android.settings.Settings$TetherNetworkSettingsActivity"

    #@1c
    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@1f
    .line 854
    const v0, 0x10808000

    #@22
    invoke-virtual {v2, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@25
    .line 856
    sget-object v0, Lcom/android/server/connectivity/Tethering;->mContext:Landroid/content/Context;

    #@27
    sget-object v5, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    #@29
    move v3, v1

    #@2a
    invoke-static/range {v0 .. v5}, Landroid/app/PendingIntent;->getActivityAsUser(Landroid/content/Context;ILandroid/content/Intent;ILandroid/os/Bundle;Landroid/os/UserHandle;)Landroid/app/PendingIntent;

    #@2d
    move-result-object v8

    #@2e
    .line 857
    .local v8, pi:Landroid/app/PendingIntent;
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@31
    move-result-object v9

    #@32
    .line 858
    .local v9, r:Landroid/content/res/Resources;
    const v0, 0x10404bd

    #@35
    invoke-virtual {v9, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@38
    move-result-object v10

    #@39
    .line 859
    .local v10, title:Ljava/lang/CharSequence;
    const v0, 0x10404be

    #@3c
    invoke-virtual {v9, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@3f
    move-result-object v6

    #@40
    .line 861
    .local v6, message:Ljava/lang/CharSequence;
    new-instance v0, Landroid/app/Notification;

    #@42
    invoke-direct {v0}, Landroid/app/Notification;-><init>()V

    #@45
    iput-object v0, p0, Lcom/android/server/connectivity/Tethering;->mMultipleTetheredNotification:Landroid/app/Notification;

    #@47
    .line 862
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mMultipleTetheredNotification:Landroid/app/Notification;

    #@49
    const-wide/16 v11, 0x0

    #@4b
    iput-wide v11, v0, Landroid/app/Notification;->when:J

    #@4d
    .line 863
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->TARGET_OPERATOR:Ljava/lang/String;

    #@4f
    const-string v1, "TMO"

    #@51
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@54
    move-result v0

    #@55
    if-eqz v0, :cond_8c

    #@57
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->TARGET_COUNTRY:Ljava/lang/String;

    #@59
    const-string v1, "US"

    #@5b
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5e
    move-result v0

    #@5f
    if-eqz v0, :cond_8c

    #@61
    .line 864
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mMultipleTetheredNotification:Landroid/app/Notification;

    #@63
    const v1, 0x2020292

    #@66
    iput v1, v0, Landroid/app/Notification;->icon:I

    #@68
    .line 868
    :goto_68
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mMultipleTetheredNotification:Landroid/app/Notification;

    #@6a
    iget v1, v0, Landroid/app/Notification;->defaults:I

    #@6c
    and-int/lit8 v1, v1, -0x2

    #@6e
    iput v1, v0, Landroid/app/Notification;->defaults:I

    #@70
    .line 869
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mMultipleTetheredNotification:Landroid/app/Notification;

    #@72
    const/4 v1, 0x2

    #@73
    iput v1, v0, Landroid/app/Notification;->flags:I

    #@75
    .line 870
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mMultipleTetheredNotification:Landroid/app/Notification;

    #@77
    iput-object v10, v0, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    #@79
    .line 871
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mMultipleTetheredNotification:Landroid/app/Notification;

    #@7b
    sget-object v1, Lcom/android/server/connectivity/Tethering;->mContext:Landroid/content/Context;

    #@7d
    invoke-virtual {v0, v1, v10, v6, v8}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    #@80
    .line 872
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mMultipleTetheredNotification:Landroid/app/Notification;

    #@82
    iget v0, v0, Landroid/app/Notification;->icon:I

    #@84
    iget-object v1, p0, Lcom/android/server/connectivity/Tethering;->mMultipleTetheredNotification:Landroid/app/Notification;

    #@86
    sget-object v3, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@88
    invoke-virtual {v7, v4, v0, v1, v3}, Landroid/app/NotificationManager;->notifyAsUser(Ljava/lang/String;ILandroid/app/Notification;Landroid/os/UserHandle;)V

    #@8b
    goto :goto_e

    #@8c
    .line 866
    :cond_8c
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mMultipleTetheredNotification:Landroid/app/Notification;

    #@8e
    const v1, 0x108056e

    #@91
    iput v1, v0, Landroid/app/Notification;->icon:I

    #@93
    goto :goto_68
.end method

.method private showUSBTetheredNotification()V
    .registers 14

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 884
    sget-object v0, Lcom/android/server/connectivity/Tethering;->mContext:Landroid/content/Context;

    #@4
    const-string v3, "notification"

    #@6
    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@9
    move-result-object v7

    #@a
    check-cast v7, Landroid/app/NotificationManager;

    #@c
    .line 886
    .local v7, notificationManager:Landroid/app/NotificationManager;
    if-nez v7, :cond_f

    #@e
    .line 939
    :cond_e
    :goto_e
    return-void

    #@f
    .line 890
    :cond_f
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mUSBTetheredNotification:Landroid/app/Notification;

    #@11
    if-nez v0, :cond_e

    #@13
    .line 894
    new-instance v2, Landroid/content/Intent;

    #@15
    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    #@18
    .line 897
    .local v2, intent:Landroid/content/Intent;
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->TARGET_OPERATOR:Ljava/lang/String;

    #@1a
    const-string v3, "VZW"

    #@1c
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1f
    move-result v0

    #@20
    if-nez v0, :cond_40

    #@22
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->TARGET_OPERATOR:Ljava/lang/String;

    #@24
    const-string v3, "SPR"

    #@26
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@29
    move-result v0

    #@2a
    if-nez v0, :cond_40

    #@2c
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->TARGET_OPERATOR:Ljava/lang/String;

    #@2e
    const-string v3, "TMO"

    #@30
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@33
    move-result v0

    #@34
    if-eqz v0, :cond_d3

    #@36
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->TARGET_COUNTRY:Ljava/lang/String;

    #@38
    const-string v3, "US"

    #@3a
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3d
    move-result v0

    #@3e
    if-eqz v0, :cond_d3

    #@40
    .line 899
    :cond_40
    const-string v0, "com.android.settings"

    #@42
    const-string v3, "com.android.settings.Settings$TetherNetworkSettingsActivity"

    #@44
    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@47
    .line 906
    :goto_47
    const v0, 0x10808000

    #@4a
    invoke-virtual {v2, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@4d
    .line 908
    sget-object v0, Lcom/android/server/connectivity/Tethering;->mContext:Landroid/content/Context;

    #@4f
    sget-object v5, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    #@51
    move v3, v1

    #@52
    invoke-static/range {v0 .. v5}, Landroid/app/PendingIntent;->getActivityAsUser(Landroid/content/Context;ILandroid/content/Intent;ILandroid/os/Bundle;Landroid/os/UserHandle;)Landroid/app/PendingIntent;

    #@55
    move-result-object v8

    #@56
    .line 911
    .local v8, pi:Landroid/app/PendingIntent;
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@59
    move-result-object v9

    #@5a
    .line 916
    .local v9, r:Landroid/content/res/Resources;
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->TARGET_OPERATOR:Ljava/lang/String;

    #@5c
    const-string v1, "SPR"

    #@5e
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@61
    move-result v0

    #@62
    if-nez v0, :cond_78

    #@64
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->TARGET_OPERATOR:Ljava/lang/String;

    #@66
    const-string v1, "TMO"

    #@68
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6b
    move-result v0

    #@6c
    if-eqz v0, :cond_ef

    #@6e
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->TARGET_COUNTRY:Ljava/lang/String;

    #@70
    const-string v1, "US"

    #@72
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@75
    move-result v0

    #@76
    if-eqz v0, :cond_ef

    #@78
    .line 917
    :cond_78
    const v0, 0x20903b1

    #@7b
    invoke-virtual {v9, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@7e
    move-result-object v10

    #@7f
    .line 918
    .local v10, title:Ljava/lang/CharSequence;
    const v0, 0x10404be

    #@82
    invoke-virtual {v9, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@85
    move-result-object v6

    #@86
    .line 927
    .local v6, message:Ljava/lang/CharSequence;
    :goto_86
    new-instance v0, Landroid/app/Notification;

    #@88
    invoke-direct {v0}, Landroid/app/Notification;-><init>()V

    #@8b
    iput-object v0, p0, Lcom/android/server/connectivity/Tethering;->mUSBTetheredNotification:Landroid/app/Notification;

    #@8d
    .line 928
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mUSBTetheredNotification:Landroid/app/Notification;

    #@8f
    const-wide/16 v11, 0x0

    #@91
    iput-wide v11, v0, Landroid/app/Notification;->when:J

    #@93
    .line 929
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->TARGET_OPERATOR:Ljava/lang/String;

    #@95
    const-string v1, "TMO"

    #@97
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9a
    move-result v0

    #@9b
    if-eqz v0, :cond_fe

    #@9d
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->TARGET_COUNTRY:Ljava/lang/String;

    #@9f
    const-string v1, "US"

    #@a1
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a4
    move-result v0

    #@a5
    if-eqz v0, :cond_fe

    #@a7
    .line 930
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mUSBTetheredNotification:Landroid/app/Notification;

    #@a9
    const v1, 0x2020292

    #@ac
    iput v1, v0, Landroid/app/Notification;->icon:I

    #@ae
    .line 934
    :goto_ae
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mUSBTetheredNotification:Landroid/app/Notification;

    #@b0
    iget v1, v0, Landroid/app/Notification;->defaults:I

    #@b2
    and-int/lit8 v1, v1, -0x2

    #@b4
    iput v1, v0, Landroid/app/Notification;->defaults:I

    #@b6
    .line 935
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mUSBTetheredNotification:Landroid/app/Notification;

    #@b8
    const/4 v1, 0x2

    #@b9
    iput v1, v0, Landroid/app/Notification;->flags:I

    #@bb
    .line 936
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mUSBTetheredNotification:Landroid/app/Notification;

    #@bd
    iput-object v10, v0, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    #@bf
    .line 937
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mUSBTetheredNotification:Landroid/app/Notification;

    #@c1
    sget-object v1, Lcom/android/server/connectivity/Tethering;->mContext:Landroid/content/Context;

    #@c3
    invoke-virtual {v0, v1, v10, v6, v8}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    #@c6
    .line 938
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mUSBTetheredNotification:Landroid/app/Notification;

    #@c8
    iget v0, v0, Landroid/app/Notification;->icon:I

    #@ca
    iget-object v1, p0, Lcom/android/server/connectivity/Tethering;->mUSBTetheredNotification:Landroid/app/Notification;

    #@cc
    sget-object v3, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@ce
    invoke-virtual {v7, v4, v0, v1, v3}, Landroid/app/NotificationManager;->notifyAsUser(Ljava/lang/String;ILandroid/app/Notification;Landroid/os/UserHandle;)V

    #@d1
    goto/16 :goto_e

    #@d3
    .line 900
    .end local v6           #message:Ljava/lang/CharSequence;
    .end local v8           #pi:Landroid/app/PendingIntent;
    .end local v9           #r:Landroid/content/res/Resources;
    .end local v10           #title:Ljava/lang/CharSequence;
    :cond_d3
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->TARGET_OPERATOR:Ljava/lang/String;

    #@d5
    const-string v3, "DCM"

    #@d7
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@da
    move-result v0

    #@db
    if-eqz v0, :cond_e6

    #@dd
    .line 901
    const-string v0, "com.android.settings"

    #@df
    const-string v3, "com.android.settings.TetherSettings"

    #@e1
    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@e4
    goto/16 :goto_47

    #@e6
    .line 903
    :cond_e6
    const-string v0, "com.android.settings"

    #@e8
    const-string v3, "com.android.settings.UsbSettings"

    #@ea
    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@ed
    goto/16 :goto_47

    #@ef
    .line 921
    .restart local v8       #pi:Landroid/app/PendingIntent;
    .restart local v9       #r:Landroid/content/res/Resources;
    :cond_ef
    const v0, 0x1040464

    #@f2
    invoke-virtual {v9, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@f5
    move-result-object v10

    #@f6
    .line 922
    .restart local v10       #title:Ljava/lang/CharSequence;
    const v0, 0x2090146

    #@f9
    invoke-virtual {v9, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@fc
    move-result-object v6

    #@fd
    .restart local v6       #message:Ljava/lang/CharSequence;
    goto :goto_86

    #@fe
    .line 932
    :cond_fe
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mUSBTetheredNotification:Landroid/app/Notification;

    #@100
    const v1, 0x108056e

    #@103
    iput v1, v0, Landroid/app/Notification;->icon:I

    #@105
    goto :goto_ae
.end method

.method private showWiFiTetheredNotification(I)V
    .registers 15
    .parameter "icon"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v4, 0x0

    #@2
    .line 994
    sget-object v0, Lcom/android/server/connectivity/Tethering;->mContext:Landroid/content/Context;

    #@4
    const-string v3, "notification"

    #@6
    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@9
    move-result-object v7

    #@a
    check-cast v7, Landroid/app/NotificationManager;

    #@c
    .line 996
    .local v7, notificationManager:Landroid/app/NotificationManager;
    if-nez v7, :cond_f

    #@e
    .line 1037
    :cond_e
    :goto_e
    return-void

    #@f
    .line 1000
    :cond_f
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mWiFiTetheredNotification:Landroid/app/Notification;

    #@11
    if-eqz v0, :cond_22

    #@13
    .line 1001
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mWiFiTetheredNotification:Landroid/app/Notification;

    #@15
    iget v0, v0, Landroid/app/Notification;->icon:I

    #@17
    if-eq v0, p1, :cond_e

    #@19
    .line 1003
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mWiFiTetheredNotification:Landroid/app/Notification;

    #@1b
    iget v0, v0, Landroid/app/Notification;->icon:I

    #@1d
    invoke-virtual {v7, v0}, Landroid/app/NotificationManager;->cancel(I)V

    #@20
    .line 1004
    iput-object v4, p0, Lcom/android/server/connectivity/Tethering;->mWiFiTetheredNotification:Landroid/app/Notification;

    #@22
    .line 1007
    :cond_22
    new-instance v2, Landroid/content/Intent;

    #@24
    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    #@27
    .line 1008
    .local v2, intent:Landroid/content/Intent;
    const-string v0, "com.android.settings"

    #@29
    const-string v3, "com.android.settings.TetherSettings"

    #@2b
    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@2e
    .line 1009
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->TARGET_OPERATOR:Ljava/lang/String;

    #@30
    const-string v3, "ATT"

    #@32
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@35
    move-result v0

    #@36
    if-eqz v0, :cond_a6

    #@38
    .line 1010
    const/high16 v0, 0x2000

    #@3a
    invoke-virtual {v2, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@3d
    .line 1014
    :goto_3d
    sget-object v0, Lcom/android/server/connectivity/Tethering;->mContext:Landroid/content/Context;

    #@3f
    sget-object v5, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    #@41
    move v3, v1

    #@42
    invoke-static/range {v0 .. v5}, Landroid/app/PendingIntent;->getActivityAsUser(Landroid/content/Context;ILandroid/content/Intent;ILandroid/os/Bundle;Landroid/os/UserHandle;)Landroid/app/PendingIntent;

    #@45
    move-result-object v8

    #@46
    .line 1017
    .local v8, pi:Landroid/app/PendingIntent;
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@49
    move-result-object v9

    #@4a
    .line 1019
    .local v9, r:Landroid/content/res/Resources;
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getOperator()Ljava/lang/String;

    #@4d
    move-result-object v0

    #@4e
    const-string v1, "ATT"

    #@50
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@53
    move-result v0

    #@54
    if-eqz v0, :cond_ad

    #@56
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getCountry()Ljava/lang/String;

    #@59
    move-result-object v0

    #@5a
    const-string v1, "US"

    #@5c
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5f
    move-result v0

    #@60
    if-eqz v0, :cond_ad

    #@62
    .line 1020
    const v0, 0x209037f

    #@65
    invoke-virtual {v9, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@68
    move-result-object v10

    #@69
    .line 1027
    .local v10, title:Ljava/lang/CharSequence;
    :goto_69
    const v0, 0x10404be

    #@6c
    invoke-virtual {v9, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@6f
    move-result-object v6

    #@70
    .line 1029
    .local v6, message:Ljava/lang/CharSequence;
    new-instance v0, Landroid/app/Notification;

    #@72
    invoke-direct {v0}, Landroid/app/Notification;-><init>()V

    #@75
    iput-object v0, p0, Lcom/android/server/connectivity/Tethering;->mWiFiTetheredNotification:Landroid/app/Notification;

    #@77
    .line 1030
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mWiFiTetheredNotification:Landroid/app/Notification;

    #@79
    const-wide/16 v11, 0x0

    #@7b
    iput-wide v11, v0, Landroid/app/Notification;->when:J

    #@7d
    .line 1031
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mWiFiTetheredNotification:Landroid/app/Notification;

    #@7f
    iput p1, v0, Landroid/app/Notification;->icon:I

    #@81
    .line 1032
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mWiFiTetheredNotification:Landroid/app/Notification;

    #@83
    iget v1, v0, Landroid/app/Notification;->defaults:I

    #@85
    and-int/lit8 v1, v1, -0x2

    #@87
    iput v1, v0, Landroid/app/Notification;->defaults:I

    #@89
    .line 1033
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mWiFiTetheredNotification:Landroid/app/Notification;

    #@8b
    const/4 v1, 0x2

    #@8c
    iput v1, v0, Landroid/app/Notification;->flags:I

    #@8e
    .line 1034
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mWiFiTetheredNotification:Landroid/app/Notification;

    #@90
    iput-object v10, v0, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    #@92
    .line 1035
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mWiFiTetheredNotification:Landroid/app/Notification;

    #@94
    sget-object v1, Lcom/android/server/connectivity/Tethering;->mContext:Landroid/content/Context;

    #@96
    invoke-virtual {v0, v1, v10, v6, v8}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    #@99
    .line 1036
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mWiFiTetheredNotification:Landroid/app/Notification;

    #@9b
    iget v0, v0, Landroid/app/Notification;->icon:I

    #@9d
    iget-object v1, p0, Lcom/android/server/connectivity/Tethering;->mWiFiTetheredNotification:Landroid/app/Notification;

    #@9f
    sget-object v3, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@a1
    invoke-virtual {v7, v4, v0, v1, v3}, Landroid/app/NotificationManager;->notifyAsUser(Ljava/lang/String;ILandroid/app/Notification;Landroid/os/UserHandle;)V

    #@a4
    goto/16 :goto_e

    #@a6
    .line 1012
    .end local v6           #message:Ljava/lang/CharSequence;
    .end local v8           #pi:Landroid/app/PendingIntent;
    .end local v9           #r:Landroid/content/res/Resources;
    .end local v10           #title:Ljava/lang/CharSequence;
    :cond_a6
    const v0, 0x10808000

    #@a9
    invoke-virtual {v2, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@ac
    goto :goto_3d

    #@ad
    .line 1021
    .restart local v8       #pi:Landroid/app/PendingIntent;
    .restart local v9       #r:Landroid/content/res/Resources;
    :cond_ad
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getOperator()Ljava/lang/String;

    #@b0
    move-result-object v0

    #@b1
    const-string v1, "TMO"

    #@b3
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b6
    move-result v0

    #@b7
    if-eqz v0, :cond_cd

    #@b9
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getCountry()Ljava/lang/String;

    #@bc
    move-result-object v0

    #@bd
    const-string v1, "US"

    #@bf
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c2
    move-result v0

    #@c3
    if-eqz v0, :cond_cd

    #@c5
    .line 1022
    const v0, 0x2090384

    #@c8
    invoke-virtual {v9, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@cb
    move-result-object v10

    #@cc
    .restart local v10       #title:Ljava/lang/CharSequence;
    goto :goto_69

    #@cd
    .line 1024
    .end local v10           #title:Ljava/lang/CharSequence;
    :cond_cd
    const v0, 0x2090383

    #@d0
    invoke-virtual {v9, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@d3
    move-result-object v10

    #@d4
    .restart local v10       #title:Ljava/lang/CharSequence;
    goto :goto_69
.end method

.method private tetherUsb(Z)V
    .registers 12
    .parameter "enable"

    #@0
    .prologue
    .line 1189
    const-string v7, "Tethering"

    #@2
    new-instance v8, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v9, "tetherUsb "

    #@9
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v8

    #@d
    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@10
    move-result-object v8

    #@11
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v8

    #@15
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1191
    const/4 v7, 0x0

    #@19
    new-array v4, v7, [Ljava/lang/String;

    #@1b
    .line 1193
    .local v4, ifaces:[Ljava/lang/String;
    :try_start_1b
    iget-object v7, p0, Lcom/android/server/connectivity/Tethering;->mNMService:Landroid/os/INetworkManagementService;

    #@1d
    invoke-interface {v7}, Landroid/os/INetworkManagementService;->listInterfaces()[Ljava/lang/String;
    :try_end_20
    .catch Ljava/lang/Exception; {:try_start_1b .. :try_end_20} :catch_37

    #@20
    move-result-object v4

    #@21
    .line 1198
    move-object v0, v4

    #@22
    .local v0, arr$:[Ljava/lang/String;
    array-length v5, v0

    #@23
    .local v5, len$:I
    const/4 v2, 0x0

    #@24
    .local v2, i$:I
    :goto_24
    if-ge v2, v5, :cond_48

    #@26
    aget-object v3, v0, v2

    #@28
    .line 1199
    .local v3, iface:Ljava/lang/String;
    invoke-direct {p0, v3}, Lcom/android/server/connectivity/Tethering;->isUsb(Ljava/lang/String;)Z

    #@2b
    move-result v7

    #@2c
    if-eqz v7, :cond_45

    #@2e
    .line 1200
    if-eqz p1, :cond_40

    #@30
    invoke-virtual {p0, v3}, Lcom/android/server/connectivity/Tethering;->tether(Ljava/lang/String;)I

    #@33
    move-result v6

    #@34
    .line 1201
    .local v6, result:I
    :goto_34
    if-nez v6, :cond_45

    #@36
    .line 1207
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v2           #i$:I
    .end local v3           #iface:Ljava/lang/String;
    .end local v5           #len$:I
    .end local v6           #result:I
    :goto_36
    return-void

    #@37
    .line 1194
    :catch_37
    move-exception v1

    #@38
    .line 1195
    .local v1, e:Ljava/lang/Exception;
    const-string v7, "Tethering"

    #@3a
    const-string v8, "Error listing Interfaces"

    #@3c
    invoke-static {v7, v8, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@3f
    goto :goto_36

    #@40
    .line 1200
    .end local v1           #e:Ljava/lang/Exception;
    .restart local v0       #arr$:[Ljava/lang/String;
    .restart local v2       #i$:I
    .restart local v3       #iface:Ljava/lang/String;
    .restart local v5       #len$:I
    :cond_40
    invoke-virtual {p0, v3}, Lcom/android/server/connectivity/Tethering;->untether(Ljava/lang/String;)I

    #@43
    move-result v6

    #@44
    goto :goto_34

    #@45
    .line 1198
    :cond_45
    add-int/lit8 v2, v2, 0x1

    #@47
    goto :goto_24

    #@48
    .line 1206
    .end local v3           #iface:Ljava/lang/String;
    :cond_48
    const-string v7, "Tethering"

    #@4a
    const-string v8, "unable start or stop USB tethering"

    #@4c
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    goto :goto_36
.end method


# virtual methods
.method addUsbIfaces()V
    .registers 12

    #@0
    .prologue
    .line 415
    const-string v8, "network_management"

    #@2
    invoke-static {v8}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@5
    move-result-object v1

    #@6
    .line 416
    .local v1, b:Landroid/os/IBinder;
    invoke-static {v1}, Landroid/os/INetworkManagementService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/INetworkManagementService;

    #@9
    move-result-object v7

    #@a
    .line 417
    .local v7, service:Landroid/os/INetworkManagementService;
    const/4 v8, 0x0

    #@b
    new-array v5, v8, [Ljava/lang/String;

    #@d
    .line 419
    .local v5, ifaces:[Ljava/lang/String;
    :try_start_d
    invoke-interface {v7}, Landroid/os/INetworkManagementService;->listInterfaces()[Ljava/lang/String;
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_10} :catch_24

    #@10
    move-result-object v5

    #@11
    .line 424
    move-object v0, v5

    #@12
    .local v0, arr$:[Ljava/lang/String;
    array-length v6, v0

    #@13
    .local v6, len$:I
    const/4 v3, 0x0

    #@14
    .local v3, i$:I
    :goto_14
    if-ge v3, v6, :cond_3d

    #@16
    aget-object v4, v0, v3

    #@18
    .line 425
    .local v4, iface:Ljava/lang/String;
    invoke-direct {p0, v4}, Lcom/android/server/connectivity/Tethering;->isUsb(Ljava/lang/String;)Z

    #@1b
    move-result v8

    #@1c
    if-eqz v8, :cond_21

    #@1e
    .line 426
    invoke-virtual {p0, v4}, Lcom/android/server/connectivity/Tethering;->interfaceAdded(Ljava/lang/String;)V

    #@21
    .line 424
    :cond_21
    add-int/lit8 v3, v3, 0x1

    #@23
    goto :goto_14

    #@24
    .line 420
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v3           #i$:I
    .end local v4           #iface:Ljava/lang/String;
    .end local v6           #len$:I
    :catch_24
    move-exception v2

    #@25
    .line 421
    .local v2, e:Ljava/lang/Exception;
    const-string v8, "Tethering"

    #@27
    new-instance v9, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const-string v10, "Error listing Interfaces :"

    #@2e
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v9

    #@32
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v9

    #@36
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v9

    #@3a
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3d
    .line 428
    .end local v2           #e:Ljava/lang/Exception;
    :cond_3d
    return-void
.end method

.method public checkDunRequired()V
    .registers 9

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/16 v3, 0x12

    #@3
    const/4 v2, 0x4

    #@4
    const/4 v6, 0x2

    #@5
    const/4 v0, 0x5

    #@6
    .line 1303
    sget-object v4, Lcom/android/server/connectivity/Tethering;->mContext:Landroid/content/Context;

    #@8
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@b
    move-result-object v4

    #@c
    const-string v5, "tether_dun_required"

    #@e
    invoke-static {v4, v5, v6}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@11
    move-result v1

    #@12
    .line 1305
    .local v1, secureSetting:I
    iget-object v4, p0, Lcom/android/server/connectivity/Tethering;->mPublicSync:Ljava/lang/Object;

    #@14
    monitor-enter v4

    #@15
    .line 1307
    :try_start_15
    iget-object v5, p0, Lcom/android/server/connectivity/Tethering;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@17
    iget-boolean v5, v5, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_MPDN_UPLUS_INIT:Z

    #@19
    if-eqz v5, :cond_a3

    #@1b
    .line 1309
    if-eq v1, v6, :cond_5a

    #@1d
    .line 1310
    if-ne v1, v7, :cond_20

    #@1f
    move v0, v3

    #@20
    .line 1313
    .local v0, requiredApn:I
    :cond_20
    if-ne v0, v3, :cond_6a

    #@22
    .line 1314
    :goto_22
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering;->mUpstreamIfaceTypes:Ljava/util/Collection;

    #@24
    sget-object v3, Lcom/android/server/connectivity/Tethering;->MOBILE_TYPE:Ljava/lang/Integer;

    #@26
    invoke-interface {v2, v3}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    #@29
    move-result v2

    #@2a
    if-eqz v2, :cond_37

    #@2c
    .line 1315
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering;->mUpstreamIfaceTypes:Ljava/util/Collection;

    #@2e
    sget-object v3, Lcom/android/server/connectivity/Tethering;->MOBILE_TYPE:Ljava/lang/Integer;

    #@30
    invoke-interface {v2, v3}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    #@33
    goto :goto_22

    #@34
    .line 1380
    .end local v0           #requiredApn:I
    :catchall_34
    move-exception v2

    #@35
    monitor-exit v4
    :try_end_36
    .catchall {:try_start_15 .. :try_end_36} :catchall_34

    #@36
    throw v2

    #@37
    .line 1317
    .restart local v0       #requiredApn:I
    :cond_37
    :goto_37
    :try_start_37
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering;->mUpstreamIfaceTypes:Ljava/util/Collection;

    #@39
    sget-object v3, Lcom/android/server/connectivity/Tethering;->HIPRI_TYPE:Ljava/lang/Integer;

    #@3b
    invoke-interface {v2, v3}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    #@3e
    move-result v2

    #@3f
    if-eqz v2, :cond_49

    #@41
    .line 1318
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering;->mUpstreamIfaceTypes:Ljava/util/Collection;

    #@43
    sget-object v3, Lcom/android/server/connectivity/Tethering;->HIPRI_TYPE:Ljava/lang/Integer;

    #@45
    invoke-interface {v2, v3}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    #@48
    goto :goto_37

    #@49
    .line 1320
    :cond_49
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering;->mUpstreamIfaceTypes:Ljava/util/Collection;

    #@4b
    sget-object v3, Lcom/android/server/connectivity/Tethering;->TETHERING_TYPE:Ljava/lang/Integer;

    #@4d
    invoke-interface {v2, v3}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    #@50
    move-result v2

    #@51
    if-nez v2, :cond_5a

    #@53
    .line 1321
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering;->mUpstreamIfaceTypes:Ljava/util/Collection;

    #@55
    sget-object v3, Lcom/android/server/connectivity/Tethering;->TETHERING_TYPE:Ljava/lang/Integer;

    #@57
    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    #@5a
    .line 1335
    .end local v0           #requiredApn:I
    :cond_5a
    :goto_5a
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering;->mUpstreamIfaceTypes:Ljava/util/Collection;

    #@5c
    sget-object v3, Lcom/android/server/connectivity/Tethering;->TETHERING_TYPE:Ljava/lang/Integer;

    #@5e
    invoke-interface {v2, v3}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    #@61
    move-result v2

    #@62
    if-eqz v2, :cond_9f

    #@64
    .line 1336
    const/16 v2, 0x12

    #@66
    iput v2, p0, Lcom/android/server/connectivity/Tethering;->mPreferredUpstreamMobileApn:I

    #@68
    .line 1380
    :goto_68
    monitor-exit v4

    #@69
    .line 1381
    return-void

    #@6a
    .line 1324
    .restart local v0       #requiredApn:I
    :cond_6a
    :goto_6a
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering;->mUpstreamIfaceTypes:Ljava/util/Collection;

    #@6c
    sget-object v3, Lcom/android/server/connectivity/Tethering;->TETHERING_TYPE:Ljava/lang/Integer;

    #@6e
    invoke-interface {v2, v3}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    #@71
    move-result v2

    #@72
    if-eqz v2, :cond_7c

    #@74
    .line 1325
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering;->mUpstreamIfaceTypes:Ljava/util/Collection;

    #@76
    sget-object v3, Lcom/android/server/connectivity/Tethering;->TETHERING_TYPE:Ljava/lang/Integer;

    #@78
    invoke-interface {v2, v3}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    #@7b
    goto :goto_6a

    #@7c
    .line 1327
    :cond_7c
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering;->mUpstreamIfaceTypes:Ljava/util/Collection;

    #@7e
    sget-object v3, Lcom/android/server/connectivity/Tethering;->MOBILE_TYPE:Ljava/lang/Integer;

    #@80
    invoke-interface {v2, v3}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    #@83
    move-result v2

    #@84
    if-nez v2, :cond_8d

    #@86
    .line 1328
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering;->mUpstreamIfaceTypes:Ljava/util/Collection;

    #@88
    sget-object v3, Lcom/android/server/connectivity/Tethering;->MOBILE_TYPE:Ljava/lang/Integer;

    #@8a
    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    #@8d
    .line 1330
    :cond_8d
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering;->mUpstreamIfaceTypes:Ljava/util/Collection;

    #@8f
    sget-object v3, Lcom/android/server/connectivity/Tethering;->HIPRI_TYPE:Ljava/lang/Integer;

    #@91
    invoke-interface {v2, v3}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    #@94
    move-result v2

    #@95
    if-nez v2, :cond_5a

    #@97
    .line 1331
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering;->mUpstreamIfaceTypes:Ljava/util/Collection;

    #@99
    sget-object v3, Lcom/android/server/connectivity/Tethering;->HIPRI_TYPE:Ljava/lang/Integer;

    #@9b
    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    #@9e
    goto :goto_5a

    #@9f
    .line 1338
    .end local v0           #requiredApn:I
    :cond_9f
    const/4 v2, 0x5

    #@a0
    iput v2, p0, Lcom/android/server/connectivity/Tethering;->mPreferredUpstreamMobileApn:I

    #@a2
    goto :goto_68

    #@a3
    .line 1346
    :cond_a3
    if-eq v1, v6, :cond_df

    #@a5
    .line 1347
    if-ne v1, v7, :cond_a8

    #@a7
    move v0, v2

    #@a8
    .line 1350
    .restart local v0       #requiredApn:I
    :cond_a8
    if-ne v0, v2, :cond_108

    #@aa
    .line 1351
    :goto_aa
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering;->mUpstreamIfaceTypes:Ljava/util/Collection;

    #@ac
    sget-object v3, Lcom/android/server/connectivity/Tethering;->MOBILE_TYPE:Ljava/lang/Integer;

    #@ae
    invoke-interface {v2, v3}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    #@b1
    move-result v2

    #@b2
    if-eqz v2, :cond_bc

    #@b4
    .line 1352
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering;->mUpstreamIfaceTypes:Ljava/util/Collection;

    #@b6
    sget-object v3, Lcom/android/server/connectivity/Tethering;->MOBILE_TYPE:Ljava/lang/Integer;

    #@b8
    invoke-interface {v2, v3}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    #@bb
    goto :goto_aa

    #@bc
    .line 1354
    :cond_bc
    :goto_bc
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering;->mUpstreamIfaceTypes:Ljava/util/Collection;

    #@be
    sget-object v3, Lcom/android/server/connectivity/Tethering;->HIPRI_TYPE:Ljava/lang/Integer;

    #@c0
    invoke-interface {v2, v3}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    #@c3
    move-result v2

    #@c4
    if-eqz v2, :cond_ce

    #@c6
    .line 1355
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering;->mUpstreamIfaceTypes:Ljava/util/Collection;

    #@c8
    sget-object v3, Lcom/android/server/connectivity/Tethering;->HIPRI_TYPE:Ljava/lang/Integer;

    #@ca
    invoke-interface {v2, v3}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    #@cd
    goto :goto_bc

    #@ce
    .line 1357
    :cond_ce
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering;->mUpstreamIfaceTypes:Ljava/util/Collection;

    #@d0
    sget-object v3, Lcom/android/server/connectivity/Tethering;->DUN_TYPE:Ljava/lang/Integer;

    #@d2
    invoke-interface {v2, v3}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    #@d5
    move-result v2

    #@d6
    if-nez v2, :cond_df

    #@d8
    .line 1358
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering;->mUpstreamIfaceTypes:Ljava/util/Collection;

    #@da
    sget-object v3, Lcom/android/server/connectivity/Tethering;->DUN_TYPE:Ljava/lang/Integer;

    #@dc
    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    #@df
    .line 1372
    .end local v0           #requiredApn:I
    :cond_df
    :goto_df
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering;->mUpstreamIfaceTypes:Ljava/util/Collection;

    #@e1
    sget-object v3, Lcom/android/server/connectivity/Tethering;->DUN_TYPE:Ljava/lang/Integer;

    #@e3
    invoke-interface {v2, v3}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    #@e6
    move-result v2

    #@e7
    if-eqz v2, :cond_13d

    #@e9
    .line 1373
    const/4 v2, 0x4

    #@ea
    iput v2, p0, Lcom/android/server/connectivity/Tethering;->mPreferredUpstreamMobileApn:I

    #@ec
    .line 1374
    const-string v2, "Tethering"

    #@ee
    new-instance v3, Ljava/lang/StringBuilder;

    #@f0
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@f3
    const-string v5, "[jlee] checkDunRequired: TYPE_MOBILE_DUN set mPreferredUpstreamMobileApn:"

    #@f5
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f8
    move-result-object v3

    #@f9
    iget v5, p0, Lcom/android/server/connectivity/Tethering;->mPreferredUpstreamMobileApn:I

    #@fb
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@fe
    move-result-object v3

    #@ff
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@102
    move-result-object v3

    #@103
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@106
    goto/16 :goto_68

    #@108
    .line 1361
    .restart local v0       #requiredApn:I
    :cond_108
    :goto_108
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering;->mUpstreamIfaceTypes:Ljava/util/Collection;

    #@10a
    sget-object v3, Lcom/android/server/connectivity/Tethering;->DUN_TYPE:Ljava/lang/Integer;

    #@10c
    invoke-interface {v2, v3}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    #@10f
    move-result v2

    #@110
    if-eqz v2, :cond_11a

    #@112
    .line 1362
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering;->mUpstreamIfaceTypes:Ljava/util/Collection;

    #@114
    sget-object v3, Lcom/android/server/connectivity/Tethering;->DUN_TYPE:Ljava/lang/Integer;

    #@116
    invoke-interface {v2, v3}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    #@119
    goto :goto_108

    #@11a
    .line 1364
    :cond_11a
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering;->mUpstreamIfaceTypes:Ljava/util/Collection;

    #@11c
    sget-object v3, Lcom/android/server/connectivity/Tethering;->MOBILE_TYPE:Ljava/lang/Integer;

    #@11e
    invoke-interface {v2, v3}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    #@121
    move-result v2

    #@122
    if-nez v2, :cond_12b

    #@124
    .line 1365
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering;->mUpstreamIfaceTypes:Ljava/util/Collection;

    #@126
    sget-object v3, Lcom/android/server/connectivity/Tethering;->MOBILE_TYPE:Ljava/lang/Integer;

    #@128
    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    #@12b
    .line 1367
    :cond_12b
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering;->mUpstreamIfaceTypes:Ljava/util/Collection;

    #@12d
    sget-object v3, Lcom/android/server/connectivity/Tethering;->HIPRI_TYPE:Ljava/lang/Integer;

    #@12f
    invoke-interface {v2, v3}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    #@132
    move-result v2

    #@133
    if-nez v2, :cond_df

    #@135
    .line 1368
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering;->mUpstreamIfaceTypes:Ljava/util/Collection;

    #@137
    sget-object v3, Lcom/android/server/connectivity/Tethering;->HIPRI_TYPE:Ljava/lang/Integer;

    #@139
    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    #@13c
    goto :goto_df

    #@13d
    .line 1376
    .end local v0           #requiredApn:I
    :cond_13d
    const/4 v2, 0x5

    #@13e
    iput v2, p0, Lcom/android/server/connectivity/Tethering;->mPreferredUpstreamMobileApn:I

    #@140
    .line 1377
    const-string v2, "Tethering"

    #@142
    new-instance v3, Ljava/lang/StringBuilder;

    #@144
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@147
    const-string v5, "[jlee] checkDunRequired: TYPE_MOBILE_HIPRI set mPreferredUpstreamMobileApn:"

    #@149
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14c
    move-result-object v3

    #@14d
    iget v5, p0, Lcom/android/server/connectivity/Tethering;->mPreferredUpstreamMobileApn:I

    #@14f
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@152
    move-result-object v3

    #@153
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@156
    move-result-object v3

    #@157
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_15a
    .catchall {:try_start_37 .. :try_end_15a} :catchall_34

    #@15a
    goto/16 :goto_68
.end method

.method public clearTetheredBlockNotification()V
    .registers 4

    #@0
    .prologue
    .line 2851
    const-string v1, "Tethering"

    #@2
    const-string v2, "[TetherSettings]  clearTetheredBlockNotification !!!! "

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 2852
    sget-object v1, Lcom/android/server/connectivity/Tethering;->mContext:Landroid/content/Context;

    #@9
    const-string v2, "notification"

    #@b
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@e
    move-result-object v0

    #@f
    check-cast v0, Landroid/app/NotificationManager;

    #@11
    .line 2854
    .local v0, notificationManager:Landroid/app/NotificationManager;
    if-eqz v0, :cond_28

    #@13
    iget-object v1, p0, Lcom/android/server/connectivity/Tethering;->mPermissionErrorNotification:Landroid/app/Notification;

    #@15
    if-eqz v1, :cond_28

    #@17
    .line 2855
    iget-object v1, p0, Lcom/android/server/connectivity/Tethering;->mPermissionErrorNotification:Landroid/app/Notification;

    #@19
    iget v1, v1, Landroid/app/Notification;->icon:I

    #@1b
    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    #@1e
    .line 2856
    const-string v1, "Tethering"

    #@20
    const-string v2, "[TetherSettings]  Notification Cleared !!!! "

    #@22
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 2857
    const/4 v1, 0x0

    #@26
    iput-object v1, p0, Lcom/android/server/connectivity/Tethering;->mPermissionErrorNotification:Landroid/app/Notification;

    #@28
    .line 2859
    :cond_28
    return-void
.end method

.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 10
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 2782
    sget-object v3, Lcom/android/server/connectivity/Tethering;->mContext:Landroid/content/Context;

    #@2
    const-string v4, "android.permission.DUMP"

    #@4
    invoke-virtual {v3, v4}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@7
    move-result v3

    #@8
    if-eqz v3, :cond_33

    #@a
    .line 2784
    new-instance v3, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v4, "Permission Denial: can\'t dump ConnectivityService.Tether from from pid="

    #@11
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v3

    #@15
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@18
    move-result v4

    #@19
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    const-string v4, ", uid="

    #@1f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@26
    move-result v4

    #@27
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v3

    #@2f
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@32
    .line 2803
    :goto_32
    return-void

    #@33
    .line 2790
    :cond_33
    iget-object v4, p0, Lcom/android/server/connectivity/Tethering;->mPublicSync:Ljava/lang/Object;

    #@35
    monitor-enter v4

    #@36
    .line 2791
    :try_start_36
    const-string v3, "mUpstreamIfaceTypes: "

    #@38
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@3b
    .line 2792
    iget-object v3, p0, Lcom/android/server/connectivity/Tethering;->mUpstreamIfaceTypes:Ljava/util/Collection;

    #@3d
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@40
    move-result-object v0

    #@41
    .local v0, i$:Ljava/util/Iterator;
    :goto_41
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@44
    move-result v3

    #@45
    if-eqz v3, :cond_67

    #@47
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@4a
    move-result-object v1

    #@4b
    check-cast v1, Ljava/lang/Integer;

    #@4d
    .line 2793
    .local v1, netType:Ljava/lang/Integer;
    new-instance v3, Ljava/lang/StringBuilder;

    #@4f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@52
    const-string v5, " "

    #@54
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v3

    #@58
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v3

    #@5c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5f
    move-result-object v3

    #@60
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@63
    goto :goto_41

    #@64
    .line 2801
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #netType:Ljava/lang/Integer;
    :catchall_64
    move-exception v3

    #@65
    monitor-exit v4
    :try_end_66
    .catchall {:try_start_36 .. :try_end_66} :catchall_64

    #@66
    throw v3

    #@67
    .line 2796
    .restart local v0       #i$:Ljava/util/Iterator;
    :cond_67
    :try_start_67
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    #@6a
    .line 2797
    const-string v3, "Tether state:"

    #@6c
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@6f
    .line 2798
    iget-object v3, p0, Lcom/android/server/connectivity/Tethering;->mIfaces:Ljava/util/HashMap;

    #@71
    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@74
    move-result-object v3

    #@75
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@78
    move-result-object v0

    #@79
    :goto_79
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@7c
    move-result v3

    #@7d
    if-eqz v3, :cond_9c

    #@7f
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@82
    move-result-object v2

    #@83
    check-cast v2, Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;

    #@85
    .line 2799
    .local v2, o:Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;
    new-instance v3, Ljava/lang/StringBuilder;

    #@87
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@8a
    const-string v5, " "

    #@8c
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v3

    #@90
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@93
    move-result-object v3

    #@94
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@97
    move-result-object v3

    #@98
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@9b
    goto :goto_79

    #@9c
    .line 2801
    .end local v2           #o:Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;
    :cond_9c
    monitor-exit v4
    :try_end_9d
    .catchall {:try_start_67 .. :try_end_9d} :catchall_64

    #@9d
    .line 2802
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    #@a0
    goto :goto_32
.end method

.method public getErroredIfaces()[Ljava/lang/String;
    .registers 10

    #@0
    .prologue
    .line 1434
    new-instance v4, Ljava/util/ArrayList;

    #@2
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 1435
    .local v4, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v8, p0, Lcom/android/server/connectivity/Tethering;->mPublicSync:Ljava/lang/Object;

    #@7
    monitor-enter v8

    #@8
    .line 1436
    :try_start_8
    iget-object v7, p0, Lcom/android/server/connectivity/Tethering;->mIfaces:Ljava/util/HashMap;

    #@a
    invoke-virtual {v7}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@d
    move-result-object v3

    #@e
    .line 1437
    .local v3, keys:Ljava/util/Set;
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@11
    move-result-object v1

    #@12
    .local v1, i$:Ljava/util/Iterator;
    :cond_12
    :goto_12
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@15
    move-result v7

    #@16
    if-eqz v7, :cond_33

    #@18
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1b
    move-result-object v2

    #@1c
    .line 1438
    .local v2, key:Ljava/lang/Object;
    iget-object v7, p0, Lcom/android/server/connectivity/Tethering;->mIfaces:Ljava/util/HashMap;

    #@1e
    invoke-virtual {v7, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@21
    move-result-object v6

    #@22
    check-cast v6, Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;

    #@24
    .line 1439
    .local v6, sm:Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;
    invoke-virtual {v6}, Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;->isErrored()Z

    #@27
    move-result v7

    #@28
    if-eqz v7, :cond_12

    #@2a
    .line 1440
    check-cast v2, Ljava/lang/String;

    #@2c
    .end local v2           #key:Ljava/lang/Object;
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2f
    goto :goto_12

    #@30
    .line 1443
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v3           #keys:Ljava/util/Set;
    .end local v6           #sm:Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;
    :catchall_30
    move-exception v7

    #@31
    monitor-exit v8
    :try_end_32
    .catchall {:try_start_8 .. :try_end_32} :catchall_30

    #@32
    throw v7

    #@33
    .restart local v1       #i$:Ljava/util/Iterator;
    .restart local v3       #keys:Ljava/util/Set;
    :cond_33
    :try_start_33
    monitor-exit v8
    :try_end_34
    .catchall {:try_start_33 .. :try_end_34} :catchall_30

    #@34
    .line 1444
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@37
    move-result v7

    #@38
    new-array v5, v7, [Ljava/lang/String;

    #@3a
    .line 1445
    .local v5, retVal:[Ljava/lang/String;
    const/4 v0, 0x0

    #@3b
    .local v0, i:I
    :goto_3b
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@3e
    move-result v7

    #@3f
    if-ge v0, v7, :cond_4c

    #@41
    .line 1446
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@44
    move-result-object v7

    #@45
    check-cast v7, Ljava/lang/String;

    #@47
    aput-object v7, v5, v0

    #@49
    .line 1445
    add-int/lit8 v0, v0, 0x1

    #@4b
    goto :goto_3b

    #@4c
    .line 1448
    :cond_4c
    return-object v5
.end method

.method public getLastTetherError(Ljava/lang/String;)I
    .registers 8
    .parameter "iface"

    #@0
    .prologue
    .line 717
    const/4 v1, 0x0

    #@1
    .line 718
    .local v1, sm:Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;
    iget-object v3, p0, Lcom/android/server/connectivity/Tethering;->mPublicSync:Ljava/lang/Object;

    #@3
    monitor-enter v3

    #@4
    .line 719
    :try_start_4
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering;->mIfaces:Ljava/util/HashMap;

    #@6
    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v2

    #@a
    move-object v0, v2

    #@b
    check-cast v0, Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;

    #@d
    move-object v1, v0

    #@e
    .line 720
    if-nez v1, :cond_31

    #@10
    .line 721
    const-string v2, "Tethering"

    #@12
    new-instance v4, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v5, "Tried to getLastTetherError on an unknown iface :"

    #@19
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v4

    #@1d
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v4

    #@21
    const-string v5, ", ignoring"

    #@23
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v4

    #@27
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v4

    #@2b
    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    .line 723
    const/4 v2, 0x1

    #@2f
    monitor-exit v3

    #@30
    .line 725
    :goto_30
    return v2

    #@31
    :cond_31
    invoke-virtual {v1}, Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;->getLastError()I

    #@34
    move-result v2

    #@35
    monitor-exit v3

    #@36
    goto :goto_30

    #@37
    .line 726
    :catchall_37
    move-exception v2

    #@38
    monitor-exit v3
    :try_end_39
    .catchall {:try_start_4 .. :try_end_39} :catchall_37

    #@39
    throw v2
.end method

.method public getTetherableBluetoothRegexs()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1257
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mTetherableBluetoothRegexs:[Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getTetherableIfaces()[Ljava/lang/String;
    .registers 10

    #@0
    .prologue
    .line 1416
    new-instance v4, Ljava/util/ArrayList;

    #@2
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 1417
    .local v4, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v8, p0, Lcom/android/server/connectivity/Tethering;->mPublicSync:Ljava/lang/Object;

    #@7
    monitor-enter v8

    #@8
    .line 1418
    :try_start_8
    iget-object v7, p0, Lcom/android/server/connectivity/Tethering;->mIfaces:Ljava/util/HashMap;

    #@a
    invoke-virtual {v7}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@d
    move-result-object v3

    #@e
    .line 1419
    .local v3, keys:Ljava/util/Set;
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@11
    move-result-object v1

    #@12
    .local v1, i$:Ljava/util/Iterator;
    :cond_12
    :goto_12
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@15
    move-result v7

    #@16
    if-eqz v7, :cond_33

    #@18
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1b
    move-result-object v2

    #@1c
    .line 1420
    .local v2, key:Ljava/lang/Object;
    iget-object v7, p0, Lcom/android/server/connectivity/Tethering;->mIfaces:Ljava/util/HashMap;

    #@1e
    invoke-virtual {v7, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@21
    move-result-object v6

    #@22
    check-cast v6, Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;

    #@24
    .line 1421
    .local v6, sm:Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;
    invoke-virtual {v6}, Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;->isAvailable()Z

    #@27
    move-result v7

    #@28
    if-eqz v7, :cond_12

    #@2a
    .line 1422
    check-cast v2, Ljava/lang/String;

    #@2c
    .end local v2           #key:Ljava/lang/Object;
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2f
    goto :goto_12

    #@30
    .line 1425
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v3           #keys:Ljava/util/Set;
    .end local v6           #sm:Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;
    :catchall_30
    move-exception v7

    #@31
    monitor-exit v8
    :try_end_32
    .catchall {:try_start_8 .. :try_end_32} :catchall_30

    #@32
    throw v7

    #@33
    .restart local v1       #i$:Ljava/util/Iterator;
    .restart local v3       #keys:Ljava/util/Set;
    :cond_33
    :try_start_33
    monitor-exit v8
    :try_end_34
    .catchall {:try_start_33 .. :try_end_34} :catchall_30

    #@34
    .line 1426
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@37
    move-result v7

    #@38
    new-array v5, v7, [Ljava/lang/String;

    #@3a
    .line 1427
    .local v5, retVal:[Ljava/lang/String;
    const/4 v0, 0x0

    #@3b
    .local v0, i:I
    :goto_3b
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@3e
    move-result v7

    #@3f
    if-ge v0, v7, :cond_4c

    #@41
    .line 1428
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@44
    move-result-object v7

    #@45
    check-cast v7, Ljava/lang/String;

    #@47
    aput-object v7, v5, v0

    #@49
    .line 1427
    add-int/lit8 v0, v0, 0x1

    #@4b
    goto :goto_3b

    #@4c
    .line 1430
    :cond_4c
    return-object v5
.end method

.method public getTetherableUsbRegexs()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1249
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mTetherableUsbRegexs:[Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getTetherableWifiRegexs()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1253
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mTetherableWifiRegexs:[Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getTetheredIfacePairs()[Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    .line 1403
    invoke-static {}, Lcom/google/android/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    #@3
    move-result-object v1

    #@4
    .line 1404
    .local v1, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/android/server/connectivity/Tethering;->mPublicSync:Ljava/lang/Object;

    #@6
    monitor-enter v4

    #@7
    .line 1405
    :try_start_7
    iget-object v3, p0, Lcom/android/server/connectivity/Tethering;->mIfaces:Ljava/util/HashMap;

    #@9
    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@c
    move-result-object v3

    #@d
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@10
    move-result-object v0

    #@11
    .local v0, i$:Ljava/util/Iterator;
    :cond_11
    :goto_11
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@14
    move-result v3

    #@15
    if-eqz v3, :cond_31

    #@17
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1a
    move-result-object v2

    #@1b
    check-cast v2, Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;

    #@1d
    .line 1406
    .local v2, sm:Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;
    invoke-virtual {v2}, Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;->isTethered()Z

    #@20
    move-result v3

    #@21
    if-eqz v3, :cond_11

    #@23
    .line 1407
    iget-object v3, v2, Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;->mMyUpstreamIfaceName:Ljava/lang/String;

    #@25
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@28
    .line 1408
    iget-object v3, v2, Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;->mIfaceName:Ljava/lang/String;

    #@2a
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2d
    goto :goto_11

    #@2e
    .line 1411
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v2           #sm:Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;
    :catchall_2e
    move-exception v3

    #@2f
    monitor-exit v4
    :try_end_30
    .catchall {:try_start_7 .. :try_end_30} :catchall_2e

    #@30
    throw v3

    #@31
    .restart local v0       #i$:Ljava/util/Iterator;
    :cond_31
    :try_start_31
    monitor-exit v4
    :try_end_32
    .catchall {:try_start_31 .. :try_end_32} :catchall_2e

    #@32
    .line 1412
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@35
    move-result v3

    #@36
    new-array v3, v3, [Ljava/lang/String;

    #@38
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@3b
    move-result-object v3

    #@3c
    check-cast v3, [Ljava/lang/String;

    #@3e
    return-object v3
.end method

.method public getTetheredIfaces()[Ljava/lang/String;
    .registers 10

    #@0
    .prologue
    .line 1385
    new-instance v4, Ljava/util/ArrayList;

    #@2
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 1386
    .local v4, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v8, p0, Lcom/android/server/connectivity/Tethering;->mPublicSync:Ljava/lang/Object;

    #@7
    monitor-enter v8

    #@8
    .line 1387
    :try_start_8
    iget-object v7, p0, Lcom/android/server/connectivity/Tethering;->mIfaces:Ljava/util/HashMap;

    #@a
    invoke-virtual {v7}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@d
    move-result-object v3

    #@e
    .line 1388
    .local v3, keys:Ljava/util/Set;
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@11
    move-result-object v1

    #@12
    .local v1, i$:Ljava/util/Iterator;
    :cond_12
    :goto_12
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@15
    move-result v7

    #@16
    if-eqz v7, :cond_33

    #@18
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1b
    move-result-object v2

    #@1c
    .line 1389
    .local v2, key:Ljava/lang/Object;
    iget-object v7, p0, Lcom/android/server/connectivity/Tethering;->mIfaces:Ljava/util/HashMap;

    #@1e
    invoke-virtual {v7, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@21
    move-result-object v6

    #@22
    check-cast v6, Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;

    #@24
    .line 1390
    .local v6, sm:Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;
    invoke-virtual {v6}, Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;->isTethered()Z

    #@27
    move-result v7

    #@28
    if-eqz v7, :cond_12

    #@2a
    .line 1391
    check-cast v2, Ljava/lang/String;

    #@2c
    .end local v2           #key:Ljava/lang/Object;
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2f
    goto :goto_12

    #@30
    .line 1394
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v3           #keys:Ljava/util/Set;
    .end local v6           #sm:Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;
    :catchall_30
    move-exception v7

    #@31
    monitor-exit v8
    :try_end_32
    .catchall {:try_start_8 .. :try_end_32} :catchall_30

    #@32
    throw v7

    #@33
    .restart local v1       #i$:Ljava/util/Iterator;
    .restart local v3       #keys:Ljava/util/Set;
    :cond_33
    :try_start_33
    monitor-exit v8
    :try_end_34
    .catchall {:try_start_33 .. :try_end_34} :catchall_30

    #@34
    .line 1395
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@37
    move-result v7

    #@38
    new-array v5, v7, [Ljava/lang/String;

    #@3a
    .line 1396
    .local v5, retVal:[Ljava/lang/String;
    const/4 v0, 0x0

    #@3b
    .local v0, i:I
    :goto_3b
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@3e
    move-result v7

    #@3f
    if-ge v0, v7, :cond_4c

    #@41
    .line 1397
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@44
    move-result-object v7

    #@45
    check-cast v7, Ljava/lang/String;

    #@47
    aput-object v7, v5, v0

    #@49
    .line 1396
    add-int/lit8 v0, v0, 0x1

    #@4b
    goto :goto_3b

    #@4c
    .line 1399
    :cond_4c
    return-object v5
.end method

.method public getUpstreamIfaceTypes()[I
    .registers 6

    #@0
    .prologue
    .line 1291
    iget-object v4, p0, Lcom/android/server/connectivity/Tethering;->mPublicSync:Ljava/lang/Object;

    #@2
    monitor-enter v4

    #@3
    .line 1292
    :try_start_3
    invoke-virtual {p0}, Lcom/android/server/connectivity/Tethering;->updateConfiguration()V

    #@6
    .line 1293
    iget-object v3, p0, Lcom/android/server/connectivity/Tethering;->mUpstreamIfaceTypes:Ljava/util/Collection;

    #@8
    invoke-interface {v3}, Ljava/util/Collection;->size()I

    #@b
    move-result v3

    #@c
    new-array v2, v3, [I

    #@e
    .line 1294
    .local v2, values:[I
    iget-object v3, p0, Lcom/android/server/connectivity/Tethering;->mUpstreamIfaceTypes:Ljava/util/Collection;

    #@10
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@13
    move-result-object v1

    #@14
    .line 1295
    .local v1, iterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/Integer;>;"
    const/4 v0, 0x0

    #@15
    .local v0, i:I
    :goto_15
    iget-object v3, p0, Lcom/android/server/connectivity/Tethering;->mUpstreamIfaceTypes:Ljava/util/Collection;

    #@17
    invoke-interface {v3}, Ljava/util/Collection;->size()I

    #@1a
    move-result v3

    #@1b
    if-ge v0, v3, :cond_2c

    #@1d
    .line 1296
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@20
    move-result-object v3

    #@21
    check-cast v3, Ljava/lang/Integer;

    #@23
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    #@26
    move-result v3

    #@27
    aput v3, v2, v0

    #@29
    .line 1295
    add-int/lit8 v0, v0, 0x1

    #@2b
    goto :goto_15

    #@2c
    .line 1298
    :cond_2c
    monitor-exit v4

    #@2d
    .line 1299
    return-object v2

    #@2e
    .line 1298
    .end local v0           #i:I
    .end local v1           #iterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .end local v2           #values:[I
    :catchall_2e
    move-exception v3

    #@2f
    monitor-exit v4
    :try_end_30
    .catchall {:try_start_3 .. :try_end_30} :catchall_2e

    #@30
    throw v3
.end method

.method public handleTetherIfaceChange(Landroid/net/NetworkInfo;)V
    .registers 4
    .parameter "info"

    #@0
    .prologue
    .line 1456
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mTetherMasterSM:Lcom/android/internal/util/StateMachine;

    #@2
    const/4 v1, 0x3

    #@3
    invoke-virtual {v0, v1, p1}, Lcom/android/internal/util/StateMachine;->sendMessage(ILjava/lang/Object;)V

    #@6
    .line 1457
    return-void
.end method

.method public interfaceAdded(Ljava/lang/String;)V
    .registers 9
    .parameter "iface"

    #@0
    .prologue
    .line 540
    const-string v3, "Tethering"

    #@2
    new-instance v4, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v5, "interfaceAdded "

    #@9
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v4

    #@d
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v4

    #@11
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v4

    #@15
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 541
    const/4 v0, 0x0

    #@19
    .line 542
    .local v0, found:Z
    const/4 v2, 0x0

    #@1a
    .line 543
    .local v2, usb:Z
    iget-object v4, p0, Lcom/android/server/connectivity/Tethering;->mPublicSync:Ljava/lang/Object;

    #@1c
    monitor-enter v4

    #@1d
    .line 544
    :try_start_1d
    invoke-virtual {p0, p1}, Lcom/android/server/connectivity/Tethering;->isWifi(Ljava/lang/String;)Z

    #@20
    move-result v3

    #@21
    if-eqz v3, :cond_24

    #@23
    .line 545
    const/4 v0, 0x1

    #@24
    .line 547
    :cond_24
    invoke-direct {p0, p1}, Lcom/android/server/connectivity/Tethering;->isUsb(Ljava/lang/String;)Z

    #@27
    move-result v3

    #@28
    if-eqz v3, :cond_41

    #@2a
    .line 548
    const/4 v0, 0x1

    #@2b
    .line 549
    const/4 v2, 0x1

    #@2c
    .line 551
    iget-object v3, p0, Lcom/android/server/connectivity/Tethering;->TARGET_OPERATOR:Ljava/lang/String;

    #@2e
    const-string v5, "VZW"

    #@30
    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@33
    move-result v3

    #@34
    if-eqz v3, :cond_41

    #@36
    .line 552
    const-string v3, "usb0"

    #@38
    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3b
    move-result v3

    #@3c
    if-eqz v3, :cond_41

    #@3e
    .line 553
    const/4 v3, 0x1

    #@3f
    iput-boolean v3, p0, Lcom/android/server/connectivity/Tethering;->usb0InterfaceAdd:Z

    #@41
    .line 558
    :cond_41
    invoke-virtual {p0, p1}, Lcom/android/server/connectivity/Tethering;->isBluetooth(Ljava/lang/String;)Z

    #@44
    move-result v3

    #@45
    if-eqz v3, :cond_48

    #@47
    .line 559
    const/4 v0, 0x1

    #@48
    .line 561
    :cond_48
    if-nez v0, :cond_64

    #@4a
    .line 562
    const-string v3, "Tethering"

    #@4c
    new-instance v5, Ljava/lang/StringBuilder;

    #@4e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@51
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v5

    #@55
    const-string v6, " is not a tetherable iface, ignoring"

    #@57
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v5

    #@5b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v5

    #@5f
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@62
    .line 563
    monitor-exit v4

    #@63
    .line 575
    :goto_63
    return-void

    #@64
    .line 566
    :cond_64
    iget-object v3, p0, Lcom/android/server/connectivity/Tethering;->mIfaces:Ljava/util/HashMap;

    #@66
    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@69
    move-result-object v1

    #@6a
    check-cast v1, Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;

    #@6c
    .line 567
    .local v1, sm:Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;
    if-eqz v1, :cond_91

    #@6e
    .line 568
    const-string v3, "Tethering"

    #@70
    new-instance v5, Ljava/lang/StringBuilder;

    #@72
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@75
    const-string v6, "active iface ("

    #@77
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v5

    #@7b
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v5

    #@7f
    const-string v6, ") reported as added, ignoring"

    #@81
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v5

    #@85
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@88
    move-result-object v5

    #@89
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8c
    .line 569
    monitor-exit v4

    #@8d
    goto :goto_63

    #@8e
    .line 574
    .end local v1           #sm:Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;
    :catchall_8e
    move-exception v3

    #@8f
    monitor-exit v4
    :try_end_90
    .catchall {:try_start_1d .. :try_end_90} :catchall_8e

    #@90
    throw v3

    #@91
    .line 571
    .restart local v1       #sm:Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;
    :cond_91
    :try_start_91
    new-instance v1, Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;

    #@93
    .end local v1           #sm:Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;
    iget-object v3, p0, Lcom/android/server/connectivity/Tethering;->mLooper:Landroid/os/Looper;

    #@95
    invoke-direct {v1, p0, p1, v3, v2}, Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;-><init>(Lcom/android/server/connectivity/Tethering;Ljava/lang/String;Landroid/os/Looper;Z)V

    #@98
    .line 572
    .restart local v1       #sm:Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;
    iget-object v3, p0, Lcom/android/server/connectivity/Tethering;->mIfaces:Ljava/util/HashMap;

    #@9a
    invoke-virtual {v3, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@9d
    .line 573
    invoke-virtual {v1}, Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;->start()V

    #@a0
    .line 574
    monitor-exit v4
    :try_end_a1
    .catchall {:try_start_91 .. :try_end_a1} :catchall_8e

    #@a1
    goto :goto_63
.end method

.method public interfaceClassDataActivityChanged(Ljava/lang/String;Z)V
    .registers 3
    .parameter "label"
    .parameter "active"

    #@0
    .prologue
    .line 601
    return-void
.end method

.method public interfaceLinkStateChanged(Ljava/lang/String;Z)V
    .registers 6
    .parameter "iface"
    .parameter "up"

    #@0
    .prologue
    .line 508
    const-string v0, "Tethering"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "interfaceLinkStateChanged "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, ", "

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 509
    invoke-virtual {p0, p1, p2}, Lcom/android/server/connectivity/Tethering;->interfaceStatusChanged(Ljava/lang/String;Z)V

    #@25
    .line 510
    return-void
.end method

.method public interfaceRemoved(Ljava/lang/String;)V
    .registers 7
    .parameter "iface"

    #@0
    .prologue
    .line 578
    const-string v1, "Tethering"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "interfaceRemoved "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 579
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering;->mPublicSync:Ljava/lang/Object;

    #@1a
    monitor-enter v2

    #@1b
    .line 580
    :try_start_1b
    iget-object v1, p0, Lcom/android/server/connectivity/Tethering;->mIfaces:Ljava/util/HashMap;

    #@1d
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@20
    move-result-object v0

    #@21
    check-cast v0, Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;

    #@23
    .line 581
    .local v0, sm:Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;
    if-nez v0, :cond_45

    #@25
    .line 583
    const-string v1, "Tethering"

    #@27
    new-instance v3, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const-string v4, "attempting to remove unknown iface ("

    #@2e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v3

    #@32
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v3

    #@36
    const-string v4, "), ignoring"

    #@38
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v3

    #@3c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v3

    #@40
    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@43
    .line 585
    monitor-exit v2

    #@44
    .line 597
    :goto_44
    return-void

    #@45
    .line 587
    :cond_45
    const/4 v1, 0x4

    #@46
    invoke-virtual {v0, v1}, Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;->sendMessage(I)V

    #@49
    .line 588
    iget-object v1, p0, Lcom/android/server/connectivity/Tethering;->mIfaces:Ljava/util/HashMap;

    #@4b
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@4e
    .line 590
    iget-object v1, p0, Lcom/android/server/connectivity/Tethering;->TARGET_OPERATOR:Ljava/lang/String;

    #@50
    const-string v3, "VZW"

    #@52
    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@55
    move-result v1

    #@56
    if-eqz v1, :cond_63

    #@58
    .line 591
    const-string v1, "usb0"

    #@5a
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5d
    move-result v1

    #@5e
    if-eqz v1, :cond_63

    #@60
    .line 592
    const/4 v1, 0x0

    #@61
    iput-boolean v1, p0, Lcom/android/server/connectivity/Tethering;->usb0InterfaceAdd:Z

    #@63
    .line 596
    :cond_63
    monitor-exit v2

    #@64
    goto :goto_44

    #@65
    .end local v0           #sm:Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;
    :catchall_65
    move-exception v1

    #@66
    monitor-exit v2
    :try_end_67
    .catchall {:try_start_1b .. :try_end_67} :catchall_65

    #@67
    throw v1
.end method

.method public interfaceStatusChanged(Ljava/lang/String;Z)V
    .registers 10
    .parameter "iface"
    .parameter "up"

    #@0
    .prologue
    .line 473
    const-string v3, "Tethering"

    #@2
    new-instance v4, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v5, "interfaceStatusChanged "

    #@9
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v4

    #@d
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v4

    #@11
    const-string v5, ", "

    #@13
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v4

    #@17
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v4

    #@1b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v4

    #@1f
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 474
    const/4 v0, 0x0

    #@23
    .line 475
    .local v0, found:Z
    const/4 v2, 0x0

    #@24
    .line 476
    .local v2, usb:Z
    iget-object v4, p0, Lcom/android/server/connectivity/Tethering;->mPublicSync:Ljava/lang/Object;

    #@26
    monitor-enter v4

    #@27
    .line 477
    :try_start_27
    invoke-virtual {p0, p1}, Lcom/android/server/connectivity/Tethering;->isWifi(Ljava/lang/String;)Z

    #@2a
    move-result v3

    #@2b
    if-eqz v3, :cond_32

    #@2d
    .line 478
    const/4 v0, 0x1

    #@2e
    .line 485
    :cond_2e
    :goto_2e
    if-nez v0, :cond_43

    #@30
    monitor-exit v4

    #@31
    .line 505
    :goto_31
    return-void

    #@32
    .line 479
    :cond_32
    invoke-direct {p0, p1}, Lcom/android/server/connectivity/Tethering;->isUsb(Ljava/lang/String;)Z

    #@35
    move-result v3

    #@36
    if-eqz v3, :cond_3b

    #@38
    .line 480
    const/4 v0, 0x1

    #@39
    .line 481
    const/4 v2, 0x1

    #@3a
    goto :goto_2e

    #@3b
    .line 482
    :cond_3b
    invoke-virtual {p0, p1}, Lcom/android/server/connectivity/Tethering;->isBluetooth(Ljava/lang/String;)Z

    #@3e
    move-result v3

    #@3f
    if-eqz v3, :cond_2e

    #@41
    .line 483
    const/4 v0, 0x1

    #@42
    goto :goto_2e

    #@43
    .line 487
    :cond_43
    iget-object v3, p0, Lcom/android/server/connectivity/Tethering;->mIfaces:Ljava/util/HashMap;

    #@45
    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@48
    move-result-object v1

    #@49
    check-cast v1, Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;

    #@4b
    .line 488
    .local v1, sm:Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;
    if-eqz p2, :cond_63

    #@4d
    .line 489
    if-nez v1, :cond_5e

    #@4f
    .line 490
    new-instance v1, Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;

    #@51
    .end local v1           #sm:Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;
    iget-object v3, p0, Lcom/android/server/connectivity/Tethering;->mLooper:Landroid/os/Looper;

    #@53
    invoke-direct {v1, p0, p1, v3, v2}, Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;-><init>(Lcom/android/server/connectivity/Tethering;Ljava/lang/String;Landroid/os/Looper;Z)V

    #@56
    .line 491
    .restart local v1       #sm:Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;
    iget-object v3, p0, Lcom/android/server/connectivity/Tethering;->mIfaces:Ljava/util/HashMap;

    #@58
    invoke-virtual {v3, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5b
    .line 492
    invoke-virtual {v1}, Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;->start()V

    #@5e
    .line 504
    :cond_5e
    :goto_5e
    monitor-exit v4

    #@5f
    goto :goto_31

    #@60
    .end local v1           #sm:Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;
    :catchall_60
    move-exception v3

    #@61
    monitor-exit v4
    :try_end_62
    .catchall {:try_start_27 .. :try_end_62} :catchall_60

    #@62
    throw v3

    #@63
    .line 495
    .restart local v1       #sm:Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;
    :cond_63
    :try_start_63
    invoke-direct {p0, p1}, Lcom/android/server/connectivity/Tethering;->isUsb(Ljava/lang/String;)Z

    #@66
    move-result v3

    #@67
    if-eqz v3, :cond_82

    #@69
    .line 498
    const-string v3, "Tethering"

    #@6b
    new-instance v5, Ljava/lang/StringBuilder;

    #@6d
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@70
    const-string v6, "ignore interface down for "

    #@72
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v5

    #@76
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v5

    #@7a
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7d
    move-result-object v5

    #@7e
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@81
    goto :goto_5e

    #@82
    .line 499
    :cond_82
    if-eqz v1, :cond_5e

    #@84
    .line 500
    const/4 v3, 0x4

    #@85
    invoke-virtual {v1, v3}, Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;->sendMessage(I)V

    #@88
    .line 501
    iget-object v3, p0, Lcom/android/server/connectivity/Tethering;->mIfaces:Ljava/util/HashMap;

    #@8a
    invoke-virtual {v3, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_8d
    .catchall {:try_start_63 .. :try_end_8d} :catchall_60

    #@8d
    goto :goto_5e
.end method

.method public isBluetooth(Ljava/lang/String;)Z
    .registers 8
    .parameter "iface"

    #@0
    .prologue
    .line 531
    iget-object v5, p0, Lcom/android/server/connectivity/Tethering;->mPublicSync:Ljava/lang/Object;

    #@2
    monitor-enter v5

    #@3
    .line 532
    :try_start_3
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mTetherableBluetoothRegexs:[Ljava/lang/String;

    #@5
    .local v0, arr$:[Ljava/lang/String;
    array-length v2, v0

    #@6
    .local v2, len$:I
    const/4 v1, 0x0

    #@7
    .local v1, i$:I
    :goto_7
    if-ge v1, v2, :cond_17

    #@9
    aget-object v3, v0, v1

    #@b
    .line 533
    .local v3, regex:Ljava/lang/String;
    invoke-virtual {p1, v3}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    #@e
    move-result v4

    #@f
    if-eqz v4, :cond_14

    #@11
    const/4 v4, 0x1

    #@12
    monitor-exit v5

    #@13
    .line 535
    .end local v3           #regex:Ljava/lang/String;
    :goto_13
    return v4

    #@14
    .line 532
    .restart local v3       #regex:Ljava/lang/String;
    :cond_14
    add-int/lit8 v1, v1, 0x1

    #@16
    goto :goto_7

    #@17
    .line 535
    .end local v3           #regex:Ljava/lang/String;
    :cond_17
    const/4 v4, 0x0

    #@18
    monitor-exit v5

    #@19
    goto :goto_13

    #@1a
    .line 536
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #i$:I
    .end local v2           #len$:I
    :catchall_1a
    move-exception v4

    #@1b
    monitor-exit v5
    :try_end_1c
    .catchall {:try_start_3 .. :try_end_1c} :catchall_1a

    #@1c
    throw v4
.end method

.method public isHotspotSupported()Z
    .registers 5

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 1468
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getCountry()Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    const-string v2, "US"

    #@8
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v1

    #@c
    if-eqz v1, :cond_2d

    #@e
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getOperator()Ljava/lang/String;

    #@11
    move-result-object v1

    #@12
    const-string v2, "ACG"

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@17
    move-result v1

    #@18
    if-eqz v1, :cond_2d

    #@1a
    .line 1471
    iget-object v1, p0, Lcom/android/server/connectivity/Tethering;->mTetherNetwork:Lcom/android/server/connectivity/TetherNetwork;

    #@1c
    if-eqz v1, :cond_25

    #@1e
    .line 1472
    iget-object v1, p0, Lcom/android/server/connectivity/Tethering;->mTetherNetwork:Lcom/android/server/connectivity/TetherNetwork;

    #@20
    invoke-virtual {v1, v3, v0, v3, v3}, Lcom/android/server/connectivity/TetherNetwork;->isAllowedTetherableInterface(ZZZZ)Z

    #@23
    move-result v0

    #@24
    .line 1494
    :cond_24
    :goto_24
    return v0

    #@25
    .line 1474
    :cond_25
    const-string v1, "Tethering"

    #@27
    const-string v2, "tether mTetherNetwork in not available"

    #@29
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    goto :goto_24

    #@2d
    .line 1480
    :cond_2d
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->isWifiChameleonFeaturedCarrier()Z

    #@30
    move-result v1

    #@31
    if-eqz v1, :cond_24

    #@33
    .line 1482
    iget-object v1, p0, Lcom/android/server/connectivity/Tethering;->mTetherNetwork:Lcom/android/server/connectivity/TetherNetwork;

    #@35
    if-eqz v1, :cond_3e

    #@37
    .line 1483
    iget-object v1, p0, Lcom/android/server/connectivity/Tethering;->mTetherNetwork:Lcom/android/server/connectivity/TetherNetwork;

    #@39
    invoke-virtual {v1, v3, v0, v3, v3}, Lcom/android/server/connectivity/TetherNetwork;->isAllowedTetherableInterface(ZZZZ)Z

    #@3c
    move-result v0

    #@3d
    goto :goto_24

    #@3e
    .line 1485
    :cond_3e
    const-string v1, "Tethering"

    #@40
    const-string v2, "tether mTetherNetwork in not available"

    #@42
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    goto :goto_24
.end method

.method public isWifi(Ljava/lang/String;)Z
    .registers 8
    .parameter "iface"

    #@0
    .prologue
    .line 522
    iget-object v5, p0, Lcom/android/server/connectivity/Tethering;->mPublicSync:Ljava/lang/Object;

    #@2
    monitor-enter v5

    #@3
    .line 523
    :try_start_3
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering;->mTetherableWifiRegexs:[Ljava/lang/String;

    #@5
    .local v0, arr$:[Ljava/lang/String;
    array-length v2, v0

    #@6
    .local v2, len$:I
    const/4 v1, 0x0

    #@7
    .local v1, i$:I
    :goto_7
    if-ge v1, v2, :cond_17

    #@9
    aget-object v3, v0, v1

    #@b
    .line 524
    .local v3, regex:Ljava/lang/String;
    invoke-virtual {p1, v3}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    #@e
    move-result v4

    #@f
    if-eqz v4, :cond_14

    #@11
    const/4 v4, 0x1

    #@12
    monitor-exit v5

    #@13
    .line 526
    .end local v3           #regex:Ljava/lang/String;
    :goto_13
    return v4

    #@14
    .line 523
    .restart local v3       #regex:Ljava/lang/String;
    :cond_14
    add-int/lit8 v1, v1, 0x1

    #@16
    goto :goto_7

    #@17
    .line 526
    .end local v3           #regex:Ljava/lang/String;
    :cond_17
    const/4 v4, 0x0

    #@18
    monitor-exit v5

    #@19
    goto :goto_13

    #@1a
    .line 527
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #i$:I
    .end local v2           #len$:I
    :catchall_1a
    move-exception v4

    #@1b
    monitor-exit v5
    :try_end_1c
    .catchall {:try_start_3 .. :try_end_1c} :catchall_1a

    #@1c
    throw v4
.end method

.method public limitReached(Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .parameter "limitName"
    .parameter "iface"

    #@0
    .prologue
    .line 599
    return-void
.end method

.method public setUsbTethering(Z)I
    .registers 8
    .parameter "enable"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 1261
    const-string v2, "Tethering"

    #@4
    new-instance v3, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v4, "setUsbTethering("

    #@b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v3

    #@f
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@12
    move-result-object v3

    #@13
    const-string v4, ")"

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v3

    #@1d
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 1263
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@23
    move-result-object v2

    #@24
    if-eqz v2, :cond_34

    #@26
    if-eqz p1, :cond_34

    #@28
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@2b
    move-result-object v2

    #@2c
    invoke-interface {v2, v5}, Lcom/lge/cappuccino/IMdm;->checkDisabledTetherType(I)Z

    #@2f
    move-result v2

    #@30
    if-eqz v2, :cond_34

    #@32
    .line 1265
    const/4 v1, 0x2

    #@33
    .line 1286
    :goto_33
    return v1

    #@34
    .line 1268
    :cond_34
    sget-object v2, Lcom/android/server/connectivity/Tethering;->mContext:Landroid/content/Context;

    #@36
    const-string v3, "usb"

    #@38
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@3b
    move-result-object v0

    #@3c
    check-cast v0, Landroid/hardware/usb/UsbManager;

    #@3e
    .line 1270
    .local v0, usbManager:Landroid/hardware/usb/UsbManager;
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering;->mPublicSync:Ljava/lang/Object;

    #@40
    monitor-enter v2

    #@41
    .line 1271
    if-eqz p1, :cond_5a

    #@43
    .line 1272
    :try_start_43
    iget-boolean v3, p0, Lcom/android/server/connectivity/Tethering;->mRndisEnabled:Z

    #@45
    if-eqz v3, :cond_50

    #@47
    .line 1273
    const/4 v3, 0x1

    #@48
    invoke-direct {p0, v3}, Lcom/android/server/connectivity/Tethering;->tetherUsb(Z)V

    #@4b
    .line 1285
    :goto_4b
    monitor-exit v2

    #@4c
    goto :goto_33

    #@4d
    :catchall_4d
    move-exception v1

    #@4e
    monitor-exit v2
    :try_end_4f
    .catchall {:try_start_43 .. :try_end_4f} :catchall_4d

    #@4f
    throw v1

    #@50
    .line 1275
    :cond_50
    const/4 v3, 0x1

    #@51
    :try_start_51
    iput-boolean v3, p0, Lcom/android/server/connectivity/Tethering;->mUsbTetherRequested:Z

    #@53
    .line 1276
    const-string v3, "ecm"

    #@55
    const/4 v4, 0x0

    #@56
    invoke-virtual {v0, v3, v4}, Landroid/hardware/usb/UsbManager;->setCurrentFunction(Ljava/lang/String;Z)V

    #@59
    goto :goto_4b

    #@5a
    .line 1279
    :cond_5a
    const/4 v3, 0x0

    #@5b
    invoke-direct {p0, v3}, Lcom/android/server/connectivity/Tethering;->tetherUsb(Z)V

    #@5e
    .line 1280
    iget-boolean v3, p0, Lcom/android/server/connectivity/Tethering;->mRndisEnabled:Z

    #@60
    if-eqz v3, :cond_67

    #@62
    .line 1281
    const/4 v3, 0x0

    #@63
    const/4 v4, 0x0

    #@64
    invoke-virtual {v0, v3, v4}, Landroid/hardware/usb/UsbManager;->setCurrentFunction(Ljava/lang/String;Z)V

    #@67
    .line 1283
    :cond_67
    const/4 v3, 0x0

    #@68
    iput-boolean v3, p0, Lcom/android/server/connectivity/Tethering;->mUsbTetherRequested:Z
    :try_end_6a
    .catchall {:try_start_51 .. :try_end_6a} :catchall_4d

    #@6a
    goto :goto_4b
.end method

.method public showTetheredBlockNotification(I)V
    .registers 11
    .parameter "icon"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 2808
    const-string v6, "Tethering"

    #@3
    const-string v7, "[Tethering]\tCreate New Notification !!!! "

    #@5
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 2809
    sget-object v6, Lcom/android/server/connectivity/Tethering;->mContext:Landroid/content/Context;

    #@a
    const-string v7, "notification"

    #@c
    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@f
    move-result-object v2

    #@10
    check-cast v2, Landroid/app/NotificationManager;

    #@12
    .line 2812
    .local v2, notificationManager:Landroid/app/NotificationManager;
    invoke-virtual {p0}, Lcom/android/server/connectivity/Tethering;->clearTetheredBlockNotification()V

    #@15
    .line 2814
    if-nez v2, :cond_18

    #@17
    .line 2848
    :goto_17
    return-void

    #@18
    .line 2818
    :cond_18
    iget-object v6, p0, Lcom/android/server/connectivity/Tethering;->mPermissionErrorNotification:Landroid/app/Notification;

    #@1a
    if-eqz v6, :cond_24

    #@1c
    .line 2819
    const-string v6, "Tethering"

    #@1e
    const-string v7, "[TetherSettings]  Notification Showed Already !!!! "

    #@20
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    goto :goto_17

    #@24
    .line 2823
    :cond_24
    new-instance v0, Landroid/content/Intent;

    #@26
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    #@29
    .line 2824
    .local v0, intent:Landroid/content/Intent;
    const-string v6, "com.android.settings"

    #@2b
    const-string v7, "com.android.settings.TetherSettings"

    #@2d
    invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@30
    .line 2825
    const/high16 v6, 0x4000

    #@32
    invoke-virtual {v0, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@35
    .line 2827
    sget-object v6, Lcom/android/server/connectivity/Tethering;->mContext:Landroid/content/Context;

    #@37
    invoke-static {v6, v8, v0, v8}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@3a
    move-result-object v3

    #@3b
    .line 2829
    .local v3, pi:Landroid/app/PendingIntent;
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@3e
    move-result-object v4

    #@3f
    .line 2833
    .local v4, r:Landroid/content/res/Resources;
    const-string v5, "Tethering Failed"

    #@41
    .line 2834
    .local v5, title:Ljava/lang/CharSequence;
    const-string v1, "Tethering is not allowed for downloaded apps"

    #@43
    .line 2836
    .local v1, message:Ljava/lang/CharSequence;
    iget-object v6, p0, Lcom/android/server/connectivity/Tethering;->mPermissionErrorNotification:Landroid/app/Notification;

    #@45
    if-nez v6, :cond_5b

    #@47
    .line 2837
    const-string v6, "Tethering"

    #@49
    const-string v7, "[TetherSettings]  Create New Notification !!!! "

    #@4b
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    .line 2838
    new-instance v6, Landroid/app/Notification;

    #@50
    invoke-direct {v6}, Landroid/app/Notification;-><init>()V

    #@53
    iput-object v6, p0, Lcom/android/server/connectivity/Tethering;->mPermissionErrorNotification:Landroid/app/Notification;

    #@55
    .line 2839
    iget-object v6, p0, Lcom/android/server/connectivity/Tethering;->mPermissionErrorNotification:Landroid/app/Notification;

    #@57
    const-wide/16 v7, 0x0

    #@59
    iput-wide v7, v6, Landroid/app/Notification;->when:J

    #@5b
    .line 2841
    :cond_5b
    iget-object v6, p0, Lcom/android/server/connectivity/Tethering;->mPermissionErrorNotification:Landroid/app/Notification;

    #@5d
    iput p1, v6, Landroid/app/Notification;->icon:I

    #@5f
    .line 2842
    iget-object v6, p0, Lcom/android/server/connectivity/Tethering;->mPermissionErrorNotification:Landroid/app/Notification;

    #@61
    iget v7, v6, Landroid/app/Notification;->defaults:I

    #@63
    and-int/lit8 v7, v7, -0x2

    #@65
    iput v7, v6, Landroid/app/Notification;->defaults:I

    #@67
    .line 2843
    iget-object v6, p0, Lcom/android/server/connectivity/Tethering;->mPermissionErrorNotification:Landroid/app/Notification;

    #@69
    const/16 v7, 0x10

    #@6b
    iput v7, v6, Landroid/app/Notification;->flags:I

    #@6d
    .line 2844
    iget-object v6, p0, Lcom/android/server/connectivity/Tethering;->mPermissionErrorNotification:Landroid/app/Notification;

    #@6f
    iput-object v5, v6, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    #@71
    .line 2845
    iget-object v6, p0, Lcom/android/server/connectivity/Tethering;->mPermissionErrorNotification:Landroid/app/Notification;

    #@73
    sget-object v7, Lcom/android/server/connectivity/Tethering;->mContext:Landroid/content/Context;

    #@75
    invoke-virtual {v6, v7, v5, v1, v3}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    #@78
    .line 2847
    iget-object v6, p0, Lcom/android/server/connectivity/Tethering;->mPermissionErrorNotification:Landroid/app/Notification;

    #@7a
    iget v6, v6, Landroid/app/Notification;->icon:I

    #@7c
    iget-object v7, p0, Lcom/android/server/connectivity/Tethering;->mPermissionErrorNotification:Landroid/app/Notification;

    #@7e
    invoke-virtual {v2, v6, v7}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    #@81
    goto :goto_17
.end method

.method public tether(Ljava/lang/String;)I
    .registers 14
    .parameter "iface"

    #@0
    .prologue
    const/4 v11, 0x0

    #@1
    const/4 v6, 0x4

    #@2
    const/4 v7, 0x3

    #@3
    const/4 v5, 0x2

    #@4
    const/4 v8, 0x0

    #@5
    .line 604
    const-string v4, "Tethering"

    #@7
    new-instance v9, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v10, "Tethering "

    #@e
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v9

    #@12
    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v9

    #@16
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v9

    #@1a
    invoke-static {v4, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    .line 606
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@20
    move-result-object v4

    #@21
    if-eqz v4, :cond_2f

    #@23
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@26
    move-result-object v4

    #@27
    invoke-interface {v4, v6}, Lcom/lge/cappuccino/IMdm;->checkDisabledTetherType(I)Z

    #@2a
    move-result v4

    #@2b
    if-eqz v4, :cond_2f

    #@2d
    move v4, v5

    #@2e
    .line 695
    :goto_2e
    return v4

    #@2f
    .line 612
    :cond_2f
    iget-object v4, p0, Lcom/android/server/connectivity/Tethering;->TARGET_OPERATOR:Ljava/lang/String;

    #@31
    const-string v9, "VZW"

    #@33
    invoke-virtual {v4, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@36
    move-result v4

    #@37
    if-eqz v4, :cond_48

    #@39
    .line 613
    const-string v4, "usb0"

    #@3b
    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3e
    move-result v4

    #@3f
    if-eqz v4, :cond_48

    #@41
    iget-boolean v4, p0, Lcom/android/server/connectivity/Tethering;->usb0InterfaceAdd:Z

    #@43
    if-nez v4, :cond_48

    #@45
    .line 614
    invoke-virtual {p0, p1}, Lcom/android/server/connectivity/Tethering;->interfaceAdded(Ljava/lang/String;)V

    #@48
    .line 618
    :cond_48
    const/4 v3, 0x0

    #@49
    .line 619
    .local v3, sm:Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;
    iget-object v9, p0, Lcom/android/server/connectivity/Tethering;->mPublicSync:Ljava/lang/Object;

    #@4b
    monitor-enter v9

    #@4c
    .line 620
    :try_start_4c
    iget-object v4, p0, Lcom/android/server/connectivity/Tethering;->mIfaces:Ljava/util/HashMap;

    #@4e
    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@51
    move-result-object v4

    #@52
    move-object v0, v4

    #@53
    check-cast v0, Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;

    #@55
    move-object v3, v0

    #@56
    .line 621
    monitor-exit v9
    :try_end_57
    .catchall {:try_start_4c .. :try_end_57} :catchall_79

    #@57
    .line 622
    if-nez v3, :cond_7c

    #@59
    .line 623
    const-string v4, "Tethering"

    #@5b
    new-instance v5, Ljava/lang/StringBuilder;

    #@5d
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@60
    const-string v6, "Tried to Tether an unknown iface :"

    #@62
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v5

    #@66
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v5

    #@6a
    const-string v6, ", ignoring"

    #@6c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v5

    #@70
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@73
    move-result-object v5

    #@74
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@77
    .line 624
    const/4 v4, 0x1

    #@78
    goto :goto_2e

    #@79
    .line 621
    :catchall_79
    move-exception v4

    #@7a
    :try_start_7a
    monitor-exit v9
    :try_end_7b
    .catchall {:try_start_7a .. :try_end_7b} :catchall_79

    #@7b
    throw v4

    #@7c
    .line 626
    :cond_7c
    invoke-virtual {v3}, Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;->isAvailable()Z

    #@7f
    move-result v4

    #@80
    if-nez v4, :cond_a8

    #@82
    invoke-virtual {v3}, Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;->isErrored()Z

    #@85
    move-result v4

    #@86
    if-nez v4, :cond_a8

    #@88
    .line 627
    const-string v4, "Tethering"

    #@8a
    new-instance v5, Ljava/lang/StringBuilder;

    #@8c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@8f
    const-string v7, "Tried to Tether an unavailable iface :"

    #@91
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v5

    #@95
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@98
    move-result-object v5

    #@99
    const-string v7, ", ignoring"

    #@9b
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v5

    #@9f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a2
    move-result-object v5

    #@a3
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@a6
    move v4, v6

    #@a7
    .line 628
    goto :goto_2e

    #@a8
    .line 632
    :cond_a8
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getCountry()Ljava/lang/String;

    #@ab
    move-result-object v4

    #@ac
    const-string v6, "US"

    #@ae
    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b1
    move-result v4

    #@b2
    if-eqz v4, :cond_160

    #@b4
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->isWifiChameleonFeaturedCarrier()Z

    #@b7
    move-result v4

    #@b8
    if-nez v4, :cond_c6

    #@ba
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getOperator()Ljava/lang/String;

    #@bd
    move-result-object v4

    #@be
    const-string v6, "ACG"

    #@c0
    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c3
    move-result v4

    #@c4
    if-eqz v4, :cond_160

    #@c6
    .line 636
    :cond_c6
    const-string v4, "Tethering"

    #@c8
    const-string v6, "tether Start TetherNetwork check"

    #@ca
    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@cd
    .line 638
    iget-object v4, p0, Lcom/android/server/connectivity/Tethering;->mTetherNetwork:Lcom/android/server/connectivity/TetherNetwork;

    #@cf
    if-eqz v4, :cond_102

    #@d1
    iget-object v4, p0, Lcom/android/server/connectivity/Tethering;->mTetherNetwork:Lcom/android/server/connectivity/TetherNetwork;

    #@d3
    invoke-direct {p0, p1}, Lcom/android/server/connectivity/Tethering;->isUsb(Ljava/lang/String;)Z

    #@d6
    move-result v6

    #@d7
    invoke-virtual {p0, p1}, Lcom/android/server/connectivity/Tethering;->isWifi(Ljava/lang/String;)Z

    #@da
    move-result v9

    #@db
    invoke-virtual {p0, p1}, Lcom/android/server/connectivity/Tethering;->isBluetooth(Ljava/lang/String;)Z

    #@de
    move-result v10

    #@df
    invoke-virtual {v4, v6, v9, v10, v8}, Lcom/android/server/connectivity/TetherNetwork;->isAllowedTetherableInterface(ZZZZ)Z

    #@e2
    move-result v4

    #@e3
    if-nez v4, :cond_102

    #@e5
    .line 640
    sget-object v4, Lcom/android/server/connectivity/Tethering;->mContext:Landroid/content/Context;

    #@e7
    const-string v5, "wifi"

    #@e9
    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@ec
    move-result-object v2

    #@ed
    check-cast v2, Landroid/net/wifi/WifiManager;

    #@ef
    .line 641
    .local v2, mWifiManager:Landroid/net/wifi/WifiManager;
    const-string v4, "Tethering"

    #@f1
    const-string v5, "isTetherNetworkAvail false"

    #@f3
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@f6
    .line 644
    invoke-virtual {p0, p1}, Lcom/android/server/connectivity/Tethering;->isWifi(Ljava/lang/String;)Z

    #@f9
    move-result v4

    #@fa
    if-eqz v4, :cond_ff

    #@fc
    .line 645
    invoke-virtual {v2, v11, v8}, Landroid/net/wifi/WifiManager;->setWifiApEnabled(Landroid/net/wifi/WifiConfiguration;Z)Z

    #@ff
    :cond_ff
    move v4, v7

    #@100
    .line 651
    goto/16 :goto_2e

    #@102
    .line 655
    .end local v2           #mWifiManager:Landroid/net/wifi/WifiManager;
    :cond_102
    const-string v4, "Tethering"

    #@104
    const-string v6, "tether Start mTetherNetwork is not created or Tether feature is blocked."

    #@106
    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@109
    .line 659
    iget-object v4, p0, Lcom/android/server/connectivity/Tethering;->mTetherNetwork:Lcom/android/server/connectivity/TetherNetwork;

    #@10b
    if-eqz v4, :cond_160

    #@10d
    iget-object v4, p0, Lcom/android/server/connectivity/Tethering;->mTetherNetwork:Lcom/android/server/connectivity/TetherNetwork;

    #@10f
    invoke-virtual {v4}, Lcom/android/server/connectivity/TetherNetwork;->isTetherNetworkAvail()Z

    #@112
    move-result v4

    #@113
    if-nez v4, :cond_160

    #@115
    .line 661
    sget-object v4, Lcom/android/server/connectivity/Tethering;->mContext:Landroid/content/Context;

    #@117
    const-string v5, "wifi"

    #@119
    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@11c
    move-result-object v2

    #@11d
    check-cast v2, Landroid/net/wifi/WifiManager;

    #@11f
    .line 662
    .restart local v2       #mWifiManager:Landroid/net/wifi/WifiManager;
    const-string v4, "Tethering"

    #@121
    const-string v5, "isTetherNetworkAvail false"

    #@123
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@126
    .line 665
    invoke-virtual {p0, p1}, Lcom/android/server/connectivity/Tethering;->isWifi(Ljava/lang/String;)Z

    #@129
    move-result v4

    #@12a
    if-eqz v4, :cond_12f

    #@12c
    .line 666
    invoke-virtual {v2, v11, v8}, Landroid/net/wifi/WifiManager;->setWifiApEnabled(Landroid/net/wifi/WifiConfiguration;Z)Z

    #@12f
    .line 670
    :cond_12f
    const-string v4, "SPR"

    #@131
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getOperator()Ljava/lang/String;

    #@134
    move-result-object v5

    #@135
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@138
    move-result v4

    #@139
    if-eqz v4, :cond_158

    #@13b
    .line 671
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    #@13e
    move-result-object v1

    #@13f
    .line 673
    .local v1, adapter:Landroid/bluetooth/BluetoothAdapter;
    if-eqz v1, :cond_149

    #@141
    .line 674
    sget-object v4, Lcom/android/server/connectivity/Tethering;->mContext:Landroid/content/Context;

    #@143
    iget-object v5, p0, Lcom/android/server/connectivity/Tethering;->mProfileServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    #@145
    const/4 v6, 0x5

    #@146
    invoke-virtual {v1, v4, v5, v6}, Landroid/bluetooth/BluetoothAdapter;->getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z

    #@149
    .line 678
    :cond_149
    invoke-virtual {p0, p1}, Lcom/android/server/connectivity/Tethering;->isBluetooth(Ljava/lang/String;)Z

    #@14c
    move-result v4

    #@14d
    if-eqz v4, :cond_158

    #@14f
    .line 679
    iget-object v4, p0, Lcom/android/server/connectivity/Tethering;->mBluetoothPan:Landroid/bluetooth/BluetoothPan;

    #@151
    if-eqz v4, :cond_158

    #@153
    .line 680
    const/16 v4, 0x66

    #@155
    invoke-virtual {v3, v4}, Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;->sendMessage(I)V

    #@158
    .line 686
    .end local v1           #adapter:Landroid/bluetooth/BluetoothAdapter;
    :cond_158
    const/16 v4, 0x65

    #@15a
    invoke-virtual {v3, v4}, Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;->sendMessage(I)V

    #@15d
    move v4, v7

    #@15e
    .line 687
    goto/16 :goto_2e

    #@160
    .line 694
    .end local v2           #mWifiManager:Landroid/net/wifi/WifiManager;
    :cond_160
    invoke-virtual {v3, v5}, Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;->sendMessage(I)V

    #@163
    move v4, v8

    #@164
    .line 695
    goto/16 :goto_2e
.end method

.method public untether(Ljava/lang/String;)I
    .registers 7
    .parameter "iface"

    #@0
    .prologue
    .line 699
    const-string v2, "Tethering"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "Untethering "

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v3

    #@15
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 700
    const/4 v1, 0x0

    #@19
    .line 701
    .local v1, sm:Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;
    iget-object v3, p0, Lcom/android/server/connectivity/Tethering;->mPublicSync:Ljava/lang/Object;

    #@1b
    monitor-enter v3

    #@1c
    .line 702
    :try_start_1c
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering;->mIfaces:Ljava/util/HashMap;

    #@1e
    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@21
    move-result-object v2

    #@22
    move-object v0, v2

    #@23
    check-cast v0, Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;

    #@25
    move-object v1, v0

    #@26
    .line 703
    monitor-exit v3
    :try_end_27
    .catchall {:try_start_1c .. :try_end_27} :catchall_49

    #@27
    .line 704
    if-nez v1, :cond_4c

    #@29
    .line 705
    const-string v2, "Tethering"

    #@2b
    new-instance v3, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    const-string v4, "Tried to Untether an unknown iface :"

    #@32
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v3

    #@36
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v3

    #@3a
    const-string v4, ", ignoring"

    #@3c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v3

    #@40
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v3

    #@44
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@47
    .line 706
    const/4 v2, 0x1

    #@48
    .line 713
    :goto_48
    return v2

    #@49
    .line 703
    :catchall_49
    move-exception v2

    #@4a
    :try_start_4a
    monitor-exit v3
    :try_end_4b
    .catchall {:try_start_4a .. :try_end_4b} :catchall_49

    #@4b
    throw v2

    #@4c
    .line 708
    :cond_4c
    invoke-virtual {v1}, Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;->isErrored()Z

    #@4f
    move-result v2

    #@50
    if-eqz v2, :cond_72

    #@52
    .line 709
    const-string v2, "Tethering"

    #@54
    new-instance v3, Ljava/lang/StringBuilder;

    #@56
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@59
    const-string v4, "Tried to Untethered an errored iface :"

    #@5b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v3

    #@5f
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v3

    #@63
    const-string v4, ", ignoring"

    #@65
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v3

    #@69
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6c
    move-result-object v3

    #@6d
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@70
    .line 710
    const/4 v2, 0x4

    #@71
    goto :goto_48

    #@72
    .line 712
    :cond_72
    const/4 v2, 0x3

    #@73
    invoke-virtual {v1, v2}, Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;->sendMessage(I)V

    #@76
    .line 713
    const/4 v2, 0x0

    #@77
    goto :goto_48
.end method

.method updateConfiguration()V
    .registers 18

    #@0
    .prologue
    .line 433
    sget-object v14, Lcom/android/server/connectivity/Tethering;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@5
    move-result-object v14

    #@6
    const v15, 0x1070020

    #@9
    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    #@c
    move-result-object v11

    #@d
    .line 435
    .local v11, tetherableUsbRegexs:[Ljava/lang/String;
    sget-object v14, Lcom/android/server/connectivity/Tethering;->mContext:Landroid/content/Context;

    #@f
    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@12
    move-result-object v14

    #@13
    const v15, 0x1070021

    #@16
    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    #@19
    move-result-object v12

    #@1a
    .line 437
    .local v12, tetherableWifiRegexs:[Ljava/lang/String;
    sget-object v14, Lcom/android/server/connectivity/Tethering;->mContext:Landroid/content/Context;

    #@1c
    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@1f
    move-result-object v14

    #@20
    const v15, 0x1070023

    #@23
    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    #@26
    move-result-object v10

    #@27
    .line 440
    .local v10, tetherableBluetoothRegexs:[Ljava/lang/String;
    sget-object v14, Lcom/android/server/connectivity/Tethering;->mContext:Landroid/content/Context;

    #@29
    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@2c
    move-result-object v14

    #@2d
    const v15, 0x1070026

    #@30
    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getIntArray(I)[I

    #@33
    move-result-object v8

    #@34
    .line 442
    .local v8, ifaceTypes:[I
    new-instance v13, Ljava/util/ArrayList;

    #@36
    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    #@39
    .line 443
    .local v13, upstreamIfaceTypes:Ljava/util/Collection;,"Ljava/util/Collection<Ljava/lang/Integer;>;"
    const-string v14, "connectivity"

    #@3b
    invoke-static {v14}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@3e
    move-result-object v3

    #@3f
    .line 444
    .local v3, b:Landroid/os/IBinder;
    invoke-static {v3}, Landroid/net/IConnectivityManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/IConnectivityManager;

    #@42
    move-result-object v4

    #@43
    .line 446
    .local v4, cm:Landroid/net/IConnectivityManager;
    :try_start_43
    invoke-interface {v4}, Landroid/net/IConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    #@46
    move-result-object v14

    #@47
    invoke-virtual {v14}, Landroid/net/NetworkInfo;->getType()I

    #@4a
    move-result v1

    #@4b
    .line 447
    .local v1, activeNetType:I
    move-object v2, v8

    #@4c
    .local v2, arr$:[I
    array-length v9, v2

    #@4d
    .local v9, len$:I
    const/4 v7, 0x0

    #@4e
    .local v7, i$:I
    :goto_4e
    if-ge v7, v9, :cond_78

    #@50
    aget v6, v2, v7

    #@52
    .line 448
    .local v6, i:I
    if-ne v6, v1, :cond_5c

    #@54
    .line 449
    new-instance v14, Ljava/lang/Integer;

    #@56
    invoke-direct {v14, v6}, Ljava/lang/Integer;-><init>(I)V

    #@59
    invoke-interface {v13, v14}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z
    :try_end_5c
    .catch Ljava/lang/Exception; {:try_start_43 .. :try_end_5c} :catch_5f

    #@5c
    .line 447
    :cond_5c
    add-int/lit8 v7, v7, 0x1

    #@5e
    goto :goto_4e

    #@5f
    .line 452
    .end local v1           #activeNetType:I
    .end local v2           #arr$:[I
    .end local v6           #i:I
    .end local v7           #i$:I
    .end local v9           #len$:I
    :catch_5f
    move-exception v5

    #@60
    .line 453
    .local v5, e:Ljava/lang/Exception;
    const-string v14, "Tethering"

    #@62
    new-instance v15, Ljava/lang/StringBuilder;

    #@64
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@67
    const-string v16, "Exception adding default nw to upstreamIfaceTypes: "

    #@69
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v15

    #@6d
    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v15

    #@71
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@74
    move-result-object v15

    #@75
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@78
    .line 455
    .end local v5           #e:Ljava/lang/Exception;
    :cond_78
    move-object v2, v8

    #@79
    .restart local v2       #arr$:[I
    array-length v9, v2

    #@7a
    .restart local v9       #len$:I
    const/4 v7, 0x0

    #@7b
    .restart local v7       #i$:I
    :goto_7b
    if-ge v7, v9, :cond_95

    #@7d
    aget v6, v2, v7

    #@7f
    .line 456
    .restart local v6       #i:I
    new-instance v14, Ljava/lang/Integer;

    #@81
    invoke-direct {v14, v6}, Ljava/lang/Integer;-><init>(I)V

    #@84
    invoke-interface {v13, v14}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    #@87
    move-result v14

    #@88
    if-nez v14, :cond_92

    #@8a
    .line 457
    new-instance v14, Ljava/lang/Integer;

    #@8c
    invoke-direct {v14, v6}, Ljava/lang/Integer;-><init>(I)V

    #@8f
    invoke-interface {v13, v14}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    #@92
    .line 455
    :cond_92
    add-int/lit8 v7, v7, 0x1

    #@94
    goto :goto_7b

    #@95
    .line 461
    .end local v6           #i:I
    :cond_95
    move-object/from16 v0, p0

    #@97
    iget-object v15, v0, Lcom/android/server/connectivity/Tethering;->mPublicSync:Ljava/lang/Object;

    #@99
    monitor-enter v15

    #@9a
    .line 462
    :try_start_9a
    move-object/from16 v0, p0

    #@9c
    iput-object v11, v0, Lcom/android/server/connectivity/Tethering;->mTetherableUsbRegexs:[Ljava/lang/String;

    #@9e
    .line 463
    move-object/from16 v0, p0

    #@a0
    iput-object v12, v0, Lcom/android/server/connectivity/Tethering;->mTetherableWifiRegexs:[Ljava/lang/String;

    #@a2
    .line 464
    move-object/from16 v0, p0

    #@a4
    iput-object v10, v0, Lcom/android/server/connectivity/Tethering;->mTetherableBluetoothRegexs:[Ljava/lang/String;

    #@a6
    .line 465
    move-object/from16 v0, p0

    #@a8
    iput-object v13, v0, Lcom/android/server/connectivity/Tethering;->mUpstreamIfaceTypes:Ljava/util/Collection;

    #@aa
    .line 466
    monitor-exit v15
    :try_end_ab
    .catchall {:try_start_9a .. :try_end_ab} :catchall_af

    #@ab
    .line 469
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/connectivity/Tethering;->checkDunRequired()V

    #@ae
    .line 470
    return-void

    #@af
    .line 466
    :catchall_af
    move-exception v14

    #@b0
    :try_start_b0
    monitor-exit v15
    :try_end_b1
    .catchall {:try_start_b0 .. :try_end_b1} :catchall_af

    #@b1
    throw v14
.end method
