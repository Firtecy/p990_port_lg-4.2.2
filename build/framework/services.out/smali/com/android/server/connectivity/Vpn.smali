.class public Lcom/android/server/connectivity/Vpn;
.super Landroid/net/BaseNetworkStateTracker;
.source "Vpn.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;,
        Lcom/android/server/connectivity/Vpn$Connection;
    }
.end annotation


# static fields
.field private static final LOGD:Z = true

.field private static final MVP_FILE_NAME:Ljava/lang/String; = "/data/misc/vpn/MVPClients"

.field private static final TAG:Ljava/lang/String; = "Vpn"


# instance fields
.field private final mCallback:Lcom/android/server/ConnectivityService$VpnCallback;

.field private mConnection:Lcom/android/server/connectivity/Vpn$Connection;

.field private mEnableNotif:Z

.field private mInterface:Ljava/lang/String;

.field private mLegacyVpnRunner:Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;

.field private mMvpOn:Z

.field private mNamespace:Lcom/android/server/connectivity/VpnNamespace;

.field private mObserver:Landroid/net/INetworkManagementEventObserver;

.field private mPackage:Ljava/lang/String;

.field private mStatusIntent:Landroid/app/PendingIntent;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/server/ConnectivityService$VpnCallback;Landroid/os/INetworkManagementService;)V
    .registers 7
    .parameter "context"
    .parameter "callback"
    .parameter "netService"

    #@0
    .prologue
    .line 121
    const/16 v1, 0x8

    #@2
    invoke-direct {p0, v1}, Landroid/net/BaseNetworkStateTracker;-><init>(I)V

    #@5
    .line 106
    const-string v1, "[Legacy VPN]"

    #@7
    iput-object v1, p0, Lcom/android/server/connectivity/Vpn;->mPackage:Ljava/lang/String;

    #@9
    .line 111
    const/4 v1, 0x1

    #@a
    iput-boolean v1, p0, Lcom/android/server/connectivity/Vpn;->mEnableNotif:Z

    #@c
    .line 114
    const/4 v1, 0x0

    #@d
    iput-boolean v1, p0, Lcom/android/server/connectivity/Vpn;->mMvpOn:Z

    #@f
    .line 752
    new-instance v1, Lcom/android/server/connectivity/Vpn$2;

    #@11
    invoke-direct {v1, p0}, Lcom/android/server/connectivity/Vpn$2;-><init>(Lcom/android/server/connectivity/Vpn;)V

    #@14
    iput-object v1, p0, Lcom/android/server/connectivity/Vpn;->mObserver:Landroid/net/INetworkManagementEventObserver;

    #@16
    .line 122
    iput-object p1, p0, Lcom/android/server/connectivity/Vpn;->mContext:Landroid/content/Context;

    #@18
    .line 123
    iput-object p2, p0, Lcom/android/server/connectivity/Vpn;->mCallback:Lcom/android/server/ConnectivityService$VpnCallback;

    #@1a
    .line 126
    :try_start_1a
    iget-object v1, p0, Lcom/android/server/connectivity/Vpn;->mObserver:Landroid/net/INetworkManagementEventObserver;

    #@1c
    invoke-interface {p3, v1}, Landroid/os/INetworkManagementService;->registerObserver(Landroid/net/INetworkManagementEventObserver;)V
    :try_end_1f
    .catch Landroid/os/RemoteException; {:try_start_1a .. :try_end_1f} :catch_20

    #@1f
    .line 130
    :goto_1f
    return-void

    #@20
    .line 127
    :catch_20
    move-exception v0

    #@21
    .line 128
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "Vpn"

    #@23
    const-string v2, "Problem registering observer"

    #@25
    invoke-static {v1, v2, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@28
    goto :goto_1f
.end method

.method static synthetic access$000(Lcom/android/server/connectivity/Vpn;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 97
    invoke-direct {p0, p1}, Lcom/android/server/connectivity/Vpn;->removeMvpClientFromList(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$1000(Lcom/android/server/connectivity/Vpn;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 97
    iget-object v0, p0, Lcom/android/server/connectivity/Vpn;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$1100(Lcom/android/server/connectivity/Vpn;Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 97
    invoke-direct {p0, p1, p2}, Lcom/android/server/connectivity/Vpn;->updateState(Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$1200(Lcom/android/server/connectivity/Vpn;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 97
    invoke-direct {p0}, Lcom/android/server/connectivity/Vpn;->mvpInterfaceRemoved()V

    #@3
    return-void
.end method

.method static synthetic access$1400(Lcom/android/server/connectivity/Vpn;Ljava/lang/String;Ljava/lang/String;)I
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 97
    invoke-direct {p0, p1, p2}, Lcom/android/server/connectivity/Vpn;->jniSetRoutes(Ljava/lang/String;Ljava/lang/String;)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1500(Lcom/android/server/connectivity/Vpn;Lcom/android/internal/net/VpnConfig;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 97
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/connectivity/Vpn;->showNotification(Lcom/android/internal/net/VpnConfig;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    #@3
    return-void
.end method

.method static synthetic access$1600(Lcom/android/server/connectivity/Vpn;)Landroid/net/NetworkInfo;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 97
    iget-object v0, p0, Lcom/android/server/connectivity/Vpn;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@2
    return-object v0
.end method

.method static synthetic access$1700(Lcom/android/server/connectivity/Vpn;)Landroid/net/NetworkInfo;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 97
    iget-object v0, p0, Lcom/android/server/connectivity/Vpn;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/android/server/connectivity/Vpn;)Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 97
    iget-object v0, p0, Lcom/android/server/connectivity/Vpn;->mLegacyVpnRunner:Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;

    #@2
    return-object v0
.end method

.method static synthetic access$302(Lcom/android/server/connectivity/Vpn;Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;)Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 97
    iput-object p1, p0, Lcom/android/server/connectivity/Vpn;->mLegacyVpnRunner:Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;

    #@2
    return-object p1
.end method

.method static synthetic access$400(Lcom/android/server/connectivity/Vpn;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 97
    iget-object v0, p0, Lcom/android/server/connectivity/Vpn;->mInterface:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$402(Lcom/android/server/connectivity/Vpn;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 97
    iput-object p1, p0, Lcom/android/server/connectivity/Vpn;->mInterface:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$500(Lcom/android/server/connectivity/Vpn;Ljava/lang/String;)I
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 97
    invoke-direct {p0, p1}, Lcom/android/server/connectivity/Vpn;->jniCheck(Ljava/lang/String;)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$600(Lcom/android/server/connectivity/Vpn;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 97
    iget-boolean v0, p0, Lcom/android/server/connectivity/Vpn;->mMvpOn:Z

    #@2
    return v0
.end method

.method static synthetic access$700(Lcom/android/server/connectivity/Vpn;)Lcom/android/server/ConnectivityService$VpnCallback;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 97
    iget-object v0, p0, Lcom/android/server/connectivity/Vpn;->mCallback:Lcom/android/server/ConnectivityService$VpnCallback;

    #@2
    return-object v0
.end method

.method static synthetic access$800(Lcom/android/server/connectivity/Vpn;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 97
    invoke-direct {p0}, Lcom/android/server/connectivity/Vpn;->hideNotification()V

    #@3
    return-void
.end method

.method static synthetic access$900(Lcom/android/server/connectivity/Vpn;)Lcom/android/server/connectivity/Vpn$Connection;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 97
    iget-object v0, p0, Lcom/android/server/connectivity/Vpn;->mConnection:Lcom/android/server/connectivity/Vpn$Connection;

    #@2
    return-object v0
.end method

.method static synthetic access$902(Lcom/android/server/connectivity/Vpn;Lcom/android/server/connectivity/Vpn$Connection;)Lcom/android/server/connectivity/Vpn$Connection;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 97
    iput-object p1, p0, Lcom/android/server/connectivity/Vpn;->mConnection:Lcom/android/server/connectivity/Vpn$Connection;

    #@2
    return-object p1
.end method

.method private addMvpClientToList(Ljava/lang/String;)V
    .registers 9
    .parameter "clientPackageName"

    #@0
    .prologue
    .line 221
    new-instance v3, Ljava/io/File;

    #@2
    const-string v4, "/data/misc/vpn/MVPClients"

    #@4
    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@7
    .line 222
    .local v3, mvpFile:Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    #@a
    move-result v4

    #@b
    if-eqz v4, :cond_13

    #@d
    invoke-direct {p0, p1, v3}, Lcom/android/server/connectivity/Vpn;->isAnMvpClient(Ljava/lang/String;Ljava/io/File;)Z

    #@10
    move-result v4

    #@11
    if-nez v4, :cond_5b

    #@13
    .line 225
    :cond_13
    :try_start_13
    new-instance v1, Ljava/io/FileOutputStream;

    #@15
    const/4 v4, 0x1

    #@16
    invoke-direct {v1, v3, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    #@19
    .line 226
    .local v1, fos:Ljava/io/FileOutputStream;
    new-instance v0, Ljava/io/BufferedWriter;

    #@1b
    new-instance v4, Ljava/io/OutputStreamWriter;

    #@1d
    invoke-direct {v4, v1}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    #@20
    const/16 v5, 0x100

    #@22
    invoke-direct {v0, v4, v5}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;I)V
    :try_end_25
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_25} :catch_61

    #@25
    .line 228
    .local v0, bWriter:Ljava/io/BufferedWriter;
    const/4 v4, 0x0

    #@26
    :try_start_26
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@29
    move-result v5

    #@2a
    invoke-virtual {v0, p1, v4, v5}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;II)V

    #@2d
    .line 229
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->newLine()V

    #@30
    .line 230
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->flush()V

    #@33
    .line 232
    const-string v4, "Vpn"

    #@35
    new-instance v5, Ljava/lang/StringBuilder;

    #@37
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@3a
    const-string v6, "Client "

    #@3c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v5

    #@40
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v5

    #@44
    const-string v6, " set as MVP client"

    #@46
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v5

    #@4a
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v5

    #@4e
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@51
    .line 234
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    #@54
    move-result-object v4

    #@55
    invoke-virtual {v4}, Ljava/io/FileDescriptor;->sync()V
    :try_end_58
    .catchall {:try_start_26 .. :try_end_58} :catchall_5c

    #@58
    .line 236
    :try_start_58
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V

    #@5b
    .line 242
    .end local v0           #bWriter:Ljava/io/BufferedWriter;
    .end local v1           #fos:Ljava/io/FileOutputStream;
    :cond_5b
    :goto_5b
    return-void

    #@5c
    .line 236
    .restart local v0       #bWriter:Ljava/io/BufferedWriter;
    .restart local v1       #fos:Ljava/io/FileOutputStream;
    :catchall_5c
    move-exception v4

    #@5d
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V

    #@60
    throw v4
    :try_end_61
    .catch Ljava/io/IOException; {:try_start_58 .. :try_end_61} :catch_61

    #@61
    .line 238
    .end local v0           #bWriter:Ljava/io/BufferedWriter;
    .end local v1           #fos:Ljava/io/FileOutputStream;
    :catch_61
    move-exception v2

    #@62
    .line 239
    .local v2, ioe:Ljava/io/IOException;
    const-string v4, "Vpn"

    #@64
    const-string v5, "Not able to create or write into MVP File"

    #@66
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@69
    goto :goto_5b
.end method

.method private clearMvpList(Ljava/io/File;)V
    .registers 13
    .parameter "mvpFile"

    #@0
    .prologue
    .line 296
    iget-object v8, p0, Lcom/android/server/connectivity/Vpn;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v8}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@5
    move-result-object v5

    #@6
    .line 298
    .local v5, pm:Landroid/content/pm/PackageManager;
    :try_start_6
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@9
    move-result-object v0

    #@a
    .line 299
    .local v0, am:Landroid/app/IActivityManager;
    if-eqz v0, :cond_54

    #@c
    .line 300
    invoke-direct {p0, p1}, Lcom/android/server/connectivity/Vpn;->getMvpClientList(Ljava/io/File;)Ljava/util/ArrayList;

    #@f
    move-result-object v8

    #@10
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@13
    move-result-object v3

    #@14
    .local v3, i$:Ljava/util/Iterator;
    :goto_14
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@17
    move-result v8

    #@18
    if-eqz v8, :cond_54

    #@1a
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1d
    move-result-object v1

    #@1e
    check-cast v1, Ljava/lang/String;

    #@20
    .line 302
    .local v1, client:Ljava/lang/String;
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_23
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_23} :catch_4c

    #@23
    move-result-wide v6

    #@24
    .line 304
    .local v6, token:J
    :try_start_24
    const-string v8, "Vpn"

    #@26
    new-instance v9, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    const-string v10, "Clearing data for client "

    #@2d
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v9

    #@31
    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v9

    #@35
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v9

    #@39
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3c
    .line 305
    new-instance v8, Lcom/android/server/connectivity/Vpn$1;

    #@3e
    invoke-direct {v8, p0}, Lcom/android/server/connectivity/Vpn$1;-><init>(Lcom/android/server/connectivity/Vpn;)V

    #@41
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@44
    move-result v9

    #@45
    invoke-interface {v0, v1, v8, v9}, Landroid/app/IActivityManager;->clearApplicationUserData(Ljava/lang/String;Landroid/content/pm/IPackageDataObserver;I)Z
    :try_end_48
    .catchall {:try_start_24 .. :try_end_48} :catchall_72
    .catch Landroid/os/RemoteException; {:try_start_24 .. :try_end_48} :catch_55

    #@48
    .line 315
    :try_start_48
    invoke-static {v6, v7}, Landroid/os/Binder;->restoreCallingIdentity(J)V
    :try_end_4b
    .catch Ljava/io/IOException; {:try_start_48 .. :try_end_4b} :catch_4c

    #@4b
    goto :goto_14

    #@4c
    .line 319
    .end local v0           #am:Landroid/app/IActivityManager;
    .end local v1           #client:Ljava/lang/String;
    .end local v3           #i$:Ljava/util/Iterator;
    .end local v6           #token:J
    :catch_4c
    move-exception v4

    #@4d
    .line 320
    .local v4, ioe:Ljava/io/IOException;
    const-string v8, "Vpn"

    #@4f
    const-string v9, "Not able to get MVP list to clear it"

    #@51
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@54
    .line 322
    .end local v4           #ioe:Ljava/io/IOException;
    :cond_54
    return-void

    #@55
    .line 312
    .restart local v0       #am:Landroid/app/IActivityManager;
    .restart local v1       #client:Ljava/lang/String;
    .restart local v3       #i$:Ljava/util/Iterator;
    .restart local v6       #token:J
    :catch_55
    move-exception v2

    #@56
    .line 313
    .local v2, e:Landroid/os/RemoteException;
    :try_start_56
    const-string v8, "Vpn"

    #@58
    new-instance v9, Ljava/lang/StringBuilder;

    #@5a
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@5d
    const-string v10, "Not able to clear client "

    #@5f
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v9

    #@63
    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v9

    #@67
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6a
    move-result-object v9

    #@6b
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6e
    .catchall {:try_start_56 .. :try_end_6e} :catchall_72

    #@6e
    .line 315
    :try_start_6e
    invoke-static {v6, v7}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@71
    goto :goto_14

    #@72
    .end local v2           #e:Landroid/os/RemoteException;
    :catchall_72
    move-exception v8

    #@73
    invoke-static {v6, v7}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@76
    throw v8
    :try_end_77
    .catch Ljava/io/IOException; {:try_start_6e .. :try_end_77} :catch_4c
.end method

.method private enforceControlPermission()V
    .registers 5

    #@0
    .prologue
    .line 820
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@3
    move-result v2

    #@4
    const/16 v3, 0x3e8

    #@6
    if-ne v2, v3, :cond_9

    #@8
    .line 829
    :cond_8
    return-void

    #@9
    .line 826
    :cond_9
    :try_start_9
    iget-object v2, p0, Lcom/android/server/connectivity/Vpn;->mContext:Landroid/content/Context;

    #@b
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@e
    move-result-object v1

    #@f
    .line 827
    .local v1, pm:Landroid/content/pm/PackageManager;
    const-string v2, "com.android.vpndialogs"

    #@11
    const/4 v3, 0x0

    #@12
    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    #@15
    move-result-object v0

    #@16
    .line 828
    .local v0, app:Landroid/content/pm/ApplicationInfo;
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@19
    move-result v2

    #@1a
    iget v3, v0, Landroid/content/pm/ApplicationInfo;->uid:I
    :try_end_1c
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_1c} :catch_26

    #@1c
    if-eq v2, v3, :cond_8

    #@1e
    .line 835
    .end local v0           #app:Landroid/content/pm/ApplicationInfo;
    .end local v1           #pm:Landroid/content/pm/PackageManager;
    :goto_1e
    new-instance v2, Ljava/lang/SecurityException;

    #@20
    const-string v3, "Unauthorized Caller"

    #@22
    invoke-direct {v2, v3}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@25
    throw v2

    #@26
    .line 831
    :catch_26
    move-exception v2

    #@27
    goto :goto_1e
.end method

.method private static findLegacyVpnGateway(Landroid/net/LinkProperties;)Ljava/lang/String;
    .registers 5
    .parameter "prop"

    #@0
    .prologue
    .line 900
    invoke-virtual {p0}, Landroid/net/LinkProperties;->getRoutes()Ljava/util/Collection;

    #@3
    move-result-object v2

    #@4
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@7
    move-result-object v0

    #@8
    .local v0, i$:Ljava/util/Iterator;
    :cond_8
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@b
    move-result v2

    #@c
    if-eqz v2, :cond_2b

    #@e
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@11
    move-result-object v1

    #@12
    check-cast v1, Landroid/net/RouteInfo;

    #@14
    .line 902
    .local v1, route:Landroid/net/RouteInfo;
    invoke-virtual {v1}, Landroid/net/RouteInfo;->isDefaultRoute()Z

    #@17
    move-result v2

    #@18
    if-eqz v2, :cond_8

    #@1a
    invoke-virtual {v1}, Landroid/net/RouteInfo;->getGateway()Ljava/net/InetAddress;

    #@1d
    move-result-object v2

    #@1e
    instance-of v2, v2, Ljava/net/Inet4Address;

    #@20
    if-eqz v2, :cond_8

    #@22
    .line 903
    invoke-virtual {v1}, Landroid/net/RouteInfo;->getGateway()Ljava/net/InetAddress;

    #@25
    move-result-object v2

    #@26
    invoke-virtual {v2}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@29
    move-result-object v2

    #@2a
    return-object v2

    #@2b
    .line 907
    .end local v1           #route:Landroid/net/RouteInfo;
    :cond_2b
    new-instance v2, Ljava/lang/IllegalStateException;

    #@2d
    const-string v3, "Unable to find suitable gateway"

    #@2f
    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@32
    throw v2
.end method

.method private getMvpClientList(Ljava/io/File;)Ljava/util/ArrayList;
    .registers 7
    .parameter "mvpFile"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 178
    new-instance v1, Ljava/util/ArrayList;

    #@2
    const/4 v3, 0x2

    #@3
    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    #@6
    .line 179
    .local v1, clientList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v0, Ljava/io/BufferedReader;

    #@8
    new-instance v3, Ljava/io/InputStreamReader;

    #@a
    new-instance v4, Ljava/io/FileInputStream;

    #@c
    invoke-direct {v4, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    #@f
    invoke-direct {v3, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    #@12
    const/16 v4, 0x100

    #@14
    invoke-direct {v0, v3, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V

    #@17
    .line 181
    .local v0, bReader:Ljava/io/BufferedReader;
    :try_start_17
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    #@1a
    move-result-object v2

    #@1b
    .line 182
    .local v2, mvpPackage:Ljava/lang/String;
    :goto_1b
    if-eqz v2, :cond_2a

    #@1d
    .line 183
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@20
    .line 184
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_23
    .catchall {:try_start_17 .. :try_end_23} :catchall_25

    #@23
    move-result-object v2

    #@24
    goto :goto_1b

    #@25
    .line 187
    .end local v2           #mvpPackage:Ljava/lang/String;
    :catchall_25
    move-exception v3

    #@26
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V

    #@29
    throw v3

    #@2a
    .restart local v2       #mvpPackage:Ljava/lang/String;
    :cond_2a
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V

    #@2d
    .line 189
    return-object v1
.end method

.method private hideNotification()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 880
    iget-boolean v1, p0, Lcom/android/server/connectivity/Vpn;->mEnableNotif:Z

    #@3
    if-nez v1, :cond_6

    #@5
    .line 889
    :cond_5
    :goto_5
    return-void

    #@6
    .line 881
    :cond_6
    iput-object v3, p0, Lcom/android/server/connectivity/Vpn;->mStatusIntent:Landroid/app/PendingIntent;

    #@8
    .line 883
    iget-object v1, p0, Lcom/android/server/connectivity/Vpn;->mContext:Landroid/content/Context;

    #@a
    const-string v2, "notification"

    #@c
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Landroid/app/NotificationManager;

    #@12
    .line 886
    .local v0, nm:Landroid/app/NotificationManager;
    if-eqz v0, :cond_5

    #@14
    .line 887
    const v1, 0x1080616

    #@17
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@19
    invoke-virtual {v0, v3, v1, v2}, Landroid/app/NotificationManager;->cancelAsUser(Ljava/lang/String;ILandroid/os/UserHandle;)V

    #@1c
    goto :goto_5
.end method

.method private isAnMvpClient(Ljava/lang/String;Ljava/io/File;)Z
    .registers 7
    .parameter "clientPackageName"
    .parameter "mvpFile"

    #@0
    .prologue
    .line 204
    :try_start_0
    invoke-direct {p0, p2}, Lcom/android/server/connectivity/Vpn;->getMvpClientList(Ljava/io/File;)Ljava/util/ArrayList;

    #@3
    move-result-object v0

    #@4
    .line 205
    .local v0, clientList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_7} :catch_9

    #@7
    move-result v2

    #@8
    .line 209
    .end local v0           #clientList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_8
    return v2

    #@9
    .line 206
    :catch_9
    move-exception v1

    #@a
    .line 207
    .local v1, ioe:Ljava/io/IOException;
    const-string v2, "Vpn"

    #@c
    const-string v3, "Not able to get MVP list"

    #@e
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 209
    const/4 v2, 0x1

    #@12
    goto :goto_8
.end method

.method private isClientAllowedForHost(Ljava/lang/String;)Z
    .registers 6
    .parameter "clientPackageName"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 351
    new-instance v0, Ljava/io/File;

    #@3
    const-string v2, "/data/misc/vpn/MVPClients"

    #@5
    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@8
    .line 352
    .local v0, mvpFile:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    #@b
    move-result v2

    #@c
    if-nez v2, :cond_f

    #@e
    .line 363
    :cond_e
    :goto_e
    return v1

    #@f
    .line 357
    :cond_f
    invoke-direct {p0}, Lcom/android/server/connectivity/Vpn;->isMvpCleanUpNeeded()Z

    #@12
    move-result v2

    #@13
    if-eqz v2, :cond_1f

    #@15
    .line 358
    const-string v2, "Vpn"

    #@17
    const-string v3, "Clearing client list for MVP"

    #@19
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 359
    invoke-direct {p0, v0}, Lcom/android/server/connectivity/Vpn;->clearMvpList(Ljava/io/File;)V

    #@1f
    .line 363
    :cond_1f
    invoke-direct {p0, p1, v0}, Lcom/android/server/connectivity/Vpn;->isAnMvpClient(Ljava/lang/String;Ljava/io/File;)Z

    #@22
    move-result v2

    #@23
    if-eqz v2, :cond_e

    #@25
    const/4 v1, 0x0

    #@26
    goto :goto_e
.end method

.method private isMvpCleanUpNeeded()Z
    .registers 7

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    .line 332
    iget-object v4, p0, Lcom/android/server/connectivity/Vpn;->mContext:Landroid/content/Context;

    #@4
    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@7
    move-result-object v1

    #@8
    .line 335
    .local v1, pm:Landroid/content/pm/PackageManager;
    :try_start_8
    const-string v4, "com.vmware.mvp.enabled"

    #@a
    const/4 v5, 0x0

    #@b
    invoke-virtual {v1, v4, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    #@e
    move-result-object v4

    #@f
    iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I

    #@11
    invoke-virtual {v1, v4}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    #@14
    move-result-object v4

    #@15
    array-length v4, v4
    :try_end_16
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_8 .. :try_end_16} :catch_1b

    #@16
    if-gt v4, v2, :cond_19

    #@18
    .line 339
    :goto_18
    return v2

    #@19
    :cond_19
    move v2, v3

    #@1a
    .line 335
    goto :goto_18

    #@1b
    .line 336
    :catch_1b
    move-exception v0

    #@1c
    .line 337
    .local v0, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v3, "Vpn"

    #@1e
    const-string v4, "Cannot find com.vmware.mvp.enabled package, we are probably rooted"

    #@20
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    goto :goto_18
.end method

.method private isRevokeNeeded()Z
    .registers 2

    #@0
    .prologue
    .line 373
    iget-boolean v0, p0, Lcom/android/server/connectivity/Vpn;->mMvpOn:Z

    #@2
    if-eqz v0, :cond_c

    #@4
    invoke-direct {p0}, Lcom/android/server/connectivity/Vpn;->isMvpCleanUpNeeded()Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method private native jniCheck(Ljava/lang/String;)I
.end method

.method private native jniCreate(I)I
.end method

.method private native jniGetName(I)Ljava/lang/String;
.end method

.method private native jniProtect(ILjava/lang/String;)V
.end method

.method private native jniReset(Ljava/lang/String;)V
.end method

.method private native jniSetAddresses(Ljava/lang/String;Ljava/lang/String;)I
.end method

.method private native jniSetRoutes(Ljava/lang/String;Ljava/lang/String;)I
.end method

.method private mvpInterfaceRemoved()V
    .registers 6

    #@0
    .prologue
    .line 803
    iget-object v3, p0, Lcom/android/server/connectivity/Vpn;->mNamespace:Lcom/android/server/connectivity/VpnNamespace;

    #@2
    if-eqz v3, :cond_21

    #@4
    .line 804
    iget-object v3, p0, Lcom/android/server/connectivity/Vpn;->mNamespace:Lcom/android/server/connectivity/VpnNamespace;

    #@6
    invoke-virtual {v3}, Lcom/android/server/connectivity/VpnNamespace;->mkIntent()Landroid/content/Intent;

    #@9
    move-result-object v0

    #@a
    .line 805
    .local v0, disconnect:Landroid/content/Intent;
    if-eqz v0, :cond_21

    #@c
    .line 806
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@f
    move-result-wide v1

    #@10
    .line 808
    .local v1, token:J
    :try_start_10
    iget-object v3, p0, Lcom/android/server/connectivity/Vpn;->mContext:Landroid/content/Context;

    #@12
    const-string v4, "com.vmware.mvp.permission.VPN_CONFIG"

    #@14
    invoke-virtual {v3, v0, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V
    :try_end_17
    .catchall {:try_start_10 .. :try_end_17} :catchall_22

    #@17
    .line 810
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@1a
    .line 812
    const-string v3, "Vpn"

    #@1c
    const-string v4, "disconnect event has been sent"

    #@1e
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    .line 815
    .end local v0           #disconnect:Landroid/content/Intent;
    .end local v1           #token:J
    :cond_21
    return-void

    #@22
    .line 810
    .restart local v0       #disconnect:Landroid/content/Intent;
    .restart local v1       #token:J
    :catchall_22
    move-exception v3

    #@23
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@26
    throw v3
.end method

.method private declared-synchronized removeMvpClientFromList(Ljava/lang/String;)V
    .registers 12
    .parameter "clientPackageName"

    #@0
    .prologue
    .line 252
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v6, Ljava/io/File;

    #@3
    const-string v7, "/data/misc/vpn/MVPClients"

    #@5
    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_1 .. :try_end_8} :catchall_60

    #@8
    .line 255
    .local v6, mvpFile:Ljava/io/File;
    :try_start_8
    invoke-direct {p0, v6}, Lcom/android/server/connectivity/Vpn;->getMvpClientList(Ljava/io/File;)Ljava/util/ArrayList;

    #@b
    move-result-object v2

    #@c
    .line 256
    .local v2, clientList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@f
    move-result v7

    #@10
    if-eqz v7, :cond_76

    #@12
    .line 257
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    #@15
    move-result v7

    #@16
    if-eqz v7, :cond_1d

    #@18
    .line 259
    invoke-virtual {v6}, Ljava/io/File;->delete()Z
    :try_end_1b
    .catchall {:try_start_8 .. :try_end_1b} :catchall_60
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_1b} :catch_57

    #@1b
    .line 284
    .end local v2           #clientList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_1b
    monitor-exit p0

    #@1c
    return-void

    #@1d
    .line 261
    .restart local v2       #clientList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_1d
    :try_start_1d
    new-instance v3, Ljava/io/FileOutputStream;

    #@1f
    const/4 v7, 0x0

    #@20
    invoke-direct {v3, v6, v7}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    #@23
    .line 262
    .local v3, fos:Ljava/io/FileOutputStream;
    new-instance v0, Ljava/io/BufferedWriter;

    #@25
    new-instance v7, Ljava/io/OutputStreamWriter;

    #@27
    invoke-direct {v7, v3}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    #@2a
    const/16 v8, 0x100

    #@2c
    invoke-direct {v0, v7, v8}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;I)V
    :try_end_2f
    .catchall {:try_start_1d .. :try_end_2f} :catchall_60
    .catch Ljava/io/IOException; {:try_start_1d .. :try_end_2f} :catch_57

    #@2f
    .line 265
    .local v0, bWriter:Ljava/io/BufferedWriter;
    :try_start_2f
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@32
    move-result-object v4

    #@33
    .local v4, i$:Ljava/util/Iterator;
    :goto_33
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    #@36
    move-result v7

    #@37
    if-eqz v7, :cond_63

    #@39
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@3c
    move-result-object v1

    #@3d
    check-cast v1, Ljava/lang/String;

    #@3f
    .line 266
    .local v1, client:Ljava/lang/String;
    const/4 v7, 0x0

    #@40
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@43
    move-result v8

    #@44
    invoke-virtual {v0, v1, v7, v8}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;II)V

    #@47
    .line 267
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->newLine()V
    :try_end_4a
    .catchall {:try_start_2f .. :try_end_4a} :catchall_71
    .catch Ljava/io/IOException; {:try_start_2f .. :try_end_4a} :catch_4b

    #@4a
    goto :goto_33

    #@4b
    .line 272
    .end local v1           #client:Ljava/lang/String;
    .end local v4           #i$:Ljava/util/Iterator;
    :catch_4b
    move-exception v5

    #@4c
    .line 273
    .local v5, ioe:Ljava/io/IOException;
    :try_start_4c
    const-string v7, "Vpn"

    #@4e
    const-string v8, "Not able to re-write into MVP File"

    #@50
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_53
    .catchall {:try_start_4c .. :try_end_53} :catchall_71

    #@53
    .line 275
    :try_start_53
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V
    :try_end_56
    .catchall {:try_start_53 .. :try_end_56} :catchall_60
    .catch Ljava/io/IOException; {:try_start_53 .. :try_end_56} :catch_57

    #@56
    goto :goto_1b

    #@57
    .line 281
    .end local v0           #bWriter:Ljava/io/BufferedWriter;
    .end local v2           #clientList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v3           #fos:Ljava/io/FileOutputStream;
    .end local v5           #ioe:Ljava/io/IOException;
    :catch_57
    move-exception v5

    #@58
    .line 282
    .restart local v5       #ioe:Ljava/io/IOException;
    :try_start_58
    const-string v7, "Vpn"

    #@5a
    const-string v8, "Not able to re-create or write into MVP File"

    #@5c
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5f
    .catchall {:try_start_58 .. :try_end_5f} :catchall_60

    #@5f
    goto :goto_1b

    #@60
    .line 252
    .end local v5           #ioe:Ljava/io/IOException;
    .end local v6           #mvpFile:Ljava/io/File;
    :catchall_60
    move-exception v7

    #@61
    monitor-exit p0

    #@62
    throw v7

    #@63
    .line 269
    .restart local v0       #bWriter:Ljava/io/BufferedWriter;
    .restart local v2       #clientList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v3       #fos:Ljava/io/FileOutputStream;
    .restart local v4       #i$:Ljava/util/Iterator;
    .restart local v6       #mvpFile:Ljava/io/File;
    :cond_63
    :try_start_63
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->flush()V

    #@66
    .line 271
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    #@69
    move-result-object v7

    #@6a
    invoke-virtual {v7}, Ljava/io/FileDescriptor;->sync()V
    :try_end_6d
    .catchall {:try_start_63 .. :try_end_6d} :catchall_71
    .catch Ljava/io/IOException; {:try_start_63 .. :try_end_6d} :catch_4b

    #@6d
    .line 275
    :try_start_6d
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V

    #@70
    goto :goto_1b

    #@71
    .end local v4           #i$:Ljava/util/Iterator;
    :catchall_71
    move-exception v7

    #@72
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V

    #@75
    throw v7

    #@76
    .line 279
    .end local v0           #bWriter:Ljava/io/BufferedWriter;
    .end local v3           #fos:Ljava/io/FileOutputStream;
    :cond_76
    const-string v7, "Vpn"

    #@78
    new-instance v8, Ljava/lang/StringBuilder;

    #@7a
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@7d
    const-string v9, "Called to remove client "

    #@7f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v8

    #@83
    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v8

    #@87
    const-string v9, " that is no more in the list"

    #@89
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v8

    #@8d
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@90
    move-result-object v8

    #@91
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_94
    .catchall {:try_start_6d .. :try_end_94} :catchall_60
    .catch Ljava/io/IOException; {:try_start_6d .. :try_end_94} :catch_57

    #@94
    goto :goto_1b
.end method

.method private showNotification(Lcom/android/internal/net/VpnConfig;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .registers 15
    .parameter "config"
    .parameter "label"
    .parameter "icon"

    #@0
    .prologue
    const v10, 0x1080616

    #@3
    const/4 v9, 0x1

    #@4
    const/4 v8, 0x0

    #@5
    .line 853
    iget-boolean v4, p0, Lcom/android/server/connectivity/Vpn;->mEnableNotif:Z

    #@7
    if-nez v4, :cond_a

    #@9
    .line 877
    :cond_9
    :goto_9
    return-void

    #@a
    .line 854
    :cond_a
    iget-object v4, p0, Lcom/android/server/connectivity/Vpn;->mContext:Landroid/content/Context;

    #@c
    invoke-static {v4, p1}, Lcom/android/internal/net/VpnConfig;->getIntentForStatusPanel(Landroid/content/Context;Lcom/android/internal/net/VpnConfig;)Landroid/app/PendingIntent;

    #@f
    move-result-object v4

    #@10
    iput-object v4, p0, Lcom/android/server/connectivity/Vpn;->mStatusIntent:Landroid/app/PendingIntent;

    #@12
    .line 856
    iget-object v4, p0, Lcom/android/server/connectivity/Vpn;->mContext:Landroid/content/Context;

    #@14
    const-string v5, "notification"

    #@16
    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@19
    move-result-object v0

    #@1a
    check-cast v0, Landroid/app/NotificationManager;

    #@1c
    .line 859
    .local v0, nm:Landroid/app/NotificationManager;
    if-eqz v0, :cond_9

    #@1e
    .line 860
    if-nez p2, :cond_6c

    #@20
    iget-object v4, p0, Lcom/android/server/connectivity/Vpn;->mContext:Landroid/content/Context;

    #@22
    const v5, 0x10404af

    #@25
    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@28
    move-result-object v3

    #@29
    .line 862
    .local v3, title:Ljava/lang/String;
    :goto_29
    iget-object v4, p1, Lcom/android/internal/net/VpnConfig;->session:Ljava/lang/String;

    #@2b
    if-nez v4, :cond_7a

    #@2d
    iget-object v4, p0, Lcom/android/server/connectivity/Vpn;->mContext:Landroid/content/Context;

    #@2f
    const v5, 0x10404b1

    #@32
    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@35
    move-result-object v2

    #@36
    .line 864
    .local v2, text:Ljava/lang/String;
    :goto_36
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@39
    move-result-wide v4

    #@3a
    iput-wide v4, p1, Lcom/android/internal/net/VpnConfig;->startTime:J

    #@3c
    .line 866
    new-instance v4, Landroid/app/Notification$Builder;

    #@3e
    iget-object v5, p0, Lcom/android/server/connectivity/Vpn;->mContext:Landroid/content/Context;

    #@40
    invoke-direct {v4, v5}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    #@43
    invoke-virtual {v4, v10}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    #@46
    move-result-object v4

    #@47
    invoke-virtual {v4, p3}, Landroid/app/Notification$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;

    #@4a
    move-result-object v4

    #@4b
    invoke-virtual {v4, v3}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    #@4e
    move-result-object v4

    #@4f
    invoke-virtual {v4, v2}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    #@52
    move-result-object v4

    #@53
    iget-object v5, p0, Lcom/android/server/connectivity/Vpn;->mStatusIntent:Landroid/app/PendingIntent;

    #@55
    invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    #@58
    move-result-object v4

    #@59
    invoke-virtual {v4, v8}, Landroid/app/Notification$Builder;->setDefaults(I)Landroid/app/Notification$Builder;

    #@5c
    move-result-object v4

    #@5d
    invoke-virtual {v4, v9}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    #@60
    move-result-object v4

    #@61
    invoke-virtual {v4}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    #@64
    move-result-object v1

    #@65
    .line 875
    .local v1, notification:Landroid/app/Notification;
    const/4 v4, 0x0

    #@66
    sget-object v5, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@68
    invoke-virtual {v0, v4, v10, v1, v5}, Landroid/app/NotificationManager;->notifyAsUser(Ljava/lang/String;ILandroid/app/Notification;Landroid/os/UserHandle;)V

    #@6b
    goto :goto_9

    #@6c
    .line 860
    .end local v1           #notification:Landroid/app/Notification;
    .end local v2           #text:Ljava/lang/String;
    .end local v3           #title:Ljava/lang/String;
    :cond_6c
    iget-object v4, p0, Lcom/android/server/connectivity/Vpn;->mContext:Landroid/content/Context;

    #@6e
    const v5, 0x10404b0

    #@71
    new-array v6, v9, [Ljava/lang/Object;

    #@73
    aput-object p2, v6, v8

    #@75
    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@78
    move-result-object v3

    #@79
    goto :goto_29

    #@7a
    .line 862
    .restart local v3       #title:Ljava/lang/String;
    :cond_7a
    iget-object v4, p0, Lcom/android/server/connectivity/Vpn;->mContext:Landroid/content/Context;

    #@7c
    const v5, 0x10404b2

    #@7f
    new-array v6, v9, [Ljava/lang/Object;

    #@81
    iget-object v7, p1, Lcom/android/internal/net/VpnConfig;->session:Ljava/lang/String;

    #@83
    aput-object v7, v6, v8

    #@85
    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@88
    move-result-object v2

    #@89
    goto :goto_36
.end method

.method private declared-synchronized startLegacyVpn(Lcom/android/internal/net/VpnConfig;[Ljava/lang/String;[Ljava/lang/String;)V
    .registers 6
    .parameter "config"
    .parameter "racoon"
    .parameter "mtpd"

    #@0
    .prologue
    .line 1019
    monitor-enter p0

    #@1
    :try_start_1
    invoke-virtual {p0}, Lcom/android/server/connectivity/Vpn;->stopLegacyVpn()V

    #@4
    .line 1022
    const/4 v0, 0x0

    #@5
    const-string v1, "[Legacy VPN]"

    #@7
    invoke-virtual {p0, v0, v1}, Lcom/android/server/connectivity/Vpn;->prepare(Ljava/lang/String;Ljava/lang/String;)Z

    #@a
    .line 1023
    sget-object v0, Landroid/net/NetworkInfo$DetailedState;->CONNECTING:Landroid/net/NetworkInfo$DetailedState;

    #@c
    const-string v1, "startLegacyVpn"

    #@e
    invoke-direct {p0, v0, v1}, Lcom/android/server/connectivity/Vpn;->updateState(Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;)V

    #@11
    .line 1026
    new-instance v0, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;

    #@13
    invoke-direct {v0, p0, p1, p2, p3}, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;-><init>(Lcom/android/server/connectivity/Vpn;Lcom/android/internal/net/VpnConfig;[Ljava/lang/String;[Ljava/lang/String;)V

    #@16
    iput-object v0, p0, Lcom/android/server/connectivity/Vpn;->mLegacyVpnRunner:Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;

    #@18
    .line 1027
    iget-object v0, p0, Lcom/android/server/connectivity/Vpn;->mLegacyVpnRunner:Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;

    #@1a
    invoke-virtual {v0}, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->start()V
    :try_end_1d
    .catchall {:try_start_1 .. :try_end_1d} :catchall_1f

    #@1d
    .line 1028
    monitor-exit p0

    #@1e
    return-void

    #@1f
    .line 1019
    :catchall_1f
    move-exception v0

    #@20
    monitor-exit p0

    #@21
    throw v0
.end method

.method private updateState(Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;)V
    .registers 6
    .parameter "detailedState"
    .parameter "reason"

    #@0
    .prologue
    .line 162
    const-string v0, "Vpn"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "setting state="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, ", reason="

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 163
    iget-object v0, p0, Lcom/android/server/connectivity/Vpn;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@24
    const/4 v1, 0x0

    #@25
    invoke-virtual {v0, p1, p2, v1}, Landroid/net/NetworkInfo;->setDetailedState(Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;Ljava/lang/String;)V

    #@28
    .line 164
    iget-object v0, p0, Lcom/android/server/connectivity/Vpn;->mCallback:Lcom/android/server/ConnectivityService$VpnCallback;

    #@2a
    new-instance v1, Landroid/net/NetworkInfo;

    #@2c
    iget-object v2, p0, Lcom/android/server/connectivity/Vpn;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@2e
    invoke-direct {v1, v2}, Landroid/net/NetworkInfo;-><init>(Landroid/net/NetworkInfo;)V

    #@31
    invoke-virtual {v0, v1}, Lcom/android/server/ConnectivityService$VpnCallback;->onStateChanged(Landroid/net/NetworkInfo;)V

    #@34
    .line 165
    return-void
.end method


# virtual methods
.method public declared-synchronized establish(Lcom/android/internal/net/VpnConfig;)Landroid/os/ParcelFileDescriptor;
    .registers 26
    .parameter "config"

    #@0
    .prologue
    .line 579
    monitor-enter p0

    #@1
    :try_start_1
    move-object/from16 v0, p0

    #@3
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn;->mContext:Landroid/content/Context;

    #@5
    move-object/from16 v21, v0

    #@7
    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_77

    #@a
    move-result-object v15

    #@b
    .line 580
    .local v15, pm:Landroid/content/pm/PackageManager;
    const/4 v3, 0x0

    #@c
    .line 582
    .local v3, app:Landroid/content/pm/ApplicationInfo;
    :try_start_c
    move-object/from16 v0, p0

    #@e
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn;->mPackage:Ljava/lang/String;

    #@10
    move-object/from16 v21, v0

    #@12
    const/16 v22, 0x0

    #@14
    move-object/from16 v0, v21

    #@16
    move/from16 v1, v22

    #@18
    invoke-virtual {v15, v0, v1}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_1b
    .catchall {:try_start_c .. :try_end_1b} :catchall_77
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_1b} :catch_2e

    #@1b
    move-result-object v3

    #@1c
    .line 586
    :try_start_1c
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@1f
    move-result v21

    #@20
    iget v0, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    #@22
    move/from16 v22, v0
    :try_end_24
    .catchall {:try_start_1c .. :try_end_24} :catchall_77

    #@24
    move/from16 v0, v21

    #@26
    move/from16 v1, v22

    #@28
    if-eq v0, v1, :cond_32

    #@2a
    .line 587
    const/16 v19, 0x0

    #@2c
    .line 730
    :goto_2c
    monitor-exit p0

    #@2d
    return-object v19

    #@2e
    .line 583
    :catch_2e
    move-exception v7

    #@2f
    .line 584
    .local v7, e:Ljava/lang/Exception;
    const/16 v19, 0x0

    #@31
    goto :goto_2c

    #@32
    .line 591
    .end local v7           #e:Ljava/lang/Exception;
    :cond_32
    :try_start_32
    new-instance v11, Landroid/content/Intent;

    #@34
    const-string v21, "android.net.VpnService"

    #@36
    move-object/from16 v0, v21

    #@38
    invoke-direct {v11, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@3b
    .line 592
    .local v11, intent:Landroid/content/Intent;
    move-object/from16 v0, p0

    #@3d
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn;->mPackage:Ljava/lang/String;

    #@3f
    move-object/from16 v21, v0

    #@41
    move-object/from16 v0, p1

    #@43
    iget-object v0, v0, Lcom/android/internal/net/VpnConfig;->user:Ljava/lang/String;

    #@45
    move-object/from16 v22, v0

    #@47
    move-object/from16 v0, v21

    #@49
    move-object/from16 v1, v22

    #@4b
    invoke-virtual {v11, v0, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@4e
    .line 593
    const/16 v21, 0x0

    #@50
    move/from16 v0, v21

    #@52
    invoke-virtual {v15, v11, v0}, Landroid/content/pm/PackageManager;->resolveService(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    #@55
    move-result-object v10

    #@56
    .line 594
    .local v10, info:Landroid/content/pm/ResolveInfo;
    if-nez v10, :cond_7a

    #@58
    .line 595
    new-instance v21, Ljava/lang/SecurityException;

    #@5a
    new-instance v22, Ljava/lang/StringBuilder;

    #@5c
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@5f
    const-string v23, "Cannot find "

    #@61
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v22

    #@65
    move-object/from16 v0, p1

    #@67
    iget-object v0, v0, Lcom/android/internal/net/VpnConfig;->user:Ljava/lang/String;

    #@69
    move-object/from16 v23, v0

    #@6b
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v22

    #@6f
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@72
    move-result-object v22

    #@73
    invoke-direct/range {v21 .. v22}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@76
    throw v21
    :try_end_77
    .catchall {:try_start_32 .. :try_end_77} :catchall_77

    #@77
    .line 579
    .end local v3           #app:Landroid/content/pm/ApplicationInfo;
    .end local v10           #info:Landroid/content/pm/ResolveInfo;
    .end local v11           #intent:Landroid/content/Intent;
    .end local v15           #pm:Landroid/content/pm/PackageManager;
    :catchall_77
    move-exception v21

    #@78
    monitor-exit p0

    #@79
    throw v21

    #@7a
    .line 597
    .restart local v3       #app:Landroid/content/pm/ApplicationInfo;
    .restart local v10       #info:Landroid/content/pm/ResolveInfo;
    .restart local v11       #intent:Landroid/content/Intent;
    .restart local v15       #pm:Landroid/content/pm/PackageManager;
    :cond_7a
    :try_start_7a
    const-string v21, "android.permission.BIND_VPN_SERVICE"

    #@7c
    iget-object v0, v10, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@7e
    move-object/from16 v22, v0

    #@80
    move-object/from16 v0, v22

    #@82
    iget-object v0, v0, Landroid/content/pm/ServiceInfo;->permission:Ljava/lang/String;

    #@84
    move-object/from16 v22, v0

    #@86
    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@89
    move-result v21

    #@8a
    if-nez v21, :cond_b1

    #@8c
    .line 598
    new-instance v21, Ljava/lang/SecurityException;

    #@8e
    new-instance v22, Ljava/lang/StringBuilder;

    #@90
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@93
    move-object/from16 v0, p1

    #@95
    iget-object v0, v0, Lcom/android/internal/net/VpnConfig;->user:Ljava/lang/String;

    #@97
    move-object/from16 v23, v0

    #@99
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v22

    #@9d
    const-string v23, " does not require "

    #@9f
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v22

    #@a3
    const-string v23, "android.permission.BIND_VPN_SERVICE"

    #@a5
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v22

    #@a9
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ac
    move-result-object v22

    #@ad
    invoke-direct/range {v21 .. v22}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@b0
    throw v21

    #@b1
    .line 602
    :cond_b1
    invoke-virtual {v3, v15}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    #@b4
    move-result-object v21

    #@b5
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@b8
    move-result-object v14

    #@b9
    .line 605
    .local v14, label:Ljava/lang/String;
    invoke-virtual {v3, v15}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    #@bc
    move-result-object v9

    #@bd
    .line 606
    .local v9, icon:Landroid/graphics/drawable/Drawable;
    const/4 v4, 0x0

    #@be
    .line 607
    .local v4, bitmap:Landroid/graphics/Bitmap;
    invoke-virtual {v9}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@c1
    move-result v21

    #@c2
    if-lez v21, :cond_146

    #@c4
    invoke-virtual {v9}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@c7
    move-result v21

    #@c8
    if-lez v21, :cond_146

    #@ca
    .line 608
    move-object/from16 v0, p0

    #@cc
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn;->mContext:Landroid/content/Context;

    #@ce
    move-object/from16 v21, v0

    #@d0
    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@d3
    move-result-object v21

    #@d4
    const v22, 0x1050005

    #@d7
    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@da
    move-result v20

    #@db
    .line 610
    .local v20, width:I
    move-object/from16 v0, p0

    #@dd
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn;->mContext:Landroid/content/Context;

    #@df
    move-object/from16 v21, v0

    #@e1
    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@e4
    move-result-object v21

    #@e5
    const v22, 0x1050006

    #@e8
    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@eb
    move-result v8

    #@ec
    .line 612
    .local v8, height:I
    const/16 v21, 0x0

    #@ee
    const/16 v22, 0x0

    #@f0
    move/from16 v0, v21

    #@f2
    move/from16 v1, v22

    #@f4
    move/from16 v2, v20

    #@f6
    invoke-virtual {v9, v0, v1, v2, v8}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@f9
    .line 613
    sget-object v21, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    #@fb
    move/from16 v0, v20

    #@fd
    move-object/from16 v1, v21

    #@ff
    invoke-static {v0, v8, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    #@102
    move-result-object v4

    #@103
    .line 614
    new-instance v5, Landroid/graphics/Canvas;

    #@105
    invoke-direct {v5, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    #@108
    .line 615
    .local v5, c:Landroid/graphics/Canvas;
    invoke-virtual {v9, v5}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@10b
    .line 616
    const/16 v21, 0x0

    #@10d
    move-object/from16 v0, v21

    #@10f
    invoke-virtual {v5, v0}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    #@112
    .line 619
    const-string v21, "ro.lge.b2b.vmware"

    #@114
    const/16 v22, 0x0

    #@116
    invoke-static/range {v21 .. v22}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@119
    move-result v21

    #@11a
    if-eqz v21, :cond_146

    #@11c
    move-object/from16 v0, p0

    #@11e
    iget-boolean v0, v0, Lcom/android/server/connectivity/Vpn;->mMvpOn:Z

    #@120
    move/from16 v21, v0
    :try_end_122
    .catchall {:try_start_7a .. :try_end_122} :catchall_77

    #@122
    if-eqz v21, :cond_146

    #@124
    .line 622
    :try_start_124
    const-string v21, "com.vmware.mvp.enabled"

    #@126
    move-object/from16 v0, v21

    #@128
    invoke-virtual {v15, v0}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    #@12b
    move-result-object v9

    #@12c
    .line 623
    div-int/lit8 v21, v20, 0x2

    #@12e
    div-int/lit8 v22, v8, 0x2

    #@130
    move/from16 v0, v21

    #@132
    move/from16 v1, v22

    #@134
    move/from16 v2, v20

    #@136
    invoke-virtual {v9, v0, v1, v2, v8}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@139
    .line 624
    invoke-virtual {v5, v4}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    #@13c
    .line 625
    invoke-virtual {v9, v5}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@13f
    .line 626
    const/16 v21, 0x0

    #@141
    move-object/from16 v0, v21

    #@143
    invoke-virtual {v5, v0}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V
    :try_end_146
    .catchall {:try_start_124 .. :try_end_146} :catchall_77
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_124 .. :try_end_146} :catch_20e

    #@146
    .line 635
    .end local v5           #c:Landroid/graphics/Canvas;
    .end local v8           #height:I
    .end local v20           #width:I
    :cond_146
    :goto_146
    :try_start_146
    move-object/from16 v0, p1

    #@148
    iget v0, v0, Lcom/android/internal/net/VpnConfig;->mtu:I

    #@14a
    move/from16 v21, v0

    #@14c
    move-object/from16 v0, p0

    #@14e
    move/from16 v1, v21

    #@150
    invoke-direct {v0, v1}, Lcom/android/server/connectivity/Vpn;->jniCreate(I)I

    #@153
    move-result v21

    #@154
    invoke-static/range {v21 .. v21}, Landroid/os/ParcelFileDescriptor;->adoptFd(I)Landroid/os/ParcelFileDescriptor;
    :try_end_157
    .catchall {:try_start_146 .. :try_end_157} :catchall_77

    #@157
    move-result-object v19

    #@158
    .line 637
    .local v19, tun:Landroid/os/ParcelFileDescriptor;
    :try_start_158
    sget-object v21, Landroid/net/NetworkInfo$DetailedState;->CONNECTING:Landroid/net/NetworkInfo$DetailedState;

    #@15a
    const-string v22, "establish"

    #@15c
    move-object/from16 v0, p0

    #@15e
    move-object/from16 v1, v21

    #@160
    move-object/from16 v2, v22

    #@162
    invoke-direct {v0, v1, v2}, Lcom/android/server/connectivity/Vpn;->updateState(Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;)V

    #@165
    .line 638
    invoke-virtual/range {v19 .. v19}, Landroid/os/ParcelFileDescriptor;->getFd()I

    #@168
    move-result v21

    #@169
    move-object/from16 v0, p0

    #@16b
    move/from16 v1, v21

    #@16d
    invoke-direct {v0, v1}, Lcom/android/server/connectivity/Vpn;->jniGetName(I)Ljava/lang/String;

    #@170
    move-result-object v12

    #@171
    .line 641
    .local v12, interfaze:Ljava/lang/String;
    const-string v21, "ro.lge.b2b.vmware"

    #@173
    const/16 v22, 0x0

    #@175
    invoke-static/range {v21 .. v22}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@178
    move-result v21

    #@179
    if-eqz v21, :cond_276

    #@17b
    .line 642
    move-object/from16 v0, p0

    #@17d
    iget-boolean v0, v0, Lcom/android/server/connectivity/Vpn;->mMvpOn:Z

    #@17f
    move/from16 v21, v0

    #@181
    if-eqz v21, :cond_241

    #@183
    .line 643
    move-object/from16 v0, p0

    #@185
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn;->mNamespace:Lcom/android/server/connectivity/VpnNamespace;

    #@187
    move-object/from16 v21, v0

    #@189
    if-nez v21, :cond_196

    #@18b
    .line 644
    new-instance v21, Lcom/android/server/connectivity/VpnNamespace;

    #@18d
    invoke-direct/range {v21 .. v21}, Lcom/android/server/connectivity/VpnNamespace;-><init>()V

    #@190
    move-object/from16 v0, v21

    #@192
    move-object/from16 v1, p0

    #@194
    iput-object v0, v1, Lcom/android/server/connectivity/Vpn;->mNamespace:Lcom/android/server/connectivity/VpnNamespace;

    #@196
    .line 647
    :cond_196
    move-object/from16 v0, p0

    #@198
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn;->mNamespace:Lcom/android/server/connectivity/VpnNamespace;

    #@19a
    move-object/from16 v21, v0

    #@19c
    move-object/from16 v0, v21

    #@19e
    move-object/from16 v1, p1

    #@1a0
    move-object/from16 v2, p0

    #@1a2
    invoke-virtual {v0, v12, v1, v2}, Lcom/android/server/connectivity/VpnNamespace;->establish(Ljava/lang/String;Lcom/android/internal/net/VpnConfig;Lcom/android/server/connectivity/Vpn;)Landroid/content/Intent;

    #@1a5
    move-result-object v16

    #@1a6
    .line 648
    .local v16, rest:Landroid/content/Intent;
    if-eqz v16, :cond_1c0

    #@1a8
    .line 652
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_1ab
    .catchall {:try_start_158 .. :try_end_1ab} :catchall_77
    .catch Ljava/lang/RuntimeException; {:try_start_158 .. :try_end_1ab} :catch_1fc
    .catch Ljava/io/IOException; {:try_start_158 .. :try_end_1ab} :catch_21d

    #@1ab
    move-result-wide v17

    #@1ac
    .line 654
    .local v17, token:J
    :try_start_1ac
    move-object/from16 v0, p0

    #@1ae
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn;->mContext:Landroid/content/Context;

    #@1b0
    move-object/from16 v21, v0

    #@1b2
    const-string v22, "com.vmware.mvp.permission.VPN_CONFIG"

    #@1b4
    move-object/from16 v0, v21

    #@1b6
    move-object/from16 v1, v16

    #@1b8
    move-object/from16 v2, v22

    #@1ba
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V
    :try_end_1bd
    .catchall {:try_start_1ac .. :try_end_1bd} :catchall_218

    #@1bd
    .line 656
    :try_start_1bd
    invoke-static/range {v17 .. v18}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@1c0
    .line 678
    .end local v16           #rest:Landroid/content/Intent;
    .end local v17           #token:J
    :cond_1c0
    :goto_1c0
    new-instance v6, Lcom/android/server/connectivity/Vpn$Connection;

    #@1c2
    const/16 v21, 0x0

    #@1c4
    move-object/from16 v0, p0

    #@1c6
    move-object/from16 v1, v21

    #@1c8
    invoke-direct {v6, v0, v1}, Lcom/android/server/connectivity/Vpn$Connection;-><init>(Lcom/android/server/connectivity/Vpn;Lcom/android/server/connectivity/Vpn$1;)V

    #@1cb
    .line 679
    .local v6, connection:Lcom/android/server/connectivity/Vpn$Connection;
    move-object/from16 v0, p0

    #@1cd
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn;->mContext:Landroid/content/Context;

    #@1cf
    move-object/from16 v21, v0

    #@1d1
    const/16 v22, 0x1

    #@1d3
    move-object/from16 v0, v21

    #@1d5
    move/from16 v1, v22

    #@1d7
    invoke-virtual {v0, v11, v6, v1}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    #@1da
    move-result v21

    #@1db
    if-nez v21, :cond_2ab

    #@1dd
    .line 680
    new-instance v21, Ljava/lang/IllegalStateException;

    #@1df
    new-instance v22, Ljava/lang/StringBuilder;

    #@1e1
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@1e4
    const-string v23, "Cannot bind "

    #@1e6
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e9
    move-result-object v22

    #@1ea
    move-object/from16 v0, p1

    #@1ec
    iget-object v0, v0, Lcom/android/internal/net/VpnConfig;->user:Ljava/lang/String;

    #@1ee
    move-object/from16 v23, v0

    #@1f0
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f3
    move-result-object v22

    #@1f4
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f7
    move-result-object v22

    #@1f8
    invoke-direct/range {v21 .. v22}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@1fb
    throw v21
    :try_end_1fc
    .catchall {:try_start_1bd .. :try_end_1fc} :catchall_77
    .catch Ljava/lang/RuntimeException; {:try_start_1bd .. :try_end_1fc} :catch_1fc
    .catch Ljava/io/IOException; {:try_start_1bd .. :try_end_1fc} :catch_21d

    #@1fc
    .line 690
    .end local v6           #connection:Lcom/android/server/connectivity/Vpn$Connection;
    .end local v12           #interfaze:Ljava/lang/String;
    :catch_1fc
    move-exception v7

    #@1fd
    .line 691
    .local v7, e:Ljava/lang/RuntimeException;
    :try_start_1fd
    sget-object v21, Landroid/net/NetworkInfo$DetailedState;->FAILED:Landroid/net/NetworkInfo$DetailedState;

    #@1ff
    const-string v22, "establish"

    #@201
    move-object/from16 v0, p0

    #@203
    move-object/from16 v1, v21

    #@205
    move-object/from16 v2, v22

    #@207
    invoke-direct {v0, v1, v2}, Lcom/android/server/connectivity/Vpn;->updateState(Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;)V

    #@20a
    .line 692
    invoke-static/range {v19 .. v19}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@20d
    .line 693
    throw v7

    #@20e
    .line 627
    .end local v7           #e:Ljava/lang/RuntimeException;
    .end local v19           #tun:Landroid/os/ParcelFileDescriptor;
    .restart local v5       #c:Landroid/graphics/Canvas;
    .restart local v8       #height:I
    .restart local v20       #width:I
    :catch_20e
    move-exception v7

    #@20f
    .line 628
    .local v7, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v21, "Vpn"

    #@211
    const-string v22, "Not able to find com.vmware.mvp.enabled"

    #@213
    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_216
    .catchall {:try_start_1fd .. :try_end_216} :catchall_77

    #@216
    goto/16 :goto_146

    #@218
    .line 656
    .end local v5           #c:Landroid/graphics/Canvas;
    .end local v7           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v8           #height:I
    .end local v20           #width:I
    .restart local v12       #interfaze:Ljava/lang/String;
    .restart local v16       #rest:Landroid/content/Intent;
    .restart local v17       #token:J
    .restart local v19       #tun:Landroid/os/ParcelFileDescriptor;
    :catchall_218
    move-exception v21

    #@219
    :try_start_219
    invoke-static/range {v17 .. v18}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@21c
    throw v21
    :try_end_21d
    .catchall {:try_start_219 .. :try_end_21d} :catchall_77
    .catch Ljava/lang/RuntimeException; {:try_start_219 .. :try_end_21d} :catch_1fc
    .catch Ljava/io/IOException; {:try_start_219 .. :try_end_21d} :catch_21d

    #@21d
    .line 695
    .end local v12           #interfaze:Ljava/lang/String;
    .end local v16           #rest:Landroid/content/Intent;
    .end local v17           #token:J
    :catch_21d
    move-exception v13

    #@21e
    .line 696
    .local v13, ioe:Ljava/io/IOException;
    :try_start_21e
    sget-object v21, Landroid/net/NetworkInfo$DetailedState;->FAILED:Landroid/net/NetworkInfo$DetailedState;

    #@220
    const-string v22, "establish"

    #@222
    move-object/from16 v0, p0

    #@224
    move-object/from16 v1, v21

    #@226
    move-object/from16 v2, v22

    #@228
    invoke-direct {v0, v1, v2}, Lcom/android/server/connectivity/Vpn;->updateState(Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;)V

    #@22b
    .line 697
    const-string v21, "Vpn"

    #@22d
    const-string v22, "MVP VPN is unable to start"

    #@22f
    move-object/from16 v0, v21

    #@231
    move-object/from16 v1, v22

    #@233
    invoke-static {v0, v1, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@236
    .line 698
    invoke-static/range {v19 .. v19}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@239
    .line 700
    new-instance v21, Ljava/lang/RuntimeException;

    #@23b
    move-object/from16 v0, v21

    #@23d
    invoke-direct {v0, v13}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@240
    throw v21
    :try_end_241
    .catchall {:try_start_21e .. :try_end_241} :catchall_77

    #@241
    .line 660
    .end local v13           #ioe:Ljava/io/IOException;
    .restart local v12       #interfaze:Ljava/lang/String;
    :cond_241
    :try_start_241
    move-object/from16 v0, p1

    #@243
    iget-object v0, v0, Lcom/android/internal/net/VpnConfig;->addresses:Ljava/lang/String;

    #@245
    move-object/from16 v21, v0

    #@247
    move-object/from16 v0, p0

    #@249
    move-object/from16 v1, v21

    #@24b
    invoke-direct {v0, v12, v1}, Lcom/android/server/connectivity/Vpn;->jniSetAddresses(Ljava/lang/String;Ljava/lang/String;)I

    #@24e
    move-result v21

    #@24f
    const/16 v22, 0x1

    #@251
    move/from16 v0, v21

    #@253
    move/from16 v1, v22

    #@255
    if-ge v0, v1, :cond_25f

    #@257
    .line 661
    new-instance v21, Ljava/lang/IllegalArgumentException;

    #@259
    const-string v22, "At least one address must be specified"

    #@25b
    invoke-direct/range {v21 .. v22}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@25e
    throw v21

    #@25f
    .line 663
    :cond_25f
    move-object/from16 v0, p1

    #@261
    iget-object v0, v0, Lcom/android/internal/net/VpnConfig;->routes:Ljava/lang/String;

    #@263
    move-object/from16 v21, v0

    #@265
    if-eqz v21, :cond_1c0

    #@267
    .line 664
    move-object/from16 v0, p1

    #@269
    iget-object v0, v0, Lcom/android/internal/net/VpnConfig;->routes:Ljava/lang/String;

    #@26b
    move-object/from16 v21, v0

    #@26d
    move-object/from16 v0, p0

    #@26f
    move-object/from16 v1, v21

    #@271
    invoke-direct {v0, v12, v1}, Lcom/android/server/connectivity/Vpn;->jniSetRoutes(Ljava/lang/String;Ljava/lang/String;)I

    #@274
    goto/16 :goto_1c0

    #@276
    .line 670
    :cond_276
    move-object/from16 v0, p1

    #@278
    iget-object v0, v0, Lcom/android/internal/net/VpnConfig;->addresses:Ljava/lang/String;

    #@27a
    move-object/from16 v21, v0

    #@27c
    move-object/from16 v0, p0

    #@27e
    move-object/from16 v1, v21

    #@280
    invoke-direct {v0, v12, v1}, Lcom/android/server/connectivity/Vpn;->jniSetAddresses(Ljava/lang/String;Ljava/lang/String;)I

    #@283
    move-result v21

    #@284
    const/16 v22, 0x1

    #@286
    move/from16 v0, v21

    #@288
    move/from16 v1, v22

    #@28a
    if-ge v0, v1, :cond_294

    #@28c
    .line 671
    new-instance v21, Ljava/lang/IllegalArgumentException;

    #@28e
    const-string v22, "At least one address must be specified"

    #@290
    invoke-direct/range {v21 .. v22}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@293
    throw v21

    #@294
    .line 673
    :cond_294
    move-object/from16 v0, p1

    #@296
    iget-object v0, v0, Lcom/android/internal/net/VpnConfig;->routes:Ljava/lang/String;

    #@298
    move-object/from16 v21, v0

    #@29a
    if-eqz v21, :cond_1c0

    #@29c
    .line 674
    move-object/from16 v0, p1

    #@29e
    iget-object v0, v0, Lcom/android/internal/net/VpnConfig;->routes:Ljava/lang/String;

    #@2a0
    move-object/from16 v21, v0

    #@2a2
    move-object/from16 v0, p0

    #@2a4
    move-object/from16 v1, v21

    #@2a6
    invoke-direct {v0, v12, v1}, Lcom/android/server/connectivity/Vpn;->jniSetRoutes(Ljava/lang/String;Ljava/lang/String;)I

    #@2a9
    goto/16 :goto_1c0

    #@2ab
    .line 682
    .restart local v6       #connection:Lcom/android/server/connectivity/Vpn$Connection;
    :cond_2ab
    move-object/from16 v0, p0

    #@2ad
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn;->mConnection:Lcom/android/server/connectivity/Vpn$Connection;

    #@2af
    move-object/from16 v21, v0

    #@2b1
    if-eqz v21, :cond_2c2

    #@2b3
    .line 683
    move-object/from16 v0, p0

    #@2b5
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn;->mContext:Landroid/content/Context;

    #@2b7
    move-object/from16 v21, v0

    #@2b9
    move-object/from16 v0, p0

    #@2bb
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn;->mConnection:Lcom/android/server/connectivity/Vpn$Connection;

    #@2bd
    move-object/from16 v22, v0

    #@2bf
    invoke-virtual/range {v21 .. v22}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    #@2c2
    .line 685
    :cond_2c2
    move-object/from16 v0, p0

    #@2c4
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn;->mInterface:Ljava/lang/String;

    #@2c6
    move-object/from16 v21, v0

    #@2c8
    if-eqz v21, :cond_2e5

    #@2ca
    move-object/from16 v0, p0

    #@2cc
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn;->mInterface:Ljava/lang/String;

    #@2ce
    move-object/from16 v21, v0

    #@2d0
    move-object/from16 v0, v21

    #@2d2
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2d5
    move-result v21

    #@2d6
    if-nez v21, :cond_2e5

    #@2d8
    .line 686
    move-object/from16 v0, p0

    #@2da
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn;->mInterface:Ljava/lang/String;

    #@2dc
    move-object/from16 v21, v0

    #@2de
    move-object/from16 v0, p0

    #@2e0
    move-object/from16 v1, v21

    #@2e2
    invoke-direct {v0, v1}, Lcom/android/server/connectivity/Vpn;->jniReset(Ljava/lang/String;)V

    #@2e5
    .line 688
    :cond_2e5
    move-object/from16 v0, p0

    #@2e7
    iput-object v6, v0, Lcom/android/server/connectivity/Vpn;->mConnection:Lcom/android/server/connectivity/Vpn$Connection;

    #@2e9
    .line 689
    move-object/from16 v0, p0

    #@2eb
    iput-object v12, v0, Lcom/android/server/connectivity/Vpn;->mInterface:Ljava/lang/String;
    :try_end_2ed
    .catchall {:try_start_241 .. :try_end_2ed} :catchall_77
    .catch Ljava/lang/RuntimeException; {:try_start_241 .. :try_end_2ed} :catch_1fc
    .catch Ljava/io/IOException; {:try_start_241 .. :try_end_2ed} :catch_21d

    #@2ed
    .line 704
    :try_start_2ed
    const-string v21, "Vpn"

    #@2ef
    new-instance v22, Ljava/lang/StringBuilder;

    #@2f1
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@2f4
    const-string v23, "Established by "

    #@2f6
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f9
    move-result-object v22

    #@2fa
    move-object/from16 v0, p1

    #@2fc
    iget-object v0, v0, Lcom/android/internal/net/VpnConfig;->user:Ljava/lang/String;

    #@2fe
    move-object/from16 v23, v0

    #@300
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@303
    move-result-object v22

    #@304
    const-string v23, " on "

    #@306
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@309
    move-result-object v22

    #@30a
    move-object/from16 v0, p0

    #@30c
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn;->mInterface:Ljava/lang/String;

    #@30e
    move-object/from16 v23, v0

    #@310
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@313
    move-result-object v22

    #@314
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@317
    move-result-object v22

    #@318
    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@31b
    .line 707
    move-object/from16 v0, p0

    #@31d
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn;->mPackage:Ljava/lang/String;

    #@31f
    move-object/from16 v21, v0

    #@321
    move-object/from16 v0, v21

    #@323
    move-object/from16 v1, p1

    #@325
    iput-object v0, v1, Lcom/android/internal/net/VpnConfig;->user:Ljava/lang/String;

    #@327
    .line 708
    move-object/from16 v0, p0

    #@329
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn;->mInterface:Ljava/lang/String;

    #@32b
    move-object/from16 v21, v0

    #@32d
    move-object/from16 v0, v21

    #@32f
    move-object/from16 v1, p1

    #@331
    iput-object v0, v1, Lcom/android/internal/net/VpnConfig;->interfaze:Ljava/lang/String;

    #@333
    .line 711
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_336
    .catchall {:try_start_2ed .. :try_end_336} :catchall_77

    #@336
    move-result-wide v17

    #@337
    .line 714
    .restart local v17       #token:J
    :try_start_337
    const-string v21, "ro.lge.b2b.vmware"

    #@339
    const/16 v22, 0x0

    #@33b
    invoke-static/range {v21 .. v22}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@33e
    move-result v21

    #@33f
    if-eqz v21, :cond_377

    #@341
    .line 716
    move-object/from16 v0, p0

    #@343
    iget-boolean v0, v0, Lcom/android/server/connectivity/Vpn;->mMvpOn:Z

    #@345
    move/from16 v21, v0

    #@347
    if-nez v21, :cond_35e

    #@349
    .line 717
    move-object/from16 v0, p0

    #@34b
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn;->mCallback:Lcom/android/server/ConnectivityService$VpnCallback;

    #@34d
    move-object/from16 v21, v0

    #@34f
    move-object/from16 v0, p1

    #@351
    iget-object v0, v0, Lcom/android/internal/net/VpnConfig;->dnsServers:Ljava/util/List;

    #@353
    move-object/from16 v22, v0

    #@355
    move-object/from16 v0, p1

    #@357
    iget-object v0, v0, Lcom/android/internal/net/VpnConfig;->searchDomains:Ljava/util/List;

    #@359
    move-object/from16 v23, v0

    #@35b
    invoke-virtual/range {v21 .. v23}, Lcom/android/server/ConnectivityService$VpnCallback;->override(Ljava/util/List;Ljava/util/List;)V

    #@35e
    .line 724
    :cond_35e
    :goto_35e
    move-object/from16 v0, p0

    #@360
    move-object/from16 v1, p1

    #@362
    invoke-direct {v0, v1, v14, v4}, Lcom/android/server/connectivity/Vpn;->showNotification(Lcom/android/internal/net/VpnConfig;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    :try_end_365
    .catchall {:try_start_337 .. :try_end_365} :catchall_38d

    #@365
    .line 726
    :try_start_365
    invoke-static/range {v17 .. v18}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@368
    .line 729
    sget-object v21, Landroid/net/NetworkInfo$DetailedState;->AUTHENTICATING:Landroid/net/NetworkInfo$DetailedState;

    #@36a
    const-string v22, "establish"

    #@36c
    move-object/from16 v0, p0

    #@36e
    move-object/from16 v1, v21

    #@370
    move-object/from16 v2, v22

    #@372
    invoke-direct {v0, v1, v2}, Lcom/android/server/connectivity/Vpn;->updateState(Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;)V
    :try_end_375
    .catchall {:try_start_365 .. :try_end_375} :catchall_77

    #@375
    goto/16 :goto_2c

    #@377
    .line 722
    :cond_377
    :try_start_377
    move-object/from16 v0, p0

    #@379
    iget-object v0, v0, Lcom/android/server/connectivity/Vpn;->mCallback:Lcom/android/server/ConnectivityService$VpnCallback;

    #@37b
    move-object/from16 v21, v0

    #@37d
    move-object/from16 v0, p1

    #@37f
    iget-object v0, v0, Lcom/android/internal/net/VpnConfig;->dnsServers:Ljava/util/List;

    #@381
    move-object/from16 v22, v0

    #@383
    move-object/from16 v0, p1

    #@385
    iget-object v0, v0, Lcom/android/internal/net/VpnConfig;->searchDomains:Ljava/util/List;

    #@387
    move-object/from16 v23, v0

    #@389
    invoke-virtual/range {v21 .. v23}, Lcom/android/server/ConnectivityService$VpnCallback;->override(Ljava/util/List;Ljava/util/List;)V
    :try_end_38c
    .catchall {:try_start_377 .. :try_end_38c} :catchall_38d

    #@38c
    goto :goto_35e

    #@38d
    .line 726
    :catchall_38d
    move-exception v21

    #@38e
    :try_start_38e
    invoke-static/range {v17 .. v18}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@391
    throw v21
    :try_end_392
    .catchall {:try_start_38e .. :try_end_392} :catchall_77
.end method

.method public getLegacyVpnConfig()Lcom/android/internal/net/VpnConfig;
    .registers 2

    #@0
    .prologue
    .line 1060
    iget-object v0, p0, Lcom/android/server/connectivity/Vpn;->mLegacyVpnRunner:Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 1061
    iget-object v0, p0, Lcom/android/server/connectivity/Vpn;->mLegacyVpnRunner:Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;

    #@6
    invoke-static {v0}, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->access$1300(Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;)Lcom/android/internal/net/VpnConfig;

    #@9
    move-result-object v0

    #@a
    .line 1063
    :goto_a
    return-object v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public declared-synchronized getLegacyVpnInfo()Lcom/android/internal/net/LegacyVpnInfo;
    .registers 3

    #@0
    .prologue
    .line 1047
    monitor-enter p0

    #@1
    :try_start_1
    invoke-direct {p0}, Lcom/android/server/connectivity/Vpn;->enforceControlPermission()V

    #@4
    .line 1048
    iget-object v1, p0, Lcom/android/server/connectivity/Vpn;->mLegacyVpnRunner:Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_2f

    #@6
    if-nez v1, :cond_b

    #@8
    const/4 v0, 0x0

    #@9
    .line 1056
    :cond_9
    :goto_9
    monitor-exit p0

    #@a
    return-object v0

    #@b
    .line 1050
    :cond_b
    :try_start_b
    new-instance v0, Lcom/android/internal/net/LegacyVpnInfo;

    #@d
    invoke-direct {v0}, Lcom/android/internal/net/LegacyVpnInfo;-><init>()V

    #@10
    .line 1051
    .local v0, info:Lcom/android/internal/net/LegacyVpnInfo;
    iget-object v1, p0, Lcom/android/server/connectivity/Vpn;->mLegacyVpnRunner:Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;

    #@12
    invoke-static {v1}, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->access$1300(Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;)Lcom/android/internal/net/VpnConfig;

    #@15
    move-result-object v1

    #@16
    iget-object v1, v1, Lcom/android/internal/net/VpnConfig;->user:Ljava/lang/String;

    #@18
    iput-object v1, v0, Lcom/android/internal/net/LegacyVpnInfo;->key:Ljava/lang/String;

    #@1a
    .line 1052
    iget-object v1, p0, Lcom/android/server/connectivity/Vpn;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@1c
    invoke-static {v1}, Lcom/android/internal/net/LegacyVpnInfo;->stateFromNetworkInfo(Landroid/net/NetworkInfo;)I

    #@1f
    move-result v1

    #@20
    iput v1, v0, Lcom/android/internal/net/LegacyVpnInfo;->state:I

    #@22
    .line 1053
    iget-object v1, p0, Lcom/android/server/connectivity/Vpn;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@24
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    #@27
    move-result v1

    #@28
    if-eqz v1, :cond_9

    #@2a
    .line 1054
    iget-object v1, p0, Lcom/android/server/connectivity/Vpn;->mStatusIntent:Landroid/app/PendingIntent;

    #@2c
    iput-object v1, v0, Lcom/android/internal/net/LegacyVpnInfo;->intent:Landroid/app/PendingIntent;
    :try_end_2e
    .catchall {:try_start_b .. :try_end_2e} :catchall_2f

    #@2e
    goto :goto_9

    #@2f
    .line 1047
    .end local v0           #info:Lcom/android/internal/net/LegacyVpnInfo;
    :catchall_2f
    move-exception v1

    #@30
    monitor-exit p0

    #@31
    throw v1
.end method

.method public getTcpBufferSizesPropName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 155
    const-string v0, "net.tcp.buffersize.unknown"

    #@2
    return-object v0
.end method

.method public interfaceRemoved(Ljava/lang/String;)V
    .registers 3
    .parameter "interfaze"

    #@0
    .prologue
    .line 745
    :try_start_0
    iget-object v0, p0, Lcom/android/server/connectivity/Vpn;->mObserver:Landroid/net/INetworkManagementEventObserver;

    #@2
    invoke-interface {v0, p1}, Landroid/net/INetworkManagementEventObserver;->interfaceRemoved(Ljava/lang/String;)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 749
    :goto_5
    return-void

    #@6
    .line 746
    :catch_6
    move-exception v0

    #@7
    goto :goto_5
.end method

.method public declared-synchronized interfaceStatusChanged(Ljava/lang/String;Z)V
    .registers 4
    .parameter "iface"
    .parameter "up"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 736
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/server/connectivity/Vpn;->mObserver:Landroid/net/INetworkManagementEventObserver;

    #@3
    invoke-interface {v0, p1, p2}, Landroid/net/INetworkManagementEventObserver;->interfaceStatusChanged(Ljava/lang/String;Z)V
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_8
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_6} :catch_b

    #@6
    .line 740
    :goto_6
    monitor-exit p0

    #@7
    return-void

    #@8
    .line 736
    :catchall_8
    move-exception v0

    #@9
    monitor-exit p0

    #@a
    throw v0

    #@b
    .line 737
    :catch_b
    move-exception v0

    #@c
    goto :goto_6
.end method

.method public declared-synchronized prepare(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 11
    .parameter "oldPackage"
    .parameter "newPackage"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 397
    monitor-enter p0

    #@3
    :try_start_3
    const-string v4, "ro.lge.b2b.vmware"

    #@5
    const/4 v5, 0x0

    #@6
    invoke-static {v4, v5}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@9
    move-result v4

    #@a
    if-eqz v4, :cond_13

    #@c
    .line 398
    const/4 v2, 0x0

    #@d
    invoke-virtual {p0, p1, p2, v2}, Lcom/android/server/connectivity/Vpn;->prepare(Ljava/lang/String;Ljava/lang/String;Z)Z
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_a4

    #@10
    move-result v2

    #@11
    .line 448
    :cond_11
    :goto_11
    monitor-exit p0

    #@12
    return v2

    #@13
    .line 403
    :cond_13
    if-eqz p1, :cond_1d

    #@15
    :try_start_15
    iget-object v4, p0, Lcom/android/server/connectivity/Vpn;->mPackage:Ljava/lang/String;

    #@17
    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1a
    move-result v4

    #@1b
    if-eqz v4, :cond_11

    #@1d
    .line 409
    :cond_1d
    if-eqz p2, :cond_2f

    #@1f
    iget-object v2, p0, Lcom/android/server/connectivity/Vpn;->mPackage:Ljava/lang/String;

    #@21
    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@24
    move-result v2

    #@25
    if-eqz v2, :cond_31

    #@27
    const-string v2, "[Legacy VPN]"

    #@29
    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2c
    move-result v2

    #@2d
    if-nez v2, :cond_31

    #@2f
    :cond_2f
    move v2, v3

    #@30
    .line 411
    goto :goto_11

    #@31
    .line 415
    :cond_31
    invoke-direct {p0}, Lcom/android/server/connectivity/Vpn;->enforceControlPermission()V

    #@34
    .line 418
    iget-object v2, p0, Lcom/android/server/connectivity/Vpn;->mInterface:Ljava/lang/String;

    #@36
    if-eqz v2, :cond_4f

    #@38
    .line 419
    iget-object v2, p0, Lcom/android/server/connectivity/Vpn;->mInterface:Ljava/lang/String;

    #@3a
    invoke-direct {p0, v2}, Lcom/android/server/connectivity/Vpn;->jniReset(Ljava/lang/String;)V

    #@3d
    .line 420
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_40
    .catchall {:try_start_15 .. :try_end_40} :catchall_a4

    #@40
    move-result-wide v0

    #@41
    .line 422
    .local v0, token:J
    :try_start_41
    iget-object v2, p0, Lcom/android/server/connectivity/Vpn;->mCallback:Lcom/android/server/ConnectivityService$VpnCallback;

    #@43
    invoke-virtual {v2}, Lcom/android/server/ConnectivityService$VpnCallback;->restore()V

    #@46
    .line 423
    invoke-direct {p0}, Lcom/android/server/connectivity/Vpn;->hideNotification()V
    :try_end_49
    .catchall {:try_start_41 .. :try_end_49} :catchall_9f

    #@49
    .line 425
    :try_start_49
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@4c
    .line 427
    const/4 v2, 0x0

    #@4d
    iput-object v2, p0, Lcom/android/server/connectivity/Vpn;->mInterface:Ljava/lang/String;

    #@4f
    .line 431
    .end local v0           #token:J
    :cond_4f
    iget-object v2, p0, Lcom/android/server/connectivity/Vpn;->mConnection:Lcom/android/server/connectivity/Vpn$Connection;
    :try_end_51
    .catchall {:try_start_49 .. :try_end_51} :catchall_a4

    #@51
    if-eqz v2, :cond_a7

    #@53
    .line 433
    :try_start_53
    iget-object v2, p0, Lcom/android/server/connectivity/Vpn;->mConnection:Lcom/android/server/connectivity/Vpn$Connection;

    #@55
    invoke-static {v2}, Lcom/android/server/connectivity/Vpn$Connection;->access$100(Lcom/android/server/connectivity/Vpn$Connection;)Landroid/os/IBinder;

    #@58
    move-result-object v2

    #@59
    const v4, 0xffffff

    #@5c
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5f
    move-result-object v5

    #@60
    const/4 v6, 0x0

    #@61
    const/4 v7, 0x1

    #@62
    invoke-interface {v2, v4, v5, v6, v7}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_65
    .catchall {:try_start_53 .. :try_end_65} :catchall_a4
    .catch Ljava/lang/Exception; {:try_start_53 .. :try_end_65} :catch_b4

    #@65
    .line 438
    :goto_65
    :try_start_65
    iget-object v2, p0, Lcom/android/server/connectivity/Vpn;->mContext:Landroid/content/Context;

    #@67
    iget-object v4, p0, Lcom/android/server/connectivity/Vpn;->mConnection:Lcom/android/server/connectivity/Vpn$Connection;

    #@69
    invoke-virtual {v2, v4}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    #@6c
    .line 439
    const/4 v2, 0x0

    #@6d
    iput-object v2, p0, Lcom/android/server/connectivity/Vpn;->mConnection:Lcom/android/server/connectivity/Vpn$Connection;

    #@6f
    .line 445
    :cond_6f
    :goto_6f
    const-string v2, "Vpn"

    #@71
    new-instance v4, Ljava/lang/StringBuilder;

    #@73
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@76
    const-string v5, "Switched from "

    #@78
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v4

    #@7c
    iget-object v5, p0, Lcom/android/server/connectivity/Vpn;->mPackage:Ljava/lang/String;

    #@7e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v4

    #@82
    const-string v5, " to "

    #@84
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v4

    #@88
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v4

    #@8c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8f
    move-result-object v4

    #@90
    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@93
    .line 446
    iput-object p2, p0, Lcom/android/server/connectivity/Vpn;->mPackage:Ljava/lang/String;

    #@95
    .line 447
    sget-object v2, Landroid/net/NetworkInfo$DetailedState;->IDLE:Landroid/net/NetworkInfo$DetailedState;

    #@97
    const-string v4, "prepare"

    #@99
    invoke-direct {p0, v2, v4}, Lcom/android/server/connectivity/Vpn;->updateState(Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;)V

    #@9c
    move v2, v3

    #@9d
    .line 448
    goto/16 :goto_11

    #@9f
    .line 425
    .restart local v0       #token:J
    :catchall_9f
    move-exception v2

    #@a0
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@a3
    throw v2
    :try_end_a4
    .catchall {:try_start_65 .. :try_end_a4} :catchall_a4

    #@a4
    .line 397
    .end local v0           #token:J
    :catchall_a4
    move-exception v2

    #@a5
    monitor-exit p0

    #@a6
    throw v2

    #@a7
    .line 440
    :cond_a7
    :try_start_a7
    iget-object v2, p0, Lcom/android/server/connectivity/Vpn;->mLegacyVpnRunner:Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;

    #@a9
    if-eqz v2, :cond_6f

    #@ab
    .line 441
    iget-object v2, p0, Lcom/android/server/connectivity/Vpn;->mLegacyVpnRunner:Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;

    #@ad
    invoke-virtual {v2}, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->exit()V

    #@b0
    .line 442
    const/4 v2, 0x0

    #@b1
    iput-object v2, p0, Lcom/android/server/connectivity/Vpn;->mLegacyVpnRunner:Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;
    :try_end_b3
    .catchall {:try_start_a7 .. :try_end_b3} :catchall_a4

    #@b3
    goto :goto_6f

    #@b4
    .line 435
    :catch_b4
    move-exception v2

    #@b5
    goto :goto_65
.end method

.method public declared-synchronized prepare(Ljava/lang/String;Ljava/lang/String;Z)Z
    .registers 13
    .parameter "oldPackage"
    .parameter "newPackage"
    .parameter "mvpOn"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 454
    monitor-enter p0

    #@3
    if-nez p3, :cond_c8

    #@5
    .line 456
    if-eqz p1, :cond_11

    #@7
    :try_start_7
    iget-object v5, p0, Lcom/android/server/connectivity/Vpn;->mPackage:Ljava/lang/String;

    #@9
    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_c
    .catchall {:try_start_7 .. :try_end_c} :catchall_da

    #@c
    move-result v5

    #@d
    if-nez v5, :cond_11

    #@f
    .line 549
    :cond_f
    :goto_f
    monitor-exit p0

    #@10
    return v3

    #@11
    .line 461
    :cond_11
    if-nez p2, :cond_19

    #@13
    :try_start_13
    invoke-direct {p0}, Lcom/android/server/connectivity/Vpn;->isRevokeNeeded()Z

    #@16
    move-result v5

    #@17
    if-nez v5, :cond_f

    #@19
    .line 466
    :cond_19
    if-eqz p2, :cond_31

    #@1b
    iget-object v5, p0, Lcom/android/server/connectivity/Vpn;->mPackage:Ljava/lang/String;

    #@1d
    invoke-virtual {p2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@20
    move-result v5

    #@21
    if-eqz v5, :cond_33

    #@23
    const-string v5, "[Legacy VPN]"

    #@25
    invoke-virtual {p2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@28
    move-result v5

    #@29
    if-nez v5, :cond_33

    #@2b
    invoke-direct {p0}, Lcom/android/server/connectivity/Vpn;->isRevokeNeeded()Z

    #@2e
    move-result v5

    #@2f
    if-nez v5, :cond_33

    #@31
    :cond_31
    move v3, v4

    #@32
    .line 468
    goto :goto_f

    #@33
    .line 472
    :cond_33
    invoke-direct {p0}, Lcom/android/server/connectivity/Vpn;->enforceControlPermission()V

    #@36
    .line 475
    invoke-direct {p0, p2}, Lcom/android/server/connectivity/Vpn;->isClientAllowedForHost(Ljava/lang/String;)Z

    #@39
    move-result v5

    #@3a
    if-eqz v5, :cond_f

    #@3c
    .line 493
    :cond_3c
    :goto_3c
    iget-object v3, p0, Lcom/android/server/connectivity/Vpn;->mInterface:Ljava/lang/String;

    #@3e
    if-eqz v3, :cond_5f

    #@40
    .line 494
    iget-boolean v3, p0, Lcom/android/server/connectivity/Vpn;->mMvpOn:Z

    #@42
    if-nez v3, :cond_dd

    #@44
    .line 495
    iget-object v3, p0, Lcom/android/server/connectivity/Vpn;->mInterface:Ljava/lang/String;

    #@46
    invoke-direct {p0, v3}, Lcom/android/server/connectivity/Vpn;->jniReset(Ljava/lang/String;)V

    #@49
    .line 509
    :cond_49
    :goto_49
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_4c
    .catchall {:try_start_13 .. :try_end_4c} :catchall_da

    #@4c
    move-result-wide v1

    #@4d
    .line 511
    .local v1, token:J
    :try_start_4d
    iget-boolean v3, p0, Lcom/android/server/connectivity/Vpn;->mMvpOn:Z

    #@4f
    if-nez v3, :cond_56

    #@51
    .line 512
    iget-object v3, p0, Lcom/android/server/connectivity/Vpn;->mCallback:Lcom/android/server/ConnectivityService$VpnCallback;

    #@53
    invoke-virtual {v3}, Lcom/android/server/ConnectivityService$VpnCallback;->restore()V

    #@56
    .line 514
    :cond_56
    invoke-direct {p0}, Lcom/android/server/connectivity/Vpn;->hideNotification()V
    :try_end_59
    .catchall {:try_start_4d .. :try_end_59} :catchall_109

    #@59
    .line 516
    :try_start_59
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@5c
    .line 518
    const/4 v3, 0x0

    #@5d
    iput-object v3, p0, Lcom/android/server/connectivity/Vpn;->mInterface:Ljava/lang/String;

    #@5f
    .line 522
    .end local v1           #token:J
    :cond_5f
    iput-boolean p3, p0, Lcom/android/server/connectivity/Vpn;->mMvpOn:Z

    #@61
    .line 523
    iget-boolean v3, p0, Lcom/android/server/connectivity/Vpn;->mMvpOn:Z

    #@63
    if-nez v3, :cond_78

    #@65
    iget-object v3, p0, Lcom/android/server/connectivity/Vpn;->mNamespace:Lcom/android/server/connectivity/VpnNamespace;

    #@67
    if-eqz v3, :cond_78

    #@69
    .line 526
    const-string v3, "Vpn"

    #@6b
    const-string v5, "namespace proxy no longer needed"

    #@6d
    invoke-static {v3, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@70
    .line 527
    iget-object v3, p0, Lcom/android/server/connectivity/Vpn;->mNamespace:Lcom/android/server/connectivity/VpnNamespace;

    #@72
    invoke-virtual {v3}, Lcom/android/server/connectivity/VpnNamespace;->close()V

    #@75
    .line 528
    const/4 v3, 0x0

    #@76
    iput-object v3, p0, Lcom/android/server/connectivity/Vpn;->mNamespace:Lcom/android/server/connectivity/VpnNamespace;

    #@78
    .line 532
    :cond_78
    iget-object v3, p0, Lcom/android/server/connectivity/Vpn;->mConnection:Lcom/android/server/connectivity/Vpn$Connection;
    :try_end_7a
    .catchall {:try_start_59 .. :try_end_7a} :catchall_da

    #@7a
    if-eqz v3, :cond_10e

    #@7c
    .line 534
    :try_start_7c
    iget-object v3, p0, Lcom/android/server/connectivity/Vpn;->mConnection:Lcom/android/server/connectivity/Vpn$Connection;

    #@7e
    invoke-static {v3}, Lcom/android/server/connectivity/Vpn$Connection;->access$100(Lcom/android/server/connectivity/Vpn$Connection;)Landroid/os/IBinder;

    #@81
    move-result-object v3

    #@82
    const v5, 0xffffff

    #@85
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@88
    move-result-object v6

    #@89
    const/4 v7, 0x0

    #@8a
    const/4 v8, 0x1

    #@8b
    invoke-interface {v3, v5, v6, v7, v8}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_8e
    .catchall {:try_start_7c .. :try_end_8e} :catchall_da
    .catch Ljava/lang/Exception; {:try_start_7c .. :try_end_8e} :catch_11c

    #@8e
    .line 539
    :goto_8e
    :try_start_8e
    iget-object v3, p0, Lcom/android/server/connectivity/Vpn;->mContext:Landroid/content/Context;

    #@90
    iget-object v5, p0, Lcom/android/server/connectivity/Vpn;->mConnection:Lcom/android/server/connectivity/Vpn$Connection;

    #@92
    invoke-virtual {v3, v5}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    #@95
    .line 540
    const/4 v3, 0x0

    #@96
    iput-object v3, p0, Lcom/android/server/connectivity/Vpn;->mConnection:Lcom/android/server/connectivity/Vpn$Connection;

    #@98
    .line 546
    :cond_98
    :goto_98
    const-string v3, "Vpn"

    #@9a
    new-instance v5, Ljava/lang/StringBuilder;

    #@9c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@9f
    const-string v6, "Switched from "

    #@a1
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v5

    #@a5
    iget-object v6, p0, Lcom/android/server/connectivity/Vpn;->mPackage:Ljava/lang/String;

    #@a7
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v5

    #@ab
    const-string v6, " to "

    #@ad
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v5

    #@b1
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v5

    #@b5
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b8
    move-result-object v5

    #@b9
    invoke-static {v3, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@bc
    .line 547
    iput-object p2, p0, Lcom/android/server/connectivity/Vpn;->mPackage:Ljava/lang/String;

    #@be
    .line 548
    sget-object v3, Landroid/net/NetworkInfo$DetailedState;->IDLE:Landroid/net/NetworkInfo$DetailedState;

    #@c0
    const-string v5, "prepare"

    #@c2
    invoke-direct {p0, v3, v5}, Lcom/android/server/connectivity/Vpn;->updateState(Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;)V

    #@c5
    move v3, v4

    #@c6
    .line 549
    goto/16 :goto_f

    #@c8
    .line 479
    :cond_c8
    if-eqz p2, :cond_f

    #@ca
    .line 484
    invoke-direct {p0}, Lcom/android/server/connectivity/Vpn;->enforceControlPermission()V

    #@cd
    .line 487
    const-string v3, "[Legacy VPN]"

    #@cf
    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d2
    move-result v3

    #@d3
    if-nez v3, :cond_3c

    #@d5
    .line 488
    invoke-direct {p0, p2}, Lcom/android/server/connectivity/Vpn;->addMvpClientToList(Ljava/lang/String;)V
    :try_end_d8
    .catchall {:try_start_8e .. :try_end_d8} :catchall_da

    #@d8
    goto/16 :goto_3c

    #@da
    .line 454
    :catchall_da
    move-exception v3

    #@db
    monitor-exit p0

    #@dc
    throw v3

    #@dd
    .line 497
    :cond_dd
    :try_start_dd
    iget-object v3, p0, Lcom/android/server/connectivity/Vpn;->mNamespace:Lcom/android/server/connectivity/VpnNamespace;
    :try_end_df
    .catchall {:try_start_dd .. :try_end_df} :catchall_da

    #@df
    if-eqz v3, :cond_49

    #@e1
    .line 499
    :try_start_e1
    iget-object v3, p0, Lcom/android/server/connectivity/Vpn;->mNamespace:Lcom/android/server/connectivity/VpnNamespace;

    #@e3
    invoke-virtual {v3}, Lcom/android/server/connectivity/VpnNamespace;->removeInterface()V
    :try_end_e6
    .catchall {:try_start_e1 .. :try_end_e6} :catchall_da
    .catch Ljava/io/IOException; {:try_start_e1 .. :try_end_e6} :catch_eb

    #@e6
    .line 505
    :goto_e6
    :try_start_e6
    invoke-direct {p0}, Lcom/android/server/connectivity/Vpn;->mvpInterfaceRemoved()V

    #@e9
    goto/16 :goto_49

    #@eb
    .line 500
    :catch_eb
    move-exception v0

    #@ec
    .line 503
    .local v0, ioe:Ljava/io/IOException;
    const-string v3, "Vpn"

    #@ee
    new-instance v5, Ljava/lang/StringBuilder;

    #@f0
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@f3
    const-string v6, "couldn\'t remove interface:"

    #@f5
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f8
    move-result-object v5

    #@f9
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    #@fc
    move-result-object v6

    #@fd
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@100
    move-result-object v5

    #@101
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@104
    move-result-object v5

    #@105
    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@108
    goto :goto_e6

    #@109
    .line 516
    .end local v0           #ioe:Ljava/io/IOException;
    .restart local v1       #token:J
    :catchall_109
    move-exception v3

    #@10a
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@10d
    throw v3

    #@10e
    .line 541
    .end local v1           #token:J
    :cond_10e
    iget-object v3, p0, Lcom/android/server/connectivity/Vpn;->mLegacyVpnRunner:Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;

    #@110
    if-eqz v3, :cond_98

    #@112
    .line 542
    iget-object v3, p0, Lcom/android/server/connectivity/Vpn;->mLegacyVpnRunner:Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;

    #@114
    invoke-virtual {v3}, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->exit()V

    #@117
    .line 543
    const/4 v3, 0x0

    #@118
    iput-object v3, p0, Lcom/android/server/connectivity/Vpn;->mLegacyVpnRunner:Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;
    :try_end_11a
    .catchall {:try_start_e6 .. :try_end_11a} :catchall_da

    #@11a
    goto/16 :goto_98

    #@11c
    .line 536
    :catch_11c
    move-exception v3

    #@11d
    goto/16 :goto_8e
.end method

.method public protect(Landroid/os/ParcelFileDescriptor;Ljava/lang/String;)V
    .registers 7
    .parameter "socket"
    .parameter "interfaze"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    #@0
    .prologue
    .line 561
    iget-object v2, p0, Lcom/android/server/connectivity/Vpn;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@5
    move-result-object v1

    #@6
    .line 562
    .local v1, pm:Landroid/content/pm/PackageManager;
    iget-object v2, p0, Lcom/android/server/connectivity/Vpn;->mPackage:Ljava/lang/String;

    #@8
    const/4 v3, 0x0

    #@9
    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    #@c
    move-result-object v0

    #@d
    .line 563
    .local v0, app:Landroid/content/pm/ApplicationInfo;
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@10
    move-result v2

    #@11
    iget v3, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    #@13
    if-eq v2, v3, :cond_1d

    #@15
    .line 564
    new-instance v2, Ljava/lang/SecurityException;

    #@17
    const-string v3, "Unauthorized Caller"

    #@19
    invoke-direct {v2, v3}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@1c
    throw v2

    #@1d
    .line 566
    :cond_1d
    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->getFd()I

    #@20
    move-result v2

    #@21
    invoke-direct {p0, v2, p2}, Lcom/android/server/connectivity/Vpn;->jniProtect(ILjava/lang/String;)V

    #@24
    .line 567
    return-void
.end method

.method public reconnect()Z
    .registers 2

    #@0
    .prologue
    .line 150
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@2
    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    #@5
    throw v0
.end method

.method public setDataConnectionMessanger(Landroid/os/Messenger;)V
    .registers 2
    .parameter "msger"

    #@0
    .prologue
    .line 1349
    return-void
.end method

.method public setEnableNotifications(Z)V
    .registers 2
    .parameter "enableNotif"

    #@0
    .prologue
    .line 133
    iput-boolean p1, p0, Lcom/android/server/connectivity/Vpn;->mEnableNotif:Z

    #@2
    .line 134
    return-void
.end method

.method public startLegacyVpn(Lcom/android/internal/net/VpnProfile;Landroid/security/KeyStore;Landroid/net/LinkProperties;)V
    .registers 16
    .parameter "profile"
    .parameter "keyStore"
    .parameter "egress"

    #@0
    .prologue
    .line 915
    invoke-virtual {p2}, Landroid/security/KeyStore;->state()Landroid/security/KeyStore$State;

    #@3
    move-result-object v10

    #@4
    sget-object v11, Landroid/security/KeyStore$State;->UNLOCKED:Landroid/security/KeyStore$State;

    #@6
    if-eq v10, v11, :cond_10

    #@8
    .line 916
    new-instance v10, Ljava/lang/IllegalStateException;

    #@a
    const-string v11, "KeyStore isn\'t unlocked"

    #@c
    invoke-direct {v10, v11}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@f
    throw v10

    #@10
    .line 919
    :cond_10
    invoke-virtual {p3}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    #@13
    move-result-object v3

    #@14
    .line 920
    .local v3, iface:Ljava/lang/String;
    invoke-static {p3}, Lcom/android/server/connectivity/Vpn;->findLegacyVpnGateway(Landroid/net/LinkProperties;)Ljava/lang/String;

    #@17
    move-result-object v2

    #@18
    .line 923
    .local v2, gateway:Ljava/lang/String;
    const-string v5, ""

    #@1a
    .line 924
    .local v5, privateKey:Ljava/lang/String;
    const-string v8, ""

    #@1c
    .line 925
    .local v8, userCert:Ljava/lang/String;
    const-string v0, ""

    #@1e
    .line 926
    .local v0, caCert:Ljava/lang/String;
    const-string v7, ""

    #@20
    .line 927
    .local v7, serverCert:Ljava/lang/String;
    iget-object v10, p1, Lcom/android/internal/net/VpnProfile;->ipsecUserCert:Ljava/lang/String;

    #@22
    invoke-virtual {v10}, Ljava/lang/String;->isEmpty()Z

    #@25
    move-result v10

    #@26
    if-nez v10, :cond_59

    #@28
    .line 928
    new-instance v10, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v11, "USRPKEY_"

    #@2f
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v10

    #@33
    iget-object v11, p1, Lcom/android/internal/net/VpnProfile;->ipsecUserCert:Ljava/lang/String;

    #@35
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v10

    #@39
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v5

    #@3d
    .line 929
    new-instance v10, Ljava/lang/StringBuilder;

    #@3f
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@42
    const-string v11, "USRCERT_"

    #@44
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v10

    #@48
    iget-object v11, p1, Lcom/android/internal/net/VpnProfile;->ipsecUserCert:Ljava/lang/String;

    #@4a
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v10

    #@4e
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@51
    move-result-object v10

    #@52
    invoke-virtual {p2, v10}, Landroid/security/KeyStore;->get(Ljava/lang/String;)[B

    #@55
    move-result-object v9

    #@56
    .line 930
    .local v9, value:[B
    if-nez v9, :cond_b1

    #@58
    const/4 v8, 0x0

    #@59
    .line 932
    .end local v9           #value:[B
    :cond_59
    :goto_59
    iget-object v10, p1, Lcom/android/internal/net/VpnProfile;->ipsecCaCert:Ljava/lang/String;

    #@5b
    invoke-virtual {v10}, Ljava/lang/String;->isEmpty()Z

    #@5e
    move-result v10

    #@5f
    if-nez v10, :cond_7d

    #@61
    .line 933
    new-instance v10, Ljava/lang/StringBuilder;

    #@63
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@66
    const-string v11, "CACERT_"

    #@68
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v10

    #@6c
    iget-object v11, p1, Lcom/android/internal/net/VpnProfile;->ipsecCaCert:Ljava/lang/String;

    #@6e
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v10

    #@72
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@75
    move-result-object v10

    #@76
    invoke-virtual {p2, v10}, Landroid/security/KeyStore;->get(Ljava/lang/String;)[B

    #@79
    move-result-object v9

    #@7a
    .line 934
    .restart local v9       #value:[B
    if-nez v9, :cond_b9

    #@7c
    const/4 v0, 0x0

    #@7d
    .line 936
    .end local v9           #value:[B
    :cond_7d
    :goto_7d
    iget-object v10, p1, Lcom/android/internal/net/VpnProfile;->ipsecServerCert:Ljava/lang/String;

    #@7f
    invoke-virtual {v10}, Ljava/lang/String;->isEmpty()Z

    #@82
    move-result v10

    #@83
    if-nez v10, :cond_a1

    #@85
    .line 937
    new-instance v10, Ljava/lang/StringBuilder;

    #@87
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@8a
    const-string v11, "USRCERT_"

    #@8c
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v10

    #@90
    iget-object v11, p1, Lcom/android/internal/net/VpnProfile;->ipsecServerCert:Ljava/lang/String;

    #@92
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v10

    #@96
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@99
    move-result-object v10

    #@9a
    invoke-virtual {p2, v10}, Landroid/security/KeyStore;->get(Ljava/lang/String;)[B

    #@9d
    move-result-object v9

    #@9e
    .line 938
    .restart local v9       #value:[B
    if-nez v9, :cond_c1

    #@a0
    const/4 v7, 0x0

    #@a1
    .line 940
    .end local v9           #value:[B
    :cond_a1
    :goto_a1
    if-eqz v5, :cond_a9

    #@a3
    if-eqz v8, :cond_a9

    #@a5
    if-eqz v0, :cond_a9

    #@a7
    if-nez v7, :cond_c9

    #@a9
    .line 941
    :cond_a9
    new-instance v10, Ljava/lang/IllegalStateException;

    #@ab
    const-string v11, "Cannot load credentials"

    #@ad
    invoke-direct {v10, v11}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b0
    throw v10

    #@b1
    .line 930
    .restart local v9       #value:[B
    :cond_b1
    new-instance v8, Ljava/lang/String;

    #@b3
    .end local v8           #userCert:Ljava/lang/String;
    sget-object v10, Ljava/nio/charset/Charsets;->UTF_8:Ljava/nio/charset/Charset;

    #@b5
    invoke-direct {v8, v9, v10}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    #@b8
    goto :goto_59

    #@b9
    .line 934
    .restart local v8       #userCert:Ljava/lang/String;
    :cond_b9
    new-instance v0, Ljava/lang/String;

    #@bb
    .end local v0           #caCert:Ljava/lang/String;
    sget-object v10, Ljava/nio/charset/Charsets;->UTF_8:Ljava/nio/charset/Charset;

    #@bd
    invoke-direct {v0, v9, v10}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    #@c0
    goto :goto_7d

    #@c1
    .line 938
    .restart local v0       #caCert:Ljava/lang/String;
    :cond_c1
    new-instance v7, Ljava/lang/String;

    #@c3
    .end local v7           #serverCert:Ljava/lang/String;
    sget-object v10, Ljava/nio/charset/Charsets;->UTF_8:Ljava/nio/charset/Charset;

    #@c5
    invoke-direct {v7, v9, v10}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    #@c8
    goto :goto_a1

    #@c9
    .line 945
    .end local v9           #value:[B
    .restart local v7       #serverCert:Ljava/lang/String;
    :cond_c9
    const/4 v6, 0x0

    #@ca
    .line 946
    .local v6, racoon:[Ljava/lang/String;
    iget v10, p1, Lcom/android/internal/net/VpnProfile;->type:I

    #@cc
    packed-switch v10, :pswitch_data_2de

    #@cf
    .line 980
    :goto_cf
    const/4 v4, 0x0

    #@d0
    .line 981
    .local v4, mtpd:[Ljava/lang/String;
    iget v10, p1, Lcom/android/internal/net/VpnProfile;->type:I

    #@d2
    packed-switch v10, :pswitch_data_2ec

    #@d5
    .line 1002
    :goto_d5
    new-instance v1, Lcom/android/internal/net/VpnConfig;

    #@d7
    invoke-direct {v1}, Lcom/android/internal/net/VpnConfig;-><init>()V

    #@da
    .line 1003
    .local v1, config:Lcom/android/internal/net/VpnConfig;
    const/4 v10, 0x1

    #@db
    iput-boolean v10, v1, Lcom/android/internal/net/VpnConfig;->legacy:Z

    #@dd
    .line 1004
    iget-object v10, p1, Lcom/android/internal/net/VpnProfile;->key:Ljava/lang/String;

    #@df
    iput-object v10, v1, Lcom/android/internal/net/VpnConfig;->user:Ljava/lang/String;

    #@e1
    .line 1005
    iput-object v3, v1, Lcom/android/internal/net/VpnConfig;->interfaze:Ljava/lang/String;

    #@e3
    .line 1006
    iget-object v10, p1, Lcom/android/internal/net/VpnProfile;->name:Ljava/lang/String;

    #@e5
    iput-object v10, v1, Lcom/android/internal/net/VpnConfig;->session:Ljava/lang/String;

    #@e7
    .line 1007
    iget-object v10, p1, Lcom/android/internal/net/VpnProfile;->routes:Ljava/lang/String;

    #@e9
    iput-object v10, v1, Lcom/android/internal/net/VpnConfig;->routes:Ljava/lang/String;

    #@eb
    .line 1008
    iget-object v10, p1, Lcom/android/internal/net/VpnProfile;->dnsServers:Ljava/lang/String;

    #@ed
    invoke-virtual {v10}, Ljava/lang/String;->isEmpty()Z

    #@f0
    move-result v10

    #@f1
    if-nez v10, :cond_101

    #@f3
    .line 1009
    iget-object v10, p1, Lcom/android/internal/net/VpnProfile;->dnsServers:Ljava/lang/String;

    #@f5
    const-string v11, " +"

    #@f7
    invoke-virtual {v10, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@fa
    move-result-object v10

    #@fb
    invoke-static {v10}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    #@fe
    move-result-object v10

    #@ff
    iput-object v10, v1, Lcom/android/internal/net/VpnConfig;->dnsServers:Ljava/util/List;

    #@101
    .line 1011
    :cond_101
    iget-object v10, p1, Lcom/android/internal/net/VpnProfile;->searchDomains:Ljava/lang/String;

    #@103
    invoke-virtual {v10}, Ljava/lang/String;->isEmpty()Z

    #@106
    move-result v10

    #@107
    if-nez v10, :cond_117

    #@109
    .line 1012
    iget-object v10, p1, Lcom/android/internal/net/VpnProfile;->searchDomains:Ljava/lang/String;

    #@10b
    const-string v11, " +"

    #@10d
    invoke-virtual {v10, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@110
    move-result-object v10

    #@111
    invoke-static {v10}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    #@114
    move-result-object v10

    #@115
    iput-object v10, v1, Lcom/android/internal/net/VpnConfig;->searchDomains:Ljava/util/List;

    #@117
    .line 1015
    :cond_117
    invoke-direct {p0, v1, v6, v4}, Lcom/android/server/connectivity/Vpn;->startLegacyVpn(Lcom/android/internal/net/VpnConfig;[Ljava/lang/String;[Ljava/lang/String;)V

    #@11a
    .line 1016
    return-void

    #@11b
    .line 948
    .end local v1           #config:Lcom/android/internal/net/VpnConfig;
    .end local v4           #mtpd:[Ljava/lang/String;
    :pswitch_11b
    const/4 v10, 0x6

    #@11c
    new-array v6, v10, [Ljava/lang/String;

    #@11e
    .end local v6           #racoon:[Ljava/lang/String;
    const/4 v10, 0x0

    #@11f
    aput-object v3, v6, v10

    #@121
    const/4 v10, 0x1

    #@122
    iget-object v11, p1, Lcom/android/internal/net/VpnProfile;->server:Ljava/lang/String;

    #@124
    aput-object v11, v6, v10

    #@126
    const/4 v10, 0x2

    #@127
    const-string v11, "udppsk"

    #@129
    aput-object v11, v6, v10

    #@12b
    const/4 v10, 0x3

    #@12c
    iget-object v11, p1, Lcom/android/internal/net/VpnProfile;->ipsecIdentifier:Ljava/lang/String;

    #@12e
    aput-object v11, v6, v10

    #@130
    const/4 v10, 0x4

    #@131
    iget-object v11, p1, Lcom/android/internal/net/VpnProfile;->ipsecSecret:Ljava/lang/String;

    #@133
    aput-object v11, v6, v10

    #@135
    const/4 v10, 0x5

    #@136
    const-string v11, "1701"

    #@138
    aput-object v11, v6, v10

    #@13a
    .line 952
    .restart local v6       #racoon:[Ljava/lang/String;
    goto :goto_cf

    #@13b
    .line 954
    :pswitch_13b
    const/16 v10, 0x8

    #@13d
    new-array v6, v10, [Ljava/lang/String;

    #@13f
    .end local v6           #racoon:[Ljava/lang/String;
    const/4 v10, 0x0

    #@140
    aput-object v3, v6, v10

    #@142
    const/4 v10, 0x1

    #@143
    iget-object v11, p1, Lcom/android/internal/net/VpnProfile;->server:Ljava/lang/String;

    #@145
    aput-object v11, v6, v10

    #@147
    const/4 v10, 0x2

    #@148
    const-string v11, "udprsa"

    #@14a
    aput-object v11, v6, v10

    #@14c
    const/4 v10, 0x3

    #@14d
    aput-object v5, v6, v10

    #@14f
    const/4 v10, 0x4

    #@150
    aput-object v8, v6, v10

    #@152
    const/4 v10, 0x5

    #@153
    aput-object v0, v6, v10

    #@155
    const/4 v10, 0x6

    #@156
    aput-object v7, v6, v10

    #@158
    const/4 v10, 0x7

    #@159
    const-string v11, "1701"

    #@15b
    aput-object v11, v6, v10

    #@15d
    .line 958
    .restart local v6       #racoon:[Ljava/lang/String;
    goto/16 :goto_cf

    #@15f
    .line 960
    :pswitch_15f
    const/16 v10, 0x9

    #@161
    new-array v6, v10, [Ljava/lang/String;

    #@163
    .end local v6           #racoon:[Ljava/lang/String;
    const/4 v10, 0x0

    #@164
    aput-object v3, v6, v10

    #@166
    const/4 v10, 0x1

    #@167
    iget-object v11, p1, Lcom/android/internal/net/VpnProfile;->server:Ljava/lang/String;

    #@169
    aput-object v11, v6, v10

    #@16b
    const/4 v10, 0x2

    #@16c
    const-string v11, "xauthpsk"

    #@16e
    aput-object v11, v6, v10

    #@170
    const/4 v10, 0x3

    #@171
    iget-object v11, p1, Lcom/android/internal/net/VpnProfile;->ipsecIdentifier:Ljava/lang/String;

    #@173
    aput-object v11, v6, v10

    #@175
    const/4 v10, 0x4

    #@176
    iget-object v11, p1, Lcom/android/internal/net/VpnProfile;->ipsecSecret:Ljava/lang/String;

    #@178
    aput-object v11, v6, v10

    #@17a
    const/4 v10, 0x5

    #@17b
    iget-object v11, p1, Lcom/android/internal/net/VpnProfile;->username:Ljava/lang/String;

    #@17d
    aput-object v11, v6, v10

    #@17f
    const/4 v10, 0x6

    #@180
    iget-object v11, p1, Lcom/android/internal/net/VpnProfile;->password:Ljava/lang/String;

    #@182
    aput-object v11, v6, v10

    #@184
    const/4 v10, 0x7

    #@185
    const-string v11, ""

    #@187
    aput-object v11, v6, v10

    #@189
    const/16 v10, 0x8

    #@18b
    aput-object v2, v6, v10

    #@18d
    .line 964
    .restart local v6       #racoon:[Ljava/lang/String;
    goto/16 :goto_cf

    #@18f
    .line 966
    :pswitch_18f
    const/16 v10, 0xb

    #@191
    new-array v6, v10, [Ljava/lang/String;

    #@193
    .end local v6           #racoon:[Ljava/lang/String;
    const/4 v10, 0x0

    #@194
    aput-object v3, v6, v10

    #@196
    const/4 v10, 0x1

    #@197
    iget-object v11, p1, Lcom/android/internal/net/VpnProfile;->server:Ljava/lang/String;

    #@199
    aput-object v11, v6, v10

    #@19b
    const/4 v10, 0x2

    #@19c
    const-string v11, "xauthrsa"

    #@19e
    aput-object v11, v6, v10

    #@1a0
    const/4 v10, 0x3

    #@1a1
    aput-object v5, v6, v10

    #@1a3
    const/4 v10, 0x4

    #@1a4
    aput-object v8, v6, v10

    #@1a6
    const/4 v10, 0x5

    #@1a7
    aput-object v0, v6, v10

    #@1a9
    const/4 v10, 0x6

    #@1aa
    aput-object v7, v6, v10

    #@1ac
    const/4 v10, 0x7

    #@1ad
    iget-object v11, p1, Lcom/android/internal/net/VpnProfile;->username:Ljava/lang/String;

    #@1af
    aput-object v11, v6, v10

    #@1b1
    const/16 v10, 0x8

    #@1b3
    iget-object v11, p1, Lcom/android/internal/net/VpnProfile;->password:Ljava/lang/String;

    #@1b5
    aput-object v11, v6, v10

    #@1b7
    const/16 v10, 0x9

    #@1b9
    const-string v11, ""

    #@1bb
    aput-object v11, v6, v10

    #@1bd
    const/16 v10, 0xa

    #@1bf
    aput-object v2, v6, v10

    #@1c1
    .line 970
    .restart local v6       #racoon:[Ljava/lang/String;
    goto/16 :goto_cf

    #@1c3
    .line 972
    :pswitch_1c3
    const/16 v10, 0x9

    #@1c5
    new-array v6, v10, [Ljava/lang/String;

    #@1c7
    .end local v6           #racoon:[Ljava/lang/String;
    const/4 v10, 0x0

    #@1c8
    aput-object v3, v6, v10

    #@1ca
    const/4 v10, 0x1

    #@1cb
    iget-object v11, p1, Lcom/android/internal/net/VpnProfile;->server:Ljava/lang/String;

    #@1cd
    aput-object v11, v6, v10

    #@1cf
    const/4 v10, 0x2

    #@1d0
    const-string v11, "hybridrsa"

    #@1d2
    aput-object v11, v6, v10

    #@1d4
    const/4 v10, 0x3

    #@1d5
    aput-object v0, v6, v10

    #@1d7
    const/4 v10, 0x4

    #@1d8
    aput-object v7, v6, v10

    #@1da
    const/4 v10, 0x5

    #@1db
    iget-object v11, p1, Lcom/android/internal/net/VpnProfile;->username:Ljava/lang/String;

    #@1dd
    aput-object v11, v6, v10

    #@1df
    const/4 v10, 0x6

    #@1e0
    iget-object v11, p1, Lcom/android/internal/net/VpnProfile;->password:Ljava/lang/String;

    #@1e2
    aput-object v11, v6, v10

    #@1e4
    const/4 v10, 0x7

    #@1e5
    const-string v11, ""

    #@1e7
    aput-object v11, v6, v10

    #@1e9
    const/16 v10, 0x8

    #@1eb
    aput-object v2, v6, v10

    #@1ed
    .restart local v6       #racoon:[Ljava/lang/String;
    goto/16 :goto_cf

    #@1ef
    .line 983
    .restart local v4       #mtpd:[Ljava/lang/String;
    :pswitch_1ef
    const/16 v10, 0x14

    #@1f1
    new-array v4, v10, [Ljava/lang/String;

    #@1f3
    .end local v4           #mtpd:[Ljava/lang/String;
    const/4 v10, 0x0

    #@1f4
    aput-object v3, v4, v10

    #@1f6
    const/4 v10, 0x1

    #@1f7
    const-string v11, "pptp"

    #@1f9
    aput-object v11, v4, v10

    #@1fb
    const/4 v10, 0x2

    #@1fc
    iget-object v11, p1, Lcom/android/internal/net/VpnProfile;->server:Ljava/lang/String;

    #@1fe
    aput-object v11, v4, v10

    #@200
    const/4 v10, 0x3

    #@201
    const-string v11, "1723"

    #@203
    aput-object v11, v4, v10

    #@205
    const/4 v10, 0x4

    #@206
    const-string v11, "name"

    #@208
    aput-object v11, v4, v10

    #@20a
    const/4 v10, 0x5

    #@20b
    iget-object v11, p1, Lcom/android/internal/net/VpnProfile;->username:Ljava/lang/String;

    #@20d
    aput-object v11, v4, v10

    #@20f
    const/4 v10, 0x6

    #@210
    const-string v11, "password"

    #@212
    aput-object v11, v4, v10

    #@214
    const/4 v10, 0x7

    #@215
    iget-object v11, p1, Lcom/android/internal/net/VpnProfile;->password:Ljava/lang/String;

    #@217
    aput-object v11, v4, v10

    #@219
    const/16 v10, 0x8

    #@21b
    const-string v11, "linkname"

    #@21d
    aput-object v11, v4, v10

    #@21f
    const/16 v10, 0x9

    #@221
    const-string v11, "vpn"

    #@223
    aput-object v11, v4, v10

    #@225
    const/16 v10, 0xa

    #@227
    const-string v11, "refuse-eap"

    #@229
    aput-object v11, v4, v10

    #@22b
    const/16 v10, 0xb

    #@22d
    const-string v11, "nodefaultroute"

    #@22f
    aput-object v11, v4, v10

    #@231
    const/16 v10, 0xc

    #@233
    const-string v11, "usepeerdns"

    #@235
    aput-object v11, v4, v10

    #@237
    const/16 v10, 0xd

    #@239
    const-string v11, "idle"

    #@23b
    aput-object v11, v4, v10

    #@23d
    const/16 v10, 0xe

    #@23f
    const-string v11, "1800"

    #@241
    aput-object v11, v4, v10

    #@243
    const/16 v10, 0xf

    #@245
    const-string v11, "mtu"

    #@247
    aput-object v11, v4, v10

    #@249
    const/16 v10, 0x10

    #@24b
    const-string v11, "1400"

    #@24d
    aput-object v11, v4, v10

    #@24f
    const/16 v10, 0x11

    #@251
    const-string v11, "mru"

    #@253
    aput-object v11, v4, v10

    #@255
    const/16 v10, 0x12

    #@257
    const-string v11, "1400"

    #@259
    aput-object v11, v4, v10

    #@25b
    const/16 v11, 0x13

    #@25d
    iget-boolean v10, p1, Lcom/android/internal/net/VpnProfile;->mppe:Z

    #@25f
    if-eqz v10, :cond_267

    #@261
    const-string v10, "+mppe"

    #@263
    :goto_263
    aput-object v10, v4, v11

    #@265
    .line 990
    .restart local v4       #mtpd:[Ljava/lang/String;
    goto/16 :goto_d5

    #@267
    .line 983
    .end local v4           #mtpd:[Ljava/lang/String;
    :cond_267
    const-string v10, "nomppe"

    #@269
    goto :goto_263

    #@26a
    .line 993
    .restart local v4       #mtpd:[Ljava/lang/String;
    :pswitch_26a
    const/16 v10, 0x14

    #@26c
    new-array v4, v10, [Ljava/lang/String;

    #@26e
    .end local v4           #mtpd:[Ljava/lang/String;
    const/4 v10, 0x0

    #@26f
    aput-object v3, v4, v10

    #@271
    const/4 v10, 0x1

    #@272
    const-string v11, "l2tp"

    #@274
    aput-object v11, v4, v10

    #@276
    const/4 v10, 0x2

    #@277
    iget-object v11, p1, Lcom/android/internal/net/VpnProfile;->server:Ljava/lang/String;

    #@279
    aput-object v11, v4, v10

    #@27b
    const/4 v10, 0x3

    #@27c
    const-string v11, "1701"

    #@27e
    aput-object v11, v4, v10

    #@280
    const/4 v10, 0x4

    #@281
    iget-object v11, p1, Lcom/android/internal/net/VpnProfile;->l2tpSecret:Ljava/lang/String;

    #@283
    aput-object v11, v4, v10

    #@285
    const/4 v10, 0x5

    #@286
    const-string v11, "name"

    #@288
    aput-object v11, v4, v10

    #@28a
    const/4 v10, 0x6

    #@28b
    iget-object v11, p1, Lcom/android/internal/net/VpnProfile;->username:Ljava/lang/String;

    #@28d
    aput-object v11, v4, v10

    #@28f
    const/4 v10, 0x7

    #@290
    const-string v11, "password"

    #@292
    aput-object v11, v4, v10

    #@294
    const/16 v10, 0x8

    #@296
    iget-object v11, p1, Lcom/android/internal/net/VpnProfile;->password:Ljava/lang/String;

    #@298
    aput-object v11, v4, v10

    #@29a
    const/16 v10, 0x9

    #@29c
    const-string v11, "linkname"

    #@29e
    aput-object v11, v4, v10

    #@2a0
    const/16 v10, 0xa

    #@2a2
    const-string v11, "vpn"

    #@2a4
    aput-object v11, v4, v10

    #@2a6
    const/16 v10, 0xb

    #@2a8
    const-string v11, "refuse-eap"

    #@2aa
    aput-object v11, v4, v10

    #@2ac
    const/16 v10, 0xc

    #@2ae
    const-string v11, "nodefaultroute"

    #@2b0
    aput-object v11, v4, v10

    #@2b2
    const/16 v10, 0xd

    #@2b4
    const-string v11, "usepeerdns"

    #@2b6
    aput-object v11, v4, v10

    #@2b8
    const/16 v10, 0xe

    #@2ba
    const-string v11, "idle"

    #@2bc
    aput-object v11, v4, v10

    #@2be
    const/16 v10, 0xf

    #@2c0
    const-string v11, "1800"

    #@2c2
    aput-object v11, v4, v10

    #@2c4
    const/16 v10, 0x10

    #@2c6
    const-string v11, "mtu"

    #@2c8
    aput-object v11, v4, v10

    #@2ca
    const/16 v10, 0x11

    #@2cc
    const-string v11, "1400"

    #@2ce
    aput-object v11, v4, v10

    #@2d0
    const/16 v10, 0x12

    #@2d2
    const-string v11, "mru"

    #@2d4
    aput-object v11, v4, v10

    #@2d6
    const/16 v10, 0x13

    #@2d8
    const-string v11, "1400"

    #@2da
    aput-object v11, v4, v10

    #@2dc
    .restart local v4       #mtpd:[Ljava/lang/String;
    goto/16 :goto_d5

    #@2de
    .line 946
    :pswitch_data_2de
    .packed-switch 0x1
        :pswitch_11b
        :pswitch_13b
        :pswitch_15f
        :pswitch_18f
        :pswitch_1c3
    .end packed-switch

    #@2ec
    .line 981
    :pswitch_data_2ec
    .packed-switch 0x0
        :pswitch_1ef
        :pswitch_26a
        :pswitch_26a
    .end packed-switch
.end method

.method protected startMonitoringInternal()V
    .registers 1

    #@0
    .prologue
    .line 139
    return-void
.end method

.method public declared-synchronized stopLegacyVpn()V
    .registers 3

    #@0
    .prologue
    .line 1031
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/server/connectivity/Vpn;->mLegacyVpnRunner:Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;

    #@3
    if-eqz v0, :cond_11

    #@5
    .line 1032
    iget-object v0, p0, Lcom/android/server/connectivity/Vpn;->mLegacyVpnRunner:Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;

    #@7
    invoke-virtual {v0}, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->exit()V

    #@a
    .line 1033
    const/4 v0, 0x0

    #@b
    iput-object v0, p0, Lcom/android/server/connectivity/Vpn;->mLegacyVpnRunner:Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;

    #@d
    .line 1035
    const-string v1, "LegacyVpnRunner"

    #@f
    monitor-enter v1
    :try_end_10
    .catchall {:try_start_1 .. :try_end_10} :catchall_16

    #@10
    .line 1038
    :try_start_10
    monitor-exit v1
    :try_end_11
    .catchall {:try_start_10 .. :try_end_11} :catchall_13

    #@11
    .line 1040
    :cond_11
    monitor-exit p0

    #@12
    return-void

    #@13
    .line 1038
    :catchall_13
    move-exception v0

    #@14
    :try_start_14
    monitor-exit v1
    :try_end_15
    .catchall {:try_start_14 .. :try_end_15} :catchall_13

    #@15
    :try_start_15
    throw v0
    :try_end_16
    .catchall {:try_start_15 .. :try_end_16} :catchall_16

    #@16
    .line 1031
    :catchall_16
    move-exception v0

    #@17
    monitor-exit p0

    #@18
    throw v0
.end method

.method public teardown()Z
    .registers 2

    #@0
    .prologue
    .line 144
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@2
    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    #@5
    throw v0
.end method
