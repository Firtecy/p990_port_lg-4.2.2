.class public Lcom/android/server/connectivity/TetherNetwork;
.super Ljava/lang/Object;
.source "TetherNetwork.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;,
        Lcom/android/server/connectivity/TetherNetwork$TetherNetworkReceiver;
    }
.end annotation


# static fields
.field private static final ACTION_NETWORK_ERROR_DIALOG:Ljava/lang/String; = "lge.tethernetwork.action.NETWORK_ERROR_DIALOG"

.field private static final ACTION_NETWORK_ERROR_TOAST:Ljava/lang/String; = "lge.tethernetwork.action.NETWORK_ERROR_TOAST"

.field private static final ACTION_NETWORK_REJECT_DIALOG:Ljava/lang/String; = "lge.tethernetwork.action.NETWORK_REJECT_DIALOG"

.field private static final DEBUG:Z = true

.field private static final DIALOG_INDEX_MIP_ERROR:I = 0x0

.field private static final DIALOG_INDEX_REJECT_ERROR:I = 0x1

.field private static final HOTSPOT_FEATURE_DISABLED:I = 0x0

.field private static final HOTSPOT_FEATURE_ENABLED:I = 0x1

.field private static final HOTSPOT_FEATURE_UNKNOWN:I = -0x1

.field private static final MSG_REMOVE_TETHER_DEVICE:I = 0x3e8

.field private static final MSG_SHOW_MIP_ERR_DLG:I = 0x3fc

.field private static final MSG_SHOW_NET_ERR_TOAST:I = 0x3fe

.field private static final MSG_SHOW_REJECT_ERR_DLG:I = 0x3fd

.field private static final MSG_START_TETHER_NETWORK:I = 0x3f2

.field private static final MSG_STOP_TETHER_NETWORK:I = 0x3f3

.field private static final TAG:Ljava/lang/String; = "TetherNetwork"

.field private static final TETHER_NETWORK_STATUS_ENABLED:I = 0x1

.field private static final TETHER_NETWORK_STATUS_IDLE:I = 0x0

.field public static final TETHER_STATE_UPDATE:Ljava/lang/String; = "android.intent.action.TETHER_STATE_UPDATE"

.field private static mTetherNetworkHandler:Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;

.field private static mTetherNetworkStatus:I

.field private static mUsbTetherEnabled:Z

.field private static mWifiTetherEnabled:Z

.field private static tetherNetworkThread:Landroid/os/HandlerThread;


# instance fields
.field private mBTRegexs:[Ljava/lang/String;

.field private mBluetoothPan:Landroid/bluetooth/BluetoothPan;

.field private mContext:Landroid/content/Context;

.field private mHotspotFeatureEnabled:I

.field private mLineNumberEnabled:Z

.field private mProfileServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

.field private mTetherNetwork:Landroid/content/BroadcastReceiver;

.field private mTetherNetworkDataTrans:Lcom/android/server/connectivity/TetherNetworkDataTransition;

.field private mUsbRegexs:[Ljava/lang/String;

.field private mWifiRegexs:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 91
    const/4 v0, 0x0

    #@1
    sput-object v0, Lcom/android/server/connectivity/TetherNetwork;->mTetherNetworkHandler:Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;

    #@3
    .line 98
    const/4 v0, 0x0

    #@4
    sput v0, Lcom/android/server/connectivity/TetherNetwork;->mTetherNetworkStatus:I

    #@6
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "ctx"

    #@0
    .prologue
    .line 142
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 86
    const/4 v0, 0x0

    #@4
    iput-boolean v0, p0, Lcom/android/server/connectivity/TetherNetwork;->mLineNumberEnabled:Z

    #@6
    .line 94
    const/4 v0, 0x0

    #@7
    iput-object v0, p0, Lcom/android/server/connectivity/TetherNetwork;->mTetherNetworkDataTrans:Lcom/android/server/connectivity/TetherNetworkDataTransition;

    #@9
    .line 118
    const/4 v0, -0x1

    #@a
    iput v0, p0, Lcom/android/server/connectivity/TetherNetwork;->mHotspotFeatureEnabled:I

    #@c
    .line 129
    new-instance v0, Lcom/android/server/connectivity/TetherNetwork$1;

    #@e
    invoke-direct {v0, p0}, Lcom/android/server/connectivity/TetherNetwork$1;-><init>(Lcom/android/server/connectivity/TetherNetwork;)V

    #@11
    iput-object v0, p0, Lcom/android/server/connectivity/TetherNetwork;->mProfileServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    #@13
    .line 143
    iput-object p1, p0, Lcom/android/server/connectivity/TetherNetwork;->mContext:Landroid/content/Context;

    #@15
    .line 145
    sget-object v0, Lcom/android/server/connectivity/TetherNetwork;->tetherNetworkThread:Landroid/os/HandlerThread;

    #@17
    if-nez v0, :cond_27

    #@19
    .line 147
    new-instance v0, Landroid/os/HandlerThread;

    #@1b
    const-string v1, "TetherNetwork"

    #@1d
    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    #@20
    sput-object v0, Lcom/android/server/connectivity/TetherNetwork;->tetherNetworkThread:Landroid/os/HandlerThread;

    #@22
    .line 148
    sget-object v0, Lcom/android/server/connectivity/TetherNetwork;->tetherNetworkThread:Landroid/os/HandlerThread;

    #@24
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    #@27
    .line 151
    :cond_27
    sget-object v0, Lcom/android/server/connectivity/TetherNetwork;->mTetherNetworkHandler:Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;

    #@29
    if-nez v0, :cond_38

    #@2b
    .line 152
    new-instance v0, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;

    #@2d
    sget-object v1, Lcom/android/server/connectivity/TetherNetwork;->tetherNetworkThread:Landroid/os/HandlerThread;

    #@2f
    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@32
    move-result-object v1

    #@33
    invoke-direct {v0, p0, v1}, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;-><init>(Lcom/android/server/connectivity/TetherNetwork;Landroid/os/Looper;)V

    #@36
    sput-object v0, Lcom/android/server/connectivity/TetherNetwork;->mTetherNetworkHandler:Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;

    #@38
    .line 154
    :cond_38
    iget-object v0, p0, Lcom/android/server/connectivity/TetherNetwork;->mTetherNetworkDataTrans:Lcom/android/server/connectivity/TetherNetworkDataTransition;

    #@3a
    if-nez v0, :cond_43

    #@3c
    .line 155
    new-instance v0, Lcom/android/server/connectivity/TetherNetworkDataTransition;

    #@3e
    invoke-direct {v0, p1}, Lcom/android/server/connectivity/TetherNetworkDataTransition;-><init>(Landroid/content/Context;)V

    #@41
    iput-object v0, p0, Lcom/android/server/connectivity/TetherNetwork;->mTetherNetworkDataTrans:Lcom/android/server/connectivity/TetherNetworkDataTransition;

    #@43
    .line 157
    :cond_43
    return-void
.end method

.method static synthetic access$002(Lcom/android/server/connectivity/TetherNetwork;Landroid/bluetooth/BluetoothPan;)Landroid/bluetooth/BluetoothPan;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 70
    iput-object p1, p0, Lcom/android/server/connectivity/TetherNetwork;->mBluetoothPan:Landroid/bluetooth/BluetoothPan;

    #@2
    return-object p1
.end method

.method static synthetic access$200(Lcom/android/server/connectivity/TetherNetwork;)Lcom/android/server/connectivity/TetherNetworkDataTransition;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 70
    iget-object v0, p0, Lcom/android/server/connectivity/TetherNetwork;->mTetherNetworkDataTrans:Lcom/android/server/connectivity/TetherNetworkDataTransition;

    #@2
    return-object v0
.end method

.method static synthetic access$300()Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;
    .registers 1

    #@0
    .prologue
    .line 70
    sget-object v0, Lcom/android/server/connectivity/TetherNetwork;->mTetherNetworkHandler:Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Lcom/android/server/connectivity/TetherNetwork;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 70
    invoke-direct {p0}, Lcom/android/server/connectivity/TetherNetwork;->turnOffWifi()V

    #@3
    return-void
.end method

.method static synthetic access$500()I
    .registers 1

    #@0
    .prologue
    .line 70
    sget v0, Lcom/android/server/connectivity/TetherNetwork;->mTetherNetworkStatus:I

    #@2
    return v0
.end method

.method static synthetic access$600(Lcom/android/server/connectivity/TetherNetwork;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 70
    iget-object v0, p0, Lcom/android/server/connectivity/TetherNetwork;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Lcom/android/server/connectivity/TetherNetwork;Landroid/content/Context;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/android/server/connectivity/TetherNetwork;->removeTetherDevices(Landroid/content/Context;)V

    #@3
    return-void
.end method

.method static synthetic access$800(Lcom/android/server/connectivity/TetherNetwork;Landroid/content/Context;I)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 70
    invoke-direct {p0, p1, p2}, Lcom/android/server/connectivity/TetherNetwork;->showDialog(Landroid/content/Context;I)V

    #@3
    return-void
.end method

.method static synthetic access$900(Lcom/android/server/connectivity/TetherNetwork;Landroid/content/Context;Ljava/lang/String;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 70
    invoke-direct {p0, p1, p2}, Lcom/android/server/connectivity/TetherNetwork;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    #@3
    return-void
.end method

.method private findIface([Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    .registers 12
    .parameter "ifaces"
    .parameter "regexes"

    #@0
    .prologue
    .line 405
    move-object v0, p1

    #@1
    .local v0, arr$:[Ljava/lang/String;
    array-length v5, v0

    #@2
    .local v5, len$:I
    const/4 v2, 0x0

    #@3
    .local v2, i$:I
    move v3, v2

    #@4
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v2           #i$:I
    .end local v5           #len$:I
    .local v3, i$:I
    :goto_4
    if-ge v3, v5, :cond_1d

    #@6
    aget-object v4, v0, v3

    #@8
    .line 406
    .local v4, iface:Ljava/lang/String;
    move-object v1, p2

    #@9
    .local v1, arr$:[Ljava/lang/String;
    array-length v6, v1

    #@a
    .local v6, len$:I
    const/4 v2, 0x0

    #@b
    .end local v3           #i$:I
    .restart local v2       #i$:I
    :goto_b
    if-ge v2, v6, :cond_19

    #@d
    aget-object v7, v1, v2

    #@f
    .line 407
    .local v7, regex:Ljava/lang/String;
    invoke-virtual {v4, v7}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    #@12
    move-result v8

    #@13
    if-eqz v8, :cond_16

    #@15
    .line 412
    .end local v1           #arr$:[Ljava/lang/String;
    .end local v2           #i$:I
    .end local v4           #iface:Ljava/lang/String;
    .end local v6           #len$:I
    .end local v7           #regex:Ljava/lang/String;
    :goto_15
    return-object v4

    #@16
    .line 406
    .restart local v1       #arr$:[Ljava/lang/String;
    .restart local v2       #i$:I
    .restart local v4       #iface:Ljava/lang/String;
    .restart local v6       #len$:I
    .restart local v7       #regex:Ljava/lang/String;
    :cond_16
    add-int/lit8 v2, v2, 0x1

    #@18
    goto :goto_b

    #@19
    .line 405
    .end local v7           #regex:Ljava/lang/String;
    :cond_19
    add-int/lit8 v2, v3, 0x1

    #@1b
    move v3, v2

    #@1c
    .end local v2           #i$:I
    .restart local v3       #i$:I
    goto :goto_4

    #@1d
    .line 412
    .end local v1           #arr$:[Ljava/lang/String;
    .end local v4           #iface:Ljava/lang/String;
    .end local v6           #len$:I
    :cond_1d
    const/4 v4, 0x0

    #@1e
    goto :goto_15
.end method

.method private notifyPhoneTransmitPower(I)V
    .registers 5
    .parameter "powerLevel"

    #@0
    .prologue
    .line 552
    const-string v1, "phone"

    #@2
    invoke-static {v1}, Landroid/os/ServiceManager;->checkService(Ljava/lang/String;)Landroid/os/IBinder;

    #@5
    move-result-object v1

    #@6
    invoke-static {v1}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    #@9
    move-result-object v0

    #@a
    .line 553
    .local v0, phone:Lcom/android/internal/telephony/ITelephony;
    if-nez v0, :cond_13

    #@c
    .line 554
    const-string v1, "TetherNetwork"

    #@e
    const-string v2, "ITelephony interface is null, can not set transmit power"

    #@10
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@13
    .line 569
    :cond_13
    return-void
.end method

.method private readChameleonIntValue(Ljava/lang/String;I)I
    .registers 19
    .parameter "fn"
    .parameter "ref_value"

    #@0
    .prologue
    .line 603
    const/4 v4, 0x0

    #@1
    .line 604
    .local v4, fr:Ljava/io/FileReader;
    const/4 v6, 0x0

    #@2
    .line 605
    .local v6, inFile:Ljava/io/BufferedReader;
    const/4 v12, 0x0

    #@3
    .line 606
    .local v12, value:Ljava/lang/String;
    move/from16 v9, p2

    #@5
    .line 608
    .local v9, ret_value:I
    new-instance v3, Ljava/io/File;

    #@7
    move-object/from16 v0, p1

    #@9
    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@c
    .line 609
    .local v3, fh:Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    #@f
    move-result v13

    #@10
    if-nez v13, :cond_14

    #@12
    move v10, v9

    #@13
    .line 657
    .end local v9           #ret_value:I
    .local v10, ret_value:I
    :goto_13
    return v10

    #@14
    .line 614
    .end local v10           #ret_value:I
    .restart local v9       #ret_value:I
    :cond_14
    :try_start_14
    new-instance v5, Ljava/io/FileReader;

    #@16
    move-object/from16 v0, p1

    #@18
    invoke-direct {v5, v0}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_1b
    .catch Ljava/io/FileNotFoundException; {:try_start_14 .. :try_end_1b} :catch_49

    #@1b
    .line 622
    .end local v4           #fr:Ljava/io/FileReader;
    .local v5, fr:Ljava/io/FileReader;
    :try_start_1b
    new-instance v7, Ljava/io/BufferedReader;

    #@1d
    invoke-direct {v7, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_20
    .catchall {:try_start_1b .. :try_end_20} :catchall_66
    .catch Ljava/io/IOException; {:try_start_1b .. :try_end_20} :catch_79

    #@20
    .line 623
    .end local v6           #inFile:Ljava/io/BufferedReader;
    .local v7, inFile:Ljava/io/BufferedReader;
    :try_start_20
    invoke-virtual {v7}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    #@23
    move-result-object v8

    #@24
    .line 624
    .local v8, line:Ljava/lang/String;
    if-eqz v8, :cond_35

    #@26
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    #@29
    move-result v13

    #@2a
    if-lez v13, :cond_35

    #@2c
    .line 625
    new-instance v11, Ljava/util/StringTokenizer;

    #@2e
    invoke-direct {v11, v8}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;)V

    #@31
    .line 626
    .local v11, tokenizer:Ljava/util/StringTokenizer;
    invoke-virtual {v11}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;
    :try_end_34
    .catchall {:try_start_20 .. :try_end_34} :catchall_8b
    .catch Ljava/io/IOException; {:try_start_20 .. :try_end_34} :catch_8e

    #@34
    move-result-object v12

    #@35
    .line 634
    .end local v11           #tokenizer:Ljava/util/StringTokenizer;
    :cond_35
    if-eqz v7, :cond_3a

    #@37
    .line 635
    :try_start_37
    invoke-virtual {v7}, Ljava/io/BufferedReader;->close()V

    #@3a
    .line 637
    :cond_3a
    if-eqz v5, :cond_3f

    #@3c
    .line 638
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V
    :try_end_3f
    .catch Ljava/lang/Exception; {:try_start_37 .. :try_end_3f} :catch_87

    #@3f
    :cond_3f
    :goto_3f
    move-object v6, v7

    #@40
    .line 648
    .end local v7           #inFile:Ljava/io/BufferedReader;
    .end local v8           #line:Ljava/lang/String;
    .restart local v6       #inFile:Ljava/io/BufferedReader;
    :cond_40
    :goto_40
    if-eqz v12, :cond_72

    #@42
    .line 649
    :try_start_42
    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_45
    .catch Ljava/lang/Exception; {:try_start_42 .. :try_end_45} :catch_75

    #@45
    move-result v9

    #@46
    :goto_46
    move-object v4, v5

    #@47
    .end local v5           #fr:Ljava/io/FileReader;
    .restart local v4       #fr:Ljava/io/FileReader;
    move v10, v9

    #@48
    .line 657
    .end local v9           #ret_value:I
    .restart local v10       #ret_value:I
    goto :goto_13

    #@49
    .line 615
    .end local v10           #ret_value:I
    .restart local v9       #ret_value:I
    :catch_49
    move-exception v2

    #@4a
    .line 616
    .local v2, fe:Ljava/io/FileNotFoundException;
    const-string v13, "TetherNetwork"

    #@4c
    new-instance v14, Ljava/lang/StringBuilder;

    #@4e
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@51
    const-string v15, "Can\'t open "

    #@53
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v14

    #@57
    move-object/from16 v0, p1

    #@59
    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v14

    #@5d
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@60
    move-result-object v14

    #@61
    invoke-static {v13, v14}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@64
    move v10, v9

    #@65
    .line 617
    .end local v9           #ret_value:I
    .restart local v10       #ret_value:I
    goto :goto_13

    #@66
    .line 632
    .end local v2           #fe:Ljava/io/FileNotFoundException;
    .end local v4           #fr:Ljava/io/FileReader;
    .end local v10           #ret_value:I
    .restart local v5       #fr:Ljava/io/FileReader;
    .restart local v9       #ret_value:I
    :catchall_66
    move-exception v13

    #@67
    .line 634
    :goto_67
    if-eqz v6, :cond_6c

    #@69
    .line 635
    :try_start_69
    invoke-virtual {v6}, Ljava/io/BufferedReader;->close()V

    #@6c
    .line 637
    :cond_6c
    if-eqz v5, :cond_71

    #@6e
    .line 638
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V
    :try_end_71
    .catch Ljava/lang/Exception; {:try_start_69 .. :try_end_71} :catch_89

    #@71
    .line 632
    :cond_71
    :goto_71
    throw v13

    #@72
    .line 651
    :cond_72
    move/from16 v9, p2

    #@74
    goto :goto_46

    #@75
    .line 653
    :catch_75
    move-exception v1

    #@76
    .line 654
    .local v1, e:Ljava/lang/Exception;
    move/from16 v9, p2

    #@78
    goto :goto_46

    #@79
    .line 630
    .end local v1           #e:Ljava/lang/Exception;
    :catch_79
    move-exception v13

    #@7a
    .line 634
    :goto_7a
    if-eqz v6, :cond_7f

    #@7c
    .line 635
    :try_start_7c
    invoke-virtual {v6}, Ljava/io/BufferedReader;->close()V

    #@7f
    .line 637
    :cond_7f
    if-eqz v5, :cond_40

    #@81
    .line 638
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V
    :try_end_84
    .catch Ljava/lang/Exception; {:try_start_7c .. :try_end_84} :catch_85

    #@84
    goto :goto_40

    #@85
    .line 642
    :catch_85
    move-exception v13

    #@86
    goto :goto_40

    #@87
    .end local v6           #inFile:Ljava/io/BufferedReader;
    .restart local v7       #inFile:Ljava/io/BufferedReader;
    .restart local v8       #line:Ljava/lang/String;
    :catch_87
    move-exception v13

    #@88
    goto :goto_3f

    #@89
    .end local v7           #inFile:Ljava/io/BufferedReader;
    .end local v8           #line:Ljava/lang/String;
    .restart local v6       #inFile:Ljava/io/BufferedReader;
    :catch_89
    move-exception v14

    #@8a
    goto :goto_71

    #@8b
    .line 632
    .end local v6           #inFile:Ljava/io/BufferedReader;
    .restart local v7       #inFile:Ljava/io/BufferedReader;
    :catchall_8b
    move-exception v13

    #@8c
    move-object v6, v7

    #@8d
    .end local v7           #inFile:Ljava/io/BufferedReader;
    .restart local v6       #inFile:Ljava/io/BufferedReader;
    goto :goto_67

    #@8e
    .line 630
    .end local v6           #inFile:Ljava/io/BufferedReader;
    .restart local v7       #inFile:Ljava/io/BufferedReader;
    :catch_8e
    move-exception v13

    #@8f
    move-object v6, v7

    #@90
    .end local v7           #inFile:Ljava/io/BufferedReader;
    .restart local v6       #inFile:Ljava/io/BufferedReader;
    goto :goto_7a
.end method

.method private readMipActiveProf()I
    .registers 5

    #@0
    .prologue
    .line 574
    const/4 v0, 0x1

    #@1
    .line 575
    .local v0, retVal:I
    const-string v1, "TetherNetwork"

    #@3
    new-instance v2, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v3, "readMipActiveProf get "

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 576
    return v0
.end method

.method private readMipErrorCode()I
    .registers 2

    #@0
    .prologue
    .line 581
    const/4 v0, 0x0

    #@1
    .line 597
    .local v0, mipError:I
    return v0
.end method

.method private registerTetherNetworkReceiver()V
    .registers 4

    #@0
    .prologue
    .line 161
    iget-object v1, p0, Lcom/android/server/connectivity/TetherNetwork;->mTetherNetwork:Landroid/content/BroadcastReceiver;

    #@2
    if-eqz v1, :cond_5

    #@4
    .line 176
    :goto_4
    return-void

    #@5
    .line 163
    :cond_5
    new-instance v1, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkReceiver;

    #@7
    const/4 v2, 0x0

    #@8
    invoke-direct {v1, p0, v2}, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkReceiver;-><init>(Lcom/android/server/connectivity/TetherNetwork;Lcom/android/server/connectivity/TetherNetwork$1;)V

    #@b
    iput-object v1, p0, Lcom/android/server/connectivity/TetherNetwork;->mTetherNetwork:Landroid/content/BroadcastReceiver;

    #@d
    .line 164
    new-instance v0, Landroid/content/IntentFilter;

    #@f
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@12
    .line 165
    .local v0, tetherNetworkFilter:Landroid/content/IntentFilter;
    const-string v1, "android.net.wifi.WIFI_AP_STATE_CHANGED"

    #@14
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@17
    .line 166
    const-string v1, "android.intent.action.ANY_DATA_STATE"

    #@19
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1c
    .line 168
    const-string v1, "com.lge.nai.Err.AuthFailed"

    #@1e
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@21
    .line 169
    const-string v1, "com.lge.nai.Err.Timeout"

    #@23
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@26
    .line 170
    const-string v1, "com.lge.nai.Noti.Success"

    #@28
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@2b
    .line 171
    const-string v1, "com.lge.nai.Noti.Failed"

    #@2d
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@30
    .line 174
    const-string v1, "com.lge.pamdisabled"

    #@32
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@35
    .line 175
    iget-object v1, p0, Lcom/android/server/connectivity/TetherNetwork;->mContext:Landroid/content/Context;

    #@37
    iget-object v2, p0, Lcom/android/server/connectivity/TetherNetwork;->mTetherNetwork:Landroid/content/BroadcastReceiver;

    #@39
    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@3c
    goto :goto_4
.end method

.method private removeTetherDevices(Landroid/content/Context;)V
    .registers 14
    .parameter "ctx"

    #@0
    .prologue
    const/4 v11, 0x0

    #@1
    .line 420
    const-string v8, "connectivity"

    #@3
    invoke-static {v8}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@6
    move-result-object v8

    #@7
    invoke-static {v8}, Landroid/net/IConnectivityManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/IConnectivityManager;

    #@a
    move-result-object v3

    #@b
    .line 422
    .local v3, mCm:Landroid/net/IConnectivityManager;
    if-nez v3, :cond_15

    #@d
    .line 424
    const-string v8, "TetherNetwork"

    #@f
    const-string v9, "[removeTetherDevices] mCm is null FATAL ERROR"

    #@11
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@14
    .line 512
    :cond_14
    :goto_14
    return-void

    #@15
    .line 429
    :cond_15
    :try_start_15
    invoke-interface {v3}, Landroid/net/IConnectivityManager;->getTetherableWifiRegexs()[Ljava/lang/String;

    #@18
    move-result-object v8

    #@19
    iput-object v8, p0, Lcom/android/server/connectivity/TetherNetwork;->mWifiRegexs:[Ljava/lang/String;
    :try_end_1b
    .catch Landroid/os/RemoteException; {:try_start_15 .. :try_end_1b} :catch_b0

    #@1b
    .line 435
    :goto_1b
    :try_start_1b
    invoke-interface {v3}, Landroid/net/IConnectivityManager;->getTetherableUsbRegexs()[Ljava/lang/String;

    #@1e
    move-result-object v8

    #@1f
    iput-object v8, p0, Lcom/android/server/connectivity/TetherNetwork;->mUsbRegexs:[Ljava/lang/String;
    :try_end_21
    .catch Landroid/os/RemoteException; {:try_start_1b .. :try_end_21} :catch_b6

    #@21
    .line 442
    :goto_21
    const-string v8, "SPR"

    #@23
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getOperator()Ljava/lang/String;

    #@26
    move-result-object v9

    #@27
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2a
    move-result v8

    #@2b
    if-eqz v8, :cond_33

    #@2d
    .line 444
    :try_start_2d
    invoke-interface {v3}, Landroid/net/IConnectivityManager;->getTetherableBluetoothRegexs()[Ljava/lang/String;

    #@30
    move-result-object v8

    #@31
    iput-object v8, p0, Lcom/android/server/connectivity/TetherNetwork;->mBTRegexs:[Ljava/lang/String;
    :try_end_33
    .catch Landroid/os/RemoteException; {:try_start_2d .. :try_end_33} :catch_bc

    #@33
    .line 452
    :cond_33
    :goto_33
    const/4 v5, 0x0

    #@34
    .line 454
    .local v5, tethered:[Ljava/lang/String;
    :try_start_34
    invoke-interface {v3}, Landroid/net/IConnectivityManager;->getTetheredIfaces()[Ljava/lang/String;
    :try_end_37
    .catch Landroid/os/RemoteException; {:try_start_34 .. :try_end_37} :catch_c2

    #@37
    move-result-object v5

    #@38
    .line 463
    iget-object v8, p0, Lcom/android/server/connectivity/TetherNetwork;->mUsbRegexs:[Ljava/lang/String;

    #@3a
    invoke-direct {p0, v5, v8}, Lcom/android/server/connectivity/TetherNetwork;->findIface([Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    #@3d
    move-result-object v6

    #@3e
    .line 464
    .local v6, usbIface:Ljava/lang/String;
    if-eqz v6, :cond_43

    #@40
    .line 467
    :try_start_40
    invoke-interface {v3, v6}, Landroid/net/IConnectivityManager;->untether(Ljava/lang/String;)I
    :try_end_43
    .catch Landroid/os/RemoteException; {:try_start_40 .. :try_end_43} :catch_c8

    #@43
    .line 473
    :cond_43
    :goto_43
    iget-object v8, p0, Lcom/android/server/connectivity/TetherNetwork;->mWifiRegexs:[Ljava/lang/String;

    #@45
    invoke-direct {p0, v5, v8}, Lcom/android/server/connectivity/TetherNetwork;->findIface([Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    #@48
    move-result-object v7

    #@49
    .line 475
    .local v7, wifiIface:Ljava/lang/String;
    if-eqz v7, :cond_57

    #@4b
    .line 477
    const-string v8, "wifi"

    #@4d
    invoke-virtual {p1, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@50
    move-result-object v4

    #@51
    check-cast v4, Landroid/net/wifi/WifiManager;

    #@53
    .line 478
    .local v4, mWifiManager:Landroid/net/wifi/WifiManager;
    const/4 v8, 0x0

    #@54
    invoke-virtual {v4, v8, v11}, Landroid/net/wifi/WifiManager;->setWifiApEnabled(Landroid/net/wifi/WifiConfiguration;Z)Z

    #@57
    .line 482
    .end local v4           #mWifiManager:Landroid/net/wifi/WifiManager;
    :cond_57
    const-string v8, "SPR"

    #@59
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getOperator()Ljava/lang/String;

    #@5c
    move-result-object v9

    #@5d
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@60
    move-result v8

    #@61
    if-eqz v8, :cond_14

    #@63
    .line 483
    iget-object v8, p0, Lcom/android/server/connectivity/TetherNetwork;->mBTRegexs:[Ljava/lang/String;

    #@65
    invoke-direct {p0, v5, v8}, Lcom/android/server/connectivity/TetherNetwork;->findIface([Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    #@68
    move-result-object v0

    #@69
    .line 485
    .local v0, BTIface:Ljava/lang/String;
    const-string v8, "TetherNetwork"

    #@6b
    new-instance v9, Ljava/lang/StringBuilder;

    #@6d
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@70
    const-string v10, "removeTetherDevices --> BTIfce : "

    #@72
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v9

    #@76
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v9

    #@7a
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7d
    move-result-object v9

    #@7e
    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@81
    .line 487
    if-eqz v0, :cond_14

    #@83
    .line 488
    const-string v8, "TetherNetwork"

    #@85
    const-string v9, "removeTetherDevices : BTIfce"

    #@87
    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@8a
    .line 490
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    #@8d
    move-result-object v1

    #@8e
    .line 492
    .local v1, adapter:Landroid/bluetooth/BluetoothAdapter;
    if-eqz v1, :cond_96

    #@90
    .line 493
    iget-object v8, p0, Lcom/android/server/connectivity/TetherNetwork;->mProfileServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    #@92
    const/4 v9, 0x5

    #@93
    invoke-virtual {v1, p1, v8, v9}, Landroid/bluetooth/BluetoothAdapter;->getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z

    #@96
    .line 497
    :cond_96
    iget-object v8, p0, Lcom/android/server/connectivity/TetherNetwork;->mBluetoothPan:Landroid/bluetooth/BluetoothPan;

    #@98
    if-eqz v8, :cond_14

    #@9a
    .line 499
    if-eqz v0, :cond_a9

    #@9c
    :try_start_9c
    invoke-interface {v3, v0}, Landroid/net/IConnectivityManager;->untether(Ljava/lang/String;)I

    #@9f
    move-result v8

    #@a0
    if-eqz v8, :cond_a9

    #@a2
    .line 501
    const-string v8, "TetherNetwork"

    #@a4
    const-string v9, "Untether initiate failed!"

    #@a6
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_a9
    .catch Landroid/os/RemoteException; {:try_start_9c .. :try_end_a9} :catch_ce

    #@a9
    .line 507
    :cond_a9
    :goto_a9
    iget-object v8, p0, Lcom/android/server/connectivity/TetherNetwork;->mBluetoothPan:Landroid/bluetooth/BluetoothPan;

    #@ab
    invoke-virtual {v8, v11}, Landroid/bluetooth/BluetoothPan;->setBluetoothTethering(Z)V

    #@ae
    goto/16 :goto_14

    #@b0
    .line 430
    .end local v0           #BTIface:Ljava/lang/String;
    .end local v1           #adapter:Landroid/bluetooth/BluetoothAdapter;
    .end local v5           #tethered:[Ljava/lang/String;
    .end local v6           #usbIface:Ljava/lang/String;
    .end local v7           #wifiIface:Ljava/lang/String;
    :catch_b0
    move-exception v2

    #@b1
    .line 432
    .local v2, e:Landroid/os/RemoteException;
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    #@b4
    goto/16 :goto_1b

    #@b6
    .line 436
    .end local v2           #e:Landroid/os/RemoteException;
    :catch_b6
    move-exception v2

    #@b7
    .line 438
    .restart local v2       #e:Landroid/os/RemoteException;
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    #@ba
    goto/16 :goto_21

    #@bc
    .line 445
    .end local v2           #e:Landroid/os/RemoteException;
    :catch_bc
    move-exception v2

    #@bd
    .line 447
    .restart local v2       #e:Landroid/os/RemoteException;
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    #@c0
    goto/16 :goto_33

    #@c2
    .line 455
    .end local v2           #e:Landroid/os/RemoteException;
    .restart local v5       #tethered:[Ljava/lang/String;
    :catch_c2
    move-exception v2

    #@c3
    .line 457
    .restart local v2       #e:Landroid/os/RemoteException;
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    #@c6
    goto/16 :goto_14

    #@c8
    .line 468
    .end local v2           #e:Landroid/os/RemoteException;
    .restart local v6       #usbIface:Ljava/lang/String;
    :catch_c8
    move-exception v2

    #@c9
    .line 470
    .restart local v2       #e:Landroid/os/RemoteException;
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    #@cc
    goto/16 :goto_43

    #@ce
    .line 503
    .end local v2           #e:Landroid/os/RemoteException;
    .restart local v0       #BTIface:Ljava/lang/String;
    .restart local v1       #adapter:Landroid/bluetooth/BluetoothAdapter;
    .restart local v7       #wifiIface:Ljava/lang/String;
    :catch_ce
    move-exception v2

    #@cf
    .line 505
    .restart local v2       #e:Landroid/os/RemoteException;
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    #@d2
    goto :goto_a9
.end method

.method private showDialog(Landroid/content/Context;I)V
    .registers 5
    .parameter "ctx"
    .parameter "index"

    #@0
    .prologue
    .line 527
    if-nez p2, :cond_10

    #@2
    .line 529
    new-instance v0, Landroid/content/Intent;

    #@4
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    #@7
    .line 530
    .local v0, itnt:Landroid/content/Intent;
    const-string v1, "lge.tethernetwork.action.NETWORK_ERROR_DIALOG"

    #@9
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@c
    .line 531
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@f
    .line 539
    .end local v0           #itnt:Landroid/content/Intent;
    :cond_f
    :goto_f
    return-void

    #@10
    .line 533
    :cond_10
    const/4 v1, 0x1

    #@11
    if-ne p2, v1, :cond_f

    #@13
    .line 535
    new-instance v0, Landroid/content/Intent;

    #@15
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    #@18
    .line 536
    .restart local v0       #itnt:Landroid/content/Intent;
    const-string v1, "lge.tethernetwork.action.NETWORK_REJECT_DIALOG"

    #@1a
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@1d
    .line 537
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@20
    goto :goto_f
.end method

.method private showToast(Landroid/content/Context;Ljava/lang/String;)V
    .registers 5
    .parameter "ctx"
    .parameter "msg"

    #@0
    .prologue
    .line 520
    new-instance v0, Landroid/content/Intent;

    #@2
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    #@5
    .line 521
    .local v0, itnt:Landroid/content/Intent;
    const-string v1, "lge.tethernetwork.action.NETWORK_ERROR_TOAST"

    #@7
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@a
    .line 522
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@d
    .line 523
    return-void
.end method

.method private turnOffWifi()V
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 774
    const/4 v1, 0x0

    #@2
    .line 775
    .local v1, mWifiManager:Landroid/net/wifi/WifiManager;
    iget-object v2, p0, Lcom/android/server/connectivity/TetherNetwork;->mContext:Landroid/content/Context;

    #@4
    if-eqz v2, :cond_10

    #@6
    .line 776
    iget-object v2, p0, Lcom/android/server/connectivity/TetherNetwork;->mContext:Landroid/content/Context;

    #@8
    const-string v3, "wifi"

    #@a
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@d
    move-result-object v1

    #@e
    .end local v1           #mWifiManager:Landroid/net/wifi/WifiManager;
    check-cast v1, Landroid/net/wifi/WifiManager;

    #@10
    .line 778
    .restart local v1       #mWifiManager:Landroid/net/wifi/WifiManager;
    :cond_10
    if-eqz v1, :cond_48

    #@12
    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getWifiState()I

    #@15
    move-result v2

    #@16
    if-eq v2, v5, :cond_48

    #@18
    .line 780
    const/4 v2, 0x0

    #@19
    invoke-virtual {v1, v2}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    #@1c
    .line 782
    const/4 v0, 0x0

    #@1d
    .local v0, i:I
    :goto_1d
    const/4 v2, 0x6

    #@1e
    if-ge v0, v2, :cond_48

    #@20
    .line 784
    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getWifiState()I

    #@23
    move-result v2

    #@24
    if-eq v2, v5, :cond_48

    #@26
    .line 787
    const-wide/16 v2, 0x1f4

    #@28
    :try_start_28
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2b
    .catch Ljava/lang/InterruptedException; {:try_start_28 .. :try_end_2b} :catch_46

    #@2b
    .line 790
    :goto_2b
    const-string v2, "TetherNetwork"

    #@2d
    new-instance v3, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    const-string v4, "turnOffWifi time : "

    #@34
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v3

    #@38
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v3

    #@3c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v3

    #@40
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@43
    .line 782
    add-int/lit8 v0, v0, 0x1

    #@45
    goto :goto_1d

    #@46
    .line 788
    :catch_46
    move-exception v2

    #@47
    goto :goto_2b

    #@48
    .line 797
    .end local v0           #i:I
    :cond_48
    return-void
.end method

.method private unregisterTetherNetworkReceiver()V
    .registers 3

    #@0
    .prologue
    .line 179
    iget-object v0, p0, Lcom/android/server/connectivity/TetherNetwork;->mTetherNetwork:Landroid/content/BroadcastReceiver;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 180
    iget-object v0, p0, Lcom/android/server/connectivity/TetherNetwork;->mContext:Landroid/content/Context;

    #@6
    iget-object v1, p0, Lcom/android/server/connectivity/TetherNetwork;->mTetherNetwork:Landroid/content/BroadcastReceiver;

    #@8
    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@b
    .line 182
    :cond_b
    const/4 v0, 0x0

    #@c
    iput-object v0, p0, Lcom/android/server/connectivity/TetherNetwork;->mTetherNetwork:Landroid/content/BroadcastReceiver;

    #@e
    .line 183
    return-void
.end method


# virtual methods
.method public disableTetherNetwork()Z
    .registers 7

    #@0
    .prologue
    const/16 v5, 0x3f3

    #@2
    const/4 v1, 0x1

    #@3
    const/4 v2, 0x0

    #@4
    .line 247
    iget-object v3, p0, Lcom/android/server/connectivity/TetherNetwork;->mContext:Landroid/content/Context;

    #@6
    const-string v4, "connectivity"

    #@8
    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@b
    move-result-object v0

    #@c
    check-cast v0, Landroid/net/ConnectivityManager;

    #@e
    .line 250
    .local v0, cm:Landroid/net/ConnectivityManager;
    sget v3, Lcom/android/server/connectivity/TetherNetwork;->mTetherNetworkStatus:I

    #@10
    if-nez v3, :cond_3c

    #@12
    .line 251
    const-string v2, "TetherNetwork"

    #@14
    new-instance v3, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v4, "disableTetherNetwork already or did not nai changed <<<<<<<<<<<<<<<<<<<< status : "

    #@1b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v3

    #@1f
    sget v4, Lcom/android/server/connectivity/TetherNetwork;->mTetherNetworkStatus:I

    #@21
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@24
    move-result-object v3

    #@25
    const-string v4, "devices : "

    #@27
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getTetheredIfaces()[Ljava/lang/String;

    #@2e
    move-result-object v4

    #@2f
    array-length v4, v4

    #@30
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@33
    move-result-object v3

    #@34
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v3

    #@38
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 268
    :goto_3b
    return v1

    #@3c
    .line 255
    :cond_3c
    if-eqz v0, :cond_4d

    #@3e
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getTetheredIfaces()[Ljava/lang/String;

    #@41
    move-result-object v3

    #@42
    array-length v3, v3

    #@43
    if-lez v3, :cond_4d

    #@45
    .line 256
    const-string v2, "TetherNetwork"

    #@47
    const-string v3, "disableTetherNetwork other interface is running>>>>>>>>>>>>>>>>"

    #@49
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@4c
    goto :goto_3b

    #@4d
    .line 260
    :cond_4d
    invoke-direct {p0}, Lcom/android/server/connectivity/TetherNetwork;->unregisterTetherNetworkReceiver()V

    #@50
    .line 261
    const-string v1, "TetherNetwork"

    #@52
    const-string v3, "disableTetherNetwork return to mobile network<<<<<<<<<<<<<<<<<<<<"

    #@54
    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@57
    .line 264
    sget-object v1, Lcom/android/server/connectivity/TetherNetwork;->mTetherNetworkHandler:Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;

    #@59
    const/16 v3, 0x3f2

    #@5b
    invoke-virtual {v1, v3}, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;->removeMessages(I)V

    #@5e
    .line 265
    sget-object v1, Lcom/android/server/connectivity/TetherNetwork;->mTetherNetworkHandler:Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;

    #@60
    invoke-virtual {v1, v5}, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;->removeMessages(I)V

    #@63
    .line 266
    sget-object v1, Lcom/android/server/connectivity/TetherNetwork;->mTetherNetworkHandler:Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;

    #@65
    const-wide/16 v3, 0x0

    #@67
    invoke-virtual {v1, v5, v3, v4}, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;->sendEmptyMessageDelayed(IJ)Z

    #@6a
    .line 267
    sput v2, Lcom/android/server/connectivity/TetherNetwork;->mTetherNetworkStatus:I

    #@6c
    move v1, v2

    #@6d
    .line 268
    goto :goto_3b
.end method

.method public enableTetherNetwork()Z
    .registers 9

    #@0
    .prologue
    const/16 v7, 0x3f2

    #@2
    const/4 v5, 0x3

    #@3
    const/4 v6, 0x1

    #@4
    .line 188
    const-string v3, "TetherNetwork"

    #@6
    const-string v4, "enableTetherNetwork called >>>>>>>>>>>>>>"

    #@8
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 190
    sget v3, Lcom/android/server/connectivity/TetherNetwork;->mTetherNetworkStatus:I

    #@d
    if-ne v3, v6, :cond_17

    #@f
    .line 191
    const-string v3, "TetherNetwork"

    #@11
    const-string v4, "enableTetherNetwork already started>>>>>>>>>>>>>>>>"

    #@13
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 241
    :goto_16
    return v6

    #@17
    .line 195
    :cond_17
    iget-object v3, p0, Lcom/android/server/connectivity/TetherNetwork;->mContext:Landroid/content/Context;

    #@19
    const-string v4, "connectivity"

    #@1b
    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@1e
    move-result-object v0

    #@1f
    check-cast v0, Landroid/net/ConnectivityManager;

    #@21
    .line 199
    .local v0, cm:Landroid/net/ConnectivityManager;
    if-eqz v0, :cond_48

    #@23
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getTetheredIfaces()[Ljava/lang/String;

    #@26
    move-result-object v3

    #@27
    array-length v3, v3

    #@28
    if-le v3, v6, :cond_48

    #@2a
    .line 200
    const-string v3, "TetherNetwork"

    #@2c
    new-instance v4, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v5, "enableTetherNetwork already started>>>>>>>>>>>>>>>> devices : "

    #@33
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v4

    #@37
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getTetheredIfaces()[Ljava/lang/String;

    #@3a
    move-result-object v5

    #@3b
    array-length v5, v5

    #@3c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v4

    #@40
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v4

    #@44
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@47
    goto :goto_16

    #@48
    .line 206
    :cond_48
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getCountry()Ljava/lang/String;

    #@4b
    move-result-object v3

    #@4c
    const-string v4, "US"

    #@4e
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@51
    move-result v3

    #@52
    if-eqz v3, :cond_6e

    #@54
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getOperator()Ljava/lang/String;

    #@57
    move-result-object v3

    #@58
    const-string v4, "ACG"

    #@5a
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5d
    move-result v3

    #@5e
    if-eqz v3, :cond_6e

    #@60
    .line 209
    invoke-virtual {p0}, Lcom/android/server/connectivity/TetherNetwork;->getTetherNaiChangeEnabled()Z

    #@63
    move-result v3

    #@64
    if-nez v3, :cond_6e

    #@66
    .line 210
    const-string v3, "TetherNetwork"

    #@68
    const-string v4, "enableTetherNetwork ,not use mobile nai"

    #@6a
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@6d
    goto :goto_16

    #@6e
    .line 217
    :cond_6e
    iget-boolean v3, p0, Lcom/android/server/connectivity/TetherNetwork;->mLineNumberEnabled:Z

    #@70
    if-nez v3, :cond_9f

    #@72
    .line 218
    const/4 v1, 0x0

    #@73
    .line 219
    .local v1, phoneNumber:Ljava/lang/String;
    iget-object v3, p0, Lcom/android/server/connectivity/TetherNetwork;->mContext:Landroid/content/Context;

    #@75
    const-string v4, "phone"

    #@77
    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@7a
    move-result-object v2

    #@7b
    check-cast v2, Landroid/telephony/TelephonyManager;

    #@7d
    .line 220
    .local v2, tm:Landroid/telephony/TelephonyManager;
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    #@80
    move-result-object v1

    #@81
    .line 222
    if-eqz v1, :cond_c0

    #@83
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@86
    move-result v3

    #@87
    if-lt v3, v5, :cond_c0

    #@89
    const-string v3, "000"

    #@8b
    const/4 v4, 0x0

    #@8c
    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@8f
    move-result-object v4

    #@90
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@93
    move-result v3

    #@94
    if-nez v3, :cond_c0

    #@96
    .line 224
    const-string v3, "TetherNetwork"

    #@98
    const-string v4, "Phone is activated and start"

    #@9a
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@9d
    .line 225
    iput-boolean v6, p0, Lcom/android/server/connectivity/TetherNetwork;->mLineNumberEnabled:Z

    #@9f
    .line 232
    .end local v1           #phoneNumber:Ljava/lang/String;
    .end local v2           #tm:Landroid/telephony/TelephonyManager;
    :cond_9f
    invoke-direct {p0}, Lcom/android/server/connectivity/TetherNetwork;->registerTetherNetworkReceiver()V

    #@a2
    .line 233
    const-string v3, "TetherNetwork"

    #@a4
    const-string v4, "enableTetherNetwork tether network start >>>>>>>>>>>>>>>>"

    #@a6
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@a9
    .line 236
    sget-object v3, Lcom/android/server/connectivity/TetherNetwork;->mTetherNetworkHandler:Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;

    #@ab
    invoke-virtual {v3, v7}, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;->removeMessages(I)V

    #@ae
    .line 237
    sget-object v3, Lcom/android/server/connectivity/TetherNetwork;->mTetherNetworkHandler:Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;

    #@b0
    const/16 v4, 0x3f3

    #@b2
    invoke-virtual {v3, v4}, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;->removeMessages(I)V

    #@b5
    .line 238
    sget-object v3, Lcom/android/server/connectivity/TetherNetwork;->mTetherNetworkHandler:Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;

    #@b7
    const-wide/16 v4, 0x0

    #@b9
    invoke-virtual {v3, v7, v4, v5}, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;->sendEmptyMessageDelayed(IJ)Z

    #@bc
    .line 239
    sput v6, Lcom/android/server/connectivity/TetherNetwork;->mTetherNetworkStatus:I

    #@be
    goto/16 :goto_16

    #@c0
    .line 227
    .restart local v1       #phoneNumber:Ljava/lang/String;
    .restart local v2       #tm:Landroid/telephony/TelephonyManager;
    :cond_c0
    const-string v3, "TetherNetwork"

    #@c2
    const-string v4, "Phone is not activated tether enabling"

    #@c4
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@c7
    goto/16 :goto_16
.end method

.method public getHotspotFeatureEnabled()I
    .registers 2

    #@0
    .prologue
    .line 716
    const/4 v0, -0x1

    #@1
    return v0
.end method

.method public getTetherNaiChangeEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 768
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public isAllowedTetherableInterface(ZZZ)Z
    .registers 13
    .parameter "usb"
    .parameter "hotspot"
    .parameter "other"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v8, -0x1

    #@2
    const/4 v3, 0x1

    #@3
    .line 277
    new-instance v0, Ljava/lang/String;

    #@5
    const-string v5, "/carrier/data/td"

    #@7
    invoke-direct {v0, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    #@a
    .line 278
    .local v0, cmln_data_tether:Ljava/lang/String;
    const-string v5, "SPR"

    #@c
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getOperator()Ljava/lang/String;

    #@f
    move-result-object v6

    #@10
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@13
    move-result v5

    #@14
    if-eqz v5, :cond_43

    #@16
    const/4 v1, 0x3

    #@17
    .line 281
    .local v1, ref_value:I
    :goto_17
    const-string v5, "SPR"

    #@19
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getOperator()Ljava/lang/String;

    #@1c
    move-result-object v6

    #@1d
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@20
    move-result v5

    #@21
    if-eqz v5, :cond_32

    #@23
    const-string v5, "fx1"

    #@25
    const-string v6, "ro.product.device"

    #@27
    invoke-static {v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@2a
    move-result-object v6

    #@2b
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2e
    move-result v5

    #@2f
    if-eqz v5, :cond_32

    #@31
    .line 282
    const/4 v1, 0x1

    #@32
    .line 286
    :cond_32
    if-eqz p2, :cond_45

    #@34
    .line 287
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->isWifiChameleonFeaturedCarrier()Z

    #@37
    move-result v5

    #@38
    if-eqz v5, :cond_45

    #@3a
    .line 291
    invoke-direct {p0, v0, v1}, Lcom/android/server/connectivity/TetherNetwork;->readChameleonIntValue(Ljava/lang/String;I)I

    #@3d
    move-result v2

    #@3e
    .line 293
    .local v2, tetherMode:I
    const/4 v5, 0x2

    #@3f
    if-ge v2, v5, :cond_42

    #@41
    move v3, v4

    #@42
    .line 319
    .end local v2           #tetherMode:I
    :cond_42
    :goto_42
    return v3

    #@43
    .end local v1           #ref_value:I
    :cond_43
    move v1, v3

    #@44
    .line 278
    goto :goto_17

    #@45
    .line 305
    .restart local v1       #ref_value:I
    :cond_45
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getCountry()Ljava/lang/String;

    #@48
    move-result-object v5

    #@49
    const-string v6, "US"

    #@4b
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4e
    move-result v5

    #@4f
    if-eqz v5, :cond_42

    #@51
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getOperator()Ljava/lang/String;

    #@54
    move-result-object v5

    #@55
    const-string v6, "ACG"

    #@57
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5a
    move-result v5

    #@5b
    if-eqz v5, :cond_42

    #@5d
    .line 308
    if-eqz p2, :cond_42

    #@5f
    .line 309
    const-string v5, "TetherNetwork"

    #@61
    new-instance v6, Ljava/lang/StringBuilder;

    #@63
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@66
    const-string v7, "TetherNetwork.isAllowedTetherableInterface  : mHotspotFeatureEnabled =  "

    #@68
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v6

    #@6c
    iget v7, p0, Lcom/android/server/connectivity/TetherNetwork;->mHotspotFeatureEnabled:I

    #@6e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@71
    move-result-object v6

    #@72
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@75
    move-result-object v6

    #@76
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@79
    .line 310
    iget v5, p0, Lcom/android/server/connectivity/TetherNetwork;->mHotspotFeatureEnabled:I

    #@7b
    if-ne v5, v8, :cond_83

    #@7d
    .line 311
    invoke-virtual {p0}, Lcom/android/server/connectivity/TetherNetwork;->getHotspotFeatureEnabled()I

    #@80
    move-result v5

    #@81
    iput v5, p0, Lcom/android/server/connectivity/TetherNetwork;->mHotspotFeatureEnabled:I

    #@83
    .line 313
    :cond_83
    iget v5, p0, Lcom/android/server/connectivity/TetherNetwork;->mHotspotFeatureEnabled:I

    #@85
    if-eq v5, v8, :cond_42

    #@87
    .line 314
    iget v5, p0, Lcom/android/server/connectivity/TetherNetwork;->mHotspotFeatureEnabled:I

    #@89
    if-nez v5, :cond_42

    #@8b
    move v3, v4

    #@8c
    goto :goto_42
.end method

.method public isAllowedTetherableInterface(ZZZZ)Z
    .registers 6
    .parameter "usb"
    .parameter "hotspot"
    .parameter "bt"
    .parameter "other"

    #@0
    .prologue
    .line 325
    if-nez p3, :cond_4

    #@2
    if-eqz p4, :cond_a

    #@4
    :cond_4
    const/4 v0, 0x1

    #@5
    :goto_5
    invoke-virtual {p0, p1, p2, v0}, Lcom/android/server/connectivity/TetherNetwork;->isAllowedTetherableInterface(ZZZ)Z

    #@8
    move-result v0

    #@9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_5
.end method

.method public isTetherNetworkAvail()Z
    .registers 2

    #@0
    .prologue
    .line 329
    iget-object v0, p0, Lcom/android/server/connectivity/TetherNetwork;->mTetherNetworkDataTrans:Lcom/android/server/connectivity/TetherNetworkDataTransition;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 330
    iget-object v0, p0, Lcom/android/server/connectivity/TetherNetwork;->mTetherNetworkDataTrans:Lcom/android/server/connectivity/TetherNetworkDataTransition;

    #@6
    invoke-virtual {v0}, Lcom/android/server/connectivity/TetherNetworkDataTransition;->isPamPdnDataProfileEnabled()Z

    #@9
    move-result v0

    #@a
    .line 332
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x1

    #@c
    goto :goto_a
.end method

.method public showAuthErrorDialog()V
    .registers 3

    #@0
    .prologue
    .line 543
    iget-object v0, p0, Lcom/android/server/connectivity/TetherNetwork;->mContext:Landroid/content/Context;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-direct {p0, v0, v1}, Lcom/android/server/connectivity/TetherNetwork;->showDialog(Landroid/content/Context;I)V

    #@6
    .line 544
    return-void
.end method
