.class Lcom/android/server/connectivity/Vpn$2;
.super Lcom/android/server/net/BaseNetworkObserver;
.source "Vpn.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/connectivity/Vpn;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/connectivity/Vpn;


# direct methods
.method constructor <init>(Lcom/android/server/connectivity/Vpn;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 752
    iput-object p1, p0, Lcom/android/server/connectivity/Vpn$2;->this$0:Lcom/android/server/connectivity/Vpn;

    #@2
    invoke-direct {p0}, Lcom/android/server/net/BaseNetworkObserver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public interfaceRemoved(Ljava/lang/String;)V
    .registers 8
    .parameter "interfaze"

    #@0
    .prologue
    .line 764
    iget-object v3, p0, Lcom/android/server/connectivity/Vpn$2;->this$0:Lcom/android/server/connectivity/Vpn;

    #@2
    monitor-enter v3

    #@3
    .line 765
    :try_start_3
    iget-object v2, p0, Lcom/android/server/connectivity/Vpn$2;->this$0:Lcom/android/server/connectivity/Vpn;

    #@5
    invoke-static {v2}, Lcom/android/server/connectivity/Vpn;->access$400(Lcom/android/server/connectivity/Vpn;)Ljava/lang/String;

    #@8
    move-result-object v2

    #@9
    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c
    move-result v2

    #@d
    if-eqz v2, :cond_77

    #@f
    iget-object v2, p0, Lcom/android/server/connectivity/Vpn$2;->this$0:Lcom/android/server/connectivity/Vpn;

    #@11
    invoke-static {v2, p1}, Lcom/android/server/connectivity/Vpn;->access$500(Lcom/android/server/connectivity/Vpn;Ljava/lang/String;)I

    #@14
    move-result v2

    #@15
    if-nez v2, :cond_77

    #@17
    .line 766
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_1a
    .catchall {:try_start_3 .. :try_end_1a} :catchall_88

    #@1a
    move-result-wide v0

    #@1b
    .line 769
    .local v0, token:J
    :try_start_1b
    const-string v2, "ro.lge.b2b.vmware"

    #@1d
    const/4 v4, 0x0

    #@1e
    invoke-static {v2, v4}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@21
    move-result v2

    #@22
    if-eqz v2, :cond_79

    #@24
    .line 770
    iget-object v2, p0, Lcom/android/server/connectivity/Vpn$2;->this$0:Lcom/android/server/connectivity/Vpn;

    #@26
    invoke-static {v2}, Lcom/android/server/connectivity/Vpn;->access$600(Lcom/android/server/connectivity/Vpn;)Z

    #@29
    move-result v2

    #@2a
    if-nez v2, :cond_35

    #@2c
    .line 771
    iget-object v2, p0, Lcom/android/server/connectivity/Vpn$2;->this$0:Lcom/android/server/connectivity/Vpn;

    #@2e
    invoke-static {v2}, Lcom/android/server/connectivity/Vpn;->access$700(Lcom/android/server/connectivity/Vpn;)Lcom/android/server/ConnectivityService$VpnCallback;

    #@31
    move-result-object v2

    #@32
    invoke-virtual {v2}, Lcom/android/server/ConnectivityService$VpnCallback;->restore()V

    #@35
    .line 778
    :cond_35
    :goto_35
    iget-object v2, p0, Lcom/android/server/connectivity/Vpn$2;->this$0:Lcom/android/server/connectivity/Vpn;

    #@37
    invoke-static {v2}, Lcom/android/server/connectivity/Vpn;->access$800(Lcom/android/server/connectivity/Vpn;)V
    :try_end_3a
    .catchall {:try_start_1b .. :try_end_3a} :catchall_83

    #@3a
    .line 780
    :try_start_3a
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@3d
    .line 782
    iget-object v2, p0, Lcom/android/server/connectivity/Vpn$2;->this$0:Lcom/android/server/connectivity/Vpn;

    #@3f
    const/4 v4, 0x0

    #@40
    invoke-static {v2, v4}, Lcom/android/server/connectivity/Vpn;->access$402(Lcom/android/server/connectivity/Vpn;Ljava/lang/String;)Ljava/lang/String;

    #@43
    .line 783
    iget-object v2, p0, Lcom/android/server/connectivity/Vpn$2;->this$0:Lcom/android/server/connectivity/Vpn;

    #@45
    invoke-static {v2}, Lcom/android/server/connectivity/Vpn;->access$900(Lcom/android/server/connectivity/Vpn;)Lcom/android/server/connectivity/Vpn$Connection;

    #@48
    move-result-object v2

    #@49
    if-eqz v2, :cond_8b

    #@4b
    .line 784
    iget-object v2, p0, Lcom/android/server/connectivity/Vpn$2;->this$0:Lcom/android/server/connectivity/Vpn;

    #@4d
    invoke-static {v2}, Lcom/android/server/connectivity/Vpn;->access$1000(Lcom/android/server/connectivity/Vpn;)Landroid/content/Context;

    #@50
    move-result-object v2

    #@51
    iget-object v4, p0, Lcom/android/server/connectivity/Vpn$2;->this$0:Lcom/android/server/connectivity/Vpn;

    #@53
    invoke-static {v4}, Lcom/android/server/connectivity/Vpn;->access$900(Lcom/android/server/connectivity/Vpn;)Lcom/android/server/connectivity/Vpn$Connection;

    #@56
    move-result-object v4

    #@57
    invoke-virtual {v2, v4}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    #@5a
    .line 785
    iget-object v2, p0, Lcom/android/server/connectivity/Vpn$2;->this$0:Lcom/android/server/connectivity/Vpn;

    #@5c
    const/4 v4, 0x0

    #@5d
    invoke-static {v2, v4}, Lcom/android/server/connectivity/Vpn;->access$902(Lcom/android/server/connectivity/Vpn;Lcom/android/server/connectivity/Vpn$Connection;)Lcom/android/server/connectivity/Vpn$Connection;

    #@60
    .line 786
    iget-object v2, p0, Lcom/android/server/connectivity/Vpn$2;->this$0:Lcom/android/server/connectivity/Vpn;

    #@62
    sget-object v4, Landroid/net/NetworkInfo$DetailedState;->DISCONNECTED:Landroid/net/NetworkInfo$DetailedState;

    #@64
    const-string v5, "interfaceRemoved"

    #@66
    invoke-static {v2, v4, v5}, Lcom/android/server/connectivity/Vpn;->access$1100(Lcom/android/server/connectivity/Vpn;Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;)V

    #@69
    .line 792
    :cond_69
    :goto_69
    const-string v2, "ro.lge.b2b.vmware"

    #@6b
    const/4 v4, 0x0

    #@6c
    invoke-static {v2, v4}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@6f
    move-result v2

    #@70
    if-eqz v2, :cond_77

    #@72
    .line 793
    iget-object v2, p0, Lcom/android/server/connectivity/Vpn$2;->this$0:Lcom/android/server/connectivity/Vpn;

    #@74
    invoke-static {v2}, Lcom/android/server/connectivity/Vpn;->access$1200(Lcom/android/server/connectivity/Vpn;)V

    #@77
    .line 797
    .end local v0           #token:J
    :cond_77
    monitor-exit v3
    :try_end_78
    .catchall {:try_start_3a .. :try_end_78} :catchall_88

    #@78
    .line 798
    return-void

    #@79
    .line 776
    .restart local v0       #token:J
    :cond_79
    :try_start_79
    iget-object v2, p0, Lcom/android/server/connectivity/Vpn$2;->this$0:Lcom/android/server/connectivity/Vpn;

    #@7b
    invoke-static {v2}, Lcom/android/server/connectivity/Vpn;->access$700(Lcom/android/server/connectivity/Vpn;)Lcom/android/server/ConnectivityService$VpnCallback;

    #@7e
    move-result-object v2

    #@7f
    invoke-virtual {v2}, Lcom/android/server/ConnectivityService$VpnCallback;->restore()V
    :try_end_82
    .catchall {:try_start_79 .. :try_end_82} :catchall_83

    #@82
    goto :goto_35

    #@83
    .line 780
    :catchall_83
    move-exception v2

    #@84
    :try_start_84
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@87
    throw v2

    #@88
    .line 797
    .end local v0           #token:J
    :catchall_88
    move-exception v2

    #@89
    monitor-exit v3
    :try_end_8a
    .catchall {:try_start_84 .. :try_end_8a} :catchall_88

    #@8a
    throw v2

    #@8b
    .line 787
    .restart local v0       #token:J
    :cond_8b
    :try_start_8b
    iget-object v2, p0, Lcom/android/server/connectivity/Vpn$2;->this$0:Lcom/android/server/connectivity/Vpn;

    #@8d
    invoke-static {v2}, Lcom/android/server/connectivity/Vpn;->access$300(Lcom/android/server/connectivity/Vpn;)Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;

    #@90
    move-result-object v2

    #@91
    if-eqz v2, :cond_69

    #@93
    .line 788
    iget-object v2, p0, Lcom/android/server/connectivity/Vpn$2;->this$0:Lcom/android/server/connectivity/Vpn;

    #@95
    invoke-static {v2}, Lcom/android/server/connectivity/Vpn;->access$300(Lcom/android/server/connectivity/Vpn;)Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;

    #@98
    move-result-object v2

    #@99
    invoke-virtual {v2}, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->exit()V

    #@9c
    .line 789
    iget-object v2, p0, Lcom/android/server/connectivity/Vpn$2;->this$0:Lcom/android/server/connectivity/Vpn;

    #@9e
    const/4 v4, 0x0

    #@9f
    invoke-static {v2, v4}, Lcom/android/server/connectivity/Vpn;->access$302(Lcom/android/server/connectivity/Vpn;Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;)Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;
    :try_end_a2
    .catchall {:try_start_8b .. :try_end_a2} :catchall_88

    #@a2
    goto :goto_69
.end method

.method public interfaceStatusChanged(Ljava/lang/String;Z)V
    .registers 5
    .parameter "interfaze"
    .parameter "up"

    #@0
    .prologue
    .line 755
    iget-object v1, p0, Lcom/android/server/connectivity/Vpn$2;->this$0:Lcom/android/server/connectivity/Vpn;

    #@2
    monitor-enter v1

    #@3
    .line 756
    if-nez p2, :cond_16

    #@5
    :try_start_5
    iget-object v0, p0, Lcom/android/server/connectivity/Vpn$2;->this$0:Lcom/android/server/connectivity/Vpn;

    #@7
    invoke-static {v0}, Lcom/android/server/connectivity/Vpn;->access$300(Lcom/android/server/connectivity/Vpn;)Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;

    #@a
    move-result-object v0

    #@b
    if-eqz v0, :cond_16

    #@d
    .line 757
    iget-object v0, p0, Lcom/android/server/connectivity/Vpn$2;->this$0:Lcom/android/server/connectivity/Vpn;

    #@f
    invoke-static {v0}, Lcom/android/server/connectivity/Vpn;->access$300(Lcom/android/server/connectivity/Vpn;)Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {v0, p1}, Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;->check(Ljava/lang/String;)V

    #@16
    .line 759
    :cond_16
    monitor-exit v1

    #@17
    .line 760
    return-void

    #@18
    .line 759
    :catchall_18
    move-exception v0

    #@19
    monitor-exit v1
    :try_end_1a
    .catchall {:try_start_5 .. :try_end_1a} :catchall_18

    #@1a
    throw v0
.end method
