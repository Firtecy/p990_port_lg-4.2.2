.class Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherModeAliveState;
.super Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;
.source "Tethering.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/connectivity/Tethering$TetherMasterSM;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "TetherModeAliveState"
.end annotation


# instance fields
.field mTryCell:Z

.field final synthetic this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;


# direct methods
.method constructor <init>(Lcom/android/server/connectivity/Tethering$TetherMasterSM;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 2579
    iput-object p1, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherModeAliveState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@2
    invoke-direct {p0, p1}, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;-><init>(Lcom/android/server/connectivity/Tethering$TetherMasterSM;)V

    #@5
    .line 2580
    const/4 v0, 0x1

    #@6
    iput-boolean v0, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherModeAliveState;->mTryCell:Z

    #@8
    return-void
.end method


# virtual methods
.method public enter()V
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 2583
    invoke-virtual {p0}, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherModeAliveState;->turnOnMasterTetherSettings()Z

    #@5
    .line 2585
    iput-boolean v0, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherModeAliveState;->mTryCell:Z

    #@7
    .line 2587
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherModeAliveState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@9
    iput v1, v2, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->mRetryCount:I

    #@b
    .line 2588
    iget-boolean v2, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherModeAliveState;->mTryCell:Z

    #@d
    invoke-virtual {p0, v2}, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherModeAliveState;->chooseUpstreamType(Z)V

    #@10
    .line 2589
    iget-boolean v2, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherModeAliveState;->mTryCell:Z

    #@12
    if-nez v2, :cond_17

    #@14
    :goto_14
    iput-boolean v0, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherModeAliveState;->mTryCell:Z

    #@16
    .line 2590
    return-void

    #@17
    :cond_17
    move v0, v1

    #@18
    .line 2589
    goto :goto_14
.end method

.method public exit()V
    .registers 2

    #@0
    .prologue
    .line 2593
    invoke-virtual {p0}, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherModeAliveState;->turnOffUpstreamMobileConnection()Z

    #@3
    .line 2594
    const/4 v0, 0x0

    #@4
    invoke-virtual {p0, v0}, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherModeAliveState;->notifyTetheredOfNewUpstreamIface(Ljava/lang/String;)V

    #@7
    .line 2595
    return-void
.end method

.method public processMessage(Landroid/os/Message;)Z
    .registers 19
    .parameter "message"

    #@0
    .prologue
    .line 2598
    const-string v14, "Tethering"

    #@2
    new-instance v15, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v16, "TetherModeAliveState.processMessage what="

    #@9
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v15

    #@d
    move-object/from16 v0, p1

    #@f
    iget v0, v0, Landroid/os/Message;->what:I

    #@11
    move/from16 v16, v0

    #@13
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v15

    #@17
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v15

    #@1b
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 2599
    const/4 v10, 0x1

    #@1f
    .line 2600
    .local v10, retValue:Z
    move-object/from16 v0, p1

    #@21
    iget v14, v0, Landroid/os/Message;->what:I

    #@23
    packed-switch v14, :pswitch_data_21e

    #@26
    .line 2697
    const/4 v10, 0x0

    #@27
    .line 2700
    :cond_27
    :goto_27
    return v10

    #@28
    .line 2602
    :pswitch_28
    move-object/from16 v0, p1

    #@2a
    iget-object v12, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2c
    check-cast v12, Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;

    #@2e
    .line 2603
    .local v12, who:Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;
    const-string v14, "Tethering"

    #@30
    new-instance v15, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    const-string v16, "Tether Mode requested by "

    #@37
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v15

    #@3b
    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v15

    #@3f
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v15

    #@43
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@46
    .line 2604
    move-object/from16 v0, p0

    #@48
    iget-object v14, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherModeAliveState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@4a
    invoke-static {v14}, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->access$6900(Lcom/android/server/connectivity/Tethering$TetherMasterSM;)Ljava/util/ArrayList;

    #@4d
    move-result-object v14

    #@4e
    invoke-virtual {v14, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@51
    .line 2605
    const/16 v14, 0xc

    #@53
    move-object/from16 v0, p0

    #@55
    iget-object v15, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherModeAliveState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@57
    invoke-static {v15}, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->access$6800(Lcom/android/server/connectivity/Tethering$TetherMasterSM;)Ljava/lang/String;

    #@5a
    move-result-object v15

    #@5b
    invoke-virtual {v12, v14, v15}, Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;->sendMessage(ILjava/lang/Object;)V

    #@5e
    goto :goto_27

    #@5f
    .line 2609
    .end local v12           #who:Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;
    :pswitch_5f
    move-object/from16 v0, p1

    #@61
    iget-object v12, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@63
    check-cast v12, Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;

    #@65
    .line 2610
    .restart local v12       #who:Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;
    const-string v14, "Tethering"

    #@67
    new-instance v15, Ljava/lang/StringBuilder;

    #@69
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@6c
    const-string v16, "Tether Mode unrequested by "

    #@6e
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v15

    #@72
    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v15

    #@76
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@79
    move-result-object v15

    #@7a
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7d
    .line 2611
    move-object/from16 v0, p0

    #@7f
    iget-object v14, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherModeAliveState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@81
    invoke-static {v14}, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->access$6900(Lcom/android/server/connectivity/Tethering$TetherMasterSM;)Ljava/util/ArrayList;

    #@84
    move-result-object v14

    #@85
    invoke-virtual {v14, v12}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    #@88
    move-result v6

    #@89
    .line 2612
    .local v6, index:I
    const/4 v14, -0x1

    #@8a
    if-eq v6, v14, :cond_11f

    #@8c
    .line 2613
    const-string v14, "Tethering"

    #@8e
    new-instance v15, Ljava/lang/StringBuilder;

    #@90
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@93
    const-string v16, "TetherModeAlive removing notifyee "

    #@95
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@98
    move-result-object v15

    #@99
    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v15

    #@9d
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a0
    move-result-object v15

    #@a1
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a4
    .line 2614
    move-object/from16 v0, p0

    #@a6
    iget-object v14, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherModeAliveState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@a8
    invoke-static {v14}, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->access$6900(Lcom/android/server/connectivity/Tethering$TetherMasterSM;)Ljava/util/ArrayList;

    #@ab
    move-result-object v14

    #@ac
    invoke-virtual {v14, v6}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@af
    .line 2615
    move-object/from16 v0, p0

    #@b1
    iget-object v14, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherModeAliveState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@b3
    invoke-static {v14}, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->access$6900(Lcom/android/server/connectivity/Tethering$TetherMasterSM;)Ljava/util/ArrayList;

    #@b6
    move-result-object v14

    #@b7
    invoke-virtual {v14}, Ljava/util/ArrayList;->isEmpty()Z

    #@ba
    move-result v14

    #@bb
    if-eqz v14, :cond_c2

    #@bd
    .line 2616
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherModeAliveState;->turnOffMasterTetherSettings()Z

    #@c0
    goto/16 :goto_27

    #@c2
    .line 2619
    :cond_c2
    const-string v14, "Tethering"

    #@c4
    new-instance v15, Ljava/lang/StringBuilder;

    #@c6
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@c9
    const-string v16, "TetherModeAlive still has "

    #@cb
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v15

    #@cf
    move-object/from16 v0, p0

    #@d1
    iget-object v0, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherModeAliveState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@d3
    move-object/from16 v16, v0

    #@d5
    invoke-static/range {v16 .. v16}, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->access$6900(Lcom/android/server/connectivity/Tethering$TetherMasterSM;)Ljava/util/ArrayList;

    #@d8
    move-result-object v16

    #@d9
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    #@dc
    move-result v16

    #@dd
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e0
    move-result-object v15

    #@e1
    const-string v16, " live requests:"

    #@e3
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e6
    move-result-object v15

    #@e7
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ea
    move-result-object v15

    #@eb
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ee
    .line 2621
    move-object/from16 v0, p0

    #@f0
    iget-object v14, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherModeAliveState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@f2
    invoke-static {v14}, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->access$6900(Lcom/android/server/connectivity/Tethering$TetherMasterSM;)Ljava/util/ArrayList;

    #@f5
    move-result-object v14

    #@f6
    invoke-virtual {v14}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@f9
    move-result-object v5

    #@fa
    .local v5, i$:Ljava/util/Iterator;
    :goto_fa
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    #@fd
    move-result v14

    #@fe
    if-eqz v14, :cond_27

    #@100
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@103
    move-result-object v8

    #@104
    check-cast v8, Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;

    #@106
    .local v8, o:Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;
    const-string v14, "Tethering"

    #@108
    new-instance v15, Ljava/lang/StringBuilder;

    #@10a
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@10d
    const-string v16, "  "

    #@10f
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@112
    move-result-object v15

    #@113
    invoke-virtual {v15, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@116
    move-result-object v15

    #@117
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11a
    move-result-object v15

    #@11b
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@11e
    goto :goto_fa

    #@11f
    .line 2625
    .end local v5           #i$:Ljava/util/Iterator;
    .end local v8           #o:Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;
    :cond_11f
    const-string v14, "Tethering"

    #@121
    new-instance v15, Ljava/lang/StringBuilder;

    #@123
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@126
    const-string v16, "TetherModeAliveState UNREQUESTED has unknown who: "

    #@128
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12b
    move-result-object v15

    #@12c
    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@12f
    move-result-object v15

    #@130
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@133
    move-result-object v15

    #@134
    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@137
    goto/16 :goto_27

    #@139
    .line 2630
    .end local v6           #index:I
    .end local v12           #who:Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;
    :pswitch_139
    move-object/from16 v0, p1

    #@13b
    iget-object v7, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@13d
    check-cast v7, Landroid/net/NetworkInfo;

    #@13f
    .line 2631
    .local v7, info:Landroid/net/NetworkInfo;
    const/4 v14, 0x1

    #@140
    move-object/from16 v0, p0

    #@142
    iput-boolean v14, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherModeAliveState;->mTryCell:Z

    #@144
    .line 2632
    move-object/from16 v0, p0

    #@146
    iget-boolean v14, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherModeAliveState;->mTryCell:Z

    #@148
    move-object/from16 v0, p0

    #@14a
    invoke-virtual {v0, v14}, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherModeAliveState;->chooseUpstreamType(Z)V

    #@14d
    .line 2633
    invoke-virtual {v7}, Landroid/net/NetworkInfo;->isConnected()Z

    #@150
    move-result v14

    #@151
    if-nez v14, :cond_16e

    #@153
    .line 2634
    const-string v14, "connectivity"

    #@155
    invoke-static {v14}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@158
    move-result-object v1

    #@159
    .line 2635
    .local v1, b:Landroid/os/IBinder;
    invoke-static {v1}, Landroid/net/IConnectivityManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/IConnectivityManager;

    #@15c
    move-result-object v3

    #@15d
    .line 2637
    .local v3, cm:Landroid/net/IConnectivityManager;
    :try_start_15d
    invoke-virtual {v7}, Landroid/net/NetworkInfo;->getType()I

    #@160
    move-result v14

    #@161
    invoke-interface {v3, v14}, Landroid/net/IConnectivityManager;->getLinkProperties(I)Landroid/net/LinkProperties;

    #@164
    move-result-object v9

    #@165
    .line 2638
    .local v9, props:Landroid/net/LinkProperties;
    invoke-virtual {v9}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    #@168
    move-result-object v14

    #@169
    move-object/from16 v0, p0

    #@16b
    invoke-virtual {v0, v14}, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherModeAliveState;->removeUpstreamV6Interface(Ljava/lang/String;)V
    :try_end_16e
    .catch Ljava/lang/Exception; {:try_start_15d .. :try_end_16e} :catch_17b

    #@16e
    .line 2643
    .end local v1           #b:Landroid/os/IBinder;
    .end local v3           #cm:Landroid/net/IConnectivityManager;
    .end local v9           #props:Landroid/net/LinkProperties;
    :cond_16e
    :goto_16e
    move-object/from16 v0, p0

    #@170
    iget-boolean v14, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherModeAliveState;->mTryCell:Z

    #@172
    if-nez v14, :cond_184

    #@174
    const/4 v14, 0x1

    #@175
    :goto_175
    move-object/from16 v0, p0

    #@177
    iput-boolean v14, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherModeAliveState;->mTryCell:Z

    #@179
    goto/16 :goto_27

    #@17b
    .line 2639
    .restart local v1       #b:Landroid/os/IBinder;
    .restart local v3       #cm:Landroid/net/IConnectivityManager;
    :catch_17b
    move-exception v4

    #@17c
    .line 2640
    .local v4, e:Ljava/lang/Exception;
    const-string v14, "Tethering"

    #@17e
    const-string v15, "Exception querying ConnectivityManager"

    #@180
    invoke-static {v14, v15, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@183
    goto :goto_16e

    #@184
    .line 2643
    .end local v1           #b:Landroid/os/IBinder;
    .end local v3           #cm:Landroid/net/IConnectivityManager;
    .end local v4           #e:Ljava/lang/Exception;
    :cond_184
    const/4 v14, 0x0

    #@185
    goto :goto_175

    #@186
    .line 2648
    .end local v7           #info:Landroid/net/NetworkInfo;
    :pswitch_186
    move-object/from16 v0, p0

    #@188
    iget-object v14, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherModeAliveState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@18a
    invoke-static {v14}, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->access$4500(Lcom/android/server/connectivity/Tethering$TetherMasterSM;)I

    #@18d
    move-result v14

    #@18e
    move-object/from16 v0, p1

    #@190
    iget v15, v0, Landroid/os/Message;->arg1:I

    #@192
    if-ne v14, v15, :cond_27

    #@194
    .line 2650
    const-string v14, "Tethering"

    #@196
    const-string v15, "renewing mobile connection - requeuing for another 40000ms"

    #@198
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@19b
    .line 2655
    const/4 v11, 0x1

    #@19c
    .line 2657
    .local v11, turnOnMobileConnectionFlag:Z
    move-object/from16 v0, p0

    #@19e
    iget-object v14, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherModeAliveState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@1a0
    iget-object v14, v14, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@1a2
    iget-object v14, v14, Lcom/android/server/connectivity/Tethering;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@1a4
    iget-boolean v14, v14, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TETHER_CHANGE_UPSTREM_TYPE:Z

    #@1a6
    if-eqz v14, :cond_1f6

    #@1a8
    .line 2659
    const-string v14, "connectivity"

    #@1aa
    invoke-static {v14}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@1ad
    move-result-object v2

    #@1ae
    .line 2660
    .local v2, binder:Landroid/os/IBinder;
    invoke-static {v2}, Landroid/net/IConnectivityManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/IConnectivityManager;

    #@1b1
    move-result-object v3

    #@1b2
    .line 2661
    .restart local v3       #cm:Landroid/net/IConnectivityManager;
    const/4 v13, 0x0

    #@1b3
    .line 2664
    .local v13, wifi:Landroid/net/NetworkInfo;
    if-eqz v3, :cond_1ba

    #@1b5
    .line 2665
    const/4 v14, 0x1

    #@1b6
    :try_start_1b6
    invoke-interface {v3, v14}, Landroid/net/IConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;
    :try_end_1b9
    .catch Ljava/lang/Exception; {:try_start_1b6 .. :try_end_1b9} :catch_1db

    #@1b9
    move-result-object v13

    #@1ba
    .line 2672
    :cond_1ba
    :goto_1ba
    if-eqz v13, :cond_1ca

    #@1bc
    invoke-virtual {v13}, Landroid/net/NetworkInfo;->isConnected()Z

    #@1bf
    move-result v14

    #@1c0
    if-eqz v14, :cond_1ca

    #@1c2
    .line 2674
    const-string v14, "Tethering"

    #@1c4
    const-string v15, "[LGE_DATA] WiFi is connected. So, don\'t call startUsingNetworkFeature()"

    #@1c6
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c9
    .line 2675
    const/4 v11, 0x0

    #@1ca
    .line 2678
    :cond_1ca
    if-eqz v11, :cond_27

    #@1cc
    .line 2679
    move-object/from16 v0, p0

    #@1ce
    iget-object v14, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherModeAliveState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@1d0
    invoke-static {v14}, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->access$4400(Lcom/android/server/connectivity/Tethering$TetherMasterSM;)I

    #@1d3
    move-result v14

    #@1d4
    move-object/from16 v0, p0

    #@1d6
    invoke-virtual {v0, v14}, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherModeAliveState;->turnOnUpstreamMobileConnection(I)Z

    #@1d9
    goto/16 :goto_27

    #@1db
    .line 2667
    :catch_1db
    move-exception v4

    #@1dc
    .line 2668
    .restart local v4       #e:Ljava/lang/Exception;
    const-string v14, "Tethering"

    #@1de
    new-instance v15, Ljava/lang/StringBuilder;

    #@1e0
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@1e3
    const-string v16, "Error getNetworkInfo(ConnectivityManager.TYPE_WIFI) :"

    #@1e5
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e8
    move-result-object v15

    #@1e9
    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1ec
    move-result-object v15

    #@1ed
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f0
    move-result-object v15

    #@1f1
    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1f4
    .line 2669
    const/4 v13, 0x0

    #@1f5
    goto :goto_1ba

    #@1f6
    .line 2685
    .end local v2           #binder:Landroid/os/IBinder;
    .end local v3           #cm:Landroid/net/IConnectivityManager;
    .end local v4           #e:Ljava/lang/Exception;
    .end local v13           #wifi:Landroid/net/NetworkInfo;
    :cond_1f6
    move-object/from16 v0, p0

    #@1f8
    iget-object v14, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherModeAliveState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@1fa
    invoke-static {v14}, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->access$4400(Lcom/android/server/connectivity/Tethering$TetherMasterSM;)I

    #@1fd
    move-result v14

    #@1fe
    move-object/from16 v0, p0

    #@200
    invoke-virtual {v0, v14}, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherModeAliveState;->turnOnUpstreamMobileConnection(I)Z

    #@203
    goto/16 :goto_27

    #@205
    .line 2693
    .end local v11           #turnOnMobileConnectionFlag:Z
    :pswitch_205
    move-object/from16 v0, p0

    #@207
    iget-boolean v14, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherModeAliveState;->mTryCell:Z

    #@209
    move-object/from16 v0, p0

    #@20b
    invoke-virtual {v0, v14}, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherModeAliveState;->chooseUpstreamType(Z)V

    #@20e
    .line 2694
    move-object/from16 v0, p0

    #@210
    iget-boolean v14, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherModeAliveState;->mTryCell:Z

    #@212
    if-nez v14, :cond_21b

    #@214
    const/4 v14, 0x1

    #@215
    :goto_215
    move-object/from16 v0, p0

    #@217
    iput-boolean v14, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherModeAliveState;->mTryCell:Z

    #@219
    goto/16 :goto_27

    #@21b
    :cond_21b
    const/4 v14, 0x0

    #@21c
    goto :goto_215

    #@21d
    .line 2600
    nop

    #@21e
    :pswitch_data_21e
    .packed-switch 0x1
        :pswitch_28
        :pswitch_5f
        :pswitch_139
        :pswitch_186
        :pswitch_205
    .end packed-switch
.end method
