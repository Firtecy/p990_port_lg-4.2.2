.class Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;
.super Lcom/android/internal/util/State;
.source "Tethering.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/connectivity/Tethering$TetherMasterSM;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "TetherMasterUtilState"
.end annotation


# static fields
.field protected static final TRY_TO_SETUP_MOBILE_CONNECTION:Z = true

.field protected static final WAIT_FOR_NETWORK_TO_SETTLE:Z


# instance fields
.field final synthetic this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;


# direct methods
.method constructor <init>(Lcom/android/server/connectivity/Tethering$TetherMasterSM;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 2097
    iput-object p1, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@2
    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method protected addUpstreamV6Interface(Ljava/lang/String;)V
    .registers 8
    .parameter "iface"

    #@0
    .prologue
    .line 2296
    const-string v3, "network_management"

    #@2
    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@5
    move-result-object v0

    #@6
    .line 2297
    .local v0, b:Landroid/os/IBinder;
    invoke-static {v0}, Landroid/os/INetworkManagementService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/INetworkManagementService;

    #@9
    move-result-object v2

    #@a
    .line 2299
    .local v2, service:Landroid/os/INetworkManagementService;
    const-string v3, "Tethering"

    #@c
    new-instance v4, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v5, "adding v6 interface "

    #@13
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v4

    #@17
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v4

    #@1b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v4

    #@1f
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 2301
    :try_start_22
    invoke-interface {v2, p1}, Landroid/os/INetworkManagementService;->addUpstreamV6Interface(Ljava/lang/String;)V
    :try_end_25
    .catch Ljava/lang/Exception; {:try_start_22 .. :try_end_25} :catch_26

    #@25
    .line 2305
    :goto_25
    return-void

    #@26
    .line 2302
    :catch_26
    move-exception v1

    #@27
    .line 2303
    .local v1, e:Ljava/lang/Exception;
    const-string v3, "Tethering"

    #@29
    const-string v4, "Unable to append v6 upstream interface"

    #@2b
    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@2e
    goto :goto_25
.end method

.method protected chooseUpstreamType(Z)V
    .registers 26
    .parameter "tryCell"

    #@0
    .prologue
    .line 2365
    const-string v20, "connectivity"

    #@2
    invoke-static/range {v20 .. v20}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@5
    move-result-object v2

    #@6
    .line 2366
    .local v2, b:Landroid/os/IBinder;
    invoke-static {v2}, Landroid/net/IConnectivityManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/IConnectivityManager;

    #@9
    move-result-object v3

    #@a
    .line 2367
    .local v3, cm:Landroid/net/IConnectivityManager;
    const/16 v18, -0x1

    #@c
    .line 2368
    .local v18, upType:I
    const/4 v10, 0x0

    #@d
    .line 2370
    .local v10, iface:Ljava/lang/String;
    move-object/from16 v0, p0

    #@f
    iget-object v0, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@11
    move-object/from16 v20, v0

    #@13
    move-object/from16 v0, v20

    #@15
    iget-object v0, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@17
    move-object/from16 v20, v0

    #@19
    invoke-virtual/range {v20 .. v20}, Lcom/android/server/connectivity/Tethering;->updateConfiguration()V

    #@1c
    .line 2373
    move-object/from16 v0, p0

    #@1e
    iget-object v0, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@20
    move-object/from16 v20, v0

    #@22
    move-object/from16 v0, v20

    #@24
    iget-object v0, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@26
    move-object/from16 v20, v0

    #@28
    const/16 v21, 0x0

    #@2a
    invoke-static/range {v20 .. v21}, Lcom/android/server/connectivity/Tethering;->access$3902(Lcom/android/server/connectivity/Tethering;Ljava/lang/String;)Ljava/lang/String;

    #@2d
    .line 2374
    move-object/from16 v0, p0

    #@2f
    iget-object v0, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@31
    move-object/from16 v20, v0

    #@33
    move-object/from16 v0, v20

    #@35
    iget-object v0, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@37
    move-object/from16 v20, v0

    #@39
    const/16 v21, 0x0

    #@3b
    invoke-static/range {v20 .. v21}, Lcom/android/server/connectivity/Tethering;->access$4002(Lcom/android/server/connectivity/Tethering;Ljava/lang/String;)Ljava/lang/String;

    #@3e
    .line 2377
    move-object/from16 v0, p0

    #@40
    iget-object v0, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@42
    move-object/from16 v20, v0

    #@44
    move-object/from16 v0, v20

    #@46
    iget-object v0, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@48
    move-object/from16 v20, v0

    #@4a
    invoke-static/range {v20 .. v20}, Lcom/android/server/connectivity/Tethering;->access$200(Lcom/android/server/connectivity/Tethering;)Ljava/lang/Object;

    #@4d
    move-result-object v21

    #@4e
    monitor-enter v21

    #@4f
    .line 2379
    :try_start_4f
    const-string v20, "Tethering"

    #@51
    const-string v22, "chooseUpstreamType has upstream iface types:"

    #@53
    move-object/from16 v0, v20

    #@55
    move-object/from16 v1, v22

    #@57
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5a
    .line 2380
    move-object/from16 v0, p0

    #@5c
    iget-object v0, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@5e
    move-object/from16 v20, v0

    #@60
    move-object/from16 v0, v20

    #@62
    iget-object v0, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@64
    move-object/from16 v20, v0

    #@66
    invoke-static/range {v20 .. v20}, Lcom/android/server/connectivity/Tethering;->access$6400(Lcom/android/server/connectivity/Tethering;)Ljava/util/Collection;

    #@69
    move-result-object v20

    #@6a
    invoke-interface/range {v20 .. v20}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@6d
    move-result-object v9

    #@6e
    .local v9, i$:Ljava/util/Iterator;
    :goto_6e
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    #@71
    move-result v20

    #@72
    if-eqz v20, :cond_9c

    #@74
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@77
    move-result-object v15

    #@78
    check-cast v15, Ljava/lang/Integer;

    #@7a
    .line 2381
    .local v15, netType:Ljava/lang/Integer;
    const-string v20, "Tethering"

    #@7c
    new-instance v22, Ljava/lang/StringBuilder;

    #@7e
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@81
    const-string v23, " "

    #@83
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v22

    #@87
    move-object/from16 v0, v22

    #@89
    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v22

    #@8d
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@90
    move-result-object v22

    #@91
    move-object/from16 v0, v20

    #@93
    move-object/from16 v1, v22

    #@95
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@98
    goto :goto_6e

    #@99
    .line 2433
    .end local v9           #i$:Ljava/util/Iterator;
    .end local v15           #netType:Ljava/lang/Integer;
    :catchall_99
    move-exception v20

    #@9a
    monitor-exit v21
    :try_end_9b
    .catchall {:try_start_4f .. :try_end_9b} :catchall_99

    #@9b
    throw v20

    #@9c
    .line 2385
    .restart local v9       #i$:Ljava/util/Iterator;
    :cond_9c
    :try_start_9c
    move-object/from16 v0, p0

    #@9e
    iget-object v0, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@a0
    move-object/from16 v20, v0

    #@a2
    move-object/from16 v0, v20

    #@a4
    iget-object v0, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@a6
    move-object/from16 v20, v0

    #@a8
    invoke-static/range {v20 .. v20}, Lcom/android/server/connectivity/Tethering;->access$6400(Lcom/android/server/connectivity/Tethering;)Ljava/util/Collection;

    #@ab
    move-result-object v20

    #@ac
    invoke-interface/range {v20 .. v20}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@af
    move-result-object v9

    #@b0
    :cond_b0
    :goto_b0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    #@b3
    move-result v20

    #@b4
    if-eqz v20, :cond_1e3

    #@b6
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@b9
    move-result-object v15

    #@ba
    check-cast v15, Ljava/lang/Integer;
    :try_end_bc
    .catchall {:try_start_9c .. :try_end_bc} :catchall_99

    #@bc
    .line 2386
    .restart local v15       #netType:Ljava/lang/Integer;
    const/4 v11, 0x0

    #@bd
    .line 2387
    .local v11, info:Landroid/net/NetworkInfo;
    const/16 v16, 0x0

    #@bf
    .line 2388
    .local v16, props:Landroid/net/LinkProperties;
    const/4 v13, 0x0

    #@c0
    .line 2390
    .local v13, isV6Connected:Z
    :try_start_c0
    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I

    #@c3
    move-result v20

    #@c4
    move/from16 v0, v20

    #@c6
    invoke-interface {v3, v0}, Landroid/net/IConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    #@c9
    move-result-object v11

    #@ca
    .line 2391
    if-eqz v11, :cond_de

    #@cc
    .line 2392
    invoke-virtual {v11}, Landroid/net/NetworkInfo;->getType()I

    #@cf
    move-result v20

    #@d0
    move/from16 v0, v20

    #@d2
    invoke-interface {v3, v0}, Landroid/net/IConnectivityManager;->getLinkProperties(I)Landroid/net/LinkProperties;

    #@d5
    move-result-object v16

    #@d6
    .line 2393
    move-object/from16 v0, p0

    #@d8
    move-object/from16 v1, v16

    #@da
    invoke-virtual {v0, v3, v1}, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->isIpv6Connected(Landroid/net/IConnectivityManager;Landroid/net/LinkProperties;)Z
    :try_end_dd
    .catchall {:try_start_c0 .. :try_end_dd} :catchall_99
    .catch Landroid/os/RemoteException; {:try_start_c0 .. :try_end_dd} :catch_41c

    #@dd
    move-result v13

    #@de
    .line 2396
    :cond_de
    :goto_de
    if-eqz v11, :cond_b0

    #@e0
    :try_start_e0
    invoke-virtual {v11}, Landroid/net/NetworkInfo;->isConnected()Z

    #@e3
    move-result v20

    #@e4
    if-eqz v20, :cond_b0

    #@e6
    .line 2397
    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I
    :try_end_e9
    .catchall {:try_start_e0 .. :try_end_e9} :catchall_99

    #@e9
    move-result v18

    #@ea
    .line 2399
    const/4 v4, 0x0

    #@eb
    .line 2402
    .local v4, current_default_active_network:Landroid/net/NetworkInfo;
    :try_start_eb
    invoke-interface {v3}, Landroid/net/IConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    #@ee
    move-result-object v4

    #@ef
    .line 2403
    const-string v20, "Tethering"

    #@f1
    new-instance v22, Ljava/lang/StringBuilder;

    #@f3
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@f6
    const-string v23, "current_default_active_network["

    #@f8
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fb
    move-result-object v22

    #@fc
    move-object/from16 v0, v22

    #@fe
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@101
    move-result-object v22

    #@102
    const-string v23, "]"

    #@104
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@107
    move-result-object v22

    #@108
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10b
    move-result-object v22

    #@10c
    move-object/from16 v0, v20

    #@10e
    move-object/from16 v1, v22

    #@110
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_113
    .catchall {:try_start_eb .. :try_end_113} :catchall_99
    .catch Landroid/os/RemoteException; {:try_start_eb .. :try_end_113} :catch_419

    #@113
    .line 2406
    :goto_113
    :try_start_113
    const-string v20, "Tethering"

    #@115
    new-instance v22, Ljava/lang/StringBuilder;

    #@117
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@11a
    const-string v23, "upType: "

    #@11c
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11f
    move-result-object v22

    #@120
    move-object/from16 v0, v22

    #@122
    move/from16 v1, v18

    #@124
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@127
    move-result-object v22

    #@128
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12b
    move-result-object v22

    #@12c
    move-object/from16 v0, v20

    #@12e
    move-object/from16 v1, v22

    #@130
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@133
    .line 2408
    if-eqz v4, :cond_15e

    #@135
    const/16 v20, 0x1

    #@137
    move/from16 v0, v18

    #@139
    move/from16 v1, v20

    #@13b
    if-ne v0, v1, :cond_15e

    #@13d
    invoke-virtual {v4}, Landroid/net/NetworkInfo;->getType()I

    #@140
    move-result v20

    #@141
    const/16 v22, 0x1

    #@143
    move/from16 v0, v20

    #@145
    move/from16 v1, v22

    #@147
    if-ne v0, v1, :cond_14f

    #@149
    invoke-virtual {v4}, Landroid/net/NetworkInfo;->isConnected()Z

    #@14c
    move-result v20

    #@14d
    if-nez v20, :cond_15e

    #@14f
    .line 2411
    :cond_14f
    const/16 v18, -0x1

    #@151
    .line 2412
    const-string v20, "Tethering"

    #@153
    const-string v22, "Current connecting network type is NOT real WIFI !!! "

    #@155
    move-object/from16 v0, v20

    #@157
    move-object/from16 v1, v22

    #@159
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@15c
    goto/16 :goto_b0

    #@15e
    .line 2418
    :cond_15e
    move-object/from16 v0, p0

    #@160
    iget-object v0, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@162
    move-object/from16 v20, v0

    #@164
    move-object/from16 v0, v20

    #@166
    iget-object v0, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@168
    move-object/from16 v20, v0

    #@16a
    move-object/from16 v0, v20

    #@16c
    iget-object v0, v0, Lcom/android/server/connectivity/Tethering;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@16e
    move-object/from16 v20, v0

    #@170
    move-object/from16 v0, v20

    #@172
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TETHER_IPV6_SUPPORT:Z

    #@174
    move/from16 v20, v0

    #@176
    if-eqz v20, :cond_2de

    #@178
    .line 2419
    move-object/from16 v0, p0

    #@17a
    move-object/from16 v1, v16

    #@17c
    invoke-virtual {v0, v3, v1}, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->isIpv4Connected(Landroid/net/IConnectivityManager;Landroid/net/LinkProperties;)Z

    #@17f
    move-result v20

    #@180
    if-eqz v20, :cond_199

    #@182
    move-object/from16 v0, p0

    #@184
    iget-object v0, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@186
    move-object/from16 v20, v0

    #@188
    move-object/from16 v0, v20

    #@18a
    iget-object v0, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@18c
    move-object/from16 v20, v0

    #@18e
    invoke-virtual/range {v16 .. v16}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    #@191
    move-result-object v22

    #@192
    move-object/from16 v0, v20

    #@194
    move-object/from16 v1, v22

    #@196
    invoke-static {v0, v1}, Lcom/android/server/connectivity/Tethering;->access$3902(Lcom/android/server/connectivity/Tethering;Ljava/lang/String;)Ljava/lang/String;

    #@199
    .line 2420
    :cond_199
    move-object/from16 v0, p0

    #@19b
    move-object/from16 v1, v16

    #@19d
    invoke-virtual {v0, v3, v1}, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->isIpv6Connected(Landroid/net/IConnectivityManager;Landroid/net/LinkProperties;)Z

    #@1a0
    move-result v20

    #@1a1
    if-eqz v20, :cond_1ba

    #@1a3
    move-object/from16 v0, p0

    #@1a5
    iget-object v0, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@1a7
    move-object/from16 v20, v0

    #@1a9
    move-object/from16 v0, v20

    #@1ab
    iget-object v0, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@1ad
    move-object/from16 v20, v0

    #@1af
    invoke-virtual/range {v16 .. v16}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    #@1b2
    move-result-object v22

    #@1b3
    move-object/from16 v0, v20

    #@1b5
    move-object/from16 v1, v22

    #@1b7
    invoke-static {v0, v1}, Lcom/android/server/connectivity/Tethering;->access$4002(Lcom/android/server/connectivity/Tethering;Ljava/lang/String;)Ljava/lang/String;

    #@1ba
    .line 2421
    :cond_1ba
    move-object/from16 v0, p0

    #@1bc
    iget-object v0, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@1be
    move-object/from16 v20, v0

    #@1c0
    move-object/from16 v0, v20

    #@1c2
    iget-object v0, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@1c4
    move-object/from16 v20, v0

    #@1c6
    invoke-static/range {v20 .. v20}, Lcom/android/server/connectivity/Tethering;->access$4000(Lcom/android/server/connectivity/Tethering;)Ljava/lang/String;

    #@1c9
    move-result-object v20

    #@1ca
    if-eqz v20, :cond_1e3

    #@1cc
    .line 2422
    move-object/from16 v0, p0

    #@1ce
    iget-object v0, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@1d0
    move-object/from16 v20, v0

    #@1d2
    move-object/from16 v0, v20

    #@1d4
    iget-object v0, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@1d6
    move-object/from16 v20, v0

    #@1d8
    invoke-static/range {v20 .. v20}, Lcom/android/server/connectivity/Tethering;->access$4000(Lcom/android/server/connectivity/Tethering;)Ljava/lang/String;

    #@1db
    move-result-object v20

    #@1dc
    move-object/from16 v0, p0

    #@1de
    move-object/from16 v1, v20

    #@1e0
    invoke-virtual {v0, v1}, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->addUpstreamV6Interface(Ljava/lang/String;)V

    #@1e3
    .line 2433
    .end local v4           #current_default_active_network:Landroid/net/NetworkInfo;
    .end local v11           #info:Landroid/net/NetworkInfo;
    .end local v13           #isV6Connected:Z
    .end local v15           #netType:Ljava/lang/Integer;
    .end local v16           #props:Landroid/net/LinkProperties;
    :cond_1e3
    :goto_1e3
    monitor-exit v21
    :try_end_1e4
    .catchall {:try_start_113 .. :try_end_1e4} :catchall_99

    #@1e4
    .line 2436
    const-string v20, "Tethering"

    #@1e6
    new-instance v21, Ljava/lang/StringBuilder;

    #@1e8
    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    #@1eb
    const-string v22, "chooseUpstreamType("

    #@1ed
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f0
    move-result-object v21

    #@1f1
    move-object/from16 v0, v21

    #@1f3
    move/from16 v1, p1

    #@1f5
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1f8
    move-result-object v21

    #@1f9
    const-string v22, "), preferredApn ="

    #@1fb
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1fe
    move-result-object v21

    #@1ff
    move-object/from16 v0, p0

    #@201
    iget-object v0, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@203
    move-object/from16 v22, v0

    #@205
    move-object/from16 v0, v22

    #@207
    iget-object v0, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@209
    move-object/from16 v22, v0

    #@20b
    invoke-static/range {v22 .. v22}, Lcom/android/server/connectivity/Tethering;->access$6500(Lcom/android/server/connectivity/Tethering;)I

    #@20e
    move-result v22

    #@20f
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@212
    move-result-object v21

    #@213
    const-string v22, ", got type="

    #@215
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@218
    move-result-object v21

    #@219
    move-object/from16 v0, v21

    #@21b
    move/from16 v1, v18

    #@21d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@220
    move-result-object v21

    #@221
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@224
    move-result-object v21

    #@225
    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@228
    .line 2441
    move-object/from16 v0, p0

    #@22a
    iget-object v0, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@22c
    move-object/from16 v20, v0

    #@22e
    move-object/from16 v0, v20

    #@230
    iget-object v0, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@232
    move-object/from16 v20, v0

    #@234
    move-object/from16 v0, v20

    #@236
    iget-object v0, v0, Lcom/android/server/connectivity/Tethering;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@238
    move-object/from16 v20, v0

    #@23a
    move-object/from16 v0, v20

    #@23c
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_PDN_MPDN_UPLUS_INIT:Z

    #@23e
    move/from16 v20, v0

    #@240
    if-eqz v20, :cond_2ed

    #@242
    .line 2443
    const/16 v20, 0x12

    #@244
    move/from16 v0, v18

    #@246
    move/from16 v1, v20

    #@248
    if-eq v0, v1, :cond_252

    #@24a
    const/16 v20, 0x5

    #@24c
    move/from16 v0, v18

    #@24e
    move/from16 v1, v20

    #@250
    if-ne v0, v1, :cond_259

    #@252
    .line 2445
    :cond_252
    move-object/from16 v0, p0

    #@254
    move/from16 v1, v18

    #@256
    invoke-virtual {v0, v1}, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->turnOnUpstreamMobileConnection(I)Z

    #@259
    .line 2470
    :cond_259
    :goto_259
    const/16 v20, -0x1

    #@25b
    move/from16 v0, v18

    #@25d
    move/from16 v1, v20

    #@25f
    if-ne v0, v1, :cond_33d

    #@261
    .line 2472
    :try_start_261
    invoke-interface {v3}, Landroid/net/IConnectivityManager;->getMobileDataEnabled()Z

    #@264
    move-result v20

    #@265
    if-eqz v20, :cond_335

    #@267
    .line 2473
    const/16 v17, 0x1

    #@269
    .line 2474
    .local v17, tryAgainLater:Z
    move-object/from16 v0, p0

    #@26b
    iget-object v0, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@26d
    move-object/from16 v20, v0

    #@26f
    move-object/from16 v0, v20

    #@271
    iget v0, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->mRetryCount:I

    #@273
    move/from16 v20, v0

    #@275
    move-object/from16 v0, p0

    #@277
    iget-object v0, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@279
    move-object/from16 v21, v0

    #@27b
    move-object/from16 v0, v21

    #@27d
    iget-object v0, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@27f
    move-object/from16 v21, v0

    #@281
    invoke-static/range {v21 .. v21}, Lcom/android/server/connectivity/Tethering;->access$6600(Lcom/android/server/connectivity/Tethering;)I

    #@284
    move-result v21

    #@285
    move/from16 v0, v20

    #@287
    move/from16 v1, v21

    #@289
    if-ge v0, v1, :cond_313

    #@28b
    .line 2475
    const/16 v20, 0x1

    #@28d
    move/from16 v0, p1

    #@28f
    move/from16 v1, v20

    #@291
    if-ne v0, v1, :cond_2c9

    #@293
    move-object/from16 v0, p0

    #@295
    iget-object v0, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@297
    move-object/from16 v20, v0

    #@299
    move-object/from16 v0, v20

    #@29b
    iget-object v0, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@29d
    move-object/from16 v20, v0

    #@29f
    invoke-static/range {v20 .. v20}, Lcom/android/server/connectivity/Tethering;->access$6500(Lcom/android/server/connectivity/Tethering;)I

    #@2a2
    move-result v20

    #@2a3
    move-object/from16 v0, p0

    #@2a5
    move/from16 v1, v20

    #@2a7
    invoke-virtual {v0, v1}, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->turnOnUpstreamMobileConnection(I)Z

    #@2aa
    move-result v20

    #@2ab
    const/16 v21, 0x1

    #@2ad
    move/from16 v0, v20

    #@2af
    move/from16 v1, v21

    #@2b1
    if-ne v0, v1, :cond_2c9

    #@2b3
    .line 2479
    const/16 v17, 0x0

    #@2b5
    .line 2480
    move-object/from16 v0, p0

    #@2b7
    iget-object v0, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@2b9
    move-object/from16 v20, v0

    #@2bb
    move-object/from16 v0, v20

    #@2bd
    iget v0, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->mRetryCount:I

    #@2bf
    move/from16 v21, v0

    #@2c1
    add-int/lit8 v21, v21, 0x1

    #@2c3
    move/from16 v0, v21

    #@2c5
    move-object/from16 v1, v20

    #@2c7
    iput v0, v1, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->mRetryCount:I

    #@2c9
    .line 2482
    :cond_2c9
    if-eqz v17, :cond_2d8

    #@2cb
    .line 2483
    move-object/from16 v0, p0

    #@2cd
    iget-object v0, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@2cf
    move-object/from16 v20, v0

    #@2d1
    const/16 v21, 0x5

    #@2d3
    const-wide/16 v22, 0x2710

    #@2d5
    invoke-virtual/range {v20 .. v23}, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->sendMessageDelayed(IJ)V
    :try_end_2d8
    .catch Landroid/os/RemoteException; {:try_start_261 .. :try_end_2d8} :catch_32c

    #@2d8
    .line 2535
    .end local v17           #tryAgainLater:Z
    :cond_2d8
    :goto_2d8
    move-object/from16 v0, p0

    #@2da
    invoke-virtual {v0, v10}, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->notifyTetheredOfNewUpstreamIface(Ljava/lang/String;)V

    #@2dd
    .line 2536
    return-void

    #@2de
    .line 2427
    .restart local v4       #current_default_active_network:Landroid/net/NetworkInfo;
    .restart local v11       #info:Landroid/net/NetworkInfo;
    .restart local v13       #isV6Connected:Z
    .restart local v15       #netType:Ljava/lang/Integer;
    .restart local v16       #props:Landroid/net/LinkProperties;
    :cond_2de
    if-eqz v13, :cond_1e3

    #@2e0
    .line 2428
    :try_start_2e0
    invoke-virtual/range {v16 .. v16}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    #@2e3
    move-result-object v20

    #@2e4
    move-object/from16 v0, p0

    #@2e6
    move-object/from16 v1, v20

    #@2e8
    invoke-virtual {v0, v1}, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->addUpstreamV6Interface(Ljava/lang/String;)V
    :try_end_2eb
    .catchall {:try_start_2e0 .. :try_end_2eb} :catchall_99

    #@2eb
    goto/16 :goto_1e3

    #@2ed
    .line 2452
    .end local v4           #current_default_active_network:Landroid/net/NetworkInfo;
    .end local v11           #info:Landroid/net/NetworkInfo;
    .end local v13           #isV6Connected:Z
    .end local v15           #netType:Ljava/lang/Integer;
    .end local v16           #props:Landroid/net/LinkProperties;
    :cond_2ed
    const/16 v20, 0x4

    #@2ef
    move/from16 v0, v18

    #@2f1
    move/from16 v1, v20

    #@2f3
    if-eq v0, v1, :cond_2fd

    #@2f5
    const/16 v20, 0x5

    #@2f7
    move/from16 v0, v18

    #@2f9
    move/from16 v1, v20

    #@2fb
    if-ne v0, v1, :cond_306

    #@2fd
    .line 2454
    :cond_2fd
    move-object/from16 v0, p0

    #@2ff
    move/from16 v1, v18

    #@301
    invoke-virtual {v0, v1}, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->turnOnUpstreamMobileConnection(I)Z

    #@304
    goto/16 :goto_259

    #@306
    .line 2455
    :cond_306
    const/16 v20, -0x1

    #@308
    move/from16 v0, v18

    #@30a
    move/from16 v1, v20

    #@30c
    if-eq v0, v1, :cond_259

    #@30e
    .line 2462
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->turnOffUpstreamMobileConnection()Z

    #@311
    goto/16 :goto_259

    #@313
    .line 2486
    .restart local v17       #tryAgainLater:Z
    :cond_313
    :try_start_313
    move-object/from16 v0, p0

    #@315
    iget-object v0, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@317
    move-object/from16 v20, v0

    #@319
    const/16 v21, 0x0

    #@31b
    move/from16 v0, v21

    #@31d
    move-object/from16 v1, v20

    #@31f
    iput v0, v1, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->mRetryCount:I

    #@321
    .line 2487
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->turnOffUpstreamMobileConnection()Z

    #@324
    .line 2488
    const-string v20, "Tethering"

    #@326
    const-string v21, "chooseUpstreamType: Reached MAX, NO RETRIES"

    #@328
    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_32b
    .catch Landroid/os/RemoteException; {:try_start_313 .. :try_end_32b} :catch_32c

    #@32b
    goto :goto_2d8

    #@32c
    .line 2493
    .end local v17           #tryAgainLater:Z
    :catch_32c
    move-exception v8

    #@32d
    .line 2494
    .local v8, e:Landroid/os/RemoteException;
    const-string v20, "Tethering"

    #@32f
    const-string v21, "Exception in getMobileDataEnabled()"

    #@331
    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@334
    goto :goto_2d8

    #@335
    .line 2491
    .end local v8           #e:Landroid/os/RemoteException;
    :cond_335
    :try_start_335
    const-string v20, "Tethering"

    #@337
    const-string v21, "Data is Disabled"

    #@339
    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_33c
    .catch Landroid/os/RemoteException; {:try_start_335 .. :try_end_33c} :catch_32c

    #@33c
    goto :goto_2d8

    #@33d
    .line 2497
    :cond_33d
    const/4 v14, 0x0

    #@33e
    .line 2499
    .local v14, linkProperties:Landroid/net/LinkProperties;
    :try_start_33e
    move-object/from16 v0, p0

    #@340
    iget-object v0, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@342
    move-object/from16 v20, v0

    #@344
    move-object/from16 v0, v20

    #@346
    iget-object v0, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@348
    move-object/from16 v20, v0

    #@34a
    invoke-static/range {v20 .. v20}, Lcom/android/server/connectivity/Tethering;->access$3800(Lcom/android/server/connectivity/Tethering;)Landroid/net/IConnectivityManager;

    #@34d
    move-result-object v20

    #@34e
    move-object/from16 v0, v20

    #@350
    move/from16 v1, v18

    #@352
    invoke-interface {v0, v1}, Landroid/net/IConnectivityManager;->getLinkProperties(I)Landroid/net/LinkProperties;
    :try_end_355
    .catch Landroid/os/RemoteException; {:try_start_33e .. :try_end_355} :catch_416

    #@355
    move-result-object v14

    #@356
    .line 2501
    :goto_356
    if-eqz v14, :cond_2d8

    #@358
    .line 2502
    invoke-virtual {v14}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    #@35b
    move-result-object v10

    #@35c
    .line 2503
    move-object/from16 v0, p0

    #@35e
    iget-object v0, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@360
    move-object/from16 v20, v0

    #@362
    move-object/from16 v0, v20

    #@364
    iget-object v0, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@366
    move-object/from16 v20, v0

    #@368
    invoke-static/range {v20 .. v20}, Lcom/android/server/connectivity/Tethering;->access$5500(Lcom/android/server/connectivity/Tethering;)[Ljava/lang/String;

    #@36b
    move-result-object v6

    #@36c
    .line 2504
    .local v6, dnsServers:[Ljava/lang/String;
    invoke-virtual {v14}, Landroid/net/LinkProperties;->getDnses()Ljava/util/Collection;

    #@36f
    move-result-object v7

    #@370
    .line 2506
    .local v7, dnses:Ljava/util/Collection;,"Ljava/util/Collection<Ljava/net/InetAddress;>;"
    move-object/from16 v0, p0

    #@372
    iget-object v0, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@374
    move-object/from16 v20, v0

    #@376
    move-object/from16 v0, v20

    #@378
    iget-object v0, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@37a
    move-object/from16 v20, v0

    #@37c
    move-object/from16 v0, v20

    #@37e
    iget-object v0, v0, Lcom/android/server/connectivity/Tethering;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@380
    move-object/from16 v20, v0

    #@382
    move-object/from16 v0, v20

    #@384
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TETHER_IPV6_SUPPORT:Z

    #@386
    move/from16 v20, v0

    #@388
    if-eqz v20, :cond_3e4

    #@38a
    .line 2508
    const-string v20, "45000"

    #@38c
    const-string v21, "gsm.sim.operator.numeric"

    #@38e
    const-string v22, ""

    #@390
    invoke-static/range {v21 .. v22}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@393
    move-result-object v21

    #@394
    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@397
    move-result v12

    #@398
    .line 2510
    .local v12, isLgeTestbed:Z
    if-eqz v7, :cond_3a2

    #@39a
    invoke-interface {v7}, Ljava/util/Collection;->size()I

    #@39d
    move-result v20

    #@39e
    if-eqz v20, :cond_3a2

    #@3a0
    if-eqz v12, :cond_3df

    #@3a2
    :cond_3a2
    move-object/from16 v0, p0

    #@3a4
    iget-object v0, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@3a6
    move-object/from16 v20, v0

    #@3a8
    move-object/from16 v0, v20

    #@3aa
    iget-object v0, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@3ac
    move-object/from16 v20, v0

    #@3ae
    invoke-static/range {v20 .. v20}, Lcom/android/server/connectivity/Tethering;->access$5400(Lcom/android/server/connectivity/Tethering;)[Ljava/lang/String;

    #@3b1
    move-result-object v6

    #@3b2
    .line 2529
    .end local v12           #isLgeTestbed:Z
    :cond_3b2
    :goto_3b2
    :try_start_3b2
    move-object/from16 v0, p0

    #@3b4
    iget-object v0, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@3b6
    move-object/from16 v20, v0

    #@3b8
    move-object/from16 v0, v20

    #@3ba
    iget-object v0, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@3bc
    move-object/from16 v20, v0

    #@3be
    invoke-static/range {v20 .. v20}, Lcom/android/server/connectivity/Tethering;->access$3200(Lcom/android/server/connectivity/Tethering;)Landroid/os/INetworkManagementService;

    #@3c1
    move-result-object v20

    #@3c2
    move-object/from16 v0, v20

    #@3c4
    invoke-interface {v0, v6}, Landroid/os/INetworkManagementService;->setDnsForwarders([Ljava/lang/String;)V
    :try_end_3c7
    .catch Ljava/lang/Exception; {:try_start_3b2 .. :try_end_3c7} :catch_3c9

    #@3c7
    goto/16 :goto_2d8

    #@3c9
    .line 2530
    :catch_3c9
    move-exception v8

    #@3ca
    .line 2531
    .local v8, e:Ljava/lang/Exception;
    move-object/from16 v0, p0

    #@3cc
    iget-object v0, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@3ce
    move-object/from16 v20, v0

    #@3d0
    move-object/from16 v0, p0

    #@3d2
    iget-object v0, v0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@3d4
    move-object/from16 v21, v0

    #@3d6
    invoke-static/range {v21 .. v21}, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->access$5600(Lcom/android/server/connectivity/Tethering$TetherMasterSM;)Lcom/android/internal/util/State;

    #@3d9
    move-result-object v21

    #@3da
    invoke-static/range {v20 .. v21}, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->access$6700(Lcom/android/server/connectivity/Tethering$TetherMasterSM;Lcom/android/internal/util/IState;)V

    #@3dd
    goto/16 :goto_2d8

    #@3df
    .line 2511
    .end local v8           #e:Ljava/lang/Exception;
    .restart local v12       #isLgeTestbed:Z
    :cond_3df
    invoke-static {v7}, Landroid/net/NetworkUtils;->makeStrings(Ljava/util/Collection;)[Ljava/lang/String;

    #@3e2
    move-result-object v6

    #@3e3
    goto :goto_3b2

    #@3e4
    .line 2515
    .end local v12           #isLgeTestbed:Z
    :cond_3e4
    if-eqz v7, :cond_3b2

    #@3e6
    .line 2517
    new-instance v19, Ljava/util/ArrayList;

    #@3e8
    invoke-interface {v7}, Ljava/util/Collection;->size()I

    #@3eb
    move-result v20

    #@3ec
    invoke-direct/range {v19 .. v20}, Ljava/util/ArrayList;-><init>(I)V

    #@3ef
    .line 2519
    .local v19, v4Dnses:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/net/InetAddress;>;"
    invoke-interface {v7}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@3f2
    move-result-object v9

    #@3f3
    :cond_3f3
    :goto_3f3
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    #@3f6
    move-result v20

    #@3f7
    if-eqz v20, :cond_40b

    #@3f9
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@3fc
    move-result-object v5

    #@3fd
    check-cast v5, Ljava/net/InetAddress;

    #@3ff
    .line 2520
    .local v5, dnsAddress:Ljava/net/InetAddress;
    instance-of v0, v5, Ljava/net/Inet4Address;

    #@401
    move/from16 v20, v0

    #@403
    if-eqz v20, :cond_3f3

    #@405
    .line 2521
    move-object/from16 v0, v19

    #@407
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@40a
    goto :goto_3f3

    #@40b
    .line 2524
    .end local v5           #dnsAddress:Ljava/net/InetAddress;
    :cond_40b
    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    #@40e
    move-result v20

    #@40f
    if-lez v20, :cond_3b2

    #@411
    .line 2525
    invoke-static/range {v19 .. v19}, Landroid/net/NetworkUtils;->makeStrings(Ljava/util/Collection;)[Ljava/lang/String;

    #@414
    move-result-object v6

    #@415
    goto :goto_3b2

    #@416
    .line 2500
    .end local v6           #dnsServers:[Ljava/lang/String;
    .end local v7           #dnses:Ljava/util/Collection;,"Ljava/util/Collection<Ljava/net/InetAddress;>;"
    .end local v19           #v4Dnses:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/net/InetAddress;>;"
    :catch_416
    move-exception v20

    #@417
    goto/16 :goto_356

    #@419
    .line 2404
    .end local v14           #linkProperties:Landroid/net/LinkProperties;
    .restart local v4       #current_default_active_network:Landroid/net/NetworkInfo;
    .restart local v11       #info:Landroid/net/NetworkInfo;
    .restart local v13       #isV6Connected:Z
    .restart local v15       #netType:Ljava/lang/Integer;
    .restart local v16       #props:Landroid/net/LinkProperties;
    :catch_419
    move-exception v20

    #@41a
    goto/16 :goto_113

    #@41c
    .line 2395
    .end local v4           #current_default_active_network:Landroid/net/NetworkInfo;
    :catch_41c
    move-exception v20

    #@41d
    goto/16 :goto_de
.end method

.method protected enableString(I)Ljava/lang/String;
    .registers 3
    .parameter "apnType"

    #@0
    .prologue
    .line 2106
    sparse-switch p1, :sswitch_data_e

    #@3
    .line 2118
    const/4 v0, 0x0

    #@4
    :goto_4
    return-object v0

    #@5
    .line 2108
    :sswitch_5
    const-string v0, "enableDUNAlways"

    #@7
    goto :goto_4

    #@8
    .line 2111
    :sswitch_8
    const-string v0, "enableHIPRI"

    #@a
    goto :goto_4

    #@b
    .line 2114
    :sswitch_b
    const-string v0, "enableTETHERING"

    #@d
    goto :goto_4

    #@e
    .line 2106
    :sswitch_data_e
    .sparse-switch
        0x0 -> :sswitch_8
        0x4 -> :sswitch_5
        0x5 -> :sswitch_8
        0x12 -> :sswitch_b
    .end sparse-switch
.end method

.method isIpv4Connected(Landroid/net/IConnectivityManager;Landroid/net/LinkProperties;)Z
    .registers 9
    .parameter "cm"
    .parameter "linkProps"

    #@0
    .prologue
    .line 2321
    const/4 v4, 0x0

    #@1
    .line 2322
    .local v4, ret:Z
    const/4 v1, 0x0

    #@2
    .line 2324
    .local v1, addresses:Ljava/util/Collection;,"Ljava/util/Collection<Ljava/net/InetAddress;>;"
    if-eqz p1, :cond_6

    #@4
    if-nez p2, :cond_8

    #@6
    .line 2325
    :cond_6
    const/4 v5, 0x0

    #@7
    .line 2338
    :goto_7
    return v5

    #@8
    .line 2327
    :cond_8
    invoke-virtual {p2}, Landroid/net/LinkProperties;->getAddresses()Ljava/util/Collection;

    #@b
    move-result-object v1

    #@c
    .line 2328
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@f
    move-result-object v2

    #@10
    .local v2, i$:Ljava/util/Iterator;
    :cond_10
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@13
    move-result v5

    #@14
    if-eqz v5, :cond_3c

    #@16
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@19
    move-result-object v0

    #@1a
    check-cast v0, Ljava/net/InetAddress;

    #@1c
    .line 2329
    .local v0, addr:Ljava/net/InetAddress;
    instance-of v5, v0, Ljava/net/Inet4Address;

    #@1e
    if-eqz v5, :cond_10

    #@20
    move-object v3, v0

    #@21
    .line 2330
    check-cast v3, Ljava/net/Inet4Address;

    #@23
    .line 2331
    .local v3, i4addr:Ljava/net/Inet4Address;
    invoke-virtual {v3}, Ljava/net/Inet4Address;->isAnyLocalAddress()Z

    #@26
    move-result v5

    #@27
    if-nez v5, :cond_10

    #@29
    invoke-virtual {v3}, Ljava/net/Inet4Address;->isLinkLocalAddress()Z

    #@2c
    move-result v5

    #@2d
    if-nez v5, :cond_10

    #@2f
    invoke-virtual {v3}, Ljava/net/Inet4Address;->isLoopbackAddress()Z

    #@32
    move-result v5

    #@33
    if-nez v5, :cond_10

    #@35
    invoke-virtual {v3}, Ljava/net/Inet4Address;->isMulticastAddress()Z

    #@38
    move-result v5

    #@39
    if-nez v5, :cond_10

    #@3b
    .line 2333
    const/4 v4, 0x1

    #@3c
    .end local v0           #addr:Ljava/net/InetAddress;
    .end local v3           #i4addr:Ljava/net/Inet4Address;
    :cond_3c
    move v5, v4

    #@3d
    .line 2338
    goto :goto_7
.end method

.method isIpv6Connected(Landroid/net/IConnectivityManager;Landroid/net/LinkProperties;)Z
    .registers 9
    .parameter "cm"
    .parameter "linkProps"

    #@0
    .prologue
    .line 2343
    const/4 v4, 0x0

    #@1
    .line 2344
    .local v4, ret:Z
    const/4 v1, 0x0

    #@2
    .line 2346
    .local v1, addresses:Ljava/util/Collection;,"Ljava/util/Collection<Ljava/net/InetAddress;>;"
    if-eqz p1, :cond_6

    #@4
    if-nez p2, :cond_8

    #@6
    .line 2347
    :cond_6
    const/4 v5, 0x0

    #@7
    .line 2360
    :goto_7
    return v5

    #@8
    .line 2349
    :cond_8
    invoke-virtual {p2}, Landroid/net/LinkProperties;->getAddresses()Ljava/util/Collection;

    #@b
    move-result-object v1

    #@c
    .line 2350
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@f
    move-result-object v2

    #@10
    .local v2, i$:Ljava/util/Iterator;
    :cond_10
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@13
    move-result v5

    #@14
    if-eqz v5, :cond_3c

    #@16
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@19
    move-result-object v0

    #@1a
    check-cast v0, Ljava/net/InetAddress;

    #@1c
    .line 2351
    .local v0, addr:Ljava/net/InetAddress;
    instance-of v5, v0, Ljava/net/Inet6Address;

    #@1e
    if-eqz v5, :cond_10

    #@20
    move-object v3, v0

    #@21
    .line 2352
    check-cast v3, Ljava/net/Inet6Address;

    #@23
    .line 2353
    .local v3, i6addr:Ljava/net/Inet6Address;
    invoke-virtual {v3}, Ljava/net/Inet6Address;->isAnyLocalAddress()Z

    #@26
    move-result v5

    #@27
    if-nez v5, :cond_10

    #@29
    invoke-virtual {v3}, Ljava/net/Inet6Address;->isLinkLocalAddress()Z

    #@2c
    move-result v5

    #@2d
    if-nez v5, :cond_10

    #@2f
    invoke-virtual {v3}, Ljava/net/Inet6Address;->isLoopbackAddress()Z

    #@32
    move-result v5

    #@33
    if-nez v5, :cond_10

    #@35
    invoke-virtual {v3}, Ljava/net/Inet6Address;->isMulticastAddress()Z

    #@38
    move-result v5

    #@39
    if-nez v5, :cond_10

    #@3b
    .line 2355
    const/4 v4, 0x1

    #@3c
    .end local v0           #addr:Ljava/net/InetAddress;
    .end local v3           #i6addr:Ljava/net/Inet6Address;
    :cond_3c
    move v5, v4

    #@3d
    .line 2360
    goto :goto_7
.end method

.method protected notifyTetheredOfNewUpstreamIface(Ljava/lang/String;)V
    .registers 7
    .parameter "ifaceName"

    #@0
    .prologue
    .line 2539
    const-string v2, "Tethering"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "notifying tethered with iface ="

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v3

    #@15
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 2540
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@1a
    invoke-static {v2, p1}, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->access$6802(Lcom/android/server/connectivity/Tethering$TetherMasterSM;Ljava/lang/String;)Ljava/lang/String;

    #@1d
    .line 2541
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@1f
    invoke-static {v2}, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->access$6900(Lcom/android/server/connectivity/Tethering$TetherMasterSM;)Ljava/util/ArrayList;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@26
    move-result-object v0

    #@27
    .local v0, i$:Ljava/util/Iterator;
    :goto_27
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@2a
    move-result v2

    #@2b
    if-eqz v2, :cond_39

    #@2d
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@30
    move-result-object v1

    #@31
    check-cast v1, Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;

    #@33
    .line 2542
    .local v1, sm:Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;
    const/16 v2, 0xc

    #@35
    invoke-virtual {v1, v2, p1}, Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;->sendMessage(ILjava/lang/Object;)V

    #@38
    goto :goto_27

    #@39
    .line 2545
    .end local v1           #sm:Lcom/android/server/connectivity/Tethering$TetherInterfaceSM;
    :cond_39
    return-void
.end method

.method public processMessage(Landroid/os/Message;)Z
    .registers 3
    .parameter "m"

    #@0
    .prologue
    .line 2103
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method protected removeUpstreamV6Interface(Ljava/lang/String;)V
    .registers 8
    .parameter "iface"

    #@0
    .prologue
    .line 2308
    const-string v3, "network_management"

    #@2
    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@5
    move-result-object v0

    #@6
    .line 2309
    .local v0, b:Landroid/os/IBinder;
    invoke-static {v0}, Landroid/os/INetworkManagementService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/INetworkManagementService;

    #@9
    move-result-object v2

    #@a
    .line 2311
    .local v2, service:Landroid/os/INetworkManagementService;
    const-string v3, "Tethering"

    #@c
    new-instance v4, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v5, "removing v6 interface "

    #@13
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v4

    #@17
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v4

    #@1b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v4

    #@1f
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 2313
    :try_start_22
    invoke-interface {v2, p1}, Landroid/os/INetworkManagementService;->removeUpstreamV6Interface(Ljava/lang/String;)V
    :try_end_25
    .catch Ljava/lang/Exception; {:try_start_22 .. :try_end_25} :catch_26

    #@25
    .line 2317
    :goto_25
    return-void

    #@26
    .line 2314
    :catch_26
    move-exception v1

    #@27
    .line 2315
    .local v1, e:Ljava/lang/Exception;
    const-string v3, "Tethering"

    #@29
    const-string v4, "Unable to remove v6 upstream interface"

    #@2b
    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@2e
    goto :goto_25
.end method

.method protected turnOffMasterTetherSettings()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 2280
    :try_start_1
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@3
    iget-object v2, v2, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@5
    invoke-static {v2}, Lcom/android/server/connectivity/Tethering;->access$3200(Lcom/android/server/connectivity/Tethering;)Landroid/os/INetworkManagementService;

    #@8
    move-result-object v2

    #@9
    invoke-interface {v2}, Landroid/os/INetworkManagementService;->stopTethering()V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_c} :catch_25

    #@c
    .line 2286
    :try_start_c
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@e
    iget-object v2, v2, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@10
    invoke-static {v2}, Lcom/android/server/connectivity/Tethering;->access$3200(Lcom/android/server/connectivity/Tethering;)Landroid/os/INetworkManagementService;

    #@13
    move-result-object v2

    #@14
    const/4 v3, 0x0

    #@15
    invoke-interface {v2, v3}, Landroid/os/INetworkManagementService;->setIpForwardingEnabled(Z)V
    :try_end_18
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_18} :catch_32

    #@18
    .line 2291
    iget-object v1, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@1a
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@1c
    invoke-static {v2}, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->access$6200(Lcom/android/server/connectivity/Tethering$TetherMasterSM;)Lcom/android/internal/util/State;

    #@1f
    move-result-object v2

    #@20
    invoke-static {v1, v2}, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->access$6300(Lcom/android/server/connectivity/Tethering$TetherMasterSM;Lcom/android/internal/util/IState;)V

    #@23
    .line 2292
    const/4 v1, 0x1

    #@24
    :goto_24
    return v1

    #@25
    .line 2281
    :catch_25
    move-exception v0

    #@26
    .line 2282
    .local v0, e:Ljava/lang/Exception;
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@28
    iget-object v3, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@2a
    invoke-static {v3}, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->access$5800(Lcom/android/server/connectivity/Tethering$TetherMasterSM;)Lcom/android/internal/util/State;

    #@2d
    move-result-object v3

    #@2e
    invoke-static {v2, v3}, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->access$5900(Lcom/android/server/connectivity/Tethering$TetherMasterSM;Lcom/android/internal/util/IState;)V

    #@31
    goto :goto_24

    #@32
    .line 2287
    .end local v0           #e:Ljava/lang/Exception;
    :catch_32
    move-exception v0

    #@33
    .line 2288
    .restart local v0       #e:Ljava/lang/Exception;
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@35
    iget-object v3, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@37
    invoke-static {v3}, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->access$6000(Lcom/android/server/connectivity/Tethering$TetherMasterSM;)Lcom/android/internal/util/State;

    #@3a
    move-result-object v3

    #@3b
    invoke-static {v2, v3}, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->access$6100(Lcom/android/server/connectivity/Tethering$TetherMasterSM;Lcom/android/internal/util/IState;)V

    #@3e
    goto :goto_24
.end method

.method protected turnOffUpstreamMobileConnection()Z
    .registers 7

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v5, -0x1

    #@2
    .line 2150
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@4
    invoke-static {v2}, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->access$4504(Lcom/android/server/connectivity/Tethering$TetherMasterSM;)I

    #@7
    .line 2151
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@9
    invoke-static {v2}, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->access$4400(Lcom/android/server/connectivity/Tethering$TetherMasterSM;)I

    #@c
    move-result v2

    #@d
    if-eq v2, v5, :cond_2a

    #@f
    .line 2153
    :try_start_f
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@11
    iget-object v2, v2, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@13
    invoke-static {v2}, Lcom/android/server/connectivity/Tethering;->access$3800(Lcom/android/server/connectivity/Tethering;)Landroid/net/IConnectivityManager;

    #@16
    move-result-object v2

    #@17
    const/4 v3, 0x0

    #@18
    iget-object v4, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@1a
    invoke-static {v4}, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->access$4400(Lcom/android/server/connectivity/Tethering$TetherMasterSM;)I

    #@1d
    move-result v4

    #@1e
    invoke-virtual {p0, v4}, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->enableString(I)Ljava/lang/String;

    #@21
    move-result-object v4

    #@22
    invoke-interface {v2, v3, v4}, Landroid/net/IConnectivityManager;->stopUsingNetworkFeature(ILjava/lang/String;)I
    :try_end_25
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_25} :catch_2c

    #@25
    .line 2158
    iget-object v1, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@27
    invoke-static {v1, v5}, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->access$4402(Lcom/android/server/connectivity/Tethering$TetherMasterSM;I)I

    #@2a
    .line 2160
    :cond_2a
    const/4 v1, 0x1

    #@2b
    :goto_2b
    return v1

    #@2c
    .line 2155
    :catch_2c
    move-exception v0

    #@2d
    .line 2156
    .local v0, e:Ljava/lang/Exception;
    goto :goto_2b
.end method

.method protected turnOnMasterTetherSettings()Z
    .registers 13

    #@0
    .prologue
    const/4 v11, 0x4

    #@1
    const/4 v10, 0x3

    #@2
    const/4 v9, 0x2

    #@3
    const/4 v5, 0x1

    #@4
    const/4 v6, 0x0

    #@5
    .line 2164
    :try_start_5
    iget-object v4, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@7
    iget-object v4, v4, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@9
    invoke-static {v4}, Lcom/android/server/connectivity/Tethering;->access$3200(Lcom/android/server/connectivity/Tethering;)Landroid/os/INetworkManagementService;

    #@c
    move-result-object v4

    #@d
    const/4 v7, 0x1

    #@e
    invoke-interface {v4, v7}, Landroid/os/INetworkManagementService;->setIpForwardingEnabled(Z)V
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_11} :catch_c9

    #@11
    .line 2176
    iget-object v4, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@13
    iget-object v4, v4, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@15
    invoke-static {}, Lcom/android/server/connectivity/Tethering;->access$4900()[Ljava/lang/String;

    #@18
    move-result-object v7

    #@19
    invoke-static {v4, v7}, Lcom/android/server/connectivity/Tethering;->access$4802(Lcom/android/server/connectivity/Tethering;[Ljava/lang/String;)[Ljava/lang/String;

    #@1c
    .line 2178
    sget-boolean v4, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@1e
    if-eqz v4, :cond_d7

    #@20
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->useMobileHotspot()Z

    #@23
    move-result v4

    #@24
    if-eqz v4, :cond_d7

    #@26
    .line 2179
    new-instance v3, Ljava/util/ArrayList;

    #@28
    invoke-static {}, Lcom/android/server/connectivity/Tethering;->access$4900()[Ljava/lang/String;

    #@2b
    move-result-object v4

    #@2c
    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    #@2f
    move-result-object v4

    #@30
    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    #@33
    .line 2181
    .local v3, listMhpDhcpRange:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    new-instance v4, Ljava/lang/StringBuilder;

    #@35
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    const-string v7, ""

    #@3a
    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v4

    #@3e
    invoke-static {}, Lcom/android/server/connectivity/Tethering;->access$900()Landroid/content/Context;

    #@41
    move-result-object v7

    #@42
    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@45
    move-result-object v7

    #@46
    const-string v8, "start_ip"

    #@48
    invoke-static {v7, v8}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@4b
    move-result-object v7

    #@4c
    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v4

    #@50
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@53
    move-result-object v4

    #@54
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@57
    .line 2183
    new-instance v4, Ljava/lang/StringBuilder;

    #@59
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@5c
    const-string v7, ""

    #@5e
    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v4

    #@62
    invoke-static {}, Lcom/android/server/connectivity/Tethering;->access$900()Landroid/content/Context;

    #@65
    move-result-object v7

    #@66
    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@69
    move-result-object v7

    #@6a
    const-string v8, "end_ip"

    #@6c
    invoke-static {v7, v8}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@6f
    move-result-object v7

    #@70
    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v4

    #@74
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@77
    move-result-object v4

    #@78
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@7b
    .line 2186
    iget-object v4, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@7d
    iget-object v7, v4, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@7f
    invoke-interface {v3}, Ljava/util/List;->size()I

    #@82
    move-result v4

    #@83
    new-array v4, v4, [Ljava/lang/String;

    #@85
    invoke-interface {v3, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@88
    move-result-object v4

    #@89
    check-cast v4, [Ljava/lang/String;

    #@8b
    invoke-static {v7, v4}, Lcom/android/server/connectivity/Tethering;->access$4802(Lcom/android/server/connectivity/Tethering;[Ljava/lang/String;)[Ljava/lang/String;

    #@8e
    .line 2188
    const/4 v2, 0x0

    #@8f
    .local v2, i:I
    :goto_8f
    iget-object v4, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@91
    iget-object v4, v4, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@93
    invoke-static {v4}, Lcom/android/server/connectivity/Tethering;->access$4800(Lcom/android/server/connectivity/Tethering;)[Ljava/lang/String;

    #@96
    move-result-object v4

    #@97
    array-length v4, v4

    #@98
    if-ge v2, v4, :cond_d7

    #@9a
    .line 2189
    const-string v4, "Tethering"

    #@9c
    new-instance v7, Ljava/lang/StringBuilder;

    #@9e
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@a1
    const-string v8, "DhcpRange["

    #@a3
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v7

    #@a7
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v7

    #@ab
    const-string v8, "] : "

    #@ad
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v7

    #@b1
    iget-object v8, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@b3
    iget-object v8, v8, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@b5
    invoke-static {v8}, Lcom/android/server/connectivity/Tethering;->access$4800(Lcom/android/server/connectivity/Tethering;)[Ljava/lang/String;

    #@b8
    move-result-object v8

    #@b9
    aget-object v8, v8, v2

    #@bb
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@be
    move-result-object v7

    #@bf
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c2
    move-result-object v7

    #@c3
    invoke-static {v4, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c6
    .line 2188
    add-int/lit8 v2, v2, 0x1

    #@c8
    goto :goto_8f

    #@c9
    .line 2165
    .end local v2           #i:I
    .end local v3           #listMhpDhcpRange:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    :catch_c9
    move-exception v0

    #@ca
    .line 2166
    .local v0, e:Ljava/lang/Exception;
    iget-object v4, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@cc
    iget-object v5, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@ce
    invoke-static {v5}, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->access$4600(Lcom/android/server/connectivity/Tethering$TetherMasterSM;)Lcom/android/internal/util/State;

    #@d1
    move-result-object v5

    #@d2
    invoke-static {v4, v5}, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->access$4700(Lcom/android/server/connectivity/Tethering$TetherMasterSM;Lcom/android/internal/util/IState;)V

    #@d5
    move v4, v6

    #@d6
    .line 2276
    .end local v0           #e:Ljava/lang/Exception;
    :goto_d6
    return v4

    #@d7
    .line 2196
    :cond_d7
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->isWifiChameleonFeaturedCarrier()Z

    #@da
    move-result v4

    #@db
    if-eqz v4, :cond_174

    #@dd
    .line 2197
    iget-object v4, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@df
    iget-object v4, v4, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@e1
    invoke-static {}, Lcom/android/server/connectivity/Tethering;->access$4900()[Ljava/lang/String;

    #@e4
    move-result-object v7

    #@e5
    invoke-static {v4, v7}, Lcom/android/server/connectivity/Tethering;->access$4802(Lcom/android/server/connectivity/Tethering;[Ljava/lang/String;)[Ljava/lang/String;

    #@e8
    .line 2198
    iget-object v4, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@ea
    iget-object v4, v4, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@ec
    invoke-static {v4}, Lcom/android/server/connectivity/Tethering;->access$4800(Lcom/android/server/connectivity/Tethering;)[Ljava/lang/String;

    #@ef
    move-result-object v4

    #@f0
    invoke-static {}, Lcom/android/server/connectivity/Tethering;->access$5000()[Ljava/lang/String;

    #@f3
    move-result-object v7

    #@f4
    aget-object v7, v7, v6

    #@f6
    aput-object v7, v4, v9

    #@f8
    .line 2199
    iget-object v4, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@fa
    iget-object v4, v4, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@fc
    invoke-static {v4}, Lcom/android/server/connectivity/Tethering;->access$4800(Lcom/android/server/connectivity/Tethering;)[Ljava/lang/String;

    #@ff
    move-result-object v4

    #@100
    invoke-static {}, Lcom/android/server/connectivity/Tethering;->access$5000()[Ljava/lang/String;

    #@103
    move-result-object v7

    #@104
    aget-object v7, v7, v5

    #@106
    aput-object v7, v4, v10

    #@108
    .line 2201
    const-string v4, "Tethering"

    #@10a
    new-instance v7, Ljava/lang/StringBuilder;

    #@10c
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@10f
    const-string v8, "Dhcp Range 0 : "

    #@111
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@114
    move-result-object v7

    #@115
    iget-object v8, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@117
    iget-object v8, v8, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@119
    invoke-static {v8}, Lcom/android/server/connectivity/Tethering;->access$4800(Lcom/android/server/connectivity/Tethering;)[Ljava/lang/String;

    #@11c
    move-result-object v8

    #@11d
    aget-object v8, v8, v6

    #@11f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@122
    move-result-object v7

    #@123
    const-string v8, " ~ "

    #@125
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@128
    move-result-object v7

    #@129
    iget-object v8, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@12b
    iget-object v8, v8, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@12d
    invoke-static {v8}, Lcom/android/server/connectivity/Tethering;->access$4800(Lcom/android/server/connectivity/Tethering;)[Ljava/lang/String;

    #@130
    move-result-object v8

    #@131
    aget-object v8, v8, v5

    #@133
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@136
    move-result-object v7

    #@137
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13a
    move-result-object v7

    #@13b
    invoke-static {v4, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@13e
    .line 2202
    const-string v4, "Tethering"

    #@140
    new-instance v7, Ljava/lang/StringBuilder;

    #@142
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@145
    const-string v8, "Dhcp Range 2 : "

    #@147
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14a
    move-result-object v7

    #@14b
    iget-object v8, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@14d
    iget-object v8, v8, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@14f
    invoke-static {v8}, Lcom/android/server/connectivity/Tethering;->access$4800(Lcom/android/server/connectivity/Tethering;)[Ljava/lang/String;

    #@152
    move-result-object v8

    #@153
    aget-object v8, v8, v9

    #@155
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@158
    move-result-object v7

    #@159
    const-string v8, " ~ "

    #@15b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15e
    move-result-object v7

    #@15f
    iget-object v8, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@161
    iget-object v8, v8, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@163
    invoke-static {v8}, Lcom/android/server/connectivity/Tethering;->access$4800(Lcom/android/server/connectivity/Tethering;)[Ljava/lang/String;

    #@166
    move-result-object v8

    #@167
    aget-object v8, v8, v10

    #@169
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16c
    move-result-object v7

    #@16d
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@170
    move-result-object v7

    #@171
    invoke-static {v4, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@174
    .line 2222
    :cond_174
    :try_start_174
    iget-object v4, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@176
    iget-object v4, v4, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@178
    invoke-static {v4}, Lcom/android/server/connectivity/Tethering;->access$3200(Lcom/android/server/connectivity/Tethering;)Landroid/os/INetworkManagementService;

    #@17b
    move-result-object v4

    #@17c
    iget-object v7, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@17e
    iget-object v7, v7, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@180
    invoke-static {v7}, Lcom/android/server/connectivity/Tethering;->access$4800(Lcom/android/server/connectivity/Tethering;)[Ljava/lang/String;

    #@183
    move-result-object v7

    #@184
    invoke-interface {v4, v7}, Landroid/os/INetworkManagementService;->startTethering([Ljava/lang/String;)V
    :try_end_187
    .catch Ljava/lang/Exception; {:try_start_174 .. :try_end_187} :catch_21e

    #@187
    .line 2239
    :goto_187
    sget-boolean v4, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@189
    if-eqz v4, :cond_24e

    #@18b
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->useMobileHotspot()Z

    #@18e
    move-result v4

    #@18f
    if-eqz v4, :cond_24e

    #@191
    .line 2240
    iget-object v4, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@193
    iget-object v4, v4, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@195
    const/4 v7, 0x5

    #@196
    new-array v7, v7, [Ljava/lang/String;

    #@198
    invoke-static {v4, v7}, Lcom/android/server/connectivity/Tethering;->access$5302(Lcom/android/server/connectivity/Tethering;[Ljava/lang/String;)[Ljava/lang/String;

    #@19b
    .line 2242
    iget-object v4, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@19d
    iget-object v4, v4, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@19f
    invoke-static {v4}, Lcom/android/server/connectivity/Tethering;->access$5300(Lcom/android/server/connectivity/Tethering;)[Ljava/lang/String;

    #@1a2
    move-result-object v4

    #@1a3
    const-string v7, "8.8.8.8"

    #@1a5
    aput-object v7, v4, v6

    #@1a7
    .line 2243
    iget-object v4, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@1a9
    iget-object v4, v4, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@1ab
    invoke-static {v4}, Lcom/android/server/connectivity/Tethering;->access$5300(Lcom/android/server/connectivity/Tethering;)[Ljava/lang/String;

    #@1ae
    move-result-object v4

    #@1af
    const-string v7, "2001:4860:4860::8888"

    #@1b1
    aput-object v7, v4, v5

    #@1b3
    .line 2244
    iget-object v4, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@1b5
    iget-object v4, v4, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@1b7
    invoke-static {v4}, Lcom/android/server/connectivity/Tethering;->access$5300(Lcom/android/server/connectivity/Tethering;)[Ljava/lang/String;

    #@1ba
    move-result-object v4

    #@1bb
    const-string v7, "8.8.4.4"

    #@1bd
    aput-object v7, v4, v9

    #@1bf
    .line 2245
    iget-object v4, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@1c1
    iget-object v4, v4, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@1c3
    invoke-static {v4}, Lcom/android/server/connectivity/Tethering;->access$5300(Lcom/android/server/connectivity/Tethering;)[Ljava/lang/String;

    #@1c6
    move-result-object v4

    #@1c7
    const-string v7, "2001:4860:4860::8844"

    #@1c9
    aput-object v7, v4, v10

    #@1cb
    .line 2246
    iget-object v4, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@1cd
    iget-object v4, v4, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@1cf
    invoke-static {v4}, Lcom/android/server/connectivity/Tethering;->access$5300(Lcom/android/server/connectivity/Tethering;)[Ljava/lang/String;

    #@1d2
    move-result-object v4

    #@1d3
    invoke-static {}, Lcom/android/server/connectivity/Tethering;->access$900()Landroid/content/Context;

    #@1d6
    move-result-object v7

    #@1d7
    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1da
    move-result-object v7

    #@1db
    const-string v8, "dns_server"

    #@1dd
    invoke-static {v7, v8}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@1e0
    move-result-object v7

    #@1e1
    aput-object v7, v4, v11

    #@1e3
    .line 2258
    :goto_1e3
    const/4 v2, 0x0

    #@1e4
    .restart local v2       #i:I
    :goto_1e4
    iget-object v4, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@1e6
    iget-object v4, v4, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@1e8
    invoke-static {v4}, Lcom/android/server/connectivity/Tethering;->access$5300(Lcom/android/server/connectivity/Tethering;)[Ljava/lang/String;

    #@1eb
    move-result-object v4

    #@1ec
    array-length v4, v4

    #@1ed
    if-ge v2, v4, :cond_289

    #@1ef
    .line 2259
    const-string v4, "Tethering"

    #@1f1
    new-instance v7, Ljava/lang/StringBuilder;

    #@1f3
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@1f6
    const-string v8, "dns["

    #@1f8
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1fb
    move-result-object v7

    #@1fc
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1ff
    move-result-object v7

    #@200
    const-string v8, "] : "

    #@202
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@205
    move-result-object v7

    #@206
    iget-object v8, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@208
    iget-object v8, v8, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@20a
    invoke-static {v8}, Lcom/android/server/connectivity/Tethering;->access$5300(Lcom/android/server/connectivity/Tethering;)[Ljava/lang/String;

    #@20d
    move-result-object v8

    #@20e
    aget-object v8, v8, v2

    #@210
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@213
    move-result-object v7

    #@214
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@217
    move-result-object v7

    #@218
    invoke-static {v4, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@21b
    .line 2258
    add-int/lit8 v2, v2, 0x1

    #@21d
    goto :goto_1e4

    #@21e
    .line 2223
    .end local v2           #i:I
    :catch_21e
    move-exception v0

    #@21f
    .line 2225
    .restart local v0       #e:Ljava/lang/Exception;
    :try_start_21f
    iget-object v4, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@221
    iget-object v4, v4, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@223
    invoke-static {v4}, Lcom/android/server/connectivity/Tethering;->access$3200(Lcom/android/server/connectivity/Tethering;)Landroid/os/INetworkManagementService;

    #@226
    move-result-object v4

    #@227
    invoke-interface {v4}, Landroid/os/INetworkManagementService;->stopTethering()V

    #@22a
    .line 2226
    iget-object v4, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@22c
    iget-object v4, v4, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@22e
    invoke-static {v4}, Lcom/android/server/connectivity/Tethering;->access$3200(Lcom/android/server/connectivity/Tethering;)Landroid/os/INetworkManagementService;

    #@231
    move-result-object v4

    #@232
    iget-object v7, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@234
    iget-object v7, v7, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@236
    invoke-static {v7}, Lcom/android/server/connectivity/Tethering;->access$4800(Lcom/android/server/connectivity/Tethering;)[Ljava/lang/String;

    #@239
    move-result-object v7

    #@23a
    invoke-interface {v4, v7}, Landroid/os/INetworkManagementService;->startTethering([Ljava/lang/String;)V
    :try_end_23d
    .catch Ljava/lang/Exception; {:try_start_21f .. :try_end_23d} :catch_23f

    #@23d
    goto/16 :goto_187

    #@23f
    .line 2227
    :catch_23f
    move-exception v1

    #@240
    .line 2228
    .local v1, ee:Ljava/lang/Exception;
    iget-object v4, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@242
    iget-object v5, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@244
    invoke-static {v5}, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->access$5100(Lcom/android/server/connectivity/Tethering$TetherMasterSM;)Lcom/android/internal/util/State;

    #@247
    move-result-object v5

    #@248
    invoke-static {v4, v5}, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->access$5200(Lcom/android/server/connectivity/Tethering$TetherMasterSM;Lcom/android/internal/util/IState;)V

    #@24b
    move v4, v6

    #@24c
    .line 2229
    goto/16 :goto_d6

    #@24e
    .line 2249
    .end local v0           #e:Ljava/lang/Exception;
    .end local v1           #ee:Ljava/lang/Exception;
    :cond_24e
    iget-object v4, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@250
    iget-object v4, v4, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@252
    new-array v7, v11, [Ljava/lang/String;

    #@254
    invoke-static {v4, v7}, Lcom/android/server/connectivity/Tethering;->access$5302(Lcom/android/server/connectivity/Tethering;[Ljava/lang/String;)[Ljava/lang/String;

    #@257
    .line 2251
    iget-object v4, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@259
    iget-object v4, v4, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@25b
    invoke-static {v4}, Lcom/android/server/connectivity/Tethering;->access$5300(Lcom/android/server/connectivity/Tethering;)[Ljava/lang/String;

    #@25e
    move-result-object v4

    #@25f
    const-string v7, "8.8.8.8"

    #@261
    aput-object v7, v4, v6

    #@263
    .line 2252
    iget-object v4, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@265
    iget-object v4, v4, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@267
    invoke-static {v4}, Lcom/android/server/connectivity/Tethering;->access$5300(Lcom/android/server/connectivity/Tethering;)[Ljava/lang/String;

    #@26a
    move-result-object v4

    #@26b
    const-string v7, "2001:4860:4860::8888"

    #@26d
    aput-object v7, v4, v5

    #@26f
    .line 2253
    iget-object v4, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@271
    iget-object v4, v4, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@273
    invoke-static {v4}, Lcom/android/server/connectivity/Tethering;->access$5300(Lcom/android/server/connectivity/Tethering;)[Ljava/lang/String;

    #@276
    move-result-object v4

    #@277
    const-string v7, "8.8.4.4"

    #@279
    aput-object v7, v4, v9

    #@27b
    .line 2254
    iget-object v4, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@27d
    iget-object v4, v4, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@27f
    invoke-static {v4}, Lcom/android/server/connectivity/Tethering;->access$5300(Lcom/android/server/connectivity/Tethering;)[Ljava/lang/String;

    #@282
    move-result-object v4

    #@283
    const-string v7, "2001:4860:4860::8844"

    #@285
    aput-object v7, v4, v10

    #@287
    goto/16 :goto_1e3

    #@289
    .line 2266
    .restart local v2       #i:I
    :cond_289
    :try_start_289
    iget-object v4, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@28b
    iget-object v4, v4, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@28d
    iget-object v4, v4, Lcom/android/server/connectivity/Tethering;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@28f
    iget-boolean v4, v4, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TETHER_IPV6_SUPPORT:Z

    #@291
    if-eqz v4, :cond_2a9

    #@293
    .line 2267
    iget-object v4, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@295
    iget-object v4, v4, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@297
    invoke-static {v4}, Lcom/android/server/connectivity/Tethering;->access$3200(Lcom/android/server/connectivity/Tethering;)Landroid/os/INetworkManagementService;

    #@29a
    move-result-object v4

    #@29b
    iget-object v7, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@29d
    iget-object v7, v7, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@29f
    invoke-static {v7}, Lcom/android/server/connectivity/Tethering;->access$5400(Lcom/android/server/connectivity/Tethering;)[Ljava/lang/String;

    #@2a2
    move-result-object v7

    #@2a3
    invoke-interface {v4, v7}, Landroid/os/INetworkManagementService;->setDnsForwarders([Ljava/lang/String;)V

    #@2a6
    :goto_2a6
    move v4, v5

    #@2a7
    .line 2276
    goto/16 :goto_d6

    #@2a9
    .line 2271
    :cond_2a9
    iget-object v4, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@2ab
    iget-object v4, v4, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@2ad
    invoke-static {v4}, Lcom/android/server/connectivity/Tethering;->access$3200(Lcom/android/server/connectivity/Tethering;)Landroid/os/INetworkManagementService;

    #@2b0
    move-result-object v4

    #@2b1
    iget-object v7, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@2b3
    iget-object v7, v7, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@2b5
    invoke-static {v7}, Lcom/android/server/connectivity/Tethering;->access$5500(Lcom/android/server/connectivity/Tethering;)[Ljava/lang/String;

    #@2b8
    move-result-object v7

    #@2b9
    invoke-interface {v4, v7}, Landroid/os/INetworkManagementService;->setDnsForwarders([Ljava/lang/String;)V
    :try_end_2bc
    .catch Ljava/lang/Exception; {:try_start_289 .. :try_end_2bc} :catch_2bd

    #@2bc
    goto :goto_2a6

    #@2bd
    .line 2272
    :catch_2bd
    move-exception v0

    #@2be
    .line 2273
    .restart local v0       #e:Ljava/lang/Exception;
    iget-object v4, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@2c0
    iget-object v5, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@2c2
    invoke-static {v5}, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->access$5600(Lcom/android/server/connectivity/Tethering$TetherMasterSM;)Lcom/android/internal/util/State;

    #@2c5
    move-result-object v5

    #@2c6
    invoke-static {v4, v5}, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->access$5700(Lcom/android/server/connectivity/Tethering$TetherMasterSM;Lcom/android/internal/util/IState;)V

    #@2c9
    move v4, v6

    #@2ca
    .line 2274
    goto/16 :goto_d6
.end method

.method protected turnOnUpstreamMobileConnection(I)Z
    .registers 9
    .parameter "apnType"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 2121
    const/4 v3, 0x1

    #@2
    .line 2122
    .local v3, retValue:Z
    const/4 v5, -0x1

    #@3
    if-ne p1, v5, :cond_6

    #@5
    .line 2146
    :cond_5
    :goto_5
    return v4

    #@6
    .line 2123
    :cond_6
    iget-object v5, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@8
    invoke-static {v5}, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->access$4400(Lcom/android/server/connectivity/Tethering$TetherMasterSM;)I

    #@b
    move-result v5

    #@c
    if-eq p1, v5, :cond_11

    #@e
    invoke-virtual {p0}, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->turnOffUpstreamMobileConnection()Z

    #@11
    .line 2124
    :cond_11
    const/4 v2, 0x3

    #@12
    .line 2125
    .local v2, result:I
    invoke-virtual {p0, p1}, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->enableString(I)Ljava/lang/String;

    #@15
    move-result-object v0

    #@16
    .line 2126
    .local v0, enableString:Ljava/lang/String;
    if-eqz v0, :cond_5

    #@18
    .line 2128
    :try_start_18
    iget-object v4, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@1a
    iget-object v4, v4, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->this$0:Lcom/android/server/connectivity/Tethering;

    #@1c
    invoke-static {v4}, Lcom/android/server/connectivity/Tethering;->access$3800(Lcom/android/server/connectivity/Tethering;)Landroid/net/IConnectivityManager;

    #@1f
    move-result-object v4

    #@20
    const/4 v5, 0x0

    #@21
    new-instance v6, Landroid/os/Binder;

    #@23
    invoke-direct {v6}, Landroid/os/Binder;-><init>()V

    #@26
    invoke-interface {v4, v5, v0, v6}, Landroid/net/IConnectivityManager;->startUsingNetworkFeature(ILjava/lang/String;Landroid/os/IBinder;)I
    :try_end_29
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_29} :catch_4d

    #@29
    move-result v2

    #@2a
    .line 2132
    :goto_2a
    packed-switch v2, :pswitch_data_50

    #@2d
    .line 2142
    const/4 v3, 0x0

    #@2e
    :goto_2e
    move v4, v3

    #@2f
    .line 2146
    goto :goto_5

    #@30
    .line 2135
    :pswitch_30
    iget-object v4, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@32
    invoke-static {v4, p1}, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->access$4402(Lcom/android/server/connectivity/Tethering$TetherMasterSM;I)I

    #@35
    .line 2136
    iget-object v4, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@37
    const/4 v5, 0x4

    #@38
    invoke-virtual {v4, v5}, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->obtainMessage(I)Landroid/os/Message;

    #@3b
    move-result-object v1

    #@3c
    .line 2137
    .local v1, m:Landroid/os/Message;
    iget-object v4, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@3e
    invoke-static {v4}, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->access$4504(Lcom/android/server/connectivity/Tethering$TetherMasterSM;)I

    #@41
    move-result v4

    #@42
    iput v4, v1, Landroid/os/Message;->arg1:I

    #@44
    .line 2138
    iget-object v4, p0, Lcom/android/server/connectivity/Tethering$TetherMasterSM$TetherMasterUtilState;->this$1:Lcom/android/server/connectivity/Tethering$TetherMasterSM;

    #@46
    const-wide/32 v5, 0x9c40

    #@49
    invoke-virtual {v4, v1, v5, v6}, Lcom/android/server/connectivity/Tethering$TetherMasterSM;->sendMessageDelayed(Landroid/os/Message;J)V

    #@4c
    goto :goto_2e

    #@4d
    .line 2130
    .end local v1           #m:Landroid/os/Message;
    :catch_4d
    move-exception v4

    #@4e
    goto :goto_2a

    #@4f
    .line 2132
    nop

    #@50
    :pswitch_data_50
    .packed-switch 0x0
        :pswitch_30
        :pswitch_30
    .end packed-switch
.end method
