.class final Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;
.super Ljava/lang/Object;
.source "VpnNamespace.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/connectivity/VpnNamespace;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "VpnSvrCommand"
.end annotation


# instance fields
.field private bout:Ljava/io/ByteArrayOutputStream;

.field private dout:Ljava/io/DataOutputStream;

.field private final in:Ljava/io/DataInputStream;

.field private final out:Ljava/io/DataOutputStream;

.field private final sock:Landroid/net/LocalSocket;

.field final synthetic this$0:Lcom/android/server/connectivity/VpnNamespace;


# direct methods
.method private constructor <init>(Lcom/android/server/connectivity/VpnNamespace;Ljava/lang/String;)V
    .registers 7
    .parameter
    .parameter "addr"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 248
    iput-object p1, p0, Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;->this$0:Lcom/android/server/connectivity/VpnNamespace;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 249
    invoke-direct {p0, p2}, Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;->initConnection(Ljava/lang/String;)Landroid/net/LocalSocket;

    #@8
    move-result-object v0

    #@9
    iput-object v0, p0, Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;->sock:Landroid/net/LocalSocket;

    #@b
    .line 250
    new-instance v0, Ljava/io/DataInputStream;

    #@d
    iget-object v1, p0, Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;->sock:Landroid/net/LocalSocket;

    #@f
    invoke-virtual {v1}, Landroid/net/LocalSocket;->getInputStream()Ljava/io/InputStream;

    #@12
    move-result-object v1

    #@13
    invoke-direct {v0, v1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    #@16
    iput-object v0, p0, Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;->in:Ljava/io/DataInputStream;

    #@18
    .line 251
    new-instance v0, Ljava/io/DataOutputStream;

    #@1a
    new-instance v1, Ljava/io/BufferedOutputStream;

    #@1c
    iget-object v2, p0, Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;->sock:Landroid/net/LocalSocket;

    #@1e
    invoke-virtual {v2}, Landroid/net/LocalSocket;->getOutputStream()Ljava/io/OutputStream;

    #@21
    move-result-object v2

    #@22
    const/16 v3, 0x2000

    #@24
    invoke-direct {v1, v2, v3}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V

    #@27
    invoke-direct {v0, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    #@2a
    iput-object v0, p0, Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;->out:Ljava/io/DataOutputStream;

    #@2c
    .line 252
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    #@2e
    const/4 v1, 0x0

    #@2f
    invoke-direct {v0, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    #@32
    iput-object v0, p0, Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;->bout:Ljava/io/ByteArrayOutputStream;

    #@34
    .line 253
    new-instance v0, Ljava/io/DataOutputStream;

    #@36
    iget-object v1, p0, Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;->bout:Ljava/io/ByteArrayOutputStream;

    #@38
    invoke-direct {v0, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    #@3b
    iput-object v0, p0, Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;->dout:Ljava/io/DataOutputStream;

    #@3d
    .line 254
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/connectivity/VpnNamespace;Ljava/lang/String;Lcom/android/server/connectivity/VpnNamespace$1;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 239
    invoke-direct {p0, p1, p2}, Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;-><init>(Lcom/android/server/connectivity/VpnNamespace;Ljava/lang/String;)V

    #@3
    return-void
.end method

.method private declared-synchronized execute(Lcom/android/server/connectivity/VpnNamespace$Cmd;Ljava/lang/Class;)Ljava/lang/Object;
    .registers 9
    .parameter "cmd"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/android/server/connectivity/VpnNamespace$Cmd;",
            "Ljava/lang/Class",
            "<*>;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 325
    .local p2, clazz:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    monitor-enter p0

    #@1
    :try_start_1
    invoke-static {p1}, Lcom/android/server/connectivity/VpnNamespace$Cmd;->access$200(Lcom/android/server/connectivity/VpnNamespace$Cmd;)Ljava/lang/Class;

    #@4
    move-result-object v3

    #@5
    invoke-virtual {p2, v3}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;
    :try_end_8
    .catchall {:try_start_1 .. :try_end_8} :catchall_70
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_8} :catch_73

    #@8
    .line 337
    :try_start_8
    iget-object v3, p0, Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;->dout:Ljava/io/DataOutputStream;

    #@a
    invoke-virtual {v3}, Ljava/io/DataOutputStream;->flush()V

    #@d
    .line 338
    iget-object v3, p0, Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;->bout:Ljava/io/ByteArrayOutputStream;

    #@f
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->size()I

    #@12
    move-result v2

    #@13
    .line 341
    .local v2, size:I
    iget-object v3, p0, Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;->out:Ljava/io/DataOutputStream;

    #@15
    invoke-static {p1, v3}, Lcom/android/server/connectivity/VpnNamespace$Cmd;->access$300(Lcom/android/server/connectivity/VpnNamespace$Cmd;Ljava/io/DataOutputStream;)V

    #@18
    .line 342
    iget-object v3, p0, Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;->out:Ljava/io/DataOutputStream;

    #@1a
    invoke-virtual {v3, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@1d
    .line 344
    if-eqz v2, :cond_2b

    #@1f
    .line 345
    iget-object v3, p0, Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;->bout:Ljava/io/ByteArrayOutputStream;

    #@21
    iget-object v4, p0, Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;->out:Ljava/io/DataOutputStream;

    #@23
    invoke-virtual {v3, v4}, Ljava/io/ByteArrayOutputStream;->writeTo(Ljava/io/OutputStream;)V

    #@26
    .line 346
    iget-object v3, p0, Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;->bout:Ljava/io/ByteArrayOutputStream;

    #@28
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->reset()V

    #@2b
    .line 350
    :cond_2b
    iget-object v3, p0, Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;->out:Ljava/io/DataOutputStream;

    #@2d
    invoke-virtual {v3}, Ljava/io/DataOutputStream;->flush()V

    #@30
    .line 353
    iget-object v3, p0, Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;->in:Ljava/io/DataInputStream;

    #@32
    invoke-virtual {v3}, Ljava/io/DataInputStream;->readInt()I

    #@35
    .line 354
    iget-object v3, p0, Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;->in:Ljava/io/DataInputStream;

    #@37
    invoke-virtual {v3}, Ljava/io/DataInputStream;->readInt()I

    #@3a
    move-result v2

    #@3b
    .line 356
    if-gtz v2, :cond_a3

    #@3d
    .line 357
    invoke-static {}, Lcom/android/server/connectivity/VpnNamespace;->access$100()Ljava/lang/String;

    #@40
    move-result-object v3

    #@41
    new-instance v4, Ljava/lang/StringBuilder;

    #@43
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@46
    const-string v5, "execute(): negative reply size = "

    #@48
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v4

    #@4c
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v4

    #@50
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@53
    move-result-object v4

    #@54
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@57
    .line 358
    new-instance v3, Ljava/io/IOException;

    #@59
    new-instance v4, Ljava/lang/StringBuilder;

    #@5b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@5e
    const-string v5, "invalid reply size "

    #@60
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v4

    #@64
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@67
    move-result-object v4

    #@68
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6b
    move-result-object v4

    #@6c
    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@6f
    throw v3
    :try_end_70
    .catchall {:try_start_8 .. :try_end_70} :catchall_70

    #@70
    .line 325
    .end local v2           #size:I
    :catchall_70
    move-exception v3

    #@71
    monitor-exit p0

    #@72
    throw v3

    #@73
    .line 326
    :catch_73
    move-exception v1

    #@74
    .line 329
    .local v1, cce:Ljava/lang/ClassCastException;
    :try_start_74
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@76
    new-instance v4, Ljava/lang/StringBuilder;

    #@78
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@7b
    const-string v5, "argument of type "

    #@7d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v4

    #@81
    invoke-static {p1}, Lcom/android/server/connectivity/VpnNamespace$Cmd;->access$200(Lcom/android/server/connectivity/VpnNamespace$Cmd;)Ljava/lang/Class;

    #@84
    move-result-object v5

    #@85
    invoke-virtual {v5}, Ljava/lang/Class;->toString()Ljava/lang/String;

    #@88
    move-result-object v5

    #@89
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v4

    #@8d
    const-string v5, " required for "

    #@8f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@92
    move-result-object v4

    #@93
    invoke-virtual {p1}, Lcom/android/server/connectivity/VpnNamespace$Cmd;->toString()Ljava/lang/String;

    #@96
    move-result-object v5

    #@97
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v4

    #@9b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9e
    move-result-object v4

    #@9f
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a2
    throw v3

    #@a3
    .line 361
    .end local v1           #cce:Ljava/lang/ClassCastException;
    .restart local v2       #size:I
    :cond_a3
    new-array v0, v2, [B

    #@a5
    .line 362
    .local v0, bytes:[B
    iget-object v3, p0, Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;->in:Ljava/io/DataInputStream;

    #@a7
    const/4 v4, 0x0

    #@a8
    invoke-virtual {v3, v0, v4, v2}, Ljava/io/DataInputStream;->readFully([BII)V

    #@ab
    .line 364
    new-instance v3, Ljava/io/DataInputStream;

    #@ad
    new-instance v4, Ljava/io/ByteArrayInputStream;

    #@af
    const/4 v5, 0x0

    #@b0
    invoke-direct {v4, v0, v5, v2}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    #@b3
    invoke-direct {v3, v4}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    #@b6
    invoke-virtual {p1, v3}, Lcom/android/server/connectivity/VpnNamespace$Cmd;->getReply(Ljava/io/DataInputStream;)Ljava/lang/Object;
    :try_end_b9
    .catchall {:try_start_74 .. :try_end_b9} :catchall_70

    #@b9
    move-result-object v3

    #@ba
    monitor-exit p0

    #@bb
    return-object v3
.end method

.method private initConnection(Ljava/lang/String;)Landroid/net/LocalSocket;
    .registers 9
    .parameter "addr"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 263
    const/16 v0, 0x1c

    #@2
    .line 265
    .local v0, MAX_CONNECTION_ATTEMPTS:I
    const/16 v1, 0x1c

    #@4
    .line 266
    .local v1, attemptsLeft:I
    new-instance v3, Landroid/net/LocalSocket;

    #@6
    invoke-direct {v3}, Landroid/net/LocalSocket;-><init>()V

    #@9
    .line 268
    .local v3, socket:Landroid/net/LocalSocket;
    :goto_9
    invoke-virtual {v3}, Landroid/net/LocalSocket;->isConnected()Z

    #@c
    move-result v4

    #@d
    if-nez v4, :cond_2f

    #@f
    if-lez v1, :cond_2f

    #@11
    .line 270
    :try_start_11
    new-instance v4, Landroid/net/LocalSocketAddress;

    #@13
    invoke-direct {v4, p1}, Landroid/net/LocalSocketAddress;-><init>(Ljava/lang/String;)V

    #@16
    invoke-virtual {v3, v4}, Landroid/net/LocalSocket;->connect(Landroid/net/LocalSocketAddress;)V
    :try_end_19
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_19} :catch_1a

    #@19
    goto :goto_9

    #@1a
    .line 271
    :catch_1a
    move-exception v2

    #@1b
    .line 272
    .local v2, ioe:Ljava/io/IOException;
    add-int/lit8 v1, v1, -0x1

    #@1d
    .line 273
    invoke-static {}, Lcom/android/server/connectivity/VpnNamespace;->access$100()Ljava/lang/String;

    #@20
    move-result-object v4

    #@21
    const-string v5, "mvpd is not up yet - let\'s wait a bit"

    #@23
    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 275
    const-wide/16 v4, 0xa

    #@28
    const/4 v6, 0x0

    #@29
    :try_start_29
    invoke-static {v4, v5, v6}, Ljava/lang/Thread;->sleep(JI)V
    :try_end_2c
    .catch Ljava/lang/InterruptedException; {:try_start_29 .. :try_end_2c} :catch_2d

    #@2c
    goto :goto_9

    #@2d
    .line 276
    :catch_2d
    move-exception v4

    #@2e
    goto :goto_9

    #@2f
    .line 281
    .end local v2           #ioe:Ljava/io/IOException;
    :cond_2f
    invoke-virtual {v3}, Landroid/net/LocalSocket;->isConnected()Z

    #@32
    move-result v4

    #@33
    if-nez v4, :cond_5f

    #@35
    .line 282
    invoke-static {}, Lcom/android/server/connectivity/VpnNamespace;->access$100()Ljava/lang/String;

    #@38
    move-result-object v4

    #@39
    new-instance v5, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string v6, "FATAL: bailed after "

    #@40
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v5

    #@44
    rsub-int/lit8 v6, v1, 0x1c

    #@46
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@49
    move-result-object v5

    #@4a
    const-string v6, " attempts"

    #@4c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v5

    #@50
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@53
    move-result-object v5

    #@54
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@57
    .line 283
    new-instance v4, Ljava/io/IOException;

    #@59
    const-string v5, "mvpd doesn\'t respond"

    #@5b
    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@5e
    throw v4

    #@5f
    .line 286
    :cond_5f
    return-object v3
.end method


# virtual methods
.method declared-synchronized destroy()V
    .registers 2

    #@0
    .prologue
    .line 373
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;->sock:Landroid/net/LocalSocket;

    #@3
    invoke-virtual {v0}, Landroid/net/LocalSocket;->isConnected()Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_e

    #@9
    .line 374
    iget-object v0, p0, Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;->sock:Landroid/net/LocalSocket;

    #@b
    invoke-virtual {v0}, Landroid/net/LocalSocket;->close()V

    #@e
    .line 376
    :cond_e
    iget-object v0, p0, Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;->in:Ljava/io/DataInputStream;

    #@10
    invoke-virtual {v0}, Ljava/io/DataInputStream;->close()V

    #@13
    .line 377
    iget-object v0, p0, Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;->out:Ljava/io/DataOutputStream;

    #@15
    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V

    #@18
    .line 378
    iget-object v0, p0, Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;->dout:Ljava/io/DataOutputStream;

    #@1a
    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V

    #@1d
    .line 379
    iget-object v0, p0, Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;->bout:Ljava/io/ByteArrayOutputStream;

    #@1f
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_22
    .catchall {:try_start_1 .. :try_end_22} :catchall_24
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_22} :catch_27

    #@22
    .line 382
    :goto_22
    monitor-exit p0

    #@23
    return-void

    #@24
    .line 373
    :catchall_24
    move-exception v0

    #@25
    monitor-exit p0

    #@26
    throw v0

    #@27
    .line 380
    :catch_27
    move-exception v0

    #@28
    goto :goto_22
.end method

.method declared-synchronized execute(Lcom/android/server/connectivity/VpnNamespace$Cmd;)Ljava/lang/Object;
    .registers 3
    .parameter "cmd"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/android/server/connectivity/VpnNamespace$Cmd;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 314
    monitor-enter p0

    #@1
    :try_start_1
    sget-object v0, Ljava/lang/Void;->TYPE:Ljava/lang/Class;

    #@3
    invoke-direct {p0, p1, v0}, Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;->execute(Lcom/android/server/connectivity/VpnNamespace$Cmd;Ljava/lang/Class;)Ljava/lang/Object;
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_9

    #@6
    move-result-object v0

    #@7
    monitor-exit p0

    #@8
    return-object v0

    #@9
    :catchall_9
    move-exception v0

    #@a
    monitor-exit p0

    #@b
    throw v0
.end method

.method declared-synchronized execute(Lcom/android/server/connectivity/VpnNamespace$Cmd;I)Ljava/lang/Object;
    .registers 4
    .parameter "cmd"
    .parameter "value"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/android/server/connectivity/VpnNamespace$Cmd;",
            "I)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 301
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;->dout:Ljava/io/DataOutputStream;

    #@3
    invoke-virtual {v0, p2}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@6
    .line 302
    sget-object v0, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    #@8
    invoke-direct {p0, p1, v0}, Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;->execute(Lcom/android/server/connectivity/VpnNamespace$Cmd;Ljava/lang/Class;)Ljava/lang/Object;
    :try_end_b
    .catchall {:try_start_1 .. :try_end_b} :catchall_e

    #@b
    move-result-object v0

    #@c
    monitor-exit p0

    #@d
    return-object v0

    #@e
    .line 301
    :catchall_e
    move-exception v0

    #@f
    monitor-exit p0

    #@10
    throw v0
.end method

.method declared-synchronized execute(Lcom/android/server/connectivity/VpnNamespace$Cmd;Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;)Ljava/lang/Object;
    .registers 4
    .parameter "cmd"
    .parameter "arg"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/android/server/connectivity/VpnNamespace$Cmd;",
            "Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 295
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;->dout:Ljava/io/DataOutputStream;

    #@3
    invoke-virtual {p2, v0}, Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;->writeObject(Ljava/io/DataOutputStream;)V

    #@6
    .line 296
    const-class v0, Lcom/android/server/connectivity/VpnNamespace$VpnNetIfAddr;

    #@8
    invoke-direct {p0, p1, v0}, Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;->execute(Lcom/android/server/connectivity/VpnNamespace$Cmd;Ljava/lang/Class;)Ljava/lang/Object;
    :try_end_b
    .catchall {:try_start_1 .. :try_end_b} :catchall_e

    #@b
    move-result-object v0

    #@c
    monitor-exit p0

    #@d
    return-object v0

    #@e
    .line 295
    :catchall_e
    move-exception v0

    #@f
    monitor-exit p0

    #@10
    throw v0
.end method

.method declared-synchronized execute(Lcom/android/server/connectivity/VpnNamespace$Cmd;Ljava/lang/String;)Ljava/lang/Object;
    .registers 5
    .parameter "cmd"
    .parameter "value"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/android/server/connectivity/VpnNamespace$Cmd;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 307
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;->dout:Ljava/io/DataOutputStream;

    #@3
    invoke-virtual {v0, p2}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    #@6
    .line 308
    iget-object v0, p0, Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;->dout:Ljava/io/DataOutputStream;

    #@8
    const/4 v1, 0x0

    #@9
    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeByte(I)V

    #@c
    .line 309
    const-class v0, Ljava/lang/String;

    #@e
    invoke-direct {p0, p1, v0}, Lcom/android/server/connectivity/VpnNamespace$VpnSvrCommand;->execute(Lcom/android/server/connectivity/VpnNamespace$Cmd;Ljava/lang/Class;)Ljava/lang/Object;
    :try_end_11
    .catchall {:try_start_1 .. :try_end_11} :catchall_14

    #@11
    move-result-object v0

    #@12
    monitor-exit p0

    #@13
    return-object v0

    #@14
    .line 307
    :catchall_14
    move-exception v0

    #@15
    monitor-exit p0

    #@16
    throw v0
.end method
