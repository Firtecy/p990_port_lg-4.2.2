.class Lcom/android/server/connectivity/VpnNamespace$TunnelMonitorThread;
.super Ljava/lang/Thread;
.source "VpnNamespace.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/connectivity/VpnNamespace;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TunnelMonitorThread"
.end annotation


# instance fields
.field private handler:Landroid/os/Handler;

.field private name:Ljava/lang/String;

.field final synthetic this$0:Lcom/android/server/connectivity/VpnNamespace;

.field private vpn:Lcom/android/server/connectivity/Vpn;


# direct methods
.method constructor <init>(Lcom/android/server/connectivity/VpnNamespace;Ljava/lang/String;Lcom/android/server/connectivity/Vpn;)V
    .registers 5
    .parameter
    .parameter "name"
    .parameter "vpn"

    #@0
    .prologue
    .line 707
    iput-object p1, p0, Lcom/android/server/connectivity/VpnNamespace$TunnelMonitorThread;->this$0:Lcom/android/server/connectivity/VpnNamespace;

    #@2
    .line 708
    const-string v0, "TunMonThread"

    #@4
    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    #@7
    .line 709
    iput-object p2, p0, Lcom/android/server/connectivity/VpnNamespace$TunnelMonitorThread;->name:Ljava/lang/String;

    #@9
    .line 710
    iput-object p3, p0, Lcom/android/server/connectivity/VpnNamespace$TunnelMonitorThread;->vpn:Lcom/android/server/connectivity/Vpn;

    #@b
    .line 711
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .registers 7
    .parameter "message"

    #@0
    .prologue
    .line 723
    if-eqz p1, :cond_39

    #@2
    .line 724
    iget v1, p1, Landroid/os/Message;->what:I

    #@4
    if-nez v1, :cond_6b

    #@6
    .line 726
    :try_start_6
    iget-object v1, p0, Lcom/android/server/connectivity/VpnNamespace$TunnelMonitorThread;->this$0:Lcom/android/server/connectivity/VpnNamespace;

    #@8
    iget-object v2, p0, Lcom/android/server/connectivity/VpnNamespace$TunnelMonitorThread;->name:Ljava/lang/String;

    #@a
    invoke-static {v1, v2}, Lcom/android/server/connectivity/VpnNamespace;->access$700(Lcom/android/server/connectivity/VpnNamespace;Ljava/lang/String;)Z

    #@d
    move-result v1

    #@e
    if-nez v1, :cond_3b

    #@10
    .line 727
    invoke-static {}, Lcom/android/server/connectivity/VpnNamespace;->access$100()Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    new-instance v2, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v3, "interface "

    #@1b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    iget-object v3, p0, Lcom/android/server/connectivity/VpnNamespace$TunnelMonitorThread;->name:Ljava/lang/String;

    #@21
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    const-string v3, " is gone"

    #@27
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v2

    #@2f
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 728
    iget-object v1, p0, Lcom/android/server/connectivity/VpnNamespace$TunnelMonitorThread;->vpn:Lcom/android/server/connectivity/Vpn;

    #@34
    iget-object v2, p0, Lcom/android/server/connectivity/VpnNamespace$TunnelMonitorThread;->name:Ljava/lang/String;

    #@36
    invoke-virtual {v1, v2}, Lcom/android/server/connectivity/Vpn;->interfaceRemoved(Ljava/lang/String;)V

    #@39
    .line 742
    :cond_39
    :goto_39
    const/4 v1, 0x1

    #@3a
    return v1

    #@3b
    .line 731
    :cond_3b
    iget-object v1, p0, Lcom/android/server/connectivity/VpnNamespace$TunnelMonitorThread;->handler:Landroid/os/Handler;

    #@3d
    const/4 v2, 0x0

    #@3e
    const-wide/16 v3, 0xbb8

    #@40
    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
    :try_end_43
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_43} :catch_44

    #@43
    goto :goto_39

    #@44
    .line 733
    :catch_44
    move-exception v0

    #@45
    .line 734
    .local v0, e:Ljava/io/IOException;
    invoke-static {}, Lcom/android/server/connectivity/VpnNamespace;->access$100()Ljava/lang/String;

    #@48
    move-result-object v1

    #@49
    new-instance v2, Ljava/lang/StringBuilder;

    #@4b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@4e
    const-string v3, "IOException while checking on tunnel:"

    #@50
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v2

    #@54
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    #@57
    move-result-object v3

    #@58
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v2

    #@5c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5f
    move-result-object v2

    #@60
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@63
    .line 736
    iget-object v1, p0, Lcom/android/server/connectivity/VpnNamespace$TunnelMonitorThread;->vpn:Lcom/android/server/connectivity/Vpn;

    #@65
    iget-object v2, p0, Lcom/android/server/connectivity/VpnNamespace$TunnelMonitorThread;->name:Ljava/lang/String;

    #@67
    invoke-virtual {v1, v2}, Lcom/android/server/connectivity/Vpn;->interfaceRemoved(Ljava/lang/String;)V

    #@6a
    goto :goto_39

    #@6b
    .line 738
    .end local v0           #e:Ljava/io/IOException;
    :cond_6b
    iget v1, p1, Landroid/os/Message;->what:I

    #@6d
    const/4 v2, -0x1

    #@6e
    if-ne v1, v2, :cond_39

    #@70
    .line 739
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@73
    move-result-object v1

    #@74
    invoke-virtual {v1}, Landroid/os/Looper;->quit()V

    #@77
    goto :goto_39
.end method

.method quit()V
    .registers 3

    #@0
    .prologue
    .line 718
    iget-object v0, p0, Lcom/android/server/connectivity/VpnNamespace$TunnelMonitorThread;->handler:Landroid/os/Handler;

    #@2
    const/4 v1, -0x1

    #@3
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@6
    .line 719
    return-void
.end method

.method restart()V
    .registers 5

    #@0
    .prologue
    .line 714
    iget-object v0, p0, Lcom/android/server/connectivity/VpnNamespace$TunnelMonitorThread;->handler:Landroid/os/Handler;

    #@2
    const/4 v1, 0x0

    #@3
    const-wide/16 v2, 0xbb8

    #@5
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@8
    .line 715
    return-void
.end method

.method public run()V
    .registers 4

    #@0
    .prologue
    .line 748
    invoke-static {}, Lcom/android/server/connectivity/VpnNamespace;->access$100()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    new-instance v1, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v2, "thread "

    #@b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {p0}, Lcom/android/server/connectivity/VpnNamespace$TunnelMonitorThread;->getName()Ljava/lang/String;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    const-string v2, " is starting"

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 749
    invoke-static {}, Landroid/os/Looper;->prepare()V

    #@27
    .line 750
    new-instance v0, Landroid/os/Handler;

    #@29
    invoke-direct {v0, p0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    #@2c
    iput-object v0, p0, Lcom/android/server/connectivity/VpnNamespace$TunnelMonitorThread;->handler:Landroid/os/Handler;

    #@2e
    .line 751
    iget-object v0, p0, Lcom/android/server/connectivity/VpnNamespace$TunnelMonitorThread;->handler:Landroid/os/Handler;

    #@30
    const/4 v1, 0x0

    #@31
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@34
    .line 752
    invoke-static {}, Landroid/os/Looper;->loop()V

    #@37
    .line 753
    invoke-static {}, Lcom/android/server/connectivity/VpnNamespace;->access$100()Ljava/lang/String;

    #@3a
    move-result-object v0

    #@3b
    new-instance v1, Ljava/lang/StringBuilder;

    #@3d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@40
    const-string v2, "thread "

    #@42
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v1

    #@46
    invoke-virtual {p0}, Lcom/android/server/connectivity/VpnNamespace$TunnelMonitorThread;->getName()Ljava/lang/String;

    #@49
    move-result-object v2

    #@4a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v1

    #@4e
    const-string v2, " is exiting"

    #@50
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v1

    #@54
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@57
    move-result-object v1

    #@58
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@5b
    .line 754
    return-void
.end method
