.class Lcom/android/server/connectivity/TetherNetwork$TetherNetworkReceiver;
.super Landroid/content/BroadcastReceiver;
.source "TetherNetwork.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/connectivity/TetherNetwork;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TetherNetworkReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/connectivity/TetherNetwork;


# direct methods
.method private constructor <init>(Lcom/android/server/connectivity/TetherNetwork;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 335
    iput-object p1, p0, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkReceiver;->this$0:Lcom/android/server/connectivity/TetherNetwork;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/connectivity/TetherNetwork;Lcom/android/server/connectivity/TetherNetwork$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 335
    invoke-direct {p0, p1}, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkReceiver;-><init>(Lcom/android/server/connectivity/TetherNetwork;)V

    #@3
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 13
    .parameter "ctx"
    .parameter "intent"

    #@0
    .prologue
    const-wide/16 v8, 0x1388

    #@2
    const/16 v7, 0x3fc

    #@4
    .line 339
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    .line 341
    .local v0, action:Ljava/lang/String;
    const-string v4, "TetherNetwork"

    #@a
    new-instance v5, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v6, "get Actions "

    #@11
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v5

    #@15
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v5

    #@19
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v5

    #@1d
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 343
    const-string v4, "android.net.wifi.WIFI_AP_STATE_CHANGED"

    #@22
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@25
    move-result v4

    #@26
    if-eqz v4, :cond_71

    #@28
    .line 344
    const-string v4, "wifi_state"

    #@2a
    invoke-virtual {p2, v4}, Landroid/content/Intent;->getExtra(Ljava/lang/String;)Ljava/lang/Object;

    #@2d
    .line 345
    const-string v4, "TetherNetwork"

    #@2f
    new-instance v5, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v6, "WIFI_AP_STATE_CHANGED_ACTION : "

    #@36
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v5

    #@3a
    const-string v6, "wifi_state"

    #@3c
    invoke-virtual {p2, v6}, Landroid/content/Intent;->getExtra(Ljava/lang/String;)Ljava/lang/Object;

    #@3f
    move-result-object v6

    #@40
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v5

    #@44
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v5

    #@48
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4b
    .line 346
    const-string v4, "wifi_state"

    #@4d
    invoke-virtual {p2, v4}, Landroid/content/Intent;->getExtra(Ljava/lang/String;)Ljava/lang/Object;

    #@50
    move-result-object v4

    #@51
    const/16 v5, 0xd

    #@53
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@56
    move-result-object v5

    #@57
    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@5a
    move-result v4

    #@5b
    if-eqz v4, :cond_5e

    #@5d
    .line 401
    :cond_5d
    :goto_5d
    return-void

    #@5e
    .line 348
    :cond_5e
    const-string v4, "wifi_state"

    #@60
    invoke-virtual {p2, v4}, Landroid/content/Intent;->getExtra(Ljava/lang/String;)Ljava/lang/Object;

    #@63
    move-result-object v4

    #@64
    const/16 v5, 0xb

    #@66
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@69
    move-result-object v5

    #@6a
    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@6d
    move-result v4

    #@6e
    if-eqz v4, :cond_5d

    #@70
    goto :goto_5d

    #@71
    .line 354
    :cond_71
    const-string v4, "android.intent.action.ANY_DATA_STATE"

    #@73
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@76
    move-result v4

    #@77
    if-eqz v4, :cond_11e

    #@79
    .line 356
    const-string v4, "state"

    #@7b
    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@7e
    move-result-object v3

    #@7f
    .line 357
    .local v3, state:Ljava/lang/String;
    const-string v4, "reason"

    #@81
    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@84
    move-result-object v2

    #@85
    .line 359
    .local v2, reason:Ljava/lang/String;
    const-string v4, "TetherNetwork"

    #@87
    new-instance v5, Ljava/lang/StringBuilder;

    #@89
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@8c
    const-string v6, "receive ACTION_ANY_DATA_CONNECTION_STATE_CHANGED to "

    #@8e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    move-result-object v5

    #@92
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v5

    #@96
    const-string v6, "Change reason is "

    #@98
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v5

    #@9c
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v5

    #@a0
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a3
    move-result-object v5

    #@a4
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a7
    .line 361
    iget-object v4, p0, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkReceiver;->this$0:Lcom/android/server/connectivity/TetherNetwork;

    #@a9
    invoke-static {v4}, Lcom/android/server/connectivity/TetherNetwork;->access$200(Lcom/android/server/connectivity/TetherNetwork;)Lcom/android/server/connectivity/TetherNetworkDataTransition;

    #@ac
    move-result-object v4

    #@ad
    if-eqz v4, :cond_5d

    #@af
    iget-object v4, p0, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkReceiver;->this$0:Lcom/android/server/connectivity/TetherNetwork;

    #@b1
    invoke-static {v4}, Lcom/android/server/connectivity/TetherNetwork;->access$200(Lcom/android/server/connectivity/TetherNetwork;)Lcom/android/server/connectivity/TetherNetworkDataTransition;

    #@b4
    move-result-object v4

    #@b5
    invoke-virtual {v4}, Lcom/android/server/connectivity/TetherNetworkDataTransition;->getTetherNetworkDataFlagSet()Z

    #@b8
    move-result v4

    #@b9
    const/4 v5, 0x1

    #@ba
    if-ne v4, v5, :cond_5d

    #@bc
    const-string v4, "DISCONNECTED"

    #@be
    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c1
    move-result v4

    #@c2
    if-eqz v4, :cond_5d

    #@c4
    .line 363
    const-string v4, "connectionMipErrorCheck"

    #@c6
    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c9
    move-result v4

    #@ca
    if-eqz v4, :cond_5d

    #@cc
    .line 364
    iget-object v4, p0, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkReceiver;->this$0:Lcom/android/server/connectivity/TetherNetwork;

    #@ce
    invoke-static {v4}, Lcom/android/server/connectivity/TetherNetwork;->access$200(Lcom/android/server/connectivity/TetherNetwork;)Lcom/android/server/connectivity/TetherNetworkDataTransition;

    #@d1
    move-result-object v4

    #@d2
    invoke-virtual {v4}, Lcom/android/server/connectivity/TetherNetworkDataTransition;->readMipErrorCode()I

    #@d5
    move-result v1

    #@d6
    .line 365
    .local v1, mipErrCode:I
    const-string v4, "TetherNetwork"

    #@d8
    new-instance v5, Ljava/lang/StringBuilder;

    #@da
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@dd
    const-string v6, "TETHER data connection failed : "

    #@df
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e2
    move-result-object v5

    #@e3
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e6
    move-result-object v5

    #@e7
    const-string v6, "errCode :"

    #@e9
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ec
    move-result-object v5

    #@ed
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f0
    move-result-object v5

    #@f1
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f4
    move-result-object v5

    #@f5
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@f8
    .line 367
    if-eqz v1, :cond_115

    #@fa
    .line 368
    const/16 v4, 0x43

    #@fc
    if-ne v1, v4, :cond_5d

    #@fe
    .line 369
    const-string v4, "TetherNetwork"

    #@100
    const-string v5, "Auth failed"

    #@102
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@105
    .line 370
    invoke-static {}, Lcom/android/server/connectivity/TetherNetwork;->access$300()Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;

    #@108
    move-result-object v4

    #@109
    invoke-virtual {v4, v7}, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;->removeMessages(I)V

    #@10c
    .line 371
    invoke-static {}, Lcom/android/server/connectivity/TetherNetwork;->access$300()Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;

    #@10f
    move-result-object v4

    #@110
    invoke-virtual {v4, v7, v8, v9}, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;->sendEmptyMessageDelayed(IJ)Z

    #@113
    goto/16 :goto_5d

    #@115
    .line 375
    :cond_115
    const-string v4, "TetherNetwork"

    #@117
    const-string v5, "TETHER data connection keep cause : readMipErrorCode == 0"

    #@119
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@11c
    goto/16 :goto_5d

    #@11e
    .line 380
    .end local v1           #mipErrCode:I
    .end local v2           #reason:Ljava/lang/String;
    .end local v3           #state:Ljava/lang/String;
    :cond_11e
    const-string v4, "com.lge.nai.Err.AuthFailed"

    #@120
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@123
    move-result v4

    #@124
    if-eqz v4, :cond_144

    #@126
    .line 381
    const-string v4, "TetherNetwork"

    #@128
    const-string v5, "Auth failed"

    #@12a
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@12d
    .line 383
    const-string v4, "TetherNetwork"

    #@12f
    const-string v5, "MSG_SHOW_MIP_ERR_DLG show Dialog for Mip Error"

    #@131
    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@134
    .line 385
    invoke-static {}, Lcom/android/server/connectivity/TetherNetwork;->access$300()Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;

    #@137
    move-result-object v4

    #@138
    invoke-virtual {v4, v7}, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;->removeMessages(I)V

    #@13b
    .line 386
    invoke-static {}, Lcom/android/server/connectivity/TetherNetwork;->access$300()Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;

    #@13e
    move-result-object v4

    #@13f
    invoke-virtual {v4, v7, v8, v9}, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;->sendEmptyMessageDelayed(IJ)Z

    #@142
    goto/16 :goto_5d

    #@144
    .line 390
    :cond_144
    const-string v4, "com.lge.nai.Err.Timeout"

    #@146
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@149
    move-result v4

    #@14a
    if-nez v4, :cond_154

    #@14c
    const-string v4, "com.lge.pamdisabled"

    #@14e
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@151
    move-result v4

    #@152
    if-eqz v4, :cond_168

    #@154
    .line 391
    :cond_154
    const-string v4, "TetherNetwork"

    #@156
    const-string v5, "Time out"

    #@158
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@15b
    .line 392
    invoke-static {}, Lcom/android/server/connectivity/TetherNetwork;->access$300()Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;

    #@15e
    move-result-object v4

    #@15f
    const/16 v5, 0x3fd

    #@161
    const-wide/16 v6, 0x0

    #@163
    invoke-virtual {v4, v5, v6, v7}, Lcom/android/server/connectivity/TetherNetwork$TetherNetworkHandler;->sendEmptyMessageDelayed(IJ)Z

    #@166
    goto/16 :goto_5d

    #@168
    .line 394
    :cond_168
    const-string v4, "com.lge.nai.Noti.Success"

    #@16a
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@16d
    move-result v4

    #@16e
    if-nez v4, :cond_5d

    #@170
    .line 397
    const-string v4, "com.lge.nai.Noti.Failed"

    #@172
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@175
    move-result v4

    #@176
    if-eqz v4, :cond_5d

    #@178
    goto/16 :goto_5d
.end method
