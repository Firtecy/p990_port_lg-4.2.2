.class public Lcom/android/server/connectivity/Tethering$EntitlementCheckService;
.super Landroid/app/Service;
.source "Tethering.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/connectivity/Tethering;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "EntitlementCheckService"
.end annotation


# static fields
.field public static final BLUETOOTH:I = 0x2

.field private static final ENTITLEMENT_FAIL_CAUSE:Ljava/lang/String; = "fail_cause"

.field private static final ENTITLEMENT_SUCCESS:Ljava/lang/String; = "success"

.field public static final FAILURE_CAUSECODE_33:I = 0x63

.field public static final HOTSPOT_CHECK_PAGE:Ljava/lang/String; = "entitlement.mobile.att.net/mhs1"

.field public static final HOTSPOT_CHECK_URL:Ljava/lang/String; = "http://entitlement.mobile.att.net/mhs1"

.field public static final TETHERING_CHECK_PAGE:Ljava/lang/String; = "entitlement.mobile.att.net/teth"

.field public static final TETHERING_CHECK_URL:Ljava/lang/String; = "http://entitlement.mobile.att.net/teth"

.field public static final USB:I = 0x1

.field public static final VIDEOCALLING_CHECK_PAGE:Ljava/lang/String; = "entitlement.mobile.att.net/gvc1"

.field public static final VIDEOCALLING_CHECK_URL:Ljava/lang/String; = "http://entitlement.mobile.att.net/gvc1"

.field public static final WIFI:I


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mAPNIntentFilter:Landroid/content/IntentFilter;

.field mCm:Landroid/net/ConnectivityManager;

.field private mEntitleType:I

.field private mEntitlementCheckType:I

.field public mHandler:Landroid/os/Handler;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field mWifiManager:Landroid/net/wifi/WifiManager;

.field private timeOutTask:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 2919
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    #@4
    .line 2920
    const-string v0, "Tethering"

    #@6
    iput-object v0, p0, Lcom/android/server/connectivity/Tethering$EntitlementCheckService;->TAG:Ljava/lang/String;

    #@8
    .line 2927
    iput v1, p0, Lcom/android/server/connectivity/Tethering$EntitlementCheckService;->mEntitleType:I

    #@a
    .line 2928
    iput v1, p0, Lcom/android/server/connectivity/Tethering$EntitlementCheckService;->mEntitlementCheckType:I

    #@c
    .line 2943
    new-instance v0, Landroid/os/Handler;

    #@e
    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    #@11
    iput-object v0, p0, Lcom/android/server/connectivity/Tethering$EntitlementCheckService;->mHandler:Landroid/os/Handler;

    #@13
    .line 2944
    new-instance v0, Lcom/android/server/connectivity/Tethering$EntitlementCheckService$1;

    #@15
    invoke-direct {v0, p0}, Lcom/android/server/connectivity/Tethering$EntitlementCheckService$1;-><init>(Lcom/android/server/connectivity/Tethering$EntitlementCheckService;)V

    #@18
    iput-object v0, p0, Lcom/android/server/connectivity/Tethering$EntitlementCheckService;->timeOutTask:Ljava/lang/Runnable;

    #@1a
    .line 3054
    new-instance v0, Lcom/android/server/connectivity/Tethering$EntitlementCheckService$3;

    #@1c
    invoke-direct {v0, p0}, Lcom/android/server/connectivity/Tethering$EntitlementCheckService$3;-><init>(Lcom/android/server/connectivity/Tethering$EntitlementCheckService;)V

    #@1f
    iput-object v0, p0, Lcom/android/server/connectivity/Tethering$EntitlementCheckService;->mReceiver:Landroid/content/BroadcastReceiver;

    #@21
    return-void
.end method

.method static synthetic access$7200(Lcom/android/server/connectivity/Tethering$EntitlementCheckService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 2919
    invoke-direct {p0}, Lcom/android/server/connectivity/Tethering$EntitlementCheckService;->performServiceLayerEntitlementCheck()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method private getEntitlementCheckType()I
    .registers 4

    #@0
    .prologue
    .line 3072
    invoke-static {}, Lcom/android/server/connectivity/Tethering;->access$900()Landroid/content/Context;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7
    move-result-object v0

    #@8
    const-string v1, "tether_entitlement_check_type"

    #@a
    const/4 v2, 0x2

    #@b
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@e
    move-result v0

    #@f
    return v0
.end method

.method private performServiceLayerEntitlementCheck()I
    .registers 13

    #@0
    .prologue
    const/4 v11, 0x1

    #@1
    .line 3076
    iget-object v8, p0, Lcom/android/server/connectivity/Tethering$EntitlementCheckService;->mCm:Landroid/net/ConnectivityManager;

    #@3
    invoke-virtual {v8, v11}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    #@6
    move-result-object v1

    #@7
    .line 3077
    .local v1, cur_wifi:Landroid/net/NetworkInfo;
    const/4 v4, 0x3

    #@8
    .line 3078
    .local v4, ret_code:I
    const/4 v7, 0x0

    #@9
    .line 3083
    .local v7, urlConnection:Ljava/net/HttpURLConnection;
    :try_start_9
    const-string v8, "Tethering"

    #@b
    new-instance v9, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v10, "[EntitlementCheck] HTTP Request with mEntitleType = "

    #@12
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v9

    #@16
    iget v10, p0, Lcom/android/server/connectivity/Tethering$EntitlementCheckService;->mEntitleType:I

    #@18
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v9

    #@1c
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v9

    #@20
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 3084
    iget v8, p0, Lcom/android/server/connectivity/Tethering$EntitlementCheckService;->mEntitleType:I

    #@25
    if-ne v8, v11, :cond_62

    #@27
    .line 3085
    new-instance v6, Ljava/net/URL;

    #@29
    const-string v8, "http://entitlement.mobile.att.net/teth"

    #@2b
    invoke-direct {v6, v8}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    #@2e
    .line 3092
    .local v6, url:Ljava/net/URL;
    :goto_2e
    invoke-virtual {v6}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    #@31
    move-result-object v8

    #@32
    move-object v0, v8

    #@33
    check-cast v0, Ljava/net/HttpURLConnection;

    #@35
    move-object v7, v0

    #@36
    .line 3093
    const/4 v8, 0x1

    #@37
    invoke-virtual {v7, v8}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    #@3a
    .line 3094
    const/16 v8, 0x2710

    #@3c
    invoke-virtual {v7, v8}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    #@3f
    .line 3095
    const/16 v8, 0x2710

    #@41
    invoke-virtual {v7, v8}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    #@44
    .line 3096
    const/4 v8, 0x0

    #@45
    invoke-virtual {v7, v8}, Ljava/net/HttpURLConnection;->setUseCaches(Z)V

    #@48
    .line 3097
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    #@4b
    .line 3099
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->getResponseCode()I

    #@4e
    move-result v3

    #@4f
    .line 3100
    .local v3, result:I
    const/16 v8, 0xc8

    #@51
    if-ne v3, v8, :cond_77

    #@53
    .line 3101
    const-string v8, "Tethering"

    #@55
    const-string v9, "[EntitlementCheck] ServiceLayerEntitlement  Success"

    #@57
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5a
    .catchall {:try_start_9 .. :try_end_5a} :catchall_f3
    .catch Ljava/net/UnknownHostException; {:try_start_9 .. :try_end_5a} :catch_8e
    .catch Ljava/net/ConnectException; {:try_start_9 .. :try_end_5a} :catch_af
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_5a} :catch_d0

    #@5a
    .line 3102
    const/4 v4, 0x0

    #@5b
    .line 3124
    :goto_5b
    if-eqz v7, :cond_60

    #@5d
    .line 3125
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->disconnect()V

    #@60
    :cond_60
    move v5, v4

    #@61
    .line 3122
    .end local v3           #result:I
    .end local v4           #ret_code:I
    .end local v6           #url:Ljava/net/URL;
    .local v5, ret_code:I
    :goto_61
    return v5

    #@62
    .line 3086
    .end local v5           #ret_code:I
    .restart local v4       #ret_code:I
    :cond_62
    :try_start_62
    iget v8, p0, Lcom/android/server/connectivity/Tethering$EntitlementCheckService;->mEntitleType:I

    #@64
    const/4 v9, 0x2

    #@65
    if-ne v8, v9, :cond_6f

    #@67
    .line 3087
    new-instance v6, Ljava/net/URL;

    #@69
    const-string v8, "http://entitlement.mobile.att.net/teth"

    #@6b
    invoke-direct {v6, v8}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    #@6e
    .restart local v6       #url:Ljava/net/URL;
    goto :goto_2e

    #@6f
    .line 3089
    .end local v6           #url:Ljava/net/URL;
    :cond_6f
    new-instance v6, Ljava/net/URL;

    #@71
    const-string v8, "http://entitlement.mobile.att.net/mhs1"

    #@73
    invoke-direct {v6, v8}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    #@76
    .restart local v6       #url:Ljava/net/URL;
    goto :goto_2e

    #@77
    .line 3103
    .restart local v3       #result:I
    :cond_77
    const/16 v8, 0x193

    #@79
    if-ne v3, v8, :cond_85

    #@7b
    .line 3104
    const-string v8, "Tethering"

    #@7d
    const-string v9, "[EntitlementCheck] ServiceLayerEntitlement  fail cause code 33"

    #@7f
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@82
    .line 3105
    const/16 v4, 0x63

    #@84
    goto :goto_5b

    #@85
    .line 3107
    :cond_85
    const-string v8, "Tethering"

    #@87
    const-string v9, "[EntitlementCheck] ServiceLayerEntitlement  fail temperal network problem"

    #@89
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8c
    .catchall {:try_start_62 .. :try_end_8c} :catchall_f3
    .catch Ljava/net/UnknownHostException; {:try_start_62 .. :try_end_8c} :catch_8e
    .catch Ljava/net/ConnectException; {:try_start_62 .. :try_end_8c} :catch_af
    .catch Ljava/io/IOException; {:try_start_62 .. :try_end_8c} :catch_d0

    #@8c
    .line 3108
    const/4 v4, 0x3

    #@8d
    goto :goto_5b

    #@8e
    .line 3111
    .end local v3           #result:I
    .end local v6           #url:Ljava/net/URL;
    :catch_8e
    move-exception v2

    #@8f
    .line 3112
    .local v2, e:Ljava/net/UnknownHostException;
    :try_start_8f
    const-string v8, "Tethering"

    #@91
    new-instance v9, Ljava/lang/StringBuilder;

    #@93
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@96
    const-string v10, "Entitlement check - "

    #@98
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v9

    #@9c
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v9

    #@a0
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a3
    move-result-object v9

    #@a4
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_a7
    .catchall {:try_start_8f .. :try_end_a7} :catchall_f3

    #@a7
    .line 3113
    const/4 v4, 0x3

    #@a8
    .line 3124
    if-eqz v7, :cond_ad

    #@aa
    .line 3125
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->disconnect()V

    #@ad
    :cond_ad
    move v5, v4

    #@ae
    .line 3114
    .end local v4           #ret_code:I
    .restart local v5       #ret_code:I
    goto :goto_61

    #@af
    .line 3115
    .end local v2           #e:Ljava/net/UnknownHostException;
    .end local v5           #ret_code:I
    .restart local v4       #ret_code:I
    :catch_af
    move-exception v2

    #@b0
    .line 3116
    .local v2, e:Ljava/net/ConnectException;
    :try_start_b0
    const-string v8, "Tethering"

    #@b2
    new-instance v9, Ljava/lang/StringBuilder;

    #@b4
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@b7
    const-string v10, "Entitlement check - "

    #@b9
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bc
    move-result-object v9

    #@bd
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v9

    #@c1
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c4
    move-result-object v9

    #@c5
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_c8
    .catchall {:try_start_b0 .. :try_end_c8} :catchall_f3

    #@c8
    .line 3117
    const/4 v4, 0x3

    #@c9
    .line 3124
    if-eqz v7, :cond_ce

    #@cb
    .line 3125
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->disconnect()V

    #@ce
    :cond_ce
    move v5, v4

    #@cf
    .line 3118
    .end local v4           #ret_code:I
    .restart local v5       #ret_code:I
    goto :goto_61

    #@d0
    .line 3119
    .end local v2           #e:Ljava/net/ConnectException;
    .end local v5           #ret_code:I
    .restart local v4       #ret_code:I
    :catch_d0
    move-exception v2

    #@d1
    .line 3120
    .local v2, e:Ljava/io/IOException;
    :try_start_d1
    const-string v8, "Tethering"

    #@d3
    new-instance v9, Ljava/lang/StringBuilder;

    #@d5
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@d8
    const-string v10, "Entitlement check - "

    #@da
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dd
    move-result-object v9

    #@de
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@e1
    move-result-object v9

    #@e2
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e5
    move-result-object v9

    #@e6
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_e9
    .catchall {:try_start_d1 .. :try_end_e9} :catchall_f3

    #@e9
    .line 3121
    const/16 v4, 0x63

    #@eb
    .line 3124
    if-eqz v7, :cond_f0

    #@ed
    .line 3125
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->disconnect()V

    #@f0
    :cond_f0
    move v5, v4

    #@f1
    .line 3122
    .end local v4           #ret_code:I
    .restart local v5       #ret_code:I
    goto/16 :goto_61

    #@f3
    .line 3124
    .end local v2           #e:Ljava/io/IOException;
    .end local v5           #ret_code:I
    .restart local v4       #ret_code:I
    :catchall_f3
    move-exception v8

    #@f4
    if-eqz v7, :cond_f9

    #@f6
    .line 3125
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->disconnect()V

    #@f9
    .line 3124
    :cond_f9
    throw v8
.end method


# virtual methods
.method public disableTethering()V
    .registers 11

    #@0
    .prologue
    const/4 v9, 0x1

    #@1
    const/4 v8, 0x0

    #@2
    .line 3038
    const-string v5, "Tethering"

    #@4
    const-string v6, "[EntitlementCheck] Disable Tethering"

    #@6
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 3039
    iget-object v5, p0, Lcom/android/server/connectivity/Tethering$EntitlementCheckService;->mCm:Landroid/net/ConnectivityManager;

    #@b
    invoke-virtual {v5}, Landroid/net/ConnectivityManager;->getTetheredIfaces()[Ljava/lang/String;

    #@e
    move-result-object v4

    #@f
    .line 3040
    .local v4, mIfaces:[Ljava/lang/String;
    move-object v0, v4

    #@10
    .local v0, arr$:[Ljava/lang/String;
    array-length v2, v0

    #@11
    .local v2, len$:I
    const/4 v1, 0x0

    #@12
    .local v1, i$:I
    :goto_12
    if-ge v1, v2, :cond_6d

    #@14
    aget-object v3, v0, v1

    #@16
    .line 3041
    .local v3, mIface:Ljava/lang/String;
    const-string v5, "Tethering"

    #@18
    new-instance v6, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v7, "[EntitlementCheck] Untethering  Interface =>"

    #@1f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v6

    #@23
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v6

    #@27
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v6

    #@2b
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    .line 3042
    iget-object v5, p0, Lcom/android/server/connectivity/Tethering$EntitlementCheckService;->mCm:Landroid/net/ConnectivityManager;

    #@30
    invoke-virtual {v5, v3}, Landroid/net/ConnectivityManager;->untether(Ljava/lang/String;)I

    #@33
    .line 3043
    const-string v5, "wlan0"

    #@35
    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@38
    move-result v5

    #@39
    if-eqz v5, :cond_6a

    #@3b
    .line 3044
    const-string v5, "Tethering"

    #@3d
    const-string v6, "[EntitlementCheck] Disable Mobile Hotspot"

    #@3f
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@42
    .line 3045
    iget-object v5, p0, Lcom/android/server/connectivity/Tethering$EntitlementCheckService;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@44
    const/4 v6, 0x0

    #@45
    invoke-virtual {v5, v6, v8}, Landroid/net/wifi/WifiManager;->setWifiApEnabled(Landroid/net/wifi/WifiConfiguration;Z)Z

    #@48
    .line 3046
    invoke-static {}, Lcom/android/server/connectivity/Tethering;->access$900()Landroid/content/Context;

    #@4b
    move-result-object v5

    #@4c
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@4f
    move-result-object v5

    #@50
    const-string v6, "wifi_saved_state"

    #@52
    invoke-static {v5, v6, v8}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@55
    move-result v5

    #@56
    if-ne v5, v9, :cond_6a

    #@58
    .line 3047
    iget-object v5, p0, Lcom/android/server/connectivity/Tethering$EntitlementCheckService;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@5a
    invoke-virtual {v5, v9}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    #@5d
    .line 3048
    invoke-static {}, Lcom/android/server/connectivity/Tethering;->access$900()Landroid/content/Context;

    #@60
    move-result-object v5

    #@61
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@64
    move-result-object v5

    #@65
    const-string v6, "wifi_saved_state"

    #@67
    invoke-static {v5, v6, v8}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@6a
    .line 3040
    :cond_6a
    add-int/lit8 v1, v1, 0x1

    #@6c
    goto :goto_12

    #@6d
    .line 3052
    .end local v3           #mIface:Ljava/lang/String;
    :cond_6d
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .registers 4
    .parameter "intent"

    #@0
    .prologue
    .line 3032
    const-string v0, "Tethering"

    #@2
    const-string v1, "[EntitlementCheck] onBind()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 3034
    const/4 v0, 0x0

    #@8
    return-object v0
.end method

.method public onCreate()V
    .registers 9

    #@0
    .prologue
    .line 2954
    const-string v5, "Tethering"

    #@2
    const-string v6, "[EntitlementCheck] onCreate()"

    #@4
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 2955
    const-string v5, "connectivity"

    #@9
    invoke-virtual {p0, v5}, Lcom/android/server/connectivity/Tethering$EntitlementCheckService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@c
    move-result-object v5

    #@d
    check-cast v5, Landroid/net/ConnectivityManager;

    #@f
    iput-object v5, p0, Lcom/android/server/connectivity/Tethering$EntitlementCheckService;->mCm:Landroid/net/ConnectivityManager;

    #@11
    .line 2956
    new-instance v5, Landroid/content/IntentFilter;

    #@13
    const-string v6, "android.net.conn.DATA_CONNECTED_STATUS"

    #@15
    invoke-direct {v5, v6}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@18
    iput-object v5, p0, Lcom/android/server/connectivity/Tethering$EntitlementCheckService;->mAPNIntentFilter:Landroid/content/IntentFilter;

    #@1a
    .line 2957
    iget-object v5, p0, Lcom/android/server/connectivity/Tethering$EntitlementCheckService;->mReceiver:Landroid/content/BroadcastReceiver;

    #@1c
    iget-object v6, p0, Lcom/android/server/connectivity/Tethering$EntitlementCheckService;->mAPNIntentFilter:Landroid/content/IntentFilter;

    #@1e
    invoke-virtual {p0, v5, v6}, Lcom/android/server/connectivity/Tethering$EntitlementCheckService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@21
    .line 2959
    const-string v5, "wifi"

    #@23
    invoke-virtual {p0, v5}, Lcom/android/server/connectivity/Tethering$EntitlementCheckService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@26
    move-result-object v5

    #@27
    check-cast v5, Landroid/net/wifi/WifiManager;

    #@29
    iput-object v5, p0, Lcom/android/server/connectivity/Tethering$EntitlementCheckService;->mWifiManager:Landroid/net/wifi/WifiManager;

    #@2b
    .line 2960
    invoke-direct {p0}, Lcom/android/server/connectivity/Tethering$EntitlementCheckService;->getEntitlementCheckType()I

    #@2e
    move-result v5

    #@2f
    iput v5, p0, Lcom/android/server/connectivity/Tethering$EntitlementCheckService;->mEntitlementCheckType:I

    #@31
    .line 2961
    const-string v5, "Tethering"

    #@33
    new-instance v6, Ljava/lang/StringBuilder;

    #@35
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    const-string v7, "[EntitlementCheck] onCreate(), mEntitlementCheckType = "

    #@3a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v6

    #@3e
    iget v7, p0, Lcom/android/server/connectivity/Tethering$EntitlementCheckService;->mEntitlementCheckType:I

    #@40
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@43
    move-result-object v6

    #@44
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v6

    #@48
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4b
    .line 2964
    iget-object v5, p0, Lcom/android/server/connectivity/Tethering$EntitlementCheckService;->mCm:Landroid/net/ConnectivityManager;

    #@4d
    invoke-virtual {v5}, Landroid/net/ConnectivityManager;->getTetheredIfaces()[Ljava/lang/String;

    #@50
    move-result-object v4

    #@51
    .line 2965
    .local v4, mIfaces:[Ljava/lang/String;
    move-object v0, v4

    #@52
    .local v0, arr$:[Ljava/lang/String;
    array-length v2, v0

    #@53
    .local v2, len$:I
    const/4 v1, 0x0

    #@54
    .local v1, i$:I
    :goto_54
    if-ge v1, v2, :cond_96

    #@56
    aget-object v3, v0, v1

    #@58
    .line 2966
    .local v3, mIface:Ljava/lang/String;
    const-string v5, "Tethering"

    #@5a
    new-instance v6, Ljava/lang/StringBuilder;

    #@5c
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@5f
    const-string v7, "[EntitlementCheck] onCreate() Find Interface =>"

    #@61
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v6

    #@65
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v6

    #@69
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6c
    move-result-object v6

    #@6d
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@70
    .line 2967
    const-string v5, "wlan0"

    #@72
    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@75
    move-result v5

    #@76
    if-eqz v5, :cond_7e

    #@78
    .line 2968
    const/4 v5, 0x0

    #@79
    iput v5, p0, Lcom/android/server/connectivity/Tethering$EntitlementCheckService;->mEntitleType:I

    #@7b
    .line 2965
    :cond_7b
    :goto_7b
    add-int/lit8 v1, v1, 0x1

    #@7d
    goto :goto_54

    #@7e
    .line 2969
    :cond_7e
    const-string v5, "usb0"

    #@80
    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@83
    move-result v5

    #@84
    if-eqz v5, :cond_8a

    #@86
    .line 2970
    const/4 v5, 0x1

    #@87
    iput v5, p0, Lcom/android/server/connectivity/Tethering$EntitlementCheckService;->mEntitleType:I

    #@89
    goto :goto_7b

    #@8a
    .line 2971
    :cond_8a
    const-string v5, "bt-pan"

    #@8c
    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8f
    move-result v5

    #@90
    if-eqz v5, :cond_7b

    #@92
    .line 2972
    const/4 v5, 0x2

    #@93
    iput v5, p0, Lcom/android/server/connectivity/Tethering$EntitlementCheckService;->mEntitleType:I

    #@95
    goto :goto_7b

    #@96
    .line 2976
    .end local v3           #mIface:Ljava/lang/String;
    :cond_96
    return-void
.end method

.method public onDestroy()V
    .registers 4

    #@0
    .prologue
    .line 3020
    const-string v0, "Tethering"

    #@2
    const-string v1, "[EntitlementCheck] onDestroy()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 3021
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering$EntitlementCheckService;->mReceiver:Landroid/content/BroadcastReceiver;

    #@9
    invoke-virtual {p0, v0}, Lcom/android/server/connectivity/Tethering$EntitlementCheckService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@c
    .line 3022
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering$EntitlementCheckService;->mHandler:Landroid/os/Handler;

    #@e
    iget-object v1, p0, Lcom/android/server/connectivity/Tethering$EntitlementCheckService;->timeOutTask:Ljava/lang/Runnable;

    #@10
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@13
    .line 3024
    iget v0, p0, Lcom/android/server/connectivity/Tethering$EntitlementCheckService;->mEntitlementCheckType:I

    #@15
    const/4 v1, 0x1

    #@16
    if-ne v0, v1, :cond_20

    #@18
    .line 3025
    iget-object v0, p0, Lcom/android/server/connectivity/Tethering$EntitlementCheckService;->mCm:Landroid/net/ConnectivityManager;

    #@1a
    const/4 v1, 0x0

    #@1b
    const-string v2, "enableENTITLEMENT"

    #@1d
    invoke-virtual {v0, v1, v2}, Landroid/net/ConnectivityManager;->stopUsingNetworkFeature(ILjava/lang/String;)I

    #@20
    .line 3027
    :cond_20
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .registers 11
    .parameter "intent"
    .parameter "flags"
    .parameter "startId"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    .line 2979
    iget v2, p0, Lcom/android/server/connectivity/Tethering$EntitlementCheckService;->mEntitlementCheckType:I

    #@3
    if-ne v2, v6, :cond_5d

    #@5
    .line 2980
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering$EntitlementCheckService;->mCm:Landroid/net/ConnectivityManager;

    #@7
    const/4 v3, 0x0

    #@8
    const-string v4, "enableENTITLEMENT"

    #@a
    invoke-virtual {v2, v3, v4}, Landroid/net/ConnectivityManager;->startUsingNetworkFeature(ILjava/lang/String;)I

    #@d
    move-result v1

    #@e
    .line 2982
    .local v1, result:I
    const/4 v2, 0x2

    #@f
    if-eq v1, v2, :cond_14

    #@11
    const/4 v2, 0x3

    #@12
    if-ne v1, v2, :cond_22

    #@14
    .line 2983
    :cond_14
    const-string v2, "Tethering"

    #@16
    const-string v3, "[EntitlementCheck] StartUsingNetwork failed   APN_REQUEST_FAILED"

    #@18
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 2984
    invoke-virtual {p0}, Lcom/android/server/connectivity/Tethering$EntitlementCheckService;->disableTethering()V

    #@1e
    .line 2985
    invoke-virtual {p0}, Lcom/android/server/connectivity/Tethering$EntitlementCheckService;->stopSelf()V

    #@21
    .line 3016
    .end local v1           #result:I
    :goto_21
    return v6

    #@22
    .line 2986
    .restart local v1       #result:I
    :cond_22
    if-nez v1, :cond_2f

    #@24
    .line 2987
    const-string v2, "Tethering"

    #@26
    const-string v3, "[EntitlementCheck] StartUsingNetwork APN_ALREADY_ACTIVE"

    #@28
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 2988
    invoke-virtual {p0}, Lcom/android/server/connectivity/Tethering$EntitlementCheckService;->stopSelf()V

    #@2e
    goto :goto_21

    #@2f
    .line 2990
    :cond_2f
    const-string v2, "Tethering"

    #@31
    new-instance v3, Ljava/lang/StringBuilder;

    #@33
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@36
    const-string v4, "[EntitlementCheck] onCreate  Entitlement Successfully tried ( "

    #@38
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v3

    #@3c
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v3

    #@40
    const-string v4, ")"

    #@42
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v3

    #@46
    const-string v4, "   Set TIMEOUT 30 sec"

    #@48
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v3

    #@4c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4f
    move-result-object v3

    #@50
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@53
    .line 2992
    iget-object v2, p0, Lcom/android/server/connectivity/Tethering$EntitlementCheckService;->mHandler:Landroid/os/Handler;

    #@55
    iget-object v3, p0, Lcom/android/server/connectivity/Tethering$EntitlementCheckService;->timeOutTask:Ljava/lang/Runnable;

    #@57
    const-wide/16 v4, 0x7530

    #@59
    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@5c
    goto :goto_21

    #@5d
    .line 2995
    .end local v1           #result:I
    :cond_5d
    new-instance v0, Ljava/lang/Thread;

    #@5f
    new-instance v2, Lcom/android/server/connectivity/Tethering$EntitlementCheckService$2;

    #@61
    invoke-direct {v2, p0}, Lcom/android/server/connectivity/Tethering$EntitlementCheckService$2;-><init>(Lcom/android/server/connectivity/Tethering$EntitlementCheckService;)V

    #@64
    invoke-direct {v0, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    #@67
    .line 3014
    .local v0, conn:Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    #@6a
    goto :goto_21
.end method
