.class Lcom/android/server/VibratorService$Vibration;
.super Ljava/lang/Object;
.source "VibratorService.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/VibratorService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Vibration"
.end annotation


# instance fields
.field private final mPattern:[J

.field private final mRepeat:I

.field private final mStartTime:J

.field private final mTimeout:J

.field private final mToken:Landroid/os/IBinder;

.field private final mUid:I

.field final synthetic this$0:Lcom/android/server/VibratorService;


# direct methods
.method constructor <init>(Lcom/android/server/VibratorService;Landroid/os/IBinder;JI)V
    .registers 14
    .parameter
    .parameter "token"
    .parameter "millis"
    .parameter "uid"

    #@0
    .prologue
    .line 80
    const/4 v5, 0x0

    #@1
    const/4 v6, 0x0

    #@2
    move-object v0, p0

    #@3
    move-object v1, p1

    #@4
    move-object v2, p2

    #@5
    move-wide v3, p3

    #@6
    move v7, p5

    #@7
    invoke-direct/range {v0 .. v7}, Lcom/android/server/VibratorService$Vibration;-><init>(Lcom/android/server/VibratorService;Landroid/os/IBinder;J[JII)V

    #@a
    .line 81
    return-void
.end method

.method private constructor <init>(Lcom/android/server/VibratorService;Landroid/os/IBinder;J[JII)V
    .registers 10
    .parameter
    .parameter "token"
    .parameter "millis"
    .parameter "pattern"
    .parameter "repeat"
    .parameter "uid"

    #@0
    .prologue
    .line 88
    iput-object p1, p0, Lcom/android/server/VibratorService$Vibration;->this$0:Lcom/android/server/VibratorService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 89
    iput-object p2, p0, Lcom/android/server/VibratorService$Vibration;->mToken:Landroid/os/IBinder;

    #@7
    .line 90
    iput-wide p3, p0, Lcom/android/server/VibratorService$Vibration;->mTimeout:J

    #@9
    .line 91
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@c
    move-result-wide v0

    #@d
    iput-wide v0, p0, Lcom/android/server/VibratorService$Vibration;->mStartTime:J

    #@f
    .line 92
    iput-object p5, p0, Lcom/android/server/VibratorService$Vibration;->mPattern:[J

    #@11
    .line 93
    iput p6, p0, Lcom/android/server/VibratorService$Vibration;->mRepeat:I

    #@13
    .line 94
    iput p7, p0, Lcom/android/server/VibratorService$Vibration;->mUid:I

    #@15
    .line 95
    return-void
.end method

.method constructor <init>(Lcom/android/server/VibratorService;Landroid/os/IBinder;[JII)V
    .registers 14
    .parameter
    .parameter "token"
    .parameter "pattern"
    .parameter "repeat"
    .parameter "uid"

    #@0
    .prologue
    .line 84
    const-wide/16 v3, 0x0

    #@2
    move-object v0, p0

    #@3
    move-object v1, p1

    #@4
    move-object v2, p2

    #@5
    move-object v5, p3

    #@6
    move v6, p4

    #@7
    move v7, p5

    #@8
    invoke-direct/range {v0 .. v7}, Lcom/android/server/VibratorService$Vibration;-><init>(Lcom/android/server/VibratorService;Landroid/os/IBinder;J[JII)V

    #@b
    .line 85
    return-void
.end method

.method static synthetic access$1100(Lcom/android/server/VibratorService$Vibration;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    iget v0, p0, Lcom/android/server/VibratorService$Vibration;->mRepeat:I

    #@2
    return v0
.end method

.method static synthetic access$500(Lcom/android/server/VibratorService$Vibration;)J
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 71
    iget-wide v0, p0, Lcom/android/server/VibratorService$Vibration;->mTimeout:J

    #@2
    return-wide v0
.end method

.method static synthetic access$600(Lcom/android/server/VibratorService$Vibration;)Landroid/os/IBinder;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    iget-object v0, p0, Lcom/android/server/VibratorService$Vibration;->mToken:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Lcom/android/server/VibratorService$Vibration;)[J
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    iget-object v0, p0, Lcom/android/server/VibratorService$Vibration;->mPattern:[J

    #@2
    return-object v0
.end method

.method static synthetic access$800(Lcom/android/server/VibratorService$Vibration;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    iget v0, p0, Lcom/android/server/VibratorService$Vibration;->mUid:I

    #@2
    return v0
.end method


# virtual methods
.method public binderDied()V
    .registers 3

    #@0
    .prologue
    .line 98
    iget-object v0, p0, Lcom/android/server/VibratorService$Vibration;->this$0:Lcom/android/server/VibratorService;

    #@2
    invoke-static {v0}, Lcom/android/server/VibratorService;->access$000(Lcom/android/server/VibratorService;)Ljava/util/LinkedList;

    #@5
    move-result-object v1

    #@6
    monitor-enter v1

    #@7
    .line 99
    :try_start_7
    iget-object v0, p0, Lcom/android/server/VibratorService$Vibration;->this$0:Lcom/android/server/VibratorService;

    #@9
    invoke-static {v0}, Lcom/android/server/VibratorService;->access$000(Lcom/android/server/VibratorService;)Ljava/util/LinkedList;

    #@c
    move-result-object v0

    #@d
    invoke-virtual {v0, p0}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    #@10
    .line 100
    iget-object v0, p0, Lcom/android/server/VibratorService$Vibration;->this$0:Lcom/android/server/VibratorService;

    #@12
    invoke-static {v0}, Lcom/android/server/VibratorService;->access$100(Lcom/android/server/VibratorService;)Lcom/android/server/VibratorService$Vibration;

    #@15
    move-result-object v0

    #@16
    if-ne p0, v0, :cond_22

    #@18
    .line 101
    iget-object v0, p0, Lcom/android/server/VibratorService$Vibration;->this$0:Lcom/android/server/VibratorService;

    #@1a
    invoke-static {v0}, Lcom/android/server/VibratorService;->access$200(Lcom/android/server/VibratorService;)V

    #@1d
    .line 102
    iget-object v0, p0, Lcom/android/server/VibratorService$Vibration;->this$0:Lcom/android/server/VibratorService;

    #@1f
    invoke-static {v0}, Lcom/android/server/VibratorService;->access$300(Lcom/android/server/VibratorService;)V

    #@22
    .line 104
    :cond_22
    monitor-exit v1

    #@23
    .line 105
    return-void

    #@24
    .line 104
    :catchall_24
    move-exception v0

    #@25
    monitor-exit v1
    :try_end_26
    .catchall {:try_start_7 .. :try_end_26} :catchall_24

    #@26
    throw v0
.end method

.method public hasLongerTimeout(J)Z
    .registers 8
    .parameter "millis"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 108
    iget-wide v1, p0, Lcom/android/server/VibratorService$Vibration;->mTimeout:J

    #@3
    const-wide/16 v3, 0x0

    #@5
    cmp-long v1, v1, v3

    #@7
    if-nez v1, :cond_a

    #@9
    .line 119
    :cond_9
    :goto_9
    return v0

    #@a
    .line 113
    :cond_a
    iget-wide v1, p0, Lcom/android/server/VibratorService$Vibration;->mStartTime:J

    #@c
    iget-wide v3, p0, Lcom/android/server/VibratorService$Vibration;->mTimeout:J

    #@e
    add-long/2addr v1, v3

    #@f
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@12
    move-result-wide v3

    #@13
    add-long/2addr v3, p1

    #@14
    cmp-long v1, v1, v3

    #@16
    if-ltz v1, :cond_9

    #@18
    .line 119
    const/4 v0, 0x1

    #@19
    goto :goto_9
.end method
