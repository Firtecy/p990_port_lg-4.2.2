.class Lcom/android/server/TelephonyRegistry;
.super Lcom/android/internal/telephony/ITelephonyRegistry$Stub;
.source "TelephonyRegistry.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/TelephonyRegistry$Record;
    }
.end annotation


# static fields
.field private static final DBG:Z = false

.field private static final DBG_LOC:Z = false

.field private static final MSG_USER_SWITCHED:I = 0x1

.field static final PHONE_STATE_PERMISSION_MASK:I = 0xec

.field private static final TAG:Ljava/lang/String; = "TelephonyRegistry"


# instance fields
.field private final mBatteryStats:Lcom/android/internal/app/IBatteryStats;

.field private final mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mCallForwarding:Z

.field private mCallIncomingNumber:Ljava/lang/String;

.field private mCallState:I

.field private mCellInfo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/telephony/CellInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mCellLocation:Landroid/os/Bundle;

.field private mConnectedApns:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;

.field private mDataActivity:I

.field private mDataConnectionApn:Ljava/lang/String;

.field private mDataConnectionLinkCapabilities:Landroid/net/LinkCapabilities;

.field private mDataConnectionLinkProperties:Landroid/net/LinkProperties;

.field private mDataConnectionNetworkType:I

.field private mDataConnectionPossible:Z

.field private mDataConnectionReason:Ljava/lang/String;

.field private mDataConnectionState:I

.field private final mHandler:Landroid/os/Handler;

.field private mMessageWaiting:Z

.field private mOtaspMode:I

.field private final mRecords:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/TelephonyRegistry$Record;",
            ">;"
        }
    .end annotation
.end field

.field private final mRemoveList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/IBinder;",
            ">;"
        }
    .end annotation
.end field

.field private mServiceState:Landroid/telephony/ServiceState;

.field private mSignalStrength:Landroid/telephony/SignalStrength;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .registers 5
    .parameter "context"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 168
    invoke-direct {p0}, Lcom/android/internal/telephony/ITelephonyRegistry$Stub;-><init>()V

    #@4
    .line 87
    new-instance v1, Ljava/util/ArrayList;

    #@6
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@9
    iput-object v1, p0, Lcom/android/server/TelephonyRegistry;->mRemoveList:Ljava/util/ArrayList;

    #@b
    .line 88
    new-instance v1, Ljava/util/ArrayList;

    #@d
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@10
    iput-object v1, p0, Lcom/android/server/TelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@12
    .line 92
    iput v2, p0, Lcom/android/server/TelephonyRegistry;->mCallState:I

    #@14
    .line 94
    const-string v1, ""

    #@16
    iput-object v1, p0, Lcom/android/server/TelephonyRegistry;->mCallIncomingNumber:Ljava/lang/String;

    #@18
    .line 96
    new-instance v1, Landroid/telephony/ServiceState;

    #@1a
    invoke-direct {v1}, Landroid/telephony/ServiceState;-><init>()V

    #@1d
    iput-object v1, p0, Lcom/android/server/TelephonyRegistry;->mServiceState:Landroid/telephony/ServiceState;

    #@1f
    .line 98
    new-instance v1, Landroid/telephony/SignalStrength;

    #@21
    invoke-direct {v1}, Landroid/telephony/SignalStrength;-><init>()V

    #@24
    iput-object v1, p0, Lcom/android/server/TelephonyRegistry;->mSignalStrength:Landroid/telephony/SignalStrength;

    #@26
    .line 100
    iput-boolean v2, p0, Lcom/android/server/TelephonyRegistry;->mMessageWaiting:Z

    #@28
    .line 102
    iput-boolean v2, p0, Lcom/android/server/TelephonyRegistry;->mCallForwarding:Z

    #@2a
    .line 104
    iput v2, p0, Lcom/android/server/TelephonyRegistry;->mDataActivity:I

    #@2c
    .line 106
    const/4 v1, -0x1

    #@2d
    iput v1, p0, Lcom/android/server/TelephonyRegistry;->mDataConnectionState:I

    #@2f
    .line 108
    iput-boolean v2, p0, Lcom/android/server/TelephonyRegistry;->mDataConnectionPossible:Z

    #@31
    .line 110
    const-string v1, ""

    #@33
    iput-object v1, p0, Lcom/android/server/TelephonyRegistry;->mDataConnectionReason:Ljava/lang/String;

    #@35
    .line 112
    const-string v1, ""

    #@37
    iput-object v1, p0, Lcom/android/server/TelephonyRegistry;->mDataConnectionApn:Ljava/lang/String;

    #@39
    .line 120
    new-instance v1, Landroid/os/Bundle;

    #@3b
    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    #@3e
    iput-object v1, p0, Lcom/android/server/TelephonyRegistry;->mCellLocation:Landroid/os/Bundle;

    #@40
    .line 124
    const/4 v1, 0x1

    #@41
    iput v1, p0, Lcom/android/server/TelephonyRegistry;->mOtaspMode:I

    #@43
    .line 126
    const/4 v1, 0x0

    #@44
    iput-object v1, p0, Lcom/android/server/TelephonyRegistry;->mCellInfo:Ljava/util/List;

    #@46
    .line 137
    new-instance v1, Lcom/android/server/TelephonyRegistry$1;

    #@48
    invoke-direct {v1, p0}, Lcom/android/server/TelephonyRegistry$1;-><init>(Lcom/android/server/TelephonyRegistry;)V

    #@4b
    iput-object v1, p0, Lcom/android/server/TelephonyRegistry;->mHandler:Landroid/os/Handler;

    #@4d
    .line 150
    new-instance v1, Lcom/android/server/TelephonyRegistry$2;

    #@4f
    invoke-direct {v1, p0}, Lcom/android/server/TelephonyRegistry$2;-><init>(Lcom/android/server/TelephonyRegistry;)V

    #@52
    iput-object v1, p0, Lcom/android/server/TelephonyRegistry;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@54
    .line 169
    invoke-static {}, Landroid/telephony/CellLocation;->getEmpty()Landroid/telephony/CellLocation;

    #@57
    move-result-object v0

    #@58
    .line 173
    .local v0, location:Landroid/telephony/CellLocation;
    if-eqz v0, :cond_5f

    #@5a
    .line 174
    iget-object v1, p0, Lcom/android/server/TelephonyRegistry;->mCellLocation:Landroid/os/Bundle;

    #@5c
    invoke-virtual {v0, v1}, Landroid/telephony/CellLocation;->fillInNotifierBundle(Landroid/os/Bundle;)V

    #@5f
    .line 176
    :cond_5f
    iput-object p1, p0, Lcom/android/server/TelephonyRegistry;->mContext:Landroid/content/Context;

    #@61
    .line 177
    invoke-static {}, Lcom/android/server/am/BatteryStatsService;->getService()Lcom/android/internal/app/IBatteryStats;

    #@64
    move-result-object v1

    #@65
    iput-object v1, p0, Lcom/android/server/TelephonyRegistry;->mBatteryStats:Lcom/android/internal/app/IBatteryStats;

    #@67
    .line 178
    new-instance v1, Ljava/util/ArrayList;

    #@69
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@6c
    iput-object v1, p0, Lcom/android/server/TelephonyRegistry;->mConnectedApns:Ljava/util/ArrayList;

    #@6e
    .line 179
    return-void
.end method

.method static synthetic access$000(Lcom/android/server/TelephonyRegistry;)Landroid/os/Bundle;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 61
    iget-object v0, p0, Lcom/android/server/TelephonyRegistry;->mCellLocation:Landroid/os/Bundle;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/server/TelephonyRegistry;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 61
    iget-object v0, p0, Lcom/android/server/TelephonyRegistry;->mHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method private broadcastCallStateChanged(ILjava/lang/String;)V
    .registers 9
    .parameter "state"
    .parameter "incomingNumber"

    #@0
    .prologue
    .line 694
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@3
    move-result-wide v0

    #@4
    .line 696
    .local v0, ident:J
    if-nez p1, :cond_37

    #@6
    .line 697
    :try_start_6
    iget-object v3, p0, Lcom/android/server/TelephonyRegistry;->mBatteryStats:Lcom/android/internal/app/IBatteryStats;

    #@8
    invoke-interface {v3}, Lcom/android/internal/app/IBatteryStats;->notePhoneOff()V
    :try_end_b
    .catchall {:try_start_6 .. :try_end_b} :catchall_3f
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_b} :catch_3d

    #@b
    .line 704
    :goto_b
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@e
    .line 707
    new-instance v2, Landroid/content/Intent;

    #@10
    const-string v3, "android.intent.action.PHONE_STATE"

    #@12
    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@15
    .line 708
    .local v2, intent:Landroid/content/Intent;
    const-string v3, "state"

    #@17
    invoke-static {p1}, Lcom/android/internal/telephony/DefaultPhoneNotifier;->convertCallState(I)Lcom/android/internal/telephony/PhoneConstants$State;

    #@1a
    move-result-object v4

    #@1b
    invoke-virtual {v4}, Lcom/android/internal/telephony/PhoneConstants$State;->toString()Ljava/lang/String;

    #@1e
    move-result-object v4

    #@1f
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@22
    .line 710
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@25
    move-result v3

    #@26
    if-nez v3, :cond_2d

    #@28
    .line 711
    const-string v3, "incoming_number"

    #@2a
    invoke-virtual {v2, v3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@2d
    .line 713
    :cond_2d
    iget-object v3, p0, Lcom/android/server/TelephonyRegistry;->mContext:Landroid/content/Context;

    #@2f
    sget-object v4, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@31
    const-string v5, "android.permission.READ_PHONE_STATE"

    #@33
    invoke-virtual {v3, v2, v4, v5}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;)V

    #@36
    .line 715
    return-void

    #@37
    .line 699
    .end local v2           #intent:Landroid/content/Intent;
    :cond_37
    :try_start_37
    iget-object v3, p0, Lcom/android/server/TelephonyRegistry;->mBatteryStats:Lcom/android/internal/app/IBatteryStats;

    #@39
    invoke-interface {v3}, Lcom/android/internal/app/IBatteryStats;->notePhoneOn()V
    :try_end_3c
    .catchall {:try_start_37 .. :try_end_3c} :catchall_3f
    .catch Landroid/os/RemoteException; {:try_start_37 .. :try_end_3c} :catch_3d

    #@3c
    goto :goto_b

    #@3d
    .line 701
    :catch_3d
    move-exception v3

    #@3e
    goto :goto_b

    #@3f
    .line 704
    :catchall_3f
    move-exception v3

    #@40
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@43
    throw v3
.end method

.method private broadcastDataConnectionFailed(Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "reason"
    .parameter "apnType"

    #@0
    .prologue
    .line 762
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "android.intent.action.DATA_CONNECTION_FAILED"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 763
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "reason"

    #@9
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@c
    .line 764
    const-string v1, "apnType"

    #@e
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@11
    .line 765
    iget-object v1, p0, Lcom/android/server/TelephonyRegistry;->mContext:Landroid/content/Context;

    #@13
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@15
    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@18
    .line 766
    return-void
.end method

.method private broadcastDataConnectionStateChanged(IZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/LinkProperties;Landroid/net/LinkCapabilities;ZI)V
    .registers 15
    .parameter "state"
    .parameter "isDataConnectivityPossible"
    .parameter "reason"
    .parameter "apn"
    .parameter "apnType"
    .parameter "linkProperties"
    .parameter "linkCapabilities"
    .parameter "roaming"
    .parameter "smCause"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 732
    new-instance v1, Landroid/content/Intent;

    #@3
    const-string v2, "android.intent.action.ANY_DATA_STATE"

    #@5
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@8
    .line 733
    .local v1, intent:Landroid/content/Intent;
    const-string v2, "state"

    #@a
    invoke-static {p1}, Lcom/android/internal/telephony/DefaultPhoneNotifier;->convertDataState(I)Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@d
    move-result-object v3

    #@e
    invoke-virtual {v3}, Lcom/android/internal/telephony/PhoneConstants$DataState;->toString()Ljava/lang/String;

    #@11
    move-result-object v3

    #@12
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@15
    .line 735
    if-nez p2, :cond_1c

    #@17
    .line 736
    const-string v2, "networkUnvailable"

    #@19
    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@1c
    .line 738
    :cond_1c
    if-eqz p3, :cond_23

    #@1e
    .line 739
    const-string v2, "reason"

    #@20
    invoke-virtual {v1, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@23
    .line 741
    :cond_23
    if-eqz p6, :cond_35

    #@25
    .line 742
    const-string v2, "linkProperties"

    #@27
    invoke-virtual {v1, v2, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@2a
    .line 743
    invoke-virtual {p6}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    #@2d
    move-result-object v0

    #@2e
    .line 744
    .local v0, iface:Ljava/lang/String;
    if-eqz v0, :cond_35

    #@30
    .line 745
    const-string v2, "iface"

    #@32
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@35
    .line 748
    .end local v0           #iface:Ljava/lang/String;
    :cond_35
    if-eqz p7, :cond_3c

    #@37
    .line 749
    const-string v2, "linkCapabilities"

    #@39
    invoke-virtual {v1, v2, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@3c
    .line 751
    :cond_3c
    if-eqz p8, :cond_43

    #@3e
    const-string v2, "networkRoaming"

    #@40
    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@43
    .line 753
    :cond_43
    const-string v2, "apn"

    #@45
    invoke-virtual {v1, v2, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@48
    .line 754
    const-string v2, "apnType"

    #@4a
    invoke-virtual {v1, v2, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@4d
    .line 756
    const-string v2, "smCause"

    #@4f
    invoke-virtual {v1, v2, p9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@52
    .line 758
    iget-object v2, p0, Lcom/android/server/TelephonyRegistry;->mContext:Landroid/content/Context;

    #@54
    sget-object v3, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@56
    invoke-virtual {v2, v1, v3}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@59
    .line 759
    return-void
.end method

.method private broadcastServiceStateChanged(Landroid/telephony/ServiceState;)V
    .registers 8
    .parameter "state"

    #@0
    .prologue
    .line 659
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@3
    move-result-wide v1

    #@4
    .line 661
    .local v1, ident:J
    :try_start_4
    iget-object v4, p0, Lcom/android/server/TelephonyRegistry;->mBatteryStats:Lcom/android/internal/app/IBatteryStats;

    #@6
    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getState()I

    #@9
    move-result v5

    #@a
    invoke-interface {v4, v5}, Lcom/android/internal/app/IBatteryStats;->notePhoneState(I)V
    :try_end_d
    .catchall {:try_start_4 .. :try_end_d} :catchall_2a
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_d} :catch_2f

    #@d
    .line 665
    :goto_d
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@10
    .line 668
    new-instance v3, Landroid/content/Intent;

    #@12
    const-string v4, "android.intent.action.SERVICE_STATE"

    #@14
    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@17
    .line 669
    .local v3, intent:Landroid/content/Intent;
    new-instance v0, Landroid/os/Bundle;

    #@19
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@1c
    .line 670
    .local v0, data:Landroid/os/Bundle;
    invoke-virtual {p1, v0}, Landroid/telephony/ServiceState;->fillInNotifierBundle(Landroid/os/Bundle;)V

    #@1f
    .line 671
    invoke-virtual {v3, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    #@22
    .line 672
    iget-object v4, p0, Lcom/android/server/TelephonyRegistry;->mContext:Landroid/content/Context;

    #@24
    sget-object v5, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@26
    invoke-virtual {v4, v3, v5}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@29
    .line 673
    return-void

    #@2a
    .line 665
    .end local v0           #data:Landroid/os/Bundle;
    .end local v3           #intent:Landroid/content/Intent;
    :catchall_2a
    move-exception v4

    #@2b
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@2e
    throw v4

    #@2f
    .line 662
    :catch_2f
    move-exception v4

    #@30
    goto :goto_d
.end method

.method private broadcastSignalStrengthChanged(Landroid/telephony/SignalStrength;)V
    .registers 8
    .parameter "signalStrength"

    #@0
    .prologue
    .line 676
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@3
    move-result-wide v1

    #@4
    .line 678
    .local v1, ident:J
    :try_start_4
    iget-object v4, p0, Lcom/android/server/TelephonyRegistry;->mBatteryStats:Lcom/android/internal/app/IBatteryStats;

    #@6
    invoke-interface {v4, p1}, Lcom/android/internal/app/IBatteryStats;->notePhoneSignalStrength(Landroid/telephony/SignalStrength;)V
    :try_end_9
    .catchall {:try_start_4 .. :try_end_9} :catchall_2b
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_30

    #@9
    .line 682
    :goto_9
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@c
    .line 685
    new-instance v3, Landroid/content/Intent;

    #@e
    const-string v4, "android.intent.action.SIG_STR"

    #@10
    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@13
    .line 686
    .local v3, intent:Landroid/content/Intent;
    const/high16 v4, 0x2000

    #@15
    invoke-virtual {v3, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@18
    .line 687
    new-instance v0, Landroid/os/Bundle;

    #@1a
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@1d
    .line 688
    .local v0, data:Landroid/os/Bundle;
    invoke-virtual {p1, v0}, Landroid/telephony/SignalStrength;->fillInNotifierBundle(Landroid/os/Bundle;)V

    #@20
    .line 689
    invoke-virtual {v3, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    #@23
    .line 690
    iget-object v4, p0, Lcom/android/server/TelephonyRegistry;->mContext:Landroid/content/Context;

    #@25
    sget-object v5, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@27
    invoke-virtual {v4, v3, v5}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@2a
    .line 691
    return-void

    #@2b
    .line 682
    .end local v0           #data:Landroid/os/Bundle;
    .end local v3           #intent:Landroid/content/Intent;
    :catchall_2b
    move-exception v4

    #@2c
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@2f
    throw v4

    #@30
    .line 679
    :catch_30
    move-exception v4

    #@31
    goto :goto_9
.end method

.method private checkListenerPermission(I)V
    .registers 5
    .parameter "events"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 780
    and-int/lit8 v0, p1, 0x10

    #@3
    if-eqz v0, :cond_c

    #@5
    .line 781
    iget-object v0, p0, Lcom/android/server/TelephonyRegistry;->mContext:Landroid/content/Context;

    #@7
    const-string v1, "android.permission.ACCESS_COARSE_LOCATION"

    #@9
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@c
    .line 786
    :cond_c
    and-int/lit16 v0, p1, 0x400

    #@e
    if-eqz v0, :cond_17

    #@10
    .line 787
    iget-object v0, p0, Lcom/android/server/TelephonyRegistry;->mContext:Landroid/content/Context;

    #@12
    const-string v1, "android.permission.ACCESS_COARSE_LOCATION"

    #@14
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@17
    .line 792
    :cond_17
    and-int/lit16 v0, p1, 0xec

    #@19
    if-eqz v0, :cond_22

    #@1b
    .line 793
    iget-object v0, p0, Lcom/android/server/TelephonyRegistry;->mContext:Landroid/content/Context;

    #@1d
    const-string v1, "android.permission.READ_PHONE_STATE"

    #@1f
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@22
    .line 796
    :cond_22
    return-void
.end method

.method private checkNotifyPermission(Ljava/lang/String;)Z
    .registers 5
    .parameter "method"

    #@0
    .prologue
    .line 769
    iget-object v1, p0, Lcom/android/server/TelephonyRegistry;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.MODIFY_PHONE_STATE"

    #@4
    invoke-virtual {v1, v2}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@7
    move-result v1

    #@8
    if-nez v1, :cond_c

    #@a
    .line 771
    const/4 v1, 0x1

    #@b
    .line 776
    :goto_b
    return v1

    #@c
    .line 773
    :cond_c
    new-instance v1, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v2, "Modify Phone State Permission Denial: "

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    const-string v2, " from pid="

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@24
    move-result v2

    #@25
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v1

    #@29
    const-string v2, ", uid="

    #@2b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v1

    #@2f
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@32
    move-result v2

    #@33
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@36
    move-result-object v1

    #@37
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v0

    #@3b
    .line 776
    .local v0, msg:Ljava/lang/String;
    const/4 v1, 0x0

    #@3c
    goto :goto_b
.end method

.method private handleRemoveListLocked()V
    .registers 4

    #@0
    .prologue
    .line 799
    iget-object v2, p0, Lcom/android/server/TelephonyRegistry;->mRemoveList:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v2

    #@6
    if-lez v2, :cond_23

    #@8
    .line 800
    iget-object v2, p0, Lcom/android/server/TelephonyRegistry;->mRemoveList:Ljava/util/ArrayList;

    #@a
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@d
    move-result-object v1

    #@e
    .local v1, i$:Ljava/util/Iterator;
    :goto_e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@11
    move-result v2

    #@12
    if-eqz v2, :cond_1e

    #@14
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@17
    move-result-object v0

    #@18
    check-cast v0, Landroid/os/IBinder;

    #@1a
    .line 801
    .local v0, b:Landroid/os/IBinder;
    invoke-direct {p0, v0}, Lcom/android/server/TelephonyRegistry;->remove(Landroid/os/IBinder;)V

    #@1d
    goto :goto_e

    #@1e
    .line 803
    .end local v0           #b:Landroid/os/IBinder;
    :cond_1e
    iget-object v2, p0, Lcom/android/server/TelephonyRegistry;->mRemoveList:Ljava/util/ArrayList;

    #@20
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    #@23
    .line 805
    .end local v1           #i$:Ljava/util/Iterator;
    :cond_23
    return-void
.end method

.method private remove(Landroid/os/IBinder;)V
    .registers 6
    .parameter "binder"

    #@0
    .prologue
    .line 316
    iget-object v3, p0, Lcom/android/server/TelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@2
    monitor-enter v3

    #@3
    .line 317
    :try_start_3
    iget-object v2, p0, Lcom/android/server/TelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@8
    move-result v1

    #@9
    .line 318
    .local v1, recordCount:I
    const/4 v0, 0x0

    #@a
    .local v0, i:I
    :goto_a
    if-ge v0, v1, :cond_22

    #@c
    .line 319
    iget-object v2, p0, Lcom/android/server/TelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@e
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@11
    move-result-object v2

    #@12
    check-cast v2, Lcom/android/server/TelephonyRegistry$Record;

    #@14
    iget-object v2, v2, Lcom/android/server/TelephonyRegistry$Record;->binder:Landroid/os/IBinder;

    #@16
    if-ne v2, p1, :cond_1f

    #@18
    .line 320
    iget-object v2, p0, Lcom/android/server/TelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@1a
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@1d
    .line 321
    monitor-exit v3

    #@1e
    .line 325
    :goto_1e
    return-void

    #@1f
    .line 318
    :cond_1f
    add-int/lit8 v0, v0, 0x1

    #@21
    goto :goto_a

    #@22
    .line 324
    :cond_22
    monitor-exit v3

    #@23
    goto :goto_1e

    #@24
    .end local v0           #i:I
    .end local v1           #recordCount:I
    :catchall_24
    move-exception v2

    #@25
    monitor-exit v3
    :try_end_26
    .catchall {:try_start_3 .. :try_end_26} :catchall_24

    #@26
    throw v2
.end method

.method private validateEventsAndUserLocked(Lcom/android/server/TelephonyRegistry$Record;I)Z
    .registers 8
    .parameter "r"
    .parameter "events"

    #@0
    .prologue
    .line 809
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@3
    move-result-wide v0

    #@4
    .line 810
    .local v0, callingIdentity:J
    const/4 v3, 0x0

    #@5
    .line 812
    .local v3, valid:Z
    :try_start_5
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    #@8
    move-result v2

    #@9
    .line 813
    .local v2, foregroundUser:I
    iget v4, p1, Lcom/android/server/TelephonyRegistry$Record;->callerUid:I

    #@b
    if-ne v4, v2, :cond_17

    #@d
    iget v4, p1, Lcom/android/server/TelephonyRegistry$Record;->events:I
    :try_end_f
    .catchall {:try_start_5 .. :try_end_f} :catchall_19

    #@f
    and-int/2addr v4, p2

    #@10
    if-eqz v4, :cond_17

    #@12
    const/4 v3, 0x1

    #@13
    .line 820
    :goto_13
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@16
    .line 822
    return v3

    #@17
    .line 813
    :cond_17
    const/4 v3, 0x0

    #@18
    goto :goto_13

    #@19
    .line 820
    .end local v2           #foregroundUser:I
    :catchall_19
    move-exception v4

    #@1a
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@1d
    throw v4
.end method


# virtual methods
.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 10
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 623
    iget-object v3, p0, Lcom/android/server/TelephonyRegistry;->mContext:Landroid/content/Context;

    #@2
    const-string v4, "android.permission.DUMP"

    #@4
    invoke-virtual {v3, v4}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@7
    move-result v3

    #@8
    if-eqz v3, :cond_33

    #@a
    .line 625
    new-instance v3, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v4, "Permission Denial: can\'t dump telephony.registry from from pid="

    #@11
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v3

    #@15
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@18
    move-result v4

    #@19
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    const-string v4, ", uid="

    #@1f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@26
    move-result v4

    #@27
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v3

    #@2f
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@32
    .line 652
    :goto_32
    return-void

    #@33
    .line 629
    :cond_33
    iget-object v4, p0, Lcom/android/server/TelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@35
    monitor-enter v4

    #@36
    .line 630
    :try_start_36
    iget-object v3, p0, Lcom/android/server/TelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@38
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@3b
    move-result v2

    #@3c
    .line 631
    .local v2, recordCount:I
    const-string v3, "last known state:"

    #@3e
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@41
    .line 632
    new-instance v3, Ljava/lang/StringBuilder;

    #@43
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@46
    const-string v5, "  mCallState="

    #@48
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v3

    #@4c
    iget v5, p0, Lcom/android/server/TelephonyRegistry;->mCallState:I

    #@4e
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@51
    move-result-object v3

    #@52
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@55
    move-result-object v3

    #@56
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@59
    .line 633
    new-instance v3, Ljava/lang/StringBuilder;

    #@5b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@5e
    const-string v5, "  mCallIncomingNumber="

    #@60
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v3

    #@64
    iget-object v5, p0, Lcom/android/server/TelephonyRegistry;->mCallIncomingNumber:Ljava/lang/String;

    #@66
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v3

    #@6a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6d
    move-result-object v3

    #@6e
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@71
    .line 634
    new-instance v3, Ljava/lang/StringBuilder;

    #@73
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@76
    const-string v5, "  mServiceState="

    #@78
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v3

    #@7c
    iget-object v5, p0, Lcom/android/server/TelephonyRegistry;->mServiceState:Landroid/telephony/ServiceState;

    #@7e
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v3

    #@82
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@85
    move-result-object v3

    #@86
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@89
    .line 635
    new-instance v3, Ljava/lang/StringBuilder;

    #@8b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@8e
    const-string v5, "  mSignalStrength="

    #@90
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@93
    move-result-object v3

    #@94
    iget-object v5, p0, Lcom/android/server/TelephonyRegistry;->mSignalStrength:Landroid/telephony/SignalStrength;

    #@96
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@99
    move-result-object v3

    #@9a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9d
    move-result-object v3

    #@9e
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@a1
    .line 636
    new-instance v3, Ljava/lang/StringBuilder;

    #@a3
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@a6
    const-string v5, "  mMessageWaiting="

    #@a8
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v3

    #@ac
    iget-boolean v5, p0, Lcom/android/server/TelephonyRegistry;->mMessageWaiting:Z

    #@ae
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v3

    #@b2
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b5
    move-result-object v3

    #@b6
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@b9
    .line 637
    new-instance v3, Ljava/lang/StringBuilder;

    #@bb
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@be
    const-string v5, "  mCallForwarding="

    #@c0
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v3

    #@c4
    iget-boolean v5, p0, Lcom/android/server/TelephonyRegistry;->mCallForwarding:Z

    #@c6
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@c9
    move-result-object v3

    #@ca
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cd
    move-result-object v3

    #@ce
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@d1
    .line 638
    new-instance v3, Ljava/lang/StringBuilder;

    #@d3
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@d6
    const-string v5, "  mDataActivity="

    #@d8
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@db
    move-result-object v3

    #@dc
    iget v5, p0, Lcom/android/server/TelephonyRegistry;->mDataActivity:I

    #@de
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e1
    move-result-object v3

    #@e2
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e5
    move-result-object v3

    #@e6
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@e9
    .line 639
    new-instance v3, Ljava/lang/StringBuilder;

    #@eb
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@ee
    const-string v5, "  mDataConnectionState="

    #@f0
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f3
    move-result-object v3

    #@f4
    iget v5, p0, Lcom/android/server/TelephonyRegistry;->mDataConnectionState:I

    #@f6
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f9
    move-result-object v3

    #@fa
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fd
    move-result-object v3

    #@fe
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@101
    .line 640
    new-instance v3, Ljava/lang/StringBuilder;

    #@103
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@106
    const-string v5, "  mDataConnectionPossible="

    #@108
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10b
    move-result-object v3

    #@10c
    iget-boolean v5, p0, Lcom/android/server/TelephonyRegistry;->mDataConnectionPossible:Z

    #@10e
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@111
    move-result-object v3

    #@112
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@115
    move-result-object v3

    #@116
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@119
    .line 641
    new-instance v3, Ljava/lang/StringBuilder;

    #@11b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@11e
    const-string v5, "  mDataConnectionReason="

    #@120
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@123
    move-result-object v3

    #@124
    iget-object v5, p0, Lcom/android/server/TelephonyRegistry;->mDataConnectionReason:Ljava/lang/String;

    #@126
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@129
    move-result-object v3

    #@12a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12d
    move-result-object v3

    #@12e
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@131
    .line 642
    new-instance v3, Ljava/lang/StringBuilder;

    #@133
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@136
    const-string v5, "  mDataConnectionApn="

    #@138
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13b
    move-result-object v3

    #@13c
    iget-object v5, p0, Lcom/android/server/TelephonyRegistry;->mDataConnectionApn:Ljava/lang/String;

    #@13e
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@141
    move-result-object v3

    #@142
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@145
    move-result-object v3

    #@146
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@149
    .line 643
    new-instance v3, Ljava/lang/StringBuilder;

    #@14b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@14e
    const-string v5, "  mDataConnectionLinkProperties="

    #@150
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@153
    move-result-object v3

    #@154
    iget-object v5, p0, Lcom/android/server/TelephonyRegistry;->mDataConnectionLinkProperties:Landroid/net/LinkProperties;

    #@156
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@159
    move-result-object v3

    #@15a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15d
    move-result-object v3

    #@15e
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@161
    .line 644
    new-instance v3, Ljava/lang/StringBuilder;

    #@163
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@166
    const-string v5, "  mDataConnectionLinkCapabilities="

    #@168
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16b
    move-result-object v3

    #@16c
    iget-object v5, p0, Lcom/android/server/TelephonyRegistry;->mDataConnectionLinkCapabilities:Landroid/net/LinkCapabilities;

    #@16e
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@171
    move-result-object v3

    #@172
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@175
    move-result-object v3

    #@176
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@179
    .line 645
    new-instance v3, Ljava/lang/StringBuilder;

    #@17b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@17e
    const-string v5, "  mCellLocation="

    #@180
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@183
    move-result-object v3

    #@184
    iget-object v5, p0, Lcom/android/server/TelephonyRegistry;->mCellLocation:Landroid/os/Bundle;

    #@186
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@189
    move-result-object v3

    #@18a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18d
    move-result-object v3

    #@18e
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@191
    .line 646
    new-instance v3, Ljava/lang/StringBuilder;

    #@193
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@196
    const-string v5, "  mCellInfo="

    #@198
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19b
    move-result-object v3

    #@19c
    iget-object v5, p0, Lcom/android/server/TelephonyRegistry;->mCellInfo:Ljava/util/List;

    #@19e
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a1
    move-result-object v3

    #@1a2
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a5
    move-result-object v3

    #@1a6
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1a9
    .line 647
    new-instance v3, Ljava/lang/StringBuilder;

    #@1ab
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1ae
    const-string v5, "registrations: count="

    #@1b0
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b3
    move-result-object v3

    #@1b4
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b7
    move-result-object v3

    #@1b8
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1bb
    move-result-object v3

    #@1bc
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1bf
    .line 648
    iget-object v3, p0, Lcom/android/server/TelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@1c1
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@1c4
    move-result-object v0

    #@1c5
    .local v0, i$:Ljava/util/Iterator;
    :goto_1c5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@1c8
    move-result v3

    #@1c9
    if-eqz v3, :cond_1fd

    #@1cb
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1ce
    move-result-object v1

    #@1cf
    check-cast v1, Lcom/android/server/TelephonyRegistry$Record;

    #@1d1
    .line 649
    .local v1, r:Lcom/android/server/TelephonyRegistry$Record;
    new-instance v3, Ljava/lang/StringBuilder;

    #@1d3
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1d6
    const-string v5, "  "

    #@1d8
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1db
    move-result-object v3

    #@1dc
    iget-object v5, v1, Lcom/android/server/TelephonyRegistry$Record;->pkgForDebug:Ljava/lang/String;

    #@1de
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e1
    move-result-object v3

    #@1e2
    const-string v5, " 0x"

    #@1e4
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e7
    move-result-object v3

    #@1e8
    iget v5, v1, Lcom/android/server/TelephonyRegistry$Record;->events:I

    #@1ea
    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@1ed
    move-result-object v5

    #@1ee
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f1
    move-result-object v3

    #@1f2
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f5
    move-result-object v3

    #@1f6
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1f9
    goto :goto_1c5

    #@1fa
    .line 651
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #r:Lcom/android/server/TelephonyRegistry$Record;
    .end local v2           #recordCount:I
    :catchall_1fa
    move-exception v3

    #@1fb
    monitor-exit v4
    :try_end_1fc
    .catchall {:try_start_36 .. :try_end_1fc} :catchall_1fa

    #@1fc
    throw v3

    #@1fd
    .restart local v0       #i$:Ljava/util/Iterator;
    .restart local v2       #recordCount:I
    :cond_1fd
    :try_start_1fd
    monitor-exit v4
    :try_end_1fe
    .catchall {:try_start_1fd .. :try_end_1fe} :catchall_1fa

    #@1fe
    goto/16 :goto_32
.end method

.method public listen(Ljava/lang/String;Lcom/android/internal/telephony/IPhoneStateListener;IZ)V
    .registers 20
    .parameter "pkgForDebug"
    .parameter "callback"
    .parameter "events"
    .parameter "notifyNow"

    #@0
    .prologue
    .line 192
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@3
    move-result v3

    #@4
    .line 193
    .local v3, callerUid:I
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@7
    move-result v7

    #@8
    .line 199
    .local v7, myUid:I
    if-eqz p3, :cond_150

    #@a
    .line 201
    move/from16 v0, p3

    #@c
    invoke-direct {p0, v0}, Lcom/android/server/TelephonyRegistry;->checkListenerPermission(I)V

    #@f
    .line 203
    iget-object v12, p0, Lcom/android/server/TelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@11
    monitor-enter v12

    #@12
    .line 205
    const/4 v8, 0x0

    #@13
    .line 207
    .local v8, r:Lcom/android/server/TelephonyRegistry$Record;
    :try_start_13
    invoke-interface/range {p2 .. p2}, Lcom/android/internal/telephony/IPhoneStateListener;->asBinder()Landroid/os/IBinder;

    #@16
    move-result-object v2

    #@17
    .line 208
    .local v2, b:Landroid/os/IBinder;
    iget-object v11, p0, Lcom/android/server/TelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@19
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I
    :try_end_1c
    .catchall {:try_start_13 .. :try_end_1c} :catchall_f7

    #@1c
    move-result v1

    #@1d
    .line 209
    .local v1, N:I
    const/4 v6, 0x0

    #@1e
    .local v6, i:I
    move-object v9, v8

    #@1f
    .end local v8           #r:Lcom/android/server/TelephonyRegistry$Record;
    .local v9, r:Lcom/android/server/TelephonyRegistry$Record;
    :goto_1f
    if-ge v6, v1, :cond_de

    #@21
    .line 210
    :try_start_21
    iget-object v11, p0, Lcom/android/server/TelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@23
    invoke-virtual {v11, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@26
    move-result-object v8

    #@27
    check-cast v8, Lcom/android/server/TelephonyRegistry$Record;
    :try_end_29
    .catchall {:try_start_21 .. :try_end_29} :catchall_158

    #@29
    .line 211
    .end local v9           #r:Lcom/android/server/TelephonyRegistry$Record;
    .restart local v8       #r:Lcom/android/server/TelephonyRegistry$Record;
    :try_start_29
    iget-object v11, v8, Lcom/android/server/TelephonyRegistry$Record;->binder:Landroid/os/IBinder;

    #@2b
    if-ne v2, v11, :cond_d9

    #@2d
    .line 223
    :goto_2d
    iget v11, v8, Lcom/android/server/TelephonyRegistry$Record;->events:I

    #@2f
    xor-int v11, v11, p3

    #@31
    and-int v10, p3, v11

    #@33
    .line 224
    .local v10, send:I
    move/from16 v0, p3

    #@35
    iput v0, v8, Lcom/android/server/TelephonyRegistry$Record;->events:I
    :try_end_37
    .catchall {:try_start_29 .. :try_end_37} :catchall_f7

    #@37
    .line 225
    if-eqz p4, :cond_d7

    #@39
    .line 226
    and-int/lit8 v11, p3, 0x1

    #@3b
    if-eqz v11, :cond_49

    #@3d
    .line 228
    :try_start_3d
    iget-object v11, v8, Lcom/android/server/TelephonyRegistry$Record;->callback:Lcom/android/internal/telephony/IPhoneStateListener;

    #@3f
    new-instance v13, Landroid/telephony/ServiceState;

    #@41
    iget-object v14, p0, Lcom/android/server/TelephonyRegistry;->mServiceState:Landroid/telephony/ServiceState;

    #@43
    invoke-direct {v13, v14}, Landroid/telephony/ServiceState;-><init>(Landroid/telephony/ServiceState;)V

    #@46
    invoke-interface {v11, v13}, Lcom/android/internal/telephony/IPhoneStateListener;->onServiceStateChanged(Landroid/telephony/ServiceState;)V
    :try_end_49
    .catchall {:try_start_3d .. :try_end_49} :catchall_f7
    .catch Landroid/os/RemoteException; {:try_start_3d .. :try_end_49} :catch_fa

    #@49
    .line 233
    :cond_49
    :goto_49
    and-int/lit8 v11, p3, 0x2

    #@4b
    if-eqz v11, :cond_5d

    #@4d
    .line 235
    :try_start_4d
    iget-object v11, p0, Lcom/android/server/TelephonyRegistry;->mSignalStrength:Landroid/telephony/SignalStrength;

    #@4f
    invoke-virtual {v11}, Landroid/telephony/SignalStrength;->getGsmSignalStrength()I

    #@52
    move-result v5

    #@53
    .line 236
    .local v5, gsmSignalStrength:I
    iget-object v11, v8, Lcom/android/server/TelephonyRegistry$Record;->callback:Lcom/android/internal/telephony/IPhoneStateListener;

    #@55
    const/16 v13, 0x63

    #@57
    if-ne v5, v13, :cond_5a

    #@59
    const/4 v5, -0x1

    #@5a
    .end local v5           #gsmSignalStrength:I
    :cond_5a
    invoke-interface {v11, v5}, Lcom/android/internal/telephony/IPhoneStateListener;->onSignalStrengthChanged(I)V
    :try_end_5d
    .catchall {:try_start_4d .. :try_end_5d} :catchall_f7
    .catch Landroid/os/RemoteException; {:try_start_4d .. :try_end_5d} :catch_102

    #@5d
    .line 242
    :cond_5d
    :goto_5d
    and-int/lit8 v11, p3, 0x4

    #@5f
    if-eqz v11, :cond_68

    #@61
    .line 244
    :try_start_61
    iget-object v11, v8, Lcom/android/server/TelephonyRegistry$Record;->callback:Lcom/android/internal/telephony/IPhoneStateListener;

    #@63
    iget-boolean v13, p0, Lcom/android/server/TelephonyRegistry;->mMessageWaiting:Z

    #@65
    invoke-interface {v11, v13}, Lcom/android/internal/telephony/IPhoneStateListener;->onMessageWaitingIndicatorChanged(Z)V
    :try_end_68
    .catchall {:try_start_61 .. :try_end_68} :catchall_f7
    .catch Landroid/os/RemoteException; {:try_start_61 .. :try_end_68} :catch_10a

    #@68
    .line 249
    :cond_68
    :goto_68
    and-int/lit8 v11, p3, 0x8

    #@6a
    if-eqz v11, :cond_73

    #@6c
    .line 251
    :try_start_6c
    iget-object v11, v8, Lcom/android/server/TelephonyRegistry$Record;->callback:Lcom/android/internal/telephony/IPhoneStateListener;

    #@6e
    iget-boolean v13, p0, Lcom/android/server/TelephonyRegistry;->mCallForwarding:Z

    #@70
    invoke-interface {v11, v13}, Lcom/android/internal/telephony/IPhoneStateListener;->onCallForwardingIndicatorChanged(Z)V
    :try_end_73
    .catchall {:try_start_6c .. :try_end_73} :catchall_f7
    .catch Landroid/os/RemoteException; {:try_start_6c .. :try_end_73} :catch_112

    #@73
    .line 256
    :cond_73
    :goto_73
    const/16 v11, 0x10

    #@75
    :try_start_75
    invoke-direct {p0, v8, v11}, Lcom/android/server/TelephonyRegistry;->validateEventsAndUserLocked(Lcom/android/server/TelephonyRegistry$Record;I)Z
    :try_end_78
    .catchall {:try_start_75 .. :try_end_78} :catchall_f7

    #@78
    move-result v11

    #@79
    if-eqz v11, :cond_87

    #@7b
    .line 259
    :try_start_7b
    iget-object v11, v8, Lcom/android/server/TelephonyRegistry$Record;->callback:Lcom/android/internal/telephony/IPhoneStateListener;

    #@7d
    new-instance v13, Landroid/os/Bundle;

    #@7f
    iget-object v14, p0, Lcom/android/server/TelephonyRegistry;->mCellLocation:Landroid/os/Bundle;

    #@81
    invoke-direct {v13, v14}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    #@84
    invoke-interface {v11, v13}, Lcom/android/internal/telephony/IPhoneStateListener;->onCellLocationChanged(Landroid/os/Bundle;)V
    :try_end_87
    .catchall {:try_start_7b .. :try_end_87} :catchall_f7
    .catch Landroid/os/RemoteException; {:try_start_7b .. :try_end_87} :catch_11a

    #@87
    .line 264
    :cond_87
    :goto_87
    and-int/lit8 v11, p3, 0x20

    #@89
    if-eqz v11, :cond_94

    #@8b
    .line 266
    :try_start_8b
    iget-object v11, v8, Lcom/android/server/TelephonyRegistry$Record;->callback:Lcom/android/internal/telephony/IPhoneStateListener;

    #@8d
    iget v13, p0, Lcom/android/server/TelephonyRegistry;->mCallState:I

    #@8f
    iget-object v14, p0, Lcom/android/server/TelephonyRegistry;->mCallIncomingNumber:Ljava/lang/String;

    #@91
    invoke-interface {v11, v13, v14}, Lcom/android/internal/telephony/IPhoneStateListener;->onCallStateChanged(ILjava/lang/String;)V
    :try_end_94
    .catchall {:try_start_8b .. :try_end_94} :catchall_f7
    .catch Landroid/os/RemoteException; {:try_start_8b .. :try_end_94} :catch_122

    #@94
    .line 271
    :cond_94
    :goto_94
    and-int/lit8 v11, p3, 0x40

    #@96
    if-eqz v11, :cond_a1

    #@98
    .line 273
    :try_start_98
    iget-object v11, v8, Lcom/android/server/TelephonyRegistry$Record;->callback:Lcom/android/internal/telephony/IPhoneStateListener;

    #@9a
    iget v13, p0, Lcom/android/server/TelephonyRegistry;->mDataConnectionState:I

    #@9c
    iget v14, p0, Lcom/android/server/TelephonyRegistry;->mDataConnectionNetworkType:I

    #@9e
    invoke-interface {v11, v13, v14}, Lcom/android/internal/telephony/IPhoneStateListener;->onDataConnectionStateChanged(II)V
    :try_end_a1
    .catchall {:try_start_98 .. :try_end_a1} :catchall_f7
    .catch Landroid/os/RemoteException; {:try_start_98 .. :try_end_a1} :catch_12a

    #@a1
    .line 279
    :cond_a1
    :goto_a1
    move/from16 v0, p3

    #@a3
    and-int/lit16 v11, v0, 0x80

    #@a5
    if-eqz v11, :cond_ae

    #@a7
    .line 281
    :try_start_a7
    iget-object v11, v8, Lcom/android/server/TelephonyRegistry$Record;->callback:Lcom/android/internal/telephony/IPhoneStateListener;

    #@a9
    iget v13, p0, Lcom/android/server/TelephonyRegistry;->mDataActivity:I

    #@ab
    invoke-interface {v11, v13}, Lcom/android/internal/telephony/IPhoneStateListener;->onDataActivity(I)V
    :try_end_ae
    .catchall {:try_start_a7 .. :try_end_ae} :catchall_f7
    .catch Landroid/os/RemoteException; {:try_start_a7 .. :try_end_ae} :catch_132

    #@ae
    .line 286
    :cond_ae
    :goto_ae
    move/from16 v0, p3

    #@b0
    and-int/lit16 v11, v0, 0x100

    #@b2
    if-eqz v11, :cond_bb

    #@b4
    .line 288
    :try_start_b4
    iget-object v11, v8, Lcom/android/server/TelephonyRegistry$Record;->callback:Lcom/android/internal/telephony/IPhoneStateListener;

    #@b6
    iget-object v13, p0, Lcom/android/server/TelephonyRegistry;->mSignalStrength:Landroid/telephony/SignalStrength;

    #@b8
    invoke-interface {v11, v13}, Lcom/android/internal/telephony/IPhoneStateListener;->onSignalStrengthsChanged(Landroid/telephony/SignalStrength;)V
    :try_end_bb
    .catchall {:try_start_b4 .. :try_end_bb} :catchall_f7
    .catch Landroid/os/RemoteException; {:try_start_b4 .. :try_end_bb} :catch_13a

    #@bb
    .line 293
    :cond_bb
    :goto_bb
    move/from16 v0, p3

    #@bd
    and-int/lit16 v11, v0, 0x200

    #@bf
    if-eqz v11, :cond_c8

    #@c1
    .line 295
    :try_start_c1
    iget-object v11, v8, Lcom/android/server/TelephonyRegistry$Record;->callback:Lcom/android/internal/telephony/IPhoneStateListener;

    #@c3
    iget v13, p0, Lcom/android/server/TelephonyRegistry;->mOtaspMode:I

    #@c5
    invoke-interface {v11, v13}, Lcom/android/internal/telephony/IPhoneStateListener;->onOtaspChanged(I)V
    :try_end_c8
    .catchall {:try_start_c1 .. :try_end_c8} :catchall_f7
    .catch Landroid/os/RemoteException; {:try_start_c1 .. :try_end_c8} :catch_142

    #@c8
    .line 300
    :cond_c8
    :goto_c8
    const/16 v11, 0x400

    #@ca
    :try_start_ca
    invoke-direct {p0, v8, v11}, Lcom/android/server/TelephonyRegistry;->validateEventsAndUserLocked(Lcom/android/server/TelephonyRegistry$Record;I)Z
    :try_end_cd
    .catchall {:try_start_ca .. :try_end_cd} :catchall_f7

    #@cd
    move-result v11

    #@ce
    if-eqz v11, :cond_d7

    #@d0
    .line 303
    :try_start_d0
    iget-object v11, v8, Lcom/android/server/TelephonyRegistry$Record;->callback:Lcom/android/internal/telephony/IPhoneStateListener;

    #@d2
    iget-object v13, p0, Lcom/android/server/TelephonyRegistry;->mCellInfo:Ljava/util/List;

    #@d4
    invoke-interface {v11, v13}, Lcom/android/internal/telephony/IPhoneStateListener;->onCellInfoChanged(Ljava/util/List;)V
    :try_end_d7
    .catchall {:try_start_d0 .. :try_end_d7} :catchall_f7
    .catch Landroid/os/RemoteException; {:try_start_d0 .. :try_end_d7} :catch_149

    #@d7
    .line 309
    :cond_d7
    :goto_d7
    :try_start_d7
    monitor-exit v12
    :try_end_d8
    .catchall {:try_start_d7 .. :try_end_d8} :catchall_f7

    #@d8
    .line 313
    .end local v1           #N:I
    .end local v2           #b:Landroid/os/IBinder;
    .end local v6           #i:I
    .end local v8           #r:Lcom/android/server/TelephonyRegistry$Record;
    .end local v10           #send:I
    :goto_d8
    return-void

    #@d9
    .line 209
    .restart local v1       #N:I
    .restart local v2       #b:Landroid/os/IBinder;
    .restart local v6       #i:I
    .restart local v8       #r:Lcom/android/server/TelephonyRegistry$Record;
    :cond_d9
    add-int/lit8 v6, v6, 0x1

    #@db
    move-object v9, v8

    #@dc
    .end local v8           #r:Lcom/android/server/TelephonyRegistry$Record;
    .restart local v9       #r:Lcom/android/server/TelephonyRegistry$Record;
    goto/16 :goto_1f

    #@de
    .line 215
    :cond_de
    :try_start_de
    new-instance v8, Lcom/android/server/TelephonyRegistry$Record;

    #@e0
    const/4 v11, 0x0

    #@e1
    invoke-direct {v8, v11}, Lcom/android/server/TelephonyRegistry$Record;-><init>(Lcom/android/server/TelephonyRegistry$1;)V
    :try_end_e4
    .catchall {:try_start_de .. :try_end_e4} :catchall_158

    #@e4
    .line 216
    .end local v9           #r:Lcom/android/server/TelephonyRegistry$Record;
    .restart local v8       #r:Lcom/android/server/TelephonyRegistry$Record;
    :try_start_e4
    iput-object v2, v8, Lcom/android/server/TelephonyRegistry$Record;->binder:Landroid/os/IBinder;

    #@e6
    .line 217
    move-object/from16 v0, p2

    #@e8
    iput-object v0, v8, Lcom/android/server/TelephonyRegistry$Record;->callback:Lcom/android/internal/telephony/IPhoneStateListener;

    #@ea
    .line 218
    move-object/from16 v0, p1

    #@ec
    iput-object v0, v8, Lcom/android/server/TelephonyRegistry$Record;->pkgForDebug:Ljava/lang/String;

    #@ee
    .line 219
    iput v3, v8, Lcom/android/server/TelephonyRegistry$Record;->callerUid:I

    #@f0
    .line 220
    iget-object v11, p0, Lcom/android/server/TelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@f2
    invoke-virtual {v11, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@f5
    goto/16 :goto_2d

    #@f7
    .line 309
    .end local v1           #N:I
    .end local v2           #b:Landroid/os/IBinder;
    .end local v6           #i:I
    :catchall_f7
    move-exception v11

    #@f8
    :goto_f8
    monitor-exit v12
    :try_end_f9
    .catchall {:try_start_e4 .. :try_end_f9} :catchall_f7

    #@f9
    throw v11

    #@fa
    .line 229
    .restart local v1       #N:I
    .restart local v2       #b:Landroid/os/IBinder;
    .restart local v6       #i:I
    .restart local v10       #send:I
    :catch_fa
    move-exception v4

    #@fb
    .line 230
    .local v4, ex:Landroid/os/RemoteException;
    :try_start_fb
    iget-object v11, v8, Lcom/android/server/TelephonyRegistry$Record;->binder:Landroid/os/IBinder;

    #@fd
    invoke-direct {p0, v11}, Lcom/android/server/TelephonyRegistry;->remove(Landroid/os/IBinder;)V

    #@100
    goto/16 :goto_49

    #@102
    .line 238
    .end local v4           #ex:Landroid/os/RemoteException;
    :catch_102
    move-exception v4

    #@103
    .line 239
    .restart local v4       #ex:Landroid/os/RemoteException;
    iget-object v11, v8, Lcom/android/server/TelephonyRegistry$Record;->binder:Landroid/os/IBinder;

    #@105
    invoke-direct {p0, v11}, Lcom/android/server/TelephonyRegistry;->remove(Landroid/os/IBinder;)V

    #@108
    goto/16 :goto_5d

    #@10a
    .line 245
    .end local v4           #ex:Landroid/os/RemoteException;
    :catch_10a
    move-exception v4

    #@10b
    .line 246
    .restart local v4       #ex:Landroid/os/RemoteException;
    iget-object v11, v8, Lcom/android/server/TelephonyRegistry$Record;->binder:Landroid/os/IBinder;

    #@10d
    invoke-direct {p0, v11}, Lcom/android/server/TelephonyRegistry;->remove(Landroid/os/IBinder;)V

    #@110
    goto/16 :goto_68

    #@112
    .line 252
    .end local v4           #ex:Landroid/os/RemoteException;
    :catch_112
    move-exception v4

    #@113
    .line 253
    .restart local v4       #ex:Landroid/os/RemoteException;
    iget-object v11, v8, Lcom/android/server/TelephonyRegistry$Record;->binder:Landroid/os/IBinder;

    #@115
    invoke-direct {p0, v11}, Lcom/android/server/TelephonyRegistry;->remove(Landroid/os/IBinder;)V

    #@118
    goto/16 :goto_73

    #@11a
    .line 260
    .end local v4           #ex:Landroid/os/RemoteException;
    :catch_11a
    move-exception v4

    #@11b
    .line 261
    .restart local v4       #ex:Landroid/os/RemoteException;
    iget-object v11, v8, Lcom/android/server/TelephonyRegistry$Record;->binder:Landroid/os/IBinder;

    #@11d
    invoke-direct {p0, v11}, Lcom/android/server/TelephonyRegistry;->remove(Landroid/os/IBinder;)V

    #@120
    goto/16 :goto_87

    #@122
    .line 267
    .end local v4           #ex:Landroid/os/RemoteException;
    :catch_122
    move-exception v4

    #@123
    .line 268
    .restart local v4       #ex:Landroid/os/RemoteException;
    iget-object v11, v8, Lcom/android/server/TelephonyRegistry$Record;->binder:Landroid/os/IBinder;

    #@125
    invoke-direct {p0, v11}, Lcom/android/server/TelephonyRegistry;->remove(Landroid/os/IBinder;)V

    #@128
    goto/16 :goto_94

    #@12a
    .line 275
    .end local v4           #ex:Landroid/os/RemoteException;
    :catch_12a
    move-exception v4

    #@12b
    .line 276
    .restart local v4       #ex:Landroid/os/RemoteException;
    iget-object v11, v8, Lcom/android/server/TelephonyRegistry$Record;->binder:Landroid/os/IBinder;

    #@12d
    invoke-direct {p0, v11}, Lcom/android/server/TelephonyRegistry;->remove(Landroid/os/IBinder;)V

    #@130
    goto/16 :goto_a1

    #@132
    .line 282
    .end local v4           #ex:Landroid/os/RemoteException;
    :catch_132
    move-exception v4

    #@133
    .line 283
    .restart local v4       #ex:Landroid/os/RemoteException;
    iget-object v11, v8, Lcom/android/server/TelephonyRegistry$Record;->binder:Landroid/os/IBinder;

    #@135
    invoke-direct {p0, v11}, Lcom/android/server/TelephonyRegistry;->remove(Landroid/os/IBinder;)V

    #@138
    goto/16 :goto_ae

    #@13a
    .line 289
    .end local v4           #ex:Landroid/os/RemoteException;
    :catch_13a
    move-exception v4

    #@13b
    .line 290
    .restart local v4       #ex:Landroid/os/RemoteException;
    iget-object v11, v8, Lcom/android/server/TelephonyRegistry$Record;->binder:Landroid/os/IBinder;

    #@13d
    invoke-direct {p0, v11}, Lcom/android/server/TelephonyRegistry;->remove(Landroid/os/IBinder;)V

    #@140
    goto/16 :goto_bb

    #@142
    .line 296
    .end local v4           #ex:Landroid/os/RemoteException;
    :catch_142
    move-exception v4

    #@143
    .line 297
    .restart local v4       #ex:Landroid/os/RemoteException;
    iget-object v11, v8, Lcom/android/server/TelephonyRegistry$Record;->binder:Landroid/os/IBinder;

    #@145
    invoke-direct {p0, v11}, Lcom/android/server/TelephonyRegistry;->remove(Landroid/os/IBinder;)V

    #@148
    goto :goto_c8

    #@149
    .line 304
    .end local v4           #ex:Landroid/os/RemoteException;
    :catch_149
    move-exception v4

    #@14a
    .line 305
    .restart local v4       #ex:Landroid/os/RemoteException;
    iget-object v11, v8, Lcom/android/server/TelephonyRegistry$Record;->binder:Landroid/os/IBinder;

    #@14c
    invoke-direct {p0, v11}, Lcom/android/server/TelephonyRegistry;->remove(Landroid/os/IBinder;)V
    :try_end_14f
    .catchall {:try_start_fb .. :try_end_14f} :catchall_f7

    #@14f
    goto :goto_d7

    #@150
    .line 311
    .end local v1           #N:I
    .end local v2           #b:Landroid/os/IBinder;
    .end local v4           #ex:Landroid/os/RemoteException;
    .end local v6           #i:I
    .end local v8           #r:Lcom/android/server/TelephonyRegistry$Record;
    .end local v10           #send:I
    :cond_150
    invoke-interface/range {p2 .. p2}, Lcom/android/internal/telephony/IPhoneStateListener;->asBinder()Landroid/os/IBinder;

    #@153
    move-result-object v11

    #@154
    invoke-direct {p0, v11}, Lcom/android/server/TelephonyRegistry;->remove(Landroid/os/IBinder;)V

    #@157
    goto :goto_d8

    #@158
    .line 309
    .restart local v1       #N:I
    .restart local v2       #b:Landroid/os/IBinder;
    .restart local v6       #i:I
    .restart local v9       #r:Lcom/android/server/TelephonyRegistry$Record;
    :catchall_158
    move-exception v11

    #@159
    move-object v8, v9

    #@15a
    .end local v9           #r:Lcom/android/server/TelephonyRegistry$Record;
    .restart local v8       #r:Lcom/android/server/TelephonyRegistry$Record;
    goto :goto_f8
.end method

.method public notifyCallForwardingChanged(Z)V
    .registers 8
    .parameter "cfi"

    #@0
    .prologue
    .line 440
    const-string v3, "notifyCallForwardingChanged()"

    #@2
    invoke-direct {p0, v3}, Lcom/android/server/TelephonyRegistry;->checkNotifyPermission(Ljava/lang/String;)Z

    #@5
    move-result v3

    #@6
    if-nez v3, :cond_9

    #@8
    .line 456
    :goto_8
    return-void

    #@9
    .line 443
    :cond_9
    iget-object v4, p0, Lcom/android/server/TelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@b
    monitor-enter v4

    #@c
    .line 444
    :try_start_c
    iput-boolean p1, p0, Lcom/android/server/TelephonyRegistry;->mCallForwarding:Z

    #@e
    .line 445
    iget-object v3, p0, Lcom/android/server/TelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@10
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@13
    move-result-object v1

    #@14
    .local v1, i$:Ljava/util/Iterator;
    :cond_14
    :goto_14
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@17
    move-result v3

    #@18
    if-eqz v3, :cond_38

    #@1a
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1d
    move-result-object v2

    #@1e
    check-cast v2, Lcom/android/server/TelephonyRegistry$Record;

    #@20
    .line 446
    .local v2, r:Lcom/android/server/TelephonyRegistry$Record;
    iget v3, v2, Lcom/android/server/TelephonyRegistry$Record;->events:I
    :try_end_22
    .catchall {:try_start_c .. :try_end_22} :catchall_35

    #@22
    and-int/lit8 v3, v3, 0x8

    #@24
    if-eqz v3, :cond_14

    #@26
    .line 448
    :try_start_26
    iget-object v3, v2, Lcom/android/server/TelephonyRegistry$Record;->callback:Lcom/android/internal/telephony/IPhoneStateListener;

    #@28
    invoke-interface {v3, p1}, Lcom/android/internal/telephony/IPhoneStateListener;->onCallForwardingIndicatorChanged(Z)V
    :try_end_2b
    .catchall {:try_start_26 .. :try_end_2b} :catchall_35
    .catch Landroid/os/RemoteException; {:try_start_26 .. :try_end_2b} :catch_2c

    #@2b
    goto :goto_14

    #@2c
    .line 449
    :catch_2c
    move-exception v0

    #@2d
    .line 450
    .local v0, ex:Landroid/os/RemoteException;
    :try_start_2d
    iget-object v3, p0, Lcom/android/server/TelephonyRegistry;->mRemoveList:Ljava/util/ArrayList;

    #@2f
    iget-object v5, v2, Lcom/android/server/TelephonyRegistry$Record;->binder:Landroid/os/IBinder;

    #@31
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@34
    goto :goto_14

    #@35
    .line 455
    .end local v0           #ex:Landroid/os/RemoteException;
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #r:Lcom/android/server/TelephonyRegistry$Record;
    :catchall_35
    move-exception v3

    #@36
    monitor-exit v4
    :try_end_37
    .catchall {:try_start_2d .. :try_end_37} :catchall_35

    #@37
    throw v3

    #@38
    .line 454
    .restart local v1       #i$:Ljava/util/Iterator;
    :cond_38
    :try_start_38
    invoke-direct {p0}, Lcom/android/server/TelephonyRegistry;->handleRemoveListLocked()V

    #@3b
    .line 455
    monitor-exit v4
    :try_end_3c
    .catchall {:try_start_38 .. :try_end_3c} :catchall_35

    #@3c
    goto :goto_8
.end method

.method public notifyCallState(ILjava/lang/String;)V
    .registers 9
    .parameter "state"
    .parameter "incomingNumber"

    #@0
    .prologue
    .line 328
    const-string v3, "notifyCallState()"

    #@2
    invoke-direct {p0, v3}, Lcom/android/server/TelephonyRegistry;->checkNotifyPermission(Ljava/lang/String;)Z

    #@5
    move-result v3

    #@6
    if-nez v3, :cond_9

    #@8
    .line 346
    :goto_8
    return-void

    #@9
    .line 331
    :cond_9
    iget-object v4, p0, Lcom/android/server/TelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@b
    monitor-enter v4

    #@c
    .line 332
    :try_start_c
    iput p1, p0, Lcom/android/server/TelephonyRegistry;->mCallState:I

    #@e
    .line 333
    iput-object p2, p0, Lcom/android/server/TelephonyRegistry;->mCallIncomingNumber:Ljava/lang/String;

    #@10
    .line 334
    iget-object v3, p0, Lcom/android/server/TelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@12
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@15
    move-result-object v1

    #@16
    .local v1, i$:Ljava/util/Iterator;
    :cond_16
    :goto_16
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@19
    move-result v3

    #@1a
    if-eqz v3, :cond_3a

    #@1c
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1f
    move-result-object v2

    #@20
    check-cast v2, Lcom/android/server/TelephonyRegistry$Record;

    #@22
    .line 335
    .local v2, r:Lcom/android/server/TelephonyRegistry$Record;
    iget v3, v2, Lcom/android/server/TelephonyRegistry$Record;->events:I
    :try_end_24
    .catchall {:try_start_c .. :try_end_24} :catchall_37

    #@24
    and-int/lit8 v3, v3, 0x20

    #@26
    if-eqz v3, :cond_16

    #@28
    .line 337
    :try_start_28
    iget-object v3, v2, Lcom/android/server/TelephonyRegistry$Record;->callback:Lcom/android/internal/telephony/IPhoneStateListener;

    #@2a
    invoke-interface {v3, p1, p2}, Lcom/android/internal/telephony/IPhoneStateListener;->onCallStateChanged(ILjava/lang/String;)V
    :try_end_2d
    .catchall {:try_start_28 .. :try_end_2d} :catchall_37
    .catch Landroid/os/RemoteException; {:try_start_28 .. :try_end_2d} :catch_2e

    #@2d
    goto :goto_16

    #@2e
    .line 338
    :catch_2e
    move-exception v0

    #@2f
    .line 339
    .local v0, ex:Landroid/os/RemoteException;
    :try_start_2f
    iget-object v3, p0, Lcom/android/server/TelephonyRegistry;->mRemoveList:Ljava/util/ArrayList;

    #@31
    iget-object v5, v2, Lcom/android/server/TelephonyRegistry$Record;->binder:Landroid/os/IBinder;

    #@33
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@36
    goto :goto_16

    #@37
    .line 344
    .end local v0           #ex:Landroid/os/RemoteException;
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #r:Lcom/android/server/TelephonyRegistry$Record;
    :catchall_37
    move-exception v3

    #@38
    monitor-exit v4
    :try_end_39
    .catchall {:try_start_2f .. :try_end_39} :catchall_37

    #@39
    throw v3

    #@3a
    .line 343
    .restart local v1       #i$:Ljava/util/Iterator;
    :cond_3a
    :try_start_3a
    invoke-direct {p0}, Lcom/android/server/TelephonyRegistry;->handleRemoveListLocked()V

    #@3d
    .line 344
    monitor-exit v4
    :try_end_3e
    .catchall {:try_start_3a .. :try_end_3e} :catchall_37

    #@3e
    .line 345
    invoke-direct {p0, p1, p2}, Lcom/android/server/TelephonyRegistry;->broadcastCallStateChanged(ILjava/lang/String;)V

    #@41
    goto :goto_8
.end method

.method public notifyCellInfo(Ljava/util/List;)V
    .registers 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/telephony/CellInfo;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 398
    .local p1, cellInfo:Ljava/util/List;,"Ljava/util/List<Landroid/telephony/CellInfo;>;"
    const-string v3, "notifyCellInfo()"

    #@2
    invoke-direct {p0, v3}, Lcom/android/server/TelephonyRegistry;->checkNotifyPermission(Ljava/lang/String;)Z

    #@5
    move-result v3

    #@6
    if-nez v3, :cond_9

    #@8
    .line 418
    :goto_8
    return-void

    #@9
    .line 402
    :cond_9
    iget-object v4, p0, Lcom/android/server/TelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@b
    monitor-enter v4

    #@c
    .line 403
    :try_start_c
    iput-object p1, p0, Lcom/android/server/TelephonyRegistry;->mCellInfo:Ljava/util/List;

    #@e
    .line 404
    iget-object v3, p0, Lcom/android/server/TelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@10
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@13
    move-result-object v1

    #@14
    .local v1, i$:Ljava/util/Iterator;
    :cond_14
    :goto_14
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@17
    move-result v3

    #@18
    if-eqz v3, :cond_3a

    #@1a
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1d
    move-result-object v2

    #@1e
    check-cast v2, Lcom/android/server/TelephonyRegistry$Record;

    #@20
    .line 405
    .local v2, r:Lcom/android/server/TelephonyRegistry$Record;
    const/16 v3, 0x400

    #@22
    invoke-direct {p0, v2, v3}, Lcom/android/server/TelephonyRegistry;->validateEventsAndUserLocked(Lcom/android/server/TelephonyRegistry$Record;I)Z
    :try_end_25
    .catchall {:try_start_c .. :try_end_25} :catchall_37

    #@25
    move-result v3

    #@26
    if-eqz v3, :cond_14

    #@28
    .line 410
    :try_start_28
    iget-object v3, v2, Lcom/android/server/TelephonyRegistry$Record;->callback:Lcom/android/internal/telephony/IPhoneStateListener;

    #@2a
    invoke-interface {v3, p1}, Lcom/android/internal/telephony/IPhoneStateListener;->onCellInfoChanged(Ljava/util/List;)V
    :try_end_2d
    .catchall {:try_start_28 .. :try_end_2d} :catchall_37
    .catch Landroid/os/RemoteException; {:try_start_28 .. :try_end_2d} :catch_2e

    #@2d
    goto :goto_14

    #@2e
    .line 411
    :catch_2e
    move-exception v0

    #@2f
    .line 412
    .local v0, ex:Landroid/os/RemoteException;
    :try_start_2f
    iget-object v3, p0, Lcom/android/server/TelephonyRegistry;->mRemoveList:Ljava/util/ArrayList;

    #@31
    iget-object v5, v2, Lcom/android/server/TelephonyRegistry$Record;->binder:Landroid/os/IBinder;

    #@33
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@36
    goto :goto_14

    #@37
    .line 417
    .end local v0           #ex:Landroid/os/RemoteException;
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #r:Lcom/android/server/TelephonyRegistry$Record;
    :catchall_37
    move-exception v3

    #@38
    monitor-exit v4
    :try_end_39
    .catchall {:try_start_2f .. :try_end_39} :catchall_37

    #@39
    throw v3

    #@3a
    .line 416
    .restart local v1       #i$:Ljava/util/Iterator;
    :cond_3a
    :try_start_3a
    invoke-direct {p0}, Lcom/android/server/TelephonyRegistry;->handleRemoveListLocked()V

    #@3d
    .line 417
    monitor-exit v4
    :try_end_3e
    .catchall {:try_start_3a .. :try_end_3e} :catchall_37

    #@3e
    goto :goto_8
.end method

.method public notifyCellLocation(Landroid/os/Bundle;)V
    .registers 8
    .parameter "cellLocation"

    #@0
    .prologue
    .line 579
    const-string v3, "notifyCellLocation()"

    #@2
    invoke-direct {p0, v3}, Lcom/android/server/TelephonyRegistry;->checkNotifyPermission(Ljava/lang/String;)Z

    #@5
    move-result v3

    #@6
    if-nez v3, :cond_9

    #@8
    .line 600
    :goto_8
    return-void

    #@9
    .line 582
    :cond_9
    iget-object v4, p0, Lcom/android/server/TelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@b
    monitor-enter v4

    #@c
    .line 583
    :try_start_c
    iput-object p1, p0, Lcom/android/server/TelephonyRegistry;->mCellLocation:Landroid/os/Bundle;

    #@e
    .line 584
    iget-object v3, p0, Lcom/android/server/TelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@10
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@13
    move-result-object v1

    #@14
    .local v1, i$:Ljava/util/Iterator;
    :cond_14
    :goto_14
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@17
    move-result v3

    #@18
    if-eqz v3, :cond_3f

    #@1a
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1d
    move-result-object v2

    #@1e
    check-cast v2, Lcom/android/server/TelephonyRegistry$Record;

    #@20
    .line 585
    .local v2, r:Lcom/android/server/TelephonyRegistry$Record;
    const/16 v3, 0x10

    #@22
    invoke-direct {p0, v2, v3}, Lcom/android/server/TelephonyRegistry;->validateEventsAndUserLocked(Lcom/android/server/TelephonyRegistry$Record;I)Z
    :try_end_25
    .catchall {:try_start_c .. :try_end_25} :catchall_3c

    #@25
    move-result v3

    #@26
    if-eqz v3, :cond_14

    #@28
    .line 591
    :try_start_28
    iget-object v3, v2, Lcom/android/server/TelephonyRegistry$Record;->callback:Lcom/android/internal/telephony/IPhoneStateListener;

    #@2a
    new-instance v5, Landroid/os/Bundle;

    #@2c
    invoke-direct {v5, p1}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    #@2f
    invoke-interface {v3, v5}, Lcom/android/internal/telephony/IPhoneStateListener;->onCellLocationChanged(Landroid/os/Bundle;)V
    :try_end_32
    .catchall {:try_start_28 .. :try_end_32} :catchall_3c
    .catch Landroid/os/RemoteException; {:try_start_28 .. :try_end_32} :catch_33

    #@32
    goto :goto_14

    #@33
    .line 592
    :catch_33
    move-exception v0

    #@34
    .line 593
    .local v0, ex:Landroid/os/RemoteException;
    :try_start_34
    iget-object v3, p0, Lcom/android/server/TelephonyRegistry;->mRemoveList:Ljava/util/ArrayList;

    #@36
    iget-object v5, v2, Lcom/android/server/TelephonyRegistry$Record;->binder:Landroid/os/IBinder;

    #@38
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@3b
    goto :goto_14

    #@3c
    .line 599
    .end local v0           #ex:Landroid/os/RemoteException;
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #r:Lcom/android/server/TelephonyRegistry$Record;
    :catchall_3c
    move-exception v3

    #@3d
    monitor-exit v4
    :try_end_3e
    .catchall {:try_start_34 .. :try_end_3e} :catchall_3c

    #@3e
    throw v3

    #@3f
    .line 598
    .restart local v1       #i$:Ljava/util/Iterator;
    :cond_3f
    :try_start_3f
    invoke-direct {p0}, Lcom/android/server/TelephonyRegistry;->handleRemoveListLocked()V

    #@42
    .line 599
    monitor-exit v4
    :try_end_43
    .catchall {:try_start_3f .. :try_end_43} :catchall_3c

    #@43
    goto :goto_8
.end method

.method public notifyDataActivity(I)V
    .registers 8
    .parameter "state"

    #@0
    .prologue
    .line 459
    const-string v3, "notifyDataActivity()"

    #@2
    invoke-direct {p0, v3}, Lcom/android/server/TelephonyRegistry;->checkNotifyPermission(Ljava/lang/String;)Z

    #@5
    move-result v3

    #@6
    if-nez v3, :cond_9

    #@8
    .line 475
    :goto_8
    return-void

    #@9
    .line 462
    :cond_9
    iget-object v4, p0, Lcom/android/server/TelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@b
    monitor-enter v4

    #@c
    .line 463
    :try_start_c
    iput p1, p0, Lcom/android/server/TelephonyRegistry;->mDataActivity:I

    #@e
    .line 464
    iget-object v3, p0, Lcom/android/server/TelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@10
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@13
    move-result-object v1

    #@14
    .local v1, i$:Ljava/util/Iterator;
    :cond_14
    :goto_14
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@17
    move-result v3

    #@18
    if-eqz v3, :cond_38

    #@1a
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1d
    move-result-object v2

    #@1e
    check-cast v2, Lcom/android/server/TelephonyRegistry$Record;

    #@20
    .line 465
    .local v2, r:Lcom/android/server/TelephonyRegistry$Record;
    iget v3, v2, Lcom/android/server/TelephonyRegistry$Record;->events:I
    :try_end_22
    .catchall {:try_start_c .. :try_end_22} :catchall_35

    #@22
    and-int/lit16 v3, v3, 0x80

    #@24
    if-eqz v3, :cond_14

    #@26
    .line 467
    :try_start_26
    iget-object v3, v2, Lcom/android/server/TelephonyRegistry$Record;->callback:Lcom/android/internal/telephony/IPhoneStateListener;

    #@28
    invoke-interface {v3, p1}, Lcom/android/internal/telephony/IPhoneStateListener;->onDataActivity(I)V
    :try_end_2b
    .catchall {:try_start_26 .. :try_end_2b} :catchall_35
    .catch Landroid/os/RemoteException; {:try_start_26 .. :try_end_2b} :catch_2c

    #@2b
    goto :goto_14

    #@2c
    .line 468
    :catch_2c
    move-exception v0

    #@2d
    .line 469
    .local v0, ex:Landroid/os/RemoteException;
    :try_start_2d
    iget-object v3, p0, Lcom/android/server/TelephonyRegistry;->mRemoveList:Ljava/util/ArrayList;

    #@2f
    iget-object v5, v2, Lcom/android/server/TelephonyRegistry$Record;->binder:Landroid/os/IBinder;

    #@31
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@34
    goto :goto_14

    #@35
    .line 474
    .end local v0           #ex:Landroid/os/RemoteException;
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #r:Lcom/android/server/TelephonyRegistry$Record;
    :catchall_35
    move-exception v3

    #@36
    monitor-exit v4
    :try_end_37
    .catchall {:try_start_2d .. :try_end_37} :catchall_35

    #@37
    throw v3

    #@38
    .line 473
    .restart local v1       #i$:Ljava/util/Iterator;
    :cond_38
    :try_start_38
    invoke-direct {p0}, Lcom/android/server/TelephonyRegistry;->handleRemoveListLocked()V

    #@3b
    .line 474
    monitor-exit v4
    :try_end_3c
    .catchall {:try_start_38 .. :try_end_3c} :catchall_35

    #@3c
    goto :goto_8
.end method

.method public notifyDataConnection(IZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/LinkProperties;Landroid/net/LinkCapabilities;IZI)V
    .registers 26
    .parameter "state"
    .parameter "isDataConnectivityPossible"
    .parameter "reason"
    .parameter "apn"
    .parameter "apnType"
    .parameter "linkProperties"
    .parameter "linkCapabilities"
    .parameter "networkType"
    .parameter "roaming"
    .parameter "smCause"

    #@0
    .prologue
    .line 487
    const-string v1, "notifyDataConnection()"

    #@2
    invoke-direct {p0, v1}, Lcom/android/server/TelephonyRegistry;->checkNotifyPermission(Ljava/lang/String;)Z

    #@5
    move-result v1

    #@6
    if-nez v1, :cond_9

    #@8
    .line 555
    :goto_8
    return-void

    #@9
    .line 499
    :cond_9
    iget-object v2, p0, Lcom/android/server/TelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@b
    monitor-enter v2

    #@c
    .line 500
    const/4 v13, 0x0

    #@d
    .line 501
    .local v13, modified:Z
    const/4 v1, 0x2

    #@e
    move/from16 v0, p1

    #@10
    if-ne v0, v1, :cond_79

    #@12
    .line 502
    :try_start_12
    iget-object v1, p0, Lcom/android/server/TelephonyRegistry;->mConnectedApns:Ljava/util/ArrayList;

    #@14
    move-object/from16 v0, p5

    #@16
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@19
    move-result v1

    #@1a
    if-nez v1, :cond_2e

    #@1c
    .line 503
    iget-object v1, p0, Lcom/android/server/TelephonyRegistry;->mConnectedApns:Ljava/util/ArrayList;

    #@1e
    move-object/from16 v0, p5

    #@20
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@23
    .line 504
    iget v1, p0, Lcom/android/server/TelephonyRegistry;->mDataConnectionState:I

    #@25
    move/from16 v0, p1

    #@27
    if-eq v1, v0, :cond_2e

    #@29
    .line 505
    move/from16 v0, p1

    #@2b
    iput v0, p0, Lcom/android/server/TelephonyRegistry;->mDataConnectionState:I

    #@2d
    .line 506
    const/4 v13, 0x1

    #@2e
    .line 520
    :cond_2e
    :goto_2e
    move/from16 v0, p2

    #@30
    iput-boolean v0, p0, Lcom/android/server/TelephonyRegistry;->mDataConnectionPossible:Z

    #@32
    .line 521
    move-object/from16 v0, p3

    #@34
    iput-object v0, p0, Lcom/android/server/TelephonyRegistry;->mDataConnectionReason:Ljava/lang/String;

    #@36
    .line 522
    move-object/from16 v0, p6

    #@38
    iput-object v0, p0, Lcom/android/server/TelephonyRegistry;->mDataConnectionLinkProperties:Landroid/net/LinkProperties;

    #@3a
    .line 523
    move-object/from16 v0, p7

    #@3c
    iput-object v0, p0, Lcom/android/server/TelephonyRegistry;->mDataConnectionLinkCapabilities:Landroid/net/LinkCapabilities;

    #@3e
    .line 524
    iget v1, p0, Lcom/android/server/TelephonyRegistry;->mDataConnectionNetworkType:I

    #@40
    move/from16 v0, p8

    #@42
    if-eq v1, v0, :cond_49

    #@44
    .line 525
    move/from16 v0, p8

    #@46
    iput v0, p0, Lcom/android/server/TelephonyRegistry;->mDataConnectionNetworkType:I

    #@48
    .line 527
    const/4 v13, 0x1

    #@49
    .line 529
    :cond_49
    if-eqz v13, :cond_94

    #@4b
    .line 534
    iget-object v1, p0, Lcom/android/server/TelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@4d
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@50
    move-result-object v12

    #@51
    .local v12, i$:Ljava/util/Iterator;
    :cond_51
    :goto_51
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    #@54
    move-result v1

    #@55
    if-eqz v1, :cond_91

    #@57
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@5a
    move-result-object v14

    #@5b
    check-cast v14, Lcom/android/server/TelephonyRegistry$Record;

    #@5d
    .line 535
    .local v14, r:Lcom/android/server/TelephonyRegistry$Record;
    iget v1, v14, Lcom/android/server/TelephonyRegistry$Record;->events:I
    :try_end_5f
    .catchall {:try_start_12 .. :try_end_5f} :catchall_76

    #@5f
    and-int/lit8 v1, v1, 0x40

    #@61
    if-eqz v1, :cond_51

    #@63
    .line 537
    :try_start_63
    iget-object v1, v14, Lcom/android/server/TelephonyRegistry$Record;->callback:Lcom/android/internal/telephony/IPhoneStateListener;

    #@65
    iget v3, p0, Lcom/android/server/TelephonyRegistry;->mDataConnectionState:I

    #@67
    iget v4, p0, Lcom/android/server/TelephonyRegistry;->mDataConnectionNetworkType:I

    #@69
    invoke-interface {v1, v3, v4}, Lcom/android/internal/telephony/IPhoneStateListener;->onDataConnectionStateChanged(II)V
    :try_end_6c
    .catchall {:try_start_63 .. :try_end_6c} :catchall_76
    .catch Landroid/os/RemoteException; {:try_start_63 .. :try_end_6c} :catch_6d

    #@6c
    goto :goto_51

    #@6d
    .line 539
    :catch_6d
    move-exception v11

    #@6e
    .line 540
    .local v11, ex:Landroid/os/RemoteException;
    :try_start_6e
    iget-object v1, p0, Lcom/android/server/TelephonyRegistry;->mRemoveList:Ljava/util/ArrayList;

    #@70
    iget-object v3, v14, Lcom/android/server/TelephonyRegistry$Record;->binder:Landroid/os/IBinder;

    #@72
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@75
    goto :goto_51

    #@76
    .line 546
    .end local v11           #ex:Landroid/os/RemoteException;
    .end local v12           #i$:Ljava/util/Iterator;
    .end local v14           #r:Lcom/android/server/TelephonyRegistry$Record;
    :catchall_76
    move-exception v1

    #@77
    monitor-exit v2
    :try_end_78
    .catchall {:try_start_6e .. :try_end_78} :catchall_76

    #@78
    throw v1

    #@79
    .line 510
    :cond_79
    :try_start_79
    iget-object v1, p0, Lcom/android/server/TelephonyRegistry;->mConnectedApns:Ljava/util/ArrayList;

    #@7b
    move-object/from16 v0, p5

    #@7d
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@80
    move-result v1

    #@81
    if-eqz v1, :cond_2e

    #@83
    .line 511
    iget-object v1, p0, Lcom/android/server/TelephonyRegistry;->mConnectedApns:Ljava/util/ArrayList;

    #@85
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    #@88
    move-result v1

    #@89
    if-eqz v1, :cond_2e

    #@8b
    .line 512
    move/from16 v0, p1

    #@8d
    iput v0, p0, Lcom/android/server/TelephonyRegistry;->mDataConnectionState:I

    #@8f
    .line 513
    const/4 v13, 0x1

    #@90
    goto :goto_2e

    #@91
    .line 544
    .restart local v12       #i$:Ljava/util/Iterator;
    :cond_91
    invoke-direct {p0}, Lcom/android/server/TelephonyRegistry;->handleRemoveListLocked()V

    #@94
    .line 546
    .end local v12           #i$:Ljava/util/Iterator;
    :cond_94
    monitor-exit v2
    :try_end_95
    .catchall {:try_start_79 .. :try_end_95} :catchall_76

    #@95
    move-object v1, p0

    #@96
    move/from16 v2, p1

    #@98
    move/from16 v3, p2

    #@9a
    move-object/from16 v4, p3

    #@9c
    move-object/from16 v5, p4

    #@9e
    move-object/from16 v6, p5

    #@a0
    move-object/from16 v7, p6

    #@a2
    move-object/from16 v8, p7

    #@a4
    move/from16 v9, p9

    #@a6
    move/from16 v10, p10

    #@a8
    .line 548
    invoke-direct/range {v1 .. v10}, Lcom/android/server/TelephonyRegistry;->broadcastDataConnectionStateChanged(IZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/LinkProperties;Landroid/net/LinkCapabilities;ZI)V

    #@ab
    goto/16 :goto_8
.end method

.method public notifyDataConnectionFailed(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "reason"
    .parameter "apnType"

    #@0
    .prologue
    .line 558
    const-string v0, "notifyDataConnectionFailed()"

    #@2
    invoke-direct {p0, v0}, Lcom/android/server/TelephonyRegistry;->checkNotifyPermission(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_9

    #@8
    .line 576
    :goto_8
    return-void

    #@9
    .line 575
    :cond_9
    invoke-direct {p0, p1, p2}, Lcom/android/server/TelephonyRegistry;->broadcastDataConnectionFailed(Ljava/lang/String;Ljava/lang/String;)V

    #@c
    goto :goto_8
.end method

.method public notifyMessageWaitingChanged(Z)V
    .registers 8
    .parameter "mwi"

    #@0
    .prologue
    .line 421
    const-string v3, "notifyMessageWaitingChanged()"

    #@2
    invoke-direct {p0, v3}, Lcom/android/server/TelephonyRegistry;->checkNotifyPermission(Ljava/lang/String;)Z

    #@5
    move-result v3

    #@6
    if-nez v3, :cond_9

    #@8
    .line 437
    :goto_8
    return-void

    #@9
    .line 424
    :cond_9
    iget-object v4, p0, Lcom/android/server/TelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@b
    monitor-enter v4

    #@c
    .line 425
    :try_start_c
    iput-boolean p1, p0, Lcom/android/server/TelephonyRegistry;->mMessageWaiting:Z

    #@e
    .line 426
    iget-object v3, p0, Lcom/android/server/TelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@10
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@13
    move-result-object v1

    #@14
    .local v1, i$:Ljava/util/Iterator;
    :cond_14
    :goto_14
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@17
    move-result v3

    #@18
    if-eqz v3, :cond_38

    #@1a
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1d
    move-result-object v2

    #@1e
    check-cast v2, Lcom/android/server/TelephonyRegistry$Record;

    #@20
    .line 427
    .local v2, r:Lcom/android/server/TelephonyRegistry$Record;
    iget v3, v2, Lcom/android/server/TelephonyRegistry$Record;->events:I
    :try_end_22
    .catchall {:try_start_c .. :try_end_22} :catchall_35

    #@22
    and-int/lit8 v3, v3, 0x4

    #@24
    if-eqz v3, :cond_14

    #@26
    .line 429
    :try_start_26
    iget-object v3, v2, Lcom/android/server/TelephonyRegistry$Record;->callback:Lcom/android/internal/telephony/IPhoneStateListener;

    #@28
    invoke-interface {v3, p1}, Lcom/android/internal/telephony/IPhoneStateListener;->onMessageWaitingIndicatorChanged(Z)V
    :try_end_2b
    .catchall {:try_start_26 .. :try_end_2b} :catchall_35
    .catch Landroid/os/RemoteException; {:try_start_26 .. :try_end_2b} :catch_2c

    #@2b
    goto :goto_14

    #@2c
    .line 430
    :catch_2c
    move-exception v0

    #@2d
    .line 431
    .local v0, ex:Landroid/os/RemoteException;
    :try_start_2d
    iget-object v3, p0, Lcom/android/server/TelephonyRegistry;->mRemoveList:Ljava/util/ArrayList;

    #@2f
    iget-object v5, v2, Lcom/android/server/TelephonyRegistry$Record;->binder:Landroid/os/IBinder;

    #@31
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@34
    goto :goto_14

    #@35
    .line 436
    .end local v0           #ex:Landroid/os/RemoteException;
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #r:Lcom/android/server/TelephonyRegistry$Record;
    :catchall_35
    move-exception v3

    #@36
    monitor-exit v4
    :try_end_37
    .catchall {:try_start_2d .. :try_end_37} :catchall_35

    #@37
    throw v3

    #@38
    .line 435
    .restart local v1       #i$:Ljava/util/Iterator;
    :cond_38
    :try_start_38
    invoke-direct {p0}, Lcom/android/server/TelephonyRegistry;->handleRemoveListLocked()V

    #@3b
    .line 436
    monitor-exit v4
    :try_end_3c
    .catchall {:try_start_38 .. :try_end_3c} :catchall_35

    #@3c
    goto :goto_8
.end method

.method public notifyOtaspChanged(I)V
    .registers 8
    .parameter "otaspMode"

    #@0
    .prologue
    .line 603
    const-string v3, "notifyOtaspChanged()"

    #@2
    invoke-direct {p0, v3}, Lcom/android/server/TelephonyRegistry;->checkNotifyPermission(Ljava/lang/String;)Z

    #@5
    move-result v3

    #@6
    if-nez v3, :cond_9

    #@8
    .line 619
    :goto_8
    return-void

    #@9
    .line 606
    :cond_9
    iget-object v4, p0, Lcom/android/server/TelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@b
    monitor-enter v4

    #@c
    .line 607
    :try_start_c
    iput p1, p0, Lcom/android/server/TelephonyRegistry;->mOtaspMode:I

    #@e
    .line 608
    iget-object v3, p0, Lcom/android/server/TelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@10
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@13
    move-result-object v1

    #@14
    .local v1, i$:Ljava/util/Iterator;
    :cond_14
    :goto_14
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@17
    move-result v3

    #@18
    if-eqz v3, :cond_38

    #@1a
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1d
    move-result-object v2

    #@1e
    check-cast v2, Lcom/android/server/TelephonyRegistry$Record;

    #@20
    .line 609
    .local v2, r:Lcom/android/server/TelephonyRegistry$Record;
    iget v3, v2, Lcom/android/server/TelephonyRegistry$Record;->events:I
    :try_end_22
    .catchall {:try_start_c .. :try_end_22} :catchall_35

    #@22
    and-int/lit16 v3, v3, 0x200

    #@24
    if-eqz v3, :cond_14

    #@26
    .line 611
    :try_start_26
    iget-object v3, v2, Lcom/android/server/TelephonyRegistry$Record;->callback:Lcom/android/internal/telephony/IPhoneStateListener;

    #@28
    invoke-interface {v3, p1}, Lcom/android/internal/telephony/IPhoneStateListener;->onOtaspChanged(I)V
    :try_end_2b
    .catchall {:try_start_26 .. :try_end_2b} :catchall_35
    .catch Landroid/os/RemoteException; {:try_start_26 .. :try_end_2b} :catch_2c

    #@2b
    goto :goto_14

    #@2c
    .line 612
    :catch_2c
    move-exception v0

    #@2d
    .line 613
    .local v0, ex:Landroid/os/RemoteException;
    :try_start_2d
    iget-object v3, p0, Lcom/android/server/TelephonyRegistry;->mRemoveList:Ljava/util/ArrayList;

    #@2f
    iget-object v5, v2, Lcom/android/server/TelephonyRegistry$Record;->binder:Landroid/os/IBinder;

    #@31
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@34
    goto :goto_14

    #@35
    .line 618
    .end local v0           #ex:Landroid/os/RemoteException;
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #r:Lcom/android/server/TelephonyRegistry$Record;
    :catchall_35
    move-exception v3

    #@36
    monitor-exit v4
    :try_end_37
    .catchall {:try_start_2d .. :try_end_37} :catchall_35

    #@37
    throw v3

    #@38
    .line 617
    .restart local v1       #i$:Ljava/util/Iterator;
    :cond_38
    :try_start_38
    invoke-direct {p0}, Lcom/android/server/TelephonyRegistry;->handleRemoveListLocked()V

    #@3b
    .line 618
    monitor-exit v4
    :try_end_3c
    .catchall {:try_start_38 .. :try_end_3c} :catchall_35

    #@3c
    goto :goto_8
.end method

.method public notifyServiceState(Landroid/telephony/ServiceState;)V
    .registers 8
    .parameter "state"

    #@0
    .prologue
    .line 349
    const-string v3, "notifyServiceState()"

    #@2
    invoke-direct {p0, v3}, Lcom/android/server/TelephonyRegistry;->checkNotifyPermission(Ljava/lang/String;)Z

    #@5
    move-result v3

    #@6
    if-nez v3, :cond_9

    #@8
    .line 366
    :goto_8
    return-void

    #@9
    .line 352
    :cond_9
    iget-object v4, p0, Lcom/android/server/TelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@b
    monitor-enter v4

    #@c
    .line 353
    :try_start_c
    iput-object p1, p0, Lcom/android/server/TelephonyRegistry;->mServiceState:Landroid/telephony/ServiceState;

    #@e
    .line 354
    iget-object v3, p0, Lcom/android/server/TelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@10
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@13
    move-result-object v1

    #@14
    .local v1, i$:Ljava/util/Iterator;
    :cond_14
    :goto_14
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@17
    move-result v3

    #@18
    if-eqz v3, :cond_3d

    #@1a
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1d
    move-result-object v2

    #@1e
    check-cast v2, Lcom/android/server/TelephonyRegistry$Record;

    #@20
    .line 355
    .local v2, r:Lcom/android/server/TelephonyRegistry$Record;
    iget v3, v2, Lcom/android/server/TelephonyRegistry$Record;->events:I
    :try_end_22
    .catchall {:try_start_c .. :try_end_22} :catchall_3a

    #@22
    and-int/lit8 v3, v3, 0x1

    #@24
    if-eqz v3, :cond_14

    #@26
    .line 357
    :try_start_26
    iget-object v3, v2, Lcom/android/server/TelephonyRegistry$Record;->callback:Lcom/android/internal/telephony/IPhoneStateListener;

    #@28
    new-instance v5, Landroid/telephony/ServiceState;

    #@2a
    invoke-direct {v5, p1}, Landroid/telephony/ServiceState;-><init>(Landroid/telephony/ServiceState;)V

    #@2d
    invoke-interface {v3, v5}, Lcom/android/internal/telephony/IPhoneStateListener;->onServiceStateChanged(Landroid/telephony/ServiceState;)V
    :try_end_30
    .catchall {:try_start_26 .. :try_end_30} :catchall_3a
    .catch Landroid/os/RemoteException; {:try_start_26 .. :try_end_30} :catch_31

    #@30
    goto :goto_14

    #@31
    .line 358
    :catch_31
    move-exception v0

    #@32
    .line 359
    .local v0, ex:Landroid/os/RemoteException;
    :try_start_32
    iget-object v3, p0, Lcom/android/server/TelephonyRegistry;->mRemoveList:Ljava/util/ArrayList;

    #@34
    iget-object v5, v2, Lcom/android/server/TelephonyRegistry$Record;->binder:Landroid/os/IBinder;

    #@36
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@39
    goto :goto_14

    #@3a
    .line 364
    .end local v0           #ex:Landroid/os/RemoteException;
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #r:Lcom/android/server/TelephonyRegistry$Record;
    :catchall_3a
    move-exception v3

    #@3b
    monitor-exit v4
    :try_end_3c
    .catchall {:try_start_32 .. :try_end_3c} :catchall_3a

    #@3c
    throw v3

    #@3d
    .line 363
    .restart local v1       #i$:Ljava/util/Iterator;
    :cond_3d
    :try_start_3d
    invoke-direct {p0}, Lcom/android/server/TelephonyRegistry;->handleRemoveListLocked()V

    #@40
    .line 364
    monitor-exit v4
    :try_end_41
    .catchall {:try_start_3d .. :try_end_41} :catchall_3a

    #@41
    .line 365
    invoke-direct {p0, p1}, Lcom/android/server/TelephonyRegistry;->broadcastServiceStateChanged(Landroid/telephony/ServiceState;)V

    #@44
    goto :goto_8
.end method

.method public notifySignalStrength(Landroid/telephony/SignalStrength;)V
    .registers 9
    .parameter "signalStrength"

    #@0
    .prologue
    .line 369
    const-string v4, "notifySignalStrength()"

    #@2
    invoke-direct {p0, v4}, Lcom/android/server/TelephonyRegistry;->checkNotifyPermission(Ljava/lang/String;)Z

    #@5
    move-result v4

    #@6
    if-nez v4, :cond_9

    #@8
    .line 395
    :goto_8
    return-void

    #@9
    .line 372
    :cond_9
    iget-object v5, p0, Lcom/android/server/TelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@b
    monitor-enter v5

    #@c
    .line 373
    :try_start_c
    iput-object p1, p0, Lcom/android/server/TelephonyRegistry;->mSignalStrength:Landroid/telephony/SignalStrength;

    #@e
    .line 374
    iget-object v4, p0, Lcom/android/server/TelephonyRegistry;->mRecords:Ljava/util/ArrayList;

    #@10
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@13
    move-result-object v2

    #@14
    .local v2, i$:Ljava/util/Iterator;
    :cond_14
    :goto_14
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@17
    move-result v4

    #@18
    if-eqz v4, :cond_5a

    #@1a
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1d
    move-result-object v3

    #@1e
    check-cast v3, Lcom/android/server/TelephonyRegistry$Record;

    #@20
    .line 375
    .local v3, r:Lcom/android/server/TelephonyRegistry$Record;
    iget v4, v3, Lcom/android/server/TelephonyRegistry$Record;->events:I
    :try_end_22
    .catchall {:try_start_c .. :try_end_22} :catchall_4e

    #@22
    and-int/lit16 v4, v4, 0x100

    #@24
    if-eqz v4, :cond_30

    #@26
    .line 377
    :try_start_26
    iget-object v4, v3, Lcom/android/server/TelephonyRegistry$Record;->callback:Lcom/android/internal/telephony/IPhoneStateListener;

    #@28
    new-instance v6, Landroid/telephony/SignalStrength;

    #@2a
    invoke-direct {v6, p1}, Landroid/telephony/SignalStrength;-><init>(Landroid/telephony/SignalStrength;)V

    #@2d
    invoke-interface {v4, v6}, Lcom/android/internal/telephony/IPhoneStateListener;->onSignalStrengthsChanged(Landroid/telephony/SignalStrength;)V
    :try_end_30
    .catchall {:try_start_26 .. :try_end_30} :catchall_4e
    .catch Landroid/os/RemoteException; {:try_start_26 .. :try_end_30} :catch_51

    #@30
    .line 382
    :cond_30
    :goto_30
    :try_start_30
    iget v4, v3, Lcom/android/server/TelephonyRegistry$Record;->events:I
    :try_end_32
    .catchall {:try_start_30 .. :try_end_32} :catchall_4e

    #@32
    and-int/lit8 v4, v4, 0x2

    #@34
    if-eqz v4, :cond_14

    #@36
    .line 384
    :try_start_36
    invoke-virtual {p1}, Landroid/telephony/SignalStrength;->getGsmSignalStrength()I

    #@39
    move-result v1

    #@3a
    .line 385
    .local v1, gsmSignalStrength:I
    iget-object v4, v3, Lcom/android/server/TelephonyRegistry$Record;->callback:Lcom/android/internal/telephony/IPhoneStateListener;

    #@3c
    const/16 v6, 0x63

    #@3e
    if-ne v1, v6, :cond_41

    #@40
    const/4 v1, -0x1

    #@41
    .end local v1           #gsmSignalStrength:I
    :cond_41
    invoke-interface {v4, v1}, Lcom/android/internal/telephony/IPhoneStateListener;->onSignalStrengthChanged(I)V
    :try_end_44
    .catchall {:try_start_36 .. :try_end_44} :catchall_4e
    .catch Landroid/os/RemoteException; {:try_start_36 .. :try_end_44} :catch_45

    #@44
    goto :goto_14

    #@45
    .line 387
    :catch_45
    move-exception v0

    #@46
    .line 388
    .local v0, ex:Landroid/os/RemoteException;
    :try_start_46
    iget-object v4, p0, Lcom/android/server/TelephonyRegistry;->mRemoveList:Ljava/util/ArrayList;

    #@48
    iget-object v6, v3, Lcom/android/server/TelephonyRegistry$Record;->binder:Landroid/os/IBinder;

    #@4a
    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@4d
    goto :goto_14

    #@4e
    .line 393
    .end local v0           #ex:Landroid/os/RemoteException;
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v3           #r:Lcom/android/server/TelephonyRegistry$Record;
    :catchall_4e
    move-exception v4

    #@4f
    monitor-exit v5
    :try_end_50
    .catchall {:try_start_46 .. :try_end_50} :catchall_4e

    #@50
    throw v4

    #@51
    .line 378
    .restart local v2       #i$:Ljava/util/Iterator;
    .restart local v3       #r:Lcom/android/server/TelephonyRegistry$Record;
    :catch_51
    move-exception v0

    #@52
    .line 379
    .restart local v0       #ex:Landroid/os/RemoteException;
    :try_start_52
    iget-object v4, p0, Lcom/android/server/TelephonyRegistry;->mRemoveList:Ljava/util/ArrayList;

    #@54
    iget-object v6, v3, Lcom/android/server/TelephonyRegistry$Record;->binder:Landroid/os/IBinder;

    #@56
    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@59
    goto :goto_30

    #@5a
    .line 392
    .end local v0           #ex:Landroid/os/RemoteException;
    .end local v3           #r:Lcom/android/server/TelephonyRegistry$Record;
    :cond_5a
    invoke-direct {p0}, Lcom/android/server/TelephonyRegistry;->handleRemoveListLocked()V

    #@5d
    .line 393
    monitor-exit v5
    :try_end_5e
    .catchall {:try_start_52 .. :try_end_5e} :catchall_4e

    #@5e
    .line 394
    invoke-direct {p0, p1}, Lcom/android/server/TelephonyRegistry;->broadcastSignalStrengthChanged(Landroid/telephony/SignalStrength;)V

    #@61
    goto :goto_8
.end method

.method public systemReady()V
    .registers 4

    #@0
    .prologue
    .line 183
    new-instance v0, Landroid/content/IntentFilter;

    #@2
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@5
    .line 184
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.USER_SWITCHED"

    #@7
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@a
    .line 185
    const-string v1, "android.intent.action.USER_REMOVED"

    #@c
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@f
    .line 186
    iget-object v1, p0, Lcom/android/server/TelephonyRegistry;->mContext:Landroid/content/Context;

    #@11
    iget-object v2, p0, Lcom/android/server/TelephonyRegistry;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@13
    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@16
    .line 187
    return-void
.end method
