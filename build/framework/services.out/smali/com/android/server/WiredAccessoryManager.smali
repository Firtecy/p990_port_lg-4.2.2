.class final Lcom/android/server/WiredAccessoryManager;
.super Ljava/lang/Object;
.source "WiredAccessoryManager.java"

# interfaces
.implements Lcom/android/server/input/InputManagerService$WiredAccessoryCallbacks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/WiredAccessoryManager$WiredAccessoryObserver;
    }
.end annotation


# static fields
.field private static final BIT_HDMI_AUDIO:I = 0x10

.field private static final BIT_HEADSET:I = 0x1

.field private static final BIT_HEADSET_NO_MIC:I = 0x2

.field private static final BIT_USB_HEADSET_ANLG:I = 0x4

.field private static final BIT_USB_HEADSET_DGTL:I = 0x8

.field private static final LOG:Z = true

.field private static final MSG_NEW_DEVICE_STATE:I = 0x1

.field private static final NAME_H2W:Ljava/lang/String; = "h2w"

.field private static final NAME_HDMI:Ljava/lang/String; = "hdmi"

.field private static final NAME_HDMI_AUDIO:Ljava/lang/String; = "hdmi_audio"

.field private static final NAME_USB_AUDIO:Ljava/lang/String; = "usb_audio"

.field private static final SUPPORTED_HEADSETS:I = 0x1f

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mAudioManager:Landroid/media/AudioManager;

.field private final mHandler:Landroid/os/Handler;

.field private mHeadsetState:I

.field private final mInputManager:Lcom/android/server/input/InputManagerService;

.field private final mLock:Ljava/lang/Object;

.field private final mObserver:Lcom/android/server/WiredAccessoryManager$WiredAccessoryObserver;

.field private mSwitchValues:I

.field private final mUseDevInputEventForAudioJack:Z

.field private final mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 54
    const-class v0, Lcom/android/server/WiredAccessoryManager;

    #@2
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/android/server/WiredAccessoryManager;->TAG:Ljava/lang/String;

    #@8
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/server/input/InputManagerService;)V
    .registers 8
    .parameter "context"
    .parameter "inputManager"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 87
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 73
    new-instance v1, Ljava/lang/Object;

    #@7
    invoke-direct/range {v1 .. v1}, Ljava/lang/Object;-><init>()V

    #@a
    iput-object v1, p0, Lcom/android/server/WiredAccessoryManager;->mLock:Ljava/lang/Object;

    #@c
    .line 213
    new-instance v1, Lcom/android/server/WiredAccessoryManager$2;

    #@e
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@11
    move-result-object v2

    #@12
    invoke-direct {v1, p0, v2, v4, v3}, Lcom/android/server/WiredAccessoryManager$2;-><init>(Lcom/android/server/WiredAccessoryManager;Landroid/os/Looper;Landroid/os/Handler$Callback;Z)V

    #@15
    iput-object v1, p0, Lcom/android/server/WiredAccessoryManager;->mHandler:Landroid/os/Handler;

    #@17
    .line 88
    const-string v1, "power"

    #@19
    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@1c
    move-result-object v0

    #@1d
    check-cast v0, Landroid/os/PowerManager;

    #@1f
    .line 89
    .local v0, pm:Landroid/os/PowerManager;
    const-string v1, "WiredAccessoryManager"

    #@21
    invoke-virtual {v0, v3, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@24
    move-result-object v1

    #@25
    iput-object v1, p0, Lcom/android/server/WiredAccessoryManager;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@27
    .line 90
    iget-object v1, p0, Lcom/android/server/WiredAccessoryManager;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@29
    const/4 v2, 0x0

    #@2a
    invoke-virtual {v1, v2}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    #@2d
    .line 91
    const-string v1, "audio"

    #@2f
    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@32
    move-result-object v1

    #@33
    check-cast v1, Landroid/media/AudioManager;

    #@35
    iput-object v1, p0, Lcom/android/server/WiredAccessoryManager;->mAudioManager:Landroid/media/AudioManager;

    #@37
    .line 92
    iput-object p2, p0, Lcom/android/server/WiredAccessoryManager;->mInputManager:Lcom/android/server/input/InputManagerService;

    #@39
    .line 94
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@3c
    move-result-object v1

    #@3d
    const v2, 0x1110048

    #@40
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@43
    move-result v1

    #@44
    iput-boolean v1, p0, Lcom/android/server/WiredAccessoryManager;->mUseDevInputEventForAudioJack:Z

    #@46
    .line 97
    new-instance v1, Lcom/android/server/WiredAccessoryManager$WiredAccessoryObserver;

    #@48
    invoke-direct {v1, p0}, Lcom/android/server/WiredAccessoryManager$WiredAccessoryObserver;-><init>(Lcom/android/server/WiredAccessoryManager;)V

    #@4b
    iput-object v1, p0, Lcom/android/server/WiredAccessoryManager;->mObserver:Lcom/android/server/WiredAccessoryManager$WiredAccessoryObserver;

    #@4d
    .line 99
    new-instance v1, Lcom/android/server/WiredAccessoryManager$1;

    #@4f
    invoke-direct {v1, p0}, Lcom/android/server/WiredAccessoryManager$1;-><init>(Lcom/android/server/WiredAccessoryManager;)V

    #@52
    new-instance v2, Landroid/content/IntentFilter;

    #@54
    const-string v3, "android.intent.action.BOOT_COMPLETED"

    #@56
    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@59
    invoke-virtual {p1, v1, v2, v4, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@5c
    .line 106
    return-void
.end method

.method static synthetic access$000(Lcom/android/server/WiredAccessoryManager;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 53
    invoke-direct {p0}, Lcom/android/server/WiredAccessoryManager;->bootCompleted()V

    #@3
    return-void
.end method

.method static synthetic access$100(Lcom/android/server/WiredAccessoryManager;IILjava/lang/String;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 53
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/WiredAccessoryManager;->setDevicesState(IILjava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$200(Lcom/android/server/WiredAccessoryManager;)Landroid/os/PowerManager$WakeLock;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 53
    iget-object v0, p0, Lcom/android/server/WiredAccessoryManager;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/android/server/WiredAccessoryManager;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 53
    iget-object v0, p0, Lcom/android/server/WiredAccessoryManager;->mLock:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$400()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 53
    sget-object v0, Lcom/android/server/WiredAccessoryManager;->TAG:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Lcom/android/server/WiredAccessoryManager;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/android/server/WiredAccessoryManager;->mUseDevInputEventForAudioJack:Z

    #@2
    return v0
.end method

.method static synthetic access$600(Lcom/android/server/WiredAccessoryManager;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 53
    iget v0, p0, Lcom/android/server/WiredAccessoryManager;->mHeadsetState:I

    #@2
    return v0
.end method

.method static synthetic access$700(Lcom/android/server/WiredAccessoryManager;Ljava/lang/String;I)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Lcom/android/server/WiredAccessoryManager;->updateLocked(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method private bootCompleted()V
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, -0x1

    #@2
    const/16 v3, -0x100

    #@4
    .line 109
    iget-boolean v1, p0, Lcom/android/server/WiredAccessoryManager;->mUseDevInputEventForAudioJack:Z

    #@6
    if-eqz v1, :cond_26

    #@8
    .line 110
    const/4 v0, 0x0

    #@9
    .line 111
    .local v0, switchValues:I
    iget-object v1, p0, Lcom/android/server/WiredAccessoryManager;->mInputManager:Lcom/android/server/input/InputManagerService;

    #@b
    const/4 v2, 0x2

    #@c
    invoke-virtual {v1, v4, v3, v2}, Lcom/android/server/input/InputManagerService;->getSwitchState(III)I

    #@f
    move-result v1

    #@10
    if-ne v1, v5, :cond_14

    #@12
    .line 112
    or-int/lit8 v0, v0, 0x4

    #@14
    .line 114
    :cond_14
    iget-object v1, p0, Lcom/android/server/WiredAccessoryManager;->mInputManager:Lcom/android/server/input/InputManagerService;

    #@16
    const/4 v2, 0x4

    #@17
    invoke-virtual {v1, v4, v3, v2}, Lcom/android/server/input/InputManagerService;->getSwitchState(III)I

    #@1a
    move-result v1

    #@1b
    if-ne v1, v5, :cond_1f

    #@1d
    .line 115
    or-int/lit8 v0, v0, 0x10

    #@1f
    .line 117
    :cond_1f
    const-wide/16 v1, 0x0

    #@21
    const/16 v3, 0x14

    #@23
    invoke-virtual {p0, v1, v2, v0, v3}, Lcom/android/server/WiredAccessoryManager;->notifyWiredAccessoryChanged(JII)V

    #@26
    .line 121
    .end local v0           #switchValues:I
    :cond_26
    iget-object v1, p0, Lcom/android/server/WiredAccessoryManager;->mObserver:Lcom/android/server/WiredAccessoryManager$WiredAccessoryObserver;

    #@28
    invoke-virtual {v1}, Lcom/android/server/WiredAccessoryManager$WiredAccessoryObserver;->init()V

    #@2b
    .line 122
    return-void
.end method

.method private setDeviceStateLocked(IIILjava/lang/String;)V
    .registers 11
    .parameter "headset"
    .parameter "headsetState"
    .parameter "prevHeadsetState"
    .parameter "headsetName"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 239
    and-int v2, p2, p1

    #@3
    and-int v3, p3, p1

    #@5
    if-eq v2, v3, :cond_34

    #@7
    .line 243
    and-int v2, p2, p1

    #@9
    if-eqz v2, :cond_35

    #@b
    .line 244
    const/4 v1, 0x1

    #@c
    .line 249
    .local v1, state:I
    :goto_c
    if-ne p1, v5, :cond_37

    #@e
    .line 250
    const/4 v0, 0x4

    #@f
    .line 265
    .local v0, device:I
    :goto_f
    sget-object v3, Lcom/android/server/WiredAccessoryManager;->TAG:Ljava/lang/String;

    #@11
    new-instance v2, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v4, "device "

    #@18
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v4

    #@20
    if-ne v1, v5, :cond_6a

    #@22
    const-string v2, " connected"

    #@24
    :goto_24
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v2

    #@28
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v2

    #@2c
    invoke-static {v3, v2}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 267
    iget-object v2, p0, Lcom/android/server/WiredAccessoryManager;->mAudioManager:Landroid/media/AudioManager;

    #@31
    invoke-virtual {v2, v0, v1, p4}, Landroid/media/AudioManager;->setWiredDeviceConnectionState(IILjava/lang/String;)V

    #@34
    .line 269
    .end local v0           #device:I
    .end local v1           #state:I
    :cond_34
    :goto_34
    return-void

    #@35
    .line 246
    :cond_35
    const/4 v1, 0x0

    #@36
    .restart local v1       #state:I
    goto :goto_c

    #@37
    .line 251
    :cond_37
    const/4 v2, 0x2

    #@38
    if-ne p1, v2, :cond_3d

    #@3a
    .line 252
    const/16 v0, 0x8

    #@3c
    .restart local v0       #device:I
    goto :goto_f

    #@3d
    .line 253
    .end local v0           #device:I
    :cond_3d
    const/4 v2, 0x4

    #@3e
    if-ne p1, v2, :cond_43

    #@40
    .line 254
    const/16 v0, 0x800

    #@42
    .restart local v0       #device:I
    goto :goto_f

    #@43
    .line 255
    .end local v0           #device:I
    :cond_43
    const/16 v2, 0x8

    #@45
    if-ne p1, v2, :cond_4a

    #@47
    .line 256
    const/16 v0, 0x1000

    #@49
    .restart local v0       #device:I
    goto :goto_f

    #@4a
    .line 257
    .end local v0           #device:I
    :cond_4a
    const/16 v2, 0x10

    #@4c
    if-ne p1, v2, :cond_51

    #@4e
    .line 258
    const/16 v0, 0x400

    #@50
    .restart local v0       #device:I
    goto :goto_f

    #@51
    .line 260
    .end local v0           #device:I
    :cond_51
    sget-object v2, Lcom/android/server/WiredAccessoryManager;->TAG:Ljava/lang/String;

    #@53
    new-instance v3, Ljava/lang/StringBuilder;

    #@55
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@58
    const-string v4, "setDeviceState() invalid headset type: "

    #@5a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v3

    #@5e
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@61
    move-result-object v3

    #@62
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@65
    move-result-object v3

    #@66
    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@69
    goto :goto_34

    #@6a
    .line 265
    .restart local v0       #device:I
    :cond_6a
    const-string v2, " disconnected"

    #@6c
    goto :goto_24
.end method

.method private setDevicesState(IILjava/lang/String;)V
    .registers 8
    .parameter "headsetState"
    .parameter "prevHeadsetState"
    .parameter "headsetName"

    #@0
    .prologue
    .line 226
    iget-object v3, p0, Lcom/android/server/WiredAccessoryManager;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v3

    #@3
    .line 227
    const/16 v0, 0x1f

    #@5
    .line 228
    .local v0, allHeadsets:I
    const/4 v1, 0x1

    #@6
    .local v1, curHeadset:I
    :goto_6
    if-eqz v0, :cond_15

    #@8
    .line 229
    and-int v2, v1, v0

    #@a
    if-eqz v2, :cond_12

    #@c
    .line 230
    :try_start_c
    invoke-direct {p0, v1, p1, p2, p3}, Lcom/android/server/WiredAccessoryManager;->setDeviceStateLocked(IIILjava/lang/String;)V

    #@f
    .line 231
    xor-int/lit8 v2, v1, -0x1

    #@11
    and-int/2addr v0, v2

    #@12
    .line 228
    :cond_12
    shl-int/lit8 v1, v1, 0x1

    #@14
    goto :goto_6

    #@15
    .line 234
    :cond_15
    monitor-exit v3

    #@16
    .line 235
    return-void

    #@17
    .line 234
    :catchall_17
    move-exception v2

    #@18
    monitor-exit v3
    :try_end_19
    .catchall {:try_start_c .. :try_end_19} :catchall_17

    #@19
    throw v2
.end method

.method private switchCodeToString(II)Ljava/lang/String;
    .registers 5
    .parameter "switchValues"
    .parameter "switchMask"

    #@0
    .prologue
    .line 272
    new-instance v0, Ljava/lang/StringBuffer;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    #@5
    .line 273
    .local v0, sb:Ljava/lang/StringBuffer;
    and-int/lit8 v1, p2, 0x4

    #@7
    if-eqz v1, :cond_12

    #@9
    and-int/lit8 v1, p1, 0x4

    #@b
    if-eqz v1, :cond_12

    #@d
    .line 275
    const-string v1, "SW_HEADPHONE_INSERT "

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@12
    .line 277
    :cond_12
    and-int/lit8 v1, p2, 0x10

    #@14
    if-eqz v1, :cond_1f

    #@16
    and-int/lit8 v1, p1, 0x10

    #@18
    if-eqz v1, :cond_1f

    #@1a
    .line 279
    const-string v1, "SW_MICROPHONE_INSERT"

    #@1c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@1f
    .line 281
    :cond_1f
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    return-object v1
.end method

.method private updateLocked(Ljava/lang/String;I)V
    .registers 13
    .parameter "newName"
    .parameter "newState"

    #@0
    .prologue
    .line 170
    and-int/lit8 v2, p2, 0x1f

    #@2
    .line 171
    .local v2, headsetState:I
    and-int/lit8 v5, v2, 0x4

    #@4
    .line 172
    .local v5, usb_headset_anlg:I
    and-int/lit8 v6, v2, 0x8

    #@6
    .line 173
    .local v6, usb_headset_dgtl:I
    and-int/lit8 v1, v2, 0x3

    #@8
    .line 174
    .local v1, h2w_headset:I
    const/4 v0, 0x1

    #@9
    .line 175
    .local v0, h2wStateChange:Z
    const/4 v4, 0x1

    #@a
    .line 176
    .local v4, usbStateChange:Z
    sget-object v7, Lcom/android/server/WiredAccessoryManager;->TAG:Ljava/lang/String;

    #@c
    new-instance v8, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v9, "newName="

    #@13
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v8

    #@17
    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v8

    #@1b
    const-string v9, " newState="

    #@1d
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v8

    #@21
    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@24
    move-result-object v8

    #@25
    const-string v9, " headsetState="

    #@27
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v8

    #@2b
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v8

    #@2f
    const-string v9, " prev headsetState="

    #@31
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v8

    #@35
    iget v9, p0, Lcom/android/server/WiredAccessoryManager;->mHeadsetState:I

    #@37
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v8

    #@3b
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v8

    #@3f
    invoke-static {v7, v8}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@42
    .line 181
    iget v7, p0, Lcom/android/server/WiredAccessoryManager;->mHeadsetState:I

    #@44
    if-ne v7, v2, :cond_4e

    #@46
    .line 182
    sget-object v7, Lcom/android/server/WiredAccessoryManager;->TAG:Ljava/lang/String;

    #@48
    const-string v8, "No state change."

    #@4a
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4d
    .line 211
    :goto_4d
    return-void

    #@4e
    .line 189
    :cond_4e
    const/4 v7, 0x3

    #@4f
    if-ne v1, v7, :cond_59

    #@51
    .line 190
    sget-object v7, Lcom/android/server/WiredAccessoryManager;->TAG:Ljava/lang/String;

    #@53
    const-string v8, "Invalid combination, unsetting h2w flag"

    #@55
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@58
    .line 191
    const/4 v0, 0x0

    #@59
    .line 195
    :cond_59
    const/4 v7, 0x4

    #@5a
    if-ne v5, v7, :cond_68

    #@5c
    const/16 v7, 0x8

    #@5e
    if-ne v6, v7, :cond_68

    #@60
    .line 196
    sget-object v7, Lcom/android/server/WiredAccessoryManager;->TAG:Ljava/lang/String;

    #@62
    const-string v8, "Invalid combination, unsetting usb flag"

    #@64
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@67
    .line 197
    const/4 v4, 0x0

    #@68
    .line 199
    :cond_68
    if-nez v0, :cond_74

    #@6a
    if-nez v4, :cond_74

    #@6c
    .line 200
    sget-object v7, Lcom/android/server/WiredAccessoryManager;->TAG:Ljava/lang/String;

    #@6e
    const-string v8, "invalid transition, returning ..."

    #@70
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@73
    goto :goto_4d

    #@74
    .line 204
    :cond_74
    iget-object v7, p0, Lcom/android/server/WiredAccessoryManager;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@76
    invoke-virtual {v7}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@79
    .line 206
    iget-object v7, p0, Lcom/android/server/WiredAccessoryManager;->mHandler:Landroid/os/Handler;

    #@7b
    const/4 v8, 0x1

    #@7c
    iget v9, p0, Lcom/android/server/WiredAccessoryManager;->mHeadsetState:I

    #@7e
    invoke-virtual {v7, v8, v2, v9, p1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@81
    move-result-object v3

    #@82
    .line 208
    .local v3, msg:Landroid/os/Message;
    iget-object v7, p0, Lcom/android/server/WiredAccessoryManager;->mHandler:Landroid/os/Handler;

    #@84
    invoke-virtual {v7, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@87
    .line 210
    iput v2, p0, Lcom/android/server/WiredAccessoryManager;->mHeadsetState:I

    #@89
    goto :goto_4d
.end method


# virtual methods
.method public notifyWiredAccessoryChanged(JII)V
    .registers 9
    .parameter "whenNanos"
    .parameter "switchValues"
    .parameter "switchMask"

    #@0
    .prologue
    .line 126
    sget-object v1, Lcom/android/server/WiredAccessoryManager;->TAG:Ljava/lang/String;

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "notifyWiredAccessoryChanged: when="

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    const-string v3, " bits="

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-direct {p0, p3, p4}, Lcom/android/server/WiredAccessoryManager;->switchCodeToString(II)Ljava/lang/String;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    const-string v3, " mask="

    #@21
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-static {p4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@28
    move-result-object v3

    #@29
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v2

    #@2d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v2

    #@31
    invoke-static {v1, v2}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    .line 130
    iget-object v2, p0, Lcom/android/server/WiredAccessoryManager;->mLock:Ljava/lang/Object;

    #@36
    monitor-enter v2

    #@37
    .line 132
    :try_start_37
    iget v1, p0, Lcom/android/server/WiredAccessoryManager;->mSwitchValues:I

    #@39
    xor-int/lit8 v3, p4, -0x1

    #@3b
    and-int/2addr v1, v3

    #@3c
    or-int/2addr v1, p3

    #@3d
    iput v1, p0, Lcom/android/server/WiredAccessoryManager;->mSwitchValues:I

    #@3f
    .line 133
    iget v1, p0, Lcom/android/server/WiredAccessoryManager;->mSwitchValues:I

    #@41
    and-int/lit8 v1, v1, 0x14

    #@43
    sparse-switch v1, :sswitch_data_5e

    #@46
    .line 151
    const/4 v0, 0x0

    #@47
    .line 155
    .local v0, headset:I
    :goto_47
    const-string v1, "h2w"

    #@49
    iget v3, p0, Lcom/android/server/WiredAccessoryManager;->mHeadsetState:I

    #@4b
    and-int/lit8 v3, v3, -0x4

    #@4d
    or-int/2addr v3, v0

    #@4e
    invoke-direct {p0, v1, v3}, Lcom/android/server/WiredAccessoryManager;->updateLocked(Ljava/lang/String;I)V

    #@51
    .line 156
    monitor-exit v2

    #@52
    .line 157
    return-void

    #@53
    .line 135
    .end local v0           #headset:I
    :sswitch_53
    const/4 v0, 0x0

    #@54
    .line 136
    .restart local v0       #headset:I
    goto :goto_47

    #@55
    .line 139
    .end local v0           #headset:I
    :sswitch_55
    const/4 v0, 0x2

    #@56
    .line 140
    .restart local v0       #headset:I
    goto :goto_47

    #@57
    .line 143
    .end local v0           #headset:I
    :sswitch_57
    const/4 v0, 0x1

    #@58
    .line 144
    .restart local v0       #headset:I
    goto :goto_47

    #@59
    .line 147
    .end local v0           #headset:I
    :sswitch_59
    const/4 v0, 0x1

    #@5a
    .line 148
    .restart local v0       #headset:I
    goto :goto_47

    #@5b
    .line 156
    .end local v0           #headset:I
    :catchall_5b
    move-exception v1

    #@5c
    monitor-exit v2
    :try_end_5d
    .catchall {:try_start_37 .. :try_end_5d} :catchall_5b

    #@5d
    throw v1

    #@5e
    .line 133
    :sswitch_data_5e
    .sparse-switch
        0x0 -> :sswitch_53
        0x4 -> :sswitch_55
        0x10 -> :sswitch_59
        0x14 -> :sswitch_57
    .end sparse-switch
.end method
