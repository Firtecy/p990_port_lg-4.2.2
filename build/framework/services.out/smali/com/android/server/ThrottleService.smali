.class public Lcom/android/server/ThrottleService;
.super Landroid/net/IThrottleManager$Stub;
.source "ThrottleService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/ThrottleService$DataRecorder;,
        Lcom/android/server/ThrottleService$MyHandler;,
        Lcom/android/server/ThrottleService$SettingsObserver;,
        Lcom/android/server/ThrottleService$InterfaceObserver;
    }
.end annotation


# static fields
.field private static final ACTION_POLL:Ljava/lang/String; = "com.android.server.ThrottleManager.action.POLL"

.field private static final ACTION_RESET:Ljava/lang/String; = "com.android.server.ThorottleManager.action.RESET"

.field private static final DBG:Z = true

.field private static final EVENT_IFACE_UP:I = 0x4

.field private static final EVENT_POLICY_CHANGED:I = 0x1

.field private static final EVENT_POLL_ALARM:I = 0x2

.field private static final EVENT_REBOOT_RECOVERY:I = 0x0

.field private static final EVENT_RESET_ALARM:I = 0x3

.field private static final INITIAL_POLL_DELAY_SEC:I = 0x5a

.field private static final MAX_NTP_CACHE_AGE:J = 0x5265c00L

.field private static final NOTIFICATION_WARNING:I = 0x2

.field private static POLL_REQUEST:I = 0x0

.field private static RESET_REQUEST:I = 0x0

.field private static final TAG:Ljava/lang/String; = "ThrottleService"

.field private static final TESTING_ENABLED_PROPERTY:Ljava/lang/String; = "persist.throttle.testing"

.field private static final TESTING_POLLING_PERIOD_SEC:I = 0x3c

.field private static final TESTING_RESET_PERIOD_SEC:I = 0x258

.field private static final TESTING_THRESHOLD:J = 0x100000L

.field private static final THROTTLE_INDEX_UNINITIALIZED:I = -0x1

.field private static final THROTTLE_INDEX_UNTHROTTLED:I

.field private static final VDBG:Z


# instance fields
.field private mAlarmManager:Landroid/app/AlarmManager;

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mIface:Ljava/lang/String;

.field private mInterfaceObserver:Lcom/android/server/ThrottleService$InterfaceObserver;

.field private mLastRead:J

.field private mLastWrite:J

.field private mMaxNtpCacheAge:J

.field private mNMService:Landroid/os/INetworkManagementService;

.field private mNotificationManager:Landroid/app/NotificationManager;

.field private mPendingPollIntent:Landroid/app/PendingIntent;

.field private mPendingResetIntent:Landroid/app/PendingIntent;

.field private mPolicyNotificationsAllowedMask:I

.field private mPolicyPollPeriodSec:I

.field private mPolicyResetDay:I

.field private mPolicyThreshold:Ljava/util/concurrent/atomic/AtomicLong;

.field private mPolicyThrottleValue:Ljava/util/concurrent/atomic/AtomicInteger;

.field private mPollStickyBroadcast:Landroid/content/Intent;

.field private mRecorder:Lcom/android/server/ThrottleService$DataRecorder;

.field private mSettingsObserver:Lcom/android/server/ThrottleService$SettingsObserver;

.field private mThread:Landroid/os/HandlerThread;

.field private mThrottleIndex:Ljava/util/concurrent/atomic/AtomicInteger;

.field private mThrottlingNotification:Landroid/app/Notification;

.field private mTime:Landroid/util/TrustedTime;

.field private mWarningNotificationSent:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 103
    const/4 v0, 0x0

    #@1
    sput v0, Lcom/android/server/ThrottleService;->POLL_REQUEST:I

    #@3
    .line 106
    const/4 v0, 0x1

    #@4
    sput v0, Lcom/android/server/ThrottleService;->RESET_REQUEST:I

    #@6
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 6
    .parameter "context"

    #@0
    .prologue
    .line 139
    invoke-static {}, Lcom/android/server/ThrottleService;->getNetworkManagementService()Landroid/os/INetworkManagementService;

    #@3
    move-result-object v0

    #@4
    invoke-static {p1}, Landroid/util/NtpTrustedTime;->getInstance(Landroid/content/Context;)Landroid/util/NtpTrustedTime;

    #@7
    move-result-object v1

    #@8
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@b
    move-result-object v2

    #@c
    const v3, 0x104003a

    #@f
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@12
    move-result-object v2

    #@13
    invoke-direct {p0, p1, v0, v1, v2}, Lcom/android/server/ThrottleService;-><init>(Landroid/content/Context;Landroid/os/INetworkManagementService;Landroid/util/TrustedTime;Ljava/lang/String;)V

    #@16
    .line 141
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/INetworkManagementService;Landroid/util/TrustedTime;Ljava/lang/String;)V
    .registers 11
    .parameter "context"
    .parameter "nmService"
    .parameter "time"
    .parameter "iface"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v4, 0x0

    #@2
    .line 144
    invoke-direct {p0}, Landroid/net/IThrottleManager$Stub;-><init>()V

    #@5
    .line 91
    const-wide/32 v2, 0x5265c00

    #@8
    iput-wide v2, p0, Lcom/android/server/ThrottleService;->mMaxNtpCacheAge:J

    #@a
    .line 120
    iput-boolean v4, p0, Lcom/android/server/ThrottleService;->mWarningNotificationSent:Z

    #@c
    .line 146
    iput-object p1, p0, Lcom/android/server/ThrottleService;->mContext:Landroid/content/Context;

    #@e
    .line 148
    new-instance v2, Ljava/util/concurrent/atomic/AtomicLong;

    #@10
    invoke-direct {v2}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    #@13
    iput-object v2, p0, Lcom/android/server/ThrottleService;->mPolicyThreshold:Ljava/util/concurrent/atomic/AtomicLong;

    #@15
    .line 149
    new-instance v2, Ljava/util/concurrent/atomic/AtomicInteger;

    #@17
    invoke-direct {v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    #@1a
    iput-object v2, p0, Lcom/android/server/ThrottleService;->mPolicyThrottleValue:Ljava/util/concurrent/atomic/AtomicInteger;

    #@1c
    .line 150
    new-instance v2, Ljava/util/concurrent/atomic/AtomicInteger;

    #@1e
    invoke-direct {v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    #@21
    iput-object v2, p0, Lcom/android/server/ThrottleService;->mThrottleIndex:Ljava/util/concurrent/atomic/AtomicInteger;

    #@23
    .line 152
    iput-object p4, p0, Lcom/android/server/ThrottleService;->mIface:Ljava/lang/String;

    #@25
    .line 153
    iget-object v2, p0, Lcom/android/server/ThrottleService;->mContext:Landroid/content/Context;

    #@27
    const-string v3, "alarm"

    #@29
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@2c
    move-result-object v2

    #@2d
    check-cast v2, Landroid/app/AlarmManager;

    #@2f
    iput-object v2, p0, Lcom/android/server/ThrottleService;->mAlarmManager:Landroid/app/AlarmManager;

    #@31
    .line 154
    new-instance v0, Landroid/content/Intent;

    #@33
    const-string v2, "com.android.server.ThrottleManager.action.POLL"

    #@35
    invoke-direct {v0, v2, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    #@38
    .line 155
    .local v0, pollIntent:Landroid/content/Intent;
    iget-object v2, p0, Lcom/android/server/ThrottleService;->mContext:Landroid/content/Context;

    #@3a
    sget v3, Lcom/android/server/ThrottleService;->POLL_REQUEST:I

    #@3c
    invoke-static {v2, v3, v0, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@3f
    move-result-object v2

    #@40
    iput-object v2, p0, Lcom/android/server/ThrottleService;->mPendingPollIntent:Landroid/app/PendingIntent;

    #@42
    .line 156
    new-instance v1, Landroid/content/Intent;

    #@44
    const-string v2, "com.android.server.ThorottleManager.action.RESET"

    #@46
    invoke-direct {v1, v2, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    #@49
    .line 157
    .local v1, resetIntent:Landroid/content/Intent;
    iget-object v2, p0, Lcom/android/server/ThrottleService;->mContext:Landroid/content/Context;

    #@4b
    sget v3, Lcom/android/server/ThrottleService;->RESET_REQUEST:I

    #@4d
    invoke-static {v2, v3, v1, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@50
    move-result-object v2

    #@51
    iput-object v2, p0, Lcom/android/server/ThrottleService;->mPendingResetIntent:Landroid/app/PendingIntent;

    #@53
    .line 159
    iput-object p2, p0, Lcom/android/server/ThrottleService;->mNMService:Landroid/os/INetworkManagementService;

    #@55
    .line 160
    iput-object p3, p0, Lcom/android/server/ThrottleService;->mTime:Landroid/util/TrustedTime;

    #@57
    .line 162
    iget-object v2, p0, Lcom/android/server/ThrottleService;->mContext:Landroid/content/Context;

    #@59
    const-string v3, "notification"

    #@5b
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@5e
    move-result-object v2

    #@5f
    check-cast v2, Landroid/app/NotificationManager;

    #@61
    iput-object v2, p0, Lcom/android/server/ThrottleService;->mNotificationManager:Landroid/app/NotificationManager;

    #@63
    .line 164
    return-void
.end method

.method static synthetic access$000(Lcom/android/server/ThrottleService;)Ljava/util/concurrent/atomic/AtomicInteger;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 72
    iget-object v0, p0, Lcom/android/server/ThrottleService;->mThrottleIndex:Ljava/util/concurrent/atomic/AtomicInteger;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/server/ThrottleService;)Lcom/android/server/ThrottleService$DataRecorder;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 72
    iget-object v0, p0, Lcom/android/server/ThrottleService;->mRecorder:Lcom/android/server/ThrottleService$DataRecorder;

    #@2
    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/server/ThrottleService;)J
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 72
    iget-wide v0, p0, Lcom/android/server/ThrottleService;->mMaxNtpCacheAge:J

    #@2
    return-wide v0
.end method

.method static synthetic access$1002(Lcom/android/server/ThrottleService;J)J
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 72
    iput-wide p1, p0, Lcom/android/server/ThrottleService;->mMaxNtpCacheAge:J

    #@2
    return-wide p1
.end method

.method static synthetic access$102(Lcom/android/server/ThrottleService;Lcom/android/server/ThrottleService$DataRecorder;)Lcom/android/server/ThrottleService$DataRecorder;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 72
    iput-object p1, p0, Lcom/android/server/ThrottleService;->mRecorder:Lcom/android/server/ThrottleService$DataRecorder;

    #@2
    return-object p1
.end method

.method static synthetic access$1100(Lcom/android/server/ThrottleService;)Landroid/util/TrustedTime;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 72
    iget-object v0, p0, Lcom/android/server/ThrottleService;->mTime:Landroid/util/TrustedTime;

    #@2
    return-object v0
.end method

.method static synthetic access$1200(Lcom/android/server/ThrottleService;)Landroid/os/INetworkManagementService;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 72
    iget-object v0, p0, Lcom/android/server/ThrottleService;->mNMService:Landroid/os/INetworkManagementService;

    #@2
    return-object v0
.end method

.method static synthetic access$1300(Lcom/android/server/ThrottleService;)J
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 72
    iget-wide v0, p0, Lcom/android/server/ThrottleService;->mLastRead:J

    #@2
    return-wide v0
.end method

.method static synthetic access$1302(Lcom/android/server/ThrottleService;J)J
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 72
    iput-wide p1, p0, Lcom/android/server/ThrottleService;->mLastRead:J

    #@2
    return-wide p1
.end method

.method static synthetic access$1314(Lcom/android/server/ThrottleService;J)J
    .registers 5
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 72
    iget-wide v0, p0, Lcom/android/server/ThrottleService;->mLastRead:J

    #@2
    add-long/2addr v0, p1

    #@3
    iput-wide v0, p0, Lcom/android/server/ThrottleService;->mLastRead:J

    #@5
    return-wide v0
.end method

.method static synthetic access$1400(Lcom/android/server/ThrottleService;)J
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 72
    iget-wide v0, p0, Lcom/android/server/ThrottleService;->mLastWrite:J

    #@2
    return-wide v0
.end method

.method static synthetic access$1402(Lcom/android/server/ThrottleService;J)J
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 72
    iput-wide p1, p0, Lcom/android/server/ThrottleService;->mLastWrite:J

    #@2
    return-wide p1
.end method

.method static synthetic access$1414(Lcom/android/server/ThrottleService;J)J
    .registers 5
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 72
    iget-wide v0, p0, Lcom/android/server/ThrottleService;->mLastWrite:J

    #@2
    add-long/2addr v0, p1

    #@3
    iput-wide v0, p0, Lcom/android/server/ThrottleService;->mLastWrite:J

    #@5
    return-wide v0
.end method

.method static synthetic access$1502(Lcom/android/server/ThrottleService;Landroid/content/Intent;)Landroid/content/Intent;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 72
    iput-object p1, p0, Lcom/android/server/ThrottleService;->mPollStickyBroadcast:Landroid/content/Intent;

    #@2
    return-object p1
.end method

.method static synthetic access$1600(Lcom/android/server/ThrottleService;)Landroid/app/PendingIntent;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 72
    iget-object v0, p0, Lcom/android/server/ThrottleService;->mPendingPollIntent:Landroid/app/PendingIntent;

    #@2
    return-object v0
.end method

.method static synthetic access$1700(Lcom/android/server/ThrottleService;)Landroid/app/AlarmManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 72
    iget-object v0, p0, Lcom/android/server/ThrottleService;->mAlarmManager:Landroid/app/AlarmManager;

    #@2
    return-object v0
.end method

.method static synthetic access$1800(Lcom/android/server/ThrottleService;)Landroid/app/NotificationManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 72
    iget-object v0, p0, Lcom/android/server/ThrottleService;->mNotificationManager:Landroid/app/NotificationManager;

    #@2
    return-object v0
.end method

.method static synthetic access$1900(Lcom/android/server/ThrottleService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 72
    iget-boolean v0, p0, Lcom/android/server/ThrottleService;->mWarningNotificationSent:Z

    #@2
    return v0
.end method

.method static synthetic access$1902(Lcom/android/server/ThrottleService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 72
    iput-boolean p1, p0, Lcom/android/server/ThrottleService;->mWarningNotificationSent:Z

    #@2
    return p1
.end method

.method static synthetic access$200(Lcom/android/server/ThrottleService;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 72
    iget-object v0, p0, Lcom/android/server/ThrottleService;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$2000(Lcom/android/server/ThrottleService;)Landroid/app/Notification;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 72
    iget-object v0, p0, Lcom/android/server/ThrottleService;->mThrottlingNotification:Landroid/app/Notification;

    #@2
    return-object v0
.end method

.method static synthetic access$2002(Lcom/android/server/ThrottleService;Landroid/app/Notification;)Landroid/app/Notification;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 72
    iput-object p1, p0, Lcom/android/server/ThrottleService;->mThrottlingNotification:Landroid/app/Notification;

    #@2
    return-object p1
.end method

.method static synthetic access$2100(Lcom/android/server/ThrottleService;)Landroid/app/PendingIntent;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 72
    iget-object v0, p0, Lcom/android/server/ThrottleService;->mPendingResetIntent:Landroid/app/PendingIntent;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/android/server/ThrottleService;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 72
    iget-object v0, p0, Lcom/android/server/ThrottleService;->mHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Lcom/android/server/ThrottleService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 72
    iget v0, p0, Lcom/android/server/ThrottleService;->mPolicyPollPeriodSec:I

    #@2
    return v0
.end method

.method static synthetic access$402(Lcom/android/server/ThrottleService;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 72
    iput p1, p0, Lcom/android/server/ThrottleService;->mPolicyPollPeriodSec:I

    #@2
    return p1
.end method

.method static synthetic access$500(Lcom/android/server/ThrottleService;)Ljava/util/concurrent/atomic/AtomicLong;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 72
    iget-object v0, p0, Lcom/android/server/ThrottleService;->mPolicyThreshold:Ljava/util/concurrent/atomic/AtomicLong;

    #@2
    return-object v0
.end method

.method static synthetic access$600(Lcom/android/server/ThrottleService;)Ljava/util/concurrent/atomic/AtomicInteger;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 72
    iget-object v0, p0, Lcom/android/server/ThrottleService;->mPolicyThrottleValue:Ljava/util/concurrent/atomic/AtomicInteger;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Lcom/android/server/ThrottleService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 72
    iget v0, p0, Lcom/android/server/ThrottleService;->mPolicyResetDay:I

    #@2
    return v0
.end method

.method static synthetic access$702(Lcom/android/server/ThrottleService;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 72
    iput p1, p0, Lcom/android/server/ThrottleService;->mPolicyResetDay:I

    #@2
    return p1
.end method

.method static synthetic access$800(Lcom/android/server/ThrottleService;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 72
    iget-object v0, p0, Lcom/android/server/ThrottleService;->mIface:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$900(Lcom/android/server/ThrottleService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 72
    iget v0, p0, Lcom/android/server/ThrottleService;->mPolicyNotificationsAllowedMask:I

    #@2
    return v0
.end method

.method static synthetic access$902(Lcom/android/server/ThrottleService;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 72
    iput p1, p0, Lcom/android/server/ThrottleService;->mPolicyNotificationsAllowedMask:I

    #@2
    return p1
.end method

.method private enforceAccessPermission()V
    .registers 4

    #@0
    .prologue
    .line 242
    iget-object v0, p0, Lcom/android/server/ThrottleService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.ACCESS_NETWORK_STATE"

    #@4
    const-string v2, "ThrottleService"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 245
    return-void
.end method

.method private static getNetworkManagementService()Landroid/os/INetworkManagementService;
    .registers 2

    #@0
    .prologue
    .line 134
    const-string v1, "network_management"

    #@2
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@5
    move-result-object v0

    #@6
    .line 135
    .local v0, b:Landroid/os/IBinder;
    invoke-static {v0}, Landroid/os/INetworkManagementService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/INetworkManagementService;

    #@9
    move-result-object v1

    #@a
    return-object v1
.end method

.method private ntpToWallTime(J)J
    .registers 9
    .parameter "ntpTime"

    #@0
    .prologue
    .line 249
    iget-object v4, p0, Lcom/android/server/ThrottleService;->mTime:Landroid/util/TrustedTime;

    #@2
    invoke-interface {v4}, Landroid/util/TrustedTime;->hasCache()Z

    #@5
    move-result v4

    #@6
    if-eqz v4, :cond_16

    #@8
    iget-object v4, p0, Lcom/android/server/ThrottleService;->mTime:Landroid/util/TrustedTime;

    #@a
    invoke-interface {v4}, Landroid/util/TrustedTime;->currentTimeMillis()J

    #@d
    move-result-wide v0

    #@e
    .line 251
    .local v0, bestNow:J
    :goto_e
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@11
    move-result-wide v2

    #@12
    .line 252
    .local v2, localNow:J
    sub-long v4, p1, v0

    #@14
    add-long/2addr v4, v2

    #@15
    return-wide v4

    #@16
    .line 249
    .end local v0           #bestNow:J
    .end local v2           #localNow:J
    :cond_16
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@19
    move-result-wide v0

    #@1a
    goto :goto_e
.end method


# virtual methods
.method dispatchPoll()V
    .registers 3

    #@0
    .prologue
    .line 377
    iget-object v0, p0, Lcom/android/server/ThrottleService;->mHandler:Landroid/os/Handler;

    #@2
    const/4 v1, 0x2

    #@3
    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@6
    move-result-object v0

    #@7
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@a
    .line 378
    return-void
.end method

.method dispatchReset()V
    .registers 3

    #@0
    .prologue
    .line 381
    iget-object v0, p0, Lcom/android/server/ThrottleService;->mHandler:Landroid/os/Handler;

    #@2
    const/4 v1, 0x3

    #@3
    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@6
    move-result-object v0

    #@7
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@a
    .line 382
    return-void
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 12
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    const-wide/16 v6, 0x3e8

    #@2
    .line 1134
    iget-object v1, p0, Lcom/android/server/ThrottleService;->mContext:Landroid/content/Context;

    #@4
    const-string v2, "android.permission.DUMP"

    #@6
    invoke-virtual {v1, v2}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_35

    #@c
    .line 1137
    new-instance v1, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v2, "Permission Denial: can\'t dump ThrottleService from from pid="

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@1a
    move-result v2

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    const-string v2, ", uid="

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@28
    move-result v2

    #@29
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v1

    #@2d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v1

    #@31
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@34
    .line 1159
    :cond_34
    return-void

    #@35
    .line 1142
    :cond_35
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    #@38
    .line 1144
    new-instance v1, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v2, "The threshold is "

    #@3f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v1

    #@43
    iget-object v2, p0, Lcom/android/server/ThrottleService;->mPolicyThreshold:Ljava/util/concurrent/atomic/AtomicLong;

    #@45
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    #@48
    move-result-wide v2

    #@49
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v1

    #@4d
    const-string v2, ", after which you experince throttling to "

    #@4f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v1

    #@53
    iget-object v2, p0, Lcom/android/server/ThrottleService;->mPolicyThrottleValue:Ljava/util/concurrent/atomic/AtomicInteger;

    #@55
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    #@58
    move-result v2

    #@59
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v1

    #@5d
    const-string v2, "kbps"

    #@5f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v1

    #@63
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@66
    move-result-object v1

    #@67
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@6a
    .line 1147
    new-instance v1, Ljava/lang/StringBuilder;

    #@6c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@6f
    const-string v2, "Current period is "

    #@71
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v1

    #@75
    iget-object v2, p0, Lcom/android/server/ThrottleService;->mRecorder:Lcom/android/server/ThrottleService$DataRecorder;

    #@77
    invoke-virtual {v2}, Lcom/android/server/ThrottleService$DataRecorder;->getPeriodEnd()J

    #@7a
    move-result-wide v2

    #@7b
    iget-object v4, p0, Lcom/android/server/ThrottleService;->mRecorder:Lcom/android/server/ThrottleService$DataRecorder;

    #@7d
    invoke-virtual {v4}, Lcom/android/server/ThrottleService$DataRecorder;->getPeriodStart()J

    #@80
    move-result-wide v4

    #@81
    sub-long/2addr v2, v4

    #@82
    div-long/2addr v2, v6

    #@83
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@86
    move-result-object v1

    #@87
    const-string v2, " seconds long "

    #@89
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v1

    #@8d
    const-string v2, "and ends in "

    #@8f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@92
    move-result-object v1

    #@93
    iget-object v2, p0, Lcom/android/server/ThrottleService;->mIface:Ljava/lang/String;

    #@95
    invoke-virtual {p0, v2}, Lcom/android/server/ThrottleService;->getResetTime(Ljava/lang/String;)J

    #@98
    move-result-wide v2

    #@99
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@9c
    move-result-wide v4

    #@9d
    sub-long/2addr v2, v4

    #@9e
    div-long/2addr v2, v6

    #@9f
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v1

    #@a3
    const-string v2, " seconds."

    #@a5
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v1

    #@a9
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ac
    move-result-object v1

    #@ad
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@b0
    .line 1151
    new-instance v1, Ljava/lang/StringBuilder;

    #@b2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b5
    const-string v2, "Polling every "

    #@b7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v1

    #@bb
    iget v2, p0, Lcom/android/server/ThrottleService;->mPolicyPollPeriodSec:I

    #@bd
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v1

    #@c1
    const-string v2, " seconds"

    #@c3
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v1

    #@c7
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ca
    move-result-object v1

    #@cb
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@ce
    .line 1152
    new-instance v1, Ljava/lang/StringBuilder;

    #@d0
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d3
    const-string v2, "Current Throttle Index is "

    #@d5
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d8
    move-result-object v1

    #@d9
    iget-object v2, p0, Lcom/android/server/ThrottleService;->mThrottleIndex:Ljava/util/concurrent/atomic/AtomicInteger;

    #@db
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    #@de
    move-result v2

    #@df
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e2
    move-result-object v1

    #@e3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e6
    move-result-object v1

    #@e7
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@ea
    .line 1153
    new-instance v1, Ljava/lang/StringBuilder;

    #@ec
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@ef
    const-string v2, "mMaxNtpCacheAge="

    #@f1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f4
    move-result-object v1

    #@f5
    iget-wide v2, p0, Lcom/android/server/ThrottleService;->mMaxNtpCacheAge:J

    #@f7
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@fa
    move-result-object v1

    #@fb
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fe
    move-result-object v1

    #@ff
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@102
    .line 1155
    const/4 v0, 0x0

    #@103
    .local v0, i:I
    :goto_103
    iget-object v1, p0, Lcom/android/server/ThrottleService;->mRecorder:Lcom/android/server/ThrottleService$DataRecorder;

    #@105
    invoke-virtual {v1}, Lcom/android/server/ThrottleService$DataRecorder;->getPeriodCount()I

    #@108
    move-result v1

    #@109
    if-ge v0, v1, :cond_34

    #@10b
    .line 1156
    new-instance v1, Ljava/lang/StringBuilder;

    #@10d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@110
    const-string v2, " Period["

    #@112
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@115
    move-result-object v1

    #@116
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@119
    move-result-object v1

    #@11a
    const-string v2, "] - read:"

    #@11c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11f
    move-result-object v1

    #@120
    iget-object v2, p0, Lcom/android/server/ThrottleService;->mRecorder:Lcom/android/server/ThrottleService$DataRecorder;

    #@122
    invoke-virtual {v2, v0}, Lcom/android/server/ThrottleService$DataRecorder;->getPeriodRx(I)J

    #@125
    move-result-wide v2

    #@126
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@129
    move-result-object v1

    #@12a
    const-string v2, ", written:"

    #@12c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12f
    move-result-object v1

    #@130
    iget-object v2, p0, Lcom/android/server/ThrottleService;->mRecorder:Lcom/android/server/ThrottleService$DataRecorder;

    #@132
    invoke-virtual {v2, v0}, Lcom/android/server/ThrottleService$DataRecorder;->getPeriodTx(I)J

    #@135
    move-result-wide v2

    #@136
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@139
    move-result-object v1

    #@13a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13d
    move-result-object v1

    #@13e
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@141
    .line 1155
    add-int/lit8 v0, v0, 0x1

    #@143
    goto :goto_103
.end method

.method public getByteCount(Ljava/lang/String;III)J
    .registers 7
    .parameter "iface"
    .parameter "dir"
    .parameter "period"
    .parameter "ago"

    #@0
    .prologue
    .line 306
    invoke-direct {p0}, Lcom/android/server/ThrottleService;->enforceAccessPermission()V

    #@3
    .line 307
    if-nez p3, :cond_1c

    #@5
    iget-object v0, p0, Lcom/android/server/ThrottleService;->mRecorder:Lcom/android/server/ThrottleService$DataRecorder;

    #@7
    if-eqz v0, :cond_1c

    #@9
    .line 308
    if-nez p2, :cond_12

    #@b
    iget-object v0, p0, Lcom/android/server/ThrottleService;->mRecorder:Lcom/android/server/ThrottleService$DataRecorder;

    #@d
    invoke-virtual {v0, p4}, Lcom/android/server/ThrottleService$DataRecorder;->getPeriodTx(I)J

    #@10
    move-result-wide v0

    #@11
    .line 311
    :goto_11
    return-wide v0

    #@12
    .line 309
    :cond_12
    const/4 v0, 0x1

    #@13
    if-ne p2, v0, :cond_1c

    #@15
    iget-object v0, p0, Lcom/android/server/ThrottleService;->mRecorder:Lcom/android/server/ThrottleService$DataRecorder;

    #@17
    invoke-virtual {v0, p4}, Lcom/android/server/ThrottleService$DataRecorder;->getPeriodRx(I)J

    #@1a
    move-result-wide v0

    #@1b
    goto :goto_11

    #@1c
    .line 311
    :cond_1c
    const-wide/16 v0, 0x0

    #@1e
    goto :goto_11
.end method

.method public getCliffLevel(Ljava/lang/String;I)I
    .registers 4
    .parameter "iface"
    .parameter "cliff"

    #@0
    .prologue
    .line 291
    invoke-direct {p0}, Lcom/android/server/ThrottleService;->enforceAccessPermission()V

    #@3
    .line 292
    const/4 v0, 0x1

    #@4
    if-ne p2, v0, :cond_d

    #@6
    .line 293
    iget-object v0, p0, Lcom/android/server/ThrottleService;->mPolicyThrottleValue:Ljava/util/concurrent/atomic/AtomicInteger;

    #@8
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    #@b
    move-result v0

    #@c
    .line 295
    :goto_c
    return v0

    #@d
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_c
.end method

.method public getCliffThreshold(Ljava/lang/String;I)J
    .registers 5
    .parameter "iface"
    .parameter "cliff"

    #@0
    .prologue
    .line 282
    invoke-direct {p0}, Lcom/android/server/ThrottleService;->enforceAccessPermission()V

    #@3
    .line 283
    const/4 v0, 0x1

    #@4
    if-ne p2, v0, :cond_d

    #@6
    .line 284
    iget-object v0, p0, Lcom/android/server/ThrottleService;->mPolicyThreshold:Ljava/util/concurrent/atomic/AtomicLong;

    #@8
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    #@b
    move-result-wide v0

    #@c
    .line 286
    :goto_c
    return-wide v0

    #@d
    :cond_d
    const-wide/16 v0, 0x0

    #@f
    goto :goto_c
.end method

.method public getHelpUri()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 299
    invoke-direct {p0}, Lcom/android/server/ThrottleService;->enforceAccessPermission()V

    #@3
    .line 300
    iget-object v0, p0, Lcom/android/server/ThrottleService;->mContext:Landroid/content/Context;

    #@5
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@8
    move-result-object v0

    #@9
    const-string v1, "throttle_help_uri"

    #@b
    invoke-static {v0, v1}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    return-object v0
.end method

.method public getPeriodStartTime(Ljava/lang/String;)J
    .registers 5
    .parameter "iface"

    #@0
    .prologue
    .line 271
    const-wide/16 v0, 0x0

    #@2
    .line 272
    .local v0, startTime:J
    invoke-direct {p0}, Lcom/android/server/ThrottleService;->enforceAccessPermission()V

    #@5
    .line 273
    iget-object v2, p0, Lcom/android/server/ThrottleService;->mRecorder:Lcom/android/server/ThrottleService$DataRecorder;

    #@7
    if-eqz v2, :cond_f

    #@9
    .line 274
    iget-object v2, p0, Lcom/android/server/ThrottleService;->mRecorder:Lcom/android/server/ThrottleService$DataRecorder;

    #@b
    invoke-virtual {v2}, Lcom/android/server/ThrottleService$DataRecorder;->getPeriodStart()J

    #@e
    move-result-wide v0

    #@f
    .line 276
    :cond_f
    invoke-direct {p0, v0, v1}, Lcom/android/server/ThrottleService;->ntpToWallTime(J)J

    #@12
    move-result-wide v0

    #@13
    .line 277
    return-wide v0
.end method

.method public getResetTime(Ljava/lang/String;)J
    .registers 5
    .parameter "iface"

    #@0
    .prologue
    .line 259
    invoke-direct {p0}, Lcom/android/server/ThrottleService;->enforceAccessPermission()V

    #@3
    .line 260
    const-wide/16 v0, 0x0

    #@5
    .line 261
    .local v0, resetTime:J
    iget-object v2, p0, Lcom/android/server/ThrottleService;->mRecorder:Lcom/android/server/ThrottleService$DataRecorder;

    #@7
    if-eqz v2, :cond_f

    #@9
    .line 262
    iget-object v2, p0, Lcom/android/server/ThrottleService;->mRecorder:Lcom/android/server/ThrottleService$DataRecorder;

    #@b
    invoke-virtual {v2}, Lcom/android/server/ThrottleService$DataRecorder;->getPeriodEnd()J

    #@e
    move-result-wide v0

    #@f
    .line 264
    :cond_f
    invoke-direct {p0, v0, v1}, Lcom/android/server/ThrottleService;->ntpToWallTime(J)J

    #@12
    move-result-wide v0

    #@13
    .line 265
    return-wide v0
.end method

.method public getThrottle(Ljava/lang/String;)I
    .registers 4
    .parameter "iface"

    #@0
    .prologue
    .line 317
    invoke-direct {p0}, Lcom/android/server/ThrottleService;->enforceAccessPermission()V

    #@3
    .line 318
    iget-object v0, p0, Lcom/android/server/ThrottleService;->mThrottleIndex:Ljava/util/concurrent/atomic/AtomicInteger;

    #@5
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    #@8
    move-result v0

    #@9
    const/4 v1, 0x1

    #@a
    if-ne v0, v1, :cond_13

    #@c
    .line 319
    iget-object v0, p0, Lcom/android/server/ThrottleService;->mPolicyThrottleValue:Ljava/util/concurrent/atomic/AtomicInteger;

    #@e
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    #@11
    move-result v0

    #@12
    .line 321
    :goto_12
    return v0

    #@13
    :cond_13
    const/4 v0, 0x0

    #@14
    goto :goto_12
.end method

.method shutdown()V
    .registers 4

    #@0
    .prologue
    .line 363
    iget-object v0, p0, Lcom/android/server/ThrottleService;->mThread:Landroid/os/HandlerThread;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 364
    iget-object v0, p0, Lcom/android/server/ThrottleService;->mThread:Landroid/os/HandlerThread;

    #@6
    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    #@9
    .line 367
    :cond_9
    iget-object v0, p0, Lcom/android/server/ThrottleService;->mSettingsObserver:Lcom/android/server/ThrottleService$SettingsObserver;

    #@b
    if-eqz v0, :cond_14

    #@d
    .line 368
    iget-object v0, p0, Lcom/android/server/ThrottleService;->mSettingsObserver:Lcom/android/server/ThrottleService$SettingsObserver;

    #@f
    iget-object v1, p0, Lcom/android/server/ThrottleService;->mContext:Landroid/content/Context;

    #@11
    invoke-virtual {v0, v1}, Lcom/android/server/ThrottleService$SettingsObserver;->unregister(Landroid/content/Context;)V

    #@14
    .line 371
    :cond_14
    iget-object v0, p0, Lcom/android/server/ThrottleService;->mPollStickyBroadcast:Landroid/content/Intent;

    #@16
    if-eqz v0, :cond_21

    #@18
    .line 372
    iget-object v0, p0, Lcom/android/server/ThrottleService;->mContext:Landroid/content/Context;

    #@1a
    iget-object v1, p0, Lcom/android/server/ThrottleService;->mPollStickyBroadcast:Landroid/content/Intent;

    #@1c
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@1e
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->removeStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@21
    .line 374
    :cond_21
    return-void
.end method

.method systemReady()V
    .registers 6

    #@0
    .prologue
    .line 326
    iget-object v1, p0, Lcom/android/server/ThrottleService;->mContext:Landroid/content/Context;

    #@2
    new-instance v2, Lcom/android/server/ThrottleService$1;

    #@4
    invoke-direct {v2, p0}, Lcom/android/server/ThrottleService$1;-><init>(Lcom/android/server/ThrottleService;)V

    #@7
    new-instance v3, Landroid/content/IntentFilter;

    #@9
    const-string v4, "com.android.server.ThrottleManager.action.POLL"

    #@b
    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@e
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@11
    .line 334
    iget-object v1, p0, Lcom/android/server/ThrottleService;->mContext:Landroid/content/Context;

    #@13
    new-instance v2, Lcom/android/server/ThrottleService$2;

    #@15
    invoke-direct {v2, p0}, Lcom/android/server/ThrottleService$2;-><init>(Lcom/android/server/ThrottleService;)V

    #@18
    new-instance v3, Landroid/content/IntentFilter;

    #@1a
    const-string v4, "com.android.server.ThorottleManager.action.RESET"

    #@1c
    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@1f
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@22
    .line 343
    new-instance v1, Landroid/os/HandlerThread;

    #@24
    const-string v2, "ThrottleService"

    #@26
    invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    #@29
    iput-object v1, p0, Lcom/android/server/ThrottleService;->mThread:Landroid/os/HandlerThread;

    #@2b
    .line 344
    iget-object v1, p0, Lcom/android/server/ThrottleService;->mThread:Landroid/os/HandlerThread;

    #@2d
    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    #@30
    .line 345
    new-instance v1, Lcom/android/server/ThrottleService$MyHandler;

    #@32
    iget-object v2, p0, Lcom/android/server/ThrottleService;->mThread:Landroid/os/HandlerThread;

    #@34
    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@37
    move-result-object v2

    #@38
    invoke-direct {v1, p0, v2}, Lcom/android/server/ThrottleService$MyHandler;-><init>(Lcom/android/server/ThrottleService;Landroid/os/Looper;)V

    #@3b
    iput-object v1, p0, Lcom/android/server/ThrottleService;->mHandler:Landroid/os/Handler;

    #@3d
    .line 346
    iget-object v1, p0, Lcom/android/server/ThrottleService;->mHandler:Landroid/os/Handler;

    #@3f
    const/4 v2, 0x0

    #@40
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@43
    move-result-object v1

    #@44
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    #@47
    .line 348
    new-instance v1, Lcom/android/server/ThrottleService$InterfaceObserver;

    #@49
    iget-object v2, p0, Lcom/android/server/ThrottleService;->mHandler:Landroid/os/Handler;

    #@4b
    const/4 v3, 0x4

    #@4c
    iget-object v4, p0, Lcom/android/server/ThrottleService;->mIface:Ljava/lang/String;

    #@4e
    invoke-direct {v1, v2, v3, v4}, Lcom/android/server/ThrottleService$InterfaceObserver;-><init>(Landroid/os/Handler;ILjava/lang/String;)V

    #@51
    iput-object v1, p0, Lcom/android/server/ThrottleService;->mInterfaceObserver:Lcom/android/server/ThrottleService$InterfaceObserver;

    #@53
    .line 350
    :try_start_53
    iget-object v1, p0, Lcom/android/server/ThrottleService;->mNMService:Landroid/os/INetworkManagementService;

    #@55
    iget-object v2, p0, Lcom/android/server/ThrottleService;->mInterfaceObserver:Lcom/android/server/ThrottleService$InterfaceObserver;

    #@57
    invoke-interface {v1, v2}, Landroid/os/INetworkManagementService;->registerObserver(Landroid/net/INetworkManagementEventObserver;)V
    :try_end_5a
    .catch Landroid/os/RemoteException; {:try_start_53 .. :try_end_5a} :catch_6c

    #@5a
    .line 355
    :goto_5a
    new-instance v1, Lcom/android/server/ThrottleService$SettingsObserver;

    #@5c
    iget-object v2, p0, Lcom/android/server/ThrottleService;->mHandler:Landroid/os/Handler;

    #@5e
    const/4 v3, 0x1

    #@5f
    invoke-direct {v1, v2, v3}, Lcom/android/server/ThrottleService$SettingsObserver;-><init>(Landroid/os/Handler;I)V

    #@62
    iput-object v1, p0, Lcom/android/server/ThrottleService;->mSettingsObserver:Lcom/android/server/ThrottleService$SettingsObserver;

    #@64
    .line 356
    iget-object v1, p0, Lcom/android/server/ThrottleService;->mSettingsObserver:Lcom/android/server/ThrottleService$SettingsObserver;

    #@66
    iget-object v2, p0, Lcom/android/server/ThrottleService;->mContext:Landroid/content/Context;

    #@68
    invoke-virtual {v1, v2}, Lcom/android/server/ThrottleService$SettingsObserver;->register(Landroid/content/Context;)V

    #@6b
    .line 357
    return-void

    #@6c
    .line 351
    :catch_6c
    move-exception v0

    #@6d
    .line 352
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "ThrottleService"

    #@6f
    new-instance v2, Ljava/lang/StringBuilder;

    #@71
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@74
    const-string v3, "Could not register InterfaceObserver "

    #@76
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v2

    #@7a
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v2

    #@7e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@81
    move-result-object v2

    #@82
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@85
    goto :goto_5a
.end method
