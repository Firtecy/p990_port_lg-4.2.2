.class Lcom/android/server/WifiService$LockList;
.super Ljava/lang/Object;
.source "WifiService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/WifiService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LockList"
.end annotation


# instance fields
.field private mList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/server/WifiService$WifiLock;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/android/server/WifiService;


# direct methods
.method private constructor <init>(Lcom/android/server/WifiService;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 1839
    iput-object p1, p0, Lcom/android/server/WifiService$LockList;->this$0:Lcom/android/server/WifiService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 1840
    new-instance v0, Ljava/util/ArrayList;

    #@7
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@a
    iput-object v0, p0, Lcom/android/server/WifiService$LockList;->mList:Ljava/util/List;

    #@c
    .line 1841
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/WifiService;Lcom/android/server/WifiService$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1836
    invoke-direct {p0, p1}, Lcom/android/server/WifiService$LockList;-><init>(Lcom/android/server/WifiService;)V

    #@3
    return-void
.end method

.method static synthetic access$3500(Lcom/android/server/WifiService$LockList;)Ljava/util/List;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1836
    iget-object v0, p0, Lcom/android/server/WifiService$LockList;->mList:Ljava/util/List;

    #@2
    return-object v0
.end method

.method static synthetic access$3600(Lcom/android/server/WifiService$LockList;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1836
    invoke-direct {p0}, Lcom/android/server/WifiService$LockList;->hasLocks()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$3700(Lcom/android/server/WifiService$LockList;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1836
    invoke-direct {p0}, Lcom/android/server/WifiService$LockList;->getStrongestLockMode()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$3800(Lcom/android/server/WifiService$LockList;Ljava/io/PrintWriter;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1836
    invoke-direct {p0, p1}, Lcom/android/server/WifiService$LockList;->dump(Ljava/io/PrintWriter;)V

    #@3
    return-void
.end method

.method static synthetic access$4500(Lcom/android/server/WifiService$LockList;Lcom/android/server/WifiService$WifiLock;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1836
    invoke-direct {p0, p1}, Lcom/android/server/WifiService$LockList;->addLock(Lcom/android/server/WifiService$WifiLock;)V

    #@3
    return-void
.end method

.method static synthetic access$4600(Lcom/android/server/WifiService$LockList;Landroid/os/IBinder;)I
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1836
    invoke-direct {p0, p1}, Lcom/android/server/WifiService$LockList;->findLockByBinder(Landroid/os/IBinder;)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$4700(Lcom/android/server/WifiService$LockList;Landroid/os/IBinder;)Lcom/android/server/WifiService$WifiLock;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1836
    invoke-direct {p0, p1}, Lcom/android/server/WifiService$LockList;->removeLock(Landroid/os/IBinder;)Lcom/android/server/WifiService$WifiLock;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method private addLock(Lcom/android/server/WifiService$WifiLock;)V
    .registers 3
    .parameter "lock"

    #@0
    .prologue
    .line 1864
    iget-object v0, p1, Lcom/android/server/WifiService$DeathRecipient;->mBinder:Landroid/os/IBinder;

    #@2
    invoke-direct {p0, v0}, Lcom/android/server/WifiService$LockList;->findLockByBinder(Landroid/os/IBinder;)I

    #@5
    move-result v0

    #@6
    if-gez v0, :cond_d

    #@8
    .line 1865
    iget-object v0, p0, Lcom/android/server/WifiService$LockList;->mList:Ljava/util/List;

    #@a
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@d
    .line 1867
    :cond_d
    return-void
.end method

.method private dump(Ljava/io/PrintWriter;)V
    .registers 5
    .parameter "pw"

    #@0
    .prologue
    .line 1889
    iget-object v2, p0, Lcom/android/server/WifiService$LockList;->mList:Ljava/util/List;

    #@2
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v0

    #@6
    .local v0, i$:Ljava/util/Iterator;
    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_1b

    #@c
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    check-cast v1, Lcom/android/server/WifiService$WifiLock;

    #@12
    .line 1890
    .local v1, l:Lcom/android/server/WifiService$WifiLock;
    const-string v2, "    "

    #@14
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@17
    .line 1891
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@1a
    goto :goto_6

    #@1b
    .line 1893
    .end local v1           #l:Lcom/android/server/WifiService$WifiLock;
    :cond_1b
    return-void
.end method

.method private findLockByBinder(Landroid/os/IBinder;)I
    .registers 5
    .parameter "binder"

    #@0
    .prologue
    .line 1881
    iget-object v2, p0, Lcom/android/server/WifiService$LockList;->mList:Ljava/util/List;

    #@2
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@5
    move-result v1

    #@6
    .line 1882
    .local v1, size:I
    add-int/lit8 v0, v1, -0x1

    #@8
    .local v0, i:I
    :goto_8
    if-ltz v0, :cond_1a

    #@a
    .line 1883
    iget-object v2, p0, Lcom/android/server/WifiService$LockList;->mList:Ljava/util/List;

    #@c
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v2

    #@10
    check-cast v2, Lcom/android/server/WifiService$WifiLock;

    #@12
    iget-object v2, v2, Lcom/android/server/WifiService$DeathRecipient;->mBinder:Landroid/os/IBinder;

    #@14
    if-ne v2, p1, :cond_17

    #@16
    .line 1885
    .end local v0           #i:I
    :goto_16
    return v0

    #@17
    .line 1882
    .restart local v0       #i:I
    :cond_17
    add-int/lit8 v0, v0, -0x1

    #@19
    goto :goto_8

    #@1a
    .line 1885
    :cond_1a
    const/4 v0, -0x1

    #@1b
    goto :goto_16
.end method

.method private declared-synchronized getStrongestLockMode()I
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 1848
    monitor-enter p0

    #@2
    :try_start_2
    iget-object v1, p0, Lcom/android/server/WifiService$LockList;->mList:Ljava/util/List;

    #@4
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z
    :try_end_7
    .catchall {:try_start_2 .. :try_end_7} :catchall_2c

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_c

    #@a
    .line 1860
    :cond_a
    :goto_a
    monitor-exit p0

    #@b
    return v0

    #@c
    .line 1852
    :cond_c
    :try_start_c
    iget-object v1, p0, Lcom/android/server/WifiService$LockList;->this$0:Lcom/android/server/WifiService;

    #@e
    invoke-static {v1}, Lcom/android/server/WifiService;->access$4100(Lcom/android/server/WifiService;)I

    #@11
    move-result v1

    #@12
    iget-object v2, p0, Lcom/android/server/WifiService$LockList;->this$0:Lcom/android/server/WifiService;

    #@14
    invoke-static {v2}, Lcom/android/server/WifiService;->access$4200(Lcom/android/server/WifiService;)I

    #@17
    move-result v2

    #@18
    if-le v1, v2, :cond_1c

    #@1a
    .line 1853
    const/4 v0, 0x3

    #@1b
    goto :goto_a

    #@1c
    .line 1856
    :cond_1c
    iget-object v1, p0, Lcom/android/server/WifiService$LockList;->this$0:Lcom/android/server/WifiService;

    #@1e
    invoke-static {v1}, Lcom/android/server/WifiService;->access$4300(Lcom/android/server/WifiService;)I

    #@21
    move-result v1

    #@22
    iget-object v2, p0, Lcom/android/server/WifiService$LockList;->this$0:Lcom/android/server/WifiService;

    #@24
    invoke-static {v2}, Lcom/android/server/WifiService;->access$4400(Lcom/android/server/WifiService;)I
    :try_end_27
    .catchall {:try_start_c .. :try_end_27} :catchall_2c

    #@27
    move-result v2

    #@28
    if-gt v1, v2, :cond_a

    #@2a
    .line 1860
    const/4 v0, 0x2

    #@2b
    goto :goto_a

    #@2c
    .line 1848
    :catchall_2c
    move-exception v0

    #@2d
    monitor-exit p0

    #@2e
    throw v0
.end method

.method private declared-synchronized hasLocks()Z
    .registers 2

    #@0
    .prologue
    .line 1844
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/server/WifiService$LockList;->mList:Ljava/util/List;

    #@3
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_e

    #@6
    move-result v0

    #@7
    if-nez v0, :cond_c

    #@9
    const/4 v0, 0x1

    #@a
    :goto_a
    monitor-exit p0

    #@b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_a

    #@e
    :catchall_e
    move-exception v0

    #@f
    monitor-exit p0

    #@10
    throw v0
.end method

.method private removeLock(Landroid/os/IBinder;)Lcom/android/server/WifiService$WifiLock;
    .registers 5
    .parameter "binder"

    #@0
    .prologue
    .line 1870
    invoke-direct {p0, p1}, Lcom/android/server/WifiService$LockList;->findLockByBinder(Landroid/os/IBinder;)I

    #@3
    move-result v0

    #@4
    .line 1871
    .local v0, index:I
    if-ltz v0, :cond_12

    #@6
    .line 1872
    iget-object v2, p0, Lcom/android/server/WifiService$LockList;->mList:Ljava/util/List;

    #@8
    invoke-interface {v2, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    #@b
    move-result-object v1

    #@c
    check-cast v1, Lcom/android/server/WifiService$WifiLock;

    #@e
    .line 1873
    .local v1, ret:Lcom/android/server/WifiService$WifiLock;
    invoke-virtual {v1}, Lcom/android/server/WifiService$WifiLock;->unlinkDeathRecipient()V

    #@11
    .line 1876
    .end local v1           #ret:Lcom/android/server/WifiService$WifiLock;
    :goto_11
    return-object v1

    #@12
    :cond_12
    const/4 v1, 0x0

    #@13
    goto :goto_11
.end method
