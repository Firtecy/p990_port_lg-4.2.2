.class Lcom/android/server/NotificationManagerService$2;
.super Landroid/content/BroadcastReceiver;
.source "NotificationManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/NotificationManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/NotificationManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/NotificationManagerService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 590
    iput-object p1, p0, Lcom/android/server/NotificationManagerService$2;->this$0:Lcom/android/server/NotificationManagerService;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 22
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 593
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v9

    #@4
    .line 595
    .local v9, action:Ljava/lang/String;
    const/16 v17, 0x0

    #@6
    .line 596
    .local v17, queryRestart:Z
    const/4 v15, 0x0

    #@7
    .line 598
    .local v15, packageChanged:Z
    const-string v1, "android.intent.action.PACKAGE_REMOVED"

    #@9
    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c
    move-result v1

    #@d
    if-nez v1, :cond_2f

    #@f
    const-string v1, "android.intent.action.PACKAGE_RESTARTED"

    #@11
    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14
    move-result v1

    #@15
    if-nez v1, :cond_2f

    #@17
    const-string v1, "android.intent.action.PACKAGE_CHANGED"

    #@19
    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1c
    move-result v15

    #@1d
    if-nez v15, :cond_2f

    #@1f
    const-string v1, "android.intent.action.QUERY_PACKAGE_RESTART"

    #@21
    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@24
    move-result v17

    #@25
    if-nez v17, :cond_2f

    #@27
    const-string v1, "android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE"

    #@29
    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2c
    move-result v1

    #@2d
    if-eqz v1, :cond_98

    #@2f
    .line 603
    :cond_2f
    const/16 v16, 0x0

    #@31
    .line 604
    .local v16, pkgList:[Ljava/lang/String;
    const-string v1, "android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE"

    #@33
    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@36
    move-result v1

    #@37
    if-eqz v1, :cond_60

    #@39
    .line 605
    const-string v1, "android.intent.extra.changed_package_list"

    #@3b
    move-object/from16 v0, p2

    #@3d
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    #@40
    move-result-object v16

    #@41
    .line 628
    :goto_41
    if-eqz v16, :cond_71

    #@43
    move-object/from16 v0, v16

    #@45
    array-length v1, v0

    #@46
    if-lez v1, :cond_71

    #@48
    .line 629
    move-object/from16 v10, v16

    #@4a
    .local v10, arr$:[Ljava/lang/String;
    array-length v14, v10

    #@4b
    .local v14, len$:I
    const/4 v13, 0x0

    #@4c
    .local v13, i$:I
    :goto_4c
    if-ge v13, v14, :cond_71

    #@4e
    aget-object v2, v10, v13

    #@50
    .line 630
    .local v2, pkgName:Ljava/lang/String;
    move-object/from16 v0, p0

    #@52
    iget-object v1, v0, Lcom/android/server/NotificationManagerService$2;->this$0:Lcom/android/server/NotificationManagerService;

    #@54
    const/4 v3, 0x0

    #@55
    const/4 v4, 0x0

    #@56
    if-nez v17, :cond_96

    #@58
    const/4 v5, 0x1

    #@59
    :goto_59
    const/4 v6, -0x1

    #@5a
    invoke-virtual/range {v1 .. v6}, Lcom/android/server/NotificationManagerService;->cancelAllNotificationsInt(Ljava/lang/String;IIZI)Z

    #@5d
    .line 629
    add-int/lit8 v13, v13, 0x1

    #@5f
    goto :goto_4c

    #@60
    .line 606
    .end local v2           #pkgName:Ljava/lang/String;
    .end local v10           #arr$:[Ljava/lang/String;
    .end local v13           #i$:I
    .end local v14           #len$:I
    :cond_60
    if-eqz v17, :cond_6b

    #@62
    .line 607
    const-string v1, "android.intent.extra.PACKAGES"

    #@64
    move-object/from16 v0, p2

    #@66
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    #@69
    move-result-object v16

    #@6a
    goto :goto_41

    #@6b
    .line 609
    :cond_6b
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    #@6e
    move-result-object v18

    #@6f
    .line 610
    .local v18, uri:Landroid/net/Uri;
    if-nez v18, :cond_72

    #@71
    .line 692
    .end local v16           #pkgList:[Ljava/lang/String;
    .end local v18           #uri:Landroid/net/Uri;
    :cond_71
    :goto_71
    return-void

    #@72
    .line 613
    .restart local v16       #pkgList:[Ljava/lang/String;
    .restart local v18       #uri:Landroid/net/Uri;
    :cond_72
    invoke-virtual/range {v18 .. v18}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    #@75
    move-result-object v2

    #@76
    .line 614
    .restart local v2       #pkgName:Ljava/lang/String;
    if-eqz v2, :cond_71

    #@78
    .line 617
    if-eqz v15, :cond_8d

    #@7a
    .line 619
    move-object/from16 v0, p0

    #@7c
    iget-object v1, v0, Lcom/android/server/NotificationManagerService$2;->this$0:Lcom/android/server/NotificationManagerService;

    #@7e
    iget-object v1, v1, Lcom/android/server/NotificationManagerService;->mContext:Landroid/content/Context;

    #@80
    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@83
    move-result-object v1

    #@84
    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I

    #@87
    move-result v12

    #@88
    .line 621
    .local v12, enabled:I
    const/4 v1, 0x1

    #@89
    if-eq v12, v1, :cond_71

    #@8b
    if-eqz v12, :cond_71

    #@8d
    .line 626
    .end local v12           #enabled:I
    :cond_8d
    const/4 v1, 0x1

    #@8e
    new-array v0, v1, [Ljava/lang/String;

    #@90
    move-object/from16 v16, v0

    #@92
    .end local v16           #pkgList:[Ljava/lang/String;
    const/4 v1, 0x0

    #@93
    aput-object v2, v16, v1

    #@95
    .restart local v16       #pkgList:[Ljava/lang/String;
    goto :goto_41

    #@96
    .line 630
    .end local v18           #uri:Landroid/net/Uri;
    .restart local v10       #arr$:[Ljava/lang/String;
    .restart local v13       #i$:I
    .restart local v14       #len$:I
    :cond_96
    const/4 v5, 0x0

    #@97
    goto :goto_59

    #@98
    .line 634
    .end local v2           #pkgName:Ljava/lang/String;
    .end local v10           #arr$:[Ljava/lang/String;
    .end local v13           #i$:I
    .end local v14           #len$:I
    .end local v16           #pkgList:[Ljava/lang/String;
    :cond_98
    const-string v1, "android.intent.action.SCREEN_ON"

    #@9a
    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9d
    move-result v1

    #@9e
    if-eqz v1, :cond_a9

    #@a0
    .line 637
    move-object/from16 v0, p0

    #@a2
    iget-object v1, v0, Lcom/android/server/NotificationManagerService$2;->this$0:Lcom/android/server/NotificationManagerService;

    #@a4
    const/4 v3, 0x1

    #@a5
    invoke-static {v1, v3}, Lcom/android/server/NotificationManagerService;->access$1202(Lcom/android/server/NotificationManagerService;Z)Z

    #@a8
    goto :goto_71

    #@a9
    .line 638
    :cond_a9
    const-string v1, "android.intent.action.SCREEN_OFF"

    #@ab
    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ae
    move-result v1

    #@af
    if-eqz v1, :cond_ba

    #@b1
    .line 639
    move-object/from16 v0, p0

    #@b3
    iget-object v1, v0, Lcom/android/server/NotificationManagerService$2;->this$0:Lcom/android/server/NotificationManagerService;

    #@b5
    const/4 v3, 0x0

    #@b6
    invoke-static {v1, v3}, Lcom/android/server/NotificationManagerService;->access$1202(Lcom/android/server/NotificationManagerService;Z)Z

    #@b9
    goto :goto_71

    #@ba
    .line 640
    :cond_ba
    const-string v1, "android.intent.action.PHONE_STATE"

    #@bc
    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@bf
    move-result v1

    #@c0
    if-eqz v1, :cond_df

    #@c2
    .line 641
    move-object/from16 v0, p0

    #@c4
    iget-object v1, v0, Lcom/android/server/NotificationManagerService$2;->this$0:Lcom/android/server/NotificationManagerService;

    #@c6
    const-string v3, "state"

    #@c8
    move-object/from16 v0, p2

    #@ca
    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@cd
    move-result-object v3

    #@ce
    sget-object v4, Landroid/telephony/TelephonyManager;->EXTRA_STATE_OFFHOOK:Ljava/lang/String;

    #@d0
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d3
    move-result v3

    #@d4
    invoke-static {v1, v3}, Lcom/android/server/NotificationManagerService;->access$1302(Lcom/android/server/NotificationManagerService;Z)Z

    #@d7
    .line 643
    move-object/from16 v0, p0

    #@d9
    iget-object v1, v0, Lcom/android/server/NotificationManagerService$2;->this$0:Lcom/android/server/NotificationManagerService;

    #@db
    invoke-static {v1}, Lcom/android/server/NotificationManagerService;->access$1400(Lcom/android/server/NotificationManagerService;)V

    #@de
    goto :goto_71

    #@df
    .line 644
    :cond_df
    const-string v1, "android.intent.action.USER_STOPPED"

    #@e1
    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e4
    move-result v1

    #@e5
    if-eqz v1, :cond_ff

    #@e7
    .line 645
    const-string v1, "android.intent.extra.user_handle"

    #@e9
    const/4 v3, -0x1

    #@ea
    move-object/from16 v0, p2

    #@ec
    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@ef
    move-result v8

    #@f0
    .line 646
    .local v8, userHandle:I
    if-ltz v8, :cond_71

    #@f2
    .line 647
    move-object/from16 v0, p0

    #@f4
    iget-object v3, v0, Lcom/android/server/NotificationManagerService$2;->this$0:Lcom/android/server/NotificationManagerService;

    #@f6
    const/4 v4, 0x0

    #@f7
    const/4 v5, 0x0

    #@f8
    const/4 v6, 0x0

    #@f9
    const/4 v7, 0x1

    #@fa
    invoke-virtual/range {v3 .. v8}, Lcom/android/server/NotificationManagerService;->cancelAllNotificationsInt(Ljava/lang/String;IIZI)Z

    #@fd
    goto/16 :goto_71

    #@ff
    .line 649
    .end local v8           #userHandle:I
    :cond_ff
    const-string v1, "android.intent.action.USER_PRESENT"

    #@101
    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@104
    move-result v1

    #@105
    if-eqz v1, :cond_128

    #@107
    .line 651
    move-object/from16 v0, p0

    #@109
    iget-object v1, v0, Lcom/android/server/NotificationManagerService$2;->this$0:Lcom/android/server/NotificationManagerService;

    #@10b
    invoke-static {v1}, Lcom/android/server/NotificationManagerService;->access$1500(Lcom/android/server/NotificationManagerService;)Ljava/util/List;

    #@10e
    move-result-object v1

    #@10f
    if-nez v1, :cond_71

    #@111
    move-object/from16 v0, p0

    #@113
    iget-object v1, v0, Lcom/android/server/NotificationManagerService$2;->this$0:Lcom/android/server/NotificationManagerService;

    #@115
    invoke-static {v1}, Lcom/android/server/NotificationManagerService;->access$1600(Lcom/android/server/NotificationManagerService;)Z

    #@118
    move-result v1

    #@119
    if-nez v1, :cond_71

    #@11b
    .line 652
    move-object/from16 v0, p0

    #@11d
    iget-object v1, v0, Lcom/android/server/NotificationManagerService$2;->this$0:Lcom/android/server/NotificationManagerService;

    #@11f
    invoke-static {v1}, Lcom/android/server/NotificationManagerService;->access$1700(Lcom/android/server/NotificationManagerService;)Lcom/android/server/LightsService$Light;

    #@122
    move-result-object v1

    #@123
    invoke-virtual {v1}, Lcom/android/server/LightsService$Light;->turnOff()V

    #@126
    goto/16 :goto_71

    #@128
    .line 656
    :cond_128
    sget-boolean v1, Lcom/lge/config/ConfigBuildFlags;->CAPP_OSP:Z

    #@12a
    if-eqz v1, :cond_189

    #@12c
    const-string v1, "com.lge.osp.ALERT_CONNECTION"

    #@12e
    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@131
    move-result v1

    #@132
    if-eqz v1, :cond_189

    #@134
    .line 658
    move-object/from16 v0, p0

    #@136
    iget-object v1, v0, Lcom/android/server/NotificationManagerService$2;->this$0:Lcom/android/server/NotificationManagerService;

    #@138
    invoke-static {v1}, Lcom/android/server/NotificationManagerService;->access$1800(Lcom/android/server/NotificationManagerService;)Landroid/content/ServiceConnection;

    #@13b
    move-result-object v1

    #@13c
    if-eqz v1, :cond_71

    #@13e
    .line 659
    const-string v1, "connected"

    #@140
    const/4 v3, 0x0

    #@141
    move-object/from16 v0, p2

    #@143
    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@146
    move-result v1

    #@147
    if-eqz v1, :cond_169

    #@149
    .line 660
    new-instance v11, Landroid/content/Intent;

    #@14b
    invoke-direct {v11}, Landroid/content/Intent;-><init>()V

    #@14e
    .line 662
    .local v11, bindIntent:Landroid/content/Intent;
    const-string v1, "com.lge.osp"

    #@150
    const-string v3, "com.lge.osp.OSPService"

    #@152
    invoke-virtual {v11, v1, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@155
    .line 663
    move-object/from16 v0, p0

    #@157
    iget-object v1, v0, Lcom/android/server/NotificationManagerService$2;->this$0:Lcom/android/server/NotificationManagerService;

    #@159
    iget-object v1, v1, Lcom/android/server/NotificationManagerService;->mContext:Landroid/content/Context;

    #@15b
    move-object/from16 v0, p0

    #@15d
    iget-object v3, v0, Lcom/android/server/NotificationManagerService$2;->this$0:Lcom/android/server/NotificationManagerService;

    #@15f
    invoke-static {v3}, Lcom/android/server/NotificationManagerService;->access$1800(Lcom/android/server/NotificationManagerService;)Landroid/content/ServiceConnection;

    #@162
    move-result-object v3

    #@163
    const/4 v4, 0x0

    #@164
    invoke-virtual {v1, v11, v3, v4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    #@167
    goto/16 :goto_71

    #@169
    .line 664
    .end local v11           #bindIntent:Landroid/content/Intent;
    :cond_169
    move-object/from16 v0, p0

    #@16b
    iget-object v1, v0, Lcom/android/server/NotificationManagerService$2;->this$0:Lcom/android/server/NotificationManagerService;

    #@16d
    invoke-static {v1}, Lcom/android/server/NotificationManagerService;->access$1900(Lcom/android/server/NotificationManagerService;)Ljava/lang/Object;

    #@170
    move-result-object v1

    #@171
    if-eqz v1, :cond_71

    #@173
    .line 666
    :try_start_173
    move-object/from16 v0, p0

    #@175
    iget-object v1, v0, Lcom/android/server/NotificationManagerService$2;->this$0:Lcom/android/server/NotificationManagerService;

    #@177
    iget-object v1, v1, Lcom/android/server/NotificationManagerService;->mContext:Landroid/content/Context;

    #@179
    move-object/from16 v0, p0

    #@17b
    iget-object v3, v0, Lcom/android/server/NotificationManagerService$2;->this$0:Lcom/android/server/NotificationManagerService;

    #@17d
    invoke-static {v3}, Lcom/android/server/NotificationManagerService;->access$1800(Lcom/android/server/NotificationManagerService;)Landroid/content/ServiceConnection;

    #@180
    move-result-object v3

    #@181
    invoke-virtual {v1, v3}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_184
    .catch Ljava/lang/IllegalArgumentException; {:try_start_173 .. :try_end_184} :catch_186

    #@184
    goto/16 :goto_71

    #@186
    .line 667
    :catch_186
    move-exception v1

    #@187
    goto/16 :goto_71

    #@189
    .line 674
    :cond_189
    const-string v1, "com.lge.media.STOP_NOTIFICATION"

    #@18b
    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@18e
    move-result v1

    #@18f
    if-eqz v1, :cond_19a

    #@191
    .line 675
    move-object/from16 v0, p0

    #@193
    iget-object v1, v0, Lcom/android/server/NotificationManagerService$2;->this$0:Lcom/android/server/NotificationManagerService;

    #@195
    invoke-virtual {v1}, Lcom/android/server/NotificationManagerService;->stopSound()V

    #@198
    goto/16 :goto_71

    #@19a
    .line 679
    :cond_19a
    const-string v1, "com.lge.intent.action.EXCESS_SPC_FAIL_EVENT"

    #@19c
    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@19f
    move-result v1

    #@1a0
    if-eqz v1, :cond_1bb

    #@1a2
    .line 680
    move-object/from16 v0, p0

    #@1a4
    iget-object v1, v0, Lcom/android/server/NotificationManagerService$2;->this$0:Lcom/android/server/NotificationManagerService;

    #@1a6
    invoke-static {v1}, Lcom/android/server/NotificationManagerService;->access$2000(Lcom/android/server/NotificationManagerService;)Ljava/lang/String;

    #@1a9
    move-result-object v1

    #@1aa
    const-string v3, "VZW"

    #@1ac
    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@1af
    move-result v1

    #@1b0
    if-eqz v1, :cond_71

    #@1b2
    .line 681
    move-object/from16 v0, p0

    #@1b4
    iget-object v1, v0, Lcom/android/server/NotificationManagerService$2;->this$0:Lcom/android/server/NotificationManagerService;

    #@1b6
    invoke-static {v1}, Lcom/android/server/NotificationManagerService;->access$2100(Lcom/android/server/NotificationManagerService;)V

    #@1b9
    goto/16 :goto_71

    #@1bb
    .line 687
    :cond_1bb
    const-string v1, "android.intent.action.BOOT_COMPLETED"

    #@1bd
    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1c0
    move-result v1

    #@1c1
    if-eqz v1, :cond_71

    #@1c3
    .line 688
    move-object/from16 v0, p0

    #@1c5
    iget-object v1, v0, Lcom/android/server/NotificationManagerService$2;->this$0:Lcom/android/server/NotificationManagerService;

    #@1c7
    const/4 v3, 0x1

    #@1c8
    invoke-static {v1, v3}, Lcom/android/server/NotificationManagerService;->access$2202(Lcom/android/server/NotificationManagerService;Z)Z

    #@1cb
    .line 689
    const-string v1, "NotificationService"

    #@1cd
    const-string v3, "Boot Completed! Start sending notification"

    #@1cf
    invoke-static {v1, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1d2
    goto/16 :goto_71
.end method
