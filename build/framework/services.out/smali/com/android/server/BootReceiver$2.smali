.class Lcom/android/server/BootReceiver$2;
.super Landroid/os/Handler;
.source "BootReceiver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/BootReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/BootReceiver;


# direct methods
.method constructor <init>(Lcom/android/server/BootReceiver;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 113
    iput-object p1, p0, Lcom/android/server/BootReceiver$2;->this$0:Lcom/android/server/BootReceiver;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 7
    .parameter "msg"

    #@0
    .prologue
    .line 116
    iget v3, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v3, :pswitch_data_32

    #@5
    .line 140
    :goto_5
    return-void

    #@6
    .line 120
    :pswitch_6
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@8
    check-cast v2, Ljava/util/HashMap;

    #@a
    .line 121
    .local v2, data:Ljava/util/HashMap;
    const-string v3, "crashinfo"

    #@c
    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Ljava/lang/String;

    #@12
    .line 122
    .local v0, crashinfo:Ljava/lang/String;
    new-instance v1, Lcom/android/server/BlueProcessCrashDialog;

    #@14
    iget-object v3, p0, Lcom/android/server/BootReceiver$2;->this$0:Lcom/android/server/BootReceiver;

    #@16
    iget-object v3, v3, Lcom/android/server/BootReceiver;->mContext:Landroid/content/Context;

    #@18
    const/16 v4, 0x5c

    #@1a
    invoke-direct {v1, v3, v0, v4}, Lcom/android/server/BlueProcessCrashDialog;-><init>(Landroid/content/Context;Ljava/lang/String;I)V

    #@1d
    .line 123
    .local v1, d:Landroid/app/Dialog;
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    #@20
    goto :goto_5

    #@21
    .line 131
    .end local v0           #crashinfo:Ljava/lang/String;
    .end local v1           #d:Landroid/app/Dialog;
    .end local v2           #data:Ljava/util/HashMap;
    :pswitch_21
    const-string v0, "Android System Server is crashed"

    #@23
    .line 132
    .restart local v0       #crashinfo:Ljava/lang/String;
    new-instance v1, Lcom/android/server/BlueProcessCrashDialog;

    #@25
    iget-object v3, p0, Lcom/android/server/BootReceiver$2;->this$0:Lcom/android/server/BootReceiver;

    #@27
    iget-object v3, v3, Lcom/android/server/BootReceiver;->mContext:Landroid/content/Context;

    #@29
    const/16 v4, 0x5d

    #@2b
    invoke-direct {v1, v3, v0, v4}, Lcom/android/server/BlueProcessCrashDialog;-><init>(Landroid/content/Context;Ljava/lang/String;I)V

    #@2e
    .line 133
    .restart local v1       #d:Landroid/app/Dialog;
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    #@31
    goto :goto_5

    #@32
    .line 116
    :pswitch_data_32
    .packed-switch 0x5c
        :pswitch_6
        :pswitch_21
    .end packed-switch
.end method
