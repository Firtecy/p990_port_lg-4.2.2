.class Lcom/android/server/MountService$5;
.super Ljava/lang/Thread;
.source "MountService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/MountService;->notifyShareAvailabilityChange(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/MountService;

.field final synthetic val$path:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/android/server/MountService;Ljava/lang/String;)V
    .registers 3
    .parameter
    .parameter

    #@0
    .prologue
    .line 1145
    iput-object p1, p0, Lcom/android/server/MountService$5;->this$0:Lcom/android/server/MountService;

    #@2
    iput-object p2, p0, Lcom/android/server/MountService$5;->val$path:Ljava/lang/String;

    #@4
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    #@7
    return-void
.end method


# virtual methods
.method public run()V
    .registers 8

    #@0
    .prologue
    .line 1150
    :try_start_0
    const-string v2, "MountService"

    #@2
    const-string v3, "Disabling UMS after cable disconnect"

    #@4
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1151
    iget-object v2, p0, Lcom/android/server/MountService$5;->this$0:Lcom/android/server/MountService;

    #@9
    iget-object v3, p0, Lcom/android/server/MountService$5;->val$path:Ljava/lang/String;

    #@b
    const-string v4, "ums"

    #@d
    const/4 v5, 0x0

    #@e
    invoke-static {v2, v3, v4, v5}, Lcom/android/server/MountService;->access$200(Lcom/android/server/MountService;Ljava/lang/String;Ljava/lang/String;Z)V

    #@11
    .line 1152
    iget-object v2, p0, Lcom/android/server/MountService$5;->this$0:Lcom/android/server/MountService;

    #@13
    iget-object v3, p0, Lcom/android/server/MountService$5;->val$path:Ljava/lang/String;

    #@15
    invoke-static {v2, v3}, Lcom/android/server/MountService;->access$1800(Lcom/android/server/MountService;Ljava/lang/String;)I

    #@18
    move-result v1

    #@19
    .local v1, rc:I
    if-eqz v1, :cond_35

    #@1b
    .line 1153
    const-string v2, "MountService"

    #@1d
    const-string v3, "Failed to remount {%s} on UMS enabled-disconnect (%d)"

    #@1f
    const/4 v4, 0x2

    #@20
    new-array v4, v4, [Ljava/lang/Object;

    #@22
    const/4 v5, 0x0

    #@23
    iget-object v6, p0, Lcom/android/server/MountService$5;->val$path:Ljava/lang/String;

    #@25
    aput-object v6, v4, v5

    #@27
    const/4 v5, 0x1

    #@28
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2b
    move-result-object v6

    #@2c
    aput-object v6, v4, v5

    #@2e
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@31
    move-result-object v3

    #@32
    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_35
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_35} :catch_36

    #@35
    .line 1160
    .end local v1           #rc:I
    :cond_35
    :goto_35
    return-void

    #@36
    .line 1157
    :catch_36
    move-exception v0

    #@37
    .line 1158
    .local v0, ex:Ljava/lang/Exception;
    const-string v2, "MountService"

    #@39
    const-string v3, "Failed to mount media on UMS enabled-disconnect"

    #@3b
    invoke-static {v2, v3, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@3e
    goto :goto_35
.end method
