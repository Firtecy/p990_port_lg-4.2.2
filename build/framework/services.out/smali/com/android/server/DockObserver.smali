.class final Lcom/android/server/DockObserver;
.super Landroid/os/UEventObserver;
.source "DockObserver.java"


# static fields
.field private static final DOCK_STATE_PATH:Ljava/lang/String; = "/sys/class/switch/dock/state"

.field private static final DOCK_UEVENT_MATCH:Ljava/lang/String; = "DEVPATH=/devices/virtual/switch/dock"

.field private static final MSG_DOCK_STATE_CHANGED:I

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mDockState:I

.field private final mHandler:Landroid/os/Handler;

.field private final mLock:Ljava/lang/Object;

.field private final mPowerManager:Landroid/os/PowerManager;

.field private mPreviousDockState:I

.field private mSystemReady:Z

.field private final mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 49
    const-class v0, Lcom/android/server/DockObserver;

    #@2
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/android/server/DockObserver;->TAG:Ljava/lang/String;

    #@8
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 5
    .parameter "context"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 67
    invoke-direct {p0}, Landroid/os/UEventObserver;-><init>()V

    #@5
    .line 56
    new-instance v0, Ljava/lang/Object;

    #@7
    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    #@a
    iput-object v0, p0, Lcom/android/server/DockObserver;->mLock:Ljava/lang/Object;

    #@c
    .line 58
    iput v1, p0, Lcom/android/server/DockObserver;->mDockState:I

    #@e
    .line 59
    iput v1, p0, Lcom/android/server/DockObserver;->mPreviousDockState:I

    #@10
    .line 220
    new-instance v0, Lcom/android/server/DockObserver$1;

    #@12
    invoke-direct {v0, p0, v2}, Lcom/android/server/DockObserver$1;-><init>(Lcom/android/server/DockObserver;Z)V

    #@15
    iput-object v0, p0, Lcom/android/server/DockObserver;->mHandler:Landroid/os/Handler;

    #@17
    .line 68
    iput-object p1, p0, Lcom/android/server/DockObserver;->mContext:Landroid/content/Context;

    #@19
    .line 70
    iget-object v0, p0, Lcom/android/server/DockObserver;->mContext:Landroid/content/Context;

    #@1b
    const-string v1, "power"

    #@1d
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@20
    move-result-object v0

    #@21
    check-cast v0, Landroid/os/PowerManager;

    #@23
    iput-object v0, p0, Lcom/android/server/DockObserver;->mPowerManager:Landroid/os/PowerManager;

    #@25
    .line 71
    iget-object v0, p0, Lcom/android/server/DockObserver;->mPowerManager:Landroid/os/PowerManager;

    #@27
    sget-object v1, Lcom/android/server/DockObserver;->TAG:Ljava/lang/String;

    #@29
    invoke-virtual {v0, v2, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@2c
    move-result-object v0

    #@2d
    iput-object v0, p0, Lcom/android/server/DockObserver;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2f
    .line 73
    invoke-direct {p0}, Lcom/android/server/DockObserver;->init()V

    #@32
    .line 74
    const-string v0, "DEVPATH=/devices/virtual/switch/dock"

    #@34
    invoke-virtual {p0, v0}, Lcom/android/server/DockObserver;->startObserving(Ljava/lang/String;)V

    #@37
    .line 75
    return-void
.end method

.method static synthetic access$000(Lcom/android/server/DockObserver;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 48
    invoke-direct {p0}, Lcom/android/server/DockObserver;->handleDockStateChange()V

    #@3
    return-void
.end method

.method private handleDockStateChange()V
    .registers 15

    #@0
    .prologue
    const/4 v13, 0x4

    #@1
    const/4 v12, 0x3

    #@2
    const/4 v11, 0x2

    #@3
    const/4 v10, 0x1

    #@4
    .line 138
    iget-object v7, p0, Lcom/android/server/DockObserver;->mLock:Ljava/lang/Object;

    #@6
    monitor-enter v7

    #@7
    .line 139
    :try_start_7
    sget-object v6, Lcom/android/server/DockObserver;->TAG:Ljava/lang/String;

    #@9
    new-instance v8, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v9, "Dock state changed: "

    #@10
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v8

    #@14
    iget v9, p0, Lcom/android/server/DockObserver;->mDockState:I

    #@16
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19
    move-result-object v8

    #@1a
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v8

    #@1e
    invoke-static {v6, v8}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    .line 142
    iget-object v6, p0, Lcom/android/server/DockObserver;->mContext:Landroid/content/Context;

    #@23
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@26
    move-result-object v0

    #@27
    .line 143
    .local v0, cr:Landroid/content/ContentResolver;
    const-string v6, "device_provisioned"

    #@29
    const/4 v8, 0x0

    #@2a
    invoke-static {v0, v6, v8}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@2d
    move-result v6

    #@2e
    if-nez v6, :cond_3e

    #@30
    .line 145
    sget-object v6, Lcom/android/server/DockObserver;->TAG:Ljava/lang/String;

    #@32
    const-string v8, "Device not provisioned, skipping dock broadcast"

    #@34
    invoke-static {v6, v8}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    .line 146
    iget-object v6, p0, Lcom/android/server/DockObserver;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@39
    invoke-virtual {v6}, Landroid/os/PowerManager$WakeLock;->release()V

    #@3c
    .line 147
    monitor-exit v7

    #@3d
    .line 218
    :goto_3d
    return-void

    #@3e
    .line 151
    :cond_3e
    const-string v6, "ro.factorytest"

    #@40
    invoke-static {v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@43
    move-result-object v6

    #@44
    const-string v8, "2"

    #@46
    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@49
    move-result v6

    #@4a
    if-eqz v6, :cond_5d

    #@4c
    .line 152
    sget-object v6, Lcom/android/server/DockObserver;->TAG:Ljava/lang/String;

    #@4e
    const-string v8, "FTM MODE, skipping dock broadcast"

    #@50
    invoke-static {v6, v8}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@53
    .line 153
    iget-object v6, p0, Lcom/android/server/DockObserver;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@55
    invoke-virtual {v6}, Landroid/os/PowerManager$WakeLock;->release()V

    #@58
    .line 154
    monitor-exit v7

    #@59
    goto :goto_3d

    #@5a
    .line 217
    .end local v0           #cr:Landroid/content/ContentResolver;
    :catchall_5a
    move-exception v6

    #@5b
    monitor-exit v7
    :try_end_5c
    .catchall {:try_start_7 .. :try_end_5c} :catchall_5a

    #@5c
    throw v6

    #@5d
    .line 159
    .restart local v0       #cr:Landroid/content/ContentResolver;
    :cond_5d
    :try_start_5d
    iget v6, p0, Lcom/android/server/DockObserver;->mDockState:I

    #@5f
    if-ne v6, v11, :cond_78

    #@61
    const-string v6, "car_home_auto_launch"

    #@63
    const/4 v8, 0x1

    #@64
    invoke-static {v0, v6, v8}, Lcom/lge/provider/SettingsEx$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@67
    move-result v6

    #@68
    if-nez v6, :cond_78

    #@6a
    .line 161
    sget-object v6, Lcom/android/server/DockObserver;->TAG:Ljava/lang/String;

    #@6c
    const-string v8, "Car Dock is disabled, skpping.."

    #@6e
    invoke-static {v6, v8}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@71
    .line 162
    iget-object v6, p0, Lcom/android/server/DockObserver;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@73
    invoke-virtual {v6}, Landroid/os/PowerManager$WakeLock;->release()V

    #@76
    .line 163
    monitor-exit v7

    #@77
    goto :goto_3d

    #@78
    .line 168
    :cond_78
    new-instance v1, Landroid/content/Intent;

    #@7a
    const-string v6, "android.intent.action.DOCK_EVENT"

    #@7c
    invoke-direct {v1, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7f
    .line 169
    .local v1, intent:Landroid/content/Intent;
    const/high16 v6, 0x2000

    #@81
    invoke-virtual {v1, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@84
    .line 170
    const-string v6, "android.intent.extra.DOCK_STATE"

    #@86
    iget v8, p0, Lcom/android/server/DockObserver;->mDockState:I

    #@88
    invoke-virtual {v1, v6, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@8b
    .line 174
    const-string v6, "dock_sounds_enabled"

    #@8d
    const/4 v8, 0x1

    #@8e
    invoke-static {v0, v6, v8}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@91
    move-result v6

    #@92
    if-ne v6, v10, :cond_d7

    #@94
    .line 176
    const/4 v5, 0x0

    #@95
    .line 177
    .local v5, whichSound:Ljava/lang/String;
    iget v6, p0, Lcom/android/server/DockObserver;->mDockState:I

    #@97
    if-nez v6, :cond_ed

    #@99
    .line 178
    iget v6, p0, Lcom/android/server/DockObserver;->mPreviousDockState:I

    #@9b
    if-eq v6, v10, :cond_a5

    #@9d
    iget v6, p0, Lcom/android/server/DockObserver;->mPreviousDockState:I

    #@9f
    if-eq v6, v12, :cond_a5

    #@a1
    iget v6, p0, Lcom/android/server/DockObserver;->mPreviousDockState:I

    #@a3
    if-ne v6, v13, :cond_e6

    #@a5
    .line 181
    :cond_a5
    const-string v5, "desk_undock_sound"

    #@a7
    .line 195
    :cond_a7
    :goto_a7
    if-eqz v5, :cond_d7

    #@a9
    .line 196
    invoke-static {v0, v5}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@ac
    move-result-object v3

    #@ad
    .line 197
    .local v3, soundPath:Ljava/lang/String;
    if-eqz v3, :cond_d7

    #@af
    .line 198
    new-instance v6, Ljava/lang/StringBuilder;

    #@b1
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@b4
    const-string v8, "file://"

    #@b6
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b9
    move-result-object v6

    #@ba
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v6

    #@be
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c1
    move-result-object v6

    #@c2
    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@c5
    move-result-object v4

    #@c6
    .line 199
    .local v4, soundUri:Landroid/net/Uri;
    if-eqz v4, :cond_d7

    #@c8
    .line 200
    iget-object v6, p0, Lcom/android/server/DockObserver;->mContext:Landroid/content/Context;

    #@ca
    invoke-static {v6, v4}, Landroid/media/RingtoneManager;->getRingtone(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/Ringtone;

    #@cd
    move-result-object v2

    #@ce
    .line 201
    .local v2, sfx:Landroid/media/Ringtone;
    if-eqz v2, :cond_d7

    #@d0
    .line 202
    const/4 v6, 0x1

    #@d1
    invoke-virtual {v2, v6}, Landroid/media/Ringtone;->setStreamType(I)V

    #@d4
    .line 203
    invoke-virtual {v2}, Landroid/media/Ringtone;->play()V

    #@d7
    .line 213
    .end local v2           #sfx:Landroid/media/Ringtone;
    .end local v3           #soundPath:Ljava/lang/String;
    .end local v4           #soundUri:Landroid/net/Uri;
    .end local v5           #whichSound:Ljava/lang/String;
    :cond_d7
    iget-object v6, p0, Lcom/android/server/DockObserver;->mContext:Landroid/content/Context;

    #@d9
    sget-object v8, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@db
    invoke-virtual {v6, v1, v8}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@de
    .line 216
    iget-object v6, p0, Lcom/android/server/DockObserver;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@e0
    invoke-virtual {v6}, Landroid/os/PowerManager$WakeLock;->release()V

    #@e3
    .line 217
    monitor-exit v7

    #@e4
    goto/16 :goto_3d

    #@e6
    .line 182
    .restart local v5       #whichSound:Ljava/lang/String;
    :cond_e6
    iget v6, p0, Lcom/android/server/DockObserver;->mPreviousDockState:I

    #@e8
    if-ne v6, v11, :cond_a7

    #@ea
    .line 183
    const-string v5, "car_undock_sound"

    #@ec
    goto :goto_a7

    #@ed
    .line 186
    :cond_ed
    iget v6, p0, Lcom/android/server/DockObserver;->mDockState:I

    #@ef
    if-eq v6, v10, :cond_f9

    #@f1
    iget v6, p0, Lcom/android/server/DockObserver;->mDockState:I

    #@f3
    if-eq v6, v12, :cond_f9

    #@f5
    iget v6, p0, Lcom/android/server/DockObserver;->mDockState:I

    #@f7
    if-ne v6, v13, :cond_fc

    #@f9
    .line 189
    :cond_f9
    const-string v5, "desk_dock_sound"

    #@fb
    goto :goto_a7

    #@fc
    .line 190
    :cond_fc
    iget v6, p0, Lcom/android/server/DockObserver;->mDockState:I

    #@fe
    if-ne v6, v11, :cond_a7

    #@100
    .line 191
    const-string v5, "car_dock_sound"
    :try_end_102
    .catchall {:try_start_5d .. :try_end_102} :catchall_5a

    #@102
    goto :goto_a7
.end method

.method private init()V
    .registers 8

    #@0
    .prologue
    .line 103
    iget-object v5, p0, Lcom/android/server/DockObserver;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v5

    #@3
    .line 105
    const/16 v4, 0x400

    #@5
    :try_start_5
    new-array v0, v4, [C

    #@7
    .line 106
    .local v0, buffer:[C
    new-instance v2, Ljava/io/FileReader;

    #@9
    const-string v4, "/sys/class/switch/dock/state"

    #@b
    invoke-direct {v2, v4}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_e
    .catchall {:try_start_5 .. :try_end_e} :catchall_40
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_e} :catch_37
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_e} :catch_43

    #@e
    .line 108
    .local v2, file:Ljava/io/FileReader;
    const/4 v4, 0x0

    #@f
    const/16 v6, 0x400

    #@11
    :try_start_11
    invoke-virtual {v2, v0, v4, v6}, Ljava/io/FileReader;->read([CII)I

    #@14
    move-result v3

    #@15
    .line 109
    .local v3, len:I
    new-instance v4, Ljava/lang/String;

    #@17
    const/4 v6, 0x0

    #@18
    invoke-direct {v4, v0, v6, v3}, Ljava/lang/String;-><init>([CII)V

    #@1b
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@1e
    move-result-object v4

    #@1f
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    #@22
    move-result-object v4

    #@23
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    #@26
    move-result v4

    #@27
    iput v4, p0, Lcom/android/server/DockObserver;->mDockState:I

    #@29
    .line 110
    iget v4, p0, Lcom/android/server/DockObserver;->mDockState:I

    #@2b
    iput v4, p0, Lcom/android/server/DockObserver;->mPreviousDockState:I
    :try_end_2d
    .catchall {:try_start_11 .. :try_end_2d} :catchall_32

    #@2d
    .line 112
    :try_start_2d
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_30
    .catchall {:try_start_2d .. :try_end_30} :catchall_40
    .catch Ljava/io/FileNotFoundException; {:try_start_2d .. :try_end_30} :catch_37
    .catch Ljava/lang/Exception; {:try_start_2d .. :try_end_30} :catch_43

    #@30
    .line 119
    .end local v0           #buffer:[C
    .end local v2           #file:Ljava/io/FileReader;
    .end local v3           #len:I
    :goto_30
    :try_start_30
    monitor-exit v5
    :try_end_31
    .catchall {:try_start_30 .. :try_end_31} :catchall_40

    #@31
    .line 120
    return-void

    #@32
    .line 112
    .restart local v0       #buffer:[C
    .restart local v2       #file:Ljava/io/FileReader;
    :catchall_32
    move-exception v4

    #@33
    :try_start_33
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V

    #@36
    throw v4
    :try_end_37
    .catchall {:try_start_33 .. :try_end_37} :catchall_40
    .catch Ljava/io/FileNotFoundException; {:try_start_33 .. :try_end_37} :catch_37
    .catch Ljava/lang/Exception; {:try_start_33 .. :try_end_37} :catch_43

    #@37
    .line 114
    .end local v0           #buffer:[C
    .end local v2           #file:Ljava/io/FileReader;
    :catch_37
    move-exception v1

    #@38
    .line 115
    .local v1, e:Ljava/io/FileNotFoundException;
    :try_start_38
    sget-object v4, Lcom/android/server/DockObserver;->TAG:Ljava/lang/String;

    #@3a
    const-string v6, "This kernel does not have dock station support"

    #@3c
    invoke-static {v4, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    goto :goto_30

    #@40
    .line 119
    .end local v1           #e:Ljava/io/FileNotFoundException;
    :catchall_40
    move-exception v4

    #@41
    monitor-exit v5
    :try_end_42
    .catchall {:try_start_38 .. :try_end_42} :catchall_40

    #@42
    throw v4

    #@43
    .line 116
    :catch_43
    move-exception v1

    #@44
    .line 117
    .local v1, e:Ljava/lang/Exception;
    :try_start_44
    sget-object v4, Lcom/android/server/DockObserver;->TAG:Ljava/lang/String;

    #@46
    const-string v6, ""

    #@48
    invoke-static {v4, v6, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4b
    .catchall {:try_start_44 .. :try_end_4b} :catchall_40

    #@4b
    goto :goto_30
.end method

.method private updateLocked()V
    .registers 3

    #@0
    .prologue
    .line 133
    iget-object v0, p0, Lcom/android/server/DockObserver;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@5
    .line 134
    iget-object v0, p0, Lcom/android/server/DockObserver;->mHandler:Landroid/os/Handler;

    #@7
    const/4 v1, 0x0

    #@8
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@b
    .line 135
    return-void
.end method


# virtual methods
.method public onUEvent(Landroid/os/UEventObserver$UEvent;)V
    .registers 8
    .parameter "event"

    #@0
    .prologue
    .line 79
    sget-object v2, Lcom/android/server/DockObserver;->TAG:Ljava/lang/String;

    #@2
    const/4 v3, 0x2

    #@3
    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@6
    move-result v2

    #@7
    if-eqz v2, :cond_25

    #@9
    .line 80
    sget-object v2, Lcom/android/server/DockObserver;->TAG:Ljava/lang/String;

    #@b
    new-instance v3, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v4, "Dock UEVENT: "

    #@12
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v3

    #@16
    invoke-virtual {p1}, Landroid/os/UEventObserver$UEvent;->toString()Ljava/lang/String;

    #@19
    move-result-object v4

    #@1a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v3

    #@22
    invoke-static {v2, v3}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 83
    :cond_25
    iget-object v3, p0, Lcom/android/server/DockObserver;->mLock:Ljava/lang/Object;

    #@27
    monitor-enter v3

    #@28
    .line 85
    :try_start_28
    const-string v2, "SWITCH_STATE"

    #@2a
    invoke-virtual {p1, v2}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    #@2d
    move-result-object v2

    #@2e
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@31
    move-result v1

    #@32
    .line 86
    .local v1, newState:I
    iget v2, p0, Lcom/android/server/DockObserver;->mDockState:I

    #@34
    if-eq v1, v2, :cond_4c

    #@36
    .line 87
    iget v2, p0, Lcom/android/server/DockObserver;->mDockState:I

    #@38
    iput v2, p0, Lcom/android/server/DockObserver;->mPreviousDockState:I

    #@3a
    .line 88
    iput v1, p0, Lcom/android/server/DockObserver;->mDockState:I

    #@3c
    .line 89
    iget-boolean v2, p0, Lcom/android/server/DockObserver;->mSystemReady:Z

    #@3e
    if-eqz v2, :cond_4c

    #@40
    .line 91
    iget-object v2, p0, Lcom/android/server/DockObserver;->mPowerManager:Landroid/os/PowerManager;

    #@42
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@45
    move-result-wide v4

    #@46
    invoke-virtual {v2, v4, v5}, Landroid/os/PowerManager;->wakeUp(J)V

    #@49
    .line 93
    invoke-direct {p0}, Lcom/android/server/DockObserver;->updateLocked()V
    :try_end_4c
    .catchall {:try_start_28 .. :try_end_4c} :catchall_68
    .catch Ljava/lang/NumberFormatException; {:try_start_28 .. :try_end_4c} :catch_4e

    #@4c
    .line 99
    .end local v1           #newState:I
    :cond_4c
    :goto_4c
    :try_start_4c
    monitor-exit v3

    #@4d
    .line 100
    return-void

    #@4e
    .line 96
    :catch_4e
    move-exception v0

    #@4f
    .line 97
    .local v0, e:Ljava/lang/NumberFormatException;
    sget-object v2, Lcom/android/server/DockObserver;->TAG:Ljava/lang/String;

    #@51
    new-instance v4, Ljava/lang/StringBuilder;

    #@53
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@56
    const-string v5, "Could not parse switch state from event "

    #@58
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v4

    #@5c
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v4

    #@60
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@63
    move-result-object v4

    #@64
    invoke-static {v2, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@67
    goto :goto_4c

    #@68
    .line 99
    .end local v0           #e:Ljava/lang/NumberFormatException;
    :catchall_68
    move-exception v2

    #@69
    monitor-exit v3
    :try_end_6a
    .catchall {:try_start_4c .. :try_end_6a} :catchall_68

    #@6a
    throw v2
.end method

.method systemReady()V
    .registers 3

    #@0
    .prologue
    .line 123
    iget-object v1, p0, Lcom/android/server/DockObserver;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 125
    :try_start_3
    iget v0, p0, Lcom/android/server/DockObserver;->mDockState:I

    #@5
    if-eqz v0, :cond_a

    #@7
    .line 126
    invoke-direct {p0}, Lcom/android/server/DockObserver;->updateLocked()V

    #@a
    .line 128
    :cond_a
    const/4 v0, 0x1

    #@b
    iput-boolean v0, p0, Lcom/android/server/DockObserver;->mSystemReady:Z

    #@d
    .line 129
    monitor-exit v1

    #@e
    .line 130
    return-void

    #@f
    .line 129
    :catchall_f
    move-exception v0

    #@10
    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    #@11
    throw v0
.end method
