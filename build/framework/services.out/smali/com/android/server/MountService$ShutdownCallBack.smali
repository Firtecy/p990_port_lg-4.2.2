.class Lcom/android/server/MountService$ShutdownCallBack;
.super Lcom/android/server/MountService$UnmountCallBack;
.source "MountService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/MountService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ShutdownCallBack"
.end annotation


# instance fields
.field observer:Landroid/os/storage/IMountShutdownObserver;

.field final synthetic this$0:Lcom/android/server/MountService;


# direct methods
.method constructor <init>(Lcom/android/server/MountService;Ljava/lang/String;Landroid/os/storage/IMountShutdownObserver;)V
    .registers 6
    .parameter
    .parameter "path"
    .parameter "observer"

    #@0
    .prologue
    .line 377
    iput-object p1, p0, Lcom/android/server/MountService$ShutdownCallBack;->this$0:Lcom/android/server/MountService;

    #@2
    .line 378
    const/4 v0, 0x1

    #@3
    const/4 v1, 0x0

    #@4
    invoke-direct {p0, p1, p2, v0, v1}, Lcom/android/server/MountService$UnmountCallBack;-><init>(Lcom/android/server/MountService;Ljava/lang/String;ZZ)V

    #@7
    .line 379
    iput-object p3, p0, Lcom/android/server/MountService$ShutdownCallBack;->observer:Landroid/os/storage/IMountShutdownObserver;

    #@9
    .line 380
    return-void
.end method


# virtual methods
.method handleFinished()V
    .registers 7

    #@0
    .prologue
    .line 384
    iget-object v2, p0, Lcom/android/server/MountService$ShutdownCallBack;->this$0:Lcom/android/server/MountService;

    #@2
    iget-object v3, p0, Lcom/android/server/MountService$UnmountCallBack;->path:Ljava/lang/String;

    #@4
    const/4 v4, 0x1

    #@5
    iget-boolean v5, p0, Lcom/android/server/MountService$UnmountCallBack;->removeEncryption:Z

    #@7
    invoke-static {v2, v3, v4, v5}, Lcom/android/server/MountService;->access$100(Lcom/android/server/MountService;Ljava/lang/String;ZZ)I

    #@a
    move-result v1

    #@b
    .line 385
    .local v1, ret:I
    iget-object v2, p0, Lcom/android/server/MountService$ShutdownCallBack;->observer:Landroid/os/storage/IMountShutdownObserver;

    #@d
    if-eqz v2, :cond_14

    #@f
    .line 387
    :try_start_f
    iget-object v2, p0, Lcom/android/server/MountService$ShutdownCallBack;->observer:Landroid/os/storage/IMountShutdownObserver;

    #@11
    invoke-interface {v2, v1}, Landroid/os/storage/IMountShutdownObserver;->onShutDownComplete(I)V
    :try_end_14
    .catch Landroid/os/RemoteException; {:try_start_f .. :try_end_14} :catch_15

    #@14
    .line 392
    :cond_14
    :goto_14
    return-void

    #@15
    .line 388
    :catch_15
    move-exception v0

    #@16
    .line 389
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "MountService"

    #@18
    const-string v3, "RemoteException when shutting down"

    #@1a
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    goto :goto_14
.end method
