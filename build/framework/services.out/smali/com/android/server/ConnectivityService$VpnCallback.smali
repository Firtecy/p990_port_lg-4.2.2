.class public Lcom/android/server/ConnectivityService$VpnCallback;
.super Ljava/lang/Object;
.source "ConnectivityService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/ConnectivityService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "VpnCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/ConnectivityService;


# direct methods
.method protected constructor <init>(Lcom/android/server/ConnectivityService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 5006
    iput-object p1, p0, Lcom/android/server/ConnectivityService$VpnCallback;->this$0:Lcom/android/server/ConnectivityService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 5007
    return-void
.end method


# virtual methods
.method public onStateChanged(Landroid/net/NetworkInfo;)V
    .registers 4
    .parameter "info"

    #@0
    .prologue
    .line 5010
    iget-object v0, p0, Lcom/android/server/ConnectivityService$VpnCallback;->this$0:Lcom/android/server/ConnectivityService;

    #@2
    invoke-static {v0}, Lcom/android/server/ConnectivityService;->access$3100(Lcom/android/server/ConnectivityService;)Lcom/android/server/ConnectivityService$InternalHandler;

    #@5
    move-result-object v0

    #@6
    const/16 v1, 0xe

    #@8
    invoke-virtual {v0, v1, p1}, Lcom/android/server/ConnectivityService$InternalHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@b
    move-result-object v0

    #@c
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@f
    .line 5011
    return-void
.end method

.method public override(Ljava/util/List;Ljava/util/List;)V
    .registers 14
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 5014
    .local p1, dnsServers:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .local p2, searchDomains:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    if-nez p1, :cond_6

    #@2
    .line 5015
    invoke-virtual {p0}, Lcom/android/server/ConnectivityService$VpnCallback;->restore()V

    #@5
    .line 5070
    :goto_5
    return-void

    #@6
    .line 5020
    :cond_6
    new-instance v1, Ljava/util/ArrayList;

    #@8
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@b
    .line 5021
    .local v1, addresses:Ljava/util/List;,"Ljava/util/List<Ljava/net/InetAddress;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@e
    move-result-object v6

    #@f
    .local v6, i$:Ljava/util/Iterator;
    :goto_f
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    #@12
    move-result v7

    #@13
    if-eqz v7, :cond_25

    #@15
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@18
    move-result-object v0

    #@19
    check-cast v0, Ljava/lang/String;

    #@1b
    .line 5024
    .local v0, address:Ljava/lang/String;
    :try_start_1b
    invoke-static {v0}, Ljava/net/InetAddress;->parseNumericAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@1e
    move-result-object v7

    #@1f
    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_22
    .catch Ljava/lang/Exception; {:try_start_1b .. :try_end_22} :catch_23

    #@22
    goto :goto_f

    #@23
    .line 5025
    :catch_23
    move-exception v7

    #@24
    goto :goto_f

    #@25
    .line 5029
    .end local v0           #address:Ljava/lang/String;
    :cond_25
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    #@28
    move-result v7

    #@29
    if-eqz v7, :cond_2f

    #@2b
    .line 5030
    invoke-virtual {p0}, Lcom/android/server/ConnectivityService$VpnCallback;->restore()V

    #@2e
    goto :goto_5

    #@2f
    .line 5035
    :cond_2f
    new-instance v2, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    .line 5036
    .local v2, buffer:Ljava/lang/StringBuilder;
    if-eqz p2, :cond_50

    #@36
    .line 5037
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@39
    move-result-object v6

    #@3a
    :goto_3a
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    #@3d
    move-result v7

    #@3e
    if-eqz v7, :cond_50

    #@40
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@43
    move-result-object v4

    #@44
    check-cast v4, Ljava/lang/String;

    #@46
    .line 5038
    .local v4, domain:Ljava/lang/String;
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v7

    #@4a
    const/16 v8, 0x20

    #@4c
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@4f
    goto :goto_3a

    #@50
    .line 5041
    .end local v4           #domain:Ljava/lang/String;
    :cond_50
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@53
    move-result-object v7

    #@54
    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@57
    move-result-object v5

    #@58
    .line 5044
    .local v5, domains:Ljava/lang/String;
    const/4 v3, 0x0

    #@59
    .line 5045
    .local v3, changed:Z
    iget-object v7, p0, Lcom/android/server/ConnectivityService$VpnCallback;->this$0:Lcom/android/server/ConnectivityService;

    #@5b
    invoke-static {v7}, Lcom/android/server/ConnectivityService;->access$3200(Lcom/android/server/ConnectivityService;)Ljava/lang/Object;

    #@5e
    move-result-object v8

    #@5f
    monitor-enter v8

    #@60
    .line 5046
    :try_start_60
    iget-object v7, p0, Lcom/android/server/ConnectivityService$VpnCallback;->this$0:Lcom/android/server/ConnectivityService;

    #@62
    const-string v9, "VPN"

    #@64
    const-string v10, "VPN"

    #@66
    invoke-static {v7, v9, v10, v1, v5}, Lcom/android/server/ConnectivityService;->access$3300(Lcom/android/server/ConnectivityService;Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;Ljava/lang/String;)Z

    #@69
    move-result v3

    #@6a
    .line 5047
    iget-object v7, p0, Lcom/android/server/ConnectivityService$VpnCallback;->this$0:Lcom/android/server/ConnectivityService;

    #@6c
    const/4 v9, 0x1

    #@6d
    invoke-static {v7, v9}, Lcom/android/server/ConnectivityService;->access$3402(Lcom/android/server/ConnectivityService;Z)Z

    #@70
    .line 5048
    monitor-exit v8
    :try_end_71
    .catchall {:try_start_60 .. :try_end_71} :catchall_86

    #@71
    .line 5049
    if-eqz v3, :cond_78

    #@73
    .line 5050
    iget-object v7, p0, Lcom/android/server/ConnectivityService$VpnCallback;->this$0:Lcom/android/server/ConnectivityService;

    #@75
    invoke-static {v7}, Lcom/android/server/ConnectivityService;->access$3500(Lcom/android/server/ConnectivityService;)V

    #@78
    .line 5054
    :cond_78
    iget-object v7, p0, Lcom/android/server/ConnectivityService$VpnCallback;->this$0:Lcom/android/server/ConnectivityService;

    #@7a
    iget-object v7, v7, Lcom/android/server/ConnectivityService;->mLGfeature:Lcom/android/internal/telephony/LGfeature;

    #@7c
    iget-boolean v7, v7, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_CONNECTIVITYSERVICE_VPN_DEF_PROXY_DCM:Z

    #@7e
    if-eqz v7, :cond_89

    #@80
    .line 5055
    const-string v7, "[jungil]VpnCallback.override: skip clearing defaultproxy when vpn connected"

    #@82
    invoke-static {v7}, Lcom/android/server/ConnectivityService;->access$200(Ljava/lang/String;)V

    #@85
    goto :goto_5

    #@86
    .line 5048
    :catchall_86
    move-exception v7

    #@87
    :try_start_87
    monitor-exit v8
    :try_end_88
    .catchall {:try_start_87 .. :try_end_88} :catchall_86

    #@88
    throw v7

    #@89
    .line 5059
    :cond_89
    const-string v7, "[jungil]VpnCallback.override: clear defaultproxy "

    #@8b
    invoke-static {v7}, Lcom/android/server/ConnectivityService;->access$200(Ljava/lang/String;)V

    #@8e
    .line 5060
    iget-object v7, p0, Lcom/android/server/ConnectivityService$VpnCallback;->this$0:Lcom/android/server/ConnectivityService;

    #@90
    invoke-static {v7}, Lcom/android/server/ConnectivityService;->access$3600(Lcom/android/server/ConnectivityService;)Ljava/lang/Object;

    #@93
    move-result-object v8

    #@94
    monitor-enter v8

    #@95
    .line 5061
    :try_start_95
    iget-object v7, p0, Lcom/android/server/ConnectivityService$VpnCallback;->this$0:Lcom/android/server/ConnectivityService;

    #@97
    const/4 v9, 0x1

    #@98
    invoke-static {v7, v9}, Lcom/android/server/ConnectivityService;->access$3702(Lcom/android/server/ConnectivityService;Z)Z

    #@9b
    .line 5062
    iget-object v7, p0, Lcom/android/server/ConnectivityService$VpnCallback;->this$0:Lcom/android/server/ConnectivityService;

    #@9d
    invoke-static {v7}, Lcom/android/server/ConnectivityService;->access$3800(Lcom/android/server/ConnectivityService;)Landroid/net/ProxyProperties;

    #@a0
    move-result-object v7

    #@a1
    if-eqz v7, :cond_a9

    #@a3
    .line 5063
    iget-object v7, p0, Lcom/android/server/ConnectivityService$VpnCallback;->this$0:Lcom/android/server/ConnectivityService;

    #@a5
    const/4 v9, 0x0

    #@a6
    invoke-static {v7, v9}, Lcom/android/server/ConnectivityService;->access$3900(Lcom/android/server/ConnectivityService;Landroid/net/ProxyProperties;)V

    #@a9
    .line 5065
    :cond_a9
    monitor-exit v8

    #@aa
    goto/16 :goto_5

    #@ac
    :catchall_ac
    move-exception v7

    #@ad
    monitor-exit v8
    :try_end_ae
    .catchall {:try_start_95 .. :try_end_ae} :catchall_ac

    #@ae
    throw v7
.end method

.method public restore()V
    .registers 4

    #@0
    .prologue
    .line 5073
    iget-object v0, p0, Lcom/android/server/ConnectivityService$VpnCallback;->this$0:Lcom/android/server/ConnectivityService;

    #@2
    invoke-static {v0}, Lcom/android/server/ConnectivityService;->access$3200(Lcom/android/server/ConnectivityService;)Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    monitor-enter v1

    #@7
    .line 5074
    :try_start_7
    iget-object v0, p0, Lcom/android/server/ConnectivityService$VpnCallback;->this$0:Lcom/android/server/ConnectivityService;

    #@9
    invoke-static {v0}, Lcom/android/server/ConnectivityService;->access$3400(Lcom/android/server/ConnectivityService;)Z

    #@c
    move-result v0

    #@d
    if-eqz v0, :cond_20

    #@f
    .line 5075
    iget-object v0, p0, Lcom/android/server/ConnectivityService$VpnCallback;->this$0:Lcom/android/server/ConnectivityService;

    #@11
    const/4 v2, 0x0

    #@12
    invoke-static {v0, v2}, Lcom/android/server/ConnectivityService;->access$3402(Lcom/android/server/ConnectivityService;Z)Z

    #@15
    .line 5076
    iget-object v0, p0, Lcom/android/server/ConnectivityService$VpnCallback;->this$0:Lcom/android/server/ConnectivityService;

    #@17
    invoke-static {v0}, Lcom/android/server/ConnectivityService;->access$3100(Lcom/android/server/ConnectivityService;)Lcom/android/server/ConnectivityService$InternalHandler;

    #@1a
    move-result-object v0

    #@1b
    const/16 v2, 0xb

    #@1d
    invoke-virtual {v0, v2}, Lcom/android/server/ConnectivityService$InternalHandler;->sendEmptyMessage(I)Z

    #@20
    .line 5078
    :cond_20
    monitor-exit v1
    :try_end_21
    .catchall {:try_start_7 .. :try_end_21} :catchall_43

    #@21
    .line 5079
    iget-object v0, p0, Lcom/android/server/ConnectivityService$VpnCallback;->this$0:Lcom/android/server/ConnectivityService;

    #@23
    invoke-static {v0}, Lcom/android/server/ConnectivityService;->access$3600(Lcom/android/server/ConnectivityService;)Ljava/lang/Object;

    #@26
    move-result-object v1

    #@27
    monitor-enter v1

    #@28
    .line 5080
    :try_start_28
    iget-object v0, p0, Lcom/android/server/ConnectivityService$VpnCallback;->this$0:Lcom/android/server/ConnectivityService;

    #@2a
    const/4 v2, 0x0

    #@2b
    invoke-static {v0, v2}, Lcom/android/server/ConnectivityService;->access$3702(Lcom/android/server/ConnectivityService;Z)Z

    #@2e
    .line 5081
    iget-object v0, p0, Lcom/android/server/ConnectivityService$VpnCallback;->this$0:Lcom/android/server/ConnectivityService;

    #@30
    invoke-static {v0}, Lcom/android/server/ConnectivityService;->access$3800(Lcom/android/server/ConnectivityService;)Landroid/net/ProxyProperties;

    #@33
    move-result-object v0

    #@34
    if-eqz v0, :cond_41

    #@36
    .line 5082
    iget-object v0, p0, Lcom/android/server/ConnectivityService$VpnCallback;->this$0:Lcom/android/server/ConnectivityService;

    #@38
    iget-object v2, p0, Lcom/android/server/ConnectivityService$VpnCallback;->this$0:Lcom/android/server/ConnectivityService;

    #@3a
    invoke-static {v2}, Lcom/android/server/ConnectivityService;->access$3800(Lcom/android/server/ConnectivityService;)Landroid/net/ProxyProperties;

    #@3d
    move-result-object v2

    #@3e
    invoke-static {v0, v2}, Lcom/android/server/ConnectivityService;->access$3900(Lcom/android/server/ConnectivityService;Landroid/net/ProxyProperties;)V

    #@41
    .line 5084
    :cond_41
    monitor-exit v1
    :try_end_42
    .catchall {:try_start_28 .. :try_end_42} :catchall_46

    #@42
    .line 5085
    return-void

    #@43
    .line 5078
    :catchall_43
    move-exception v0

    #@44
    :try_start_44
    monitor-exit v1
    :try_end_45
    .catchall {:try_start_44 .. :try_end_45} :catchall_43

    #@45
    throw v0

    #@46
    .line 5084
    :catchall_46
    move-exception v0

    #@47
    :try_start_47
    monitor-exit v1
    :try_end_48
    .catchall {:try_start_47 .. :try_end_48} :catchall_46

    #@48
    throw v0
.end method
