.class public Lcom/android/server/TextServicesManagerService;
.super Lcom/android/internal/textservice/ITextServicesManager$Stub;
.source "TextServicesManagerService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/TextServicesManagerService$TextServicesSettings;,
        Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;,
        Lcom/android/server/TextServicesManagerService$InternalServiceConnection;,
        Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;,
        Lcom/android/server/TextServicesManagerService$TextServicesMonitor;
    }
.end annotation


# static fields
.field private static final DBG:Z

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mMonitor:Lcom/android/server/TextServicesManagerService$TextServicesMonitor;

.field private final mSettings:Lcom/android/server/TextServicesManagerService$TextServicesSettings;

.field private final mSpellCheckerBindGroups:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;",
            ">;"
        }
    .end annotation
.end field

.field private final mSpellCheckerList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/textservice/SpellCheckerInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mSpellCheckerMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/view/textservice/SpellCheckerInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mSystemReady:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 66
    const-class v0, Lcom/android/server/TextServicesManagerService;

    #@2
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/android/server/TextServicesManagerService;->TAG:Ljava/lang/String;

    #@8
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 7
    .parameter "context"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 85
    invoke-direct {p0}, Lcom/android/internal/textservice/ITextServicesManager$Stub;-><init>()V

    #@4
    .line 72
    new-instance v2, Ljava/util/HashMap;

    #@6
    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    #@9
    iput-object v2, p0, Lcom/android/server/TextServicesManagerService;->mSpellCheckerMap:Ljava/util/HashMap;

    #@b
    .line 74
    new-instance v2, Ljava/util/ArrayList;

    #@d
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@10
    iput-object v2, p0, Lcom/android/server/TextServicesManagerService;->mSpellCheckerList:Ljava/util/ArrayList;

    #@12
    .line 75
    new-instance v2, Ljava/util/HashMap;

    #@14
    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    #@17
    iput-object v2, p0, Lcom/android/server/TextServicesManagerService;->mSpellCheckerBindGroups:Ljava/util/HashMap;

    #@19
    .line 86
    const/4 v2, 0x0

    #@1a
    iput-boolean v2, p0, Lcom/android/server/TextServicesManagerService;->mSystemReady:Z

    #@1c
    .line 87
    iput-object p1, p0, Lcom/android/server/TextServicesManagerService;->mContext:Landroid/content/Context;

    #@1e
    .line 88
    const/4 v1, 0x0

    #@1f
    .line 90
    .local v1, userId:I
    :try_start_1f
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@22
    move-result-object v2

    #@23
    new-instance v3, Lcom/android/server/TextServicesManagerService$1;

    #@25
    invoke-direct {v3, p0}, Lcom/android/server/TextServicesManagerService$1;-><init>(Lcom/android/server/TextServicesManagerService;)V

    #@28
    invoke-interface {v2, v3}, Landroid/app/IActivityManager;->registerUserSwitchObserver(Landroid/app/IUserSwitchObserver;)V

    #@2b
    .line 109
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@2e
    move-result-object v2

    #@2f
    invoke-interface {v2}, Landroid/app/IActivityManager;->getCurrentUser()Landroid/content/pm/UserInfo;

    #@32
    move-result-object v2

    #@33
    iget v1, v2, Landroid/content/pm/UserInfo;->id:I
    :try_end_35
    .catch Landroid/os/RemoteException; {:try_start_1f .. :try_end_35} :catch_51

    #@35
    .line 113
    :goto_35
    new-instance v2, Lcom/android/server/TextServicesManagerService$TextServicesMonitor;

    #@37
    invoke-direct {v2, p0, v4}, Lcom/android/server/TextServicesManagerService$TextServicesMonitor;-><init>(Lcom/android/server/TextServicesManagerService;Lcom/android/server/TextServicesManagerService$1;)V

    #@3a
    iput-object v2, p0, Lcom/android/server/TextServicesManagerService;->mMonitor:Lcom/android/server/TextServicesManagerService$TextServicesMonitor;

    #@3c
    .line 114
    iget-object v2, p0, Lcom/android/server/TextServicesManagerService;->mMonitor:Lcom/android/server/TextServicesManagerService$TextServicesMonitor;

    #@3e
    const/4 v3, 0x1

    #@3f
    invoke-virtual {v2, p1, v4, v3}, Lcom/android/server/TextServicesManagerService$TextServicesMonitor;->register(Landroid/content/Context;Landroid/os/Looper;Z)V

    #@42
    .line 115
    new-instance v2, Lcom/android/server/TextServicesManagerService$TextServicesSettings;

    #@44
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@47
    move-result-object v3

    #@48
    invoke-direct {v2, v3, v1}, Lcom/android/server/TextServicesManagerService$TextServicesSettings;-><init>(Landroid/content/ContentResolver;I)V

    #@4b
    iput-object v2, p0, Lcom/android/server/TextServicesManagerService;->mSettings:Lcom/android/server/TextServicesManagerService$TextServicesSettings;

    #@4d
    .line 118
    invoke-direct {p0, v1}, Lcom/android/server/TextServicesManagerService;->switchUserLocked(I)V

    #@50
    .line 119
    return-void

    #@51
    .line 110
    :catch_51
    move-exception v0

    #@52
    .line 111
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Lcom/android/server/TextServicesManagerService;->TAG:Ljava/lang/String;

    #@54
    const-string v3, "Couldn\'t get current user ID; guessing it\'s 0"

    #@56
    invoke-static {v2, v3, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@59
    goto :goto_35
.end method

.method static synthetic access$000(Lcom/android/server/TextServicesManagerService;)Ljava/util/HashMap;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 65
    iget-object v0, p0, Lcom/android/server/TextServicesManagerService;->mSpellCheckerMap:Ljava/util/HashMap;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/server/TextServicesManagerService;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/android/server/TextServicesManagerService;->switchUserLocked(I)V

    #@3
    return-void
.end method

.method static synthetic access$1300(Lcom/android/server/TextServicesManagerService;)Ljava/util/HashMap;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 65
    iget-object v0, p0, Lcom/android/server/TextServicesManagerService;->mSpellCheckerBindGroups:Ljava/util/HashMap;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/android/server/TextServicesManagerService;)Lcom/android/server/TextServicesManagerService$TextServicesSettings;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 65
    iget-object v0, p0, Lcom/android/server/TextServicesManagerService;->mSettings:Lcom/android/server/TextServicesManagerService$TextServicesSettings;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Lcom/android/server/TextServicesManagerService;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 65
    iget-object v0, p0, Lcom/android/server/TextServicesManagerService;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Lcom/android/server/TextServicesManagerService;)Ljava/util/ArrayList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 65
    iget-object v0, p0, Lcom/android/server/TextServicesManagerService;->mSpellCheckerList:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method static synthetic access$600(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/HashMap;Lcom/android/server/TextServicesManagerService$TextServicesSettings;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 65
    invoke-static {p0, p1, p2, p3}, Lcom/android/server/TextServicesManagerService;->buildSpellCheckerMapLocked(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/HashMap;Lcom/android/server/TextServicesManagerService$TextServicesSettings;)V

    #@3
    return-void
.end method

.method static synthetic access$700(Lcom/android/server/TextServicesManagerService;Ljava/lang/String;Ljava/lang/String;)Landroid/view/textservice/SpellCheckerInfo;
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Lcom/android/server/TextServicesManagerService;->findAvailSpellCheckerLocked(Ljava/lang/String;Ljava/lang/String;)Landroid/view/textservice/SpellCheckerInfo;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$800(Lcom/android/server/TextServicesManagerService;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/android/server/TextServicesManagerService;->setCurrentSpellCheckerLocked(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method private bindCurrentSpellCheckerService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
    .registers 7
    .parameter "service"
    .parameter "conn"
    .parameter "flags"

    #@0
    .prologue
    .line 247
    if-eqz p1, :cond_4

    #@2
    if-nez p2, :cond_28

    #@4
    .line 248
    :cond_4
    sget-object v0, Lcom/android/server/TextServicesManagerService;->TAG:Ljava/lang/String;

    #@6
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v2, "--- bind failed: service = "

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    const-string v2, ", conn = "

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 249
    const/4 v0, 0x0

    #@27
    .line 251
    :goto_27
    return v0

    #@28
    :cond_28
    iget-object v0, p0, Lcom/android/server/TextServicesManagerService;->mContext:Landroid/content/Context;

    #@2a
    iget-object v1, p0, Lcom/android/server/TextServicesManagerService;->mSettings:Lcom/android/server/TextServicesManagerService$TextServicesSettings;

    #@2c
    invoke-virtual {v1}, Lcom/android/server/TextServicesManagerService$TextServicesSettings;->getCurrentUserId()I

    #@2f
    move-result v1

    #@30
    invoke-virtual {v0, p1, p2, p3, v1}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;II)Z

    #@33
    move-result v0

    #@34
    goto :goto_27
.end method

.method private static buildSpellCheckerMapLocked(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/HashMap;Lcom/android/server/TextServicesManagerService$TextServicesSettings;)V
    .registers 16
    .parameter "context"
    .parameter
    .parameter
    .parameter "settings"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/textservice/SpellCheckerInfo;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/view/textservice/SpellCheckerInfo;",
            ">;",
            "Lcom/android/server/TextServicesManagerService$TextServicesSettings;",
            ")V"
        }
    .end annotation

    #@0
    .prologue
    .line 176
    .local p1, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/textservice/SpellCheckerInfo;>;"
    .local p2, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Landroid/view/textservice/SpellCheckerInfo;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    #@3
    .line 177
    invoke-virtual {p2}, Ljava/util/HashMap;->clear()V

    #@6
    .line 178
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@9
    move-result-object v4

    #@a
    .line 179
    .local v4, pm:Landroid/content/pm/PackageManager;
    new-instance v9, Landroid/content/Intent;

    #@c
    const-string v10, "android.service.textservice.SpellCheckerService"

    #@e
    invoke-direct {v9, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@11
    const/16 v10, 0x80

    #@13
    invoke-virtual {p3}, Lcom/android/server/TextServicesManagerService$TextServicesSettings;->getCurrentUserId()I

    #@16
    move-result v11

    #@17
    invoke-virtual {v4, v9, v10, v11}, Landroid/content/pm/PackageManager;->queryIntentServicesAsUser(Landroid/content/Intent;II)Ljava/util/List;

    #@1a
    move-result-object v7

    #@1b
    .line 182
    .local v7, services:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v7}, Ljava/util/List;->size()I

    #@1e
    move-result v0

    #@1f
    .line 183
    .local v0, N:I
    const/4 v3, 0x0

    #@20
    .local v3, i:I
    :goto_20
    if-ge v3, v0, :cond_cd

    #@22
    .line 184
    invoke-interface {v7, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@25
    move-result-object v5

    #@26
    check-cast v5, Landroid/content/pm/ResolveInfo;

    #@28
    .line 185
    .local v5, ri:Landroid/content/pm/ResolveInfo;
    iget-object v8, v5, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@2a
    .line 186
    .local v8, si:Landroid/content/pm/ServiceInfo;
    new-instance v1, Landroid/content/ComponentName;

    #@2c
    iget-object v9, v8, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    #@2e
    iget-object v10, v8, Landroid/content/pm/ComponentInfo;->name:Ljava/lang/String;

    #@30
    invoke-direct {v1, v9, v10}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@33
    .line 187
    .local v1, compName:Landroid/content/ComponentName;
    const-string v9, "android.permission.BIND_TEXT_SERVICE"

    #@35
    iget-object v10, v8, Landroid/content/pm/ServiceInfo;->permission:Ljava/lang/String;

    #@37
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3a
    move-result v9

    #@3b
    if-nez v9, :cond_64

    #@3d
    .line 188
    sget-object v9, Lcom/android/server/TextServicesManagerService;->TAG:Ljava/lang/String;

    #@3f
    new-instance v10, Ljava/lang/StringBuilder;

    #@41
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@44
    const-string v11, "Skipping text service "

    #@46
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v10

    #@4a
    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v10

    #@4e
    const-string v11, ": it does not require the permission "

    #@50
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v10

    #@54
    const-string v11, "android.permission.BIND_TEXT_SERVICE"

    #@56
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v10

    #@5a
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5d
    move-result-object v10

    #@5e
    invoke-static {v9, v10}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@61
    .line 183
    :goto_61
    add-int/lit8 v3, v3, 0x1

    #@63
    goto :goto_20

    #@64
    .line 195
    :cond_64
    :try_start_64
    new-instance v6, Landroid/view/textservice/SpellCheckerInfo;

    #@66
    invoke-direct {v6, p0, v5}, Landroid/view/textservice/SpellCheckerInfo;-><init>(Landroid/content/Context;Landroid/content/pm/ResolveInfo;)V

    #@69
    .line 196
    .local v6, sci:Landroid/view/textservice/SpellCheckerInfo;
    invoke-virtual {v6}, Landroid/view/textservice/SpellCheckerInfo;->getSubtypeCount()I

    #@6c
    move-result v9

    #@6d
    if-gtz v9, :cond_a8

    #@6f
    .line 197
    sget-object v9, Lcom/android/server/TextServicesManagerService;->TAG:Ljava/lang/String;

    #@71
    new-instance v10, Ljava/lang/StringBuilder;

    #@73
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@76
    const-string v11, "Skipping text service "

    #@78
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v10

    #@7c
    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v10

    #@80
    const-string v11, ": it does not contain subtypes."

    #@82
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v10

    #@86
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@89
    move-result-object v10

    #@8a
    invoke-static {v9, v10}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8d
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_64 .. :try_end_8d} :catch_8e
    .catch Ljava/io/IOException; {:try_start_64 .. :try_end_8d} :catch_b3

    #@8d
    goto :goto_61

    #@8e
    .line 203
    .end local v6           #sci:Landroid/view/textservice/SpellCheckerInfo;
    :catch_8e
    move-exception v2

    #@8f
    .line 204
    .local v2, e:Lorg/xmlpull/v1/XmlPullParserException;
    sget-object v9, Lcom/android/server/TextServicesManagerService;->TAG:Ljava/lang/String;

    #@91
    new-instance v10, Ljava/lang/StringBuilder;

    #@93
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@96
    const-string v11, "Unable to load the spell checker "

    #@98
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v10

    #@9c
    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v10

    #@a0
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a3
    move-result-object v10

    #@a4
    invoke-static {v9, v10, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@a7
    goto :goto_61

    #@a8
    .line 201
    .end local v2           #e:Lorg/xmlpull/v1/XmlPullParserException;
    .restart local v6       #sci:Landroid/view/textservice/SpellCheckerInfo;
    :cond_a8
    :try_start_a8
    invoke-virtual {p1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@ab
    .line 202
    invoke-virtual {v6}, Landroid/view/textservice/SpellCheckerInfo;->getId()Ljava/lang/String;

    #@ae
    move-result-object v9

    #@af
    invoke-virtual {p2, v9, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_b2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_a8 .. :try_end_b2} :catch_8e
    .catch Ljava/io/IOException; {:try_start_a8 .. :try_end_b2} :catch_b3

    #@b2
    goto :goto_61

    #@b3
    .line 205
    .end local v6           #sci:Landroid/view/textservice/SpellCheckerInfo;
    :catch_b3
    move-exception v2

    #@b4
    .line 206
    .local v2, e:Ljava/io/IOException;
    sget-object v9, Lcom/android/server/TextServicesManagerService;->TAG:Ljava/lang/String;

    #@b6
    new-instance v10, Ljava/lang/StringBuilder;

    #@b8
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@bb
    const-string v11, "Unable to load the spell checker "

    #@bd
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v10

    #@c1
    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c4
    move-result-object v10

    #@c5
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c8
    move-result-object v10

    #@c9
    invoke-static {v9, v10, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@cc
    goto :goto_61

    #@cd
    .line 212
    .end local v1           #compName:Landroid/content/ComponentName;
    .end local v2           #e:Ljava/io/IOException;
    .end local v5           #ri:Landroid/content/pm/ResolveInfo;
    .end local v8           #si:Landroid/content/pm/ServiceInfo;
    :cond_cd
    return-void
.end method

.method private calledFromValidUser()Z
    .registers 6

    #@0
    .prologue
    .line 219
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@3
    move-result v0

    #@4
    .line 220
    .local v0, uid:I
    invoke-static {v0}, Landroid/os/UserHandle;->getUserId(I)I

    #@7
    move-result v1

    #@8
    .line 237
    .local v1, userId:I
    const/16 v2, 0x3e8

    #@a
    if-eq v0, v2, :cond_14

    #@c
    iget-object v2, p0, Lcom/android/server/TextServicesManagerService;->mSettings:Lcom/android/server/TextServicesManagerService$TextServicesSettings;

    #@e
    invoke-virtual {v2}, Lcom/android/server/TextServicesManagerService$TextServicesSettings;->getCurrentUserId()I

    #@11
    move-result v2

    #@12
    if-ne v1, v2, :cond_16

    #@14
    .line 238
    :cond_14
    const/4 v2, 0x1

    #@15
    .line 241
    :goto_15
    return v2

    #@16
    .line 240
    :cond_16
    sget-object v2, Lcom/android/server/TextServicesManagerService;->TAG:Ljava/lang/String;

    #@18
    new-instance v3, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v4, "--- IPC called from background users. Ignore. \n"

    #@1f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    invoke-static {}, Lcom/android/server/TextServicesManagerService;->getStackTrace()Ljava/lang/String;

    #@26
    move-result-object v4

    #@27
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v3

    #@2f
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 241
    const/4 v2, 0x0

    #@33
    goto :goto_15
.end method

.method private findAvailSpellCheckerLocked(Ljava/lang/String;Ljava/lang/String;)Landroid/view/textservice/SpellCheckerInfo;
    .registers 8
    .parameter "locale"
    .parameter "prefPackage"

    #@0
    .prologue
    .line 263
    iget-object v3, p0, Lcom/android/server/TextServicesManagerService;->mSpellCheckerList:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v2

    #@6
    .line 264
    .local v2, spellCheckersCount:I
    if-nez v2, :cond_11

    #@8
    .line 265
    sget-object v3, Lcom/android/server/TextServicesManagerService;->TAG:Ljava/lang/String;

    #@a
    const-string v4, "no available spell checker services found"

    #@c
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 266
    const/4 v1, 0x0

    #@10
    .line 282
    :cond_10
    :goto_10
    return-object v1

    #@11
    .line 268
    :cond_11
    if-eqz p2, :cond_2b

    #@13
    .line 269
    const/4 v0, 0x0

    #@14
    .local v0, i:I
    :goto_14
    if-ge v0, v2, :cond_2b

    #@16
    .line 270
    iget-object v3, p0, Lcom/android/server/TextServicesManagerService;->mSpellCheckerList:Ljava/util/ArrayList;

    #@18
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1b
    move-result-object v1

    #@1c
    check-cast v1, Landroid/view/textservice/SpellCheckerInfo;

    #@1e
    .line 271
    .local v1, sci:Landroid/view/textservice/SpellCheckerInfo;
    invoke-virtual {v1}, Landroid/view/textservice/SpellCheckerInfo;->getPackageName()Ljava/lang/String;

    #@21
    move-result-object v3

    #@22
    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@25
    move-result v3

    #@26
    if-nez v3, :cond_10

    #@28
    .line 269
    add-int/lit8 v0, v0, 0x1

    #@2a
    goto :goto_14

    #@2b
    .line 279
    .end local v0           #i:I
    .end local v1           #sci:Landroid/view/textservice/SpellCheckerInfo;
    :cond_2b
    const/4 v3, 0x1

    #@2c
    if-le v2, v3, :cond_35

    #@2e
    .line 280
    sget-object v3, Lcom/android/server/TextServicesManagerService;->TAG:Ljava/lang/String;

    #@30
    const-string v4, "more than one spell checker service found, picking first"

    #@32
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@35
    .line 282
    :cond_35
    iget-object v3, p0, Lcom/android/server/TextServicesManagerService;->mSpellCheckerList:Ljava/util/ArrayList;

    #@37
    const/4 v4, 0x0

    #@38
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@3b
    move-result-object v3

    #@3c
    check-cast v3, Landroid/view/textservice/SpellCheckerInfo;

    #@3e
    move-object v1, v3

    #@3f
    goto :goto_10
.end method

.method private static getStackTrace()Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    .line 968
    new-instance v3, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 970
    .local v3, sb:Ljava/lang/StringBuilder;
    :try_start_5
    new-instance v4, Ljava/lang/RuntimeException;

    #@7
    invoke-direct {v4}, Ljava/lang/RuntimeException;-><init>()V

    #@a
    throw v4
    :try_end_b
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_b} :catch_b

    #@b
    .line 971
    :catch_b
    move-exception v0

    #@c
    .line 972
    .local v0, e:Ljava/lang/RuntimeException;
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getStackTrace()[Ljava/lang/StackTraceElement;

    #@f
    move-result-object v1

    #@10
    .line 974
    .local v1, frames:[Ljava/lang/StackTraceElement;
    const/4 v2, 0x1

    #@11
    .local v2, j:I
    :goto_11
    array-length v4, v1

    #@12
    if-ge v2, v4, :cond_33

    #@14
    .line 975
    new-instance v4, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    aget-object v5, v1, v2

    #@1b
    invoke-virtual {v5}, Ljava/lang/StackTraceElement;->toString()Ljava/lang/String;

    #@1e
    move-result-object v5

    #@1f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v4

    #@23
    const-string v5, "\n"

    #@25
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v4

    #@29
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v4

    #@2d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    .line 974
    add-int/lit8 v2, v2, 0x1

    #@32
    goto :goto_11

    #@33
    .line 978
    :cond_33
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v4

    #@37
    return-object v4
.end method

.method private isSpellCheckerEnabledLocked()Z
    .registers 5

    #@0
    .prologue
    .line 631
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@3
    move-result-wide v0

    #@4
    .line 633
    .local v0, ident:J
    :try_start_4
    iget-object v3, p0, Lcom/android/server/TextServicesManagerService;->mSettings:Lcom/android/server/TextServicesManagerService$TextServicesSettings;

    #@6
    invoke-virtual {v3}, Lcom/android/server/TextServicesManagerService$TextServicesSettings;->isSpellCheckerEnabled()Z
    :try_end_9
    .catchall {:try_start_4 .. :try_end_9} :catchall_e

    #@9
    move-result v2

    #@a
    .line 639
    .local v2, retval:Z
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@d
    .line 637
    return v2

    #@e
    .line 639
    .end local v2           #retval:Z
    :catchall_e
    move-exception v3

    #@f
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@12
    throw v3
.end method

.method private setCurrentSpellCheckerLocked(Ljava/lang/String;)V
    .registers 6
    .parameter "sciId"

    #@0
    .prologue
    .line 583
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@3
    move-result v3

    #@4
    if-nez v3, :cond_e

    #@6
    iget-object v3, p0, Lcom/android/server/TextServicesManagerService;->mSpellCheckerMap:Ljava/util/HashMap;

    #@8
    invoke-virtual {v3, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@b
    move-result v3

    #@c
    if-nez v3, :cond_f

    #@e
    .line 596
    :cond_e
    :goto_e
    return-void

    #@f
    .line 584
    :cond_f
    const/4 v3, 0x0

    #@10
    invoke-virtual {p0, v3}, Lcom/android/server/TextServicesManagerService;->getCurrentSpellChecker(Ljava/lang/String;)Landroid/view/textservice/SpellCheckerInfo;

    #@13
    move-result-object v0

    #@14
    .line 585
    .local v0, currentSci:Landroid/view/textservice/SpellCheckerInfo;
    if-eqz v0, :cond_20

    #@16
    invoke-virtual {v0}, Landroid/view/textservice/SpellCheckerInfo;->getId()Ljava/lang/String;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1d
    move-result v3

    #@1e
    if-nez v3, :cond_e

    #@20
    .line 589
    :cond_20
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@23
    move-result-wide v1

    #@24
    .line 591
    .local v1, ident:J
    :try_start_24
    iget-object v3, p0, Lcom/android/server/TextServicesManagerService;->mSettings:Lcom/android/server/TextServicesManagerService$TextServicesSettings;

    #@26
    invoke-virtual {v3, p1}, Lcom/android/server/TextServicesManagerService$TextServicesSettings;->putSelectedSpellChecker(Ljava/lang/String;)V

    #@29
    .line 592
    const/4 v3, 0x0

    #@2a
    invoke-direct {p0, v3}, Lcom/android/server/TextServicesManagerService;->setCurrentSpellCheckerSubtypeLocked(I)V
    :try_end_2d
    .catchall {:try_start_24 .. :try_end_2d} :catchall_31

    #@2d
    .line 594
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@30
    goto :goto_e

    #@31
    :catchall_31
    move-exception v3

    #@32
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@35
    throw v3
.end method

.method private setCurrentSpellCheckerSubtypeLocked(I)V
    .registers 8
    .parameter "hashCode"

    #@0
    .prologue
    .line 602
    const/4 v5, 0x0

    #@1
    invoke-virtual {p0, v5}, Lcom/android/server/TextServicesManagerService;->getCurrentSpellChecker(Ljava/lang/String;)Landroid/view/textservice/SpellCheckerInfo;

    #@4
    move-result-object v3

    #@5
    .line 603
    .local v3, sci:Landroid/view/textservice/SpellCheckerInfo;
    const/4 v4, 0x0

    #@6
    .line 604
    .local v4, tempHashCode:I
    const/4 v0, 0x0

    #@7
    .local v0, i:I
    :goto_7
    if-eqz v3, :cond_1a

    #@9
    invoke-virtual {v3}, Landroid/view/textservice/SpellCheckerInfo;->getSubtypeCount()I

    #@c
    move-result v5

    #@d
    if-ge v0, v5, :cond_1a

    #@f
    .line 605
    invoke-virtual {v3, v0}, Landroid/view/textservice/SpellCheckerInfo;->getSubtypeAt(I)Landroid/view/textservice/SpellCheckerSubtype;

    #@12
    move-result-object v5

    #@13
    invoke-virtual {v5}, Landroid/view/textservice/SpellCheckerSubtype;->hashCode()I

    #@16
    move-result v5

    #@17
    if-ne v5, p1, :cond_27

    #@19
    .line 606
    move v4, p1

    #@1a
    .line 610
    :cond_1a
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@1d
    move-result-wide v1

    #@1e
    .line 612
    .local v1, ident:J
    :try_start_1e
    iget-object v5, p0, Lcom/android/server/TextServicesManagerService;->mSettings:Lcom/android/server/TextServicesManagerService$TextServicesSettings;

    #@20
    invoke-virtual {v5, v4}, Lcom/android/server/TextServicesManagerService$TextServicesSettings;->putSelectedSpellCheckerSubtype(I)V
    :try_end_23
    .catchall {:try_start_1e .. :try_end_23} :catchall_2a

    #@23
    .line 614
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@26
    .line 616
    return-void

    #@27
    .line 604
    .end local v1           #ident:J
    :cond_27
    add-int/lit8 v0, v0, 0x1

    #@29
    goto :goto_7

    #@2a
    .line 614
    .restart local v1       #ident:J
    :catchall_2a
    move-exception v5

    #@2b
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@2e
    throw v5
.end method

.method private setSpellCheckerEnabledLocked(Z)V
    .registers 5
    .parameter "enabled"

    #@0
    .prologue
    .line 622
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@3
    move-result-wide v0

    #@4
    .line 624
    .local v0, ident:J
    :try_start_4
    iget-object v2, p0, Lcom/android/server/TextServicesManagerService;->mSettings:Lcom/android/server/TextServicesManagerService$TextServicesSettings;

    #@6
    invoke-virtual {v2, p1}, Lcom/android/server/TextServicesManagerService$TextServicesSettings;->setSpellCheckerEnabled(Z)V
    :try_end_9
    .catchall {:try_start_4 .. :try_end_9} :catchall_d

    #@9
    .line 626
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@c
    .line 628
    return-void

    #@d
    .line 626
    :catchall_d
    move-exception v2

    #@e
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@11
    throw v2
.end method

.method private startSpellCheckerServiceInnerLocked(Landroid/view/textservice/SpellCheckerInfo;Ljava/lang/String;Lcom/android/internal/textservice/ITextServicesSessionListener;Lcom/android/internal/textservice/ISpellCheckerSessionListener;ILandroid/os/Bundle;)V
    .registers 18
    .parameter "info"
    .parameter "locale"
    .parameter "tsListener"
    .parameter "scListener"
    .parameter "uid"
    .parameter "bundle"

    #@0
    .prologue
    .line 473
    invoke-virtual {p1}, Landroid/view/textservice/SpellCheckerInfo;->getId()Ljava/lang/String;

    #@3
    move-result-object v9

    #@4
    .line 474
    .local v9, sciId:Ljava/lang/String;
    new-instance v3, Lcom/android/server/TextServicesManagerService$InternalServiceConnection;

    #@6
    move-object/from16 v0, p6

    #@8
    invoke-direct {v3, p0, v9, p2, v0}, Lcom/android/server/TextServicesManagerService$InternalServiceConnection;-><init>(Lcom/android/server/TextServicesManagerService;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    #@b
    .line 476
    .local v3, connection:Lcom/android/server/TextServicesManagerService$InternalServiceConnection;
    new-instance v10, Landroid/content/Intent;

    #@d
    const-string v2, "android.service.textservice.SpellCheckerService"

    #@f
    invoke-direct {v10, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@12
    .line 477
    .local v10, serviceIntent:Landroid/content/Intent;
    invoke-virtual {p1}, Landroid/view/textservice/SpellCheckerInfo;->getComponent()Landroid/content/ComponentName;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v10, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@19
    .line 481
    const/4 v2, 0x1

    #@1a
    invoke-direct {p0, v10, v3, v2}, Lcom/android/server/TextServicesManagerService;->bindCurrentSpellCheckerService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    #@1d
    move-result v2

    #@1e
    if-nez v2, :cond_28

    #@20
    .line 482
    sget-object v2, Lcom/android/server/TextServicesManagerService;->TAG:Ljava/lang/String;

    #@22
    const-string v4, "Failed to get a spell checker service."

    #@24
    invoke-static {v2, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 488
    :goto_27
    return-void

    #@28
    .line 485
    :cond_28
    new-instance v1, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;

    #@2a
    move-object v2, p0

    #@2b
    move-object v4, p3

    #@2c
    move-object v5, p2

    #@2d
    move-object v6, p4

    #@2e
    move/from16 v7, p5

    #@30
    move-object/from16 v8, p6

    #@32
    invoke-direct/range {v1 .. v8}, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;-><init>(Lcom/android/server/TextServicesManagerService;Lcom/android/server/TextServicesManagerService$InternalServiceConnection;Lcom/android/internal/textservice/ITextServicesSessionListener;Ljava/lang/String;Lcom/android/internal/textservice/ISpellCheckerSessionListener;ILandroid/os/Bundle;)V

    #@35
    .line 487
    .local v1, group:Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;
    iget-object v2, p0, Lcom/android/server/TextServicesManagerService;->mSpellCheckerBindGroups:Ljava/util/HashMap;

    #@37
    invoke-virtual {v2, v9, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3a
    goto :goto_27
.end method

.method private switchUserLocked(I)V
    .registers 8
    .parameter "userId"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 122
    iget-object v1, p0, Lcom/android/server/TextServicesManagerService;->mSettings:Lcom/android/server/TextServicesManagerService$TextServicesSettings;

    #@3
    invoke-virtual {v1, p1}, Lcom/android/server/TextServicesManagerService$TextServicesSettings;->setCurrentUserId(I)V

    #@6
    .line 123
    invoke-direct {p0}, Lcom/android/server/TextServicesManagerService;->unbindServiceLocked()V

    #@9
    .line 124
    iget-object v1, p0, Lcom/android/server/TextServicesManagerService;->mContext:Landroid/content/Context;

    #@b
    iget-object v2, p0, Lcom/android/server/TextServicesManagerService;->mSpellCheckerList:Ljava/util/ArrayList;

    #@d
    iget-object v3, p0, Lcom/android/server/TextServicesManagerService;->mSpellCheckerMap:Ljava/util/HashMap;

    #@f
    iget-object v4, p0, Lcom/android/server/TextServicesManagerService;->mSettings:Lcom/android/server/TextServicesManagerService$TextServicesSettings;

    #@11
    invoke-static {v1, v2, v3, v4}, Lcom/android/server/TextServicesManagerService;->buildSpellCheckerMapLocked(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/HashMap;Lcom/android/server/TextServicesManagerService$TextServicesSettings;)V

    #@14
    .line 125
    invoke-virtual {p0, v5}, Lcom/android/server/TextServicesManagerService;->getCurrentSpellChecker(Ljava/lang/String;)Landroid/view/textservice/SpellCheckerInfo;

    #@17
    move-result-object v0

    #@18
    .line 126
    .local v0, sci:Landroid/view/textservice/SpellCheckerInfo;
    if-nez v0, :cond_27

    #@1a
    .line 127
    invoke-direct {p0, v5, v5}, Lcom/android/server/TextServicesManagerService;->findAvailSpellCheckerLocked(Ljava/lang/String;Ljava/lang/String;)Landroid/view/textservice/SpellCheckerInfo;

    #@1d
    move-result-object v0

    #@1e
    .line 128
    if-eqz v0, :cond_27

    #@20
    .line 132
    invoke-virtual {v0}, Landroid/view/textservice/SpellCheckerInfo;->getId()Ljava/lang/String;

    #@23
    move-result-object v1

    #@24
    invoke-direct {p0, v1}, Lcom/android/server/TextServicesManagerService;->setCurrentSpellCheckerLocked(Ljava/lang/String;)V

    #@27
    .line 135
    :cond_27
    return-void
.end method

.method private unbindServiceLocked()V
    .registers 4

    #@0
    .prologue
    .line 255
    iget-object v2, p0, Lcom/android/server/TextServicesManagerService;->mSpellCheckerBindGroups:Ljava/util/HashMap;

    #@2
    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@5
    move-result-object v2

    #@6
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@9
    move-result-object v0

    #@a
    .local v0, i$:Ljava/util/Iterator;
    :goto_a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@d
    move-result v2

    #@e
    if-eqz v2, :cond_1a

    #@10
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@13
    move-result-object v1

    #@14
    check-cast v1, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;

    #@16
    .line 256
    .local v1, scbg:Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;
    invoke-virtual {v1}, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->removeAll()V

    #@19
    goto :goto_a

    #@1a
    .line 258
    .end local v1           #scbg:Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;
    :cond_1a
    iget-object v2, p0, Lcom/android/server/TextServicesManagerService;->mSpellCheckerBindGroups:Ljava/util/HashMap;

    #@1c
    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    #@1f
    .line 259
    return-void
.end method


# virtual methods
.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 16
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 645
    iget-object v10, p0, Lcom/android/server/TextServicesManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v11, "android.permission.DUMP"

    #@4
    invoke-virtual {v10, v11}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@7
    move-result v10

    #@8
    if-eqz v10, :cond_33

    #@a
    .line 648
    new-instance v10, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v11, "Permission Denial: can\'t dump TextServicesManagerService from from pid="

    #@11
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v10

    #@15
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@18
    move-result v11

    #@19
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v10

    #@1d
    const-string v11, ", uid="

    #@1f
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v10

    #@23
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@26
    move-result v11

    #@27
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v10

    #@2b
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v10

    #@2f
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@32
    .line 701
    :goto_32
    return-void

    #@33
    .line 654
    :cond_33
    iget-object v11, p0, Lcom/android/server/TextServicesManagerService;->mSpellCheckerMap:Ljava/util/HashMap;

    #@35
    monitor-enter v11

    #@36
    .line 655
    :try_start_36
    const-string v10, "Current Text Services Manager state:"

    #@38
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@3b
    .line 656
    const-string v10, "  Spell Checker Map:"

    #@3d
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@40
    .line 657
    iget-object v10, p0, Lcom/android/server/TextServicesManagerService;->mSpellCheckerMap:Ljava/util/HashMap;

    #@42
    invoke-virtual {v10}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@45
    move-result-object v10

    #@46
    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@49
    move-result-object v6

    #@4a
    .local v6, i$:Ljava/util/Iterator;
    :cond_4a
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    #@4d
    move-result v10

    #@4e
    if-eqz v10, :cond_d7

    #@50
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@53
    move-result-object v2

    #@54
    check-cast v2, Ljava/util/Map$Entry;

    #@56
    .line 658
    .local v2, ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Landroid/view/textservice/SpellCheckerInfo;>;"
    const-string v10, "    "

    #@58
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@5b
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@5e
    move-result-object v10

    #@5f
    check-cast v10, Ljava/lang/String;

    #@61
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@64
    const-string v10, ":"

    #@66
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@69
    .line 659
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@6c
    move-result-object v7

    #@6d
    check-cast v7, Landroid/view/textservice/SpellCheckerInfo;

    #@6f
    .line 660
    .local v7, info:Landroid/view/textservice/SpellCheckerInfo;
    const-string v10, "      "

    #@71
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@74
    const-string v10, "id="

    #@76
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@79
    invoke-virtual {v7}, Landroid/view/textservice/SpellCheckerInfo;->getId()Ljava/lang/String;

    #@7c
    move-result-object v10

    #@7d
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@80
    .line 661
    const-string v10, "      "

    #@82
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@85
    const-string v10, "comp="

    #@87
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@8a
    .line 662
    invoke-virtual {v7}, Landroid/view/textservice/SpellCheckerInfo;->getComponent()Landroid/content/ComponentName;

    #@8d
    move-result-object v10

    #@8e
    invoke-virtual {v10}, Landroid/content/ComponentName;->toShortString()Ljava/lang/String;

    #@91
    move-result-object v10

    #@92
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@95
    .line 663
    invoke-virtual {v7}, Landroid/view/textservice/SpellCheckerInfo;->getSubtypeCount()I

    #@98
    move-result v1

    #@99
    .line 664
    .local v1, NS:I
    const/4 v5, 0x0

    #@9a
    .local v5, i:I
    :goto_9a
    if-ge v5, v1, :cond_4a

    #@9c
    .line 665
    invoke-virtual {v7, v5}, Landroid/view/textservice/SpellCheckerInfo;->getSubtypeAt(I)Landroid/view/textservice/SpellCheckerSubtype;

    #@9f
    move-result-object v9

    #@a0
    .line 666
    .local v9, st:Landroid/view/textservice/SpellCheckerSubtype;
    const-string v10, "      "

    #@a2
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@a5
    const-string v10, "Subtype #"

    #@a7
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@aa
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(I)V

    #@ad
    const-string v10, ":"

    #@af
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@b2
    .line 667
    const-string v10, "        "

    #@b4
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@b7
    const-string v10, "locale="

    #@b9
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@bc
    invoke-virtual {v9}, Landroid/view/textservice/SpellCheckerSubtype;->getLocale()Ljava/lang/String;

    #@bf
    move-result-object v10

    #@c0
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@c3
    .line 668
    const-string v10, "        "

    #@c5
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@c8
    const-string v10, "extraValue="

    #@ca
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@cd
    .line 669
    invoke-virtual {v9}, Landroid/view/textservice/SpellCheckerSubtype;->getExtraValue()Ljava/lang/String;

    #@d0
    move-result-object v10

    #@d1
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@d4
    .line 664
    add-int/lit8 v5, v5, 0x1

    #@d6
    goto :goto_9a

    #@d7
    .line 672
    .end local v1           #NS:I
    .end local v2           #ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Landroid/view/textservice/SpellCheckerInfo;>;"
    .end local v5           #i:I
    .end local v7           #info:Landroid/view/textservice/SpellCheckerInfo;
    .end local v9           #st:Landroid/view/textservice/SpellCheckerSubtype;
    :cond_d7
    const-string v10, ""

    #@d9
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@dc
    .line 673
    const-string v10, "  Spell Checker Bind Groups:"

    #@de
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@e1
    .line 675
    iget-object v10, p0, Lcom/android/server/TextServicesManagerService;->mSpellCheckerBindGroups:Ljava/util/HashMap;

    #@e3
    invoke-virtual {v10}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@e6
    move-result-object v10

    #@e7
    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@ea
    move-result-object v6

    #@eb
    :cond_eb
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    #@ee
    move-result v10

    #@ef
    if-eqz v10, :cond_1c3

    #@f1
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f4
    move-result-object v3

    #@f5
    check-cast v3, Ljava/util/Map$Entry;

    #@f7
    .line 676
    .local v3, ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@fa
    move-result-object v4

    #@fb
    check-cast v4, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;

    #@fd
    .line 677
    .local v4, grp:Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;
    const-string v10, "    "

    #@ff
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@102
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@105
    move-result-object v10

    #@106
    check-cast v10, Ljava/lang/String;

    #@108
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@10b
    const-string v10, " "

    #@10d
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@110
    .line 678
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    #@113
    const-string v10, ":"

    #@115
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@118
    .line 679
    const-string v10, "      "

    #@11a
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@11d
    const-string v10, "mInternalConnection="

    #@11f
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@122
    .line 680
    invoke-static {v4}, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->access$900(Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;)Lcom/android/server/TextServicesManagerService$InternalServiceConnection;

    #@125
    move-result-object v10

    #@126
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@129
    .line 681
    const-string v10, "      "

    #@12b
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@12e
    const-string v10, "mSpellChecker="

    #@130
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@133
    .line 682
    iget-object v10, v4, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->mSpellChecker:Lcom/android/internal/textservice/ISpellCheckerService;

    #@135
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@138
    .line 683
    const-string v10, "      "

    #@13a
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@13d
    const-string v10, "mBound="

    #@13f
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@142
    iget-boolean v10, v4, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->mBound:Z

    #@144
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->print(Z)V

    #@147
    .line 684
    const-string v10, " mConnected="

    #@149
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@14c
    iget-boolean v10, v4, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->mConnected:Z

    #@14e
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->println(Z)V

    #@151
    .line 685
    invoke-static {v4}, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->access$1000(Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;)Ljava/util/concurrent/CopyOnWriteArrayList;

    #@154
    move-result-object v10

    #@155
    invoke-virtual {v10}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    #@158
    move-result v0

    #@159
    .line 686
    .local v0, NL:I
    const/4 v5, 0x0

    #@15a
    .restart local v5       #i:I
    :goto_15a
    if-ge v5, v0, :cond_eb

    #@15c
    .line 687
    invoke-static {v4}, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->access$1000(Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;)Ljava/util/concurrent/CopyOnWriteArrayList;

    #@15f
    move-result-object v10

    #@160
    invoke-virtual {v10, v5}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    #@163
    move-result-object v8

    #@164
    check-cast v8, Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;

    #@166
    .line 688
    .local v8, listener:Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;
    const-string v10, "      "

    #@168
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@16b
    const-string v10, "Listener #"

    #@16d
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@170
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(I)V

    #@173
    const-string v10, ":"

    #@175
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@178
    .line 689
    const-string v10, "        "

    #@17a
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@17d
    const-string v10, "mTsListener="

    #@17f
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@182
    .line 690
    iget-object v10, v8, Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;->mTsListener:Lcom/android/internal/textservice/ITextServicesSessionListener;

    #@184
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@187
    .line 691
    const-string v10, "        "

    #@189
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@18c
    const-string v10, "mScListener="

    #@18e
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@191
    .line 692
    iget-object v10, v8, Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;->mScListener:Lcom/android/internal/textservice/ISpellCheckerSessionListener;

    #@193
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@196
    .line 693
    const-string v10, "        "

    #@198
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@19b
    const-string v10, "mGroup="

    #@19d
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1a0
    .line 694
    invoke-static {v8}, Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;->access$1100(Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;)Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;

    #@1a3
    move-result-object v10

    #@1a4
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@1a7
    .line 695
    const-string v10, "        "

    #@1a9
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1ac
    const-string v10, "mScLocale="

    #@1ae
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1b1
    .line 696
    iget-object v10, v8, Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;->mScLocale:Ljava/lang/String;

    #@1b3
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1b6
    .line 697
    const-string v10, " mUid="

    #@1b8
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1bb
    iget v10, v8, Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;->mUid:I

    #@1bd
    invoke-virtual {p2, v10}, Ljava/io/PrintWriter;->println(I)V

    #@1c0
    .line 686
    add-int/lit8 v5, v5, 0x1

    #@1c2
    goto :goto_15a

    #@1c3
    .line 700
    .end local v0           #NL:I
    .end local v3           #ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;>;"
    .end local v4           #grp:Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;
    .end local v5           #i:I
    .end local v8           #listener:Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;
    :cond_1c3
    monitor-exit v11

    #@1c4
    goto/16 :goto_32

    #@1c6
    .end local v6           #i$:Ljava/util/Iterator;
    :catchall_1c6
    move-exception v10

    #@1c7
    monitor-exit v11
    :try_end_1c8
    .catchall {:try_start_36 .. :try_end_1c8} :catchall_1c6

    #@1c8
    throw v10
.end method

.method public finishSpellCheckerService(Lcom/android/internal/textservice/ISpellCheckerSessionListener;)V
    .registers 9
    .parameter "listener"

    #@0
    .prologue
    .line 507
    invoke-direct {p0}, Lcom/android/server/TextServicesManagerService;->calledFromValidUser()Z

    #@3
    move-result v5

    #@4
    if-nez v5, :cond_7

    #@6
    .line 526
    :goto_6
    return-void

    #@7
    .line 513
    :cond_7
    iget-object v6, p0, Lcom/android/server/TextServicesManagerService;->mSpellCheckerMap:Ljava/util/HashMap;

    #@9
    monitor-enter v6

    #@a
    .line 514
    :try_start_a
    new-instance v3, Ljava/util/ArrayList;

    #@c
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@f
    .line 516
    .local v3, removeList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;>;"
    iget-object v5, p0, Lcom/android/server/TextServicesManagerService;->mSpellCheckerBindGroups:Ljava/util/HashMap;

    #@11
    invoke-virtual {v5}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@14
    move-result-object v5

    #@15
    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@18
    move-result-object v2

    #@19
    .local v2, i$:Ljava/util/Iterator;
    :cond_19
    :goto_19
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@1c
    move-result v5

    #@1d
    if-eqz v5, :cond_2e

    #@1f
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@22
    move-result-object v0

    #@23
    check-cast v0, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;

    #@25
    .line 517
    .local v0, group:Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;
    if-eqz v0, :cond_19

    #@27
    .line 519
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2a
    goto :goto_19

    #@2b
    .line 525
    .end local v0           #group:Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v3           #removeList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;>;"
    :catchall_2b
    move-exception v5

    #@2c
    monitor-exit v6
    :try_end_2d
    .catchall {:try_start_a .. :try_end_2d} :catchall_2b

    #@2d
    throw v5

    #@2e
    .line 521
    .restart local v2       #i$:Ljava/util/Iterator;
    .restart local v3       #removeList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;>;"
    :cond_2e
    :try_start_2e
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@31
    move-result v4

    #@32
    .line 522
    .local v4, removeSize:I
    const/4 v1, 0x0

    #@33
    .local v1, i:I
    :goto_33
    if-ge v1, v4, :cond_41

    #@35
    .line 523
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@38
    move-result-object v5

    #@39
    check-cast v5, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;

    #@3b
    invoke-virtual {v5, p1}, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->removeListener(Lcom/android/internal/textservice/ISpellCheckerSessionListener;)V

    #@3e
    .line 522
    add-int/lit8 v1, v1, 0x1

    #@40
    goto :goto_33

    #@41
    .line 525
    :cond_41
    monitor-exit v6
    :try_end_42
    .catchall {:try_start_2e .. :try_end_42} :catchall_2b

    #@42
    goto :goto_6
.end method

.method public getCurrentSpellChecker(Ljava/lang/String;)Landroid/view/textservice/SpellCheckerInfo;
    .registers 6
    .parameter "locale"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 290
    invoke-direct {p0}, Lcom/android/server/TextServicesManagerService;->calledFromValidUser()Z

    #@4
    move-result v2

    #@5
    if-nez v2, :cond_8

    #@7
    .line 301
    :goto_7
    return-object v1

    #@8
    .line 293
    :cond_8
    iget-object v2, p0, Lcom/android/server/TextServicesManagerService;->mSpellCheckerMap:Ljava/util/HashMap;

    #@a
    monitor-enter v2

    #@b
    .line 294
    :try_start_b
    iget-object v3, p0, Lcom/android/server/TextServicesManagerService;->mSettings:Lcom/android/server/TextServicesManagerService$TextServicesSettings;

    #@d
    invoke-virtual {v3}, Lcom/android/server/TextServicesManagerService$TextServicesSettings;->getSelectedSpellChecker()Ljava/lang/String;

    #@10
    move-result-object v0

    #@11
    .line 298
    .local v0, curSpellCheckerId:Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@14
    move-result v3

    #@15
    if-eqz v3, :cond_1c

    #@17
    .line 299
    monitor-exit v2

    #@18
    goto :goto_7

    #@19
    .line 302
    .end local v0           #curSpellCheckerId:Ljava/lang/String;
    :catchall_19
    move-exception v1

    #@1a
    monitor-exit v2
    :try_end_1b
    .catchall {:try_start_b .. :try_end_1b} :catchall_19

    #@1b
    throw v1

    #@1c
    .line 301
    .restart local v0       #curSpellCheckerId:Ljava/lang/String;
    :cond_1c
    :try_start_1c
    iget-object v1, p0, Lcom/android/server/TextServicesManagerService;->mSpellCheckerMap:Ljava/util/HashMap;

    #@1e
    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@21
    move-result-object v1

    #@22
    check-cast v1, Landroid/view/textservice/SpellCheckerInfo;

    #@24
    monitor-exit v2
    :try_end_25
    .catchall {:try_start_1c .. :try_end_25} :catchall_19

    #@25
    goto :goto_7
.end method

.method public getCurrentSpellCheckerSubtype(Ljava/lang/String;Z)Landroid/view/textservice/SpellCheckerSubtype;
    .registers 17
    .parameter "locale"
    .parameter "allowImplicitlySelectedSubtype"

    #@0
    .prologue
    .line 311
    invoke-direct {p0}, Lcom/android/server/TextServicesManagerService;->calledFromValidUser()Z

    #@3
    move-result v11

    #@4
    if-nez v11, :cond_8

    #@6
    .line 312
    const/4 v8, 0x0

    #@7
    .line 382
    :goto_7
    return-object v8

    #@8
    .line 314
    :cond_8
    iget-object v12, p0, Lcom/android/server/TextServicesManagerService;->mSpellCheckerMap:Ljava/util/HashMap;

    #@a
    monitor-enter v12

    #@b
    .line 315
    :try_start_b
    iget-object v11, p0, Lcom/android/server/TextServicesManagerService;->mSettings:Lcom/android/server/TextServicesManagerService$TextServicesSettings;

    #@d
    invoke-virtual {v11}, Lcom/android/server/TextServicesManagerService$TextServicesSettings;->getSelectedSpellCheckerSubtype()Ljava/lang/String;

    #@10
    move-result-object v10

    #@11
    .line 319
    .local v10, subtypeHashCodeStr:Ljava/lang/String;
    const/4 v11, 0x0

    #@12
    invoke-virtual {p0, v11}, Lcom/android/server/TextServicesManagerService;->getCurrentSpellChecker(Ljava/lang/String;)Landroid/view/textservice/SpellCheckerInfo;

    #@15
    move-result-object v7

    #@16
    .line 320
    .local v7, sci:Landroid/view/textservice/SpellCheckerInfo;
    if-eqz v7, :cond_1e

    #@18
    invoke-virtual {v7}, Landroid/view/textservice/SpellCheckerInfo;->getSubtypeCount()I

    #@1b
    move-result v11

    #@1c
    if-nez v11, :cond_24

    #@1e
    .line 324
    :cond_1e
    const/4 v8, 0x0

    #@1f
    monitor-exit v12

    #@20
    goto :goto_7

    #@21
    .line 383
    .end local v7           #sci:Landroid/view/textservice/SpellCheckerInfo;
    .end local v10           #subtypeHashCodeStr:Ljava/lang/String;
    :catchall_21
    move-exception v11

    #@22
    monitor-exit v12
    :try_end_23
    .catchall {:try_start_b .. :try_end_23} :catchall_21

    #@23
    throw v11

    #@24
    .line 327
    .restart local v7       #sci:Landroid/view/textservice/SpellCheckerInfo;
    .restart local v10       #subtypeHashCodeStr:Ljava/lang/String;
    :cond_24
    :try_start_24
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@27
    move-result v11

    #@28
    if-nez v11, :cond_39

    #@2a
    .line 328
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    #@2d
    move-result-object v11

    #@2e
    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    #@31
    move-result v3

    #@32
    .line 332
    .local v3, hashCode:I
    :goto_32
    if-nez v3, :cond_3b

    #@34
    if-nez p2, :cond_3b

    #@36
    .line 333
    const/4 v8, 0x0

    #@37
    monitor-exit v12

    #@38
    goto :goto_7

    #@39
    .line 330
    .end local v3           #hashCode:I
    :cond_39
    const/4 v3, 0x0

    #@3a
    .restart local v3       #hashCode:I
    goto :goto_32

    #@3b
    .line 335
    :cond_3b
    const/4 v1, 0x0

    #@3c
    .line 336
    .local v1, candidateLocale:Ljava/lang/String;
    if-nez v3, :cond_6d

    #@3e
    .line 338
    iget-object v11, p0, Lcom/android/server/TextServicesManagerService;->mContext:Landroid/content/Context;

    #@40
    const-string v13, "input_method"

    #@42
    invoke-virtual {v11, v13}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@45
    move-result-object v5

    #@46
    check-cast v5, Landroid/view/inputmethod/InputMethodManager;

    #@48
    .line 340
    .local v5, imm:Landroid/view/inputmethod/InputMethodManager;
    if-eqz v5, :cond_5b

    #@4a
    .line 341
    invoke-virtual {v5}, Landroid/view/inputmethod/InputMethodManager;->getCurrentInputMethodSubtype()Landroid/view/inputmethod/InputMethodSubtype;

    #@4d
    move-result-object v2

    #@4e
    .line 343
    .local v2, currentInputMethodSubtype:Landroid/view/inputmethod/InputMethodSubtype;
    if-eqz v2, :cond_5b

    #@50
    .line 344
    invoke-virtual {v2}, Landroid/view/inputmethod/InputMethodSubtype;->getLocale()Ljava/lang/String;

    #@53
    move-result-object v6

    #@54
    .line 345
    .local v6, localeString:Ljava/lang/String;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@57
    move-result v11

    #@58
    if-nez v11, :cond_5b

    #@5a
    .line 347
    move-object v1, v6

    #@5b
    .line 351
    .end local v2           #currentInputMethodSubtype:Landroid/view/inputmethod/InputMethodSubtype;
    .end local v6           #localeString:Ljava/lang/String;
    :cond_5b
    if-nez v1, :cond_6d

    #@5d
    .line 353
    iget-object v11, p0, Lcom/android/server/TextServicesManagerService;->mContext:Landroid/content/Context;

    #@5f
    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@62
    move-result-object v11

    #@63
    invoke-virtual {v11}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@66
    move-result-object v11

    #@67
    iget-object v11, v11, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@69
    invoke-virtual {v11}, Ljava/util/Locale;->toString()Ljava/lang/String;

    #@6c
    move-result-object v1

    #@6d
    .line 356
    .end local v5           #imm:Landroid/view/inputmethod/InputMethodManager;
    :cond_6d
    const/4 v0, 0x0

    #@6e
    .line 357
    .local v0, candidate:Landroid/view/textservice/SpellCheckerSubtype;
    const/4 v4, 0x0

    #@6f
    .local v4, i:I
    :goto_6f
    invoke-virtual {v7}, Landroid/view/textservice/SpellCheckerInfo;->getSubtypeCount()I

    #@72
    move-result v11

    #@73
    if-ge v4, v11, :cond_aa

    #@75
    .line 358
    invoke-virtual {v7, v4}, Landroid/view/textservice/SpellCheckerInfo;->getSubtypeAt(I)Landroid/view/textservice/SpellCheckerSubtype;

    #@78
    move-result-object v8

    #@79
    .line 359
    .local v8, scs:Landroid/view/textservice/SpellCheckerSubtype;
    if-nez v3, :cond_a1

    #@7b
    .line 360
    invoke-virtual {v8}, Landroid/view/textservice/SpellCheckerSubtype;->getLocale()Ljava/lang/String;

    #@7e
    move-result-object v9

    #@7f
    .line 361
    .local v9, scsLocale:Ljava/lang/String;
    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@82
    move-result v11

    #@83
    if-eqz v11, :cond_87

    #@85
    .line 362
    monitor-exit v12

    #@86
    goto :goto_7

    #@87
    .line 363
    :cond_87
    if-nez v0, :cond_9e

    #@89
    .line 364
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@8c
    move-result v11

    #@8d
    const/4 v13, 0x2

    #@8e
    if-lt v11, v13, :cond_9e

    #@90
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    #@93
    move-result v11

    #@94
    const/4 v13, 0x2

    #@95
    if-lt v11, v13, :cond_9e

    #@97
    invoke-virtual {v1, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@9a
    move-result v11

    #@9b
    if-eqz v11, :cond_9e

    #@9d
    .line 367
    move-object v0, v8

    #@9e
    .line 357
    .end local v9           #scsLocale:Ljava/lang/String;
    :cond_9e
    add-int/lit8 v4, v4, 0x1

    #@a0
    goto :goto_6f

    #@a1
    .line 370
    :cond_a1
    invoke-virtual {v8}, Landroid/view/textservice/SpellCheckerSubtype;->hashCode()I

    #@a4
    move-result v11

    #@a5
    if-ne v11, v3, :cond_9e

    #@a7
    .line 376
    monitor-exit v12

    #@a8
    goto/16 :goto_7

    #@aa
    .line 382
    .end local v8           #scs:Landroid/view/textservice/SpellCheckerSubtype;
    :cond_aa
    monitor-exit v12
    :try_end_ab
    .catchall {:try_start_24 .. :try_end_ab} :catchall_21

    #@ab
    move-object v8, v0

    #@ac
    goto/16 :goto_7
.end method

.method public getEnabledSpellCheckers()[Landroid/view/textservice/SpellCheckerInfo;
    .registers 3

    #@0
    .prologue
    .line 493
    invoke-direct {p0}, Lcom/android/server/TextServicesManagerService;->calledFromValidUser()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_8

    #@6
    .line 494
    const/4 v0, 0x0

    #@7
    .line 502
    :goto_7
    return-object v0

    #@8
    :cond_8
    iget-object v0, p0, Lcom/android/server/TextServicesManagerService;->mSpellCheckerList:Ljava/util/ArrayList;

    #@a
    iget-object v1, p0, Lcom/android/server/TextServicesManagerService;->mSpellCheckerList:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@f
    move-result v1

    #@10
    new-array v1, v1, [Landroid/view/textservice/SpellCheckerInfo;

    #@12
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@15
    move-result-object v0

    #@16
    check-cast v0, [Landroid/view/textservice/SpellCheckerInfo;

    #@18
    goto :goto_7
.end method

.method public getSpellCheckerService(Ljava/lang/String;Ljava/lang/String;Lcom/android/internal/textservice/ITextServicesSessionListener;Lcom/android/internal/textservice/ISpellCheckerSessionListener;Landroid/os/Bundle;)V
    .registers 27
    .parameter "sciId"
    .parameter "locale"
    .parameter "tsListener"
    .parameter "scListener"
    .parameter "bundle"

    #@0
    .prologue
    .line 390
    invoke-direct/range {p0 .. p0}, Lcom/android/server/TextServicesManagerService;->calledFromValidUser()Z

    #@3
    move-result v2

    #@4
    if-nez v2, :cond_7

    #@6
    .line 454
    :cond_6
    :goto_6
    return-void

    #@7
    .line 393
    :cond_7
    move-object/from16 v0, p0

    #@9
    iget-boolean v2, v0, Lcom/android/server/TextServicesManagerService;->mSystemReady:Z

    #@b
    if-eqz v2, :cond_6

    #@d
    .line 396
    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@10
    move-result v2

    #@11
    if-nez v2, :cond_17

    #@13
    if-eqz p3, :cond_17

    #@15
    if-nez p4, :cond_1f

    #@17
    .line 397
    :cond_17
    sget-object v2, Lcom/android/server/TextServicesManagerService;->TAG:Ljava/lang/String;

    #@19
    const-string v3, "getSpellCheckerService: Invalid input."

    #@1b
    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    goto :goto_6

    #@1f
    .line 400
    :cond_1f
    move-object/from16 v0, p0

    #@21
    iget-object v0, v0, Lcom/android/server/TextServicesManagerService;->mSpellCheckerMap:Ljava/util/HashMap;

    #@23
    move-object/from16 v20, v0

    #@25
    monitor-enter v20

    #@26
    .line 401
    :try_start_26
    move-object/from16 v0, p0

    #@28
    iget-object v2, v0, Lcom/android/server/TextServicesManagerService;->mSpellCheckerMap:Ljava/util/HashMap;

    #@2a
    move-object/from16 v0, p1

    #@2c
    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@2f
    move-result v2

    #@30
    if-nez v2, :cond_37

    #@32
    .line 402
    monitor-exit v20

    #@33
    goto :goto_6

    #@34
    .line 453
    :catchall_34
    move-exception v2

    #@35
    monitor-exit v20
    :try_end_36
    .catchall {:try_start_26 .. :try_end_36} :catchall_34

    #@36
    throw v2

    #@37
    .line 404
    :cond_37
    :try_start_37
    move-object/from16 v0, p0

    #@39
    iget-object v2, v0, Lcom/android/server/TextServicesManagerService;->mSpellCheckerMap:Ljava/util/HashMap;

    #@3b
    move-object/from16 v0, p1

    #@3d
    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@40
    move-result-object v8

    #@41
    check-cast v8, Landroid/view/textservice/SpellCheckerInfo;

    #@43
    .line 405
    .local v8, sci:Landroid/view/textservice/SpellCheckerInfo;
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@46
    move-result v6

    #@47
    .line 406
    .local v6, uid:I
    move-object/from16 v0, p0

    #@49
    iget-object v2, v0, Lcom/android/server/TextServicesManagerService;->mSpellCheckerBindGroups:Ljava/util/HashMap;

    #@4b
    move-object/from16 v0, p1

    #@4d
    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@50
    move-result v2

    #@51
    if-eqz v2, :cond_91

    #@53
    .line 407
    move-object/from16 v0, p0

    #@55
    iget-object v2, v0, Lcom/android/server/TextServicesManagerService;->mSpellCheckerBindGroups:Ljava/util/HashMap;

    #@57
    move-object/from16 v0, p1

    #@59
    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5c
    move-result-object v14

    #@5d
    check-cast v14, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;

    #@5f
    .line 408
    .local v14, bindGroup:Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;
    if-eqz v14, :cond_91

    #@61
    .line 409
    move-object/from16 v0, p0

    #@63
    iget-object v2, v0, Lcom/android/server/TextServicesManagerService;->mSpellCheckerBindGroups:Ljava/util/HashMap;

    #@65
    move-object/from16 v0, p1

    #@67
    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@6a
    move-result-object v2

    #@6b
    check-cast v2, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;

    #@6d
    move-object/from16 v3, p3

    #@6f
    move-object/from16 v4, p2

    #@71
    move-object/from16 v5, p4

    #@73
    move-object/from16 v7, p5

    #@75
    invoke-virtual/range {v2 .. v7}, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->addListener(Lcom/android/internal/textservice/ITextServicesSessionListener;Ljava/lang/String;Lcom/android/internal/textservice/ISpellCheckerSessionListener;ILandroid/os/Bundle;)Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;

    #@78
    move-result-object v18

    #@79
    .line 412
    .local v18, recipient:Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;
    if-nez v18, :cond_7d

    #@7b
    .line 416
    monitor-exit v20

    #@7c
    goto :goto_6

    #@7d
    .line 418
    :cond_7d
    iget-object v2, v14, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->mSpellChecker:Lcom/android/internal/textservice/ISpellCheckerService;

    #@7f
    if-nez v2, :cond_a9

    #@81
    const/4 v2, 0x1

    #@82
    :goto_82
    iget-boolean v3, v14, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->mConnected:Z

    #@84
    and-int/2addr v2, v3

    #@85
    if-eqz v2, :cond_ab

    #@87
    .line 419
    sget-object v2, Lcom/android/server/TextServicesManagerService;->TAG:Ljava/lang/String;

    #@89
    const-string v3, "The state of the spell checker bind group is illegal."

    #@8b
    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@8e
    .line 420
    invoke-virtual {v14}, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->removeAll()V

    #@91
    .line 446
    .end local v14           #bindGroup:Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;
    .end local v18           #recipient:Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;
    :cond_91
    :goto_91
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_94
    .catchall {:try_start_37 .. :try_end_94} :catchall_34

    #@94
    move-result-wide v16

    #@95
    .local v16, ident:J
    move-object/from16 v7, p0

    #@97
    move-object/from16 v9, p2

    #@99
    move-object/from16 v10, p3

    #@9b
    move-object/from16 v11, p4

    #@9d
    move v12, v6

    #@9e
    move-object/from16 v13, p5

    #@a0
    .line 448
    :try_start_a0
    invoke-direct/range {v7 .. v13}, Lcom/android/server/TextServicesManagerService;->startSpellCheckerServiceInnerLocked(Landroid/view/textservice/SpellCheckerInfo;Ljava/lang/String;Lcom/android/internal/textservice/ITextServicesSessionListener;Lcom/android/internal/textservice/ISpellCheckerSessionListener;ILandroid/os/Bundle;)V
    :try_end_a3
    .catchall {:try_start_a0 .. :try_end_a3} :catchall_ec

    #@a3
    .line 451
    :try_start_a3
    invoke-static/range {v16 .. v17}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@a6
    .line 453
    monitor-exit v20

    #@a7
    goto/16 :goto_6

    #@a9
    .line 418
    .end local v16           #ident:J
    .restart local v14       #bindGroup:Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;
    .restart local v18       #recipient:Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;
    :cond_a9
    const/4 v2, 0x0

    #@aa
    goto :goto_82

    #@ab
    .line 421
    :cond_ab
    iget-object v2, v14, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->mSpellChecker:Lcom/android/internal/textservice/ISpellCheckerService;
    :try_end_ad
    .catchall {:try_start_a3 .. :try_end_ad} :catchall_34

    #@ad
    if-eqz v2, :cond_91

    #@af
    .line 427
    :try_start_af
    iget-object v2, v14, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->mSpellChecker:Lcom/android/internal/textservice/ISpellCheckerService;

    #@b1
    move-object/from16 v0, v18

    #@b3
    iget-object v3, v0, Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;->mScLocale:Ljava/lang/String;

    #@b5
    move-object/from16 v0, v18

    #@b7
    iget-object v4, v0, Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;->mScListener:Lcom/android/internal/textservice/ISpellCheckerSessionListener;

    #@b9
    move-object/from16 v0, p5

    #@bb
    invoke-interface {v2, v3, v4, v0}, Lcom/android/internal/textservice/ISpellCheckerService;->getISpellCheckerSession(Ljava/lang/String;Lcom/android/internal/textservice/ISpellCheckerSessionListener;Landroid/os/Bundle;)Lcom/android/internal/textservice/ISpellCheckerSession;

    #@be
    move-result-object v19

    #@bf
    .line 430
    .local v19, session:Lcom/android/internal/textservice/ISpellCheckerSession;
    if-eqz v19, :cond_cb

    #@c1
    .line 431
    move-object/from16 v0, p3

    #@c3
    move-object/from16 v1, v19

    #@c5
    invoke-interface {v0, v1}, Lcom/android/internal/textservice/ITextServicesSessionListener;->onServiceConnected(Lcom/android/internal/textservice/ISpellCheckerSession;)V
    :try_end_c8
    .catchall {:try_start_af .. :try_end_c8} :catchall_34
    .catch Landroid/os/RemoteException; {:try_start_af .. :try_end_c8} :catch_cf

    #@c8
    .line 432
    :try_start_c8
    monitor-exit v20
    :try_end_c9
    .catchall {:try_start_c8 .. :try_end_c9} :catchall_34

    #@c9
    goto/16 :goto_6

    #@cb
    .line 437
    :cond_cb
    :try_start_cb
    invoke-virtual {v14}, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->removeAll()V
    :try_end_ce
    .catchall {:try_start_cb .. :try_end_ce} :catchall_34
    .catch Landroid/os/RemoteException; {:try_start_cb .. :try_end_ce} :catch_cf

    #@ce
    goto :goto_91

    #@cf
    .line 439
    .end local v19           #session:Lcom/android/internal/textservice/ISpellCheckerSession;
    :catch_cf
    move-exception v15

    #@d0
    .line 440
    .local v15, e:Landroid/os/RemoteException;
    :try_start_d0
    sget-object v2, Lcom/android/server/TextServicesManagerService;->TAG:Ljava/lang/String;

    #@d2
    new-instance v3, Ljava/lang/StringBuilder;

    #@d4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@d7
    const-string v4, "Exception in getting spell checker session: "

    #@d9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dc
    move-result-object v3

    #@dd
    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@e0
    move-result-object v3

    #@e1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e4
    move-result-object v3

    #@e5
    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@e8
    .line 441
    invoke-virtual {v14}, Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;->removeAll()V

    #@eb
    goto :goto_91

    #@ec
    .line 451
    .end local v14           #bindGroup:Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;
    .end local v15           #e:Landroid/os/RemoteException;
    .end local v18           #recipient:Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;
    .restart local v16       #ident:J
    :catchall_ec
    move-exception v2

    #@ed
    invoke-static/range {v16 .. v17}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@f0
    throw v2
    :try_end_f1
    .catchall {:try_start_d0 .. :try_end_f1} :catchall_34
.end method

.method public isSpellCheckerEnabled()Z
    .registers 3

    #@0
    .prologue
    .line 459
    invoke-direct {p0}, Lcom/android/server/TextServicesManagerService;->calledFromValidUser()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_8

    #@6
    .line 460
    const/4 v0, 0x0

    #@7
    .line 463
    :goto_7
    return v0

    #@8
    .line 462
    :cond_8
    iget-object v1, p0, Lcom/android/server/TextServicesManagerService;->mSpellCheckerMap:Ljava/util/HashMap;

    #@a
    monitor-enter v1

    #@b
    .line 463
    :try_start_b
    invoke-direct {p0}, Lcom/android/server/TextServicesManagerService;->isSpellCheckerEnabledLocked()Z

    #@e
    move-result v0

    #@f
    monitor-exit v1

    #@10
    goto :goto_7

    #@11
    .line 464
    :catchall_11
    move-exception v0

    #@12
    monitor-exit v1
    :try_end_13
    .catchall {:try_start_b .. :try_end_13} :catchall_11

    #@13
    throw v0
.end method

.method public setCurrentSpellChecker(Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "locale"
    .parameter "sciId"

    #@0
    .prologue
    .line 530
    invoke-direct {p0}, Lcom/android/server/TextServicesManagerService;->calledFromValidUser()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_7

    #@6
    .line 543
    :goto_6
    return-void

    #@7
    .line 533
    :cond_7
    iget-object v1, p0, Lcom/android/server/TextServicesManagerService;->mSpellCheckerMap:Ljava/util/HashMap;

    #@9
    monitor-enter v1

    #@a
    .line 534
    :try_start_a
    iget-object v0, p0, Lcom/android/server/TextServicesManagerService;->mContext:Landroid/content/Context;

    #@c
    const-string v2, "android.permission.WRITE_SECURE_SETTINGS"

    #@e
    invoke-virtual {v0, v2}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@11
    move-result v0

    #@12
    if-eqz v0, :cond_1f

    #@14
    .line 537
    new-instance v0, Ljava/lang/SecurityException;

    #@16
    const-string v2, "Requires permission android.permission.WRITE_SECURE_SETTINGS"

    #@18
    invoke-direct {v0, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v0

    #@1c
    .line 542
    :catchall_1c
    move-exception v0

    #@1d
    monitor-exit v1
    :try_end_1e
    .catchall {:try_start_a .. :try_end_1e} :catchall_1c

    #@1e
    throw v0

    #@1f
    .line 541
    :cond_1f
    :try_start_1f
    invoke-direct {p0, p2}, Lcom/android/server/TextServicesManagerService;->setCurrentSpellCheckerLocked(Ljava/lang/String;)V

    #@22
    .line 542
    monitor-exit v1
    :try_end_23
    .catchall {:try_start_1f .. :try_end_23} :catchall_1c

    #@23
    goto :goto_6
.end method

.method public setCurrentSpellCheckerSubtype(Ljava/lang/String;I)V
    .registers 6
    .parameter "locale"
    .parameter "hashCode"

    #@0
    .prologue
    .line 547
    invoke-direct {p0}, Lcom/android/server/TextServicesManagerService;->calledFromValidUser()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_7

    #@6
    .line 560
    :goto_6
    return-void

    #@7
    .line 550
    :cond_7
    iget-object v1, p0, Lcom/android/server/TextServicesManagerService;->mSpellCheckerMap:Ljava/util/HashMap;

    #@9
    monitor-enter v1

    #@a
    .line 551
    :try_start_a
    iget-object v0, p0, Lcom/android/server/TextServicesManagerService;->mContext:Landroid/content/Context;

    #@c
    const-string v2, "android.permission.WRITE_SECURE_SETTINGS"

    #@e
    invoke-virtual {v0, v2}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@11
    move-result v0

    #@12
    if-eqz v0, :cond_1f

    #@14
    .line 554
    new-instance v0, Ljava/lang/SecurityException;

    #@16
    const-string v2, "Requires permission android.permission.WRITE_SECURE_SETTINGS"

    #@18
    invoke-direct {v0, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v0

    #@1c
    .line 559
    :catchall_1c
    move-exception v0

    #@1d
    monitor-exit v1
    :try_end_1e
    .catchall {:try_start_a .. :try_end_1e} :catchall_1c

    #@1e
    throw v0

    #@1f
    .line 558
    :cond_1f
    :try_start_1f
    invoke-direct {p0, p2}, Lcom/android/server/TextServicesManagerService;->setCurrentSpellCheckerSubtypeLocked(I)V

    #@22
    .line 559
    monitor-exit v1
    :try_end_23
    .catchall {:try_start_1f .. :try_end_23} :catchall_1c

    #@23
    goto :goto_6
.end method

.method public setSpellCheckerEnabled(Z)V
    .registers 5
    .parameter "enabled"

    #@0
    .prologue
    .line 564
    invoke-direct {p0}, Lcom/android/server/TextServicesManagerService;->calledFromValidUser()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_7

    #@6
    .line 577
    :goto_6
    return-void

    #@7
    .line 567
    :cond_7
    iget-object v1, p0, Lcom/android/server/TextServicesManagerService;->mSpellCheckerMap:Ljava/util/HashMap;

    #@9
    monitor-enter v1

    #@a
    .line 568
    :try_start_a
    iget-object v0, p0, Lcom/android/server/TextServicesManagerService;->mContext:Landroid/content/Context;

    #@c
    const-string v2, "android.permission.WRITE_SECURE_SETTINGS"

    #@e
    invoke-virtual {v0, v2}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@11
    move-result v0

    #@12
    if-eqz v0, :cond_1f

    #@14
    .line 571
    new-instance v0, Ljava/lang/SecurityException;

    #@16
    const-string v2, "Requires permission android.permission.WRITE_SECURE_SETTINGS"

    #@18
    invoke-direct {v0, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v0

    #@1c
    .line 576
    :catchall_1c
    move-exception v0

    #@1d
    monitor-exit v1
    :try_end_1e
    .catchall {:try_start_a .. :try_end_1e} :catchall_1c

    #@1e
    throw v0

    #@1f
    .line 575
    :cond_1f
    :try_start_1f
    invoke-direct {p0, p1}, Lcom/android/server/TextServicesManagerService;->setSpellCheckerEnabledLocked(Z)V

    #@22
    .line 576
    monitor-exit v1
    :try_end_23
    .catchall {:try_start_1f .. :try_end_23} :catchall_1c

    #@23
    goto :goto_6
.end method

.method public systemReady()V
    .registers 2

    #@0
    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/android/server/TextServicesManagerService;->mSystemReady:Z

    #@2
    if-nez v0, :cond_7

    #@4
    .line 81
    const/4 v0, 0x1

    #@5
    iput-boolean v0, p0, Lcom/android/server/TextServicesManagerService;->mSystemReady:Z

    #@7
    .line 83
    :cond_7
    return-void
.end method
