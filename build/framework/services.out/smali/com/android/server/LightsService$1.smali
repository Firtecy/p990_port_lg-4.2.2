.class Lcom/android/server/LightsService$1;
.super Landroid/os/IHardwareService$Stub;
.source "LightsService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/LightsService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field private static final FLASHLIGHT_FILE:Ljava/lang/String; = "/sys/class/leds/spotlight/brightness"


# instance fields
.field final synthetic this$0:Lcom/android/server/LightsService;


# direct methods
.method constructor <init>(Lcom/android/server/LightsService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 160
    iput-object p1, p0, Lcom/android/server/LightsService$1;->this$0:Lcom/android/server/LightsService;

    #@2
    invoke-direct {p0}, Landroid/os/IHardwareService$Stub;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public getFlashlightEnabled()Z
    .registers 6

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 166
    :try_start_1
    new-instance v1, Ljava/io/FileInputStream;

    #@3
    const-string v4, "/sys/class/leds/spotlight/brightness"

    #@5
    invoke-direct {v1, v4}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    #@8
    .line 167
    .local v1, fis:Ljava/io/FileInputStream;
    invoke-virtual {v1}, Ljava/io/FileInputStream;->read()I

    #@b
    move-result v2

    #@c
    .line 168
    .local v2, result:I
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_f} :catch_15

    #@f
    .line 169
    const/16 v4, 0x30

    #@11
    if-eq v2, v4, :cond_14

    #@13
    const/4 v3, 0x1

    #@14
    .line 171
    .end local v1           #fis:Ljava/io/FileInputStream;
    .end local v2           #result:I
    :cond_14
    :goto_14
    return v3

    #@15
    .line 170
    :catch_15
    move-exception v0

    #@16
    .line 171
    .local v0, e:Ljava/lang/Exception;
    goto :goto_14
.end method

.method public setFlashlightEnabled(Z)V
    .registers 6
    .parameter "on"

    #@0
    .prologue
    .line 176
    iget-object v2, p0, Lcom/android/server/LightsService$1;->this$0:Lcom/android/server/LightsService;

    #@2
    invoke-static {v2}, Lcom/android/server/LightsService;->access$500(Lcom/android/server/LightsService;)Landroid/content/Context;

    #@5
    move-result-object v2

    #@6
    const-string v3, "android.permission.FLASHLIGHT"

    #@8
    invoke-virtual {v2, v3}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@b
    move-result v2

    #@c
    if-eqz v2, :cond_24

    #@e
    iget-object v2, p0, Lcom/android/server/LightsService$1;->this$0:Lcom/android/server/LightsService;

    #@10
    invoke-static {v2}, Lcom/android/server/LightsService;->access$500(Lcom/android/server/LightsService;)Landroid/content/Context;

    #@13
    move-result-object v2

    #@14
    const-string v3, "android.permission.HARDWARE_TEST"

    #@16
    invoke-virtual {v2, v3}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@19
    move-result v2

    #@1a
    if-eqz v2, :cond_24

    #@1c
    .line 180
    new-instance v2, Ljava/lang/SecurityException;

    #@1e
    const-string v3, "Requires FLASHLIGHT or HARDWARE_TEST permission"

    #@20
    invoke-direct {v2, v3}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@23
    throw v2

    #@24
    .line 183
    :cond_24
    :try_start_24
    new-instance v1, Ljava/io/FileOutputStream;

    #@26
    const-string v2, "/sys/class/leds/spotlight/brightness"

    #@28
    invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    #@2b
    .line 184
    .local v1, fos:Ljava/io/FileOutputStream;
    const/4 v2, 0x2

    #@2c
    new-array v0, v2, [B

    #@2e
    .line 185
    .local v0, bytes:[B
    const/4 v3, 0x0

    #@2f
    if-eqz p1, :cond_42

    #@31
    const/16 v2, 0x31

    #@33
    :goto_33
    int-to-byte v2, v2

    #@34
    aput-byte v2, v0, v3

    #@36
    .line 186
    const/4 v2, 0x1

    #@37
    const/16 v3, 0xa

    #@39
    aput-byte v3, v0, v2

    #@3b
    .line 187
    invoke-virtual {v1, v0}, Ljava/io/FileOutputStream;->write([B)V

    #@3e
    .line 188
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_41
    .catch Ljava/lang/Exception; {:try_start_24 .. :try_end_41} :catch_45

    #@41
    .line 192
    .end local v0           #bytes:[B
    .end local v1           #fos:Ljava/io/FileOutputStream;
    :goto_41
    return-void

    #@42
    .line 185
    .restart local v0       #bytes:[B
    .restart local v1       #fos:Ljava/io/FileOutputStream;
    :cond_42
    const/16 v2, 0x30

    #@44
    goto :goto_33

    #@45
    .line 189
    .end local v0           #bytes:[B
    .end local v1           #fos:Ljava/io/FileOutputStream;
    :catch_45
    move-exception v2

    #@46
    goto :goto_41
.end method
