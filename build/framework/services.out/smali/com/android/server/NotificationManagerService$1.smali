.class Lcom/android/server/NotificationManagerService$1;
.super Ljava/lang/Object;
.source "NotificationManagerService.java"

# interfaces
.implements Lcom/android/server/StatusBarManagerService$NotificationCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/NotificationManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/NotificationManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/NotificationManagerService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 479
    iput-object p1, p0, Lcom/android/server/NotificationManagerService$1;->this$0:Lcom/android/server/NotificationManagerService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onClearAll()V
    .registers 3

    #@0
    .prologue
    .line 513
    iget-object v0, p0, Lcom/android/server/NotificationManagerService$1;->this$0:Lcom/android/server/NotificationManagerService;

    #@2
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    #@5
    move-result v1

    #@6
    invoke-virtual {v0, v1}, Lcom/android/server/NotificationManagerService;->cancelAll(I)V

    #@9
    .line 514
    return-void
.end method

.method public onNotificationClear(Ljava/lang/String;Ljava/lang/String;I)V
    .registers 12
    .parameter "pkg"
    .parameter "tag"
    .parameter "id"

    #@0
    .prologue
    .line 527
    iget-object v0, p0, Lcom/android/server/NotificationManagerService$1;->this$0:Lcom/android/server/NotificationManagerService;

    #@2
    const/4 v4, 0x0

    #@3
    const/16 v5, 0x42

    #@5
    const/4 v6, 0x1

    #@6
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    #@9
    move-result v7

    #@a
    move-object v1, p1

    #@b
    move-object v2, p2

    #@c
    move v3, p3

    #@d
    invoke-static/range {v0 .. v7}, Lcom/android/server/NotificationManagerService;->access$400(Lcom/android/server/NotificationManagerService;Ljava/lang/String;Ljava/lang/String;IIIZI)V

    #@10
    .line 530
    return-void
.end method

.method public onNotificationClick(Ljava/lang/String;Ljava/lang/String;I)V
    .registers 12
    .parameter "pkg"
    .parameter "tag"
    .parameter "id"

    #@0
    .prologue
    .line 519
    iget-object v0, p0, Lcom/android/server/NotificationManagerService$1;->this$0:Lcom/android/server/NotificationManagerService;

    #@2
    const/16 v4, 0x10

    #@4
    const/16 v5, 0x40

    #@6
    const/4 v6, 0x0

    #@7
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    #@a
    move-result v7

    #@b
    move-object v1, p1

    #@c
    move-object v2, p2

    #@d
    move v3, p3

    #@e
    invoke-static/range {v0 .. v7}, Lcom/android/server/NotificationManagerService;->access$400(Lcom/android/server/NotificationManagerService;Ljava/lang/String;Ljava/lang/String;IIIZI)V

    #@11
    .line 522
    return-void
.end method

.method public onNotificationError(Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;)V
    .registers 18
    .parameter "pkg"
    .parameter "tag"
    .parameter "id"
    .parameter "uid"
    .parameter "initialPid"
    .parameter "message"

    #@0
    .prologue
    .line 574
    const-string v1, "NotificationService"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "onNotification error pkg="

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    const-string v3, " tag="

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    const-string v3, " id="

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    const-string v3, "; will crashApplication(uid="

    #@27
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    const-string v3, ", pid="

    #@31
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v2

    #@35
    move/from16 v0, p5

    #@37
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v2

    #@3b
    const-string v3, ")"

    #@3d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v2

    #@41
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v2

    #@45
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    .line 578
    iget-object v1, p0, Lcom/android/server/NotificationManagerService$1;->this$0:Lcom/android/server/NotificationManagerService;

    #@4a
    const/4 v5, 0x0

    #@4b
    const/4 v6, 0x0

    #@4c
    const/4 v7, 0x0

    #@4d
    invoke-static {p4}, Landroid/os/UserHandle;->getUserId(I)I

    #@50
    move-result v8

    #@51
    move-object v2, p1

    #@52
    move-object v3, p2

    #@53
    move v4, p3

    #@54
    invoke-static/range {v1 .. v8}, Lcom/android/server/NotificationManagerService;->access$400(Lcom/android/server/NotificationManagerService;Ljava/lang/String;Ljava/lang/String;IIIZI)V

    #@57
    .line 579
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@5a
    move-result-wide v9

    #@5b
    .line 581
    .local v9, ident:J
    :try_start_5b
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@5e
    move-result-object v1

    #@5f
    new-instance v2, Ljava/lang/StringBuilder;

    #@61
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@64
    const-string v3, "Bad notification posted from package "

    #@66
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v2

    #@6a
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v2

    #@6e
    const-string v3, ": "

    #@70
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v2

    #@74
    move-object/from16 v0, p6

    #@76
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v2

    #@7a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7d
    move-result-object v2

    #@7e
    move/from16 v0, p5

    #@80
    invoke-interface {v1, p4, v0, p1, v2}, Landroid/app/IActivityManager;->crashApplication(IILjava/lang/String;Ljava/lang/String;)V
    :try_end_83
    .catch Landroid/os/RemoteException; {:try_start_5b .. :try_end_83} :catch_87

    #@83
    .line 586
    :goto_83
    invoke-static {v9, v10}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@86
    .line 587
    return-void

    #@87
    .line 584
    :catch_87
    move-exception v1

    #@88
    goto :goto_83
.end method

.method public onPanelRevealed()V
    .registers 8

    #@0
    .prologue
    .line 533
    iget-object v3, p0, Lcom/android/server/NotificationManagerService$1;->this$0:Lcom/android/server/NotificationManagerService;

    #@2
    invoke-static {v3}, Lcom/android/server/NotificationManagerService;->access$100(Lcom/android/server/NotificationManagerService;)Ljava/util/ArrayList;

    #@5
    move-result-object v4

    #@6
    monitor-enter v4

    #@7
    .line 535
    :try_start_7
    iget-object v3, p0, Lcom/android/server/NotificationManagerService$1;->this$0:Lcom/android/server/NotificationManagerService;

    #@9
    const/4 v5, 0x0

    #@a
    invoke-static {v3, v5}, Lcom/android/server/NotificationManagerService;->access$502(Lcom/android/server/NotificationManagerService;Lcom/android/server/NotificationManagerService$NotificationRecord;)Lcom/android/server/NotificationManagerService$NotificationRecord;

    #@d
    .line 537
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_10
    .catchall {:try_start_7 .. :try_end_10} :catchall_5c

    #@10
    move-result-wide v0

    #@11
    .line 539
    .local v0, identity:J
    :try_start_11
    iget-object v3, p0, Lcom/android/server/NotificationManagerService$1;->this$0:Lcom/android/server/NotificationManagerService;

    #@13
    invoke-static {v3}, Lcom/android/server/NotificationManagerService;->access$300(Lcom/android/server/NotificationManagerService;)Landroid/media/IAudioService;

    #@16
    move-result-object v3

    #@17
    invoke-interface {v3}, Landroid/media/IAudioService;->getRingtonePlayer()Landroid/media/IRingtonePlayer;

    #@1a
    move-result-object v2

    #@1b
    .line 540
    .local v2, player:Landroid/media/IRingtonePlayer;
    if-eqz v2, :cond_20

    #@1d
    .line 541
    invoke-interface {v2}, Landroid/media/IRingtonePlayer;->stopAsync()V
    :try_end_20
    .catchall {:try_start_11 .. :try_end_20} :catchall_57
    .catch Landroid/os/RemoteException; {:try_start_11 .. :try_end_20} :catch_64

    #@20
    .line 545
    :cond_20
    :try_start_20
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@23
    .line 549
    .end local v2           #player:Landroid/media/IRingtonePlayer;
    :goto_23
    iget-object v3, p0, Lcom/android/server/NotificationManagerService$1;->this$0:Lcom/android/server/NotificationManagerService;

    #@25
    const/4 v5, 0x0

    #@26
    invoke-static {v3, v5}, Lcom/android/server/NotificationManagerService;->access$602(Lcom/android/server/NotificationManagerService;Lcom/android/server/NotificationManagerService$NotificationRecord;)Lcom/android/server/NotificationManagerService$NotificationRecord;

    #@29
    .line 550
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_2c
    .catchall {:try_start_20 .. :try_end_2c} :catchall_5c

    #@2c
    move-result-wide v0

    #@2d
    .line 554
    :try_start_2d
    iget-object v3, p0, Lcom/android/server/NotificationManagerService$1;->this$0:Lcom/android/server/NotificationManagerService;

    #@2f
    iget-object v3, v3, Lcom/android/server/NotificationManagerService;->mVolumeVibrator:Lcom/lge/loader/volumevibrator/IVolumeVibratorLoader;

    #@31
    invoke-interface {v3}, Lcom/lge/loader/volumevibrator/IVolumeVibratorLoader;->cancel()V
    :try_end_34
    .catchall {:try_start_2d .. :try_end_34} :catchall_5f

    #@34
    .line 557
    :try_start_34
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@37
    .line 561
    iget-object v3, p0, Lcom/android/server/NotificationManagerService$1;->this$0:Lcom/android/server/NotificationManagerService;

    #@39
    invoke-static {v3}, Lcom/android/server/NotificationManagerService;->access$700(Lcom/android/server/NotificationManagerService;)Ljava/util/ArrayList;

    #@3c
    move-result-object v3

    #@3d
    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    #@40
    .line 564
    iget-object v3, p0, Lcom/android/server/NotificationManagerService$1;->this$0:Lcom/android/server/NotificationManagerService;

    #@42
    const/4 v5, 0x0

    #@43
    invoke-static {}, Lcom/android/server/NotificationManagerService;->access$800()I

    #@46
    move-result v6

    #@47
    invoke-static {v3, v5, v6}, Lcom/android/server/NotificationManagerService;->access$900(Lcom/android/server/NotificationManagerService;Lcom/android/server/NotificationManagerService$NotificationRecord;I)V

    #@4a
    .line 567
    iget-object v3, p0, Lcom/android/server/NotificationManagerService$1;->this$0:Lcom/android/server/NotificationManagerService;

    #@4c
    const/4 v5, 0x0

    #@4d
    invoke-static {v3, v5}, Lcom/android/server/NotificationManagerService;->access$1002(Lcom/android/server/NotificationManagerService;Lcom/android/server/NotificationManagerService$NotificationRecord;)Lcom/android/server/NotificationManagerService$NotificationRecord;

    #@50
    .line 568
    iget-object v3, p0, Lcom/android/server/NotificationManagerService$1;->this$0:Lcom/android/server/NotificationManagerService;

    #@52
    invoke-static {v3}, Lcom/android/server/NotificationManagerService;->access$1100(Lcom/android/server/NotificationManagerService;)V

    #@55
    .line 569
    monitor-exit v4

    #@56
    .line 570
    return-void

    #@57
    .line 545
    :catchall_57
    move-exception v3

    #@58
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@5b
    throw v3

    #@5c
    .line 569
    .end local v0           #identity:J
    :catchall_5c
    move-exception v3

    #@5d
    monitor-exit v4
    :try_end_5e
    .catchall {:try_start_34 .. :try_end_5e} :catchall_5c

    #@5e
    throw v3

    #@5f
    .line 557
    .restart local v0       #identity:J
    :catchall_5f
    move-exception v3

    #@60
    :try_start_60
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@63
    throw v3

    #@64
    .line 543
    :catch_64
    move-exception v3

    #@65
    .line 545
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V
    :try_end_68
    .catchall {:try_start_60 .. :try_end_68} :catchall_5c

    #@68
    goto :goto_23
.end method

.method public onSetDisabled(I)V
    .registers 8
    .parameter "status"

    #@0
    .prologue
    .line 482
    iget-object v3, p0, Lcom/android/server/NotificationManagerService$1;->this$0:Lcom/android/server/NotificationManagerService;

    #@2
    invoke-static {v3}, Lcom/android/server/NotificationManagerService;->access$100(Lcom/android/server/NotificationManagerService;)Ljava/util/ArrayList;

    #@5
    move-result-object v4

    #@6
    monitor-enter v4

    #@7
    .line 483
    :try_start_7
    iget-object v3, p0, Lcom/android/server/NotificationManagerService$1;->this$0:Lcom/android/server/NotificationManagerService;

    #@9
    invoke-static {v3, p1}, Lcom/android/server/NotificationManagerService;->access$202(Lcom/android/server/NotificationManagerService;I)I

    #@c
    .line 484
    iget-object v3, p0, Lcom/android/server/NotificationManagerService$1;->this$0:Lcom/android/server/NotificationManagerService;

    #@e
    invoke-static {v3}, Lcom/android/server/NotificationManagerService;->access$200(Lcom/android/server/NotificationManagerService;)I

    #@11
    move-result v3

    #@12
    const/high16 v5, 0x4

    #@14
    and-int/2addr v3, v5

    #@15
    if-eqz v3, :cond_3b

    #@17
    .line 486
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_1a
    .catchall {:try_start_7 .. :try_end_1a} :catchall_42

    #@1a
    move-result-wide v0

    #@1b
    .line 488
    .local v0, identity:J
    :try_start_1b
    iget-object v3, p0, Lcom/android/server/NotificationManagerService$1;->this$0:Lcom/android/server/NotificationManagerService;

    #@1d
    invoke-static {v3}, Lcom/android/server/NotificationManagerService;->access$300(Lcom/android/server/NotificationManagerService;)Landroid/media/IAudioService;

    #@20
    move-result-object v3

    #@21
    invoke-interface {v3}, Landroid/media/IAudioService;->getRingtonePlayer()Landroid/media/IRingtonePlayer;

    #@24
    move-result-object v2

    #@25
    .line 489
    .local v2, player:Landroid/media/IRingtonePlayer;
    if-eqz v2, :cond_2a

    #@27
    .line 490
    invoke-interface {v2}, Landroid/media/IRingtonePlayer;->stopAsync()V
    :try_end_2a
    .catchall {:try_start_1b .. :try_end_2a} :catchall_3d
    .catch Landroid/os/RemoteException; {:try_start_1b .. :try_end_2a} :catch_4a

    #@2a
    .line 494
    :cond_2a
    :try_start_2a
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@2d
    .line 497
    .end local v2           #player:Landroid/media/IRingtonePlayer;
    :goto_2d
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_30
    .catchall {:try_start_2a .. :try_end_30} :catchall_42

    #@30
    move-result-wide v0

    #@31
    .line 501
    :try_start_31
    iget-object v3, p0, Lcom/android/server/NotificationManagerService$1;->this$0:Lcom/android/server/NotificationManagerService;

    #@33
    iget-object v3, v3, Lcom/android/server/NotificationManagerService;->mVolumeVibrator:Lcom/lge/loader/volumevibrator/IVolumeVibratorLoader;

    #@35
    #invoke-interface {v3}, Lcom/lge/loader/volumevibrator/IVolumeVibratorLoader;->cancel()V
    :try_end_38
    .catchall {:try_start_31 .. :try_end_38} :catchall_45

    #@38
    .line 504
    :try_start_38
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@3b
    .line 507
    .end local v0           #identity:J
    :cond_3b
    monitor-exit v4

    #@3c
    .line 508
    return-void

    #@3d
    .line 494
    .restart local v0       #identity:J
    :catchall_3d
    move-exception v3

    #@3e
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@41
    throw v3

    #@42
    .line 507
    .end local v0           #identity:J
    :catchall_42
    move-exception v3

    #@43
    monitor-exit v4
    :try_end_44
    .catchall {:try_start_38 .. :try_end_44} :catchall_42

    #@44
    throw v3

    #@45
    .line 504
    .restart local v0       #identity:J
    :catchall_45
    move-exception v3

    #@46
    :try_start_46
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@49
    throw v3

    #@4a
    .line 492
    :catch_4a
    move-exception v3

    #@4b
    .line 494
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V
    :try_end_4e
    .catchall {:try_start_46 .. :try_end_4e} :catchall_42

    #@4e
    goto :goto_2d
.end method
