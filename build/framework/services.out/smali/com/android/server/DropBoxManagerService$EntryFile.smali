.class final Lcom/android/server/DropBoxManagerService$EntryFile;
.super Ljava/lang/Object;
.source "DropBoxManagerService.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/DropBoxManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "EntryFile"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/android/server/DropBoxManagerService$EntryFile;",
        ">;"
    }
.end annotation


# instance fields
.field public final blocks:I

.field public final file:Ljava/io/File;

.field public final flags:I

.field public final tag:Ljava/lang/String;

.field public final timestampMillis:J


# direct methods
.method public constructor <init>(J)V
    .registers 5
    .parameter "millis"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 567
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 568
    iput-object v1, p0, Lcom/android/server/DropBoxManagerService$EntryFile;->tag:Ljava/lang/String;

    #@6
    .line 569
    iput-wide p1, p0, Lcom/android/server/DropBoxManagerService$EntryFile;->timestampMillis:J

    #@8
    .line 570
    const/4 v0, 0x1

    #@9
    iput v0, p0, Lcom/android/server/DropBoxManagerService$EntryFile;->flags:I

    #@b
    .line 571
    iput-object v1, p0, Lcom/android/server/DropBoxManagerService$EntryFile;->file:Ljava/io/File;

    #@d
    .line 572
    const/4 v0, 0x0

    #@e
    iput v0, p0, Lcom/android/server/DropBoxManagerService$EntryFile;->blocks:I

    #@10
    .line 573
    return-void
.end method

.method public constructor <init>(Ljava/io/File;I)V
    .registers 13
    .parameter "file"
    .parameter "blockSize"

    #@0
    .prologue
    .line 524
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 525
    iput-object p1, p0, Lcom/android/server/DropBoxManagerService$EntryFile;->file:Ljava/io/File;

    #@5
    .line 526
    iget-object v6, p0, Lcom/android/server/DropBoxManagerService$EntryFile;->file:Ljava/io/File;

    #@7
    invoke-virtual {v6}, Ljava/io/File;->length()J

    #@a
    move-result-wide v6

    #@b
    int-to-long v8, p2

    #@c
    add-long/2addr v6, v8

    #@d
    const-wide/16 v8, 0x1

    #@f
    sub-long/2addr v6, v8

    #@10
    int-to-long v8, p2

    #@11
    div-long/2addr v6, v8

    #@12
    long-to-int v6, v6

    #@13
    iput v6, p0, Lcom/android/server/DropBoxManagerService$EntryFile;->blocks:I

    #@15
    .line 528
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    #@18
    move-result-object v5

    #@19
    .line 529
    .local v5, name:Ljava/lang/String;
    const/16 v6, 0x40

    #@1b
    invoke-virtual {v5, v6}, Ljava/lang/String;->lastIndexOf(I)I

    #@1e
    move-result v0

    #@1f
    .line 530
    .local v0, at:I
    if-gez v0, :cond_2c

    #@21
    .line 531
    const/4 v6, 0x0

    #@22
    iput-object v6, p0, Lcom/android/server/DropBoxManagerService$EntryFile;->tag:Ljava/lang/String;

    #@24
    .line 532
    const-wide/16 v6, 0x0

    #@26
    iput-wide v6, p0, Lcom/android/server/DropBoxManagerService$EntryFile;->timestampMillis:J

    #@28
    .line 533
    const/4 v6, 0x1

    #@29
    iput v6, p0, Lcom/android/server/DropBoxManagerService$EntryFile;->flags:I

    #@2b
    .line 561
    :goto_2b
    return-void

    #@2c
    .line 537
    :cond_2c
    const/4 v2, 0x0

    #@2d
    .line 538
    .local v2, flags:I
    const/4 v6, 0x0

    #@2e
    invoke-virtual {v5, v6, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@31
    move-result-object v6

    #@32
    invoke-static {v6}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    #@35
    move-result-object v6

    #@36
    iput-object v6, p0, Lcom/android/server/DropBoxManagerService$EntryFile;->tag:Ljava/lang/String;

    #@38
    .line 539
    const-string v6, ".gz"

    #@3a
    invoke-virtual {v5, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@3d
    move-result v6

    #@3e
    if-eqz v6, :cond_4d

    #@40
    .line 540
    or-int/lit8 v2, v2, 0x4

    #@42
    .line 541
    const/4 v6, 0x0

    #@43
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    #@46
    move-result v7

    #@47
    add-int/lit8 v7, v7, -0x3

    #@49
    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@4c
    move-result-object v5

    #@4d
    .line 543
    :cond_4d
    const-string v6, ".lost"

    #@4f
    invoke-virtual {v5, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@52
    move-result v6

    #@53
    if-eqz v6, :cond_70

    #@55
    .line 544
    or-int/lit8 v2, v2, 0x1

    #@57
    .line 545
    add-int/lit8 v6, v0, 0x1

    #@59
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    #@5c
    move-result v7

    #@5d
    add-int/lit8 v7, v7, -0x5

    #@5f
    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@62
    move-result-object v5

    #@63
    .line 556
    :goto_63
    iput v2, p0, Lcom/android/server/DropBoxManagerService$EntryFile;->flags:I

    #@65
    .line 559
    :try_start_65
    invoke-static {v5}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    #@68
    move-result-object v6

    #@69
    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J
    :try_end_6c
    .catch Ljava/lang/NumberFormatException; {:try_start_65 .. :try_end_6c} :catch_a4

    #@6c
    move-result-wide v3

    #@6d
    .line 560
    .local v3, millis:J
    :goto_6d
    iput-wide v3, p0, Lcom/android/server/DropBoxManagerService$EntryFile;->timestampMillis:J

    #@6f
    goto :goto_2b

    #@70
    .line 546
    .end local v3           #millis:J
    :cond_70
    const-string v6, ".txt"

    #@72
    invoke-virtual {v5, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@75
    move-result v6

    #@76
    if-eqz v6, :cond_87

    #@78
    .line 547
    or-int/lit8 v2, v2, 0x2

    #@7a
    .line 548
    add-int/lit8 v6, v0, 0x1

    #@7c
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    #@7f
    move-result v7

    #@80
    add-int/lit8 v7, v7, -0x4

    #@82
    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@85
    move-result-object v5

    #@86
    goto :goto_63

    #@87
    .line 549
    :cond_87
    const-string v6, ".dat"

    #@89
    invoke-virtual {v5, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@8c
    move-result v6

    #@8d
    if-eqz v6, :cond_9c

    #@8f
    .line 550
    add-int/lit8 v6, v0, 0x1

    #@91
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    #@94
    move-result v7

    #@95
    add-int/lit8 v7, v7, -0x4

    #@97
    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@9a
    move-result-object v5

    #@9b
    goto :goto_63

    #@9c
    .line 552
    :cond_9c
    const/4 v6, 0x1

    #@9d
    iput v6, p0, Lcom/android/server/DropBoxManagerService$EntryFile;->flags:I

    #@9f
    .line 553
    const-wide/16 v6, 0x0

    #@a1
    iput-wide v6, p0, Lcom/android/server/DropBoxManagerService$EntryFile;->timestampMillis:J

    #@a3
    goto :goto_2b

    #@a4
    .line 559
    :catch_a4
    move-exception v1

    #@a5
    .local v1, e:Ljava/lang/NumberFormatException;
    const-wide/16 v3, 0x0

    #@a7
    .restart local v3       #millis:J
    goto :goto_6d
.end method

.method public constructor <init>(Ljava/io/File;Ljava/io/File;Ljava/lang/String;JII)V
    .registers 12
    .parameter "temp"
    .parameter "dir"
    .parameter "tag"
    .parameter "timestampMillis"
    .parameter "flags"
    .parameter "blockSize"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 487
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 488
    and-int/lit8 v0, p6, 0x1

    #@5
    if-eqz v0, :cond_d

    #@7
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@9
    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    #@c
    throw v0

    #@d
    .line 490
    :cond_d
    iput-object p3, p0, Lcom/android/server/DropBoxManagerService$EntryFile;->tag:Ljava/lang/String;

    #@f
    .line 491
    iput-wide p4, p0, Lcom/android/server/DropBoxManagerService$EntryFile;->timestampMillis:J

    #@11
    .line 492
    iput p6, p0, Lcom/android/server/DropBoxManagerService$EntryFile;->flags:I

    #@13
    .line 493
    new-instance v1, Ljava/io/File;

    #@15
    new-instance v0, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    invoke-static {p3}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v0

    #@22
    const-string v2, "@"

    #@24
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v0

    #@28
    invoke-virtual {v0, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v2

    #@2c
    and-int/lit8 v0, p6, 0x2

    #@2e
    if-eqz v0, :cond_76

    #@30
    const-string v0, ".txt"

    #@32
    :goto_32
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v2

    #@36
    and-int/lit8 v0, p6, 0x4

    #@38
    if-eqz v0, :cond_79

    #@3a
    const-string v0, ".gz"

    #@3c
    :goto_3c
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v0

    #@40
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v0

    #@44
    invoke-direct {v1, p2, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@47
    iput-object v1, p0, Lcom/android/server/DropBoxManagerService$EntryFile;->file:Ljava/io/File;

    #@49
    .line 497
    iget-object v0, p0, Lcom/android/server/DropBoxManagerService$EntryFile;->file:Ljava/io/File;

    #@4b
    invoke-virtual {p1, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    #@4e
    move-result v0

    #@4f
    if-nez v0, :cond_7c

    #@51
    .line 498
    new-instance v0, Ljava/io/IOException;

    #@53
    new-instance v1, Ljava/lang/StringBuilder;

    #@55
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@58
    const-string v2, "Can\'t rename "

    #@5a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v1

    #@5e
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v1

    #@62
    const-string v2, " to "

    #@64
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v1

    #@68
    iget-object v2, p0, Lcom/android/server/DropBoxManagerService$EntryFile;->file:Ljava/io/File;

    #@6a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v1

    #@6e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@71
    move-result-object v1

    #@72
    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@75
    throw v0

    #@76
    .line 493
    :cond_76
    const-string v0, ".dat"

    #@78
    goto :goto_32

    #@79
    :cond_79
    const-string v0, ""

    #@7b
    goto :goto_3c

    #@7c
    .line 500
    :cond_7c
    iget-object v0, p0, Lcom/android/server/DropBoxManagerService$EntryFile;->file:Ljava/io/File;

    #@7e
    invoke-virtual {v0}, Ljava/io/File;->length()J

    #@81
    move-result-wide v0

    #@82
    int-to-long v2, p7

    #@83
    add-long/2addr v0, v2

    #@84
    const-wide/16 v2, 0x1

    #@86
    sub-long/2addr v0, v2

    #@87
    int-to-long v2, p7

    #@88
    div-long/2addr v0, v2

    #@89
    long-to-int v0, v0

    #@8a
    iput v0, p0, Lcom/android/server/DropBoxManagerService$EntryFile;->blocks:I

    #@8c
    .line 501
    return-void
.end method

.method public constructor <init>(Ljava/io/File;Ljava/lang/String;J)V
    .registers 8
    .parameter "dir"
    .parameter "tag"
    .parameter "timestampMillis"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 510
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 511
    iput-object p2, p0, Lcom/android/server/DropBoxManagerService$EntryFile;->tag:Ljava/lang/String;

    #@5
    .line 512
    iput-wide p3, p0, Lcom/android/server/DropBoxManagerService$EntryFile;->timestampMillis:J

    #@7
    .line 513
    const/4 v0, 0x1

    #@8
    iput v0, p0, Lcom/android/server/DropBoxManagerService$EntryFile;->flags:I

    #@a
    .line 514
    new-instance v0, Ljava/io/File;

    #@c
    new-instance v1, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    invoke-static {p2}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    const-string v2, "@"

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    const-string v2, ".lost"

    #@25
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v1

    #@29
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v1

    #@2d
    invoke-direct {v0, p1, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@30
    iput-object v0, p0, Lcom/android/server/DropBoxManagerService$EntryFile;->file:Ljava/io/File;

    #@32
    .line 515
    const/4 v0, 0x0

    #@33
    iput v0, p0, Lcom/android/server/DropBoxManagerService$EntryFile;->blocks:I

    #@35
    .line 516
    new-instance v0, Ljava/io/FileOutputStream;

    #@37
    iget-object v1, p0, Lcom/android/server/DropBoxManagerService$EntryFile;->file:Ljava/io/File;

    #@39
    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    #@3c
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V

    #@3f
    .line 517
    return-void
.end method


# virtual methods
.method public final compareTo(Lcom/android/server/DropBoxManagerService$EntryFile;)I
    .registers 9
    .parameter "o"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    const/4 v0, -0x1

    #@3
    .line 465
    iget-wide v3, p0, Lcom/android/server/DropBoxManagerService$EntryFile;->timestampMillis:J

    #@5
    iget-wide v5, p1, Lcom/android/server/DropBoxManagerService$EntryFile;->timestampMillis:J

    #@7
    cmp-long v3, v3, v5

    #@9
    if-gez v3, :cond_c

    #@b
    .line 473
    :cond_b
    :goto_b
    return v0

    #@c
    .line 466
    :cond_c
    iget-wide v3, p0, Lcom/android/server/DropBoxManagerService$EntryFile;->timestampMillis:J

    #@e
    iget-wide v5, p1, Lcom/android/server/DropBoxManagerService$EntryFile;->timestampMillis:J

    #@10
    cmp-long v3, v3, v5

    #@12
    if-lez v3, :cond_16

    #@14
    move v0, v1

    #@15
    goto :goto_b

    #@16
    .line 467
    :cond_16
    iget-object v3, p0, Lcom/android/server/DropBoxManagerService$EntryFile;->file:Ljava/io/File;

    #@18
    if-eqz v3, :cond_27

    #@1a
    iget-object v3, p1, Lcom/android/server/DropBoxManagerService$EntryFile;->file:Ljava/io/File;

    #@1c
    if-eqz v3, :cond_27

    #@1e
    iget-object v0, p0, Lcom/android/server/DropBoxManagerService$EntryFile;->file:Ljava/io/File;

    #@20
    iget-object v1, p1, Lcom/android/server/DropBoxManagerService$EntryFile;->file:Ljava/io/File;

    #@22
    invoke-virtual {v0, v1}, Ljava/io/File;->compareTo(Ljava/io/File;)I

    #@25
    move-result v0

    #@26
    goto :goto_b

    #@27
    .line 468
    :cond_27
    iget-object v3, p1, Lcom/android/server/DropBoxManagerService$EntryFile;->file:Ljava/io/File;

    #@29
    if-nez v3, :cond_b

    #@2b
    .line 469
    iget-object v3, p0, Lcom/android/server/DropBoxManagerService$EntryFile;->file:Ljava/io/File;

    #@2d
    if-eqz v3, :cond_31

    #@2f
    move v0, v1

    #@30
    goto :goto_b

    #@31
    .line 470
    :cond_31
    if-ne p0, p1, :cond_35

    #@33
    move v0, v2

    #@34
    goto :goto_b

    #@35
    .line 471
    :cond_35
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    #@38
    move-result v3

    #@39
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    #@3c
    move-result v4

    #@3d
    if-lt v3, v4, :cond_b

    #@3f
    .line 472
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    #@42
    move-result v0

    #@43
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    #@46
    move-result v3

    #@47
    if-le v0, v3, :cond_4b

    #@49
    move v0, v1

    #@4a
    goto :goto_b

    #@4b
    :cond_4b
    move v0, v2

    #@4c
    .line 473
    goto :goto_b
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 456
    check-cast p1, Lcom/android/server/DropBoxManagerService$EntryFile;

    #@2
    .end local p1
    invoke-virtual {p0, p1}, Lcom/android/server/DropBoxManagerService$EntryFile;->compareTo(Lcom/android/server/DropBoxManagerService$EntryFile;)I

    #@5
    move-result v0

    #@6
    return v0
.end method
