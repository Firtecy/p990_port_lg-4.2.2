.class Lcom/android/server/VibratorService$4;
.super Landroid/content/BroadcastReceiver;
.source "VibratorService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/VibratorService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/VibratorService;


# direct methods
.method constructor <init>(Lcom/android/server/VibratorService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 524
    iput-object p1, p0, Lcom/android/server/VibratorService$4;->this$0:Lcom/android/server/VibratorService;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 8
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 526
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v2

    #@4
    const-string v3, "android.intent.action.SCREEN_OFF"

    #@6
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_43

    #@c
    .line 527
    iget-object v2, p0, Lcom/android/server/VibratorService$4;->this$0:Lcom/android/server/VibratorService;

    #@e
    invoke-static {v2}, Lcom/android/server/VibratorService;->access$000(Lcom/android/server/VibratorService;)Ljava/util/LinkedList;

    #@11
    move-result-object v3

    #@12
    monitor-enter v3

    #@13
    .line 528
    :try_start_13
    iget-object v2, p0, Lcom/android/server/VibratorService$4;->this$0:Lcom/android/server/VibratorService;

    #@15
    invoke-static {v2}, Lcom/android/server/VibratorService;->access$200(Lcom/android/server/VibratorService;)V

    #@18
    .line 530
    iget-object v2, p0, Lcom/android/server/VibratorService$4;->this$0:Lcom/android/server/VibratorService;

    #@1a
    invoke-static {v2}, Lcom/android/server/VibratorService;->access$000(Lcom/android/server/VibratorService;)Ljava/util/LinkedList;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    #@21
    move-result v1

    #@22
    .line 531
    .local v1, size:I
    const/4 v0, 0x0

    #@23
    .local v0, i:I
    :goto_23
    if-ge v0, v1, :cond_39

    #@25
    .line 532
    iget-object v4, p0, Lcom/android/server/VibratorService$4;->this$0:Lcom/android/server/VibratorService;

    #@27
    iget-object v2, p0, Lcom/android/server/VibratorService$4;->this$0:Lcom/android/server/VibratorService;

    #@29
    invoke-static {v2}, Lcom/android/server/VibratorService;->access$000(Lcom/android/server/VibratorService;)Ljava/util/LinkedList;

    #@2c
    move-result-object v2

    #@2d
    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    #@30
    move-result-object v2

    #@31
    check-cast v2, Lcom/android/server/VibratorService$Vibration;

    #@33
    invoke-static {v4, v2}, Lcom/android/server/VibratorService;->access$1300(Lcom/android/server/VibratorService;Lcom/android/server/VibratorService$Vibration;)V

    #@36
    .line 531
    add-int/lit8 v0, v0, 0x1

    #@38
    goto :goto_23

    #@39
    .line 535
    :cond_39
    iget-object v2, p0, Lcom/android/server/VibratorService$4;->this$0:Lcom/android/server/VibratorService;

    #@3b
    invoke-static {v2}, Lcom/android/server/VibratorService;->access$000(Lcom/android/server/VibratorService;)Ljava/util/LinkedList;

    #@3e
    move-result-object v2

    #@3f
    invoke-virtual {v2}, Ljava/util/LinkedList;->clear()V

    #@42
    .line 536
    monitor-exit v3

    #@43
    .line 538
    .end local v0           #i:I
    .end local v1           #size:I
    :cond_43
    return-void

    #@44
    .line 536
    :catchall_44
    move-exception v2

    #@45
    monitor-exit v3
    :try_end_46
    .catchall {:try_start_13 .. :try_end_46} :catchall_44

    #@46
    throw v2
.end method
