.class Lcom/android/server/ConnectivityService$RadioAttributes;
.super Ljava/lang/Object;
.source "ConnectivityService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/ConnectivityService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RadioAttributes"
.end annotation


# instance fields
.field public mSimultaneity:I

.field public mType:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 4
    .parameter "init"

    #@0
    .prologue
    .line 509
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 510
    const-string v1, ","

    #@5
    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    .line 511
    .local v0, fragments:[Ljava/lang/String;
    const/4 v1, 0x0

    #@a
    aget-object v1, v0, v1

    #@c
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@f
    move-result v1

    #@10
    iput v1, p0, Lcom/android/server/ConnectivityService$RadioAttributes;->mType:I

    #@12
    .line 512
    const/4 v1, 0x1

    #@13
    aget-object v1, v0, v1

    #@15
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@18
    move-result v1

    #@19
    iput v1, p0, Lcom/android/server/ConnectivityService$RadioAttributes;->mSimultaneity:I

    #@1b
    .line 513
    return-void
.end method
