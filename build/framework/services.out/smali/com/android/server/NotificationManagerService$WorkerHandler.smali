.class final Lcom/android/server/NotificationManagerService$WorkerHandler;
.super Landroid/os/Handler;
.source "NotificationManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/NotificationManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "WorkerHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/NotificationManagerService;


# direct methods
.method private constructor <init>(Lcom/android/server/NotificationManagerService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1059
    iput-object p1, p0, Lcom/android/server/NotificationManagerService$WorkerHandler;->this$0:Lcom/android/server/NotificationManagerService;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/NotificationManagerService;Lcom/android/server/NotificationManagerService$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1059
    invoke-direct {p0, p1}, Lcom/android/server/NotificationManagerService$WorkerHandler;-><init>(Lcom/android/server/NotificationManagerService;)V

    #@3
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 4
    .parameter "msg"

    #@0
    .prologue
    .line 1064
    iget v0, p1, Landroid/os/Message;->what:I

    #@2
    sparse-switch v0, :sswitch_data_2e

    #@5
    .line 1079
    :cond_5
    :goto_5
    return-void

    #@6
    .line 1067
    :sswitch_6
    iget-object v1, p0, Lcom/android/server/NotificationManagerService$WorkerHandler;->this$0:Lcom/android/server/NotificationManagerService;

    #@8
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@a
    check-cast v0, Lcom/android/server/NotificationManagerService$ToastRecord;

    #@c
    invoke-static {v1, v0}, Lcom/android/server/NotificationManagerService;->access$2500(Lcom/android/server/NotificationManagerService;Lcom/android/server/NotificationManagerService$ToastRecord;)V

    #@f
    goto :goto_5

    #@10
    .line 1071
    :sswitch_10
    const-string v0, "NotificationService"

    #@12
    const-string v1, "spc error ShutDown"

    #@14
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@17
    .line 1072
    iget-object v0, p0, Lcom/android/server/NotificationManagerService$WorkerHandler;->this$0:Lcom/android/server/NotificationManagerService;

    #@19
    iget-object v0, v0, Lcom/android/server/NotificationManagerService;->mContext:Landroid/content/Context;

    #@1b
    const/4 v1, 0x0

    #@1c
    invoke-static {v0, v1}, Lcom/android/server/power/ShutdownThread;->shutdown(Landroid/content/Context;Z)V

    #@1f
    .line 1073
    invoke-static {}, Lcom/android/server/NotificationManagerService;->access$2600()Landroid/app/AlertDialog;

    #@22
    move-result-object v0

    #@23
    if-eqz v0, :cond_5

    #@25
    .line 1074
    invoke-static {}, Lcom/android/server/NotificationManagerService;->access$2600()Landroid/app/AlertDialog;

    #@28
    move-result-object v0

    #@29
    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    #@2c
    goto :goto_5

    #@2d
    .line 1064
    nop

    #@2e
    :sswitch_data_2e
    .sparse-switch
        0x2 -> :sswitch_6
        0x7 -> :sswitch_10
    .end sparse-switch
.end method
