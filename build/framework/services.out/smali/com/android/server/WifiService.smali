.class public Lcom/android/server/WifiService;
.super Landroid/net/wifi/IWifiManager$Stub;
.source "WifiService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/WifiService$5;,
        Lcom/android/server/WifiService$NotificationEnabledSettingObserver;,
        Lcom/android/server/WifiService$Multicaster;,
        Lcom/android/server/WifiService$DeathRecipient;,
        Lcom/android/server/WifiService$LockList;,
        Lcom/android/server/WifiService$WifiLock;,
        Lcom/android/server/WifiService$WifiStateMachineHandler;,
        Lcom/android/server/WifiService$AsyncServiceHandler;
    }
.end annotation


# static fields
.field private static final ACTION_DEVICE_IDLE:Ljava/lang/String; = "com.android.server.WifiManager.action.DEVICE_IDLE"

.field private static final DBG:Z = true

.field private static final DEFAULT_IDLE_MS:J = 0xdbba0L

#the value of this static final field might be set in the static constructor
.field private static final ICON_NETWORKS_AVAILABLE:I = 0x0

.field private static final IDLE_REQUEST:I = 0x0

.field private static final NUM_SCANS_BEFORE_ACTUALLY_SCANNING:I = 0x3

.field private static final POLL_TRAFFIC_STATS_INTERVAL_MSECS:I = 0x3e8

.field private static final TAG:Ljava/lang/String; = "WifiService"

#the value of this static final field might be set in the static constructor
.field private static final WFC_FEATURE:Z = false

.field private static final WIFI_DISABLED:I = 0x0

.field private static final WIFI_DISABLED_AFTER_AIRPLANE_ON_DUETO_POWERSAVE_MODE:I = 0x5

.field private static final WIFI_DISABLED_AIRPLANE_ON:I = 0x3

.field private static final WIFI_ENABLED:I = 0x1

.field private static final WIFI_ENABLED_AFTER_DISABLED_AIRPLANE_ON:I = 0x4

.field private static final WIFI_ENABLED_AIRPLANE_OVERRIDE:I = 0x2


# instance fields
.field private final NOTIFICATION_REPEAT_DELAY_MS:J

.field private mAirplaneModeOn:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private mAlarmManager:Landroid/app/AlarmManager;

.field private mAsyncServiceHandler:Lcom/android/server/WifiService$AsyncServiceHandler;

.field private final mBatteryStats:Lcom/android/internal/app/IBatteryStats;

.field private mClients:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/util/AsyncChannel;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mDataActivity:I

.field private mDeviceIdle:Z

.field private mDeviceManager:Lcom/android/server/DeviceManager3LMService;

.field private mEmergencyCallbackMode:Z

.field private mEnableTrafficStatsPoll:Z

.field private mFullHighPerfLocksAcquired:I

.field private mFullHighPerfLocksReleased:I

.field private mFullLocksAcquired:I

.field private mFullLocksReleased:I

.field private mIdleIntent:Landroid/app/PendingIntent;

.field private mInterfaceName:Ljava/lang/String;

.field private mIsReceiverRegistered:Z

.field private final mLocks:Lcom/android/server/WifiService$LockList;

.field private mLowSignal:Z

.field private mLowSignalNWs:I

.field private mMulticastDisabled:I

.field private mMulticastEnabled:I

.field private final mMulticasters:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/server/WifiService$Multicaster;",
            ">;"
        }
    .end annotation
.end field

.field mNetworkInfo:Landroid/net/NetworkInfo;

.field private mNotification:Landroid/app/Notification;

.field private mNotificationEnabled:Z

.field private mNotificationEnabledSettingObserver:Lcom/android/server/WifiService$NotificationEnabledSettingObserver;

.field private mNotificationRepeatTime:J

.field private mNotificationShown:Z

.field private mNumScansSinceNetworkStateChange:I

.field private mPasspointLayoutNotificationShown:Z

.field private mPersistWifiState:Ljava/util/concurrent/atomic/AtomicInteger;

.field private mPluggedType:I

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mRxPkts:J

.field private mScanLocksAcquired:I

.field private mScanLocksReleased:I

.field private mScanWorkSource:Landroid/os/WorkSource;

.field private mScreenOff:Z

.field private final mTmpWorkSource:Landroid/os/WorkSource;

.field private mTrafficStatsPollToken:I

.field private mTxPkts:J

.field private mWiFiAggregationIfaceIface:Lcom/lge/wifi_iface/WiFiAggregationIfaceIface;

.field private mWiFiOffloadingIfaceIface:Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;

.field private mWifiEnabled:Z

.field private mWifiNeedOnE911:Z

.field private mWifiOffDelayIfNotUsed:Lcom/lge/wifi_iface/WifiOffDelayIfNotUsedIface;

.field private mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

.field private final mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

.field private mWifiStateMachineChannel:Lcom/android/internal/util/AsyncChannel;

.field mWifiStateMachineHandler:Lcom/android/server/WifiService$WifiStateMachineHandler;

.field private mWifiWatchdogStateMachine:Landroid/net/wifi/WifiWatchdogStateMachine;

.field private wifiNetworkAvailableSettings:I


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 232
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getOperator()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    const-string v1, "SKT"

    #@6
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_18

    #@c
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getOperator()Ljava/lang/String;

    #@f
    move-result-object v0

    #@10
    const-string v1, "LGU"

    #@12
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@15
    move-result v0

    #@16
    if-eqz v0, :cond_2f

    #@18
    .line 233
    :cond_18
    const v0, 0x2020569

    #@1b
    sput v0, Lcom/android/server/WifiService;->ICON_NETWORKS_AVAILABLE:I

    #@1d
    .line 242
    :goto_1d
    const-string v0, "ims"

    #@1f
    const-string v1, "ro.product.ims"

    #@21
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@28
    move-result v0

    #@29
    if-eqz v0, :cond_35

    #@2b
    .line 243
    const/4 v0, 0x1

    #@2c
    sput-boolean v0, Lcom/android/server/WifiService;->WFC_FEATURE:Z

    #@2e
    .line 246
    :goto_2e
    return-void

    #@2f
    .line 235
    :cond_2f
    const v0, 0x2020568

    #@32
    sput v0, Lcom/android/server/WifiService;->ICON_NETWORKS_AVAILABLE:I

    #@34
    goto :goto_1d

    #@35
    .line 245
    :cond_35
    const/4 v0, 0x0

    #@36
    sput-boolean v0, Lcom/android/server/WifiService;->WFC_FEATURE:Z

    #@38
    goto :goto_2e
.end method

.method constructor <init>(Landroid/content/Context;)V
    .registers 12
    .parameter "context"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    const/4 v8, 0x1

    #@2
    const/4 v7, 0x0

    #@3
    .line 513
    invoke-direct {p0}, Landroid/net/wifi/IWifiManager$Stub;-><init>()V

    #@6
    .line 144
    iput-boolean v7, p0, Lcom/android/server/WifiService;->mEmergencyCallbackMode:Z

    #@8
    .line 147
    new-instance v4, Lcom/android/server/WifiService$LockList;

    #@a
    invoke-direct {v4, p0, v9}, Lcom/android/server/WifiService$LockList;-><init>(Lcom/android/server/WifiService;Lcom/android/server/WifiService$1;)V

    #@d
    iput-object v4, p0, Lcom/android/server/WifiService;->mLocks:Lcom/android/server/WifiService$LockList;

    #@f
    .line 156
    new-instance v4, Ljava/util/ArrayList;

    #@11
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    #@14
    iput-object v4, p0, Lcom/android/server/WifiService;->mMulticasters:Ljava/util/List;

    #@16
    .line 163
    iput-boolean v7, p0, Lcom/android/server/WifiService;->mEnableTrafficStatsPoll:Z

    #@18
    .line 164
    iput v7, p0, Lcom/android/server/WifiService;->mTrafficStatsPollToken:I

    #@1a
    .line 203
    new-instance v4, Ljava/util/concurrent/atomic/AtomicInteger;

    #@1c
    invoke-direct {v4, v7}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    #@1f
    iput-object v4, p0, Lcom/android/server/WifiService;->mPersistWifiState:Ljava/util/concurrent/atomic/AtomicInteger;

    #@21
    .line 205
    new-instance v4, Ljava/util/concurrent/atomic/AtomicBoolean;

    #@23
    invoke-direct {v4, v7}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    #@26
    iput-object v4, p0, Lcom/android/server/WifiService;->mAirplaneModeOn:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@28
    .line 213
    iput-boolean v7, p0, Lcom/android/server/WifiService;->mIsReceiverRegistered:Z

    #@2a
    .line 215
    iput-boolean v7, p0, Lcom/android/server/WifiService;->mPasspointLayoutNotificationShown:Z

    #@2c
    .line 219
    iput-boolean v7, p0, Lcom/android/server/WifiService;->mWifiNeedOnE911:Z

    #@2e
    .line 222
    new-instance v4, Landroid/net/NetworkInfo;

    #@30
    const-string v5, "WIFI"

    #@32
    const-string v6, ""

    #@34
    invoke-direct {v4, v8, v7, v5, v6}, Landroid/net/NetworkInfo;-><init>(IILjava/lang/String;Ljava/lang/String;)V

    #@37
    iput-object v4, p0, Lcom/android/server/WifiService;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@39
    .line 299
    new-instance v4, Ljava/util/ArrayList;

    #@3b
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    #@3e
    iput-object v4, p0, Lcom/android/server/WifiService;->mClients:Ljava/util/List;

    #@40
    .line 302
    iput v8, p0, Lcom/android/server/WifiService;->wifiNetworkAvailableSettings:I

    #@42
    .line 479
    iput-boolean v7, p0, Lcom/android/server/WifiService;->mLowSignal:Z

    #@44
    .line 480
    iput v7, p0, Lcom/android/server/WifiService;->mLowSignalNWs:I

    #@46
    .line 487
    new-instance v4, Landroid/os/WorkSource;

    #@48
    invoke-direct {v4}, Landroid/os/WorkSource;-><init>()V

    #@4b
    iput-object v4, p0, Lcom/android/server/WifiService;->mTmpWorkSource:Landroid/os/WorkSource;

    #@4d
    .line 509
    iput-object v9, p0, Lcom/android/server/WifiService;->mWiFiAggregationIfaceIface:Lcom/lge/wifi_iface/WiFiAggregationIfaceIface;

    #@4f
    .line 1491
    new-instance v4, Lcom/android/server/WifiService$4;

    #@51
    invoke-direct {v4, p0}, Lcom/android/server/WifiService$4;-><init>(Lcom/android/server/WifiService;)V

    #@54
    iput-object v4, p0, Lcom/android/server/WifiService;->mReceiver:Landroid/content/BroadcastReceiver;

    #@56
    .line 514
    iput-object p1, p0, Lcom/android/server/WifiService;->mContext:Landroid/content/Context;

    #@58
    .line 516
    const-string v4, "wifi.interface"

    #@5a
    const-string v5, "wlan0"

    #@5c
    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@5f
    move-result-object v4

    #@60
    iput-object v4, p0, Lcom/android/server/WifiService;->mInterfaceName:Ljava/lang/String;

    #@62
    .line 518
    new-instance v4, Landroid/net/wifi/WifiStateMachine;

    #@64
    iget-object v5, p0, Lcom/android/server/WifiService;->mContext:Landroid/content/Context;

    #@66
    iget-object v6, p0, Lcom/android/server/WifiService;->mInterfaceName:Ljava/lang/String;

    #@68
    invoke-direct {v4, v5, v6}, Landroid/net/wifi/WifiStateMachine;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    #@6b
    iput-object v4, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@6d
    .line 519
    iget-object v4, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@6f
    invoke-virtual {v4, v8}, Landroid/net/wifi/WifiStateMachine;->enableRssiPolling(Z)V

    #@72
    .line 520
    invoke-static {}, Lcom/android/server/am/BatteryStatsService;->getService()Lcom/android/internal/app/IBatteryStats;

    #@75
    move-result-object v4

    #@76
    iput-object v4, p0, Lcom/android/server/WifiService;->mBatteryStats:Lcom/android/internal/app/IBatteryStats;

    #@78
    .line 522
    iget-object v4, p0, Lcom/android/server/WifiService;->mContext:Landroid/content/Context;

    #@7a
    const-string v5, "alarm"

    #@7c
    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@7f
    move-result-object v4

    #@80
    check-cast v4, Landroid/app/AlarmManager;

    #@82
    iput-object v4, p0, Lcom/android/server/WifiService;->mAlarmManager:Landroid/app/AlarmManager;

    #@84
    .line 523
    new-instance v1, Landroid/content/Intent;

    #@86
    const-string v4, "com.android.server.WifiManager.action.DEVICE_IDLE"

    #@88
    invoke-direct {v1, v4, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    #@8b
    .line 524
    .local v1, idleIntent:Landroid/content/Intent;
    iget-object v4, p0, Lcom/android/server/WifiService;->mContext:Landroid/content/Context;

    #@8d
    invoke-static {v4, v7, v1, v7}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@90
    move-result-object v4

    #@91
    iput-object v4, p0, Lcom/android/server/WifiService;->mIdleIntent:Landroid/app/PendingIntent;

    #@93
    .line 525
    sget-boolean v4, Lcom/lge/config/ThreelmMdmConfig;->THREELM_MDM:Z

    #@95
    if-eqz v4, :cond_9d

    #@97
    .line 526
    invoke-static {}, Lcom/android/server/DeviceManager3LMService;->getInstance()Lcom/android/server/DeviceManager3LMService;

    #@9a
    move-result-object v4

    #@9b
    iput-object v4, p0, Lcom/android/server/WifiService;->mDeviceManager:Lcom/android/server/DeviceManager3LMService;

    #@9d
    .line 529
    :cond_9d
    iget-object v4, p0, Lcom/android/server/WifiService;->mContext:Landroid/content/Context;

    #@9f
    new-instance v5, Lcom/android/server/WifiService$1;

    #@a1
    invoke-direct {v5, p0}, Lcom/android/server/WifiService$1;-><init>(Lcom/android/server/WifiService;)V

    #@a4
    new-instance v6, Landroid/content/IntentFilter;

    #@a6
    const-string v7, "android.intent.action.AIRPLANE_MODE"

    #@a8
    invoke-direct {v6, v7}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@ab
    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@ae
    .line 540
    new-instance v0, Landroid/content/IntentFilter;

    #@b0
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@b3
    .line 541
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v4, "android.net.wifi.WIFI_STATE_CHANGED"

    #@b5
    invoke-virtual {v0, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@b8
    .line 542
    const-string v4, "android.net.wifi.STATE_CHANGE"

    #@ba
    invoke-virtual {v0, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@bd
    .line 543
    const-string v4, "android.net.wifi.SCAN_RESULTS"

    #@bf
    invoke-virtual {v0, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@c2
    .line 545
    const-string v4, "android.net.wifi.HS20_AP_EVENT"

    #@c4
    invoke-virtual {v0, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@c7
    .line 546
    const-string v4, "android.net.wifi.HS20_TRY_CONNECTION"

    #@c9
    invoke-virtual {v0, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@cc
    .line 549
    iget-object v4, p0, Lcom/android/server/WifiService;->mContext:Landroid/content/Context;

    #@ce
    new-instance v5, Lcom/android/server/WifiService$2;

    #@d0
    invoke-direct {v5, p0}, Lcom/android/server/WifiService$2;-><init>(Lcom/android/server/WifiService;)V

    #@d3
    invoke-virtual {v4, v5, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@d6
    .line 626
    sget-boolean v4, Lcom/android/server/WifiService;->WFC_FEATURE:Z

    #@d8
    if-eqz v4, :cond_eb

    #@da
    .line 627
    iget-object v4, p0, Lcom/android/server/WifiService;->mContext:Landroid/content/Context;

    #@dc
    new-instance v5, Lcom/android/server/WifiService$3;

    #@de
    invoke-direct {v5, p0}, Lcom/android/server/WifiService$3;-><init>(Lcom/android/server/WifiService;)V

    #@e1
    new-instance v6, Landroid/content/IntentFilter;

    #@e3
    const-string v7, "IMS_LOWSIGNAL"

    #@e5
    invoke-direct {v6, v7}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@e8
    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@eb
    .line 646
    :cond_eb
    new-instance v3, Landroid/os/HandlerThread;

    #@ed
    const-string v4, "WifiService"

    #@ef
    invoke-direct {v3, v4}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    #@f2
    .line 647
    .local v3, wifiThread:Landroid/os/HandlerThread;
    invoke-virtual {v3}, Landroid/os/HandlerThread;->start()V

    #@f5
    .line 648
    new-instance v4, Lcom/android/server/WifiService$AsyncServiceHandler;

    #@f7
    invoke-virtual {v3}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@fa
    move-result-object v5

    #@fb
    invoke-direct {v4, p0, v5}, Lcom/android/server/WifiService$AsyncServiceHandler;-><init>(Lcom/android/server/WifiService;Landroid/os/Looper;)V

    #@fe
    iput-object v4, p0, Lcom/android/server/WifiService;->mAsyncServiceHandler:Lcom/android/server/WifiService$AsyncServiceHandler;

    #@100
    .line 649
    new-instance v4, Lcom/android/server/WifiService$WifiStateMachineHandler;

    #@102
    invoke-virtual {v3}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@105
    move-result-object v5

    #@106
    invoke-direct {v4, p0, v5}, Lcom/android/server/WifiService$WifiStateMachineHandler;-><init>(Lcom/android/server/WifiService;Landroid/os/Looper;)V

    #@109
    iput-object v4, p0, Lcom/android/server/WifiService;->mWifiStateMachineHandler:Lcom/android/server/WifiService$WifiStateMachineHandler;

    #@10b
    .line 652
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@10e
    move-result-object v4

    #@10f
    const-string v5, "wifi_networks_available_repeat_delay"

    #@111
    const/16 v6, 0x384

    #@113
    invoke-static {v4, v5, v6}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@116
    move-result v4

    #@117
    int-to-long v4, v4

    #@118
    const-wide/16 v6, 0x3e8

    #@11a
    mul-long/2addr v4, v6

    #@11b
    iput-wide v4, p0, Lcom/android/server/WifiService;->NOTIFICATION_REPEAT_DELAY_MS:J

    #@11d
    .line 654
    new-instance v4, Lcom/android/server/WifiService$NotificationEnabledSettingObserver;

    #@11f
    new-instance v5, Landroid/os/Handler;

    #@121
    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    #@124
    invoke-direct {v4, p0, v5}, Lcom/android/server/WifiService$NotificationEnabledSettingObserver;-><init>(Lcom/android/server/WifiService;Landroid/os/Handler;)V

    #@127
    iput-object v4, p0, Lcom/android/server/WifiService;->mNotificationEnabledSettingObserver:Lcom/android/server/WifiService$NotificationEnabledSettingObserver;

    #@129
    .line 655
    iget-object v4, p0, Lcom/android/server/WifiService;->mNotificationEnabledSettingObserver:Lcom/android/server/WifiService$NotificationEnabledSettingObserver;

    #@12b
    invoke-virtual {v4}, Lcom/android/server/WifiService$NotificationEnabledSettingObserver;->register()V

    #@12e
    .line 658
    sget-boolean v4, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@130
    if-eqz v4, :cond_192

    #@132
    .line 663
    invoke-static {}, Lcom/lge/wifi_iface/WifiIfaceManager;->getWifiServiceExtIface()Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@135
    move-result-object v4

    #@136
    iput-object v4, p0, Lcom/android/server/WifiService;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@138
    .line 664
    iget-object v4, p0, Lcom/android/server/WifiService;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@13a
    if-eqz v4, :cond_14f

    #@13c
    .line 669
    iget-object v4, p0, Lcom/android/server/WifiService;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@13e
    invoke-interface {v4}, Lcom/lge/wifi_iface/WifiServiceExtIface;->getWifiOffDelayIfNotUsed()Lcom/lge/wifi_iface/WifiOffDelayIfNotUsedIface;

    #@141
    move-result-object v4

    #@142
    iput-object v4, p0, Lcom/android/server/WifiService;->mWifiOffDelayIfNotUsed:Lcom/lge/wifi_iface/WifiOffDelayIfNotUsedIface;

    #@144
    .line 670
    iget-object v4, p0, Lcom/android/server/WifiService;->mWifiOffDelayIfNotUsed:Lcom/lge/wifi_iface/WifiOffDelayIfNotUsedIface;

    #@146
    if-eqz v4, :cond_14f

    #@148
    .line 671
    iget-object v4, p0, Lcom/android/server/WifiService;->mWifiOffDelayIfNotUsed:Lcom/lge/wifi_iface/WifiOffDelayIfNotUsedIface;

    #@14a
    iget-object v5, p0, Lcom/android/server/WifiService;->mIdleIntent:Landroid/app/PendingIntent;

    #@14c
    invoke-interface {v4, v5}, Lcom/lge/wifi_iface/WifiOffDelayIfNotUsedIface;->init(Landroid/app/PendingIntent;)V

    #@14f
    .line 681
    :cond_14f
    :goto_14f
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getOperator()Ljava/lang/String;

    #@152
    move-result-object v4

    #@153
    const-string v5, "VZW"

    #@155
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@158
    move-result v4

    #@159
    if-eqz v4, :cond_197

    #@15b
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getCountry()Ljava/lang/String;

    #@15e
    move-result-object v4

    #@15f
    const-string v5, "US"

    #@161
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@164
    move-result v4

    #@165
    if-eqz v4, :cond_197

    #@167
    .line 682
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@16a
    move-result-object v2

    #@16b
    .line 683
    .local v2, pm_w:Landroid/content/pm/PackageManager;
    const-string v4, "WifiService"

    #@16d
    const-string v5, "captive portal enabled in wifi service"

    #@16f
    invoke-static {v4, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@172
    .line 684
    new-instance v4, Landroid/content/ComponentName;

    #@174
    const-string v5, "com.android.settings"

    #@176
    const-string v6, "com.android.settings.captiveportal.WifiCaptivePortal"

    #@178
    invoke-direct {v4, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@17b
    invoke-virtual {v2, v4, v8, v8}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    #@17e
    .line 688
    const-string v4, "WifiService"

    #@180
    const-string v5, "WiFi E911 request enabled in wifi service"

    #@182
    invoke-static {v4, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@185
    .line 689
    new-instance v4, Landroid/content/ComponentName;

    #@187
    const-string v5, "com.android.settings"

    #@189
    const-string v6, "com.android.settings.wifi.WifiE911Request"

    #@18b
    invoke-direct {v4, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@18e
    invoke-virtual {v2, v4, v8, v8}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    #@191
    .line 701
    :goto_191
    return-void

    #@192
    .line 675
    .end local v2           #pm_w:Landroid/content/pm/PackageManager;
    :cond_192
    iput-object v9, p0, Lcom/android/server/WifiService;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@194
    .line 676
    iput-object v9, p0, Lcom/android/server/WifiService;->mWifiOffDelayIfNotUsed:Lcom/lge/wifi_iface/WifiOffDelayIfNotUsedIface;

    #@196
    goto :goto_14f

    #@197
    .line 693
    :cond_197
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@19a
    move-result-object v2

    #@19b
    .line 694
    .restart local v2       #pm_w:Landroid/content/pm/PackageManager;
    const-string v4, "WifiService"

    #@19d
    const-string v5, "captive portal disabled in wifi service"

    #@19f
    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a2
    .line 695
    new-instance v4, Landroid/content/ComponentName;

    #@1a4
    const-string v5, "com.android.settings"

    #@1a6
    const-string v6, "com.android.settings.captiveportal.WifiCaptivePortal"

    #@1a8
    invoke-direct {v4, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@1ab
    const/4 v5, 0x2

    #@1ac
    invoke-virtual {v2, v4, v5, v8}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    #@1af
    goto :goto_191
.end method

.method static synthetic access$100(Lcom/android/server/WifiService;)Ljava/util/List;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 131
    iget-object v0, p0, Lcom/android/server/WifiService;->mClients:Ljava/util/List;

    #@2
    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/server/WifiService;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 131
    invoke-direct {p0, p1}, Lcom/android/server/WifiService;->handleAirplaneModeToggled(Z)V

    #@3
    return-void
.end method

.method static synthetic access$1100(Lcom/android/server/WifiService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 131
    invoke-direct {p0}, Lcom/android/server/WifiService;->updateWifiState()V

    #@3
    return-void
.end method

.method static synthetic access$1202(Lcom/android/server/WifiService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 131
    iput-boolean p1, p0, Lcom/android/server/WifiService;->mWifiEnabled:Z

    #@2
    return p1
.end method

.method static synthetic access$1300(Lcom/android/server/WifiService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 131
    invoke-direct {p0}, Lcom/android/server/WifiService;->resetNotification()V

    #@3
    return-void
.end method

.method static synthetic access$1400(Lcom/android/server/WifiService;)Lcom/lge/wifi_iface/WifiServiceExtIface;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 131
    iget-object v0, p0, Lcom/android/server/WifiService;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@2
    return-object v0
.end method

.method static synthetic access$1500(Lcom/android/server/WifiService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 131
    invoke-direct {p0}, Lcom/android/server/WifiService;->unregBrdcastReceiver()V

    #@3
    return-void
.end method

.method static synthetic access$1600(Lcom/android/server/WifiService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 131
    iget-boolean v0, p0, Lcom/android/server/WifiService;->mIsReceiverRegistered:Z

    #@2
    return v0
.end method

.method static synthetic access$1602(Lcom/android/server/WifiService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 131
    iput-boolean p1, p0, Lcom/android/server/WifiService;->mIsReceiverRegistered:Z

    #@2
    return p1
.end method

.method static synthetic access$1700(Lcom/android/server/WifiService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 131
    invoke-direct {p0}, Lcom/android/server/WifiService;->registerForBroadcasts()V

    #@3
    return-void
.end method

.method static synthetic access$1800(Lcom/android/server/WifiService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 131
    invoke-direct {p0}, Lcom/android/server/WifiService;->evaluateTrafficStatsPolling()V

    #@3
    return-void
.end method

.method static synthetic access$1900(Lcom/android/server/WifiService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 131
    invoke-direct {p0}, Lcom/android/server/WifiService;->hidePasspointNotification()V

    #@3
    return-void
.end method

.method static synthetic access$200(Lcom/android/server/WifiService;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 131
    iget-object v0, p0, Lcom/android/server/WifiService;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$2000(Lcom/android/server/WifiService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 131
    invoke-direct {p0}, Lcom/android/server/WifiService;->noteScanEnd()V

    #@3
    return-void
.end method

.method static synthetic access$2100(Lcom/android/server/WifiService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 131
    invoke-direct {p0}, Lcom/android/server/WifiService;->checkAndSetNotification()V

    #@3
    return-void
.end method

.method static synthetic access$2200(Lcom/android/server/WifiService;Ljava/lang/String;I)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 131
    invoke-direct {p0, p1, p2}, Lcom/android/server/WifiService;->showPasspointNotification(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method static synthetic access$2300(Lcom/android/server/WifiService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 131
    iget-boolean v0, p0, Lcom/android/server/WifiService;->mLowSignal:Z

    #@2
    return v0
.end method

.method static synthetic access$2302(Lcom/android/server/WifiService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 131
    iput-boolean p1, p0, Lcom/android/server/WifiService;->mLowSignal:Z

    #@2
    return p1
.end method

.method static synthetic access$2400(Lcom/android/server/WifiService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 131
    iget-boolean v0, p0, Lcom/android/server/WifiService;->mNotificationShown:Z

    #@2
    return v0
.end method

.method static synthetic access$2502(Lcom/android/server/WifiService;J)J
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 131
    iput-wide p1, p0, Lcom/android/server/WifiService;->mNotificationRepeatTime:J

    #@2
    return-wide p1
.end method

.method static synthetic access$2600(Lcom/android/server/WifiService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 131
    iget v0, p0, Lcom/android/server/WifiService;->mLowSignalNWs:I

    #@2
    return v0
.end method

.method static synthetic access$2700(Lcom/android/server/WifiService;ZIZI)V
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    #@0
    .prologue
    .line 131
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/WifiService;->setNotificationVisible(ZIZI)V

    #@3
    return-void
.end method

.method static synthetic access$2800(Lcom/android/server/WifiService;)Landroid/app/PendingIntent;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 131
    iget-object v0, p0, Lcom/android/server/WifiService;->mIdleIntent:Landroid/app/PendingIntent;

    #@2
    return-object v0
.end method

.method static synthetic access$2900(Lcom/android/server/WifiService;)Landroid/app/AlarmManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 131
    iget-object v0, p0, Lcom/android/server/WifiService;->mAlarmManager:Landroid/app/AlarmManager;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/android/server/WifiService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 131
    iget-boolean v0, p0, Lcom/android/server/WifiService;->mEnableTrafficStatsPoll:Z

    #@2
    return v0
.end method

.method static synthetic access$3000(Lcom/android/server/WifiService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 131
    iget-boolean v0, p0, Lcom/android/server/WifiService;->mScreenOff:Z

    #@2
    return v0
.end method

.method static synthetic access$3002(Lcom/android/server/WifiService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 131
    iput-boolean p1, p0, Lcom/android/server/WifiService;->mScreenOff:Z

    #@2
    return p1
.end method

.method static synthetic access$302(Lcom/android/server/WifiService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 131
    iput-boolean p1, p0, Lcom/android/server/WifiService;->mEnableTrafficStatsPoll:Z

    #@2
    return p1
.end method

.method static synthetic access$3100(Lcom/android/server/WifiService;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 131
    invoke-direct {p0, p1}, Lcom/android/server/WifiService;->setDeviceIdleAndUpdateWifi(Z)V

    #@3
    return-void
.end method

.method static synthetic access$3200(Lcom/android/server/WifiService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 131
    iget v0, p0, Lcom/android/server/WifiService;->mPluggedType:I

    #@2
    return v0
.end method

.method static synthetic access$3202(Lcom/android/server/WifiService;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 131
    iput p1, p0, Lcom/android/server/WifiService;->mPluggedType:I

    #@2
    return p1
.end method

.method static synthetic access$3300(Lcom/android/server/WifiService;)Lcom/lge/wifi_iface/WifiOffDelayIfNotUsedIface;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 131
    iget-object v0, p0, Lcom/android/server/WifiService;->mWifiOffDelayIfNotUsed:Lcom/lge/wifi_iface/WifiOffDelayIfNotUsedIface;

    #@2
    return-object v0
.end method

.method static synthetic access$3402(Lcom/android/server/WifiService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 131
    iput-boolean p1, p0, Lcom/android/server/WifiService;->mEmergencyCallbackMode:Z

    #@2
    return p1
.end method

.method static synthetic access$3900(Lcom/android/server/WifiService;)Lcom/android/server/WifiService$LockList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 131
    iget-object v0, p0, Lcom/android/server/WifiService;->mLocks:Lcom/android/server/WifiService$LockList;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Lcom/android/server/WifiService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 131
    iget v0, p0, Lcom/android/server/WifiService;->mTrafficStatsPollToken:I

    #@2
    return v0
.end method

.method static synthetic access$4000(Lcom/android/server/WifiService;Landroid/os/IBinder;)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 131
    invoke-direct {p0, p1}, Lcom/android/server/WifiService;->releaseWifiLockLocked(Landroid/os/IBinder;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$408(Lcom/android/server/WifiService;)I
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 131
    iget v0, p0, Lcom/android/server/WifiService;->mTrafficStatsPollToken:I

    #@2
    add-int/lit8 v1, v0, 0x1

    #@4
    iput v1, p0, Lcom/android/server/WifiService;->mTrafficStatsPollToken:I

    #@6
    return v0
.end method

.method static synthetic access$4100(Lcom/android/server/WifiService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 131
    iget v0, p0, Lcom/android/server/WifiService;->mFullHighPerfLocksAcquired:I

    #@2
    return v0
.end method

.method static synthetic access$4200(Lcom/android/server/WifiService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 131
    iget v0, p0, Lcom/android/server/WifiService;->mFullHighPerfLocksReleased:I

    #@2
    return v0
.end method

.method static synthetic access$4300(Lcom/android/server/WifiService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 131
    iget v0, p0, Lcom/android/server/WifiService;->mFullLocksAcquired:I

    #@2
    return v0
.end method

.method static synthetic access$4400(Lcom/android/server/WifiService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 131
    iget v0, p0, Lcom/android/server/WifiService;->mFullLocksReleased:I

    #@2
    return v0
.end method

.method static synthetic access$4800(Lcom/android/server/WifiService;)Ljava/util/List;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 131
    iget-object v0, p0, Lcom/android/server/WifiService;->mMulticasters:Ljava/util/List;

    #@2
    return-object v0
.end method

.method static synthetic access$4900(Lcom/android/server/WifiService;II)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 131
    invoke-direct {p0, p1, p2}, Lcom/android/server/WifiService;->removeMulticasterLocked(II)V

    #@3
    return-void
.end method

.method static synthetic access$500(Lcom/android/server/WifiService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 131
    invoke-direct {p0}, Lcom/android/server/WifiService;->notifyOnDataActivity()V

    #@3
    return-void
.end method

.method static synthetic access$5002(Lcom/android/server/WifiService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 131
    iput-boolean p1, p0, Lcom/android/server/WifiService;->mNotificationEnabled:Z

    #@2
    return p1
.end method

.method static synthetic access$600(Lcom/android/server/WifiService;)Landroid/net/wifi/WifiStateMachine;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 131
    iget-object v0, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@2
    return-object v0
.end method

.method static synthetic access$702(Lcom/android/server/WifiService;Lcom/android/internal/util/AsyncChannel;)Lcom/android/internal/util/AsyncChannel;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 131
    iput-object p1, p0, Lcom/android/server/WifiService;->mWifiStateMachineChannel:Lcom/android/internal/util/AsyncChannel;

    #@2
    return-object p1
.end method

.method static synthetic access$800(Lcom/android/server/WifiService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 131
    invoke-direct {p0}, Lcom/android/server/WifiService;->isAirplaneModeOn()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$900(Lcom/android/server/WifiService;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 131
    iget-object v0, p0, Lcom/android/server/WifiService;->mAirplaneModeOn:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@2
    return-object v0
.end method

.method private acquireWifiLockLocked(Lcom/android/server/WifiService$WifiLock;)Z
    .registers 8
    .parameter "wifiLock"

    #@0
    .prologue
    .line 1949
    const-string v3, "WifiService"

    #@2
    new-instance v4, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v5, "acquireWifiLockLocked: "

    #@9
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v4

    #@d
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v4

    #@11
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v4

    #@15
    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1951
    iget-object v3, p0, Lcom/android/server/WifiService;->mLocks:Lcom/android/server/WifiService$LockList;

    #@1a
    invoke-static {v3, p1}, Lcom/android/server/WifiService$LockList;->access$4500(Lcom/android/server/WifiService$LockList;Lcom/android/server/WifiService$WifiLock;)V

    #@1d
    .line 1953
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@20
    move-result-wide v1

    #@21
    .line 1955
    .local v1, ident:J
    :try_start_21
    invoke-direct {p0, p1}, Lcom/android/server/WifiService;->noteAcquireWifiLock(Lcom/android/server/WifiService$WifiLock;)V

    #@24
    .line 1956
    iget v3, p1, Lcom/android/server/WifiService$DeathRecipient;->mMode:I

    #@26
    packed-switch v3, :pswitch_data_64

    #@29
    .line 1971
    :goto_29
    invoke-direct {p0}, Lcom/android/server/WifiService;->reportStartWorkSource()V

    #@2c
    .line 1977
    sget-boolean v3, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@2e
    if-eqz v3, :cond_3f

    #@30
    .line 1978
    iget-object v3, p0, Lcom/android/server/WifiService;->mWifiOffDelayIfNotUsed:Lcom/lge/wifi_iface/WifiOffDelayIfNotUsedIface;

    #@32
    if-eqz v3, :cond_3f

    #@34
    .line 1980
    iget-object v3, p0, Lcom/android/server/WifiService;->mWifiOffDelayIfNotUsed:Lcom/lge/wifi_iface/WifiOffDelayIfNotUsedIface;

    #@36
    iget-object v4, p0, Lcom/android/server/WifiService;->mLocks:Lcom/android/server/WifiService$LockList;

    #@38
    invoke-static {v4}, Lcom/android/server/WifiService$LockList;->access$3600(Lcom/android/server/WifiService$LockList;)Z

    #@3b
    move-result v4

    #@3c
    invoke-interface {v3, v4}, Lcom/lge/wifi_iface/WifiOffDelayIfNotUsedIface;->setHasWifiLocks(Z)V

    #@3f
    .line 1984
    :cond_3f
    invoke-direct {p0}, Lcom/android/server/WifiService;->updateWifiState()V
    :try_end_42
    .catchall {:try_start_21 .. :try_end_42} :catchall_58
    .catch Landroid/os/RemoteException; {:try_start_21 .. :try_end_42} :catch_4e

    #@42
    .line 1985
    const/4 v3, 0x1

    #@43
    .line 1989
    :goto_43
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@46
    .line 1987
    return v3

    #@47
    .line 1958
    :pswitch_47
    :try_start_47
    iget v3, p0, Lcom/android/server/WifiService;->mFullLocksAcquired:I

    #@49
    add-int/lit8 v3, v3, 0x1

    #@4b
    iput v3, p0, Lcom/android/server/WifiService;->mFullLocksAcquired:I

    #@4d
    goto :goto_29

    #@4e
    .line 1986
    :catch_4e
    move-exception v0

    #@4f
    .line 1987
    .local v0, e:Landroid/os/RemoteException;
    const/4 v3, 0x0

    #@50
    goto :goto_43

    #@51
    .line 1961
    .end local v0           #e:Landroid/os/RemoteException;
    :pswitch_51
    iget v3, p0, Lcom/android/server/WifiService;->mFullHighPerfLocksAcquired:I

    #@53
    add-int/lit8 v3, v3, 0x1

    #@55
    iput v3, p0, Lcom/android/server/WifiService;->mFullHighPerfLocksAcquired:I
    :try_end_57
    .catchall {:try_start_47 .. :try_end_57} :catchall_58
    .catch Landroid/os/RemoteException; {:try_start_47 .. :try_end_57} :catch_4e

    #@57
    goto :goto_29

    #@58
    .line 1989
    :catchall_58
    move-exception v3

    #@59
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@5c
    throw v3

    #@5d
    .line 1965
    :pswitch_5d
    :try_start_5d
    iget v3, p0, Lcom/android/server/WifiService;->mScanLocksAcquired:I

    #@5f
    add-int/lit8 v3, v3, 0x1

    #@61
    iput v3, p0, Lcom/android/server/WifiService;->mScanLocksAcquired:I
    :try_end_63
    .catchall {:try_start_5d .. :try_end_63} :catchall_58
    .catch Landroid/os/RemoteException; {:try_start_5d .. :try_end_63} :catch_4e

    #@63
    goto :goto_29

    #@64
    .line 1956
    :pswitch_data_64
    .packed-switch 0x1
        :pswitch_47
        :pswitch_5d
        :pswitch_51
    .end packed-switch
.end method

.method private checkAndSetNotification()V
    .registers 13

    #@0
    .prologue
    const/4 v11, 0x3

    #@1
    const/4 v10, 0x1

    #@2
    const/4 v9, 0x0

    #@3
    .line 2272
    iget-boolean v6, p0, Lcom/android/server/WifiService;->mNotificationEnabled:Z

    #@5
    if-nez v6, :cond_27

    #@7
    .line 2273
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getOperator()Ljava/lang/String;

    #@a
    move-result-object v6

    #@b
    const-string v7, "ATT"

    #@d
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10
    move-result v6

    #@11
    if-eqz v6, :cond_26

    #@13
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getCountry()Ljava/lang/String;

    #@16
    move-result-object v6

    #@17
    const-string v7, "US"

    #@19
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1c
    move-result v6

    #@1d
    if-eqz v6, :cond_26

    #@1f
    .line 2274
    const-string v6, "WifiService"

    #@21
    const-string v7, "checkAndSetNotification(), return case - mNotificationEnabled is diabled!"

    #@23
    invoke-static {v6, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 2347
    :cond_26
    :goto_26
    return-void

    #@27
    .line 2278
    :cond_27
    iget-object v6, p0, Lcom/android/server/WifiService;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@29
    invoke-virtual {v6}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    #@2c
    move-result-object v5

    #@2d
    .line 2279
    .local v5, state:Landroid/net/NetworkInfo$State;
    sget-object v6, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    #@2f
    if-eq v5, v6, :cond_35

    #@31
    sget-object v6, Landroid/net/NetworkInfo$State;->UNKNOWN:Landroid/net/NetworkInfo$State;

    #@33
    if-ne v5, v6, :cond_f0

    #@35
    .line 2282
    :cond_35
    iget-object v6, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@37
    invoke-virtual {v6}, Landroid/net/wifi/WifiStateMachine;->syncGetScanResultsList()Ljava/util/List;

    #@3a
    move-result-object v4

    #@3b
    .line 2283
    .local v4, scanResults:Ljava/util/List;,"Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    if-eqz v4, :cond_f0

    #@3d
    .line 2284
    const/4 v1, 0x0

    #@3e
    .line 2287
    .local v1, numOpenNetworks:I
    const/4 v2, 0x0

    #@3f
    .line 2289
    .local v2, numSecuredNetworks:I
    invoke-interface {v4}, Ljava/util/List;->size()I

    #@42
    move-result v6

    #@43
    add-int/lit8 v0, v6, -0x1

    #@45
    .local v0, i:I
    :goto_45
    if-ltz v0, :cond_63

    #@47
    .line 2290
    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@4a
    move-result-object v3

    #@4b
    check-cast v3, Landroid/net/wifi/ScanResult;

    #@4d
    .line 2294
    .local v3, scanResult:Landroid/net/wifi/ScanResult;
    iget-object v6, v3, Landroid/net/wifi/ScanResult;->capabilities:Ljava/lang/String;

    #@4f
    if-eqz v6, :cond_60

    #@51
    iget-object v6, v3, Landroid/net/wifi/ScanResult;->capabilities:Ljava/lang/String;

    #@53
    const-string v7, "[ESS]"

    #@55
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@58
    move-result v6

    #@59
    if-eqz v6, :cond_60

    #@5b
    .line 2296
    add-int/lit8 v1, v1, 0x1

    #@5d
    .line 2289
    :goto_5d
    add-int/lit8 v0, v0, -0x1

    #@5f
    goto :goto_45

    #@60
    .line 2300
    :cond_60
    add-int/lit8 v2, v2, 0x1

    #@62
    goto :goto_5d

    #@63
    .line 2305
    .end local v3           #scanResult:Landroid/net/wifi/ScanResult;
    :cond_63
    sget-boolean v6, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@65
    if-eqz v6, :cond_92

    #@67
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->isWifiChameleonFeaturedCarrier()Z

    #@6a
    move-result v6

    #@6b
    if-eqz v6, :cond_92

    #@6d
    .line 2308
    iget-object v6, p0, Lcom/android/server/WifiService;->mContext:Landroid/content/Context;

    #@6f
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@72
    move-result-object v6

    #@73
    const-string v7, "wifi_networks_available_notification_settings"

    #@75
    invoke-static {v6, v7, v10}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@78
    move-result v6

    #@79
    iput v6, p0, Lcom/android/server/WifiService;->wifiNetworkAvailableSettings:I

    #@7b
    .line 2311
    iget v6, p0, Lcom/android/server/WifiService;->wifiNetworkAvailableSettings:I

    #@7d
    const/4 v7, 0x2

    #@7e
    if-ne v6, v7, :cond_92

    #@80
    .line 2312
    if-gtz v1, :cond_84

    #@82
    if-lez v2, :cond_92

    #@84
    .line 2313
    :cond_84
    iget v6, p0, Lcom/android/server/WifiService;->mNumScansSinceNetworkStateChange:I

    #@86
    add-int/lit8 v6, v6, 0x1

    #@88
    iput v6, p0, Lcom/android/server/WifiService;->mNumScansSinceNetworkStateChange:I

    #@8a
    if-lt v6, v11, :cond_26

    #@8c
    .line 2314
    add-int v6, v1, v2

    #@8e
    invoke-direct {p0, v10, v6, v9, v9}, Lcom/android/server/WifiService;->setNotificationVisible(ZIZI)V

    #@91
    goto :goto_26

    #@92
    .line 2322
    :cond_92
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getOperator()Ljava/lang/String;

    #@95
    move-result-object v6

    #@96
    const-string v7, "ATT"

    #@98
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9b
    move-result v6

    #@9c
    if-eqz v6, :cond_c2

    #@9e
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getCountry()Ljava/lang/String;

    #@a1
    move-result-object v6

    #@a2
    const-string v7, "US"

    #@a4
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a7
    move-result v6

    #@a8
    if-eqz v6, :cond_c2

    #@aa
    .line 2323
    const-string v6, "WifiService"

    #@ac
    new-instance v7, Ljava/lang/StringBuilder;

    #@ae
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@b1
    const-string v8, "checkAndSetNotification(), numOpenNetworks = "

    #@b3
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b6
    move-result-object v7

    #@b7
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v7

    #@bb
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@be
    move-result-object v7

    #@bf
    invoke-static {v6, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@c2
    .line 2326
    :cond_c2
    if-lez v1, :cond_f0

    #@c4
    .line 2327
    iget v6, p0, Lcom/android/server/WifiService;->mNumScansSinceNetworkStateChange:I

    #@c6
    add-int/lit8 v6, v6, 0x1

    #@c8
    iput v6, p0, Lcom/android/server/WifiService;->mNumScansSinceNetworkStateChange:I

    #@ca
    if-lt v6, v11, :cond_26

    #@cc
    .line 2328
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getOperator()Ljava/lang/String;

    #@cf
    move-result-object v6

    #@d0
    const-string v7, "ATT"

    #@d2
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d5
    move-result v6

    #@d6
    if-eqz v6, :cond_eb

    #@d8
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getCountry()Ljava/lang/String;

    #@db
    move-result-object v6

    #@dc
    const-string v7, "US"

    #@de
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e1
    move-result v6

    #@e2
    if-eqz v6, :cond_eb

    #@e4
    .line 2329
    const-string v6, "WifiService"

    #@e6
    const-string v7, "checkAndSetNotification(), call setNotificationVisible(true)"

    #@e8
    invoke-static {v6, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@eb
    .line 2338
    :cond_eb
    invoke-direct {p0, v10, v1, v9, v9}, Lcom/android/server/WifiService;->setNotificationVisible(ZIZI)V

    #@ee
    goto/16 :goto_26

    #@f0
    .line 2346
    .end local v0           #i:I
    .end local v1           #numOpenNetworks:I
    .end local v2           #numSecuredNetworks:I
    .end local v4           #scanResults:Ljava/util/List;,"Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    :cond_f0
    invoke-direct {p0, v9, v9, v9, v9}, Lcom/android/server/WifiService;->setNotificationVisible(ZIZI)V

    #@f3
    goto/16 :goto_26
.end method

.method private enforceAccessPermission()V
    .registers 4

    #@0
    .prologue
    .line 898
    iget-object v0, p0, Lcom/android/server/WifiService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.ACCESS_WIFI_STATE"

    #@4
    const-string v2, "WifiService"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 900
    return-void
.end method

.method private enforceChangePermission()V
    .registers 4

    #@0
    .prologue
    .line 903
    iget-object v0, p0, Lcom/android/server/WifiService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.CHANGE_WIFI_STATE"

    #@4
    const-string v2, "WifiService"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 906
    return-void
.end method

.method private enforceConnectivityInternalPermission()V
    .registers 4

    #@0
    .prologue
    .line 915
    iget-object v0, p0, Lcom/android/server/WifiService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.CONNECTIVITY_INTERNAL"

    #@4
    const-string v2, "ConnectivityService"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 918
    return-void
.end method

.method private enforceMulticastChangePermission()V
    .registers 4

    #@0
    .prologue
    .line 909
    iget-object v0, p0, Lcom/android/server/WifiService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.CHANGE_WIFI_MULTICAST_STATE"

    #@4
    const-string v2, "WifiService"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 912
    return-void
.end method

.method private evaluateTrafficStatsPolling()V
    .registers 6

    #@0
    .prologue
    const v4, 0x2501f

    #@3
    const/4 v3, 0x0

    #@4
    .line 2231
    iget-object v1, p0, Lcom/android/server/WifiService;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@6
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    #@9
    move-result-object v1

    #@a
    sget-object v2, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    #@c
    if-ne v1, v2, :cond_1d

    #@e
    iget-boolean v1, p0, Lcom/android/server/WifiService;->mScreenOff:Z

    #@10
    if-nez v1, :cond_1d

    #@12
    .line 2232
    iget-object v1, p0, Lcom/android/server/WifiService;->mAsyncServiceHandler:Lcom/android/server/WifiService$AsyncServiceHandler;

    #@14
    const/4 v2, 0x1

    #@15
    invoke-static {v1, v4, v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    #@18
    move-result-object v0

    #@19
    .line 2238
    .local v0, msg:Landroid/os/Message;
    :goto_19
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@1c
    .line 2239
    return-void

    #@1d
    .line 2235
    .end local v0           #msg:Landroid/os/Message;
    :cond_1d
    iget-object v1, p0, Lcom/android/server/WifiService;->mAsyncServiceHandler:Lcom/android/server/WifiService$AsyncServiceHandler;

    #@1f
    invoke-static {v1, v4, v3, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    #@22
    move-result-object v0

    #@23
    .restart local v0       #msg:Landroid/os/Message;
    goto :goto_19
.end method

.method private getPersistedWifiState()I
    .registers 5

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 801
    iget-object v3, p0, Lcom/android/server/WifiService;->mContext:Landroid/content/Context;

    #@3
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@6
    move-result-object v0

    #@7
    .line 803
    .local v0, cr:Landroid/content/ContentResolver;
    :try_start_7
    const-string v3, "wifi_on"

    #@9
    invoke-static {v0, v3}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_c
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_7 .. :try_end_c} :catch_e

    #@c
    move-result v2

    #@d
    .line 806
    :goto_d
    return v2

    #@e
    .line 804
    :catch_e
    move-exception v1

    #@f
    .line 805
    .local v1, e:Landroid/provider/Settings$SettingNotFoundException;
    const-string v3, "wifi_on"

    #@11
    invoke-static {v0, v3, v2}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@14
    goto :goto_d
.end method

.method private handleAirplaneModeToggled(Z)V
    .registers 4
    .parameter "airplaneEnabled"

    #@0
    .prologue
    .line 850
    if-eqz p1, :cond_b

    #@2
    .line 852
    iget-boolean v0, p0, Lcom/android/server/WifiService;->mWifiEnabled:Z

    #@4
    if-eqz v0, :cond_a

    #@6
    .line 853
    const/4 v0, 0x3

    #@7
    invoke-direct {p0, v0}, Lcom/android/server/WifiService;->persistWifiState(I)V

    #@a
    .line 862
    :cond_a
    :goto_a
    return-void

    #@b
    .line 857
    :cond_b
    invoke-direct {p0}, Lcom/android/server/WifiService;->testAndClearWifiSavedState()Z

    #@e
    move-result v0

    #@f
    if-nez v0, :cond_1a

    #@11
    iget-object v0, p0, Lcom/android/server/WifiService;->mPersistWifiState:Ljava/util/concurrent/atomic/AtomicInteger;

    #@13
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    #@16
    move-result v0

    #@17
    const/4 v1, 0x2

    #@18
    if-ne v0, v1, :cond_a

    #@1a
    .line 859
    :cond_1a
    const/4 v0, 0x1

    #@1b
    invoke-direct {p0, v0}, Lcom/android/server/WifiService;->persistWifiState(I)V

    #@1e
    goto :goto_a
.end method

.method private handleWifiToggled(Z)V
    .registers 6
    .parameter "wifiEnabled"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 833
    iget-object v3, p0, Lcom/android/server/WifiService;->mAirplaneModeOn:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@4
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    #@7
    move-result v3

    #@8
    if-eqz v3, :cond_1a

    #@a
    invoke-direct {p0}, Lcom/android/server/WifiService;->isAirplaneToggleable()Z

    #@d
    move-result v3

    #@e
    if-eqz v3, :cond_1a

    #@10
    move v0, v1

    #@11
    .line 834
    .local v0, airplaneEnabled:Z
    :goto_11
    if-eqz p1, :cond_20

    #@13
    .line 835
    if-eqz v0, :cond_1c

    #@15
    .line 836
    const/4 v1, 0x2

    #@16
    invoke-direct {p0, v1}, Lcom/android/server/WifiService;->persistWifiState(I)V

    #@19
    .line 847
    :goto_19
    return-void

    #@1a
    .end local v0           #airplaneEnabled:Z
    :cond_1a
    move v0, v2

    #@1b
    .line 833
    goto :goto_11

    #@1c
    .line 838
    .restart local v0       #airplaneEnabled:Z
    :cond_1c
    invoke-direct {p0, v1}, Lcom/android/server/WifiService;->persistWifiState(I)V

    #@1f
    goto :goto_19

    #@20
    .line 845
    :cond_20
    invoke-direct {p0, v2}, Lcom/android/server/WifiService;->persistWifiState(I)V

    #@23
    goto :goto_19
.end method

.method private hidePasspointNotification()V
    .registers 5

    #@0
    .prologue
    .line 2388
    iget-object v1, p0, Lcom/android/server/WifiService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "notification"

    #@4
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@7
    move-result-object v0

    #@8
    check-cast v0, Landroid/app/NotificationManager;

    #@a
    .line 2390
    .local v0, manager:Landroid/app/NotificationManager;
    const-string v1, "WifiService"

    #@c
    new-instance v2, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v3, "hidePasspointNotification off ="

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    iget-boolean v3, p0, Lcom/android/server/WifiService;->mPasspointLayoutNotificationShown:Z

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 2391
    iget-boolean v1, p0, Lcom/android/server/WifiService;->mPasspointLayoutNotificationShown:Z

    #@26
    const/4 v2, 0x1

    #@27
    if-ne v1, v2, :cond_2f

    #@29
    .line 2392
    invoke-virtual {v0}, Landroid/app/NotificationManager;->cancelAll()V

    #@2c
    .line 2393
    const/4 v1, 0x0

    #@2d
    iput-boolean v1, p0, Lcom/android/server/WifiService;->mPasspointLayoutNotificationShown:Z

    #@2f
    .line 2395
    :cond_2f
    return-void
.end method

.method private isAirplaneModeOn()Z
    .registers 5

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 1767
    invoke-direct {p0}, Lcom/android/server/WifiService;->isAirplaneSensitive()Z

    #@5
    move-result v2

    #@6
    if-eqz v2, :cond_17

    #@8
    iget-object v2, p0, Lcom/android/server/WifiService;->mContext:Landroid/content/Context;

    #@a
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@d
    move-result-object v2

    #@e
    const-string v3, "airplane_mode_on"

    #@10
    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@13
    move-result v2

    #@14
    if-ne v2, v0, :cond_17

    #@16
    :goto_16
    return v0

    #@17
    :cond_17
    move v0, v1

    #@18
    goto :goto_16
.end method

.method private isAirplaneSensitive()Z
    .registers 4

    #@0
    .prologue
    .line 1748
    iget-object v1, p0, Lcom/android/server/WifiService;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5
    move-result-object v1

    #@6
    const-string v2, "airplane_mode_radios"

    #@8
    invoke-static {v1, v2}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@b
    move-result-object v0

    #@c
    .line 1750
    .local v0, airplaneModeRadios:Ljava/lang/String;
    if-eqz v0, :cond_16

    #@e
    const-string v1, "wifi"

    #@10
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@13
    move-result v1

    #@14
    if-eqz v1, :cond_18

    #@16
    :cond_16
    const/4 v1, 0x1

    #@17
    :goto_17
    return v1

    #@18
    :cond_18
    const/4 v1, 0x0

    #@19
    goto :goto_17
.end method

.method private isAirplaneToggleable()Z
    .registers 4

    #@0
    .prologue
    .line 1755
    iget-object v1, p0, Lcom/android/server/WifiService;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5
    move-result-object v1

    #@6
    const-string v2, "airplane_mode_toggleable_radios"

    #@8
    invoke-static {v1, v2}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@b
    move-result-object v0

    #@c
    .line 1757
    .local v0, toggleableRadios:Ljava/lang/String;
    if-eqz v0, :cond_18

    #@e
    const-string v1, "wifi"

    #@10
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@13
    move-result v1

    #@14
    if-eqz v1, :cond_18

    #@16
    const/4 v1, 0x1

    #@17
    :goto_17
    return v1

    #@18
    :cond_18
    const/4 v1, 0x0

    #@19
    goto :goto_17
.end method

.method private isPowerSaveModeEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 2698
    sget-boolean v0, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@2
    if-eqz v0, :cond_f

    #@4
    iget-object v0, p0, Lcom/android/server/WifiService;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@6
    if-eqz v0, :cond_f

    #@8
    .line 2699
    iget-object v0, p0, Lcom/android/server/WifiService;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@a
    invoke-interface {v0}, Lcom/lge/wifi_iface/WifiServiceExtIface;->getWiFiPowerSaveModeEnabled()Z

    #@d
    move-result v0

    #@e
    .line 2701
    :goto_e
    return v0

    #@f
    :cond_f
    const/4 v0, 0x0

    #@10
    goto :goto_e
.end method

.method private noteAcquireWifiLock(Lcom/android/server/WifiService$WifiLock;)V
    .registers 4
    .parameter "wifiLock"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1929
    iget v0, p1, Lcom/android/server/WifiService$DeathRecipient;->mMode:I

    #@2
    packed-switch v0, :pswitch_data_e

    #@5
    .line 1936
    :goto_5
    return-void

    #@6
    .line 1933
    :pswitch_6
    iget-object v0, p0, Lcom/android/server/WifiService;->mBatteryStats:Lcom/android/internal/app/IBatteryStats;

    #@8
    iget-object v1, p1, Lcom/android/server/WifiService$DeathRecipient;->mWorkSource:Landroid/os/WorkSource;

    #@a
    invoke-interface {v0, v1}, Lcom/android/internal/app/IBatteryStats;->noteFullWifiLockAcquiredFromSource(Landroid/os/WorkSource;)V

    #@d
    goto :goto_5

    #@e
    .line 1929
    :pswitch_data_e
    .packed-switch 0x1
        :pswitch_6
        :pswitch_6
        :pswitch_6
    .end packed-switch
.end method

.method private noteReleaseWifiLock(Lcom/android/server/WifiService$WifiLock;)V
    .registers 4
    .parameter "wifiLock"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1939
    iget v0, p1, Lcom/android/server/WifiService$DeathRecipient;->mMode:I

    #@2
    packed-switch v0, :pswitch_data_e

    #@5
    .line 1946
    :goto_5
    return-void

    #@6
    .line 1943
    :pswitch_6
    iget-object v0, p0, Lcom/android/server/WifiService;->mBatteryStats:Lcom/android/internal/app/IBatteryStats;

    #@8
    iget-object v1, p1, Lcom/android/server/WifiService$DeathRecipient;->mWorkSource:Landroid/os/WorkSource;

    #@a
    invoke-interface {v0, v1}, Lcom/android/internal/app/IBatteryStats;->noteFullWifiLockReleasedFromSource(Landroid/os/WorkSource;)V

    #@d
    goto :goto_5

    #@e
    .line 1939
    :pswitch_data_e
    .packed-switch 0x1
        :pswitch_6
        :pswitch_6
        :pswitch_6
    .end packed-switch
.end method

.method private noteScanEnd()V
    .registers 4

    #@0
    .prologue
    .line 727
    const/4 v1, 0x0

    #@1
    .line 728
    .local v1, scanWorkSource:Landroid/os/WorkSource;
    monitor-enter p0

    #@2
    .line 729
    :try_start_2
    iget-object v1, p0, Lcom/android/server/WifiService;->mScanWorkSource:Landroid/os/WorkSource;

    #@4
    .line 730
    const/4 v2, 0x0

    #@5
    iput-object v2, p0, Lcom/android/server/WifiService;->mScanWorkSource:Landroid/os/WorkSource;

    #@7
    .line 731
    monitor-exit p0
    :try_end_8
    .catchall {:try_start_2 .. :try_end_8} :catchall_10

    #@8
    .line 732
    if-eqz v1, :cond_f

    #@a
    .line 734
    :try_start_a
    iget-object v2, p0, Lcom/android/server/WifiService;->mBatteryStats:Lcom/android/internal/app/IBatteryStats;

    #@c
    invoke-interface {v2, v1}, Lcom/android/internal/app/IBatteryStats;->noteWifiScanStoppedFromSource(Landroid/os/WorkSource;)V
    :try_end_f
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_f} :catch_13

    #@f
    .line 739
    :cond_f
    :goto_f
    return-void

    #@10
    .line 731
    :catchall_10
    move-exception v2

    #@11
    :try_start_11
    monitor-exit p0
    :try_end_12
    .catchall {:try_start_11 .. :try_end_12} :catchall_10

    #@12
    throw v2

    #@13
    .line 735
    :catch_13
    move-exception v0

    #@14
    .line 736
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "WifiService"

    #@16
    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    #@19
    goto :goto_f
.end method

.method private noteScanStart()V
    .registers 7

    #@0
    .prologue
    .line 705
    const/4 v3, 0x0

    #@1
    .line 706
    .local v3, scanWorkSource:Landroid/os/WorkSource;
    monitor-enter p0

    #@2
    .line 707
    :try_start_2
    iget-object v5, p0, Lcom/android/server/WifiService;->mScanWorkSource:Landroid/os/WorkSource;

    #@4
    if-eqz v5, :cond_8

    #@6
    .line 709
    monitor-exit p0

    #@7
    .line 723
    :goto_7
    return-void

    #@8
    .line 711
    :cond_8
    new-instance v4, Landroid/os/WorkSource;

    #@a
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@d
    move-result v5

    #@e
    invoke-direct {v4, v5}, Landroid/os/WorkSource;-><init>(I)V
    :try_end_11
    .catchall {:try_start_2 .. :try_end_11} :catchall_22

    #@11
    .line 712
    .end local v3           #scanWorkSource:Landroid/os/WorkSource;
    .local v4, scanWorkSource:Landroid/os/WorkSource;
    :try_start_11
    iput-object v4, p0, Lcom/android/server/WifiService;->mScanWorkSource:Landroid/os/WorkSource;

    #@13
    .line 713
    monitor-exit p0
    :try_end_14
    .catchall {:try_start_11 .. :try_end_14} :catchall_31

    #@14
    .line 715
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@17
    move-result-wide v1

    #@18
    .line 717
    .local v1, id:J
    :try_start_18
    iget-object v5, p0, Lcom/android/server/WifiService;->mBatteryStats:Lcom/android/internal/app/IBatteryStats;

    #@1a
    invoke-interface {v5, v4}, Lcom/android/internal/app/IBatteryStats;->noteWifiScanStartedFromSource(Landroid/os/WorkSource;)V
    :try_end_1d
    .catchall {:try_start_18 .. :try_end_1d} :catchall_2c
    .catch Landroid/os/RemoteException; {:try_start_18 .. :try_end_1d} :catch_25

    #@1d
    .line 721
    :goto_1d
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@20
    move-object v3, v4

    #@21
    .line 723
    .end local v4           #scanWorkSource:Landroid/os/WorkSource;
    .restart local v3       #scanWorkSource:Landroid/os/WorkSource;
    goto :goto_7

    #@22
    .line 713
    .end local v1           #id:J
    :catchall_22
    move-exception v5

    #@23
    :goto_23
    :try_start_23
    monitor-exit p0
    :try_end_24
    .catchall {:try_start_23 .. :try_end_24} :catchall_22

    #@24
    throw v5

    #@25
    .line 718
    .end local v3           #scanWorkSource:Landroid/os/WorkSource;
    .restart local v1       #id:J
    .restart local v4       #scanWorkSource:Landroid/os/WorkSource;
    :catch_25
    move-exception v0

    #@26
    .line 719
    .local v0, e:Landroid/os/RemoteException;
    :try_start_26
    const-string v5, "WifiService"

    #@28
    invoke-static {v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2b
    .catchall {:try_start_26 .. :try_end_2b} :catchall_2c

    #@2b
    goto :goto_1d

    #@2c
    .line 721
    .end local v0           #e:Landroid/os/RemoteException;
    :catchall_2c
    move-exception v5

    #@2d
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@30
    throw v5

    #@31
    .line 713
    .end local v1           #id:J
    :catchall_31
    move-exception v5

    #@32
    move-object v3, v4

    #@33
    .end local v4           #scanWorkSource:Landroid/os/WorkSource;
    .restart local v3       #scanWorkSource:Landroid/os/WorkSource;
    goto :goto_23
.end method

.method private notifyOnDataActivity()V
    .registers 16

    #@0
    .prologue
    const-wide/16 v13, 0x0

    #@2
    .line 2243
    iget-wide v5, p0, Lcom/android/server/WifiService;->mTxPkts:J

    #@4
    .local v5, preTxPkts:J
    iget-wide v3, p0, Lcom/android/server/WifiService;->mRxPkts:J

    #@6
    .line 2244
    .local v3, preRxPkts:J
    const/4 v1, 0x0

    #@7
    .line 2246
    .local v1, dataActivity:I
    iget-object v11, p0, Lcom/android/server/WifiService;->mInterfaceName:Ljava/lang/String;

    #@9
    invoke-static {v11}, Landroid/net/TrafficStats;->getTxPackets(Ljava/lang/String;)J

    #@c
    move-result-wide v11

    #@d
    iput-wide v11, p0, Lcom/android/server/WifiService;->mTxPkts:J

    #@f
    .line 2247
    iget-object v11, p0, Lcom/android/server/WifiService;->mInterfaceName:Ljava/lang/String;

    #@11
    invoke-static {v11}, Landroid/net/TrafficStats;->getRxPackets(Ljava/lang/String;)J

    #@14
    move-result-wide v11

    #@15
    iput-wide v11, p0, Lcom/android/server/WifiService;->mRxPkts:J

    #@17
    .line 2249
    cmp-long v11, v5, v13

    #@19
    if-gtz v11, :cond_1f

    #@1b
    cmp-long v11, v3, v13

    #@1d
    if-lez v11, :cond_56

    #@1f
    .line 2250
    :cond_1f
    iget-wide v11, p0, Lcom/android/server/WifiService;->mTxPkts:J

    #@21
    sub-long v9, v11, v5

    #@23
    .line 2251
    .local v9, sent:J
    iget-wide v11, p0, Lcom/android/server/WifiService;->mRxPkts:J

    #@25
    sub-long v7, v11, v3

    #@27
    .line 2252
    .local v7, received:J
    cmp-long v11, v9, v13

    #@29
    if-lez v11, :cond_2d

    #@2b
    .line 2253
    or-int/lit8 v1, v1, 0x2

    #@2d
    .line 2255
    :cond_2d
    cmp-long v11, v7, v13

    #@2f
    if-lez v11, :cond_33

    #@31
    .line 2256
    or-int/lit8 v1, v1, 0x1

    #@33
    .line 2259
    :cond_33
    iget v11, p0, Lcom/android/server/WifiService;->mDataActivity:I

    #@35
    if-eq v1, v11, :cond_56

    #@37
    iget-boolean v11, p0, Lcom/android/server/WifiService;->mScreenOff:Z

    #@39
    if-nez v11, :cond_56

    #@3b
    .line 2260
    iput v1, p0, Lcom/android/server/WifiService;->mDataActivity:I

    #@3d
    .line 2261
    iget-object v11, p0, Lcom/android/server/WifiService;->mClients:Ljava/util/List;

    #@3f
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@42
    move-result-object v2

    #@43
    .local v2, i$:Ljava/util/Iterator;
    :goto_43
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@46
    move-result v11

    #@47
    if-eqz v11, :cond_56

    #@49
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@4c
    move-result-object v0

    #@4d
    check-cast v0, Lcom/android/internal/util/AsyncChannel;

    #@4f
    .line 2262
    .local v0, client:Lcom/android/internal/util/AsyncChannel;
    const/4 v11, 0x1

    #@50
    iget v12, p0, Lcom/android/server/WifiService;->mDataActivity:I

    #@52
    invoke-virtual {v0, v11, v12}, Lcom/android/internal/util/AsyncChannel;->sendMessage(II)V

    #@55
    goto :goto_43

    #@56
    .line 2266
    .end local v0           #client:Lcom/android/internal/util/AsyncChannel;
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v7           #received:J
    .end local v9           #sent:J
    :cond_56
    return-void
.end method

.method private persistWifiState(I)V
    .registers 4
    .parameter "state"

    #@0
    .prologue
    .line 865
    iget-object v1, p0, Lcom/android/server/WifiService;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5
    move-result-object v0

    #@6
    .line 866
    .local v0, cr:Landroid/content/ContentResolver;
    iget-object v1, p0, Lcom/android/server/WifiService;->mPersistWifiState:Ljava/util/concurrent/atomic/AtomicInteger;

    #@8
    invoke-virtual {v1, p1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    #@b
    .line 867
    const-string v1, "wifi_on"

    #@d
    invoke-static {v0, v1, p1}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@10
    .line 868
    return-void
.end method

.method private registerForBroadcasts()V
    .registers 4

    #@0
    .prologue
    .line 1737
    new-instance v0, Landroid/content/IntentFilter;

    #@2
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@5
    .line 1738
    .local v0, intentFilter:Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.SCREEN_ON"

    #@7
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@a
    .line 1739
    const-string v1, "android.intent.action.SCREEN_OFF"

    #@c
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@f
    .line 1740
    const-string v1, "android.intent.action.BATTERY_CHANGED"

    #@11
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@14
    .line 1741
    const-string v1, "com.android.server.WifiManager.action.DEVICE_IDLE"

    #@16
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@19
    .line 1742
    const-string v1, "android.bluetooth.adapter.action.CONNECTION_STATE_CHANGED"

    #@1b
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1e
    .line 1743
    const-string v1, "android.intent.action.EMERGENCY_CALLBACK_MODE_CHANGED"

    #@20
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@23
    .line 1744
    iget-object v1, p0, Lcom/android/server/WifiService;->mContext:Landroid/content/Context;

    #@25
    iget-object v2, p0, Lcom/android/server/WifiService;->mReceiver:Landroid/content/BroadcastReceiver;

    #@27
    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@2a
    .line 1745
    return-void
.end method

.method private releaseWifiLockLocked(Landroid/os/IBinder;)Z
    .registers 9
    .parameter "lock"

    #@0
    .prologue
    .line 2030
    iget-object v4, p0, Lcom/android/server/WifiService;->mLocks:Lcom/android/server/WifiService$LockList;

    #@2
    invoke-static {v4, p1}, Lcom/android/server/WifiService$LockList;->access$4700(Lcom/android/server/WifiService$LockList;Landroid/os/IBinder;)Lcom/android/server/WifiService$WifiLock;

    #@5
    move-result-object v3

    #@6
    .line 2032
    .local v3, wifiLock:Lcom/android/server/WifiService$WifiLock;
    const-string v4, "WifiService"

    #@8
    new-instance v5, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v6, "releaseWifiLockLocked: "

    #@f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v5

    #@13
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v5

    #@17
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v5

    #@1b
    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 2034
    if-eqz v3, :cond_60

    #@20
    const/4 v0, 0x1

    #@21
    .line 2036
    .local v0, hadLock:Z
    :goto_21
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@24
    move-result-wide v1

    #@25
    .line 2038
    .local v1, ident:J
    if-eqz v0, :cond_46

    #@27
    .line 2044
    :try_start_27
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->useWifiDLNA()Z

    #@2a
    move-result v4

    #@2b
    if-eqz v4, :cond_3e

    #@2d
    .line 2045
    iget-object v4, v3, Lcom/android/server/WifiService$DeathRecipient;->mTag:Ljava/lang/String;

    #@2f
    if-eqz v4, :cond_3e

    #@31
    iget-object v4, v3, Lcom/android/server/WifiService$DeathRecipient;->mTag:Ljava/lang/String;

    #@33
    const-string v5, "dlna.wifilock"

    #@35
    invoke-virtual {v4, v5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@38
    move-result v4

    #@39
    if-nez v4, :cond_3e

    #@3b
    .line 2046
    invoke-direct {p0}, Lcom/android/server/WifiService;->setDLNADisabled()V

    #@3e
    .line 2049
    :cond_3e
    invoke-direct {p0, v3}, Lcom/android/server/WifiService;->noteReleaseWifiLock(Lcom/android/server/WifiService$WifiLock;)V

    #@41
    .line 2050
    iget v4, v3, Lcom/android/server/WifiService$DeathRecipient;->mMode:I

    #@43
    packed-switch v4, :pswitch_data_7e

    #@46
    .line 2067
    :cond_46
    :goto_46
    sget-boolean v4, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@48
    if-eqz v4, :cond_59

    #@4a
    .line 2068
    iget-object v4, p0, Lcom/android/server/WifiService;->mWifiOffDelayIfNotUsed:Lcom/lge/wifi_iface/WifiOffDelayIfNotUsedIface;

    #@4c
    if-eqz v4, :cond_59

    #@4e
    .line 2070
    iget-object v4, p0, Lcom/android/server/WifiService;->mWifiOffDelayIfNotUsed:Lcom/lge/wifi_iface/WifiOffDelayIfNotUsedIface;

    #@50
    iget-object v5, p0, Lcom/android/server/WifiService;->mLocks:Lcom/android/server/WifiService$LockList;

    #@52
    invoke-static {v5}, Lcom/android/server/WifiService$LockList;->access$3600(Lcom/android/server/WifiService$LockList;)Z

    #@55
    move-result v5

    #@56
    invoke-interface {v4, v5}, Lcom/lge/wifi_iface/WifiOffDelayIfNotUsedIface;->setHasWifiLocks(Z)V

    #@59
    .line 2075
    :cond_59
    invoke-direct {p0}, Lcom/android/server/WifiService;->updateWifiState()V
    :try_end_5c
    .catchall {:try_start_27 .. :try_end_5c} :catchall_72
    .catch Landroid/os/RemoteException; {:try_start_27 .. :try_end_5c} :catch_69

    #@5c
    .line 2079
    :goto_5c
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@5f
    .line 2082
    return v0

    #@60
    .line 2034
    .end local v0           #hadLock:Z
    .end local v1           #ident:J
    :cond_60
    const/4 v0, 0x0

    #@61
    goto :goto_21

    #@62
    .line 2052
    .restart local v0       #hadLock:Z
    .restart local v1       #ident:J
    :pswitch_62
    :try_start_62
    iget v4, p0, Lcom/android/server/WifiService;->mFullLocksReleased:I

    #@64
    add-int/lit8 v4, v4, 0x1

    #@66
    iput v4, p0, Lcom/android/server/WifiService;->mFullLocksReleased:I

    #@68
    goto :goto_46

    #@69
    .line 2077
    :catch_69
    move-exception v4

    #@6a
    goto :goto_5c

    #@6b
    .line 2055
    :pswitch_6b
    iget v4, p0, Lcom/android/server/WifiService;->mFullHighPerfLocksReleased:I

    #@6d
    add-int/lit8 v4, v4, 0x1

    #@6f
    iput v4, p0, Lcom/android/server/WifiService;->mFullHighPerfLocksReleased:I
    :try_end_71
    .catchall {:try_start_62 .. :try_end_71} :catchall_72
    .catch Landroid/os/RemoteException; {:try_start_62 .. :try_end_71} :catch_69

    #@71
    goto :goto_46

    #@72
    .line 2079
    :catchall_72
    move-exception v4

    #@73
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@76
    throw v4

    #@77
    .line 2058
    :pswitch_77
    :try_start_77
    iget v4, p0, Lcom/android/server/WifiService;->mScanLocksReleased:I

    #@79
    add-int/lit8 v4, v4, 0x1

    #@7b
    iput v4, p0, Lcom/android/server/WifiService;->mScanLocksReleased:I
    :try_end_7d
    .catchall {:try_start_77 .. :try_end_7d} :catchall_72
    .catch Landroid/os/RemoteException; {:try_start_77 .. :try_end_7d} :catch_69

    #@7d
    goto :goto_46

    #@7e
    .line 2050
    :pswitch_data_7e
    .packed-switch 0x1
        :pswitch_62
        :pswitch_77
        :pswitch_6b
    .end packed-switch
.end method

.method private removeMulticasterLocked(II)V
    .registers 8
    .parameter "i"
    .parameter "uid"

    #@0
    .prologue
    .line 2199
    iget-object v2, p0, Lcom/android/server/WifiService;->mMulticasters:Ljava/util/List;

    #@2
    invoke-interface {v2, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    check-cast v1, Lcom/android/server/WifiService$Multicaster;

    #@8
    .line 2201
    .local v1, removed:Lcom/android/server/WifiService$Multicaster;
    if-eqz v1, :cond_d

    #@a
    .line 2202
    invoke-virtual {v1}, Lcom/android/server/WifiService$Multicaster;->unlinkDeathRecipient()V

    #@d
    .line 2204
    :cond_d
    iget-object v2, p0, Lcom/android/server/WifiService;->mMulticasters:Ljava/util/List;

    #@f
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@12
    move-result v2

    #@13
    if-nez v2, :cond_1a

    #@15
    .line 2205
    iget-object v2, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@17
    invoke-virtual {v2}, Landroid/net/wifi/WifiStateMachine;->startFilteringMulticastV4Packets()V

    #@1a
    .line 2208
    :cond_1a
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@1d
    move-result-wide v2

    #@1e
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@21
    move-result-object v0

    #@22
    .line 2210
    .local v0, ident:Ljava/lang/Long;
    :try_start_22
    iget-object v2, p0, Lcom/android/server/WifiService;->mBatteryStats:Lcom/android/internal/app/IBatteryStats;

    #@24
    invoke-interface {v2, p2}, Lcom/android/internal/app/IBatteryStats;->noteWifiMulticastDisabled(I)V
    :try_end_27
    .catchall {:try_start_22 .. :try_end_27} :catchall_2f
    .catch Landroid/os/RemoteException; {:try_start_22 .. :try_end_27} :catch_38

    #@27
    .line 2213
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    #@2a
    move-result-wide v2

    #@2b
    :goto_2b
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@2e
    .line 2215
    return-void

    #@2f
    .line 2213
    :catchall_2f
    move-exception v2

    #@30
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    #@33
    move-result-wide v3

    #@34
    invoke-static {v3, v4}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@37
    throw v2

    #@38
    .line 2211
    :catch_38
    move-exception v2

    #@39
    .line 2213
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    #@3c
    move-result-wide v2

    #@3d
    goto :goto_2b
.end method

.method private declared-synchronized reportStartWorkSource()V
    .registers 4

    #@0
    .prologue
    .line 1677
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v1, p0, Lcom/android/server/WifiService;->mTmpWorkSource:Landroid/os/WorkSource;

    #@3
    invoke-virtual {v1}, Landroid/os/WorkSource;->clear()V

    #@6
    .line 1678
    iget-boolean v1, p0, Lcom/android/server/WifiService;->mDeviceIdle:Z

    #@8
    if-eqz v1, :cond_2d

    #@a
    .line 1679
    const/4 v0, 0x0

    #@b
    .local v0, i:I
    :goto_b
    iget-object v1, p0, Lcom/android/server/WifiService;->mLocks:Lcom/android/server/WifiService$LockList;

    #@d
    invoke-static {v1}, Lcom/android/server/WifiService$LockList;->access$3500(Lcom/android/server/WifiService$LockList;)Ljava/util/List;

    #@10
    move-result-object v1

    #@11
    invoke-interface {v1}, Ljava/util/List;->size()I

    #@14
    move-result v1

    #@15
    if-ge v0, v1, :cond_2d

    #@17
    .line 1680
    iget-object v2, p0, Lcom/android/server/WifiService;->mTmpWorkSource:Landroid/os/WorkSource;

    #@19
    iget-object v1, p0, Lcom/android/server/WifiService;->mLocks:Lcom/android/server/WifiService$LockList;

    #@1b
    invoke-static {v1}, Lcom/android/server/WifiService$LockList;->access$3500(Lcom/android/server/WifiService$LockList;)Ljava/util/List;

    #@1e
    move-result-object v1

    #@1f
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@22
    move-result-object v1

    #@23
    check-cast v1, Lcom/android/server/WifiService$WifiLock;

    #@25
    iget-object v1, v1, Lcom/android/server/WifiService$DeathRecipient;->mWorkSource:Landroid/os/WorkSource;

    #@27
    invoke-virtual {v2, v1}, Landroid/os/WorkSource;->add(Landroid/os/WorkSource;)Z

    #@2a
    .line 1679
    add-int/lit8 v0, v0, 0x1

    #@2c
    goto :goto_b

    #@2d
    .line 1683
    .end local v0           #i:I
    :cond_2d
    iget-object v1, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@2f
    iget-object v2, p0, Lcom/android/server/WifiService;->mTmpWorkSource:Landroid/os/WorkSource;

    #@31
    invoke-virtual {v1, v2}, Landroid/net/wifi/WifiStateMachine;->updateBatteryWorkSource(Landroid/os/WorkSource;)V
    :try_end_34
    .catchall {:try_start_1 .. :try_end_34} :catchall_36

    #@34
    .line 1684
    monitor-exit p0

    #@35
    return-void

    #@36
    .line 1677
    :catchall_36
    move-exception v1

    #@37
    monitor-exit p0

    #@38
    throw v1
.end method

.method private resetNotification()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 2354
    const-wide/16 v0, 0x0

    #@3
    iput-wide v0, p0, Lcom/android/server/WifiService;->mNotificationRepeatTime:J

    #@5
    .line 2355
    iput v2, p0, Lcom/android/server/WifiService;->mNumScansSinceNetworkStateChange:I

    #@7
    .line 2356
    invoke-direct {p0, v2, v2, v2, v2}, Lcom/android/server/WifiService;->setNotificationVisible(ZIZI)V

    #@a
    .line 2357
    return-void
.end method

.method private setDLNADisabled()V
    .registers 3

    #@0
    .prologue
    .line 2583
    sget-boolean v0, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@2
    if-eqz v0, :cond_e

    #@4
    iget-object v0, p0, Lcom/android/server/WifiService;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@6
    if-eqz v0, :cond_e

    #@8
    .line 2584
    iget-object v0, p0, Lcom/android/server/WifiService;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@a
    const/4 v1, 0x0

    #@b
    invoke-interface {v0, v1}, Lcom/lge/wifi_iface/WifiServiceExtIface;->setDlnaEnabled(Z)Z

    #@e
    .line 2586
    :cond_e
    return-void
.end method

.method private setDeviceIdleAndUpdateWifi(Z)V
    .registers 2
    .parameter "deviceIdle"

    #@0
    .prologue
    .line 1671
    iput-boolean p1, p0, Lcom/android/server/WifiService;->mDeviceIdle:Z

    #@2
    .line 1672
    invoke-direct {p0}, Lcom/android/server/WifiService;->reportStartWorkSource()V

    #@5
    .line 1673
    invoke-direct {p0}, Lcom/android/server/WifiService;->updateWifiState()V

    #@8
    .line 1674
    return-void
.end method

.method private setNotificationVisible(ZIZI)V
    .registers 15
    .parameter "visible"
    .parameter "numNetworks"
    .parameter "force"
    .parameter "delay"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    const/4 v4, 0x0

    #@2
    .line 2417
    if-nez p1, :cond_b

    #@4
    iget-boolean v5, p0, Lcom/android/server/WifiService;->mNotificationShown:Z

    #@6
    if-nez v5, :cond_b

    #@8
    if-nez p3, :cond_b

    #@a
    .line 2516
    :cond_a
    :goto_a
    return-void

    #@b
    .line 2422
    :cond_b
    sget-boolean v5, Lcom/android/server/WifiService;->WFC_FEATURE:Z

    #@d
    if-eqz v5, :cond_11

    #@f
    .line 2423
    iput p2, p0, Lcom/android/server/WifiService;->mLowSignalNWs:I

    #@11
    .line 2429
    :cond_11
    sget-boolean v5, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@13
    if-eqz v5, :cond_112

    #@15
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->useWiFiOffloading()Z

    #@18
    move-result v5

    #@19
    if-eqz v5, :cond_112

    #@1b
    .line 2430
    invoke-static {}, Lcom/lge/wifi_iface/WifiIfaceManager;->getWiFiOffloadingIfaceIface()Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;

    #@1e
    move-result-object v5

    #@1f
    iput-object v5, p0, Lcom/android/server/WifiService;->mWiFiOffloadingIfaceIface:Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;

    #@21
    .line 2431
    iget-object v5, p0, Lcom/android/server/WifiService;->mWiFiOffloadingIfaceIface:Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;

    #@23
    if-eqz v5, :cond_109

    #@25
    .line 2432
    const-string v5, "WifiService"

    #@27
    new-instance v6, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const-string v7, "isWifiOffloadingEnabled() : "

    #@2e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v6

    #@32
    iget-object v7, p0, Lcom/android/server/WifiService;->mWiFiOffloadingIfaceIface:Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;

    #@34
    invoke-interface {v7}, Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;->isWifiOffloadingEnabled()I

    #@37
    move-result v7

    #@38
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v6

    #@3c
    const-string v7, "in WifiService.java"

    #@3e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v6

    #@42
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v6

    #@46
    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@49
    .line 2433
    iget-object v5, p0, Lcom/android/server/WifiService;->mWiFiOffloadingIfaceIface:Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;

    #@4b
    invoke-interface {v5}, Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;->isWifiOffloadingEnabled()I

    #@4e
    move-result v5

    #@4f
    if-eqz v5, :cond_a

    #@51
    .line 2446
    :goto_51
    iget-object v5, p0, Lcom/android/server/WifiService;->mContext:Landroid/content/Context;

    #@53
    const-string v6, "notification"

    #@55
    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@58
    move-result-object v2

    #@59
    check-cast v2, Landroid/app/NotificationManager;

    #@5b
    .line 2452
    .local v2, notificationManager:Landroid/app/NotificationManager;
    move v0, p1

    #@5c
    .line 2454
    .local v0, bNotiVisible:Z
    sget-boolean v5, Lcom/android/server/WifiService;->WFC_FEATURE:Z

    #@5e
    if-eqz v5, :cond_67

    #@60
    .line 2455
    if-eqz p1, :cond_13e

    #@62
    iget-boolean v5, p0, Lcom/android/server/WifiService;->mLowSignal:Z

    #@64
    if-nez v5, :cond_13e

    #@66
    const/4 v0, 0x1

    #@67
    .line 2458
    :cond_67
    :goto_67
    if-eqz v0, :cond_15e

    #@69
    .line 2461
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@6c
    move-result-wide v5

    #@6d
    iget-wide v7, p0, Lcom/android/server/WifiService;->mNotificationRepeatTime:J

    #@6f
    cmp-long v5, v5, v7

    #@71
    if-ltz v5, :cond_a

    #@73
    .line 2465
    iget-object v5, p0, Lcom/android/server/WifiService;->mNotification:Landroid/app/Notification;

    #@75
    if-nez v5, :cond_af

    #@77
    .line 2467
    new-instance v5, Landroid/app/Notification;

    #@79
    invoke-direct {v5}, Landroid/app/Notification;-><init>()V

    #@7c
    iput-object v5, p0, Lcom/android/server/WifiService;->mNotification:Landroid/app/Notification;

    #@7e
    .line 2468
    iget-object v5, p0, Lcom/android/server/WifiService;->mNotification:Landroid/app/Notification;

    #@80
    const-wide/16 v6, 0x0

    #@82
    iput-wide v6, v5, Landroid/app/Notification;->when:J

    #@84
    .line 2469
    iget-object v5, p0, Lcom/android/server/WifiService;->mNotification:Landroid/app/Notification;

    #@86
    sget v6, Lcom/android/server/WifiService;->ICON_NETWORKS_AVAILABLE:I

    #@88
    iput v6, v5, Landroid/app/Notification;->icon:I

    #@8a
    .line 2470
    iget-object v5, p0, Lcom/android/server/WifiService;->mNotification:Landroid/app/Notification;

    #@8c
    const/16 v6, 0x10

    #@8e
    iput v6, v5, Landroid/app/Notification;->flags:I

    #@90
    .line 2472
    sget-boolean v5, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@92
    if-eqz v5, :cond_141

    #@94
    .line 2473
    iget-object v5, p0, Lcom/android/server/WifiService;->mNotification:Landroid/app/Notification;

    #@96
    iget-object v6, p0, Lcom/android/server/WifiService;->mContext:Landroid/content/Context;

    #@98
    invoke-static {v6}, Landroid/app/TaskStackBuilder;->create(Landroid/content/Context;)Landroid/app/TaskStackBuilder;

    #@9b
    move-result-object v6

    #@9c
    new-instance v7, Landroid/content/Intent;

    #@9e
    const-string v8, "android.settings.WIFI_SETTINGS"

    #@a0
    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@a3
    invoke-virtual {v6, v7}, Landroid/app/TaskStackBuilder;->addNextIntentWithParentStack(Landroid/content/Intent;)Landroid/app/TaskStackBuilder;

    #@a6
    move-result-object v6

    #@a7
    sget-object v7, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    #@a9
    invoke-virtual {v6, v4, v4, v9, v7}, Landroid/app/TaskStackBuilder;->getPendingIntent(IILandroid/os/Bundle;Landroid/os/UserHandle;)Landroid/app/PendingIntent;

    #@ac
    move-result-object v4

    #@ad
    iput-object v4, v5, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    #@af
    .line 2488
    :cond_af
    :goto_af
    iget-object v4, p0, Lcom/android/server/WifiService;->mContext:Landroid/content/Context;

    #@b1
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@b4
    move-result-object v4

    #@b5
    const v5, 0x1130014

    #@b8
    invoke-virtual {v4, v5, p2}, Landroid/content/res/Resources;->getQuantityText(II)Ljava/lang/CharSequence;

    #@bb
    move-result-object v3

    #@bc
    .line 2490
    .local v3, title:Ljava/lang/CharSequence;
    iget-object v4, p0, Lcom/android/server/WifiService;->mContext:Landroid/content/Context;

    #@be
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@c1
    move-result-object v4

    #@c2
    const v5, 0x1130015

    #@c5
    invoke-virtual {v4, v5, p2}, Landroid/content/res/Resources;->getQuantityText(II)Ljava/lang/CharSequence;

    #@c8
    move-result-object v1

    #@c9
    .line 2494
    .local v1, details:Ljava/lang/CharSequence;
    sget-boolean v4, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@cb
    if-eqz v4, :cond_e4

    #@cd
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->isWifiChameleonFeaturedCarrier()Z

    #@d0
    move-result v4

    #@d1
    if-eqz v4, :cond_e4

    #@d3
    .line 2497
    iget v4, p0, Lcom/android/server/WifiService;->wifiNetworkAvailableSettings:I

    #@d5
    const/4 v5, 0x2

    #@d6
    if-ne v4, v5, :cond_e4

    #@d8
    .line 2498
    iget-object v4, p0, Lcom/android/server/WifiService;->mContext:Landroid/content/Context;

    #@da
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@dd
    move-result-object v4

    #@de
    const/high16 v5, 0x20e

    #@e0
    invoke-virtual {v4, v5, p2}, Landroid/content/res/Resources;->getQuantityText(II)Ljava/lang/CharSequence;

    #@e3
    move-result-object v1

    #@e4
    .line 2504
    :cond_e4
    iget-object v4, p0, Lcom/android/server/WifiService;->mNotification:Landroid/app/Notification;

    #@e6
    iput-object v3, v4, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    #@e8
    .line 2505
    iget-object v4, p0, Lcom/android/server/WifiService;->mNotification:Landroid/app/Notification;

    #@ea
    iget-object v5, p0, Lcom/android/server/WifiService;->mContext:Landroid/content/Context;

    #@ec
    iget-object v6, p0, Lcom/android/server/WifiService;->mNotification:Landroid/app/Notification;

    #@ee
    iget-object v6, v6, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    #@f0
    invoke-virtual {v4, v5, v3, v1, v6}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    #@f3
    .line 2507
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@f6
    move-result-wide v4

    #@f7
    iget-wide v6, p0, Lcom/android/server/WifiService;->NOTIFICATION_REPEAT_DELAY_MS:J

    #@f9
    add-long/2addr v4, v6

    #@fa
    iput-wide v4, p0, Lcom/android/server/WifiService;->mNotificationRepeatTime:J

    #@fc
    .line 2509
    sget v4, Lcom/android/server/WifiService;->ICON_NETWORKS_AVAILABLE:I

    #@fe
    iget-object v5, p0, Lcom/android/server/WifiService;->mNotification:Landroid/app/Notification;

    #@100
    sget-object v6, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@102
    invoke-virtual {v2, v9, v4, v5, v6}, Landroid/app/NotificationManager;->notifyAsUser(Ljava/lang/String;ILandroid/app/Notification;Landroid/os/UserHandle;)V

    #@105
    .line 2515
    .end local v1           #details:Ljava/lang/CharSequence;
    .end local v3           #title:Ljava/lang/CharSequence;
    :goto_105
    iput-boolean p1, p0, Lcom/android/server/WifiService;->mNotificationShown:Z

    #@107
    goto/16 :goto_a

    #@109
    .line 2437
    .end local v0           #bNotiVisible:Z
    .end local v2           #notificationManager:Landroid/app/NotificationManager;
    :cond_109
    const-string v5, "WifiService"

    #@10b
    const-string v6, "WiFiOffloadingIfaceIface is null in WifiService.java"

    #@10d
    invoke-static {v5, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@110
    goto/16 :goto_51

    #@112
    .line 2440
    :cond_112
    iput-object v9, p0, Lcom/android/server/WifiService;->mWiFiOffloadingIfaceIface:Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;

    #@114
    .line 2441
    const-string v5, "WifiService"

    #@116
    new-instance v6, Ljava/lang/StringBuilder;

    #@118
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@11b
    const-string v7, "WifiMHPIfaceIface : \nuseMobileHotspot() : "

    #@11d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@120
    move-result-object v6

    #@121
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->useMobileHotspot()Z

    #@124
    move-result v7

    #@125
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@128
    move-result-object v6

    #@129
    const-string v7, "\nCONFIG_LGE_WLAN_PATH : "

    #@12b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12e
    move-result-object v6

    #@12f
    sget-boolean v7, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@131
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@134
    move-result-object v6

    #@135
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@138
    move-result-object v6

    #@139
    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@13c
    goto/16 :goto_51

    #@13e
    .restart local v0       #bNotiVisible:Z
    .restart local v2       #notificationManager:Landroid/app/NotificationManager;
    :cond_13e
    move v0, v4

    #@13f
    .line 2455
    goto/16 :goto_67

    #@141
    .line 2480
    :cond_141
    iget-object v5, p0, Lcom/android/server/WifiService;->mNotification:Landroid/app/Notification;

    #@143
    iget-object v6, p0, Lcom/android/server/WifiService;->mContext:Landroid/content/Context;

    #@145
    invoke-static {v6}, Landroid/app/TaskStackBuilder;->create(Landroid/content/Context;)Landroid/app/TaskStackBuilder;

    #@148
    move-result-object v6

    #@149
    new-instance v7, Landroid/content/Intent;

    #@14b
    const-string v8, "android.net.wifi.PICK_WIFI_NETWORK"

    #@14d
    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@150
    invoke-virtual {v6, v7}, Landroid/app/TaskStackBuilder;->addNextIntentWithParentStack(Landroid/content/Intent;)Landroid/app/TaskStackBuilder;

    #@153
    move-result-object v6

    #@154
    sget-object v7, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    #@156
    invoke-virtual {v6, v4, v4, v9, v7}, Landroid/app/TaskStackBuilder;->getPendingIntent(IILandroid/os/Bundle;Landroid/os/UserHandle;)Landroid/app/PendingIntent;

    #@159
    move-result-object v4

    #@15a
    iput-object v4, v5, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    #@15c
    goto/16 :goto_af

    #@15e
    .line 2512
    :cond_15e
    sget v4, Lcom/android/server/WifiService;->ICON_NETWORKS_AVAILABLE:I

    #@160
    sget-object v5, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@162
    invoke-virtual {v2, v9, v4, v5}, Landroid/app/NotificationManager;->cancelAsUser(Ljava/lang/String;ILandroid/os/UserHandle;)V

    #@165
    goto :goto_105
.end method

.method private shouldWifiBeEnabled()Z
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x5

    #@1
    const/4 v3, 0x2

    #@2
    const/4 v1, 0x1

    #@3
    const/4 v0, 0x0

    #@4
    .line 811
    iget-object v2, p0, Lcom/android/server/WifiService;->mAirplaneModeOn:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@6
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_38

    #@c
    .line 812
    sget-boolean v2, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@e
    if-eqz v2, :cond_2c

    #@10
    .line 813
    iget-object v2, p0, Lcom/android/server/WifiService;->mPersistWifiState:Ljava/util/concurrent/atomic/AtomicInteger;

    #@12
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    #@15
    move-result v2

    #@16
    if-ne v2, v4, :cond_19

    #@18
    .line 827
    :cond_18
    :goto_18
    return v0

    #@19
    .line 817
    :cond_19
    iget-object v2, p0, Lcom/android/server/WifiService;->mPersistWifiState:Ljava/util/concurrent/atomic/AtomicInteger;

    #@1b
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    #@1e
    move-result v2

    #@1f
    if-eq v2, v3, :cond_2a

    #@21
    iget-object v2, p0, Lcom/android/server/WifiService;->mPersistWifiState:Ljava/util/concurrent/atomic/AtomicInteger;

    #@23
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    #@26
    move-result v2

    #@27
    const/4 v3, 0x4

    #@28
    if-ne v2, v3, :cond_18

    #@2a
    :cond_2a
    move v0, v1

    #@2b
    goto :goto_18

    #@2c
    .line 821
    :cond_2c
    iget-object v2, p0, Lcom/android/server/WifiService;->mPersistWifiState:Ljava/util/concurrent/atomic/AtomicInteger;

    #@2e
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    #@31
    move-result v2

    #@32
    if-ne v2, v3, :cond_36

    #@34
    :goto_34
    move v0, v1

    #@35
    goto :goto_18

    #@36
    :cond_36
    move v1, v0

    #@37
    goto :goto_34

    #@38
    .line 824
    :cond_38
    iget-object v2, p0, Lcom/android/server/WifiService;->mPersistWifiState:Ljava/util/concurrent/atomic/AtomicInteger;

    #@3a
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    #@3d
    move-result v2

    #@3e
    if-eq v2, v4, :cond_18

    #@40
    .line 827
    iget-object v2, p0, Lcom/android/server/WifiService;->mPersistWifiState:Ljava/util/concurrent/atomic/AtomicInteger;

    #@42
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    #@45
    move-result v2

    #@46
    if-eqz v2, :cond_4a

    #@48
    :goto_48
    move v0, v1

    #@49
    goto :goto_18

    #@4a
    :cond_4a
    move v1, v0

    #@4b
    goto :goto_48
.end method

.method private showPasspointNotification(Ljava/lang/String;I)V
    .registers 11
    .parameter "hs20Ssid"
    .parameter "roamingInd"

    #@0
    .prologue
    const/4 v7, 0x2

    #@1
    .line 2361
    iget-object v3, p0, Lcom/android/server/WifiService;->mContext:Landroid/content/Context;

    #@3
    const-string v4, "notification"

    #@5
    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@8
    move-result-object v1

    #@9
    check-cast v1, Landroid/app/NotificationManager;

    #@b
    .line 2362
    .local v1, manager:Landroid/app/NotificationManager;
    const/4 v2, 0x0

    #@c
    .line 2363
    .local v2, notification:Landroid/app/Notification;
    const-string v3, "WifiService"

    #@e
    new-instance v4, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v5, "showPasspointNotification [mPasspointLayoutNotificationShown]  ="

    #@15
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v4

    #@19
    iget-boolean v5, p0, Lcom/android/server/WifiService;->mPasspointLayoutNotificationShown:Z

    #@1b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v4

    #@1f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v4

    #@23
    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 2364
    const v0, 0x20206dd

    #@29
    .line 2366
    .local v0, iconRes:I
    if-ne p2, v7, :cond_9e

    #@2b
    .line 2367
    const v0, 0x20206de

    #@2e
    .line 2371
    :cond_2e
    :goto_2e
    iget-boolean v3, p0, Lcom/android/server/WifiService;->mPasspointLayoutNotificationShown:Z

    #@30
    if-nez v3, :cond_9d

    #@32
    .line 2372
    new-instance v3, Landroid/app/Notification$BigTextStyle;

    #@34
    new-instance v4, Landroid/app/Notification$Builder;

    #@36
    iget-object v5, p0, Lcom/android/server/WifiService;->mContext:Landroid/content/Context;

    #@38
    invoke-direct {v4, v5}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    #@3b
    new-instance v5, Ljava/lang/StringBuilder;

    #@3d
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@40
    const-string v6, " "

    #@42
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v5

    #@46
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v5

    #@4a
    const-string v6, " connected via passpoint"

    #@4c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v5

    #@50
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@53
    move-result-object v5

    #@54
    invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    #@57
    move-result-object v4

    #@58
    const-string v5, "Content text"

    #@5a
    invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    #@5d
    move-result-object v4

    #@5e
    invoke-virtual {v4, v0}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    #@61
    move-result-object v4

    #@62
    new-instance v5, Ljava/lang/StringBuilder;

    #@64
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@67
    const-string v6, " "

    #@69
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v5

    #@6d
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v5

    #@71
    const-string v6, " connected via passpoint"

    #@73
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v5

    #@77
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7a
    move-result-object v5

    #@7b
    invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    #@7e
    move-result-object v4

    #@7f
    invoke-virtual {v4, v7}, Landroid/app/Notification$Builder;->setPriority(I)Landroid/app/Notification$Builder;

    #@82
    move-result-object v4

    #@83
    invoke-direct {v3, v4}, Landroid/app/Notification$BigTextStyle;-><init>(Landroid/app/Notification$Builder;)V

    #@86
    const-string v4, "Touch to set up."

    #@88
    invoke-virtual {v3, v4}, Landroid/app/Notification$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    #@8b
    move-result-object v3

    #@8c
    invoke-virtual {v3}, Landroid/app/Notification$BigTextStyle;->build()Landroid/app/Notification;

    #@8f
    move-result-object v2

    #@90
    .line 2381
    iget v3, v2, Landroid/app/Notification;->flags:I

    #@92
    or-int/lit8 v3, v3, 0x20

    #@94
    iput v3, v2, Landroid/app/Notification;->flags:I

    #@96
    .line 2382
    const/4 v3, 0x0

    #@97
    invoke-virtual {v1, v3, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    #@9a
    .line 2383
    const/4 v3, 0x1

    #@9b
    iput-boolean v3, p0, Lcom/android/server/WifiService;->mPasspointLayoutNotificationShown:Z

    #@9d
    .line 2385
    :cond_9d
    return-void

    #@9e
    .line 2368
    :cond_9e
    if-nez p2, :cond_2e

    #@a0
    .line 2369
    const v0, 0x20206de

    #@a3
    goto :goto_2e
.end method

.method private testAndClearWifiSavedState()Z
    .registers 7

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    .line 788
    iget-object v4, p0, Lcom/android/server/WifiService;->mContext:Landroid/content/Context;

    #@4
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7
    move-result-object v0

    #@8
    .line 789
    .local v0, cr:Landroid/content/ContentResolver;
    const/4 v1, 0x0

    #@9
    .line 791
    .local v1, wifiSavedState:I
    :try_start_9
    const-string v4, "wifi_saved_state"

    #@b
    invoke-static {v0, v4}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    #@e
    move-result v1

    #@f
    .line 792
    if-ne v1, v2, :cond_17

    #@11
    .line 793
    const-string v4, "wifi_saved_state"

    #@13
    const/4 v5, 0x0

    #@14
    invoke-static {v0, v4, v5}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_17
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_9 .. :try_end_17} :catch_1c

    #@17
    .line 797
    :cond_17
    :goto_17
    if-ne v1, v2, :cond_1a

    #@19
    :goto_19
    return v2

    #@1a
    :cond_1a
    move v2, v3

    #@1b
    goto :goto_19

    #@1c
    .line 794
    :catch_1c
    move-exception v4

    #@1d
    goto :goto_17
.end method

.method private declared-synchronized unregBrdcastReceiver()V
    .registers 3

    #@0
    .prologue
    .line 2551
    monitor-enter p0

    #@1
    :try_start_1
    const-string v0, "WifiService"

    #@3
    const-string v1, "unregBrdcastReceiver"

    #@5
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 2552
    iget-boolean v0, p0, Lcom/android/server/WifiService;->mIsReceiverRegistered:Z

    #@a
    if-eqz v0, :cond_16

    #@c
    .line 2553
    iget-object v0, p0, Lcom/android/server/WifiService;->mContext:Landroid/content/Context;

    #@e
    iget-object v1, p0, Lcom/android/server/WifiService;->mReceiver:Landroid/content/BroadcastReceiver;

    #@10
    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@13
    .line 2554
    const/4 v0, 0x0

    #@14
    iput-boolean v0, p0, Lcom/android/server/WifiService;->mIsReceiverRegistered:Z

    #@16
    .line 2556
    :cond_16
    const/4 v0, 0x0

    #@17
    iput-boolean v0, p0, Lcom/android/server/WifiService;->mWifiEnabled:Z
    :try_end_19
    .catchall {:try_start_1 .. :try_end_19} :catchall_1b

    #@19
    .line 2557
    monitor-exit p0

    #@1a
    return-void

    #@1b
    .line 2551
    :catchall_1b
    move-exception v0

    #@1c
    monitor-exit p0

    #@1d
    throw v0
.end method

.method private updateWifiState()V
    .registers 9

    #@0
    .prologue
    const/4 v7, 0x2

    #@1
    const/4 v4, 0x1

    #@2
    const/4 v3, 0x0

    #@3
    .line 1687
    iget-object v5, p0, Lcom/android/server/WifiService;->mLocks:Lcom/android/server/WifiService$LockList;

    #@5
    invoke-static {v5}, Lcom/android/server/WifiService$LockList;->access$3600(Lcom/android/server/WifiService$LockList;)Z

    #@8
    move-result v0

    #@9
    .line 1688
    .local v0, lockHeld:Z
    const/4 v1, 0x1

    #@a
    .line 1691
    .local v1, strongestLockMode:I
    iget-boolean v5, p0, Lcom/android/server/WifiService;->mEmergencyCallbackMode:Z

    #@c
    if-eqz v5, :cond_54

    #@e
    .line 1692
    const/4 v2, 0x0

    #@f
    .line 1697
    .local v2, wifiShouldBeStarted:Z
    :goto_f
    if-eqz v0, :cond_17

    #@11
    .line 1698
    iget-object v5, p0, Lcom/android/server/WifiService;->mLocks:Lcom/android/server/WifiService$LockList;

    #@13
    invoke-static {v5}, Lcom/android/server/WifiService$LockList;->access$3700(Lcom/android/server/WifiService$LockList;)I

    #@16
    move-result v1

    #@17
    .line 1701
    :cond_17
    iget-boolean v5, p0, Lcom/android/server/WifiService;->mDeviceIdle:Z

    #@19
    if-nez v5, :cond_1e

    #@1b
    if-ne v1, v7, :cond_1e

    #@1d
    .line 1702
    const/4 v1, 0x1

    #@1e
    .line 1706
    :cond_1e
    iget-object v5, p0, Lcom/android/server/WifiService;->mAirplaneModeOn:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@20
    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    #@23
    move-result v5

    #@24
    if-eqz v5, :cond_2c

    #@26
    .line 1707
    iget-object v5, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@28
    const/4 v6, 0x0

    #@29
    invoke-virtual {v5, v6, v3}, Landroid/net/wifi/WifiStateMachine;->setWifiApEnabled(Landroid/net/wifi/WifiConfiguration;Z)V

    #@2c
    .line 1710
    :cond_2c
    invoke-direct {p0}, Lcom/android/server/WifiService;->shouldWifiBeEnabled()Z

    #@2f
    move-result v5

    #@30
    if-eqz v5, :cond_77

    #@32
    .line 1711
    if-eqz v2, :cond_62

    #@34
    .line 1712
    invoke-direct {p0}, Lcom/android/server/WifiService;->reportStartWorkSource()V

    #@37
    .line 1713
    iget-object v5, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@39
    invoke-virtual {v5, v4}, Landroid/net/wifi/WifiStateMachine;->setWifiEnabled(Z)V

    #@3c
    .line 1714
    iget-object v6, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@3e
    if-ne v1, v7, :cond_5e

    #@40
    move v5, v4

    #@41
    :goto_41
    invoke-virtual {v6, v5}, Landroid/net/wifi/WifiStateMachine;->setScanOnlyMode(Z)V

    #@44
    .line 1716
    iget-object v5, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@46
    iget-boolean v6, p0, Lcom/android/server/WifiService;->mEmergencyCallbackMode:Z

    #@48
    invoke-virtual {v5, v4, v6}, Landroid/net/wifi/WifiStateMachine;->setDriverStart(ZZ)V

    #@4b
    .line 1717
    iget-object v5, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@4d
    const/4 v6, 0x3

    #@4e
    if-ne v1, v6, :cond_60

    #@50
    :goto_50
    invoke-virtual {v5, v4}, Landroid/net/wifi/WifiStateMachine;->setHighPerfModeEnabled(Z)V

    #@53
    .line 1734
    :cond_53
    :goto_53
    return-void

    #@54
    .line 1694
    .end local v2           #wifiShouldBeStarted:Z
    :cond_54
    iget-boolean v5, p0, Lcom/android/server/WifiService;->mDeviceIdle:Z

    #@56
    if-eqz v5, :cond_5a

    #@58
    if-eqz v0, :cond_5c

    #@5a
    :cond_5a
    move v2, v4

    #@5b
    .restart local v2       #wifiShouldBeStarted:Z
    :goto_5b
    goto :goto_f

    #@5c
    .end local v2           #wifiShouldBeStarted:Z
    :cond_5c
    move v2, v3

    #@5d
    goto :goto_5b

    #@5e
    .restart local v2       #wifiShouldBeStarted:Z
    :cond_5e
    move v5, v3

    #@5f
    .line 1714
    goto :goto_41

    #@60
    :cond_60
    move v4, v3

    #@61
    .line 1717
    goto :goto_50

    #@62
    .line 1720
    :cond_62
    iget-object v4, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@64
    iget-boolean v5, p0, Lcom/android/server/WifiService;->mEmergencyCallbackMode:Z

    #@66
    invoke-virtual {v4, v3, v5}, Landroid/net/wifi/WifiStateMachine;->setDriverStart(ZZ)V

    #@69
    .line 1725
    sget-boolean v3, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@6b
    if-eqz v3, :cond_53

    #@6d
    .line 1726
    iget-object v3, p0, Lcom/android/server/WifiService;->mWifiOffDelayIfNotUsed:Lcom/lge/wifi_iface/WifiOffDelayIfNotUsedIface;

    #@6f
    if-eqz v3, :cond_53

    #@71
    .line 1727
    iget-object v3, p0, Lcom/android/server/WifiService;->mWifiOffDelayIfNotUsed:Lcom/lge/wifi_iface/WifiOffDelayIfNotUsedIface;

    #@73
    invoke-interface {v3}, Lcom/lge/wifi_iface/WifiOffDelayIfNotUsedIface;->stopMonitoring()V

    #@76
    goto :goto_53

    #@77
    .line 1732
    :cond_77
    iget-object v4, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@79
    invoke-virtual {v4, v3}, Landroid/net/wifi/WifiStateMachine;->setWifiEnabled(Z)V

    #@7c
    goto :goto_53
.end method


# virtual methods
.method public acquireMulticastLock(Landroid/os/IBinder;Ljava/lang/String;)V
    .registers 8
    .parameter "binder"
    .parameter "tag"

    #@0
    .prologue
    .line 2159
    invoke-direct {p0}, Lcom/android/server/WifiService;->enforceMulticastChangePermission()V

    #@3
    .line 2161
    iget-object v3, p0, Lcom/android/server/WifiService;->mMulticasters:Ljava/util/List;

    #@5
    monitor-enter v3

    #@6
    .line 2162
    :try_start_6
    iget v2, p0, Lcom/android/server/WifiService;->mMulticastEnabled:I

    #@8
    add-int/lit8 v2, v2, 0x1

    #@a
    iput v2, p0, Lcom/android/server/WifiService;->mMulticastEnabled:I

    #@c
    .line 2163
    iget-object v2, p0, Lcom/android/server/WifiService;->mMulticasters:Ljava/util/List;

    #@e
    new-instance v4, Lcom/android/server/WifiService$Multicaster;

    #@10
    invoke-direct {v4, p0, p2, p1}, Lcom/android/server/WifiService$Multicaster;-><init>(Lcom/android/server/WifiService;Ljava/lang/String;Landroid/os/IBinder;)V

    #@13
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@16
    .line 2168
    iget-object v2, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@18
    invoke-virtual {v2}, Landroid/net/wifi/WifiStateMachine;->stopFilteringMulticastV4Packets()V

    #@1b
    .line 2169
    monitor-exit v3
    :try_end_1c
    .catchall {:try_start_6 .. :try_end_1c} :catchall_35

    #@1c
    .line 2171
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@1f
    move-result v1

    #@20
    .line 2172
    .local v1, uid:I
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@23
    move-result-wide v2

    #@24
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@27
    move-result-object v0

    #@28
    .line 2174
    .local v0, ident:Ljava/lang/Long;
    :try_start_28
    iget-object v2, p0, Lcom/android/server/WifiService;->mBatteryStats:Lcom/android/internal/app/IBatteryStats;

    #@2a
    invoke-interface {v2, v1}, Lcom/android/internal/app/IBatteryStats;->noteWifiMulticastEnabled(I)V
    :try_end_2d
    .catchall {:try_start_28 .. :try_end_2d} :catchall_38
    .catch Landroid/os/RemoteException; {:try_start_28 .. :try_end_2d} :catch_41

    #@2d
    .line 2177
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    #@30
    move-result-wide v2

    #@31
    :goto_31
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@34
    .line 2179
    return-void

    #@35
    .line 2169
    .end local v0           #ident:Ljava/lang/Long;
    .end local v1           #uid:I
    :catchall_35
    move-exception v2

    #@36
    :try_start_36
    monitor-exit v3
    :try_end_37
    .catchall {:try_start_36 .. :try_end_37} :catchall_35

    #@37
    throw v2

    #@38
    .line 2177
    .restart local v0       #ident:Ljava/lang/Long;
    .restart local v1       #uid:I
    :catchall_38
    move-exception v2

    #@39
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    #@3c
    move-result-wide v3

    #@3d
    invoke-static {v3, v4}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@40
    throw v2

    #@41
    .line 2175
    :catch_41
    move-exception v2

    #@42
    .line 2177
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    #@45
    move-result-wide v2

    #@46
    goto :goto_31
.end method

.method public acquireWifiLock(Landroid/os/IBinder;ILjava/lang/String;Landroid/os/WorkSource;)Z
    .registers 11
    .parameter "binder"
    .parameter "lockMode"
    .parameter "tag"
    .parameter "ws"

    #@0
    .prologue
    .line 1905
    iget-object v1, p0, Lcom/android/server/WifiService;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "android.permission.WAKE_LOCK"

    #@4
    const/4 v3, 0x0

    #@5
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 1906
    const/4 v1, 0x1

    #@9
    if-eq p2, v1, :cond_42

    #@b
    const/4 v1, 0x2

    #@c
    if-eq p2, v1, :cond_42

    #@e
    const/4 v1, 0x3

    #@f
    if-eq p2, v1, :cond_42

    #@11
    .line 1909
    const-string v1, "WifiService"

    #@13
    new-instance v2, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v3, "Illegal argument, lockMode= "

    #@1a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@21
    move-result-object v2

    #@22
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v2

    #@26
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 1910
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@2b
    new-instance v2, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    const-string v3, "lockMode="

    #@32
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v2

    #@36
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@39
    move-result-object v2

    #@3a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d
    move-result-object v2

    #@3e
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@41
    throw v1

    #@42
    .line 1913
    :cond_42
    if-eqz p4, :cond_4b

    #@44
    invoke-virtual {p4}, Landroid/os/WorkSource;->size()I

    #@47
    move-result v1

    #@48
    if-nez v1, :cond_4b

    #@4a
    .line 1914
    const/4 p4, 0x0

    #@4b
    .line 1916
    :cond_4b
    if-eqz p4, :cond_58

    #@4d
    .line 1917
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@50
    move-result v1

    #@51
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@54
    move-result v2

    #@55
    invoke-virtual {p0, v1, v2}, Lcom/android/server/WifiService;->enforceWakeSourcePermission(II)V

    #@58
    .line 1919
    :cond_58
    if-nez p4, :cond_63

    #@5a
    .line 1920
    new-instance p4, Landroid/os/WorkSource;

    #@5c
    .end local p4
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@5f
    move-result v1

    #@60
    invoke-direct {p4, v1}, Landroid/os/WorkSource;-><init>(I)V

    #@63
    .line 1922
    .restart local p4
    :cond_63
    new-instance v0, Lcom/android/server/WifiService$WifiLock;

    #@65
    move-object v1, p0

    #@66
    move v2, p2

    #@67
    move-object v3, p3

    #@68
    move-object v4, p1

    #@69
    move-object v5, p4

    #@6a
    invoke-direct/range {v0 .. v5}, Lcom/android/server/WifiService$WifiLock;-><init>(Lcom/android/server/WifiService;ILjava/lang/String;Landroid/os/IBinder;Landroid/os/WorkSource;)V

    #@6d
    .line 1923
    .local v0, wifiLock:Lcom/android/server/WifiService$WifiLock;
    iget-object v2, p0, Lcom/android/server/WifiService;->mLocks:Lcom/android/server/WifiService$LockList;

    #@6f
    monitor-enter v2

    #@70
    .line 1924
    :try_start_70
    invoke-direct {p0, v0}, Lcom/android/server/WifiService;->acquireWifiLockLocked(Lcom/android/server/WifiService$WifiLock;)Z

    #@73
    move-result v1

    #@74
    monitor-exit v2

    #@75
    return v1

    #@76
    .line 1925
    :catchall_76
    move-exception v1

    #@77
    monitor-exit v2
    :try_end_78
    .catchall {:try_start_70 .. :try_end_78} :catchall_76

    #@78
    throw v1
.end method

.method public addOrUpdateNetwork(Landroid/net/wifi/WifiConfiguration;)I
    .registers 6
    .parameter "config"

    #@0
    .prologue
    const/4 v0, -0x1

    #@1
    .line 1171
    const-string v1, "WifiService"

    #@3
    new-instance v2, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v3, "addOrUpdateNetwork pid="

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@11
    move-result v3

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    const-string v3, ", uid="

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@1f
    move-result v3

    #@20
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 1174
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@2e
    move-result-object v1

    #@2f
    if-eqz v1, :cond_55

    #@31
    .line 1175
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@34
    move-result-object v1

    #@35
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@38
    move-result v2

    #@39
    invoke-interface {v1, p1, v0, v2}, Lcom/lge/cappuccino/IMdm;->checkDisabledWifiSecurity(Landroid/net/wifi/WifiConfiguration;II)Z

    #@3c
    move-result v1

    #@3d
    if-eqz v1, :cond_40

    #@3f
    .line 1201
    :cond_3f
    :goto_3f
    return v0

    #@40
    .line 1180
    :cond_40
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@43
    move-result-object v1

    #@44
    if-eqz v1, :cond_55

    #@46
    .line 1181
    iget v1, p1, Landroid/net/wifi/WifiConfiguration;->networkId:I

    #@48
    if-ne v1, v0, :cond_73

    #@4a
    .line 1182
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@4d
    move-result-object v1

    #@4e
    const/4 v2, 0x1

    #@4f
    invoke-interface {v1, v2}, Lcom/lge/cappuccino/IMdm;->getAllowWiFiProfileManagement(I)Z

    #@52
    move-result v1

    #@53
    if-eqz v1, :cond_3f

    #@55
    .line 1194
    :cond_55
    invoke-direct {p0}, Lcom/android/server/WifiService;->enforceChangePermission()V

    #@58
    .line 1196
    sget-boolean v1, Lcom/lge/config/ThreelmMdmConfig;->THREELM_MDM:Z

    #@5a
    if-eqz v1, :cond_66

    #@5c
    iget-object v1, p0, Lcom/android/server/WifiService;->mDeviceManager:Lcom/android/server/DeviceManager3LMService;

    #@5e
    iget-object v2, p1, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    #@60
    invoke-virtual {v1, v2}, Lcom/android/server/DeviceManager3LMService;->isSsidAllowed(Ljava/lang/String;)Z

    #@63
    move-result v1

    #@64
    if-eqz v1, :cond_3f

    #@66
    .line 1197
    :cond_66
    iget-object v1, p0, Lcom/android/server/WifiService;->mWifiStateMachineChannel:Lcom/android/internal/util/AsyncChannel;

    #@68
    if-eqz v1, :cond_7f

    #@6a
    .line 1198
    iget-object v0, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@6c
    iget-object v1, p0, Lcom/android/server/WifiService;->mWifiStateMachineChannel:Lcom/android/internal/util/AsyncChannel;

    #@6e
    invoke-virtual {v0, v1, p1}, Landroid/net/wifi/WifiStateMachine;->syncAddOrUpdateNetwork(Lcom/android/internal/util/AsyncChannel;Landroid/net/wifi/WifiConfiguration;)I

    #@71
    move-result v0

    #@72
    goto :goto_3f

    #@73
    .line 1186
    :cond_73
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@76
    move-result-object v1

    #@77
    const/4 v2, 0x2

    #@78
    invoke-interface {v1, v2}, Lcom/lge/cappuccino/IMdm;->getAllowWiFiProfileManagement(I)Z

    #@7b
    move-result v1

    #@7c
    if-nez v1, :cond_55

    #@7e
    goto :goto_3f

    #@7f
    .line 1200
    :cond_7f
    const-string v1, "WifiService"

    #@81
    const-string v2, "mWifiStateMachineChannel is not initialized"

    #@83
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@86
    goto :goto_3f
.end method

.method public addToBlacklist(Ljava/lang/String;)V
    .registers 3
    .parameter "bssid"

    #@0
    .prologue
    .line 1451
    invoke-direct {p0}, Lcom/android/server/WifiService;->enforceChangePermission()V

    #@3
    .line 1453
    iget-object v0, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@5
    invoke-virtual {v0, p1}, Landroid/net/wifi/WifiStateMachine;->addToBlacklist(Ljava/lang/String;)V

    #@8
    .line 1454
    return-void
.end method

.method public captivePortalCheckComplete()V
    .registers 2

    #@0
    .prologue
    .line 1429
    invoke-direct {p0}, Lcom/android/server/WifiService;->enforceConnectivityInternalPermission()V

    #@3
    .line 1430
    iget-object v0, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@5
    invoke-virtual {v0}, Landroid/net/wifi/WifiStateMachine;->captivePortalCheckComplete()V

    #@8
    .line 1431
    return-void
.end method

.method public checkAndStartWifi()V
    .registers 6

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 748
    sget-boolean v2, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@3
    if-eqz v2, :cond_15

    #@5
    .line 749
    iget-object v2, p0, Lcom/android/server/WifiService;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@7
    invoke-interface {v2}, Lcom/lge/wifi_iface/WifiServiceExtIface;->startWifiDelay()Z

    #@a
    move-result v2

    #@b
    if-nez v2, :cond_15

    #@d
    .line 750
    const-string v1, "WifiService"

    #@f
    const-string v2, "WifiService starting up with delayed Wi-Fi"

    #@11
    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@14
    .line 785
    :goto_14
    return-void

    #@15
    .line 755
    :cond_15
    iget-object v2, p0, Lcom/android/server/WifiService;->mAirplaneModeOn:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@17
    invoke-direct {p0}, Lcom/android/server/WifiService;->isAirplaneModeOn()Z

    #@1a
    move-result v3

    #@1b
    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    #@1e
    .line 756
    iget-object v2, p0, Lcom/android/server/WifiService;->mPersistWifiState:Ljava/util/concurrent/atomic/AtomicInteger;

    #@20
    invoke-direct {p0}, Lcom/android/server/WifiService;->getPersistedWifiState()I

    #@23
    move-result v3

    #@24
    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    #@27
    .line 758
    invoke-direct {p0}, Lcom/android/server/WifiService;->shouldWifiBeEnabled()Z

    #@2a
    move-result v2

    #@2b
    if-nez v2, :cond_33

    #@2d
    invoke-direct {p0}, Lcom/android/server/WifiService;->testAndClearWifiSavedState()Z

    #@30
    move-result v2

    #@31
    if-eqz v2, :cond_7e

    #@33
    :cond_33
    const/4 v0, 0x1

    #@34
    .line 759
    .local v0, wifiEnabled:Z
    :goto_34
    const-string v3, "WifiService"

    #@36
    new-instance v2, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v4, "WifiService starting up with Wi-Fi "

    #@3d
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v4

    #@41
    if-eqz v0, :cond_80

    #@43
    const-string v2, "enabled"

    #@45
    :goto_45
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v2

    #@49
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c
    move-result-object v2

    #@4d
    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@50
    .line 764
    if-eqz v0, :cond_55

    #@52
    invoke-virtual {p0, v0}, Lcom/android/server/WifiService;->setWifiEnabled(Z)Z

    #@55
    .line 766
    :cond_55
    iget-object v2, p0, Lcom/android/server/WifiService;->mContext:Landroid/content/Context;

    #@57
    invoke-static {v2}, Landroid/net/wifi/WifiWatchdogStateMachine;->makeWifiWatchdogStateMachine(Landroid/content/Context;)Landroid/net/wifi/WifiWatchdogStateMachine;

    #@5a
    move-result-object v2

    #@5b
    iput-object v2, p0, Lcom/android/server/WifiService;->mWifiWatchdogStateMachine:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@5d
    .line 770
    sget-boolean v2, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@5f
    if-eqz v2, :cond_8b

    #@61
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->useWiFiOffloading()Z

    #@64
    move-result v2

    #@65
    if-eqz v2, :cond_8b

    #@67
    .line 771
    invoke-static {}, Lcom/lge/wifi_iface/WifiIfaceManager;->getWiFiOffloadingIfaceIface()Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;

    #@6a
    move-result-object v2

    #@6b
    iput-object v2, p0, Lcom/android/server/WifiService;->mWiFiOffloadingIfaceIface:Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;

    #@6d
    .line 772
    iget-object v2, p0, Lcom/android/server/WifiService;->mWiFiOffloadingIfaceIface:Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;

    #@6f
    if-eqz v2, :cond_83

    #@71
    .line 773
    iget-object v2, p0, Lcom/android/server/WifiService;->mWiFiOffloadingIfaceIface:Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;

    #@73
    invoke-interface {v2, v1}, Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;->resetWifioffloadTimerReminder(I)Z

    #@76
    .line 774
    const-string v1, "WifiService"

    #@78
    const-string v2, "resetWifioffloadTimerReminder(0) in WifiService.java"

    #@7a
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7d
    goto :goto_14

    #@7e
    .end local v0           #wifiEnabled:Z
    :cond_7e
    move v0, v1

    #@7f
    .line 758
    goto :goto_34

    #@80
    .line 759
    .restart local v0       #wifiEnabled:Z
    :cond_80
    const-string v2, "disabled"

    #@82
    goto :goto_45

    #@83
    .line 776
    :cond_83
    const-string v1, "WifiService"

    #@85
    const-string v2, "WiFiOffloadingIfaceIface is null in WifiService.java"

    #@87
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@8a
    goto :goto_14

    #@8b
    .line 779
    :cond_8b
    const/4 v1, 0x0

    #@8c
    iput-object v1, p0, Lcom/android/server/WifiService;->mWiFiOffloadingIfaceIface:Lcom/lge/wifi_iface/WiFiOffloadingIfaceIface;

    #@8e
    .line 780
    const-string v1, "WifiService"

    #@90
    new-instance v2, Ljava/lang/StringBuilder;

    #@92
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@95
    const-string v3, "Couldn\'t get WifiMHPIfaceIface : \nuseMobileHotspot() : "

    #@97
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v2

    #@9b
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->useMobileHotspot()Z

    #@9e
    move-result v3

    #@9f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v2

    #@a3
    const-string v3, "\nCONFIG_LGE_WLAN_PATH : "

    #@a5
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v2

    #@a9
    sget-boolean v3, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@ab
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v2

    #@af
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b2
    move-result-object v2

    #@b3
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@b6
    goto/16 :goto_14
.end method

.method public checkAndStartWifiExt()V
    .registers 1

    #@0
    .prologue
    .line 2568
    invoke-virtual {p0}, Lcom/android/server/WifiService;->checkAndStartWifi()V

    #@3
    .line 2569
    return-void
.end method

.method public clearBlacklist()V
    .registers 2

    #@0
    .prologue
    .line 1461
    invoke-direct {p0}, Lcom/android/server/WifiService;->enforceChangePermission()V

    #@3
    .line 1463
    iget-object v0, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@5
    invoke-virtual {v0}, Landroid/net/wifi/WifiStateMachine;->clearBlacklist()V

    #@8
    .line 1464
    return-void
.end method

.method public disableNetwork(I)Z
    .registers 5
    .parameter "netId"

    #@0
    .prologue
    .line 1268
    const-string v0, "WifiService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "disableNetwork pid="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@10
    move-result v2

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    const-string v2, ", uid="

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@1e
    move-result v2

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 1270
    invoke-direct {p0}, Lcom/android/server/WifiService;->enforceChangePermission()V

    #@2d
    .line 1271
    iget-object v0, p0, Lcom/android/server/WifiService;->mWifiStateMachineChannel:Lcom/android/internal/util/AsyncChannel;

    #@2f
    if-eqz v0, :cond_3a

    #@31
    .line 1272
    iget-object v0, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@33
    iget-object v1, p0, Lcom/android/server/WifiService;->mWifiStateMachineChannel:Lcom/android/internal/util/AsyncChannel;

    #@35
    invoke-virtual {v0, v1, p1}, Landroid/net/wifi/WifiStateMachine;->syncDisableNetwork(Lcom/android/internal/util/AsyncChannel;I)Z

    #@38
    move-result v0

    #@39
    .line 1275
    :goto_39
    return v0

    #@3a
    .line 1274
    :cond_3a
    const-string v0, "WifiService"

    #@3c
    const-string v1, "mWifiStateMachineChannel is not initialized"

    #@3e
    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@41
    .line 1275
    const/4 v0, 0x0

    #@42
    goto :goto_39
.end method

.method public disconnect()V
    .registers 4

    #@0
    .prologue
    .line 1122
    const-string v0, "WifiService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "disconnect pid="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@10
    move-result v2

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    const-string v2, ", uid="

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@1e
    move-result v2

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 1124
    invoke-direct {p0}, Lcom/android/server/WifiService;->enforceChangePermission()V

    #@2d
    .line 1125
    iget-object v0, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@2f
    invoke-virtual {v0}, Landroid/net/wifi/WifiStateMachine;->disconnectCommand()V

    #@32
    .line 1126
    return-void
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 12
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 1773
    iget-object v3, p0, Lcom/android/server/WifiService;->mContext:Landroid/content/Context;

    #@3
    const-string v4, "android.permission.DUMP"

    #@5
    invoke-virtual {v3, v4}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@8
    move-result v3

    #@9
    if-eqz v3, :cond_34

    #@b
    .line 1775
    new-instance v3, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v4, "Permission Denial: can\'t dump WifiService from from pid="

    #@12
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v3

    #@16
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@19
    move-result v4

    #@1a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    const-string v4, ", uid="

    #@20
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v3

    #@24
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@27
    move-result v4

    #@28
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v3

    #@2c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v3

    #@30
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@33
    .line 1818
    :goto_33
    return-void

    #@34
    .line 1780
    :cond_34
    new-instance v3, Ljava/lang/StringBuilder;

    #@36
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    const-string v4, "Wi-Fi is "

    #@3b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v3

    #@3f
    iget-object v4, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@41
    invoke-virtual {v4}, Landroid/net/wifi/WifiStateMachine;->syncGetWifiStateByName()Ljava/lang/String;

    #@44
    move-result-object v4

    #@45
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v3

    #@49
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c
    move-result-object v3

    #@4d
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@50
    .line 1781
    new-instance v3, Ljava/lang/StringBuilder;

    #@52
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@55
    const-string v4, "Stay-awake conditions: "

    #@57
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v3

    #@5b
    iget-object v4, p0, Lcom/android/server/WifiService;->mContext:Landroid/content/Context;

    #@5d
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@60
    move-result-object v4

    #@61
    const-string v5, "stay_on_while_plugged_in"

    #@63
    invoke-static {v4, v5, v7}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@66
    move-result v4

    #@67
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v3

    #@6b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6e
    move-result-object v3

    #@6f
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@72
    .line 1784
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    #@75
    .line 1786
    const-string v3, "Internal state:"

    #@77
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@7a
    .line 1787
    iget-object v3, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@7c
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@7f
    .line 1788
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    #@82
    .line 1789
    const-string v3, "Latest scan results:"

    #@84
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@87
    .line 1790
    iget-object v3, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@89
    invoke-virtual {v3}, Landroid/net/wifi/WifiStateMachine;->syncGetScanResultsList()Ljava/util/List;

    #@8c
    move-result-object v2

    #@8d
    .line 1791
    .local v2, scanResults:Ljava/util/List;,"Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    if-eqz v2, :cond_da

    #@8f
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@92
    move-result v3

    #@93
    if-eqz v3, :cond_da

    #@95
    .line 1792
    const-string v3, "  BSSID              Frequency   RSSI  Flags             SSID"

    #@97
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@9a
    .line 1793
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@9d
    move-result-object v0

    #@9e
    .local v0, i$:Ljava/util/Iterator;
    :goto_9e
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@a1
    move-result v3

    #@a2
    if-eqz v3, :cond_da

    #@a4
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@a7
    move-result-object v1

    #@a8
    check-cast v1, Landroid/net/wifi/ScanResult;

    #@aa
    .line 1794
    .local v1, r:Landroid/net/wifi/ScanResult;
    const-string v4, "  %17s  %9d  %5d  %-16s  %s%n"

    #@ac
    const/4 v3, 0x5

    #@ad
    new-array v5, v3, [Ljava/lang/Object;

    #@af
    iget-object v3, v1, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    #@b1
    aput-object v3, v5, v7

    #@b3
    const/4 v3, 0x1

    #@b4
    iget v6, v1, Landroid/net/wifi/ScanResult;->frequency:I

    #@b6
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@b9
    move-result-object v6

    #@ba
    aput-object v6, v5, v3

    #@bc
    const/4 v3, 0x2

    #@bd
    iget v6, v1, Landroid/net/wifi/ScanResult;->level:I

    #@bf
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@c2
    move-result-object v6

    #@c3
    aput-object v6, v5, v3

    #@c5
    const/4 v3, 0x3

    #@c6
    iget-object v6, v1, Landroid/net/wifi/ScanResult;->capabilities:Ljava/lang/String;

    #@c8
    aput-object v6, v5, v3

    #@ca
    const/4 v6, 0x4

    #@cb
    iget-object v3, v1, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    #@cd
    if-nez v3, :cond_d7

    #@cf
    const-string v3, ""

    #@d1
    :goto_d1
    aput-object v3, v5, v6

    #@d3
    invoke-virtual {p2, v4, v5}, Ljava/io/PrintWriter;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    #@d6
    goto :goto_9e

    #@d7
    :cond_d7
    iget-object v3, v1, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    #@d9
    goto :goto_d1

    #@da
    .line 1802
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #r:Landroid/net/wifi/ScanResult;
    :cond_da
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    #@dd
    .line 1803
    new-instance v3, Ljava/lang/StringBuilder;

    #@df
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@e2
    const-string v4, "Locks acquired: "

    #@e4
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e7
    move-result-object v3

    #@e8
    iget v4, p0, Lcom/android/server/WifiService;->mFullLocksAcquired:I

    #@ea
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ed
    move-result-object v3

    #@ee
    const-string v4, " full, "

    #@f0
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f3
    move-result-object v3

    #@f4
    iget v4, p0, Lcom/android/server/WifiService;->mFullHighPerfLocksAcquired:I

    #@f6
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f9
    move-result-object v3

    #@fa
    const-string v4, " full high perf, "

    #@fc
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ff
    move-result-object v3

    #@100
    iget v4, p0, Lcom/android/server/WifiService;->mScanLocksAcquired:I

    #@102
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@105
    move-result-object v3

    #@106
    const-string v4, " scan"

    #@108
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10b
    move-result-object v3

    #@10c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10f
    move-result-object v3

    #@110
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@113
    .line 1806
    new-instance v3, Ljava/lang/StringBuilder;

    #@115
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@118
    const-string v4, "Locks released: "

    #@11a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11d
    move-result-object v3

    #@11e
    iget v4, p0, Lcom/android/server/WifiService;->mFullLocksReleased:I

    #@120
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@123
    move-result-object v3

    #@124
    const-string v4, " full, "

    #@126
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@129
    move-result-object v3

    #@12a
    iget v4, p0, Lcom/android/server/WifiService;->mFullHighPerfLocksReleased:I

    #@12c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12f
    move-result-object v3

    #@130
    const-string v4, " full high perf, "

    #@132
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@135
    move-result-object v3

    #@136
    iget v4, p0, Lcom/android/server/WifiService;->mScanLocksReleased:I

    #@138
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13b
    move-result-object v3

    #@13c
    const-string v4, " scan"

    #@13e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@141
    move-result-object v3

    #@142
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@145
    move-result-object v3

    #@146
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@149
    .line 1809
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    #@14c
    .line 1810
    const-string v3, "Locks held:"

    #@14e
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@151
    .line 1811
    iget-object v3, p0, Lcom/android/server/WifiService;->mLocks:Lcom/android/server/WifiService$LockList;

    #@153
    invoke-static {v3, p2}, Lcom/android/server/WifiService$LockList;->access$3800(Lcom/android/server/WifiService$LockList;Ljava/io/PrintWriter;)V

    #@156
    .line 1813
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    #@159
    .line 1814
    const-string v3, "WifiWatchdogStateMachine dump"

    #@15b
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@15e
    .line 1815
    iget-object v3, p0, Lcom/android/server/WifiService;->mWifiWatchdogStateMachine:Landroid/net/wifi/WifiWatchdogStateMachine;

    #@160
    invoke-virtual {v3, p2}, Landroid/net/wifi/WifiWatchdogStateMachine;->dump(Ljava/io/PrintWriter;)V

    #@163
    .line 1816
    const-string v3, "WifiStateMachine dump"

    #@165
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@168
    .line 1817
    iget-object v3, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@16a
    invoke-virtual {v3, p1, p2, p3}, Landroid/net/wifi/WifiStateMachine;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    #@16d
    goto/16 :goto_33
.end method

.method public enableNetwork(IZ)Z
    .registers 7
    .parameter "netId"
    .parameter "disableOthers"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 1240
    const-string v1, "WifiService"

    #@3
    new-instance v2, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v3, "enableNetwork pid="

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@11
    move-result v3

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    const-string v3, ", uid="

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@1f
    move-result v3

    #@20
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 1243
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@2e
    move-result-object v1

    #@2f
    if-eqz v1, :cond_41

    #@31
    .line 1244
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@34
    move-result-object v1

    #@35
    const/4 v2, 0x0

    #@36
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@39
    move-result v3

    #@3a
    invoke-interface {v1, v2, p1, v3}, Lcom/lge/cappuccino/IMdm;->checkDisabledWifiSecurity(Landroid/net/wifi/WifiConfiguration;II)Z

    #@3d
    move-result v1

    #@3e
    if-eqz v1, :cond_41

    #@40
    .line 1256
    :goto_40
    return v0

    #@41
    .line 1250
    :cond_41
    invoke-direct {p0}, Lcom/android/server/WifiService;->enforceChangePermission()V

    #@44
    .line 1251
    iget-object v1, p0, Lcom/android/server/WifiService;->mWifiStateMachineChannel:Lcom/android/internal/util/AsyncChannel;

    #@46
    if-eqz v1, :cond_51

    #@48
    .line 1252
    iget-object v0, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@4a
    iget-object v1, p0, Lcom/android/server/WifiService;->mWifiStateMachineChannel:Lcom/android/internal/util/AsyncChannel;

    #@4c
    invoke-virtual {v0, v1, p1, p2}, Landroid/net/wifi/WifiStateMachine;->syncEnableNetwork(Lcom/android/internal/util/AsyncChannel;IZ)Z

    #@4f
    move-result v0

    #@50
    goto :goto_40

    #@51
    .line 1255
    :cond_51
    const-string v1, "WifiService"

    #@53
    const-string v2, "mWifiStateMachineChannel is not initialized"

    #@55
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@58
    goto :goto_40
.end method

.method enforceWakeSourcePermission(II)V
    .registers 6
    .parameter "uid"
    .parameter "pid"

    #@0
    .prologue
    .line 1897
    invoke-static {}, Landroid/os/Process;->myUid()I

    #@3
    move-result v0

    #@4
    if-ne p1, v0, :cond_7

    #@6
    .line 1902
    :goto_6
    return-void

    #@7
    .line 1900
    :cond_7
    iget-object v0, p0, Lcom/android/server/WifiService;->mContext:Landroid/content/Context;

    #@9
    const-string v1, "android.permission.UPDATE_DEVICE_STATS"

    #@b
    const/4 v2, 0x0

    #@c
    invoke-virtual {v0, v1, p2, p1, v2}, Landroid/content/Context;->enforcePermission(Ljava/lang/String;IILjava/lang/String;)V

    #@f
    goto :goto_6
.end method

.method public getConfigFile()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1487
    invoke-direct {p0}, Lcom/android/server/WifiService;->enforceAccessPermission()V

    #@3
    .line 1488
    iget-object v0, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@5
    invoke-virtual {v0}, Landroid/net/wifi/WifiStateMachine;->getConfigFile()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method public getConfiguredNetworks()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/net/wifi/WifiConfiguration;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1155
    invoke-direct {p0}, Lcom/android/server/WifiService;->enforceAccessPermission()V

    #@3
    .line 1156
    iget-object v0, p0, Lcom/android/server/WifiService;->mWifiStateMachineChannel:Lcom/android/internal/util/AsyncChannel;

    #@5
    if-eqz v0, :cond_10

    #@7
    .line 1157
    iget-object v0, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@9
    iget-object v1, p0, Lcom/android/server/WifiService;->mWifiStateMachineChannel:Lcom/android/internal/util/AsyncChannel;

    #@b
    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiStateMachine;->syncGetConfiguredNetworks(Lcom/android/internal/util/AsyncChannel;)Ljava/util/List;

    #@e
    move-result-object v0

    #@f
    .line 1160
    :goto_f
    return-object v0

    #@10
    .line 1159
    :cond_10
    const-string v0, "WifiService"

    #@12
    const-string v1, "mWifiStateMachineChannel is not initialized"

    #@14
    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@17
    .line 1160
    const/4 v0, 0x0

    #@18
    goto :goto_f
.end method

.method public getConnectionInfo()Landroid/net/wifi/WifiInfo;
    .registers 2

    #@0
    .prologue
    .line 1284
    invoke-direct {p0}, Lcom/android/server/WifiService;->enforceAccessPermission()V

    #@3
    .line 1289
    iget-object v0, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@5
    invoke-virtual {v0}, Landroid/net/wifi/WifiStateMachine;->syncRequestConnectionInfo()Landroid/net/wifi/WifiInfo;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method public getDhcpInfo()Landroid/net/DhcpInfo;
    .registers 2

    #@0
    .prologue
    .line 1402
    invoke-direct {p0}, Lcom/android/server/WifiService;->enforceAccessPermission()V

    #@3
    .line 1403
    iget-object v0, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@5
    invoke-virtual {v0}, Landroid/net/wifi/WifiStateMachine;->syncGetDhcpInfo()Landroid/net/DhcpInfo;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method public getFrequencyBand()I
    .registers 2

    #@0
    .prologue
    .line 1380
    invoke-direct {p0}, Lcom/android/server/WifiService;->enforceAccessPermission()V

    #@3
    .line 1381
    iget-object v0, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@5
    invoke-virtual {v0}, Landroid/net/wifi/WifiStateMachine;->getFrequencyBand()I

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public getScanResults()Ljava/util/List;
    .registers 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/net/wifi/ScanResult;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1299
    const-string v4, "WifiService"

    #@2
    new-instance v5, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v6, "getScanResults pid="

    #@9
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v5

    #@d
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@10
    move-result v6

    #@11
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v5

    #@15
    const-string v6, ", uid="

    #@17
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v5

    #@1b
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@1e
    move-result v6

    #@1f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v5

    #@23
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v5

    #@27
    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 1301
    invoke-direct {p0}, Lcom/android/server/WifiService;->enforceAccessPermission()V

    #@2d
    .line 1302
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@30
    move-result v3

    #@31
    .line 1303
    .local v3, userId:I
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@34
    move-result-wide v1

    #@35
    .line 1305
    .local v1, ident:J
    :try_start_35
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    #@38
    move-result v0

    #@39
    .line 1306
    .local v0, currentUser:I
    if-eq v3, v0, :cond_44

    #@3b
    .line 1307
    new-instance v4, Ljava/util/ArrayList;

    #@3d
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V
    :try_end_40
    .catchall {:try_start_35 .. :try_end_40} :catchall_4b

    #@40
    .line 1312
    :goto_40
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@43
    .line 1309
    return-object v4

    #@44
    :cond_44
    :try_start_44
    iget-object v4, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@46
    invoke-virtual {v4}, Landroid/net/wifi/WifiStateMachine;->syncGetScanResultsList()Ljava/util/List;
    :try_end_49
    .catchall {:try_start_44 .. :try_end_49} :catchall_4b

    #@49
    move-result-object v4

    #@4a
    goto :goto_40

    #@4b
    .line 1312
    .end local v0           #currentUser:I
    :catchall_4b
    move-exception v4

    #@4c
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@4f
    throw v4
.end method

.method public getVZWMobileHotspotSSID()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 2708
    invoke-static {}, Lcom/lge/wifi_iface/WifiIfaceManager;->getWifiMHPIfaceIface()Lcom/lge/wifi_iface/WifiMHPIfaceIface;

    #@3
    move-result-object v1

    #@4
    if-eqz v1, :cond_11

    #@6
    .line 2709
    invoke-static {}, Lcom/lge/wifi_iface/WifiIfaceManager;->getWifiMHPIfaceIface()Lcom/lge/wifi_iface/WifiMHPIfaceIface;

    #@9
    move-result-object v0

    #@a
    .line 2710
    .local v0, wifiMHPIfaceIface:Lcom/lge/wifi_iface/WifiMHPIfaceIface;
    if-eqz v0, :cond_11

    #@c
    .line 2711
    invoke-interface {v0}, Lcom/lge/wifi_iface/WifiMHPIfaceIface;->getName()Ljava/lang/String;

    #@f
    move-result-object v1

    #@10
    .line 2714
    .end local v0           #wifiMHPIfaceIface:Lcom/lge/wifi_iface/WifiMHPIfaceIface;
    :goto_10
    return-object v1

    #@11
    :cond_11
    const/4 v1, 0x0

    #@12
    goto :goto_10
.end method

.method public getWifiApConfiguration()Landroid/net/wifi/WifiConfiguration;
    .registers 2

    #@0
    .prologue
    .line 1102
    invoke-direct {p0}, Lcom/android/server/WifiService;->enforceAccessPermission()V

    #@3
    .line 1103
    iget-object v0, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@5
    invoke-virtual {v0}, Landroid/net/wifi/WifiStateMachine;->syncGetWifiApConfiguration()Landroid/net/wifi/WifiConfiguration;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method public getWifiApEnabledState()I
    .registers 2

    #@0
    .prologue
    .line 1093
    invoke-direct {p0}, Lcom/android/server/WifiService;->enforceAccessPermission()V

    #@3
    .line 1094
    iget-object v0, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@5
    invoke-virtual {v0}, Landroid/net/wifi/WifiStateMachine;->syncGetWifiApState()I

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public getWifiEnabledState()I
    .registers 2

    #@0
    .prologue
    .line 1041
    invoke-direct {p0}, Lcom/android/server/WifiService;->enforceAccessPermission()V

    #@3
    .line 1042
    iget-object v0, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@5
    invoke-virtual {v0}, Landroid/net/wifi/WifiStateMachine;->syncGetWifiState()I

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public getWifiNeedOn()Z
    .registers 2

    #@0
    .prologue
    .line 2644
    iget-boolean v0, p0, Lcom/android/server/WifiService;->mWifiNeedOnE911:Z

    #@2
    return v0
.end method

.method public getWifiServiceMessenger()Landroid/os/Messenger;
    .registers 3

    #@0
    .prologue
    .line 1471
    invoke-direct {p0}, Lcom/android/server/WifiService;->enforceAccessPermission()V

    #@3
    .line 1472
    invoke-direct {p0}, Lcom/android/server/WifiService;->enforceChangePermission()V

    #@6
    .line 1473
    new-instance v0, Landroid/os/Messenger;

    #@8
    iget-object v1, p0, Lcom/android/server/WifiService;->mAsyncServiceHandler:Lcom/android/server/WifiService$AsyncServiceHandler;

    #@a
    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    #@d
    return-object v0
.end method

.method public getWifiStateMachineMessenger()Landroid/os/Messenger;
    .registers 2

    #@0
    .prologue
    .line 1478
    invoke-direct {p0}, Lcom/android/server/WifiService;->enforceAccessPermission()V

    #@3
    .line 1479
    invoke-direct {p0}, Lcom/android/server/WifiService;->enforceChangePermission()V

    #@6
    .line 1480
    iget-object v0, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@8
    invoke-virtual {v0}, Landroid/net/wifi/WifiStateMachine;->getMessenger()Landroid/os/Messenger;

    #@b
    move-result-object v0

    #@c
    return-object v0
.end method

.method public getWifiVZWApConfiguration()Landroid/net/wifi/WifiVZWConfiguration;
    .registers 2

    #@0
    .prologue
    .line 2620
    invoke-direct {p0}, Lcom/android/server/WifiService;->enforceAccessPermission()V

    #@3
    .line 2621
    iget-object v0, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@5
    invoke-virtual {v0}, Landroid/net/wifi/WifiStateMachine;->syncGetWifiVZWApConfiguration()Landroid/net/wifi/WifiVZWConfiguration;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method public initializeMulticastFiltering()V
    .registers 3

    #@0
    .prologue
    .line 2146
    invoke-direct {p0}, Lcom/android/server/WifiService;->enforceMulticastChangePermission()V

    #@3
    .line 2148
    iget-object v1, p0, Lcom/android/server/WifiService;->mMulticasters:Ljava/util/List;

    #@5
    monitor-enter v1

    #@6
    .line 2150
    :try_start_6
    iget-object v0, p0, Lcom/android/server/WifiService;->mMulticasters:Ljava/util/List;

    #@8
    invoke-interface {v0}, Ljava/util/List;->size()I

    #@b
    move-result v0

    #@c
    if-eqz v0, :cond_10

    #@e
    .line 2151
    monitor-exit v1

    #@f
    .line 2156
    :goto_f
    return-void

    #@10
    .line 2153
    :cond_10
    iget-object v0, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@12
    invoke-virtual {v0}, Landroid/net/wifi/WifiStateMachine;->startFilteringMulticastV4Packets()V

    #@15
    .line 2155
    monitor-exit v1

    #@16
    goto :goto_f

    #@17
    :catchall_17
    move-exception v0

    #@18
    monitor-exit v1
    :try_end_19
    .catchall {:try_start_6 .. :try_end_19} :catchall_17

    #@19
    throw v0
.end method

.method public isDualBandSupported()Z
    .registers 3

    #@0
    .prologue
    .line 1388
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getCountry()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    const-string v1, "IL"

    #@6
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_15

    #@c
    .line 1389
    const-string v0, "WifiService"

    #@e
    const-string v1, "WifiService isDualBandSupported: Block 5GHz band"

    #@10
    invoke-static {v0, v1}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@13
    .line 1390
    const/4 v0, 0x0

    #@14
    .line 1392
    :goto_14
    return v0

    #@15
    :cond_15
    iget-object v0, p0, Lcom/android/server/WifiService;->mContext:Landroid/content/Context;

    #@17
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@1a
    move-result-object v0

    #@1b
    const v1, 0x1110015

    #@1e
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@21
    move-result v0

    #@22
    goto :goto_14
.end method

.method public isMulticastEnabled()Z
    .registers 3

    #@0
    .prologue
    .line 2218
    invoke-direct {p0}, Lcom/android/server/WifiService;->enforceAccessPermission()V

    #@3
    .line 2220
    iget-object v1, p0, Lcom/android/server/WifiService;->mMulticasters:Ljava/util/List;

    #@5
    monitor-enter v1

    #@6
    .line 2221
    :try_start_6
    iget-object v0, p0, Lcom/android/server/WifiService;->mMulticasters:Ljava/util/List;

    #@8
    invoke-interface {v0}, Ljava/util/List;->size()I

    #@b
    move-result v0

    #@c
    if-lez v0, :cond_11

    #@e
    const/4 v0, 0x1

    #@f
    :goto_f
    monitor-exit v1

    #@10
    return v0

    #@11
    :cond_11
    const/4 v0, 0x0

    #@12
    goto :goto_f

    #@13
    .line 2222
    :catchall_13
    move-exception v0

    #@14
    monitor-exit v1
    :try_end_15
    .catchall {:try_start_6 .. :try_end_15} :catchall_13

    #@15
    throw v0
.end method

.method public isVZWMobileHotspotEnabled()Z
    .registers 3

    #@0
    .prologue
    .line 2659
    invoke-static {}, Lcom/lge/wifi_iface/WifiIfaceManager;->getWifiMHPIfaceIface()Lcom/lge/wifi_iface/WifiMHPIfaceIface;

    #@3
    move-result-object v1

    #@4
    if-eqz v1, :cond_11

    #@6
    .line 2660
    invoke-static {}, Lcom/lge/wifi_iface/WifiIfaceManager;->getWifiMHPIfaceIface()Lcom/lge/wifi_iface/WifiMHPIfaceIface;

    #@9
    move-result-object v0

    #@a
    .line 2661
    .local v0, wifiMHPIfaceIface:Lcom/lge/wifi_iface/WifiMHPIfaceIface;
    if-eqz v0, :cond_11

    #@c
    .line 2662
    invoke-interface {v0}, Lcom/lge/wifi_iface/WifiMHPIfaceIface;->isMHPEnabled()Z

    #@f
    move-result v1

    #@10
    .line 2665
    .end local v0           #wifiMHPIfaceIface:Lcom/lge/wifi_iface/WifiMHPIfaceIface;
    :goto_10
    return v1

    #@11
    :cond_11
    const/4 v1, 0x0

    #@12
    goto :goto_10
.end method

.method public pingSupplicant()Z
    .registers 3

    #@0
    .prologue
    .line 875
    invoke-direct {p0}, Lcom/android/server/WifiService;->enforceAccessPermission()V

    #@3
    .line 876
    iget-object v0, p0, Lcom/android/server/WifiService;->mWifiStateMachineChannel:Lcom/android/internal/util/AsyncChannel;

    #@5
    if-eqz v0, :cond_10

    #@7
    .line 877
    iget-object v0, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@9
    iget-object v1, p0, Lcom/android/server/WifiService;->mWifiStateMachineChannel:Lcom/android/internal/util/AsyncChannel;

    #@b
    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiStateMachine;->syncPingSupplicant(Lcom/android/internal/util/AsyncChannel;)Z

    #@e
    move-result v0

    #@f
    .line 880
    :goto_f
    return v0

    #@10
    .line 879
    :cond_10
    const-string v0, "WifiService"

    #@12
    const-string v1, "mWifiStateMachineChannel is not initialized"

    #@14
    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@17
    .line 880
    const/4 v0, 0x0

    #@18
    goto :goto_f
.end method

.method public reassociate()V
    .registers 4

    #@0
    .prologue
    .line 1144
    const-string v0, "WifiService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "reassociate pid="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@10
    move-result v2

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    const-string v2, ", uid="

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@1e
    move-result v2

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 1146
    invoke-direct {p0}, Lcom/android/server/WifiService;->enforceChangePermission()V

    #@2d
    .line 1147
    iget-object v0, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@2f
    invoke-virtual {v0}, Landroid/net/wifi/WifiStateMachine;->reassociateCommand()V

    #@32
    .line 1148
    return-void
.end method

.method public reconnect()V
    .registers 4

    #@0
    .prologue
    .line 1133
    const-string v0, "WifiService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "reconnect pid="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@10
    move-result v2

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    const-string v2, ", uid="

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@1e
    move-result v2

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 1135
    invoke-direct {p0}, Lcom/android/server/WifiService;->enforceChangePermission()V

    #@2d
    .line 1136
    iget-object v0, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@2f
    invoke-virtual {v0}, Landroid/net/wifi/WifiStateMachine;->reconnectCommand()V

    #@32
    .line 1137
    return-void
.end method

.method public releaseMulticastLock()V
    .registers 7

    #@0
    .prologue
    .line 2182
    invoke-direct {p0}, Lcom/android/server/WifiService;->enforceMulticastChangePermission()V

    #@3
    .line 2184
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@6
    move-result v3

    #@7
    .line 2185
    .local v3, uid:I
    iget-object v5, p0, Lcom/android/server/WifiService;->mMulticasters:Ljava/util/List;

    #@9
    monitor-enter v5

    #@a
    .line 2186
    :try_start_a
    iget v4, p0, Lcom/android/server/WifiService;->mMulticastDisabled:I

    #@c
    add-int/lit8 v4, v4, 0x1

    #@e
    iput v4, p0, Lcom/android/server/WifiService;->mMulticastDisabled:I

    #@10
    .line 2187
    iget-object v4, p0, Lcom/android/server/WifiService;->mMulticasters:Ljava/util/List;

    #@12
    invoke-interface {v4}, Ljava/util/List;->size()I

    #@15
    move-result v2

    #@16
    .line 2188
    .local v2, size:I
    add-int/lit8 v0, v2, -0x1

    #@18
    .local v0, i:I
    :goto_18
    if-ltz v0, :cond_30

    #@1a
    .line 2189
    iget-object v4, p0, Lcom/android/server/WifiService;->mMulticasters:Ljava/util/List;

    #@1c
    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@1f
    move-result-object v1

    #@20
    check-cast v1, Lcom/android/server/WifiService$Multicaster;

    #@22
    .line 2190
    .local v1, m:Lcom/android/server/WifiService$Multicaster;
    if-eqz v1, :cond_2d

    #@24
    invoke-virtual {v1}, Lcom/android/server/WifiService$Multicaster;->getUid()I

    #@27
    move-result v4

    #@28
    if-ne v4, v3, :cond_2d

    #@2a
    .line 2191
    invoke-direct {p0, v0, v3}, Lcom/android/server/WifiService;->removeMulticasterLocked(II)V

    #@2d
    .line 2188
    :cond_2d
    add-int/lit8 v0, v0, -0x1

    #@2f
    goto :goto_18

    #@30
    .line 2194
    .end local v1           #m:Lcom/android/server/WifiService$Multicaster;
    :cond_30
    monitor-exit v5

    #@31
    .line 2195
    return-void

    #@32
    .line 2194
    .end local v0           #i:I
    .end local v2           #size:I
    :catchall_32
    move-exception v4

    #@33
    monitor-exit v5
    :try_end_34
    .catchall {:try_start_a .. :try_end_34} :catchall_32

    #@34
    throw v4
.end method

.method public releaseWifiLock(Landroid/os/IBinder;)Z
    .registers 5
    .parameter "lock"

    #@0
    .prologue
    .line 2021
    iget-object v0, p0, Lcom/android/server/WifiService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.WAKE_LOCK"

    #@4
    const/4 v2, 0x0

    #@5
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 2022
    iget-object v1, p0, Lcom/android/server/WifiService;->mLocks:Lcom/android/server/WifiService$LockList;

    #@a
    monitor-enter v1

    #@b
    .line 2023
    :try_start_b
    invoke-direct {p0, p1}, Lcom/android/server/WifiService;->releaseWifiLockLocked(Landroid/os/IBinder;)Z

    #@e
    move-result v0

    #@f
    monitor-exit v1

    #@10
    return v0

    #@11
    .line 2024
    :catchall_11
    move-exception v0

    #@12
    monitor-exit v1
    :try_end_13
    .catchall {:try_start_b .. :try_end_13} :catchall_11

    #@13
    throw v0
.end method

.method public removeNetwork(I)Z
    .registers 6
    .parameter "netId"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 1213
    const-string v1, "WifiService"

    #@3
    new-instance v2, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v3, "removeNetwork pid="

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@11
    move-result v3

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    const-string v3, ", uid="

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@1f
    move-result v3

    #@20
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 1215
    invoke-direct {p0}, Lcom/android/server/WifiService;->enforceChangePermission()V

    #@2e
    .line 1217
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@31
    move-result-object v1

    #@32
    if-eqz v1, :cond_40

    #@34
    .line 1218
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@37
    move-result-object v1

    #@38
    const/4 v2, 0x3

    #@39
    invoke-interface {v1, v2}, Lcom/lge/cappuccino/IMdm;->getAllowWiFiProfileManagement(I)Z

    #@3c
    move-result v1

    #@3d
    if-nez v1, :cond_40

    #@3f
    .line 1227
    :goto_3f
    return v0

    #@40
    .line 1223
    :cond_40
    iget-object v1, p0, Lcom/android/server/WifiService;->mWifiStateMachineChannel:Lcom/android/internal/util/AsyncChannel;

    #@42
    if-eqz v1, :cond_4d

    #@44
    .line 1224
    iget-object v0, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@46
    iget-object v1, p0, Lcom/android/server/WifiService;->mWifiStateMachineChannel:Lcom/android/internal/util/AsyncChannel;

    #@48
    invoke-virtual {v0, v1, p1}, Landroid/net/wifi/WifiStateMachine;->syncRemoveNetwork(Lcom/android/internal/util/AsyncChannel;I)Z

    #@4b
    move-result v0

    #@4c
    goto :goto_3f

    #@4d
    .line 1226
    :cond_4d
    const-string v1, "WifiService"

    #@4f
    const-string v2, "mWifiStateMachineChannel is not initialized"

    #@51
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@54
    goto :goto_3f
.end method

.method public saveConfiguration()Z
    .registers 5

    #@0
    .prologue
    .line 1324
    const-string v1, "WifiService"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "saveConfiguration pid="

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@10
    move-result v3

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    const-string v3, ", uid="

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@1e
    move-result v3

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v2

    #@27
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 1326
    const/4 v0, 0x1

    #@2b
    .line 1327
    .local v0, result:Z
    invoke-direct {p0}, Lcom/android/server/WifiService;->enforceChangePermission()V

    #@2e
    .line 1328
    iget-object v1, p0, Lcom/android/server/WifiService;->mWifiStateMachineChannel:Lcom/android/internal/util/AsyncChannel;

    #@30
    if-eqz v1, :cond_3b

    #@32
    .line 1329
    iget-object v1, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@34
    iget-object v2, p0, Lcom/android/server/WifiService;->mWifiStateMachineChannel:Lcom/android/internal/util/AsyncChannel;

    #@36
    invoke-virtual {v1, v2}, Landroid/net/wifi/WifiStateMachine;->syncSaveConfig(Lcom/android/internal/util/AsyncChannel;)Z

    #@39
    move-result v1

    #@3a
    .line 1332
    :goto_3a
    return v1

    #@3b
    .line 1331
    :cond_3b
    const-string v1, "WifiService"

    #@3d
    const-string v2, "mWifiStateMachineChannel is not initialized"

    #@3f
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@42
    .line 1332
    const/4 v1, 0x0

    #@43
    goto :goto_3a
.end method

.method public setCountryCode(Ljava/lang/String;Z)V
    .registers 6
    .parameter "countryCode"
    .parameter "persist"

    #@0
    .prologue
    .line 1347
    const-string v0, "WifiService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "setCountryCode pid="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@10
    move-result v2

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    const-string v2, ", uid="

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@1e
    move-result v2

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 1349
    const-string v0, "WifiService"

    #@2c
    new-instance v1, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v2, "WifiService trying to set country code to "

    #@33
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v1

    #@37
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v1

    #@3b
    const-string v2, " with persist set to "

    #@3d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v1

    #@41
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@44
    move-result-object v1

    #@45
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v1

    #@49
    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@4c
    .line 1351
    invoke-direct {p0}, Lcom/android/server/WifiService;->enforceChangePermission()V

    #@4f
    .line 1352
    iget-object v0, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@51
    invoke-virtual {v0, p1, p2}, Landroid/net/wifi/WifiStateMachine;->setCountryCode(Ljava/lang/String;Z)V

    #@54
    .line 1353
    return-void
.end method

.method public setDLNAEnabled()Z
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 2576
    sget-boolean v0, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@3
    if-eqz v0, :cond_e

    #@5
    iget-object v0, p0, Lcom/android/server/WifiService;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@7
    if-eqz v0, :cond_e

    #@9
    .line 2577
    iget-object v0, p0, Lcom/android/server/WifiService;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@b
    invoke-interface {v0, v1}, Lcom/lge/wifi_iface/WifiServiceExtIface;->setDlnaEnabled(Z)Z

    #@e
    .line 2579
    :cond_e
    return v1
.end method

.method public setFrequencyBand(IZ)V
    .registers 6
    .parameter "band"
    .parameter "persist"

    #@0
    .prologue
    .line 1366
    const-string v0, "WifiService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "setFrequencyBand pid="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@10
    move-result v2

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    const-string v2, ", uid="

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@1e
    move-result v2

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 1368
    invoke-direct {p0}, Lcom/android/server/WifiService;->enforceChangePermission()V

    #@2d
    .line 1369
    invoke-virtual {p0}, Lcom/android/server/WifiService;->isDualBandSupported()Z

    #@30
    move-result v0

    #@31
    if-nez v0, :cond_34

    #@33
    .line 1373
    :goto_33
    return-void

    #@34
    .line 1370
    :cond_34
    const-string v0, "WifiService"

    #@36
    new-instance v1, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v2, "WifiService trying to set frequency band to "

    #@3d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v1

    #@41
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@44
    move-result-object v1

    #@45
    const-string v2, " with persist set to "

    #@47
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v1

    #@4b
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v1

    #@4f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v1

    #@53
    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@56
    .line 1372
    iget-object v0, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@58
    invoke-virtual {v0, p1, p2}, Landroid/net/wifi/WifiStateMachine;->setFrequencyBand(IZ)V

    #@5b
    goto :goto_33
.end method

.method public setVZWMobileHotspot(Z)Z
    .registers 5
    .parameter "enable"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 2672
    if-eqz p1, :cond_4

    #@3
    .line 2688
    :cond_3
    :goto_3
    return v1

    #@4
    .line 2678
    :cond_4
    invoke-static {}, Lcom/lge/wifi_iface/WifiIfaceManager;->getWifiMHPIfaceIface()Lcom/lge/wifi_iface/WifiMHPIfaceIface;

    #@7
    move-result-object v2

    #@8
    if-eqz v2, :cond_3

    #@a
    .line 2679
    invoke-static {}, Lcom/lge/wifi_iface/WifiIfaceManager;->getWifiMHPIfaceIface()Lcom/lge/wifi_iface/WifiMHPIfaceIface;

    #@d
    move-result-object v0

    #@e
    .line 2680
    .local v0, wifiMHPIfaceIface:Lcom/lge/wifi_iface/WifiMHPIfaceIface;
    if-eqz v0, :cond_3

    #@10
    .line 2681
    invoke-interface {v0}, Lcom/lge/wifi_iface/WifiMHPIfaceIface;->disableMobileHotspot()Z

    #@13
    .line 2682
    const/4 v1, 0x1

    #@14
    goto :goto_3
.end method

.method public setWifiApConfiguration(Landroid/net/wifi/WifiConfiguration;)V
    .registers 3
    .parameter "wifiConfig"

    #@0
    .prologue
    .line 1111
    invoke-direct {p0}, Lcom/android/server/WifiService;->enforceChangePermission()V

    #@3
    .line 1112
    if-nez p1, :cond_6

    #@5
    .line 1115
    :goto_5
    return-void

    #@6
    .line 1114
    :cond_6
    iget-object v0, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@8
    invoke-virtual {v0, p1}, Landroid/net/wifi/WifiStateMachine;->setWifiApConfiguration(Landroid/net/wifi/WifiConfiguration;)V

    #@b
    goto :goto_5
.end method

.method public setWifiApEnabled(Landroid/net/wifi/WifiConfiguration;Z)V
    .registers 7
    .parameter "wifiConfig"
    .parameter "enabled"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 1053
    sget-boolean v0, Lcom/lge/config/ThreelmMdmConfig;->THREELM_MDM:Z

    #@4
    if-eqz v0, :cond_17

    #@6
    if-eqz p2, :cond_17

    #@8
    iget-object v0, p0, Lcom/android/server/WifiService;->mContext:Landroid/content/Context;

    #@a
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@d
    move-result-object v0

    #@e
    const-string v1, "tethering_blocked"

    #@10
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@13
    move-result v0

    #@14
    if-ne v0, v3, :cond_17

    #@16
    .line 1082
    :goto_16
    return-void

    #@17
    .line 1058
    :cond_17
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@1a
    move-result-object v0

    #@1b
    if-eqz v0, :cond_35

    #@1d
    if-eqz p2, :cond_35

    #@1f
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@22
    move-result-object v0

    #@23
    const/4 v1, 0x2

    #@24
    invoke-interface {v0, v1}, Lcom/lge/cappuccino/IMdm;->checkDisabledTetherType(I)Z

    #@27
    move-result v0

    #@28
    if-eqz v0, :cond_35

    #@2a
    .line 1062
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@2d
    move-result-object v0

    #@2e
    const v1, 0x2090051

    #@31
    invoke-interface {v0, v1}, Lcom/lge/cappuccino/IMdm;->sendToastMessageId(I)V

    #@34
    goto :goto_16

    #@35
    .line 1068
    :cond_35
    sget-boolean v0, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@37
    if-eqz v0, :cond_54

    #@39
    iget-object v0, p0, Lcom/android/server/WifiService;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@3b
    if-eqz v0, :cond_54

    #@3d
    .line 1069
    if-eqz p2, :cond_4f

    #@3f
    .line 1070
    iget-object v0, p0, Lcom/android/server/WifiService;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@41
    invoke-interface {v0}, Lcom/lge/wifi_iface/WifiServiceExtIface;->isProvisioned()Z

    #@44
    move-result v0

    #@45
    if-eq v3, v0, :cond_54

    #@47
    .line 1071
    const-string v0, "WifiService"

    #@49
    const-string v1, "Hotspot is Blocked by VZW Provisioning"

    #@4b
    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    goto :goto_16

    #@4f
    .line 1075
    :cond_4f
    iget-object v0, p0, Lcom/android/server/WifiService;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@51
    invoke-interface {v0, v2}, Lcom/lge/wifi_iface/WifiServiceExtIface;->setProvisioned(Z)V

    #@54
    .line 1080
    :cond_54
    invoke-direct {p0}, Lcom/android/server/WifiService;->enforceChangePermission()V

    #@57
    .line 1081
    iget-object v0, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@59
    invoke-virtual {v0, p1, p2}, Landroid/net/wifi/WifiStateMachine;->setWifiApEnabled(Landroid/net/wifi/WifiConfiguration;Z)V

    #@5c
    goto :goto_16
.end method

.method public declared-synchronized setWifiEnabled(Z)Z
    .registers 12
    .parameter "enable"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 927
    monitor-enter p0

    #@3
    :try_start_3
    invoke-direct {p0}, Lcom/android/server/WifiService;->enforceChangePermission()V

    #@6
    .line 928
    const-string v7, "WifiService"

    #@8
    new-instance v8, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v9, "setWifiEnabled: "

    #@f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v8

    #@13
    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@16
    move-result-object v8

    #@17
    const-string v9, " pid="

    #@19
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v8

    #@1d
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@20
    move-result v9

    #@21
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@24
    move-result-object v8

    #@25
    const-string v9, ", uid="

    #@27
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v8

    #@2b
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@2e
    move-result v9

    #@2f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v8

    #@33
    const-string v9, ", Enabled = "

    #@35
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v8

    #@39
    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v8

    #@3d
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v8

    #@41
    invoke-static {v7, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    .line 932
    sget-boolean v7, Lcom/lge/config/ThreelmMdmConfig;->THREELM_MDM:Z

    #@46
    if-eqz v7, :cond_63

    #@48
    if-eqz p1, :cond_63

    #@4a
    iget-object v7, p0, Lcom/android/server/WifiService;->mDeviceManager:Lcom/android/server/DeviceManager3LMService;

    #@4c
    invoke-virtual {v7}, Lcom/android/server/DeviceManager3LMService;->getWifiState()I

    #@4f
    move-result v7

    #@50
    if-nez v7, :cond_63

    #@52
    .line 933
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@55
    .line 934
    iget-object v6, p0, Lcom/android/server/WifiService;->mDeviceManager:Lcom/android/server/DeviceManager3LMService;

    #@57
    const v7, 0x1040032

    #@5a
    const v8, 0x1040032

    #@5d
    const/4 v9, 0x0

    #@5e
    invoke-virtual {v6, v7, v8, v9}, Lcom/android/server/DeviceManager3LMService;->notification(III)V
    :try_end_61
    .catchall {:try_start_3 .. :try_end_61} :catchall_bc

    #@61
    .line 1029
    :cond_61
    :goto_61
    monitor-exit p0

    #@62
    return v5

    #@63
    .line 939
    :cond_63
    :try_start_63
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getOperator()Ljava/lang/String;

    #@66
    move-result-object v7

    #@67
    const-string v8, "TMO"

    #@69
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6c
    move-result v7

    #@6d
    if-eqz v7, :cond_a1

    #@6f
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getCountry()Ljava/lang/String;

    #@72
    move-result-object v7

    #@73
    const-string v8, "US"

    #@75
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@78
    move-result v7

    #@79
    if-eqz v7, :cond_a1

    #@7b
    sget-boolean v7, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@7d
    if-eqz v7, :cond_a1

    #@7f
    if-eqz p1, :cond_a1

    #@81
    .line 940
    iget-object v7, p0, Lcom/android/server/WifiService;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@83
    invoke-interface {v7}, Lcom/lge/wifi_iface/WifiServiceExtIface;->checkWifiEnableCondition()Z

    #@86
    move-result v4

    #@87
    .line 941
    .local v4, ret:Z
    const-string v7, "WifiService"

    #@89
    new-instance v8, Ljava/lang/StringBuilder;

    #@8b
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@8e
    const-string v9, "checkWifiEnableCondition() return value = "

    #@90
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@93
    move-result-object v8

    #@94
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@97
    move-result-object v8

    #@98
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9b
    move-result-object v8

    #@9c
    invoke-static {v7, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@9f
    .line 942
    if-eqz v4, :cond_61

    #@a1
    .line 948
    .end local v4           #ret:Z
    :cond_a1
    const-string v7, "WifiService"

    #@a3
    const-string v8, "Invoking mWifiStateMachine.setWifiEnabled\n"

    #@a5
    invoke-static {v7, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@a8
    .line 950
    sget-boolean v7, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@aa
    if-eqz v7, :cond_c6

    #@ac
    .line 951
    iget-object v7, p0, Lcom/android/server/WifiService;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@ae
    invoke-interface {v7}, Lcom/lge/wifi_iface/WifiServiceExtIface;->startWifiDelaySendMsg()Z

    #@b1
    move-result v7

    #@b2
    if-nez v7, :cond_bf

    #@b4
    .line 952
    const-string v6, "WifiService"

    #@b6
    const-string v7, "setWifiEnabled startWifiDelaySendMsg false "

    #@b8
    invoke-static {v6, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_bb
    .catchall {:try_start_63 .. :try_end_bb} :catchall_bc

    #@bb
    goto :goto_61

    #@bc
    .line 927
    :catchall_bc
    move-exception v5

    #@bd
    monitor-exit p0

    #@be
    throw v5

    #@bf
    .line 955
    :cond_bf
    :try_start_bf
    const-string v7, "WifiService"

    #@c1
    const-string v8, "setWifiEnabled startWifiDelaySendMsg true"

    #@c3
    invoke-static {v7, v8}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@c6
    .line 958
    :cond_c6
    sget-boolean v7, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@c8
    if-eqz v7, :cond_d3

    #@ca
    if-eqz p1, :cond_d3

    #@cc
    .line 959
    iget-object v7, p0, Lcom/android/server/WifiService;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@ce
    const-string v8, "wifi"

    #@d0
    invoke-interface {v7, v8}, Lcom/lge/wifi_iface/WifiServiceExtIface;->checkWifiStartPossible(Ljava/lang/String;)V

    #@d3
    .line 962
    :cond_d3
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@d6
    move-result-object v7

    #@d7
    if-eqz v7, :cond_f1

    #@d9
    if-eqz p1, :cond_f1

    #@db
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@de
    move-result-object v7

    #@df
    const/4 v8, 0x0

    #@e0
    const-string v9, "LGMDMWifiUIAdapter"

    #@e2
    invoke-interface {v7, v8, v9}, Lcom/lge/cappuccino/IMdm;->checkDisabledSystemService(Landroid/content/ComponentName;Ljava/lang/String;)Z

    #@e5
    move-result v7

    #@e6
    if-eqz v7, :cond_f1

    #@e8
    .line 966
    const-string v6, "WifiService"

    #@ea
    const-string v7, "setWifiEnabled() MDM Block"

    #@ec
    invoke-static {v6, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@ef
    goto/16 :goto_61

    #@f1
    .line 972
    :cond_f1
    sget-boolean v5, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@f3
    if-eqz v5, :cond_121

    #@f5
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->useWiFiAggregation()Z

    #@f8
    move-result v5

    #@f9
    if-eqz v5, :cond_121

    #@fb
    .line 974
    if-eqz p1, :cond_144

    #@fd
    .line 980
    const-string v5, "WifiService"

    #@ff
    const-string v7, "WIFI_AGGREGATION_START"

    #@101
    invoke-static {v5, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@104
    .line 982
    const-string v5, "WifiService"

    #@106
    const-string v7, "WifiIfaceManager.getWiFiAggregationIfaceIface "

    #@108
    invoke-static {v5, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@10b
    .line 983
    invoke-static {}, Lcom/lge/wifi_iface/WifiIfaceManager;->getWiFiAggregationIfaceIface()Lcom/lge/wifi_iface/WiFiAggregationIfaceIface;

    #@10e
    move-result-object v5

    #@10f
    iput-object v5, p0, Lcom/android/server/WifiService;->mWiFiAggregationIfaceIface:Lcom/lge/wifi_iface/WiFiAggregationIfaceIface;

    #@111
    .line 984
    iget-object v5, p0, Lcom/android/server/WifiService;->mWiFiAggregationIfaceIface:Lcom/lge/wifi_iface/WiFiAggregationIfaceIface;

    #@113
    if-eqz v5, :cond_121

    #@115
    .line 985
    const-string v5, "WifiService"

    #@117
    const-string v7, "null != mWiFiAggregationIfaceIface "

    #@119
    invoke-static {v5, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@11c
    .line 986
    iget-object v5, p0, Lcom/android/server/WifiService;->mWiFiAggregationIfaceIface:Lcom/lge/wifi_iface/WiFiAggregationIfaceIface;

    #@11e
    invoke-interface {v5}, Lcom/lge/wifi_iface/WiFiAggregationIfaceIface;->processingAggregation()V

    #@121
    .line 1000
    :cond_121
    :goto_121
    if-eqz p1, :cond_126

    #@123
    .line 1001
    invoke-direct {p0}, Lcom/android/server/WifiService;->reportStartWorkSource()V

    #@126
    .line 1003
    :cond_126
    iget-object v5, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@128
    invoke-virtual {v5, p1}, Landroid/net/wifi/WifiStateMachine;->setWifiEnabled(Z)V

    #@12b
    .line 1010
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_12e
    .catchall {:try_start_bf .. :try_end_12e} :catchall_bc

    #@12e
    move-result-wide v1

    #@12f
    .line 1012
    .local v1, ident:J
    :try_start_12f
    invoke-direct {p0, p1}, Lcom/android/server/WifiService;->handleWifiToggled(Z)V
    :try_end_132
    .catchall {:try_start_12f .. :try_end_132} :catchall_16b

    #@132
    .line 1014
    :try_start_132
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@135
    .line 1017
    if-eqz p1, :cond_170

    #@137
    .line 1018
    iget-boolean v5, p0, Lcom/android/server/WifiService;->mIsReceiverRegistered:Z

    #@139
    if-nez v5, :cond_141

    #@13b
    .line 1019
    invoke-direct {p0}, Lcom/android/server/WifiService;->registerForBroadcasts()V

    #@13e
    .line 1020
    const/4 v5, 0x1

    #@13f
    iput-boolean v5, p0, Lcom/android/server/WifiService;->mIsReceiverRegistered:Z

    #@141
    :cond_141
    :goto_141
    move v5, v6

    #@142
    .line 1029
    goto/16 :goto_61

    #@144
    .line 990
    .end local v1           #ident:J
    :cond_144
    const-string v5, "WifiService"

    #@146
    new-instance v7, Ljava/lang/StringBuilder;

    #@148
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@14b
    const-string v8, "WIFI_AGGREGATION_STOP"

    #@14d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@150
    move-result-object v7

    #@151
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@154
    move-result-object v7

    #@155
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@158
    move-result-object v7

    #@159
    invoke-static {v5, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@15c
    .line 991
    const-string v0, "android.wifi.intent.action.WIFI_AGGREGATION_STOP"

    #@15e
    .line 992
    .local v0, WIFI_AGGREGATION_STOP:Ljava/lang/String;
    new-instance v3, Landroid/content/Intent;

    #@160
    invoke-direct {v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V
    :try_end_163
    .catchall {:try_start_132 .. :try_end_163} :catchall_bc

    #@163
    .line 994
    .local v3, intent:Landroid/content/Intent;
    :try_start_163
    iget-object v5, p0, Lcom/android/server/WifiService;->mContext:Landroid/content/Context;

    #@165
    invoke-virtual {v5, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_168
    .catchall {:try_start_163 .. :try_end_168} :catchall_bc
    .catch Ljava/lang/Exception; {:try_start_163 .. :try_end_168} :catch_169

    #@168
    goto :goto_121

    #@169
    .line 995
    :catch_169
    move-exception v5

    #@16a
    goto :goto_121

    #@16b
    .line 1014
    .end local v0           #WIFI_AGGREGATION_STOP:Ljava/lang/String;
    .end local v3           #intent:Landroid/content/Intent;
    .restart local v1       #ident:J
    :catchall_16b
    move-exception v5

    #@16c
    :try_start_16c
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@16f
    throw v5

    #@170
    .line 1022
    :cond_170
    sget-boolean v5, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@172
    if-eqz v5, :cond_178

    #@174
    .line 1023
    invoke-direct {p0}, Lcom/android/server/WifiService;->unregBrdcastReceiver()V

    #@177
    goto :goto_141

    #@178
    .line 1024
    :cond_178
    iget-boolean v5, p0, Lcom/android/server/WifiService;->mIsReceiverRegistered:Z

    #@17a
    if-eqz v5, :cond_141

    #@17c
    .line 1025
    iget-object v5, p0, Lcom/android/server/WifiService;->mContext:Landroid/content/Context;

    #@17e
    iget-object v7, p0, Lcom/android/server/WifiService;->mReceiver:Landroid/content/BroadcastReceiver;

    #@180
    invoke-virtual {v5, v7}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@183
    .line 1026
    const/4 v5, 0x0

    #@184
    iput-boolean v5, p0, Lcom/android/server/WifiService;->mIsReceiverRegistered:Z
    :try_end_186
    .catchall {:try_start_16c .. :try_end_186} :catchall_bc

    #@186
    goto :goto_141
.end method

.method public setWifiNeedOn(Z)Z
    .registers 3
    .parameter "enable"

    #@0
    .prologue
    .line 2639
    iput-boolean p1, p0, Lcom/android/server/WifiService;->mWifiNeedOnE911:Z

    #@2
    .line 2640
    const/4 v0, 0x1

    #@3
    return v0
.end method

.method public setWifiVZWApConfiguration(Landroid/net/wifi/WifiVZWConfiguration;)V
    .registers 3
    .parameter "wifiConfig"

    #@0
    .prologue
    .line 2627
    invoke-direct {p0}, Lcom/android/server/WifiService;->enforceChangePermission()V

    #@3
    .line 2628
    if-nez p1, :cond_6

    #@5
    .line 2631
    :goto_5
    return-void

    #@6
    .line 2630
    :cond_6
    iget-object v0, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@8
    invoke-virtual {v0, p1}, Landroid/net/wifi/WifiStateMachine;->setWifiVZWApConfiguration(Landroid/net/wifi/WifiVZWConfiguration;)V

    #@b
    goto :goto_5
.end method

.method public setWifiVZWApEnabled(Landroid/net/wifi/WifiVZWConfiguration;Z)V
    .registers 6
    .parameter "wifiConfig"
    .parameter "enabled"

    #@0
    .prologue
    .line 2601
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@3
    move-result-object v0

    #@4
    if-eqz v0, :cond_18

    #@6
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@9
    move-result-object v0

    #@a
    const/4 v1, 0x0

    #@b
    const-string v2, "LGMDMTetheringUIAdapter"

    #@d
    invoke-interface {v0, v1, v2}, Lcom/lge/cappuccino/IMdm;->checkDisabledSystemService(Landroid/content/ComponentName;Ljava/lang/String;)Z

    #@10
    move-result v0

    #@11
    if-eqz v0, :cond_18

    #@13
    if-eqz p2, :cond_18

    #@15
    .line 2603
    if-eqz p2, :cond_18

    #@17
    .line 2610
    :goto_17
    return-void

    #@18
    .line 2608
    :cond_18
    invoke-direct {p0}, Lcom/android/server/WifiService;->enforceChangePermission()V

    #@1b
    .line 2609
    iget-object v0, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@1d
    invoke-virtual {v0, p1, p2}, Landroid/net/wifi/WifiStateMachine;->setWifiVZWApEnabled(Landroid/net/wifi/WifiVZWConfiguration;Z)V

    #@20
    goto :goto_17
.end method

.method public startScan(Z)V
    .registers 5
    .parameter "forceActive"

    #@0
    .prologue
    .line 889
    const-string v0, "WifiService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "startScan pid="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@10
    move-result v2

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    const-string v2, ", uid="

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@1e
    move-result v2

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    const-string v2, ", forceActive = "

    #@25
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v1

    #@29
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v1

    #@2d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v1

    #@31
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    .line 892
    invoke-direct {p0}, Lcom/android/server/WifiService;->enforceChangePermission()V

    #@37
    .line 893
    iget-object v0, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@39
    invoke-virtual {v0, p1}, Landroid/net/wifi/WifiStateMachine;->startScan(Z)V

    #@3c
    .line 894
    invoke-direct {p0}, Lcom/android/server/WifiService;->noteScanStart()V

    #@3f
    .line 895
    return-void
.end method

.method public startWifi()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 1411
    invoke-direct {p0}, Lcom/android/server/WifiService;->enforceConnectivityInternalPermission()V

    #@4
    .line 1417
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->getOperator()Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    const-string v1, "BELL"

    #@a
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d
    move-result v0

    #@e
    if-ne v0, v3, :cond_27

    #@10
    .line 1418
    iget-object v0, p0, Lcom/android/server/WifiService;->mContext:Landroid/content/Context;

    #@12
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@15
    move-result-object v0

    #@16
    const-string v1, "ssid_update_completed"

    #@18
    const/4 v2, 0x0

    #@19
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@1c
    move-result v0

    #@1d
    if-nez v0, :cond_27

    #@1f
    .line 1419
    const-string v0, "WifiService"

    #@21
    const-string v1, "Default SSID is not set yet, Therefore start wifi will be ignored until SIM detected and wifi on"

    #@23
    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 1426
    :goto_26
    return-void

    #@27
    .line 1424
    :cond_27
    iget-object v0, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@29
    iget-boolean v1, p0, Lcom/android/server/WifiService;->mEmergencyCallbackMode:Z

    #@2b
    invoke-virtual {v0, v3, v1}, Landroid/net/wifi/WifiStateMachine;->setDriverStart(ZZ)V

    #@2e
    .line 1425
    iget-object v0, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@30
    invoke-virtual {v0}, Landroid/net/wifi/WifiStateMachine;->reconnectCommand()V

    #@33
    goto :goto_26
.end method

.method public stopWifi()V
    .registers 4

    #@0
    .prologue
    .line 1438
    invoke-direct {p0}, Lcom/android/server/WifiService;->enforceConnectivityInternalPermission()V

    #@3
    .line 1443
    iget-object v0, p0, Lcom/android/server/WifiService;->mWifiStateMachine:Landroid/net/wifi/WifiStateMachine;

    #@5
    const/4 v1, 0x0

    #@6
    iget-boolean v2, p0, Lcom/android/server/WifiService;->mEmergencyCallbackMode:Z

    #@8
    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/WifiStateMachine;->setDriverStart(ZZ)V

    #@b
    .line 1444
    return-void
.end method

.method public updateWifiLockWorkSource(Landroid/os/IBinder;Landroid/os/WorkSource;)V
    .registers 12
    .parameter "lock"
    .parameter "ws"

    #@0
    .prologue
    .line 1994
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@3
    move-result v4

    #@4
    .line 1995
    .local v4, uid:I
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@7
    move-result v3

    #@8
    .line 1996
    .local v3, pid:I
    if-eqz p2, :cond_11

    #@a
    invoke-virtual {p2}, Landroid/os/WorkSource;->size()I

    #@d
    move-result v6

    #@e
    if-nez v6, :cond_11

    #@10
    .line 1997
    const/4 p2, 0x0

    #@11
    .line 1999
    :cond_11
    if-eqz p2, :cond_16

    #@13
    .line 2000
    invoke-virtual {p0, v4, v3}, Lcom/android/server/WifiService;->enforceWakeSourcePermission(II)V

    #@16
    .line 2002
    :cond_16
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@19
    move-result-wide v0

    #@1a
    .line 2004
    .local v0, ident:J
    :try_start_1a
    iget-object v7, p0, Lcom/android/server/WifiService;->mLocks:Lcom/android/server/WifiService$LockList;

    #@1c
    monitor-enter v7
    :try_end_1d
    .catchall {:try_start_1a .. :try_end_1d} :catchall_58
    .catch Landroid/os/RemoteException; {:try_start_1a .. :try_end_1d} :catch_30

    #@1d
    .line 2005
    :try_start_1d
    iget-object v6, p0, Lcom/android/server/WifiService;->mLocks:Lcom/android/server/WifiService$LockList;

    #@1f
    invoke-static {v6, p1}, Lcom/android/server/WifiService$LockList;->access$4600(Lcom/android/server/WifiService$LockList;Landroid/os/IBinder;)I

    #@22
    move-result v2

    #@23
    .line 2006
    .local v2, index:I
    if-gez v2, :cond_35

    #@25
    .line 2007
    new-instance v6, Ljava/lang/IllegalArgumentException;

    #@27
    const-string v8, "Wifi lock not active"

    #@29
    invoke-direct {v6, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@2c
    throw v6

    #@2d
    .line 2013
    .end local v2           #index:I
    :catchall_2d
    move-exception v6

    #@2e
    monitor-exit v7
    :try_end_2f
    .catchall {:try_start_1d .. :try_end_2f} :catchall_2d

    #@2f
    :try_start_2f
    throw v6
    :try_end_30
    .catchall {:try_start_2f .. :try_end_30} :catchall_58
    .catch Landroid/os/RemoteException; {:try_start_2f .. :try_end_30} :catch_30

    #@30
    .line 2014
    :catch_30
    move-exception v6

    #@31
    .line 2016
    :goto_31
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@34
    .line 2018
    return-void

    #@35
    .line 2009
    .restart local v2       #index:I
    :cond_35
    :try_start_35
    iget-object v6, p0, Lcom/android/server/WifiService;->mLocks:Lcom/android/server/WifiService$LockList;

    #@37
    invoke-static {v6}, Lcom/android/server/WifiService$LockList;->access$3500(Lcom/android/server/WifiService$LockList;)Ljava/util/List;

    #@3a
    move-result-object v6

    #@3b
    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@3e
    move-result-object v5

    #@3f
    check-cast v5, Lcom/android/server/WifiService$WifiLock;

    #@41
    .line 2010
    .local v5, wl:Lcom/android/server/WifiService$WifiLock;
    invoke-direct {p0, v5}, Lcom/android/server/WifiService;->noteReleaseWifiLock(Lcom/android/server/WifiService$WifiLock;)V

    #@44
    .line 2011
    if-eqz p2, :cond_52

    #@46
    new-instance v6, Landroid/os/WorkSource;

    #@48
    invoke-direct {v6, p2}, Landroid/os/WorkSource;-><init>(Landroid/os/WorkSource;)V

    #@4b
    :goto_4b
    iput-object v6, v5, Lcom/android/server/WifiService$DeathRecipient;->mWorkSource:Landroid/os/WorkSource;

    #@4d
    .line 2012
    invoke-direct {p0, v5}, Lcom/android/server/WifiService;->noteAcquireWifiLock(Lcom/android/server/WifiService$WifiLock;)V

    #@50
    .line 2013
    monitor-exit v7

    #@51
    goto :goto_31

    #@52
    .line 2011
    :cond_52
    new-instance v6, Landroid/os/WorkSource;

    #@54
    invoke-direct {v6, v4}, Landroid/os/WorkSource;-><init>(I)V
    :try_end_57
    .catchall {:try_start_35 .. :try_end_57} :catchall_2d

    #@57
    goto :goto_4b

    #@58
    .line 2016
    .end local v2           #index:I
    .end local v5           #wl:Lcom/android/server/WifiService$WifiLock;
    :catchall_58
    move-exception v6

    #@59
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@5c
    throw v6
.end method
