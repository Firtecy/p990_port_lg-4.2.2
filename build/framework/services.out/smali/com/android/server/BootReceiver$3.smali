.class Lcom/android/server/BootReceiver$3;
.super Landroid/os/FileObserver;
.source "BootReceiver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/BootReceiver;->logBootEvents(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/BootReceiver;

.field final synthetic val$db:Landroid/os/DropBoxManager;

.field final synthetic val$headers:Ljava/lang/String;

.field final synthetic val$prefs:Landroid/content/SharedPreferences;


# direct methods
.method constructor <init>(Lcom/android/server/BootReceiver;Ljava/lang/String;ILandroid/os/DropBoxManager;Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .registers 7
    .parameter
    .parameter "x0"
    .parameter "x1"
    .parameter
    .parameter
    .parameter

    #@0
    .prologue
    .line 210
    iput-object p1, p0, Lcom/android/server/BootReceiver$3;->this$0:Lcom/android/server/BootReceiver;

    #@2
    iput-object p4, p0, Lcom/android/server/BootReceiver$3;->val$db:Landroid/os/DropBoxManager;

    #@4
    iput-object p5, p0, Lcom/android/server/BootReceiver$3;->val$prefs:Landroid/content/SharedPreferences;

    #@6
    iput-object p6, p0, Lcom/android/server/BootReceiver$3;->val$headers:Ljava/lang/String;

    #@8
    invoke-direct {p0, p2, p3}, Landroid/os/FileObserver;-><init>(Ljava/lang/String;I)V

    #@b
    return-void
.end method


# virtual methods
.method public onEvent(ILjava/lang/String;)V
    .registers 13
    .parameter "event"
    .parameter "path"

    #@0
    .prologue
    .line 214
    :try_start_0
    new-instance v0, Ljava/io/File;

    #@2
    invoke-static {}, Lcom/android/server/BootReceiver;->access$200()Ljava/io/File;

    #@5
    move-result-object v1

    #@6
    invoke-direct {v0, v1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@9
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@c
    move-result-object v3

    #@d
    .line 215
    .local v3, filename:Ljava/lang/String;
    iget-object v0, p0, Lcom/android/server/BootReceiver$3;->val$db:Landroid/os/DropBoxManager;

    #@f
    iget-object v1, p0, Lcom/android/server/BootReceiver$3;->val$prefs:Landroid/content/SharedPreferences;

    #@11
    iget-object v2, p0, Lcom/android/server/BootReceiver$3;->val$headers:Ljava/lang/String;

    #@13
    invoke-static {}, Lcom/android/server/BootReceiver;->access$300()I

    #@16
    move-result v4

    #@17
    const-string v5, "SYSTEM_TOMBSTONE"

    #@19
    invoke-static/range {v0 .. v5}, Lcom/android/server/BootReceiver;->access$400(Landroid/os/DropBoxManager;Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    #@1c
    .line 217
    const-string v0, "ro.build.type"

    #@1e
    const/4 v1, 0x0

    #@1f
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@22
    move-result-object v0

    #@23
    const-string v1, "user"

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@28
    move-result v0

    #@29
    if-eqz v0, :cond_3b

    #@2b
    const-string v0, "ro.blue_handler.level"

    #@2d
    const/4 v1, 0x0

    #@2e
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@31
    move-result-object v0

    #@32
    const-string v1, "0"

    #@34
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@37
    move-result v0

    #@38
    if-eqz v0, :cond_3b

    #@3a
    .line 236
    .end local v3           #filename:Ljava/lang/String;
    :goto_3a
    return-void

    #@3b
    .line 222
    .restart local v3       #filename:Ljava/lang/String;
    :cond_3b
    new-instance v0, Ljava/lang/StringBuilder;

    #@3d
    const/16 v1, 0x200

    #@3f
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    #@42
    new-instance v1, Ljava/io/File;

    #@44
    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@47
    const/16 v2, 0x200

    #@49
    const-string v4, "[[TRUNCATED]]\n"

    #@4b
    invoke-static {v1, v2, v4}, Landroid/os/FileUtils;->readTextFile(Ljava/io/File;ILjava/lang/String;)Ljava/lang/String;

    #@4e
    move-result-object v1

    #@4f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v0

    #@53
    const-string v1, "\n"

    #@55
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v0

    #@59
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5c
    move-result-object v6

    #@5d
    .line 226
    .local v6, crashinfo:Ljava/lang/String;
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@60
    move-result-object v9

    #@61
    .line 227
    .local v9, msg:Landroid/os/Message;
    new-instance v8, Ljava/util/HashMap;

    #@63
    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    #@66
    .line 228
    .local v8, map:Ljava/util/HashMap;
    const/16 v0, 0x5c

    #@68
    iput v0, v9, Landroid/os/Message;->what:I

    #@6a
    .line 229
    iput-object v8, v9, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@6c
    .line 230
    const-string v0, "crashinfo"

    #@6e
    invoke-virtual {v8, v0, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@71
    .line 231
    iget-object v0, p0, Lcom/android/server/BootReceiver$3;->this$0:Lcom/android/server/BootReceiver;

    #@73
    iget-object v0, v0, Lcom/android/server/BootReceiver;->mHandler:Landroid/os/Handler;

    #@75
    invoke-virtual {v0, v9}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_78
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_78} :catch_79

    #@78
    goto :goto_3a

    #@79
    .line 233
    .end local v3           #filename:Ljava/lang/String;
    .end local v6           #crashinfo:Ljava/lang/String;
    .end local v8           #map:Ljava/util/HashMap;
    .end local v9           #msg:Landroid/os/Message;
    :catch_79
    move-exception v7

    #@7a
    .line 234
    .local v7, e:Ljava/io/IOException;
    const-string v0, "BootReceiver"

    #@7c
    const-string v1, "Can\'t log tombstone"

    #@7e
    invoke-static {v0, v1, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@81
    goto :goto_3a
.end method
