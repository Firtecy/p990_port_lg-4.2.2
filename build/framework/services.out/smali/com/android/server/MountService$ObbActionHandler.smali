.class Lcom/android/server/MountService$ObbActionHandler;
.super Landroid/os/Handler;
.source "MountService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/MountService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ObbActionHandler"
.end annotation


# instance fields
.field private final mActions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/server/MountService$ObbAction;",
            ">;"
        }
    .end annotation
.end field

.field private mBound:Z

.field final synthetic this$0:Lcom/android/server/MountService;


# direct methods
.method constructor <init>(Lcom/android/server/MountService;Landroid/os/Looper;)V
    .registers 4
    .parameter
    .parameter "l"

    #@0
    .prologue
    .line 2241
    iput-object p1, p0, Lcom/android/server/MountService$ObbActionHandler;->this$0:Lcom/android/server/MountService;

    #@2
    .line 2242
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@5
    .line 2238
    const/4 v0, 0x0

    #@6
    iput-boolean v0, p0, Lcom/android/server/MountService$ObbActionHandler;->mBound:Z

    #@8
    .line 2239
    new-instance v0, Ljava/util/LinkedList;

    #@a
    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    #@d
    iput-object v0, p0, Lcom/android/server/MountService$ObbActionHandler;->mActions:Ljava/util/List;

    #@f
    .line 2243
    return-void
.end method

.method private connectToService()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 2380
    new-instance v2, Landroid/content/Intent;

    #@3
    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    #@6
    sget-object v3, Lcom/android/server/MountService;->DEFAULT_CONTAINER_COMPONENT:Landroid/content/ComponentName;

    #@8
    invoke-virtual {v2, v3}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@b
    move-result-object v0

    #@c
    .line 2381
    .local v0, service:Landroid/content/Intent;
    iget-object v2, p0, Lcom/android/server/MountService$ObbActionHandler;->this$0:Lcom/android/server/MountService;

    #@e
    invoke-static {v2}, Lcom/android/server/MountService;->access$2400(Lcom/android/server/MountService;)Landroid/content/Context;

    #@11
    move-result-object v2

    #@12
    iget-object v3, p0, Lcom/android/server/MountService$ObbActionHandler;->this$0:Lcom/android/server/MountService;

    #@14
    invoke-static {v3}, Lcom/android/server/MountService;->access$2300(Lcom/android/server/MountService;)Lcom/android/server/MountService$DefaultContainerConnection;

    #@17
    move-result-object v3

    #@18
    invoke-virtual {v2, v0, v3, v1}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    #@1b
    move-result v2

    #@1c
    if-eqz v2, :cond_21

    #@1e
    .line 2382
    iput-boolean v1, p0, Lcom/android/server/MountService$ObbActionHandler;->mBound:Z

    #@20
    .line 2385
    :goto_20
    return v1

    #@21
    :cond_21
    const/4 v1, 0x0

    #@22
    goto :goto_20
.end method

.method private disconnectService()V
    .registers 3

    #@0
    .prologue
    .line 2389
    iget-object v0, p0, Lcom/android/server/MountService$ObbActionHandler;->this$0:Lcom/android/server/MountService;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-static {v0, v1}, Lcom/android/server/MountService;->access$1902(Lcom/android/server/MountService;Lcom/android/internal/app/IMediaContainerService;)Lcom/android/internal/app/IMediaContainerService;

    #@6
    .line 2390
    const/4 v0, 0x0

    #@7
    iput-boolean v0, p0, Lcom/android/server/MountService$ObbActionHandler;->mBound:Z

    #@9
    .line 2391
    iget-object v0, p0, Lcom/android/server/MountService$ObbActionHandler;->this$0:Lcom/android/server/MountService;

    #@b
    invoke-static {v0}, Lcom/android/server/MountService;->access$2400(Lcom/android/server/MountService;)Landroid/content/Context;

    #@e
    move-result-object v0

    #@f
    iget-object v1, p0, Lcom/android/server/MountService$ObbActionHandler;->this$0:Lcom/android/server/MountService;

    #@11
    invoke-static {v1}, Lcom/android/server/MountService;->access$2300(Lcom/android/server/MountService;)Lcom/android/server/MountService$DefaultContainerConnection;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    #@18
    .line 2392
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 15
    .parameter "msg"

    #@0
    .prologue
    const/4 v9, 0x2

    #@1
    const/4 v10, 0x0

    #@2
    .line 2247
    iget v8, p1, Landroid/os/Message;->what:I

    #@4
    packed-switch v8, :pswitch_data_158

    #@7
    .line 2374
    :cond_7
    :goto_7
    return-void

    #@8
    .line 2249
    :pswitch_8
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@a
    check-cast v0, Lcom/android/server/MountService$ObbAction;

    #@c
    .line 2257
    .local v0, action:Lcom/android/server/MountService$ObbAction;
    iget-boolean v8, p0, Lcom/android/server/MountService$ObbActionHandler;->mBound:Z

    #@e
    if-nez v8, :cond_21

    #@10
    .line 2260
    invoke-direct {p0}, Lcom/android/server/MountService$ObbActionHandler;->connectToService()Z

    #@13
    move-result v8

    #@14
    if-nez v8, :cond_21

    #@16
    .line 2261
    const-string v8, "MountService"

    #@18
    const-string v9, "Failed to bind to media container service"

    #@1a
    invoke-static {v8, v9}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    .line 2262
    invoke-virtual {v0}, Lcom/android/server/MountService$ObbAction;->handleError()V

    #@20
    goto :goto_7

    #@21
    .line 2267
    :cond_21
    iget-object v8, p0, Lcom/android/server/MountService$ObbActionHandler;->mActions:Ljava/util/List;

    #@23
    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@26
    goto :goto_7

    #@27
    .line 2273
    .end local v0           #action:Lcom/android/server/MountService$ObbAction;
    :pswitch_27
    iget-object v8, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@29
    if-eqz v8, :cond_34

    #@2b
    .line 2274
    iget-object v9, p0, Lcom/android/server/MountService$ObbActionHandler;->this$0:Lcom/android/server/MountService;

    #@2d
    iget-object v8, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2f
    check-cast v8, Lcom/android/internal/app/IMediaContainerService;

    #@31
    invoke-static {v9, v8}, Lcom/android/server/MountService;->access$1902(Lcom/android/server/MountService;Lcom/android/internal/app/IMediaContainerService;)Lcom/android/internal/app/IMediaContainerService;

    #@34
    .line 2276
    :cond_34
    iget-object v8, p0, Lcom/android/server/MountService$ObbActionHandler;->this$0:Lcom/android/server/MountService;

    #@36
    invoke-static {v8}, Lcom/android/server/MountService;->access$1900(Lcom/android/server/MountService;)Lcom/android/internal/app/IMediaContainerService;

    #@39
    move-result-object v8

    #@3a
    if-nez v8, :cond_5f

    #@3c
    .line 2278
    const-string v8, "MountService"

    #@3e
    const-string v9, "Cannot bind to media container service"

    #@40
    invoke-static {v8, v9}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@43
    .line 2279
    iget-object v8, p0, Lcom/android/server/MountService$ObbActionHandler;->mActions:Ljava/util/List;

    #@45
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@48
    move-result-object v3

    #@49
    .local v3, i$:Ljava/util/Iterator;
    :goto_49
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@4c
    move-result v8

    #@4d
    if-eqz v8, :cond_59

    #@4f
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@52
    move-result-object v0

    #@53
    check-cast v0, Lcom/android/server/MountService$ObbAction;

    #@55
    .line 2281
    .restart local v0       #action:Lcom/android/server/MountService$ObbAction;
    invoke-virtual {v0}, Lcom/android/server/MountService$ObbAction;->handleError()V

    #@58
    goto :goto_49

    #@59
    .line 2283
    .end local v0           #action:Lcom/android/server/MountService$ObbAction;
    :cond_59
    iget-object v8, p0, Lcom/android/server/MountService$ObbActionHandler;->mActions:Ljava/util/List;

    #@5b
    invoke-interface {v8}, Ljava/util/List;->clear()V

    #@5e
    goto :goto_7

    #@5f
    .line 2284
    .end local v3           #i$:Ljava/util/Iterator;
    :cond_5f
    iget-object v8, p0, Lcom/android/server/MountService$ObbActionHandler;->mActions:Ljava/util/List;

    #@61
    invoke-interface {v8}, Ljava/util/List;->size()I

    #@64
    move-result v8

    #@65
    if-lez v8, :cond_75

    #@67
    .line 2285
    iget-object v8, p0, Lcom/android/server/MountService$ObbActionHandler;->mActions:Ljava/util/List;

    #@69
    invoke-interface {v8, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@6c
    move-result-object v0

    #@6d
    check-cast v0, Lcom/android/server/MountService$ObbAction;

    #@6f
    .line 2286
    .restart local v0       #action:Lcom/android/server/MountService$ObbAction;
    if-eqz v0, :cond_7

    #@71
    .line 2287
    invoke-virtual {v0, p0}, Lcom/android/server/MountService$ObbAction;->execute(Lcom/android/server/MountService$ObbActionHandler;)V

    #@74
    goto :goto_7

    #@75
    .line 2291
    .end local v0           #action:Lcom/android/server/MountService$ObbAction;
    :cond_75
    const-string v8, "MountService"

    #@77
    const-string v9, "Empty queue"

    #@79
    invoke-static {v8, v9}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7c
    goto :goto_7

    #@7d
    .line 2298
    :pswitch_7d
    iget-object v8, p0, Lcom/android/server/MountService$ObbActionHandler;->mActions:Ljava/util/List;

    #@7f
    invoke-interface {v8}, Ljava/util/List;->size()I

    #@82
    move-result v8

    #@83
    if-lez v8, :cond_7

    #@85
    .line 2299
    iget-boolean v8, p0, Lcom/android/server/MountService$ObbActionHandler;->mBound:Z

    #@87
    if-eqz v8, :cond_8c

    #@89
    .line 2300
    invoke-direct {p0}, Lcom/android/server/MountService$ObbActionHandler;->disconnectService()V

    #@8c
    .line 2302
    :cond_8c
    invoke-direct {p0}, Lcom/android/server/MountService$ObbActionHandler;->connectToService()Z

    #@8f
    move-result v8

    #@90
    if-nez v8, :cond_7

    #@92
    .line 2303
    const-string v8, "MountService"

    #@94
    const-string v9, "Failed to bind to media container service"

    #@96
    invoke-static {v8, v9}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@99
    .line 2304
    iget-object v8, p0, Lcom/android/server/MountService$ObbActionHandler;->mActions:Ljava/util/List;

    #@9b
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@9e
    move-result-object v3

    #@9f
    .restart local v3       #i$:Ljava/util/Iterator;
    :goto_9f
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@a2
    move-result v8

    #@a3
    if-eqz v8, :cond_af

    #@a5
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@a8
    move-result-object v0

    #@a9
    check-cast v0, Lcom/android/server/MountService$ObbAction;

    #@ab
    .line 2306
    .restart local v0       #action:Lcom/android/server/MountService$ObbAction;
    invoke-virtual {v0}, Lcom/android/server/MountService$ObbAction;->handleError()V

    #@ae
    goto :goto_9f

    #@af
    .line 2308
    .end local v0           #action:Lcom/android/server/MountService$ObbAction;
    :cond_af
    iget-object v8, p0, Lcom/android/server/MountService$ObbActionHandler;->mActions:Ljava/util/List;

    #@b1
    invoke-interface {v8}, Ljava/util/List;->clear()V

    #@b4
    goto/16 :goto_7

    #@b6
    .line 2318
    .end local v3           #i$:Ljava/util/Iterator;
    :pswitch_b6
    iget-object v8, p0, Lcom/android/server/MountService$ObbActionHandler;->mActions:Ljava/util/List;

    #@b8
    invoke-interface {v8}, Ljava/util/List;->size()I

    #@bb
    move-result v8

    #@bc
    if-lez v8, :cond_c3

    #@be
    .line 2319
    iget-object v8, p0, Lcom/android/server/MountService$ObbActionHandler;->mActions:Ljava/util/List;

    #@c0
    invoke-interface {v8, v10}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    #@c3
    .line 2321
    :cond_c3
    iget-object v8, p0, Lcom/android/server/MountService$ObbActionHandler;->mActions:Ljava/util/List;

    #@c5
    invoke-interface {v8}, Ljava/util/List;->size()I

    #@c8
    move-result v8

    #@c9
    if-nez v8, :cond_d4

    #@cb
    .line 2322
    iget-boolean v8, p0, Lcom/android/server/MountService$ObbActionHandler;->mBound:Z

    #@cd
    if-eqz v8, :cond_7

    #@cf
    .line 2323
    invoke-direct {p0}, Lcom/android/server/MountService$ObbActionHandler;->disconnectService()V

    #@d2
    goto/16 :goto_7

    #@d4
    .line 2329
    :cond_d4
    iget-object v8, p0, Lcom/android/server/MountService$ObbActionHandler;->this$0:Lcom/android/server/MountService;

    #@d6
    invoke-static {v8}, Lcom/android/server/MountService;->access$000(Lcom/android/server/MountService;)Lcom/android/server/MountService$ObbActionHandler;

    #@d9
    move-result-object v8

    #@da
    invoke-virtual {v8, v9}, Lcom/android/server/MountService$ObbActionHandler;->sendEmptyMessage(I)Z

    #@dd
    goto/16 :goto_7

    #@df
    .line 2334
    :pswitch_df
    iget-object v6, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@e1
    check-cast v6, Ljava/lang/String;

    #@e3
    .line 2339
    .local v6, path:Ljava/lang/String;
    iget-object v8, p0, Lcom/android/server/MountService$ObbActionHandler;->this$0:Lcom/android/server/MountService;

    #@e5
    invoke-static {v8}, Lcom/android/server/MountService;->access$2000(Lcom/android/server/MountService;)Ljava/util/Map;

    #@e8
    move-result-object v9

    #@e9
    monitor-enter v9

    #@ea
    .line 2340
    :try_start_ea
    new-instance v5, Ljava/util/LinkedList;

    #@ec
    invoke-direct {v5}, Ljava/util/LinkedList;-><init>()V

    #@ef
    .line 2342
    .local v5, obbStatesToRemove:Ljava/util/List;,"Ljava/util/List<Lcom/android/server/MountService$ObbState;>;"
    iget-object v8, p0, Lcom/android/server/MountService$ObbActionHandler;->this$0:Lcom/android/server/MountService;

    #@f1
    invoke-static {v8}, Lcom/android/server/MountService;->access$2100(Lcom/android/server/MountService;)Ljava/util/Map;

    #@f4
    move-result-object v8

    #@f5
    invoke-interface {v8}, Ljava/util/Map;->values()Ljava/util/Collection;

    #@f8
    move-result-object v8

    #@f9
    invoke-interface {v8}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@fc
    move-result-object v2

    #@fd
    .line 2343
    .local v2, i:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/MountService$ObbState;>;"
    :cond_fd
    :goto_fd
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@100
    move-result v8

    #@101
    if-eqz v8, :cond_118

    #@103
    .line 2344
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@106
    move-result-object v7

    #@107
    check-cast v7, Lcom/android/server/MountService$ObbState;

    #@109
    .line 2351
    .local v7, state:Lcom/android/server/MountService$ObbState;
    iget-object v8, v7, Lcom/android/server/MountService$ObbState;->canonicalPath:Ljava/lang/String;

    #@10b
    invoke-virtual {v8, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@10e
    move-result v8

    #@10f
    if-eqz v8, :cond_fd

    #@111
    .line 2352
    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@114
    goto :goto_fd

    #@115
    .line 2370
    .end local v2           #i:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/MountService$ObbState;>;"
    .end local v5           #obbStatesToRemove:Ljava/util/List;,"Ljava/util/List<Lcom/android/server/MountService$ObbState;>;"
    .end local v7           #state:Lcom/android/server/MountService$ObbState;
    :catchall_115
    move-exception v8

    #@116
    monitor-exit v9
    :try_end_117
    .catchall {:try_start_ea .. :try_end_117} :catchall_115

    #@117
    throw v8

    #@118
    .line 2356
    .restart local v2       #i:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/android/server/MountService$ObbState;>;"
    .restart local v5       #obbStatesToRemove:Ljava/util/List;,"Ljava/util/List<Lcom/android/server/MountService$ObbState;>;"
    :cond_118
    :try_start_118
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@11b
    move-result-object v3

    #@11c
    .restart local v3       #i$:Ljava/util/Iterator;
    :goto_11c
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@11f
    move-result v8

    #@120
    if-eqz v8, :cond_154

    #@122
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@125
    move-result-object v4

    #@126
    check-cast v4, Lcom/android/server/MountService$ObbState;

    #@128
    .line 2360
    .local v4, obbState:Lcom/android/server/MountService$ObbState;
    iget-object v8, p0, Lcom/android/server/MountService$ObbActionHandler;->this$0:Lcom/android/server/MountService;

    #@12a
    invoke-static {v8, v4}, Lcom/android/server/MountService;->access$2200(Lcom/android/server/MountService;Lcom/android/server/MountService$ObbState;)V
    :try_end_12d
    .catchall {:try_start_118 .. :try_end_12d} :catchall_115

    #@12d
    .line 2363
    :try_start_12d
    iget-object v8, v4, Lcom/android/server/MountService$ObbState;->token:Landroid/os/storage/IObbActionListener;

    #@12f
    iget-object v10, v4, Lcom/android/server/MountService$ObbState;->rawPath:Ljava/lang/String;

    #@131
    iget v11, v4, Lcom/android/server/MountService$ObbState;->nonce:I

    #@133
    const/4 v12, 0x2

    #@134
    invoke-interface {v8, v10, v11, v12}, Landroid/os/storage/IObbActionListener;->onObbResult(Ljava/lang/String;II)V
    :try_end_137
    .catchall {:try_start_12d .. :try_end_137} :catchall_115
    .catch Landroid/os/RemoteException; {:try_start_12d .. :try_end_137} :catch_138

    #@137
    goto :goto_11c

    #@138
    .line 2365
    :catch_138
    move-exception v1

    #@139
    .line 2366
    .local v1, e:Landroid/os/RemoteException;
    :try_start_139
    const-string v8, "MountService"

    #@13b
    new-instance v10, Ljava/lang/StringBuilder;

    #@13d
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@140
    const-string v11, "Couldn\'t send unmount notification for  OBB: "

    #@142
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@145
    move-result-object v10

    #@146
    iget-object v11, v4, Lcom/android/server/MountService$ObbState;->rawPath:Ljava/lang/String;

    #@148
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14b
    move-result-object v10

    #@14c
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14f
    move-result-object v10

    #@150
    invoke-static {v8, v10}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@153
    goto :goto_11c

    #@154
    .line 2370
    .end local v1           #e:Landroid/os/RemoteException;
    .end local v4           #obbState:Lcom/android/server/MountService$ObbState;
    :cond_154
    monitor-exit v9
    :try_end_155
    .catchall {:try_start_139 .. :try_end_155} :catchall_115

    #@155
    goto/16 :goto_7

    #@157
    .line 2247
    nop

    #@158
    :pswitch_data_158
    .packed-switch 0x1
        :pswitch_8
        :pswitch_27
        :pswitch_b6
        :pswitch_7d
        :pswitch_df
    .end packed-switch
.end method
