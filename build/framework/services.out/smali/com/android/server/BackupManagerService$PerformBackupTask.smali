.class Lcom/android/server/BackupManagerService$PerformBackupTask;
.super Ljava/lang/Object;
.source "BackupManagerService.java"

# interfaces
.implements Lcom/android/server/BackupManagerService$BackupRestoreTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/BackupManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PerformBackupTask"
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "PerformBackupTask"


# instance fields
.field mBackupData:Landroid/os/ParcelFileDescriptor;

.field mBackupDataName:Ljava/io/File;

.field mCurrentPackage:Landroid/content/pm/PackageInfo;

.field mCurrentState:Lcom/android/server/BackupManagerService$BackupState;

.field mFinished:Z

.field mJournal:Ljava/io/File;

.field mNewState:Landroid/os/ParcelFileDescriptor;

.field mNewStateName:Ljava/io/File;

.field mOriginalQueue:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/BackupManagerService$BackupRequest;",
            ">;"
        }
    .end annotation
.end field

.field mQueue:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/BackupManagerService$BackupRequest;",
            ">;"
        }
    .end annotation
.end field

.field mSavedState:Landroid/os/ParcelFileDescriptor;

.field mSavedStateName:Ljava/io/File;

.field mStateDir:Ljava/io/File;

.field mStatus:I

.field mTransport:Lcom/android/internal/backup/IBackupTransport;

.field final synthetic this$0:Lcom/android/server/BackupManagerService;


# direct methods
.method public constructor <init>(Lcom/android/server/BackupManagerService;Lcom/android/internal/backup/IBackupTransport;Ljava/util/ArrayList;Ljava/io/File;)V
    .registers 8
    .parameter
    .parameter "transport"
    .parameter
    .parameter "journal"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/internal/backup/IBackupTransport;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/server/BackupManagerService$BackupRequest;",
            ">;",
            "Ljava/io/File;",
            ")V"
        }
    .end annotation

    #@0
    .prologue
    .line 1821
    .local p3, queue:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/BackupManagerService$BackupRequest;>;"
    iput-object p1, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 1822
    iput-object p2, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mTransport:Lcom/android/internal/backup/IBackupTransport;

    #@7
    .line 1823
    iput-object p3, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mOriginalQueue:Ljava/util/ArrayList;

    #@9
    .line 1824
    iput-object p4, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mJournal:Ljava/io/File;

    #@b
    .line 1827
    :try_start_b
    new-instance v0, Ljava/io/File;

    #@d
    iget-object v1, p1, Lcom/android/server/BackupManagerService;->mBaseStateDir:Ljava/io/File;

    #@f
    invoke-interface {p2}, Lcom/android/internal/backup/IBackupTransport;->transportDirName()Ljava/lang/String;

    #@12
    move-result-object v2

    #@13
    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@16
    iput-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStateDir:Ljava/io/File;
    :try_end_18
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_18} :catch_25

    #@18
    .line 1832
    :goto_18
    sget-object v0, Lcom/android/server/BackupManagerService$BackupState;->INITIAL:Lcom/android/server/BackupManagerService$BackupState;

    #@1a
    iput-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mCurrentState:Lcom/android/server/BackupManagerService$BackupState;

    #@1c
    .line 1833
    const/4 v0, 0x0

    #@1d
    iput-boolean v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mFinished:Z

    #@1f
    .line 1835
    const-string v0, "STATE => INITIAL"

    #@21
    invoke-virtual {p1, v0}, Lcom/android/server/BackupManagerService;->addBackupTrace(Ljava/lang/String;)V

    #@24
    .line 1836
    return-void

    #@25
    .line 1828
    :catch_25
    move-exception v0

    #@26
    goto :goto_18
.end method


# virtual methods
.method agentErrorCleanup()V
    .registers 2

    #@0
    .prologue
    .line 2263
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mBackupDataName:Ljava/io/File;

    #@2
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    #@5
    .line 2264
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mNewStateName:Ljava/io/File;

    #@7
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    #@a
    .line 2265
    invoke-virtual {p0}, Lcom/android/server/BackupManagerService$PerformBackupTask;->clearAgentState()V

    #@d
    .line 2267
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mQueue:Ljava/util/ArrayList;

    #@f
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    #@12
    move-result v0

    #@13
    if-eqz v0, :cond_1b

    #@15
    sget-object v0, Lcom/android/server/BackupManagerService$BackupState;->FINAL:Lcom/android/server/BackupManagerService$BackupState;

    #@17
    :goto_17
    invoke-virtual {p0, v0}, Lcom/android/server/BackupManagerService$PerformBackupTask;->executeNextState(Lcom/android/server/BackupManagerService$BackupState;)V

    #@1a
    .line 2268
    return-void

    #@1b
    .line 2267
    :cond_1b
    sget-object v0, Lcom/android/server/BackupManagerService$BackupState;->RUNNING_QUEUE:Lcom/android/server/BackupManagerService$BackupState;

    #@1d
    goto :goto_17
.end method

.method beginBackup()V
    .registers 12

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    .line 1865
    iget-object v7, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@3
    invoke-virtual {v7}, Lcom/android/server/BackupManagerService;->clearBackupTrace()V

    #@6
    .line 1866
    new-instance v0, Ljava/lang/StringBuilder;

    #@8
    const/16 v7, 0x100

    #@a
    invoke-direct {v0, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    #@d
    .line 1867
    .local v0, b:Ljava/lang/StringBuilder;
    const-string v7, "beginBackup: ["

    #@f
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    .line 1868
    iget-object v7, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mOriginalQueue:Ljava/util/ArrayList;

    #@14
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@17
    move-result-object v2

    #@18
    .local v2, i$:Ljava/util/Iterator;
    :goto_18
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@1b
    move-result v7

    #@1c
    if-eqz v7, :cond_2f

    #@1e
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@21
    move-result-object v5

    #@22
    check-cast v5, Lcom/android/server/BackupManagerService$BackupRequest;

    #@24
    .line 1869
    .local v5, req:Lcom/android/server/BackupManagerService$BackupRequest;
    const/16 v7, 0x20

    #@26
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@29
    .line 1870
    iget-object v7, v5, Lcom/android/server/BackupManagerService$BackupRequest;->packageName:Ljava/lang/String;

    #@2b
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    goto :goto_18

    #@2f
    .line 1872
    .end local v5           #req:Lcom/android/server/BackupManagerService$BackupRequest;
    :cond_2f
    const-string v7, " ]"

    #@31
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    .line 1873
    iget-object v7, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@36
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v8

    #@3a
    invoke-virtual {v7, v8}, Lcom/android/server/BackupManagerService;->addBackupTrace(Ljava/lang/String;)V

    #@3d
    .line 1876
    iput v9, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I

    #@3f
    .line 1879
    iget-object v7, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mOriginalQueue:Ljava/util/ArrayList;

    #@41
    invoke-virtual {v7}, Ljava/util/ArrayList;->isEmpty()Z

    #@44
    move-result v7

    #@45
    if-eqz v7, :cond_5b

    #@47
    .line 1880
    const-string v7, "PerformBackupTask"

    #@49
    const-string v8, "Backup begun with an empty queue - nothing to do."

    #@4b
    invoke-static {v7, v8}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    .line 1881
    iget-object v7, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@50
    const-string v8, "queue empty at begin"

    #@52
    invoke-virtual {v7, v8}, Lcom/android/server/BackupManagerService;->addBackupTrace(Ljava/lang/String;)V

    #@55
    .line 1882
    sget-object v7, Lcom/android/server/BackupManagerService$BackupState;->FINAL:Lcom/android/server/BackupManagerService$BackupState;

    #@57
    invoke-virtual {p0, v7}, Lcom/android/server/BackupManagerService$PerformBackupTask;->executeNextState(Lcom/android/server/BackupManagerService$BackupState;)V

    #@5a
    .line 1949
    :cond_5a
    :goto_5a
    return-void

    #@5b
    .line 1889
    :cond_5b
    iget-object v7, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mOriginalQueue:Ljava/util/ArrayList;

    #@5d
    invoke-virtual {v7}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    #@60
    move-result-object v7

    #@61
    check-cast v7, Ljava/util/ArrayList;

    #@63
    iput-object v7, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mQueue:Ljava/util/ArrayList;

    #@65
    .line 1893
    new-instance v4, Ljava/io/File;

    #@67
    iget-object v7, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStateDir:Ljava/io/File;

    #@69
    const-string v8, "@pm@"

    #@6b
    invoke-direct {v4, v7, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@6e
    .line 1895
    .local v4, pmState:Ljava/io/File;
    :try_start_6e
    iget-object v7, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mTransport:Lcom/android/internal/backup/IBackupTransport;

    #@70
    invoke-interface {v7}, Lcom/android/internal/backup/IBackupTransport;->transportDirName()Ljava/lang/String;

    #@73
    move-result-object v6

    #@74
    .line 1896
    .local v6, transportName:Ljava/lang/String;
    const/16 v7, 0xb05

    #@76
    invoke-static {v7, v6}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    #@79
    .line 1899
    iget v7, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I

    #@7b
    if-nez v7, :cond_db

    #@7d
    invoke-virtual {v4}, Ljava/io/File;->length()J

    #@80
    move-result-wide v7

    #@81
    const-wide/16 v9, 0x0

    #@83
    cmp-long v7, v7, v9

    #@85
    if-gtz v7, :cond_db

    #@87
    .line 1900
    const-string v7, "PerformBackupTask"

    #@89
    const-string v8, "Initializing (wiping) backup state and transport storage"

    #@8b
    invoke-static {v7, v8}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@8e
    .line 1901
    iget-object v7, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@90
    new-instance v8, Ljava/lang/StringBuilder;

    #@92
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@95
    const-string v9, "initializing transport "

    #@97
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v8

    #@9b
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v8

    #@9f
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a2
    move-result-object v8

    #@a3
    invoke-virtual {v7, v8}, Lcom/android/server/BackupManagerService;->addBackupTrace(Ljava/lang/String;)V

    #@a6
    .line 1902
    iget-object v7, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@a8
    iget-object v8, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStateDir:Ljava/io/File;

    #@aa
    invoke-virtual {v7, v8}, Lcom/android/server/BackupManagerService;->resetBackupState(Ljava/io/File;)V

    #@ad
    .line 1903
    iget-object v7, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mTransport:Lcom/android/internal/backup/IBackupTransport;

    #@af
    invoke-interface {v7}, Lcom/android/internal/backup/IBackupTransport;->initializeDevice()I

    #@b2
    move-result v7

    #@b3
    iput v7, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I

    #@b5
    .line 1905
    iget-object v7, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@b7
    new-instance v8, Ljava/lang/StringBuilder;

    #@b9
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@bc
    const-string v9, "transport.initializeDevice() == "

    #@be
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c1
    move-result-object v8

    #@c2
    iget v9, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I

    #@c4
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c7
    move-result-object v8

    #@c8
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cb
    move-result-object v8

    #@cc
    invoke-virtual {v7, v8}, Lcom/android/server/BackupManagerService;->addBackupTrace(Ljava/lang/String;)V

    #@cf
    .line 1906
    iget v7, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I

    #@d1
    if-nez v7, :cond_158

    #@d3
    .line 1907
    const/16 v7, 0xb0b

    #@d5
    const/4 v8, 0x0

    #@d6
    new-array v8, v8, [Ljava/lang/Object;

    #@d8
    invoke-static {v7, v8}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@db
    .line 1919
    :cond_db
    :goto_db
    iget v7, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I

    #@dd
    if-nez v7, :cond_11c

    #@df
    .line 1920
    new-instance v3, Lcom/android/server/PackageManagerBackupAgent;

    #@e1
    iget-object v7, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@e3
    invoke-static {v7}, Lcom/android/server/BackupManagerService;->access$600(Lcom/android/server/BackupManagerService;)Landroid/content/pm/PackageManager;

    #@e6
    move-result-object v7

    #@e7
    iget-object v8, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@e9
    invoke-virtual {v8}, Lcom/android/server/BackupManagerService;->allAgentPackages()Ljava/util/List;

    #@ec
    move-result-object v8

    #@ed
    invoke-direct {v3, v7, v8}, Lcom/android/server/PackageManagerBackupAgent;-><init>(Landroid/content/pm/PackageManager;Ljava/util/List;)V

    #@f0
    .line 1922
    .local v3, pmAgent:Lcom/android/server/PackageManagerBackupAgent;
    const-string v7, "@pm@"

    #@f2
    invoke-virtual {v3}, Lcom/android/server/PackageManagerBackupAgent;->onBind()Landroid/os/IBinder;

    #@f5
    move-result-object v8

    #@f6
    invoke-static {v8}, Landroid/app/IBackupAgent$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/IBackupAgent;

    #@f9
    move-result-object v8

    #@fa
    iget-object v9, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mTransport:Lcom/android/internal/backup/IBackupTransport;

    #@fc
    invoke-virtual {p0, v7, v8, v9}, Lcom/android/server/BackupManagerService$PerformBackupTask;->invokeAgentForBackup(Ljava/lang/String;Landroid/app/IBackupAgent;Lcom/android/internal/backup/IBackupTransport;)I

    #@ff
    move-result v7

    #@100
    iput v7, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I

    #@102
    .line 1924
    iget-object v7, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@104
    new-instance v8, Ljava/lang/StringBuilder;

    #@106
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@109
    const-string v9, "PMBA invoke: "

    #@10b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10e
    move-result-object v8

    #@10f
    iget v9, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I

    #@111
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@114
    move-result-object v8

    #@115
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@118
    move-result-object v8

    #@119
    invoke-virtual {v7, v8}, Lcom/android/server/BackupManagerService;->addBackupTrace(Ljava/lang/String;)V

    #@11c
    .line 1927
    .end local v3           #pmAgent:Lcom/android/server/PackageManagerBackupAgent;
    :cond_11c
    iget v7, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I

    #@11e
    const/4 v8, 0x2

    #@11f
    if-ne v7, v8, :cond_12c

    #@121
    .line 1931
    const/16 v7, 0xb0a

    #@123
    iget-object v8, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mTransport:Lcom/android/internal/backup/IBackupTransport;

    #@125
    invoke-interface {v8}, Lcom/android/internal/backup/IBackupTransport;->transportDirName()Ljava/lang/String;

    #@128
    move-result-object v8

    #@129
    invoke-static {v7, v8}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I
    :try_end_12c
    .catchall {:try_start_6e .. :try_end_12c} :catchall_1b7
    .catch Ljava/lang/Exception; {:try_start_6e .. :try_end_12c} :catch_168

    #@12c
    .line 1941
    :cond_12c
    iget-object v7, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@12e
    new-instance v8, Ljava/lang/StringBuilder;

    #@130
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@133
    const-string v9, "exiting prelim: "

    #@135
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@138
    move-result-object v8

    #@139
    iget v9, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I

    #@13b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13e
    move-result-object v8

    #@13f
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@142
    move-result-object v8

    #@143
    invoke-virtual {v7, v8}, Lcom/android/server/BackupManagerService;->addBackupTrace(Ljava/lang/String;)V

    #@146
    .line 1942
    iget v7, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I

    #@148
    if-eqz v7, :cond_5a

    #@14a
    .line 1945
    iget-object v7, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@14c
    iget-object v8, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStateDir:Ljava/io/File;

    #@14e
    invoke-virtual {v7, v8}, Lcom/android/server/BackupManagerService;->resetBackupState(Ljava/io/File;)V

    #@151
    .line 1946
    sget-object v7, Lcom/android/server/BackupManagerService$BackupState;->FINAL:Lcom/android/server/BackupManagerService$BackupState;

    #@153
    invoke-virtual {p0, v7}, Lcom/android/server/BackupManagerService$PerformBackupTask;->executeNextState(Lcom/android/server/BackupManagerService$BackupState;)V

    #@156
    goto/16 :goto_5a

    #@158
    .line 1909
    :cond_158
    const/16 v7, 0xb06

    #@15a
    :try_start_15a
    const-string v8, "(initialize)"

    #@15c
    invoke-static {v7, v8}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    #@15f
    .line 1910
    const-string v7, "PerformBackupTask"

    #@161
    const-string v8, "Transport error in initializeDevice()"

    #@163
    invoke-static {v7, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_166
    .catchall {:try_start_15a .. :try_end_166} :catchall_1b7
    .catch Ljava/lang/Exception; {:try_start_15a .. :try_end_166} :catch_168

    #@166
    goto/16 :goto_db

    #@168
    .line 1933
    .end local v6           #transportName:Ljava/lang/String;
    :catch_168
    move-exception v1

    #@169
    .line 1934
    .local v1, e:Ljava/lang/Exception;
    :try_start_169
    const-string v7, "PerformBackupTask"

    #@16b
    const-string v8, "Error in backup thread"

    #@16d
    invoke-static {v7, v8, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@170
    .line 1935
    iget-object v7, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@172
    new-instance v8, Ljava/lang/StringBuilder;

    #@174
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@177
    const-string v9, "Exception in backup thread: "

    #@179
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17c
    move-result-object v8

    #@17d
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@180
    move-result-object v8

    #@181
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@184
    move-result-object v8

    #@185
    invoke-virtual {v7, v8}, Lcom/android/server/BackupManagerService;->addBackupTrace(Ljava/lang/String;)V

    #@188
    .line 1936
    const/4 v7, 0x1

    #@189
    iput v7, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I
    :try_end_18b
    .catchall {:try_start_169 .. :try_end_18b} :catchall_1b7

    #@18b
    .line 1941
    iget-object v7, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@18d
    new-instance v8, Ljava/lang/StringBuilder;

    #@18f
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@192
    const-string v9, "exiting prelim: "

    #@194
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@197
    move-result-object v8

    #@198
    iget v9, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I

    #@19a
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19d
    move-result-object v8

    #@19e
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a1
    move-result-object v8

    #@1a2
    invoke-virtual {v7, v8}, Lcom/android/server/BackupManagerService;->addBackupTrace(Ljava/lang/String;)V

    #@1a5
    .line 1942
    iget v7, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I

    #@1a7
    if-eqz v7, :cond_5a

    #@1a9
    .line 1945
    iget-object v7, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@1ab
    iget-object v8, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStateDir:Ljava/io/File;

    #@1ad
    invoke-virtual {v7, v8}, Lcom/android/server/BackupManagerService;->resetBackupState(Ljava/io/File;)V

    #@1b0
    .line 1946
    sget-object v7, Lcom/android/server/BackupManagerService$BackupState;->FINAL:Lcom/android/server/BackupManagerService$BackupState;

    #@1b2
    invoke-virtual {p0, v7}, Lcom/android/server/BackupManagerService$PerformBackupTask;->executeNextState(Lcom/android/server/BackupManagerService$BackupState;)V

    #@1b5
    goto/16 :goto_5a

    #@1b7
    .line 1941
    .end local v1           #e:Ljava/lang/Exception;
    :catchall_1b7
    move-exception v7

    #@1b8
    iget-object v8, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@1ba
    new-instance v9, Ljava/lang/StringBuilder;

    #@1bc
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@1bf
    const-string v10, "exiting prelim: "

    #@1c1
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c4
    move-result-object v9

    #@1c5
    iget v10, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I

    #@1c7
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1ca
    move-result-object v9

    #@1cb
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ce
    move-result-object v9

    #@1cf
    invoke-virtual {v8, v9}, Lcom/android/server/BackupManagerService;->addBackupTrace(Ljava/lang/String;)V

    #@1d2
    .line 1942
    iget v8, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I

    #@1d4
    if-eqz v8, :cond_1e2

    #@1d6
    .line 1945
    iget-object v8, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@1d8
    iget-object v9, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStateDir:Ljava/io/File;

    #@1da
    invoke-virtual {v8, v9}, Lcom/android/server/BackupManagerService;->resetBackupState(Ljava/io/File;)V

    #@1dd
    .line 1946
    sget-object v8, Lcom/android/server/BackupManagerService$BackupState;->FINAL:Lcom/android/server/BackupManagerService$BackupState;

    #@1df
    invoke-virtual {p0, v8}, Lcom/android/server/BackupManagerService$PerformBackupTask;->executeNextState(Lcom/android/server/BackupManagerService$BackupState;)V

    #@1e2
    :cond_1e2
    throw v7
.end method

.method clearAgentState()V
    .registers 4

    #@0
    .prologue
    .line 2272
    :try_start_0
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mSavedState:Landroid/os/ParcelFileDescriptor;

    #@2
    if-eqz v0, :cond_9

    #@4
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mSavedState:Landroid/os/ParcelFileDescriptor;

    #@6
    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_9} :catch_68

    #@9
    .line 2273
    :cond_9
    :goto_9
    :try_start_9
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mBackupData:Landroid/os/ParcelFileDescriptor;

    #@b
    if-eqz v0, :cond_12

    #@d
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mBackupData:Landroid/os/ParcelFileDescriptor;

    #@f
    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_12} :catch_66

    #@12
    .line 2274
    :cond_12
    :goto_12
    :try_start_12
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mNewState:Landroid/os/ParcelFileDescriptor;

    #@14
    if-eqz v0, :cond_1b

    #@16
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mNewState:Landroid/os/ParcelFileDescriptor;

    #@18
    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_1b
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_1b} :catch_64

    #@1b
    .line 2275
    :cond_1b
    :goto_1b
    const/4 v0, 0x0

    #@1c
    iput-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mNewState:Landroid/os/ParcelFileDescriptor;

    #@1e
    iput-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mBackupData:Landroid/os/ParcelFileDescriptor;

    #@20
    iput-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mSavedState:Landroid/os/ParcelFileDescriptor;

    #@22
    .line 2276
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@24
    iget-object v1, v0, Lcom/android/server/BackupManagerService;->mCurrentOpLock:Ljava/lang/Object;

    #@26
    monitor-enter v1

    #@27
    .line 2277
    :try_start_27
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@29
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mCurrentOperations:Landroid/util/SparseArray;

    #@2b
    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    #@2e
    .line 2278
    monitor-exit v1
    :try_end_2f
    .catchall {:try_start_27 .. :try_end_2f} :catchall_5f

    #@2f
    .line 2281
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mCurrentPackage:Landroid/content/pm/PackageInfo;

    #@31
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@33
    if-eqz v0, :cond_5e

    #@35
    .line 2282
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@37
    new-instance v1, Ljava/lang/StringBuilder;

    #@39
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@3c
    const-string v2, "unbinding "

    #@3e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v1

    #@42
    iget-object v2, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mCurrentPackage:Landroid/content/pm/PackageInfo;

    #@44
    iget-object v2, v2, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@46
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v1

    #@4a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v1

    #@4e
    invoke-virtual {v0, v1}, Lcom/android/server/BackupManagerService;->addBackupTrace(Ljava/lang/String;)V

    #@51
    .line 2284
    :try_start_51
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@53
    invoke-static {v0}, Lcom/android/server/BackupManagerService;->access$800(Lcom/android/server/BackupManagerService;)Landroid/app/IActivityManager;

    #@56
    move-result-object v0

    #@57
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mCurrentPackage:Landroid/content/pm/PackageInfo;

    #@59
    iget-object v1, v1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@5b
    invoke-interface {v0, v1}, Landroid/app/IActivityManager;->unbindBackupAgent(Landroid/content/pm/ApplicationInfo;)V
    :try_end_5e
    .catch Landroid/os/RemoteException; {:try_start_51 .. :try_end_5e} :catch_62

    #@5e
    .line 2287
    :cond_5e
    :goto_5e
    return-void

    #@5f
    .line 2278
    :catchall_5f
    move-exception v0

    #@60
    :try_start_60
    monitor-exit v1
    :try_end_61
    .catchall {:try_start_60 .. :try_end_61} :catchall_5f

    #@61
    throw v0

    #@62
    .line 2285
    :catch_62
    move-exception v0

    #@63
    goto :goto_5e

    #@64
    .line 2274
    :catch_64
    move-exception v0

    #@65
    goto :goto_1b

    #@66
    .line 2273
    :catch_66
    move-exception v0

    #@67
    goto :goto_12

    #@68
    .line 2272
    :catch_68
    move-exception v0

    #@69
    goto :goto_9
.end method

.method clearMetadata()V
    .registers 4

    #@0
    .prologue
    .line 2093
    new-instance v0, Ljava/io/File;

    #@2
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStateDir:Ljava/io/File;

    #@4
    const-string v2, "@pm@"

    #@6
    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@9
    .line 2094
    .local v0, pmState:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    #@c
    move-result v1

    #@d
    if-eqz v1, :cond_12

    #@f
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    #@12
    .line 2095
    :cond_12
    return-void
.end method

.method public execute()V
    .registers 3

    #@0
    .prologue
    .line 1842
    sget-object v0, Lcom/android/server/BackupManagerService$4;->$SwitchMap$com$android$server$BackupManagerService$BackupState:[I

    #@2
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mCurrentState:Lcom/android/server/BackupManagerService$BackupState;

    #@4
    invoke-virtual {v1}, Lcom/android/server/BackupManagerService$BackupState;->ordinal()I

    #@7
    move-result v1

    #@8
    aget v0, v0, v1

    #@a
    packed-switch v0, :pswitch_data_2a

    #@d
    .line 1859
    :goto_d
    return-void

    #@e
    .line 1844
    :pswitch_e
    invoke-virtual {p0}, Lcom/android/server/BackupManagerService$PerformBackupTask;->beginBackup()V

    #@11
    goto :goto_d

    #@12
    .line 1848
    :pswitch_12
    invoke-virtual {p0}, Lcom/android/server/BackupManagerService$PerformBackupTask;->invokeNextAgent()V

    #@15
    goto :goto_d

    #@16
    .line 1852
    :pswitch_16
    iget-boolean v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mFinished:Z

    #@18
    if-nez v0, :cond_21

    #@1a
    invoke-virtual {p0}, Lcom/android/server/BackupManagerService$PerformBackupTask;->finalizeBackup()V

    #@1d
    .line 1856
    :goto_1d
    const/4 v0, 0x1

    #@1e
    iput-boolean v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mFinished:Z

    #@20
    goto :goto_d

    #@21
    .line 1854
    :cond_21
    const-string v0, "PerformBackupTask"

    #@23
    const-string v1, "Duplicate finish"

    #@25
    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    goto :goto_1d

    #@29
    .line 1842
    nop

    #@2a
    :pswitch_data_2a
    .packed-switch 0x1
        :pswitch_e
        :pswitch_12
        :pswitch_16
    .end packed-switch
.end method

.method executeNextState(Lcom/android/server/BackupManagerService$BackupState;)V
    .registers 6
    .parameter "nextState"

    #@0
    .prologue
    .line 2301
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "executeNextState => "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v1, v2}, Lcom/android/server/BackupManagerService;->addBackupTrace(Ljava/lang/String;)V

    #@18
    .line 2302
    iput-object p1, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mCurrentState:Lcom/android/server/BackupManagerService$BackupState;

    #@1a
    .line 2303
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@1c
    iget-object v1, v1, Lcom/android/server/BackupManagerService;->mBackupHandler:Lcom/android/server/BackupManagerService$BackupHandler;

    #@1e
    const/16 v2, 0x14

    #@20
    invoke-virtual {v1, v2, p0}, Lcom/android/server/BackupManagerService$BackupHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@23
    move-result-object v0

    #@24
    .line 2304
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@26
    iget-object v1, v1, Lcom/android/server/BackupManagerService;->mBackupHandler:Lcom/android/server/BackupManagerService$BackupHandler;

    #@28
    invoke-virtual {v1, v0}, Lcom/android/server/BackupManagerService$BackupHandler;->sendMessage(Landroid/os/Message;)Z

    #@2b
    .line 2305
    return-void
.end method

.method finalizeBackup()V
    .registers 5

    #@0
    .prologue
    .line 2050
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@2
    const-string v1, "finishing"

    #@4
    invoke-virtual {v0, v1}, Lcom/android/server/BackupManagerService;->addBackupTrace(Ljava/lang/String;)V

    #@7
    .line 2056
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mJournal:Ljava/io/File;

    #@9
    if-eqz v0, :cond_2d

    #@b
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mJournal:Ljava/io/File;

    #@d
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    #@10
    move-result v0

    #@11
    if-nez v0, :cond_2d

    #@13
    .line 2057
    const-string v0, "PerformBackupTask"

    #@15
    new-instance v1, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v2, "Unable to remove backup journal file "

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    iget-object v2, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mJournal:Ljava/io/File;

    #@22
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v1

    #@26
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v1

    #@2a
    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 2063
    :cond_2d
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@2f
    iget-wide v0, v0, Lcom/android/server/BackupManagerService;->mCurrentToken:J

    #@31
    const-wide/16 v2, 0x0

    #@33
    cmp-long v0, v0, v2

    #@35
    if-nez v0, :cond_51

    #@37
    iget v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I

    #@39
    if-nez v0, :cond_51

    #@3b
    .line 2064
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@3d
    const-string v1, "success; recording token"

    #@3f
    invoke-virtual {v0, v1}, Lcom/android/server/BackupManagerService;->addBackupTrace(Ljava/lang/String;)V

    #@42
    .line 2066
    :try_start_42
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@44
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mTransport:Lcom/android/internal/backup/IBackupTransport;

    #@46
    invoke-interface {v1}, Lcom/android/internal/backup/IBackupTransport;->getCurrentRestoreSet()J

    #@49
    move-result-wide v1

    #@4a
    iput-wide v1, v0, Lcom/android/server/BackupManagerService;->mCurrentToken:J
    :try_end_4c
    .catch Landroid/os/RemoteException; {:try_start_42 .. :try_end_4c} :catch_87

    #@4c
    .line 2068
    :goto_4c
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@4e
    invoke-virtual {v0}, Lcom/android/server/BackupManagerService;->writeRestoreTokens()V

    #@51
    .line 2074
    :cond_51
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@53
    iget-object v1, v0, Lcom/android/server/BackupManagerService;->mQueueLock:Ljava/lang/Object;

    #@55
    monitor-enter v1

    #@56
    .line 2075
    :try_start_56
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@58
    const/4 v2, 0x0

    #@59
    iput-boolean v2, v0, Lcom/android/server/BackupManagerService;->mBackupRunning:Z

    #@5b
    .line 2076
    iget v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I

    #@5d
    const/4 v2, 0x2

    #@5e
    if-ne v0, v2, :cond_6f

    #@60
    .line 2078
    invoke-virtual {p0}, Lcom/android/server/BackupManagerService$PerformBackupTask;->clearMetadata()V

    #@63
    .line 2080
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@65
    const-string v2, "init required; rerunning"

    #@67
    invoke-virtual {v0, v2}, Lcom/android/server/BackupManagerService;->addBackupTrace(Ljava/lang/String;)V

    #@6a
    .line 2081
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@6c
    invoke-virtual {v0}, Lcom/android/server/BackupManagerService;->backupNow()V

    #@6f
    .line 2083
    :cond_6f
    monitor-exit v1
    :try_end_70
    .catchall {:try_start_56 .. :try_end_70} :catchall_84

    #@70
    .line 2086
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@72
    invoke-virtual {v0}, Lcom/android/server/BackupManagerService;->clearBackupTrace()V

    #@75
    .line 2087
    const-string v0, "PerformBackupTask"

    #@77
    const-string v1, "Backup pass finished."

    #@79
    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7c
    .line 2088
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@7e
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mWakelock:Landroid/os/PowerManager$WakeLock;

    #@80
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    #@83
    .line 2089
    return-void

    #@84
    .line 2083
    :catchall_84
    move-exception v0

    #@85
    :try_start_85
    monitor-exit v1
    :try_end_86
    .catchall {:try_start_85 .. :try_end_86} :catchall_84

    #@86
    throw v0

    #@87
    .line 2067
    :catch_87
    move-exception v0

    #@88
    goto :goto_4c
.end method

.method public handleTimeout()V
    .registers 5

    #@0
    .prologue
    .line 2243
    const-string v0, "PerformBackupTask"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "Timeout backing up "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget-object v2, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mCurrentPackage:Landroid/content/pm/PackageInfo;

    #@f
    iget-object v2, v2, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 2244
    const/16 v0, 0xb07

    #@1e
    const/4 v1, 0x2

    #@1f
    new-array v1, v1, [Ljava/lang/Object;

    #@21
    const/4 v2, 0x0

    #@22
    iget-object v3, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mCurrentPackage:Landroid/content/pm/PackageInfo;

    #@24
    iget-object v3, v3, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@26
    aput-object v3, v1, v2

    #@28
    const/4 v2, 0x1

    #@29
    const-string v3, "timeout"

    #@2b
    aput-object v3, v1, v2

    #@2d
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@30
    .line 2246
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@32
    new-instance v1, Ljava/lang/StringBuilder;

    #@34
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@37
    const-string v2, "timeout of "

    #@39
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v1

    #@3d
    iget-object v2, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mCurrentPackage:Landroid/content/pm/PackageInfo;

    #@3f
    iget-object v2, v2, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@41
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v1

    #@45
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v1

    #@49
    invoke-virtual {v0, v1}, Lcom/android/server/BackupManagerService;->addBackupTrace(Ljava/lang/String;)V

    #@4c
    .line 2247
    invoke-virtual {p0}, Lcom/android/server/BackupManagerService$PerformBackupTask;->agentErrorCleanup()V

    #@4f
    .line 2248
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@51
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mCurrentPackage:Landroid/content/pm/PackageInfo;

    #@53
    iget-object v1, v1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@55
    invoke-static {v0, v1}, Lcom/android/server/BackupManagerService;->access$700(Lcom/android/server/BackupManagerService;Ljava/lang/String;)V

    #@58
    .line 2249
    return-void
.end method

.method invokeAgentForBackup(Ljava/lang/String;Landroid/app/IBackupAgent;Lcom/android/internal/backup/IBackupTransport;)I
    .registers 12
    .parameter "packageName"
    .parameter "agent"
    .parameter "transport"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v5, 0x0

    #@2
    .line 2102
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@4
    new-instance v1, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v2, "invoking "

    #@b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v0, v1}, Lcom/android/server/BackupManagerService;->addBackupTrace(Ljava/lang/String;)V

    #@1a
    .line 2104
    new-instance v0, Ljava/io/File;

    #@1c
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStateDir:Ljava/io/File;

    #@1e
    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@21
    iput-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mSavedStateName:Ljava/io/File;

    #@23
    .line 2105
    new-instance v0, Ljava/io/File;

    #@25
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@27
    iget-object v1, v1, Lcom/android/server/BackupManagerService;->mDataDir:Ljava/io/File;

    #@29
    new-instance v2, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v2

    #@32
    const-string v3, ".data"

    #@34
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v2

    #@38
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v2

    #@3c
    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@3f
    iput-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mBackupDataName:Ljava/io/File;

    #@41
    .line 2106
    new-instance v0, Ljava/io/File;

    #@43
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStateDir:Ljava/io/File;

    #@45
    new-instance v2, Ljava/lang/StringBuilder;

    #@47
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@4a
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v2

    #@4e
    const-string v3, ".new"

    #@50
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v2

    #@54
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@57
    move-result-object v2

    #@58
    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@5b
    iput-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mNewStateName:Ljava/io/File;

    #@5d
    .line 2108
    iput-object v5, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mSavedState:Landroid/os/ParcelFileDescriptor;

    #@5f
    .line 2109
    iput-object v5, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mBackupData:Landroid/os/ParcelFileDescriptor;

    #@61
    .line 2110
    iput-object v5, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mNewState:Landroid/os/ParcelFileDescriptor;

    #@63
    .line 2112
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@65
    invoke-virtual {v0}, Lcom/android/server/BackupManagerService;->generateToken()I

    #@68
    move-result v4

    #@69
    .line 2117
    .local v4, token:I
    :try_start_69
    const-string v0, "@pm@"

    #@6b
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6e
    move-result v0

    #@6f
    if-eqz v0, :cond_7c

    #@71
    .line 2120
    new-instance v0, Landroid/content/pm/PackageInfo;

    #@73
    invoke-direct {v0}, Landroid/content/pm/PackageInfo;-><init>()V

    #@76
    iput-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mCurrentPackage:Landroid/content/pm/PackageInfo;

    #@78
    .line 2121
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mCurrentPackage:Landroid/content/pm/PackageInfo;

    #@7a
    iput-object p1, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@7c
    .line 2127
    :cond_7c
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mSavedStateName:Ljava/io/File;

    #@7e
    const/high16 v1, 0x1800

    #@80
    invoke-static {v0, v1}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    #@83
    move-result-object v0

    #@84
    iput-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mSavedState:Landroid/os/ParcelFileDescriptor;

    #@86
    .line 2131
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mBackupDataName:Ljava/io/File;

    #@88
    const/high16 v1, 0x3c00

    #@8a
    invoke-static {v0, v1}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    #@8d
    move-result-object v0

    #@8e
    iput-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mBackupData:Landroid/os/ParcelFileDescriptor;

    #@90
    .line 2136
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mNewStateName:Ljava/io/File;

    #@92
    const/high16 v1, 0x3c00

    #@94
    invoke-static {v0, v1}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    #@97
    move-result-object v0

    #@98
    iput-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mNewState:Landroid/os/ParcelFileDescriptor;

    #@9a
    .line 2142
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@9c
    const-string v1, "setting timeout"

    #@9e
    invoke-virtual {v0, v1}, Lcom/android/server/BackupManagerService;->addBackupTrace(Ljava/lang/String;)V

    #@a1
    .line 2143
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@a3
    const-wide/16 v1, 0x7530

    #@a5
    invoke-virtual {v0, v4, v1, v2, p0}, Lcom/android/server/BackupManagerService;->prepareOperationTimeout(IJLcom/android/server/BackupManagerService$BackupRestoreTask;)V

    #@a8
    .line 2144
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@aa
    const-string v1, "calling agent doBackup()"

    #@ac
    invoke-virtual {v0, v1}, Lcom/android/server/BackupManagerService;->addBackupTrace(Ljava/lang/String;)V

    #@af
    .line 2145
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mSavedState:Landroid/os/ParcelFileDescriptor;

    #@b1
    iget-object v2, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mBackupData:Landroid/os/ParcelFileDescriptor;

    #@b3
    iget-object v3, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mNewState:Landroid/os/ParcelFileDescriptor;

    #@b5
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@b7
    iget-object v5, v0, Lcom/android/server/BackupManagerService;->mBackupManagerBinder:Landroid/app/backup/IBackupManager;

    #@b9
    move-object v0, p2

    #@ba
    invoke-interface/range {v0 .. v5}, Landroid/app/IBackupAgent;->doBackup(Landroid/os/ParcelFileDescriptor;Landroid/os/ParcelFileDescriptor;Landroid/os/ParcelFileDescriptor;ILandroid/app/backup/IBackupManager;)V
    :try_end_bd
    .catch Ljava/lang/Exception; {:try_start_69 .. :try_end_bd} :catch_c6

    #@bd
    .line 2159
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@bf
    const-string v1, "invoke success"

    #@c1
    invoke-virtual {v0, v1}, Lcom/android/server/BackupManagerService;->addBackupTrace(Ljava/lang/String;)V

    #@c4
    move v0, v7

    #@c5
    .line 2160
    :goto_c5
    return v0

    #@c6
    .line 2146
    :catch_c6
    move-exception v6

    #@c7
    .line 2147
    .local v6, e:Ljava/lang/Exception;
    const-string v0, "PerformBackupTask"

    #@c9
    new-instance v1, Ljava/lang/StringBuilder;

    #@cb
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@ce
    const-string v2, "Error invoking for backup on "

    #@d0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d3
    move-result-object v1

    #@d4
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d7
    move-result-object v1

    #@d8
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@db
    move-result-object v1

    #@dc
    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@df
    .line 2148
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@e1
    new-instance v1, Ljava/lang/StringBuilder;

    #@e3
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@e6
    const-string v2, "exception: "

    #@e8
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@eb
    move-result-object v1

    #@ec
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@ef
    move-result-object v1

    #@f0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f3
    move-result-object v1

    #@f4
    invoke-virtual {v0, v1}, Lcom/android/server/BackupManagerService;->addBackupTrace(Ljava/lang/String;)V

    #@f7
    .line 2149
    const/16 v0, 0xb07

    #@f9
    const/4 v1, 0x2

    #@fa
    new-array v1, v1, [Ljava/lang/Object;

    #@fc
    aput-object p1, v1, v7

    #@fe
    const/4 v2, 0x1

    #@ff
    invoke-virtual {v6}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@102
    move-result-object v3

    #@103
    aput-object v3, v1, v2

    #@105
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@108
    .line 2151
    invoke-virtual {p0}, Lcom/android/server/BackupManagerService$PerformBackupTask;->agentErrorCleanup()V

    #@10b
    .line 2152
    const/4 v0, 0x3

    #@10c
    goto :goto_c5
.end method

.method invokeNextAgent()V
    .registers 13

    #@0
    .prologue
    const/4 v11, 0x0

    #@1
    const/4 v10, 0x4

    #@2
    const/4 v9, 0x3

    #@3
    const/4 v6, 0x0

    #@4
    .line 1954
    iput v6, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I

    #@6
    .line 1955
    iget-object v5, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@8
    new-instance v7, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v8, "invoke q="

    #@f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v7

    #@13
    iget-object v8, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mQueue:Ljava/util/ArrayList;

    #@15
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    #@18
    move-result v8

    #@19
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v7

    #@1d
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v7

    #@21
    invoke-virtual {v5, v7}, Lcom/android/server/BackupManagerService;->addBackupTrace(Ljava/lang/String;)V

    #@24
    .line 1959
    iget-object v5, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mQueue:Ljava/util/ArrayList;

    #@26
    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    #@29
    move-result v5

    #@2a
    if-eqz v5, :cond_32

    #@2c
    .line 1961
    sget-object v5, Lcom/android/server/BackupManagerService$BackupState;->FINAL:Lcom/android/server/BackupManagerService$BackupState;

    #@2e
    invoke-virtual {p0, v5}, Lcom/android/server/BackupManagerService$PerformBackupTask;->executeNextState(Lcom/android/server/BackupManagerService$BackupState;)V

    #@31
    .line 2047
    :goto_31
    return-void

    #@32
    .line 1966
    :cond_32
    iget-object v5, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mQueue:Ljava/util/ArrayList;

    #@34
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@37
    move-result-object v4

    #@38
    check-cast v4, Lcom/android/server/BackupManagerService$BackupRequest;

    #@3a
    .line 1967
    .local v4, request:Lcom/android/server/BackupManagerService$BackupRequest;
    iget-object v5, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mQueue:Ljava/util/ArrayList;

    #@3c
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@3f
    .line 1969
    const-string v5, "PerformBackupTask"

    #@41
    new-instance v7, Ljava/lang/StringBuilder;

    #@43
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@46
    const-string v8, "starting agent for backup of "

    #@48
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v7

    #@4c
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v7

    #@50
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@53
    move-result-object v7

    #@54
    invoke-static {v5, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@57
    .line 1970
    iget-object v5, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@59
    new-instance v7, Ljava/lang/StringBuilder;

    #@5b
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@5e
    const-string v8, "launch agent for "

    #@60
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v7

    #@64
    iget-object v8, v4, Lcom/android/server/BackupManagerService$BackupRequest;->packageName:Ljava/lang/String;

    #@66
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v7

    #@6a
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6d
    move-result-object v7

    #@6e
    invoke-virtual {v5, v7}, Lcom/android/server/BackupManagerService;->addBackupTrace(Ljava/lang/String;)V

    #@71
    .line 1978
    :try_start_71
    iget-object v5, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@73
    invoke-static {v5}, Lcom/android/server/BackupManagerService;->access$600(Lcom/android/server/BackupManagerService;)Landroid/content/pm/PackageManager;

    #@76
    move-result-object v5

    #@77
    iget-object v7, v4, Lcom/android/server/BackupManagerService$BackupRequest;->packageName:Ljava/lang/String;

    #@79
    const/16 v8, 0x440

    #@7b
    invoke-virtual {v5, v7, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    #@7e
    move-result-object v5

    #@7f
    iput-object v5, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mCurrentPackage:Landroid/content/pm/PackageInfo;

    #@81
    .line 1980
    iget-object v5, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mCurrentPackage:Landroid/content/pm/PackageInfo;

    #@83
    iget-object v5, v5, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@85
    iget-object v5, v5, Landroid/content/pm/ApplicationInfo;->backupAgentName:Ljava/lang/String;

    #@87
    if-nez v5, :cond_de

    #@89
    .line 1984
    const-string v5, "PerformBackupTask"

    #@8b
    new-instance v7, Ljava/lang/StringBuilder;

    #@8d
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@90
    const-string v8, "Package "

    #@92
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v7

    #@96
    iget-object v8, v4, Lcom/android/server/BackupManagerService$BackupRequest;->packageName:Ljava/lang/String;

    #@98
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v7

    #@9c
    const-string v8, " no longer supports backup; skipping"

    #@9e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v7

    #@a2
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a5
    move-result-object v7

    #@a6
    invoke-static {v5, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@a9
    .line 1986
    iget-object v5, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@ab
    const-string v7, "skipping - no agent, completion is noop"

    #@ad
    invoke-virtual {v5, v7}, Lcom/android/server/BackupManagerService;->addBackupTrace(Ljava/lang/String;)V

    #@b0
    .line 1987
    sget-object v5, Lcom/android/server/BackupManagerService$BackupState;->RUNNING_QUEUE:Lcom/android/server/BackupManagerService$BackupState;

    #@b2
    invoke-virtual {p0, v5}, Lcom/android/server/BackupManagerService$PerformBackupTask;->executeNextState(Lcom/android/server/BackupManagerService$BackupState;)V
    :try_end_b5
    .catchall {:try_start_71 .. :try_end_b5} :catchall_199
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_71 .. :try_end_b5} :catch_161

    #@b5
    .line 2017
    iget-object v5, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@b7
    iget-object v5, v5, Lcom/android/server/BackupManagerService;->mWakelock:Landroid/os/PowerManager$WakeLock;

    #@b9
    invoke-virtual {v5, v11}, Landroid/os/PowerManager$WakeLock;->setWorkSource(Landroid/os/WorkSource;)V

    #@bc
    .line 2021
    iget v5, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I

    #@be
    if-eqz v5, :cond_1ef

    #@c0
    .line 2022
    sget-object v3, Lcom/android/server/BackupManagerService$BackupState;->RUNNING_QUEUE:Lcom/android/server/BackupManagerService$BackupState;

    #@c2
    .line 2026
    .local v3, nextState:Lcom/android/server/BackupManagerService$BackupState;
    iget v5, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I

    #@c4
    if-ne v5, v9, :cond_1f4

    #@c6
    .line 2029
    iget-object v5, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@c8
    iget-object v7, v4, Lcom/android/server/BackupManagerService$BackupRequest;->packageName:Ljava/lang/String;

    #@ca
    invoke-static {v5, v7}, Lcom/android/server/BackupManagerService;->access$700(Lcom/android/server/BackupManagerService;Ljava/lang/String;)V

    #@cd
    .line 2030
    iput v6, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I

    #@cf
    .line 2031
    iget-object v5, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mQueue:Ljava/util/ArrayList;

    #@d1
    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    #@d4
    move-result v5

    #@d5
    if-eqz v5, :cond_d9

    #@d7
    sget-object v3, Lcom/android/server/BackupManagerService$BackupState;->FINAL:Lcom/android/server/BackupManagerService$BackupState;

    #@d9
    .line 2042
    :cond_d9
    :goto_d9
    invoke-virtual {p0, v3}, Lcom/android/server/BackupManagerService$PerformBackupTask;->executeNextState(Lcom/android/server/BackupManagerService$BackupState;)V

    #@dc
    goto/16 :goto_31

    #@de
    .line 1991
    .end local v3           #nextState:Lcom/android/server/BackupManagerService$BackupState;
    :cond_de
    const/4 v0, 0x0

    #@df
    .line 1993
    .local v0, agent:Landroid/app/IBackupAgent;
    :try_start_df
    iget-object v5, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@e1
    iget-object v5, v5, Lcom/android/server/BackupManagerService;->mWakelock:Landroid/os/PowerManager$WakeLock;

    #@e3
    new-instance v7, Landroid/os/WorkSource;

    #@e5
    iget-object v8, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mCurrentPackage:Landroid/content/pm/PackageInfo;

    #@e7
    iget-object v8, v8, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@e9
    iget v8, v8, Landroid/content/pm/ApplicationInfo;->uid:I

    #@eb
    invoke-direct {v7, v8}, Landroid/os/WorkSource;-><init>(I)V

    #@ee
    invoke-virtual {v5, v7}, Landroid/os/PowerManager$WakeLock;->setWorkSource(Landroid/os/WorkSource;)V

    #@f1
    .line 1994
    iget-object v5, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@f3
    iget-object v7, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mCurrentPackage:Landroid/content/pm/PackageInfo;

    #@f5
    iget-object v7, v7, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@f7
    const/4 v8, 0x0

    #@f8
    invoke-virtual {v5, v7, v8}, Lcom/android/server/BackupManagerService;->bindToAgentSynchronous(Landroid/content/pm/ApplicationInfo;I)Landroid/app/IBackupAgent;

    #@fb
    move-result-object v0

    #@fc
    .line 1996
    iget-object v7, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@fe
    new-instance v5, Ljava/lang/StringBuilder;

    #@100
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@103
    const-string v8, "agent bound; a? = "

    #@105
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@108
    move-result-object v8

    #@109
    if-eqz v0, :cond_148

    #@10b
    const/4 v5, 0x1

    #@10c
    :goto_10c
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@10f
    move-result-object v5

    #@110
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@113
    move-result-object v5

    #@114
    invoke-virtual {v7, v5}, Lcom/android/server/BackupManagerService;->addBackupTrace(Ljava/lang/String;)V

    #@117
    .line 1997
    if-eqz v0, :cond_14a

    #@119
    .line 1998
    iget-object v5, v4, Lcom/android/server/BackupManagerService$BackupRequest;->packageName:Ljava/lang/String;

    #@11b
    iget-object v7, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mTransport:Lcom/android/internal/backup/IBackupTransport;

    #@11d
    invoke-virtual {p0, v5, v0, v7}, Lcom/android/server/BackupManagerService$PerformBackupTask;->invokeAgentForBackup(Ljava/lang/String;Landroid/app/IBackupAgent;Lcom/android/internal/backup/IBackupTransport;)I

    #@120
    move-result v5

    #@121
    iput v5, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I
    :try_end_123
    .catchall {:try_start_df .. :try_end_123} :catchall_199
    .catch Ljava/lang/SecurityException; {:try_start_df .. :try_end_123} :catch_14e
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_df .. :try_end_123} :catch_161

    #@123
    .line 2017
    :goto_123
    iget-object v5, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@125
    iget-object v5, v5, Lcom/android/server/BackupManagerService;->mWakelock:Landroid/os/PowerManager$WakeLock;

    #@127
    invoke-virtual {v5, v11}, Landroid/os/PowerManager$WakeLock;->setWorkSource(Landroid/os/WorkSource;)V

    #@12a
    .line 2021
    iget v5, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I

    #@12c
    if-eqz v5, :cond_203

    #@12e
    .line 2022
    sget-object v3, Lcom/android/server/BackupManagerService$BackupState;->RUNNING_QUEUE:Lcom/android/server/BackupManagerService$BackupState;

    #@130
    .line 2026
    .restart local v3       #nextState:Lcom/android/server/BackupManagerService$BackupState;
    iget v5, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I

    #@132
    if-ne v5, v9, :cond_208

    #@134
    .line 2029
    iget-object v5, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@136
    iget-object v7, v4, Lcom/android/server/BackupManagerService$BackupRequest;->packageName:Ljava/lang/String;

    #@138
    invoke-static {v5, v7}, Lcom/android/server/BackupManagerService;->access$700(Lcom/android/server/BackupManagerService;Ljava/lang/String;)V

    #@13b
    .line 2030
    iput v6, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I

    #@13d
    .line 2031
    iget-object v5, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mQueue:Ljava/util/ArrayList;

    #@13f
    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    #@142
    move-result v5

    #@143
    if-eqz v5, :cond_d9

    #@145
    sget-object v3, Lcom/android/server/BackupManagerService$BackupState;->FINAL:Lcom/android/server/BackupManagerService$BackupState;

    #@147
    goto :goto_d9

    #@148
    .end local v3           #nextState:Lcom/android/server/BackupManagerService$BackupState;
    :cond_148
    move v5, v6

    #@149
    .line 1996
    goto :goto_10c

    #@14a
    .line 2004
    :cond_14a
    const/4 v5, 0x3

    #@14b
    :try_start_14b
    iput v5, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I
    :try_end_14d
    .catchall {:try_start_14b .. :try_end_14d} :catchall_199
    .catch Ljava/lang/SecurityException; {:try_start_14b .. :try_end_14d} :catch_14e
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_14b .. :try_end_14d} :catch_161

    #@14d
    goto :goto_123

    #@14e
    .line 2006
    :catch_14e
    move-exception v2

    #@14f
    .line 2008
    .local v2, ex:Ljava/lang/SecurityException;
    :try_start_14f
    const-string v5, "PerformBackupTask"

    #@151
    const-string v7, "error in bind/backup"

    #@153
    invoke-static {v5, v7, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@156
    .line 2009
    const/4 v5, 0x3

    #@157
    iput v5, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I

    #@159
    .line 2010
    iget-object v5, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@15b
    const-string v7, "agent SE"

    #@15d
    invoke-virtual {v5, v7}, Lcom/android/server/BackupManagerService;->addBackupTrace(Ljava/lang/String;)V
    :try_end_160
    .catchall {:try_start_14f .. :try_end_160} :catchall_199
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_14f .. :try_end_160} :catch_161

    #@160
    goto :goto_123

    #@161
    .line 2012
    .end local v0           #agent:Landroid/app/IBackupAgent;
    .end local v2           #ex:Ljava/lang/SecurityException;
    :catch_161
    move-exception v1

    #@162
    .line 2013
    .local v1, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_162
    const-string v5, "PerformBackupTask"

    #@164
    const-string v7, "Package does not exist; skipping"

    #@166
    invoke-static {v5, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@169
    .line 2014
    iget-object v5, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@16b
    const-string v7, "no such package"

    #@16d
    invoke-virtual {v5, v7}, Lcom/android/server/BackupManagerService;->addBackupTrace(Ljava/lang/String;)V

    #@170
    .line 2015
    const/4 v5, 0x4

    #@171
    iput v5, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I
    :try_end_173
    .catchall {:try_start_162 .. :try_end_173} :catchall_199

    #@173
    .line 2017
    iget-object v5, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@175
    iget-object v5, v5, Lcom/android/server/BackupManagerService;->mWakelock:Landroid/os/PowerManager$WakeLock;

    #@177
    invoke-virtual {v5, v11}, Landroid/os/PowerManager$WakeLock;->setWorkSource(Landroid/os/WorkSource;)V

    #@17a
    .line 2021
    iget v5, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I

    #@17c
    if-eqz v5, :cond_1d7

    #@17e
    .line 2022
    sget-object v3, Lcom/android/server/BackupManagerService$BackupState;->RUNNING_QUEUE:Lcom/android/server/BackupManagerService$BackupState;

    #@180
    .line 2026
    .restart local v3       #nextState:Lcom/android/server/BackupManagerService$BackupState;
    iget v5, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I

    #@182
    if-ne v5, v9, :cond_1e0

    #@184
    .line 2029
    iget-object v5, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@186
    iget-object v7, v4, Lcom/android/server/BackupManagerService$BackupRequest;->packageName:Ljava/lang/String;

    #@188
    invoke-static {v5, v7}, Lcom/android/server/BackupManagerService;->access$700(Lcom/android/server/BackupManagerService;Ljava/lang/String;)V

    #@18b
    .line 2030
    iput v6, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I

    #@18d
    .line 2031
    iget-object v5, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mQueue:Ljava/util/ArrayList;

    #@18f
    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    #@192
    move-result v5

    #@193
    if-eqz v5, :cond_d9

    #@195
    sget-object v3, Lcom/android/server/BackupManagerService$BackupState;->FINAL:Lcom/android/server/BackupManagerService$BackupState;

    #@197
    goto/16 :goto_d9

    #@199
    .line 2017
    .end local v1           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v3           #nextState:Lcom/android/server/BackupManagerService$BackupState;
    :catchall_199
    move-exception v5

    #@19a
    iget-object v7, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@19c
    iget-object v7, v7, Lcom/android/server/BackupManagerService;->mWakelock:Landroid/os/PowerManager$WakeLock;

    #@19e
    invoke-virtual {v7, v11}, Landroid/os/PowerManager$WakeLock;->setWorkSource(Landroid/os/WorkSource;)V

    #@1a1
    .line 2021
    iget v7, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I

    #@1a3
    if-eqz v7, :cond_1c2

    #@1a5
    .line 2022
    sget-object v3, Lcom/android/server/BackupManagerService$BackupState;->RUNNING_QUEUE:Lcom/android/server/BackupManagerService$BackupState;

    #@1a7
    .line 2026
    .restart local v3       #nextState:Lcom/android/server/BackupManagerService$BackupState;
    iget v7, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I

    #@1a9
    if-ne v7, v9, :cond_1ca

    #@1ab
    .line 2029
    iget-object v7, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@1ad
    iget-object v8, v4, Lcom/android/server/BackupManagerService$BackupRequest;->packageName:Ljava/lang/String;

    #@1af
    invoke-static {v7, v8}, Lcom/android/server/BackupManagerService;->access$700(Lcom/android/server/BackupManagerService;Ljava/lang/String;)V

    #@1b2
    .line 2030
    iput v6, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I

    #@1b4
    .line 2031
    iget-object v6, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mQueue:Ljava/util/ArrayList;

    #@1b6
    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    #@1b9
    move-result v6

    #@1ba
    if-eqz v6, :cond_1be

    #@1bc
    sget-object v3, Lcom/android/server/BackupManagerService$BackupState;->FINAL:Lcom/android/server/BackupManagerService$BackupState;

    #@1be
    .line 2042
    :cond_1be
    :goto_1be
    invoke-virtual {p0, v3}, Lcom/android/server/BackupManagerService$PerformBackupTask;->executeNextState(Lcom/android/server/BackupManagerService$BackupState;)V

    #@1c1
    .line 2017
    .end local v3           #nextState:Lcom/android/server/BackupManagerService$BackupState;
    :goto_1c1
    throw v5

    #@1c2
    .line 2044
    :cond_1c2
    iget-object v6, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@1c4
    const-string v7, "expecting completion/timeout callback"

    #@1c6
    invoke-virtual {v6, v7}, Lcom/android/server/BackupManagerService;->addBackupTrace(Ljava/lang/String;)V

    #@1c9
    goto :goto_1c1

    #@1ca
    .line 2032
    .restart local v3       #nextState:Lcom/android/server/BackupManagerService$BackupState;
    :cond_1ca
    iget v7, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I

    #@1cc
    if-ne v7, v10, :cond_1d1

    #@1ce
    .line 2035
    iput v6, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I

    #@1d0
    goto :goto_1be

    #@1d1
    .line 2038
    :cond_1d1
    invoke-virtual {p0}, Lcom/android/server/BackupManagerService$PerformBackupTask;->revertAndEndBackup()V

    #@1d4
    .line 2039
    sget-object v3, Lcom/android/server/BackupManagerService$BackupState;->FINAL:Lcom/android/server/BackupManagerService$BackupState;

    #@1d6
    goto :goto_1be

    #@1d7
    .line 2044
    .end local v3           #nextState:Lcom/android/server/BackupManagerService$BackupState;
    .restart local v1       #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_1d7
    iget-object v5, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@1d9
    const-string v6, "expecting completion/timeout callback"

    #@1db
    .end local v1           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    :goto_1db
    invoke-virtual {v5, v6}, Lcom/android/server/BackupManagerService;->addBackupTrace(Ljava/lang/String;)V

    #@1de
    goto/16 :goto_31

    #@1e0
    .line 2032
    .restart local v1       #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v3       #nextState:Lcom/android/server/BackupManagerService$BackupState;
    :cond_1e0
    iget v5, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I

    #@1e2
    if-ne v5, v10, :cond_1e8

    #@1e4
    .line 2035
    iput v6, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I

    #@1e6
    goto/16 :goto_d9

    #@1e8
    .line 2038
    :cond_1e8
    invoke-virtual {p0}, Lcom/android/server/BackupManagerService$PerformBackupTask;->revertAndEndBackup()V

    #@1eb
    .line 2039
    sget-object v3, Lcom/android/server/BackupManagerService$BackupState;->FINAL:Lcom/android/server/BackupManagerService$BackupState;

    #@1ed
    goto/16 :goto_d9

    #@1ef
    .line 2044
    .end local v1           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v3           #nextState:Lcom/android/server/BackupManagerService$BackupState;
    :cond_1ef
    iget-object v5, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@1f1
    const-string v6, "expecting completion/timeout callback"

    #@1f3
    goto :goto_1db

    #@1f4
    .line 2032
    .restart local v3       #nextState:Lcom/android/server/BackupManagerService$BackupState;
    :cond_1f4
    iget v5, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I

    #@1f6
    if-ne v5, v10, :cond_1fc

    #@1f8
    .line 2035
    iput v6, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I

    #@1fa
    goto/16 :goto_d9

    #@1fc
    .line 2038
    :cond_1fc
    invoke-virtual {p0}, Lcom/android/server/BackupManagerService$PerformBackupTask;->revertAndEndBackup()V

    #@1ff
    .line 2039
    sget-object v3, Lcom/android/server/BackupManagerService$BackupState;->FINAL:Lcom/android/server/BackupManagerService$BackupState;

    #@201
    goto/16 :goto_d9

    #@203
    .line 2044
    .end local v3           #nextState:Lcom/android/server/BackupManagerService$BackupState;
    .restart local v0       #agent:Landroid/app/IBackupAgent;
    :cond_203
    iget-object v5, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@205
    const-string v6, "expecting completion/timeout callback"

    #@207
    goto :goto_1db

    #@208
    .line 2032
    .restart local v3       #nextState:Lcom/android/server/BackupManagerService$BackupState;
    :cond_208
    iget v5, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I

    #@20a
    if-ne v5, v10, :cond_210

    #@20c
    .line 2035
    iput v6, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I

    #@20e
    goto/16 :goto_d9

    #@210
    .line 2038
    :cond_210
    invoke-virtual {p0}, Lcom/android/server/BackupManagerService$PerformBackupTask;->revertAndEndBackup()V

    #@213
    .line 2039
    sget-object v3, Lcom/android/server/BackupManagerService$BackupState;->FINAL:Lcom/android/server/BackupManagerService$BackupState;

    #@215
    goto/16 :goto_d9
.end method

.method public operationComplete()V
    .registers 9

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 2169
    iget-object v4, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@3
    iget-object v4, v4, Lcom/android/server/BackupManagerService;->mBackupHandler:Lcom/android/server/BackupManagerService$BackupHandler;

    #@5
    const/4 v5, 0x7

    #@6
    invoke-virtual {v4, v5}, Lcom/android/server/BackupManagerService$BackupHandler;->removeMessages(I)V

    #@9
    .line 2170
    invoke-virtual {p0}, Lcom/android/server/BackupManagerService$PerformBackupTask;->clearAgentState()V

    #@c
    .line 2171
    iget-object v4, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@e
    const-string v5, "operation complete"

    #@10
    invoke-virtual {v4, v5}, Lcom/android/server/BackupManagerService;->addBackupTrace(Ljava/lang/String;)V

    #@13
    .line 2173
    const/4 v0, 0x0

    #@14
    .line 2174
    .local v0, backupData:Landroid/os/ParcelFileDescriptor;
    iput v6, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I

    #@16
    .line 2176
    :try_start_16
    iget-object v4, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mBackupDataName:Ljava/io/File;

    #@18
    invoke-virtual {v4}, Ljava/io/File;->length()J

    #@1b
    move-result-wide v4

    #@1c
    long-to-int v3, v4

    #@1d
    .line 2177
    .local v3, size:I
    if-lez v3, :cond_c4

    #@1f
    .line 2178
    iget v4, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I

    #@21
    if-nez v4, :cond_3c

    #@23
    .line 2179
    iget-object v4, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mBackupDataName:Ljava/io/File;

    #@25
    const/high16 v5, 0x1000

    #@27
    invoke-static {v4, v5}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    #@2a
    move-result-object v0

    #@2b
    .line 2181
    iget-object v4, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@2d
    const-string v5, "sending data to transport"

    #@2f
    invoke-virtual {v4, v5}, Lcom/android/server/BackupManagerService;->addBackupTrace(Ljava/lang/String;)V

    #@32
    .line 2182
    iget-object v4, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mTransport:Lcom/android/internal/backup/IBackupTransport;

    #@34
    iget-object v5, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mCurrentPackage:Landroid/content/pm/PackageInfo;

    #@36
    invoke-interface {v4, v5, v0}, Lcom/android/internal/backup/IBackupTransport;->performBackup(Landroid/content/pm/PackageInfo;Landroid/os/ParcelFileDescriptor;)I

    #@39
    move-result v4

    #@3a
    iput v4, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I

    #@3c
    .line 2190
    :cond_3c
    iget-object v4, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@3e
    new-instance v5, Ljava/lang/StringBuilder;

    #@40
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@43
    const-string v6, "data delivered: "

    #@45
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v5

    #@49
    iget v6, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I

    #@4b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v5

    #@4f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v5

    #@53
    invoke-virtual {v4, v5}, Lcom/android/server/BackupManagerService;->addBackupTrace(Ljava/lang/String;)V

    #@56
    .line 2191
    iget v4, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I

    #@58
    if-nez v4, :cond_83

    #@5a
    .line 2192
    iget-object v4, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@5c
    const-string v5, "finishing op on transport"

    #@5e
    invoke-virtual {v4, v5}, Lcom/android/server/BackupManagerService;->addBackupTrace(Ljava/lang/String;)V

    #@61
    .line 2193
    iget-object v4, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mTransport:Lcom/android/internal/backup/IBackupTransport;

    #@63
    invoke-interface {v4}, Lcom/android/internal/backup/IBackupTransport;->finishBackup()I

    #@66
    move-result v4

    #@67
    iput v4, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I

    #@69
    .line 2194
    iget-object v4, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@6b
    new-instance v5, Ljava/lang/StringBuilder;

    #@6d
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@70
    const-string v6, "finished: "

    #@72
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v5

    #@76
    iget v6, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I

    #@78
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v5

    #@7c
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7f
    move-result-object v5

    #@80
    invoke-virtual {v4, v5}, Lcom/android/server/BackupManagerService;->addBackupTrace(Ljava/lang/String;)V

    #@83
    .line 2204
    :cond_83
    :goto_83
    iget v4, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I

    #@85
    if-nez v4, :cond_fd

    #@87
    .line 2205
    iget-object v4, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mBackupDataName:Ljava/io/File;

    #@89
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    #@8c
    .line 2206
    iget-object v4, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mNewStateName:Ljava/io/File;

    #@8e
    iget-object v5, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mSavedStateName:Ljava/io/File;

    #@90
    invoke-virtual {v4, v5}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    #@93
    .line 2207
    const/16 v4, 0xb08

    #@95
    const/4 v5, 0x2

    #@96
    new-array v5, v5, [Ljava/lang/Object;

    #@98
    const/4 v6, 0x0

    #@99
    iget-object v7, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mCurrentPackage:Landroid/content/pm/PackageInfo;

    #@9b
    iget-object v7, v7, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@9d
    aput-object v7, v5, v6

    #@9f
    const/4 v6, 0x1

    #@a0
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@a3
    move-result-object v7

    #@a4
    aput-object v7, v5, v6

    #@a6
    invoke-static {v4, v5}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@a9
    .line 2209
    iget-object v4, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@ab
    iget-object v5, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mCurrentPackage:Landroid/content/pm/PackageInfo;

    #@ad
    iget-object v5, v5, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@af
    invoke-virtual {v4, v5}, Lcom/android/server/BackupManagerService;->logBackupComplete(Ljava/lang/String;)V
    :try_end_b2
    .catchall {:try_start_16 .. :try_end_b2} :catchall_107
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_b2} :catch_cc

    #@b2
    .line 2220
    :goto_b2
    if-eqz v0, :cond_b7

    #@b4
    :try_start_b4
    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_b7
    .catch Ljava/io/IOException; {:try_start_b4 .. :try_end_b7} :catch_fb

    #@b7
    .line 2226
    .end local v3           #size:I
    :cond_b7
    :goto_b7
    iget v4, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I

    #@b9
    if-eqz v4, :cond_10e

    #@bb
    .line 2227
    invoke-virtual {p0}, Lcom/android/server/BackupManagerService$PerformBackupTask;->revertAndEndBackup()V

    #@be
    .line 2228
    sget-object v2, Lcom/android/server/BackupManagerService$BackupState;->FINAL:Lcom/android/server/BackupManagerService$BackupState;

    #@c0
    .line 2234
    .local v2, nextState:Lcom/android/server/BackupManagerService$BackupState;
    :goto_c0
    invoke-virtual {p0, v2}, Lcom/android/server/BackupManagerService$PerformBackupTask;->executeNextState(Lcom/android/server/BackupManagerService$BackupState;)V

    #@c3
    .line 2235
    return-void

    #@c4
    .line 2198
    .end local v2           #nextState:Lcom/android/server/BackupManagerService$BackupState;
    .restart local v3       #size:I
    :cond_c4
    :try_start_c4
    iget-object v4, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@c6
    const-string v5, "no data to send"

    #@c8
    invoke-virtual {v4, v5}, Lcom/android/server/BackupManagerService;->addBackupTrace(Ljava/lang/String;)V
    :try_end_cb
    .catchall {:try_start_c4 .. :try_end_cb} :catchall_107
    .catch Ljava/lang/Exception; {:try_start_c4 .. :try_end_cb} :catch_cc

    #@cb
    goto :goto_83

    #@cc
    .line 2214
    .end local v3           #size:I
    :catch_cc
    move-exception v1

    #@cd
    .line 2215
    .local v1, e:Ljava/lang/Exception;
    :try_start_cd
    const-string v4, "PerformBackupTask"

    #@cf
    new-instance v5, Ljava/lang/StringBuilder;

    #@d1
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@d4
    const-string v6, "Transport error backing up "

    #@d6
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d9
    move-result-object v5

    #@da
    iget-object v6, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mCurrentPackage:Landroid/content/pm/PackageInfo;

    #@dc
    iget-object v6, v6, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@de
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e1
    move-result-object v5

    #@e2
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e5
    move-result-object v5

    #@e6
    invoke-static {v4, v5, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@e9
    .line 2216
    const/16 v4, 0xb06

    #@eb
    iget-object v5, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mCurrentPackage:Landroid/content/pm/PackageInfo;

    #@ed
    iget-object v5, v5, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@ef
    invoke-static {v4, v5}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    #@f2
    .line 2218
    const/4 v4, 0x1

    #@f3
    iput v4, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mStatus:I
    :try_end_f5
    .catchall {:try_start_cd .. :try_end_f5} :catchall_107

    #@f5
    .line 2220
    if-eqz v0, :cond_b7

    #@f7
    :try_start_f7
    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_fa
    .catch Ljava/io/IOException; {:try_start_f7 .. :try_end_fa} :catch_fb

    #@fa
    goto :goto_b7

    #@fb
    .end local v1           #e:Ljava/lang/Exception;
    :catch_fb
    move-exception v4

    #@fc
    goto :goto_b7

    #@fd
    .line 2211
    .restart local v3       #size:I
    :cond_fd
    const/16 v4, 0xb06

    #@ff
    :try_start_ff
    iget-object v5, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mCurrentPackage:Landroid/content/pm/PackageInfo;

    #@101
    iget-object v5, v5, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@103
    invoke-static {v4, v5}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I
    :try_end_106
    .catchall {:try_start_ff .. :try_end_106} :catchall_107
    .catch Ljava/lang/Exception; {:try_start_ff .. :try_end_106} :catch_cc

    #@106
    goto :goto_b2

    #@107
    .line 2220
    .end local v3           #size:I
    :catchall_107
    move-exception v4

    #@108
    if-eqz v0, :cond_10d

    #@10a
    :try_start_10a
    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_10d
    .catch Ljava/io/IOException; {:try_start_10a .. :try_end_10d} :catch_11c

    #@10d
    :cond_10d
    :goto_10d
    throw v4

    #@10e
    .line 2231
    :cond_10e
    iget-object v4, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mQueue:Ljava/util/ArrayList;

    #@110
    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    #@113
    move-result v4

    #@114
    if-eqz v4, :cond_119

    #@116
    sget-object v2, Lcom/android/server/BackupManagerService$BackupState;->FINAL:Lcom/android/server/BackupManagerService$BackupState;

    #@118
    .restart local v2       #nextState:Lcom/android/server/BackupManagerService$BackupState;
    :goto_118
    goto :goto_c0

    #@119
    .end local v2           #nextState:Lcom/android/server/BackupManagerService$BackupState;
    :cond_119
    sget-object v2, Lcom/android/server/BackupManagerService$BackupState;->RUNNING_QUEUE:Lcom/android/server/BackupManagerService$BackupState;

    #@11b
    goto :goto_118

    #@11c
    .line 2220
    :catch_11c
    move-exception v5

    #@11d
    goto :goto_10d
.end method

.method restartBackupAlarm()V
    .registers 5

    #@0
    .prologue
    .line 2290
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@2
    const-string v1, "setting backup trigger"

    #@4
    invoke-virtual {v0, v1}, Lcom/android/server/BackupManagerService;->addBackupTrace(Ljava/lang/String;)V

    #@7
    .line 2291
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@9
    iget-object v1, v0, Lcom/android/server/BackupManagerService;->mQueueLock:Ljava/lang/Object;

    #@b
    monitor-enter v1

    #@c
    .line 2293
    :try_start_c
    iget-object v0, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@e
    iget-object v2, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mTransport:Lcom/android/internal/backup/IBackupTransport;

    #@10
    invoke-interface {v2}, Lcom/android/internal/backup/IBackupTransport;->requestBackupTime()J

    #@13
    move-result-wide v2

    #@14
    invoke-static {v0, v2, v3}, Lcom/android/server/BackupManagerService;->access$000(Lcom/android/server/BackupManagerService;J)V
    :try_end_17
    .catchall {:try_start_c .. :try_end_17} :catchall_19
    .catch Landroid/os/RemoteException; {:try_start_c .. :try_end_17} :catch_1c

    #@17
    .line 2295
    :goto_17
    :try_start_17
    monitor-exit v1

    #@18
    .line 2296
    return-void

    #@19
    .line 2295
    :catchall_19
    move-exception v0

    #@1a
    monitor-exit v1
    :try_end_1b
    .catchall {:try_start_17 .. :try_end_1b} :catchall_19

    #@1b
    throw v0

    #@1c
    .line 2294
    :catch_1c
    move-exception v0

    #@1d
    goto :goto_17
.end method

.method revertAndEndBackup()V
    .registers 5

    #@0
    .prologue
    .line 2253
    iget-object v2, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@2
    const-string v3, "transport error; reverting"

    #@4
    invoke-virtual {v2, v3}, Lcom/android/server/BackupManagerService;->addBackupTrace(Ljava/lang/String;)V

    #@7
    .line 2254
    iget-object v2, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->mOriginalQueue:Ljava/util/ArrayList;

    #@9
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@c
    move-result-object v0

    #@d
    .local v0, i$:Ljava/util/Iterator;
    :goto_d
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@10
    move-result v2

    #@11
    if-eqz v2, :cond_21

    #@13
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@16
    move-result-object v1

    #@17
    check-cast v1, Lcom/android/server/BackupManagerService$BackupRequest;

    #@19
    .line 2255
    .local v1, request:Lcom/android/server/BackupManagerService$BackupRequest;
    iget-object v2, p0, Lcom/android/server/BackupManagerService$PerformBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@1b
    iget-object v3, v1, Lcom/android/server/BackupManagerService$BackupRequest;->packageName:Ljava/lang/String;

    #@1d
    invoke-static {v2, v3}, Lcom/android/server/BackupManagerService;->access$700(Lcom/android/server/BackupManagerService;Ljava/lang/String;)V

    #@20
    goto :goto_d

    #@21
    .line 2259
    .end local v1           #request:Lcom/android/server/BackupManagerService$BackupRequest;
    :cond_21
    invoke-virtual {p0}, Lcom/android/server/BackupManagerService$PerformBackupTask;->restartBackupAlarm()V

    #@24
    .line 2260
    return-void
.end method
