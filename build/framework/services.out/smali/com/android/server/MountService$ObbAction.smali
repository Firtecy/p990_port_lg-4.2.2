.class abstract Lcom/android/server/MountService$ObbAction;
.super Ljava/lang/Object;
.source "MountService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/MountService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x400
    name = "ObbAction"
.end annotation


# static fields
.field private static final MAX_RETRIES:I = 0x3


# instance fields
.field mObbState:Lcom/android/server/MountService$ObbState;

.field private mRetries:I

.field final synthetic this$0:Lcom/android/server/MountService;


# direct methods
.method constructor <init>(Lcom/android/server/MountService;Lcom/android/server/MountService$ObbState;)V
    .registers 3
    .parameter
    .parameter "obbState"

    #@0
    .prologue
    .line 2401
    iput-object p1, p0, Lcom/android/server/MountService$ObbAction;->this$0:Lcom/android/server/MountService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 2402
    iput-object p2, p0, Lcom/android/server/MountService$ObbAction;->mObbState:Lcom/android/server/MountService$ObbState;

    #@7
    .line 2403
    return-void
.end method


# virtual methods
.method public execute(Lcom/android/server/MountService$ObbActionHandler;)V
    .registers 6
    .parameter "handler"

    #@0
    .prologue
    const/4 v3, 0x3

    #@1
    .line 2409
    :try_start_1
    iget v1, p0, Lcom/android/server/MountService$ObbAction;->mRetries:I

    #@3
    add-int/lit8 v1, v1, 0x1

    #@5
    iput v1, p0, Lcom/android/server/MountService$ObbAction;->mRetries:I

    #@7
    .line 2410
    iget v1, p0, Lcom/android/server/MountService$ObbAction;->mRetries:I

    #@9
    if-le v1, v3, :cond_20

    #@b
    .line 2411
    const-string v1, "MountService"

    #@d
    const-string v2, "Failed to invoke remote methods on default container service. Giving up"

    #@f
    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@12
    .line 2412
    iget-object v1, p0, Lcom/android/server/MountService$ObbAction;->this$0:Lcom/android/server/MountService;

    #@14
    invoke-static {v1}, Lcom/android/server/MountService;->access$000(Lcom/android/server/MountService;)Lcom/android/server/MountService$ObbActionHandler;

    #@17
    move-result-object v1

    #@18
    const/4 v2, 0x3

    #@19
    invoke-virtual {v1, v2}, Lcom/android/server/MountService$ObbActionHandler;->sendEmptyMessage(I)Z

    #@1c
    .line 2413
    invoke-virtual {p0}, Lcom/android/server/MountService$ObbAction;->handleError()V

    #@1f
    .line 2431
    :goto_1f
    return-void

    #@20
    .line 2416
    :cond_20
    invoke-virtual {p0}, Lcom/android/server/MountService$ObbAction;->handleExecute()V

    #@23
    .line 2419
    iget-object v1, p0, Lcom/android/server/MountService$ObbAction;->this$0:Lcom/android/server/MountService;

    #@25
    invoke-static {v1}, Lcom/android/server/MountService;->access$000(Lcom/android/server/MountService;)Lcom/android/server/MountService$ObbActionHandler;

    #@28
    move-result-object v1

    #@29
    const/4 v2, 0x3

    #@2a
    invoke-virtual {v1, v2}, Lcom/android/server/MountService$ObbActionHandler;->sendEmptyMessage(I)Z
    :try_end_2d
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_2d} :catch_2e
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_2d} :catch_3a

    #@2d
    goto :goto_1f

    #@2e
    .line 2421
    :catch_2e
    move-exception v0

    #@2f
    .line 2424
    .local v0, e:Landroid/os/RemoteException;
    iget-object v1, p0, Lcom/android/server/MountService$ObbAction;->this$0:Lcom/android/server/MountService;

    #@31
    invoke-static {v1}, Lcom/android/server/MountService;->access$000(Lcom/android/server/MountService;)Lcom/android/server/MountService$ObbActionHandler;

    #@34
    move-result-object v1

    #@35
    const/4 v2, 0x4

    #@36
    invoke-virtual {v1, v2}, Lcom/android/server/MountService$ObbActionHandler;->sendEmptyMessage(I)Z

    #@39
    goto :goto_1f

    #@3a
    .line 2425
    .end local v0           #e:Landroid/os/RemoteException;
    :catch_3a
    move-exception v0

    #@3b
    .line 2428
    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {p0}, Lcom/android/server/MountService$ObbAction;->handleError()V

    #@3e
    .line 2429
    iget-object v1, p0, Lcom/android/server/MountService$ObbAction;->this$0:Lcom/android/server/MountService;

    #@40
    invoke-static {v1}, Lcom/android/server/MountService;->access$000(Lcom/android/server/MountService;)Lcom/android/server/MountService$ObbActionHandler;

    #@43
    move-result-object v1

    #@44
    invoke-virtual {v1, v3}, Lcom/android/server/MountService$ObbActionHandler;->sendEmptyMessage(I)Z

    #@47
    goto :goto_1f
.end method

.method protected getObbInfo()Landroid/content/res/ObbInfo;
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 2439
    :try_start_0
    iget-object v2, p0, Lcom/android/server/MountService$ObbAction;->this$0:Lcom/android/server/MountService;

    #@2
    invoke-static {v2}, Lcom/android/server/MountService;->access$1900(Lcom/android/server/MountService;)Lcom/android/internal/app/IMediaContainerService;

    #@5
    move-result-object v2

    #@6
    iget-object v3, p0, Lcom/android/server/MountService$ObbAction;->mObbState:Lcom/android/server/MountService$ObbState;

    #@8
    iget-object v3, v3, Lcom/android/server/MountService$ObbState;->ownerPath:Ljava/lang/String;

    #@a
    invoke-interface {v2, v3}, Lcom/android/internal/app/IMediaContainerService;->getObbInfo(Ljava/lang/String;)Landroid/content/res/ObbInfo;
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_d} :catch_2d

    #@d
    move-result-object v1

    #@e
    .line 2445
    .local v1, obbInfo:Landroid/content/res/ObbInfo;
    :goto_e
    if-nez v1, :cond_4c

    #@10
    .line 2446
    new-instance v2, Ljava/io/IOException;

    #@12
    new-instance v3, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v4, "Couldn\'t read OBB file: "

    #@19
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    iget-object v4, p0, Lcom/android/server/MountService$ObbAction;->mObbState:Lcom/android/server/MountService$ObbState;

    #@1f
    iget-object v4, v4, Lcom/android/server/MountService$ObbState;->ownerPath:Ljava/lang/String;

    #@21
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v3

    #@25
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v3

    #@29
    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@2c
    throw v2

    #@2d
    .line 2440
    .end local v1           #obbInfo:Landroid/content/res/ObbInfo;
    :catch_2d
    move-exception v0

    #@2e
    .line 2441
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "MountService"

    #@30
    new-instance v3, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    const-string v4, "Couldn\'t call DefaultContainerService to fetch OBB info for "

    #@37
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v3

    #@3b
    iget-object v4, p0, Lcom/android/server/MountService$ObbAction;->mObbState:Lcom/android/server/MountService$ObbState;

    #@3d
    iget-object v4, v4, Lcom/android/server/MountService$ObbState;->ownerPath:Ljava/lang/String;

    #@3f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v3

    #@43
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v3

    #@47
    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4a
    .line 2443
    const/4 v1, 0x0

    #@4b
    .restart local v1       #obbInfo:Landroid/content/res/ObbInfo;
    goto :goto_e

    #@4c
    .line 2448
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_4c
    return-object v1
.end method

.method abstract handleError()V
.end method

.method abstract handleExecute()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method protected sendNewStatusOrIgnore(I)V
    .registers 6
    .parameter "status"

    #@0
    .prologue
    .line 2452
    iget-object v1, p0, Lcom/android/server/MountService$ObbAction;->mObbState:Lcom/android/server/MountService$ObbState;

    #@2
    if-eqz v1, :cond_a

    #@4
    iget-object v1, p0, Lcom/android/server/MountService$ObbAction;->mObbState:Lcom/android/server/MountService$ObbState;

    #@6
    iget-object v1, v1, Lcom/android/server/MountService$ObbState;->token:Landroid/os/storage/IObbActionListener;

    #@8
    if-nez v1, :cond_b

    #@a
    .line 2461
    :cond_a
    :goto_a
    return-void

    #@b
    .line 2457
    :cond_b
    :try_start_b
    iget-object v1, p0, Lcom/android/server/MountService$ObbAction;->mObbState:Lcom/android/server/MountService$ObbState;

    #@d
    iget-object v1, v1, Lcom/android/server/MountService$ObbState;->token:Landroid/os/storage/IObbActionListener;

    #@f
    iget-object v2, p0, Lcom/android/server/MountService$ObbAction;->mObbState:Lcom/android/server/MountService$ObbState;

    #@11
    iget-object v2, v2, Lcom/android/server/MountService$ObbState;->rawPath:Ljava/lang/String;

    #@13
    iget-object v3, p0, Lcom/android/server/MountService$ObbAction;->mObbState:Lcom/android/server/MountService$ObbState;

    #@15
    iget v3, v3, Lcom/android/server/MountService$ObbState;->nonce:I

    #@17
    invoke-interface {v1, v2, v3, p1}, Landroid/os/storage/IObbActionListener;->onObbResult(Ljava/lang/String;II)V
    :try_end_1a
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_1a} :catch_1b

    #@1a
    goto :goto_a

    #@1b
    .line 2458
    :catch_1b
    move-exception v0

    #@1c
    .line 2459
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "MountService"

    #@1e
    const-string v2, "MountServiceListener went away while calling onObbStateChanged"

    #@20
    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    goto :goto_a
.end method
