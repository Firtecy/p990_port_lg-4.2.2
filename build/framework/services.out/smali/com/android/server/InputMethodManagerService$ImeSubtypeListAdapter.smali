.class Lcom/android/server/InputMethodManagerService$ImeSubtypeListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "InputMethodManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/InputMethodManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ImeSubtypeListAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/android/server/InputMethodManagerService$ImeSubtypeListItem;",
        ">;"
    }
.end annotation


# instance fields
.field private final mCheckedItem:I

.field private final mInflater:Landroid/view/LayoutInflater;

.field private final mItemsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/server/InputMethodManagerService$ImeSubtypeListItem;",
            ">;"
        }
    .end annotation
.end field

.field private final mTextViewResourceId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/List;I)V
    .registers 6
    .parameter "context"
    .parameter "textViewResourceId"
    .parameter
    .parameter "checkedItem"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/android/server/InputMethodManagerService$ImeSubtypeListItem;",
            ">;I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 2843
    .local p3, itemsList:Ljava/util/List;,"Ljava/util/List<Lcom/android/server/InputMethodManagerService$ImeSubtypeListItem;>;"
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    #@3
    .line 2844
    iput p2, p0, Lcom/android/server/InputMethodManagerService$ImeSubtypeListAdapter;->mTextViewResourceId:I

    #@5
    .line 2845
    iput-object p3, p0, Lcom/android/server/InputMethodManagerService$ImeSubtypeListAdapter;->mItemsList:Ljava/util/List;

    #@7
    .line 2846
    iput p4, p0, Lcom/android/server/InputMethodManagerService$ImeSubtypeListAdapter;->mCheckedItem:I

    #@9
    .line 2847
    const-string v0, "layout_inflater"

    #@b
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@e
    move-result-object v0

    #@f
    check-cast v0, Landroid/view/LayoutInflater;

    #@11
    iput-object v0, p0, Lcom/android/server/InputMethodManagerService$ImeSubtypeListAdapter;->mInflater:Landroid/view/LayoutInflater;

    #@13
    .line 2848
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 15
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 2852
    if-eqz p2, :cond_f

    #@3
    move-object v6, p2

    #@4
    .line 2854
    .local v6, view:Landroid/view/View;
    :goto_4
    if-ltz p1, :cond_e

    #@6
    iget-object v8, p0, Lcom/android/server/InputMethodManagerService$ImeSubtypeListAdapter;->mItemsList:Ljava/util/List;

    #@8
    invoke-interface {v8}, Ljava/util/List;->size()I

    #@b
    move-result v8

    #@c
    if-lt p1, v8, :cond_19

    #@e
    .line 2871
    :cond_e
    :goto_e
    return-object v6

    #@f
    .line 2852
    .end local v6           #view:Landroid/view/View;
    :cond_f
    iget-object v8, p0, Lcom/android/server/InputMethodManagerService$ImeSubtypeListAdapter;->mInflater:Landroid/view/LayoutInflater;

    #@11
    iget v9, p0, Lcom/android/server/InputMethodManagerService$ImeSubtypeListAdapter;->mTextViewResourceId:I

    #@13
    const/4 v10, 0x0

    #@14
    invoke-virtual {v8, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@17
    move-result-object v6

    #@18
    goto :goto_4

    #@19
    .line 2855
    .restart local v6       #view:Landroid/view/View;
    :cond_19
    iget-object v8, p0, Lcom/android/server/InputMethodManagerService$ImeSubtypeListAdapter;->mItemsList:Ljava/util/List;

    #@1b
    invoke-interface {v8, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@1e
    move-result-object v2

    #@1f
    check-cast v2, Lcom/android/server/InputMethodManagerService$ImeSubtypeListItem;

    #@21
    .line 2856
    .local v2, item:Lcom/android/server/InputMethodManagerService$ImeSubtypeListItem;
    iget-object v1, v2, Lcom/android/server/InputMethodManagerService$ImeSubtypeListItem;->mImeName:Ljava/lang/CharSequence;

    #@23
    .line 2857
    .local v1, imeName:Ljava/lang/CharSequence;
    iget-object v5, v2, Lcom/android/server/InputMethodManagerService$ImeSubtypeListItem;->mSubtypeName:Ljava/lang/CharSequence;

    #@25
    .line 2858
    .local v5, subtypeName:Ljava/lang/CharSequence;
    const v8, 0x1020014

    #@28
    invoke-virtual {v6, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@2b
    move-result-object v0

    #@2c
    check-cast v0, Landroid/widget/TextView;

    #@2e
    .line 2859
    .local v0, firstTextView:Landroid/widget/TextView;
    const v8, 0x1020015

    #@31
    invoke-virtual {v6, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@34
    move-result-object v4

    #@35
    check-cast v4, Landroid/widget/TextView;

    #@37
    .line 2860
    .local v4, secondTextView:Landroid/widget/TextView;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@3a
    move-result v8

    #@3b
    if-eqz v8, :cond_57

    #@3d
    .line 2861
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@40
    .line 2862
    const/16 v8, 0x8

    #@42
    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    #@45
    .line 2868
    :goto_45
    const v8, 0x1020322

    #@48
    invoke-virtual {v6, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@4b
    move-result-object v3

    #@4c
    check-cast v3, Landroid/widget/RadioButton;

    #@4e
    .line 2870
    .local v3, radioButton:Landroid/widget/RadioButton;
    iget v8, p0, Lcom/android/server/InputMethodManagerService$ImeSubtypeListAdapter;->mCheckedItem:I

    #@50
    if-ne p1, v8, :cond_53

    #@52
    const/4 v7, 0x1

    #@53
    :cond_53
    invoke-virtual {v3, v7}, Landroid/widget/RadioButton;->setChecked(Z)V

    #@56
    goto :goto_e

    #@57
    .line 2864
    .end local v3           #radioButton:Landroid/widget/RadioButton;
    :cond_57
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@5a
    .line 2865
    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@5d
    .line 2866
    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    #@60
    goto :goto_45
.end method
