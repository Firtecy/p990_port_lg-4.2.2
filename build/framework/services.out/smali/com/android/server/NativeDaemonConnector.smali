.class final Lcom/android/server/NativeDaemonConnector;
.super Ljava/lang/Object;
.source "NativeDaemonConnector.java"

# interfaces
.implements Ljava/lang/Runnable;
.implements Landroid/os/Handler$Callback;
.implements Lcom/android/server/Watchdog$Monitor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/NativeDaemonConnector$ResponseQueue;,
        Lcom/android/server/NativeDaemonConnector$Command;,
        Lcom/android/server/NativeDaemonConnector$NativeDaemonFailureException;,
        Lcom/android/server/NativeDaemonConnector$NativeDaemonArgumentException;
    }
.end annotation


# static fields
.field private static final DEFAULT_TIMEOUT:I = 0xea60

.field private static final LOGD:Z = true

.field private static final WARN_EXECUTE_DELAY_MS:J = 0x1f4L


# instance fields
.field private final BUFFER_SIZE:I

.field private final TAG:Ljava/lang/String;

.field private mCallbackHandler:Landroid/os/Handler;

.field private mCallbacks:Lcom/android/server/INativeDaemonConnectorCallbacks;

.field private final mDaemonLock:Ljava/lang/Object;

.field private mLocalLog:Landroid/util/LocalLog;

.field private mOutputStream:Ljava/io/OutputStream;

.field private final mResponseQueue:Lcom/android/server/NativeDaemonConnector$ResponseQueue;

.field private mSequenceNumber:Ljava/util/concurrent/atomic/AtomicInteger;

.field private mSocket:Ljava/lang/String;

.field private mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;


# direct methods
.method constructor <init>(Lcom/android/server/INativeDaemonConnectorCallbacks;Ljava/lang/String;ILjava/lang/String;I)V
    .registers 8
    .parameter "callbacks"
    .parameter "socket"
    .parameter "responseQueueSize"
    .parameter "logTag"
    .parameter "maxLogSize"

    #@0
    .prologue
    .line 81
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 76
    new-instance v0, Ljava/lang/Object;

    #@5
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/server/NativeDaemonConnector;->mDaemonLock:Ljava/lang/Object;

    #@a
    .line 78
    const/16 v0, 0x1000

    #@c
    iput v0, p0, Lcom/android/server/NativeDaemonConnector;->BUFFER_SIZE:I

    #@e
    .line 82
    iput-object p1, p0, Lcom/android/server/NativeDaemonConnector;->mCallbacks:Lcom/android/server/INativeDaemonConnectorCallbacks;

    #@10
    .line 83
    iput-object p2, p0, Lcom/android/server/NativeDaemonConnector;->mSocket:Ljava/lang/String;

    #@12
    .line 84
    new-instance v0, Lcom/android/server/NativeDaemonConnector$ResponseQueue;

    #@14
    invoke-direct {v0, p3}, Lcom/android/server/NativeDaemonConnector$ResponseQueue;-><init>(I)V

    #@17
    iput-object v0, p0, Lcom/android/server/NativeDaemonConnector;->mResponseQueue:Lcom/android/server/NativeDaemonConnector$ResponseQueue;

    #@19
    .line 85
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    #@1b
    const/4 v1, 0x0

    #@1c
    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    #@1f
    iput-object v0, p0, Lcom/android/server/NativeDaemonConnector;->mSequenceNumber:Ljava/util/concurrent/atomic/AtomicInteger;

    #@21
    .line 86
    if-eqz p4, :cond_37

    #@23
    .end local p4
    :goto_23
    iput-object p4, p0, Lcom/android/server/NativeDaemonConnector;->TAG:Ljava/lang/String;

    #@25
    .line 87
    new-instance v0, Landroid/util/LocalLog;

    #@27
    invoke-direct {v0, p5}, Landroid/util/LocalLog;-><init>(I)V

    #@2a
    iput-object v0, p0, Lcom/android/server/NativeDaemonConnector;->mLocalLog:Landroid/util/LocalLog;

    #@2c
    .line 89
    sget-boolean v0, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@2e
    if-eqz v0, :cond_36

    #@30
    .line 90
    invoke-static {}, Lcom/lge/wifi_iface/WifiIfaceManager;->getWifiServiceExtIface()Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@33
    move-result-object v0

    #@34
    iput-object v0, p0, Lcom/android/server/NativeDaemonConnector;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@36
    .line 93
    :cond_36
    return-void

    #@37
    .line 86
    .restart local p4
    :cond_37
    const-string p4, "NativeDaemonConnector"

    #@39
    goto :goto_23
.end method

.method static appendEscaped(Ljava/lang/StringBuilder;Ljava/lang/String;)V
    .registers 8
    .parameter "builder"
    .parameter "arg"

    #@0
    .prologue
    const/16 v5, 0x22

    #@2
    .line 431
    const/16 v4, 0x20

    #@4
    invoke-virtual {p1, v4}, Ljava/lang/String;->indexOf(I)I

    #@7
    move-result v4

    #@8
    if-ltz v4, :cond_25

    #@a
    const/4 v1, 0x1

    #@b
    .line 432
    .local v1, hasSpaces:Z
    :goto_b
    if-eqz v1, :cond_10

    #@d
    .line 433
    invoke-virtual {p0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@10
    .line 436
    :cond_10
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@13
    move-result v3

    #@14
    .line 437
    .local v3, length:I
    const/4 v2, 0x0

    #@15
    .local v2, i:I
    :goto_15
    if-ge v2, v3, :cond_35

    #@17
    .line 438
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    #@1a
    move-result v0

    #@1b
    .line 440
    .local v0, c:C
    if-ne v0, v5, :cond_27

    #@1d
    .line 441
    const-string v4, "\\\""

    #@1f
    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    .line 437
    :goto_22
    add-int/lit8 v2, v2, 0x1

    #@24
    goto :goto_15

    #@25
    .line 431
    .end local v0           #c:C
    .end local v1           #hasSpaces:Z
    .end local v2           #i:I
    .end local v3           #length:I
    :cond_25
    const/4 v1, 0x0

    #@26
    goto :goto_b

    #@27
    .line 442
    .restart local v0       #c:C
    .restart local v1       #hasSpaces:Z
    .restart local v2       #i:I
    .restart local v3       #length:I
    :cond_27
    const/16 v4, 0x5c

    #@29
    if-ne v0, v4, :cond_31

    #@2b
    .line 443
    const-string v4, "\\\\"

    #@2d
    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    goto :goto_22

    #@31
    .line 445
    :cond_31
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@34
    goto :goto_22

    #@35
    .line 449
    .end local v0           #c:C
    :cond_35
    if-eqz v1, :cond_3a

    #@37
    .line 450
    invoke-virtual {p0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@3a
    .line 452
    :cond_3a
    return-void
.end method

.method private listenToSocket()V
    .registers 20
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 125
    const/4 v12, 0x0

    #@1
    .line 128
    .local v12, socket:Landroid/net/LocalSocket;
    :try_start_1
    new-instance v13, Landroid/net/LocalSocket;

    #@3
    invoke-direct {v13}, Landroid/net/LocalSocket;-><init>()V
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_c0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_6} :catch_249

    #@6
    .line 129
    .end local v12           #socket:Landroid/net/LocalSocket;
    .local v13, socket:Landroid/net/LocalSocket;
    :try_start_6
    new-instance v2, Landroid/net/LocalSocketAddress;

    #@8
    move-object/from16 v0, p0

    #@a
    iget-object v15, v0, Lcom/android/server/NativeDaemonConnector;->mSocket:Ljava/lang/String;

    #@c
    sget-object v16, Landroid/net/LocalSocketAddress$Namespace;->RESERVED:Landroid/net/LocalSocketAddress$Namespace;

    #@e
    move-object/from16 v0, v16

    #@10
    invoke-direct {v2, v15, v0}, Landroid/net/LocalSocketAddress;-><init>(Ljava/lang/String;Landroid/net/LocalSocketAddress$Namespace;)V

    #@13
    .line 132
    .local v2, address:Landroid/net/LocalSocketAddress;
    invoke-virtual {v13, v2}, Landroid/net/LocalSocket;->connect(Landroid/net/LocalSocketAddress;)V

    #@16
    .line 134
    invoke-virtual {v13}, Landroid/net/LocalSocket;->getInputStream()Ljava/io/InputStream;

    #@19
    move-result-object v9

    #@1a
    .line 135
    .local v9, inputStream:Ljava/io/InputStream;
    move-object/from16 v0, p0

    #@1c
    iget-object v0, v0, Lcom/android/server/NativeDaemonConnector;->mDaemonLock:Ljava/lang/Object;

    #@1e
    move-object/from16 v16, v0

    #@20
    monitor-enter v16
    :try_end_21
    .catchall {:try_start_6 .. :try_end_21} :catchall_194
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_21} :catch_a5

    #@21
    .line 136
    :try_start_21
    invoke-virtual {v13}, Landroid/net/LocalSocket;->getOutputStream()Ljava/io/OutputStream;

    #@24
    move-result-object v15

    #@25
    move-object/from16 v0, p0

    #@27
    iput-object v15, v0, Lcom/android/server/NativeDaemonConnector;->mOutputStream:Ljava/io/OutputStream;

    #@29
    .line 137
    monitor-exit v16
    :try_end_2a
    .catchall {:try_start_21 .. :try_end_2a} :catchall_a2

    #@2a
    .line 139
    :try_start_2a
    move-object/from16 v0, p0

    #@2c
    iget-object v15, v0, Lcom/android/server/NativeDaemonConnector;->mCallbacks:Lcom/android/server/INativeDaemonConnectorCallbacks;

    #@2e
    invoke-interface {v15}, Lcom/android/server/INativeDaemonConnectorCallbacks;->onDaemonConnected()V

    #@31
    .line 141
    const/16 v15, 0x1000

    #@33
    new-array v3, v15, [B

    #@35
    .line 142
    .local v3, buffer:[B
    const/4 v14, 0x0

    #@36
    .line 145
    .local v14, start:I
    :goto_36
    rsub-int v15, v14, 0x1000

    #@38
    invoke-virtual {v9, v3, v14, v15}, Ljava/io/InputStream;->read([BII)I

    #@3b
    move-result v4

    #@3c
    .line 146
    .local v4, count:I
    if-gez v4, :cond_108

    #@3e
    .line 147
    new-instance v15, Ljava/lang/StringBuilder;

    #@40
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@43
    const-string v16, "got "

    #@45
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v15

    #@49
    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v15

    #@4d
    const-string v16, " reading with start = "

    #@4f
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v15

    #@53
    invoke-virtual {v15, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@56
    move-result-object v15

    #@57
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v15

    #@5b
    move-object/from16 v0, p0

    #@5d
    invoke-direct {v0, v15}, Lcom/android/server/NativeDaemonConnector;->loge(Ljava/lang/String;)V
    :try_end_60
    .catchall {:try_start_2a .. :try_end_60} :catchall_194
    .catch Ljava/io/IOException; {:try_start_2a .. :try_end_60} :catch_a5

    #@60
    .line 197
    move-object/from16 v0, p0

    #@62
    iget-object v0, v0, Lcom/android/server/NativeDaemonConnector;->mDaemonLock:Ljava/lang/Object;

    #@64
    move-object/from16 v16, v0

    #@66
    monitor-enter v16

    #@67
    .line 198
    :try_start_67
    move-object/from16 v0, p0

    #@69
    iget-object v15, v0, Lcom/android/server/NativeDaemonConnector;->mOutputStream:Ljava/io/OutputStream;
    :try_end_6b
    .catchall {:try_start_67 .. :try_end_6b} :catchall_20e

    #@6b
    if-eqz v15, :cond_9b

    #@6d
    .line 200
    :try_start_6d
    new-instance v15, Ljava/lang/StringBuilder;

    #@6f
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@72
    const-string v17, "closing stream for "

    #@74
    move-object/from16 v0, v17

    #@76
    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v15

    #@7a
    move-object/from16 v0, p0

    #@7c
    iget-object v0, v0, Lcom/android/server/NativeDaemonConnector;->mSocket:Ljava/lang/String;

    #@7e
    move-object/from16 v17, v0

    #@80
    move-object/from16 v0, v17

    #@82
    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v15

    #@86
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@89
    move-result-object v15

    #@8a
    move-object/from16 v0, p0

    #@8c
    invoke-direct {v0, v15}, Lcom/android/server/NativeDaemonConnector;->loge(Ljava/lang/String;)V

    #@8f
    .line 201
    move-object/from16 v0, p0

    #@91
    iget-object v15, v0, Lcom/android/server/NativeDaemonConnector;->mOutputStream:Ljava/io/OutputStream;

    #@93
    invoke-virtual {v15}, Ljava/io/OutputStream;->close()V
    :try_end_96
    .catchall {:try_start_6d .. :try_end_96} :catchall_20e
    .catch Ljava/io/IOException; {:try_start_6d .. :try_end_96} :catch_22c

    #@96
    .line 205
    :goto_96
    const/4 v15, 0x0

    #@97
    :try_start_97
    move-object/from16 v0, p0

    #@99
    iput-object v15, v0, Lcom/android/server/NativeDaemonConnector;->mOutputStream:Ljava/io/OutputStream;

    #@9b
    .line 207
    :cond_9b
    monitor-exit v16
    :try_end_9c
    .catchall {:try_start_97 .. :try_end_9c} :catchall_20e

    #@9c
    .line 210
    if-eqz v13, :cond_a1

    #@9e
    .line 211
    :try_start_9e
    invoke-virtual {v13}, Landroid/net/LocalSocket;->close()V
    :try_end_a1
    .catch Ljava/io/IOException; {:try_start_9e .. :try_end_a1} :catch_211

    #@a1
    .line 217
    :cond_a1
    :goto_a1
    return-void

    #@a2
    .line 137
    .end local v3           #buffer:[B
    .end local v4           #count:I
    .end local v14           #start:I
    :catchall_a2
    move-exception v15

    #@a3
    :try_start_a3
    monitor-exit v16
    :try_end_a4
    .catchall {:try_start_a3 .. :try_end_a4} :catchall_a2

    #@a4
    :try_start_a4
    throw v15
    :try_end_a5
    .catchall {:try_start_a4 .. :try_end_a5} :catchall_194
    .catch Ljava/io/IOException; {:try_start_a4 .. :try_end_a5} :catch_a5

    #@a5
    .line 193
    .end local v2           #address:Landroid/net/LocalSocketAddress;
    .end local v9           #inputStream:Ljava/io/InputStream;
    :catch_a5
    move-exception v7

    #@a6
    move-object v12, v13

    #@a7
    .line 194
    .end local v13           #socket:Landroid/net/LocalSocket;
    .local v7, ex:Ljava/io/IOException;
    .restart local v12       #socket:Landroid/net/LocalSocket;
    :goto_a7
    :try_start_a7
    new-instance v15, Ljava/lang/StringBuilder;

    #@a9
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@ac
    const-string v16, "Communications error: "

    #@ae
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v15

    #@b2
    invoke-virtual {v15, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v15

    #@b6
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b9
    move-result-object v15

    #@ba
    move-object/from16 v0, p0

    #@bc
    invoke-direct {v0, v15}, Lcom/android/server/NativeDaemonConnector;->loge(Ljava/lang/String;)V

    #@bf
    .line 195
    throw v7
    :try_end_c0
    .catchall {:try_start_a7 .. :try_end_c0} :catchall_c0

    #@c0
    .line 197
    .end local v7           #ex:Ljava/io/IOException;
    :catchall_c0
    move-exception v15

    #@c1
    :goto_c1
    move-object/from16 v0, p0

    #@c3
    iget-object v0, v0, Lcom/android/server/NativeDaemonConnector;->mDaemonLock:Ljava/lang/Object;

    #@c5
    move-object/from16 v16, v0

    #@c7
    monitor-enter v16

    #@c8
    .line 198
    :try_start_c8
    move-object/from16 v0, p0

    #@ca
    iget-object v0, v0, Lcom/android/server/NativeDaemonConnector;->mOutputStream:Ljava/io/OutputStream;

    #@cc
    move-object/from16 v17, v0
    :try_end_ce
    .catchall {:try_start_c8 .. :try_end_ce} :catchall_1cd

    #@ce
    if-eqz v17, :cond_101

    #@d0
    .line 200
    :try_start_d0
    new-instance v17, Ljava/lang/StringBuilder;

    #@d2
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@d5
    const-string v18, "closing stream for "

    #@d7
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@da
    move-result-object v17

    #@db
    move-object/from16 v0, p0

    #@dd
    iget-object v0, v0, Lcom/android/server/NativeDaemonConnector;->mSocket:Ljava/lang/String;

    #@df
    move-object/from16 v18, v0

    #@e1
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e4
    move-result-object v17

    #@e5
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e8
    move-result-object v17

    #@e9
    move-object/from16 v0, p0

    #@eb
    move-object/from16 v1, v17

    #@ed
    invoke-direct {v0, v1}, Lcom/android/server/NativeDaemonConnector;->loge(Ljava/lang/String;)V

    #@f0
    .line 201
    move-object/from16 v0, p0

    #@f2
    iget-object v0, v0, Lcom/android/server/NativeDaemonConnector;->mOutputStream:Ljava/io/OutputStream;

    #@f4
    move-object/from16 v17, v0

    #@f6
    invoke-virtual/range {v17 .. v17}, Ljava/io/OutputStream;->close()V
    :try_end_f9
    .catchall {:try_start_d0 .. :try_end_f9} :catchall_1cd
    .catch Ljava/io/IOException; {:try_start_d0 .. :try_end_f9} :catch_1ef

    #@f9
    .line 205
    :goto_f9
    const/16 v17, 0x0

    #@fb
    :try_start_fb
    move-object/from16 v0, v17

    #@fd
    move-object/from16 v1, p0

    #@ff
    iput-object v0, v1, Lcom/android/server/NativeDaemonConnector;->mOutputStream:Ljava/io/OutputStream;

    #@101
    .line 207
    :cond_101
    monitor-exit v16
    :try_end_102
    .catchall {:try_start_fb .. :try_end_102} :catchall_1cd

    #@102
    .line 210
    if-eqz v12, :cond_107

    #@104
    .line 211
    :try_start_104
    invoke-virtual {v12}, Landroid/net/LocalSocket;->close()V
    :try_end_107
    .catch Ljava/io/IOException; {:try_start_104 .. :try_end_107} :catch_1d0

    #@107
    .line 197
    :cond_107
    :goto_107
    throw v15

    #@108
    .line 152
    .end local v12           #socket:Landroid/net/LocalSocket;
    .restart local v2       #address:Landroid/net/LocalSocketAddress;
    .restart local v3       #buffer:[B
    .restart local v4       #count:I
    .restart local v9       #inputStream:Ljava/io/InputStream;
    .restart local v13       #socket:Landroid/net/LocalSocket;
    .restart local v14       #start:I
    :cond_108
    add-int/2addr v4, v14

    #@109
    .line 153
    const/4 v14, 0x0

    #@10a
    .line 155
    const/4 v8, 0x0

    #@10b
    .local v8, i:I
    :goto_10b
    if-ge v8, v4, :cond_198

    #@10d
    .line 156
    :try_start_10d
    aget-byte v15, v3, v8

    #@10f
    if-nez v15, :cond_15f

    #@111
    .line 157
    new-instance v10, Ljava/lang/String;

    #@113
    sub-int v15, v8, v14

    #@115
    sget-object v16, Ljava/nio/charset/Charsets;->UTF_8:Ljava/nio/charset/Charset;

    #@117
    move-object/from16 v0, v16

    #@119
    invoke-direct {v10, v3, v14, v15, v0}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    #@11c
    .line 159
    .local v10, rawEvent:Ljava/lang/String;
    new-instance v15, Ljava/lang/StringBuilder;

    #@11e
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@121
    const-string v16, "RCV <- {"

    #@123
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@126
    move-result-object v15

    #@127
    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12a
    move-result-object v15

    #@12b
    const-string v16, "}"

    #@12d
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@130
    move-result-object v15

    #@131
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@134
    move-result-object v15

    #@135
    move-object/from16 v0, p0

    #@137
    invoke-direct {v0, v15}, Lcom/android/server/NativeDaemonConnector;->log(Ljava/lang/String;)V
    :try_end_13a
    .catchall {:try_start_10d .. :try_end_13a} :catchall_194
    .catch Ljava/io/IOException; {:try_start_10d .. :try_end_13a} :catch_a5

    #@13a
    .line 162
    :try_start_13a
    invoke-static {v10}, Lcom/android/server/NativeDaemonEvent;->parseRawEvent(Ljava/lang/String;)Lcom/android/server/NativeDaemonEvent;

    #@13d
    move-result-object v6

    #@13e
    .line 164
    .local v6, event:Lcom/android/server/NativeDaemonEvent;
    invoke-virtual {v6}, Lcom/android/server/NativeDaemonEvent;->isClassUnsolicited()Z

    #@141
    move-result v15

    #@142
    if-eqz v15, :cond_162

    #@144
    .line 166
    move-object/from16 v0, p0

    #@146
    iget-object v15, v0, Lcom/android/server/NativeDaemonConnector;->mCallbackHandler:Landroid/os/Handler;

    #@148
    move-object/from16 v0, p0

    #@14a
    iget-object v0, v0, Lcom/android/server/NativeDaemonConnector;->mCallbackHandler:Landroid/os/Handler;

    #@14c
    move-object/from16 v16, v0

    #@14e
    invoke-virtual {v6}, Lcom/android/server/NativeDaemonEvent;->getCode()I

    #@151
    move-result v17

    #@152
    invoke-virtual {v6}, Lcom/android/server/NativeDaemonEvent;->getRawEvent()Ljava/lang/String;

    #@155
    move-result-object v18

    #@156
    invoke-virtual/range {v16 .. v18}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@159
    move-result-object v16

    #@15a
    invoke-virtual/range {v15 .. v16}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@15d
    .line 175
    .end local v6           #event:Lcom/android/server/NativeDaemonEvent;
    :goto_15d
    add-int/lit8 v14, v8, 0x1

    #@15f
    .line 155
    .end local v10           #rawEvent:Ljava/lang/String;
    :cond_15f
    add-int/lit8 v8, v8, 0x1

    #@161
    goto :goto_10b

    #@162
    .line 169
    .restart local v6       #event:Lcom/android/server/NativeDaemonEvent;
    .restart local v10       #rawEvent:Ljava/lang/String;
    :cond_162
    move-object/from16 v0, p0

    #@164
    iget-object v15, v0, Lcom/android/server/NativeDaemonConnector;->mResponseQueue:Lcom/android/server/NativeDaemonConnector$ResponseQueue;

    #@166
    invoke-virtual {v6}, Lcom/android/server/NativeDaemonEvent;->getCmdNumber()I

    #@169
    move-result v16

    #@16a
    move/from16 v0, v16

    #@16c
    invoke-virtual {v15, v0, v6}, Lcom/android/server/NativeDaemonConnector$ResponseQueue;->add(ILcom/android/server/NativeDaemonEvent;)V
    :try_end_16f
    .catchall {:try_start_13a .. :try_end_16f} :catchall_194
    .catch Ljava/lang/IllegalArgumentException; {:try_start_13a .. :try_end_16f} :catch_170
    .catch Ljava/io/IOException; {:try_start_13a .. :try_end_16f} :catch_a5

    #@16f
    goto :goto_15d

    #@170
    .line 171
    .end local v6           #event:Lcom/android/server/NativeDaemonEvent;
    :catch_170
    move-exception v5

    #@171
    .line 172
    .local v5, e:Ljava/lang/IllegalArgumentException;
    :try_start_171
    new-instance v15, Ljava/lang/StringBuilder;

    #@173
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@176
    const-string v16, "Problem parsing message: "

    #@178
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17b
    move-result-object v15

    #@17c
    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17f
    move-result-object v15

    #@180
    const-string v16, " - "

    #@182
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@185
    move-result-object v15

    #@186
    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@189
    move-result-object v15

    #@18a
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18d
    move-result-object v15

    #@18e
    move-object/from16 v0, p0

    #@190
    invoke-direct {v0, v15}, Lcom/android/server/NativeDaemonConnector;->log(Ljava/lang/String;)V

    #@193
    goto :goto_15d

    #@194
    .line 197
    .end local v2           #address:Landroid/net/LocalSocketAddress;
    .end local v3           #buffer:[B
    .end local v4           #count:I
    .end local v5           #e:Ljava/lang/IllegalArgumentException;
    .end local v8           #i:I
    .end local v9           #inputStream:Ljava/io/InputStream;
    .end local v10           #rawEvent:Ljava/lang/String;
    .end local v14           #start:I
    :catchall_194
    move-exception v15

    #@195
    move-object v12, v13

    #@196
    .end local v13           #socket:Landroid/net/LocalSocket;
    .restart local v12       #socket:Landroid/net/LocalSocket;
    goto/16 :goto_c1

    #@198
    .line 178
    .end local v12           #socket:Landroid/net/LocalSocket;
    .restart local v2       #address:Landroid/net/LocalSocketAddress;
    .restart local v3       #buffer:[B
    .restart local v4       #count:I
    .restart local v8       #i:I
    .restart local v9       #inputStream:Ljava/io/InputStream;
    .restart local v13       #socket:Landroid/net/LocalSocket;
    .restart local v14       #start:I
    :cond_198
    if-nez v14, :cond_1bf

    #@19a
    .line 179
    new-instance v10, Ljava/lang/String;

    #@19c
    sget-object v15, Ljava/nio/charset/Charsets;->UTF_8:Ljava/nio/charset/Charset;

    #@19e
    invoke-direct {v10, v3, v14, v4, v15}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    #@1a1
    .line 180
    .restart local v10       #rawEvent:Ljava/lang/String;
    new-instance v15, Ljava/lang/StringBuilder;

    #@1a3
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@1a6
    const-string v16, "RCV incomplete <- {"

    #@1a8
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ab
    move-result-object v15

    #@1ac
    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1af
    move-result-object v15

    #@1b0
    const-string v16, "}"

    #@1b2
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b5
    move-result-object v15

    #@1b6
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b9
    move-result-object v15

    #@1ba
    move-object/from16 v0, p0

    #@1bc
    invoke-direct {v0, v15}, Lcom/android/server/NativeDaemonConnector;->log(Ljava/lang/String;)V

    #@1bf
    .line 185
    .end local v10           #rawEvent:Ljava/lang/String;
    :cond_1bf
    if-eq v14, v4, :cond_1ca

    #@1c1
    .line 186
    rsub-int v11, v14, 0x1000

    #@1c3
    .line 187
    .local v11, remaining:I
    const/4 v15, 0x0

    #@1c4
    invoke-static {v3, v14, v3, v15, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_1c7
    .catchall {:try_start_171 .. :try_end_1c7} :catchall_194
    .catch Ljava/io/IOException; {:try_start_171 .. :try_end_1c7} :catch_a5

    #@1c7
    .line 188
    move v14, v11

    #@1c8
    .line 189
    goto/16 :goto_36

    #@1ca
    .line 190
    .end local v11           #remaining:I
    :cond_1ca
    const/4 v14, 0x0

    #@1cb
    goto/16 :goto_36

    #@1cd
    .line 207
    .end local v2           #address:Landroid/net/LocalSocketAddress;
    .end local v3           #buffer:[B
    .end local v4           #count:I
    .end local v8           #i:I
    .end local v9           #inputStream:Ljava/io/InputStream;
    .end local v13           #socket:Landroid/net/LocalSocket;
    .end local v14           #start:I
    .restart local v12       #socket:Landroid/net/LocalSocket;
    :catchall_1cd
    move-exception v15

    #@1ce
    :try_start_1ce
    monitor-exit v16
    :try_end_1cf
    .catchall {:try_start_1ce .. :try_end_1cf} :catchall_1cd

    #@1cf
    throw v15

    #@1d0
    .line 213
    :catch_1d0
    move-exception v7

    #@1d1
    .line 214
    .restart local v7       #ex:Ljava/io/IOException;
    new-instance v16, Ljava/lang/StringBuilder;

    #@1d3
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@1d6
    const-string v17, "Failed closing socket: "

    #@1d8
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1db
    move-result-object v16

    #@1dc
    move-object/from16 v0, v16

    #@1de
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1e1
    move-result-object v16

    #@1e2
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e5
    move-result-object v16

    #@1e6
    move-object/from16 v0, p0

    #@1e8
    move-object/from16 v1, v16

    #@1ea
    invoke-direct {v0, v1}, Lcom/android/server/NativeDaemonConnector;->loge(Ljava/lang/String;)V

    #@1ed
    goto/16 :goto_107

    #@1ef
    .line 202
    .end local v7           #ex:Ljava/io/IOException;
    :catch_1ef
    move-exception v5

    #@1f0
    .line 203
    .local v5, e:Ljava/io/IOException;
    :try_start_1f0
    new-instance v17, Ljava/lang/StringBuilder;

    #@1f2
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@1f5
    const-string v18, "Failed closing output stream: "

    #@1f7
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1fa
    move-result-object v17

    #@1fb
    move-object/from16 v0, v17

    #@1fd
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@200
    move-result-object v17

    #@201
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@204
    move-result-object v17

    #@205
    move-object/from16 v0, p0

    #@207
    move-object/from16 v1, v17

    #@209
    invoke-direct {v0, v1}, Lcom/android/server/NativeDaemonConnector;->loge(Ljava/lang/String;)V
    :try_end_20c
    .catchall {:try_start_1f0 .. :try_end_20c} :catchall_1cd

    #@20c
    goto/16 :goto_f9

    #@20e
    .line 207
    .end local v5           #e:Ljava/io/IOException;
    .end local v12           #socket:Landroid/net/LocalSocket;
    .restart local v2       #address:Landroid/net/LocalSocketAddress;
    .restart local v3       #buffer:[B
    .restart local v4       #count:I
    .restart local v9       #inputStream:Ljava/io/InputStream;
    .restart local v13       #socket:Landroid/net/LocalSocket;
    .restart local v14       #start:I
    :catchall_20e
    move-exception v15

    #@20f
    :try_start_20f
    monitor-exit v16
    :try_end_210
    .catchall {:try_start_20f .. :try_end_210} :catchall_20e

    #@210
    throw v15

    #@211
    .line 213
    :catch_211
    move-exception v7

    #@212
    .line 214
    .restart local v7       #ex:Ljava/io/IOException;
    new-instance v15, Ljava/lang/StringBuilder;

    #@214
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@217
    const-string v16, "Failed closing socket: "

    #@219
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21c
    move-result-object v15

    #@21d
    invoke-virtual {v15, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@220
    move-result-object v15

    #@221
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@224
    move-result-object v15

    #@225
    move-object/from16 v0, p0

    #@227
    invoke-direct {v0, v15}, Lcom/android/server/NativeDaemonConnector;->loge(Ljava/lang/String;)V

    #@22a
    goto/16 :goto_a1

    #@22c
    .line 202
    .end local v7           #ex:Ljava/io/IOException;
    :catch_22c
    move-exception v5

    #@22d
    .line 203
    .restart local v5       #e:Ljava/io/IOException;
    :try_start_22d
    new-instance v15, Ljava/lang/StringBuilder;

    #@22f
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@232
    const-string v17, "Failed closing output stream: "

    #@234
    move-object/from16 v0, v17

    #@236
    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@239
    move-result-object v15

    #@23a
    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@23d
    move-result-object v15

    #@23e
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@241
    move-result-object v15

    #@242
    move-object/from16 v0, p0

    #@244
    invoke-direct {v0, v15}, Lcom/android/server/NativeDaemonConnector;->loge(Ljava/lang/String;)V
    :try_end_247
    .catchall {:try_start_22d .. :try_end_247} :catchall_20e

    #@247
    goto/16 :goto_96

    #@249
    .line 193
    .end local v2           #address:Landroid/net/LocalSocketAddress;
    .end local v3           #buffer:[B
    .end local v4           #count:I
    .end local v5           #e:Ljava/io/IOException;
    .end local v9           #inputStream:Ljava/io/InputStream;
    .end local v13           #socket:Landroid/net/LocalSocket;
    .end local v14           #start:I
    .restart local v12       #socket:Landroid/net/LocalSocket;
    :catch_249
    move-exception v7

    #@24a
    goto/16 :goto_a7
.end method

.method private log(Ljava/lang/String;)V
    .registers 3
    .parameter "logstring"

    #@0
    .prologue
    .line 503
    iget-object v0, p0, Lcom/android/server/NativeDaemonConnector;->TAG:Ljava/lang/String;

    #@2
    invoke-static {v0, p1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 504
    iget-object v0, p0, Lcom/android/server/NativeDaemonConnector;->mLocalLog:Landroid/util/LocalLog;

    #@7
    invoke-virtual {v0, p1}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V

    #@a
    .line 505
    return-void
.end method

.method private loge(Ljava/lang/String;)V
    .registers 3
    .parameter "logstring"

    #@0
    .prologue
    .line 508
    iget-object v0, p0, Lcom/android/server/NativeDaemonConnector;->TAG:Ljava/lang/String;

    #@2
    invoke-static {v0, p1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 509
    iget-object v0, p0, Lcom/android/server/NativeDaemonConnector;->mLocalLog:Landroid/util/LocalLog;

    #@7
    invoke-virtual {v0, p1}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V

    #@a
    .line 510
    return-void
.end method

.method private varargs makeCommand(Ljava/lang/StringBuilder;Ljava/lang/String;[Ljava/lang/Object;)V
    .registers 12
    .parameter "builder"
    .parameter "cmd"
    .parameter "args"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/server/NativeDaemonConnectorException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 225
    invoke-virtual {p2, v6}, Ljava/lang/String;->indexOf(I)I

    #@4
    move-result v5

    #@5
    if-ltz v5, :cond_20

    #@7
    .line 226
    new-instance v5, Ljava/lang/IllegalArgumentException;

    #@9
    new-instance v6, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v7, "unexpected command: "

    #@10
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v6

    #@14
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v6

    #@18
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v6

    #@1c
    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v5

    #@20
    .line 229
    :cond_20
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    .line 230
    move-object v2, p3

    #@24
    .local v2, arr$:[Ljava/lang/Object;
    array-length v4, v2

    #@25
    .local v4, len$:I
    const/4 v3, 0x0

    #@26
    .local v3, i$:I
    :goto_26
    if-ge v3, v4, :cond_58

    #@28
    aget-object v0, v2, v3

    #@2a
    .line 231
    .local v0, arg:Ljava/lang/Object;
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    #@2d
    move-result-object v1

    #@2e
    .line 232
    .local v1, argString:Ljava/lang/String;
    invoke-virtual {v1, v6}, Ljava/lang/String;->indexOf(I)I

    #@31
    move-result v5

    #@32
    if-ltz v5, :cond_4d

    #@34
    .line 233
    new-instance v5, Ljava/lang/IllegalArgumentException;

    #@36
    new-instance v6, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v7, "unexpected argument: "

    #@3d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v6

    #@41
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v6

    #@45
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v6

    #@49
    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@4c
    throw v5

    #@4d
    .line 236
    :cond_4d
    const/16 v5, 0x20

    #@4f
    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@52
    .line 237
    invoke-static {p1, v1}, Lcom/android/server/NativeDaemonConnector;->appendEscaped(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    #@55
    .line 230
    add-int/lit8 v3, v3, 0x1

    #@57
    goto :goto_26

    #@58
    .line 239
    .end local v0           #arg:Ljava/lang/Object;
    .end local v1           #argString:Ljava/lang/String;
    :cond_58
    return-void
.end method


# virtual methods
.method public doCommand(Ljava/lang/String;)Ljava/util/ArrayList;
    .registers 9
    .parameter "cmd"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/server/NativeDaemonConnectorException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 388
    invoke-static {}, Lcom/google/android/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    #@3
    move-result-object v5

    #@4
    .line 389
    .local v5, rawEvents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v6, 0x0

    #@5
    new-array v6, v6, [Ljava/lang/Object;

    #@7
    invoke-virtual {p0, p1, v6}, Lcom/android/server/NativeDaemonConnector;->executeForList(Ljava/lang/String;[Ljava/lang/Object;)[Lcom/android/server/NativeDaemonEvent;

    #@a
    move-result-object v2

    #@b
    .line 390
    .local v2, events:[Lcom/android/server/NativeDaemonEvent;
    move-object v0, v2

    #@c
    .local v0, arr$:[Lcom/android/server/NativeDaemonEvent;
    array-length v4, v0

    #@d
    .local v4, len$:I
    const/4 v3, 0x0

    #@e
    .local v3, i$:I
    :goto_e
    if-ge v3, v4, :cond_1c

    #@10
    aget-object v1, v0, v3

    #@12
    .line 391
    .local v1, event:Lcom/android/server/NativeDaemonEvent;
    invoke-virtual {v1}, Lcom/android/server/NativeDaemonEvent;->getRawEvent()Ljava/lang/String;

    #@15
    move-result-object v6

    #@16
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@19
    .line 390
    add-int/lit8 v3, v3, 0x1

    #@1b
    goto :goto_e

    #@1c
    .line 393
    .end local v1           #event:Lcom/android/server/NativeDaemonEvent;
    :cond_1c
    return-object v5
.end method

.method public doListCommand(Ljava/lang/String;I)[Ljava/lang/String;
    .registers 12
    .parameter "cmd"
    .parameter "expectedCode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/server/NativeDaemonConnectorException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 403
    invoke-static {}, Lcom/google/android/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    #@3
    move-result-object v5

    #@4
    .line 405
    .local v5, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v6, 0x0

    #@5
    new-array v6, v6, [Ljava/lang/Object;

    #@7
    invoke-virtual {p0, p1, v6}, Lcom/android/server/NativeDaemonConnector;->executeForList(Ljava/lang/String;[Ljava/lang/Object;)[Lcom/android/server/NativeDaemonEvent;

    #@a
    move-result-object v2

    #@b
    .line 406
    .local v2, events:[Lcom/android/server/NativeDaemonEvent;
    const/4 v4, 0x0

    #@c
    .local v4, i:I
    :goto_c
    array-length v6, v2

    #@d
    add-int/lit8 v6, v6, -0x1

    #@f
    if-ge v4, v6, :cond_46

    #@11
    .line 407
    aget-object v1, v2, v4

    #@13
    .line 408
    .local v1, event:Lcom/android/server/NativeDaemonEvent;
    invoke-virtual {v1}, Lcom/android/server/NativeDaemonEvent;->getCode()I

    #@16
    move-result v0

    #@17
    .line 409
    .local v0, code:I
    if-ne v0, p2, :cond_23

    #@19
    .line 410
    invoke-virtual {v1}, Lcom/android/server/NativeDaemonEvent;->getMessage()Ljava/lang/String;

    #@1c
    move-result-object v6

    #@1d
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@20
    .line 406
    add-int/lit8 v4, v4, 0x1

    #@22
    goto :goto_c

    #@23
    .line 412
    :cond_23
    new-instance v6, Lcom/android/server/NativeDaemonConnectorException;

    #@25
    new-instance v7, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    const-string v8, "unexpected list response "

    #@2c
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v7

    #@30
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@33
    move-result-object v7

    #@34
    const-string v8, " instead of "

    #@36
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v7

    #@3a
    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v7

    #@3e
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v7

    #@42
    invoke-direct {v6, v7}, Lcom/android/server/NativeDaemonConnectorException;-><init>(Ljava/lang/String;)V

    #@45
    throw v6

    #@46
    .line 417
    .end local v0           #code:I
    .end local v1           #event:Lcom/android/server/NativeDaemonEvent;
    :cond_46
    array-length v6, v2

    #@47
    add-int/lit8 v6, v6, -0x1

    #@49
    aget-object v3, v2, v6

    #@4b
    .line 418
    .local v3, finalEvent:Lcom/android/server/NativeDaemonEvent;
    invoke-virtual {v3}, Lcom/android/server/NativeDaemonEvent;->isClassOk()Z

    #@4e
    move-result v6

    #@4f
    if-nez v6, :cond_6a

    #@51
    .line 419
    new-instance v6, Lcom/android/server/NativeDaemonConnectorException;

    #@53
    new-instance v7, Ljava/lang/StringBuilder;

    #@55
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@58
    const-string v8, "unexpected final event: "

    #@5a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v7

    #@5e
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v7

    #@62
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@65
    move-result-object v7

    #@66
    invoke-direct {v6, v7}, Lcom/android/server/NativeDaemonConnectorException;-><init>(Ljava/lang/String;)V

    #@69
    throw v6

    #@6a
    .line 422
    :cond_6a
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@6d
    move-result v6

    #@6e
    new-array v6, v6, [Ljava/lang/String;

    #@70
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@73
    move-result-object v6

    #@74
    check-cast v6, [Ljava/lang/String;

    #@76
    return-object v6
.end method

.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 5
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 497
    iget-object v0, p0, Lcom/android/server/NativeDaemonConnector;->mLocalLog:Landroid/util/LocalLog;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/util/LocalLog;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    #@5
    .line 498
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    #@8
    .line 499
    iget-object v0, p0, Lcom/android/server/NativeDaemonConnector;->mResponseQueue:Lcom/android/server/NativeDaemonConnector$ResponseQueue;

    #@a
    invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/NativeDaemonConnector$ResponseQueue;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    #@d
    .line 500
    return-void
.end method

.method public execute(Lcom/android/server/NativeDaemonConnector$Command;)Lcom/android/server/NativeDaemonEvent;
    .registers 4
    .parameter "cmd"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/server/NativeDaemonConnectorException;
        }
    .end annotation

    #@0
    .prologue
    .line 251
    invoke-static {p1}, Lcom/android/server/NativeDaemonConnector$Command;->access$000(Lcom/android/server/NativeDaemonConnector$Command;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-static {p1}, Lcom/android/server/NativeDaemonConnector$Command;->access$100(Lcom/android/server/NativeDaemonConnector$Command;)Ljava/util/ArrayList;

    #@7
    move-result-object v1

    #@8
    invoke-virtual {v1}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    #@b
    move-result-object v1

    #@c
    invoke-virtual {p0, v0, v1}, Lcom/android/server/NativeDaemonConnector;->execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;

    #@f
    move-result-object v0

    #@10
    return-object v0
.end method

.method public varargs execute(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
    .registers 7
    .parameter "cmd"
    .parameter "args"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/server/NativeDaemonConnectorException;
        }
    .end annotation

    #@0
    .prologue
    .line 265
    invoke-virtual {p0, p1, p2}, Lcom/android/server/NativeDaemonConnector;->executeForList(Ljava/lang/String;[Ljava/lang/Object;)[Lcom/android/server/NativeDaemonEvent;

    #@3
    move-result-object v0

    #@4
    .line 266
    .local v0, events:[Lcom/android/server/NativeDaemonEvent;
    array-length v1, v0

    #@5
    const/4 v2, 0x1

    #@6
    if-eq v1, v2, :cond_22

    #@8
    .line 267
    new-instance v1, Lcom/android/server/NativeDaemonConnectorException;

    #@a
    new-instance v2, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v3, "Expected exactly one response, but received "

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    array-length v3, v0

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    invoke-direct {v1, v2}, Lcom/android/server/NativeDaemonConnectorException;-><init>(Ljava/lang/String;)V

    #@21
    throw v1

    #@22
    .line 270
    :cond_22
    const/4 v1, 0x0

    #@23
    aget-object v1, v0, v1

    #@25
    return-object v1
.end method

.method public varargs execute(ILjava/lang/String;[Ljava/lang/Object;)[Lcom/android/server/NativeDaemonEvent;
    .registers 22
    .parameter "timeout"
    .parameter "cmd"
    .parameter "args"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/server/NativeDaemonConnectorException;
        }
    .end annotation

    #@0
    .prologue
    .line 315
    invoke-static {}, Lcom/google/android/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    #@3
    move-result-object v8

    #@4
    .line 317
    .local v8, events:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/NativeDaemonEvent;>;"
    move-object/from16 v0, p0

    #@6
    iget-object v14, v0, Lcom/android/server/NativeDaemonConnector;->mSequenceNumber:Ljava/util/concurrent/atomic/AtomicInteger;

    #@8
    invoke-virtual {v14}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    #@b
    move-result v11

    #@c
    .line 318
    .local v11, sequenceNumber:I
    new-instance v14, Ljava/lang/StringBuilder;

    #@e
    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@11
    move-result-object v15

    #@12
    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@15
    const/16 v15, 0x20

    #@17
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    .line 320
    .local v3, cmdBuilder:Ljava/lang/StringBuilder;
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@1e
    move-result-wide v12

    #@1f
    .line 322
    .local v12, startTime:J
    move-object/from16 v0, p0

    #@21
    move-object/from16 v1, p2

    #@23
    move-object/from16 v2, p3

    #@25
    invoke-direct {v0, v3, v1, v2}, Lcom/android/server/NativeDaemonConnector;->makeCommand(Ljava/lang/StringBuilder;Ljava/lang/String;[Ljava/lang/Object;)V

    #@28
    .line 324
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v9

    #@2c
    .line 325
    .local v9, logCmd:Ljava/lang/String;
    const-string v14, "psk"

    #@2e
    invoke-virtual {v9, v14}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@31
    move-result v14

    #@32
    if-nez v14, :cond_72

    #@34
    .line 326
    new-instance v14, Ljava/lang/StringBuilder;

    #@36
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    const-string v15, "SND -> {"

    #@3b
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v14

    #@3f
    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v14

    #@43
    const-string v15, "}"

    #@45
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v14

    #@49
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c
    move-result-object v14

    #@4d
    move-object/from16 v0, p0

    #@4f
    invoke-direct {v0, v14}, Lcom/android/server/NativeDaemonConnector;->log(Ljava/lang/String;)V

    #@52
    .line 330
    :goto_52
    const/4 v14, 0x0

    #@53
    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@56
    .line 331
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v10

    #@5a
    .line 333
    .local v10, sentCmd:Ljava/lang/String;
    move-object/from16 v0, p0

    #@5c
    iget-object v15, v0, Lcom/android/server/NativeDaemonConnector;->mDaemonLock:Ljava/lang/Object;

    #@5e
    monitor-enter v15

    #@5f
    .line 334
    :try_start_5f
    move-object/from16 v0, p0

    #@61
    iget-object v14, v0, Lcom/android/server/NativeDaemonConnector;->mOutputStream:Ljava/io/OutputStream;

    #@63
    if-nez v14, :cond_7a

    #@65
    .line 335
    new-instance v14, Lcom/android/server/NativeDaemonConnectorException;

    #@67
    const-string v16, "missing output stream"

    #@69
    move-object/from16 v0, v16

    #@6b
    invoke-direct {v14, v0}, Lcom/android/server/NativeDaemonConnectorException;-><init>(Ljava/lang/String;)V

    #@6e
    throw v14

    #@6f
    .line 352
    :catchall_6f
    move-exception v14

    #@70
    monitor-exit v15
    :try_end_71
    .catchall {:try_start_5f .. :try_end_71} :catchall_6f

    #@71
    throw v14

    #@72
    .line 328
    .end local v10           #sentCmd:Ljava/lang/String;
    :cond_72
    const-string v14, "SND -> { SKIP psk log }"

    #@74
    move-object/from16 v0, p0

    #@76
    invoke-direct {v0, v14}, Lcom/android/server/NativeDaemonConnector;->log(Ljava/lang/String;)V

    #@79
    goto :goto_52

    #@7a
    .line 339
    .restart local v10       #sentCmd:Ljava/lang/String;
    :cond_7a
    :try_start_7a
    sget-boolean v14, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@7c
    if-eqz v14, :cond_cb

    #@7e
    move-object/from16 v0, p0

    #@80
    iget-object v14, v0, Lcom/android/server/NativeDaemonConnector;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@82
    if-eqz v14, :cond_cb

    #@84
    move-object/from16 v0, p0

    #@86
    iget-object v14, v0, Lcom/android/server/NativeDaemonConnector;->mWifiServiceExt:Lcom/lge/wifi_iface/WifiServiceExtIface;

    #@88
    invoke-interface {v14, v3}, Lcom/lge/wifi_iface/WifiServiceExtIface;->isHotspotSSIDKSC5601(Ljava/lang/StringBuilder;)Z

    #@8b
    move-result v14

    #@8c
    if-eqz v14, :cond_cb

    #@8e
    .line 341
    move-object/from16 v0, p0

    #@90
    iget-object v14, v0, Lcom/android/server/NativeDaemonConnector;->mOutputStream:Ljava/io/OutputStream;

    #@92
    const-string v16, "KSC5601"

    #@94
    move-object/from16 v0, v16

    #@96
    invoke-virtual {v10, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    #@99
    move-result-object v16

    #@9a
    move-object/from16 v0, v16

    #@9c
    invoke-virtual {v14, v0}, Ljava/io/OutputStream;->write([B)V
    :try_end_9f
    .catchall {:try_start_7a .. :try_end_9f} :catchall_6f
    .catch Ljava/io/IOException; {:try_start_7a .. :try_end_9f} :catch_dd

    #@9f
    .line 352
    :goto_9f
    :try_start_9f
    monitor-exit v15
    :try_end_a0
    .catchall {:try_start_9f .. :try_end_a0} :catchall_6f

    #@a0
    .line 354
    const/4 v7, 0x0

    #@a1
    .line 356
    .local v7, event:Lcom/android/server/NativeDaemonEvent;
    :cond_a1
    move-object/from16 v0, p0

    #@a3
    iget-object v14, v0, Lcom/android/server/NativeDaemonConnector;->mResponseQueue:Lcom/android/server/NativeDaemonConnector$ResponseQueue;

    #@a5
    move/from16 v0, p1

    #@a7
    invoke-virtual {v14, v11, v0, v10}, Lcom/android/server/NativeDaemonConnector$ResponseQueue;->remove(IILjava/lang/String;)Lcom/android/server/NativeDaemonEvent;

    #@aa
    move-result-object v7

    #@ab
    .line 357
    if-nez v7, :cond_e8

    #@ad
    .line 358
    new-instance v14, Ljava/lang/StringBuilder;

    #@af
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@b2
    const-string v15, "timed-out waiting for response to "

    #@b4
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b7
    move-result-object v14

    #@b8
    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bb
    move-result-object v14

    #@bc
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bf
    move-result-object v14

    #@c0
    move-object/from16 v0, p0

    #@c2
    invoke-direct {v0, v14}, Lcom/android/server/NativeDaemonConnector;->loge(Ljava/lang/String;)V

    #@c5
    .line 359
    new-instance v14, Lcom/android/server/NativeDaemonConnector$NativeDaemonFailureException;

    #@c7
    invoke-direct {v14, v9, v7}, Lcom/android/server/NativeDaemonConnector$NativeDaemonFailureException;-><init>(Ljava/lang/String;Lcom/android/server/NativeDaemonEvent;)V

    #@ca
    throw v14

    #@cb
    .line 346
    .end local v7           #event:Lcom/android/server/NativeDaemonEvent;
    :cond_cb
    :try_start_cb
    move-object/from16 v0, p0

    #@cd
    iget-object v14, v0, Lcom/android/server/NativeDaemonConnector;->mOutputStream:Ljava/io/OutputStream;

    #@cf
    sget-object v16, Ljava/nio/charset/Charsets;->UTF_8:Ljava/nio/charset/Charset;

    #@d1
    move-object/from16 v0, v16

    #@d3
    invoke-virtual {v10, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    #@d6
    move-result-object v16

    #@d7
    move-object/from16 v0, v16

    #@d9
    invoke-virtual {v14, v0}, Ljava/io/OutputStream;->write([B)V
    :try_end_dc
    .catchall {:try_start_cb .. :try_end_dc} :catchall_6f
    .catch Ljava/io/IOException; {:try_start_cb .. :try_end_dc} :catch_dd

    #@dc
    goto :goto_9f

    #@dd
    .line 348
    :catch_dd
    move-exception v4

    #@de
    .line 349
    .local v4, e:Ljava/io/IOException;
    :try_start_de
    new-instance v14, Lcom/android/server/NativeDaemonConnectorException;

    #@e0
    const-string v16, "problem sending command"

    #@e2
    move-object/from16 v0, v16

    #@e4
    invoke-direct {v14, v0, v4}, Lcom/android/server/NativeDaemonConnectorException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@e7
    throw v14
    :try_end_e8
    .catchall {:try_start_de .. :try_end_e8} :catchall_6f

    #@e8
    .line 361
    .end local v4           #e:Ljava/io/IOException;
    .restart local v7       #event:Lcom/android/server/NativeDaemonEvent;
    :cond_e8
    new-instance v14, Ljava/lang/StringBuilder;

    #@ea
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@ed
    const-string v15, "RMV <- {"

    #@ef
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f2
    move-result-object v14

    #@f3
    invoke-virtual {v14, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@f6
    move-result-object v14

    #@f7
    const-string v15, "}"

    #@f9
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fc
    move-result-object v14

    #@fd
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@100
    move-result-object v14

    #@101
    move-object/from16 v0, p0

    #@103
    invoke-direct {v0, v14}, Lcom/android/server/NativeDaemonConnector;->log(Ljava/lang/String;)V

    #@106
    .line 362
    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@109
    .line 363
    invoke-virtual {v7}, Lcom/android/server/NativeDaemonEvent;->isClassContinue()Z

    #@10c
    move-result v14

    #@10d
    if-nez v14, :cond_a1

    #@10f
    .line 365
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@112
    move-result-wide v5

    #@113
    .line 366
    .local v5, endTime:J
    sub-long v14, v5, v12

    #@115
    const-wide/16 v16, 0x1f4

    #@117
    cmp-long v14, v14, v16

    #@119
    if-lez v14, :cond_145

    #@11b
    .line 367
    new-instance v14, Ljava/lang/StringBuilder;

    #@11d
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@120
    const-string v15, "NDC Command {"

    #@122
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@125
    move-result-object v14

    #@126
    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@129
    move-result-object v14

    #@12a
    const-string v15, "} took too long ("

    #@12c
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12f
    move-result-object v14

    #@130
    sub-long v15, v5, v12

    #@132
    invoke-virtual/range {v14 .. v16}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@135
    move-result-object v14

    #@136
    const-string v15, "ms)"

    #@138
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13b
    move-result-object v14

    #@13c
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13f
    move-result-object v14

    #@140
    move-object/from16 v0, p0

    #@142
    invoke-direct {v0, v14}, Lcom/android/server/NativeDaemonConnector;->loge(Ljava/lang/String;)V

    #@145
    .line 370
    :cond_145
    invoke-virtual {v7}, Lcom/android/server/NativeDaemonEvent;->isClassClientError()Z

    #@148
    move-result v14

    #@149
    if-eqz v14, :cond_151

    #@14b
    .line 371
    new-instance v14, Lcom/android/server/NativeDaemonConnector$NativeDaemonArgumentException;

    #@14d
    invoke-direct {v14, v9, v7}, Lcom/android/server/NativeDaemonConnector$NativeDaemonArgumentException;-><init>(Ljava/lang/String;Lcom/android/server/NativeDaemonEvent;)V

    #@150
    throw v14

    #@151
    .line 373
    :cond_151
    invoke-virtual {v7}, Lcom/android/server/NativeDaemonEvent;->isClassServerError()Z

    #@154
    move-result v14

    #@155
    if-eqz v14, :cond_15d

    #@157
    .line 374
    new-instance v14, Lcom/android/server/NativeDaemonConnector$NativeDaemonFailureException;

    #@159
    invoke-direct {v14, v9, v7}, Lcom/android/server/NativeDaemonConnector$NativeDaemonFailureException;-><init>(Ljava/lang/String;Lcom/android/server/NativeDaemonEvent;)V

    #@15c
    throw v14

    #@15d
    .line 377
    :cond_15d
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    #@160
    move-result v14

    #@161
    new-array v14, v14, [Lcom/android/server/NativeDaemonEvent;

    #@163
    invoke-virtual {v8, v14}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@166
    move-result-object v14

    #@167
    check-cast v14, [Lcom/android/server/NativeDaemonEvent;

    #@169
    return-object v14
.end method

.method public executeForList(Lcom/android/server/NativeDaemonConnector$Command;)[Lcom/android/server/NativeDaemonEvent;
    .registers 4
    .parameter "cmd"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/server/NativeDaemonConnectorException;
        }
    .end annotation

    #@0
    .prologue
    .line 284
    invoke-static {p1}, Lcom/android/server/NativeDaemonConnector$Command;->access$000(Lcom/android/server/NativeDaemonConnector$Command;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-static {p1}, Lcom/android/server/NativeDaemonConnector$Command;->access$100(Lcom/android/server/NativeDaemonConnector$Command;)Ljava/util/ArrayList;

    #@7
    move-result-object v1

    #@8
    invoke-virtual {v1}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    #@b
    move-result-object v1

    #@c
    invoke-virtual {p0, v0, v1}, Lcom/android/server/NativeDaemonConnector;->executeForList(Ljava/lang/String;[Ljava/lang/Object;)[Lcom/android/server/NativeDaemonEvent;

    #@f
    move-result-object v0

    #@10
    return-object v0
.end method

.method public varargs executeForList(Ljava/lang/String;[Ljava/lang/Object;)[Lcom/android/server/NativeDaemonEvent;
    .registers 4
    .parameter "cmd"
    .parameter "args"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/server/NativeDaemonConnectorException;
        }
    .end annotation

    #@0
    .prologue
    .line 299
    const v0, 0xea60

    #@3
    invoke-virtual {p0, v0, p1, p2}, Lcom/android/server/NativeDaemonConnector;->execute(ILjava/lang/String;[Ljava/lang/Object;)[Lcom/android/server/NativeDaemonEvent;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .registers 8
    .parameter "msg"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 113
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@3
    check-cast v1, Ljava/lang/String;

    #@5
    .line 115
    .local v1, event:Ljava/lang/String;
    :try_start_5
    iget-object v2, p0, Lcom/android/server/NativeDaemonConnector;->mCallbacks:Lcom/android/server/INativeDaemonConnectorCallbacks;

    #@7
    iget v3, p1, Landroid/os/Message;->what:I

    #@9
    invoke-static {v1}, Lcom/android/server/NativeDaemonEvent;->unescapeArgs(Ljava/lang/String;)[Ljava/lang/String;

    #@c
    move-result-object v4

    #@d
    invoke-interface {v2, v3, v1, v4}, Lcom/android/server/INativeDaemonConnectorCallbacks;->onEvent(ILjava/lang/String;[Ljava/lang/String;)Z

    #@10
    move-result v2

    #@11
    if-nez v2, :cond_22

    #@13
    .line 116
    const-string v2, "Unhandled event \'%s\'"

    #@15
    const/4 v3, 0x1

    #@16
    new-array v3, v3, [Ljava/lang/Object;

    #@18
    const/4 v4, 0x0

    #@19
    aput-object v1, v3, v4

    #@1b
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    invoke-direct {p0, v2}, Lcom/android/server/NativeDaemonConnector;->log(Ljava/lang/String;)V
    :try_end_22
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_22} :catch_23

    #@22
    .line 121
    :cond_22
    :goto_22
    return v5

    #@23
    .line 118
    :catch_23
    move-exception v0

    #@24
    .line 119
    .local v0, e:Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v3, "Error handling \'"

    #@2b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    const-string v3, "\': "

    #@35
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v2

    #@39
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v2

    #@3d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v2

    #@41
    invoke-direct {p0, v2}, Lcom/android/server/NativeDaemonConnector;->loge(Ljava/lang/String;)V

    #@44
    goto :goto_22
.end method

.method public monitor()V
    .registers 3

    #@0
    .prologue
    .line 493
    iget-object v1, p0, Lcom/android/server/NativeDaemonConnector;->mDaemonLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    :try_start_3
    monitor-exit v1

    #@4
    .line 494
    return-void

    #@5
    .line 493
    :catchall_5
    move-exception v0

    #@6
    monitor-exit v1
    :try_end_7
    .catchall {:try_start_3 .. :try_end_7} :catchall_5

    #@7
    throw v0
.end method

.method public run()V
    .registers 5

    #@0
    .prologue
    .line 97
    new-instance v1, Landroid/os/HandlerThread;

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    iget-object v3, p0, Lcom/android/server/NativeDaemonConnector;->TAG:Ljava/lang/String;

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    const-string v3, ".CallbackHandler"

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v2

    #@17
    invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    #@1a
    .line 98
    .local v1, thread:Landroid/os/HandlerThread;
    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    #@1d
    .line 99
    new-instance v2, Landroid/os/Handler;

    #@1f
    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@22
    move-result-object v3

    #@23
    invoke-direct {v2, v3, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    #@26
    iput-object v2, p0, Lcom/android/server/NativeDaemonConnector;->mCallbackHandler:Landroid/os/Handler;

    #@28
    .line 103
    :goto_28
    :try_start_28
    invoke-direct {p0}, Lcom/android/server/NativeDaemonConnector;->listenToSocket()V
    :try_end_2b
    .catch Ljava/lang/Exception; {:try_start_28 .. :try_end_2b} :catch_2c

    #@2b
    goto :goto_28

    #@2c
    .line 104
    :catch_2c
    move-exception v0

    #@2d
    .line 105
    .local v0, e:Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    const-string v3, "Error in NativeDaemonConnector: "

    #@34
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v2

    #@38
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v2

    #@3c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v2

    #@40
    invoke-direct {p0, v2}, Lcom/android/server/NativeDaemonConnector;->loge(Ljava/lang/String;)V

    #@43
    .line 106
    const-wide/16 v2, 0x1388

    #@45
    invoke-static {v2, v3}, Landroid/os/SystemClock;->sleep(J)V

    #@48
    goto :goto_28
.end method
