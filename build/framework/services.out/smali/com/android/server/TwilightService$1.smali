.class Lcom/android/server/TwilightService$1;
.super Landroid/content/BroadcastReceiver;
.source "TwilightService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/TwilightService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/TwilightService;


# direct methods
.method constructor <init>(Lcom/android/server/TwilightService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 527
    iput-object p1, p0, Lcom/android/server/TwilightService$1;->this$0:Lcom/android/server/TwilightService;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 5
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 530
    const-string v0, "android.intent.action.AIRPLANE_MODE"

    #@2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_1f

    #@c
    const-string v0, "state"

    #@e
    const/4 v1, 0x0

    #@f
    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@12
    move-result v0

    #@13
    if-nez v0, :cond_1f

    #@15
    .line 533
    iget-object v0, p0, Lcom/android/server/TwilightService$1;->this$0:Lcom/android/server/TwilightService;

    #@17
    invoke-static {v0}, Lcom/android/server/TwilightService;->access$800(Lcom/android/server/TwilightService;)Lcom/android/server/TwilightService$LocationHandler;

    #@1a
    move-result-object v0

    #@1b
    invoke-virtual {v0}, Lcom/android/server/TwilightService$LocationHandler;->requestLocationUpdate()V

    #@1e
    .line 539
    :goto_1e
    return-void

    #@1f
    .line 538
    :cond_1f
    iget-object v0, p0, Lcom/android/server/TwilightService$1;->this$0:Lcom/android/server/TwilightService;

    #@21
    invoke-static {v0}, Lcom/android/server/TwilightService;->access$800(Lcom/android/server/TwilightService;)Lcom/android/server/TwilightService$LocationHandler;

    #@24
    move-result-object v0

    #@25
    invoke-virtual {v0}, Lcom/android/server/TwilightService$LocationHandler;->requestTwilightUpdate()V

    #@28
    goto :goto_1e
.end method
