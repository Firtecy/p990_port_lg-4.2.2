.class public Lcom/android/server/DevicePolicyManagerService;
.super Landroid/app/admin/IDevicePolicyManager$Stub;
.source "DevicePolicyManagerService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;,
        Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    }
.end annotation


# static fields
.field protected static final ACTION_EXPIRED_PASSWORD_NOTIFICATION:Ljava/lang/String; = "com.android.server.ACTION_EXPIRED_PASSWORD_NOTIFICATION"

.field protected static final ACTION_MAMXIMUM_TIME_TOLOCK:Ljava/lang/String; = "com.lge.mdm.ACTION_MAMXIMUM_TIME_TOLOCK"

.field private static final DBG:Z = false

.field private static final DEVICE_POLICIES_XML:Ljava/lang/String; = "device_policies.xml"

.field private static final EXPIRATION_GRACE_PERIOD_MS:J = 0x19bfcc00L

.field private static final MS_PER_DAY:J = 0x5265c00L

.field private static final REQUEST_EXPIRE_PASSWORD:I = 0x15c3

.field public static final SYSTEM_PROP_DISABLE_CAMERA:Ljava/lang/String; = "sys.secpolicy.camera.disabled"

.field private static final TAG:Ljava/lang/String; = "DevicePolicyManagerService"


# instance fields
.field final mContext:Landroid/content/Context;

.field mHandler:Landroid/os/Handler;

.field mIPowerManager:Landroid/os/IPowerManager;

.field mIWindowManager:Landroid/view/IWindowManager;

.field mReceiver:Landroid/content/BroadcastReceiver;

.field final mUserData:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;",
            ">;"
        }
    .end annotation
.end field

.field final mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 8
    .parameter "context"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 524
    invoke-direct {p0}, Landroid/app/admin/IDevicePolicyManager$Stub;-><init>()V

    #@4
    .line 144
    new-instance v0, Landroid/util/SparseArray;

    #@6
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    #@9
    iput-object v0, p0, Lcom/android/server/DevicePolicyManagerService;->mUserData:Landroid/util/SparseArray;

    #@b
    .line 146
    new-instance v0, Landroid/os/Handler;

    #@d
    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    #@10
    iput-object v0, p0, Lcom/android/server/DevicePolicyManagerService;->mHandler:Landroid/os/Handler;

    #@12
    .line 148
    new-instance v0, Lcom/android/server/DevicePolicyManagerService$1;

    #@14
    invoke-direct {v0, p0}, Lcom/android/server/DevicePolicyManagerService$1;-><init>(Lcom/android/server/DevicePolicyManagerService;)V

    #@17
    iput-object v0, p0, Lcom/android/server/DevicePolicyManagerService;->mReceiver:Landroid/content/BroadcastReceiver;

    #@19
    .line 525
    iput-object p1, p0, Lcom/android/server/DevicePolicyManagerService;->mContext:Landroid/content/Context;

    #@1b
    .line 526
    const-string v0, "power"

    #@1d
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@20
    move-result-object v0

    #@21
    check-cast v0, Landroid/os/PowerManager;

    #@23
    const/4 v1, 0x1

    #@24
    const-string v2, "DPM"

    #@26
    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@29
    move-result-object v0

    #@2a
    iput-object v0, p0, Lcom/android/server/DevicePolicyManagerService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2c
    .line 528
    new-instance v3, Landroid/content/IntentFilter;

    #@2e
    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    #@31
    .line 529
    .local v3, filter:Landroid/content/IntentFilter;
    const-string v0, "android.intent.action.BOOT_COMPLETED"

    #@33
    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@36
    .line 530
    const-string v0, "com.android.server.ACTION_EXPIRED_PASSWORD_NOTIFICATION"

    #@38
    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@3b
    .line 531
    const-string v0, "android.intent.action.USER_REMOVED"

    #@3d
    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@40
    .line 532
    const-string v0, "android.intent.action.USER_STARTED"

    #@42
    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@45
    .line 533
    iget-object v1, p0, Lcom/android/server/DevicePolicyManagerService;->mReceiver:Landroid/content/BroadcastReceiver;

    #@47
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@49
    iget-object v5, p0, Lcom/android/server/DevicePolicyManagerService;->mHandler:Landroid/os/Handler;

    #@4b
    move-object v0, p1

    #@4c
    invoke-virtual/range {v0 .. v5}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@4f
    .line 534
    new-instance v3, Landroid/content/IntentFilter;

    #@51
    .end local v3           #filter:Landroid/content/IntentFilter;
    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    #@54
    .line 535
    .restart local v3       #filter:Landroid/content/IntentFilter;
    const-string v0, "android.intent.action.PACKAGE_CHANGED"

    #@56
    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@59
    .line 536
    const-string v0, "android.intent.action.PACKAGE_REMOVED"

    #@5b
    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@5e
    .line 537
    const-string v0, "android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE"

    #@60
    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@63
    .line 538
    const-string v0, "package"

    #@65
    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    #@68
    .line 539
    iget-object v1, p0, Lcom/android/server/DevicePolicyManagerService;->mReceiver:Landroid/content/BroadcastReceiver;

    #@6a
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@6c
    iget-object v5, p0, Lcom/android/server/DevicePolicyManagerService;->mHandler:Landroid/os/Handler;

    #@6e
    move-object v0, p1

    #@6f
    invoke-virtual/range {v0 .. v5}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@72
    .line 540
    return-void
.end method

.method static synthetic access$000(Lcom/android/server/DevicePolicyManagerService;Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 93
    invoke-direct {p0, p1}, Lcom/android/server/DevicePolicyManagerService;->handlePasswordExpirationNotification(Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;)V

    #@3
    return-void
.end method

.method static synthetic access$100(Lcom/android/server/DevicePolicyManagerService;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 93
    invoke-direct {p0, p1}, Lcom/android/server/DevicePolicyManagerService;->handlePackagesChanged(I)V

    #@3
    return-void
.end method

.method static synthetic access$200(Lcom/android/server/DevicePolicyManagerService;Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 93
    invoke-direct {p0, p1}, Lcom/android/server/DevicePolicyManagerService;->resetGlobalProxyLocked(Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;)V

    #@3
    return-void
.end method

.method static synthetic access$300(Lcom/android/server/DevicePolicyManagerService;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 93
    invoke-direct {p0, p1}, Lcom/android/server/DevicePolicyManagerService;->saveSettingsLocked(I)V

    #@3
    return-void
.end method

.method private enforceCrossUserPermission(I)V
    .registers 6
    .parameter "userHandle"

    #@0
    .prologue
    .line 2491
    if-gez p1, :cond_1b

    #@2
    .line 2492
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@4
    new-instance v2, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v3, "Invalid userId "

    #@b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v2

    #@17
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v1

    #@1b
    .line 2494
    :cond_1b
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@1e
    move-result v0

    #@1f
    .line 2495
    .local v0, callingUid:I
    invoke-static {v0}, Landroid/os/UserHandle;->getUserId(I)I

    #@22
    move-result v1

    #@23
    if-ne p1, v1, :cond_26

    #@25
    .line 2501
    :cond_25
    :goto_25
    return-void

    #@26
    .line 2496
    :cond_26
    const/16 v1, 0x3e8

    #@28
    if-eq v0, v1, :cond_25

    #@2a
    if-eqz v0, :cond_25

    #@2c
    .line 2497
    iget-object v1, p0, Lcom/android/server/DevicePolicyManagerService;->mContext:Landroid/content/Context;

    #@2e
    const-string v2, "android.permission.INTERACT_ACROSS_USERS_FULL"

    #@30
    const-string v3, "Must be system or have INTERACT_ACROSS_USERS_FULL permission"

    #@32
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@35
    goto :goto_25
.end method

.method private getEncryptionStatus()I
    .registers 4

    #@0
    .prologue
    .line 2382
    const-string v1, "ro.crypto.state"

    #@2
    const-string v2, "unsupported"

    #@4
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    .line 2383
    .local v0, status:Ljava/lang/String;
    const-string v1, "encrypted"

    #@a
    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_12

    #@10
    .line 2384
    const/4 v1, 0x3

    #@11
    .line 2388
    :goto_11
    return v1

    #@12
    .line 2385
    :cond_12
    const-string v1, "unencrypted"

    #@14
    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@17
    move-result v1

    #@18
    if-eqz v1, :cond_1c

    #@1a
    .line 2386
    const/4 v1, 0x1

    #@1b
    goto :goto_11

    #@1c
    .line 2388
    :cond_1c
    const/4 v1, 0x0

    #@1d
    goto :goto_11
.end method

.method private getIPowerManager()Landroid/os/IPowerManager;
    .registers 3

    #@0
    .prologue
    .line 618
    iget-object v1, p0, Lcom/android/server/DevicePolicyManagerService;->mIPowerManager:Landroid/os/IPowerManager;

    #@2
    if-nez v1, :cond_10

    #@4
    .line 619
    const-string v1, "power"

    #@6
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@9
    move-result-object v0

    #@a
    .line 620
    .local v0, b:Landroid/os/IBinder;
    invoke-static {v0}, Landroid/os/IPowerManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/IPowerManager;

    #@d
    move-result-object v1

    #@e
    iput-object v1, p0, Lcom/android/server/DevicePolicyManagerService;->mIPowerManager:Landroid/os/IPowerManager;

    #@10
    .line 622
    .end local v0           #b:Landroid/os/IBinder;
    :cond_10
    iget-object v1, p0, Lcom/android/server/DevicePolicyManagerService;->mIPowerManager:Landroid/os/IPowerManager;

    #@12
    return-object v1
.end method

.method private getPasswordExpirationLocked(Landroid/content/ComponentName;I)J
    .registers 13
    .parameter "who"
    .parameter "userHandle"

    #@0
    .prologue
    const-wide/16 v6, 0x0

    #@2
    .line 1357
    if-eqz p1, :cond_d

    #@4
    .line 1358
    invoke-virtual {p0, p1, p2}, Lcom/android/server/DevicePolicyManagerService;->getActiveAdminUncheckedLocked(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@7
    move-result-object v1

    #@8
    .line 1359
    .local v1, admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    if-eqz v1, :cond_c

    #@a
    iget-wide v6, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->passwordExpirationDate:J

    #@c
    .line 1372
    .end local v1           #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_c
    :goto_c
    return-wide v6

    #@d
    .line 1362
    :cond_d
    const-wide/16 v4, 0x0

    #@f
    .line 1363
    .local v4, timeout:J
    invoke-virtual {p0, p2}, Lcom/android/server/DevicePolicyManagerService;->getUserData(I)Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;

    #@12
    move-result-object v3

    #@13
    .line 1364
    .local v3, policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    iget-object v8, v3, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@15
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    #@18
    move-result v0

    #@19
    .line 1365
    .local v0, N:I
    const/4 v2, 0x0

    #@1a
    .local v2, i:I
    :goto_1a
    if-ge v2, v0, :cond_39

    #@1c
    .line 1366
    iget-object v8, v3, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@1e
    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@21
    move-result-object v1

    #@22
    check-cast v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@24
    .line 1367
    .restart local v1       #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    cmp-long v8, v4, v6

    #@26
    if-eqz v8, :cond_34

    #@28
    iget-wide v8, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->passwordExpirationDate:J

    #@2a
    cmp-long v8, v8, v6

    #@2c
    if-eqz v8, :cond_36

    #@2e
    iget-wide v8, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->passwordExpirationDate:J

    #@30
    cmp-long v8, v4, v8

    #@32
    if-lez v8, :cond_36

    #@34
    .line 1369
    :cond_34
    iget-wide v4, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->passwordExpirationDate:J

    #@36
    .line 1365
    :cond_36
    add-int/lit8 v2, v2, 0x1

    #@38
    goto :goto_1a

    #@39
    .end local v1           #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_39
    move-wide v6, v4

    #@3a
    .line 1372
    goto :goto_c
.end method

.method private getWindowManager()Landroid/view/IWindowManager;
    .registers 3

    #@0
    .prologue
    .line 626
    iget-object v1, p0, Lcom/android/server/DevicePolicyManagerService;->mIWindowManager:Landroid/view/IWindowManager;

    #@2
    if-nez v1, :cond_10

    #@4
    .line 627
    const-string v1, "window"

    #@6
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@9
    move-result-object v0

    #@a
    .line 628
    .local v0, b:Landroid/os/IBinder;
    invoke-static {v0}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    #@d
    move-result-object v1

    #@e
    iput-object v1, p0, Lcom/android/server/DevicePolicyManagerService;->mIWindowManager:Landroid/view/IWindowManager;

    #@10
    .line 630
    .end local v0           #b:Landroid/os/IBinder;
    :cond_10
    iget-object v1, p0, Lcom/android/server/DevicePolicyManagerService;->mIWindowManager:Landroid/view/IWindowManager;

    #@12
    return-object v1
.end method

.method private handlePackagesChanged(I)V
    .registers 9
    .parameter "userHandle"

    #@0
    .prologue
    .line 498
    const/4 v4, 0x0

    #@1
    .line 500
    .local v4, removed:Z
    invoke-virtual {p0, p1}, Lcom/android/server/DevicePolicyManagerService;->getUserData(I)Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;

    #@4
    move-result-object v3

    #@5
    .line 501
    .local v3, policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    #@8
    move-result-object v2

    #@9
    .line 502
    .local v2, pm:Landroid/content/pm/IPackageManager;
    iget-object v5, v3, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@e
    move-result v5

    #@f
    add-int/lit8 v1, v5, -0x1

    #@11
    .local v1, i:I
    :goto_11
    if-ltz v1, :cond_3e

    #@13
    .line 503
    iget-object v5, v3, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@15
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@18
    move-result-object v0

    #@19
    check-cast v0, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@1b
    .line 505
    .local v0, aa:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :try_start_1b
    iget-object v5, v0, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->info:Landroid/app/admin/DeviceAdminInfo;

    #@1d
    invoke-virtual {v5}, Landroid/app/admin/DeviceAdminInfo;->getPackageName()Ljava/lang/String;

    #@20
    move-result-object v5

    #@21
    const/4 v6, 0x0

    #@22
    invoke-interface {v2, v5, v6, p1}, Landroid/content/pm/IPackageManager;->getPackageInfo(Ljava/lang/String;II)Landroid/content/pm/PackageInfo;

    #@25
    move-result-object v5

    #@26
    if-eqz v5, :cond_35

    #@28
    iget-object v5, v0, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->info:Landroid/app/admin/DeviceAdminInfo;

    #@2a
    invoke-virtual {v5}, Landroid/app/admin/DeviceAdminInfo;->getComponent()Landroid/content/ComponentName;

    #@2d
    move-result-object v5

    #@2e
    const/4 v6, 0x0

    #@2f
    invoke-interface {v2, v5, v6, p1}, Landroid/content/pm/IPackageManager;->getReceiverInfo(Landroid/content/ComponentName;II)Landroid/content/pm/ActivityInfo;

    #@32
    move-result-object v5

    #@33
    if-nez v5, :cond_3b

    #@35
    .line 507
    :cond_35
    const/4 v4, 0x1

    #@36
    .line 508
    iget-object v5, v3, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@38
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;
    :try_end_3b
    .catch Landroid/os/RemoteException; {:try_start_1b .. :try_end_3b} :catch_4c

    #@3b
    .line 502
    :cond_3b
    :goto_3b
    add-int/lit8 v1, v1, -0x1

    #@3d
    goto :goto_11

    #@3e
    .line 514
    .end local v0           #aa:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_3e
    if-eqz v4, :cond_4b

    #@40
    .line 515
    invoke-virtual {p0, v3}, Lcom/android/server/DevicePolicyManagerService;->validatePasswordOwnerLocked(Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;)V

    #@43
    .line 516
    invoke-virtual {p0, v3}, Lcom/android/server/DevicePolicyManagerService;->syncDeviceCapabilitiesLocked(Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;)V

    #@46
    .line 517
    iget v5, v3, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mUserHandle:I

    #@48
    invoke-direct {p0, v5}, Lcom/android/server/DevicePolicyManagerService;->saveSettingsLocked(I)V

    #@4b
    .line 519
    :cond_4b
    return-void

    #@4c
    .line 510
    .restart local v0       #aa:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :catch_4c
    move-exception v5

    #@4d
    goto :goto_3b
.end method

.method private handlePasswordExpirationNotification(Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;)V
    .registers 13
    .parameter "policy"

    #@0
    .prologue
    const-wide/16 v9, 0x0

    #@2
    .line 1054
    monitor-enter p0

    #@3
    .line 1055
    :try_start_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@6
    move-result-wide v3

    #@7
    .line 1056
    .local v3, now:J
    iget-object v5, p1, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@9
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@c
    move-result v0

    #@d
    .line 1057
    .local v0, N:I
    if-gtz v0, :cond_11

    #@f
    .line 1058
    monitor-exit p0

    #@10
    .line 1071
    :goto_10
    return-void

    #@11
    .line 1060
    :cond_11
    const/4 v2, 0x0

    #@12
    .local v2, i:I
    :goto_12
    if-ge v2, v0, :cond_43

    #@14
    .line 1061
    iget-object v5, p1, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@16
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@19
    move-result-object v1

    #@1a
    check-cast v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@1c
    .line 1062
    .local v1, admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    iget-object v5, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->info:Landroid/app/admin/DeviceAdminInfo;

    #@1e
    const/4 v6, 0x6

    #@1f
    invoke-virtual {v5, v6}, Landroid/app/admin/DeviceAdminInfo;->usesPolicy(I)Z

    #@22
    move-result v5

    #@23
    if-eqz v5, :cond_40

    #@25
    iget-wide v5, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->passwordExpirationTimeout:J

    #@27
    cmp-long v5, v5, v9

    #@29
    if-lez v5, :cond_40

    #@2b
    iget-wide v5, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->passwordExpirationDate:J

    #@2d
    cmp-long v5, v5, v9

    #@2f
    if-lez v5, :cond_40

    #@31
    iget-wide v5, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->passwordExpirationDate:J

    #@33
    const-wide/32 v7, 0x19bfcc00

    #@36
    sub-long/2addr v5, v7

    #@37
    cmp-long v5, v3, v5

    #@39
    if-ltz v5, :cond_40

    #@3b
    .line 1066
    const-string v5, "android.app.action.ACTION_PASSWORD_EXPIRING"

    #@3d
    invoke-virtual {p0, v1, v5}, Lcom/android/server/DevicePolicyManagerService;->sendAdminCommandLocked(Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;Ljava/lang/String;)V

    #@40
    .line 1060
    :cond_40
    add-int/lit8 v2, v2, 0x1

    #@42
    goto :goto_12

    #@43
    .line 1069
    .end local v1           #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_43
    iget-object v5, p0, Lcom/android/server/DevicePolicyManagerService;->mContext:Landroid/content/Context;

    #@45
    invoke-virtual {p0, v5, p1}, Lcom/android/server/DevicePolicyManagerService;->setExpirationAlarmCheckLocked(Landroid/content/Context;Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;)V

    #@48
    .line 1070
    monitor-exit p0

    #@49
    goto :goto_10

    #@4a
    .end local v0           #N:I
    .end local v2           #i:I
    .end local v3           #now:J
    :catchall_4a
    move-exception v5

    #@4b
    monitor-exit p0
    :try_end_4c
    .catchall {:try_start_3 .. :try_end_4c} :catchall_4a

    #@4c
    throw v5
.end method

.method private isEncryptionSupported()Z
    .registers 2

    #@0
    .prologue
    .line 2372
    invoke-direct {p0}, Lcom/android/server/DevicePolicyManagerService;->getEncryptionStatus()I

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method private isExtStorageEncrypted()Z
    .registers 3

    #@0
    .prologue
    .line 1932
    const-string v1, "vold.decrypt"

    #@2
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 1933
    .local v0, state:Ljava/lang/String;
    const-string v1, ""

    #@8
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v1

    #@c
    if-nez v1, :cond_10

    #@e
    const/4 v1, 0x1

    #@f
    :goto_f
    return v1

    #@10
    :cond_10
    const/4 v1, 0x0

    #@11
    goto :goto_f
.end method

.method private loadSettingsLocked(Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;I)V
    .registers 21
    .parameter "policy"
    .parameter "userHandle"

    #@0
    .prologue
    .line 867
    invoke-static/range {p2 .. p2}, Lcom/android/server/DevicePolicyManagerService;->makeJournaledFile(I)Lcom/android/internal/util/JournaledFile;

    #@3
    move-result-object v6

    #@4
    .line 868
    .local v6, journal:Lcom/android/internal/util/JournaledFile;
    const/4 v10, 0x0

    #@5
    .line 869
    .local v10, stream:Ljava/io/FileInputStream;
    invoke-virtual {v6}, Lcom/android/internal/util/JournaledFile;->chooseForRead()Ljava/io/File;

    #@8
    move-result-object v5

    #@9
    .line 871
    .local v5, file:Ljava/io/File;
    :try_start_9
    new-instance v11, Ljava/io/FileInputStream;

    #@b
    invoke-direct {v11, v5}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_e
    .catch Ljava/lang/NullPointerException; {:try_start_9 .. :try_end_e} :catch_320
    .catch Ljava/lang/NumberFormatException; {:try_start_9 .. :try_end_e} :catch_31d
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_9 .. :try_end_e} :catch_31a
    .catch Ljava/io/FileNotFoundException; {:try_start_9 .. :try_end_e} :catch_317
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_e} :catch_315
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_9 .. :try_end_e} :catch_313

    #@e
    .line 872
    .end local v10           #stream:Ljava/io/FileInputStream;
    .local v11, stream:Ljava/io/FileInputStream;
    :try_start_e
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    #@11
    move-result-object v9

    #@12
    .line 873
    .local v9, parser:Lorg/xmlpull/v1/XmlPullParser;
    const/4 v15, 0x0

    #@13
    invoke-interface {v9, v11, v15}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    #@16
    .line 877
    :cond_16
    invoke-interface {v9}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@19
    move-result v13

    #@1a
    .local v13, type:I
    const/4 v15, 0x1

    #@1b
    if-eq v13, v15, :cond_20

    #@1d
    const/4 v15, 0x2

    #@1e
    if-ne v13, v15, :cond_16

    #@20
    .line 879
    :cond_20
    invoke-interface {v9}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@23
    move-result-object v12

    #@24
    .line 880
    .local v12, tag:Ljava/lang/String;
    const-string v15, "policies"

    #@26
    invoke-virtual {v15, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@29
    move-result v15

    #@2a
    if-nez v15, :cond_f1

    #@2c
    .line 881
    new-instance v15, Lorg/xmlpull/v1/XmlPullParserException;

    #@2e
    new-instance v16, Ljava/lang/StringBuilder;

    #@30
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@33
    const-string v17, "Settings do not start with policies tag: found "

    #@35
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v16

    #@39
    move-object/from16 v0, v16

    #@3b
    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v16

    #@3f
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v16

    #@43
    invoke-direct/range {v15 .. v16}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@46
    throw v15
    :try_end_47
    .catch Ljava/lang/NullPointerException; {:try_start_e .. :try_end_47} :catch_47
    .catch Ljava/lang/NumberFormatException; {:try_start_e .. :try_end_47} :catch_171
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_e .. :try_end_47} :catch_1b9
    .catch Ljava/io/FileNotFoundException; {:try_start_e .. :try_end_47} :catch_201
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_47} :catch_29a
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_e .. :try_end_47} :catch_2e3

    #@47
    .line 943
    .end local v9           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v12           #tag:Ljava/lang/String;
    .end local v13           #type:I
    :catch_47
    move-exception v4

    #@48
    move-object v10, v11

    #@49
    .line 944
    .end local v11           #stream:Ljava/io/FileInputStream;
    .local v4, e:Ljava/lang/NullPointerException;
    .restart local v10       #stream:Ljava/io/FileInputStream;
    :goto_49
    const-string v15, "DevicePolicyManagerService"

    #@4b
    new-instance v16, Ljava/lang/StringBuilder;

    #@4d
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@50
    const-string v17, "failed parsing "

    #@52
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v16

    #@56
    move-object/from16 v0, v16

    #@58
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v16

    #@5c
    const-string v17, " "

    #@5e
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v16

    #@62
    move-object/from16 v0, v16

    #@64
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v16

    #@68
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6b
    move-result-object v16

    #@6c
    invoke-static/range {v15 .. v16}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@6f
    .line 957
    .end local v4           #e:Ljava/lang/NullPointerException;
    :goto_6f
    if-eqz v10, :cond_74

    #@71
    .line 958
    :try_start_71
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V
    :try_end_74
    .catch Ljava/io/IOException; {:try_start_71 .. :try_end_74} :catch_310

    #@74
    .line 968
    :cond_74
    :goto_74
    new-instance v14, Lcom/android/internal/widget/LockPatternUtils;

    #@76
    move-object/from16 v0, p0

    #@78
    iget-object v15, v0, Lcom/android/server/DevicePolicyManagerService;->mContext:Landroid/content/Context;

    #@7a
    invoke-direct {v14, v15}, Lcom/android/internal/widget/LockPatternUtils;-><init>(Landroid/content/Context;)V

    #@7d
    .line 969
    .local v14, utils:Lcom/android/internal/widget/LockPatternUtils;
    invoke-virtual {v14}, Lcom/android/internal/widget/LockPatternUtils;->getActivePasswordQuality()I

    #@80
    move-result v15

    #@81
    move-object/from16 v0, p1

    #@83
    iget v0, v0, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordQuality:I

    #@85
    move/from16 v16, v0

    #@87
    move/from16 v0, v16

    #@89
    if-ge v15, v0, :cond_e7

    #@8b
    .line 970
    const-string v15, "DevicePolicyManagerService"

    #@8d
    new-instance v16, Ljava/lang/StringBuilder;

    #@8f
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@92
    const-string v17, "Active password quality 0x"

    #@94
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@97
    move-result-object v16

    #@98
    move-object/from16 v0, p1

    #@9a
    iget v0, v0, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordQuality:I

    #@9c
    move/from16 v17, v0

    #@9e
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@a1
    move-result-object v17

    #@a2
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v16

    #@a6
    const-string v17, " does not match actual quality 0x"

    #@a8
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v16

    #@ac
    invoke-virtual {v14}, Lcom/android/internal/widget/LockPatternUtils;->getActivePasswordQuality()I

    #@af
    move-result v17

    #@b0
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@b3
    move-result-object v17

    #@b4
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b7
    move-result-object v16

    #@b8
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bb
    move-result-object v16

    #@bc
    invoke-static/range {v15 .. v16}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@bf
    .line 974
    const/4 v15, 0x0

    #@c0
    move-object/from16 v0, p1

    #@c2
    iput v15, v0, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordQuality:I

    #@c4
    .line 975
    const/4 v15, 0x0

    #@c5
    move-object/from16 v0, p1

    #@c7
    iput v15, v0, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordLength:I

    #@c9
    .line 976
    const/4 v15, 0x0

    #@ca
    move-object/from16 v0, p1

    #@cc
    iput v15, v0, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordUpperCase:I

    #@ce
    .line 977
    const/4 v15, 0x0

    #@cf
    move-object/from16 v0, p1

    #@d1
    iput v15, v0, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordLowerCase:I

    #@d3
    .line 978
    const/4 v15, 0x0

    #@d4
    move-object/from16 v0, p1

    #@d6
    iput v15, v0, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordLetters:I

    #@d8
    .line 979
    const/4 v15, 0x0

    #@d9
    move-object/from16 v0, p1

    #@db
    iput v15, v0, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordNumeric:I

    #@dd
    .line 980
    const/4 v15, 0x0

    #@de
    move-object/from16 v0, p1

    #@e0
    iput v15, v0, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordSymbols:I

    #@e2
    .line 981
    const/4 v15, 0x0

    #@e3
    move-object/from16 v0, p1

    #@e5
    iput v15, v0, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordNonLetter:I

    #@e7
    .line 984
    :cond_e7
    invoke-virtual/range {p0 .. p1}, Lcom/android/server/DevicePolicyManagerService;->validatePasswordOwnerLocked(Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;)V

    #@ea
    .line 985
    invoke-virtual/range {p0 .. p1}, Lcom/android/server/DevicePolicyManagerService;->syncDeviceCapabilitiesLocked(Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;)V

    #@ed
    .line 986
    invoke-virtual/range {p0 .. p1}, Lcom/android/server/DevicePolicyManagerService;->updateMaximumTimeToLockLocked(Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;)V

    #@f0
    .line 987
    return-void

    #@f1
    .line 884
    .end local v10           #stream:Ljava/io/FileInputStream;
    .end local v14           #utils:Lcom/android/internal/widget/LockPatternUtils;
    .restart local v9       #parser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v11       #stream:Ljava/io/FileInputStream;
    .restart local v12       #tag:Ljava/lang/String;
    .restart local v13       #type:I
    :cond_f1
    :try_start_f1
    invoke-interface {v9}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@f4
    move-result v13

    #@f5
    .line 885
    invoke-interface {v9}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@f8
    move-result v8

    #@f9
    .line 887
    .local v8, outerDepth:I
    :cond_f9
    :goto_f9
    invoke-interface {v9}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@fc
    move-result v13

    #@fd
    const/4 v15, 0x1

    #@fe
    if-eq v13, v15, :cond_30d

    #@100
    const/4 v15, 0x3

    #@101
    if-ne v13, v15, :cond_109

    #@103
    invoke-interface {v9}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@106
    move-result v15

    #@107
    if-le v15, v8, :cond_30d

    #@109
    .line 888
    :cond_109
    const/4 v15, 0x3

    #@10a
    if-eq v13, v15, :cond_f9

    #@10c
    const/4 v15, 0x4

    #@10d
    if-eq v13, v15, :cond_f9

    #@10f
    .line 891
    invoke-interface {v9}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@112
    move-result-object v12

    #@113
    .line 892
    const-string v15, "admin"

    #@115
    invoke-virtual {v15, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@118
    move-result v15

    #@119
    if-eqz v15, :cond_19b

    #@11b
    .line 893
    const/4 v15, 0x0

    #@11c
    const-string v16, "name"

    #@11e
    move-object/from16 v0, v16

    #@120
    invoke-interface {v9, v15, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_123
    .catch Ljava/lang/NullPointerException; {:try_start_f1 .. :try_end_123} :catch_47
    .catch Ljava/lang/NumberFormatException; {:try_start_f1 .. :try_end_123} :catch_171
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_f1 .. :try_end_123} :catch_1b9
    .catch Ljava/io/FileNotFoundException; {:try_start_f1 .. :try_end_123} :catch_201
    .catch Ljava/io/IOException; {:try_start_f1 .. :try_end_123} :catch_29a
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_f1 .. :try_end_123} :catch_2e3

    #@123
    move-result-object v7

    #@124
    .line 895
    .local v7, name:Ljava/lang/String;
    :try_start_124
    invoke-static {v7}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    #@127
    move-result-object v15

    #@128
    move-object/from16 v0, p0

    #@12a
    move/from16 v1, p2

    #@12c
    invoke-virtual {v0, v15, v1}, Lcom/android/server/DevicePolicyManagerService;->findAdmin(Landroid/content/ComponentName;I)Landroid/app/admin/DeviceAdminInfo;

    #@12f
    move-result-object v3

    #@130
    .line 903
    .local v3, dai:Landroid/app/admin/DeviceAdminInfo;
    if-eqz v3, :cond_f9

    #@132
    .line 904
    new-instance v2, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@134
    invoke-direct {v2, v3}, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;-><init>(Landroid/app/admin/DeviceAdminInfo;)V

    #@137
    .line 905
    .local v2, ap:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    invoke-virtual {v2, v9}, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->readFromXml(Lorg/xmlpull/v1/XmlPullParser;)V

    #@13a
    .line 906
    move-object/from16 v0, p1

    #@13c
    iget-object v15, v0, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminMap:Ljava/util/HashMap;

    #@13e
    iget-object v0, v2, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->info:Landroid/app/admin/DeviceAdminInfo;

    #@140
    move-object/from16 v16, v0

    #@142
    invoke-virtual/range {v16 .. v16}, Landroid/app/admin/DeviceAdminInfo;->getComponent()Landroid/content/ComponentName;

    #@145
    move-result-object v16

    #@146
    move-object/from16 v0, v16

    #@148
    invoke-virtual {v15, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@14b
    .line 907
    move-object/from16 v0, p1

    #@14d
    iget-object v15, v0, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@14f
    invoke-virtual {v15, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_152
    .catch Ljava/lang/RuntimeException; {:try_start_124 .. :try_end_152} :catch_153
    .catch Ljava/lang/NullPointerException; {:try_start_124 .. :try_end_152} :catch_47
    .catch Ljava/lang/NumberFormatException; {:try_start_124 .. :try_end_152} :catch_171
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_124 .. :try_end_152} :catch_1b9
    .catch Ljava/io/FileNotFoundException; {:try_start_124 .. :try_end_152} :catch_201
    .catch Ljava/io/IOException; {:try_start_124 .. :try_end_152} :catch_29a
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_124 .. :try_end_152} :catch_2e3

    #@152
    goto :goto_f9

    #@153
    .line 909
    .end local v2           #ap:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    .end local v3           #dai:Landroid/app/admin/DeviceAdminInfo;
    :catch_153
    move-exception v4

    #@154
    .line 910
    .local v4, e:Ljava/lang/RuntimeException;
    :try_start_154
    const-string v15, "DevicePolicyManagerService"

    #@156
    new-instance v16, Ljava/lang/StringBuilder;

    #@158
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@15b
    const-string v17, "Failed loading admin "

    #@15d
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@160
    move-result-object v16

    #@161
    move-object/from16 v0, v16

    #@163
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@166
    move-result-object v16

    #@167
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16a
    move-result-object v16

    #@16b
    move-object/from16 v0, v16

    #@16d
    invoke-static {v15, v0, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_170
    .catch Ljava/lang/NullPointerException; {:try_start_154 .. :try_end_170} :catch_47
    .catch Ljava/lang/NumberFormatException; {:try_start_154 .. :try_end_170} :catch_171
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_154 .. :try_end_170} :catch_1b9
    .catch Ljava/io/FileNotFoundException; {:try_start_154 .. :try_end_170} :catch_201
    .catch Ljava/io/IOException; {:try_start_154 .. :try_end_170} :catch_29a
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_154 .. :try_end_170} :catch_2e3

    #@170
    goto :goto_f9

    #@171
    .line 945
    .end local v4           #e:Ljava/lang/RuntimeException;
    .end local v7           #name:Ljava/lang/String;
    .end local v8           #outerDepth:I
    .end local v9           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v12           #tag:Ljava/lang/String;
    .end local v13           #type:I
    :catch_171
    move-exception v4

    #@172
    move-object v10, v11

    #@173
    .line 946
    .end local v11           #stream:Ljava/io/FileInputStream;
    .local v4, e:Ljava/lang/NumberFormatException;
    .restart local v10       #stream:Ljava/io/FileInputStream;
    :goto_173
    const-string v15, "DevicePolicyManagerService"

    #@175
    new-instance v16, Ljava/lang/StringBuilder;

    #@177
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@17a
    const-string v17, "failed parsing "

    #@17c
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17f
    move-result-object v16

    #@180
    move-object/from16 v0, v16

    #@182
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@185
    move-result-object v16

    #@186
    const-string v17, " "

    #@188
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18b
    move-result-object v16

    #@18c
    move-object/from16 v0, v16

    #@18e
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@191
    move-result-object v16

    #@192
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@195
    move-result-object v16

    #@196
    invoke-static/range {v15 .. v16}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@199
    goto/16 :goto_6f

    #@19b
    .line 912
    .end local v4           #e:Ljava/lang/NumberFormatException;
    .end local v10           #stream:Ljava/io/FileInputStream;
    .restart local v8       #outerDepth:I
    .restart local v9       #parser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v11       #stream:Ljava/io/FileInputStream;
    .restart local v12       #tag:Ljava/lang/String;
    .restart local v13       #type:I
    :cond_19b
    :try_start_19b
    const-string v15, "failed-password-attempts"

    #@19d
    invoke-virtual {v15, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1a0
    move-result v15

    #@1a1
    if-eqz v15, :cond_1e3

    #@1a3
    .line 913
    const/4 v15, 0x0

    #@1a4
    const-string v16, "value"

    #@1a6
    move-object/from16 v0, v16

    #@1a8
    invoke-interface {v9, v15, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1ab
    move-result-object v15

    #@1ac
    invoke-static {v15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@1af
    move-result v15

    #@1b0
    move-object/from16 v0, p1

    #@1b2
    iput v15, v0, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mFailedPasswordAttempts:I

    #@1b4
    .line 915
    invoke-static {v9}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V
    :try_end_1b7
    .catch Ljava/lang/NullPointerException; {:try_start_19b .. :try_end_1b7} :catch_47
    .catch Ljava/lang/NumberFormatException; {:try_start_19b .. :try_end_1b7} :catch_171
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_19b .. :try_end_1b7} :catch_1b9
    .catch Ljava/io/FileNotFoundException; {:try_start_19b .. :try_end_1b7} :catch_201
    .catch Ljava/io/IOException; {:try_start_19b .. :try_end_1b7} :catch_29a
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_19b .. :try_end_1b7} :catch_2e3

    #@1b7
    goto/16 :goto_f9

    #@1b9
    .line 947
    .end local v8           #outerDepth:I
    .end local v9           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v12           #tag:Ljava/lang/String;
    .end local v13           #type:I
    :catch_1b9
    move-exception v4

    #@1ba
    move-object v10, v11

    #@1bb
    .line 948
    .end local v11           #stream:Ljava/io/FileInputStream;
    .local v4, e:Lorg/xmlpull/v1/XmlPullParserException;
    .restart local v10       #stream:Ljava/io/FileInputStream;
    :goto_1bb
    const-string v15, "DevicePolicyManagerService"

    #@1bd
    new-instance v16, Ljava/lang/StringBuilder;

    #@1bf
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@1c2
    const-string v17, "failed parsing "

    #@1c4
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c7
    move-result-object v16

    #@1c8
    move-object/from16 v0, v16

    #@1ca
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1cd
    move-result-object v16

    #@1ce
    const-string v17, " "

    #@1d0
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d3
    move-result-object v16

    #@1d4
    move-object/from16 v0, v16

    #@1d6
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1d9
    move-result-object v16

    #@1da
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1dd
    move-result-object v16

    #@1de
    invoke-static/range {v15 .. v16}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1e1
    goto/16 :goto_6f

    #@1e3
    .line 916
    .end local v4           #e:Lorg/xmlpull/v1/XmlPullParserException;
    .end local v10           #stream:Ljava/io/FileInputStream;
    .restart local v8       #outerDepth:I
    .restart local v9       #parser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v11       #stream:Ljava/io/FileInputStream;
    .restart local v12       #tag:Ljava/lang/String;
    .restart local v13       #type:I
    :cond_1e3
    :try_start_1e3
    const-string v15, "password-owner"

    #@1e5
    invoke-virtual {v15, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1e8
    move-result v15

    #@1e9
    if-eqz v15, :cond_205

    #@1eb
    .line 917
    const/4 v15, 0x0

    #@1ec
    const-string v16, "value"

    #@1ee
    move-object/from16 v0, v16

    #@1f0
    invoke-interface {v9, v15, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1f3
    move-result-object v15

    #@1f4
    invoke-static {v15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@1f7
    move-result v15

    #@1f8
    move-object/from16 v0, p1

    #@1fa
    iput v15, v0, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mPasswordOwner:I

    #@1fc
    .line 919
    invoke-static {v9}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@1ff
    goto/16 :goto_f9

    #@201
    .line 949
    .end local v8           #outerDepth:I
    .end local v9           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v12           #tag:Ljava/lang/String;
    .end local v13           #type:I
    :catch_201
    move-exception v15

    #@202
    move-object v10, v11

    #@203
    .end local v11           #stream:Ljava/io/FileInputStream;
    .restart local v10       #stream:Ljava/io/FileInputStream;
    goto/16 :goto_6f

    #@205
    .line 920
    .end local v10           #stream:Ljava/io/FileInputStream;
    .restart local v8       #outerDepth:I
    .restart local v9       #parser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v11       #stream:Ljava/io/FileInputStream;
    .restart local v12       #tag:Ljava/lang/String;
    .restart local v13       #type:I
    :cond_205
    const-string v15, "active-password"

    #@207
    invoke-virtual {v15, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@20a
    move-result v15

    #@20b
    if-eqz v15, :cond_2c4

    #@20d
    .line 921
    const/4 v15, 0x0

    #@20e
    const-string v16, "quality"

    #@210
    move-object/from16 v0, v16

    #@212
    invoke-interface {v9, v15, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@215
    move-result-object v15

    #@216
    invoke-static {v15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@219
    move-result v15

    #@21a
    move-object/from16 v0, p1

    #@21c
    iput v15, v0, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordQuality:I

    #@21e
    .line 923
    const/4 v15, 0x0

    #@21f
    const-string v16, "length"

    #@221
    move-object/from16 v0, v16

    #@223
    invoke-interface {v9, v15, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@226
    move-result-object v15

    #@227
    invoke-static {v15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@22a
    move-result v15

    #@22b
    move-object/from16 v0, p1

    #@22d
    iput v15, v0, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordLength:I

    #@22f
    .line 925
    const/4 v15, 0x0

    #@230
    const-string v16, "uppercase"

    #@232
    move-object/from16 v0, v16

    #@234
    invoke-interface {v9, v15, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@237
    move-result-object v15

    #@238
    invoke-static {v15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@23b
    move-result v15

    #@23c
    move-object/from16 v0, p1

    #@23e
    iput v15, v0, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordUpperCase:I

    #@240
    .line 927
    const/4 v15, 0x0

    #@241
    const-string v16, "lowercase"

    #@243
    move-object/from16 v0, v16

    #@245
    invoke-interface {v9, v15, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@248
    move-result-object v15

    #@249
    invoke-static {v15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@24c
    move-result v15

    #@24d
    move-object/from16 v0, p1

    #@24f
    iput v15, v0, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordLowerCase:I

    #@251
    .line 929
    const/4 v15, 0x0

    #@252
    const-string v16, "letters"

    #@254
    move-object/from16 v0, v16

    #@256
    invoke-interface {v9, v15, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@259
    move-result-object v15

    #@25a
    invoke-static {v15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@25d
    move-result v15

    #@25e
    move-object/from16 v0, p1

    #@260
    iput v15, v0, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordLetters:I

    #@262
    .line 931
    const/4 v15, 0x0

    #@263
    const-string v16, "numeric"

    #@265
    move-object/from16 v0, v16

    #@267
    invoke-interface {v9, v15, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@26a
    move-result-object v15

    #@26b
    invoke-static {v15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@26e
    move-result v15

    #@26f
    move-object/from16 v0, p1

    #@271
    iput v15, v0, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordNumeric:I

    #@273
    .line 933
    const/4 v15, 0x0

    #@274
    const-string v16, "symbols"

    #@276
    move-object/from16 v0, v16

    #@278
    invoke-interface {v9, v15, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@27b
    move-result-object v15

    #@27c
    invoke-static {v15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@27f
    move-result v15

    #@280
    move-object/from16 v0, p1

    #@282
    iput v15, v0, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordSymbols:I

    #@284
    .line 935
    const/4 v15, 0x0

    #@285
    const-string v16, "nonletter"

    #@287
    move-object/from16 v0, v16

    #@289
    invoke-interface {v9, v15, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@28c
    move-result-object v15

    #@28d
    invoke-static {v15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@290
    move-result v15

    #@291
    move-object/from16 v0, p1

    #@293
    iput v15, v0, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordNonLetter:I

    #@295
    .line 937
    invoke-static {v9}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V
    :try_end_298
    .catch Ljava/lang/NullPointerException; {:try_start_1e3 .. :try_end_298} :catch_47
    .catch Ljava/lang/NumberFormatException; {:try_start_1e3 .. :try_end_298} :catch_171
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1e3 .. :try_end_298} :catch_1b9
    .catch Ljava/io/FileNotFoundException; {:try_start_1e3 .. :try_end_298} :catch_201
    .catch Ljava/io/IOException; {:try_start_1e3 .. :try_end_298} :catch_29a
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1e3 .. :try_end_298} :catch_2e3

    #@298
    goto/16 :goto_f9

    #@29a
    .line 951
    .end local v8           #outerDepth:I
    .end local v9           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v12           #tag:Ljava/lang/String;
    .end local v13           #type:I
    :catch_29a
    move-exception v4

    #@29b
    move-object v10, v11

    #@29c
    .line 952
    .end local v11           #stream:Ljava/io/FileInputStream;
    .local v4, e:Ljava/io/IOException;
    .restart local v10       #stream:Ljava/io/FileInputStream;
    :goto_29c
    const-string v15, "DevicePolicyManagerService"

    #@29e
    new-instance v16, Ljava/lang/StringBuilder;

    #@2a0
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@2a3
    const-string v17, "failed parsing "

    #@2a5
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a8
    move-result-object v16

    #@2a9
    move-object/from16 v0, v16

    #@2ab
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2ae
    move-result-object v16

    #@2af
    const-string v17, " "

    #@2b1
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b4
    move-result-object v16

    #@2b5
    move-object/from16 v0, v16

    #@2b7
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2ba
    move-result-object v16

    #@2bb
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2be
    move-result-object v16

    #@2bf
    invoke-static/range {v15 .. v16}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2c2
    goto/16 :goto_6f

    #@2c4
    .line 939
    .end local v4           #e:Ljava/io/IOException;
    .end local v10           #stream:Ljava/io/FileInputStream;
    .restart local v8       #outerDepth:I
    .restart local v9       #parser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v11       #stream:Ljava/io/FileInputStream;
    .restart local v12       #tag:Ljava/lang/String;
    .restart local v13       #type:I
    :cond_2c4
    :try_start_2c4
    const-string v15, "DevicePolicyManagerService"

    #@2c6
    new-instance v16, Ljava/lang/StringBuilder;

    #@2c8
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@2cb
    const-string v17, "Unknown tag: "

    #@2cd
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d0
    move-result-object v16

    #@2d1
    move-object/from16 v0, v16

    #@2d3
    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d6
    move-result-object v16

    #@2d7
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2da
    move-result-object v16

    #@2db
    invoke-static/range {v15 .. v16}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2de
    .line 940
    invoke-static {v9}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V
    :try_end_2e1
    .catch Ljava/lang/NullPointerException; {:try_start_2c4 .. :try_end_2e1} :catch_47
    .catch Ljava/lang/NumberFormatException; {:try_start_2c4 .. :try_end_2e1} :catch_171
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2c4 .. :try_end_2e1} :catch_1b9
    .catch Ljava/io/FileNotFoundException; {:try_start_2c4 .. :try_end_2e1} :catch_201
    .catch Ljava/io/IOException; {:try_start_2c4 .. :try_end_2e1} :catch_29a
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_2c4 .. :try_end_2e1} :catch_2e3

    #@2e1
    goto/16 :goto_f9

    #@2e3
    .line 953
    .end local v8           #outerDepth:I
    .end local v9           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v12           #tag:Ljava/lang/String;
    .end local v13           #type:I
    :catch_2e3
    move-exception v4

    #@2e4
    move-object v10, v11

    #@2e5
    .line 954
    .end local v11           #stream:Ljava/io/FileInputStream;
    .local v4, e:Ljava/lang/IndexOutOfBoundsException;
    .restart local v10       #stream:Ljava/io/FileInputStream;
    :goto_2e5
    const-string v15, "DevicePolicyManagerService"

    #@2e7
    new-instance v16, Ljava/lang/StringBuilder;

    #@2e9
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@2ec
    const-string v17, "failed parsing "

    #@2ee
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f1
    move-result-object v16

    #@2f2
    move-object/from16 v0, v16

    #@2f4
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2f7
    move-result-object v16

    #@2f8
    const-string v17, " "

    #@2fa
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2fd
    move-result-object v16

    #@2fe
    move-object/from16 v0, v16

    #@300
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@303
    move-result-object v16

    #@304
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@307
    move-result-object v16

    #@308
    invoke-static/range {v15 .. v16}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@30b
    goto/16 :goto_6f

    #@30d
    .end local v4           #e:Ljava/lang/IndexOutOfBoundsException;
    .end local v10           #stream:Ljava/io/FileInputStream;
    .restart local v8       #outerDepth:I
    .restart local v9       #parser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v11       #stream:Ljava/io/FileInputStream;
    .restart local v12       #tag:Ljava/lang/String;
    .restart local v13       #type:I
    :cond_30d
    move-object v10, v11

    #@30e
    .line 955
    .end local v11           #stream:Ljava/io/FileInputStream;
    .restart local v10       #stream:Ljava/io/FileInputStream;
    goto/16 :goto_6f

    #@310
    .line 960
    .end local v8           #outerDepth:I
    .end local v9           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v12           #tag:Ljava/lang/String;
    .end local v13           #type:I
    :catch_310
    move-exception v15

    #@311
    goto/16 :goto_74

    #@313
    .line 953
    :catch_313
    move-exception v4

    #@314
    goto :goto_2e5

    #@315
    .line 951
    :catch_315
    move-exception v4

    #@316
    goto :goto_29c

    #@317
    .line 949
    :catch_317
    move-exception v15

    #@318
    goto/16 :goto_6f

    #@31a
    .line 947
    :catch_31a
    move-exception v4

    #@31b
    goto/16 :goto_1bb

    #@31d
    .line 945
    :catch_31d
    move-exception v4

    #@31e
    goto/16 :goto_173

    #@320
    .line 943
    :catch_320
    move-exception v4

    #@321
    goto/16 :goto_49
.end method

.method private lockNowUnchecked()V
    .registers 7

    #@0
    .prologue
    .line 1918
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@3
    move-result-wide v0

    #@4
    .line 1921
    .local v0, ident:J
    :try_start_4
    invoke-direct {p0}, Lcom/android/server/DevicePolicyManagerService;->getIPowerManager()Landroid/os/IPowerManager;

    #@7
    move-result-object v2

    #@8
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@b
    move-result-wide v3

    #@c
    const/4 v5, 0x1

    #@d
    invoke-interface {v2, v3, v4, v5}, Landroid/os/IPowerManager;->goToSleep(JI)V

    #@10
    .line 1924
    invoke-direct {p0}, Lcom/android/server/DevicePolicyManagerService;->getWindowManager()Landroid/view/IWindowManager;

    #@13
    move-result-object v2

    #@14
    const/4 v3, 0x0

    #@15
    invoke-interface {v2, v3}, Landroid/view/IWindowManager;->lockNow(Landroid/os/Bundle;)V
    :try_end_18
    .catchall {:try_start_4 .. :try_end_18} :catchall_1c
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_18} :catch_21

    #@18
    .line 1927
    :goto_18
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@1b
    .line 1929
    return-void

    #@1c
    .line 1927
    :catchall_1c
    move-exception v2

    #@1d
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@20
    throw v2

    #@21
    .line 1925
    :catch_21
    move-exception v2

    #@22
    goto :goto_18
.end method

.method private static makeJournaledFile(I)Lcom/android/internal/util/JournaledFile;
    .registers 7
    .parameter "userHandle"

    #@0
    .prologue
    .line 775
    if-nez p0, :cond_27

    #@2
    const-string v0, "/data/system/device_policies.xml"

    #@4
    .line 779
    .local v0, base:Ljava/lang/String;
    :goto_4
    new-instance v1, Lcom/android/internal/util/JournaledFile;

    #@6
    new-instance v2, Ljava/io/File;

    #@8
    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@b
    new-instance v3, Ljava/io/File;

    #@d
    new-instance v4, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v4

    #@16
    const-string v5, ".tmp"

    #@18
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v4

    #@1c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v4

    #@20
    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@23
    invoke-direct {v1, v2, v3}, Lcom/android/internal/util/JournaledFile;-><init>(Ljava/io/File;Ljava/io/File;)V

    #@26
    return-object v1

    #@27
    .line 775
    .end local v0           #base:Ljava/lang/String;
    :cond_27
    new-instance v1, Ljava/io/File;

    #@29
    invoke-static {p0}, Landroid/os/Environment;->getUserSystemDirectory(I)Ljava/io/File;

    #@2c
    move-result-object v2

    #@2d
    const-string v3, "device_policies.xml"

    #@2f
    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@32
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@35
    move-result-object v0

    #@36
    goto :goto_4
.end method

.method private resetGlobalProxyLocked(Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;)V
    .registers 7
    .parameter "policy"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 2242
    iget-object v3, p1, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v0

    #@7
    .line 2243
    .local v0, N:I
    const/4 v2, 0x0

    #@8
    .local v2, i:I
    :goto_8
    if-ge v2, v0, :cond_21

    #@a
    .line 2244
    iget-object v3, p1, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    check-cast v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@12
    .line 2245
    .local v1, ap:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    iget-boolean v3, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->specifiesGlobalProxy:Z

    #@14
    if-eqz v3, :cond_1e

    #@16
    .line 2246
    iget-object v3, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->globalProxySpec:Ljava/lang/String;

    #@18
    iget-object v4, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->globalProxyExclusionList:Ljava/lang/String;

    #@1a
    invoke-direct {p0, v3, v4}, Lcom/android/server/DevicePolicyManagerService;->saveGlobalProxyLocked(Ljava/lang/String;Ljava/lang/String;)V

    #@1d
    .line 2252
    .end local v1           #ap:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :goto_1d
    return-void

    #@1e
    .line 2243
    .restart local v1       #ap:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_1e
    add-int/lit8 v2, v2, 0x1

    #@20
    goto :goto_8

    #@21
    .line 2251
    .end local v1           #ap:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_21
    invoke-direct {p0, v4, v4}, Lcom/android/server/DevicePolicyManagerService;->saveGlobalProxyLocked(Ljava/lang/String;Ljava/lang/String;)V

    #@24
    goto :goto_1d
.end method

.method private saveGlobalProxyLocked(Ljava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter "proxySpec"
    .parameter "exclusionList"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 2255
    if-nez p2, :cond_5

    #@3
    .line 2256
    const-string p2, ""

    #@5
    .line 2258
    :cond_5
    if-nez p1, :cond_9

    #@7
    .line 2259
    const-string p1, ""

    #@9
    .line 2262
    :cond_9
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@c
    move-result-object p1

    #@d
    .line 2263
    const-string v3, ":"

    #@f
    invoke-virtual {p1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    .line 2264
    .local v0, data:[Ljava/lang/String;
    const/16 v1, 0x1f90

    #@15
    .line 2265
    .local v1, proxyPort:I
    array-length v3, v0

    #@16
    if-le v3, v4, :cond_1f

    #@18
    .line 2267
    const/4 v3, 0x1

    #@19
    :try_start_19
    aget-object v3, v0, v3

    #@1b
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1e
    .catch Ljava/lang/NumberFormatException; {:try_start_19 .. :try_end_1e} :catch_3c

    #@1e
    move-result v1

    #@1f
    .line 2270
    :cond_1f
    :goto_1f
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@22
    move-result-object p2

    #@23
    .line 2271
    iget-object v3, p0, Lcom/android/server/DevicePolicyManagerService;->mContext:Landroid/content/Context;

    #@25
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@28
    move-result-object v2

    #@29
    .line 2272
    .local v2, res:Landroid/content/ContentResolver;
    const-string v3, "global_http_proxy_host"

    #@2b
    const/4 v4, 0x0

    #@2c
    aget-object v4, v0, v4

    #@2e
    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Global;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    #@31
    .line 2273
    const-string v3, "global_http_proxy_port"

    #@33
    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@36
    .line 2274
    const-string v3, "global_http_proxy_exclusion_list"

    #@38
    invoke-static {v2, v3, p2}, Landroid/provider/Settings$Global;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    #@3b
    .line 2276
    return-void

    #@3c
    .line 2268
    .end local v2           #res:Landroid/content/ContentResolver;
    :catch_3c
    move-exception v3

    #@3d
    goto :goto_1f
.end method

.method private saveSettingsLocked(I)V
    .registers 14
    .parameter "userHandle"

    #@0
    .prologue
    .line 783
    invoke-virtual {p0, p1}, Lcom/android/server/DevicePolicyManagerService;->getUserData(I)Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;

    #@3
    move-result-object v6

    #@4
    .line 784
    .local v6, policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    invoke-static {p1}, Lcom/android/server/DevicePolicyManagerService;->makeJournaledFile(I)Lcom/android/internal/util/JournaledFile;

    #@7
    move-result-object v4

    #@8
    .line 785
    .local v4, journal:Lcom/android/internal/util/JournaledFile;
    const/4 v7, 0x0

    #@9
    .line 787
    .local v7, stream:Ljava/io/FileOutputStream;
    :try_start_9
    new-instance v8, Ljava/io/FileOutputStream;

    #@b
    invoke-virtual {v4}, Lcom/android/internal/util/JournaledFile;->chooseForWrite()Ljava/io/File;

    #@e
    move-result-object v9

    #@f
    const/4 v10, 0x0

    #@10
    invoke-direct {v8, v9, v10}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_13} :catch_13c

    #@13
    .line 788
    .end local v7           #stream:Ljava/io/FileOutputStream;
    .local v8, stream:Ljava/io/FileOutputStream;
    :try_start_13
    new-instance v5, Lcom/android/internal/util/FastXmlSerializer;

    #@15
    invoke-direct {v5}, Lcom/android/internal/util/FastXmlSerializer;-><init>()V

    #@18
    .line 789
    .local v5, out:Lorg/xmlpull/v1/XmlSerializer;
    const-string v9, "utf-8"

    #@1a
    invoke-interface {v5, v8, v9}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V

    #@1d
    .line 790
    const/4 v9, 0x0

    #@1e
    const/4 v10, 0x1

    #@1f
    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@22
    move-result-object v10

    #@23
    invoke-interface {v5, v9, v10}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    #@26
    .line 792
    const/4 v9, 0x0

    #@27
    const-string v10, "policies"

    #@29
    invoke-interface {v5, v9, v10}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@2c
    .line 794
    iget-object v9, v6, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@2e
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    #@31
    move-result v0

    #@32
    .line 795
    .local v0, N:I
    const/4 v3, 0x0

    #@33
    .local v3, i:I
    :goto_33
    if-ge v3, v0, :cond_61

    #@35
    .line 796
    iget-object v9, v6, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@37
    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@3a
    move-result-object v1

    #@3b
    check-cast v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@3d
    .line 797
    .local v1, ap:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    if-eqz v1, :cond_5e

    #@3f
    .line 798
    const/4 v9, 0x0

    #@40
    const-string v10, "admin"

    #@42
    invoke-interface {v5, v9, v10}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@45
    .line 799
    const/4 v9, 0x0

    #@46
    const-string v10, "name"

    #@48
    iget-object v11, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->info:Landroid/app/admin/DeviceAdminInfo;

    #@4a
    invoke-virtual {v11}, Landroid/app/admin/DeviceAdminInfo;->getComponent()Landroid/content/ComponentName;

    #@4d
    move-result-object v11

    #@4e
    invoke-virtual {v11}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    #@51
    move-result-object v11

    #@52
    invoke-interface {v5, v9, v10, v11}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@55
    .line 800
    invoke-virtual {v1, v5}, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->writeToXml(Lorg/xmlpull/v1/XmlSerializer;)V

    #@58
    .line 801
    const/4 v9, 0x0

    #@59
    const-string v10, "admin"

    #@5b
    invoke-interface {v5, v9, v10}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@5e
    .line 795
    :cond_5e
    add-int/lit8 v3, v3, 0x1

    #@60
    goto :goto_33

    #@61
    .line 805
    .end local v1           #ap:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_61
    iget v9, v6, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mPasswordOwner:I

    #@63
    if-ltz v9, :cond_7d

    #@65
    .line 806
    const/4 v9, 0x0

    #@66
    const-string v10, "password-owner"

    #@68
    invoke-interface {v5, v9, v10}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@6b
    .line 807
    const/4 v9, 0x0

    #@6c
    const-string v10, "value"

    #@6e
    iget v11, v6, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mPasswordOwner:I

    #@70
    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@73
    move-result-object v11

    #@74
    invoke-interface {v5, v9, v10, v11}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@77
    .line 808
    const/4 v9, 0x0

    #@78
    const-string v10, "password-owner"

    #@7a
    invoke-interface {v5, v9, v10}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@7d
    .line 811
    :cond_7d
    iget v9, v6, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mFailedPasswordAttempts:I

    #@7f
    if-eqz v9, :cond_99

    #@81
    .line 812
    const/4 v9, 0x0

    #@82
    const-string v10, "failed-password-attempts"

    #@84
    invoke-interface {v5, v9, v10}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@87
    .line 813
    const/4 v9, 0x0

    #@88
    const-string v10, "value"

    #@8a
    iget v11, v6, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mFailedPasswordAttempts:I

    #@8c
    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@8f
    move-result-object v11

    #@90
    invoke-interface {v5, v9, v10, v11}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@93
    .line 814
    const/4 v9, 0x0

    #@94
    const-string v10, "failed-password-attempts"

    #@96
    invoke-interface {v5, v9, v10}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@99
    .line 817
    :cond_99
    iget v9, v6, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordQuality:I

    #@9b
    if-nez v9, :cond_b9

    #@9d
    iget v9, v6, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordLength:I

    #@9f
    if-nez v9, :cond_b9

    #@a1
    iget v9, v6, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordUpperCase:I

    #@a3
    if-nez v9, :cond_b9

    #@a5
    iget v9, v6, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordLowerCase:I

    #@a7
    if-nez v9, :cond_b9

    #@a9
    iget v9, v6, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordLetters:I

    #@ab
    if-nez v9, :cond_b9

    #@ad
    iget v9, v6, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordNumeric:I

    #@af
    if-nez v9, :cond_b9

    #@b1
    iget v9, v6, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordSymbols:I

    #@b3
    if-nez v9, :cond_b9

    #@b5
    iget v9, v6, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordNonLetter:I

    #@b7
    if-eqz v9, :cond_125

    #@b9
    .line 821
    :cond_b9
    const/4 v9, 0x0

    #@ba
    const-string v10, "active-password"

    #@bc
    invoke-interface {v5, v9, v10}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@bf
    .line 822
    const/4 v9, 0x0

    #@c0
    const-string v10, "quality"

    #@c2
    iget v11, v6, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordQuality:I

    #@c4
    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@c7
    move-result-object v11

    #@c8
    invoke-interface {v5, v9, v10, v11}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@cb
    .line 823
    const/4 v9, 0x0

    #@cc
    const-string v10, "length"

    #@ce
    iget v11, v6, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordLength:I

    #@d0
    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@d3
    move-result-object v11

    #@d4
    invoke-interface {v5, v9, v10, v11}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@d7
    .line 824
    const/4 v9, 0x0

    #@d8
    const-string v10, "uppercase"

    #@da
    iget v11, v6, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordUpperCase:I

    #@dc
    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@df
    move-result-object v11

    #@e0
    invoke-interface {v5, v9, v10, v11}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@e3
    .line 825
    const/4 v9, 0x0

    #@e4
    const-string v10, "lowercase"

    #@e6
    iget v11, v6, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordLowerCase:I

    #@e8
    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@eb
    move-result-object v11

    #@ec
    invoke-interface {v5, v9, v10, v11}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@ef
    .line 826
    const/4 v9, 0x0

    #@f0
    const-string v10, "letters"

    #@f2
    iget v11, v6, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordLetters:I

    #@f4
    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@f7
    move-result-object v11

    #@f8
    invoke-interface {v5, v9, v10, v11}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@fb
    .line 827
    const/4 v9, 0x0

    #@fc
    const-string v10, "numeric"

    #@fe
    iget v11, v6, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordNumeric:I

    #@100
    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@103
    move-result-object v11

    #@104
    invoke-interface {v5, v9, v10, v11}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@107
    .line 829
    const/4 v9, 0x0

    #@108
    const-string v10, "symbols"

    #@10a
    iget v11, v6, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordSymbols:I

    #@10c
    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@10f
    move-result-object v11

    #@110
    invoke-interface {v5, v9, v10, v11}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@113
    .line 830
    const/4 v9, 0x0

    #@114
    const-string v10, "nonletter"

    #@116
    iget v11, v6, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordNonLetter:I

    #@118
    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@11b
    move-result-object v11

    #@11c
    invoke-interface {v5, v9, v10, v11}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@11f
    .line 831
    const/4 v9, 0x0

    #@120
    const-string v10, "active-password"

    #@122
    invoke-interface {v5, v9, v10}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@125
    .line 834
    :cond_125
    const/4 v9, 0x0

    #@126
    const-string v10, "policies"

    #@128
    invoke-interface {v5, v9, v10}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    #@12b
    .line 836
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V

    #@12e
    .line 838
    invoke-static {v8}, Landroid/os/FileUtils;->sync(Ljava/io/FileOutputStream;)Z

    #@131
    .line 840
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V

    #@134
    .line 841
    invoke-virtual {v4}, Lcom/android/internal/util/JournaledFile;->commit()V

    #@137
    .line 842
    invoke-direct {p0, p1}, Lcom/android/server/DevicePolicyManagerService;->sendChangedNotification(I)V
    :try_end_13a
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13a} :catch_148

    #@13a
    move-object v7, v8

    #@13b
    .line 853
    .end local v0           #N:I
    .end local v3           #i:I
    .end local v5           #out:Lorg/xmlpull/v1/XmlSerializer;
    .end local v8           #stream:Ljava/io/FileOutputStream;
    .restart local v7       #stream:Ljava/io/FileOutputStream;
    :goto_13b
    return-void

    #@13c
    .line 843
    :catch_13c
    move-exception v2

    #@13d
    .line 845
    .local v2, e:Ljava/io/IOException;
    :goto_13d
    if-eqz v7, :cond_142

    #@13f
    .line 846
    :try_start_13f
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_142
    .catch Ljava/io/IOException; {:try_start_13f .. :try_end_142} :catch_146

    #@142
    .line 851
    :cond_142
    :goto_142
    invoke-virtual {v4}, Lcom/android/internal/util/JournaledFile;->rollback()V

    #@145
    goto :goto_13b

    #@146
    .line 848
    :catch_146
    move-exception v9

    #@147
    goto :goto_142

    #@148
    .line 843
    .end local v2           #e:Ljava/io/IOException;
    .end local v7           #stream:Ljava/io/FileOutputStream;
    .restart local v8       #stream:Ljava/io/FileOutputStream;
    :catch_148
    move-exception v2

    #@149
    move-object v7, v8

    #@14a
    .end local v8           #stream:Ljava/io/FileOutputStream;
    .restart local v7       #stream:Ljava/io/FileOutputStream;
    goto :goto_13d
.end method

.method private sendChangedNotification(I)V
    .registers 7
    .parameter "userHandle"

    #@0
    .prologue
    .line 856
    new-instance v2, Landroid/content/Intent;

    #@2
    const-string v3, "android.app.action.DEVICE_POLICY_MANAGER_STATE_CHANGED"

    #@4
    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 857
    .local v2, intent:Landroid/content/Intent;
    const/high16 v3, 0x4000

    #@9
    invoke-virtual {v2, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@c
    .line 858
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@f
    move-result-wide v0

    #@10
    .line 860
    .local v0, ident:J
    :try_start_10
    iget-object v3, p0, Lcom/android/server/DevicePolicyManagerService;->mContext:Landroid/content/Context;

    #@12
    new-instance v4, Landroid/os/UserHandle;

    #@14
    invoke-direct {v4, p1}, Landroid/os/UserHandle;-><init>(I)V

    #@17
    invoke-virtual {v3, v2, v4}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
    :try_end_1a
    .catchall {:try_start_10 .. :try_end_1a} :catchall_1e

    #@1a
    .line 862
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@1d
    .line 864
    return-void

    #@1e
    .line 862
    :catchall_1e
    move-exception v3

    #@1f
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@22
    throw v3
.end method

.method private setEncryptionRequested(Z)V
    .registers 2
    .parameter "encrypt"

    #@0
    .prologue
    .line 2396
    return-void
.end method

.method private updatePasswordExpirationsLocked(I)V
    .registers 14
    .parameter "userHandle"

    #@0
    .prologue
    const-wide/16 v8, 0x0

    #@2
    .line 2110
    invoke-virtual {p0, p1}, Lcom/android/server/DevicePolicyManagerService;->getUserData(I)Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;

    #@5
    move-result-object v5

    #@6
    .line 2111
    .local v5, policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    iget-object v10, v5, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@8
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    #@b
    move-result v0

    #@c
    .line 2112
    .local v0, N:I
    if-lez v0, :cond_38

    #@e
    .line 2113
    const/4 v4, 0x0

    #@f
    .local v4, i:I
    :goto_f
    if-ge v4, v0, :cond_35

    #@11
    .line 2114
    iget-object v10, v5, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@13
    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@16
    move-result-object v1

    #@17
    check-cast v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@19
    .line 2115
    .local v1, admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    iget-object v10, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->info:Landroid/app/admin/DeviceAdminInfo;

    #@1b
    const/4 v11, 0x6

    #@1c
    invoke-virtual {v10, v11}, Landroid/app/admin/DeviceAdminInfo;->usesPolicy(I)Z

    #@1f
    move-result v10

    #@20
    if-eqz v10, :cond_30

    #@22
    .line 2116
    iget-wide v6, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->passwordExpirationTimeout:J

    #@24
    .line 2117
    .local v6, timeout:J
    cmp-long v10, v6, v8

    #@26
    if-lez v10, :cond_33

    #@28
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@2b
    move-result-wide v10

    #@2c
    add-long v2, v6, v10

    #@2e
    .line 2118
    .local v2, expiration:J
    :goto_2e
    iput-wide v2, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->passwordExpirationDate:J

    #@30
    .line 2113
    .end local v2           #expiration:J
    .end local v6           #timeout:J
    :cond_30
    add-int/lit8 v4, v4, 0x1

    #@32
    goto :goto_f

    #@33
    .restart local v6       #timeout:J
    :cond_33
    move-wide v2, v8

    #@34
    .line 2117
    goto :goto_2e

    #@35
    .line 2121
    .end local v1           #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    .end local v6           #timeout:J
    :cond_35
    invoke-direct {p0, p1}, Lcom/android/server/DevicePolicyManagerService;->saveSettingsLocked(I)V

    #@38
    .line 2123
    .end local v4           #i:I
    :cond_38
    return-void
.end method

.method static validateQualityConstant(I)V
    .registers 4
    .parameter "quality"

    #@0
    .prologue
    .line 990
    sparse-switch p0, :sswitch_data_22

    #@3
    .line 1000
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@5
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v2, "Invalid quality constant: 0x"

    #@c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    invoke-static {p0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v0

    #@20
    .line 998
    :sswitch_20
    return-void

    #@21
    .line 990
    nop

    #@22
    :sswitch_data_22
    .sparse-switch
        0x0 -> :sswitch_20
        0x8000 -> :sswitch_20
        0x10000 -> :sswitch_20
        0x20000 -> :sswitch_20
        0x40000 -> :sswitch_20
        0x50000 -> :sswitch_20
        0x60000 -> :sswitch_20
    .end sparse-switch
.end method

.method private wipeDeviceOrUserLocked(II)V
    .registers 6
    .parameter "flags"
    .parameter "userHandle"

    #@0
    .prologue
    .line 2002
    if-nez p2, :cond_3a

    #@2
    .line 2004
    sget-boolean v1, Lcom/lge/config/ConfigBuildFlags;->CAPP_MDM:Z

    #@4
    if-eqz v1, :cond_36

    #@6
    .line 2005
    const-string v1, "ro.build.target_operator"

    #@8
    const-string v2, "OPEN"

    #@a
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@d
    move-result-object v0

    #@e
    .line 2007
    .local v0, OPERATOR_CODE:Ljava/lang/String;
    const-string v1, "VZW"

    #@10
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@13
    move-result v1

    #@14
    if-eqz v1, :cond_26

    #@16
    .line 2008
    const-string v1, "DevicePolicyManagerService"

    #@18
    const-string v2, "CAPP_MDM reportFailedPasswordAttempt() called policy : 2"

    #@1a
    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    .line 2010
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@20
    move-result-object v1

    #@21
    const/4 v2, 0x2

    #@22
    invoke-interface {v1, v2}, Lcom/lge/cappuccino/IMdm;->wipeData(I)V

    #@25
    .line 2037
    .end local v0           #OPERATOR_CODE:Ljava/lang/String;
    :goto_25
    return-void

    #@26
    .line 2012
    .restart local v0       #OPERATOR_CODE:Ljava/lang/String;
    :cond_26
    const-string v1, "DevicePolicyManagerService"

    #@28
    const-string v2, "CAPP_MDM reportFailedPasswordAttempt() called policy : 1"

    #@2a
    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 2014
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@30
    move-result-object v1

    #@31
    const/4 v2, 0x1

    #@32
    invoke-interface {v1, v2}, Lcom/lge/cappuccino/IMdm;->wipeData(I)V

    #@35
    goto :goto_25

    #@36
    .line 2017
    .end local v0           #OPERATOR_CODE:Ljava/lang/String;
    :cond_36
    invoke-virtual {p0, p1}, Lcom/android/server/DevicePolicyManagerService;->wipeDataLocked(I)V

    #@39
    goto :goto_25

    #@3a
    .line 2024
    :cond_3a
    invoke-direct {p0}, Lcom/android/server/DevicePolicyManagerService;->lockNowUnchecked()V

    #@3d
    .line 2025
    iget-object v1, p0, Lcom/android/server/DevicePolicyManagerService;->mHandler:Landroid/os/Handler;

    #@3f
    new-instance v2, Lcom/android/server/DevicePolicyManagerService$3;

    #@41
    invoke-direct {v2, p0, p2}, Lcom/android/server/DevicePolicyManagerService$3;-><init>(Lcom/android/server/DevicePolicyManagerService;I)V

    #@44
    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@47
    goto :goto_25
.end method


# virtual methods
.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 13
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 2505
    iget-object v7, p0, Lcom/android/server/DevicePolicyManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v8, "android.permission.DUMP"

    #@4
    invoke-virtual {v7, v8}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@7
    move-result v7

    #@8
    if-eqz v7, :cond_33

    #@a
    .line 2508
    new-instance v7, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v8, "Permission Denial: can\'t dump DevicePolicyManagerService from from pid="

    #@11
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v7

    #@15
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@18
    move-result v8

    #@19
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v7

    #@1d
    const-string v8, ", uid="

    #@1f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v7

    #@23
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@26
    move-result v8

    #@27
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v7

    #@2b
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v7

    #@2f
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@32
    .line 2537
    :goto_32
    return-void

    #@33
    .line 2514
    :cond_33
    new-instance v3, Landroid/util/PrintWriterPrinter;

    #@35
    invoke-direct {v3, p2}, Landroid/util/PrintWriterPrinter;-><init>(Ljava/io/PrintWriter;)V

    #@38
    .line 2516
    .local v3, p:Landroid/util/Printer;
    monitor-enter p0

    #@39
    .line 2517
    :try_start_39
    const-string v7, "Current Device Policy Manager state:"

    #@3b
    invoke-interface {v3, v7}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@3e
    .line 2519
    iget-object v7, p0, Lcom/android/server/DevicePolicyManagerService;->mUserData:Landroid/util/SparseArray;

    #@40
    invoke-virtual {v7}, Landroid/util/SparseArray;->size()I

    #@43
    move-result v6

    #@44
    .line 2520
    .local v6, userCount:I
    const/4 v5, 0x0

    #@45
    .local v5, u:I
    :goto_45
    if-ge v5, v6, :cond_b3

    #@47
    .line 2521
    iget-object v7, p0, Lcom/android/server/DevicePolicyManagerService;->mUserData:Landroid/util/SparseArray;

    #@49
    invoke-virtual {v7, v5}, Landroid/util/SparseArray;->keyAt(I)I

    #@4c
    move-result v7

    #@4d
    invoke-virtual {p0, v7}, Lcom/android/server/DevicePolicyManagerService;->getUserData(I)Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;

    #@50
    move-result-object v4

    #@51
    .line 2522
    .local v4, policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    new-instance v7, Ljava/lang/StringBuilder;

    #@53
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@56
    const-string v8, "  Enabled Device Admins (User "

    #@58
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v7

    #@5c
    iget v8, v4, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mUserHandle:I

    #@5e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@61
    move-result-object v7

    #@62
    const-string v8, "):"

    #@64
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v7

    #@68
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6b
    move-result-object v7

    #@6c
    invoke-interface {v3, v7}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@6f
    .line 2523
    iget-object v7, v4, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@71
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    #@74
    move-result v0

    #@75
    .line 2524
    .local v0, N:I
    const/4 v2, 0x0

    #@76
    .local v2, i:I
    :goto_76
    if-ge v2, v0, :cond_a1

    #@78
    .line 2525
    iget-object v7, v4, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@7a
    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@7d
    move-result-object v1

    #@7e
    check-cast v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@80
    .line 2526
    .local v1, ap:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    if-eqz v1, :cond_9e

    #@82
    .line 2527
    const-string v7, "  "

    #@84
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@87
    iget-object v7, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->info:Landroid/app/admin/DeviceAdminInfo;

    #@89
    invoke-virtual {v7}, Landroid/app/admin/DeviceAdminInfo;->getComponent()Landroid/content/ComponentName;

    #@8c
    move-result-object v7

    #@8d
    invoke-virtual {v7}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    #@90
    move-result-object v7

    #@91
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@94
    .line 2528
    const-string v7, ":"

    #@96
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@99
    .line 2529
    const-string v7, "    "

    #@9b
    invoke-virtual {v1, v7, p2}, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V

    #@9e
    .line 2524
    :cond_9e
    add-int/lit8 v2, v2, 0x1

    #@a0
    goto :goto_76

    #@a1
    .line 2533
    .end local v1           #ap:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_a1
    const-string v7, " "

    #@a3
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@a6
    .line 2534
    const-string v7, "  mPasswordOwner="

    #@a8
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@ab
    iget v7, v4, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mPasswordOwner:I

    #@ad
    invoke-virtual {p2, v7}, Ljava/io/PrintWriter;->println(I)V

    #@b0
    .line 2520
    add-int/lit8 v5, v5, 0x1

    #@b2
    goto :goto_45

    #@b3
    .line 2536
    .end local v0           #N:I
    .end local v2           #i:I
    .end local v4           #policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    :cond_b3
    monitor-exit p0

    #@b4
    goto/16 :goto_32

    #@b6
    .end local v5           #u:I
    .end local v6           #userCount:I
    :catchall_b6
    move-exception v7

    #@b7
    monitor-exit p0
    :try_end_b8
    .catchall {:try_start_39 .. :try_end_b8} :catchall_b6

    #@b8
    throw v7
.end method

.method public findAdmin(Landroid/content/ComponentName;I)Landroid/app/admin/DeviceAdminInfo;
    .registers 10
    .parameter "adminName"
    .parameter "userHandle"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 754
    invoke-direct {p0, p2}, Lcom/android/server/DevicePolicyManagerService;->enforceCrossUserPermission(I)V

    #@4
    .line 755
    new-instance v2, Landroid/content/Intent;

    #@6
    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    #@9
    .line 756
    .local v2, resolveIntent:Landroid/content/Intent;
    invoke-virtual {v2, p1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@c
    .line 757
    iget-object v3, p0, Lcom/android/server/DevicePolicyManagerService;->mContext:Landroid/content/Context;

    #@e
    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@11
    move-result-object v3

    #@12
    const/16 v4, 0x80

    #@14
    invoke-virtual {v3, v2, v4, p2}, Landroid/content/pm/PackageManager;->queryBroadcastReceivers(Landroid/content/Intent;II)Ljava/util/List;

    #@17
    move-result-object v1

    #@18
    .line 759
    .local v1, infos:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-eqz v1, :cond_20

    #@1a
    invoke-interface {v1}, Ljava/util/List;->size()I

    #@1d
    move-result v3

    #@1e
    if-gtz v3, :cond_39

    #@20
    .line 760
    :cond_20
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@22
    new-instance v4, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v5, "Unknown admin: "

    #@29
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v4

    #@2d
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v4

    #@31
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v4

    #@35
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@38
    throw v3

    #@39
    .line 764
    :cond_39
    :try_start_39
    new-instance v4, Landroid/app/admin/DeviceAdminInfo;

    #@3b
    iget-object v6, p0, Lcom/android/server/DevicePolicyManagerService;->mContext:Landroid/content/Context;

    #@3d
    const/4 v3, 0x0

    #@3e
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@41
    move-result-object v3

    #@42
    check-cast v3, Landroid/content/pm/ResolveInfo;

    #@44
    invoke-direct {v4, v6, v3}, Landroid/app/admin/DeviceAdminInfo;-><init>(Landroid/content/Context;Landroid/content/pm/ResolveInfo;)V
    :try_end_47
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_39 .. :try_end_47} :catch_49
    .catch Ljava/io/IOException; {:try_start_39 .. :try_end_47} :catch_6e

    #@47
    move-object v3, v4

    #@48
    .line 770
    :goto_48
    return-object v3

    #@49
    .line 765
    :catch_49
    move-exception v0

    #@4a
    .line 766
    .local v0, e:Lorg/xmlpull/v1/XmlPullParserException;
    const-string v3, "DevicePolicyManagerService"

    #@4c
    new-instance v4, Ljava/lang/StringBuilder;

    #@4e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@51
    const-string v6, "Bad device admin requested for user="

    #@53
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v4

    #@57
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v4

    #@5b
    const-string v6, ": "

    #@5d
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v4

    #@61
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v4

    #@65
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@68
    move-result-object v4

    #@69
    invoke-static {v3, v4, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@6c
    move-object v3, v5

    #@6d
    .line 767
    goto :goto_48

    #@6e
    .line 768
    .end local v0           #e:Lorg/xmlpull/v1/XmlPullParserException;
    :catch_6e
    move-exception v0

    #@6f
    .line 769
    .local v0, e:Ljava/io/IOException;
    const-string v3, "DevicePolicyManagerService"

    #@71
    new-instance v4, Ljava/lang/StringBuilder;

    #@73
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@76
    const-string v6, "Bad device admin requested for user="

    #@78
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v4

    #@7c
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v4

    #@80
    const-string v6, ": "

    #@82
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v4

    #@86
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@89
    move-result-object v4

    #@8a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8d
    move-result-object v4

    #@8e
    invoke-static {v3, v4, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@91
    move-object v3, v5

    #@92
    .line 770
    goto :goto_48
.end method

.method getActiveAdminForCallerLocked(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    .registers 14
    .parameter "who"
    .parameter "reqPolicy"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    #@0
    .prologue
    .line 645
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@3
    move-result v2

    #@4
    .line 646
    .local v2, callingUid:I
    invoke-static {v2}, Landroid/os/UserHandle;->getUserId(I)I

    #@7
    move-result v7

    #@8
    .line 647
    .local v7, userHandle:I
    invoke-virtual {p0, v7}, Lcom/android/server/DevicePolicyManagerService;->getUserData(I)Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;

    #@b
    move-result-object v5

    #@c
    .line 648
    .local v5, policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    if-eqz p1, :cond_b5

    #@e
    .line 649
    iget-object v8, v5, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminMap:Ljava/util/HashMap;

    #@10
    invoke-virtual {v8, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@13
    move-result-object v1

    #@14
    check-cast v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@16
    .line 650
    .local v1, admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    if-nez v1, :cond_31

    #@18
    .line 651
    new-instance v8, Ljava/lang/SecurityException;

    #@1a
    new-instance v9, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v10, "No active admin "

    #@21
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v9

    #@25
    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v9

    #@29
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v9

    #@2d
    invoke-direct {v8, v9}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@30
    throw v8

    #@31
    .line 653
    :cond_31
    invoke-virtual {v1}, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->getUid()I

    #@34
    move-result v8

    #@35
    if-eq v8, v2, :cond_68

    #@37
    iget-object v8, p0, Lcom/android/server/DevicePolicyManagerService;->mContext:Landroid/content/Context;

    #@39
    const-string v9, "com.sprint.internal.permission.DEVICEMANAGEMENT"

    #@3b
    invoke-virtual {v8, v9}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@3e
    move-result v8

    #@3f
    if-eqz v8, :cond_68

    #@41
    .line 659
    new-instance v8, Ljava/lang/SecurityException;

    #@43
    new-instance v9, Ljava/lang/StringBuilder;

    #@45
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@48
    const-string v10, "Admin "

    #@4a
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v9

    #@4e
    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v9

    #@52
    const-string v10, " is not owned by uid "

    #@54
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v9

    #@58
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@5b
    move-result v10

    #@5c
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v9

    #@60
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@63
    move-result-object v9

    #@64
    invoke-direct {v8, v9}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@67
    throw v8

    #@68
    .line 662
    :cond_68
    const/4 v6, 0x0

    #@69
    .line 663
    .local v6, signed:Z
    sget-boolean v8, Lcom/lge/config/ThreelmMdmConfig;->THREELM_MDM:Z

    #@6b
    if-eqz v8, :cond_7c

    #@6d
    .line 665
    const-string v8, "DeviceManager3LM"

    #@6f
    invoke-static {v8}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@72
    move-result-object v8

    #@73
    invoke-static {v8}, Landroid/os/IDeviceManager3LM$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/IDeviceManager3LM;

    #@76
    move-result-object v3

    #@77
    .line 667
    .local v3, dm:Landroid/os/IDeviceManager3LM;
    const/4 v6, 0x0

    #@78
    .line 669
    :try_start_78
    invoke-interface {v3, v2}, Landroid/os/IDeviceManager3LM;->checkSignature(I)Z
    :try_end_7b
    .catch Landroid/os/RemoteException; {:try_start_78 .. :try_end_7b} :catch_ff

    #@7b
    move-result v6

    #@7c
    .line 675
    .end local v3           #dm:Landroid/os/IDeviceManager3LM;
    :cond_7c
    :goto_7c
    if-nez v6, :cond_d4

    #@7e
    iget-object v8, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->info:Landroid/app/admin/DeviceAdminInfo;

    #@80
    invoke-virtual {v8, p2}, Landroid/app/admin/DeviceAdminInfo;->usesPolicy(I)Z

    #@83
    move-result v8

    #@84
    if-nez v8, :cond_d4

    #@86
    .line 676
    new-instance v8, Ljava/lang/SecurityException;

    #@88
    new-instance v9, Ljava/lang/StringBuilder;

    #@8a
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@8d
    const-string v10, "Admin "

    #@8f
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@92
    move-result-object v9

    #@93
    iget-object v10, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->info:Landroid/app/admin/DeviceAdminInfo;

    #@95
    invoke-virtual {v10}, Landroid/app/admin/DeviceAdminInfo;->getComponent()Landroid/content/ComponentName;

    #@98
    move-result-object v10

    #@99
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v9

    #@9d
    const-string v10, " did not specify uses-policy for: "

    #@9f
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v9

    #@a3
    iget-object v10, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->info:Landroid/app/admin/DeviceAdminInfo;

    #@a5
    invoke-virtual {v10, p2}, Landroid/app/admin/DeviceAdminInfo;->getTagForPolicy(I)Ljava/lang/String;

    #@a8
    move-result-object v10

    #@a9
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v9

    #@ad
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b0
    move-result-object v9

    #@b1
    invoke-direct {v8, v9}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@b4
    throw v8

    #@b5
    .line 682
    .end local v1           #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    .end local v6           #signed:Z
    :cond_b5
    iget-object v8, v5, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@b7
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    #@ba
    move-result v0

    #@bb
    .line 683
    .local v0, N:I
    const/4 v4, 0x0

    #@bc
    .local v4, i:I
    :goto_bc
    if-ge v4, v0, :cond_d8

    #@be
    .line 684
    iget-object v8, v5, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@c0
    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@c3
    move-result-object v1

    #@c4
    check-cast v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@c6
    .line 685
    .restart local v1       #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    invoke-virtual {v1}, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->getUid()I

    #@c9
    move-result v8

    #@ca
    if-ne v8, v2, :cond_d5

    #@cc
    iget-object v8, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->info:Landroid/app/admin/DeviceAdminInfo;

    #@ce
    invoke-virtual {v8, p2}, Landroid/app/admin/DeviceAdminInfo;->usesPolicy(I)Z

    #@d1
    move-result v8

    #@d2
    if-eqz v8, :cond_d5

    #@d4
    .line 686
    .end local v0           #N:I
    .end local v4           #i:I
    :cond_d4
    return-object v1

    #@d5
    .line 683
    .restart local v0       #N:I
    .restart local v4       #i:I
    :cond_d5
    add-int/lit8 v4, v4, 0x1

    #@d7
    goto :goto_bc

    #@d8
    .line 689
    .end local v1           #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_d8
    new-instance v8, Ljava/lang/SecurityException;

    #@da
    new-instance v9, Ljava/lang/StringBuilder;

    #@dc
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@df
    const-string v10, "No active admin owned by uid "

    #@e1
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e4
    move-result-object v9

    #@e5
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@e8
    move-result v10

    #@e9
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ec
    move-result-object v9

    #@ed
    const-string v10, " for policy #"

    #@ef
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f2
    move-result-object v9

    #@f3
    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f6
    move-result-object v9

    #@f7
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fa
    move-result-object v9

    #@fb
    invoke-direct {v8, v9}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@fe
    throw v8

    #@ff
    .line 670
    .end local v0           #N:I
    .end local v4           #i:I
    .restart local v1       #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    .restart local v3       #dm:Landroid/os/IDeviceManager3LM;
    .restart local v6       #signed:Z
    :catch_ff
    move-exception v8

    #@100
    goto/16 :goto_7c
.end method

.method getActiveAdminUncheckedLocked(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    .registers 6
    .parameter "who"
    .parameter "userHandle"

    #@0
    .prologue
    .line 634
    invoke-virtual {p0, p2}, Lcom/android/server/DevicePolicyManagerService;->getUserData(I)Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;

    #@3
    move-result-object v1

    #@4
    iget-object v1, v1, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminMap:Ljava/util/HashMap;

    #@6
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@c
    .line 635
    .local v0, admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    if-eqz v0, :cond_33

    #@e
    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@11
    move-result-object v1

    #@12
    iget-object v2, v0, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->info:Landroid/app/admin/DeviceAdminInfo;

    #@14
    invoke-virtual {v2}, Landroid/app/admin/DeviceAdminInfo;->getActivityInfo()Landroid/content/pm/ActivityInfo;

    #@17
    move-result-object v2

    #@18
    iget-object v2, v2, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    #@1a
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1d
    move-result v1

    #@1e
    if-eqz v1, :cond_33

    #@20
    invoke-virtual {p1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    #@23
    move-result-object v1

    #@24
    iget-object v2, v0, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->info:Landroid/app/admin/DeviceAdminInfo;

    #@26
    invoke-virtual {v2}, Landroid/app/admin/DeviceAdminInfo;->getActivityInfo()Landroid/content/pm/ActivityInfo;

    #@29
    move-result-object v2

    #@2a
    iget-object v2, v2, Landroid/content/pm/ComponentInfo;->name:Ljava/lang/String;

    #@2c
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2f
    move-result v1

    #@30
    if-eqz v1, :cond_33

    #@32
    .line 640
    .end local v0           #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :goto_32
    return-object v0

    #@33
    .restart local v0       #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_33
    const/4 v0, 0x0

    #@34
    goto :goto_32
.end method

.method public getActiveAdmins(I)Ljava/util/List;
    .registers 7
    .parameter "userHandle"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1138
    invoke-direct {p0, p1}, Lcom/android/server/DevicePolicyManagerService;->enforceCrossUserPermission(I)V

    #@3
    .line 1139
    monitor-enter p0

    #@4
    .line 1140
    :try_start_4
    invoke-virtual {p0, p1}, Lcom/android/server/DevicePolicyManagerService;->getUserData(I)Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;

    #@7
    move-result-object v2

    #@8
    .line 1141
    .local v2, policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    iget-object v4, v2, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@a
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@d
    move-result v0

    #@e
    .line 1142
    .local v0, N:I
    if-gtz v0, :cond_13

    #@10
    .line 1143
    const/4 v3, 0x0

    #@11
    monitor-exit p0

    #@12
    .line 1149
    :goto_12
    return-object v3

    #@13
    .line 1145
    :cond_13
    new-instance v3, Ljava/util/ArrayList;

    #@15
    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    #@18
    .line 1146
    .local v3, res:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ComponentName;>;"
    const/4 v1, 0x0

    #@19
    .local v1, i:I
    :goto_19
    if-ge v1, v0, :cond_2f

    #@1b
    .line 1147
    iget-object v4, v2, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@1d
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@20
    move-result-object v4

    #@21
    check-cast v4, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@23
    iget-object v4, v4, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->info:Landroid/app/admin/DeviceAdminInfo;

    #@25
    invoke-virtual {v4}, Landroid/app/admin/DeviceAdminInfo;->getComponent()Landroid/content/ComponentName;

    #@28
    move-result-object v4

    #@29
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2c
    .line 1146
    add-int/lit8 v1, v1, 0x1

    #@2e
    goto :goto_19

    #@2f
    .line 1149
    :cond_2f
    monitor-exit p0

    #@30
    goto :goto_12

    #@31
    .line 1150
    .end local v0           #N:I
    .end local v1           #i:I
    .end local v2           #policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    .end local v3           #res:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ComponentName;>;"
    :catchall_31
    move-exception v4

    #@32
    monitor-exit p0
    :try_end_33
    .catchall {:try_start_4 .. :try_end_33} :catchall_31

    #@33
    throw v4
.end method

.method public getCameraDisabled(Landroid/content/ComponentName;I)Z
    .registers 9
    .parameter "who"
    .parameter "userHandle"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 2428
    monitor-enter p0

    #@2
    .line 2429
    if-eqz p1, :cond_e

    #@4
    .line 2430
    :try_start_4
    invoke-virtual {p0, p1, p2}, Lcom/android/server/DevicePolicyManagerService;->getActiveAdminUncheckedLocked(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@7
    move-result-object v1

    #@8
    .line 2431
    .local v1, admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    if-eqz v1, :cond_c

    #@a
    iget-boolean v4, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->disableCamera:Z

    #@c
    :cond_c
    monitor-exit p0

    #@d
    .line 2443
    .end local v1           #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :goto_d
    return v4

    #@e
    .line 2434
    :cond_e
    invoke-virtual {p0, p2}, Lcom/android/server/DevicePolicyManagerService;->getUserData(I)Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;

    #@11
    move-result-object v3

    #@12
    .line 2436
    .local v3, policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    iget-object v5, v3, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@14
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@17
    move-result v0

    #@18
    .line 2437
    .local v0, N:I
    const/4 v2, 0x0

    #@19
    .local v2, i:I
    :goto_19
    if-ge v2, v0, :cond_30

    #@1b
    .line 2438
    iget-object v5, v3, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@1d
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@20
    move-result-object v1

    #@21
    check-cast v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@23
    .line 2439
    .restart local v1       #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    iget-boolean v5, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->disableCamera:Z

    #@25
    if-eqz v5, :cond_2d

    #@27
    .line 2440
    const/4 v4, 0x1

    #@28
    monitor-exit p0

    #@29
    goto :goto_d

    #@2a
    .line 2444
    .end local v0           #N:I
    .end local v1           #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    .end local v2           #i:I
    .end local v3           #policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    :catchall_2a
    move-exception v4

    #@2b
    monitor-exit p0
    :try_end_2c
    .catchall {:try_start_4 .. :try_end_2c} :catchall_2a

    #@2c
    throw v4

    #@2d
    .line 2437
    .restart local v0       #N:I
    .restart local v1       #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    .restart local v2       #i:I
    .restart local v3       #policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    :cond_2d
    add-int/lit8 v2, v2, 0x1

    #@2f
    goto :goto_19

    #@30
    .line 2443
    .end local v1           #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_30
    :try_start_30
    monitor-exit p0
    :try_end_31
    .catchall {:try_start_30 .. :try_end_31} :catchall_2a

    #@31
    goto :goto_d
.end method

.method public getCurrentFailedPasswordAttempts(I)I
    .registers 4
    .parameter "userHandle"

    #@0
    .prologue
    .line 1629
    invoke-direct {p0, p1}, Lcom/android/server/DevicePolicyManagerService;->enforceCrossUserPermission(I)V

    #@3
    .line 1630
    monitor-enter p0

    #@4
    .line 1633
    const/4 v0, 0x0

    #@5
    const/4 v1, 0x1

    #@6
    :try_start_6
    invoke-virtual {p0, v0, v1}, Lcom/android/server/DevicePolicyManagerService;->getActiveAdminForCallerLocked(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@9
    .line 1635
    invoke-virtual {p0, p1}, Lcom/android/server/DevicePolicyManagerService;->getUserData(I)Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;

    #@c
    move-result-object v0

    #@d
    iget v0, v0, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mFailedPasswordAttempts:I

    #@f
    monitor-exit p0

    #@10
    return v0

    #@11
    .line 1636
    :catchall_11
    move-exception v0

    #@12
    monitor-exit p0
    :try_end_13
    .catchall {:try_start_6 .. :try_end_13} :catchall_11

    #@13
    throw v0
.end method

.method public getCurrentFailedPasswordAttemptsMDM(I)I
    .registers 4
    .parameter "userHandle"

    #@0
    .prologue
    .line 1641
    monitor-enter p0

    #@1
    .line 1642
    const/4 v1, 0x0

    #@2
    :try_start_2
    invoke-virtual {p0, v1, p1}, Lcom/android/server/DevicePolicyManagerService;->getMaximumFailedPasswordsForWipe(Landroid/content/ComponentName;I)I

    #@5
    move-result v0

    #@6
    .line 1643
    .local v0, failedMax:I
    if-lez v0, :cond_10

    #@8
    .line 1644
    invoke-virtual {p0, p1}, Lcom/android/server/DevicePolicyManagerService;->getUserData(I)Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;

    #@b
    move-result-object v1

    #@c
    iget v1, v1, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mFailedPasswordAttempts:I

    #@e
    monitor-exit p0

    #@f
    .line 1646
    :goto_f
    return v1

    #@10
    :cond_10
    const/4 v1, -0x1

    #@11
    monitor-exit p0

    #@12
    goto :goto_f

    #@13
    .line 1647
    .end local v0           #failedMax:I
    :catchall_13
    move-exception v1

    #@14
    monitor-exit p0
    :try_end_15
    .catchall {:try_start_2 .. :try_end_15} :catchall_13

    #@15
    throw v1
.end method

.method public getGlobalProxyAdmin(I)Landroid/content/ComponentName;
    .registers 7
    .parameter "userHandle"

    #@0
    .prologue
    .line 2222
    invoke-direct {p0, p1}, Lcom/android/server/DevicePolicyManagerService;->enforceCrossUserPermission(I)V

    #@3
    .line 2223
    monitor-enter p0

    #@4
    .line 2224
    const/4 v4, 0x0

    #@5
    :try_start_5
    invoke-virtual {p0, v4}, Lcom/android/server/DevicePolicyManagerService;->getUserData(I)Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;

    #@8
    move-result-object v3

    #@9
    .line 2227
    .local v3, policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    iget-object v4, v3, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@e
    move-result v0

    #@f
    .line 2228
    .local v0, N:I
    const/4 v2, 0x0

    #@10
    .local v2, i:I
    :goto_10
    if-ge v2, v0, :cond_29

    #@12
    .line 2229
    iget-object v4, v3, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@14
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@17
    move-result-object v1

    #@18
    check-cast v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@1a
    .line 2230
    .local v1, ap:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    iget-boolean v4, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->specifiesGlobalProxy:Z

    #@1c
    if-eqz v4, :cond_26

    #@1e
    .line 2233
    iget-object v4, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->info:Landroid/app/admin/DeviceAdminInfo;

    #@20
    invoke-virtual {v4}, Landroid/app/admin/DeviceAdminInfo;->getComponent()Landroid/content/ComponentName;

    #@23
    move-result-object v4

    #@24
    monitor-exit p0

    #@25
    .line 2238
    .end local v1           #ap:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :goto_25
    return-object v4

    #@26
    .line 2228
    .restart local v1       #ap:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_26
    add-int/lit8 v2, v2, 0x1

    #@28
    goto :goto_10

    #@29
    .line 2236
    .end local v1           #ap:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_29
    monitor-exit p0

    #@2a
    .line 2238
    const/4 v4, 0x0

    #@2b
    goto :goto_25

    #@2c
    .line 2236
    .end local v0           #N:I
    .end local v2           #i:I
    .end local v3           #policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    :catchall_2c
    move-exception v4

    #@2d
    monitor-exit p0
    :try_end_2e
    .catchall {:try_start_5 .. :try_end_2e} :catchall_2c

    #@2e
    throw v4
.end method

.method public getKeyguardDisabledFeatures(Landroid/content/ComponentName;I)I
    .registers 9
    .parameter "who"
    .parameter "userHandle"

    #@0
    .prologue
    .line 2471
    invoke-direct {p0, p2}, Lcom/android/server/DevicePolicyManagerService;->enforceCrossUserPermission(I)V

    #@3
    .line 2472
    monitor-enter p0

    #@4
    .line 2473
    if-eqz p1, :cond_12

    #@6
    .line 2474
    :try_start_6
    invoke-virtual {p0, p1, p2}, Lcom/android/server/DevicePolicyManagerService;->getActiveAdminUncheckedLocked(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@9
    move-result-object v1

    #@a
    .line 2475
    .local v1, admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    if-eqz v1, :cond_10

    #@c
    iget v4, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->disabledKeyguardFeatures:I

    #@e
    :goto_e
    monitor-exit p0

    #@f
    .line 2486
    .end local v1           #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :goto_f
    return v4

    #@10
    .line 2475
    .restart local v1       #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_10
    const/4 v4, 0x0

    #@11
    goto :goto_e

    #@12
    .line 2479
    .end local v1           #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_12
    invoke-virtual {p0, p2}, Lcom/android/server/DevicePolicyManagerService;->getUserData(I)Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;

    #@15
    move-result-object v3

    #@16
    .line 2480
    .local v3, policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    iget-object v5, v3, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@18
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@1b
    move-result v0

    #@1c
    .line 2481
    .local v0, N:I
    const/4 v4, 0x0

    #@1d
    .line 2482
    .local v4, which:I
    const/4 v2, 0x0

    #@1e
    .local v2, i:I
    :goto_1e
    if-ge v2, v0, :cond_2e

    #@20
    .line 2483
    iget-object v5, v3, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@22
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@25
    move-result-object v1

    #@26
    check-cast v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@28
    .line 2484
    .restart local v1       #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    iget v5, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->disabledKeyguardFeatures:I

    #@2a
    or-int/2addr v4, v5

    #@2b
    .line 2482
    add-int/lit8 v2, v2, 0x1

    #@2d
    goto :goto_1e

    #@2e
    .line 2486
    .end local v1           #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_2e
    monitor-exit p0

    #@2f
    goto :goto_f

    #@30
    .line 2487
    .end local v0           #N:I
    .end local v2           #i:I
    .end local v3           #policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    .end local v4           #which:I
    :catchall_30
    move-exception v5

    #@31
    monitor-exit p0
    :try_end_32
    .catchall {:try_start_6 .. :try_end_32} :catchall_30

    #@32
    throw v5
.end method

.method public getMaximumFailedPasswordsForWipe(Landroid/content/ComponentName;I)I
    .registers 9
    .parameter "who"
    .parameter "userHandle"

    #@0
    .prologue
    .line 1668
    invoke-direct {p0, p2}, Lcom/android/server/DevicePolicyManagerService;->enforceCrossUserPermission(I)V

    #@3
    .line 1669
    monitor-enter p0

    #@4
    .line 1670
    :try_start_4
    invoke-virtual {p0, p2}, Lcom/android/server/DevicePolicyManagerService;->getUserData(I)Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;

    #@7
    move-result-object v4

    #@8
    .line 1671
    .local v4, policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    const/4 v2, 0x0

    #@9
    .line 1673
    .local v2, count:I
    if-eqz p1, :cond_17

    #@b
    .line 1674
    invoke-virtual {p0, p1, p2}, Lcom/android/server/DevicePolicyManagerService;->getActiveAdminUncheckedLocked(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@e
    move-result-object v1

    #@f
    .line 1675
    .local v1, admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    if-eqz v1, :cond_15

    #@11
    iget v5, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->maximumFailedPasswordsForWipe:I

    #@13
    :goto_13
    monitor-exit p0

    #@14
    .line 1688
    .end local v1           #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :goto_14
    return v5

    #@15
    .restart local v1       #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_15
    move v5, v2

    #@16
    .line 1675
    goto :goto_13

    #@17
    .line 1678
    .end local v1           #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_17
    iget-object v5, v4, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@19
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@1c
    move-result v0

    #@1d
    .line 1679
    .local v0, N:I
    const/4 v3, 0x0

    #@1e
    .local v3, i:I
    :goto_1e
    if-ge v3, v0, :cond_3a

    #@20
    .line 1680
    iget-object v5, v4, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@22
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@25
    move-result-object v1

    #@26
    check-cast v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@28
    .line 1681
    .restart local v1       #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    if-nez v2, :cond_2f

    #@2a
    .line 1682
    iget v2, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->maximumFailedPasswordsForWipe:I

    #@2c
    .line 1679
    :cond_2c
    :goto_2c
    add-int/lit8 v3, v3, 0x1

    #@2e
    goto :goto_1e

    #@2f
    .line 1683
    :cond_2f
    iget v5, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->maximumFailedPasswordsForWipe:I

    #@31
    if-eqz v5, :cond_2c

    #@33
    iget v5, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->maximumFailedPasswordsForWipe:I

    #@35
    if-le v2, v5, :cond_2c

    #@37
    .line 1685
    iget v2, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->maximumFailedPasswordsForWipe:I

    #@39
    goto :goto_2c

    #@3a
    .line 1688
    .end local v1           #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_3a
    monitor-exit p0

    #@3b
    move v5, v2

    #@3c
    goto :goto_14

    #@3d
    .line 1689
    .end local v0           #N:I
    .end local v2           #count:I
    .end local v3           #i:I
    .end local v4           #policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    :catchall_3d
    move-exception v5

    #@3e
    monitor-exit p0
    :try_end_3f
    .catchall {:try_start_4 .. :try_end_3f} :catchall_3d

    #@3f
    throw v5
.end method

.method public getMaximumTimeToLock(Landroid/content/ComponentName;I)J
    .registers 13
    .parameter "who"
    .parameter "userHandle"

    #@0
    .prologue
    const-wide/16 v8, 0x0

    #@2
    .line 1883
    invoke-direct {p0, p2}, Lcom/android/server/DevicePolicyManagerService;->enforceCrossUserPermission(I)V

    #@5
    .line 1884
    monitor-enter p0

    #@6
    .line 1885
    const-wide/16 v4, 0x0

    #@8
    .line 1887
    .local v4, time:J
    if-eqz p1, :cond_16

    #@a
    .line 1888
    :try_start_a
    invoke-virtual {p0, p1, p2}, Lcom/android/server/DevicePolicyManagerService;->getActiveAdminUncheckedLocked(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@d
    move-result-object v1

    #@e
    .line 1889
    .local v1, admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    if-eqz v1, :cond_14

    #@10
    iget-wide v6, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->maximumTimeToUnlock:J

    #@12
    :goto_12
    monitor-exit p0

    #@13
    .line 1903
    .end local v1           #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :goto_13
    return-wide v6

    #@14
    .restart local v1       #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_14
    move-wide v6, v4

    #@15
    .line 1889
    goto :goto_12

    #@16
    .line 1892
    .end local v1           #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_16
    invoke-virtual {p0, p2}, Lcom/android/server/DevicePolicyManagerService;->getUserData(I)Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;

    #@19
    move-result-object v3

    #@1a
    .line 1893
    .local v3, policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    iget-object v6, v3, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@1c
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@1f
    move-result v0

    #@20
    .line 1894
    .local v0, N:I
    const/4 v2, 0x0

    #@21
    .local v2, i:I
    :goto_21
    if-ge v2, v0, :cond_43

    #@23
    .line 1895
    iget-object v6, v3, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@25
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@28
    move-result-object v1

    #@29
    check-cast v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@2b
    .line 1896
    .restart local v1       #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    cmp-long v6, v4, v8

    #@2d
    if-nez v6, :cond_34

    #@2f
    .line 1897
    iget-wide v4, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->maximumTimeToUnlock:J

    #@31
    .line 1894
    :cond_31
    :goto_31
    add-int/lit8 v2, v2, 0x1

    #@33
    goto :goto_21

    #@34
    .line 1898
    :cond_34
    iget-wide v6, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->maximumTimeToUnlock:J

    #@36
    cmp-long v6, v6, v8

    #@38
    if-eqz v6, :cond_31

    #@3a
    iget-wide v6, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->maximumTimeToUnlock:J

    #@3c
    cmp-long v6, v4, v6

    #@3e
    if-lez v6, :cond_31

    #@40
    .line 1900
    iget-wide v4, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->maximumTimeToUnlock:J

    #@42
    goto :goto_31

    #@43
    .line 1903
    .end local v1           #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_43
    monitor-exit p0

    #@44
    move-wide v6, v4

    #@45
    goto :goto_13

    #@46
    .line 1904
    .end local v0           #N:I
    .end local v2           #i:I
    .end local v3           #policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    :catchall_46
    move-exception v6

    #@47
    monitor-exit p0
    :try_end_48
    .catchall {:try_start_a .. :try_end_48} :catchall_46

    #@48
    throw v6
.end method

.method public getPasswordExpiration(Landroid/content/ComponentName;I)J
    .registers 5
    .parameter "who"
    .parameter "userHandle"

    #@0
    .prologue
    .line 1376
    invoke-direct {p0, p2}, Lcom/android/server/DevicePolicyManagerService;->enforceCrossUserPermission(I)V

    #@3
    .line 1377
    monitor-enter p0

    #@4
    .line 1378
    :try_start_4
    invoke-direct {p0, p1, p2}, Lcom/android/server/DevicePolicyManagerService;->getPasswordExpirationLocked(Landroid/content/ComponentName;I)J

    #@7
    move-result-wide v0

    #@8
    monitor-exit p0

    #@9
    return-wide v0

    #@a
    .line 1379
    :catchall_a
    move-exception v0

    #@b
    monitor-exit p0
    :try_end_c
    .catchall {:try_start_4 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method

.method public getPasswordExpirationTimeout(Landroid/content/ComponentName;I)J
    .registers 13
    .parameter "who"
    .parameter "userHandle"

    #@0
    .prologue
    const-wide/16 v6, 0x0

    #@2
    .line 1331
    invoke-direct {p0, p2}, Lcom/android/server/DevicePolicyManagerService;->enforceCrossUserPermission(I)V

    #@5
    .line 1332
    monitor-enter p0

    #@6
    .line 1333
    if-eqz p1, :cond_14

    #@8
    .line 1334
    :try_start_8
    invoke-virtual {p0, p1, p2}, Lcom/android/server/DevicePolicyManagerService;->getActiveAdminUncheckedLocked(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@b
    move-result-object v1

    #@c
    .line 1335
    .local v1, admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    if-eqz v1, :cond_12

    #@e
    iget-wide v4, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->passwordExpirationTimeout:J

    #@10
    :goto_10
    monitor-exit p0

    #@11
    .line 1348
    .end local v1           #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :goto_11
    return-wide v4

    #@12
    .restart local v1       #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_12
    move-wide v4, v6

    #@13
    .line 1335
    goto :goto_10

    #@14
    .line 1338
    .end local v1           #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_14
    const-wide/16 v4, 0x0

    #@16
    .line 1339
    .local v4, timeout:J
    invoke-virtual {p0, p2}, Lcom/android/server/DevicePolicyManagerService;->getUserData(I)Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;

    #@19
    move-result-object v3

    #@1a
    .line 1340
    .local v3, policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    iget-object v8, v3, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@1c
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    #@1f
    move-result v0

    #@20
    .line 1341
    .local v0, N:I
    const/4 v2, 0x0

    #@21
    .local v2, i:I
    :goto_21
    if-ge v2, v0, :cond_40

    #@23
    .line 1342
    iget-object v8, v3, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@25
    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@28
    move-result-object v1

    #@29
    check-cast v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@2b
    .line 1343
    .restart local v1       #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    cmp-long v8, v4, v6

    #@2d
    if-eqz v8, :cond_3b

    #@2f
    iget-wide v8, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->passwordExpirationTimeout:J

    #@31
    cmp-long v8, v8, v6

    #@33
    if-eqz v8, :cond_3d

    #@35
    iget-wide v8, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->passwordExpirationTimeout:J

    #@37
    cmp-long v8, v4, v8

    #@39
    if-lez v8, :cond_3d

    #@3b
    .line 1345
    :cond_3b
    iget-wide v4, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->passwordExpirationTimeout:J

    #@3d
    .line 1341
    :cond_3d
    add-int/lit8 v2, v2, 0x1

    #@3f
    goto :goto_21

    #@40
    .line 1348
    .end local v1           #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_40
    monitor-exit p0

    #@41
    goto :goto_11

    #@42
    .line 1349
    .end local v0           #N:I
    .end local v2           #i:I
    .end local v3           #policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    .end local v4           #timeout:J
    :catchall_42
    move-exception v6

    #@43
    monitor-exit p0
    :try_end_44
    .catchall {:try_start_8 .. :try_end_44} :catchall_42

    #@44
    throw v6
.end method

.method public getPasswordHistoryLength(Landroid/content/ComponentName;I)I
    .registers 9
    .parameter "who"
    .parameter "userHandle"

    #@0
    .prologue
    .line 1279
    invoke-direct {p0, p2}, Lcom/android/server/DevicePolicyManagerService;->enforceCrossUserPermission(I)V

    #@3
    .line 1280
    monitor-enter p0

    #@4
    .line 1281
    :try_start_4
    invoke-virtual {p0, p2}, Lcom/android/server/DevicePolicyManagerService;->getUserData(I)Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;

    #@7
    move-result-object v4

    #@8
    .line 1282
    .local v4, policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    const/4 v3, 0x0

    #@9
    .line 1284
    .local v3, length:I
    if-eqz p1, :cond_17

    #@b
    .line 1285
    invoke-virtual {p0, p1, p2}, Lcom/android/server/DevicePolicyManagerService;->getActiveAdminUncheckedLocked(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@e
    move-result-object v1

    #@f
    .line 1286
    .local v1, admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    if-eqz v1, :cond_15

    #@11
    iget v5, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->passwordHistoryLength:I

    #@13
    :goto_13
    monitor-exit p0

    #@14
    .line 1296
    .end local v1           #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :goto_14
    return v5

    #@15
    .restart local v1       #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_15
    move v5, v3

    #@16
    .line 1286
    goto :goto_13

    #@17
    .line 1289
    .end local v1           #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_17
    iget-object v5, v4, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@19
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@1c
    move-result v0

    #@1d
    .line 1290
    .local v0, N:I
    const/4 v2, 0x0

    #@1e
    .local v2, i:I
    :goto_1e
    if-ge v2, v0, :cond_31

    #@20
    .line 1291
    iget-object v5, v4, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@22
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@25
    move-result-object v1

    #@26
    check-cast v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@28
    .line 1292
    .restart local v1       #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    iget v5, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->passwordHistoryLength:I

    #@2a
    if-ge v3, v5, :cond_2e

    #@2c
    .line 1293
    iget v3, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->passwordHistoryLength:I

    #@2e
    .line 1290
    :cond_2e
    add-int/lit8 v2, v2, 0x1

    #@30
    goto :goto_1e

    #@31
    .line 1296
    .end local v1           #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_31
    monitor-exit p0

    #@32
    move v5, v3

    #@33
    goto :goto_14

    #@34
    .line 1297
    .end local v0           #N:I
    .end local v2           #i:I
    .end local v3           #length:I
    .end local v4           #policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    :catchall_34
    move-exception v5

    #@35
    monitor-exit p0
    :try_end_36
    .catchall {:try_start_4 .. :try_end_36} :catchall_34

    #@36
    throw v5
.end method

.method public getPasswordMinimumLength(Landroid/content/ComponentName;I)I
    .registers 9
    .parameter "who"
    .parameter "userHandle"

    #@0
    .prologue
    .line 1242
    invoke-direct {p0, p2}, Lcom/android/server/DevicePolicyManagerService;->enforceCrossUserPermission(I)V

    #@3
    .line 1243
    monitor-enter p0

    #@4
    .line 1244
    :try_start_4
    invoke-virtual {p0, p2}, Lcom/android/server/DevicePolicyManagerService;->getUserData(I)Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;

    #@7
    move-result-object v4

    #@8
    .line 1245
    .local v4, policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    const/4 v3, 0x0

    #@9
    .line 1247
    .local v3, length:I
    if-eqz p1, :cond_17

    #@b
    .line 1248
    invoke-virtual {p0, p1, p2}, Lcom/android/server/DevicePolicyManagerService;->getActiveAdminUncheckedLocked(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@e
    move-result-object v1

    #@f
    .line 1249
    .local v1, admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    if-eqz v1, :cond_15

    #@11
    iget v5, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->minimumPasswordLength:I

    #@13
    :goto_13
    monitor-exit p0

    #@14
    .line 1259
    .end local v1           #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :goto_14
    return v5

    #@15
    .restart local v1       #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_15
    move v5, v3

    #@16
    .line 1249
    goto :goto_13

    #@17
    .line 1252
    .end local v1           #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_17
    iget-object v5, v4, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@19
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@1c
    move-result v0

    #@1d
    .line 1253
    .local v0, N:I
    const/4 v2, 0x0

    #@1e
    .local v2, i:I
    :goto_1e
    if-ge v2, v0, :cond_31

    #@20
    .line 1254
    iget-object v5, v4, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@22
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@25
    move-result-object v1

    #@26
    check-cast v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@28
    .line 1255
    .restart local v1       #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    iget v5, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->minimumPasswordLength:I

    #@2a
    if-ge v3, v5, :cond_2e

    #@2c
    .line 1256
    iget v3, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->minimumPasswordLength:I

    #@2e
    .line 1253
    :cond_2e
    add-int/lit8 v2, v2, 0x1

    #@30
    goto :goto_1e

    #@31
    .line 1259
    .end local v1           #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_31
    monitor-exit p0

    #@32
    move v5, v3

    #@33
    goto :goto_14

    #@34
    .line 1260
    .end local v0           #N:I
    .end local v2           #i:I
    .end local v3           #length:I
    .end local v4           #policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    :catchall_34
    move-exception v5

    #@35
    monitor-exit p0
    :try_end_36
    .catchall {:try_start_4 .. :try_end_36} :catchall_34

    #@36
    throw v5
.end method

.method public getPasswordMinimumLetters(Landroid/content/ComponentName;I)I
    .registers 9
    .parameter "who"
    .parameter "userHandle"

    #@0
    .prologue
    .line 1472
    invoke-direct {p0, p2}, Lcom/android/server/DevicePolicyManagerService;->enforceCrossUserPermission(I)V

    #@3
    .line 1473
    monitor-enter p0

    #@4
    .line 1474
    const/4 v3, 0x0

    #@5
    .line 1476
    .local v3, length:I
    if-eqz p1, :cond_13

    #@7
    .line 1477
    :try_start_7
    invoke-virtual {p0, p1, p2}, Lcom/android/server/DevicePolicyManagerService;->getActiveAdminUncheckedLocked(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@a
    move-result-object v1

    #@b
    .line 1478
    .local v1, admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    if-eqz v1, :cond_11

    #@d
    iget v5, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->minimumPasswordLetters:I

    #@f
    :goto_f
    monitor-exit p0

    #@10
    .line 1489
    .end local v1           #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :goto_10
    return v5

    #@11
    .restart local v1       #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_11
    move v5, v3

    #@12
    .line 1478
    goto :goto_f

    #@13
    .line 1481
    .end local v1           #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_13
    invoke-virtual {p0, p2}, Lcom/android/server/DevicePolicyManagerService;->getUserData(I)Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;

    #@16
    move-result-object v4

    #@17
    .line 1482
    .local v4, policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    iget-object v5, v4, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@19
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@1c
    move-result v0

    #@1d
    .line 1483
    .local v0, N:I
    const/4 v2, 0x0

    #@1e
    .local v2, i:I
    :goto_1e
    if-ge v2, v0, :cond_31

    #@20
    .line 1484
    iget-object v5, v4, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@22
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@25
    move-result-object v1

    #@26
    check-cast v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@28
    .line 1485
    .restart local v1       #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    iget v5, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->minimumPasswordLetters:I

    #@2a
    if-ge v3, v5, :cond_2e

    #@2c
    .line 1486
    iget v3, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->minimumPasswordLetters:I

    #@2e
    .line 1483
    :cond_2e
    add-int/lit8 v2, v2, 0x1

    #@30
    goto :goto_1e

    #@31
    .line 1489
    .end local v1           #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_31
    monitor-exit p0

    #@32
    move v5, v3

    #@33
    goto :goto_10

    #@34
    .line 1490
    .end local v0           #N:I
    .end local v2           #i:I
    .end local v4           #policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    :catchall_34
    move-exception v5

    #@35
    monitor-exit p0
    :try_end_36
    .catchall {:try_start_7 .. :try_end_36} :catchall_34

    #@36
    throw v5
.end method

.method public getPasswordMinimumLowerCase(Landroid/content/ComponentName;I)I
    .registers 9
    .parameter "who"
    .parameter "userHandle"

    #@0
    .prologue
    .line 1435
    invoke-direct {p0, p2}, Lcom/android/server/DevicePolicyManagerService;->enforceCrossUserPermission(I)V

    #@3
    .line 1436
    monitor-enter p0

    #@4
    .line 1437
    const/4 v3, 0x0

    #@5
    .line 1439
    .local v3, length:I
    if-eqz p1, :cond_13

    #@7
    .line 1440
    :try_start_7
    invoke-virtual {p0, p1, p2}, Lcom/android/server/DevicePolicyManagerService;->getActiveAdminUncheckedLocked(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@a
    move-result-object v1

    #@b
    .line 1441
    .local v1, admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    if-eqz v1, :cond_11

    #@d
    iget v5, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->minimumPasswordLowerCase:I

    #@f
    :goto_f
    monitor-exit p0

    #@10
    .line 1452
    .end local v1           #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :goto_10
    return v5

    #@11
    .restart local v1       #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_11
    move v5, v3

    #@12
    .line 1441
    goto :goto_f

    #@13
    .line 1444
    .end local v1           #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_13
    invoke-virtual {p0, p2}, Lcom/android/server/DevicePolicyManagerService;->getUserData(I)Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;

    #@16
    move-result-object v4

    #@17
    .line 1445
    .local v4, policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    iget-object v5, v4, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@19
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@1c
    move-result v0

    #@1d
    .line 1446
    .local v0, N:I
    const/4 v2, 0x0

    #@1e
    .local v2, i:I
    :goto_1e
    if-ge v2, v0, :cond_31

    #@20
    .line 1447
    iget-object v5, v4, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@22
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@25
    move-result-object v1

    #@26
    check-cast v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@28
    .line 1448
    .restart local v1       #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    iget v5, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->minimumPasswordLowerCase:I

    #@2a
    if-ge v3, v5, :cond_2e

    #@2c
    .line 1449
    iget v3, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->minimumPasswordLowerCase:I

    #@2e
    .line 1446
    :cond_2e
    add-int/lit8 v2, v2, 0x1

    #@30
    goto :goto_1e

    #@31
    .line 1452
    .end local v1           #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_31
    monitor-exit p0

    #@32
    move v5, v3

    #@33
    goto :goto_10

    #@34
    .line 1453
    .end local v0           #N:I
    .end local v2           #i:I
    .end local v4           #policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    :catchall_34
    move-exception v5

    #@35
    monitor-exit p0
    :try_end_36
    .catchall {:try_start_7 .. :try_end_36} :catchall_34

    #@36
    throw v5
.end method

.method public getPasswordMinimumNonLetter(Landroid/content/ComponentName;I)I
    .registers 9
    .parameter "who"
    .parameter "userHandle"

    #@0
    .prologue
    .line 1583
    invoke-direct {p0, p2}, Lcom/android/server/DevicePolicyManagerService;->enforceCrossUserPermission(I)V

    #@3
    .line 1584
    monitor-enter p0

    #@4
    .line 1585
    const/4 v3, 0x0

    #@5
    .line 1587
    .local v3, length:I
    if-eqz p1, :cond_13

    #@7
    .line 1588
    :try_start_7
    invoke-virtual {p0, p1, p2}, Lcom/android/server/DevicePolicyManagerService;->getActiveAdminUncheckedLocked(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@a
    move-result-object v1

    #@b
    .line 1589
    .local v1, admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    if-eqz v1, :cond_11

    #@d
    iget v5, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->minimumPasswordNonLetter:I

    #@f
    :goto_f
    monitor-exit p0

    #@10
    .line 1600
    .end local v1           #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :goto_10
    return v5

    #@11
    .restart local v1       #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_11
    move v5, v3

    #@12
    .line 1589
    goto :goto_f

    #@13
    .line 1592
    .end local v1           #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_13
    invoke-virtual {p0, p2}, Lcom/android/server/DevicePolicyManagerService;->getUserData(I)Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;

    #@16
    move-result-object v4

    #@17
    .line 1593
    .local v4, policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    iget-object v5, v4, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@19
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@1c
    move-result v0

    #@1d
    .line 1594
    .local v0, N:I
    const/4 v2, 0x0

    #@1e
    .local v2, i:I
    :goto_1e
    if-ge v2, v0, :cond_31

    #@20
    .line 1595
    iget-object v5, v4, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@22
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@25
    move-result-object v1

    #@26
    check-cast v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@28
    .line 1596
    .restart local v1       #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    iget v5, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->minimumPasswordNonLetter:I

    #@2a
    if-ge v3, v5, :cond_2e

    #@2c
    .line 1597
    iget v3, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->minimumPasswordNonLetter:I

    #@2e
    .line 1594
    :cond_2e
    add-int/lit8 v2, v2, 0x1

    #@30
    goto :goto_1e

    #@31
    .line 1600
    .end local v1           #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_31
    monitor-exit p0

    #@32
    move v5, v3

    #@33
    goto :goto_10

    #@34
    .line 1601
    .end local v0           #N:I
    .end local v2           #i:I
    .end local v4           #policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    :catchall_34
    move-exception v5

    #@35
    monitor-exit p0
    :try_end_36
    .catchall {:try_start_7 .. :try_end_36} :catchall_34

    #@36
    throw v5
.end method

.method public getPasswordMinimumNumeric(Landroid/content/ComponentName;I)I
    .registers 9
    .parameter "who"
    .parameter "userHandle"

    #@0
    .prologue
    .line 1509
    invoke-direct {p0, p2}, Lcom/android/server/DevicePolicyManagerService;->enforceCrossUserPermission(I)V

    #@3
    .line 1510
    monitor-enter p0

    #@4
    .line 1511
    const/4 v3, 0x0

    #@5
    .line 1513
    .local v3, length:I
    if-eqz p1, :cond_13

    #@7
    .line 1514
    :try_start_7
    invoke-virtual {p0, p1, p2}, Lcom/android/server/DevicePolicyManagerService;->getActiveAdminUncheckedLocked(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@a
    move-result-object v1

    #@b
    .line 1515
    .local v1, admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    if-eqz v1, :cond_11

    #@d
    iget v5, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->minimumPasswordNumeric:I

    #@f
    :goto_f
    monitor-exit p0

    #@10
    .line 1526
    .end local v1           #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :goto_10
    return v5

    #@11
    .restart local v1       #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_11
    move v5, v3

    #@12
    .line 1515
    goto :goto_f

    #@13
    .line 1518
    .end local v1           #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_13
    invoke-virtual {p0, p2}, Lcom/android/server/DevicePolicyManagerService;->getUserData(I)Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;

    #@16
    move-result-object v4

    #@17
    .line 1519
    .local v4, policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    iget-object v5, v4, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@19
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@1c
    move-result v0

    #@1d
    .line 1520
    .local v0, N:I
    const/4 v2, 0x0

    #@1e
    .local v2, i:I
    :goto_1e
    if-ge v2, v0, :cond_31

    #@20
    .line 1521
    iget-object v5, v4, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@22
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@25
    move-result-object v1

    #@26
    check-cast v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@28
    .line 1522
    .restart local v1       #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    iget v5, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->minimumPasswordNumeric:I

    #@2a
    if-ge v3, v5, :cond_2e

    #@2c
    .line 1523
    iget v3, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->minimumPasswordNumeric:I

    #@2e
    .line 1520
    :cond_2e
    add-int/lit8 v2, v2, 0x1

    #@30
    goto :goto_1e

    #@31
    .line 1526
    .end local v1           #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_31
    monitor-exit p0

    #@32
    move v5, v3

    #@33
    goto :goto_10

    #@34
    .line 1527
    .end local v0           #N:I
    .end local v2           #i:I
    .end local v4           #policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    :catchall_34
    move-exception v5

    #@35
    monitor-exit p0
    :try_end_36
    .catchall {:try_start_7 .. :try_end_36} :catchall_34

    #@36
    throw v5
.end method

.method public getPasswordMinimumSymbols(Landroid/content/ComponentName;I)I
    .registers 9
    .parameter "who"
    .parameter "userHandle"

    #@0
    .prologue
    .line 1546
    invoke-direct {p0, p2}, Lcom/android/server/DevicePolicyManagerService;->enforceCrossUserPermission(I)V

    #@3
    .line 1547
    monitor-enter p0

    #@4
    .line 1548
    const/4 v3, 0x0

    #@5
    .line 1550
    .local v3, length:I
    if-eqz p1, :cond_13

    #@7
    .line 1551
    :try_start_7
    invoke-virtual {p0, p1, p2}, Lcom/android/server/DevicePolicyManagerService;->getActiveAdminUncheckedLocked(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@a
    move-result-object v1

    #@b
    .line 1552
    .local v1, admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    if-eqz v1, :cond_11

    #@d
    iget v5, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->minimumPasswordSymbols:I

    #@f
    :goto_f
    monitor-exit p0

    #@10
    .line 1563
    .end local v1           #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :goto_10
    return v5

    #@11
    .restart local v1       #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_11
    move v5, v3

    #@12
    .line 1552
    goto :goto_f

    #@13
    .line 1555
    .end local v1           #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_13
    invoke-virtual {p0, p2}, Lcom/android/server/DevicePolicyManagerService;->getUserData(I)Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;

    #@16
    move-result-object v4

    #@17
    .line 1556
    .local v4, policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    iget-object v5, v4, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@19
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@1c
    move-result v0

    #@1d
    .line 1557
    .local v0, N:I
    const/4 v2, 0x0

    #@1e
    .local v2, i:I
    :goto_1e
    if-ge v2, v0, :cond_31

    #@20
    .line 1558
    iget-object v5, v4, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@22
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@25
    move-result-object v1

    #@26
    check-cast v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@28
    .line 1559
    .restart local v1       #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    iget v5, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->minimumPasswordSymbols:I

    #@2a
    if-ge v3, v5, :cond_2e

    #@2c
    .line 1560
    iget v3, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->minimumPasswordSymbols:I

    #@2e
    .line 1557
    :cond_2e
    add-int/lit8 v2, v2, 0x1

    #@30
    goto :goto_1e

    #@31
    .line 1563
    .end local v1           #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_31
    monitor-exit p0

    #@32
    move v5, v3

    #@33
    goto :goto_10

    #@34
    .line 1564
    .end local v0           #N:I
    .end local v2           #i:I
    .end local v4           #policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    :catchall_34
    move-exception v5

    #@35
    monitor-exit p0
    :try_end_36
    .catchall {:try_start_7 .. :try_end_36} :catchall_34

    #@36
    throw v5
.end method

.method public getPasswordMinimumUpperCase(Landroid/content/ComponentName;I)I
    .registers 9
    .parameter "who"
    .parameter "userHandle"

    #@0
    .prologue
    .line 1398
    invoke-direct {p0, p2}, Lcom/android/server/DevicePolicyManagerService;->enforceCrossUserPermission(I)V

    #@3
    .line 1399
    monitor-enter p0

    #@4
    .line 1400
    const/4 v3, 0x0

    #@5
    .line 1402
    .local v3, length:I
    if-eqz p1, :cond_13

    #@7
    .line 1403
    :try_start_7
    invoke-virtual {p0, p1, p2}, Lcom/android/server/DevicePolicyManagerService;->getActiveAdminUncheckedLocked(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@a
    move-result-object v1

    #@b
    .line 1404
    .local v1, admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    if-eqz v1, :cond_11

    #@d
    iget v5, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->minimumPasswordUpperCase:I

    #@f
    :goto_f
    monitor-exit p0

    #@10
    .line 1415
    .end local v1           #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :goto_10
    return v5

    #@11
    .restart local v1       #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_11
    move v5, v3

    #@12
    .line 1404
    goto :goto_f

    #@13
    .line 1407
    .end local v1           #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_13
    invoke-virtual {p0, p2}, Lcom/android/server/DevicePolicyManagerService;->getUserData(I)Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;

    #@16
    move-result-object v4

    #@17
    .line 1408
    .local v4, policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    iget-object v5, v4, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@19
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@1c
    move-result v0

    #@1d
    .line 1409
    .local v0, N:I
    const/4 v2, 0x0

    #@1e
    .local v2, i:I
    :goto_1e
    if-ge v2, v0, :cond_31

    #@20
    .line 1410
    iget-object v5, v4, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@22
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@25
    move-result-object v1

    #@26
    check-cast v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@28
    .line 1411
    .restart local v1       #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    iget v5, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->minimumPasswordUpperCase:I

    #@2a
    if-ge v3, v5, :cond_2e

    #@2c
    .line 1412
    iget v3, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->minimumPasswordUpperCase:I

    #@2e
    .line 1409
    :cond_2e
    add-int/lit8 v2, v2, 0x1

    #@30
    goto :goto_1e

    #@31
    .line 1415
    .end local v1           #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_31
    monitor-exit p0

    #@32
    move v5, v3

    #@33
    goto :goto_10

    #@34
    .line 1416
    .end local v0           #N:I
    .end local v2           #i:I
    .end local v4           #policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    :catchall_34
    move-exception v5

    #@35
    monitor-exit p0
    :try_end_36
    .catchall {:try_start_7 .. :try_end_36} :catchall_34

    #@36
    throw v5
.end method

.method public getPasswordQuality(Landroid/content/ComponentName;I)I
    .registers 9
    .parameter "who"
    .parameter "userHandle"

    #@0
    .prologue
    .line 1205
    invoke-direct {p0, p2}, Lcom/android/server/DevicePolicyManagerService;->enforceCrossUserPermission(I)V

    #@3
    .line 1206
    monitor-enter p0

    #@4
    .line 1207
    const/4 v3, 0x0

    #@5
    .line 1208
    .local v3, mode:I
    :try_start_5
    invoke-virtual {p0, p2}, Lcom/android/server/DevicePolicyManagerService;->getUserData(I)Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;

    #@8
    move-result-object v4

    #@9
    .line 1210
    .local v4, policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    if-eqz p1, :cond_17

    #@b
    .line 1211
    invoke-virtual {p0, p1, p2}, Lcom/android/server/DevicePolicyManagerService;->getActiveAdminUncheckedLocked(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@e
    move-result-object v1

    #@f
    .line 1212
    .local v1, admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    if-eqz v1, :cond_15

    #@11
    iget v5, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->passwordQuality:I

    #@13
    :goto_13
    monitor-exit p0

    #@14
    .line 1222
    .end local v1           #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :goto_14
    return v5

    #@15
    .restart local v1       #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_15
    move v5, v3

    #@16
    .line 1212
    goto :goto_13

    #@17
    .line 1215
    .end local v1           #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_17
    iget-object v5, v4, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@19
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@1c
    move-result v0

    #@1d
    .line 1216
    .local v0, N:I
    const/4 v2, 0x0

    #@1e
    .local v2, i:I
    :goto_1e
    if-ge v2, v0, :cond_31

    #@20
    .line 1217
    iget-object v5, v4, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@22
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@25
    move-result-object v1

    #@26
    check-cast v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@28
    .line 1218
    .restart local v1       #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    iget v5, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->passwordQuality:I

    #@2a
    if-ge v3, v5, :cond_2e

    #@2c
    .line 1219
    iget v3, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->passwordQuality:I

    #@2e
    .line 1216
    :cond_2e
    add-int/lit8 v2, v2, 0x1

    #@30
    goto :goto_1e

    #@31
    .line 1222
    .end local v1           #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_31
    monitor-exit p0

    #@32
    move v5, v3

    #@33
    goto :goto_14

    #@34
    .line 1223
    .end local v0           #N:I
    .end local v2           #i:I
    .end local v4           #policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    :catchall_34
    move-exception v5

    #@35
    monitor-exit p0
    :try_end_36
    .catchall {:try_start_5 .. :try_end_36} :catchall_34

    #@36
    throw v5
.end method

.method public getRemoveWarning(Landroid/content/ComponentName;Landroid/os/RemoteCallback;I)V
    .registers 14
    .parameter "comp"
    .parameter "result"
    .parameter "userHandle"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 2040
    invoke-direct {p0, p3}, Lcom/android/server/DevicePolicyManagerService;->enforceCrossUserPermission(I)V

    #@4
    .line 2041
    iget-object v0, p0, Lcom/android/server/DevicePolicyManagerService;->mContext:Landroid/content/Context;

    #@6
    const-string v2, "android.permission.BIND_DEVICE_ADMIN"

    #@8
    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@b
    .line 2044
    monitor-enter p0

    #@c
    .line 2045
    :try_start_c
    invoke-virtual {p0, p1, p3}, Lcom/android/server/DevicePolicyManagerService;->getActiveAdminUncheckedLocked(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :try_end_f
    .catchall {:try_start_c .. :try_end_f} :catchall_3e

    #@f
    move-result-object v9

    #@10
    .line 2046
    .local v9, admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    if-nez v9, :cond_18

    #@12
    .line 2048
    const/4 v0, 0x0

    #@13
    :try_start_13
    invoke-virtual {p2, v0}, Landroid/os/RemoteCallback;->sendResult(Landroid/os/Bundle;)V
    :try_end_16
    .catchall {:try_start_13 .. :try_end_16} :catchall_3e
    .catch Landroid/os/RemoteException; {:try_start_13 .. :try_end_16} :catch_41

    #@16
    .line 2051
    :goto_16
    :try_start_16
    monitor-exit p0

    #@17
    .line 2066
    :goto_17
    return-void

    #@18
    .line 2053
    :cond_18
    new-instance v1, Landroid/content/Intent;

    #@1a
    const-string v0, "android.app.action.DEVICE_ADMIN_DISABLE_REQUESTED"

    #@1c
    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@1f
    .line 2054
    .local v1, intent:Landroid/content/Intent;
    iget-object v0, v9, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->info:Landroid/app/admin/DeviceAdminInfo;

    #@21
    invoke-virtual {v0}, Landroid/app/admin/DeviceAdminInfo;->getComponent()Landroid/content/ComponentName;

    #@24
    move-result-object v0

    #@25
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@28
    .line 2055
    iget-object v0, p0, Lcom/android/server/DevicePolicyManagerService;->mContext:Landroid/content/Context;

    #@2a
    new-instance v2, Landroid/os/UserHandle;

    #@2c
    invoke-direct {v2, p3}, Landroid/os/UserHandle;-><init>(I)V

    #@2f
    const/4 v3, 0x0

    #@30
    new-instance v4, Lcom/android/server/DevicePolicyManagerService$4;

    #@32
    invoke-direct {v4, p0, p2}, Lcom/android/server/DevicePolicyManagerService$4;-><init>(Lcom/android/server/DevicePolicyManagerService;Landroid/os/RemoteCallback;)V

    #@35
    const/4 v5, 0x0

    #@36
    const/4 v6, -0x1

    #@37
    const/4 v7, 0x0

    #@38
    const/4 v8, 0x0

    #@39
    invoke-virtual/range {v0 .. v8}, Landroid/content/Context;->sendOrderedBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V

    #@3c
    .line 2065
    monitor-exit p0

    #@3d
    goto :goto_17

    #@3e
    .end local v1           #intent:Landroid/content/Intent;
    .end local v9           #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :catchall_3e
    move-exception v0

    #@3f
    monitor-exit p0
    :try_end_40
    .catchall {:try_start_16 .. :try_end_40} :catchall_3e

    #@40
    throw v0

    #@41
    .line 2049
    .restart local v9       #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :catch_41
    move-exception v0

    #@42
    goto :goto_16
.end method

.method public getStorageEncryption(Landroid/content/ComponentName;I)Z
    .registers 9
    .parameter "who"
    .parameter "userHandle"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 2334
    invoke-direct {p0, p2}, Lcom/android/server/DevicePolicyManagerService;->enforceCrossUserPermission(I)V

    #@4
    .line 2335
    monitor-enter p0

    #@5
    .line 2337
    if-eqz p1, :cond_13

    #@7
    .line 2339
    :try_start_7
    invoke-virtual {p0, p1, p2}, Lcom/android/server/DevicePolicyManagerService;->getActiveAdminUncheckedLocked(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@a
    move-result-object v1

    #@b
    .line 2340
    .local v1, ap:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    if-eqz v1, :cond_11

    #@d
    iget-boolean v4, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->encryptionRequested:Z

    #@f
    :goto_f
    monitor-exit p0

    #@10
    .line 2352
    .end local v1           #ap:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :goto_10
    return v4

    #@11
    .restart local v1       #ap:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_11
    move v4, v5

    #@12
    .line 2340
    goto :goto_f

    #@13
    .line 2345
    .end local v1           #ap:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_13
    invoke-virtual {p0, p2}, Lcom/android/server/DevicePolicyManagerService;->getUserData(I)Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;

    #@16
    move-result-object v3

    #@17
    .line 2346
    .local v3, policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    iget-object v4, v3, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@19
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@1c
    move-result v0

    #@1d
    .line 2347
    .local v0, N:I
    const/4 v2, 0x0

    #@1e
    .local v2, i:I
    :goto_1e
    if-ge v2, v0, :cond_35

    #@20
    .line 2348
    iget-object v4, v3, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@22
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@25
    move-result-object v4

    #@26
    check-cast v4, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@28
    iget-boolean v4, v4, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->encryptionRequested:Z

    #@2a
    if-eqz v4, :cond_32

    #@2c
    .line 2349
    const/4 v4, 0x1

    #@2d
    monitor-exit p0

    #@2e
    goto :goto_10

    #@2f
    .line 2353
    .end local v0           #N:I
    .end local v2           #i:I
    .end local v3           #policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    :catchall_2f
    move-exception v4

    #@30
    monitor-exit p0
    :try_end_31
    .catchall {:try_start_7 .. :try_end_31} :catchall_2f

    #@31
    throw v4

    #@32
    .line 2347
    .restart local v0       #N:I
    .restart local v2       #i:I
    .restart local v3       #policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    :cond_32
    add-int/lit8 v2, v2, 0x1

    #@34
    goto :goto_1e

    #@35
    .line 2352
    :cond_35
    :try_start_35
    monitor-exit p0
    :try_end_36
    .catchall {:try_start_35 .. :try_end_36} :catchall_2f

    #@36
    move v4, v5

    #@37
    goto :goto_10
.end method

.method public getStorageEncryptionStatus(I)I
    .registers 3
    .parameter "userHandle"

    #@0
    .prologue
    .line 2360
    invoke-direct {p0, p1}, Lcom/android/server/DevicePolicyManagerService;->enforceCrossUserPermission(I)V

    #@3
    .line 2361
    invoke-direct {p0}, Lcom/android/server/DevicePolicyManagerService;->getEncryptionStatus()I

    #@6
    move-result v0

    #@7
    return v0
.end method

.method getUserData(I)Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    .registers 4
    .parameter "userHandle"

    #@0
    .prologue
    .line 548
    monitor-enter p0

    #@1
    .line 549
    :try_start_1
    iget-object v1, p0, Lcom/android/server/DevicePolicyManagerService;->mUserData:Landroid/util/SparseArray;

    #@3
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@6
    move-result-object v0

    #@7
    check-cast v0, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;

    #@9
    .line 550
    .local v0, policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    if-nez v0, :cond_18

    #@b
    .line 551
    new-instance v0, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;

    #@d
    .end local v0           #policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    invoke-direct {v0, p1}, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;-><init>(I)V

    #@10
    .line 552
    .restart local v0       #policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    iget-object v1, p0, Lcom/android/server/DevicePolicyManagerService;->mUserData:Landroid/util/SparseArray;

    #@12
    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@15
    .line 553
    invoke-direct {p0, v0, p1}, Lcom/android/server/DevicePolicyManagerService;->loadSettingsLocked(Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;I)V

    #@18
    .line 555
    :cond_18
    monitor-exit p0

    #@19
    return-object v0

    #@1a
    .line 556
    .end local v0           #policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    :catchall_1a
    move-exception v1

    #@1b
    monitor-exit p0
    :try_end_1c
    .catchall {:try_start_1 .. :try_end_1c} :catchall_1a

    #@1c
    throw v1
.end method

.method public hasGrantedPolicy(Landroid/content/ComponentName;II)Z
    .registers 8
    .parameter "adminReceiver"
    .parameter "policyId"
    .parameter "userHandle"

    #@0
    .prologue
    .line 1127
    invoke-direct {p0, p3}, Lcom/android/server/DevicePolicyManagerService;->enforceCrossUserPermission(I)V

    #@3
    .line 1128
    monitor-enter p0

    #@4
    .line 1129
    :try_start_4
    invoke-virtual {p0, p1, p3}, Lcom/android/server/DevicePolicyManagerService;->getActiveAdminUncheckedLocked(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@7
    move-result-object v0

    #@8
    .line 1130
    .local v0, administrator:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    if-nez v0, :cond_26

    #@a
    .line 1131
    new-instance v1, Ljava/lang/SecurityException;

    #@c
    new-instance v2, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v3, "No active admin "

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@22
    throw v1

    #@23
    .line 1134
    .end local v0           #administrator:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :catchall_23
    move-exception v1

    #@24
    monitor-exit p0
    :try_end_25
    .catchall {:try_start_4 .. :try_end_25} :catchall_23

    #@25
    throw v1

    #@26
    .line 1133
    .restart local v0       #administrator:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_26
    :try_start_26
    iget-object v1, v0, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->info:Landroid/app/admin/DeviceAdminInfo;

    #@28
    invoke-virtual {v1, p2}, Landroid/app/admin/DeviceAdminInfo;->usesPolicy(I)Z

    #@2b
    move-result v1

    #@2c
    monitor-exit p0
    :try_end_2d
    .catchall {:try_start_26 .. :try_end_2d} :catchall_23

    #@2d
    return v1
.end method

.method public isActivePasswordSufficient(I)Z
    .registers 7
    .parameter "userHandle"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 1605
    invoke-direct {p0, p1}, Lcom/android/server/DevicePolicyManagerService;->enforceCrossUserPermission(I)V

    #@5
    .line 1606
    monitor-enter p0

    #@6
    .line 1607
    :try_start_6
    invoke-virtual {p0, p1}, Lcom/android/server/DevicePolicyManagerService;->getUserData(I)Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;

    #@9
    move-result-object v0

    #@a
    .line 1610
    .local v0, policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    const/4 v3, 0x0

    #@b
    const/4 v4, 0x0

    #@c
    invoke-virtual {p0, v3, v4}, Lcom/android/server/DevicePolicyManagerService;->getActiveAdminForCallerLocked(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@f
    .line 1612
    iget v3, v0, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordQuality:I

    #@11
    const/4 v4, 0x0

    #@12
    invoke-virtual {p0, v4, p1}, Lcom/android/server/DevicePolicyManagerService;->getPasswordQuality(Landroid/content/ComponentName;I)I

    #@15
    move-result v4

    #@16
    if-lt v3, v4, :cond_21

    #@18
    iget v3, v0, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordLength:I

    #@1a
    const/4 v4, 0x0

    #@1b
    invoke-virtual {p0, v4, p1}, Lcom/android/server/DevicePolicyManagerService;->getPasswordMinimumLength(Landroid/content/ComponentName;I)I

    #@1e
    move-result v4

    #@1f
    if-ge v3, v4, :cond_24

    #@21
    .line 1614
    :cond_21
    monitor-exit p0

    #@22
    move v1, v2

    #@23
    .line 1619
    :goto_23
    return v1

    #@24
    .line 1616
    :cond_24
    iget v3, v0, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordQuality:I

    #@26
    const/high16 v4, 0x6

    #@28
    if-eq v3, v4, :cond_2f

    #@2a
    .line 1617
    monitor-exit p0

    #@2b
    goto :goto_23

    #@2c
    .line 1625
    .end local v0           #policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    :catchall_2c
    move-exception v1

    #@2d
    monitor-exit p0
    :try_end_2e
    .catchall {:try_start_6 .. :try_end_2e} :catchall_2c

    #@2e
    throw v1

    #@2f
    .line 1619
    .restart local v0       #policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    :cond_2f
    :try_start_2f
    iget v3, v0, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordUpperCase:I

    #@31
    const/4 v4, 0x0

    #@32
    invoke-virtual {p0, v4, p1}, Lcom/android/server/DevicePolicyManagerService;->getPasswordMinimumUpperCase(Landroid/content/ComponentName;I)I

    #@35
    move-result v4

    #@36
    if-lt v3, v4, :cond_67

    #@38
    iget v3, v0, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordLowerCase:I

    #@3a
    const/4 v4, 0x0

    #@3b
    invoke-virtual {p0, v4, p1}, Lcom/android/server/DevicePolicyManagerService;->getPasswordMinimumLowerCase(Landroid/content/ComponentName;I)I

    #@3e
    move-result v4

    #@3f
    if-lt v3, v4, :cond_67

    #@41
    iget v3, v0, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordLetters:I

    #@43
    const/4 v4, 0x0

    #@44
    invoke-virtual {p0, v4, p1}, Lcom/android/server/DevicePolicyManagerService;->getPasswordMinimumLetters(Landroid/content/ComponentName;I)I

    #@47
    move-result v4

    #@48
    if-lt v3, v4, :cond_67

    #@4a
    iget v3, v0, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordNumeric:I

    #@4c
    const/4 v4, 0x0

    #@4d
    invoke-virtual {p0, v4, p1}, Lcom/android/server/DevicePolicyManagerService;->getPasswordMinimumNumeric(Landroid/content/ComponentName;I)I

    #@50
    move-result v4

    #@51
    if-lt v3, v4, :cond_67

    #@53
    iget v3, v0, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordSymbols:I

    #@55
    const/4 v4, 0x0

    #@56
    invoke-virtual {p0, v4, p1}, Lcom/android/server/DevicePolicyManagerService;->getPasswordMinimumSymbols(Landroid/content/ComponentName;I)I

    #@59
    move-result v4

    #@5a
    if-lt v3, v4, :cond_67

    #@5c
    iget v3, v0, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordNonLetter:I

    #@5e
    const/4 v4, 0x0

    #@5f
    invoke-virtual {p0, v4, p1}, Lcom/android/server/DevicePolicyManagerService;->getPasswordMinimumNonLetter(Landroid/content/ComponentName;I)I

    #@62
    move-result v4

    #@63
    if-lt v3, v4, :cond_67

    #@65
    :goto_65
    monitor-exit p0
    :try_end_66
    .catchall {:try_start_2f .. :try_end_66} :catchall_2c

    #@66
    goto :goto_23

    #@67
    :cond_67
    move v1, v2

    #@68
    goto :goto_65
.end method

.method public isAdminActive(Landroid/content/ComponentName;I)Z
    .registers 4
    .parameter "adminReceiver"
    .parameter "userHandle"

    #@0
    .prologue
    .line 1120
    invoke-direct {p0, p2}, Lcom/android/server/DevicePolicyManagerService;->enforceCrossUserPermission(I)V

    #@3
    .line 1121
    monitor-enter p0

    #@4
    .line 1122
    :try_start_4
    invoke-virtual {p0, p1, p2}, Lcom/android/server/DevicePolicyManagerService;->getActiveAdminUncheckedLocked(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@7
    move-result-object v0

    #@8
    if-eqz v0, :cond_d

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    monitor-exit p0

    #@c
    return v0

    #@d
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_b

    #@f
    .line 1123
    :catchall_f
    move-exception v0

    #@10
    monitor-exit p0
    :try_end_11
    .catchall {:try_start_4 .. :try_end_11} :catchall_f

    #@11
    throw v0
.end method

.method public lockNow()V
    .registers 3

    #@0
    .prologue
    .line 1908
    monitor-enter p0

    #@1
    .line 1911
    const/4 v0, 0x0

    #@2
    const/4 v1, 0x3

    #@3
    :try_start_3
    invoke-virtual {p0, v0, v1}, Lcom/android/server/DevicePolicyManagerService;->getActiveAdminForCallerLocked(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@6
    .line 1913
    invoke-direct {p0}, Lcom/android/server/DevicePolicyManagerService;->lockNowUnchecked()V

    #@9
    .line 1914
    monitor-exit p0

    #@a
    .line 1915
    return-void

    #@b
    .line 1914
    :catchall_b
    move-exception v0

    #@c
    monitor-exit p0
    :try_end_d
    .catchall {:try_start_3 .. :try_end_d} :catchall_b

    #@d
    throw v0
.end method

.method public packageHasActiveAdmins(Ljava/lang/String;I)Z
    .registers 7
    .parameter "packageName"
    .parameter "userHandle"

    #@0
    .prologue
    .line 1154
    invoke-direct {p0, p2}, Lcom/android/server/DevicePolicyManagerService;->enforceCrossUserPermission(I)V

    #@3
    .line 1155
    monitor-enter p0

    #@4
    .line 1156
    :try_start_4
    invoke-virtual {p0, p2}, Lcom/android/server/DevicePolicyManagerService;->getUserData(I)Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;

    #@7
    move-result-object v2

    #@8
    .line 1157
    .local v2, policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    iget-object v3, v2, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@a
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@d
    move-result v0

    #@e
    .line 1158
    .local v0, N:I
    const/4 v1, 0x0

    #@f
    .local v1, i:I
    :goto_f
    if-ge v1, v0, :cond_2b

    #@11
    .line 1159
    iget-object v3, v2, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@13
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@16
    move-result-object v3

    #@17
    check-cast v3, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@19
    iget-object v3, v3, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->info:Landroid/app/admin/DeviceAdminInfo;

    #@1b
    invoke-virtual {v3}, Landroid/app/admin/DeviceAdminInfo;->getPackageName()Ljava/lang/String;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@22
    move-result v3

    #@23
    if-eqz v3, :cond_28

    #@25
    .line 1160
    const/4 v3, 0x1

    #@26
    monitor-exit p0

    #@27
    .line 1163
    :goto_27
    return v3

    #@28
    .line 1158
    :cond_28
    add-int/lit8 v1, v1, 0x1

    #@2a
    goto :goto_f

    #@2b
    .line 1163
    :cond_2b
    const/4 v3, 0x0

    #@2c
    monitor-exit p0

    #@2d
    goto :goto_27

    #@2e
    .line 1164
    .end local v0           #N:I
    .end local v1           #i:I
    .end local v2           #policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    :catchall_2e
    move-exception v3

    #@2f
    monitor-exit p0
    :try_end_30
    .catchall {:try_start_4 .. :try_end_30} :catchall_2e

    #@30
    throw v3
.end method

.method public removeActiveAdmin(Landroid/content/ComponentName;I)V
    .registers 9
    .parameter "adminReceiver"
    .parameter "userHandle"

    #@0
    .prologue
    .line 1168
    invoke-direct {p0, p2}, Lcom/android/server/DevicePolicyManagerService;->enforceCrossUserPermission(I)V

    #@3
    .line 1169
    monitor-enter p0

    #@4
    .line 1170
    :try_start_4
    invoke-virtual {p0, p1, p2}, Lcom/android/server/DevicePolicyManagerService;->getActiveAdminUncheckedLocked(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@7
    move-result-object v0

    #@8
    .line 1171
    .local v0, admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    if-nez v0, :cond_c

    #@a
    .line 1172
    monitor-exit p0

    #@b
    .line 1185
    :goto_b
    return-void

    #@c
    .line 1174
    :cond_c
    invoke-virtual {v0}, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->getUid()I

    #@f
    move-result v3

    #@10
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@13
    move-result v4

    #@14
    if-eq v3, v4, :cond_1e

    #@16
    .line 1175
    iget-object v3, p0, Lcom/android/server/DevicePolicyManagerService;->mContext:Landroid/content/Context;

    #@18
    const-string v4, "android.permission.BIND_DEVICE_ADMIN"

    #@1a
    const/4 v5, 0x0

    #@1b
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@1e
    .line 1178
    :cond_1e
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_21
    .catchall {:try_start_4 .. :try_end_21} :catchall_2a

    #@21
    move-result-wide v1

    #@22
    .line 1180
    .local v1, ident:J
    :try_start_22
    invoke-virtual {p0, p1, p2}, Lcom/android/server/DevicePolicyManagerService;->removeActiveAdminLocked(Landroid/content/ComponentName;I)V
    :try_end_25
    .catchall {:try_start_22 .. :try_end_25} :catchall_2d

    #@25
    .line 1182
    :try_start_25
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@28
    .line 1184
    monitor-exit p0

    #@29
    goto :goto_b

    #@2a
    .end local v0           #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    .end local v1           #ident:J
    :catchall_2a
    move-exception v3

    #@2b
    monitor-exit p0
    :try_end_2c
    .catchall {:try_start_25 .. :try_end_2c} :catchall_2a

    #@2c
    throw v3

    #@2d
    .line 1182
    .restart local v0       #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    .restart local v1       #ident:J
    :catchall_2d
    move-exception v3

    #@2e
    :try_start_2e
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@31
    throw v3
    :try_end_32
    .catchall {:try_start_2e .. :try_end_32} :catchall_2a
.end method

.method removeActiveAdminLocked(Landroid/content/ComponentName;I)V
    .registers 6
    .parameter "adminReceiver"
    .parameter "userHandle"

    #@0
    .prologue
    .line 726
    invoke-virtual {p0, p1, p2}, Lcom/android/server/DevicePolicyManagerService;->getActiveAdminUncheckedLocked(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@3
    move-result-object v0

    #@4
    .line 727
    .local v0, admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    if-eqz v0, :cond_10

    #@6
    .line 728
    const-string v1, "android.app.action.DEVICE_ADMIN_DISABLED"

    #@8
    new-instance v2, Lcom/android/server/DevicePolicyManagerService$2;

    #@a
    invoke-direct {v2, p0, v0, p1}, Lcom/android/server/DevicePolicyManagerService$2;-><init>(Lcom/android/server/DevicePolicyManagerService;Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;Landroid/content/ComponentName;)V

    #@d
    invoke-virtual {p0, v0, v1, v2}, Lcom/android/server/DevicePolicyManagerService;->sendAdminCommandLocked(Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;Ljava/lang/String;Landroid/content/BroadcastReceiver;)V

    #@10
    .line 751
    :cond_10
    return-void
.end method

.method removeUserData(I)V
    .registers 7
    .parameter "userHandle"

    #@0
    .prologue
    .line 560
    monitor-enter p0

    #@1
    .line 561
    if-nez p1, :cond_c

    #@3
    .line 562
    :try_start_3
    const-string v2, "DevicePolicyManagerService"

    #@5
    const-string v3, "Tried to remove device policy file for user 0! Ignoring."

    #@7
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@a
    .line 563
    monitor-exit p0

    #@b
    .line 574
    :goto_b
    return-void

    #@c
    .line 565
    :cond_c
    iget-object v2, p0, Lcom/android/server/DevicePolicyManagerService;->mUserData:Landroid/util/SparseArray;

    #@e
    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@11
    move-result-object v0

    #@12
    check-cast v0, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;

    #@14
    .line 566
    .local v0, policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    if-eqz v0, :cond_1b

    #@16
    .line 567
    iget-object v2, p0, Lcom/android/server/DevicePolicyManagerService;->mUserData:Landroid/util/SparseArray;

    #@18
    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->remove(I)V

    #@1b
    .line 569
    :cond_1b
    new-instance v1, Ljava/io/File;

    #@1d
    invoke-static {p1}, Landroid/os/Environment;->getUserSystemDirectory(I)Ljava/io/File;

    #@20
    move-result-object v2

    #@21
    const-string v3, "device_policies.xml"

    #@23
    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@26
    .line 571
    .local v1, policyFile:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    #@29
    .line 572
    const-string v2, "DevicePolicyManagerService"

    #@2b
    new-instance v3, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    const-string v4, "Removed device policy file "

    #@32
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v3

    #@36
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@39
    move-result-object v4

    #@3a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v3

    #@3e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v3

    #@42
    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    .line 573
    monitor-exit p0

    #@46
    goto :goto_b

    #@47
    .end local v0           #policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    .end local v1           #policyFile:Ljava/io/File;
    :catchall_47
    move-exception v2

    #@48
    monitor-exit p0
    :try_end_49
    .catchall {:try_start_3 .. :try_end_49} :catchall_47

    #@49
    throw v2
.end method

.method public reportFailedPasswordAttempt(I)V
    .registers 9
    .parameter "userHandle"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 2126
    invoke-direct {p0, p1}, Lcom/android/server/DevicePolicyManagerService;->enforceCrossUserPermission(I)V

    #@4
    .line 2127
    iget-object v4, p0, Lcom/android/server/DevicePolicyManagerService;->mContext:Landroid/content/Context;

    #@6
    const-string v5, "android.permission.BIND_DEVICE_ADMIN"

    #@8
    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@b
    .line 2130
    monitor-enter p0

    #@c
    .line 2131
    :try_start_c
    invoke-virtual {p0, p1}, Lcom/android/server/DevicePolicyManagerService;->getUserData(I)Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;

    #@f
    move-result-object v3

    #@10
    .line 2132
    .local v3, policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_13
    .catchall {:try_start_c .. :try_end_13} :catchall_3c

    #@13
    move-result-wide v0

    #@14
    .line 2134
    .local v0, ident:J
    :try_start_14
    iget v4, v3, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mFailedPasswordAttempts:I

    #@16
    add-int/lit8 v4, v4, 0x1

    #@18
    iput v4, v3, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mFailedPasswordAttempts:I

    #@1a
    .line 2135
    invoke-direct {p0, p1}, Lcom/android/server/DevicePolicyManagerService;->saveSettingsLocked(I)V

    #@1d
    .line 2136
    const/4 v4, 0x0

    #@1e
    invoke-virtual {p0, v4, p1}, Lcom/android/server/DevicePolicyManagerService;->getMaximumFailedPasswordsForWipe(Landroid/content/ComponentName;I)I

    #@21
    move-result v2

    #@22
    .line 2137
    .local v2, max:I
    if-lez v2, :cond_2c

    #@24
    iget v4, v3, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mFailedPasswordAttempts:I

    #@26
    if-lt v4, v2, :cond_2c

    #@28
    .line 2138
    const/4 v4, 0x0

    #@29
    invoke-direct {p0, v4, p1}, Lcom/android/server/DevicePolicyManagerService;->wipeDeviceOrUserLocked(II)V

    #@2c
    .line 2140
    :cond_2c
    const-string v4, "android.app.action.ACTION_PASSWORD_FAILED"

    #@2e
    const/4 v5, 0x1

    #@2f
    invoke-virtual {p0, v4, v5, p1}, Lcom/android/server/DevicePolicyManagerService;->sendAdminCommandLocked(Ljava/lang/String;II)V
    :try_end_32
    .catchall {:try_start_14 .. :try_end_32} :catchall_37

    #@32
    .line 2143
    :try_start_32
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@35
    .line 2145
    monitor-exit p0

    #@36
    .line 2146
    return-void

    #@37
    .line 2143
    .end local v2           #max:I
    :catchall_37
    move-exception v4

    #@38
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@3b
    throw v4

    #@3c
    .line 2145
    .end local v0           #ident:J
    .end local v3           #policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    :catchall_3c
    move-exception v4

    #@3d
    monitor-exit p0
    :try_end_3e
    .catchall {:try_start_32 .. :try_end_3e} :catchall_3c

    #@3e
    throw v4
.end method

.method public reportSuccessfulPasswordAttempt(I)V
    .registers 8
    .parameter "userHandle"

    #@0
    .prologue
    .line 2149
    invoke-direct {p0, p1}, Lcom/android/server/DevicePolicyManagerService;->enforceCrossUserPermission(I)V

    #@3
    .line 2150
    iget-object v3, p0, Lcom/android/server/DevicePolicyManagerService;->mContext:Landroid/content/Context;

    #@5
    const-string v4, "android.permission.BIND_DEVICE_ADMIN"

    #@7
    const/4 v5, 0x0

    #@8
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@b
    .line 2153
    monitor-enter p0

    #@c
    .line 2154
    :try_start_c
    invoke-virtual {p0, p1}, Lcom/android/server/DevicePolicyManagerService;->getUserData(I)Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;

    #@f
    move-result-object v2

    #@10
    .line 2155
    .local v2, policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    iget v3, v2, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mFailedPasswordAttempts:I

    #@12
    if-nez v3, :cond_18

    #@14
    iget v3, v2, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mPasswordOwner:I

    #@16
    if-ltz v3, :cond_2e

    #@18
    .line 2156
    :cond_18
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_1b
    .catchall {:try_start_c .. :try_end_1b} :catchall_35

    #@1b
    move-result-wide v0

    #@1c
    .line 2158
    .local v0, ident:J
    const/4 v3, 0x0

    #@1d
    :try_start_1d
    iput v3, v2, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mFailedPasswordAttempts:I

    #@1f
    .line 2159
    const/4 v3, -0x1

    #@20
    iput v3, v2, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mPasswordOwner:I

    #@22
    .line 2160
    invoke-direct {p0, p1}, Lcom/android/server/DevicePolicyManagerService;->saveSettingsLocked(I)V

    #@25
    .line 2161
    const-string v3, "android.app.action.ACTION_PASSWORD_SUCCEEDED"

    #@27
    const/4 v4, 0x1

    #@28
    invoke-virtual {p0, v3, v4, p1}, Lcom/android/server/DevicePolicyManagerService;->sendAdminCommandLocked(Ljava/lang/String;II)V
    :try_end_2b
    .catchall {:try_start_1d .. :try_end_2b} :catchall_30

    #@2b
    .line 2164
    :try_start_2b
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@2e
    .line 2167
    .end local v0           #ident:J
    :cond_2e
    monitor-exit p0

    #@2f
    .line 2168
    return-void

    #@30
    .line 2164
    .restart local v0       #ident:J
    :catchall_30
    move-exception v3

    #@31
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@34
    throw v3

    #@35
    .line 2167
    .end local v0           #ident:J
    .end local v2           #policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    :catchall_35
    move-exception v3

    #@36
    monitor-exit p0
    :try_end_37
    .catchall {:try_start_2b .. :try_end_37} :catchall_35

    #@37
    throw v3
.end method

.method public resetPassword(Ljava/lang/String;II)Z
    .registers 35
    .parameter "password"
    .parameter "flags"
    .parameter "userHandle"

    #@0
    .prologue
    .line 1693
    move-object/from16 v0, p0

    #@2
    move/from16 v1, p3

    #@4
    invoke-direct {v0, v1}, Lcom/android/server/DevicePolicyManagerService;->enforceCrossUserPermission(I)V

    #@7
    .line 1695
    monitor-enter p0

    #@8
    .line 1698
    const/16 v28, 0x0

    #@a
    const/16 v29, 0x2

    #@c
    :try_start_c
    move-object/from16 v0, p0

    #@e
    move-object/from16 v1, v28

    #@10
    move/from16 v2, v29

    #@12
    invoke-virtual {v0, v1, v2}, Lcom/android/server/DevicePolicyManagerService;->getActiveAdminForCallerLocked(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@15
    .line 1700
    const/16 v28, 0x0

    #@17
    move-object/from16 v0, p0

    #@19
    move-object/from16 v1, v28

    #@1b
    move/from16 v2, p3

    #@1d
    invoke-virtual {v0, v1, v2}, Lcom/android/server/DevicePolicyManagerService;->getPasswordQuality(Landroid/content/ComponentName;I)I

    #@20
    move-result v23

    #@21
    .line 1701
    .local v23, quality:I
    if-eqz v23, :cond_6b

    #@23
    .line 1702
    invoke-static/range {p1 .. p1}, Lcom/android/internal/widget/LockPatternUtils;->computePasswordQuality(Ljava/lang/String;)I

    #@26
    move-result v24

    #@27
    .line 1703
    .local v24, realQuality:I
    move/from16 v0, v24

    #@29
    move/from16 v1, v23

    #@2b
    if-ge v0, v1, :cond_63

    #@2d
    const/high16 v28, 0x6

    #@2f
    move/from16 v0, v23

    #@31
    move/from16 v1, v28

    #@33
    if-eq v0, v1, :cond_63

    #@35
    .line 1705
    const-string v28, "DevicePolicyManagerService"

    #@37
    new-instance v29, Ljava/lang/StringBuilder;

    #@39
    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    #@3c
    const-string v30, "resetPassword: password quality 0x"

    #@3e
    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v29

    #@42
    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@45
    move-result-object v30

    #@46
    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v29

    #@4a
    const-string v30, " does not meet required quality 0x"

    #@4c
    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v29

    #@50
    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@53
    move-result-object v30

    #@54
    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v29

    #@58
    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5b
    move-result-object v29

    #@5c
    invoke-static/range {v28 .. v29}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@5f
    .line 1709
    const/16 v28, 0x0

    #@61
    monitor-exit p0

    #@62
    .line 1828
    .end local v24           #realQuality:I
    :goto_62
    return v28

    #@63
    .line 1711
    .restart local v24       #realQuality:I
    :cond_63
    move/from16 v0, v24

    #@65
    move/from16 v1, v23

    #@67
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@6a
    move-result v23

    #@6b
    .line 1713
    .end local v24           #realQuality:I
    :cond_6b
    const/16 v28, 0x0

    #@6d
    move-object/from16 v0, p0

    #@6f
    move-object/from16 v1, v28

    #@71
    move/from16 v2, p3

    #@73
    invoke-virtual {v0, v1, v2}, Lcom/android/server/DevicePolicyManagerService;->getPasswordMinimumLength(Landroid/content/ComponentName;I)I

    #@76
    move-result v10

    #@77
    .line 1714
    .local v10, length:I
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    #@7a
    move-result v28

    #@7b
    move/from16 v0, v28

    #@7d
    if-ge v0, v10, :cond_ae

    #@7f
    .line 1715
    const-string v28, "DevicePolicyManagerService"

    #@81
    new-instance v29, Ljava/lang/StringBuilder;

    #@83
    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    #@86
    const-string v30, "resetPassword: password length "

    #@88
    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v29

    #@8c
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    #@8f
    move-result v30

    #@90
    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@93
    move-result-object v29

    #@94
    const-string v30, " does not meet required length "

    #@96
    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@99
    move-result-object v29

    #@9a
    move-object/from16 v0, v29

    #@9c
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v29

    #@a0
    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a3
    move-result-object v29

    #@a4
    invoke-static/range {v28 .. v29}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@a7
    .line 1717
    const/16 v28, 0x0

    #@a9
    monitor-exit p0

    #@aa
    goto :goto_62

    #@ab
    .line 1792
    .end local v10           #length:I
    .end local v23           #quality:I
    :catchall_ab
    move-exception v28

    #@ac
    monitor-exit p0
    :try_end_ad
    .catchall {:try_start_c .. :try_end_ad} :catchall_ab

    #@ad
    throw v28

    #@ae
    .line 1719
    .restart local v10       #length:I
    .restart local v23       #quality:I
    :cond_ae
    const/high16 v28, 0x6

    #@b0
    move/from16 v0, v23

    #@b2
    move/from16 v1, v28

    #@b4
    if-ne v0, v1, :cond_29d

    #@b6
    .line 1720
    const/4 v11, 0x0

    #@b7
    .line 1721
    .local v11, letters:I
    const/16 v26, 0x0

    #@b9
    .line 1722
    .local v26, uppercase:I
    const/4 v12, 0x0

    #@ba
    .line 1723
    .local v12, lowercase:I
    const/16 v21, 0x0

    #@bc
    .line 1724
    .local v21, numbers:I
    const/16 v25, 0x0

    #@be
    .line 1725
    .local v25, symbols:I
    const/16 v20, 0x0

    #@c0
    .line 1726
    .local v20, nonletter:I
    const/4 v7, 0x0

    #@c1
    .local v7, i:I
    :goto_c1
    :try_start_c1
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    #@c4
    move-result v28

    #@c5
    move/from16 v0, v28

    #@c7
    if-ge v7, v0, :cond_109

    #@c9
    .line 1727
    move-object/from16 v0, p1

    #@cb
    invoke-virtual {v0, v7}, Ljava/lang/String;->charAt(I)C

    #@ce
    move-result v5

    #@cf
    .line 1728
    .local v5, c:C
    const/16 v28, 0x41

    #@d1
    move/from16 v0, v28

    #@d3
    if-lt v5, v0, :cond_e2

    #@d5
    const/16 v28, 0x5a

    #@d7
    move/from16 v0, v28

    #@d9
    if-gt v5, v0, :cond_e2

    #@db
    .line 1729
    add-int/lit8 v11, v11, 0x1

    #@dd
    .line 1730
    add-int/lit8 v26, v26, 0x1

    #@df
    .line 1726
    :goto_df
    add-int/lit8 v7, v7, 0x1

    #@e1
    goto :goto_c1

    #@e2
    .line 1731
    :cond_e2
    const/16 v28, 0x61

    #@e4
    move/from16 v0, v28

    #@e6
    if-lt v5, v0, :cond_f3

    #@e8
    const/16 v28, 0x7a

    #@ea
    move/from16 v0, v28

    #@ec
    if-gt v5, v0, :cond_f3

    #@ee
    .line 1732
    add-int/lit8 v11, v11, 0x1

    #@f0
    .line 1733
    add-int/lit8 v12, v12, 0x1

    #@f2
    goto :goto_df

    #@f3
    .line 1734
    :cond_f3
    const/16 v28, 0x30

    #@f5
    move/from16 v0, v28

    #@f7
    if-lt v5, v0, :cond_104

    #@f9
    const/16 v28, 0x39

    #@fb
    move/from16 v0, v28

    #@fd
    if-gt v5, v0, :cond_104

    #@ff
    .line 1735
    add-int/lit8 v21, v21, 0x1

    #@101
    .line 1736
    add-int/lit8 v20, v20, 0x1

    #@103
    goto :goto_df

    #@104
    .line 1738
    :cond_104
    add-int/lit8 v25, v25, 0x1

    #@106
    .line 1739
    add-int/lit8 v20, v20, 0x1

    #@108
    goto :goto_df

    #@109
    .line 1742
    .end local v5           #c:C
    :cond_109
    const/16 v28, 0x0

    #@10b
    move-object/from16 v0, p0

    #@10d
    move-object/from16 v1, v28

    #@10f
    move/from16 v2, p3

    #@111
    invoke-virtual {v0, v1, v2}, Lcom/android/server/DevicePolicyManagerService;->getPasswordMinimumLetters(Landroid/content/ComponentName;I)I

    #@114
    move-result v13

    #@115
    .line 1743
    .local v13, neededLetters:I
    if-ge v11, v13, :cond_142

    #@117
    .line 1744
    const-string v28, "DevicePolicyManagerService"

    #@119
    new-instance v29, Ljava/lang/StringBuilder;

    #@11b
    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    #@11e
    const-string v30, "resetPassword: number of letters "

    #@120
    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@123
    move-result-object v29

    #@124
    move-object/from16 v0, v29

    #@126
    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@129
    move-result-object v29

    #@12a
    const-string v30, " does not meet required number of letters "

    #@12c
    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12f
    move-result-object v29

    #@130
    move-object/from16 v0, v29

    #@132
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@135
    move-result-object v29

    #@136
    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@139
    move-result-object v29

    #@13a
    invoke-static/range {v28 .. v29}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@13d
    .line 1746
    const/16 v28, 0x0

    #@13f
    monitor-exit p0

    #@140
    goto/16 :goto_62

    #@142
    .line 1748
    :cond_142
    const/16 v28, 0x0

    #@144
    move-object/from16 v0, p0

    #@146
    move-object/from16 v1, v28

    #@148
    move/from16 v2, p3

    #@14a
    invoke-virtual {v0, v1, v2}, Lcom/android/server/DevicePolicyManagerService;->getPasswordMinimumNumeric(Landroid/content/ComponentName;I)I

    #@14d
    move-result v16

    #@14e
    .line 1749
    .local v16, neededNumbers:I
    move/from16 v0, v21

    #@150
    move/from16 v1, v16

    #@152
    if-ge v0, v1, :cond_183

    #@154
    .line 1750
    const-string v28, "DevicePolicyManagerService"

    #@156
    new-instance v29, Ljava/lang/StringBuilder;

    #@158
    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    #@15b
    const-string v30, "resetPassword: number of numerical digits "

    #@15d
    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@160
    move-result-object v29

    #@161
    move-object/from16 v0, v29

    #@163
    move/from16 v1, v21

    #@165
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@168
    move-result-object v29

    #@169
    const-string v30, " does not meet required number of numerical digits "

    #@16b
    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16e
    move-result-object v29

    #@16f
    move-object/from16 v0, v29

    #@171
    move/from16 v1, v16

    #@173
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@176
    move-result-object v29

    #@177
    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17a
    move-result-object v29

    #@17b
    invoke-static/range {v28 .. v29}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@17e
    .line 1753
    const/16 v28, 0x0

    #@180
    monitor-exit p0

    #@181
    goto/16 :goto_62

    #@183
    .line 1755
    :cond_183
    const/16 v28, 0x0

    #@185
    move-object/from16 v0, p0

    #@187
    move-object/from16 v1, v28

    #@189
    move/from16 v2, p3

    #@18b
    invoke-virtual {v0, v1, v2}, Lcom/android/server/DevicePolicyManagerService;->getPasswordMinimumLowerCase(Landroid/content/ComponentName;I)I

    #@18e
    move-result v14

    #@18f
    .line 1756
    .local v14, neededLowerCase:I
    if-ge v12, v14, :cond_1bc

    #@191
    .line 1757
    const-string v28, "DevicePolicyManagerService"

    #@193
    new-instance v29, Ljava/lang/StringBuilder;

    #@195
    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    #@198
    const-string v30, "resetPassword: number of lowercase letters "

    #@19a
    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19d
    move-result-object v29

    #@19e
    move-object/from16 v0, v29

    #@1a0
    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a3
    move-result-object v29

    #@1a4
    const-string v30, " does not meet required number of lowercase letters "

    #@1a6
    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a9
    move-result-object v29

    #@1aa
    move-object/from16 v0, v29

    #@1ac
    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1af
    move-result-object v29

    #@1b0
    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b3
    move-result-object v29

    #@1b4
    invoke-static/range {v28 .. v29}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1b7
    .line 1760
    const/16 v28, 0x0

    #@1b9
    monitor-exit p0

    #@1ba
    goto/16 :goto_62

    #@1bc
    .line 1762
    :cond_1bc
    const/16 v28, 0x0

    #@1be
    move-object/from16 v0, p0

    #@1c0
    move-object/from16 v1, v28

    #@1c2
    move/from16 v2, p3

    #@1c4
    invoke-virtual {v0, v1, v2}, Lcom/android/server/DevicePolicyManagerService;->getPasswordMinimumUpperCase(Landroid/content/ComponentName;I)I

    #@1c7
    move-result v18

    #@1c8
    .line 1763
    .local v18, neededUpperCase:I
    move/from16 v0, v26

    #@1ca
    move/from16 v1, v18

    #@1cc
    if-ge v0, v1, :cond_1fd

    #@1ce
    .line 1764
    const-string v28, "DevicePolicyManagerService"

    #@1d0
    new-instance v29, Ljava/lang/StringBuilder;

    #@1d2
    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    #@1d5
    const-string v30, "resetPassword: number of uppercase letters "

    #@1d7
    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1da
    move-result-object v29

    #@1db
    move-object/from16 v0, v29

    #@1dd
    move/from16 v1, v26

    #@1df
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e2
    move-result-object v29

    #@1e3
    const-string v30, " does not meet required number of uppercase letters "

    #@1e5
    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e8
    move-result-object v29

    #@1e9
    move-object/from16 v0, v29

    #@1eb
    move/from16 v1, v18

    #@1ed
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f0
    move-result-object v29

    #@1f1
    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f4
    move-result-object v29

    #@1f5
    invoke-static/range {v28 .. v29}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1f8
    .line 1767
    const/16 v28, 0x0

    #@1fa
    monitor-exit p0

    #@1fb
    goto/16 :goto_62

    #@1fd
    .line 1769
    :cond_1fd
    const/16 v28, 0x0

    #@1ff
    move-object/from16 v0, p0

    #@201
    move-object/from16 v1, v28

    #@203
    move/from16 v2, p3

    #@205
    invoke-virtual {v0, v1, v2}, Lcom/android/server/DevicePolicyManagerService;->getPasswordMinimumSymbols(Landroid/content/ComponentName;I)I

    #@208
    move-result v17

    #@209
    .line 1770
    .local v17, neededSymbols:I
    move/from16 v0, v25

    #@20b
    move/from16 v1, v17

    #@20d
    if-ge v0, v1, :cond_23e

    #@20f
    .line 1771
    const-string v28, "DevicePolicyManagerService"

    #@211
    new-instance v29, Ljava/lang/StringBuilder;

    #@213
    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    #@216
    const-string v30, "resetPassword: number of special symbols "

    #@218
    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21b
    move-result-object v29

    #@21c
    move-object/from16 v0, v29

    #@21e
    move/from16 v1, v25

    #@220
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@223
    move-result-object v29

    #@224
    const-string v30, " does not meet required number of special symbols "

    #@226
    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@229
    move-result-object v29

    #@22a
    move-object/from16 v0, v29

    #@22c
    move/from16 v1, v17

    #@22e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@231
    move-result-object v29

    #@232
    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@235
    move-result-object v29

    #@236
    invoke-static/range {v28 .. v29}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@239
    .line 1773
    const/16 v28, 0x0

    #@23b
    monitor-exit p0

    #@23c
    goto/16 :goto_62

    #@23e
    .line 1775
    :cond_23e
    const/16 v28, 0x0

    #@240
    move-object/from16 v0, p0

    #@242
    move-object/from16 v1, v28

    #@244
    move/from16 v2, p3

    #@246
    invoke-virtual {v0, v1, v2}, Lcom/android/server/DevicePolicyManagerService;->getPasswordMinimumNonLetter(Landroid/content/ComponentName;I)I

    #@249
    move-result v15

    #@24a
    .line 1776
    .local v15, neededNonLetter:I
    move/from16 v0, v20

    #@24c
    if-ge v0, v15, :cond_27b

    #@24e
    .line 1777
    const-string v28, "DevicePolicyManagerService"

    #@250
    new-instance v29, Ljava/lang/StringBuilder;

    #@252
    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    #@255
    const-string v30, "resetPassword: number of non-letter characters "

    #@257
    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25a
    move-result-object v29

    #@25b
    move-object/from16 v0, v29

    #@25d
    move/from16 v1, v20

    #@25f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@262
    move-result-object v29

    #@263
    const-string v30, " does not meet required number of non-letter characters "

    #@265
    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@268
    move-result-object v29

    #@269
    move-object/from16 v0, v29

    #@26b
    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@26e
    move-result-object v29

    #@26f
    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@272
    move-result-object v29

    #@273
    invoke-static/range {v28 .. v29}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@276
    .line 1780
    const/16 v28, 0x0

    #@278
    monitor-exit p0

    #@279
    goto/16 :goto_62

    #@27b
    .line 1783
    :cond_27b
    sget-boolean v28, Lcom/lge/config/ConfigBuildFlags;->CAPP_MDM:Z

    #@27d
    if-eqz v28, :cond_29d

    #@27f
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@282
    move-result-object v28

    #@283
    const/16 v29, 0x0

    #@285
    invoke-interface/range {v28 .. v29}, Lcom/lge/cappuccino/IMdm;->getAllowPasswordComplexForEAS(Landroid/content/ComponentName;)Z

    #@288
    move-result v28

    #@289
    if-eqz v28, :cond_29d

    #@28b
    .line 1785
    if-nez v25, :cond_294

    #@28d
    if-nez v12, :cond_294

    #@28f
    .line 1786
    const/16 v28, 0x0

    #@291
    monitor-exit p0

    #@292
    goto/16 :goto_62

    #@294
    .line 1787
    :cond_294
    if-nez v25, :cond_29d

    #@296
    if-nez v26, :cond_29d

    #@298
    .line 1788
    const/16 v28, 0x0

    #@29a
    monitor-exit p0

    #@29b
    goto/16 :goto_62

    #@29d
    .line 1792
    .end local v7           #i:I
    .end local v11           #letters:I
    .end local v12           #lowercase:I
    .end local v13           #neededLetters:I
    .end local v14           #neededLowerCase:I
    .end local v15           #neededNonLetter:I
    .end local v16           #neededNumbers:I
    .end local v17           #neededSymbols:I
    .end local v18           #neededUpperCase:I
    .end local v20           #nonletter:I
    .end local v21           #numbers:I
    .end local v25           #symbols:I
    .end local v26           #uppercase:I
    :cond_29d
    monitor-exit p0
    :try_end_29e
    .catchall {:try_start_c1 .. :try_end_29e} :catchall_ab

    #@29e
    .line 1795
    sget-boolean v28, Lcom/lge/config/ConfigBuildFlags;->CAPP_MDM:Z

    #@2a0
    if-eqz v28, :cond_2ce

    #@2a2
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@2a5
    move-result-object v28

    #@2a6
    const/16 v29, 0x0

    #@2a8
    move-object/from16 v0, v28

    #@2aa
    move-object/from16 v1, v29

    #@2ac
    move/from16 v2, p3

    #@2ae
    invoke-interface {v0, v1, v2}, Lcom/lge/cappuccino/IMdm;->getAllowSimplePassword(Landroid/content/ComponentName;I)Z

    #@2b1
    move-result v28

    #@2b2
    if-nez v28, :cond_2ce

    #@2b4
    .line 1797
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@2b7
    move-result-object v28

    #@2b8
    move-object/from16 v0, p0

    #@2ba
    iget-object v0, v0, Lcom/android/server/DevicePolicyManagerService;->mContext:Landroid/content/Context;

    #@2bc
    move-object/from16 v29, v0

    #@2be
    move-object/from16 v0, v28

    #@2c0
    move-object/from16 v1, v29

    #@2c2
    move-object/from16 v2, p1

    #@2c4
    invoke-interface {v0, v1, v2}, Lcom/lge/cappuccino/IMdm;->checkSimplepasswordCorrect(Landroid/content/Context;Ljava/lang/String;)Z

    #@2c7
    move-result v28

    #@2c8
    if-eqz v28, :cond_2ce

    #@2ca
    .line 1798
    const/16 v28, 0x0

    #@2cc
    goto/16 :goto_62

    #@2ce
    .line 1803
    :cond_2ce
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@2d1
    move-result v6

    #@2d2
    .line 1804
    .local v6, callingUid:I
    move-object/from16 v0, p0

    #@2d4
    move/from16 v1, p3

    #@2d6
    invoke-virtual {v0, v1}, Lcom/android/server/DevicePolicyManagerService;->getUserData(I)Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;

    #@2d9
    move-result-object v22

    #@2da
    .line 1805
    .local v22, policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    move-object/from16 v0, v22

    #@2dc
    iget v0, v0, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mPasswordOwner:I

    #@2de
    move/from16 v28, v0

    #@2e0
    if-ltz v28, :cond_2f7

    #@2e2
    move-object/from16 v0, v22

    #@2e4
    iget v0, v0, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mPasswordOwner:I

    #@2e6
    move/from16 v28, v0

    #@2e8
    move/from16 v0, v28

    #@2ea
    if-eq v0, v6, :cond_2f7

    #@2ec
    .line 1806
    const-string v28, "DevicePolicyManagerService"

    #@2ee
    const-string v29, "resetPassword: already set by another uid and not entered by user"

    #@2f0
    invoke-static/range {v28 .. v29}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2f3
    .line 1807
    const/16 v28, 0x0

    #@2f5
    goto/16 :goto_62

    #@2f7
    .line 1812
    :cond_2f7
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@2fa
    move-result-wide v8

    #@2fb
    .line 1814
    .local v8, ident:J
    :try_start_2fb
    new-instance v27, Lcom/android/internal/widget/LockPatternUtils;

    #@2fd
    move-object/from16 v0, p0

    #@2ff
    iget-object v0, v0, Lcom/android/server/DevicePolicyManagerService;->mContext:Landroid/content/Context;

    #@301
    move-object/from16 v28, v0

    #@303
    invoke-direct/range {v27 .. v28}, Lcom/android/internal/widget/LockPatternUtils;-><init>(Landroid/content/Context;)V

    #@306
    .line 1815
    .local v27, utils:Lcom/android/internal/widget/LockPatternUtils;
    const/16 v28, 0x0

    #@308
    move-object/from16 v0, v27

    #@30a
    move-object/from16 v1, p1

    #@30c
    move/from16 v2, v23

    #@30e
    move/from16 v3, v28

    #@310
    move/from16 v4, p3

    #@312
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/internal/widget/LockPatternUtils;->saveLockPassword(Ljava/lang/String;IZI)V

    #@315
    .line 1816
    monitor-enter p0
    :try_end_316
    .catchall {:try_start_2fb .. :try_end_316} :catchall_343

    #@316
    .line 1817
    and-int/lit8 v28, p2, 0x1

    #@318
    if-eqz v28, :cond_33d

    #@31a
    move/from16 v19, v6

    #@31c
    .line 1819
    .local v19, newOwner:I
    :goto_31c
    :try_start_31c
    move-object/from16 v0, v22

    #@31e
    iget v0, v0, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mPasswordOwner:I

    #@320
    move/from16 v28, v0

    #@322
    move/from16 v0, v28

    #@324
    move/from16 v1, v19

    #@326
    if-eq v0, v1, :cond_335

    #@328
    .line 1820
    move/from16 v0, v19

    #@32a
    move-object/from16 v1, v22

    #@32c
    iput v0, v1, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mPasswordOwner:I

    #@32e
    .line 1821
    move-object/from16 v0, p0

    #@330
    move/from16 v1, p3

    #@332
    invoke-direct {v0, v1}, Lcom/android/server/DevicePolicyManagerService;->saveSettingsLocked(I)V

    #@335
    .line 1823
    :cond_335
    monitor-exit p0
    :try_end_336
    .catchall {:try_start_31c .. :try_end_336} :catchall_340

    #@336
    .line 1825
    invoke-static {v8, v9}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@339
    .line 1828
    const/16 v28, 0x1

    #@33b
    goto/16 :goto_62

    #@33d
    .line 1817
    .end local v19           #newOwner:I
    :cond_33d
    const/16 v19, -0x1

    #@33f
    goto :goto_31c

    #@340
    .line 1823
    .restart local v19       #newOwner:I
    :catchall_340
    move-exception v28

    #@341
    :try_start_341
    monitor-exit p0
    :try_end_342
    .catchall {:try_start_341 .. :try_end_342} :catchall_340

    #@342
    :try_start_342
    throw v28
    :try_end_343
    .catchall {:try_start_342 .. :try_end_343} :catchall_343

    #@343
    .line 1825
    .end local v19           #newOwner:I
    .end local v27           #utils:Lcom/android/internal/widget/LockPatternUtils;
    :catchall_343
    move-exception v28

    #@344
    invoke-static {v8, v9}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@347
    throw v28
.end method

.method sendAdminCommandLocked(Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;Ljava/lang/String;)V
    .registers 4
    .parameter "admin"
    .parameter "action"

    #@0
    .prologue
    .line 695
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, p2, v0}, Lcom/android/server/DevicePolicyManagerService;->sendAdminCommandLocked(Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;Ljava/lang/String;Landroid/content/BroadcastReceiver;)V

    #@4
    .line 696
    return-void
.end method

.method sendAdminCommandLocked(Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;Ljava/lang/String;Landroid/content/BroadcastReceiver;)V
    .registers 13
    .parameter "admin"
    .parameter "action"
    .parameter "result"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 699
    new-instance v1, Landroid/content/Intent;

    #@3
    invoke-direct {v1, p2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@6
    .line 700
    .local v1, intent:Landroid/content/Intent;
    iget-object v0, p1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->info:Landroid/app/admin/DeviceAdminInfo;

    #@8
    invoke-virtual {v0}, Landroid/app/admin/DeviceAdminInfo;->getComponent()Landroid/content/ComponentName;

    #@b
    move-result-object v0

    #@c
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@f
    .line 701
    const-string v0, "android.app.action.ACTION_PASSWORD_EXPIRING"

    #@11
    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14
    move-result v0

    #@15
    if-eqz v0, :cond_1e

    #@17
    .line 702
    const-string v0, "expiration"

    #@19
    iget-wide v4, p1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->passwordExpirationDate:J

    #@1b
    invoke-virtual {v1, v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    #@1e
    .line 704
    :cond_1e
    if-eqz p3, :cond_30

    #@20
    .line 705
    iget-object v0, p0, Lcom/android/server/DevicePolicyManagerService;->mContext:Landroid/content/Context;

    #@22
    invoke-virtual {p1}, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->getUserHandle()Landroid/os/UserHandle;

    #@25
    move-result-object v2

    #@26
    iget-object v5, p0, Lcom/android/server/DevicePolicyManagerService;->mHandler:Landroid/os/Handler;

    #@28
    const/4 v6, -0x1

    #@29
    move-object v4, p3

    #@2a
    move-object v7, v3

    #@2b
    move-object v8, v3

    #@2c
    invoke-virtual/range {v0 .. v8}, Landroid/content/Context;->sendOrderedBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V

    #@2f
    .line 710
    :goto_2f
    return-void

    #@30
    .line 708
    :cond_30
    iget-object v0, p0, Lcom/android/server/DevicePolicyManagerService;->mContext:Landroid/content/Context;

    #@32
    sget-object v2, Landroid/os/UserHandle;->OWNER:Landroid/os/UserHandle;

    #@34
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@37
    goto :goto_2f
.end method

.method sendAdminCommandLocked(Ljava/lang/String;II)V
    .registers 9
    .parameter "action"
    .parameter "reqPolicy"
    .parameter "userHandle"

    #@0
    .prologue
    .line 713
    invoke-virtual {p0, p3}, Lcom/android/server/DevicePolicyManagerService;->getUserData(I)Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;

    #@3
    move-result-object v3

    #@4
    .line 714
    .local v3, policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    iget-object v4, v3, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@9
    move-result v1

    #@a
    .line 715
    .local v1, count:I
    if-lez v1, :cond_25

    #@c
    .line 716
    const/4 v2, 0x0

    #@d
    .local v2, i:I
    :goto_d
    if-ge v2, v1, :cond_25

    #@f
    .line 717
    iget-object v4, v3, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@11
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@14
    move-result-object v0

    #@15
    check-cast v0, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@17
    .line 718
    .local v0, admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    iget-object v4, v0, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->info:Landroid/app/admin/DeviceAdminInfo;

    #@19
    invoke-virtual {v4, p2}, Landroid/app/admin/DeviceAdminInfo;->usesPolicy(I)Z

    #@1c
    move-result v4

    #@1d
    if-eqz v4, :cond_22

    #@1f
    .line 719
    invoke-virtual {p0, v0, p1}, Lcom/android/server/DevicePolicyManagerService;->sendAdminCommandLocked(Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;Ljava/lang/String;)V

    #@22
    .line 716
    :cond_22
    add-int/lit8 v2, v2, 0x1

    #@24
    goto :goto_d

    #@25
    .line 723
    .end local v0           #admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    .end local v2           #i:I
    :cond_25
    return-void
.end method

.method public setActiveAdmin(Landroid/content/ComponentName;ZI)V
    .registers 16
    .parameter "adminReceiver"
    .parameter "refreshing"
    .parameter "userHandle"

    #@0
    .prologue
    .line 1078
    iget-object v9, p0, Lcom/android/server/DevicePolicyManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v10, "android.permission.BIND_DEVICE_ADMIN"

    #@4
    const/4 v11, 0x0

    #@5
    invoke-virtual {v9, v10, v11}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 1080
    invoke-direct {p0, p3}, Lcom/android/server/DevicePolicyManagerService;->enforceCrossUserPermission(I)V

    #@b
    .line 1082
    invoke-virtual {p0, p3}, Lcom/android/server/DevicePolicyManagerService;->getUserData(I)Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;

    #@e
    move-result-object v7

    #@f
    .line 1083
    .local v7, policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    invoke-virtual {p0, p1, p3}, Lcom/android/server/DevicePolicyManagerService;->findAdmin(Landroid/content/ComponentName;I)Landroid/app/admin/DeviceAdminInfo;

    #@12
    move-result-object v4

    #@13
    .line 1084
    .local v4, info:Landroid/app/admin/DeviceAdminInfo;
    if-nez v4, :cond_2e

    #@15
    .line 1085
    new-instance v9, Ljava/lang/IllegalArgumentException;

    #@17
    new-instance v10, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v11, "Bad admin: "

    #@1e
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v10

    #@22
    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v10

    #@26
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v10

    #@2a
    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@2d
    throw v9

    #@2e
    .line 1087
    :cond_2e
    monitor-enter p0

    #@2f
    .line 1088
    :try_start_2f
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_32
    .catchall {:try_start_2f .. :try_end_32} :catchall_48

    #@32
    move-result-wide v2

    #@33
    .line 1090
    .local v2, ident:J
    if-nez p2, :cond_4b

    #@35
    :try_start_35
    invoke-virtual {p0, p1, p3}, Lcom/android/server/DevicePolicyManagerService;->getActiveAdminUncheckedLocked(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@38
    move-result-object v9

    #@39
    if-eqz v9, :cond_4b

    #@3b
    .line 1091
    new-instance v9, Ljava/lang/IllegalArgumentException;

    #@3d
    const-string v10, "Admin is already added"

    #@3f
    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@42
    throw v9
    :try_end_43
    .catchall {:try_start_35 .. :try_end_43} :catchall_43

    #@43
    .line 1114
    :catchall_43
    move-exception v9

    #@44
    :try_start_44
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@47
    throw v9

    #@48
    .line 1116
    .end local v2           #ident:J
    :catchall_48
    move-exception v9

    #@49
    monitor-exit p0
    :try_end_4a
    .catchall {:try_start_44 .. :try_end_4a} :catchall_48

    #@4a
    throw v9

    #@4b
    .line 1093
    .restart local v2       #ident:J
    :cond_4b
    :try_start_4b
    new-instance v5, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@4d
    invoke-direct {v5, v4}, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;-><init>(Landroid/app/admin/DeviceAdminInfo;)V

    #@50
    .line 1094
    .local v5, newAdmin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    iget-object v9, v7, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminMap:Ljava/util/HashMap;

    #@52
    invoke-virtual {v9, p1, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@55
    .line 1095
    const/4 v8, -0x1

    #@56
    .line 1096
    .local v8, replaceIndex:I
    if-eqz p2, :cond_76

    #@58
    .line 1097
    iget-object v9, v7, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@5a
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    #@5d
    move-result v0

    #@5e
    .line 1098
    .local v0, N:I
    const/4 v1, 0x0

    #@5f
    .local v1, i:I
    :goto_5f
    if-ge v1, v0, :cond_76

    #@61
    .line 1099
    iget-object v9, v7, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@63
    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@66
    move-result-object v6

    #@67
    check-cast v6, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@69
    .line 1100
    .local v6, oldAdmin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    iget-object v9, v6, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->info:Landroid/app/admin/DeviceAdminInfo;

    #@6b
    invoke-virtual {v9}, Landroid/app/admin/DeviceAdminInfo;->getComponent()Landroid/content/ComponentName;

    #@6e
    move-result-object v9

    #@6f
    invoke-virtual {v9, p1}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    #@72
    move-result v9

    #@73
    if-eqz v9, :cond_8b

    #@75
    .line 1101
    move v8, v1

    #@76
    .line 1106
    .end local v0           #N:I
    .end local v1           #i:I
    .end local v6           #oldAdmin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_76
    const/4 v9, -0x1

    #@77
    if-ne v8, v9, :cond_8e

    #@79
    .line 1107
    iget-object v9, v7, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@7b
    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@7e
    .line 1111
    :goto_7e
    invoke-direct {p0, p3}, Lcom/android/server/DevicePolicyManagerService;->saveSettingsLocked(I)V

    #@81
    .line 1112
    const-string v9, "android.app.action.DEVICE_ADMIN_ENABLED"

    #@83
    invoke-virtual {p0, v5, v9}, Lcom/android/server/DevicePolicyManagerService;->sendAdminCommandLocked(Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;Ljava/lang/String;)V
    :try_end_86
    .catchall {:try_start_4b .. :try_end_86} :catchall_43

    #@86
    .line 1114
    :try_start_86
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@89
    .line 1116
    monitor-exit p0
    :try_end_8a
    .catchall {:try_start_86 .. :try_end_8a} :catchall_48

    #@8a
    .line 1117
    return-void

    #@8b
    .line 1098
    .restart local v0       #N:I
    .restart local v1       #i:I
    .restart local v6       #oldAdmin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_8b
    add-int/lit8 v1, v1, 0x1

    #@8d
    goto :goto_5f

    #@8e
    .line 1109
    .end local v0           #N:I
    .end local v1           #i:I
    .end local v6           #oldAdmin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :cond_8e
    :try_start_8e
    iget-object v9, v7, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@90
    invoke-virtual {v9, v8, v5}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;
    :try_end_93
    .catchall {:try_start_8e .. :try_end_93} :catchall_43

    #@93
    goto :goto_7e
.end method

.method public setActivePasswordState(IIIIIIIII)V
    .registers 16
    .parameter "quality"
    .parameter "length"
    .parameter "letters"
    .parameter "uppercase"
    .parameter "lowercase"
    .parameter "numbers"
    .parameter "symbols"
    .parameter "nonletter"
    .parameter "userHandle"

    #@0
    .prologue
    .line 2070
    invoke-direct {p0, p9}, Lcom/android/server/DevicePolicyManagerService;->enforceCrossUserPermission(I)V

    #@3
    .line 2071
    iget-object v3, p0, Lcom/android/server/DevicePolicyManagerService;->mContext:Landroid/content/Context;

    #@5
    const-string v4, "android.permission.BIND_DEVICE_ADMIN"

    #@7
    const/4 v5, 0x0

    #@8
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@b
    .line 2073
    invoke-virtual {p0, p9}, Lcom/android/server/DevicePolicyManagerService;->getUserData(I)Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;

    #@e
    move-result-object v2

    #@f
    .line 2075
    .local v2, p:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    invoke-static {p1}, Lcom/android/server/DevicePolicyManagerService;->validateQualityConstant(I)V

    #@12
    .line 2077
    monitor-enter p0

    #@13
    .line 2078
    :try_start_13
    iget v3, v2, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordQuality:I

    #@15
    if-ne v3, p1, :cond_37

    #@17
    iget v3, v2, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordLength:I

    #@19
    if-ne v3, p2, :cond_37

    #@1b
    iget v3, v2, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mFailedPasswordAttempts:I

    #@1d
    if-nez v3, :cond_37

    #@1f
    iget v3, v2, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordLetters:I

    #@21
    if-ne v3, p3, :cond_37

    #@23
    iget v3, v2, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordUpperCase:I

    #@25
    if-ne v3, p4, :cond_37

    #@27
    iget v3, v2, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordLowerCase:I

    #@29
    if-ne v3, p5, :cond_37

    #@2b
    iget v3, v2, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordNumeric:I

    #@2d
    if-ne v3, p6, :cond_37

    #@2f
    iget v3, v2, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordSymbols:I

    #@31
    if-ne v3, p7, :cond_37

    #@33
    iget v3, v2, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordNonLetter:I

    #@35
    if-eq v3, p8, :cond_62

    #@37
    .line 2083
    :cond_37
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_3a
    .catchall {:try_start_13 .. :try_end_3a} :catchall_69

    #@3a
    move-result-wide v0

    #@3b
    .line 2085
    .local v0, ident:J
    :try_start_3b
    iput p1, v2, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordQuality:I

    #@3d
    .line 2086
    iput p2, v2, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordLength:I

    #@3f
    .line 2087
    iput p3, v2, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordLetters:I

    #@41
    .line 2088
    iput p5, v2, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordLowerCase:I

    #@43
    .line 2089
    iput p4, v2, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordUpperCase:I

    #@45
    .line 2090
    iput p6, v2, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordNumeric:I

    #@47
    .line 2091
    iput p7, v2, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordSymbols:I

    #@49
    .line 2092
    iput p8, v2, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mActivePasswordNonLetter:I

    #@4b
    .line 2093
    const/4 v3, 0x0

    #@4c
    iput v3, v2, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mFailedPasswordAttempts:I

    #@4e
    .line 2094
    invoke-direct {p0, p9}, Lcom/android/server/DevicePolicyManagerService;->saveSettingsLocked(I)V

    #@51
    .line 2095
    invoke-direct {p0, p9}, Lcom/android/server/DevicePolicyManagerService;->updatePasswordExpirationsLocked(I)V

    #@54
    .line 2096
    iget-object v3, p0, Lcom/android/server/DevicePolicyManagerService;->mContext:Landroid/content/Context;

    #@56
    invoke-virtual {p0, v3, v2}, Lcom/android/server/DevicePolicyManagerService;->setExpirationAlarmCheckLocked(Landroid/content/Context;Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;)V

    #@59
    .line 2097
    const-string v3, "android.app.action.ACTION_PASSWORD_CHANGED"

    #@5b
    const/4 v4, 0x0

    #@5c
    invoke-virtual {p0, v3, v4, p9}, Lcom/android/server/DevicePolicyManagerService;->sendAdminCommandLocked(Ljava/lang/String;II)V
    :try_end_5f
    .catchall {:try_start_3b .. :try_end_5f} :catchall_64

    #@5f
    .line 2100
    :try_start_5f
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@62
    .line 2103
    .end local v0           #ident:J
    :cond_62
    monitor-exit p0

    #@63
    .line 2104
    return-void

    #@64
    .line 2100
    .restart local v0       #ident:J
    :catchall_64
    move-exception v3

    #@65
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@68
    throw v3

    #@69
    .line 2103
    .end local v0           #ident:J
    :catchall_69
    move-exception v3

    #@6a
    monitor-exit p0
    :try_end_6b
    .catchall {:try_start_5f .. :try_end_6b} :catchall_69

    #@6b
    throw v3
.end method

.method public setCameraDisabled(Landroid/content/ComponentName;ZI)V
    .registers 7
    .parameter "who"
    .parameter "disabled"
    .parameter "userHandle"

    #@0
    .prologue
    .line 2408
    invoke-direct {p0, p3}, Lcom/android/server/DevicePolicyManagerService;->enforceCrossUserPermission(I)V

    #@3
    .line 2409
    monitor-enter p0

    #@4
    .line 2410
    if-nez p1, :cond_11

    #@6
    .line 2411
    :try_start_6
    new-instance v1, Ljava/lang/NullPointerException;

    #@8
    const-string v2, "ComponentName is null"

    #@a
    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@d
    throw v1

    #@e
    .line 2420
    :catchall_e
    move-exception v1

    #@f
    monitor-exit p0
    :try_end_10
    .catchall {:try_start_6 .. :try_end_10} :catchall_e

    #@10
    throw v1

    #@11
    .line 2413
    :cond_11
    const/16 v1, 0x8

    #@13
    :try_start_13
    invoke-virtual {p0, p1, v1}, Lcom/android/server/DevicePolicyManagerService;->getActiveAdminForCallerLocked(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@16
    move-result-object v0

    #@17
    .line 2415
    .local v0, ap:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    iget-boolean v1, v0, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->disableCamera:Z

    #@19
    if-eq v1, p2, :cond_20

    #@1b
    .line 2416
    iput-boolean p2, v0, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->disableCamera:Z

    #@1d
    .line 2417
    invoke-direct {p0, p3}, Lcom/android/server/DevicePolicyManagerService;->saveSettingsLocked(I)V

    #@20
    .line 2419
    :cond_20
    invoke-virtual {p0, p3}, Lcom/android/server/DevicePolicyManagerService;->getUserData(I)Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;

    #@23
    move-result-object v1

    #@24
    invoke-virtual {p0, v1}, Lcom/android/server/DevicePolicyManagerService;->syncDeviceCapabilitiesLocked(Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;)V

    #@27
    .line 2420
    monitor-exit p0
    :try_end_28
    .catchall {:try_start_13 .. :try_end_28} :catchall_e

    #@28
    .line 2421
    return-void
.end method

.method protected setExpirationAlarmCheckLocked(Landroid/content/Context;Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;)V
    .registers 27
    .parameter "context"
    .parameter "policy"

    #@0
    .prologue
    .line 581
    const/16 v19, 0x0

    #@2
    move-object/from16 v0, p2

    #@4
    iget v0, v0, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mUserHandle:I

    #@6
    move/from16 v20, v0

    #@8
    move-object/from16 v0, p0

    #@a
    move-object/from16 v1, v19

    #@c
    move/from16 v2, v20

    #@e
    invoke-direct {v0, v1, v2}, Lcom/android/server/DevicePolicyManagerService;->getPasswordExpirationLocked(Landroid/content/ComponentName;I)J

    #@11
    move-result-wide v10

    #@12
    .line 582
    .local v10, expiration:J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@15
    move-result-wide v12

    #@16
    .line 583
    .local v12, now:J
    sub-long v15, v10, v12

    #@18
    .line 585
    .local v15, timeToExpire:J
    const-wide/16 v19, 0x0

    #@1a
    cmp-long v19, v10, v19

    #@1c
    if-nez v19, :cond_68

    #@1e
    .line 587
    const-wide/16 v7, 0x0

    #@20
    .line 601
    .local v7, alarmTime:J
    :goto_20
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@23
    move-result-wide v17

    #@24
    .line 603
    .local v17, token:J
    :try_start_24
    const-string v19, "alarm"

    #@26
    move-object/from16 v0, p1

    #@28
    move-object/from16 v1, v19

    #@2a
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@2d
    move-result-object v9

    #@2e
    check-cast v9, Landroid/app/AlarmManager;

    #@30
    .line 604
    .local v9, am:Landroid/app/AlarmManager;
    const/16 v19, 0x15c3

    #@32
    new-instance v20, Landroid/content/Intent;

    #@34
    const-string v21, "com.android.server.ACTION_EXPIRED_PASSWORD_NOTIFICATION"

    #@36
    invoke-direct/range {v20 .. v21}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@39
    const/high16 v21, 0x4800

    #@3b
    new-instance v22, Landroid/os/UserHandle;

    #@3d
    move-object/from16 v0, p2

    #@3f
    iget v0, v0, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mUserHandle:I

    #@41
    move/from16 v23, v0

    #@43
    invoke-direct/range {v22 .. v23}, Landroid/os/UserHandle;-><init>(I)V

    #@46
    move-object/from16 v0, p1

    #@48
    move/from16 v1, v19

    #@4a
    move-object/from16 v2, v20

    #@4c
    move/from16 v3, v21

    #@4e
    move-object/from16 v4, v22

    #@50
    invoke-static {v0, v1, v2, v3, v4}, Landroid/app/PendingIntent;->getBroadcastAsUser(Landroid/content/Context;ILandroid/content/Intent;ILandroid/os/UserHandle;)Landroid/app/PendingIntent;

    #@53
    move-result-object v14

    #@54
    .line 608
    .local v14, pi:Landroid/app/PendingIntent;
    invoke-virtual {v9, v14}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    #@57
    .line 609
    const-wide/16 v19, 0x0

    #@59
    cmp-long v19, v7, v19

    #@5b
    if-eqz v19, :cond_64

    #@5d
    .line 610
    const/16 v19, 0x1

    #@5f
    move/from16 v0, v19

    #@61
    invoke-virtual {v9, v0, v7, v8, v14}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V
    :try_end_64
    .catchall {:try_start_24 .. :try_end_64} :catchall_85

    #@64
    .line 613
    :cond_64
    invoke-static/range {v17 .. v18}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@67
    .line 615
    return-void

    #@68
    .line 588
    .end local v7           #alarmTime:J
    .end local v9           #am:Landroid/app/AlarmManager;
    .end local v14           #pi:Landroid/app/PendingIntent;
    .end local v17           #token:J
    :cond_68
    const-wide/16 v19, 0x0

    #@6a
    cmp-long v19, v15, v19

    #@6c
    if-gtz v19, :cond_74

    #@6e
    .line 590
    const-wide/32 v19, 0x5265c00

    #@71
    add-long v7, v12, v19

    #@73
    .restart local v7       #alarmTime:J
    goto :goto_20

    #@74
    .line 594
    .end local v7           #alarmTime:J
    :cond_74
    const-wide/32 v19, 0x5265c00

    #@77
    rem-long v5, v15, v19

    #@79
    .line 595
    .local v5, alarmInterval:J
    const-wide/16 v19, 0x0

    #@7b
    cmp-long v19, v5, v19

    #@7d
    if-nez v19, :cond_82

    #@7f
    .line 596
    const-wide/32 v5, 0x5265c00

    #@82
    .line 598
    :cond_82
    add-long v7, v12, v5

    #@84
    .restart local v7       #alarmTime:J
    goto :goto_20

    #@85
    .line 613
    .end local v5           #alarmInterval:J
    .restart local v17       #token:J
    :catchall_85
    move-exception v19

    #@86
    invoke-static/range {v17 .. v18}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@89
    throw v19
.end method

.method public setGlobalProxy(Landroid/content/ComponentName;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/ComponentName;
    .registers 16
    .parameter "who"
    .parameter "proxySpec"
    .parameter "exclusionList"
    .parameter "userHandle"

    #@0
    .prologue
    .line 2172
    invoke-direct {p0, p4}, Lcom/android/server/DevicePolicyManagerService;->enforceCrossUserPermission(I)V

    #@3
    .line 2173
    monitor-enter p0

    #@4
    .line 2174
    if-nez p1, :cond_11

    #@6
    .line 2175
    :try_start_6
    new-instance v8, Ljava/lang/NullPointerException;

    #@8
    const-string v9, "ComponentName is null"

    #@a
    invoke-direct {v8, v9}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@d
    throw v8

    #@e
    .line 2218
    :catchall_e
    move-exception v8

    #@f
    monitor-exit p0
    :try_end_10
    .catchall {:try_start_6 .. :try_end_10} :catchall_e

    #@10
    throw v8

    #@11
    .line 2179
    :cond_11
    const/4 v8, 0x0

    #@12
    :try_start_12
    invoke-virtual {p0, v8}, Lcom/android/server/DevicePolicyManagerService;->getUserData(I)Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;

    #@15
    move-result-object v7

    #@16
    .line 2180
    .local v7, policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    const/4 v8, 0x5

    #@17
    invoke-virtual {p0, p1, v8}, Lcom/android/server/DevicePolicyManagerService;->getActiveAdminForCallerLocked(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@1a
    move-result-object v0

    #@1b
    .line 2185
    .local v0, admin:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    iget-object v8, v7, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminMap:Ljava/util/HashMap;

    #@1d
    invoke-virtual {v8}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@20
    move-result-object v2

    #@21
    .line 2186
    .local v2, compSet:Ljava/util/Set;,"Ljava/util/Set<Landroid/content/ComponentName;>;"
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@24
    move-result-object v4

    #@25
    .local v4, i$:Ljava/util/Iterator;
    :cond_25
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    #@28
    move-result v8

    #@29
    if-eqz v8, :cond_45

    #@2b
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2e
    move-result-object v3

    #@2f
    check-cast v3, Landroid/content/ComponentName;

    #@31
    .line 2187
    .local v3, component:Landroid/content/ComponentName;
    iget-object v8, v7, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminMap:Ljava/util/HashMap;

    #@33
    invoke-virtual {v8, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@36
    move-result-object v1

    #@37
    check-cast v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@39
    .line 2188
    .local v1, ap:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    iget-boolean v8, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->specifiesGlobalProxy:Z

    #@3b
    if-eqz v8, :cond_25

    #@3d
    invoke-virtual {v3, p1}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    #@40
    move-result v8

    #@41
    if-nez v8, :cond_25

    #@43
    .line 2191
    monitor-exit p0

    #@44
    .line 2217
    .end local v1           #ap:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    .end local v3           #component:Landroid/content/ComponentName;
    :goto_44
    return-object v3

    #@45
    .line 2196
    :cond_45
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@48
    move-result v8

    #@49
    if-eqz v8, :cond_6c

    #@4b
    .line 2197
    const-string v8, "DevicePolicyManagerService"

    #@4d
    new-instance v9, Ljava/lang/StringBuilder;

    #@4f
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@52
    const-string v10, "Only the owner is allowed to set the global proxy. User "

    #@54
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v9

    #@58
    invoke-virtual {v9, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v9

    #@5c
    const-string v10, " is not permitted."

    #@5e
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v9

    #@62
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@65
    move-result-object v9

    #@66
    invoke-static {v8, v9}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@69
    .line 2199
    const/4 v3, 0x0

    #@6a
    monitor-exit p0

    #@6b
    goto :goto_44

    #@6c
    .line 2201
    :cond_6c
    if-nez p2, :cond_84

    #@6e
    .line 2202
    const/4 v8, 0x0

    #@6f
    iput-boolean v8, v0, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->specifiesGlobalProxy:Z

    #@71
    .line 2203
    const/4 v8, 0x0

    #@72
    iput-object v8, v0, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->globalProxySpec:Ljava/lang/String;

    #@74
    .line 2204
    const/4 v8, 0x0

    #@75
    iput-object v8, v0, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->globalProxyExclusionList:Ljava/lang/String;

    #@77
    .line 2214
    :goto_77
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@7a
    move-result-wide v5

    #@7b
    .line 2215
    .local v5, origId:J
    invoke-direct {p0, v7}, Lcom/android/server/DevicePolicyManagerService;->resetGlobalProxyLocked(Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;)V

    #@7e
    .line 2216
    invoke-static {v5, v6}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@81
    .line 2217
    const/4 v3, 0x0

    #@82
    monitor-exit p0

    #@83
    goto :goto_44

    #@84
    .line 2207
    .end local v5           #origId:J
    :cond_84
    const/4 v8, 0x1

    #@85
    iput-boolean v8, v0, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->specifiesGlobalProxy:Z

    #@87
    .line 2208
    iput-object p2, v0, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->globalProxySpec:Ljava/lang/String;

    #@89
    .line 2209
    iput-object p3, v0, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->globalProxyExclusionList:Ljava/lang/String;
    :try_end_8b
    .catchall {:try_start_12 .. :try_end_8b} :catchall_e

    #@8b
    goto :goto_77
.end method

.method public setKeyguardDisabledFeatures(Landroid/content/ComponentName;II)V
    .registers 7
    .parameter "who"
    .parameter "which"
    .parameter "userHandle"

    #@0
    .prologue
    .line 2451
    invoke-direct {p0, p3}, Lcom/android/server/DevicePolicyManagerService;->enforceCrossUserPermission(I)V

    #@3
    .line 2452
    monitor-enter p0

    #@4
    .line 2453
    if-nez p1, :cond_11

    #@6
    .line 2454
    :try_start_6
    new-instance v1, Ljava/lang/NullPointerException;

    #@8
    const-string v2, "ComponentName is null"

    #@a
    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@d
    throw v1

    #@e
    .line 2463
    :catchall_e
    move-exception v1

    #@f
    monitor-exit p0
    :try_end_10
    .catchall {:try_start_6 .. :try_end_10} :catchall_e

    #@10
    throw v1

    #@11
    .line 2456
    :cond_11
    const/16 v1, 0x9

    #@13
    :try_start_13
    invoke-virtual {p0, p1, v1}, Lcom/android/server/DevicePolicyManagerService;->getActiveAdminForCallerLocked(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@16
    move-result-object v0

    #@17
    .line 2458
    .local v0, ap:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    iget v1, v0, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->disabledKeyguardFeatures:I

    #@19
    if-eq v1, p2, :cond_20

    #@1b
    .line 2459
    iput p2, v0, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->disabledKeyguardFeatures:I

    #@1d
    .line 2460
    invoke-direct {p0, p3}, Lcom/android/server/DevicePolicyManagerService;->saveSettingsLocked(I)V

    #@20
    .line 2462
    :cond_20
    invoke-virtual {p0, p3}, Lcom/android/server/DevicePolicyManagerService;->getUserData(I)Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;

    #@23
    move-result-object v1

    #@24
    invoke-virtual {p0, v1}, Lcom/android/server/DevicePolicyManagerService;->syncDeviceCapabilitiesLocked(Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;)V

    #@27
    .line 2463
    monitor-exit p0
    :try_end_28
    .catchall {:try_start_13 .. :try_end_28} :catchall_e

    #@28
    .line 2464
    return-void
.end method

.method public setMaximumFailedPasswordsForWipe(Landroid/content/ComponentName;II)V
    .registers 6
    .parameter "who"
    .parameter "num"
    .parameter "userHandle"

    #@0
    .prologue
    .line 1652
    invoke-direct {p0, p3}, Lcom/android/server/DevicePolicyManagerService;->enforceCrossUserPermission(I)V

    #@3
    .line 1653
    monitor-enter p0

    #@4
    .line 1656
    const/4 v1, 0x4

    #@5
    :try_start_5
    invoke-virtual {p0, p1, v1}, Lcom/android/server/DevicePolicyManagerService;->getActiveAdminForCallerLocked(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@8
    .line 1658
    const/4 v1, 0x1

    #@9
    invoke-virtual {p0, p1, v1}, Lcom/android/server/DevicePolicyManagerService;->getActiveAdminForCallerLocked(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@c
    move-result-object v0

    #@d
    .line 1660
    .local v0, ap:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    iget v1, v0, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->maximumFailedPasswordsForWipe:I

    #@f
    if-eq v1, p2, :cond_16

    #@11
    .line 1661
    iput p2, v0, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->maximumFailedPasswordsForWipe:I

    #@13
    .line 1662
    invoke-direct {p0, p3}, Lcom/android/server/DevicePolicyManagerService;->saveSettingsLocked(I)V

    #@16
    .line 1664
    :cond_16
    monitor-exit p0

    #@17
    .line 1665
    return-void

    #@18
    .line 1664
    .end local v0           #ap:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    :catchall_18
    move-exception v1

    #@19
    monitor-exit p0
    :try_end_1a
    .catchall {:try_start_5 .. :try_end_1a} :catchall_18

    #@1a
    throw v1
.end method

.method public setMaximumTimeToLock(Landroid/content/ComponentName;JI)V
    .registers 8
    .parameter "who"
    .parameter "timeMs"
    .parameter "userHandle"

    #@0
    .prologue
    .line 1832
    invoke-direct {p0, p4}, Lcom/android/server/DevicePolicyManagerService;->enforceCrossUserPermission(I)V

    #@3
    .line 1833
    monitor-enter p0

    #@4
    .line 1834
    if-nez p1, :cond_11

    #@6
    .line 1835
    :try_start_6
    new-instance v1, Ljava/lang/NullPointerException;

    #@8
    const-string v2, "ComponentName is null"

    #@a
    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@d
    throw v1

    #@e
    .line 1844
    :catchall_e
    move-exception v1

    #@f
    monitor-exit p0
    :try_end_10
    .catchall {:try_start_6 .. :try_end_10} :catchall_e

    #@10
    throw v1

    #@11
    .line 1837
    :cond_11
    const/4 v1, 0x3

    #@12
    :try_start_12
    invoke-virtual {p0, p1, v1}, Lcom/android/server/DevicePolicyManagerService;->getActiveAdminForCallerLocked(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@15
    move-result-object v0

    #@16
    .line 1839
    .local v0, ap:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    iget-wide v1, v0, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->maximumTimeToUnlock:J

    #@18
    cmp-long v1, v1, p2

    #@1a
    if-eqz v1, :cond_28

    #@1c
    .line 1840
    iput-wide p2, v0, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->maximumTimeToUnlock:J

    #@1e
    .line 1841
    invoke-direct {p0, p4}, Lcom/android/server/DevicePolicyManagerService;->saveSettingsLocked(I)V

    #@21
    .line 1842
    invoke-virtual {p0, p4}, Lcom/android/server/DevicePolicyManagerService;->getUserData(I)Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {p0, v1}, Lcom/android/server/DevicePolicyManagerService;->updateMaximumTimeToLockLocked(Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;)V

    #@28
    .line 1844
    :cond_28
    monitor-exit p0
    :try_end_29
    .catchall {:try_start_12 .. :try_end_29} :catchall_e

    #@29
    .line 1845
    return-void
.end method

.method public setPasswordExpirationTimeout(Landroid/content/ComponentName;JI)V
    .registers 12
    .parameter "who"
    .parameter "timeout"
    .parameter "userHandle"

    #@0
    .prologue
    const-wide/16 v3, 0x0

    #@2
    .line 1301
    invoke-direct {p0, p4}, Lcom/android/server/DevicePolicyManagerService;->enforceCrossUserPermission(I)V

    #@5
    .line 1302
    monitor-enter p0

    #@6
    .line 1303
    if-nez p1, :cond_13

    #@8
    .line 1304
    :try_start_8
    new-instance v3, Ljava/lang/NullPointerException;

    #@a
    const-string v4, "ComponentName is null"

    #@c
    invoke-direct {v3, v4}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@f
    throw v3

    #@10
    .line 1323
    :catchall_10
    move-exception v3

    #@11
    monitor-exit p0
    :try_end_12
    .catchall {:try_start_8 .. :try_end_12} :catchall_10

    #@12
    throw v3

    #@13
    .line 1306
    :cond_13
    cmp-long v5, p2, v3

    #@15
    if-gez v5, :cond_1f

    #@17
    .line 1307
    :try_start_17
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@19
    const-string v4, "Timeout must be >= 0 ms"

    #@1b
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1e
    throw v3

    #@1f
    .line 1309
    :cond_1f
    const/4 v5, 0x6

    #@20
    invoke-virtual {p0, p1, v5}, Lcom/android/server/DevicePolicyManagerService;->getActiveAdminForCallerLocked(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@23
    move-result-object v0

    #@24
    .line 1312
    .local v0, ap:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    cmp-long v5, p2, v3

    #@26
    if-lez v5, :cond_6b

    #@28
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@2b
    move-result-wide v5

    #@2c
    add-long v1, p2, v5

    #@2e
    .line 1313
    .local v1, expiration:J
    :goto_2e
    iput-wide v1, v0, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->passwordExpirationDate:J

    #@30
    .line 1314
    iput-wide p2, v0, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->passwordExpirationTimeout:J

    #@32
    .line 1315
    cmp-long v3, p2, v3

    #@34
    if-lez v3, :cond_5d

    #@36
    .line 1316
    const-string v3, "DevicePolicyManagerService"

    #@38
    new-instance v4, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v5, "setPasswordExpiration(): password will expire on "

    #@3f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v4

    #@43
    const/4 v5, 0x2

    #@44
    const/4 v6, 0x2

    #@45
    invoke-static {v5, v6}, Ljava/text/DateFormat;->getDateTimeInstance(II)Ljava/text/DateFormat;

    #@48
    move-result-object v5

    #@49
    new-instance v6, Ljava/util/Date;

    #@4b
    invoke-direct {v6, v1, v2}, Ljava/util/Date;-><init>(J)V

    #@4e
    invoke-virtual {v5, v6}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    #@51
    move-result-object v5

    #@52
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v4

    #@56
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v4

    #@5a
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@5d
    .line 1320
    :cond_5d
    invoke-direct {p0, p4}, Lcom/android/server/DevicePolicyManagerService;->saveSettingsLocked(I)V

    #@60
    .line 1322
    iget-object v3, p0, Lcom/android/server/DevicePolicyManagerService;->mContext:Landroid/content/Context;

    #@62
    invoke-virtual {p0, p4}, Lcom/android/server/DevicePolicyManagerService;->getUserData(I)Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;

    #@65
    move-result-object v4

    #@66
    invoke-virtual {p0, v3, v4}, Lcom/android/server/DevicePolicyManagerService;->setExpirationAlarmCheckLocked(Landroid/content/Context;Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;)V

    #@69
    .line 1323
    monitor-exit p0
    :try_end_6a
    .catchall {:try_start_17 .. :try_end_6a} :catchall_10

    #@6a
    .line 1324
    return-void

    #@6b
    .end local v1           #expiration:J
    :cond_6b
    move-wide v1, v3

    #@6c
    .line 1312
    goto :goto_2e
.end method

.method public setPasswordHistoryLength(Landroid/content/ComponentName;II)V
    .registers 7
    .parameter "who"
    .parameter "length"
    .parameter "userHandle"

    #@0
    .prologue
    .line 1264
    invoke-direct {p0, p3}, Lcom/android/server/DevicePolicyManagerService;->enforceCrossUserPermission(I)V

    #@3
    .line 1265
    monitor-enter p0

    #@4
    .line 1266
    if-nez p1, :cond_11

    #@6
    .line 1267
    :try_start_6
    new-instance v1, Ljava/lang/NullPointerException;

    #@8
    const-string v2, "ComponentName is null"

    #@a
    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@d
    throw v1

    #@e
    .line 1275
    :catchall_e
    move-exception v1

    #@f
    monitor-exit p0
    :try_end_10
    .catchall {:try_start_6 .. :try_end_10} :catchall_e

    #@10
    throw v1

    #@11
    .line 1269
    :cond_11
    const/4 v1, 0x0

    #@12
    :try_start_12
    invoke-virtual {p0, p1, v1}, Lcom/android/server/DevicePolicyManagerService;->getActiveAdminForCallerLocked(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@15
    move-result-object v0

    #@16
    .line 1271
    .local v0, ap:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    iget v1, v0, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->passwordHistoryLength:I

    #@18
    if-eq v1, p2, :cond_1f

    #@1a
    .line 1272
    iput p2, v0, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->passwordHistoryLength:I

    #@1c
    .line 1273
    invoke-direct {p0, p3}, Lcom/android/server/DevicePolicyManagerService;->saveSettingsLocked(I)V

    #@1f
    .line 1275
    :cond_1f
    monitor-exit p0
    :try_end_20
    .catchall {:try_start_12 .. :try_end_20} :catchall_e

    #@20
    .line 1276
    return-void
.end method

.method public setPasswordMinimumLength(Landroid/content/ComponentName;II)V
    .registers 7
    .parameter "who"
    .parameter "length"
    .parameter "userHandle"

    #@0
    .prologue
    .line 1227
    invoke-direct {p0, p3}, Lcom/android/server/DevicePolicyManagerService;->enforceCrossUserPermission(I)V

    #@3
    .line 1228
    monitor-enter p0

    #@4
    .line 1229
    if-nez p1, :cond_11

    #@6
    .line 1230
    :try_start_6
    new-instance v1, Ljava/lang/NullPointerException;

    #@8
    const-string v2, "ComponentName is null"

    #@a
    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@d
    throw v1

    #@e
    .line 1238
    :catchall_e
    move-exception v1

    #@f
    monitor-exit p0
    :try_end_10
    .catchall {:try_start_6 .. :try_end_10} :catchall_e

    #@10
    throw v1

    #@11
    .line 1232
    :cond_11
    const/4 v1, 0x0

    #@12
    :try_start_12
    invoke-virtual {p0, p1, v1}, Lcom/android/server/DevicePolicyManagerService;->getActiveAdminForCallerLocked(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@15
    move-result-object v0

    #@16
    .line 1234
    .local v0, ap:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    iget v1, v0, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->minimumPasswordLength:I

    #@18
    if-eq v1, p2, :cond_1f

    #@1a
    .line 1235
    iput p2, v0, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->minimumPasswordLength:I

    #@1c
    .line 1236
    invoke-direct {p0, p3}, Lcom/android/server/DevicePolicyManagerService;->saveSettingsLocked(I)V

    #@1f
    .line 1238
    :cond_1f
    monitor-exit p0
    :try_end_20
    .catchall {:try_start_12 .. :try_end_20} :catchall_e

    #@20
    .line 1239
    return-void
.end method

.method public setPasswordMinimumLetters(Landroid/content/ComponentName;II)V
    .registers 7
    .parameter "who"
    .parameter "length"
    .parameter "userHandle"

    #@0
    .prologue
    .line 1457
    invoke-direct {p0, p3}, Lcom/android/server/DevicePolicyManagerService;->enforceCrossUserPermission(I)V

    #@3
    .line 1458
    monitor-enter p0

    #@4
    .line 1459
    if-nez p1, :cond_11

    #@6
    .line 1460
    :try_start_6
    new-instance v1, Ljava/lang/NullPointerException;

    #@8
    const-string v2, "ComponentName is null"

    #@a
    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@d
    throw v1

    #@e
    .line 1468
    :catchall_e
    move-exception v1

    #@f
    monitor-exit p0
    :try_end_10
    .catchall {:try_start_6 .. :try_end_10} :catchall_e

    #@10
    throw v1

    #@11
    .line 1462
    :cond_11
    const/4 v1, 0x0

    #@12
    :try_start_12
    invoke-virtual {p0, p1, v1}, Lcom/android/server/DevicePolicyManagerService;->getActiveAdminForCallerLocked(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@15
    move-result-object v0

    #@16
    .line 1464
    .local v0, ap:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    iget v1, v0, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->minimumPasswordLetters:I

    #@18
    if-eq v1, p2, :cond_1f

    #@1a
    .line 1465
    iput p2, v0, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->minimumPasswordLetters:I

    #@1c
    .line 1466
    invoke-direct {p0, p3}, Lcom/android/server/DevicePolicyManagerService;->saveSettingsLocked(I)V

    #@1f
    .line 1468
    :cond_1f
    monitor-exit p0
    :try_end_20
    .catchall {:try_start_12 .. :try_end_20} :catchall_e

    #@20
    .line 1469
    return-void
.end method

.method public setPasswordMinimumLowerCase(Landroid/content/ComponentName;II)V
    .registers 7
    .parameter "who"
    .parameter "length"
    .parameter "userHandle"

    #@0
    .prologue
    .line 1420
    invoke-direct {p0, p3}, Lcom/android/server/DevicePolicyManagerService;->enforceCrossUserPermission(I)V

    #@3
    .line 1421
    monitor-enter p0

    #@4
    .line 1422
    if-nez p1, :cond_11

    #@6
    .line 1423
    :try_start_6
    new-instance v1, Ljava/lang/NullPointerException;

    #@8
    const-string v2, "ComponentName is null"

    #@a
    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@d
    throw v1

    #@e
    .line 1431
    :catchall_e
    move-exception v1

    #@f
    monitor-exit p0
    :try_end_10
    .catchall {:try_start_6 .. :try_end_10} :catchall_e

    #@10
    throw v1

    #@11
    .line 1425
    :cond_11
    const/4 v1, 0x0

    #@12
    :try_start_12
    invoke-virtual {p0, p1, v1}, Lcom/android/server/DevicePolicyManagerService;->getActiveAdminForCallerLocked(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@15
    move-result-object v0

    #@16
    .line 1427
    .local v0, ap:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    iget v1, v0, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->minimumPasswordLowerCase:I

    #@18
    if-eq v1, p2, :cond_1f

    #@1a
    .line 1428
    iput p2, v0, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->minimumPasswordLowerCase:I

    #@1c
    .line 1429
    invoke-direct {p0, p3}, Lcom/android/server/DevicePolicyManagerService;->saveSettingsLocked(I)V

    #@1f
    .line 1431
    :cond_1f
    monitor-exit p0
    :try_end_20
    .catchall {:try_start_12 .. :try_end_20} :catchall_e

    #@20
    .line 1432
    return-void
.end method

.method public setPasswordMinimumNonLetter(Landroid/content/ComponentName;II)V
    .registers 7
    .parameter "who"
    .parameter "length"
    .parameter "userHandle"

    #@0
    .prologue
    .line 1568
    invoke-direct {p0, p3}, Lcom/android/server/DevicePolicyManagerService;->enforceCrossUserPermission(I)V

    #@3
    .line 1569
    monitor-enter p0

    #@4
    .line 1570
    if-nez p1, :cond_11

    #@6
    .line 1571
    :try_start_6
    new-instance v1, Ljava/lang/NullPointerException;

    #@8
    const-string v2, "ComponentName is null"

    #@a
    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@d
    throw v1

    #@e
    .line 1579
    :catchall_e
    move-exception v1

    #@f
    monitor-exit p0
    :try_end_10
    .catchall {:try_start_6 .. :try_end_10} :catchall_e

    #@10
    throw v1

    #@11
    .line 1573
    :cond_11
    const/4 v1, 0x0

    #@12
    :try_start_12
    invoke-virtual {p0, p1, v1}, Lcom/android/server/DevicePolicyManagerService;->getActiveAdminForCallerLocked(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@15
    move-result-object v0

    #@16
    .line 1575
    .local v0, ap:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    iget v1, v0, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->minimumPasswordNonLetter:I

    #@18
    if-eq v1, p2, :cond_1f

    #@1a
    .line 1576
    iput p2, v0, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->minimumPasswordNonLetter:I

    #@1c
    .line 1577
    invoke-direct {p0, p3}, Lcom/android/server/DevicePolicyManagerService;->saveSettingsLocked(I)V

    #@1f
    .line 1579
    :cond_1f
    monitor-exit p0
    :try_end_20
    .catchall {:try_start_12 .. :try_end_20} :catchall_e

    #@20
    .line 1580
    return-void
.end method

.method public setPasswordMinimumNumeric(Landroid/content/ComponentName;II)V
    .registers 7
    .parameter "who"
    .parameter "length"
    .parameter "userHandle"

    #@0
    .prologue
    .line 1494
    invoke-direct {p0, p3}, Lcom/android/server/DevicePolicyManagerService;->enforceCrossUserPermission(I)V

    #@3
    .line 1495
    monitor-enter p0

    #@4
    .line 1496
    if-nez p1, :cond_11

    #@6
    .line 1497
    :try_start_6
    new-instance v1, Ljava/lang/NullPointerException;

    #@8
    const-string v2, "ComponentName is null"

    #@a
    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@d
    throw v1

    #@e
    .line 1505
    :catchall_e
    move-exception v1

    #@f
    monitor-exit p0
    :try_end_10
    .catchall {:try_start_6 .. :try_end_10} :catchall_e

    #@10
    throw v1

    #@11
    .line 1499
    :cond_11
    const/4 v1, 0x0

    #@12
    :try_start_12
    invoke-virtual {p0, p1, v1}, Lcom/android/server/DevicePolicyManagerService;->getActiveAdminForCallerLocked(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@15
    move-result-object v0

    #@16
    .line 1501
    .local v0, ap:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    iget v1, v0, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->minimumPasswordNumeric:I

    #@18
    if-eq v1, p2, :cond_1f

    #@1a
    .line 1502
    iput p2, v0, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->minimumPasswordNumeric:I

    #@1c
    .line 1503
    invoke-direct {p0, p3}, Lcom/android/server/DevicePolicyManagerService;->saveSettingsLocked(I)V

    #@1f
    .line 1505
    :cond_1f
    monitor-exit p0
    :try_end_20
    .catchall {:try_start_12 .. :try_end_20} :catchall_e

    #@20
    .line 1506
    return-void
.end method

.method public setPasswordMinimumSymbols(Landroid/content/ComponentName;II)V
    .registers 7
    .parameter "who"
    .parameter "length"
    .parameter "userHandle"

    #@0
    .prologue
    .line 1531
    invoke-direct {p0, p3}, Lcom/android/server/DevicePolicyManagerService;->enforceCrossUserPermission(I)V

    #@3
    .line 1532
    monitor-enter p0

    #@4
    .line 1533
    if-nez p1, :cond_11

    #@6
    .line 1534
    :try_start_6
    new-instance v1, Ljava/lang/NullPointerException;

    #@8
    const-string v2, "ComponentName is null"

    #@a
    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@d
    throw v1

    #@e
    .line 1542
    :catchall_e
    move-exception v1

    #@f
    monitor-exit p0
    :try_end_10
    .catchall {:try_start_6 .. :try_end_10} :catchall_e

    #@10
    throw v1

    #@11
    .line 1536
    :cond_11
    const/4 v1, 0x0

    #@12
    :try_start_12
    invoke-virtual {p0, p1, v1}, Lcom/android/server/DevicePolicyManagerService;->getActiveAdminForCallerLocked(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@15
    move-result-object v0

    #@16
    .line 1538
    .local v0, ap:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    iget v1, v0, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->minimumPasswordSymbols:I

    #@18
    if-eq v1, p2, :cond_1f

    #@1a
    .line 1539
    iput p2, v0, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->minimumPasswordSymbols:I

    #@1c
    .line 1540
    invoke-direct {p0, p3}, Lcom/android/server/DevicePolicyManagerService;->saveSettingsLocked(I)V

    #@1f
    .line 1542
    :cond_1f
    monitor-exit p0
    :try_end_20
    .catchall {:try_start_12 .. :try_end_20} :catchall_e

    #@20
    .line 1543
    return-void
.end method

.method public setPasswordMinimumUpperCase(Landroid/content/ComponentName;II)V
    .registers 7
    .parameter "who"
    .parameter "length"
    .parameter "userHandle"

    #@0
    .prologue
    .line 1383
    invoke-direct {p0, p3}, Lcom/android/server/DevicePolicyManagerService;->enforceCrossUserPermission(I)V

    #@3
    .line 1384
    monitor-enter p0

    #@4
    .line 1385
    if-nez p1, :cond_11

    #@6
    .line 1386
    :try_start_6
    new-instance v1, Ljava/lang/NullPointerException;

    #@8
    const-string v2, "ComponentName is null"

    #@a
    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@d
    throw v1

    #@e
    .line 1394
    :catchall_e
    move-exception v1

    #@f
    monitor-exit p0
    :try_end_10
    .catchall {:try_start_6 .. :try_end_10} :catchall_e

    #@10
    throw v1

    #@11
    .line 1388
    :cond_11
    const/4 v1, 0x0

    #@12
    :try_start_12
    invoke-virtual {p0, p1, v1}, Lcom/android/server/DevicePolicyManagerService;->getActiveAdminForCallerLocked(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@15
    move-result-object v0

    #@16
    .line 1390
    .local v0, ap:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    iget v1, v0, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->minimumPasswordUpperCase:I

    #@18
    if-eq v1, p2, :cond_1f

    #@1a
    .line 1391
    iput p2, v0, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->minimumPasswordUpperCase:I

    #@1c
    .line 1392
    invoke-direct {p0, p3}, Lcom/android/server/DevicePolicyManagerService;->saveSettingsLocked(I)V

    #@1f
    .line 1394
    :cond_1f
    monitor-exit p0
    :try_end_20
    .catchall {:try_start_12 .. :try_end_20} :catchall_e

    #@20
    .line 1395
    return-void
.end method

.method public setPasswordQuality(Landroid/content/ComponentName;II)V
    .registers 7
    .parameter "who"
    .parameter "quality"
    .parameter "userHandle"

    #@0
    .prologue
    .line 1188
    invoke-static {p2}, Lcom/android/server/DevicePolicyManagerService;->validateQualityConstant(I)V

    #@3
    .line 1189
    invoke-direct {p0, p3}, Lcom/android/server/DevicePolicyManagerService;->enforceCrossUserPermission(I)V

    #@6
    .line 1191
    monitor-enter p0

    #@7
    .line 1192
    if-nez p1, :cond_14

    #@9
    .line 1193
    :try_start_9
    new-instance v1, Ljava/lang/NullPointerException;

    #@b
    const-string v2, "ComponentName is null"

    #@d
    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@10
    throw v1

    #@11
    .line 1201
    :catchall_11
    move-exception v1

    #@12
    monitor-exit p0
    :try_end_13
    .catchall {:try_start_9 .. :try_end_13} :catchall_11

    #@13
    throw v1

    #@14
    .line 1195
    :cond_14
    const/4 v1, 0x0

    #@15
    :try_start_15
    invoke-virtual {p0, p1, v1}, Lcom/android/server/DevicePolicyManagerService;->getActiveAdminForCallerLocked(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@18
    move-result-object v0

    #@19
    .line 1197
    .local v0, ap:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    iget v1, v0, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->passwordQuality:I

    #@1b
    if-eq v1, p2, :cond_22

    #@1d
    .line 1198
    iput p2, v0, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->passwordQuality:I

    #@1f
    .line 1199
    invoke-direct {p0, p3}, Lcom/android/server/DevicePolicyManagerService;->saveSettingsLocked(I)V

    #@22
    .line 1201
    :cond_22
    monitor-exit p0
    :try_end_23
    .catchall {:try_start_15 .. :try_end_23} :catchall_11

    #@23
    .line 1202
    return-void
.end method

.method public setStorageEncryption(Landroid/content/ComponentName;ZI)I
    .registers 13
    .parameter "who"
    .parameter "encrypt"
    .parameter "userHandle"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 2283
    invoke-direct {p0, p3}, Lcom/android/server/DevicePolicyManagerService;->enforceCrossUserPermission(I)V

    #@4
    .line 2284
    monitor-enter p0

    #@5
    .line 2286
    if-nez p1, :cond_12

    #@7
    .line 2287
    :try_start_7
    new-instance v5, Ljava/lang/NullPointerException;

    #@9
    const-string v6, "ComponentName is null"

    #@b
    invoke-direct {v5, v6}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@e
    throw v5

    #@f
    .line 2326
    :catchall_f
    move-exception v5

    #@10
    monitor-exit p0
    :try_end_11
    .catchall {:try_start_7 .. :try_end_11} :catchall_f

    #@11
    throw v5

    #@12
    .line 2290
    :cond_12
    if-nez p3, :cond_1a

    #@14
    :try_start_14
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@17
    move-result v6

    #@18
    if-eqz v6, :cond_3e

    #@1a
    .line 2292
    :cond_1a
    const-string v6, "DevicePolicyManagerService"

    #@1c
    new-instance v7, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v8, "Only owner is allowed to set storage encryption. User "

    #@23
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v7

    #@27
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@2a
    move-result v8

    #@2b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v7

    #@2f
    const-string v8, " is not permitted."

    #@31
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v7

    #@35
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v7

    #@39
    invoke-static {v6, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3c
    .line 2294
    monitor-exit p0

    #@3d
    .line 2323
    :goto_3d
    return v5

    #@3e
    .line 2297
    :cond_3e
    const/4 v6, 0x7

    #@3f
    invoke-virtual {p0, p1, v6}, Lcom/android/server/DevicePolicyManagerService;->getActiveAdminForCallerLocked(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@42
    move-result-object v1

    #@43
    .line 2301
    .local v1, ap:Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
    invoke-direct {p0}, Lcom/android/server/DevicePolicyManagerService;->isEncryptionSupported()Z

    #@46
    move-result v6

    #@47
    if-nez v6, :cond_4b

    #@49
    .line 2302
    monitor-exit p0

    #@4a
    goto :goto_3d

    #@4b
    .line 2306
    :cond_4b
    iget-boolean v5, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->encryptionRequested:Z

    #@4d
    if-eq v5, p2, :cond_54

    #@4f
    .line 2307
    iput-boolean p2, v1, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->encryptionRequested:Z

    #@51
    .line 2308
    invoke-direct {p0, p3}, Lcom/android/server/DevicePolicyManagerService;->saveSettingsLocked(I)V

    #@54
    .line 2311
    :cond_54
    const/4 v5, 0x0

    #@55
    invoke-virtual {p0, v5}, Lcom/android/server/DevicePolicyManagerService;->getUserData(I)Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;

    #@58
    move-result-object v4

    #@59
    .line 2313
    .local v4, policy:Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;
    const/4 v3, 0x0

    #@5a
    .line 2314
    .local v3, newRequested:Z
    iget-object v5, v4, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@5c
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@5f
    move-result v0

    #@60
    .line 2315
    .local v0, N:I
    const/4 v2, 0x0

    #@61
    .local v2, i:I
    :goto_61
    if-ge v2, v0, :cond_71

    #@63
    .line 2316
    iget-object v5, v4, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@65
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@68
    move-result-object v5

    #@69
    check-cast v5, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@6b
    iget-boolean v5, v5, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->encryptionRequested:Z

    #@6d
    or-int/2addr v3, v5

    #@6e
    .line 2315
    add-int/lit8 v2, v2, 0x1

    #@70
    goto :goto_61

    #@71
    .line 2320
    :cond_71
    invoke-direct {p0, v3}, Lcom/android/server/DevicePolicyManagerService;->setEncryptionRequested(Z)V

    #@74
    .line 2323
    if-eqz v3, :cond_79

    #@76
    const/4 v5, 0x3

    #@77
    :goto_77
    monitor-exit p0
    :try_end_78
    .catchall {:try_start_14 .. :try_end_78} :catchall_f

    #@78
    goto :goto_3d

    #@79
    :cond_79
    const/4 v5, 0x1

    #@7a
    goto :goto_77
.end method

.method syncDeviceCapabilitiesLocked(Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;)V
    .registers 9
    .parameter "policy"

    #@0
    .prologue
    .line 1028
    const-string v5, "sys.secpolicy.camera.disabled"

    #@2
    const/4 v6, 0x0

    #@3
    invoke-static {v5, v6}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@6
    move-result v1

    #@7
    .line 1029
    .local v1, systemState:Z
    const/4 v5, 0x0

    #@8
    iget v6, p1, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mUserHandle:I

    #@a
    invoke-virtual {p0, v5, v6}, Lcom/android/server/DevicePolicyManagerService;->getCameraDisabled(Landroid/content/ComponentName;I)Z

    #@d
    move-result v0

    #@e
    .line 1030
    .local v0, cameraDisabled:Z
    if-eq v0, v1, :cond_2f

    #@10
    .line 1031
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@13
    move-result-wide v2

    #@14
    .line 1033
    .local v2, token:J
    if-eqz v0, :cond_30

    #@16
    :try_start_16
    const-string v4, "1"

    #@18
    .line 1036
    .local v4, value:Ljava/lang/String;
    :goto_18
    const-string v5, "sys.secpolicy.camera.disabled"

    #@1a
    invoke-static {v5, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@1d
    .line 1038
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@20
    move-result-object v5

    #@21
    if-eqz v5, :cond_2c

    #@23
    .line 1039
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@26
    move-result-object v5

    #@27
    iget v6, p1, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mUserHandle:I

    #@29
    invoke-interface {v5, v0, v6}, Lcom/lge/cappuccino/IMdm;->setCameraStop(ZI)V
    :try_end_2c
    .catchall {:try_start_16 .. :try_end_2c} :catchall_33

    #@2c
    .line 1042
    :cond_2c
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@2f
    .line 1045
    .end local v2           #token:J
    .end local v4           #value:Ljava/lang/String;
    :cond_2f
    return-void

    #@30
    .line 1033
    .restart local v2       #token:J
    :cond_30
    :try_start_30
    const-string v4, "0"
    :try_end_32
    .catchall {:try_start_30 .. :try_end_32} :catchall_33

    #@32
    goto :goto_18

    #@33
    .line 1042
    :catchall_33
    move-exception v5

    #@34
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@37
    throw v5
.end method

.method public systemReady()V
    .registers 3

    #@0
    .prologue
    .line 1048
    monitor-enter p0

    #@1
    .line 1049
    const/4 v0, 0x0

    #@2
    :try_start_2
    invoke-virtual {p0, v0}, Lcom/android/server/DevicePolicyManagerService;->getUserData(I)Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;

    #@5
    move-result-object v0

    #@6
    const/4 v1, 0x0

    #@7
    invoke-direct {p0, v0, v1}, Lcom/android/server/DevicePolicyManagerService;->loadSettingsLocked(Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;I)V

    #@a
    .line 1050
    monitor-exit p0

    #@b
    .line 1051
    return-void

    #@c
    .line 1050
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0
    :try_end_e
    .catchall {:try_start_2 .. :try_end_e} :catchall_c

    #@e
    throw v0
.end method

.method updateMaximumTimeToLockLocked(Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;)V
    .registers 11
    .parameter "policy"

    #@0
    .prologue
    .line 1848
    const/4 v6, 0x0

    #@1
    iget v7, p1, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mUserHandle:I

    #@3
    invoke-virtual {p0, v6, v7}, Lcom/android/server/DevicePolicyManagerService;->getMaximumTimeToLock(Landroid/content/ComponentName;I)J

    #@6
    move-result-wide v4

    #@7
    .line 1849
    .local v4, timeMs:J
    iget-wide v6, p1, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mLastMaximumTimeToLock:J

    #@9
    cmp-long v6, v6, v4

    #@b
    if-nez v6, :cond_e

    #@d
    .line 1880
    :goto_d
    return-void

    #@e
    .line 1853
    :cond_e
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@11
    move-result-wide v1

    #@12
    .line 1855
    .local v1, ident:J
    const-wide/16 v6, 0x0

    #@14
    cmp-long v6, v4, v6

    #@16
    if-gtz v6, :cond_48

    #@18
    .line 1856
    const-wide/32 v4, 0x7fffffff

    #@1b
    .line 1864
    :goto_1b
    :try_start_1b
    iput-wide v4, p1, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mLastMaximumTimeToLock:J
    :try_end_1d
    .catchall {:try_start_1b .. :try_end_1d} :catchall_55

    #@1d
    .line 1867
    :try_start_1d
    invoke-direct {p0}, Lcom/android/server/DevicePolicyManagerService;->getIPowerManager()Landroid/os/IPowerManager;

    #@20
    move-result-object v6

    #@21
    long-to-int v7, v4

    #@22
    invoke-interface {v6, v7}, Landroid/os/IPowerManager;->setMaximumScreenOffTimeoutFromDeviceAdmin(I)V

    #@25
    .line 1869
    new-instance v3, Landroid/content/Intent;

    #@27
    const-string v6, "com.lge.mdm.ACTION_MAMXIMUM_TIME_TOLOCK"

    #@29
    invoke-direct {v3, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@2c
    .line 1870
    .local v3, intent:Landroid/content/Intent;
    const-string v6, "MaximumTime"

    #@2e
    iget-wide v7, p1, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mLastMaximumTimeToLock:J

    #@30
    invoke-virtual {v3, v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    #@33
    .line 1871
    const/high16 v6, 0x800

    #@35
    invoke-virtual {v3, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@38
    .line 1872
    iget-object v6, p0, Lcom/android/server/DevicePolicyManagerService;->mContext:Landroid/content/Context;

    #@3a
    new-instance v7, Landroid/os/UserHandle;

    #@3c
    iget v8, p1, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mUserHandle:I

    #@3e
    invoke-direct {v7, v8}, Landroid/os/UserHandle;-><init>(I)V

    #@41
    invoke-virtual {v6, v3, v7}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
    :try_end_44
    .catchall {:try_start_1d .. :try_end_44} :catchall_55
    .catch Landroid/os/RemoteException; {:try_start_1d .. :try_end_44} :catch_5a

    #@44
    .line 1878
    .end local v3           #intent:Landroid/content/Intent;
    :goto_44
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@47
    goto :goto_d

    #@48
    .line 1860
    :cond_48
    :try_start_48
    iget-object v6, p0, Lcom/android/server/DevicePolicyManagerService;->mContext:Landroid/content/Context;

    #@4a
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@4d
    move-result-object v6

    #@4e
    const-string v7, "stay_on_while_plugged_in"

    #@50
    const/4 v8, 0x0

    #@51
    invoke-static {v6, v7, v8}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_54
    .catchall {:try_start_48 .. :try_end_54} :catchall_55

    #@54
    goto :goto_1b

    #@55
    .line 1878
    :catchall_55
    move-exception v6

    #@56
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@59
    throw v6

    #@5a
    .line 1874
    :catch_5a
    move-exception v0

    #@5b
    .line 1875
    .local v0, e:Landroid/os/RemoteException;
    :try_start_5b
    const-string v6, "DevicePolicyManagerService"

    #@5d
    const-string v7, "Failure talking with power manager"

    #@5f
    invoke-static {v6, v7, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_62
    .catchall {:try_start_5b .. :try_end_62} :catchall_55

    #@62
    goto :goto_44
.end method

.method validatePasswordOwnerLocked(Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;)V
    .registers 7
    .parameter "policy"

    #@0
    .prologue
    .line 1005
    iget v2, p1, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mPasswordOwner:I

    #@2
    if-ltz v2, :cond_45

    #@4
    .line 1006
    const/4 v0, 0x0

    #@5
    .line 1007
    .local v0, haveOwner:Z
    iget-object v2, p1, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@a
    move-result v2

    #@b
    add-int/lit8 v1, v2, -0x1

    #@d
    .local v1, i:I
    :goto_d
    if-ltz v1, :cond_20

    #@f
    .line 1008
    iget-object v2, p1, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mAdminList:Ljava/util/ArrayList;

    #@11
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@14
    move-result-object v2

    #@15
    check-cast v2, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@17
    invoke-virtual {v2}, Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;->getUid()I

    #@1a
    move-result v2

    #@1b
    iget v3, p1, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mPasswordOwner:I

    #@1d
    if-ne v2, v3, :cond_46

    #@1f
    .line 1009
    const/4 v0, 0x1

    #@20
    .line 1013
    :cond_20
    if-nez v0, :cond_45

    #@22
    .line 1014
    const-string v2, "DevicePolicyManagerService"

    #@24
    new-instance v3, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v4, "Previous password owner "

    #@2b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v3

    #@2f
    iget v4, p1, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mPasswordOwner:I

    #@31
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@34
    move-result-object v3

    #@35
    const-string v4, " no longer active; disabling"

    #@37
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v3

    #@3b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v3

    #@3f
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@42
    .line 1016
    const/4 v2, -0x1

    #@43
    iput v2, p1, Lcom/android/server/DevicePolicyManagerService$DevicePolicyData;->mPasswordOwner:I

    #@45
    .line 1019
    .end local v0           #haveOwner:Z
    .end local v1           #i:I
    :cond_45
    return-void

    #@46
    .line 1007
    .restart local v0       #haveOwner:Z
    .restart local v1       #i:I
    :cond_46
    add-int/lit8 v1, v1, -0x1

    #@48
    goto :goto_d
.end method

.method public wipeData(II)V
    .registers 7
    .parameter "flags"
    .parameter "userHandle"

    #@0
    .prologue
    .line 1986
    invoke-direct {p0, p2}, Lcom/android/server/DevicePolicyManagerService;->enforceCrossUserPermission(I)V

    #@3
    .line 1987
    monitor-enter p0

    #@4
    .line 1990
    const/4 v2, 0x0

    #@5
    const/4 v3, 0x4

    #@6
    :try_start_6
    invoke-virtual {p0, v2, v3}, Lcom/android/server/DevicePolicyManagerService;->getActiveAdminForCallerLocked(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;

    #@9
    .line 1992
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_c
    .catchall {:try_start_6 .. :try_end_c} :catchall_1a

    #@c
    move-result-wide v0

    #@d
    .line 1994
    .local v0, ident:J
    :try_start_d
    invoke-direct {p0, p1, p2}, Lcom/android/server/DevicePolicyManagerService;->wipeDeviceOrUserLocked(II)V
    :try_end_10
    .catchall {:try_start_d .. :try_end_10} :catchall_15

    #@10
    .line 1996
    :try_start_10
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@13
    .line 1998
    monitor-exit p0

    #@14
    .line 1999
    return-void

    #@15
    .line 1996
    :catchall_15
    move-exception v2

    #@16
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@19
    throw v2

    #@1a
    .line 1998
    .end local v0           #ident:J
    :catchall_1a
    move-exception v2

    #@1b
    monitor-exit p0
    :try_end_1c
    .catchall {:try_start_10 .. :try_end_1c} :catchall_1a

    #@1c
    throw v2
.end method

.method wipeDataLocked(I)V
    .registers 12
    .parameter "flags"

    #@0
    .prologue
    const-wide/16 v8, 0x2710

    #@2
    const/4 v5, 0x0

    #@3
    const/4 v4, 0x1

    #@4
    .line 1938
    invoke-static {}, Landroid/os/Environment;->isExternalStorageRemovable()Z

    #@7
    move-result v6

    #@8
    if-nez v6, :cond_37

    #@a
    invoke-direct {p0}, Lcom/android/server/DevicePolicyManagerService;->isExtStorageEncrypted()Z

    #@d
    move-result v6

    #@e
    if-eqz v6, :cond_37

    #@10
    move v1, v4

    #@11
    .line 1939
    .local v1, forceExtWipe:Z
    :goto_11
    and-int/lit8 v6, p1, 0x1

    #@13
    if-eqz v6, :cond_39

    #@15
    move v3, v4

    #@16
    .line 1942
    .local v3, wipeExtRequested:Z
    :goto_16
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@19
    move-result-object v6

    #@1a
    if-eqz v6, :cond_43

    #@1c
    .line 1943
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@1f
    move-result-object v6

    #@20
    iget-object v7, p0, Lcom/android/server/DevicePolicyManagerService;->mContext:Landroid/content/Context;

    #@22
    invoke-interface {v6, v7}, Lcom/lge/cappuccino/IMdm;->getExternalMemoryMounted(Landroid/content/Context;)Z

    #@25
    move-result v1

    #@26
    .line 1944
    if-eqz v1, :cond_3b

    #@28
    if-eqz v3, :cond_3b

    #@2a
    .line 1945
    iget-object v5, p0, Lcom/android/server/DevicePolicyManagerService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2c
    invoke-virtual {v5, v8, v9}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    #@2f
    .line 1946
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@32
    move-result-object v5

    #@33
    invoke-interface {v5, v4}, Lcom/lge/cappuccino/IMdm;->wipeData(I)V

    #@36
    .line 1983
    :goto_36
    return-void

    #@37
    .end local v1           #forceExtWipe:Z
    .end local v3           #wipeExtRequested:Z
    :cond_37
    move v1, v5

    #@38
    .line 1938
    goto :goto_11

    #@39
    .restart local v1       #forceExtWipe:Z
    :cond_39
    move v3, v5

    #@3a
    .line 1939
    goto :goto_16

    #@3b
    .line 1948
    .restart local v3       #wipeExtRequested:Z
    :cond_3b
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@3e
    move-result-object v4

    #@3f
    invoke-interface {v4, v5}, Lcom/lge/cappuccino/IMdm;->wipeData(I)V

    #@42
    goto :goto_36

    #@43
    .line 1952
    :cond_43
    if-nez v1, :cond_47

    #@45
    if-eqz v3, :cond_69

    #@47
    :cond_47
    invoke-static {}, Landroid/os/Environment;->isExternalStorageEmulated()Z

    #@4a
    move-result v5

    #@4b
    if-nez v5, :cond_69

    #@4d
    .line 1953
    new-instance v2, Landroid/content/Intent;

    #@4f
    const-string v5, "com.android.internal.os.storage.FORMAT_AND_FACTORY_RESET"

    #@51
    invoke-direct {v2, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@54
    .line 1954
    .local v2, intent:Landroid/content/Intent;
    const-string v5, "always_reset"

    #@56
    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@59
    .line 1955
    sget-object v4, Lcom/android/internal/os/storage/ExternalStorageFormatter;->COMPONENT_NAME:Landroid/content/ComponentName;

    #@5b
    invoke-virtual {v2, v4}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@5e
    .line 1956
    iget-object v4, p0, Lcom/android/server/DevicePolicyManagerService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@60
    invoke-virtual {v4, v8, v9}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    #@63
    .line 1957
    iget-object v4, p0, Lcom/android/server/DevicePolicyManagerService;->mContext:Landroid/content/Context;

    #@65
    invoke-virtual {v4, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    #@68
    goto :goto_36

    #@69
    .line 1960
    .end local v2           #intent:Landroid/content/Intent;
    :cond_69
    :try_start_69
    iget-object v4, p0, Lcom/android/server/DevicePolicyManagerService;->mContext:Landroid/content/Context;

    #@6b
    invoke-static {v4}, Landroid/os/RecoverySystem;->rebootWipeUserData(Landroid/content/Context;)V
    :try_end_6e
    .catch Ljava/io/IOException; {:try_start_69 .. :try_end_6e} :catch_6f

    #@6e
    goto :goto_36

    #@6f
    .line 1961
    :catch_6f
    move-exception v0

    #@70
    .line 1962
    .local v0, e:Ljava/io/IOException;
    const-string v4, "DevicePolicyManagerService"

    #@72
    const-string v5, "Failed requesting data wipe"

    #@74
    invoke-static {v4, v5, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@77
    goto :goto_36
.end method
