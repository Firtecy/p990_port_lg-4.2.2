.class Lcom/android/server/AppWidgetServiceImpl$3;
.super Ljava/lang/Object;
.source "AppWidgetServiceImpl.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/AppWidgetServiceImpl;->notifyAppWidgetViewDataChangedInstanceLocked(Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/AppWidgetServiceImpl;


# direct methods
.method constructor <init>(Lcom/android/server/AppWidgetServiceImpl;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1082
    iput-object p1, p0, Lcom/android/server/AppWidgetServiceImpl$3;->this$0:Lcom/android/server/AppWidgetServiceImpl;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .registers 6
    .parameter "name"
    .parameter "service"

    #@0
    .prologue
    .line 1085
    invoke-static {p2}, Lcom/android/internal/widget/IRemoteViewsFactory$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/widget/IRemoteViewsFactory;

    #@3
    move-result-object v0

    #@4
    .line 1088
    .local v0, cb:Lcom/android/internal/widget/IRemoteViewsFactory;
    :try_start_4
    invoke-interface {v0}, Lcom/android/internal/widget/IRemoteViewsFactory;->onDataSetChangedAsync()V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_7} :catch_f
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_7} :catch_14

    #@7
    .line 1094
    :goto_7
    iget-object v2, p0, Lcom/android/server/AppWidgetServiceImpl$3;->this$0:Lcom/android/server/AppWidgetServiceImpl;

    #@9
    iget-object v2, v2, Lcom/android/server/AppWidgetServiceImpl;->mContext:Landroid/content/Context;

    #@b
    invoke-virtual {v2, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    #@e
    .line 1095
    return-void

    #@f
    .line 1089
    :catch_f
    move-exception v1

    #@10
    .line 1090
    .local v1, e:Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    #@13
    goto :goto_7

    #@14
    .line 1091
    .end local v1           #e:Landroid/os/RemoteException;
    :catch_14
    move-exception v1

    #@15
    .line 1092
    .local v1, e:Ljava/lang/RuntimeException;
    invoke-virtual {v1}, Ljava/lang/RuntimeException;->printStackTrace()V

    #@18
    goto :goto_7
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 1100
    return-void
.end method
