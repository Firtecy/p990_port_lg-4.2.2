.class Lcom/android/server/WifiService$4;
.super Landroid/content/BroadcastReceiver;
.source "WifiService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/WifiService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/WifiService;


# direct methods
.method constructor <init>(Lcom/android/server/WifiService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1491
    iput-object p1, p0, Lcom/android/server/WifiService$4;->this$0:Lcom/android/server/WifiService;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method

.method private shouldDeviceStayAwake(II)Z
    .registers 4
    .parameter "stayAwakeConditions"
    .parameter "pluggedType"

    #@0
    .prologue
    .line 1666
    and-int v0, p1, p2

    #@2
    if-eqz v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method private shouldWifiStayAwake(II)Z
    .registers 8
    .parameter "stayAwakeConditions"
    .parameter "pluggedType"

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    const/4 v1, 0x1

    #@2
    .line 1634
    iget-object v2, p0, Lcom/android/server/WifiService$4;->this$0:Lcom/android/server/WifiService;

    #@4
    invoke-static {v2}, Lcom/android/server/WifiService;->access$200(Lcom/android/server/WifiService;)Landroid/content/Context;

    #@7
    move-result-object v2

    #@8
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@b
    move-result-object v2

    #@c
    const-string v3, "wifi_sleep_policy"

    #@e
    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@11
    move-result v0

    #@12
    .line 1638
    .local v0, wifiSleepPolicy:I
    if-ne v0, v4, :cond_15

    #@14
    .line 1647
    :cond_14
    :goto_14
    return v1

    #@15
    .line 1641
    :cond_15
    if-ne v0, v1, :cond_19

    #@17
    if-nez p2, :cond_14

    #@19
    .line 1647
    :cond_19
    invoke-direct {p0, p1, p2}, Lcom/android/server/WifiService$4;->shouldDeviceStayAwake(II)Z

    #@1c
    move-result v1

    #@1d
    goto :goto_14
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 19
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 1494
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    .line 1496
    .local v1, action:Ljava/lang/String;
    move-object/from16 v0, p0

    #@6
    iget-object v11, v0, Lcom/android/server/WifiService$4;->this$0:Lcom/android/server/WifiService;

    #@8
    invoke-static {v11}, Lcom/android/server/WifiService;->access$200(Lcom/android/server/WifiService;)Landroid/content/Context;

    #@b
    move-result-object v11

    #@c
    invoke-virtual {v11}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@f
    move-result-object v11

    #@10
    const-string v12, "wifi_idle_ms"

    #@12
    const-wide/32 v13, 0xdbba0

    #@15
    invoke-static {v11, v12, v13, v14}, Landroid/provider/Settings$Global;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    #@18
    move-result-wide v4

    #@19
    .line 1499
    .local v4, idleMillis:J
    move-object/from16 v0, p0

    #@1b
    iget-object v11, v0, Lcom/android/server/WifiService$4;->this$0:Lcom/android/server/WifiService;

    #@1d
    invoke-static {v11}, Lcom/android/server/WifiService;->access$200(Lcom/android/server/WifiService;)Landroid/content/Context;

    #@20
    move-result-object v11

    #@21
    invoke-virtual {v11}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@24
    move-result-object v11

    #@25
    const-string v12, "stay_on_while_plugged_in"

    #@27
    const/4 v13, 0x0

    #@28
    invoke-static {v11, v12, v13}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@2b
    move-result v8

    #@2c
    .line 1502
    .local v8, stayAwakeConditions:I
    const-string v11, "android.intent.action.SCREEN_ON"

    #@2e
    invoke-virtual {v1, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@31
    move-result v11

    #@32
    if-eqz v11, :cond_66

    #@34
    .line 1504
    const-string v11, "WifiService"

    #@36
    const-string v12, "ACTION_SCREEN_ON"

    #@38
    invoke-static {v11, v12}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 1506
    move-object/from16 v0, p0

    #@3d
    iget-object v11, v0, Lcom/android/server/WifiService$4;->this$0:Lcom/android/server/WifiService;

    #@3f
    invoke-static {v11}, Lcom/android/server/WifiService;->access$2900(Lcom/android/server/WifiService;)Landroid/app/AlarmManager;

    #@42
    move-result-object v11

    #@43
    move-object/from16 v0, p0

    #@45
    iget-object v12, v0, Lcom/android/server/WifiService$4;->this$0:Lcom/android/server/WifiService;

    #@47
    invoke-static {v12}, Lcom/android/server/WifiService;->access$2800(Lcom/android/server/WifiService;)Landroid/app/PendingIntent;

    #@4a
    move-result-object v12

    #@4b
    invoke-virtual {v11, v12}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    #@4e
    .line 1507
    move-object/from16 v0, p0

    #@50
    iget-object v11, v0, Lcom/android/server/WifiService$4;->this$0:Lcom/android/server/WifiService;

    #@52
    const/4 v12, 0x0

    #@53
    invoke-static {v11, v12}, Lcom/android/server/WifiService;->access$3002(Lcom/android/server/WifiService;Z)Z

    #@56
    .line 1508
    move-object/from16 v0, p0

    #@58
    iget-object v11, v0, Lcom/android/server/WifiService$4;->this$0:Lcom/android/server/WifiService;

    #@5a
    invoke-static {v11}, Lcom/android/server/WifiService;->access$1800(Lcom/android/server/WifiService;)V

    #@5d
    .line 1509
    move-object/from16 v0, p0

    #@5f
    iget-object v11, v0, Lcom/android/server/WifiService$4;->this$0:Lcom/android/server/WifiService;

    #@61
    const/4 v12, 0x0

    #@62
    invoke-static {v11, v12}, Lcom/android/server/WifiService;->access$3100(Lcom/android/server/WifiService;Z)V

    #@65
    .line 1623
    :cond_65
    :goto_65
    return-void

    #@66
    .line 1510
    :cond_66
    const-string v11, "android.intent.action.SCREEN_OFF"

    #@68
    invoke-virtual {v1, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6b
    move-result v11

    #@6c
    if-eqz v11, :cond_149

    #@6e
    .line 1512
    const-string v11, "WifiService"

    #@70
    const-string v12, "ACTION_SCREEN_OFF"

    #@72
    invoke-static {v11, v12}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@75
    .line 1514
    move-object/from16 v0, p0

    #@77
    iget-object v11, v0, Lcom/android/server/WifiService$4;->this$0:Lcom/android/server/WifiService;

    #@79
    const/4 v12, 0x1

    #@7a
    invoke-static {v11, v12}, Lcom/android/server/WifiService;->access$3002(Lcom/android/server/WifiService;Z)Z

    #@7d
    .line 1515
    move-object/from16 v0, p0

    #@7f
    iget-object v11, v0, Lcom/android/server/WifiService$4;->this$0:Lcom/android/server/WifiService;

    #@81
    invoke-static {v11}, Lcom/android/server/WifiService;->access$1800(Lcom/android/server/WifiService;)V

    #@84
    .line 1522
    move-object/from16 v0, p0

    #@86
    iget-object v11, v0, Lcom/android/server/WifiService$4;->this$0:Lcom/android/server/WifiService;

    #@88
    invoke-static {v11}, Lcom/android/server/WifiService;->access$3200(Lcom/android/server/WifiService;)I

    #@8b
    move-result v11

    #@8c
    move-object/from16 v0, p0

    #@8e
    invoke-direct {v0, v8, v11}, Lcom/android/server/WifiService$4;->shouldWifiStayAwake(II)Z

    #@91
    move-result v11

    #@92
    if-nez v11, :cond_65

    #@94
    .line 1524
    move-object/from16 v0, p0

    #@96
    iget-object v11, v0, Lcom/android/server/WifiService$4;->this$0:Lcom/android/server/WifiService;

    #@98
    iget-object v11, v11, Lcom/android/server/WifiService;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@9a
    invoke-virtual {v11}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    #@9d
    move-result-object v11

    #@9e
    sget-object v12, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    #@a0
    if-ne v11, v12, :cond_fb

    #@a2
    .line 1529
    sget-boolean v11, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@a4
    if-eqz v11, :cond_c2

    #@a6
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->useWifiOffDelayAfter15alarm()Z

    #@a9
    move-result v11

    #@aa
    if-nez v11, :cond_c2

    #@ac
    move-object/from16 v0, p0

    #@ae
    iget-object v11, v0, Lcom/android/server/WifiService$4;->this$0:Lcom/android/server/WifiService;

    #@b0
    invoke-static {v11}, Lcom/android/server/WifiService;->access$3300(Lcom/android/server/WifiService;)Lcom/lge/wifi_iface/WifiOffDelayIfNotUsedIface;

    #@b3
    move-result-object v11

    #@b4
    if-eqz v11, :cond_c2

    #@b6
    .line 1532
    move-object/from16 v0, p0

    #@b8
    iget-object v11, v0, Lcom/android/server/WifiService$4;->this$0:Lcom/android/server/WifiService;

    #@ba
    invoke-static {v11}, Lcom/android/server/WifiService;->access$3300(Lcom/android/server/WifiService;)Lcom/lge/wifi_iface/WifiOffDelayIfNotUsedIface;

    #@bd
    move-result-object v11

    #@be
    invoke-interface {v11}, Lcom/lge/wifi_iface/WifiOffDelayIfNotUsedIface;->startMonitoring()V

    #@c1
    goto :goto_65

    #@c2
    .line 1534
    :cond_c2
    const-string v11, "WifiService"

    #@c4
    new-instance v12, Ljava/lang/StringBuilder;

    #@c6
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@c9
    const-string v13, "setting ACTION_DEVICE_IDLE: "

    #@cb
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v12

    #@cf
    invoke-virtual {v12, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@d2
    move-result-object v12

    #@d3
    const-string v13, " ms"

    #@d5
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d8
    move-result-object v12

    #@d9
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@dc
    move-result-object v12

    #@dd
    invoke-static {v11, v12}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e0
    .line 1535
    move-object/from16 v0, p0

    #@e2
    iget-object v11, v0, Lcom/android/server/WifiService$4;->this$0:Lcom/android/server/WifiService;

    #@e4
    invoke-static {v11}, Lcom/android/server/WifiService;->access$2900(Lcom/android/server/WifiService;)Landroid/app/AlarmManager;

    #@e7
    move-result-object v11

    #@e8
    const/4 v12, 0x0

    #@e9
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@ec
    move-result-wide v13

    #@ed
    add-long/2addr v13, v4

    #@ee
    move-object/from16 v0, p0

    #@f0
    iget-object v15, v0, Lcom/android/server/WifiService$4;->this$0:Lcom/android/server/WifiService;

    #@f2
    invoke-static {v15}, Lcom/android/server/WifiService;->access$2800(Lcom/android/server/WifiService;)Landroid/app/PendingIntent;

    #@f5
    move-result-object v15

    #@f6
    invoke-virtual {v11, v12, v13, v14, v15}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    #@f9
    goto/16 :goto_65

    #@fb
    .line 1540
    :cond_fb
    sget-boolean v11, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@fd
    if-eqz v11, :cond_13f

    #@ff
    sget-boolean v11, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_QCOM_PATCH:Z

    #@101
    if-nez v11, :cond_13f

    #@103
    .line 1541
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@106
    move-result-wide v11

    #@107
    const-wide/16 v13, 0x7d0

    #@109
    add-long v2, v11, v13

    #@10b
    .line 1542
    .local v2, delayTime:J
    move-object/from16 v0, p0

    #@10d
    iget-object v11, v0, Lcom/android/server/WifiService$4;->this$0:Lcom/android/server/WifiService;

    #@10f
    invoke-static {v11}, Lcom/android/server/WifiService;->access$2900(Lcom/android/server/WifiService;)Landroid/app/AlarmManager;

    #@112
    move-result-object v11

    #@113
    const/4 v12, 0x0

    #@114
    move-object/from16 v0, p0

    #@116
    iget-object v13, v0, Lcom/android/server/WifiService$4;->this$0:Lcom/android/server/WifiService;

    #@118
    invoke-static {v13}, Lcom/android/server/WifiService;->access$2800(Lcom/android/server/WifiService;)Landroid/app/PendingIntent;

    #@11b
    move-result-object v13

    #@11c
    invoke-virtual {v11, v12, v2, v3, v13}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    #@11f
    .line 1543
    const-string v11, "WifiService"

    #@121
    new-instance v12, Ljava/lang/StringBuilder;

    #@123
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@126
    const-string v13, "setting ACTION_DEVICE_IDLE: "

    #@128
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12b
    move-result-object v12

    #@12c
    invoke-virtual {v12, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@12f
    move-result-object v12

    #@130
    const-string v13, "ms"

    #@132
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@135
    move-result-object v12

    #@136
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@139
    move-result-object v12

    #@13a
    invoke-static {v11, v12}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@13d
    goto/16 :goto_65

    #@13f
    .line 1546
    .end local v2           #delayTime:J
    :cond_13f
    move-object/from16 v0, p0

    #@141
    iget-object v11, v0, Lcom/android/server/WifiService$4;->this$0:Lcom/android/server/WifiService;

    #@143
    const/4 v12, 0x1

    #@144
    invoke-static {v11, v12}, Lcom/android/server/WifiService;->access$3100(Lcom/android/server/WifiService;Z)V

    #@147
    goto/16 :goto_65

    #@149
    .line 1550
    :cond_149
    const-string v11, "com.android.server.WifiManager.action.DEVICE_IDLE"

    #@14b
    invoke-virtual {v1, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14e
    move-result v11

    #@14f
    if-eqz v11, :cond_1eb

    #@151
    .line 1555
    sget-boolean v11, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@153
    if-eqz v11, :cond_172

    #@155
    move-object/from16 v0, p0

    #@157
    iget-object v11, v0, Lcom/android/server/WifiService$4;->this$0:Lcom/android/server/WifiService;

    #@159
    invoke-static {v11}, Lcom/android/server/WifiService;->access$3300(Lcom/android/server/WifiService;)Lcom/lge/wifi_iface/WifiOffDelayIfNotUsedIface;

    #@15c
    move-result-object v11

    #@15d
    if-eqz v11, :cond_172

    #@15f
    .line 1556
    move-object/from16 v0, p0

    #@161
    iget-object v11, v0, Lcom/android/server/WifiService$4;->this$0:Lcom/android/server/WifiService;

    #@163
    invoke-static {v11}, Lcom/android/server/WifiService;->access$3000(Lcom/android/server/WifiService;)Z

    #@166
    move-result v11

    #@167
    if-nez v11, :cond_172

    #@169
    .line 1557
    const-string v11, "WifiService"

    #@16b
    const-string v12, "Screen is already on"

    #@16d
    invoke-static {v11, v12}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@170
    goto/16 :goto_65

    #@172
    .line 1565
    :cond_172
    sget-boolean v11, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@174
    if-eqz v11, :cond_1e1

    #@176
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->useWifiOffDelayAfter15alarm()Z

    #@179
    move-result v11

    #@17a
    if-eqz v11, :cond_1e1

    #@17c
    move-object/from16 v0, p0

    #@17e
    iget-object v11, v0, Lcom/android/server/WifiService$4;->this$0:Lcom/android/server/WifiService;

    #@180
    invoke-static {v11}, Lcom/android/server/WifiService;->access$3300(Lcom/android/server/WifiService;)Lcom/lge/wifi_iface/WifiOffDelayIfNotUsedIface;

    #@183
    move-result-object v11

    #@184
    if-eqz v11, :cond_1e1

    #@186
    .line 1568
    move-object/from16 v0, p0

    #@188
    iget-object v11, v0, Lcom/android/server/WifiService$4;->this$0:Lcom/android/server/WifiService;

    #@18a
    invoke-static {v11}, Lcom/android/server/WifiService;->access$3300(Lcom/android/server/WifiService;)Lcom/lge/wifi_iface/WifiOffDelayIfNotUsedIface;

    #@18d
    move-result-object v11

    #@18e
    invoke-interface {v11}, Lcom/lge/wifi_iface/WifiOffDelayIfNotUsedIface;->getShouldWifiStopped()Z

    #@191
    move-result v11

    #@192
    if-eqz v11, :cond_1b1

    #@194
    .line 1569
    const-string v11, "WifiService"

    #@196
    const-string v12, "mShouldWifiStopped is true and setShouldWifiStopped is called"

    #@198
    invoke-static {v11, v12}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@19b
    .line 1570
    move-object/from16 v0, p0

    #@19d
    iget-object v11, v0, Lcom/android/server/WifiService$4;->this$0:Lcom/android/server/WifiService;

    #@19f
    invoke-static {v11}, Lcom/android/server/WifiService;->access$3300(Lcom/android/server/WifiService;)Lcom/lge/wifi_iface/WifiOffDelayIfNotUsedIface;

    #@1a2
    move-result-object v11

    #@1a3
    const/4 v12, 0x0

    #@1a4
    invoke-interface {v11, v12}, Lcom/lge/wifi_iface/WifiOffDelayIfNotUsedIface;->setShouldWifiStopped(Z)V

    #@1a7
    .line 1571
    move-object/from16 v0, p0

    #@1a9
    iget-object v11, v0, Lcom/android/server/WifiService$4;->this$0:Lcom/android/server/WifiService;

    #@1ab
    const/4 v12, 0x1

    #@1ac
    invoke-static {v11, v12}, Lcom/android/server/WifiService;->access$3100(Lcom/android/server/WifiService;Z)V

    #@1af
    goto/16 :goto_65

    #@1b1
    .line 1572
    :cond_1b1
    sget-boolean v11, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_QCOM_PATCH:Z

    #@1b3
    if-nez v11, :cond_1d4

    #@1b5
    move-object/from16 v0, p0

    #@1b7
    iget-object v11, v0, Lcom/android/server/WifiService$4;->this$0:Lcom/android/server/WifiService;

    #@1b9
    iget-object v11, v11, Lcom/android/server/WifiService;->mNetworkInfo:Landroid/net/NetworkInfo;

    #@1bb
    invoke-virtual {v11}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    #@1be
    move-result-object v11

    #@1bf
    sget-object v12, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    #@1c1
    if-eq v11, v12, :cond_1d4

    #@1c3
    .line 1574
    const-string v11, "WifiService"

    #@1c5
    const-string v12, "mShouldWifiStopped is false, but Wifi driver should be stopped.(Wifi is not connection state) "

    #@1c7
    invoke-static {v11, v12}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1ca
    .line 1575
    move-object/from16 v0, p0

    #@1cc
    iget-object v11, v0, Lcom/android/server/WifiService$4;->this$0:Lcom/android/server/WifiService;

    #@1ce
    const/4 v12, 0x1

    #@1cf
    invoke-static {v11, v12}, Lcom/android/server/WifiService;->access$3100(Lcom/android/server/WifiService;Z)V

    #@1d2
    goto/16 :goto_65

    #@1d4
    .line 1577
    :cond_1d4
    move-object/from16 v0, p0

    #@1d6
    iget-object v11, v0, Lcom/android/server/WifiService$4;->this$0:Lcom/android/server/WifiService;

    #@1d8
    invoke-static {v11}, Lcom/android/server/WifiService;->access$3300(Lcom/android/server/WifiService;)Lcom/lge/wifi_iface/WifiOffDelayIfNotUsedIface;

    #@1db
    move-result-object v11

    #@1dc
    invoke-interface {v11}, Lcom/lge/wifi_iface/WifiOffDelayIfNotUsedIface;->startMonitoring()V

    #@1df
    goto/16 :goto_65

    #@1e1
    .line 1580
    :cond_1e1
    move-object/from16 v0, p0

    #@1e3
    iget-object v11, v0, Lcom/android/server/WifiService$4;->this$0:Lcom/android/server/WifiService;

    #@1e5
    const/4 v12, 0x1

    #@1e6
    invoke-static {v11, v12}, Lcom/android/server/WifiService;->access$3100(Lcom/android/server/WifiService;Z)V

    #@1e9
    goto/16 :goto_65

    #@1eb
    .line 1582
    :cond_1eb
    const-string v11, "android.intent.action.BATTERY_CHANGED"

    #@1ed
    invoke-virtual {v1, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1f0
    move-result v11

    #@1f1
    if-eqz v11, :cond_29e

    #@1f3
    .line 1590
    const-string v11, "plugged"

    #@1f5
    const/4 v12, 0x0

    #@1f6
    move-object/from16 v0, p2

    #@1f8
    invoke-virtual {v0, v11, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@1fb
    move-result v6

    #@1fc
    .line 1592
    .local v6, pluggedType:I
    const-string v11, "WifiService"

    #@1fe
    new-instance v12, Ljava/lang/StringBuilder;

    #@200
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@203
    const-string v13, "ACTION_BATTERY_CHANGED pluggedType: "

    #@205
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@208
    move-result-object v12

    #@209
    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20c
    move-result-object v12

    #@20d
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@210
    move-result-object v12

    #@211
    invoke-static {v11, v12}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@214
    .line 1594
    move-object/from16 v0, p0

    #@216
    iget-object v11, v0, Lcom/android/server/WifiService$4;->this$0:Lcom/android/server/WifiService;

    #@218
    invoke-static {v11}, Lcom/android/server/WifiService;->access$3000(Lcom/android/server/WifiService;)Z

    #@21b
    move-result v11

    #@21c
    if-eqz v11, :cond_25c

    #@21e
    move-object/from16 v0, p0

    #@220
    iget-object v11, v0, Lcom/android/server/WifiService$4;->this$0:Lcom/android/server/WifiService;

    #@222
    invoke-static {v11}, Lcom/android/server/WifiService;->access$3200(Lcom/android/server/WifiService;)I

    #@225
    move-result v11

    #@226
    move-object/from16 v0, p0

    #@228
    invoke-direct {v0, v8, v11}, Lcom/android/server/WifiService$4;->shouldWifiStayAwake(II)Z

    #@22b
    move-result v11

    #@22c
    if-eqz v11, :cond_25c

    #@22e
    move-object/from16 v0, p0

    #@230
    invoke-direct {v0, v8, v6}, Lcom/android/server/WifiService$4;->shouldWifiStayAwake(II)Z

    #@233
    move-result v11

    #@234
    if-nez v11, :cond_25c

    #@236
    .line 1600
    sget-boolean v11, Lcom/lge/wifi/WifiLgeConfig;->CONFIG_LGE_WLAN_PATCH:Z

    #@238
    if-eqz v11, :cond_265

    #@23a
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->useWifiOffDelayAfter15alarm()Z

    #@23d
    move-result v11

    #@23e
    if-nez v11, :cond_265

    #@240
    move-object/from16 v0, p0

    #@242
    iget-object v11, v0, Lcom/android/server/WifiService$4;->this$0:Lcom/android/server/WifiService;

    #@244
    invoke-static {v11}, Lcom/android/server/WifiService;->access$3300(Lcom/android/server/WifiService;)Lcom/lge/wifi_iface/WifiOffDelayIfNotUsedIface;

    #@247
    move-result-object v11

    #@248
    if-eqz v11, :cond_265

    #@24a
    .line 1603
    const-string v11, "WifiService"

    #@24c
    const-string v12, "ACTION_BATTERY_CHANGED : USB is unpluged"

    #@24e
    invoke-static {v11, v12}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@251
    .line 1604
    move-object/from16 v0, p0

    #@253
    iget-object v11, v0, Lcom/android/server/WifiService$4;->this$0:Lcom/android/server/WifiService;

    #@255
    invoke-static {v11}, Lcom/android/server/WifiService;->access$3300(Lcom/android/server/WifiService;)Lcom/lge/wifi_iface/WifiOffDelayIfNotUsedIface;

    #@258
    move-result-object v11

    #@259
    invoke-interface {v11}, Lcom/lge/wifi_iface/WifiOffDelayIfNotUsedIface;->startMonitoring()V

    #@25c
    .line 1614
    :cond_25c
    :goto_25c
    move-object/from16 v0, p0

    #@25e
    iget-object v11, v0, Lcom/android/server/WifiService$4;->this$0:Lcom/android/server/WifiService;

    #@260
    invoke-static {v11, v6}, Lcom/android/server/WifiService;->access$3202(Lcom/android/server/WifiService;I)I

    #@263
    goto/16 :goto_65

    #@265
    .line 1606
    :cond_265
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@268
    move-result-wide v11

    #@269
    add-long v9, v11, v4

    #@26b
    .line 1608
    .local v9, triggerTime:J
    const-string v11, "WifiService"

    #@26d
    new-instance v12, Ljava/lang/StringBuilder;

    #@26f
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@272
    const-string v13, "setting ACTION_DEVICE_IDLE timer for "

    #@274
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@277
    move-result-object v12

    #@278
    invoke-virtual {v12, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@27b
    move-result-object v12

    #@27c
    const-string v13, "ms"

    #@27e
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@281
    move-result-object v12

    #@282
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@285
    move-result-object v12

    #@286
    invoke-static {v11, v12}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@289
    .line 1610
    move-object/from16 v0, p0

    #@28b
    iget-object v11, v0, Lcom/android/server/WifiService$4;->this$0:Lcom/android/server/WifiService;

    #@28d
    invoke-static {v11}, Lcom/android/server/WifiService;->access$2900(Lcom/android/server/WifiService;)Landroid/app/AlarmManager;

    #@290
    move-result-object v11

    #@291
    const/4 v12, 0x0

    #@292
    move-object/from16 v0, p0

    #@294
    iget-object v13, v0, Lcom/android/server/WifiService$4;->this$0:Lcom/android/server/WifiService;

    #@296
    invoke-static {v13}, Lcom/android/server/WifiService;->access$2800(Lcom/android/server/WifiService;)Landroid/app/PendingIntent;

    #@299
    move-result-object v13

    #@29a
    invoke-virtual {v11, v12, v9, v10, v13}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    #@29d
    goto :goto_25c

    #@29e
    .line 1615
    .end local v6           #pluggedType:I
    .end local v9           #triggerTime:J
    :cond_29e
    const-string v11, "android.bluetooth.adapter.action.CONNECTION_STATE_CHANGED"

    #@2a0
    invoke-virtual {v1, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2a3
    move-result v11

    #@2a4
    if-eqz v11, :cond_2bc

    #@2a6
    .line 1616
    const-string v11, "android.bluetooth.adapter.extra.CONNECTION_STATE"

    #@2a8
    const/4 v12, 0x0

    #@2a9
    move-object/from16 v0, p2

    #@2ab
    invoke-virtual {v0, v11, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@2ae
    move-result v7

    #@2af
    .line 1618
    .local v7, state:I
    move-object/from16 v0, p0

    #@2b1
    iget-object v11, v0, Lcom/android/server/WifiService$4;->this$0:Lcom/android/server/WifiService;

    #@2b3
    invoke-static {v11}, Lcom/android/server/WifiService;->access$600(Lcom/android/server/WifiService;)Landroid/net/wifi/WifiStateMachine;

    #@2b6
    move-result-object v11

    #@2b7
    invoke-virtual {v11, v7}, Landroid/net/wifi/WifiStateMachine;->sendBluetoothAdapterStateChange(I)V

    #@2ba
    goto/16 :goto_65

    #@2bc
    .line 1619
    .end local v7           #state:I
    :cond_2bc
    const-string v11, "android.intent.action.EMERGENCY_CALLBACK_MODE_CHANGED"

    #@2be
    invoke-virtual {v1, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2c1
    move-result v11

    #@2c2
    if-eqz v11, :cond_65

    #@2c4
    .line 1620
    move-object/from16 v0, p0

    #@2c6
    iget-object v11, v0, Lcom/android/server/WifiService$4;->this$0:Lcom/android/server/WifiService;

    #@2c8
    const-string v12, "phoneinECMState"

    #@2ca
    const/4 v13, 0x0

    #@2cb
    move-object/from16 v0, p2

    #@2cd
    invoke-virtual {v0, v12, v13}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@2d0
    move-result v12

    #@2d1
    invoke-static {v11, v12}, Lcom/android/server/WifiService;->access$3402(Lcom/android/server/WifiService;Z)Z

    #@2d4
    .line 1621
    move-object/from16 v0, p0

    #@2d6
    iget-object v11, v0, Lcom/android/server/WifiService$4;->this$0:Lcom/android/server/WifiService;

    #@2d8
    invoke-static {v11}, Lcom/android/server/WifiService;->access$1100(Lcom/android/server/WifiService;)V

    #@2db
    goto/16 :goto_65
.end method
