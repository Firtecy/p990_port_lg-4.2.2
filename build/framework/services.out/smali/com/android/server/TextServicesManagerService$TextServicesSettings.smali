.class Lcom/android/server/TextServicesManagerService$TextServicesSettings;
.super Ljava/lang/Object;
.source "TextServicesManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/TextServicesManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TextServicesSettings"
.end annotation


# instance fields
.field private mCurrentUserId:I

.field private final mResolver:Landroid/content/ContentResolver;


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;I)V
    .registers 3
    .parameter "resolver"
    .parameter "userId"

    #@0
    .prologue
    .line 915
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 916
    iput-object p1, p0, Lcom/android/server/TextServicesManagerService$TextServicesSettings;->mResolver:Landroid/content/ContentResolver;

    #@5
    .line 917
    iput p2, p0, Lcom/android/server/TextServicesManagerService$TextServicesSettings;->mCurrentUserId:I

    #@7
    .line 918
    return-void
.end method


# virtual methods
.method public getCurrentUserId()I
    .registers 2

    #@0
    .prologue
    .line 930
    iget v0, p0, Lcom/android/server/TextServicesManagerService$TextServicesSettings;->mCurrentUserId:I

    #@2
    return v0
.end method

.method public getSelectedSpellChecker()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 950
    iget-object v0, p0, Lcom/android/server/TextServicesManagerService$TextServicesSettings;->mResolver:Landroid/content/ContentResolver;

    #@2
    const-string v1, "selected_spell_checker"

    #@4
    iget v2, p0, Lcom/android/server/TextServicesManagerService$TextServicesSettings;->mCurrentUserId:I

    #@6
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    return-object v0
.end method

.method public getSelectedSpellCheckerSubtype()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 955
    iget-object v0, p0, Lcom/android/server/TextServicesManagerService$TextServicesSettings;->mResolver:Landroid/content/ContentResolver;

    #@2
    const-string v1, "selected_spell_checker_subtype"

    #@4
    iget v2, p0, Lcom/android/server/TextServicesManagerService$TextServicesSettings;->mCurrentUserId:I

    #@6
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    return-object v0
.end method

.method public isSpellCheckerEnabled()Z
    .registers 5

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 960
    iget-object v1, p0, Lcom/android/server/TextServicesManagerService$TextServicesSettings;->mResolver:Landroid/content/ContentResolver;

    #@3
    const-string v2, "spell_checker_enabled"

    #@5
    iget v3, p0, Lcom/android/server/TextServicesManagerService$TextServicesSettings;->mCurrentUserId:I

    #@7
    invoke-static {v1, v2, v0, v3}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    #@a
    move-result v1

    #@b
    if-ne v1, v0, :cond_e

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public putSelectedSpellChecker(Ljava/lang/String;)V
    .registers 5
    .parameter "sciId"

    #@0
    .prologue
    .line 934
    iget-object v0, p0, Lcom/android/server/TextServicesManagerService$TextServicesSettings;->mResolver:Landroid/content/ContentResolver;

    #@2
    const-string v1, "selected_spell_checker"

    #@4
    iget v2, p0, Lcom/android/server/TextServicesManagerService$TextServicesSettings;->mCurrentUserId:I

    #@6
    invoke-static {v0, v1, p1, v2}, Landroid/provider/Settings$Secure;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    #@9
    .line 936
    return-void
.end method

.method public putSelectedSpellCheckerSubtype(I)V
    .registers 6
    .parameter "hashCode"

    #@0
    .prologue
    .line 939
    iget-object v0, p0, Lcom/android/server/TextServicesManagerService$TextServicesSettings;->mResolver:Landroid/content/ContentResolver;

    #@2
    const-string v1, "selected_spell_checker_subtype"

    #@4
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@7
    move-result-object v2

    #@8
    iget v3, p0, Lcom/android/server/TextServicesManagerService$TextServicesSettings;->mCurrentUserId:I

    #@a
    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$Secure;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    #@d
    .line 942
    return-void
.end method

.method public setCurrentUserId(I)V
    .registers 2
    .parameter "userId"

    #@0
    .prologue
    .line 926
    iput p1, p0, Lcom/android/server/TextServicesManagerService$TextServicesSettings;->mCurrentUserId:I

    #@2
    .line 927
    return-void
.end method

.method public setSpellCheckerEnabled(Z)V
    .registers 6
    .parameter "enabled"

    #@0
    .prologue
    .line 945
    iget-object v1, p0, Lcom/android/server/TextServicesManagerService$TextServicesSettings;->mResolver:Landroid/content/ContentResolver;

    #@2
    const-string v2, "spell_checker_enabled"

    #@4
    if-eqz p1, :cond_d

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    iget v3, p0, Lcom/android/server/TextServicesManagerService$TextServicesSettings;->mCurrentUserId:I

    #@9
    invoke-static {v1, v2, v0, v3}, Landroid/provider/Settings$Secure;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    #@c
    .line 947
    return-void

    #@d
    .line 945
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_7
.end method
