.class Lcom/android/server/BackupManagerService$PerformFullBackupTask;
.super Ljava/lang/Object;
.source "BackupManagerService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/BackupManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PerformFullBackupTask"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/BackupManagerService$PerformFullBackupTask$FullBackupRunner;
    }
.end annotation


# instance fields
.field mAllApps:Z

.field mCurrentPassword:Ljava/lang/String;

.field mDeflater:Ljava/util/zip/DeflaterOutputStream;

.field mEncryptPassword:Ljava/lang/String;

.field mFilesDir:Ljava/io/File;

.field mIncludeApks:Z

.field mIncludeShared:Z

.field final mIncludeSystem:Z

.field mLatchObject:Ljava/util/concurrent/atomic/AtomicBoolean;

.field mManifestFile:Ljava/io/File;

.field mObserver:Landroid/app/backup/IFullBackupRestoreObserver;

.field mOutputFile:Landroid/os/ParcelFileDescriptor;

.field mPackages:[Ljava/lang/String;

.field final synthetic this$0:Lcom/android/server/BackupManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/BackupManagerService;Landroid/os/ParcelFileDescriptor;Landroid/app/backup/IFullBackupRestoreObserver;ZZLjava/lang/String;Ljava/lang/String;ZZ[Ljava/lang/String;Ljava/util/concurrent/atomic/AtomicBoolean;)V
    .registers 15
    .parameter
    .parameter "fd"
    .parameter "observer"
    .parameter "includeApks"
    .parameter "includeShared"
    .parameter "curPassword"
    .parameter "encryptPassword"
    .parameter "doAllApps"
    .parameter "doSystem"
    .parameter "packages"
    .parameter "latch"

    #@0
    .prologue
    .line 2382
    iput-object p1, p0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 2383
    iput-object p2, p0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mOutputFile:Landroid/os/ParcelFileDescriptor;

    #@7
    .line 2384
    iput-object p3, p0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mObserver:Landroid/app/backup/IFullBackupRestoreObserver;

    #@9
    .line 2385
    iput-boolean p4, p0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mIncludeApks:Z

    #@b
    .line 2386
    iput-boolean p5, p0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mIncludeShared:Z

    #@d
    .line 2387
    iput-boolean p8, p0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mAllApps:Z

    #@f
    .line 2388
    iput-boolean p9, p0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mIncludeSystem:Z

    #@11
    .line 2389
    iput-object p10, p0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mPackages:[Ljava/lang/String;

    #@13
    .line 2390
    iput-object p6, p0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mCurrentPassword:Ljava/lang/String;

    #@15
    .line 2395
    if-eqz p7, :cond_1f

    #@17
    const-string v0, ""

    #@19
    invoke-virtual {v0, p7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1c
    move-result v0

    #@1d
    if-eqz v0, :cond_38

    #@1f
    .line 2396
    :cond_1f
    iput-object p6, p0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mEncryptPassword:Ljava/lang/String;

    #@21
    .line 2400
    :goto_21
    iput-object p11, p0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mLatchObject:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@23
    .line 2402
    new-instance v0, Ljava/io/File;

    #@25
    const-string v1, "/data/system"

    #@27
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@2a
    iput-object v0, p0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mFilesDir:Ljava/io/File;

    #@2c
    .line 2403
    new-instance v0, Ljava/io/File;

    #@2e
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mFilesDir:Ljava/io/File;

    #@30
    const-string v2, "_manifest"

    #@32
    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@35
    iput-object v0, p0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mManifestFile:Ljava/io/File;

    #@37
    .line 2404
    return-void

    #@38
    .line 2398
    :cond_38
    iput-object p7, p0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mEncryptPassword:Ljava/lang/String;

    #@3a
    goto :goto_21
.end method

.method static synthetic access$1000(Lcom/android/server/BackupManagerService$PerformFullBackupTask;Landroid/content/pm/PackageInfo;Landroid/app/backup/BackupDataOutput;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 2311
    invoke-direct {p0, p1, p2}, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->writeApkToBackup(Landroid/content/pm/PackageInfo;Landroid/app/backup/BackupDataOutput;)V

    #@3
    return-void
.end method

.method static synthetic access$900(Lcom/android/server/BackupManagerService$PerformFullBackupTask;Landroid/content/pm/PackageInfo;Ljava/io/File;Z)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 2311
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->writeAppManifest(Landroid/content/pm/PackageInfo;Ljava/io/File;Z)V

    #@3
    return-void
.end method

.method private backupOnePackage(Landroid/content/pm/PackageInfo;Ljava/io/OutputStream;)V
    .registers 23
    .parameter "pkg"
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 2662
    const-string v2, "BackupManagerService"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v5, "Binding to full backup agent : "

    #@9
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    move-object/from16 v0, p1

    #@f
    iget-object v5, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@11
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v3

    #@15
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v3

    #@19
    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 2664
    move-object/from16 v0, p0

    #@1e
    iget-object v2, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@20
    move-object/from16 v0, p1

    #@22
    iget-object v3, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@24
    const/4 v5, 0x1

    #@25
    invoke-virtual {v2, v3, v5}, Lcom/android/server/BackupManagerService;->bindToAgentSynchronous(Landroid/content/pm/ApplicationInfo;I)Landroid/app/IBackupAgent;

    #@28
    move-result-object v4

    #@29
    .line 2666
    .local v4, agent:Landroid/app/IBackupAgent;
    if-eqz v4, :cond_17d

    #@2b
    .line 2667
    const/16 v16, 0x0

    #@2d
    .line 2669
    .local v16, pipes:[Landroid/os/ParcelFileDescriptor;
    :try_start_2d
    invoke-static {}, Landroid/os/ParcelFileDescriptor;->createPipe()[Landroid/os/ParcelFileDescriptor;

    #@30
    move-result-object v16

    #@31
    .line 2671
    move-object/from16 v0, p1

    #@33
    iget-object v9, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@35
    .line 2672
    .local v9, app:Landroid/content/pm/ApplicationInfo;
    move-object/from16 v0, p1

    #@37
    iget-object v2, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@39
    const-string v3, "com.android.sharedstoragebackup"

    #@3b
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3e
    move-result v14

    #@3f
    .line 2673
    .local v14, isSharedStorage:Z
    move-object/from16 v0, p0

    #@41
    iget-boolean v2, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mIncludeApks:Z

    #@43
    if-eqz v2, :cond_c5

    #@45
    if-nez v14, :cond_c5

    #@47
    iget v2, v9, Landroid/content/pm/ApplicationInfo;->flags:I

    #@49
    const/high16 v3, 0x2000

    #@4b
    and-int/2addr v2, v3

    #@4c
    if-nez v2, :cond_c5

    #@4e
    iget v2, v9, Landroid/content/pm/ApplicationInfo;->flags:I

    #@50
    and-int/lit8 v2, v2, 0x1

    #@52
    if-eqz v2, :cond_5a

    #@54
    iget v2, v9, Landroid/content/pm/ApplicationInfo;->flags:I

    #@56
    and-int/lit16 v2, v2, 0x80

    #@58
    if-eqz v2, :cond_c5

    #@5a
    :cond_5a
    const/4 v7, 0x1

    #@5b
    .line 2679
    .local v7, sendApk:Z
    :goto_5b
    if-eqz v14, :cond_c7

    #@5d
    const-string v2, "Shared storage"

    #@5f
    :goto_5f
    move-object/from16 v0, p0

    #@61
    invoke-virtual {v0, v2}, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->sendOnBackupPackage(Ljava/lang/String;)V

    #@64
    .line 2681
    move-object/from16 v0, p0

    #@66
    iget-object v2, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@68
    invoke-virtual {v2}, Lcom/android/server/BackupManagerService;->generateToken()I

    #@6b
    move-result v6

    #@6c
    .line 2682
    .local v6, token:I
    new-instance v1, Lcom/android/server/BackupManagerService$PerformFullBackupTask$FullBackupRunner;

    #@6e
    const/4 v2, 0x1

    #@6f
    aget-object v5, v16, v2

    #@71
    if-nez v14, :cond_cc

    #@73
    const/4 v8, 0x1

    #@74
    :goto_74
    move-object/from16 v2, p0

    #@76
    move-object/from16 v3, p1

    #@78
    invoke-direct/range {v1 .. v8}, Lcom/android/server/BackupManagerService$PerformFullBackupTask$FullBackupRunner;-><init>(Lcom/android/server/BackupManagerService$PerformFullBackupTask;Landroid/content/pm/PackageInfo;Landroid/app/IBackupAgent;Landroid/os/ParcelFileDescriptor;IZZ)V

    #@7b
    .line 2684
    .local v1, runner:Lcom/android/server/BackupManagerService$PerformFullBackupTask$FullBackupRunner;
    const/4 v2, 0x1

    #@7c
    aget-object v2, v16, v2

    #@7e
    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->close()V

    #@81
    .line 2685
    const/4 v2, 0x1

    #@82
    const/4 v3, 0x0

    #@83
    aput-object v3, v16, v2

    #@85
    .line 2686
    new-instance v18, Ljava/lang/Thread;

    #@87
    move-object/from16 v0, v18

    #@89
    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    #@8c
    .line 2687
    .local v18, t:Ljava/lang/Thread;
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Thread;->start()V
    :try_end_8f
    .catchall {:try_start_2d .. :try_end_8f} :catchall_160
    .catch Ljava/io/IOException; {:try_start_2d .. :try_end_8f} :catch_11e

    #@8f
    .line 2691
    :try_start_8f
    new-instance v17, Ljava/io/FileInputStream;

    #@91
    const/4 v2, 0x0

    #@92
    aget-object v2, v16, v2

    #@94
    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@97
    move-result-object v2

    #@98
    move-object/from16 v0, v17

    #@9a
    invoke-direct {v0, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V

    #@9d
    .line 2692
    .local v17, raw:Ljava/io/FileInputStream;
    new-instance v13, Ljava/io/DataInputStream;

    #@9f
    move-object/from16 v0, v17

    #@a1
    invoke-direct {v13, v0}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    #@a4
    .line 2694
    .local v13, in:Ljava/io/DataInputStream;
    const/16 v2, 0x4000

    #@a6
    new-array v10, v2, [B

    #@a8
    .line 2696
    .local v10, buffer:[B
    :cond_a8
    invoke-virtual {v13}, Ljava/io/DataInputStream;->readInt()I

    #@ab
    move-result v11

    #@ac
    .local v11, chunkTotal:I
    if-lez v11, :cond_d9

    #@ae
    .line 2697
    :goto_ae
    if-lez v11, :cond_a8

    #@b0
    .line 2698
    array-length v2, v10

    #@b1
    if-le v11, v2, :cond_ce

    #@b3
    array-length v0, v10

    #@b4
    move/from16 v19, v0

    #@b6
    .line 2700
    .local v19, toRead:I
    :goto_b6
    const/4 v2, 0x0

    #@b7
    move/from16 v0, v19

    #@b9
    invoke-virtual {v13, v10, v2, v0}, Ljava/io/DataInputStream;->read([BII)I

    #@bc
    move-result v15

    #@bd
    .line 2701
    .local v15, nRead:I
    const/4 v2, 0x0

    #@be
    move-object/from16 v0, p2

    #@c0
    invoke-virtual {v0, v10, v2, v15}, Ljava/io/OutputStream;->write([BII)V
    :try_end_c3
    .catchall {:try_start_8f .. :try_end_c3} :catchall_160
    .catch Ljava/io/IOException; {:try_start_8f .. :try_end_c3} :catch_d1

    #@c3
    .line 2702
    sub-int/2addr v11, v15

    #@c4
    .line 2703
    goto :goto_ae

    #@c5
    .line 2673
    .end local v1           #runner:Lcom/android/server/BackupManagerService$PerformFullBackupTask$FullBackupRunner;
    .end local v6           #token:I
    .end local v7           #sendApk:Z
    .end local v10           #buffer:[B
    .end local v11           #chunkTotal:I
    .end local v13           #in:Ljava/io/DataInputStream;
    .end local v15           #nRead:I
    .end local v17           #raw:Ljava/io/FileInputStream;
    .end local v18           #t:Ljava/lang/Thread;
    .end local v19           #toRead:I
    :cond_c5
    const/4 v7, 0x0

    #@c6
    goto :goto_5b

    #@c7
    .line 2679
    .restart local v7       #sendApk:Z
    :cond_c7
    :try_start_c7
    move-object/from16 v0, p1

    #@c9
    iget-object v2, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@cb
    goto :goto_5f

    #@cc
    .line 2682
    .restart local v6       #token:I
    :cond_cc
    const/4 v8, 0x0

    #@cd
    goto :goto_74

    #@ce
    .restart local v1       #runner:Lcom/android/server/BackupManagerService$PerformFullBackupTask$FullBackupRunner;
    .restart local v10       #buffer:[B
    .restart local v11       #chunkTotal:I
    .restart local v13       #in:Ljava/io/DataInputStream;
    .restart local v17       #raw:Ljava/io/FileInputStream;
    .restart local v18       #t:Ljava/lang/Thread;
    :cond_ce
    move/from16 v19, v11

    #@d0
    .line 2698
    goto :goto_b6

    #@d1
    .line 2705
    .end local v10           #buffer:[B
    .end local v11           #chunkTotal:I
    .end local v13           #in:Ljava/io/DataInputStream;
    .end local v17           #raw:Ljava/io/FileInputStream;
    :catch_d1
    move-exception v12

    #@d2
    .line 2706
    .local v12, e:Ljava/io/IOException;
    const-string v2, "BackupManagerService"

    #@d4
    const-string v3, "Caught exception reading from agent"

    #@d6
    invoke-static {v2, v3, v12}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@d9
    .line 2709
    .end local v12           #e:Ljava/io/IOException;
    :cond_d9
    move-object/from16 v0, p0

    #@db
    iget-object v2, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@dd
    invoke-virtual {v2, v6}, Lcom/android/server/BackupManagerService;->waitUntilOperationComplete(I)Z

    #@e0
    move-result v2

    #@e1
    if-nez v2, :cond_ff

    #@e3
    .line 2710
    const-string v2, "BackupManagerService"

    #@e5
    new-instance v3, Ljava/lang/StringBuilder;

    #@e7
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@ea
    const-string v5, "Full backup failed on package "

    #@ec
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ef
    move-result-object v3

    #@f0
    move-object/from16 v0, p1

    #@f2
    iget-object v5, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@f4
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f7
    move-result-object v3

    #@f8
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fb
    move-result-object v3

    #@fc
    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_ff
    .catchall {:try_start_c7 .. :try_end_ff} :catchall_160
    .catch Ljava/io/IOException; {:try_start_c7 .. :try_end_ff} :catch_11e

    #@ff
    .line 2720
    :cond_ff
    :try_start_ff
    invoke-virtual/range {p2 .. p2}, Ljava/io/OutputStream;->flush()V

    #@102
    .line 2721
    if-eqz v16, :cond_11a

    #@104
    .line 2722
    const/4 v2, 0x0

    #@105
    aget-object v2, v16, v2

    #@107
    if-eqz v2, :cond_10f

    #@109
    const/4 v2, 0x0

    #@10a
    aget-object v2, v16, v2

    #@10c
    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->close()V

    #@10f
    .line 2723
    :cond_10f
    const/4 v2, 0x1

    #@110
    aget-object v2, v16, v2

    #@112
    if-eqz v2, :cond_11a

    #@114
    const/4 v2, 0x1

    #@115
    aget-object v2, v16, v2

    #@117
    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_11a
    .catch Ljava/io/IOException; {:try_start_ff .. :try_end_11a} :catch_1a3

    #@11a
    .line 2732
    .end local v1           #runner:Lcom/android/server/BackupManagerService$PerformFullBackupTask$FullBackupRunner;
    .end local v6           #token:I
    .end local v7           #sendApk:Z
    .end local v9           #app:Landroid/content/pm/ApplicationInfo;
    .end local v14           #isSharedStorage:Z
    .end local v16           #pipes:[Landroid/os/ParcelFileDescriptor;
    .end local v18           #t:Ljava/lang/Thread;
    :cond_11a
    :goto_11a
    invoke-direct/range {p0 .. p1}, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->tearDown(Landroid/content/pm/PackageInfo;)V

    #@11d
    .line 2733
    return-void

    #@11e
    .line 2715
    .restart local v16       #pipes:[Landroid/os/ParcelFileDescriptor;
    :catch_11e
    move-exception v12

    #@11f
    .line 2716
    .restart local v12       #e:Ljava/io/IOException;
    :try_start_11f
    const-string v2, "BackupManagerService"

    #@121
    new-instance v3, Ljava/lang/StringBuilder;

    #@123
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@126
    const-string v5, "Error backing up "

    #@128
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12b
    move-result-object v3

    #@12c
    move-object/from16 v0, p1

    #@12e
    iget-object v5, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@130
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@133
    move-result-object v3

    #@134
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@137
    move-result-object v3

    #@138
    invoke-static {v2, v3, v12}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_13b
    .catchall {:try_start_11f .. :try_end_13b} :catchall_160

    #@13b
    .line 2720
    :try_start_13b
    invoke-virtual/range {p2 .. p2}, Ljava/io/OutputStream;->flush()V

    #@13e
    .line 2721
    if-eqz v16, :cond_11a

    #@140
    .line 2722
    const/4 v2, 0x0

    #@141
    aget-object v2, v16, v2

    #@143
    if-eqz v2, :cond_14b

    #@145
    const/4 v2, 0x0

    #@146
    aget-object v2, v16, v2

    #@148
    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->close()V

    #@14b
    .line 2723
    :cond_14b
    const/4 v2, 0x1

    #@14c
    aget-object v2, v16, v2

    #@14e
    if-eqz v2, :cond_11a

    #@150
    const/4 v2, 0x1

    #@151
    aget-object v2, v16, v2

    #@153
    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_156
    .catch Ljava/io/IOException; {:try_start_13b .. :try_end_156} :catch_157

    #@156
    goto :goto_11a

    #@157
    .line 2725
    :catch_157
    move-exception v12

    #@158
    .line 2726
    const-string v2, "BackupManagerService"

    #@15a
    const-string v3, "Error bringing down backup stack"

    #@15c
    :goto_15c
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@15f
    goto :goto_11a

    #@160
    .line 2718
    .end local v12           #e:Ljava/io/IOException;
    :catchall_160
    move-exception v2

    #@161
    .line 2720
    :try_start_161
    invoke-virtual/range {p2 .. p2}, Ljava/io/OutputStream;->flush()V

    #@164
    .line 2721
    if-eqz v16, :cond_17c

    #@166
    .line 2722
    const/4 v3, 0x0

    #@167
    aget-object v3, v16, v3

    #@169
    if-eqz v3, :cond_171

    #@16b
    const/4 v3, 0x0

    #@16c
    aget-object v3, v16, v3

    #@16e
    invoke-virtual {v3}, Landroid/os/ParcelFileDescriptor;->close()V

    #@171
    .line 2723
    :cond_171
    const/4 v3, 0x1

    #@172
    aget-object v3, v16, v3

    #@174
    if-eqz v3, :cond_17c

    #@176
    const/4 v3, 0x1

    #@177
    aget-object v3, v16, v3

    #@179
    invoke-virtual {v3}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_17c
    .catch Ljava/io/IOException; {:try_start_161 .. :try_end_17c} :catch_19a

    #@17c
    .line 2718
    :cond_17c
    :goto_17c
    throw v2

    #@17d
    .line 2730
    .end local v16           #pipes:[Landroid/os/ParcelFileDescriptor;
    :cond_17d
    const-string v2, "BackupManagerService"

    #@17f
    new-instance v3, Ljava/lang/StringBuilder;

    #@181
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@184
    const-string v5, "Unable to bind to full agent for "

    #@186
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@189
    move-result-object v3

    #@18a
    move-object/from16 v0, p1

    #@18c
    iget-object v5, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@18e
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@191
    move-result-object v3

    #@192
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@195
    move-result-object v3

    #@196
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@199
    goto :goto_11a

    #@19a
    .line 2725
    .restart local v16       #pipes:[Landroid/os/ParcelFileDescriptor;
    :catch_19a
    move-exception v12

    #@19b
    .line 2726
    .restart local v12       #e:Ljava/io/IOException;
    const-string v3, "BackupManagerService"

    #@19d
    const-string v5, "Error bringing down backup stack"

    #@19f
    invoke-static {v3, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1a2
    goto :goto_17c

    #@1a3
    .line 2725
    .end local v12           #e:Ljava/io/IOException;
    .restart local v1       #runner:Lcom/android/server/BackupManagerService$PerformFullBackupTask$FullBackupRunner;
    .restart local v6       #token:I
    .restart local v7       #sendApk:Z
    .restart local v9       #app:Landroid/content/pm/ApplicationInfo;
    .restart local v14       #isSharedStorage:Z
    .restart local v18       #t:Ljava/lang/Thread;
    :catch_1a3
    move-exception v12

    #@1a4
    .line 2726
    .restart local v12       #e:Ljava/io/IOException;
    const-string v2, "BackupManagerService"

    #@1a6
    const-string v3, "Error bringing down backup stack"

    #@1a8
    goto :goto_15c
.end method

.method private emitAesBackupHeader(Ljava/lang/StringBuilder;Ljava/io/OutputStream;)Ljava/io/OutputStream;
    .registers 23
    .parameter "headerbuf"
    .parameter "ofstream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    #@0
    .prologue
    .line 2592
    move-object/from16 v0, p0

    #@2
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@4
    move-object/from16 v17, v0

    #@6
    const/16 v18, 0x200

    #@8
    invoke-static/range {v17 .. v18}, Lcom/android/server/BackupManagerService;->access$1100(Lcom/android/server/BackupManagerService;I)[B

    #@b
    move-result-object v15

    #@c
    .line 2593
    .local v15, newUserSalt:[B
    move-object/from16 v0, p0

    #@e
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@10
    move-object/from16 v17, v0

    #@12
    move-object/from16 v0, p0

    #@14
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mEncryptPassword:Ljava/lang/String;

    #@16
    move-object/from16 v18, v0

    #@18
    const/16 v19, 0x2710

    #@1a
    move-object/from16 v0, v17

    #@1c
    move-object/from16 v1, v18

    #@1e
    move/from16 v2, v19

    #@20
    invoke-static {v0, v1, v15, v2}, Lcom/android/server/BackupManagerService;->access$1200(Lcom/android/server/BackupManagerService;Ljava/lang/String;[BI)Ljavax/crypto/SecretKey;

    #@23
    move-result-object v16

    #@24
    .line 2597
    .local v16, userKey:Ljavax/crypto/SecretKey;
    const/16 v17, 0x20

    #@26
    move/from16 v0, v17

    #@28
    new-array v11, v0, [B

    #@2a
    .line 2598
    .local v11, masterPw:[B
    move-object/from16 v0, p0

    #@2c
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@2e
    move-object/from16 v17, v0

    #@30
    invoke-static/range {v17 .. v17}, Lcom/android/server/BackupManagerService;->access$1300(Lcom/android/server/BackupManagerService;)Ljava/security/SecureRandom;

    #@33
    move-result-object v17

    #@34
    move-object/from16 v0, v17

    #@36
    invoke-virtual {v0, v11}, Ljava/security/SecureRandom;->nextBytes([B)V

    #@39
    .line 2599
    move-object/from16 v0, p0

    #@3b
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@3d
    move-object/from16 v17, v0

    #@3f
    const/16 v18, 0x200

    #@41
    invoke-static/range {v17 .. v18}, Lcom/android/server/BackupManagerService;->access$1100(Lcom/android/server/BackupManagerService;I)[B

    #@44
    move-result-object v7

    #@45
    .line 2602
    .local v7, checksumSalt:[B
    const-string v17, "AES/CBC/PKCS5Padding"

    #@47
    invoke-static/range {v17 .. v17}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    #@4a
    move-result-object v5

    #@4b
    .line 2603
    .local v5, c:Ljavax/crypto/Cipher;
    new-instance v10, Ljavax/crypto/spec/SecretKeySpec;

    #@4d
    const-string v17, "AES"

    #@4f
    move-object/from16 v0, v17

    #@51
    invoke-direct {v10, v11, v0}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    #@54
    .line 2604
    .local v10, masterKeySpec:Ljavax/crypto/spec/SecretKeySpec;
    const/16 v17, 0x1

    #@56
    move/from16 v0, v17

    #@58
    invoke-virtual {v5, v0, v10}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    #@5b
    .line 2605
    new-instance v9, Ljavax/crypto/CipherOutputStream;

    #@5d
    move-object/from16 v0, p2

    #@5f
    invoke-direct {v9, v0, v5}, Ljavax/crypto/CipherOutputStream;-><init>(Ljava/io/OutputStream;Ljavax/crypto/Cipher;)V

    #@62
    .line 2608
    .local v9, finalOutput:Ljava/io/OutputStream;
    const-string v17, "AES-256"

    #@64
    move-object/from16 v0, p1

    #@66
    move-object/from16 v1, v17

    #@68
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    .line 2609
    const/16 v17, 0xa

    #@6d
    move-object/from16 v0, p1

    #@6f
    move/from16 v1, v17

    #@71
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@74
    .line 2611
    move-object/from16 v0, p0

    #@76
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@78
    move-object/from16 v17, v0

    #@7a
    move-object/from16 v0, v17

    #@7c
    invoke-static {v0, v15}, Lcom/android/server/BackupManagerService;->access$1400(Lcom/android/server/BackupManagerService;[B)Ljava/lang/String;

    #@7f
    move-result-object v17

    #@80
    move-object/from16 v0, p1

    #@82
    move-object/from16 v1, v17

    #@84
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    .line 2612
    const/16 v17, 0xa

    #@89
    move-object/from16 v0, p1

    #@8b
    move/from16 v1, v17

    #@8d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@90
    .line 2614
    move-object/from16 v0, p0

    #@92
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@94
    move-object/from16 v17, v0

    #@96
    move-object/from16 v0, v17

    #@98
    invoke-static {v0, v7}, Lcom/android/server/BackupManagerService;->access$1400(Lcom/android/server/BackupManagerService;[B)Ljava/lang/String;

    #@9b
    move-result-object v17

    #@9c
    move-object/from16 v0, p1

    #@9e
    move-object/from16 v1, v17

    #@a0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a3
    .line 2615
    const/16 v17, 0xa

    #@a5
    move-object/from16 v0, p1

    #@a7
    move/from16 v1, v17

    #@a9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@ac
    .line 2617
    const/16 v17, 0x2710

    #@ae
    move-object/from16 v0, p1

    #@b0
    move/from16 v1, v17

    #@b2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b5
    .line 2618
    const/16 v17, 0xa

    #@b7
    move-object/from16 v0, p1

    #@b9
    move/from16 v1, v17

    #@bb
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@be
    .line 2621
    const-string v17, "AES/CBC/PKCS5Padding"

    #@c0
    invoke-static/range {v17 .. v17}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    #@c3
    move-result-object v13

    #@c4
    .line 2622
    .local v13, mkC:Ljavax/crypto/Cipher;
    const/16 v17, 0x1

    #@c6
    move/from16 v0, v17

    #@c8
    move-object/from16 v1, v16

    #@ca
    invoke-virtual {v13, v0, v1}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    #@cd
    .line 2624
    invoke-virtual {v13}, Ljavax/crypto/Cipher;->getIV()[B

    #@d0
    move-result-object v3

    #@d1
    .line 2625
    .local v3, IV:[B
    move-object/from16 v0, p0

    #@d3
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@d5
    move-object/from16 v17, v0

    #@d7
    move-object/from16 v0, v17

    #@d9
    invoke-static {v0, v3}, Lcom/android/server/BackupManagerService;->access$1400(Lcom/android/server/BackupManagerService;[B)Ljava/lang/String;

    #@dc
    move-result-object v17

    #@dd
    move-object/from16 v0, p1

    #@df
    move-object/from16 v1, v17

    #@e1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e4
    .line 2626
    const/16 v17, 0xa

    #@e6
    move-object/from16 v0, p1

    #@e8
    move/from16 v1, v17

    #@ea
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@ed
    .line 2638
    invoke-virtual {v5}, Ljavax/crypto/Cipher;->getIV()[B

    #@f0
    move-result-object v3

    #@f1
    .line 2639
    invoke-virtual {v10}, Ljavax/crypto/spec/SecretKeySpec;->getEncoded()[B

    #@f4
    move-result-object v12

    #@f5
    .line 2640
    .local v12, mk:[B
    move-object/from16 v0, p0

    #@f7
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@f9
    move-object/from16 v17, v0

    #@fb
    invoke-virtual {v10}, Ljavax/crypto/spec/SecretKeySpec;->getEncoded()[B

    #@fe
    move-result-object v18

    #@ff
    const/16 v19, 0x2710

    #@101
    move-object/from16 v0, v17

    #@103
    move-object/from16 v1, v18

    #@105
    move/from16 v2, v19

    #@107
    invoke-static {v0, v1, v7, v2}, Lcom/android/server/BackupManagerService;->access$1500(Lcom/android/server/BackupManagerService;[B[BI)[B

    #@10a
    move-result-object v6

    #@10b
    .line 2643
    .local v6, checksum:[B
    new-instance v4, Ljava/io/ByteArrayOutputStream;

    #@10d
    array-length v0, v3

    #@10e
    move/from16 v17, v0

    #@110
    array-length v0, v12

    #@111
    move/from16 v18, v0

    #@113
    add-int v17, v17, v18

    #@115
    array-length v0, v6

    #@116
    move/from16 v18, v0

    #@118
    add-int v17, v17, v18

    #@11a
    add-int/lit8 v17, v17, 0x3

    #@11c
    move/from16 v0, v17

    #@11e
    invoke-direct {v4, v0}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    #@121
    .line 2645
    .local v4, blob:Ljava/io/ByteArrayOutputStream;
    new-instance v14, Ljava/io/DataOutputStream;

    #@123
    invoke-direct {v14, v4}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    #@126
    .line 2646
    .local v14, mkOut:Ljava/io/DataOutputStream;
    array-length v0, v3

    #@127
    move/from16 v17, v0

    #@129
    move/from16 v0, v17

    #@12b
    invoke-virtual {v14, v0}, Ljava/io/DataOutputStream;->writeByte(I)V

    #@12e
    .line 2647
    invoke-virtual {v14, v3}, Ljava/io/DataOutputStream;->write([B)V

    #@131
    .line 2648
    array-length v0, v12

    #@132
    move/from16 v17, v0

    #@134
    move/from16 v0, v17

    #@136
    invoke-virtual {v14, v0}, Ljava/io/DataOutputStream;->writeByte(I)V

    #@139
    .line 2649
    invoke-virtual {v14, v12}, Ljava/io/DataOutputStream;->write([B)V

    #@13c
    .line 2650
    array-length v0, v6

    #@13d
    move/from16 v17, v0

    #@13f
    move/from16 v0, v17

    #@141
    invoke-virtual {v14, v0}, Ljava/io/DataOutputStream;->writeByte(I)V

    #@144
    .line 2651
    invoke-virtual {v14, v6}, Ljava/io/DataOutputStream;->write([B)V

    #@147
    .line 2652
    invoke-virtual {v14}, Ljava/io/DataOutputStream;->flush()V

    #@14a
    .line 2653
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@14d
    move-result-object v17

    #@14e
    move-object/from16 v0, v17

    #@150
    invoke-virtual {v13, v0}, Ljavax/crypto/Cipher;->doFinal([B)[B

    #@153
    move-result-object v8

    #@154
    .line 2654
    .local v8, encryptedMk:[B
    move-object/from16 v0, p0

    #@156
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@158
    move-object/from16 v17, v0

    #@15a
    move-object/from16 v0, v17

    #@15c
    invoke-static {v0, v8}, Lcom/android/server/BackupManagerService;->access$1400(Lcom/android/server/BackupManagerService;[B)Ljava/lang/String;

    #@15f
    move-result-object v17

    #@160
    move-object/from16 v0, p1

    #@162
    move-object/from16 v1, v17

    #@164
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@167
    .line 2655
    const/16 v17, 0xa

    #@169
    move-object/from16 v0, p1

    #@16b
    move/from16 v1, v17

    #@16d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@170
    .line 2657
    return-object v9
.end method

.method private finalizeBackup(Ljava/io/OutputStream;)V
    .registers 6
    .parameter "out"

    #@0
    .prologue
    .line 2765
    const/16 v2, 0x400

    #@2
    :try_start_2
    new-array v1, v2, [B

    #@4
    .line 2766
    .local v1, eof:[B
    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_7} :catch_8

    #@7
    .line 2770
    .end local v1           #eof:[B
    :goto_7
    return-void

    #@8
    .line 2767
    :catch_8
    move-exception v0

    #@9
    .line 2768
    .local v0, e:Ljava/io/IOException;
    const-string v2, "BackupManagerService"

    #@b
    const-string v3, "Error attempting to finalize backup stream"

    #@d
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    goto :goto_7
.end method

.method private tearDown(Landroid/content/pm/PackageInfo;)V
    .registers 7
    .parameter "pkg"

    #@0
    .prologue
    .line 2812
    if-eqz p1, :cond_28

    #@2
    .line 2813
    iget-object v0, p1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@4
    .line 2814
    .local v0, app:Landroid/content/pm/ApplicationInfo;
    if-eqz v0, :cond_28

    #@6
    .line 2817
    :try_start_6
    iget-object v2, p0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@8
    invoke-static {v2}, Lcom/android/server/BackupManagerService;->access$800(Lcom/android/server/BackupManagerService;)Landroid/app/IActivityManager;

    #@b
    move-result-object v2

    #@c
    invoke-interface {v2, v0}, Landroid/app/IActivityManager;->unbindBackupAgent(Landroid/content/pm/ApplicationInfo;)V

    #@f
    .line 2820
    iget v2, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    #@11
    const/16 v3, 0x3e8

    #@13
    if-eq v2, v3, :cond_28

    #@15
    iget v2, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    #@17
    const/16 v3, 0x3e9

    #@19
    if-eq v2, v3, :cond_28

    #@1b
    .line 2823
    iget-object v2, p0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@1d
    invoke-static {v2}, Lcom/android/server/BackupManagerService;->access$800(Lcom/android/server/BackupManagerService;)Landroid/app/IActivityManager;

    #@20
    move-result-object v2

    #@21
    iget-object v3, v0, Landroid/content/pm/ApplicationInfo;->processName:Ljava/lang/String;

    #@23
    iget v4, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    #@25
    invoke-interface {v2, v3, v4}, Landroid/app/IActivityManager;->killApplicationProcess(Ljava/lang/String;I)V
    :try_end_28
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_28} :catch_29

    #@28
    .line 2832
    .end local v0           #app:Landroid/content/pm/ApplicationInfo;
    :cond_28
    :goto_28
    return-void

    #@29
    .line 2827
    .restart local v0       #app:Landroid/content/pm/ApplicationInfo;
    :catch_29
    move-exception v1

    #@2a
    .line 2828
    .local v1, e:Landroid/os/RemoteException;
    const-string v2, "BackupManagerService"

    #@2c
    const-string v3, "Lost app trying to shut down"

    #@2e
    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@31
    goto :goto_28
.end method

.method private writeApkToBackup(Landroid/content/pm/PackageInfo;Landroid/app/backup/BackupDataOutput;)V
    .registers 22
    .parameter "pkg"
    .parameter "output"

    #@0
    .prologue
    .line 2737
    move-object/from16 v0, p1

    #@2
    iget-object v1, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@4
    iget-object v5, v1, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    #@6
    .line 2738
    .local v5, appSourceDir:Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    #@8
    invoke-direct {v1, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@b
    invoke-virtual {v1}, Ljava/io/File;->getParent()Ljava/lang/String;

    #@e
    move-result-object v4

    #@f
    .line 2739
    .local v4, apkDir:Ljava/lang/String;
    move-object/from16 v0, p1

    #@11
    iget-object v1, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@13
    const-string v2, "a"

    #@15
    const/4 v3, 0x0

    #@16
    move-object/from16 v6, p2

    #@18
    invoke-static/range {v1 .. v6}, Landroid/app/backup/FullBackup;->backupToTar(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/backup/BackupDataOutput;)I

    #@1b
    .line 2747
    new-instance v18, Landroid/os/Environment$UserEnvironment;

    #@1d
    const/4 v1, 0x0

    #@1e
    move-object/from16 v0, v18

    #@20
    invoke-direct {v0, v1}, Landroid/os/Environment$UserEnvironment;-><init>(I)V

    #@23
    .line 2748
    .local v18, userEnv:Landroid/os/Environment$UserEnvironment;
    move-object/from16 v0, p1

    #@25
    iget-object v1, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@27
    move-object/from16 v0, v18

    #@29
    invoke-virtual {v0, v1}, Landroid/os/Environment$UserEnvironment;->getExternalStorageAppObbDirectory(Ljava/lang/String;)Ljava/io/File;

    #@2c
    move-result-object v16

    #@2d
    .line 2749
    .local v16, obbDir:Ljava/io/File;
    if-eqz v16, :cond_54

    #@2f
    .line 2751
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->listFiles()[Ljava/io/File;

    #@32
    move-result-object v17

    #@33
    .line 2752
    .local v17, obbFiles:[Ljava/io/File;
    if-eqz v17, :cond_54

    #@35
    .line 2753
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@38
    move-result-object v9

    #@39
    .line 2754
    .local v9, obbDirName:Ljava/lang/String;
    move-object/from16 v12, v17

    #@3b
    .local v12, arr$:[Ljava/io/File;
    array-length v14, v12

    #@3c
    .local v14, len$:I
    const/4 v13, 0x0

    #@3d
    .local v13, i$:I
    :goto_3d
    if-ge v13, v14, :cond_54

    #@3f
    aget-object v15, v12, v13

    #@41
    .line 2755
    .local v15, obb:Ljava/io/File;
    move-object/from16 v0, p1

    #@43
    iget-object v6, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@45
    const-string v7, "obb"

    #@47
    const/4 v8, 0x0

    #@48
    invoke-virtual {v15}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@4b
    move-result-object v10

    #@4c
    move-object/from16 v11, p2

    #@4e
    invoke-static/range {v6 .. v11}, Landroid/app/backup/FullBackup;->backupToTar(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/backup/BackupDataOutput;)I

    #@51
    .line 2754
    add-int/lit8 v13, v13, 0x1

    #@53
    goto :goto_3d

    #@54
    .line 2760
    .end local v9           #obbDirName:Ljava/lang/String;
    .end local v12           #arr$:[Ljava/io/File;
    .end local v13           #i$:I
    .end local v14           #len$:I
    .end local v15           #obb:Ljava/io/File;
    .end local v17           #obbFiles:[Ljava/io/File;
    :cond_54
    return-void
.end method

.method private writeAppManifest(Landroid/content/pm/PackageInfo;Ljava/io/File;Z)V
    .registers 14
    .parameter "pkg"
    .parameter "manifestFile"
    .parameter "withApk"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 2785
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    const/16 v8, 0x1000

    #@4
    invoke-direct {v1, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    #@7
    .line 2786
    .local v1, builder:Ljava/lang/StringBuilder;
    new-instance v6, Landroid/util/StringBuilderPrinter;

    #@9
    invoke-direct {v6, v1}, Landroid/util/StringBuilderPrinter;-><init>(Ljava/lang/StringBuilder;)V

    #@c
    .line 2788
    .local v6, printer:Landroid/util/StringBuilderPrinter;
    const/4 v8, 0x1

    #@d
    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@10
    move-result-object v8

    #@11
    invoke-virtual {v6, v8}, Landroid/util/StringBuilderPrinter;->println(Ljava/lang/String;)V

    #@14
    .line 2789
    iget-object v8, p1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@16
    invoke-virtual {v6, v8}, Landroid/util/StringBuilderPrinter;->println(Ljava/lang/String;)V

    #@19
    .line 2790
    iget v8, p1, Landroid/content/pm/PackageInfo;->versionCode:I

    #@1b
    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@1e
    move-result-object v8

    #@1f
    invoke-virtual {v6, v8}, Landroid/util/StringBuilderPrinter;->println(Ljava/lang/String;)V

    #@22
    .line 2791
    sget v8, Landroid/os/Build$VERSION;->SDK_INT:I

    #@24
    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@27
    move-result-object v8

    #@28
    invoke-virtual {v6, v8}, Landroid/util/StringBuilderPrinter;->println(Ljava/lang/String;)V

    #@2b
    .line 2793
    iget-object v8, p0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@2d
    invoke-static {v8}, Lcom/android/server/BackupManagerService;->access$600(Lcom/android/server/BackupManagerService;)Landroid/content/pm/PackageManager;

    #@30
    move-result-object v8

    #@31
    iget-object v9, p1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@33
    invoke-virtual {v8, v9}, Landroid/content/pm/PackageManager;->getInstallerPackageName(Ljava/lang/String;)Ljava/lang/String;

    #@36
    move-result-object v3

    #@37
    .line 2794
    .local v3, installerName:Ljava/lang/String;
    if-eqz v3, :cond_60

    #@39
    .end local v3           #installerName:Ljava/lang/String;
    :goto_39
    invoke-virtual {v6, v3}, Landroid/util/StringBuilderPrinter;->println(Ljava/lang/String;)V

    #@3c
    .line 2796
    if-eqz p3, :cond_63

    #@3e
    const-string v8, "1"

    #@40
    :goto_40
    invoke-virtual {v6, v8}, Landroid/util/StringBuilderPrinter;->println(Ljava/lang/String;)V

    #@43
    .line 2797
    iget-object v8, p1, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    #@45
    if-nez v8, :cond_66

    #@47
    .line 2798
    const-string v8, "0"

    #@49
    invoke-virtual {v6, v8}, Landroid/util/StringBuilderPrinter;->println(Ljava/lang/String;)V

    #@4c
    .line 2806
    :cond_4c
    new-instance v5, Ljava/io/FileOutputStream;

    #@4e
    invoke-direct {v5, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    #@51
    .line 2807
    .local v5, outstream:Ljava/io/FileOutputStream;
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v8

    #@55
    invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B

    #@58
    move-result-object v8

    #@59
    invoke-virtual {v5, v8}, Ljava/io/FileOutputStream;->write([B)V

    #@5c
    .line 2808
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V

    #@5f
    .line 2809
    return-void

    #@60
    .line 2794
    .end local v5           #outstream:Ljava/io/FileOutputStream;
    .restart local v3       #installerName:Ljava/lang/String;
    :cond_60
    const-string v3, ""

    #@62
    goto :goto_39

    #@63
    .line 2796
    .end local v3           #installerName:Ljava/lang/String;
    :cond_63
    const-string v8, "0"

    #@65
    goto :goto_40

    #@66
    .line 2800
    :cond_66
    iget-object v8, p1, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    #@68
    array-length v8, v8

    #@69
    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@6c
    move-result-object v8

    #@6d
    invoke-virtual {v6, v8}, Landroid/util/StringBuilderPrinter;->println(Ljava/lang/String;)V

    #@70
    .line 2801
    iget-object v0, p1, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    #@72
    .local v0, arr$:[Landroid/content/pm/Signature;
    array-length v4, v0

    #@73
    .local v4, len$:I
    const/4 v2, 0x0

    #@74
    .local v2, i$:I
    :goto_74
    if-ge v2, v4, :cond_4c

    #@76
    aget-object v7, v0, v2

    #@78
    .line 2802
    .local v7, sig:Landroid/content/pm/Signature;
    invoke-virtual {v7}, Landroid/content/pm/Signature;->toCharsString()Ljava/lang/String;

    #@7b
    move-result-object v8

    #@7c
    invoke-virtual {v6, v8}, Landroid/util/StringBuilderPrinter;->println(Ljava/lang/String;)V

    #@7f
    .line 2801
    add-int/lit8 v2, v2, 0x1

    #@81
    goto :goto_74
.end method


# virtual methods
.method public run()V
    .registers 26

    #@0
    .prologue
    .line 2408
    new-instance v18, Ljava/util/ArrayList;

    #@2
    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 2410
    .local v18, packagesToBackup:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    const-string v21, "BackupManagerService"

    #@7
    const-string v22, "--- Performing full-dataset backup ---"

    #@9
    invoke-static/range {v21 .. v22}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 2411
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->sendStartBackup()V

    #@f
    .line 2414
    move-object/from16 v0, p0

    #@11
    iget-boolean v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mAllApps:Z

    #@13
    move/from16 v21, v0

    #@15
    if-eqz v21, :cond_59

    #@17
    .line 2415
    move-object/from16 v0, p0

    #@19
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@1b
    move-object/from16 v21, v0

    #@1d
    invoke-static/range {v21 .. v21}, Lcom/android/server/BackupManagerService;->access$600(Lcom/android/server/BackupManagerService;)Landroid/content/pm/PackageManager;

    #@20
    move-result-object v21

    #@21
    const/16 v22, 0x40

    #@23
    invoke-virtual/range {v21 .. v22}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    #@26
    move-result-object v18

    #@27
    .line 2418
    move-object/from16 v0, p0

    #@29
    iget-boolean v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mIncludeSystem:Z

    #@2b
    move/from16 v21, v0

    #@2d
    if-nez v21, :cond_59

    #@2f
    .line 2419
    const/4 v13, 0x0

    #@30
    .local v13, i:I
    :goto_30
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    #@33
    move-result v21

    #@34
    move/from16 v0, v21

    #@36
    if-ge v13, v0, :cond_59

    #@38
    .line 2420
    move-object/from16 v0, v18

    #@3a
    invoke-interface {v0, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@3d
    move-result-object v19

    #@3e
    check-cast v19, Landroid/content/pm/PackageInfo;

    #@40
    .line 2421
    .local v19, pkg:Landroid/content/pm/PackageInfo;
    move-object/from16 v0, v19

    #@42
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@44
    move-object/from16 v21, v0

    #@46
    move-object/from16 v0, v21

    #@48
    iget v0, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@4a
    move/from16 v21, v0

    #@4c
    and-int/lit8 v21, v21, 0x1

    #@4e
    if-eqz v21, :cond_56

    #@50
    .line 2422
    move-object/from16 v0, v18

    #@52
    invoke-interface {v0, v13}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    #@55
    goto :goto_30

    #@56
    .line 2424
    :cond_56
    add-int/lit8 v13, v13, 0x1

    #@58
    goto :goto_30

    #@59
    .line 2433
    .end local v13           #i:I
    .end local v19           #pkg:Landroid/content/pm/PackageInfo;
    :cond_59
    move-object/from16 v0, p0

    #@5b
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mPackages:[Ljava/lang/String;

    #@5d
    move-object/from16 v21, v0

    #@5f
    if-eqz v21, :cond_af

    #@61
    .line 2434
    move-object/from16 v0, p0

    #@63
    iget-object v4, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mPackages:[Ljava/lang/String;

    #@65
    .local v4, arr$:[Ljava/lang/String;
    array-length v15, v4

    #@66
    .local v15, len$:I
    const/4 v14, 0x0

    #@67
    .local v14, i$:I
    :goto_67
    if-ge v14, v15, :cond_af

    #@69
    aget-object v20, v4, v14

    #@6b
    .line 2436
    .local v20, pkgName:Ljava/lang/String;
    :try_start_6b
    move-object/from16 v0, p0

    #@6d
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@6f
    move-object/from16 v21, v0

    #@71
    invoke-static/range {v21 .. v21}, Lcom/android/server/BackupManagerService;->access$600(Lcom/android/server/BackupManagerService;)Landroid/content/pm/PackageManager;

    #@74
    move-result-object v21

    #@75
    const/16 v22, 0x40

    #@77
    move-object/from16 v0, v21

    #@79
    move-object/from16 v1, v20

    #@7b
    move/from16 v2, v22

    #@7d
    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    #@80
    move-result-object v21

    #@81
    move-object/from16 v0, v18

    #@83
    move-object/from16 v1, v21

    #@85
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_88
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_6b .. :try_end_88} :catch_8b

    #@88
    .line 2434
    :goto_88
    add-int/lit8 v14, v14, 0x1

    #@8a
    goto :goto_67

    #@8b
    .line 2438
    :catch_8b
    move-exception v7

    #@8c
    .line 2439
    .local v7, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v21, "BackupManagerService"

    #@8e
    new-instance v22, Ljava/lang/StringBuilder;

    #@90
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@93
    const-string v23, "Unknown package "

    #@95
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@98
    move-result-object v22

    #@99
    move-object/from16 v0, v22

    #@9b
    move-object/from16 v1, v20

    #@9d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a0
    move-result-object v22

    #@a1
    const-string v23, ", skipping"

    #@a3
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v22

    #@a7
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@aa
    move-result-object v22

    #@ab
    invoke-static/range {v21 .. v22}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@ae
    goto :goto_88

    #@af
    .line 2447
    .end local v4           #arr$:[Ljava/lang/String;
    .end local v7           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v14           #i$:I
    .end local v15           #len$:I
    .end local v20           #pkgName:Ljava/lang/String;
    :cond_af
    const/4 v13, 0x0

    #@b0
    .restart local v13       #i:I
    :goto_b0
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    #@b3
    move-result v21

    #@b4
    move/from16 v0, v21

    #@b6
    if-ge v13, v0, :cond_ea

    #@b8
    .line 2448
    move-object/from16 v0, v18

    #@ba
    invoke-interface {v0, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@bd
    move-result-object v19

    #@be
    check-cast v19, Landroid/content/pm/PackageInfo;

    #@c0
    .line 2449
    .restart local v19       #pkg:Landroid/content/pm/PackageInfo;
    move-object/from16 v0, v19

    #@c2
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@c4
    move-object/from16 v21, v0

    #@c6
    move-object/from16 v0, v21

    #@c8
    iget v0, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@ca
    move/from16 v21, v0

    #@cc
    const v22, 0x8000

    #@cf
    and-int v21, v21, v22

    #@d1
    if-eqz v21, :cond_e1

    #@d3
    move-object/from16 v0, v19

    #@d5
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    #@d7
    move-object/from16 v21, v0

    #@d9
    const-string v22, "com.android.sharedstoragebackup"

    #@db
    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@de
    move-result v21

    #@df
    if-eqz v21, :cond_e7

    #@e1
    .line 2451
    :cond_e1
    move-object/from16 v0, v18

    #@e3
    invoke-interface {v0, v13}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    #@e6
    goto :goto_b0

    #@e7
    .line 2453
    :cond_e7
    add-int/lit8 v13, v13, 0x1

    #@e9
    goto :goto_b0

    #@ea
    .line 2459
    .end local v19           #pkg:Landroid/content/pm/PackageInfo;
    :cond_ea
    const/4 v13, 0x0

    #@eb
    :goto_eb
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    #@ee
    move-result v21

    #@ef
    move/from16 v0, v21

    #@f1
    if-ge v13, v0, :cond_126

    #@f3
    .line 2460
    move-object/from16 v0, v18

    #@f5
    invoke-interface {v0, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@f8
    move-result-object v19

    #@f9
    check-cast v19, Landroid/content/pm/PackageInfo;

    #@fb
    .line 2461
    .restart local v19       #pkg:Landroid/content/pm/PackageInfo;
    move-object/from16 v0, v19

    #@fd
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@ff
    move-object/from16 v21, v0

    #@101
    move-object/from16 v0, v21

    #@103
    iget v0, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    #@105
    move/from16 v21, v0

    #@107
    const/16 v22, 0x2710

    #@109
    move/from16 v0, v21

    #@10b
    move/from16 v1, v22

    #@10d
    if-ge v0, v1, :cond_123

    #@10f
    move-object/from16 v0, v19

    #@111
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@113
    move-object/from16 v21, v0

    #@115
    move-object/from16 v0, v21

    #@117
    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->backupAgentName:Ljava/lang/String;

    #@119
    move-object/from16 v21, v0

    #@11b
    if-nez v21, :cond_123

    #@11d
    .line 2466
    move-object/from16 v0, v18

    #@11f
    invoke-interface {v0, v13}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    #@122
    goto :goto_eb

    #@123
    .line 2468
    :cond_123
    add-int/lit8 v13, v13, 0x1

    #@125
    goto :goto_eb

    #@126
    .line 2472
    .end local v19           #pkg:Landroid/content/pm/PackageInfo;
    :cond_126
    new-instance v16, Ljava/io/FileOutputStream;

    #@128
    move-object/from16 v0, p0

    #@12a
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mOutputFile:Landroid/os/ParcelFileDescriptor;

    #@12c
    move-object/from16 v21, v0

    #@12e
    invoke-virtual/range {v21 .. v21}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@131
    move-result-object v21

    #@132
    move-object/from16 v0, v16

    #@134
    move-object/from16 v1, v21

    #@136
    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V

    #@139
    .line 2473
    .local v16, ofstream:Ljava/io/FileOutputStream;
    const/16 v17, 0x0

    #@13b
    .line 2475
    .local v17, out:Ljava/io/OutputStream;
    const/16 v19, 0x0

    #@13d
    .line 2477
    .restart local v19       #pkg:Landroid/content/pm/PackageInfo;
    :try_start_13d
    move-object/from16 v0, p0

    #@13f
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mEncryptPassword:Ljava/lang/String;

    #@141
    move-object/from16 v21, v0

    #@143
    if-eqz v21, :cond_1da

    #@145
    move-object/from16 v0, p0

    #@147
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mEncryptPassword:Ljava/lang/String;

    #@149
    move-object/from16 v21, v0

    #@14b
    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    #@14e
    move-result v21

    #@14f
    if-lez v21, :cond_1da

    #@151
    const/4 v8, 0x1

    #@152
    .line 2478
    .local v8, encrypting:Z
    :goto_152
    const/4 v5, 0x1

    #@153
    .line 2479
    .local v5, compressing:Z
    move-object/from16 v9, v16

    #@155
    .line 2483
    .local v9, finalOutput:Ljava/io/OutputStream;
    move-object/from16 v0, p0

    #@157
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@159
    move-object/from16 v21, v0

    #@15b
    invoke-virtual/range {v21 .. v21}, Lcom/android/server/BackupManagerService;->hasBackupPassword()Z

    #@15e
    move-result v21

    #@15f
    if-eqz v21, :cond_1dd

    #@161
    .line 2484
    move-object/from16 v0, p0

    #@163
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@165
    move-object/from16 v21, v0

    #@167
    move-object/from16 v0, p0

    #@169
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mCurrentPassword:Ljava/lang/String;

    #@16b
    move-object/from16 v22, v0

    #@16d
    const/16 v23, 0x2710

    #@16f
    invoke-virtual/range {v21 .. v23}, Lcom/android/server/BackupManagerService;->passwordMatchesSaved(Ljava/lang/String;I)Z
    :try_end_172
    .catchall {:try_start_13d .. :try_end_172} :catchall_430
    .catch Landroid/os/RemoteException; {:try_start_13d .. :try_end_172} :catch_2ec
    .catch Ljava/lang/Exception; {:try_start_13d .. :try_end_172} :catch_3c1

    #@172
    move-result v21

    #@173
    if-nez v21, :cond_1dd

    #@175
    .line 2569
    move-object/from16 v0, p0

    #@177
    move-object/from16 v1, v19

    #@179
    invoke-direct {v0, v1}, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->tearDown(Landroid/content/pm/PackageInfo;)V

    #@17c
    .line 2571
    if-eqz v17, :cond_181

    #@17e
    :try_start_17e
    #Replaced unresolvable odex instruction with a throw
    throw v17
    #invoke-virtual-quick/range {v17 .. v17}, vtable@0xc

    #@181
    .line 2572
    :cond_181
    move-object/from16 v0, p0

    #@183
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mOutputFile:Landroid/os/ParcelFileDescriptor;

    #@185
    move-object/from16 v21, v0

    #@187
    invoke-virtual/range {v21 .. v21}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_18a
    .catch Ljava/io/IOException; {:try_start_17e .. :try_end_18a} :catch_4bc

    #@18a
    .line 2576
    :goto_18a
    move-object/from16 v0, p0

    #@18c
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@18e
    move-object/from16 v21, v0

    #@190
    move-object/from16 v0, v21

    #@192
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mCurrentOpLock:Ljava/lang/Object;

    #@194
    move-object/from16 v22, v0

    #@196
    monitor-enter v22

    #@197
    .line 2577
    :try_start_197
    move-object/from16 v0, p0

    #@199
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@19b
    move-object/from16 v21, v0

    #@19d
    move-object/from16 v0, v21

    #@19f
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mCurrentOperations:Landroid/util/SparseArray;

    #@1a1
    move-object/from16 v21, v0

    #@1a3
    invoke-virtual/range {v21 .. v21}, Landroid/util/SparseArray;->clear()V

    #@1a6
    .line 2578
    monitor-exit v22
    :try_end_1a7
    .catchall {:try_start_197 .. :try_end_1a7} :catchall_4a4

    #@1a7
    .line 2579
    move-object/from16 v0, p0

    #@1a9
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mLatchObject:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@1ab
    move-object/from16 v22, v0

    #@1ad
    monitor-enter v22

    #@1ae
    .line 2580
    :try_start_1ae
    move-object/from16 v0, p0

    #@1b0
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mLatchObject:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@1b2
    move-object/from16 v21, v0

    #@1b4
    const/16 v23, 0x1

    #@1b6
    move-object/from16 v0, v21

    #@1b8
    move/from16 v1, v23

    #@1ba
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    #@1bd
    .line 2581
    move-object/from16 v0, p0

    #@1bf
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mLatchObject:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@1c1
    move-object/from16 v21, v0

    #@1c3
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Object;->notifyAll()V

    #@1c6
    .line 2582
    monitor-exit v22
    :try_end_1c7
    .catchall {:try_start_1ae .. :try_end_1c7} :catchall_4a7

    #@1c7
    .line 2583
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->sendEndBackup()V

    #@1ca
    .line 2585
    move-object/from16 v0, p0

    #@1cc
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@1ce
    move-object/from16 v21, v0

    #@1d0
    move-object/from16 v0, v21

    #@1d2
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mWakelock:Landroid/os/PowerManager$WakeLock;

    #@1d4
    move-object/from16 v21, v0

    #@1d6
    .end local v5           #compressing:Z
    .end local v8           #encrypting:Z
    .end local v9           #finalOutput:Ljava/io/OutputStream;
    :goto_1d6
    invoke-virtual/range {v21 .. v21}, Landroid/os/PowerManager$WakeLock;->release()V

    #@1d9
    .line 2587
    return-void

    #@1da
    .line 2477
    :cond_1da
    const/4 v8, 0x0

    #@1db
    goto/16 :goto_152

    #@1dd
    .line 2515
    .restart local v5       #compressing:Z
    .restart local v8       #encrypting:Z
    .restart local v9       #finalOutput:Ljava/io/OutputStream;
    :cond_1dd
    :try_start_1dd
    new-instance v12, Ljava/lang/StringBuilder;

    #@1df
    const/16 v21, 0x400

    #@1e1
    move/from16 v0, v21

    #@1e3
    invoke-direct {v12, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    #@1e6
    .line 2517
    .local v12, headerbuf:Ljava/lang/StringBuilder;
    const-string v21, "ANDROID BACKUP\n"

    #@1e8
    move-object/from16 v0, v21

    #@1ea
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ed
    .line 2518
    const/16 v21, 0x1

    #@1ef
    move/from16 v0, v21

    #@1f1
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f4
    .line 2519
    if-eqz v5, :cond_267

    #@1f6
    const-string v21, "\n1\n"

    #@1f8
    :goto_1f8
    move-object/from16 v0, v21

    #@1fa
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1fd
    .catchall {:try_start_1dd .. :try_end_1fd} :catchall_430
    .catch Landroid/os/RemoteException; {:try_start_1dd .. :try_end_1fd} :catch_2ec
    .catch Ljava/lang/Exception; {:try_start_1dd .. :try_end_1fd} :catch_3c1

    #@1fd
    .line 2523
    if-eqz v8, :cond_26a

    #@1ff
    .line 2524
    :try_start_1ff
    move-object/from16 v0, p0

    #@201
    invoke-direct {v0, v12, v9}, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->emitAesBackupHeader(Ljava/lang/StringBuilder;Ljava/io/OutputStream;)Ljava/io/OutputStream;
    :try_end_204
    .catchall {:try_start_1ff .. :try_end_204} :catchall_430
    .catch Ljava/lang/Exception; {:try_start_1ff .. :try_end_204} :catch_273
    .catch Landroid/os/RemoteException; {:try_start_1ff .. :try_end_204} :catch_2ec

    #@204
    move-result-object v9

    #@205
    move-object v10, v9

    #@206
    .line 2529
    .end local v9           #finalOutput:Ljava/io/OutputStream;
    .local v10, finalOutput:Ljava/io/OutputStream;
    :goto_206
    :try_start_206
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@209
    move-result-object v21

    #@20a
    const-string v22, "UTF-8"

    #@20c
    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    #@20f
    move-result-object v11

    #@210
    .line 2530
    .local v11, header:[B
    move-object/from16 v0, v16

    #@212
    invoke-virtual {v0, v11}, Ljava/io/FileOutputStream;->write([B)V

    #@215
    .line 2533
    if-eqz v5, :cond_4cb

    #@217
    .line 2534
    new-instance v6, Ljava/util/zip/Deflater;

    #@219
    const/16 v21, 0x9

    #@21b
    move/from16 v0, v21

    #@21d
    invoke-direct {v6, v0}, Ljava/util/zip/Deflater;-><init>(I)V

    #@220
    .line 2535
    .local v6, deflater:Ljava/util/zip/Deflater;
    new-instance v9, Ljava/util/zip/DeflaterOutputStream;

    #@222
    const/16 v21, 0x1

    #@224
    move/from16 v0, v21

    #@226
    invoke-direct {v9, v10, v6, v0}, Ljava/util/zip/DeflaterOutputStream;-><init>(Ljava/io/OutputStream;Ljava/util/zip/Deflater;Z)V
    :try_end_229
    .catchall {:try_start_206 .. :try_end_229} :catchall_430
    .catch Ljava/lang/Exception; {:try_start_206 .. :try_end_229} :catch_4c7
    .catch Landroid/os/RemoteException; {:try_start_206 .. :try_end_229} :catch_2ec

    #@229
    .line 2538
    .end local v6           #deflater:Ljava/util/zip/Deflater;
    .end local v10           #finalOutput:Ljava/io/OutputStream;
    .restart local v9       #finalOutput:Ljava/io/OutputStream;
    :goto_229
    move-object/from16 v17, v9

    #@22b
    .line 2546
    :try_start_22b
    move-object/from16 v0, p0

    #@22d
    iget-boolean v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mIncludeShared:Z

    #@22f
    move/from16 v21, v0
    :try_end_231
    .catchall {:try_start_22b .. :try_end_231} :catchall_430
    .catch Landroid/os/RemoteException; {:try_start_22b .. :try_end_231} :catch_2ec
    .catch Ljava/lang/Exception; {:try_start_22b .. :try_end_231} :catch_3c1

    #@231
    if-eqz v21, :cond_248

    #@233
    .line 2548
    :try_start_233
    move-object/from16 v0, p0

    #@235
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@237
    move-object/from16 v21, v0

    #@239
    invoke-static/range {v21 .. v21}, Lcom/android/server/BackupManagerService;->access$600(Lcom/android/server/BackupManagerService;)Landroid/content/pm/PackageManager;

    #@23c
    move-result-object v21

    #@23d
    const-string v22, "com.android.sharedstoragebackup"

    #@23f
    const/16 v23, 0x0

    #@241
    invoke-virtual/range {v21 .. v23}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    #@244
    move-result-object v19

    #@245
    .line 2549
    invoke-interface/range {v18 .. v19}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_248
    .catchall {:try_start_233 .. :try_end_248} :catchall_430
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_233 .. :try_end_248} :catch_2e2
    .catch Landroid/os/RemoteException; {:try_start_233 .. :try_end_248} :catch_2ec
    .catch Ljava/lang/Exception; {:try_start_233 .. :try_end_248} :catch_3c1

    #@248
    .line 2556
    :cond_248
    :goto_248
    :try_start_248
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    #@24b
    move-result v3

    #@24c
    .line 2557
    .local v3, N:I
    const/4 v13, 0x0

    #@24d
    :goto_24d
    if-ge v13, v3, :cond_357

    #@24f
    .line 2558
    move-object/from16 v0, v18

    #@251
    invoke-interface {v0, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@254
    move-result-object v21

    #@255
    move-object/from16 v0, v21

    #@257
    check-cast v0, Landroid/content/pm/PackageInfo;

    #@259
    move-object/from16 v19, v0

    #@25b
    .line 2559
    move-object/from16 v0, p0

    #@25d
    move-object/from16 v1, v19

    #@25f
    move-object/from16 v2, v17

    #@261
    invoke-direct {v0, v1, v2}, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->backupOnePackage(Landroid/content/pm/PackageInfo;Ljava/io/OutputStream;)V

    #@264
    .line 2557
    add-int/lit8 v13, v13, 0x1

    #@266
    goto :goto_24d

    #@267
    .line 2519
    .end local v3           #N:I
    .end local v11           #header:[B
    :cond_267
    const-string v21, "\n0\n"
    :try_end_269
    .catchall {:try_start_248 .. :try_end_269} :catchall_430
    .catch Landroid/os/RemoteException; {:try_start_248 .. :try_end_269} :catch_2ec
    .catch Ljava/lang/Exception; {:try_start_248 .. :try_end_269} :catch_3c1

    #@269
    goto :goto_1f8

    #@26a
    .line 2526
    :cond_26a
    :try_start_26a
    const-string v21, "none\n"

    #@26c
    move-object/from16 v0, v21

    #@26e
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_271
    .catchall {:try_start_26a .. :try_end_271} :catchall_430
    .catch Ljava/lang/Exception; {:try_start_26a .. :try_end_271} :catch_273
    .catch Landroid/os/RemoteException; {:try_start_26a .. :try_end_271} :catch_2ec

    #@271
    move-object v10, v9

    #@272
    .end local v9           #finalOutput:Ljava/io/OutputStream;
    .restart local v10       #finalOutput:Ljava/io/OutputStream;
    goto :goto_206

    #@273
    .line 2539
    .end local v10           #finalOutput:Ljava/io/OutputStream;
    .restart local v9       #finalOutput:Ljava/io/OutputStream;
    :catch_273
    move-exception v7

    #@274
    .line 2541
    .local v7, e:Ljava/lang/Exception;
    :goto_274
    :try_start_274
    const-string v21, "BackupManagerService"

    #@276
    const-string v22, "Unable to emit archive header"

    #@278
    move-object/from16 v0, v21

    #@27a
    move-object/from16 v1, v22

    #@27c
    invoke-static {v0, v1, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_27f
    .catchall {:try_start_274 .. :try_end_27f} :catchall_430
    .catch Landroid/os/RemoteException; {:try_start_274 .. :try_end_27f} :catch_2ec
    .catch Ljava/lang/Exception; {:try_start_274 .. :try_end_27f} :catch_3c1

    #@27f
    .line 2569
    move-object/from16 v0, p0

    #@281
    move-object/from16 v1, v19

    #@283
    invoke-direct {v0, v1}, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->tearDown(Landroid/content/pm/PackageInfo;)V

    #@286
    .line 2571
    if-eqz v17, :cond_28b

    #@288
    :try_start_288
    #Replaced unresolvable odex instruction with a throw
    throw v17
    #invoke-virtual-quick/range {v17 .. v17}, vtable@0xc

    #@28b
    .line 2572
    :cond_28b
    move-object/from16 v0, p0

    #@28d
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mOutputFile:Landroid/os/ParcelFileDescriptor;

    #@28f
    move-object/from16 v21, v0

    #@291
    invoke-virtual/range {v21 .. v21}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_294
    .catch Ljava/io/IOException; {:try_start_288 .. :try_end_294} :catch_4b9

    #@294
    .line 2576
    :goto_294
    move-object/from16 v0, p0

    #@296
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@298
    move-object/from16 v21, v0

    #@29a
    move-object/from16 v0, v21

    #@29c
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mCurrentOpLock:Ljava/lang/Object;

    #@29e
    move-object/from16 v22, v0

    #@2a0
    monitor-enter v22

    #@2a1
    .line 2577
    :try_start_2a1
    move-object/from16 v0, p0

    #@2a3
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@2a5
    move-object/from16 v21, v0

    #@2a7
    move-object/from16 v0, v21

    #@2a9
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mCurrentOperations:Landroid/util/SparseArray;

    #@2ab
    move-object/from16 v21, v0

    #@2ad
    invoke-virtual/range {v21 .. v21}, Landroid/util/SparseArray;->clear()V

    #@2b0
    .line 2578
    monitor-exit v22
    :try_end_2b1
    .catchall {:try_start_2a1 .. :try_end_2b1} :catchall_4aa

    #@2b1
    .line 2579
    move-object/from16 v0, p0

    #@2b3
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mLatchObject:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@2b5
    move-object/from16 v22, v0

    #@2b7
    monitor-enter v22

    #@2b8
    .line 2580
    :try_start_2b8
    move-object/from16 v0, p0

    #@2ba
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mLatchObject:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@2bc
    move-object/from16 v21, v0

    #@2be
    const/16 v23, 0x1

    #@2c0
    move-object/from16 v0, v21

    #@2c2
    move/from16 v1, v23

    #@2c4
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    #@2c7
    .line 2581
    move-object/from16 v0, p0

    #@2c9
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mLatchObject:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@2cb
    move-object/from16 v21, v0

    #@2cd
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Object;->notifyAll()V

    #@2d0
    .line 2582
    monitor-exit v22
    :try_end_2d1
    .catchall {:try_start_2b8 .. :try_end_2d1} :catchall_4ad

    #@2d1
    .line 2583
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->sendEndBackup()V

    #@2d4
    .line 2585
    move-object/from16 v0, p0

    #@2d6
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@2d8
    move-object/from16 v21, v0

    #@2da
    move-object/from16 v0, v21

    #@2dc
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mWakelock:Landroid/os/PowerManager$WakeLock;

    #@2de
    move-object/from16 v21, v0

    #@2e0
    goto/16 :goto_1d6

    #@2e2
    .line 2550
    .end local v7           #e:Ljava/lang/Exception;
    .restart local v11       #header:[B
    :catch_2e2
    move-exception v7

    #@2e3
    .line 2551
    .local v7, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_2e3
    const-string v21, "BackupManagerService"

    #@2e5
    const-string v22, "Unable to find shared-storage backup handler"

    #@2e7
    invoke-static/range {v21 .. v22}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2ea
    .catchall {:try_start_2e3 .. :try_end_2ea} :catchall_430
    .catch Landroid/os/RemoteException; {:try_start_2e3 .. :try_end_2ea} :catch_2ec
    .catch Ljava/lang/Exception; {:try_start_2e3 .. :try_end_2ea} :catch_3c1

    #@2ea
    goto/16 :goto_248

    #@2ec
    .line 2564
    .end local v5           #compressing:Z
    .end local v7           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v8           #encrypting:Z
    .end local v9           #finalOutput:Ljava/io/OutputStream;
    .end local v11           #header:[B
    .end local v12           #headerbuf:Ljava/lang/StringBuilder;
    :catch_2ec
    move-exception v7

    #@2ed
    .line 2565
    .local v7, e:Landroid/os/RemoteException;
    :try_start_2ed
    const-string v21, "BackupManagerService"

    #@2ef
    const-string v22, "App died during full backup"

    #@2f1
    invoke-static/range {v21 .. v22}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2f4
    .catchall {:try_start_2ed .. :try_end_2f4} :catchall_430

    #@2f4
    .line 2569
    move-object/from16 v0, p0

    #@2f6
    move-object/from16 v1, v19

    #@2f8
    invoke-direct {v0, v1}, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->tearDown(Landroid/content/pm/PackageInfo;)V

    #@2fb
    .line 2571
    if-eqz v17, :cond_300

    #@2fd
    :try_start_2fd
    invoke-virtual/range {v17 .. v17}, Ljava/io/OutputStream;->close()V

    #@300
    .line 2572
    :cond_300
    move-object/from16 v0, p0

    #@302
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mOutputFile:Landroid/os/ParcelFileDescriptor;

    #@304
    move-object/from16 v21, v0

    #@306
    invoke-virtual/range {v21 .. v21}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_309
    .catch Ljava/io/IOException; {:try_start_2fd .. :try_end_309} :catch_4c2

    #@309
    .line 2576
    :goto_309
    move-object/from16 v0, p0

    #@30b
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@30d
    move-object/from16 v21, v0

    #@30f
    move-object/from16 v0, v21

    #@311
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mCurrentOpLock:Ljava/lang/Object;

    #@313
    move-object/from16 v22, v0

    #@315
    monitor-enter v22

    #@316
    .line 2577
    :try_start_316
    move-object/from16 v0, p0

    #@318
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@31a
    move-object/from16 v21, v0

    #@31c
    move-object/from16 v0, v21

    #@31e
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mCurrentOperations:Landroid/util/SparseArray;

    #@320
    move-object/from16 v21, v0

    #@322
    invoke-virtual/range {v21 .. v21}, Landroid/util/SparseArray;->clear()V

    #@325
    .line 2578
    monitor-exit v22
    :try_end_326
    .catchall {:try_start_316 .. :try_end_326} :catchall_498

    #@326
    .line 2579
    move-object/from16 v0, p0

    #@328
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mLatchObject:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@32a
    move-object/from16 v22, v0

    #@32c
    monitor-enter v22

    #@32d
    .line 2580
    :try_start_32d
    move-object/from16 v0, p0

    #@32f
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mLatchObject:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@331
    move-object/from16 v21, v0

    #@333
    const/16 v23, 0x1

    #@335
    move-object/from16 v0, v21

    #@337
    move/from16 v1, v23

    #@339
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    #@33c
    .line 2581
    move-object/from16 v0, p0

    #@33e
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mLatchObject:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@340
    move-object/from16 v21, v0

    #@342
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Object;->notifyAll()V

    #@345
    .line 2582
    monitor-exit v22
    :try_end_346
    .catchall {:try_start_32d .. :try_end_346} :catchall_49b

    #@346
    .line 2583
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->sendEndBackup()V

    #@349
    .line 2585
    move-object/from16 v0, p0

    #@34b
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@34d
    move-object/from16 v21, v0

    #@34f
    move-object/from16 v0, v21

    #@351
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mWakelock:Landroid/os/PowerManager$WakeLock;

    #@353
    move-object/from16 v21, v0

    #@355
    goto/16 :goto_1d6

    #@357
    .line 2563
    .end local v7           #e:Landroid/os/RemoteException;
    .restart local v3       #N:I
    .restart local v5       #compressing:Z
    .restart local v8       #encrypting:Z
    .restart local v9       #finalOutput:Ljava/io/OutputStream;
    .restart local v11       #header:[B
    .restart local v12       #headerbuf:Ljava/lang/StringBuilder;
    :cond_357
    :try_start_357
    move-object/from16 v0, p0

    #@359
    move-object/from16 v1, v17

    #@35b
    invoke-direct {v0, v1}, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->finalizeBackup(Ljava/io/OutputStream;)V
    :try_end_35e
    .catchall {:try_start_357 .. :try_end_35e} :catchall_430
    .catch Landroid/os/RemoteException; {:try_start_357 .. :try_end_35e} :catch_2ec
    .catch Ljava/lang/Exception; {:try_start_357 .. :try_end_35e} :catch_3c1

    #@35e
    .line 2569
    move-object/from16 v0, p0

    #@360
    move-object/from16 v1, v19

    #@362
    invoke-direct {v0, v1}, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->tearDown(Landroid/content/pm/PackageInfo;)V

    #@365
    .line 2571
    if-eqz v17, :cond_36a

    #@367
    :try_start_367
    invoke-virtual/range {v17 .. v17}, Ljava/io/OutputStream;->close()V

    #@36a
    .line 2572
    :cond_36a
    move-object/from16 v0, p0

    #@36c
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mOutputFile:Landroid/os/ParcelFileDescriptor;

    #@36e
    move-object/from16 v21, v0

    #@370
    invoke-virtual/range {v21 .. v21}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_373
    .catch Ljava/io/IOException; {:try_start_367 .. :try_end_373} :catch_4b6

    #@373
    .line 2576
    :goto_373
    move-object/from16 v0, p0

    #@375
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@377
    move-object/from16 v21, v0

    #@379
    move-object/from16 v0, v21

    #@37b
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mCurrentOpLock:Ljava/lang/Object;

    #@37d
    move-object/from16 v22, v0

    #@37f
    monitor-enter v22

    #@380
    .line 2577
    :try_start_380
    move-object/from16 v0, p0

    #@382
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@384
    move-object/from16 v21, v0

    #@386
    move-object/from16 v0, v21

    #@388
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mCurrentOperations:Landroid/util/SparseArray;

    #@38a
    move-object/from16 v21, v0

    #@38c
    invoke-virtual/range {v21 .. v21}, Landroid/util/SparseArray;->clear()V

    #@38f
    .line 2578
    monitor-exit v22
    :try_end_390
    .catchall {:try_start_380 .. :try_end_390} :catchall_4b0

    #@390
    .line 2579
    move-object/from16 v0, p0

    #@392
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mLatchObject:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@394
    move-object/from16 v22, v0

    #@396
    monitor-enter v22

    #@397
    .line 2580
    :try_start_397
    move-object/from16 v0, p0

    #@399
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mLatchObject:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@39b
    move-object/from16 v21, v0

    #@39d
    const/16 v23, 0x1

    #@39f
    move-object/from16 v0, v21

    #@3a1
    move/from16 v1, v23

    #@3a3
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    #@3a6
    .line 2581
    move-object/from16 v0, p0

    #@3a8
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mLatchObject:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@3aa
    move-object/from16 v21, v0

    #@3ac
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Object;->notifyAll()V

    #@3af
    .line 2582
    monitor-exit v22
    :try_end_3b0
    .catchall {:try_start_397 .. :try_end_3b0} :catchall_4b3

    #@3b0
    .line 2583
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->sendEndBackup()V

    #@3b3
    .line 2585
    move-object/from16 v0, p0

    #@3b5
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@3b7
    move-object/from16 v21, v0

    #@3b9
    move-object/from16 v0, v21

    #@3bb
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mWakelock:Landroid/os/PowerManager$WakeLock;

    #@3bd
    move-object/from16 v21, v0

    #@3bf
    goto/16 :goto_1d6

    #@3c1
    .line 2566
    .end local v3           #N:I
    .end local v5           #compressing:Z
    .end local v8           #encrypting:Z
    .end local v9           #finalOutput:Ljava/io/OutputStream;
    .end local v11           #header:[B
    .end local v12           #headerbuf:Ljava/lang/StringBuilder;
    :catch_3c1
    move-exception v7

    #@3c2
    .line 2567
    .local v7, e:Ljava/lang/Exception;
    :try_start_3c2
    const-string v21, "BackupManagerService"

    #@3c4
    const-string v22, "Internal exception during full backup"

    #@3c6
    move-object/from16 v0, v21

    #@3c8
    move-object/from16 v1, v22

    #@3ca
    invoke-static {v0, v1, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3cd
    .catchall {:try_start_3c2 .. :try_end_3cd} :catchall_430

    #@3cd
    .line 2569
    move-object/from16 v0, p0

    #@3cf
    move-object/from16 v1, v19

    #@3d1
    invoke-direct {v0, v1}, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->tearDown(Landroid/content/pm/PackageInfo;)V

    #@3d4
    .line 2571
    if-eqz v17, :cond_3d9

    #@3d6
    :try_start_3d6
    invoke-virtual/range {v17 .. v17}, Ljava/io/OutputStream;->close()V

    #@3d9
    .line 2572
    :cond_3d9
    move-object/from16 v0, p0

    #@3db
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mOutputFile:Landroid/os/ParcelFileDescriptor;

    #@3dd
    move-object/from16 v21, v0

    #@3df
    invoke-virtual/range {v21 .. v21}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_3e2
    .catch Ljava/io/IOException; {:try_start_3d6 .. :try_end_3e2} :catch_4bf

    #@3e2
    .line 2576
    :goto_3e2
    move-object/from16 v0, p0

    #@3e4
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@3e6
    move-object/from16 v21, v0

    #@3e8
    move-object/from16 v0, v21

    #@3ea
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mCurrentOpLock:Ljava/lang/Object;

    #@3ec
    move-object/from16 v22, v0

    #@3ee
    monitor-enter v22

    #@3ef
    .line 2577
    :try_start_3ef
    move-object/from16 v0, p0

    #@3f1
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@3f3
    move-object/from16 v21, v0

    #@3f5
    move-object/from16 v0, v21

    #@3f7
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mCurrentOperations:Landroid/util/SparseArray;

    #@3f9
    move-object/from16 v21, v0

    #@3fb
    invoke-virtual/range {v21 .. v21}, Landroid/util/SparseArray;->clear()V

    #@3fe
    .line 2578
    monitor-exit v22
    :try_end_3ff
    .catchall {:try_start_3ef .. :try_end_3ff} :catchall_49e

    #@3ff
    .line 2579
    move-object/from16 v0, p0

    #@401
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mLatchObject:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@403
    move-object/from16 v22, v0

    #@405
    monitor-enter v22

    #@406
    .line 2580
    :try_start_406
    move-object/from16 v0, p0

    #@408
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mLatchObject:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@40a
    move-object/from16 v21, v0

    #@40c
    const/16 v23, 0x1

    #@40e
    move-object/from16 v0, v21

    #@410
    move/from16 v1, v23

    #@412
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    #@415
    .line 2581
    move-object/from16 v0, p0

    #@417
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mLatchObject:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@419
    move-object/from16 v21, v0

    #@41b
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Object;->notifyAll()V

    #@41e
    .line 2582
    monitor-exit v22
    :try_end_41f
    .catchall {:try_start_406 .. :try_end_41f} :catchall_4a1

    #@41f
    .line 2583
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->sendEndBackup()V

    #@422
    .line 2585
    move-object/from16 v0, p0

    #@424
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@426
    move-object/from16 v21, v0

    #@428
    move-object/from16 v0, v21

    #@42a
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mWakelock:Landroid/os/PowerManager$WakeLock;

    #@42c
    move-object/from16 v21, v0

    #@42e
    goto/16 :goto_1d6

    #@430
    .line 2569
    .end local v7           #e:Ljava/lang/Exception;
    :catchall_430
    move-exception v21

    #@431
    move-object/from16 v0, p0

    #@433
    move-object/from16 v1, v19

    #@435
    invoke-direct {v0, v1}, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->tearDown(Landroid/content/pm/PackageInfo;)V

    #@438
    .line 2571
    if-eqz v17, :cond_43d

    #@43a
    :try_start_43a
    invoke-virtual/range {v17 .. v17}, Ljava/io/OutputStream;->close()V

    #@43d
    .line 2572
    :cond_43d
    move-object/from16 v0, p0

    #@43f
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mOutputFile:Landroid/os/ParcelFileDescriptor;

    #@441
    move-object/from16 v22, v0

    #@443
    invoke-virtual/range {v22 .. v22}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_446
    .catch Ljava/io/IOException; {:try_start_43a .. :try_end_446} :catch_4c5

    #@446
    .line 2576
    :goto_446
    move-object/from16 v0, p0

    #@448
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@44a
    move-object/from16 v22, v0

    #@44c
    move-object/from16 v0, v22

    #@44e
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mCurrentOpLock:Ljava/lang/Object;

    #@450
    move-object/from16 v22, v0

    #@452
    monitor-enter v22

    #@453
    .line 2577
    :try_start_453
    move-object/from16 v0, p0

    #@455
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@457
    move-object/from16 v23, v0

    #@459
    move-object/from16 v0, v23

    #@45b
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mCurrentOperations:Landroid/util/SparseArray;

    #@45d
    move-object/from16 v23, v0

    #@45f
    invoke-virtual/range {v23 .. v23}, Landroid/util/SparseArray;->clear()V

    #@462
    .line 2578
    monitor-exit v22
    :try_end_463
    .catchall {:try_start_453 .. :try_end_463} :catchall_492

    #@463
    .line 2579
    move-object/from16 v0, p0

    #@465
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mLatchObject:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@467
    move-object/from16 v22, v0

    #@469
    monitor-enter v22

    #@46a
    .line 2580
    :try_start_46a
    move-object/from16 v0, p0

    #@46c
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mLatchObject:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@46e
    move-object/from16 v23, v0

    #@470
    const/16 v24, 0x1

    #@472
    invoke-virtual/range {v23 .. v24}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    #@475
    .line 2581
    move-object/from16 v0, p0

    #@477
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mLatchObject:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@479
    move-object/from16 v23, v0

    #@47b
    invoke-virtual/range {v23 .. v23}, Ljava/lang/Object;->notifyAll()V

    #@47e
    .line 2582
    monitor-exit v22
    :try_end_47f
    .catchall {:try_start_46a .. :try_end_47f} :catchall_495

    #@47f
    .line 2583
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->sendEndBackup()V

    #@482
    .line 2585
    move-object/from16 v0, p0

    #@484
    iget-object v0, v0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->this$0:Lcom/android/server/BackupManagerService;

    #@486
    move-object/from16 v22, v0

    #@488
    move-object/from16 v0, v22

    #@48a
    iget-object v0, v0, Lcom/android/server/BackupManagerService;->mWakelock:Landroid/os/PowerManager$WakeLock;

    #@48c
    move-object/from16 v22, v0

    #@48e
    invoke-virtual/range {v22 .. v22}, Landroid/os/PowerManager$WakeLock;->release()V

    #@491
    .line 2569
    throw v21

    #@492
    .line 2578
    :catchall_492
    move-exception v21

    #@493
    :try_start_493
    monitor-exit v22
    :try_end_494
    .catchall {:try_start_493 .. :try_end_494} :catchall_492

    #@494
    throw v21

    #@495
    .line 2582
    :catchall_495
    move-exception v21

    #@496
    :try_start_496
    monitor-exit v22
    :try_end_497
    .catchall {:try_start_496 .. :try_end_497} :catchall_495

    #@497
    throw v21

    #@498
    .line 2578
    .local v7, e:Landroid/os/RemoteException;
    :catchall_498
    move-exception v21

    #@499
    :try_start_499
    monitor-exit v22
    :try_end_49a
    .catchall {:try_start_499 .. :try_end_49a} :catchall_498

    #@49a
    throw v21

    #@49b
    .line 2582
    :catchall_49b
    move-exception v21

    #@49c
    :try_start_49c
    monitor-exit v22
    :try_end_49d
    .catchall {:try_start_49c .. :try_end_49d} :catchall_49b

    #@49d
    throw v21

    #@49e
    .line 2578
    .local v7, e:Ljava/lang/Exception;
    :catchall_49e
    move-exception v21

    #@49f
    :try_start_49f
    monitor-exit v22
    :try_end_4a0
    .catchall {:try_start_49f .. :try_end_4a0} :catchall_49e

    #@4a0
    throw v21

    #@4a1
    .line 2582
    :catchall_4a1
    move-exception v21

    #@4a2
    :try_start_4a2
    monitor-exit v22
    :try_end_4a3
    .catchall {:try_start_4a2 .. :try_end_4a3} :catchall_4a1

    #@4a3
    throw v21

    #@4a4
    .line 2578
    .end local v7           #e:Ljava/lang/Exception;
    .restart local v5       #compressing:Z
    .restart local v8       #encrypting:Z
    .restart local v9       #finalOutput:Ljava/io/OutputStream;
    :catchall_4a4
    move-exception v21

    #@4a5
    :try_start_4a5
    monitor-exit v22
    :try_end_4a6
    .catchall {:try_start_4a5 .. :try_end_4a6} :catchall_4a4

    #@4a6
    throw v21

    #@4a7
    .line 2582
    :catchall_4a7
    move-exception v21

    #@4a8
    :try_start_4a8
    monitor-exit v22
    :try_end_4a9
    .catchall {:try_start_4a8 .. :try_end_4a9} :catchall_4a7

    #@4a9
    throw v21

    #@4aa
    .line 2578
    .restart local v7       #e:Ljava/lang/Exception;
    .restart local v12       #headerbuf:Ljava/lang/StringBuilder;
    :catchall_4aa
    move-exception v21

    #@4ab
    :try_start_4ab
    monitor-exit v22
    :try_end_4ac
    .catchall {:try_start_4ab .. :try_end_4ac} :catchall_4aa

    #@4ac
    throw v21

    #@4ad
    .line 2582
    :catchall_4ad
    move-exception v21

    #@4ae
    :try_start_4ae
    monitor-exit v22
    :try_end_4af
    .catchall {:try_start_4ae .. :try_end_4af} :catchall_4ad

    #@4af
    throw v21

    #@4b0
    .line 2578
    .end local v7           #e:Ljava/lang/Exception;
    .restart local v3       #N:I
    .restart local v11       #header:[B
    :catchall_4b0
    move-exception v21

    #@4b1
    :try_start_4b1
    monitor-exit v22
    :try_end_4b2
    .catchall {:try_start_4b1 .. :try_end_4b2} :catchall_4b0

    #@4b2
    throw v21

    #@4b3
    .line 2582
    :catchall_4b3
    move-exception v21

    #@4b4
    :try_start_4b4
    monitor-exit v22
    :try_end_4b5
    .catchall {:try_start_4b4 .. :try_end_4b5} :catchall_4b3

    #@4b5
    throw v21

    #@4b6
    .line 2573
    :catch_4b6
    move-exception v21

    #@4b7
    goto/16 :goto_373

    #@4b9
    .end local v3           #N:I
    .end local v11           #header:[B
    .restart local v7       #e:Ljava/lang/Exception;
    :catch_4b9
    move-exception v21

    #@4ba
    goto/16 :goto_294

    #@4bc
    .end local v7           #e:Ljava/lang/Exception;
    .end local v12           #headerbuf:Ljava/lang/StringBuilder;
    :catch_4bc
    move-exception v21

    #@4bd
    goto/16 :goto_18a

    #@4bf
    .end local v5           #compressing:Z
    .end local v8           #encrypting:Z
    .end local v9           #finalOutput:Ljava/io/OutputStream;
    .restart local v7       #e:Ljava/lang/Exception;
    :catch_4bf
    move-exception v21

    #@4c0
    goto/16 :goto_3e2

    #@4c2
    .local v7, e:Landroid/os/RemoteException;
    :catch_4c2
    move-exception v21

    #@4c3
    goto/16 :goto_309

    #@4c5
    .end local v7           #e:Landroid/os/RemoteException;
    :catch_4c5
    move-exception v22

    #@4c6
    goto :goto_446

    #@4c7
    .line 2539
    .restart local v5       #compressing:Z
    .restart local v8       #encrypting:Z
    .restart local v10       #finalOutput:Ljava/io/OutputStream;
    .restart local v12       #headerbuf:Ljava/lang/StringBuilder;
    :catch_4c7
    move-exception v7

    #@4c8
    move-object v9, v10

    #@4c9
    .end local v10           #finalOutput:Ljava/io/OutputStream;
    .restart local v9       #finalOutput:Ljava/io/OutputStream;
    goto/16 :goto_274

    #@4cb
    .end local v9           #finalOutput:Ljava/io/OutputStream;
    .restart local v10       #finalOutput:Ljava/io/OutputStream;
    .restart local v11       #header:[B
    :cond_4cb
    move-object v9, v10

    #@4cc
    .end local v10           #finalOutput:Ljava/io/OutputStream;
    .restart local v9       #finalOutput:Ljava/io/OutputStream;
    goto/16 :goto_229
.end method

.method sendEndBackup()V
    .registers 4

    #@0
    .prologue
    .line 2859
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mObserver:Landroid/app/backup/IFullBackupRestoreObserver;

    #@2
    if-eqz v1, :cond_9

    #@4
    .line 2861
    :try_start_4
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mObserver:Landroid/app/backup/IFullBackupRestoreObserver;

    #@6
    invoke-interface {v1}, Landroid/app/backup/IFullBackupRestoreObserver;->onEndBackup()V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_a

    #@9
    .line 2867
    :cond_9
    :goto_9
    return-void

    #@a
    .line 2862
    :catch_a
    move-exception v0

    #@b
    .line 2863
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BackupManagerService"

    #@d
    const-string v2, "full backup observer went away: endBackup"

    #@f
    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@12
    .line 2864
    const/4 v1, 0x0

    #@13
    iput-object v1, p0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mObserver:Landroid/app/backup/IFullBackupRestoreObserver;

    #@15
    goto :goto_9
.end method

.method sendOnBackupPackage(Ljava/lang/String;)V
    .registers 5
    .parameter "name"

    #@0
    .prologue
    .line 2847
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mObserver:Landroid/app/backup/IFullBackupRestoreObserver;

    #@2
    if-eqz v1, :cond_9

    #@4
    .line 2850
    :try_start_4
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mObserver:Landroid/app/backup/IFullBackupRestoreObserver;

    #@6
    invoke-interface {v1, p1}, Landroid/app/backup/IFullBackupRestoreObserver;->onBackupPackage(Ljava/lang/String;)V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_a

    #@9
    .line 2856
    :cond_9
    :goto_9
    return-void

    #@a
    .line 2851
    :catch_a
    move-exception v0

    #@b
    .line 2852
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BackupManagerService"

    #@d
    const-string v2, "full backup observer went away: backupPackage"

    #@f
    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@12
    .line 2853
    const/4 v1, 0x0

    #@13
    iput-object v1, p0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mObserver:Landroid/app/backup/IFullBackupRestoreObserver;

    #@15
    goto :goto_9
.end method

.method sendStartBackup()V
    .registers 4

    #@0
    .prologue
    .line 2836
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mObserver:Landroid/app/backup/IFullBackupRestoreObserver;

    #@2
    if-eqz v1, :cond_9

    #@4
    .line 2838
    :try_start_4
    iget-object v1, p0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mObserver:Landroid/app/backup/IFullBackupRestoreObserver;

    #@6
    invoke-interface {v1}, Landroid/app/backup/IFullBackupRestoreObserver;->onStartBackup()V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_a

    #@9
    .line 2844
    :cond_9
    :goto_9
    return-void

    #@a
    .line 2839
    :catch_a
    move-exception v0

    #@b
    .line 2840
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BackupManagerService"

    #@d
    const-string v2, "full backup observer went away: startBackup"

    #@f
    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@12
    .line 2841
    const/4 v1, 0x0

    #@13
    iput-object v1, p0, Lcom/android/server/BackupManagerService$PerformFullBackupTask;->mObserver:Landroid/app/backup/IFullBackupRestoreObserver;

    #@15
    goto :goto_9
.end method
