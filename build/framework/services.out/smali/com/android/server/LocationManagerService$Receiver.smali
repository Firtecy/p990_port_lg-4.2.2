.class final Lcom/android/server/LocationManagerService$Receiver;
.super Ljava/lang/Object;
.source "LocationManagerService.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;
.implements Landroid/app/PendingIntent$OnFinished;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/LocationManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "Receiver"
.end annotation


# instance fields
.field final mAllowedResolutionLevel:I

.field final mKey:Ljava/lang/Object;

.field final mListener:Landroid/location/ILocationListener;

.field final mPackageName:Ljava/lang/String;

.field mPendingBroadcasts:I

.field final mPendingIntent:Landroid/app/PendingIntent;

.field final mPid:I

.field final mUid:I

.field final mUpdateRecords:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/server/LocationManagerService$UpdateRecord;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/android/server/LocationManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/LocationManagerService;Landroid/location/ILocationListener;Landroid/app/PendingIntent;IILjava/lang/String;)V
    .registers 8
    .parameter
    .parameter "listener"
    .parameter "intent"
    .parameter "pid"
    .parameter "uid"
    .parameter "packageName"

    #@0
    .prologue
    .line 466
    iput-object p1, p0, Lcom/android/server/LocationManagerService$Receiver;->this$0:Lcom/android/server/LocationManagerService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 461
    new-instance v0, Ljava/util/HashMap;

    #@7
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@a
    iput-object v0, p0, Lcom/android/server/LocationManagerService$Receiver;->mUpdateRecords:Ljava/util/HashMap;

    #@c
    .line 467
    iput-object p2, p0, Lcom/android/server/LocationManagerService$Receiver;->mListener:Landroid/location/ILocationListener;

    #@e
    .line 468
    iput-object p3, p0, Lcom/android/server/LocationManagerService$Receiver;->mPendingIntent:Landroid/app/PendingIntent;

    #@10
    .line 469
    if-eqz p2, :cond_25

    #@12
    .line 470
    invoke-interface {p2}, Landroid/location/ILocationListener;->asBinder()Landroid/os/IBinder;

    #@15
    move-result-object v0

    #@16
    iput-object v0, p0, Lcom/android/server/LocationManagerService$Receiver;->mKey:Ljava/lang/Object;

    #@18
    .line 474
    :goto_18
    invoke-static {p1, p4, p5}, Lcom/android/server/LocationManagerService;->access$400(Lcom/android/server/LocationManagerService;II)I

    #@1b
    move-result v0

    #@1c
    iput v0, p0, Lcom/android/server/LocationManagerService$Receiver;->mAllowedResolutionLevel:I

    #@1e
    .line 475
    iput p5, p0, Lcom/android/server/LocationManagerService$Receiver;->mUid:I

    #@20
    .line 476
    iput p4, p0, Lcom/android/server/LocationManagerService$Receiver;->mPid:I

    #@22
    .line 477
    iput-object p6, p0, Lcom/android/server/LocationManagerService$Receiver;->mPackageName:Ljava/lang/String;

    #@24
    .line 478
    return-void

    #@25
    .line 472
    :cond_25
    iput-object p3, p0, Lcom/android/server/LocationManagerService$Receiver;->mKey:Ljava/lang/Object;

    #@27
    goto :goto_18
.end method

.method static synthetic access$1100(Lcom/android/server/LocationManagerService$Receiver;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 451
    invoke-direct {p0}, Lcom/android/server/LocationManagerService$Receiver;->decrementPendingBroadcastsLocked()V

    #@3
    return-void
.end method

.method private decrementPendingBroadcastsLocked()V
    .registers 2

    #@0
    .prologue
    .line 664
    iget v0, p0, Lcom/android/server/LocationManagerService$Receiver;->mPendingBroadcasts:I

    #@2
    add-int/lit8 v0, v0, -0x1

    #@4
    iput v0, p0, Lcom/android/server/LocationManagerService$Receiver;->mPendingBroadcasts:I

    #@6
    if-nez v0, :cond_d

    #@8
    .line 665
    iget-object v0, p0, Lcom/android/server/LocationManagerService$Receiver;->this$0:Lcom/android/server/LocationManagerService;

    #@a
    invoke-static {v0}, Lcom/android/server/LocationManagerService;->access$900(Lcom/android/server/LocationManagerService;)V

    #@d
    .line 667
    :cond_d
    return-void
.end method

.method private incrementPendingBroadcastsLocked()V
    .registers 3

    #@0
    .prologue
    .line 658
    iget v0, p0, Lcom/android/server/LocationManagerService$Receiver;->mPendingBroadcasts:I

    #@2
    add-int/lit8 v1, v0, 0x1

    #@4
    iput v1, p0, Lcom/android/server/LocationManagerService$Receiver;->mPendingBroadcasts:I

    #@6
    if-nez v0, :cond_d

    #@8
    .line 659
    iget-object v0, p0, Lcom/android/server/LocationManagerService$Receiver;->this$0:Lcom/android/server/LocationManagerService;

    #@a
    invoke-static {v0}, Lcom/android/server/LocationManagerService;->access$1000(Lcom/android/server/LocationManagerService;)V

    #@d
    .line 661
    :cond_d
    return-void
.end method


# virtual methods
.method public binderDied()V
    .registers 3

    #@0
    .prologue
    .line 634
    const-string v0, "LocationManagerService"

    #@2
    const-string v1, "Location listener died"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 636
    iget-object v0, p0, Lcom/android/server/LocationManagerService$Receiver;->this$0:Lcom/android/server/LocationManagerService;

    #@9
    invoke-static {v0}, Lcom/android/server/LocationManagerService;->access$100(Lcom/android/server/LocationManagerService;)Ljava/lang/Object;

    #@c
    move-result-object v1

    #@d
    monitor-enter v1

    #@e
    .line 637
    :try_start_e
    iget-object v0, p0, Lcom/android/server/LocationManagerService$Receiver;->this$0:Lcom/android/server/LocationManagerService;

    #@10
    invoke-static {v0, p0}, Lcom/android/server/LocationManagerService;->access$800(Lcom/android/server/LocationManagerService;Lcom/android/server/LocationManagerService$Receiver;)V

    #@13
    .line 638
    monitor-exit v1
    :try_end_14
    .catchall {:try_start_e .. :try_end_14} :catchall_23

    #@14
    .line 639
    monitor-enter p0

    #@15
    .line 640
    :try_start_15
    iget v0, p0, Lcom/android/server/LocationManagerService$Receiver;->mPendingBroadcasts:I

    #@17
    if-lez v0, :cond_21

    #@19
    .line 641
    iget-object v0, p0, Lcom/android/server/LocationManagerService$Receiver;->this$0:Lcom/android/server/LocationManagerService;

    #@1b
    invoke-static {v0}, Lcom/android/server/LocationManagerService;->access$900(Lcom/android/server/LocationManagerService;)V

    #@1e
    .line 642
    const/4 v0, 0x0

    #@1f
    iput v0, p0, Lcom/android/server/LocationManagerService$Receiver;->mPendingBroadcasts:I

    #@21
    .line 644
    :cond_21
    monitor-exit p0
    :try_end_22
    .catchall {:try_start_15 .. :try_end_22} :catchall_26

    #@22
    .line 645
    return-void

    #@23
    .line 638
    :catchall_23
    move-exception v0

    #@24
    :try_start_24
    monitor-exit v1
    :try_end_25
    .catchall {:try_start_24 .. :try_end_25} :catchall_23

    #@25
    throw v0

    #@26
    .line 644
    :catchall_26
    move-exception v0

    #@27
    :try_start_27
    monitor-exit p0
    :try_end_28
    .catchall {:try_start_27 .. :try_end_28} :catchall_26

    #@28
    throw v0
.end method

.method public callLocationChangedLocked(Landroid/location/Location;)Z
    .registers 11
    .parameter "location"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 561
    iget-object v0, p0, Lcom/android/server/LocationManagerService$Receiver;->mListener:Landroid/location/ILocationListener;

    #@3
    if-eqz v0, :cond_1c

    #@5
    .line 563
    :try_start_5
    monitor-enter p0
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_6} :catch_19

    #@6
    .line 566
    :try_start_6
    iget-object v0, p0, Lcom/android/server/LocationManagerService$Receiver;->mListener:Landroid/location/ILocationListener;

    #@8
    new-instance v1, Landroid/location/Location;

    #@a
    invoke-direct {v1, p1}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    #@d
    invoke-interface {v0, v1}, Landroid/location/ILocationListener;->onLocationChanged(Landroid/location/Location;)V

    #@10
    .line 569
    invoke-direct {p0}, Lcom/android/server/LocationManagerService$Receiver;->incrementPendingBroadcastsLocked()V

    #@13
    .line 570
    monitor-exit p0

    #@14
    .line 591
    :goto_14
    const/4 v0, 0x1

    #@15
    :goto_15
    return v0

    #@16
    .line 570
    :catchall_16
    move-exception v0

    #@17
    monitor-exit p0
    :try_end_18
    .catchall {:try_start_6 .. :try_end_18} :catchall_16

    #@18
    :try_start_18
    throw v0
    :try_end_19
    .catch Landroid/os/RemoteException; {:try_start_18 .. :try_end_19} :catch_19

    #@19
    .line 571
    :catch_19
    move-exception v7

    #@1a
    .local v7, e:Landroid/os/RemoteException;
    move v0, v8

    #@1b
    .line 572
    goto :goto_15

    #@1c
    .line 575
    .end local v7           #e:Landroid/os/RemoteException;
    :cond_1c
    new-instance v3, Landroid/content/Intent;

    #@1e
    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    #@21
    .line 576
    .local v3, locationChanged:Landroid/content/Intent;
    const-string v0, "location"

    #@23
    new-instance v1, Landroid/location/Location;

    #@25
    invoke-direct {v1, p1}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    #@28
    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@2b
    .line 578
    :try_start_2b
    monitor-enter p0
    :try_end_2c
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_2b .. :try_end_2c} :catch_4f

    #@2c
    .line 581
    :try_start_2c
    iget-object v0, p0, Lcom/android/server/LocationManagerService$Receiver;->mPendingIntent:Landroid/app/PendingIntent;

    #@2e
    iget-object v1, p0, Lcom/android/server/LocationManagerService$Receiver;->this$0:Lcom/android/server/LocationManagerService;

    #@30
    invoke-static {v1}, Lcom/android/server/LocationManagerService;->access$500(Lcom/android/server/LocationManagerService;)Landroid/content/Context;

    #@33
    move-result-object v1

    #@34
    const/4 v2, 0x0

    #@35
    iget-object v4, p0, Lcom/android/server/LocationManagerService$Receiver;->this$0:Lcom/android/server/LocationManagerService;

    #@37
    invoke-static {v4}, Lcom/android/server/LocationManagerService;->access$600(Lcom/android/server/LocationManagerService;)Lcom/android/server/LocationManagerService$LocationWorkerHandler;

    #@3a
    move-result-object v5

    #@3b
    iget-object v4, p0, Lcom/android/server/LocationManagerService$Receiver;->this$0:Lcom/android/server/LocationManagerService;

    #@3d
    iget v6, p0, Lcom/android/server/LocationManagerService$Receiver;->mAllowedResolutionLevel:I

    #@3f
    invoke-static {v4, v6}, Lcom/android/server/LocationManagerService;->access$700(Lcom/android/server/LocationManagerService;I)Ljava/lang/String;

    #@42
    move-result-object v6

    #@43
    move-object v4, p0

    #@44
    invoke-virtual/range {v0 .. v6}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;Landroid/app/PendingIntent$OnFinished;Landroid/os/Handler;Ljava/lang/String;)V

    #@47
    .line 585
    invoke-direct {p0}, Lcom/android/server/LocationManagerService$Receiver;->incrementPendingBroadcastsLocked()V

    #@4a
    .line 586
    monitor-exit p0

    #@4b
    goto :goto_14

    #@4c
    :catchall_4c
    move-exception v0

    #@4d
    monitor-exit p0
    :try_end_4e
    .catchall {:try_start_2c .. :try_end_4e} :catchall_4c

    #@4e
    :try_start_4e
    throw v0
    :try_end_4f
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_4e .. :try_end_4f} :catch_4f

    #@4f
    .line 587
    :catch_4f
    move-exception v7

    #@50
    .local v7, e:Landroid/app/PendingIntent$CanceledException;
    move v0, v8

    #@51
    .line 588
    goto :goto_15
.end method

.method public callProviderEnabledLocked(Ljava/lang/String;Z)Z
    .registers 12
    .parameter "provider"
    .parameter "enabled"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 595
    iget-object v0, p0, Lcom/android/server/LocationManagerService$Receiver;->mListener:Landroid/location/ILocationListener;

    #@3
    if-eqz v0, :cond_1f

    #@5
    .line 597
    :try_start_5
    monitor-enter p0
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_6} :catch_1c

    #@6
    .line 600
    if-eqz p2, :cond_13

    #@8
    .line 601
    :try_start_8
    iget-object v0, p0, Lcom/android/server/LocationManagerService$Receiver;->mListener:Landroid/location/ILocationListener;

    #@a
    invoke-interface {v0, p1}, Landroid/location/ILocationListener;->onProviderEnabled(Ljava/lang/String;)V

    #@d
    .line 607
    :goto_d
    invoke-direct {p0}, Lcom/android/server/LocationManagerService$Receiver;->incrementPendingBroadcastsLocked()V

    #@10
    .line 608
    monitor-exit p0

    #@11
    .line 629
    :goto_11
    const/4 v0, 0x1

    #@12
    :goto_12
    return v0

    #@13
    .line 603
    :cond_13
    iget-object v0, p0, Lcom/android/server/LocationManagerService$Receiver;->mListener:Landroid/location/ILocationListener;

    #@15
    invoke-interface {v0, p1}, Landroid/location/ILocationListener;->onProviderDisabled(Ljava/lang/String;)V

    #@18
    goto :goto_d

    #@19
    .line 608
    :catchall_19
    move-exception v0

    #@1a
    monitor-exit p0
    :try_end_1b
    .catchall {:try_start_8 .. :try_end_1b} :catchall_19

    #@1b
    :try_start_1b
    throw v0
    :try_end_1c
    .catch Landroid/os/RemoteException; {:try_start_1b .. :try_end_1c} :catch_1c

    #@1c
    .line 609
    :catch_1c
    move-exception v7

    #@1d
    .local v7, e:Landroid/os/RemoteException;
    move v0, v8

    #@1e
    .line 610
    goto :goto_12

    #@1f
    .line 613
    .end local v7           #e:Landroid/os/RemoteException;
    :cond_1f
    new-instance v3, Landroid/content/Intent;

    #@21
    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    #@24
    .line 614
    .local v3, providerIntent:Landroid/content/Intent;
    const-string v0, "providerEnabled"

    #@26
    invoke-virtual {v3, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@29
    .line 616
    :try_start_29
    monitor-enter p0
    :try_end_2a
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_29 .. :try_end_2a} :catch_4d

    #@2a
    .line 619
    :try_start_2a
    iget-object v0, p0, Lcom/android/server/LocationManagerService$Receiver;->mPendingIntent:Landroid/app/PendingIntent;

    #@2c
    iget-object v1, p0, Lcom/android/server/LocationManagerService$Receiver;->this$0:Lcom/android/server/LocationManagerService;

    #@2e
    invoke-static {v1}, Lcom/android/server/LocationManagerService;->access$500(Lcom/android/server/LocationManagerService;)Landroid/content/Context;

    #@31
    move-result-object v1

    #@32
    const/4 v2, 0x0

    #@33
    iget-object v4, p0, Lcom/android/server/LocationManagerService$Receiver;->this$0:Lcom/android/server/LocationManagerService;

    #@35
    invoke-static {v4}, Lcom/android/server/LocationManagerService;->access$600(Lcom/android/server/LocationManagerService;)Lcom/android/server/LocationManagerService$LocationWorkerHandler;

    #@38
    move-result-object v5

    #@39
    iget-object v4, p0, Lcom/android/server/LocationManagerService$Receiver;->this$0:Lcom/android/server/LocationManagerService;

    #@3b
    iget v6, p0, Lcom/android/server/LocationManagerService$Receiver;->mAllowedResolutionLevel:I

    #@3d
    invoke-static {v4, v6}, Lcom/android/server/LocationManagerService;->access$700(Lcom/android/server/LocationManagerService;I)Ljava/lang/String;

    #@40
    move-result-object v6

    #@41
    move-object v4, p0

    #@42
    invoke-virtual/range {v0 .. v6}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;Landroid/app/PendingIntent$OnFinished;Landroid/os/Handler;Ljava/lang/String;)V

    #@45
    .line 623
    invoke-direct {p0}, Lcom/android/server/LocationManagerService$Receiver;->incrementPendingBroadcastsLocked()V

    #@48
    .line 624
    monitor-exit p0

    #@49
    goto :goto_11

    #@4a
    :catchall_4a
    move-exception v0

    #@4b
    monitor-exit p0
    :try_end_4c
    .catchall {:try_start_2a .. :try_end_4c} :catchall_4a

    #@4c
    :try_start_4c
    throw v0
    :try_end_4d
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_4c .. :try_end_4d} :catch_4d

    #@4d
    .line 625
    :catch_4d
    move-exception v7

    #@4e
    .local v7, e:Landroid/app/PendingIntent$CanceledException;
    move v0, v8

    #@4f
    .line 626
    goto :goto_12
.end method

.method public callStatusChangedLocked(Ljava/lang/String;ILandroid/os/Bundle;)Z
    .registers 13
    .parameter "provider"
    .parameter "status"
    .parameter "extras"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 526
    iget-object v0, p0, Lcom/android/server/LocationManagerService$Receiver;->mListener:Landroid/location/ILocationListener;

    #@3
    if-eqz v0, :cond_17

    #@5
    .line 528
    :try_start_5
    monitor-enter p0
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_6} :catch_14

    #@6
    .line 531
    :try_start_6
    iget-object v0, p0, Lcom/android/server/LocationManagerService$Receiver;->mListener:Landroid/location/ILocationListener;

    #@8
    invoke-interface {v0, p1, p2, p3}, Landroid/location/ILocationListener;->onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V

    #@b
    .line 534
    invoke-direct {p0}, Lcom/android/server/LocationManagerService$Receiver;->incrementPendingBroadcastsLocked()V

    #@e
    .line 535
    monitor-exit p0

    #@f
    .line 557
    :goto_f
    const/4 v0, 0x1

    #@10
    :goto_10
    return v0

    #@11
    .line 535
    :catchall_11
    move-exception v0

    #@12
    monitor-exit p0
    :try_end_13
    .catchall {:try_start_6 .. :try_end_13} :catchall_11

    #@13
    :try_start_13
    throw v0
    :try_end_14
    .catch Landroid/os/RemoteException; {:try_start_13 .. :try_end_14} :catch_14

    #@14
    .line 536
    :catch_14
    move-exception v7

    #@15
    .local v7, e:Landroid/os/RemoteException;
    move v0, v8

    #@16
    .line 537
    goto :goto_10

    #@17
    .line 540
    .end local v7           #e:Landroid/os/RemoteException;
    :cond_17
    new-instance v3, Landroid/content/Intent;

    #@19
    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    #@1c
    .line 541
    .local v3, statusChanged:Landroid/content/Intent;
    new-instance v0, Landroid/os/Bundle;

    #@1e
    invoke-direct {v0, p3}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    #@21
    invoke-virtual {v3, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    #@24
    .line 542
    const-string v0, "status"

    #@26
    invoke-virtual {v3, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@29
    .line 544
    :try_start_29
    monitor-enter p0
    :try_end_2a
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_29 .. :try_end_2a} :catch_4d

    #@2a
    .line 547
    :try_start_2a
    iget-object v0, p0, Lcom/android/server/LocationManagerService$Receiver;->mPendingIntent:Landroid/app/PendingIntent;

    #@2c
    iget-object v1, p0, Lcom/android/server/LocationManagerService$Receiver;->this$0:Lcom/android/server/LocationManagerService;

    #@2e
    invoke-static {v1}, Lcom/android/server/LocationManagerService;->access$500(Lcom/android/server/LocationManagerService;)Landroid/content/Context;

    #@31
    move-result-object v1

    #@32
    const/4 v2, 0x0

    #@33
    iget-object v4, p0, Lcom/android/server/LocationManagerService$Receiver;->this$0:Lcom/android/server/LocationManagerService;

    #@35
    invoke-static {v4}, Lcom/android/server/LocationManagerService;->access$600(Lcom/android/server/LocationManagerService;)Lcom/android/server/LocationManagerService$LocationWorkerHandler;

    #@38
    move-result-object v5

    #@39
    iget-object v4, p0, Lcom/android/server/LocationManagerService$Receiver;->this$0:Lcom/android/server/LocationManagerService;

    #@3b
    iget v6, p0, Lcom/android/server/LocationManagerService$Receiver;->mAllowedResolutionLevel:I

    #@3d
    invoke-static {v4, v6}, Lcom/android/server/LocationManagerService;->access$700(Lcom/android/server/LocationManagerService;I)Ljava/lang/String;

    #@40
    move-result-object v6

    #@41
    move-object v4, p0

    #@42
    invoke-virtual/range {v0 .. v6}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;Landroid/app/PendingIntent$OnFinished;Landroid/os/Handler;Ljava/lang/String;)V

    #@45
    .line 551
    invoke-direct {p0}, Lcom/android/server/LocationManagerService$Receiver;->incrementPendingBroadcastsLocked()V

    #@48
    .line 552
    monitor-exit p0

    #@49
    goto :goto_f

    #@4a
    :catchall_4a
    move-exception v0

    #@4b
    monitor-exit p0
    :try_end_4c
    .catchall {:try_start_2a .. :try_end_4c} :catchall_4a

    #@4c
    :try_start_4c
    throw v0
    :try_end_4d
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_4c .. :try_end_4d} :catch_4d

    #@4d
    .line 553
    :catch_4d
    move-exception v7

    #@4e
    .local v7, e:Landroid/app/PendingIntent$CanceledException;
    move v0, v8

    #@4f
    .line 554
    goto :goto_10
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 4
    .parameter "otherObj"

    #@0
    .prologue
    .line 482
    instance-of v0, p1, Lcom/android/server/LocationManagerService$Receiver;

    #@2
    if-eqz v0, :cond_f

    #@4
    .line 483
    iget-object v0, p0, Lcom/android/server/LocationManagerService$Receiver;->mKey:Ljava/lang/Object;

    #@6
    check-cast p1, Lcom/android/server/LocationManagerService$Receiver;

    #@8
    .end local p1
    iget-object v1, p1, Lcom/android/server/LocationManagerService$Receiver;->mKey:Ljava/lang/Object;

    #@a
    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@d
    move-result v0

    #@e
    .line 485
    :goto_e
    return v0

    #@f
    .restart local p1
    :cond_f
    const/4 v0, 0x0

    #@10
    goto :goto_e
.end method

.method public getListener()Landroid/location/ILocationListener;
    .registers 3

    #@0
    .prologue
    .line 519
    iget-object v0, p0, Lcom/android/server/LocationManagerService$Receiver;->mListener:Landroid/location/ILocationListener;

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 520
    iget-object v0, p0, Lcom/android/server/LocationManagerService$Receiver;->mListener:Landroid/location/ILocationListener;

    #@6
    return-object v0

    #@7
    .line 522
    :cond_7
    new-instance v0, Ljava/lang/IllegalStateException;

    #@9
    const-string v1, "Request for non-existent listener"

    #@b
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@e
    throw v0
.end method

.method public hashCode()I
    .registers 2

    #@0
    .prologue
    .line 490
    iget-object v0, p0, Lcom/android/server/LocationManagerService$Receiver;->mKey:Ljava/lang/Object;

    #@2
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isListener()Z
    .registers 2

    #@0
    .prologue
    .line 511
    iget-object v0, p0, Lcom/android/server/LocationManagerService$Receiver;->mListener:Landroid/location/ILocationListener;

    #@2
    if-eqz v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public isPendingIntent()Z
    .registers 2

    #@0
    .prologue
    .line 515
    iget-object v0, p0, Lcom/android/server/LocationManagerService$Receiver;->mPendingIntent:Landroid/app/PendingIntent;

    #@2
    if-eqz v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public onSendFinished(Landroid/app/PendingIntent;Landroid/content/Intent;ILjava/lang/String;Landroid/os/Bundle;)V
    .registers 7
    .parameter "pendingIntent"
    .parameter "intent"
    .parameter "resultCode"
    .parameter "resultData"
    .parameter "resultExtras"

    #@0
    .prologue
    .line 650
    monitor-enter p0

    #@1
    .line 651
    :try_start_1
    invoke-direct {p0}, Lcom/android/server/LocationManagerService$Receiver;->decrementPendingBroadcastsLocked()V

    #@4
    .line 652
    monitor-exit p0

    #@5
    .line 653
    return-void

    #@6
    .line 652
    :catchall_6
    move-exception v0

    #@7
    monitor-exit p0
    :try_end_8
    .catchall {:try_start_1 .. :try_end_8} :catchall_6

    #@8
    throw v0
.end method

.method public toString()Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    .line 495
    new-instance v2, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 496
    .local v2, s:Ljava/lang/StringBuilder;
    const-string v3, "Reciever["

    #@7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    .line 497
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@d
    move-result v3

    #@e
    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@11
    move-result-object v3

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    .line 498
    iget-object v3, p0, Lcom/android/server/LocationManagerService$Receiver;->mListener:Landroid/location/ILocationListener;

    #@17
    if-eqz v3, :cond_4a

    #@19
    .line 499
    const-string v3, " listener"

    #@1b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    .line 503
    :goto_1e
    iget-object v3, p0, Lcom/android/server/LocationManagerService$Receiver;->mUpdateRecords:Ljava/util/HashMap;

    #@20
    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@23
    move-result-object v3

    #@24
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@27
    move-result-object v0

    #@28
    .local v0, i$:Ljava/util/Iterator;
    :goto_28
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@2b
    move-result v3

    #@2c
    if-eqz v3, :cond_50

    #@2e
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@31
    move-result-object v1

    #@32
    check-cast v1, Ljava/lang/String;

    #@34
    .line 504
    .local v1, p:Ljava/lang/String;
    const-string v3, " "

    #@36
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v4

    #@3a
    iget-object v3, p0, Lcom/android/server/LocationManagerService$Receiver;->mUpdateRecords:Ljava/util/HashMap;

    #@3c
    invoke-virtual {v3, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@3f
    move-result-object v3

    #@40
    check-cast v3, Lcom/android/server/LocationManagerService$UpdateRecord;

    #@42
    invoke-virtual {v3}, Lcom/android/server/LocationManagerService$UpdateRecord;->toString()Ljava/lang/String;

    #@45
    move-result-object v3

    #@46
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    goto :goto_28

    #@4a
    .line 501
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #p:Ljava/lang/String;
    :cond_4a
    const-string v3, " intent"

    #@4c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    goto :goto_1e

    #@50
    .line 506
    .restart local v0       #i$:Ljava/util/Iterator;
    :cond_50
    const-string v3, "]"

    #@52
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    .line 507
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@58
    move-result-object v3

    #@59
    return-object v3
.end method
