.class Lcom/android/server/BackupManagerService$RunBackupReceiver;
.super Landroid/content/BroadcastReceiver;
.source "BackupManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/BackupManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RunBackupReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/BackupManagerService;


# direct methods
.method private constructor <init>(Lcom/android/server/BackupManagerService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 860
    iput-object p1, p0, Lcom/android/server/BackupManagerService$RunBackupReceiver;->this$0:Lcom/android/server/BackupManagerService;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/BackupManagerService;Lcom/android/server/BackupManagerService$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 860
    invoke-direct {p0, p1}, Lcom/android/server/BackupManagerService$RunBackupReceiver;-><init>(Lcom/android/server/BackupManagerService;)V

    #@3
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 9
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 862
    const-string v2, "android.app.backup.intent.RUN"

    #@2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@5
    move-result-object v3

    #@6
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_30

    #@c
    .line 863
    iget-object v2, p0, Lcom/android/server/BackupManagerService$RunBackupReceiver;->this$0:Lcom/android/server/BackupManagerService;

    #@e
    iget-object v3, v2, Lcom/android/server/BackupManagerService;->mQueueLock:Ljava/lang/Object;

    #@10
    monitor-enter v3

    #@11
    .line 864
    :try_start_11
    iget-object v2, p0, Lcom/android/server/BackupManagerService$RunBackupReceiver;->this$0:Lcom/android/server/BackupManagerService;

    #@13
    iget-object v2, v2, Lcom/android/server/BackupManagerService;->mPendingInits:Ljava/util/HashSet;

    #@15
    invoke-virtual {v2}, Ljava/util/HashSet;->size()I
    :try_end_18
    .catchall {:try_start_11 .. :try_end_18} :catchall_3a

    #@18
    move-result v2

    #@19
    if-lez v2, :cond_3d

    #@1b
    .line 869
    :try_start_1b
    iget-object v2, p0, Lcom/android/server/BackupManagerService$RunBackupReceiver;->this$0:Lcom/android/server/BackupManagerService;

    #@1d
    invoke-static {v2}, Lcom/android/server/BackupManagerService;->access$400(Lcom/android/server/BackupManagerService;)Landroid/app/AlarmManager;

    #@20
    move-result-object v2

    #@21
    iget-object v4, p0, Lcom/android/server/BackupManagerService$RunBackupReceiver;->this$0:Lcom/android/server/BackupManagerService;

    #@23
    iget-object v4, v4, Lcom/android/server/BackupManagerService;->mRunInitIntent:Landroid/app/PendingIntent;

    #@25
    invoke-virtual {v2, v4}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    #@28
    .line 870
    iget-object v2, p0, Lcom/android/server/BackupManagerService$RunBackupReceiver;->this$0:Lcom/android/server/BackupManagerService;

    #@2a
    iget-object v2, v2, Lcom/android/server/BackupManagerService;->mRunInitIntent:Landroid/app/PendingIntent;

    #@2c
    invoke-virtual {v2}, Landroid/app/PendingIntent;->send()V
    :try_end_2f
    .catchall {:try_start_1b .. :try_end_2f} :catchall_3a
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_1b .. :try_end_2f} :catch_31

    #@2f
    .line 896
    :goto_2f
    :try_start_2f
    monitor-exit v3

    #@30
    .line 898
    :cond_30
    return-void

    #@31
    .line 871
    :catch_31
    move-exception v0

    #@32
    .line 872
    .local v0, ce:Landroid/app/PendingIntent$CanceledException;
    const-string v2, "BackupManagerService"

    #@34
    const-string v4, "Run init intent cancelled"

    #@36
    invoke-static {v2, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@39
    goto :goto_2f

    #@3a
    .line 896
    .end local v0           #ce:Landroid/app/PendingIntent$CanceledException;
    :catchall_3a
    move-exception v2

    #@3b
    monitor-exit v3
    :try_end_3c
    .catchall {:try_start_2f .. :try_end_3c} :catchall_3a

    #@3c
    throw v2

    #@3d
    .line 878
    :cond_3d
    :try_start_3d
    iget-object v2, p0, Lcom/android/server/BackupManagerService$RunBackupReceiver;->this$0:Lcom/android/server/BackupManagerService;

    #@3f
    iget-boolean v2, v2, Lcom/android/server/BackupManagerService;->mEnabled:Z

    #@41
    if-eqz v2, :cond_74

    #@43
    iget-object v2, p0, Lcom/android/server/BackupManagerService$RunBackupReceiver;->this$0:Lcom/android/server/BackupManagerService;

    #@45
    iget-boolean v2, v2, Lcom/android/server/BackupManagerService;->mProvisioned:Z

    #@47
    if-eqz v2, :cond_74

    #@49
    .line 879
    iget-object v2, p0, Lcom/android/server/BackupManagerService$RunBackupReceiver;->this$0:Lcom/android/server/BackupManagerService;

    #@4b
    iget-boolean v2, v2, Lcom/android/server/BackupManagerService;->mBackupRunning:Z

    #@4d
    if-nez v2, :cond_6c

    #@4f
    .line 884
    iget-object v2, p0, Lcom/android/server/BackupManagerService$RunBackupReceiver;->this$0:Lcom/android/server/BackupManagerService;

    #@51
    const/4 v4, 0x1

    #@52
    iput-boolean v4, v2, Lcom/android/server/BackupManagerService;->mBackupRunning:Z

    #@54
    .line 885
    iget-object v2, p0, Lcom/android/server/BackupManagerService$RunBackupReceiver;->this$0:Lcom/android/server/BackupManagerService;

    #@56
    iget-object v2, v2, Lcom/android/server/BackupManagerService;->mWakelock:Landroid/os/PowerManager$WakeLock;

    #@58
    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@5b
    .line 887
    iget-object v2, p0, Lcom/android/server/BackupManagerService$RunBackupReceiver;->this$0:Lcom/android/server/BackupManagerService;

    #@5d
    iget-object v2, v2, Lcom/android/server/BackupManagerService;->mBackupHandler:Lcom/android/server/BackupManagerService$BackupHandler;

    #@5f
    const/4 v4, 0x1

    #@60
    invoke-virtual {v2, v4}, Lcom/android/server/BackupManagerService$BackupHandler;->obtainMessage(I)Landroid/os/Message;

    #@63
    move-result-object v1

    #@64
    .line 888
    .local v1, msg:Landroid/os/Message;
    iget-object v2, p0, Lcom/android/server/BackupManagerService$RunBackupReceiver;->this$0:Lcom/android/server/BackupManagerService;

    #@66
    iget-object v2, v2, Lcom/android/server/BackupManagerService;->mBackupHandler:Lcom/android/server/BackupManagerService$BackupHandler;

    #@68
    invoke-virtual {v2, v1}, Lcom/android/server/BackupManagerService$BackupHandler;->sendMessage(Landroid/os/Message;)Z

    #@6b
    goto :goto_2f

    #@6c
    .line 890
    .end local v1           #msg:Landroid/os/Message;
    :cond_6c
    const-string v2, "BackupManagerService"

    #@6e
    const-string v4, "Backup time but one already running"

    #@70
    invoke-static {v2, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@73
    goto :goto_2f

    #@74
    .line 893
    :cond_74
    const-string v2, "BackupManagerService"

    #@76
    new-instance v4, Ljava/lang/StringBuilder;

    #@78
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@7b
    const-string v5, "Backup pass but e="

    #@7d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v4

    #@81
    iget-object v5, p0, Lcom/android/server/BackupManagerService$RunBackupReceiver;->this$0:Lcom/android/server/BackupManagerService;

    #@83
    iget-boolean v5, v5, Lcom/android/server/BackupManagerService;->mEnabled:Z

    #@85
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@88
    move-result-object v4

    #@89
    const-string v5, " p="

    #@8b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v4

    #@8f
    iget-object v5, p0, Lcom/android/server/BackupManagerService$RunBackupReceiver;->this$0:Lcom/android/server/BackupManagerService;

    #@91
    iget-boolean v5, v5, Lcom/android/server/BackupManagerService;->mProvisioned:Z

    #@93
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@96
    move-result-object v4

    #@97
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9a
    move-result-object v4

    #@9b
    invoke-static {v2, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9e
    .catchall {:try_start_3d .. :try_end_9e} :catchall_3a

    #@9e
    goto :goto_2f
.end method
