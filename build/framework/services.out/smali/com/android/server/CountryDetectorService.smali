.class public Lcom/android/server/CountryDetectorService;
.super Landroid/location/ICountryDetector$Stub;
.source "CountryDetectorService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/CountryDetectorService$Receiver;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final TAG:Ljava/lang/String; = "CountryDetector"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mCountryDetector:Lcom/android/server/location/ComprehensiveCountryDetector;

.field private mHandler:Landroid/os/Handler;

.field private mLocationBasedDetectorListener:Landroid/location/CountryListener;

.field private final mReceivers:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/os/IBinder;",
            "Lcom/android/server/CountryDetectorService$Receiver;",
            ">;"
        }
    .end annotation
.end field

.field private mSystemReady:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 95
    invoke-direct {p0}, Landroid/location/ICountryDetector$Stub;-><init>()V

    #@3
    .line 96
    new-instance v0, Ljava/util/HashMap;

    #@5
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/server/CountryDetectorService;->mReceivers:Ljava/util/HashMap;

    #@a
    .line 97
    iput-object p1, p0, Lcom/android/server/CountryDetectorService;->mContext:Landroid/content/Context;

    #@c
    .line 98
    return-void
.end method

.method static synthetic access$000(Lcom/android/server/CountryDetectorService;Landroid/os/IBinder;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/android/server/CountryDetectorService;->removeListener(Landroid/os/IBinder;)V

    #@3
    return-void
.end method

.method static synthetic access$100(Lcom/android/server/CountryDetectorService;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 45
    iget-object v0, p0, Lcom/android/server/CountryDetectorService;->mHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Lcom/android/server/CountryDetectorService;)Lcom/android/server/location/ComprehensiveCountryDetector;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 45
    iget-object v0, p0, Lcom/android/server/CountryDetectorService;->mCountryDetector:Lcom/android/server/location/ComprehensiveCountryDetector;

    #@2
    return-object v0
.end method

.method private addListener(Landroid/location/ICountryListener;)V
    .registers 7
    .parameter "listener"

    #@0
    .prologue
    .line 131
    iget-object v3, p0, Lcom/android/server/CountryDetectorService;->mReceivers:Ljava/util/HashMap;

    #@2
    monitor-enter v3

    #@3
    .line 132
    :try_start_3
    new-instance v1, Lcom/android/server/CountryDetectorService$Receiver;

    #@5
    invoke-direct {v1, p0, p1}, Lcom/android/server/CountryDetectorService$Receiver;-><init>(Lcom/android/server/CountryDetectorService;Landroid/location/ICountryListener;)V
    :try_end_8
    .catchall {:try_start_3 .. :try_end_8} :catchall_39

    #@8
    .line 134
    .local v1, r:Lcom/android/server/CountryDetectorService$Receiver;
    :try_start_8
    invoke-interface {p1}, Landroid/location/ICountryListener;->asBinder()Landroid/os/IBinder;

    #@b
    move-result-object v2

    #@c
    const/4 v4, 0x0

    #@d
    invoke-interface {v2, v1, v4}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V

    #@10
    .line 135
    iget-object v2, p0, Lcom/android/server/CountryDetectorService;->mReceivers:Ljava/util/HashMap;

    #@12
    invoke-interface {p1}, Landroid/location/ICountryListener;->asBinder()Landroid/os/IBinder;

    #@15
    move-result-object v4

    #@16
    invoke-virtual {v2, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@19
    .line 136
    iget-object v2, p0, Lcom/android/server/CountryDetectorService;->mReceivers:Ljava/util/HashMap;

    #@1b
    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    #@1e
    move-result v2

    #@1f
    const/4 v4, 0x1

    #@20
    if-ne v2, v4, :cond_2e

    #@22
    .line 137
    const-string v2, "CountryDetector"

    #@24
    const-string v4, "The first listener is added"

    #@26
    invoke-static {v2, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 138
    iget-object v2, p0, Lcom/android/server/CountryDetectorService;->mLocationBasedDetectorListener:Landroid/location/CountryListener;

    #@2b
    invoke-virtual {p0, v2}, Lcom/android/server/CountryDetectorService;->setCountryListener(Landroid/location/CountryListener;)V
    :try_end_2e
    .catchall {:try_start_8 .. :try_end_2e} :catchall_39
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_2e} :catch_30

    #@2e
    .line 143
    :cond_2e
    :goto_2e
    :try_start_2e
    monitor-exit v3

    #@2f
    .line 144
    return-void

    #@30
    .line 140
    :catch_30
    move-exception v0

    #@31
    .line 141
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "CountryDetector"

    #@33
    const-string v4, "linkToDeath failed:"

    #@35
    invoke-static {v2, v4, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@38
    goto :goto_2e

    #@39
    .line 143
    .end local v0           #e:Landroid/os/RemoteException;
    .end local v1           #r:Lcom/android/server/CountryDetectorService$Receiver;
    :catchall_39
    move-exception v2

    #@3a
    monitor-exit v3
    :try_end_3b
    .catchall {:try_start_2e .. :try_end_3b} :catchall_39

    #@3b
    throw v2
.end method

.method private initialize()V
    .registers 3

    #@0
    .prologue
    .line 177
    new-instance v0, Lcom/android/server/location/ComprehensiveCountryDetector;

    #@2
    iget-object v1, p0, Lcom/android/server/CountryDetectorService;->mContext:Landroid/content/Context;

    #@4
    invoke-direct {v0, v1}, Lcom/android/server/location/ComprehensiveCountryDetector;-><init>(Landroid/content/Context;)V

    #@7
    iput-object v0, p0, Lcom/android/server/CountryDetectorService;->mCountryDetector:Lcom/android/server/location/ComprehensiveCountryDetector;

    #@9
    .line 178
    new-instance v0, Lcom/android/server/CountryDetectorService$1;

    #@b
    invoke-direct {v0, p0}, Lcom/android/server/CountryDetectorService$1;-><init>(Lcom/android/server/CountryDetectorService;)V

    #@e
    iput-object v0, p0, Lcom/android/server/CountryDetectorService;->mLocationBasedDetectorListener:Landroid/location/CountryListener;

    #@10
    .line 187
    return-void
.end method

.method private removeListener(Landroid/os/IBinder;)V
    .registers 5
    .parameter "key"

    #@0
    .prologue
    .line 147
    iget-object v1, p0, Lcom/android/server/CountryDetectorService;->mReceivers:Ljava/util/HashMap;

    #@2
    monitor-enter v1

    #@3
    .line 148
    :try_start_3
    iget-object v0, p0, Lcom/android/server/CountryDetectorService;->mReceivers:Ljava/util/HashMap;

    #@5
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    .line 149
    iget-object v0, p0, Lcom/android/server/CountryDetectorService;->mReceivers:Ljava/util/HashMap;

    #@a
    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_1b

    #@10
    .line 150
    const/4 v0, 0x0

    #@11
    invoke-virtual {p0, v0}, Lcom/android/server/CountryDetectorService;->setCountryListener(Landroid/location/CountryListener;)V

    #@14
    .line 151
    const-string v0, "CountryDetector"

    #@16
    const-string v2, "No listener is left"

    #@18
    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 153
    :cond_1b
    monitor-exit v1

    #@1c
    .line 154
    return-void

    #@1d
    .line 153
    :catchall_1d
    move-exception v0

    #@1e
    monitor-exit v1
    :try_end_1f
    .catchall {:try_start_3 .. :try_end_1f} :catchall_1d

    #@1f
    throw v0
.end method


# virtual methods
.method public addCountryListener(Landroid/location/ICountryListener;)V
    .registers 3
    .parameter "listener"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 113
    iget-boolean v0, p0, Lcom/android/server/CountryDetectorService;->mSystemReady:Z

    #@2
    if-nez v0, :cond_a

    #@4
    .line 114
    new-instance v0, Landroid/os/RemoteException;

    #@6
    invoke-direct {v0}, Landroid/os/RemoteException;-><init>()V

    #@9
    throw v0

    #@a
    .line 116
    :cond_a
    invoke-direct {p0, p1}, Lcom/android/server/CountryDetectorService;->addListener(Landroid/location/ICountryListener;)V

    #@d
    .line 117
    return-void
.end method

.method public detectCountry()Landroid/location/Country;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 102
    iget-boolean v0, p0, Lcom/android/server/CountryDetectorService;->mSystemReady:Z

    #@2
    if-nez v0, :cond_a

    #@4
    .line 103
    new-instance v0, Landroid/os/RemoteException;

    #@6
    invoke-direct {v0}, Landroid/os/RemoteException;-><init>()V

    #@9
    throw v0

    #@a
    .line 105
    :cond_a
    iget-object v0, p0, Lcom/android/server/CountryDetectorService;->mCountryDetector:Lcom/android/server/location/ComprehensiveCountryDetector;

    #@c
    invoke-virtual {v0}, Lcom/android/server/location/ComprehensiveCountryDetector;->detectCountry()Landroid/location/Country;

    #@f
    move-result-object v0

    #@10
    return-object v0
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 7
    .parameter "fd"
    .parameter "fout"
    .parameter "args"

    #@0
    .prologue
    .line 215
    iget-object v0, p0, Lcom/android/server/CountryDetectorService;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.DUMP"

    #@4
    const-string v2, "CountryDetector"

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 217
    return-void
.end method

.method isSystemReady()Z
    .registers 2

    #@0
    .prologue
    .line 209
    iget-boolean v0, p0, Lcom/android/server/CountryDetectorService;->mSystemReady:Z

    #@2
    return v0
.end method

.method protected notifyReceivers(Landroid/location/Country;)V
    .registers 8
    .parameter "country"

    #@0
    .prologue
    .line 158
    iget-object v4, p0, Lcom/android/server/CountryDetectorService;->mReceivers:Ljava/util/HashMap;

    #@2
    monitor-enter v4

    #@3
    .line 159
    :try_start_3
    iget-object v3, p0, Lcom/android/server/CountryDetectorService;->mReceivers:Ljava/util/HashMap;

    #@5
    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@8
    move-result-object v3

    #@9
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@c
    move-result-object v1

    #@d
    .local v1, i$:Ljava/util/Iterator;
    :goto_d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@10
    move-result v3

    #@11
    if-eqz v3, :cond_2d

    #@13
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@16
    move-result-object v2

    #@17
    check-cast v2, Lcom/android/server/CountryDetectorService$Receiver;
    :try_end_19
    .catchall {:try_start_3 .. :try_end_19} :catchall_2a

    #@19
    .line 161
    .local v2, receiver:Lcom/android/server/CountryDetectorService$Receiver;
    :try_start_19
    invoke-virtual {v2}, Lcom/android/server/CountryDetectorService$Receiver;->getListener()Landroid/location/ICountryListener;

    #@1c
    move-result-object v3

    #@1d
    invoke-interface {v3, p1}, Landroid/location/ICountryListener;->onCountryDetected(Landroid/location/Country;)V
    :try_end_20
    .catchall {:try_start_19 .. :try_end_20} :catchall_2a
    .catch Landroid/os/RemoteException; {:try_start_19 .. :try_end_20} :catch_21

    #@20
    goto :goto_d

    #@21
    .line 162
    :catch_21
    move-exception v0

    #@22
    .line 164
    .local v0, e:Landroid/os/RemoteException;
    :try_start_22
    const-string v3, "CountryDetector"

    #@24
    const-string v5, "notifyReceivers failed:"

    #@26
    invoke-static {v3, v5, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@29
    goto :goto_d

    #@2a
    .line 167
    .end local v0           #e:Landroid/os/RemoteException;
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #receiver:Lcom/android/server/CountryDetectorService$Receiver;
    :catchall_2a
    move-exception v3

    #@2b
    monitor-exit v4
    :try_end_2c
    .catchall {:try_start_22 .. :try_end_2c} :catchall_2a

    #@2c
    throw v3

    #@2d
    .restart local v1       #i$:Ljava/util/Iterator;
    :cond_2d
    :try_start_2d
    monitor-exit v4
    :try_end_2e
    .catchall {:try_start_2d .. :try_end_2e} :catchall_2a

    #@2e
    .line 168
    return-void
.end method

.method public removeCountryListener(Landroid/location/ICountryListener;)V
    .registers 3
    .parameter "listener"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 124
    iget-boolean v0, p0, Lcom/android/server/CountryDetectorService;->mSystemReady:Z

    #@2
    if-nez v0, :cond_a

    #@4
    .line 125
    new-instance v0, Landroid/os/RemoteException;

    #@6
    invoke-direct {v0}, Landroid/os/RemoteException;-><init>()V

    #@9
    throw v0

    #@a
    .line 127
    :cond_a
    invoke-interface {p1}, Landroid/location/ICountryListener;->asBinder()Landroid/os/IBinder;

    #@d
    move-result-object v0

    #@e
    invoke-direct {p0, v0}, Lcom/android/server/CountryDetectorService;->removeListener(Landroid/os/IBinder;)V

    #@11
    .line 128
    return-void
.end method

.method public run()V
    .registers 2

    #@0
    .prologue
    .line 190
    const/16 v0, 0xa

    #@2
    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    #@5
    .line 191
    invoke-static {}, Landroid/os/Looper;->prepare()V

    #@8
    .line 192
    new-instance v0, Landroid/os/Handler;

    #@a
    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    #@d
    iput-object v0, p0, Lcom/android/server/CountryDetectorService;->mHandler:Landroid/os/Handler;

    #@f
    .line 193
    invoke-direct {p0}, Lcom/android/server/CountryDetectorService;->initialize()V

    #@12
    .line 194
    const/4 v0, 0x1

    #@13
    iput-boolean v0, p0, Lcom/android/server/CountryDetectorService;->mSystemReady:Z

    #@15
    .line 195
    invoke-static {}, Landroid/os/Looper;->loop()V

    #@18
    .line 196
    return-void
.end method

.method protected setCountryListener(Landroid/location/CountryListener;)V
    .registers 4
    .parameter "listener"

    #@0
    .prologue
    .line 199
    iget-object v0, p0, Lcom/android/server/CountryDetectorService;->mHandler:Landroid/os/Handler;

    #@2
    new-instance v1, Lcom/android/server/CountryDetectorService$2;

    #@4
    invoke-direct {v1, p0, p1}, Lcom/android/server/CountryDetectorService$2;-><init>(Lcom/android/server/CountryDetectorService;Landroid/location/CountryListener;)V

    #@7
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@a
    .line 205
    return-void
.end method

.method systemReady()V
    .registers 3

    #@0
    .prologue
    .line 172
    new-instance v0, Ljava/lang/Thread;

    #@2
    const-string v1, "CountryDetectorService"

    #@4
    invoke-direct {v0, p0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    #@7
    .line 173
    .local v0, thread:Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    #@a
    .line 174
    return-void
.end method
