.class Lcom/android/server/MountService$3;
.super Ljava/lang/Thread;
.source "MountService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/MountService;->onDaemonConnected()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/MountService;


# direct methods
.method constructor <init>(Lcom/android/server/MountService;Ljava/lang/String;)V
    .registers 3
    .parameter
    .parameter "x0"

    #@0
    .prologue
    .line 705
    iput-object p1, p0, Lcom/android/server/MountService$3;->this$0:Lcom/android/server/MountService;

    #@2
    invoke-direct {p0, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    #@5
    return-void
.end method


# virtual methods
.method public run()V
    .registers 19

    #@0
    .prologue
    .line 719
    :try_start_0
    const-string v13, "vold.decrypt"

    #@2
    invoke-static {v13}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v13

    #@6
    const-string v14, "trigger_restart_min_framework"

    #@8
    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_b
    .catchall {:try_start_0 .. :try_end_b} :catchall_e1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_b} :catch_93

    #@b
    move-result v13

    #@c
    if-eqz v13, :cond_30

    #@e
    .line 770
    move-object/from16 v0, p0

    #@10
    iget-object v13, v0, Lcom/android/server/MountService$3;->this$0:Lcom/android/server/MountService;

    #@12
    invoke-static {v13}, Lcom/android/server/MountService;->access$1600(Lcom/android/server/MountService;)Ljava/util/concurrent/CountDownLatch;

    #@15
    move-result-object v13

    #@16
    invoke-virtual {v13}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    #@19
    .line 773
    move-object/from16 v0, p0

    #@1b
    iget-object v13, v0, Lcom/android/server/MountService$3;->this$0:Lcom/android/server/MountService;

    #@1d
    invoke-static {v13}, Lcom/android/server/MountService;->access$300(Lcom/android/server/MountService;)Lcom/android/server/pm/PackageManagerService;

    #@20
    move-result-object v13

    #@21
    invoke-virtual {v13}, Lcom/android/server/pm/PackageManagerService;->scanAvailableAsecs()V

    #@24
    .line 776
    move-object/from16 v0, p0

    #@26
    iget-object v13, v0, Lcom/android/server/MountService$3;->this$0:Lcom/android/server/MountService;

    #@28
    invoke-static {v13}, Lcom/android/server/MountService;->access$1700(Lcom/android/server/MountService;)Ljava/util/concurrent/CountDownLatch;

    #@2b
    move-result-object v13

    #@2c
    :goto_2c
    invoke-virtual {v13}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    #@2f
    .line 778
    return-void

    #@30
    .line 722
    :cond_30
    :try_start_30
    move-object/from16 v0, p0

    #@32
    iget-object v13, v0, Lcom/android/server/MountService$3;->this$0:Lcom/android/server/MountService;

    #@34
    invoke-static {v13}, Lcom/android/server/MountService;->access$1200(Lcom/android/server/MountService;)Lcom/android/server/NativeDaemonConnector;

    #@37
    move-result-object v13

    #@38
    const-string v14, "volume"

    #@3a
    const/4 v15, 0x1

    #@3b
    new-array v15, v15, [Ljava/lang/Object;

    #@3d
    const/16 v16, 0x0

    #@3f
    const-string v17, "list"

    #@41
    aput-object v17, v15, v16

    #@43
    invoke-virtual {v13, v14, v15}, Lcom/android/server/NativeDaemonConnector;->executeForList(Ljava/lang/String;[Ljava/lang/Object;)[Lcom/android/server/NativeDaemonEvent;

    #@46
    move-result-object v13

    #@47
    const/16 v14, 0x6e

    #@49
    invoke-static {v13, v14}, Lcom/android/server/NativeDaemonEvent;->filterMessageList([Lcom/android/server/NativeDaemonEvent;I)[Ljava/lang/String;

    #@4c
    move-result-object v10

    #@4d
    .line 727
    .local v10, vols:[Ljava/lang/String;
    move-object v1, v10

    #@4e
    .local v1, arr$:[Ljava/lang/String;
    array-length v4, v1

    #@4f
    .local v4, len$:I
    const/4 v3, 0x0

    #@50
    .local v3, i$:I
    :goto_50
    if-ge v3, v4, :cond_129

    #@52
    aget-object v11, v1, v3

    #@54
    .line 728
    .local v11, volstr:Ljava/lang/String;
    const-string v13, " "

    #@56
    invoke-virtual {v11, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@59
    move-result-object v9

    #@5a
    .line 730
    .local v9, tok:[Ljava/lang/String;
    const/4 v13, 0x1

    #@5b
    aget-object v5, v9, v13

    #@5d
    .line 731
    .local v5, path:Ljava/lang/String;
    const-string v8, "removed"

    #@5f
    .line 734
    .local v8, state:Ljava/lang/String;
    move-object/from16 v0, p0

    #@61
    iget-object v13, v0, Lcom/android/server/MountService$3;->this$0:Lcom/android/server/MountService;

    #@63
    invoke-static {v13}, Lcom/android/server/MountService;->access$600(Lcom/android/server/MountService;)Ljava/lang/Object;

    #@66
    move-result-object v14

    #@67
    monitor-enter v14
    :try_end_68
    .catchall {:try_start_30 .. :try_end_68} :catchall_e1
    .catch Ljava/lang/Exception; {:try_start_30 .. :try_end_68} :catch_93

    #@68
    .line 735
    :try_start_68
    move-object/from16 v0, p0

    #@6a
    iget-object v13, v0, Lcom/android/server/MountService$3;->this$0:Lcom/android/server/MountService;

    #@6c
    invoke-static {v13}, Lcom/android/server/MountService;->access$1300(Lcom/android/server/MountService;)Ljava/util/HashMap;

    #@6f
    move-result-object v13

    #@70
    invoke-virtual {v13, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@73
    move-result-object v12

    #@74
    check-cast v12, Landroid/os/storage/StorageVolume;

    #@76
    .line 736
    .local v12, volume:Landroid/os/storage/StorageVolume;
    monitor-exit v14
    :try_end_77
    .catchall {:try_start_68 .. :try_end_77} :catchall_90

    #@77
    .line 738
    const/4 v13, 0x2

    #@78
    :try_start_78
    aget-object v13, v9, v13

    #@7a
    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@7d
    move-result v7

    #@7e
    .line 739
    .local v7, st:I
    if-nez v7, :cond_ce

    #@80
    .line 740
    const-string v8, "removed"

    #@82
    .line 753
    :goto_82
    if-eqz v12, :cond_8d

    #@84
    if-eqz v8, :cond_8d

    #@86
    .line 755
    move-object/from16 v0, p0

    #@88
    iget-object v13, v0, Lcom/android/server/MountService$3;->this$0:Lcom/android/server/MountService;

    #@8a
    invoke-static {v13, v12, v8}, Lcom/android/server/MountService;->access$1400(Lcom/android/server/MountService;Landroid/os/storage/StorageVolume;Ljava/lang/String;)V
    :try_end_8d
    .catchall {:try_start_78 .. :try_end_8d} :catchall_e1
    .catch Ljava/lang/Exception; {:try_start_78 .. :try_end_8d} :catch_93

    #@8d
    .line 727
    :cond_8d
    add-int/lit8 v3, v3, 0x1

    #@8f
    goto :goto_50

    #@90
    .line 736
    .end local v7           #st:I
    .end local v12           #volume:Landroid/os/storage/StorageVolume;
    :catchall_90
    move-exception v13

    #@91
    :try_start_91
    monitor-exit v14
    :try_end_92
    .catchall {:try_start_91 .. :try_end_92} :catchall_90

    #@92
    :try_start_92
    throw v13
    :try_end_93
    .catchall {:try_start_92 .. :try_end_93} :catchall_e1
    .catch Ljava/lang/Exception; {:try_start_92 .. :try_end_93} :catch_93

    #@93
    .line 758
    .end local v1           #arr$:[Ljava/lang/String;
    .end local v3           #i$:I
    .end local v4           #len$:I
    .end local v5           #path:Ljava/lang/String;
    .end local v8           #state:Ljava/lang/String;
    .end local v9           #tok:[Ljava/lang/String;
    .end local v10           #vols:[Ljava/lang/String;
    .end local v11           #volstr:Ljava/lang/String;
    :catch_93
    move-exception v2

    #@94
    .line 759
    .local v2, e:Ljava/lang/Exception;
    :try_start_94
    const-string v13, "MountService"

    #@96
    const-string v14, "Error processing initial volume state"

    #@98
    invoke-static {v13, v14, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@9b
    .line 760
    move-object/from16 v0, p0

    #@9d
    iget-object v13, v0, Lcom/android/server/MountService$3;->this$0:Lcom/android/server/MountService;

    #@9f
    invoke-static {v13}, Lcom/android/server/MountService;->access$1500(Lcom/android/server/MountService;)Landroid/os/storage/StorageVolume;

    #@a2
    move-result-object v6

    #@a3
    .line 761
    .local v6, primary:Landroid/os/storage/StorageVolume;
    if-eqz v6, :cond_ae

    #@a5
    .line 762
    move-object/from16 v0, p0

    #@a7
    iget-object v13, v0, Lcom/android/server/MountService$3;->this$0:Lcom/android/server/MountService;

    #@a9
    const-string v14, "removed"

    #@ab
    invoke-static {v13, v6, v14}, Lcom/android/server/MountService;->access$1400(Lcom/android/server/MountService;Landroid/os/storage/StorageVolume;Ljava/lang/String;)V
    :try_end_ae
    .catchall {:try_start_94 .. :try_end_ae} :catchall_e1

    #@ae
    .line 770
    :cond_ae
    move-object/from16 v0, p0

    #@b0
    iget-object v13, v0, Lcom/android/server/MountService$3;->this$0:Lcom/android/server/MountService;

    #@b2
    invoke-static {v13}, Lcom/android/server/MountService;->access$1600(Lcom/android/server/MountService;)Ljava/util/concurrent/CountDownLatch;

    #@b5
    move-result-object v13

    #@b6
    invoke-virtual {v13}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    #@b9
    .line 773
    move-object/from16 v0, p0

    #@bb
    iget-object v13, v0, Lcom/android/server/MountService$3;->this$0:Lcom/android/server/MountService;

    #@bd
    invoke-static {v13}, Lcom/android/server/MountService;->access$300(Lcom/android/server/MountService;)Lcom/android/server/pm/PackageManagerService;

    #@c0
    move-result-object v13

    #@c1
    invoke-virtual {v13}, Lcom/android/server/pm/PackageManagerService;->scanAvailableAsecs()V

    #@c4
    .line 776
    move-object/from16 v0, p0

    #@c6
    iget-object v13, v0, Lcom/android/server/MountService$3;->this$0:Lcom/android/server/MountService;

    #@c8
    invoke-static {v13}, Lcom/android/server/MountService;->access$1700(Lcom/android/server/MountService;)Ljava/util/concurrent/CountDownLatch;

    #@cb
    move-result-object v13

    #@cc
    goto/16 :goto_2c

    #@ce
    .line 741
    .end local v2           #e:Ljava/lang/Exception;
    .end local v6           #primary:Landroid/os/storage/StorageVolume;
    .restart local v1       #arr$:[Ljava/lang/String;
    .restart local v3       #i$:I
    .restart local v4       #len$:I
    .restart local v5       #path:Ljava/lang/String;
    .restart local v7       #st:I
    .restart local v8       #state:Ljava/lang/String;
    .restart local v9       #tok:[Ljava/lang/String;
    .restart local v10       #vols:[Ljava/lang/String;
    .restart local v11       #volstr:Ljava/lang/String;
    .restart local v12       #volume:Landroid/os/storage/StorageVolume;
    :cond_ce
    const/4 v13, 0x1

    #@cf
    if-ne v7, v13, :cond_d4

    #@d1
    .line 742
    :try_start_d1
    const-string v8, "unmounted"

    #@d3
    goto :goto_82

    #@d4
    .line 743
    :cond_d4
    const/4 v13, 0x4

    #@d5
    if-ne v7, v13, :cond_104

    #@d7
    .line 744
    const-string v8, "mounted"

    #@d9
    .line 745
    const-string v13, "MountService"

    #@db
    const-string v14, "Media already mounted on daemon connection"

    #@dd
    invoke-static {v13, v14}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_e0
    .catchall {:try_start_d1 .. :try_end_e0} :catchall_e1
    .catch Ljava/lang/Exception; {:try_start_d1 .. :try_end_e0} :catch_93

    #@e0
    goto :goto_82

    #@e1
    .line 770
    .end local v1           #arr$:[Ljava/lang/String;
    .end local v3           #i$:I
    .end local v4           #len$:I
    .end local v5           #path:Ljava/lang/String;
    .end local v7           #st:I
    .end local v8           #state:Ljava/lang/String;
    .end local v9           #tok:[Ljava/lang/String;
    .end local v10           #vols:[Ljava/lang/String;
    .end local v11           #volstr:Ljava/lang/String;
    .end local v12           #volume:Landroid/os/storage/StorageVolume;
    :catchall_e1
    move-exception v13

    #@e2
    move-object/from16 v0, p0

    #@e4
    iget-object v14, v0, Lcom/android/server/MountService$3;->this$0:Lcom/android/server/MountService;

    #@e6
    invoke-static {v14}, Lcom/android/server/MountService;->access$1600(Lcom/android/server/MountService;)Ljava/util/concurrent/CountDownLatch;

    #@e9
    move-result-object v14

    #@ea
    invoke-virtual {v14}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    #@ed
    .line 773
    move-object/from16 v0, p0

    #@ef
    iget-object v14, v0, Lcom/android/server/MountService$3;->this$0:Lcom/android/server/MountService;

    #@f1
    invoke-static {v14}, Lcom/android/server/MountService;->access$300(Lcom/android/server/MountService;)Lcom/android/server/pm/PackageManagerService;

    #@f4
    move-result-object v14

    #@f5
    invoke-virtual {v14}, Lcom/android/server/pm/PackageManagerService;->scanAvailableAsecs()V

    #@f8
    .line 776
    move-object/from16 v0, p0

    #@fa
    iget-object v14, v0, Lcom/android/server/MountService$3;->this$0:Lcom/android/server/MountService;

    #@fc
    invoke-static {v14}, Lcom/android/server/MountService;->access$1700(Lcom/android/server/MountService;)Ljava/util/concurrent/CountDownLatch;

    #@ff
    move-result-object v14

    #@100
    invoke-virtual {v14}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    #@103
    .line 770
    throw v13

    #@104
    .line 746
    .restart local v1       #arr$:[Ljava/lang/String;
    .restart local v3       #i$:I
    .restart local v4       #len$:I
    .restart local v5       #path:Ljava/lang/String;
    .restart local v7       #st:I
    .restart local v8       #state:Ljava/lang/String;
    .restart local v9       #tok:[Ljava/lang/String;
    .restart local v10       #vols:[Ljava/lang/String;
    .restart local v11       #volstr:Ljava/lang/String;
    .restart local v12       #volume:Landroid/os/storage/StorageVolume;
    :cond_104
    const/4 v13, 0x7

    #@105
    if-ne v7, v13, :cond_112

    #@107
    .line 747
    :try_start_107
    const-string v8, "shared"

    #@109
    .line 748
    const-string v13, "MountService"

    #@10b
    const-string v14, "Media shared on daemon connection"

    #@10d
    invoke-static {v13, v14}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@110
    goto/16 :goto_82

    #@112
    .line 750
    :cond_112
    new-instance v13, Ljava/lang/Exception;

    #@114
    const-string v14, "Unexpected state %d"

    #@116
    const/4 v15, 0x1

    #@117
    new-array v15, v15, [Ljava/lang/Object;

    #@119
    const/16 v16, 0x0

    #@11b
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@11e
    move-result-object v17

    #@11f
    aput-object v17, v15, v16

    #@121
    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@124
    move-result-object v14

    #@125
    invoke-direct {v13, v14}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    #@128
    throw v13
    :try_end_129
    .catchall {:try_start_107 .. :try_end_129} :catchall_e1
    .catch Ljava/lang/Exception; {:try_start_107 .. :try_end_129} :catch_93

    #@129
    .line 770
    .end local v5           #path:Ljava/lang/String;
    .end local v7           #st:I
    .end local v8           #state:Ljava/lang/String;
    .end local v9           #tok:[Ljava/lang/String;
    .end local v11           #volstr:Ljava/lang/String;
    .end local v12           #volume:Landroid/os/storage/StorageVolume;
    :cond_129
    move-object/from16 v0, p0

    #@12b
    iget-object v13, v0, Lcom/android/server/MountService$3;->this$0:Lcom/android/server/MountService;

    #@12d
    invoke-static {v13}, Lcom/android/server/MountService;->access$1600(Lcom/android/server/MountService;)Ljava/util/concurrent/CountDownLatch;

    #@130
    move-result-object v13

    #@131
    invoke-virtual {v13}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    #@134
    .line 773
    move-object/from16 v0, p0

    #@136
    iget-object v13, v0, Lcom/android/server/MountService$3;->this$0:Lcom/android/server/MountService;

    #@138
    invoke-static {v13}, Lcom/android/server/MountService;->access$300(Lcom/android/server/MountService;)Lcom/android/server/pm/PackageManagerService;

    #@13b
    move-result-object v13

    #@13c
    invoke-virtual {v13}, Lcom/android/server/pm/PackageManagerService;->scanAvailableAsecs()V

    #@13f
    .line 776
    move-object/from16 v0, p0

    #@141
    iget-object v13, v0, Lcom/android/server/MountService$3;->this$0:Lcom/android/server/MountService;

    #@143
    invoke-static {v13}, Lcom/android/server/MountService;->access$1700(Lcom/android/server/MountService;)Ljava/util/concurrent/CountDownLatch;

    #@146
    move-result-object v13

    #@147
    goto/16 :goto_2c
.end method
