.class Lcom/android/server/ClipboardService$PerUserClipboard;
.super Ljava/lang/Object;
.source "ClipboardService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/ClipboardService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PerUserClipboard"
.end annotation


# instance fields
.field final activePermissionOwners:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field hasNoValidCliptrayClip:Z

.field mdmClip:Landroid/content/ClipData;

.field primaryClip:Landroid/content/ClipData;

.field primaryClipList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ClipData;",
            ">;"
        }
    .end annotation
.end field

.field final primaryClipListeners:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList",
            "<",
            "Landroid/content/IOnPrimaryClipChangedListener;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/android/server/ClipboardService;

.field final userId:I


# direct methods
.method constructor <init>(Lcom/android/server/ClipboardService;I)V
    .registers 4
    .parameter
    .parameter "userId"

    #@0
    .prologue
    .line 112
    iput-object p1, p0, Lcom/android/server/ClipboardService$PerUserClipboard;->this$0:Lcom/android/server/ClipboardService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 96
    new-instance v0, Landroid/os/RemoteCallbackList;

    #@7
    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    #@a
    iput-object v0, p0, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClipListeners:Landroid/os/RemoteCallbackList;

    #@c
    .line 100
    new-instance v0, Ljava/util/ArrayList;

    #@e
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@11
    iput-object v0, p0, Lcom/android/server/ClipboardService$PerUserClipboard;->primaryClipList:Ljava/util/ArrayList;

    #@13
    .line 101
    const/4 v0, 0x0

    #@14
    iput-boolean v0, p0, Lcom/android/server/ClipboardService$PerUserClipboard;->hasNoValidCliptrayClip:Z

    #@16
    .line 109
    new-instance v0, Ljava/util/HashSet;

    #@18
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    #@1b
    iput-object v0, p0, Lcom/android/server/ClipboardService$PerUserClipboard;->activePermissionOwners:Ljava/util/HashSet;

    #@1d
    .line 113
    iput p2, p0, Lcom/android/server/ClipboardService$PerUserClipboard;->userId:I

    #@1f
    .line 114
    return-void
.end method
