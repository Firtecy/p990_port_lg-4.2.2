.class Lcom/android/server/DeviceManager3LMService$PackageInstallObserver;
.super Landroid/content/pm/IPackageInstallObserver$Stub;
.source "DeviceManager3LMService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/DeviceManager3LMService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PackageInstallObserver"
.end annotation


# instance fields
.field private doneFlag:Z

.field public returnCode:I

.field final synthetic this$0:Lcom/android/server/DeviceManager3LMService;


# direct methods
.method constructor <init>(Lcom/android/server/DeviceManager3LMService;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 1394
    iput-object p1, p0, Lcom/android/server/DeviceManager3LMService$PackageInstallObserver;->this$0:Lcom/android/server/DeviceManager3LMService;

    #@2
    invoke-direct {p0}, Landroid/content/pm/IPackageInstallObserver$Stub;-><init>()V

    #@5
    .line 1396
    const/4 v0, 0x0

    #@6
    iput-boolean v0, p0, Lcom/android/server/DeviceManager3LMService$PackageInstallObserver;->doneFlag:Z

    #@8
    return-void
.end method


# virtual methods
.method public isDone()Z
    .registers 2

    #@0
    .prologue
    .line 1407
    iget-boolean v0, p0, Lcom/android/server/DeviceManager3LMService$PackageInstallObserver;->doneFlag:Z

    #@2
    return v0
.end method

.method public packageInstalled(Ljava/lang/String;I)V
    .registers 4
    .parameter "packageName"
    .parameter "returnCode"

    #@0
    .prologue
    .line 1399
    monitor-enter p0

    #@1
    .line 1400
    :try_start_1
    iput p2, p0, Lcom/android/server/DeviceManager3LMService$PackageInstallObserver;->returnCode:I

    #@3
    .line 1401
    const/4 v0, 0x1

    #@4
    iput-boolean v0, p0, Lcom/android/server/DeviceManager3LMService$PackageInstallObserver;->doneFlag:Z

    #@6
    .line 1402
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    #@9
    .line 1403
    monitor-exit p0

    #@a
    .line 1404
    return-void

    #@b
    .line 1403
    :catchall_b
    move-exception v0

    #@c
    monitor-exit p0
    :try_end_d
    .catchall {:try_start_1 .. :try_end_d} :catchall_b

    #@d
    throw v0
.end method
