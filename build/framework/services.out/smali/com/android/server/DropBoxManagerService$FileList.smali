.class final Lcom/android/server/DropBoxManagerService$FileList;
.super Ljava/lang/Object;
.source "DropBoxManagerService.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/DropBoxManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "FileList"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/android/server/DropBoxManagerService$FileList;",
        ">;"
    }
.end annotation


# instance fields
.field public blocks:I

.field public final contents:Ljava/util/TreeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeSet",
            "<",
            "Lcom/android/server/DropBoxManagerService$EntryFile;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 441
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 442
    const/4 v0, 0x0

    #@4
    iput v0, p0, Lcom/android/server/DropBoxManagerService$FileList;->blocks:I

    #@6
    .line 443
    new-instance v0, Ljava/util/TreeSet;

    #@8
    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    #@b
    iput-object v0, p0, Lcom/android/server/DropBoxManagerService$FileList;->contents:Ljava/util/TreeSet;

    #@d
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/DropBoxManagerService$1;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 441
    invoke-direct {p0}, Lcom/android/server/DropBoxManagerService$FileList;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public final compareTo(Lcom/android/server/DropBoxManagerService$FileList;)I
    .registers 5
    .parameter "o"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 447
    iget v1, p0, Lcom/android/server/DropBoxManagerService$FileList;->blocks:I

    #@3
    iget v2, p1, Lcom/android/server/DropBoxManagerService$FileList;->blocks:I

    #@5
    if-eq v1, v2, :cond_d

    #@7
    iget v0, p1, Lcom/android/server/DropBoxManagerService$FileList;->blocks:I

    #@9
    iget v1, p0, Lcom/android/server/DropBoxManagerService$FileList;->blocks:I

    #@b
    sub-int/2addr v0, v1

    #@c
    .line 451
    :cond_c
    :goto_c
    return v0

    #@d
    .line 448
    :cond_d
    if-eq p0, p1, :cond_c

    #@f
    .line 449
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    #@12
    move-result v1

    #@13
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    #@16
    move-result v2

    #@17
    if-ge v1, v2, :cond_1b

    #@19
    const/4 v0, -0x1

    #@1a
    goto :goto_c

    #@1b
    .line 450
    :cond_1b
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    #@1e
    move-result v1

    #@1f
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    #@22
    move-result v2

    #@23
    if-le v1, v2, :cond_c

    #@25
    const/4 v0, 0x1

    #@26
    goto :goto_c
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 441
    check-cast p1, Lcom/android/server/DropBoxManagerService$FileList;

    #@2
    .end local p1
    invoke-virtual {p0, p1}, Lcom/android/server/DropBoxManagerService$FileList;->compareTo(Lcom/android/server/DropBoxManagerService$FileList;)I

    #@5
    move-result v0

    #@6
    return v0
.end method
