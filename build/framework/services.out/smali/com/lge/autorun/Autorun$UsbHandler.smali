.class final Lcom/lge/autorun/Autorun$UsbHandler;
.super Landroid/os/Handler;
.source "Autorun.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/autorun/Autorun;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "UsbHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/autorun/Autorun;


# direct methods
.method public constructor <init>(Lcom/lge/autorun/Autorun;Landroid/os/Looper;)V
    .registers 5
    .parameter
    .parameter "looper"

    #@0
    .prologue
    .line 208
    iput-object p1, p0, Lcom/lge/autorun/Autorun$UsbHandler;->this$0:Lcom/lge/autorun/Autorun;

    #@2
    .line 209
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@5
    .line 210
    invoke-static {p1}, Lcom/lge/autorun/Autorun;->access$300(Lcom/lge/autorun/Autorun;)Z

    #@8
    move-result v0

    #@9
    if-nez v0, :cond_18

    #@b
    .line 211
    invoke-static {p1}, Lcom/lge/autorun/Autorun;->access$400(Lcom/lge/autorun/Autorun;)Landroid/os/UEventObserver;

    #@e
    move-result-object v0

    #@f
    const-string v1, "DEVPATH=/devices/virtual/misc/usb_autorun"

    #@11
    invoke-virtual {v0, v1}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V

    #@14
    .line 212
    const/4 v0, 0x1

    #@15
    invoke-static {p1, v0}, Lcom/lge/autorun/Autorun;->access$302(Lcom/lge/autorun/Autorun;Z)Z

    #@18
    .line 214
    :cond_18
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 4
    .parameter "msg"

    #@0
    .prologue
    .line 231
    iget v0, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v0, :pswitch_data_24

    #@5
    .line 243
    const-string v0, "AUTORUN"

    #@7
    const-string v1, "[AUTORUN] ERROR!! unknown msg is delivered"

    #@9
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 245
    :goto_c
    return-void

    #@d
    .line 233
    :pswitch_d
    iget-object v0, p0, Lcom/lge/autorun/Autorun$UsbHandler;->this$0:Lcom/lge/autorun/Autorun;

    #@f
    invoke-virtual {v0}, Lcom/lge/autorun/Autorun;->onUsbUnPlugged()V

    #@12
    goto :goto_c

    #@13
    .line 236
    :pswitch_13
    iget-object v0, p0, Lcom/lge/autorun/Autorun$UsbHandler;->this$0:Lcom/lge/autorun/Autorun;

    #@15
    invoke-virtual {v0}, Lcom/lge/autorun/Autorun;->onUsbPlugged()V

    #@18
    goto :goto_c

    #@19
    .line 239
    :pswitch_19
    iget-object v1, p0, Lcom/lge/autorun/Autorun$UsbHandler;->this$0:Lcom/lge/autorun/Autorun;

    #@1b
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1d
    check-cast v0, Ljava/lang/String;

    #@1f
    invoke-virtual {v1, v0}, Lcom/lge/autorun/Autorun;->onAutorunUpdateState(Ljava/lang/String;)V

    #@22
    goto :goto_c

    #@23
    .line 231
    nop

    #@24
    :pswitch_data_24
    .packed-switch 0x0
        :pswitch_d
        :pswitch_13
        :pswitch_19
    .end packed-switch
.end method

.method public sendMessage(I)V
    .registers 3
    .parameter "what"

    #@0
    .prologue
    .line 217
    invoke-virtual {p0, p1}, Lcom/lge/autorun/Autorun$UsbHandler;->removeMessages(I)V

    #@3
    .line 218
    invoke-static {p0, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    #@6
    move-result-object v0

    #@7
    .line 219
    .local v0, m:Landroid/os/Message;
    invoke-virtual {p0, v0}, Lcom/lge/autorun/Autorun$UsbHandler;->sendMessage(Landroid/os/Message;)Z

    #@a
    .line 220
    return-void
.end method

.method public sendMessage(ILjava/lang/Object;)V
    .registers 4
    .parameter "what"
    .parameter "arg"

    #@0
    .prologue
    .line 223
    invoke-virtual {p0, p1}, Lcom/lge/autorun/Autorun$UsbHandler;->removeMessages(I)V

    #@3
    .line 224
    invoke-static {p0, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    #@6
    move-result-object v0

    #@7
    .line 225
    .local v0, m:Landroid/os/Message;
    iput-object p2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@9
    .line 226
    invoke-virtual {p0, v0}, Lcom/lge/autorun/Autorun$UsbHandler;->sendMessage(Landroid/os/Message;)Z

    #@c
    .line 227
    return-void
.end method
