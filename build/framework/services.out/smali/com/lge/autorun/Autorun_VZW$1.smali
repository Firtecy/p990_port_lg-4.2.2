.class Lcom/lge/autorun/Autorun_VZW$1;
.super Landroid/content/BroadcastReceiver;
.source "Autorun_VZW.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/autorun/Autorun_VZW;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/autorun/Autorun_VZW;


# direct methods
.method constructor <init>(Lcom/lge/autorun/Autorun_VZW;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 46
    iput-object p1, p0, Lcom/lge/autorun/Autorun_VZW$1;->this$0:Lcom/lge/autorun/Autorun_VZW;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 8
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 49
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 50
    .local v0, action:Ljava/lang/String;
    const-string v1, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    #@8
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v1

    #@c
    if-eqz v1, :cond_4d

    #@e
    .line 51
    const-string v1, "reason"

    #@10
    invoke-virtual {p2, v1}, Landroid/content/Intent;->getExtra(Ljava/lang/String;)Ljava/lang/Object;

    #@13
    move-result-object v1

    #@14
    if-eqz v1, :cond_4c

    #@16
    const-string v1, "reason"

    #@18
    invoke-virtual {p2, v1}, Landroid/content/Intent;->getExtra(Ljava/lang/String;)Ljava/lang/Object;

    #@1b
    move-result-object v1

    #@1c
    const-string v2, "lock"

    #@1e
    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@21
    move-result v1

    #@22
    if-eqz v1, :cond_4c

    #@24
    .line 52
    iget-object v1, p0, Lcom/lge/autorun/Autorun_VZW$1;->this$0:Lcom/lge/autorun/Autorun_VZW;

    #@26
    invoke-static {v1, v4}, Lcom/lge/autorun/Autorun_VZW;->access$000(Lcom/lge/autorun/Autorun_VZW;Z)V

    #@29
    .line 53
    iget-object v1, p0, Lcom/lge/autorun/Autorun_VZW$1;->this$0:Lcom/lge/autorun/Autorun_VZW;

    #@2b
    invoke-virtual {v1}, Lcom/lge/autorun/Autorun_VZW;->getUsbUnPlugState()Z

    #@2e
    move-result v1

    #@2f
    if-eqz v1, :cond_4c

    #@31
    iget-object v1, p0, Lcom/lge/autorun/Autorun_VZW$1;->this$0:Lcom/lge/autorun/Autorun_VZW;

    #@33
    invoke-static {v1}, Lcom/lge/autorun/Autorun_VZW;->access$100(Lcom/lge/autorun/Autorun_VZW;)Z

    #@36
    move-result v1

    #@37
    if-eqz v1, :cond_4c

    #@39
    .line 54
    iget-object v1, p0, Lcom/lge/autorun/Autorun_VZW$1;->this$0:Lcom/lge/autorun/Autorun_VZW;

    #@3b
    invoke-static {v1, v4}, Lcom/lge/autorun/Autorun_VZW;->access$202(Lcom/lge/autorun/Autorun_VZW;Z)Z

    #@3e
    .line 55
    const-string v1, "AUTORUN"

    #@40
    const-string v2, "[AUTORUN] setCurrentFunction(charge_only,false)::onReceive when securelocked"

    #@42
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    .line 56
    iget-object v1, p0, Lcom/lge/autorun/Autorun_VZW$1;->this$0:Lcom/lge/autorun/Autorun_VZW;

    #@47
    const-string v2, "charge_only"

    #@49
    invoke-virtual {v1, v2, v3}, Lcom/lge/autorun/Autorun_VZW;->setCurrentFunction(Ljava/lang/String;Z)V

    #@4c
    .line 76
    :cond_4c
    :goto_4c
    return-void

    #@4d
    .line 59
    :cond_4d
    const-string v1, "android.intent.action.USER_PRESENT"

    #@4f
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@52
    move-result v1

    #@53
    if-eqz v1, :cond_4c

    #@55
    .line 60
    iget-object v1, p0, Lcom/lge/autorun/Autorun_VZW$1;->this$0:Lcom/lge/autorun/Autorun_VZW;

    #@57
    invoke-static {v1}, Lcom/lge/autorun/Autorun_VZW;->access$100(Lcom/lge/autorun/Autorun_VZW;)Z

    #@5a
    move-result v1

    #@5b
    if-eqz v1, :cond_4c

    #@5d
    .line 61
    iget-object v1, p0, Lcom/lge/autorun/Autorun_VZW$1;->this$0:Lcom/lge/autorun/Autorun_VZW;

    #@5f
    invoke-static {v1, v3}, Lcom/lge/autorun/Autorun_VZW;->access$102(Lcom/lge/autorun/Autorun_VZW;Z)Z

    #@62
    .line 62
    iget-object v1, p0, Lcom/lge/autorun/Autorun_VZW$1;->this$0:Lcom/lge/autorun/Autorun_VZW;

    #@64
    invoke-static {v1}, Lcom/lge/autorun/Autorun_VZW;->access$200(Lcom/lge/autorun/Autorun_VZW;)Z

    #@67
    move-result v1

    #@68
    if-eqz v1, :cond_4c

    #@6a
    .line 63
    iget-object v1, p0, Lcom/lge/autorun/Autorun_VZW$1;->this$0:Lcom/lge/autorun/Autorun_VZW;

    #@6c
    invoke-static {v1, v3}, Lcom/lge/autorun/Autorun_VZW;->access$202(Lcom/lge/autorun/Autorun_VZW;Z)Z

    #@6f
    .line 65
    iget-object v1, p0, Lcom/lge/autorun/Autorun_VZW$1;->this$0:Lcom/lge/autorun/Autorun_VZW;

    #@71
    const-string v2, "cdrom_storage"

    #@73
    invoke-virtual {v1, v2, v3}, Lcom/lge/autorun/Autorun_VZW;->setCurrentFunction(Ljava/lang/String;Z)V

    #@76
    .line 66
    iget-object v1, p0, Lcom/lge/autorun/Autorun_VZW$1;->this$0:Lcom/lge/autorun/Autorun_VZW;

    #@78
    invoke-virtual {v1}, Lcom/lge/autorun/Autorun_VZW;->getUsbUnPlugState()Z

    #@7b
    move-result v1

    #@7c
    if-eqz v1, :cond_8d

    #@7e
    .line 67
    const-string v1, "AUTORUN"

    #@80
    const-string v2, "[AUTORUN] setCurrentFunction(cdrom_storage,false)::onReceive when lock released"

    #@82
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@85
    .line 68
    iget-object v1, p0, Lcom/lge/autorun/Autorun_VZW$1;->this$0:Lcom/lge/autorun/Autorun_VZW;

    #@87
    const-string v2, "cdrom_storage"

    #@89
    invoke-virtual {v1, v2, v3}, Lcom/lge/autorun/Autorun_VZW;->setCurrentFunction(Ljava/lang/String;Z)V

    #@8c
    goto :goto_4c

    #@8d
    .line 70
    :cond_8d
    const-string v1, "AUTORUN"

    #@8f
    const-string v2, "[AUTORUN] autorunStartTimer is called::onReceive when lock released"

    #@91
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@94
    .line 71
    iget-object v1, p0, Lcom/lge/autorun/Autorun_VZW$1;->this$0:Lcom/lge/autorun/Autorun_VZW;

    #@96
    invoke-virtual {v1}, Lcom/lge/autorun/Autorun_VZW;->autorunStartTimer()V

    #@99
    goto :goto_4c
.end method
