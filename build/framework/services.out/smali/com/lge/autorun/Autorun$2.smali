.class Lcom/lge/autorun/Autorun$2;
.super Landroid/os/UEventObserver;
.source "Autorun.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/autorun/Autorun;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/autorun/Autorun;


# direct methods
.method constructor <init>(Lcom/lge/autorun/Autorun;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 173
    iput-object p1, p0, Lcom/lge/autorun/Autorun$2;->this$0:Lcom/lge/autorun/Autorun;

    #@2
    invoke-direct {p0}, Landroid/os/UEventObserver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onUEvent(Landroid/os/UEventObserver$UEvent;)V
    .registers 9
    .parameter "event"

    #@0
    .prologue
    const/high16 v6, 0x6000

    #@2
    .line 177
    const-string v3, "AUTORUN"

    #@4
    new-instance v4, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v5, "[AUTORUN] USB UEVENT: "

    #@b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v4

    #@f
    invoke-virtual {p1}, Landroid/os/UEventObserver$UEvent;->toString()Ljava/lang/String;

    #@12
    move-result-object v5

    #@13
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v4

    #@17
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v4

    #@1b
    invoke-static {v3, v4}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 178
    const-string v3, "AUTORUN"

    #@20
    invoke-virtual {p1, v3}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    #@23
    move-result-object v0

    #@24
    .line 180
    .local v0, autorun:Ljava/lang/String;
    const-string v3, "USB_DRV"

    #@26
    invoke-virtual {p1, v3}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    #@29
    move-result-object v2

    #@2a
    .line 183
    .local v2, usb_drv:Ljava/lang/String;
    if-eqz v0, :cond_37

    #@2c
    .line 184
    iget-object v3, p0, Lcom/lge/autorun/Autorun$2;->this$0:Lcom/lge/autorun/Autorun;

    #@2e
    invoke-static {v3}, Lcom/lge/autorun/Autorun;->access$200(Lcom/lge/autorun/Autorun;)Lcom/lge/autorun/Autorun$UsbHandler;

    #@31
    move-result-object v3

    #@32
    const/4 v4, 0x2

    #@33
    invoke-virtual {v3, v4, v0}, Lcom/lge/autorun/Autorun$UsbHandler;->sendMessage(ILjava/lang/Object;)V

    #@36
    .line 203
    :cond_36
    :goto_36
    return-void

    #@37
    .line 187
    :cond_37
    if-eqz v2, :cond_36

    #@39
    .line 188
    const-string v3, "uninstalled"

    #@3b
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3e
    move-result v3

    #@3f
    if-eqz v3, :cond_60

    #@41
    .line 189
    const-string v3, "AUTORUN"

    #@43
    const-string v4, "USB driver is not installed"

    #@45
    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    .line 190
    new-instance v1, Landroid/content/Intent;

    #@4a
    const-string v3, "com.lge.intent.action.USB_DRIVER_INSTALLED"

    #@4c
    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@4f
    .line 191
    .local v1, intent:Landroid/content/Intent;
    invoke-virtual {v1, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@52
    .line 192
    const-string v3, "installed"

    #@54
    const/4 v4, 0x0

    #@55
    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@58
    .line 193
    iget-object v3, p0, Lcom/lge/autorun/Autorun$2;->this$0:Lcom/lge/autorun/Autorun;

    #@5a
    iget-object v3, v3, Lcom/lge/autorun/Autorun;->mContext:Landroid/content/Context;

    #@5c
    invoke-virtual {v3, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@5f
    goto :goto_36

    #@60
    .line 194
    .end local v1           #intent:Landroid/content/Intent;
    :cond_60
    const-string v3, "installed"

    #@62
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@65
    move-result v3

    #@66
    if-eqz v3, :cond_36

    #@68
    .line 195
    const-string v3, "AUTORUN"

    #@6a
    const-string v4, "USB driver is installed"

    #@6c
    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6f
    .line 196
    new-instance v1, Landroid/content/Intent;

    #@71
    const-string v3, "com.lge.intent.action.USB_DRIVER_INSTALLED"

    #@73
    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@76
    .line 197
    .restart local v1       #intent:Landroid/content/Intent;
    invoke-virtual {v1, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@79
    .line 198
    const-string v3, "installed"

    #@7b
    const/4 v4, 0x1

    #@7c
    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@7f
    .line 199
    iget-object v3, p0, Lcom/lge/autorun/Autorun$2;->this$0:Lcom/lge/autorun/Autorun;

    #@81
    iget-object v3, v3, Lcom/lge/autorun/Autorun;->mContext:Landroid/content/Context;

    #@83
    invoke-virtual {v3, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@86
    goto :goto_36
.end method
