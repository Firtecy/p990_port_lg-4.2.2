.class public Lcom/lge/autorun/Autorun;
.super Ljava/lang/Object;
.source "Autorun.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/autorun/Autorun$autorunTimerTask;,
        Lcom/lge/autorun/Autorun$UsbPlugCheckTimerTask;,
        Lcom/lge/autorun/Autorun$UsbHandler;
    }
.end annotation


# static fields
.field public static AUTORUN_DELAY_TIME:I = 0x0

.field private static final AUTORUN_MATCH:Ljava/lang/String; = "DEVPATH=/devices/virtual/misc/usb_autorun"

.field private static final DEBUG:Z = false

.field private static HARD_DISCONNECTED_DELAY_TIME:I = 0x0

.field private static final MSG_AUTORUN_UPDATE:I = 0x2

.field private static final MSG_USB_PLUG:I = 0x1

.field private static final MSG_USB_UNPLUG:I = 0x0

.field private static final STATE_PATH:Ljava/lang/String; = "/sys/class/android_usb/android0/state"

.field private static final USB_STATE_PATH:Ljava/lang/String; = "DEVPATH=/devices/virtual/android_usb/android0"


# instance fields
.field private final ACTION_USB_CONNECTED_POPUP:Ljava/lang/String;

.field public final TAG:Ljava/lang/String;

.field private mAutorunObservered:Z

.field mAutorunTimer:Ljava/util/Timer;

.field private mAutorunUEventObserver:Landroid/os/UEventObserver;

.field private mBootCompleted:Z

.field protected final mContext:Landroid/content/Context;

.field private mDeviceManager:Lcom/android/server/usb/UsbDeviceManager;

.field private mHandler:Lcom/lge/autorun/Autorun$UsbHandler;

.field private mSetupWizardRunning:Z

.field mUsbPlugTimer:Ljava/util/Timer;

.field private mUsbStateObservered:Z

.field mUsbStateReceiver:Landroid/content/BroadcastReceiver;

.field private mUsbStateUEventObserver:Landroid/os/UEventObserver;

.field private mUsbUnPlugged:Z

.field private final setupwizard_trigger:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 38
    const/16 v0, 0x12c

    #@2
    sput v0, Lcom/lge/autorun/Autorun;->HARD_DISCONNECTED_DELAY_TIME:I

    #@4
    .line 54
    const/16 v0, 0x7530

    #@6
    sput v0, Lcom/lge/autorun/Autorun;->AUTORUN_DELAY_TIME:I

    #@8
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 11
    .parameter "context"

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    const/4 v7, 0x0

    #@2
    const/4 v6, 0x0

    #@3
    .line 65
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 33
    const-string v5, "AUTORUN"

    #@8
    iput-object v5, p0, Lcom/lge/autorun/Autorun;->TAG:Ljava/lang/String;

    #@a
    .line 34
    const-string v5, "com.lge.setup_wizard.AUTORUNON"

    #@c
    iput-object v5, p0, Lcom/lge/autorun/Autorun;->setupwizard_trigger:Ljava/lang/String;

    #@e
    .line 35
    iput-object v7, p0, Lcom/lge/autorun/Autorun;->mUsbPlugTimer:Ljava/util/Timer;

    #@10
    .line 36
    iput-object v7, p0, Lcom/lge/autorun/Autorun;->mAutorunTimer:Ljava/util/Timer;

    #@12
    .line 37
    iput-object v7, p0, Lcom/lge/autorun/Autorun;->mUsbStateUEventObserver:Landroid/os/UEventObserver;

    #@14
    .line 39
    iput-boolean v8, p0, Lcom/lge/autorun/Autorun;->mUsbUnPlugged:Z

    #@16
    .line 41
    iput-boolean v6, p0, Lcom/lge/autorun/Autorun;->mBootCompleted:Z

    #@18
    .line 42
    const-string v5, "com.lge.android.server.usb_connected_popup"

    #@1a
    iput-object v5, p0, Lcom/lge/autorun/Autorun;->ACTION_USB_CONNECTED_POPUP:Ljava/lang/String;

    #@1c
    .line 44
    iput-boolean v6, p0, Lcom/lge/autorun/Autorun;->mAutorunObservered:Z

    #@1e
    .line 45
    iput-boolean v6, p0, Lcom/lge/autorun/Autorun;->mUsbStateObservered:Z

    #@20
    .line 63
    iput-boolean v6, p0, Lcom/lge/autorun/Autorun;->mSetupWizardRunning:Z

    #@22
    .line 147
    new-instance v5, Lcom/lge/autorun/Autorun$1;

    #@24
    invoke-direct {v5, p0}, Lcom/lge/autorun/Autorun$1;-><init>(Lcom/lge/autorun/Autorun;)V

    #@27
    iput-object v5, p0, Lcom/lge/autorun/Autorun;->mUsbStateReceiver:Landroid/content/BroadcastReceiver;

    #@29
    .line 173
    new-instance v5, Lcom/lge/autorun/Autorun$2;

    #@2b
    invoke-direct {v5, p0}, Lcom/lge/autorun/Autorun$2;-><init>(Lcom/lge/autorun/Autorun;)V

    #@2e
    iput-object v5, p0, Lcom/lge/autorun/Autorun;->mAutorunUEventObserver:Landroid/os/UEventObserver;

    #@30
    .line 66
    iput-object p1, p0, Lcom/lge/autorun/Autorun;->mContext:Landroid/content/Context;

    #@32
    .line 68
    new-instance v1, Landroid/content/IntentFilter;

    #@34
    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    #@37
    .line 69
    .local v1, filter:Landroid/content/IntentFilter;
    const-string v5, "com.lge.setup_wizard.AUTORUNON"

    #@39
    invoke-virtual {v1, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@3c
    .line 70
    const-string v5, "android.intent.action.BOOT_COMPLETED"

    #@3e
    invoke-virtual {v1, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@41
    .line 71
    iget-object v5, p0, Lcom/lge/autorun/Autorun;->mContext:Landroid/content/Context;

    #@43
    iget-object v6, p0, Lcom/lge/autorun/Autorun;->mUsbStateReceiver:Landroid/content/BroadcastReceiver;

    #@45
    invoke-virtual {v5, v6, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@48
    .line 74
    :try_start_48
    new-instance v5, Ljava/io/File;

    #@4a
    const-string v6, "/sys/class/android_usb/android0/state"

    #@4c
    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@4f
    const/4 v6, 0x0

    #@50
    const/4 v7, 0x0

    #@51
    invoke-static {v5, v6, v7}, Landroid/os/FileUtils;->readTextFile(Ljava/io/File;ILjava/lang/String;)Ljava/lang/String;

    #@54
    move-result-object v5

    #@55
    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@58
    move-result-object v3

    #@59
    .line 75
    .local v3, state:Ljava/lang/String;
    const-string v5, "DISCONNECTED"

    #@5b
    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5e
    move-result v5

    #@5f
    if-eqz v5, :cond_87

    #@61
    .line 76
    const/4 v5, 0x1

    #@62
    iput-boolean v5, p0, Lcom/lge/autorun/Autorun;->mUsbUnPlugged:Z
    :try_end_64
    .catch Ljava/lang/Exception; {:try_start_48 .. :try_end_64} :catch_8b

    #@64
    .line 84
    .end local v3           #state:Ljava/lang/String;
    :goto_64
    new-instance v4, Landroid/os/HandlerThread;

    #@66
    const-string v5, "Autorun"

    #@68
    const/16 v6, 0xa

    #@6a
    invoke-direct {v4, v5, v6}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    #@6d
    .line 85
    .local v4, thread:Landroid/os/HandlerThread;
    invoke-virtual {v4}, Landroid/os/HandlerThread;->start()V

    #@70
    .line 88
    invoke-virtual {v4}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@73
    move-result-object v2

    #@74
    .line 90
    .local v2, looper:Landroid/os/Looper;
    :goto_74
    if-nez v2, :cond_ab

    #@76
    .line 91
    const-string v5, "AUTORUN"

    #@78
    const-string v6, "[AUTORUN] Thread looper is not ready, which waits 100 ms."

    #@7a
    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7d
    .line 93
    const-wide/16 v5, 0x32

    #@7f
    :try_start_7f
    invoke-static {v5, v6}, Ljava/lang/Thread;->sleep(J)V
    :try_end_82
    .catch Ljava/lang/InterruptedException; {:try_start_7f .. :try_end_82} :catch_b6

    #@82
    .line 96
    :goto_82
    invoke-virtual {v4}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@85
    move-result-object v2

    #@86
    goto :goto_74

    #@87
    .line 78
    .end local v2           #looper:Landroid/os/Looper;
    .end local v4           #thread:Landroid/os/HandlerThread;
    .restart local v3       #state:Ljava/lang/String;
    :cond_87
    const/4 v5, 0x0

    #@88
    :try_start_88
    iput-boolean v5, p0, Lcom/lge/autorun/Autorun;->mUsbUnPlugged:Z
    :try_end_8a
    .catch Ljava/lang/Exception; {:try_start_88 .. :try_end_8a} :catch_8b

    #@8a
    goto :goto_64

    #@8b
    .line 81
    .end local v3           #state:Ljava/lang/String;
    :catch_8b
    move-exception v0

    #@8c
    .line 82
    .local v0, e:Ljava/lang/Exception;
    const-string v5, "AUTORUN"

    #@8e
    new-instance v6, Ljava/lang/StringBuilder;

    #@90
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@93
    const-string v7, "[AUTORUN] Exception is ocurred when read state ["

    #@95
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@98
    move-result-object v6

    #@99
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v6

    #@9d
    const-string v7, "]"

    #@9f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v6

    #@a3
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a6
    move-result-object v6

    #@a7
    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@aa
    goto :goto_64

    #@ab
    .line 98
    .end local v0           #e:Ljava/lang/Exception;
    .restart local v2       #looper:Landroid/os/Looper;
    .restart local v4       #thread:Landroid/os/HandlerThread;
    :cond_ab
    new-instance v5, Lcom/lge/autorun/Autorun$UsbHandler;

    #@ad
    invoke-direct {v5, p0, v2}, Lcom/lge/autorun/Autorun$UsbHandler;-><init>(Lcom/lge/autorun/Autorun;Landroid/os/Looper;)V

    #@b0
    iput-object v5, p0, Lcom/lge/autorun/Autorun;->mHandler:Lcom/lge/autorun/Autorun$UsbHandler;

    #@b2
    .line 103
    invoke-virtual {p0}, Lcom/lge/autorun/Autorun;->callUsbPlugFunction()V

    #@b5
    .line 104
    return-void

    #@b6
    .line 94
    :catch_b6
    move-exception v5

    #@b7
    goto :goto_82
.end method

.method static synthetic access$002(Lcom/lge/autorun/Autorun;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 30
    iput-boolean p1, p0, Lcom/lge/autorun/Autorun;->mBootCompleted:Z

    #@2
    return p1
.end method

.method static synthetic access$102(Lcom/lge/autorun/Autorun;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 30
    iput-boolean p1, p0, Lcom/lge/autorun/Autorun;->mSetupWizardRunning:Z

    #@2
    return p1
.end method

.method static synthetic access$200(Lcom/lge/autorun/Autorun;)Lcom/lge/autorun/Autorun$UsbHandler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 30
    iget-object v0, p0, Lcom/lge/autorun/Autorun;->mHandler:Lcom/lge/autorun/Autorun$UsbHandler;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/lge/autorun/Autorun;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/lge/autorun/Autorun;->mAutorunObservered:Z

    #@2
    return v0
.end method

.method static synthetic access$302(Lcom/lge/autorun/Autorun;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 30
    iput-boolean p1, p0, Lcom/lge/autorun/Autorun;->mAutorunObservered:Z

    #@2
    return p1
.end method

.method static synthetic access$400(Lcom/lge/autorun/Autorun;)Landroid/os/UEventObserver;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 30
    iget-object v0, p0, Lcom/lge/autorun/Autorun;->mAutorunUEventObserver:Landroid/os/UEventObserver;

    #@2
    return-object v0
.end method

.method static synthetic access$502(Lcom/lge/autorun/Autorun;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 30
    iput-boolean p1, p0, Lcom/lge/autorun/Autorun;->mUsbUnPlugged:Z

    #@2
    return p1
.end method

.method static synthetic access$600(Lcom/lge/autorun/Autorun;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/lge/autorun/Autorun;->judgeUsbPlugState(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method private judgeUsbPlugState(Ljava/lang/String;)V
    .registers 3
    .parameter "state"

    #@0
    .prologue
    .line 329
    const-string v0, "DISCONNECTED"

    #@2
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_c

    #@8
    .line 330
    invoke-direct {p0}, Lcom/lge/autorun/Autorun;->startUsbPlugTimer()V

    #@b
    .line 334
    :goto_b
    return-void

    #@c
    .line 332
    :cond_c
    invoke-direct {p0}, Lcom/lge/autorun/Autorun;->stopUsbPlugTimer()V

    #@f
    goto :goto_b
.end method

.method private startUsbPlugTimer()V
    .registers 6

    #@0
    .prologue
    .line 337
    iget-object v1, p0, Lcom/lge/autorun/Autorun;->mUsbPlugTimer:Ljava/util/Timer;

    #@2
    if-eqz v1, :cond_c

    #@4
    .line 338
    iget-object v1, p0, Lcom/lge/autorun/Autorun;->mUsbPlugTimer:Ljava/util/Timer;

    #@6
    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    #@9
    .line 339
    const/4 v1, 0x0

    #@a
    iput-object v1, p0, Lcom/lge/autorun/Autorun;->mUsbPlugTimer:Ljava/util/Timer;

    #@c
    .line 341
    :cond_c
    new-instance v1, Ljava/util/Timer;

    #@e
    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    #@11
    iput-object v1, p0, Lcom/lge/autorun/Autorun;->mUsbPlugTimer:Ljava/util/Timer;

    #@13
    .line 343
    :try_start_13
    iget-object v1, p0, Lcom/lge/autorun/Autorun;->mUsbPlugTimer:Ljava/util/Timer;

    #@15
    new-instance v2, Lcom/lge/autorun/Autorun$UsbPlugCheckTimerTask;

    #@17
    invoke-direct {v2, p0}, Lcom/lge/autorun/Autorun$UsbPlugCheckTimerTask;-><init>(Lcom/lge/autorun/Autorun;)V

    #@1a
    sget v3, Lcom/lge/autorun/Autorun;->HARD_DISCONNECTED_DELAY_TIME:I

    #@1c
    int-to-long v3, v3

    #@1d
    invoke-virtual {v1, v2, v3, v4}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_20
    .catch Ljava/lang/IllegalStateException; {:try_start_13 .. :try_end_20} :catch_21

    #@20
    .line 347
    :goto_20
    return-void

    #@21
    .line 344
    :catch_21
    move-exception v0

    #@22
    .line 345
    .local v0, e:Ljava/lang/IllegalStateException;
    const-string v1, "AUTORUN"

    #@24
    const-string v2, "[AUTORUN] error!! Timer already have been canceled. but not treate it"

    #@26
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    goto :goto_20
.end method

.method private stopUsbPlugTimer()V
    .registers 3

    #@0
    .prologue
    .line 350
    iget-object v0, p0, Lcom/lge/autorun/Autorun;->mUsbPlugTimer:Ljava/util/Timer;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 351
    iget-object v0, p0, Lcom/lge/autorun/Autorun;->mUsbPlugTimer:Ljava/util/Timer;

    #@6
    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    #@9
    .line 352
    const/4 v0, 0x0

    #@a
    iput-object v0, p0, Lcom/lge/autorun/Autorun;->mUsbPlugTimer:Ljava/util/Timer;

    #@c
    .line 354
    :cond_c
    iget-boolean v0, p0, Lcom/lge/autorun/Autorun;->mUsbUnPlugged:Z

    #@e
    if-eqz v0, :cond_19

    #@10
    .line 356
    const/4 v0, 0x0

    #@11
    iput-boolean v0, p0, Lcom/lge/autorun/Autorun;->mUsbUnPlugged:Z

    #@13
    .line 357
    iget-object v0, p0, Lcom/lge/autorun/Autorun;->mHandler:Lcom/lge/autorun/Autorun$UsbHandler;

    #@15
    const/4 v1, 0x1

    #@16
    invoke-virtual {v0, v1}, Lcom/lge/autorun/Autorun$UsbHandler;->sendMessage(I)V

    #@19
    .line 359
    :cond_19
    return-void
.end method


# virtual methods
.method public addUsbDeviceManager(Lcom/android/server/usb/UsbDeviceManager;)V
    .registers 2
    .parameter "mgr"

    #@0
    .prologue
    .line 110
    iput-object p1, p0, Lcom/lge/autorun/Autorun;->mDeviceManager:Lcom/android/server/usb/UsbDeviceManager;

    #@2
    .line 111
    return-void
.end method

.method protected autorunStartTimer()V
    .registers 6

    #@0
    .prologue
    .line 431
    invoke-virtual {p0}, Lcom/lge/autorun/Autorun;->autorunStopTimer()V

    #@3
    .line 433
    new-instance v1, Ljava/util/Timer;

    #@5
    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    #@8
    iput-object v1, p0, Lcom/lge/autorun/Autorun;->mAutorunTimer:Ljava/util/Timer;

    #@a
    .line 437
    :try_start_a
    iget-object v1, p0, Lcom/lge/autorun/Autorun;->mAutorunTimer:Ljava/util/Timer;

    #@c
    new-instance v2, Lcom/lge/autorun/Autorun$autorunTimerTask;

    #@e
    invoke-direct {v2, p0}, Lcom/lge/autorun/Autorun$autorunTimerTask;-><init>(Lcom/lge/autorun/Autorun;)V

    #@11
    sget v3, Lcom/lge/autorun/Autorun;->AUTORUN_DELAY_TIME:I

    #@13
    int-to-long v3, v3

    #@14
    invoke-virtual {v1, v2, v3, v4}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_17
    .catch Ljava/lang/IllegalStateException; {:try_start_a .. :try_end_17} :catch_18

    #@17
    .line 443
    :goto_17
    return-void

    #@18
    .line 438
    :catch_18
    move-exception v0

    #@19
    .line 439
    .local v0, e:Ljava/lang/IllegalStateException;
    const-string v1, "AUTORUN"

    #@1b
    const-string v2, "[AUTORUN] error!! Timer already have been canceled. but not treate it"

    #@1d
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    goto :goto_17
.end method

.method protected autorunStopTimer()V
    .registers 2

    #@0
    .prologue
    .line 445
    iget-object v0, p0, Lcom/lge/autorun/Autorun;->mAutorunTimer:Ljava/util/Timer;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 447
    iget-object v0, p0, Lcom/lge/autorun/Autorun;->mAutorunTimer:Ljava/util/Timer;

    #@6
    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    #@9
    .line 448
    const/4 v0, 0x0

    #@a
    iput-object v0, p0, Lcom/lge/autorun/Autorun;->mAutorunTimer:Ljava/util/Timer;

    #@c
    .line 450
    :cond_c
    return-void
.end method

.method protected callUsbPlugFunction()V
    .registers 3

    #@0
    .prologue
    .line 370
    new-instance v0, Lcom/lge/autorun/Autorun$3;

    #@2
    invoke-direct {v0, p0}, Lcom/lge/autorun/Autorun$3;-><init>(Lcom/lge/autorun/Autorun;)V

    #@5
    iput-object v0, p0, Lcom/lge/autorun/Autorun;->mUsbStateUEventObserver:Landroid/os/UEventObserver;

    #@7
    .line 380
    iget-object v0, p0, Lcom/lge/autorun/Autorun;->mUsbStateUEventObserver:Landroid/os/UEventObserver;

    #@9
    const-string v1, "DEVPATH=/devices/virtual/android_usb/android0"

    #@b
    invoke-virtual {v0, v1}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V

    #@e
    .line 381
    const/4 v0, 0x1

    #@f
    iput-boolean v0, p0, Lcom/lge/autorun/Autorun;->mUsbStateObservered:Z

    #@11
    .line 382
    return-void
.end method

.method public distroy()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 117
    iget-boolean v0, p0, Lcom/lge/autorun/Autorun;->mAutorunObservered:Z

    #@3
    if-eqz v0, :cond_10

    #@5
    iget-object v0, p0, Lcom/lge/autorun/Autorun;->mAutorunUEventObserver:Landroid/os/UEventObserver;

    #@7
    if-eqz v0, :cond_10

    #@9
    .line 118
    iget-object v0, p0, Lcom/lge/autorun/Autorun;->mAutorunUEventObserver:Landroid/os/UEventObserver;

    #@b
    invoke-virtual {v0}, Landroid/os/UEventObserver;->stopObserving()V

    #@e
    .line 119
    iput-boolean v1, p0, Lcom/lge/autorun/Autorun;->mAutorunObservered:Z

    #@10
    .line 121
    :cond_10
    iget-boolean v0, p0, Lcom/lge/autorun/Autorun;->mUsbStateObservered:Z

    #@12
    if-eqz v0, :cond_1f

    #@14
    iget-object v0, p0, Lcom/lge/autorun/Autorun;->mUsbStateUEventObserver:Landroid/os/UEventObserver;

    #@16
    if-eqz v0, :cond_1f

    #@18
    .line 122
    iget-object v0, p0, Lcom/lge/autorun/Autorun;->mUsbStateUEventObserver:Landroid/os/UEventObserver;

    #@1a
    invoke-virtual {v0}, Landroid/os/UEventObserver;->stopObserving()V

    #@1d
    .line 123
    iput-boolean v1, p0, Lcom/lge/autorun/Autorun;->mUsbStateObservered:Z

    #@1f
    .line 127
    :cond_1f
    return-void
.end method

.method public getCurrentFunction()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 319
    const-string v2, "sys.usb.config"

    #@2
    const-string v3, ""

    #@4
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v1

    #@8
    .line 320
    .local v1, functions:Ljava/lang/String;
    const/16 v2, 0x2c

    #@a
    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(I)I

    #@d
    move-result v0

    #@e
    .line 321
    .local v0, commaIndex:I
    if-lez v0, :cond_15

    #@10
    .line 322
    const/4 v2, 0x0

    #@11
    invoke-virtual {v1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    .line 324
    .end local v1           #functions:Ljava/lang/String;
    :cond_15
    return-object v1
.end method

.method public getIsBootCompleted()Z
    .registers 2

    #@0
    .prologue
    .line 388
    iget-boolean v0, p0, Lcom/lge/autorun/Autorun;->mBootCompleted:Z

    #@2
    return v0
.end method

.method public getIsSetupWizardRunning()Z
    .registers 10

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 396
    :try_start_2
    iget-object v6, p0, Lcom/lge/autorun/Autorun;->mContext:Landroid/content/Context;

    #@4
    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@7
    move-result-object v3

    #@8
    .line 397
    .local v3, pm:Landroid/content/pm/PackageManager;
    const-string v6, "com.android.LGSetupWizard"

    #@a
    invoke-virtual {v3, v6}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I

    #@d
    move-result v2

    #@e
    .line 398
    .local v2, mComponentState:I
    iget-object v6, p0, Lcom/lge/autorun/Autorun;->mContext:Landroid/content/Context;

    #@10
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@13
    move-result-object v6

    #@14
    const-string v7, "device_provisioned"

    #@16
    const/4 v8, 0x0

    #@17
    invoke-static {v6, v7, v8}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I
    :try_end_1a
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_1a} :catch_29

    #@1a
    move-result v6

    #@1b
    if-lez v6, :cond_27

    #@1d
    move v1, v4

    #@1e
    .line 399
    .local v1, isProvisioned:Z
    :goto_1e
    const/4 v6, 0x2

    #@1f
    if-eq v2, v6, :cond_25

    #@21
    if-nez v2, :cond_26

    #@23
    if-eqz v1, :cond_26

    #@25
    :cond_25
    move v4, v5

    #@26
    .line 406
    .end local v1           #isProvisioned:Z
    .end local v2           #mComponentState:I
    .end local v3           #pm:Landroid/content/pm/PackageManager;
    :cond_26
    :goto_26
    return v4

    #@27
    .restart local v2       #mComponentState:I
    .restart local v3       #pm:Landroid/content/pm/PackageManager;
    :cond_27
    move v1, v5

    #@28
    .line 398
    goto :goto_1e

    #@29
    .line 404
    .end local v2           #mComponentState:I
    .end local v3           #pm:Landroid/content/pm/PackageManager;
    :catch_29
    move-exception v0

    #@2a
    .line 405
    .local v0, e:Ljava/lang/Exception;
    const-string v5, "AUTORUN"

    #@2c
    const-string v6, "[AUTORUN] error is occurred when getApplicationEnabledSetting"

    #@2e
    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@31
    goto :goto_26
.end method

.method public getSetupWizardState()Z
    .registers 2

    #@0
    .prologue
    .line 312
    iget-boolean v0, p0, Lcom/lge/autorun/Autorun;->mSetupWizardRunning:Z

    #@2
    return v0
.end method

.method public getUsbUnPlugState()Z
    .registers 2

    #@0
    .prologue
    .line 305
    iget-boolean v0, p0, Lcom/lge/autorun/Autorun;->mUsbUnPlugged:Z

    #@2
    return v0
.end method

.method protected onAutorunUpdateState(Ljava/lang/String;)V
    .registers 8
    .parameter "state"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 249
    const/4 v0, 0x0

    #@2
    .line 250
    .local v0, alwaysAsk:Z
    const/4 v2, 0x0

    #@3
    .line 252
    .local v2, mDefaultFunctions:Ljava/lang/String;
    const-string v3, "ACK"

    #@5
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8
    move-result v3

    #@9
    if-eqz v3, :cond_f

    #@b
    .line 253
    invoke-virtual {p0}, Lcom/lge/autorun/Autorun;->autorunStopTimer()V

    #@e
    .line 293
    :goto_e
    return-void

    #@f
    .line 256
    :cond_f
    iget-object v3, p0, Lcom/lge/autorun/Autorun;->mAutorunTimer:Ljava/util/Timer;

    #@11
    if-eqz v3, :cond_16

    #@13
    .line 257
    invoke-virtual {p0}, Lcom/lge/autorun/Autorun;->autorunStopTimer()V

    #@16
    .line 260
    :cond_16
    const-string v3, "change_ask"

    #@18
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1b
    move-result v3

    #@1c
    if-eqz v3, :cond_46

    #@1e
    .line 262
    if-ne v0, v5, :cond_3e

    #@20
    .line 264
    const-string v2, "charge_only"

    #@22
    .line 285
    :cond_22
    :goto_22
    const-wide/16 v3, 0x32

    #@24
    :try_start_24
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_27
    .catch Ljava/lang/InterruptedException; {:try_start_24 .. :try_end_27} :catch_88

    #@27
    .line 290
    :goto_27
    const-string v3, "AUTORUN"

    #@29
    const-string v4, "[AUTORUN] call setCurrentFunction by UsbDeviceManager #9"

    #@2b
    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    .line 291
    invoke-virtual {p0, v2, v5}, Lcom/lge/autorun/Autorun;->setCurrentFunction(Ljava/lang/String;Z)V

    #@31
    .line 292
    iget-object v3, p0, Lcom/lge/autorun/Autorun;->mContext:Landroid/content/Context;

    #@33
    new-instance v4, Landroid/content/Intent;

    #@35
    const-string v5, "com.lge.android.server.usb_connected_popup"

    #@37
    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@3a
    invoke-virtual {v3, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@3d
    goto :goto_e

    #@3e
    .line 267
    :cond_3e
    const-string v3, "AUTORUN"

    #@40
    const-string v4, "[AUTORUN] alwaysAsk false"

    #@42
    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    goto :goto_22

    #@46
    .line 269
    :cond_46
    const-string v3, "change_acm"

    #@48
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4b
    move-result v3

    #@4c
    if-eqz v3, :cond_51

    #@4e
    .line 270
    const-string v2, "pc_suite"

    #@50
    goto :goto_22

    #@51
    .line 271
    :cond_51
    const-string v3, "change_mtp"

    #@53
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@56
    move-result v3

    #@57
    if-eqz v3, :cond_5c

    #@59
    .line 272
    const-string v2, "mtp_only"

    #@5b
    goto :goto_22

    #@5c
    .line 273
    :cond_5c
    const-string v3, "change_ptp"

    #@5e
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@61
    move-result v3

    #@62
    if-eqz v3, :cond_67

    #@64
    .line 274
    const-string v2, "ptp_only"

    #@66
    goto :goto_22

    #@67
    .line 275
    :cond_67
    const-string v3, "change_charge"

    #@69
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6c
    move-result v3

    #@6d
    if-eqz v3, :cond_72

    #@6f
    .line 276
    const-string v2, "charge_only"

    #@71
    goto :goto_22

    #@72
    .line 277
    :cond_72
    const-string v3, "change_tether"

    #@74
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@77
    move-result v3

    #@78
    if-eqz v3, :cond_7d

    #@7a
    .line 278
    const-string v2, "ecm"

    #@7c
    goto :goto_22

    #@7d
    .line 279
    :cond_7d
    const-string v3, "TIMEOUT"

    #@7f
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@82
    move-result v3

    #@83
    if-eqz v3, :cond_22

    #@85
    .line 281
    const-string v2, "mtp_only"

    #@87
    goto :goto_22

    #@88
    .line 286
    :catch_88
    move-exception v1

    #@89
    .line 287
    .local v1, e:Ljava/lang/InterruptedException;
    const-string v3, "AUTORUN"

    #@8b
    const-string v4, "[AUTORUN] Exception wait for termination of PC Launcher, so just skip it"

    #@8d
    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@90
    goto :goto_27
.end method

.method protected onBootComplete()V
    .registers 1

    #@0
    .prologue
    .line 165
    return-void
.end method

.method protected onSetupWizardEnd()V
    .registers 3

    #@0
    .prologue
    .line 169
    const-string v0, "sys.usb.provisioned"

    #@2
    const-string v1, "1"

    #@4
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 170
    const/4 v0, 0x0

    #@8
    iput-boolean v0, p0, Lcom/lge/autorun/Autorun;->mSetupWizardRunning:Z

    #@a
    .line 171
    return-void
.end method

.method protected onUsbPlugged()V
    .registers 1

    #@0
    .prologue
    .line 299
    return-void
.end method

.method protected onUsbUnPlugged()V
    .registers 1

    #@0
    .prologue
    .line 296
    return-void
.end method

.method public resume()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 133
    iget-boolean v0, p0, Lcom/lge/autorun/Autorun;->mAutorunObservered:Z

    #@3
    if-nez v0, :cond_12

    #@5
    iget-object v0, p0, Lcom/lge/autorun/Autorun;->mAutorunUEventObserver:Landroid/os/UEventObserver;

    #@7
    if-eqz v0, :cond_12

    #@9
    .line 134
    iget-object v0, p0, Lcom/lge/autorun/Autorun;->mAutorunUEventObserver:Landroid/os/UEventObserver;

    #@b
    const-string v1, "DEVPATH=/devices/virtual/misc/usb_autorun"

    #@d
    invoke-virtual {v0, v1}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V

    #@10
    .line 135
    iput-boolean v2, p0, Lcom/lge/autorun/Autorun;->mAutorunObservered:Z

    #@12
    .line 137
    :cond_12
    iget-boolean v0, p0, Lcom/lge/autorun/Autorun;->mUsbStateObservered:Z

    #@14
    if-nez v0, :cond_23

    #@16
    iget-object v0, p0, Lcom/lge/autorun/Autorun;->mUsbStateUEventObserver:Landroid/os/UEventObserver;

    #@18
    if-eqz v0, :cond_23

    #@1a
    .line 138
    iget-object v0, p0, Lcom/lge/autorun/Autorun;->mUsbStateUEventObserver:Landroid/os/UEventObserver;

    #@1c
    const-string v1, "DEVPATH=/devices/virtual/android_usb/android0"

    #@1e
    invoke-virtual {v0, v1}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V

    #@21
    .line 139
    iput-boolean v2, p0, Lcom/lge/autorun/Autorun;->mUsbStateObservered:Z

    #@23
    .line 141
    :cond_23
    return-void
.end method

.method public setCurrentFunction(Ljava/lang/String;Z)V
    .registers 4
    .parameter "function"
    .parameter "makeDefault"

    #@0
    .prologue
    .line 414
    iget-object v0, p0, Lcom/lge/autorun/Autorun;->mDeviceManager:Lcom/android/server/usb/UsbDeviceManager;

    #@2
    if-eqz v0, :cond_10

    #@4
    .line 416
    iget-object v0, p0, Lcom/lge/autorun/Autorun;->mAutorunTimer:Ljava/util/Timer;

    #@6
    if-eqz v0, :cond_b

    #@8
    .line 417
    invoke-virtual {p0}, Lcom/lge/autorun/Autorun;->autorunStopTimer()V

    #@b
    .line 419
    :cond_b
    iget-object v0, p0, Lcom/lge/autorun/Autorun;->mDeviceManager:Lcom/android/server/usb/UsbDeviceManager;

    #@d
    invoke-virtual {v0, p1, p2}, Lcom/android/server/usb/UsbDeviceManager;->setCurrentFunctions(Ljava/lang/String;Z)V

    #@10
    .line 421
    :cond_10
    return-void
.end method
