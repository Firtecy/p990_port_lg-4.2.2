.class public Lcom/lge/autorun/AutorunManager;
.super Ljava/lang/Object;
.source "AutorunManager.java"


# static fields
.field public static final AUTORUN_CMD_STOP:I = 0x64

.field public static final AUTORUN_CMD_TIMER_STOP:I = 0x2

.field public static final AUTORUN_CMD_VZW_START:I = 0x1

.field private static final AUTORUN_STATE_INIT:I = 0x0

.field private static final AUTORUN_STATE_START:I = 0x1

.field private static final AUTORUN_STATE_STOP:I = 0x64

.field private static final TAG:Ljava/lang/String; = "AUTORUN"


# instance fields
.field private final autorun_trigger:Ljava/lang/String;

.field private mAutorun:Lcom/lge/autorun/Autorun;

.field private mAutorunState:I

.field private mAutorunVZW:Z

.field private final mContext:Landroid/content/Context;

.field private mDeviceManager:Lcom/android/server/usb/UsbDeviceManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/server/usb/UsbDeviceManager;)V
    .registers 6
    .parameter "context"
    .parameter "mgr"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 30
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 11
    const-string v0, "com.lge.android.server.autorun_enable_changed"

    #@6
    iput-object v0, p0, Lcom/lge/autorun/AutorunManager;->autorun_trigger:Ljava/lang/String;

    #@8
    .line 26
    iput-boolean v2, p0, Lcom/lge/autorun/AutorunManager;->mAutorunVZW:Z

    #@a
    .line 28
    const/4 v0, -0x1

    #@b
    iput v0, p0, Lcom/lge/autorun/AutorunManager;->mAutorunState:I

    #@d
    .line 31
    iput-object p1, p0, Lcom/lge/autorun/AutorunManager;->mContext:Landroid/content/Context;

    #@f
    .line 32
    iput-object p2, p0, Lcom/lge/autorun/AutorunManager;->mDeviceManager:Lcom/android/server/usb/UsbDeviceManager;

    #@11
    .line 33
    const-string v0, "AUTORUN"

    #@13
    const-string v1, "[AUTORUN] AutorunManager start"

    #@15
    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 35
    const-string v0, "VZW"

    #@1a
    const-string v1, "ro.build.target_operator"

    #@1c
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23
    move-result v0

    #@24
    if-eqz v0, :cond_2c

    #@26
    .line 36
    const/4 v0, 0x1

    #@27
    iput-boolean v0, p0, Lcom/lge/autorun/AutorunManager;->mAutorunVZW:Z

    #@29
    .line 40
    :goto_29
    iput v2, p0, Lcom/lge/autorun/AutorunManager;->mAutorunState:I

    #@2b
    .line 41
    return-void

    #@2c
    .line 38
    :cond_2c
    iput-boolean v2, p0, Lcom/lge/autorun/AutorunManager;->mAutorunVZW:Z

    #@2e
    goto :goto_29
.end method


# virtual methods
.method public command(I)V
    .registers 5
    .parameter "cmd"

    #@0
    .prologue
    const/16 v1, 0x64

    #@2
    const/4 v2, 0x1

    #@3
    .line 48
    iget-boolean v0, p0, Lcom/lge/autorun/AutorunManager;->mAutorunVZW:Z

    #@5
    if-nez v0, :cond_f

    #@7
    .line 49
    const-string v0, "AUTORUN"

    #@9
    const-string v1, "non-vzw autorun was implemented by settings. so discard it here"

    #@b
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 94
    :cond_e
    :goto_e
    return-void

    #@f
    .line 53
    :cond_f
    sparse-switch p1, :sswitch_data_6e

    #@12
    .line 92
    const-string v0, "AUTORUN"

    #@14
    const-string v1, "[AUTORUN] Unknown cmd...."

    #@16
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    goto :goto_e

    #@1a
    .line 56
    :sswitch_1a
    iget v0, p0, Lcom/lge/autorun/AutorunManager;->mAutorunState:I

    #@1c
    if-ne v0, v2, :cond_26

    #@1e
    .line 57
    const-string v0, "AUTORUN"

    #@20
    const-string v1, "autorun already started"

    #@22
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    goto :goto_e

    #@26
    .line 61
    :cond_26
    iget-object v0, p0, Lcom/lge/autorun/AutorunManager;->mAutorun:Lcom/lge/autorun/Autorun;

    #@28
    if-nez v0, :cond_3d

    #@2a
    .line 62
    new-instance v0, Lcom/lge/autorun/Autorun_VZW;

    #@2c
    iget-object v1, p0, Lcom/lge/autorun/AutorunManager;->mContext:Landroid/content/Context;

    #@2e
    invoke-direct {v0, v1}, Lcom/lge/autorun/Autorun_VZW;-><init>(Landroid/content/Context;)V

    #@31
    iput-object v0, p0, Lcom/lge/autorun/AutorunManager;->mAutorun:Lcom/lge/autorun/Autorun;

    #@33
    .line 63
    iget-object v0, p0, Lcom/lge/autorun/AutorunManager;->mAutorun:Lcom/lge/autorun/Autorun;

    #@35
    iget-object v1, p0, Lcom/lge/autorun/AutorunManager;->mDeviceManager:Lcom/android/server/usb/UsbDeviceManager;

    #@37
    invoke-virtual {v0, v1}, Lcom/lge/autorun/Autorun;->addUsbDeviceManager(Lcom/android/server/usb/UsbDeviceManager;)V

    #@3a
    .line 67
    :cond_3a
    :goto_3a
    iput v2, p0, Lcom/lge/autorun/AutorunManager;->mAutorunState:I

    #@3c
    goto :goto_e

    #@3d
    .line 64
    :cond_3d
    iget v0, p0, Lcom/lge/autorun/AutorunManager;->mAutorunState:I

    #@3f
    if-ne v0, v1, :cond_3a

    #@41
    .line 65
    iget-object v0, p0, Lcom/lge/autorun/AutorunManager;->mAutorun:Lcom/lge/autorun/Autorun;

    #@43
    invoke-virtual {v0}, Lcom/lge/autorun/Autorun;->resume()V

    #@46
    goto :goto_3a

    #@47
    .line 72
    :sswitch_47
    iget v0, p0, Lcom/lge/autorun/AutorunManager;->mAutorunState:I

    #@49
    if-ne v0, v1, :cond_53

    #@4b
    .line 73
    const-string v0, "AUTORUN"

    #@4d
    const-string v1, "autorun already stopped"

    #@4f
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@52
    goto :goto_e

    #@53
    .line 77
    :cond_53
    iget-object v0, p0, Lcom/lge/autorun/AutorunManager;->mAutorun:Lcom/lge/autorun/Autorun;

    #@55
    if-eqz v0, :cond_5c

    #@57
    .line 78
    iget-object v0, p0, Lcom/lge/autorun/AutorunManager;->mAutorun:Lcom/lge/autorun/Autorun;

    #@59
    invoke-virtual {v0}, Lcom/lge/autorun/Autorun;->distroy()V

    #@5c
    .line 80
    :cond_5c
    iput v1, p0, Lcom/lge/autorun/AutorunManager;->mAutorunState:I

    #@5e
    goto :goto_e

    #@5f
    .line 84
    :sswitch_5f
    iget v0, p0, Lcom/lge/autorun/AutorunManager;->mAutorunState:I

    #@61
    if-ne v0, v2, :cond_e

    #@63
    .line 85
    iget-object v0, p0, Lcom/lge/autorun/AutorunManager;->mAutorun:Lcom/lge/autorun/Autorun;

    #@65
    if-eqz v0, :cond_e

    #@67
    .line 86
    iget-object v0, p0, Lcom/lge/autorun/AutorunManager;->mAutorun:Lcom/lge/autorun/Autorun;

    #@69
    invoke-virtual {v0}, Lcom/lge/autorun/Autorun;->autorunStopTimer()V

    #@6c
    goto :goto_e

    #@6d
    .line 53
    nop

    #@6e
    :sswitch_data_6e
    .sparse-switch
        0x1 -> :sswitch_1a
        0x2 -> :sswitch_5f
        0x64 -> :sswitch_47
    .end sparse-switch
.end method
