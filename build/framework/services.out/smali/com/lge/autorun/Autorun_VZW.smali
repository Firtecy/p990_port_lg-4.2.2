.class public Lcom/lge/autorun/Autorun_VZW;
.super Lcom/lge/autorun/Autorun;
.source "Autorun_VZW.java"


# instance fields
.field private final iSecureLockLevel:I

.field private mAutorunDelayed:Z

.field protected final mContext:Landroid/content/Context;

.field mSecureLockReceiver:Landroid/content/BroadcastReceiver;

.field private mSecureLocked:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 6
    .parameter "context"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 20
    invoke-direct {p0, p1}, Lcom/lge/autorun/Autorun;-><init>(Landroid/content/Context;)V

    #@4
    .line 15
    iput-boolean v3, p0, Lcom/lge/autorun/Autorun_VZW;->mAutorunDelayed:Z

    #@6
    .line 16
    iput-boolean v3, p0, Lcom/lge/autorun/Autorun_VZW;->mSecureLocked:Z

    #@8
    .line 17
    const/4 v1, 0x2

    #@9
    iput v1, p0, Lcom/lge/autorun/Autorun_VZW;->iSecureLockLevel:I

    #@b
    .line 46
    new-instance v1, Lcom/lge/autorun/Autorun_VZW$1;

    #@d
    invoke-direct {v1, p0}, Lcom/lge/autorun/Autorun_VZW$1;-><init>(Lcom/lge/autorun/Autorun_VZW;)V

    #@10
    iput-object v1, p0, Lcom/lge/autorun/Autorun_VZW;->mSecureLockReceiver:Landroid/content/BroadcastReceiver;

    #@12
    .line 21
    iput-object p1, p0, Lcom/lge/autorun/Autorun_VZW;->mContext:Landroid/content/Context;

    #@14
    .line 22
    const-string v1, "AUTORUN"

    #@16
    const-string v2, "[AUTORUN] Autorun_VZW"

    #@18
    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 23
    invoke-direct {p0, v3}, Lcom/lge/autorun/Autorun_VZW;->setSecureLockState(Z)V

    #@1e
    .line 25
    new-instance v0, Landroid/content/IntentFilter;

    #@20
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@23
    .line 26
    .local v0, secured_filter:Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    #@25
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@28
    .line 27
    const-string v1, "android.intent.action.USER_PRESENT"

    #@2a
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@2d
    .line 28
    iget-object v1, p0, Lcom/lge/autorun/Autorun_VZW;->mContext:Landroid/content/Context;

    #@2f
    iget-object v2, p0, Lcom/lge/autorun/Autorun_VZW;->mSecureLockReceiver:Landroid/content/BroadcastReceiver;

    #@31
    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@34
    .line 29
    return-void
.end method

.method static synthetic access$000(Lcom/lge/autorun/Autorun_VZW;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 13
    invoke-direct {p0, p1}, Lcom/lge/autorun/Autorun_VZW;->setSecureLockState(Z)V

    #@3
    return-void
.end method

.method static synthetic access$100(Lcom/lge/autorun/Autorun_VZW;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 13
    iget-boolean v0, p0, Lcom/lge/autorun/Autorun_VZW;->mSecureLocked:Z

    #@2
    return v0
.end method

.method static synthetic access$102(Lcom/lge/autorun/Autorun_VZW;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 13
    iput-boolean p1, p0, Lcom/lge/autorun/Autorun_VZW;->mSecureLocked:Z

    #@2
    return p1
.end method

.method static synthetic access$200(Lcom/lge/autorun/Autorun_VZW;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 13
    iget-boolean v0, p0, Lcom/lge/autorun/Autorun_VZW;->mAutorunDelayed:Z

    #@2
    return v0
.end method

.method static synthetic access$202(Lcom/lge/autorun/Autorun_VZW;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 13
    iput-boolean p1, p0, Lcom/lge/autorun/Autorun_VZW;->mAutorunDelayed:Z

    #@2
    return p1
.end method

.method private setSecureLockState(Z)V
    .registers 7
    .parameter "isSleep"

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    const/4 v3, 0x0

    #@2
    .line 80
    if-eqz p1, :cond_9

    #@4
    .line 82
    const-wide/16 v1, 0x12c

    #@6
    :try_start_6
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_9
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_9} :catch_1d

    #@9
    .line 87
    :cond_9
    :goto_9
    const-string v1, "service.keyguard.status"

    #@b
    invoke-static {v1, v3}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@e
    move-result v1

    #@f
    if-ge v1, v4, :cond_19

    #@11
    const-string v1, "persist.service.keyguard.status"

    #@13
    invoke-static {v1, v3}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@16
    move-result v1

    #@17
    if-lt v1, v4, :cond_22

    #@19
    .line 89
    :cond_19
    const/4 v1, 0x1

    #@1a
    iput-boolean v1, p0, Lcom/lge/autorun/Autorun_VZW;->mSecureLocked:Z

    #@1c
    .line 93
    :goto_1c
    return-void

    #@1d
    .line 83
    :catch_1d
    move-exception v0

    #@1e
    .line 84
    .local v0, e:Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    #@21
    goto :goto_9

    #@22
    .line 91
    .end local v0           #e:Ljava/lang/InterruptedException;
    :cond_22
    iput-boolean v3, p0, Lcom/lge/autorun/Autorun_VZW;->mSecureLocked:Z

    #@24
    goto :goto_1c
.end method


# virtual methods
.method protected onBootComplete()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 97
    iget-boolean v0, p0, Lcom/lge/autorun/Autorun_VZW;->mSecureLocked:Z

    #@3
    if-eqz v0, :cond_15

    #@5
    .line 98
    const/4 v0, 0x1

    #@6
    iput-boolean v0, p0, Lcom/lge/autorun/Autorun_VZW;->mAutorunDelayed:Z

    #@8
    .line 99
    const-string v0, "AUTORUN"

    #@a
    const-string v1, "[AUTORUN] setCurrentFunction(charge_only,false)::onBootComplete when securelocked"

    #@c
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 100
    const-string v0, "charge_only"

    #@11
    invoke-virtual {p0, v0, v2}, Lcom/lge/autorun/Autorun_VZW;->setCurrentFunction(Ljava/lang/String;Z)V

    #@14
    .line 107
    :cond_14
    :goto_14
    return-void

    #@15
    .line 102
    :cond_15
    invoke-virtual {p0}, Lcom/lge/autorun/Autorun_VZW;->getSetupWizardState()Z

    #@18
    move-result v0

    #@19
    if-nez v0, :cond_14

    #@1b
    invoke-virtual {p0}, Lcom/lge/autorun/Autorun_VZW;->getUsbUnPlugState()Z

    #@1e
    move-result v0

    #@1f
    if-eqz v0, :cond_14

    #@21
    .line 103
    const-string v0, "AUTORUN"

    #@23
    const-string v1, "[AUTORUN] set to cdrom_storage :: onBootComplete when lock was released"

    #@25
    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 104
    const-string v0, "cdrom_storage"

    #@2a
    invoke-virtual {p0, v0, v2}, Lcom/lge/autorun/Autorun_VZW;->setCurrentFunction(Ljava/lang/String;Z)V

    #@2d
    goto :goto_14
.end method

.method protected onUsbPlugged()V
    .registers 3

    #@0
    .prologue
    .line 125
    iget-boolean v0, p0, Lcom/lge/autorun/Autorun_VZW;->mSecureLocked:Z

    #@2
    if-nez v0, :cond_1a

    #@4
    .line 126
    invoke-virtual {p0}, Lcom/lge/autorun/Autorun_VZW;->getIsBootCompleted()Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_1a

    #@a
    invoke-virtual {p0}, Lcom/lge/autorun/Autorun_VZW;->getSetupWizardState()Z

    #@d
    move-result v0

    #@e
    if-nez v0, :cond_1a

    #@10
    .line 127
    const-string v0, "AUTORUN"

    #@12
    const-string v1, "[AUTORUN] call startAutorun :: onUsbPlugged"

    #@14
    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@17
    .line 128
    invoke-virtual {p0}, Lcom/lge/autorun/Autorun_VZW;->startAutorun()V

    #@1a
    .line 131
    :cond_1a
    return-void
.end method

.method protected onUsbUnPlugged()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 111
    iget-boolean v0, p0, Lcom/lge/autorun/Autorun_VZW;->mSecureLocked:Z

    #@3
    if-eqz v0, :cond_15

    #@5
    .line 112
    const/4 v0, 0x1

    #@6
    iput-boolean v0, p0, Lcom/lge/autorun/Autorun_VZW;->mAutorunDelayed:Z

    #@8
    .line 113
    const-string v0, "AUTORUN"

    #@a
    const-string v1, "[AUTORUN] setCurrentFunction(charge_only,false)::onUsbUnPlugged when securelocked"

    #@c
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 114
    const-string v0, "charge_only"

    #@11
    invoke-virtual {p0, v0, v2}, Lcom/lge/autorun/Autorun_VZW;->setCurrentFunction(Ljava/lang/String;Z)V

    #@14
    .line 121
    :cond_14
    :goto_14
    return-void

    #@15
    .line 116
    :cond_15
    invoke-virtual {p0}, Lcom/lge/autorun/Autorun_VZW;->getSetupWizardState()Z

    #@18
    move-result v0

    #@19
    if-nez v0, :cond_14

    #@1b
    invoke-virtual {p0}, Lcom/lge/autorun/Autorun_VZW;->getUsbUnPlugState()Z

    #@1e
    move-result v0

    #@1f
    if-eqz v0, :cond_14

    #@21
    .line 117
    const-string v0, "AUTORUN"

    #@23
    const-string v1, "[AUTORUN] set to cdrom_storage :: onUsbUnPlugged"

    #@25
    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 118
    const-string v0, "cdrom_storage"

    #@2a
    invoke-virtual {p0, v0, v2}, Lcom/lge/autorun/Autorun_VZW;->setCurrentFunction(Ljava/lang/String;Z)V

    #@2d
    goto :goto_14
.end method

.method protected startAutorun()V
    .registers 4

    #@0
    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/lge/autorun/Autorun_VZW;->getUsbUnPlugState()Z

    #@3
    move-result v1

    #@4
    if-nez v1, :cond_13

    #@6
    .line 34
    invoke-virtual {p0}, Lcom/lge/autorun/Autorun_VZW;->getCurrentFunction()Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    .line 35
    .local v0, currentFunction:Ljava/lang/String;
    if-nez v0, :cond_14

    #@c
    .line 36
    const-string v1, "AUTORUN"

    #@e
    const-string v2, "exception case about currentFunction which is null"

    #@10
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@13
    .line 45
    .end local v0           #currentFunction:Ljava/lang/String;
    :cond_13
    :goto_13
    return-void

    #@14
    .line 39
    .restart local v0       #currentFunction:Ljava/lang/String;
    :cond_14
    const-string v1, "cdrom_storage"

    #@16
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@19
    move-result v1

    #@1a
    if-nez v1, :cond_31

    #@1c
    const-string v1, "cdrom_storage,adb"

    #@1e
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@21
    move-result v1

    #@22
    if-nez v1, :cond_31

    #@24
    .line 40
    const-string v1, "AUTORUN"

    #@26
    const-string v2, "[AUTORUN] set to cdrom_storage :: startAutorun"

    #@28
    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 41
    const-string v1, "cdrom_storage"

    #@2d
    const/4 v2, 0x0

    #@2e
    invoke-virtual {p0, v1, v2}, Lcom/lge/autorun/Autorun_VZW;->setCurrentFunction(Ljava/lang/String;Z)V

    #@31
    .line 43
    :cond_31
    invoke-virtual {p0}, Lcom/lge/autorun/Autorun_VZW;->autorunStartTimer()V

    #@34
    goto :goto_13
.end method
