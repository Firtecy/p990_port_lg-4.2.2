.class Lcom/lge/autorun/Autorun$1;
.super Landroid/content/BroadcastReceiver;
.source "Autorun.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/autorun/Autorun;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/autorun/Autorun;


# direct methods
.method constructor <init>(Lcom/lge/autorun/Autorun;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 147
    iput-object p1, p0, Lcom/lge/autorun/Autorun$1;->this$0:Lcom/lge/autorun/Autorun;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 7
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 149
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 151
    .local v0, action:Ljava/lang/String;
    const-string v1, "com.lge.setup_wizard.AUTORUNON"

    #@6
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_12

    #@c
    .line 152
    iget-object v1, p0, Lcom/lge/autorun/Autorun$1;->this$0:Lcom/lge/autorun/Autorun;

    #@e
    invoke-virtual {v1}, Lcom/lge/autorun/Autorun;->onSetupWizardEnd()V

    #@11
    .line 161
    :goto_11
    return-void

    #@12
    .line 153
    :cond_12
    const-string v1, "android.intent.action.BOOT_COMPLETED"

    #@14
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@17
    move-result v1

    #@18
    if-eqz v1, :cond_31

    #@1a
    .line 154
    iget-object v1, p0, Lcom/lge/autorun/Autorun$1;->this$0:Lcom/lge/autorun/Autorun;

    #@1c
    const/4 v2, 0x1

    #@1d
    invoke-static {v1, v2}, Lcom/lge/autorun/Autorun;->access$002(Lcom/lge/autorun/Autorun;Z)Z

    #@20
    .line 155
    iget-object v1, p0, Lcom/lge/autorun/Autorun$1;->this$0:Lcom/lge/autorun/Autorun;

    #@22
    iget-object v2, p0, Lcom/lge/autorun/Autorun$1;->this$0:Lcom/lge/autorun/Autorun;

    #@24
    invoke-virtual {v2}, Lcom/lge/autorun/Autorun;->getIsSetupWizardRunning()Z

    #@27
    move-result v2

    #@28
    invoke-static {v1, v2}, Lcom/lge/autorun/Autorun;->access$102(Lcom/lge/autorun/Autorun;Z)Z

    #@2b
    .line 156
    iget-object v1, p0, Lcom/lge/autorun/Autorun$1;->this$0:Lcom/lge/autorun/Autorun;

    #@2d
    invoke-virtual {v1}, Lcom/lge/autorun/Autorun;->onBootComplete()V

    #@30
    goto :goto_11

    #@31
    .line 158
    :cond_31
    const-string v1, "AUTORUN"

    #@33
    new-instance v2, Ljava/lang/StringBuilder;

    #@35
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    const-string v3, "[AUTORUN] intent("

    #@3a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v2

    #@3e
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v2

    #@42
    const-string v3, ") is not treated"

    #@44
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v2

    #@48
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v2

    #@4c
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    goto :goto_11
.end method
