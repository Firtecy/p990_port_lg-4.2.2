.class public final Lcom/lge/provider/SettingsEx$Global;
.super Landroid/provider/Settings$NameValueTable;
.source "SettingsEx.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/provider/SettingsEx;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Global"
.end annotation


# static fields
.field public static final ASSISTED_GPS_ENABLED_FOR_CMCC:Ljava/lang/String; = "assisted_gps_enabled_for_cmcc"

.field public static final CONTENT_URI:Landroid/net/Uri;

.field public static final SETTINGS_EX_TO_BACKUP:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 2621
    const-string v0, "content://settings/global"

    #@2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/lge/provider/SettingsEx$Global;->CONTENT_URI:Landroid/net/Uri;

    #@8
    .line 2700
    const/4 v0, 0x1

    #@9
    new-array v0, v0, [Ljava/lang/String;

    #@b
    const/4 v1, 0x0

    #@c
    const-string v2, "assisted_gps_enabled_for_cmcc"

    #@e
    aput-object v2, v0, v1

    #@10
    sput-object v0, Lcom/lge/provider/SettingsEx$Global;->SETTINGS_EX_TO_BACKUP:[Ljava/lang/String;

    #@12
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 2616
    invoke-direct {p0}, Landroid/provider/Settings$NameValueTable;-><init>()V

    #@3
    return-void
.end method

.method public static getFloat(Landroid/content/ContentResolver;Ljava/lang/String;)F
    .registers 4
    .parameter "cr"
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lge/provider/SettingsEx$SettingNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 2673
    :try_start_0
    invoke-static {p0, p1}, Landroid/provider/Settings$Global;->getFloat(Landroid/content/ContentResolver;Ljava/lang/String;)F
    :try_end_3
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_3} :catch_5

    #@3
    move-result v1

    #@4
    return v1

    #@5
    .line 2674
    :catch_5
    move-exception v0

    #@6
    .line 2675
    .local v0, e:Landroid/provider/Settings$SettingNotFoundException;
    new-instance v1, Lcom/lge/provider/SettingsEx$SettingNotFoundException;

    #@8
    invoke-direct {v1, p1}, Lcom/lge/provider/SettingsEx$SettingNotFoundException;-><init>(Ljava/lang/String;)V

    #@b
    throw v1
.end method

.method public static getFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)F
    .registers 4
    .parameter "cr"
    .parameter "name"
    .parameter "def"

    #@0
    .prologue
    .line 2668
    invoke-static {p0, p1, p2}, Landroid/provider/Settings$Global;->getFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)F

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public static getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    .registers 4
    .parameter "cr"
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lge/provider/SettingsEx$SettingNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 2641
    :try_start_0
    invoke-static {p0, p1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_3
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_3} :catch_5

    #@3
    move-result v1

    #@4
    return v1

    #@5
    .line 2642
    :catch_5
    move-exception v0

    #@6
    .line 2643
    .local v0, e:Landroid/provider/Settings$SettingNotFoundException;
    new-instance v1, Lcom/lge/provider/SettingsEx$SettingNotFoundException;

    #@8
    invoke-direct {v1, p1}, Lcom/lge/provider/SettingsEx$SettingNotFoundException;-><init>(Ljava/lang/String;)V

    #@b
    throw v1
.end method

.method public static getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I
    .registers 4
    .parameter "cr"
    .parameter "name"
    .parameter "def"

    #@0
    .prologue
    .line 2636
    invoke-static {p0, p1, p2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public static getLong(Landroid/content/ContentResolver;Ljava/lang/String;)J
    .registers 5
    .parameter "cr"
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/lge/provider/SettingsEx$SettingNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 2657
    :try_start_0
    invoke-static {p0, p1}, Landroid/provider/Settings$Global;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;)J
    :try_end_3
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_3} :catch_5

    #@3
    move-result-wide v1

    #@4
    return-wide v1

    #@5
    .line 2658
    :catch_5
    move-exception v0

    #@6
    .line 2659
    .local v0, e:Landroid/provider/Settings$SettingNotFoundException;
    new-instance v1, Lcom/lge/provider/SettingsEx$SettingNotFoundException;

    #@8
    invoke-direct {v1, p1}, Lcom/lge/provider/SettingsEx$SettingNotFoundException;-><init>(Ljava/lang/String;)V

    #@b
    throw v1
.end method

.method public static getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J
    .registers 6
    .parameter "cr"
    .parameter "name"
    .parameter "def"

    #@0
    .prologue
    .line 2652
    invoke-static {p0, p1, p2, p3}, Landroid/provider/Settings$Global;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    #@3
    move-result-wide v0

    #@4
    return-wide v0
.end method

.method public static declared-synchronized getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "resolver"
    .parameter "name"

    #@0
    .prologue
    .line 2624
    const-class v0, Lcom/lge/provider/SettingsEx$Global;

    #@2
    monitor-enter v0

    #@3
    :try_start_3
    invoke-static {p0, p1}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;
    :try_end_6
    .catchall {:try_start_3 .. :try_end_6} :catchall_9

    #@6
    move-result-object v1

    #@7
    monitor-exit v0

    #@8
    return-object v1

    #@9
    :catchall_9
    move-exception v1

    #@a
    monitor-exit v0

    #@b
    throw v1
.end method

.method public static getUriFor(Ljava/lang/String;)Landroid/net/Uri;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 2632
    invoke-static {p0}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public static putFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)Z
    .registers 4
    .parameter "cr"
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 2680
    invoke-static {p0, p1, p2}, Landroid/provider/Settings$Global;->putFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public static putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    .registers 4
    .parameter "cr"
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 2648
    invoke-static {p0, p1, p2}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public static putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z
    .registers 5
    .parameter "cr"
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 2664
    invoke-static {p0, p1, p2, p3}, Landroid/provider/Settings$Global;->putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public static putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z
    .registers 4
    .parameter "resolver"
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 2628
    invoke-static {p0, p1, p2}, Landroid/provider/Settings$Global;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method
