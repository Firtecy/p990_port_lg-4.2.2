.class public final Lcom/lge/provider/GoogleSettingsContract$Partner;
.super Lcom/lge/provider/GoogleSettingsContract$NameValueTable;
.source "GoogleSettingsContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/provider/GoogleSettingsContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Partner"
.end annotation


# static fields
.field public static final CLIENT_ID:Ljava/lang/String; = "client_id"

.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field public static final DATA_STORE_VERSION:Ljava/lang/String; = "data_store_version"

.field public static final LOGGING_ID2:Ljava/lang/String; = "logging_id2"

.field public static final MAPS_CLIENT_ID:Ljava/lang/String; = "maps_client_id"

.field public static final MARKET_CHECKIN:Ljava/lang/String; = "market_checkin"

.field public static final MARKET_CLIENT_ID:Ljava/lang/String; = "market_client_id"

.field public static final NETWORK_LOCATION_OPT_IN:Ljava/lang/String; = "network_location_opt_in"

.field public static final RLZ:Ljava/lang/String; = "rlz"

.field public static final USE_LOCATION_FOR_SERVICES:Ljava/lang/String; = "use_location_for_services"

.field public static final VOICESEARCH_CLIENT_ID:Ljava/lang/String; = "voicesearch_client_id"

.field public static final YOUTUBE_CLIENT_ID:Ljava/lang/String; = "youtube_client_id"


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 184
    const-string v0, "content://com.google.settings/partner"

    #@2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/lge/provider/GoogleSettingsContract$Partner;->CONTENT_URI:Landroid/net/Uri;

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 76
    invoke-direct {p0}, Lcom/lge/provider/GoogleSettingsContract$NameValueTable;-><init>()V

    #@3
    return-void
.end method

.method public static getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I
    .registers 6
    .parameter "resolver"
    .parameter "name"
    .parameter "defValue"

    #@0
    .prologue
    .line 145
    invoke-static {p0, p1}, Lcom/lge/provider/GoogleSettingsContract$Partner;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    .line 148
    .local v1, valString:Ljava/lang/String;
    if-eqz v1, :cond_b

    #@6
    :try_start_6
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_9
    .catch Ljava/lang/NumberFormatException; {:try_start_6 .. :try_end_9} :catch_d

    #@9
    move-result v2

    #@a
    .line 152
    .local v2, value:I
    :goto_a
    return v2

    #@b
    .end local v2           #value:I
    :cond_b
    move v2, p2

    #@c
    .line 148
    goto :goto_a

    #@d
    .line 149
    :catch_d
    move-exception v0

    #@e
    .line 150
    .local v0, e:Ljava/lang/NumberFormatException;
    move v2, p2

    #@f
    .restart local v2       #value:I
    goto :goto_a
.end method

.method public static getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J
    .registers 8
    .parameter "resolver"
    .parameter "name"
    .parameter "defValue"

    #@0
    .prologue
    .line 161
    invoke-static {p0, p1}, Lcom/lge/provider/GoogleSettingsContract$Partner;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    .line 164
    .local v1, valString:Ljava/lang/String;
    if-eqz v1, :cond_b

    #@6
    :try_start_6
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_9
    .catch Ljava/lang/NumberFormatException; {:try_start_6 .. :try_end_9} :catch_d

    #@9
    move-result-wide v2

    #@a
    .line 168
    .local v2, value:J
    :goto_a
    return-wide v2

    #@b
    .end local v2           #value:J
    :cond_b
    move-wide v2, p2

    #@c
    .line 164
    goto :goto_a

    #@d
    .line 165
    :catch_d
    move-exception v0

    #@e
    .line 166
    .local v0, e:Ljava/lang/NumberFormatException;
    move-wide v2, p2

    #@f
    .restart local v2       #value:J
    goto :goto_a
.end method

.method public static getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;
    .registers 11
    .parameter "resolver"
    .parameter "name"

    #@0
    .prologue
    .line 84
    const/4 v8, 0x0

    #@1
    .line 85
    .local v8, value:Ljava/lang/String;
    const/4 v6, 0x0

    #@2
    .line 87
    .local v6, c:Landroid/database/Cursor;
    :try_start_2
    sget-object v1, Lcom/lge/provider/GoogleSettingsContract$Partner;->CONTENT_URI:Landroid/net/Uri;

    #@4
    const/4 v0, 0x1

    #@5
    new-array v2, v0, [Ljava/lang/String;

    #@7
    const/4 v0, 0x0

    #@8
    const-string v3, "value"

    #@a
    aput-object v3, v2, v0

    #@c
    const-string v3, "name=?"

    #@e
    const/4 v0, 0x1

    #@f
    new-array v4, v0, [Ljava/lang/String;

    #@11
    const/4 v0, 0x0

    #@12
    aput-object p1, v4, v0

    #@14
    const/4 v5, 0x0

    #@15
    move-object v0, p0

    #@16
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@19
    move-result-object v6

    #@1a
    .line 89
    if-eqz v6, :cond_27

    #@1c
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    #@1f
    move-result v0

    #@20
    if-eqz v0, :cond_27

    #@22
    const/4 v0, 0x0

    #@23
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_26
    .catchall {:try_start_2 .. :try_end_26} :catchall_58
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_26} :catch_2d

    #@26
    move-result-object v8

    #@27
    .line 94
    :cond_27
    if-eqz v6, :cond_2c

    #@29
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@2c
    .line 96
    :cond_2c
    :goto_2c
    return-object v8

    #@2d
    .line 90
    :catch_2d
    move-exception v7

    #@2e
    .line 92
    .local v7, e:Landroid/database/SQLException;
    :try_start_2e
    const-string v0, "GoogleSettings"

    #@30
    new-instance v1, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    const-string v2, "Can\'t get key "

    #@37
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v1

    #@3b
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v1

    #@3f
    const-string v2, " from "

    #@41
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v1

    #@45
    sget-object v2, Lcom/lge/provider/GoogleSettingsContract$Partner;->CONTENT_URI:Landroid/net/Uri;

    #@47
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v1

    #@4b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v1

    #@4f
    invoke-static {v0, v1, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_52
    .catchall {:try_start_2e .. :try_end_52} :catchall_58

    #@52
    .line 94
    if-eqz v6, :cond_2c

    #@54
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@57
    goto :goto_2c

    #@58
    .end local v7           #e:Landroid/database/SQLException;
    :catchall_58
    move-exception v0

    #@59
    if-eqz v6, :cond_5e

    #@5b
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@5e
    :cond_5e
    throw v0
.end method

.method public static getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "resolver"
    .parameter "name"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 107
    invoke-static {p0, p1}, Lcom/lge/provider/GoogleSettingsContract$Partner;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 108
    .local v0, value:Ljava/lang/String;
    if-nez v0, :cond_7

    #@6
    .line 109
    move-object v0, p2

    #@7
    .line 112
    :cond_7
    return-object v0
.end method

.method public static getUriFor(Ljava/lang/String;)Landroid/net/Uri;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 178
    sget-object v0, Lcom/lge/provider/GoogleSettingsContract$Partner;->CONTENT_URI:Landroid/net/Uri;

    #@2
    invoke-static {v0, p0}, Lcom/lge/provider/GoogleSettingsContract$Partner;->getUriFor(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public static putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    .registers 4
    .parameter "resolver"
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 136
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-static {p0, p1, v0}, Lcom/lge/provider/GoogleSettingsContract$Partner;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public static putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z
    .registers 4
    .parameter "resolver"
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 124
    sget-object v0, Lcom/lge/provider/GoogleSettingsContract$Partner;->CONTENT_URI:Landroid/net/Uri;

    #@2
    invoke-static {p0, v0, p1, p2}, Lcom/lge/provider/GoogleSettingsContract$Partner;->putString(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method
