.class public Lcom/android/internal/view/menu/MenuBuilder;
.super Ljava/lang/Object;
.source "MenuBuilder.java"

# interfaces
.implements Landroid/view/Menu;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/view/menu/MenuBuilder$ItemInvoker;,
        Lcom/android/internal/view/menu/MenuBuilder$Callback;
    }
.end annotation


# static fields
.field private static final ACTION_VIEW_STATES_KEY:Ljava/lang/String; = "android:menu:actionviewstates"

.field private static final EXPANDED_ACTION_VIEW_ID:Ljava/lang/String; = "android:menu:expandedactionview"

.field private static final PRESENTER_KEY:Ljava/lang/String; = "android:menu:presenters"

.field private static final TAG:Ljava/lang/String; = "MenuBuilder"

.field private static final sCategoryToOrder:[I


# instance fields
.field private mActionItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/view/menu/MenuItemImpl;",
            ">;"
        }
    .end annotation
.end field

.field private mCallback:Lcom/android/internal/view/menu/MenuBuilder$Callback;

.field private final mContext:Landroid/content/Context;

.field private mCurrentMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

.field private mDefaultShowAsAction:I

.field private mExpandedItem:Lcom/android/internal/view/menu/MenuItemImpl;

.field private mFrozenViewStates:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation
.end field

.field mHeaderIcon:Landroid/graphics/drawable/Drawable;

.field mHeaderTitle:Ljava/lang/CharSequence;

.field mHeaderView:Landroid/view/View;

.field private mIsActionItemsStale:Z

.field private mIsClosing:Z

.field private mIsVisibleItemsStale:Z

.field private mItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/view/menu/MenuItemImpl;",
            ">;"
        }
    .end annotation
.end field

.field private mItemsChangedWhileDispatchPrevented:Z

.field private mNonActionItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/view/menu/MenuItemImpl;",
            ">;"
        }
    .end annotation
.end field

.field private mOptionalIconsVisible:Z

.field private mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/android/internal/view/menu/MenuPresenter;",
            ">;>;"
        }
    .end annotation
.end field

.field private mPreventDispatchingItemsChanged:Z

.field private mQwertyMode:Z

.field private final mResources:Landroid/content/res/Resources;

.field private mShortcutsVisible:Z

.field private mTempShortcutItemList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/view/menu/MenuItemImpl;",
            ">;"
        }
    .end annotation
.end field

.field private mVisibleItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/view/menu/MenuItemImpl;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 56
    const/4 v0, 0x6

    #@1
    new-array v0, v0, [I

    #@3
    fill-array-data v0, :array_a

    #@6
    sput-object v0, Lcom/android/internal/view/menu/MenuBuilder;->sCategoryToOrder:[I

    #@8
    return-void

    #@9
    nop

    #@a
    :array_a
    .array-data 0x4
        0x1t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v0, 0x0

    #@2
    .line 186
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 116
    iput v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mDefaultShowAsAction:I

    #@7
    .line 142
    iput-boolean v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mPreventDispatchingItemsChanged:Z

    #@9
    .line 143
    iput-boolean v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mItemsChangedWhileDispatchPrevented:Z

    #@b
    .line 145
    iput-boolean v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mOptionalIconsVisible:Z

    #@d
    .line 147
    iput-boolean v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mIsClosing:Z

    #@f
    .line 149
    new-instance v0, Ljava/util/ArrayList;

    #@11
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@14
    iput-object v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mTempShortcutItemList:Ljava/util/ArrayList;

    #@16
    .line 151
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    #@18
    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    #@1b
    iput-object v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@1d
    .line 187
    iput-object p1, p0, Lcom/android/internal/view/menu/MenuBuilder;->mContext:Landroid/content/Context;

    #@1f
    .line 188
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@22
    move-result-object v0

    #@23
    iput-object v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mResources:Landroid/content/res/Resources;

    #@25
    .line 190
    new-instance v0, Ljava/util/ArrayList;

    #@27
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@2a
    iput-object v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mItems:Ljava/util/ArrayList;

    #@2c
    .line 192
    new-instance v0, Ljava/util/ArrayList;

    #@2e
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@31
    iput-object v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mVisibleItems:Ljava/util/ArrayList;

    #@33
    .line 193
    iput-boolean v1, p0, Lcom/android/internal/view/menu/MenuBuilder;->mIsVisibleItemsStale:Z

    #@35
    .line 195
    new-instance v0, Ljava/util/ArrayList;

    #@37
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@3a
    iput-object v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mActionItems:Ljava/util/ArrayList;

    #@3c
    .line 196
    new-instance v0, Ljava/util/ArrayList;

    #@3e
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@41
    iput-object v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mNonActionItems:Ljava/util/ArrayList;

    #@43
    .line 197
    iput-boolean v1, p0, Lcom/android/internal/view/menu/MenuBuilder;->mIsActionItemsStale:Z

    #@45
    .line 199
    invoke-direct {p0, v1}, Lcom/android/internal/view/menu/MenuBuilder;->setShortcutsVisibleInner(Z)V

    #@48
    .line 200
    return-void
.end method

.method private addInternal(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;
    .registers 13
    .parameter "group"
    .parameter "id"
    .parameter "categoryOrder"
    .parameter "title"

    #@0
    .prologue
    .line 387
    invoke-static {p3}, Lcom/android/internal/view/menu/MenuBuilder;->getOrdering(I)I

    #@3
    move-result v5

    #@4
    .line 389
    .local v5, ordering:I
    new-instance v0, Lcom/android/internal/view/menu/MenuItemImpl;

    #@6
    iget v7, p0, Lcom/android/internal/view/menu/MenuBuilder;->mDefaultShowAsAction:I

    #@8
    move-object v1, p0

    #@9
    move v2, p1

    #@a
    move v3, p2

    #@b
    move v4, p3

    #@c
    move-object v6, p4

    #@d
    invoke-direct/range {v0 .. v7}, Lcom/android/internal/view/menu/MenuItemImpl;-><init>(Lcom/android/internal/view/menu/MenuBuilder;IIIILjava/lang/CharSequence;I)V

    #@10
    .line 392
    .local v0, item:Lcom/android/internal/view/menu/MenuItemImpl;
    iget-object v1, p0, Lcom/android/internal/view/menu/MenuBuilder;->mCurrentMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

    #@12
    if-eqz v1, :cond_19

    #@14
    .line 394
    iget-object v1, p0, Lcom/android/internal/view/menu/MenuBuilder;->mCurrentMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

    #@16
    invoke-virtual {v0, v1}, Lcom/android/internal/view/menu/MenuItemImpl;->setMenuInfo(Landroid/view/ContextMenu$ContextMenuInfo;)V

    #@19
    .line 397
    :cond_19
    iget-object v1, p0, Lcom/android/internal/view/menu/MenuBuilder;->mItems:Ljava/util/ArrayList;

    #@1b
    iget-object v2, p0, Lcom/android/internal/view/menu/MenuBuilder;->mItems:Ljava/util/ArrayList;

    #@1d
    invoke-static {v2, v5}, Lcom/android/internal/view/menu/MenuBuilder;->findInsertIndex(Ljava/util/ArrayList;I)I

    #@20
    move-result v2

    #@21
    invoke-virtual {v1, v2, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    #@24
    .line 398
    const/4 v1, 0x1

    #@25
    invoke-virtual {p0, v1}, Lcom/android/internal/view/menu/MenuBuilder;->onItemsChanged(Z)V

    #@28
    .line 400
    return-object v0
.end method

.method private dispatchPresenterUpdate(Z)V
    .registers 6
    .parameter "cleared"

    #@0
    .prologue
    .line 236
    iget-object v3, p0, Lcom/android/internal/view/menu/MenuBuilder;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@2
    invoke-virtual {v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    #@5
    move-result v3

    #@6
    if-eqz v3, :cond_9

    #@8
    .line 248
    :goto_8
    return-void

    #@9
    .line 238
    :cond_9
    invoke-virtual {p0}, Lcom/android/internal/view/menu/MenuBuilder;->stopDispatchingItemsChanged()V

    #@c
    .line 239
    iget-object v3, p0, Lcom/android/internal/view/menu/MenuBuilder;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@e
    invoke-virtual {v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    #@11
    move-result-object v0

    #@12
    .local v0, i$:Ljava/util/Iterator;
    :goto_12
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@15
    move-result v3

    #@16
    if-eqz v3, :cond_30

    #@18
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1b
    move-result-object v2

    #@1c
    check-cast v2, Ljava/lang/ref/WeakReference;

    #@1e
    .line 240
    .local v2, ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Lcom/android/internal/view/menu/MenuPresenter;>;"
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@21
    move-result-object v1

    #@22
    check-cast v1, Lcom/android/internal/view/menu/MenuPresenter;

    #@24
    .line 241
    .local v1, presenter:Lcom/android/internal/view/menu/MenuPresenter;
    if-nez v1, :cond_2c

    #@26
    .line 242
    iget-object v3, p0, Lcom/android/internal/view/menu/MenuBuilder;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@28
    invoke-virtual {v3, v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    #@2b
    goto :goto_12

    #@2c
    .line 244
    :cond_2c
    invoke-interface {v1, p1}, Lcom/android/internal/view/menu/MenuPresenter;->updateMenuView(Z)V

    #@2f
    goto :goto_12

    #@30
    .line 247
    .end local v1           #presenter:Lcom/android/internal/view/menu/MenuPresenter;
    .end local v2           #ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Lcom/android/internal/view/menu/MenuPresenter;>;"
    :cond_30
    invoke-virtual {p0}, Lcom/android/internal/view/menu/MenuBuilder;->startDispatchingItemsChanged()V

    #@33
    goto :goto_8
.end method

.method private dispatchRestoreInstanceState(Landroid/os/Bundle;)V
    .registers 9
    .parameter "state"

    #@0
    .prologue
    .line 290
    const-string v6, "android:menu:presenters"

    #@2
    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;

    #@5
    move-result-object v4

    #@6
    .line 292
    .local v4, presenterStates:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Landroid/os/Parcelable;>;"
    if-eqz v4, :cond_10

    #@8
    iget-object v6, p0, Lcom/android/internal/view/menu/MenuBuilder;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@a
    invoke-virtual {v6}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    #@d
    move-result v6

    #@e
    if-eqz v6, :cond_11

    #@10
    .line 308
    :cond_10
    return-void

    #@11
    .line 294
    :cond_11
    iget-object v6, p0, Lcom/android/internal/view/menu/MenuBuilder;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@13
    invoke-virtual {v6}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    #@16
    move-result-object v0

    #@17
    .local v0, i$:Ljava/util/Iterator;
    :cond_17
    :goto_17
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@1a
    move-result v6

    #@1b
    if-eqz v6, :cond_10

    #@1d
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@20
    move-result-object v5

    #@21
    check-cast v5, Ljava/lang/ref/WeakReference;

    #@23
    .line 295
    .local v5, ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Lcom/android/internal/view/menu/MenuPresenter;>;"
    invoke-virtual {v5}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@26
    move-result-object v3

    #@27
    check-cast v3, Lcom/android/internal/view/menu/MenuPresenter;

    #@29
    .line 296
    .local v3, presenter:Lcom/android/internal/view/menu/MenuPresenter;
    if-nez v3, :cond_31

    #@2b
    .line 297
    iget-object v6, p0, Lcom/android/internal/view/menu/MenuBuilder;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@2d
    invoke-virtual {v6, v5}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    #@30
    goto :goto_17

    #@31
    .line 299
    :cond_31
    invoke-interface {v3}, Lcom/android/internal/view/menu/MenuPresenter;->getId()I

    #@34
    move-result v1

    #@35
    .line 300
    .local v1, id:I
    if-lez v1, :cond_17

    #@37
    .line 301
    invoke-virtual {v4, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@3a
    move-result-object v2

    #@3b
    check-cast v2, Landroid/os/Parcelable;

    #@3d
    .line 302
    .local v2, parcel:Landroid/os/Parcelable;
    if-eqz v2, :cond_17

    #@3f
    .line 303
    invoke-interface {v3, v2}, Lcom/android/internal/view/menu/MenuPresenter;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    #@42
    goto :goto_17
.end method

.method private dispatchSaveInstanceState(Landroid/os/Bundle;)V
    .registers 9
    .parameter "outState"

    #@0
    .prologue
    .line 267
    iget-object v6, p0, Lcom/android/internal/view/menu/MenuBuilder;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@2
    invoke-virtual {v6}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    #@5
    move-result v6

    #@6
    if-eqz v6, :cond_9

    #@8
    .line 287
    :goto_8
    return-void

    #@9
    .line 269
    :cond_9
    new-instance v3, Landroid/util/SparseArray;

    #@b
    invoke-direct {v3}, Landroid/util/SparseArray;-><init>()V

    #@e
    .line 271
    .local v3, presenterStates:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Landroid/os/Parcelable;>;"
    iget-object v6, p0, Lcom/android/internal/view/menu/MenuBuilder;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@10
    invoke-virtual {v6}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    #@13
    move-result-object v0

    #@14
    .local v0, i$:Ljava/util/Iterator;
    :cond_14
    :goto_14
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@17
    move-result v6

    #@18
    if-eqz v6, :cond_3e

    #@1a
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1d
    move-result-object v4

    #@1e
    check-cast v4, Ljava/lang/ref/WeakReference;

    #@20
    .line 272
    .local v4, ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Lcom/android/internal/view/menu/MenuPresenter;>;"
    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@23
    move-result-object v2

    #@24
    check-cast v2, Lcom/android/internal/view/menu/MenuPresenter;

    #@26
    .line 273
    .local v2, presenter:Lcom/android/internal/view/menu/MenuPresenter;
    if-nez v2, :cond_2e

    #@28
    .line 274
    iget-object v6, p0, Lcom/android/internal/view/menu/MenuBuilder;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@2a
    invoke-virtual {v6, v4}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    #@2d
    goto :goto_14

    #@2e
    .line 276
    :cond_2e
    invoke-interface {v2}, Lcom/android/internal/view/menu/MenuPresenter;->getId()I

    #@31
    move-result v1

    #@32
    .line 277
    .local v1, id:I
    if-lez v1, :cond_14

    #@34
    .line 278
    invoke-interface {v2}, Lcom/android/internal/view/menu/MenuPresenter;->onSaveInstanceState()Landroid/os/Parcelable;

    #@37
    move-result-object v5

    #@38
    .line 279
    .local v5, state:Landroid/os/Parcelable;
    if-eqz v5, :cond_14

    #@3a
    .line 280
    invoke-virtual {v3, v1, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@3d
    goto :goto_14

    #@3e
    .line 286
    .end local v1           #id:I
    .end local v2           #presenter:Lcom/android/internal/view/menu/MenuPresenter;
    .end local v4           #ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Lcom/android/internal/view/menu/MenuPresenter;>;"
    .end local v5           #state:Landroid/os/Parcelable;
    :cond_3e
    const-string v6, "android:menu:presenters"

    #@40
    invoke-virtual {p1, v6, v3}, Landroid/os/Bundle;->putSparseParcelableArray(Ljava/lang/String;Landroid/util/SparseArray;)V

    #@43
    goto :goto_8
.end method

.method private dispatchSubMenuSelected(Lcom/android/internal/view/menu/SubMenuBuilder;)Z
    .registers 7
    .parameter "subMenu"

    #@0
    .prologue
    .line 251
    iget-object v4, p0, Lcom/android/internal/view/menu/MenuBuilder;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@2
    invoke-virtual {v4}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    #@5
    move-result v4

    #@6
    if-eqz v4, :cond_a

    #@8
    const/4 v3, 0x0

    #@9
    .line 263
    :cond_9
    return v3

    #@a
    .line 253
    :cond_a
    const/4 v3, 0x0

    #@b
    .line 255
    .local v3, result:Z
    iget-object v4, p0, Lcom/android/internal/view/menu/MenuBuilder;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@d
    invoke-virtual {v4}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    #@10
    move-result-object v0

    #@11
    .local v0, i$:Ljava/util/Iterator;
    :cond_11
    :goto_11
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@14
    move-result v4

    #@15
    if-eqz v4, :cond_9

    #@17
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1a
    move-result-object v2

    #@1b
    check-cast v2, Ljava/lang/ref/WeakReference;

    #@1d
    .line 256
    .local v2, ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Lcom/android/internal/view/menu/MenuPresenter;>;"
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@20
    move-result-object v1

    #@21
    check-cast v1, Lcom/android/internal/view/menu/MenuPresenter;

    #@23
    .line 257
    .local v1, presenter:Lcom/android/internal/view/menu/MenuPresenter;
    if-nez v1, :cond_2b

    #@25
    .line 258
    iget-object v4, p0, Lcom/android/internal/view/menu/MenuBuilder;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@27
    invoke-virtual {v4, v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    #@2a
    goto :goto_11

    #@2b
    .line 259
    :cond_2b
    if-nez v3, :cond_11

    #@2d
    .line 260
    invoke-interface {v1, p1}, Lcom/android/internal/view/menu/MenuPresenter;->onSubMenuSelected(Lcom/android/internal/view/menu/SubMenuBuilder;)Z

    #@30
    move-result v3

    #@31
    goto :goto_11
.end method

.method private static findInsertIndex(Ljava/util/ArrayList;I)I
    .registers 5
    .parameter
    .parameter "ordering"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/view/menu/MenuItemImpl;",
            ">;I)I"
        }
    .end annotation

    #@0
    .prologue
    .line 748
    .local p0, items:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/view/menu/MenuItemImpl;>;"
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    #@3
    move-result v2

    #@4
    add-int/lit8 v0, v2, -0x1

    #@6
    .local v0, i:I
    :goto_6
    if-ltz v0, :cond_1a

    #@8
    .line 749
    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@b
    move-result-object v1

    #@c
    check-cast v1, Lcom/android/internal/view/menu/MenuItemImpl;

    #@e
    .line 750
    .local v1, item:Lcom/android/internal/view/menu/MenuItemImpl;
    invoke-virtual {v1}, Lcom/android/internal/view/menu/MenuItemImpl;->getOrdering()I

    #@11
    move-result v2

    #@12
    if-gt v2, p1, :cond_17

    #@14
    .line 751
    add-int/lit8 v2, v0, 0x1

    #@16
    .line 755
    .end local v1           #item:Lcom/android/internal/view/menu/MenuItemImpl;
    :goto_16
    return v2

    #@17
    .line 748
    .restart local v1       #item:Lcom/android/internal/view/menu/MenuItemImpl;
    :cond_17
    add-int/lit8 v0, v0, -0x1

    #@19
    goto :goto_6

    #@1a
    .line 755
    .end local v1           #item:Lcom/android/internal/view/menu/MenuItemImpl;
    :cond_1a
    const/4 v2, 0x0

    #@1b
    goto :goto_16
.end method

.method private static getOrdering(I)I
    .registers 4
    .parameter "categoryOrder"

    #@0
    .prologue
    .line 681
    const/high16 v1, -0x1

    #@2
    and-int/2addr v1, p0

    #@3
    shr-int/lit8 v0, v1, 0x10

    #@5
    .line 683
    .local v0, index:I
    if-ltz v0, :cond_c

    #@7
    sget-object v1, Lcom/android/internal/view/menu/MenuBuilder;->sCategoryToOrder:[I

    #@9
    array-length v1, v1

    #@a
    if-lt v0, v1, :cond_14

    #@c
    .line 684
    :cond_c
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@e
    const-string v2, "order does not contain a valid category."

    #@10
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@13
    throw v1

    #@14
    .line 687
    :cond_14
    sget-object v1, Lcom/android/internal/view/menu/MenuBuilder;->sCategoryToOrder:[I

    #@16
    aget v1, v1, v0

    #@18
    shl-int/lit8 v1, v1, 0x10

    #@1a
    const v2, 0xffff

    #@1d
    and-int/2addr v2, p0

    #@1e
    or-int/2addr v1, v2

    #@1f
    return v1
.end method

.method private removeItemAtInt(IZ)V
    .registers 4
    .parameter "index"
    .parameter "updateChildrenOnMenuViews"

    #@0
    .prologue
    .line 499
    if-ltz p1, :cond_a

    #@2
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mItems:Ljava/util/ArrayList;

    #@4
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@7
    move-result v0

    #@8
    if-lt p1, v0, :cond_b

    #@a
    .line 504
    :cond_a
    :goto_a
    return-void

    #@b
    .line 501
    :cond_b
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mItems:Ljava/util/ArrayList;

    #@d
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@10
    .line 503
    if-eqz p2, :cond_a

    #@12
    const/4 v0, 0x1

    #@13
    invoke-virtual {p0, v0}, Lcom/android/internal/view/menu/MenuBuilder;->onItemsChanged(Z)V

    #@16
    goto :goto_a
.end method

.method private setHeaderInternal(ILjava/lang/CharSequence;ILandroid/graphics/drawable/Drawable;Landroid/view/View;)V
    .registers 9
    .parameter "titleRes"
    .parameter "title"
    .parameter "iconRes"
    .parameter "icon"
    .parameter "view"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1107
    invoke-virtual {p0}, Lcom/android/internal/view/menu/MenuBuilder;->getResources()Landroid/content/res/Resources;

    #@4
    move-result-object v0

    #@5
    .line 1109
    .local v0, r:Landroid/content/res/Resources;
    if-eqz p5, :cond_12

    #@7
    .line 1110
    iput-object p5, p0, Lcom/android/internal/view/menu/MenuBuilder;->mHeaderView:Landroid/view/View;

    #@9
    .line 1113
    iput-object v2, p0, Lcom/android/internal/view/menu/MenuBuilder;->mHeaderTitle:Ljava/lang/CharSequence;

    #@b
    .line 1114
    iput-object v2, p0, Lcom/android/internal/view/menu/MenuBuilder;->mHeaderIcon:Landroid/graphics/drawable/Drawable;

    #@d
    .line 1133
    :goto_d
    const/4 v1, 0x0

    #@e
    invoke-virtual {p0, v1}, Lcom/android/internal/view/menu/MenuBuilder;->onItemsChanged(Z)V

    #@11
    .line 1134
    return-void

    #@12
    .line 1116
    :cond_12
    if-lez p1, :cond_25

    #@14
    .line 1117
    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@17
    move-result-object v1

    #@18
    iput-object v1, p0, Lcom/android/internal/view/menu/MenuBuilder;->mHeaderTitle:Ljava/lang/CharSequence;

    #@1a
    .line 1122
    :cond_1a
    :goto_1a
    if-lez p3, :cond_2a

    #@1c
    .line 1123
    invoke-virtual {v0, p3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@1f
    move-result-object v1

    #@20
    iput-object v1, p0, Lcom/android/internal/view/menu/MenuBuilder;->mHeaderIcon:Landroid/graphics/drawable/Drawable;

    #@22
    .line 1129
    :cond_22
    :goto_22
    iput-object v2, p0, Lcom/android/internal/view/menu/MenuBuilder;->mHeaderView:Landroid/view/View;

    #@24
    goto :goto_d

    #@25
    .line 1118
    :cond_25
    if-eqz p2, :cond_1a

    #@27
    .line 1119
    iput-object p2, p0, Lcom/android/internal/view/menu/MenuBuilder;->mHeaderTitle:Ljava/lang/CharSequence;

    #@29
    goto :goto_1a

    #@2a
    .line 1124
    :cond_2a
    if-eqz p4, :cond_22

    #@2c
    .line 1125
    iput-object p4, p0, Lcom/android/internal/view/menu/MenuBuilder;->mHeaderIcon:Landroid/graphics/drawable/Drawable;

    #@2e
    goto :goto_22
.end method

.method private setShortcutsVisibleInner(Z)V
    .registers 5
    .parameter "shortcutsVisible"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 713
    if-eqz p1, :cond_1b

    #@3
    iget-object v1, p0, Lcom/android/internal/view/menu/MenuBuilder;->mResources:Landroid/content/res/Resources;

    #@5
    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@8
    move-result-object v1

    #@9
    iget v1, v1, Landroid/content/res/Configuration;->keyboard:I

    #@b
    if-eq v1, v0, :cond_1b

    #@d
    iget-object v1, p0, Lcom/android/internal/view/menu/MenuBuilder;->mResources:Landroid/content/res/Resources;

    #@f
    const v2, 0x1110036

    #@12
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@15
    move-result v1

    #@16
    if-eqz v1, :cond_1b

    #@18
    :goto_18
    iput-boolean v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mShortcutsVisible:Z

    #@1a
    .line 717
    return-void

    #@1b
    .line 713
    :cond_1b
    const/4 v0, 0x0

    #@1c
    goto :goto_18
.end method


# virtual methods
.method public add(I)Landroid/view/MenuItem;
    .registers 4
    .parameter "titleRes"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 408
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mResources:Landroid/content/res/Resources;

    #@3
    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    invoke-direct {p0, v1, v1, v1, v0}, Lcom/android/internal/view/menu/MenuBuilder;->addInternal(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    #@a
    move-result-object v0

    #@b
    return-object v0
.end method

.method public add(IIII)Landroid/view/MenuItem;
    .registers 6
    .parameter "group"
    .parameter "id"
    .parameter "categoryOrder"
    .parameter "title"

    #@0
    .prologue
    .line 416
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mResources:Landroid/content/res/Resources;

    #@2
    invoke-virtual {v0, p4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/android/internal/view/menu/MenuBuilder;->addInternal(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    #@9
    move-result-object v0

    #@a
    return-object v0
.end method

.method public add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;
    .registers 6
    .parameter "group"
    .parameter "id"
    .parameter "categoryOrder"
    .parameter "title"

    #@0
    .prologue
    .line 412
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/internal/view/menu/MenuBuilder;->addInternal(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .registers 3
    .parameter "title"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 404
    invoke-direct {p0, v0, v0, v0, p1}, Lcom/android/internal/view/menu/MenuBuilder;->addInternal(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public addIntentOptions(IIILandroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I[Landroid/view/MenuItem;)I
    .registers 22
    .parameter "group"
    .parameter "id"
    .parameter "categoryOrder"
    .parameter "caller"
    .parameter "specifics"
    .parameter "intent"
    .parameter "flags"
    .parameter "outSpecificItems"

    #@0
    .prologue
    .line 441
    iget-object v10, p0, Lcom/android/internal/view/menu/MenuBuilder;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v10}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@5
    move-result-object v7

    #@6
    .line 442
    .local v7, pm:Landroid/content/pm/PackageManager;
    const/4 v10, 0x0

    #@7
    move-object/from16 v0, p4

    #@9
    move-object/from16 v1, p5

    #@b
    move-object/from16 v2, p6

    #@d
    invoke-virtual {v7, v0, v1, v2, v10}, Landroid/content/pm/PackageManager;->queryIntentActivityOptions(Landroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I)Ljava/util/List;

    #@10
    move-result-object v6

    #@11
    .line 444
    .local v6, lri:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-eqz v6, :cond_67

    #@13
    invoke-interface {v6}, Ljava/util/List;->size()I

    #@16
    move-result v3

    #@17
    .line 446
    .local v3, N:I
    :goto_17
    and-int/lit8 v10, p7, 0x1

    #@19
    if-nez v10, :cond_1e

    #@1b
    .line 447
    invoke-virtual {p0, p1}, Lcom/android/internal/view/menu/MenuBuilder;->removeGroup(I)V

    #@1e
    .line 450
    :cond_1e
    const/4 v4, 0x0

    #@1f
    .local v4, i:I
    :goto_1f
    if-ge v4, v3, :cond_6e

    #@21
    .line 451
    invoke-interface {v6, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@24
    move-result-object v8

    #@25
    check-cast v8, Landroid/content/pm/ResolveInfo;

    #@27
    .line 452
    .local v8, ri:Landroid/content/pm/ResolveInfo;
    new-instance v9, Landroid/content/Intent;

    #@29
    iget v10, v8, Landroid/content/pm/ResolveInfo;->specificIndex:I

    #@2b
    if-gez v10, :cond_69

    #@2d
    move-object/from16 v10, p6

    #@2f
    :goto_2f
    invoke-direct {v9, v10}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    #@32
    .line 454
    .local v9, rintent:Landroid/content/Intent;
    new-instance v10, Landroid/content/ComponentName;

    #@34
    iget-object v11, v8, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@36
    iget-object v11, v11, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@38
    iget-object v11, v11, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    #@3a
    iget-object v12, v8, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@3c
    iget-object v12, v12, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    #@3e
    invoke-direct {v10, v11, v12}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@41
    invoke-virtual {v9, v10}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@44
    .line 457
    invoke-virtual {v8, v7}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    #@47
    move-result-object v10

    #@48
    move/from16 v0, p3

    #@4a
    invoke-virtual {p0, p1, p2, v0, v10}, Lcom/android/internal/view/menu/MenuBuilder;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    #@4d
    move-result-object v10

    #@4e
    invoke-virtual {v8, v7}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    #@51
    move-result-object v11

    #@52
    invoke-interface {v10, v11}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    #@55
    move-result-object v10

    #@56
    invoke-interface {v10, v9}, Landroid/view/MenuItem;->setIntent(Landroid/content/Intent;)Landroid/view/MenuItem;

    #@59
    move-result-object v5

    #@5a
    .line 460
    .local v5, item:Landroid/view/MenuItem;
    if-eqz p8, :cond_64

    #@5c
    iget v10, v8, Landroid/content/pm/ResolveInfo;->specificIndex:I

    #@5e
    if-ltz v10, :cond_64

    #@60
    .line 461
    iget v10, v8, Landroid/content/pm/ResolveInfo;->specificIndex:I

    #@62
    aput-object v5, p8, v10

    #@64
    .line 450
    :cond_64
    add-int/lit8 v4, v4, 0x1

    #@66
    goto :goto_1f

    #@67
    .line 444
    .end local v3           #N:I
    .end local v4           #i:I
    .end local v5           #item:Landroid/view/MenuItem;
    .end local v8           #ri:Landroid/content/pm/ResolveInfo;
    .end local v9           #rintent:Landroid/content/Intent;
    :cond_67
    const/4 v3, 0x0

    #@68
    goto :goto_17

    #@69
    .line 452
    .restart local v3       #N:I
    .restart local v4       #i:I
    .restart local v8       #ri:Landroid/content/pm/ResolveInfo;
    :cond_69
    iget v10, v8, Landroid/content/pm/ResolveInfo;->specificIndex:I

    #@6b
    aget-object v10, p5, v10

    #@6d
    goto :goto_2f

    #@6e
    .line 465
    .end local v8           #ri:Landroid/content/pm/ResolveInfo;
    :cond_6e
    return v3
.end method

.method public addMenuPresenter(Lcom/android/internal/view/menu/MenuPresenter;)V
    .registers 4
    .parameter "presenter"

    #@0
    .prologue
    .line 215
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@2
    new-instance v1, Ljava/lang/ref/WeakReference;

    #@4
    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@7
    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    #@a
    .line 216
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mContext:Landroid/content/Context;

    #@c
    invoke-interface {p1, v0, p0}, Lcom/android/internal/view/menu/MenuPresenter;->initForMenu(Landroid/content/Context;Lcom/android/internal/view/menu/MenuBuilder;)V

    #@f
    .line 217
    const/4 v0, 0x1

    #@10
    iput-boolean v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mIsActionItemsStale:Z

    #@12
    .line 218
    return-void
.end method

.method public addSubMenu(I)Landroid/view/SubMenu;
    .registers 4
    .parameter "titleRes"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 424
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mResources:Landroid/content/res/Resources;

    #@3
    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    invoke-virtual {p0, v1, v1, v1, v0}, Lcom/android/internal/view/menu/MenuBuilder;->addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;

    #@a
    move-result-object v0

    #@b
    return-object v0
.end method

.method public addSubMenu(IIII)Landroid/view/SubMenu;
    .registers 6
    .parameter "group"
    .parameter "id"
    .parameter "categoryOrder"
    .parameter "title"

    #@0
    .prologue
    .line 436
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mResources:Landroid/content/res/Resources;

    #@2
    invoke-virtual {v0, p4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/android/internal/view/menu/MenuBuilder;->addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;

    #@9
    move-result-object v0

    #@a
    return-object v0
.end method

.method public addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;
    .registers 8
    .parameter "group"
    .parameter "id"
    .parameter "categoryOrder"
    .parameter "title"

    #@0
    .prologue
    .line 428
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/internal/view/menu/MenuBuilder;->addInternal(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    #@3
    move-result-object v0

    #@4
    check-cast v0, Lcom/android/internal/view/menu/MenuItemImpl;

    #@6
    .line 429
    .local v0, item:Lcom/android/internal/view/menu/MenuItemImpl;
    new-instance v1, Lcom/android/internal/view/menu/SubMenuBuilder;

    #@8
    iget-object v2, p0, Lcom/android/internal/view/menu/MenuBuilder;->mContext:Landroid/content/Context;

    #@a
    invoke-direct {v1, v2, p0, v0}, Lcom/android/internal/view/menu/SubMenuBuilder;-><init>(Landroid/content/Context;Lcom/android/internal/view/menu/MenuBuilder;Lcom/android/internal/view/menu/MenuItemImpl;)V

    #@d
    .line 430
    .local v1, subMenu:Lcom/android/internal/view/menu/SubMenuBuilder;
    invoke-virtual {v0, v1}, Lcom/android/internal/view/menu/MenuItemImpl;->setSubMenu(Lcom/android/internal/view/menu/SubMenuBuilder;)V

    #@10
    .line 432
    return-object v1
.end method

.method public addSubMenu(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
    .registers 3
    .parameter "title"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 420
    invoke-virtual {p0, v0, v0, v0, p1}, Lcom/android/internal/view/menu/MenuBuilder;->addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public changeMenuMode()V
    .registers 2

    #@0
    .prologue
    .line 742
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mCallback:Lcom/android/internal/view/menu/MenuBuilder$Callback;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 743
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mCallback:Lcom/android/internal/view/menu/MenuBuilder$Callback;

    #@6
    invoke-interface {v0, p0}, Lcom/android/internal/view/menu/MenuBuilder$Callback;->onMenuModeChange(Lcom/android/internal/view/menu/MenuBuilder;)V

    #@9
    .line 745
    :cond_9
    return-void
.end method

.method public clear()V
    .registers 2

    #@0
    .prologue
    .line 520
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mExpandedItem:Lcom/android/internal/view/menu/MenuItemImpl;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 521
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mExpandedItem:Lcom/android/internal/view/menu/MenuItemImpl;

    #@6
    invoke-virtual {p0, v0}, Lcom/android/internal/view/menu/MenuBuilder;->collapseItemActionView(Lcom/android/internal/view/menu/MenuItemImpl;)Z

    #@9
    .line 523
    :cond_9
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mItems:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    #@e
    .line 525
    const/4 v0, 0x1

    #@f
    invoke-virtual {p0, v0}, Lcom/android/internal/view/menu/MenuBuilder;->onItemsChanged(Z)V

    #@12
    .line 526
    return-void
.end method

.method public clearAll()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v0, 0x0

    #@2
    .line 511
    iput-boolean v1, p0, Lcom/android/internal/view/menu/MenuBuilder;->mPreventDispatchingItemsChanged:Z

    #@4
    .line 512
    invoke-virtual {p0}, Lcom/android/internal/view/menu/MenuBuilder;->clear()V

    #@7
    .line 513
    invoke-virtual {p0}, Lcom/android/internal/view/menu/MenuBuilder;->clearHeader()V

    #@a
    .line 514
    iput-boolean v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mPreventDispatchingItemsChanged:Z

    #@c
    .line 515
    iput-boolean v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mItemsChangedWhileDispatchPrevented:Z

    #@e
    .line 516
    invoke-virtual {p0, v1}, Lcom/android/internal/view/menu/MenuBuilder;->onItemsChanged(Z)V

    #@11
    .line 517
    return-void
.end method

.method public clearHeader()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 1086
    iput-object v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mHeaderIcon:Landroid/graphics/drawable/Drawable;

    #@3
    .line 1087
    iput-object v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mHeaderTitle:Ljava/lang/CharSequence;

    #@5
    .line 1088
    iput-object v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mHeaderView:Landroid/view/View;

    #@7
    .line 1090
    const/4 v0, 0x0

    #@8
    invoke-virtual {p0, v0}, Lcom/android/internal/view/menu/MenuBuilder;->onItemsChanged(Z)V

    #@b
    .line 1091
    return-void
.end method

.method public close()V
    .registers 2

    #@0
    .prologue
    .line 929
    const/4 v0, 0x1

    #@1
    invoke-virtual {p0, v0}, Lcom/android/internal/view/menu/MenuBuilder;->close(Z)V

    #@4
    .line 930
    return-void
.end method

.method final close(Z)V
    .registers 6
    .parameter "allMenusAreClosing"

    #@0
    .prologue
    .line 913
    iget-boolean v3, p0, Lcom/android/internal/view/menu/MenuBuilder;->mIsClosing:Z

    #@2
    if-eqz v3, :cond_5

    #@4
    .line 925
    :goto_4
    return-void

    #@5
    .line 915
    :cond_5
    const/4 v3, 0x1

    #@6
    iput-boolean v3, p0, Lcom/android/internal/view/menu/MenuBuilder;->mIsClosing:Z

    #@8
    .line 916
    iget-object v3, p0, Lcom/android/internal/view/menu/MenuBuilder;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@a
    invoke-virtual {v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    #@d
    move-result-object v0

    #@e
    .local v0, i$:Ljava/util/Iterator;
    :goto_e
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@11
    move-result v3

    #@12
    if-eqz v3, :cond_2c

    #@14
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@17
    move-result-object v2

    #@18
    check-cast v2, Ljava/lang/ref/WeakReference;

    #@1a
    .line 917
    .local v2, ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Lcom/android/internal/view/menu/MenuPresenter;>;"
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@1d
    move-result-object v1

    #@1e
    check-cast v1, Lcom/android/internal/view/menu/MenuPresenter;

    #@20
    .line 918
    .local v1, presenter:Lcom/android/internal/view/menu/MenuPresenter;
    if-nez v1, :cond_28

    #@22
    .line 919
    iget-object v3, p0, Lcom/android/internal/view/menu/MenuBuilder;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@24
    invoke-virtual {v3, v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    #@27
    goto :goto_e

    #@28
    .line 921
    :cond_28
    invoke-interface {v1, p0, p1}, Lcom/android/internal/view/menu/MenuPresenter;->onCloseMenu(Lcom/android/internal/view/menu/MenuBuilder;Z)V

    #@2b
    goto :goto_e

    #@2c
    .line 924
    .end local v1           #presenter:Lcom/android/internal/view/menu/MenuPresenter;
    .end local v2           #ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Lcom/android/internal/view/menu/MenuPresenter;>;"
    :cond_2c
    const/4 v3, 0x0

    #@2d
    iput-boolean v3, p0, Lcom/android/internal/view/menu/MenuBuilder;->mIsClosing:Z

    #@2f
    goto :goto_4
.end method

.method public collapseItemActionView(Lcom/android/internal/view/menu/MenuItemImpl;)Z
    .registers 7
    .parameter "item"

    #@0
    .prologue
    .line 1258
    iget-object v4, p0, Lcom/android/internal/view/menu/MenuBuilder;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@2
    invoke-virtual {v4}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    #@5
    move-result v4

    #@6
    if-nez v4, :cond_c

    #@8
    iget-object v4, p0, Lcom/android/internal/view/menu/MenuBuilder;->mExpandedItem:Lcom/android/internal/view/menu/MenuItemImpl;

    #@a
    if-eq v4, p1, :cond_e

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    .line 1276
    :cond_d
    :goto_d
    return v0

    #@e
    .line 1260
    :cond_e
    const/4 v0, 0x0

    #@f
    .line 1262
    .local v0, collapsed:Z
    invoke-virtual {p0}, Lcom/android/internal/view/menu/MenuBuilder;->stopDispatchingItemsChanged()V

    #@12
    .line 1263
    iget-object v4, p0, Lcom/android/internal/view/menu/MenuBuilder;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@14
    invoke-virtual {v4}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    #@17
    move-result-object v1

    #@18
    .local v1, i$:Ljava/util/Iterator;
    :cond_18
    :goto_18
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@1b
    move-result v4

    #@1c
    if-eqz v4, :cond_38

    #@1e
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@21
    move-result-object v3

    #@22
    check-cast v3, Ljava/lang/ref/WeakReference;

    #@24
    .line 1264
    .local v3, ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Lcom/android/internal/view/menu/MenuPresenter;>;"
    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@27
    move-result-object v2

    #@28
    check-cast v2, Lcom/android/internal/view/menu/MenuPresenter;

    #@2a
    .line 1265
    .local v2, presenter:Lcom/android/internal/view/menu/MenuPresenter;
    if-nez v2, :cond_32

    #@2c
    .line 1266
    iget-object v4, p0, Lcom/android/internal/view/menu/MenuBuilder;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@2e
    invoke-virtual {v4, v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    #@31
    goto :goto_18

    #@32
    .line 1267
    :cond_32
    invoke-interface {v2, p0, p1}, Lcom/android/internal/view/menu/MenuPresenter;->collapseItemActionView(Lcom/android/internal/view/menu/MenuBuilder;Lcom/android/internal/view/menu/MenuItemImpl;)Z

    #@35
    move-result v0

    #@36
    if-eqz v0, :cond_18

    #@38
    .line 1271
    .end local v2           #presenter:Lcom/android/internal/view/menu/MenuPresenter;
    .end local v3           #ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Lcom/android/internal/view/menu/MenuPresenter;>;"
    :cond_38
    invoke-virtual {p0}, Lcom/android/internal/view/menu/MenuBuilder;->startDispatchingItemsChanged()V

    #@3b
    .line 1273
    if-eqz v0, :cond_d

    #@3d
    .line 1274
    const/4 v4, 0x0

    #@3e
    iput-object v4, p0, Lcom/android/internal/view/menu/MenuBuilder;->mExpandedItem:Lcom/android/internal/view/menu/MenuItemImpl;

    #@40
    goto :goto_d
.end method

.method dispatchMenuItemSelected(Lcom/android/internal/view/menu/MenuBuilder;Landroid/view/MenuItem;)Z
    .registers 4
    .parameter "menu"
    .parameter "item"

    #@0
    .prologue
    .line 735
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mCallback:Lcom/android/internal/view/menu/MenuBuilder$Callback;

    #@2
    if-eqz v0, :cond_e

    #@4
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mCallback:Lcom/android/internal/view/menu/MenuBuilder$Callback;

    #@6
    invoke-interface {v0, p1, p2}, Lcom/android/internal/view/menu/MenuBuilder$Callback;->onMenuItemSelected(Lcom/android/internal/view/menu/MenuBuilder;Landroid/view/MenuItem;)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public expandItemActionView(Lcom/android/internal/view/menu/MenuItemImpl;)Z
    .registers 7
    .parameter "item"

    #@0
    .prologue
    .line 1236
    iget-object v4, p0, Lcom/android/internal/view/menu/MenuBuilder;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@2
    invoke-virtual {v4}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    #@5
    move-result v4

    #@6
    if-eqz v4, :cond_a

    #@8
    const/4 v0, 0x0

    #@9
    .line 1254
    :cond_9
    :goto_9
    return v0

    #@a
    .line 1238
    :cond_a
    const/4 v0, 0x0

    #@b
    .line 1240
    .local v0, expanded:Z
    invoke-virtual {p0}, Lcom/android/internal/view/menu/MenuBuilder;->stopDispatchingItemsChanged()V

    #@e
    .line 1241
    iget-object v4, p0, Lcom/android/internal/view/menu/MenuBuilder;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@10
    invoke-virtual {v4}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    #@13
    move-result-object v1

    #@14
    .local v1, i$:Ljava/util/Iterator;
    :cond_14
    :goto_14
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@17
    move-result v4

    #@18
    if-eqz v4, :cond_34

    #@1a
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1d
    move-result-object v3

    #@1e
    check-cast v3, Ljava/lang/ref/WeakReference;

    #@20
    .line 1242
    .local v3, ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Lcom/android/internal/view/menu/MenuPresenter;>;"
    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@23
    move-result-object v2

    #@24
    check-cast v2, Lcom/android/internal/view/menu/MenuPresenter;

    #@26
    .line 1243
    .local v2, presenter:Lcom/android/internal/view/menu/MenuPresenter;
    if-nez v2, :cond_2e

    #@28
    .line 1244
    iget-object v4, p0, Lcom/android/internal/view/menu/MenuBuilder;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@2a
    invoke-virtual {v4, v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    #@2d
    goto :goto_14

    #@2e
    .line 1245
    :cond_2e
    invoke-interface {v2, p0, p1}, Lcom/android/internal/view/menu/MenuPresenter;->expandItemActionView(Lcom/android/internal/view/menu/MenuBuilder;Lcom/android/internal/view/menu/MenuItemImpl;)Z

    #@31
    move-result v0

    #@32
    if-eqz v0, :cond_14

    #@34
    .line 1249
    .end local v2           #presenter:Lcom/android/internal/view/menu/MenuPresenter;
    .end local v3           #ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Lcom/android/internal/view/menu/MenuPresenter;>;"
    :cond_34
    invoke-virtual {p0}, Lcom/android/internal/view/menu/MenuBuilder;->startDispatchingItemsChanged()V

    #@37
    .line 1251
    if-eqz v0, :cond_9

    #@39
    .line 1252
    iput-object p1, p0, Lcom/android/internal/view/menu/MenuBuilder;->mExpandedItem:Lcom/android/internal/view/menu/MenuItemImpl;

    #@3b
    goto :goto_9
.end method

.method public findGroupIndex(I)I
    .registers 3
    .parameter "group"

    #@0
    .prologue
    .line 629
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0}, Lcom/android/internal/view/menu/MenuBuilder;->findGroupIndex(II)I

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public findGroupIndex(II)I
    .registers 7
    .parameter "group"
    .parameter "start"

    #@0
    .prologue
    .line 633
    invoke-virtual {p0}, Lcom/android/internal/view/menu/MenuBuilder;->size()I

    #@3
    move-result v2

    #@4
    .line 635
    .local v2, size:I
    if-gez p2, :cond_7

    #@6
    .line 636
    const/4 p2, 0x0

    #@7
    .line 639
    :cond_7
    move v0, p2

    #@8
    .local v0, i:I
    :goto_8
    if-ge v0, v2, :cond_1c

    #@a
    .line 640
    iget-object v3, p0, Lcom/android/internal/view/menu/MenuBuilder;->mItems:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    check-cast v1, Lcom/android/internal/view/menu/MenuItemImpl;

    #@12
    .line 642
    .local v1, item:Lcom/android/internal/view/menu/MenuItemImpl;
    invoke-virtual {v1}, Lcom/android/internal/view/menu/MenuItemImpl;->getGroupId()I

    #@15
    move-result v3

    #@16
    if-ne v3, p1, :cond_19

    #@18
    .line 647
    .end local v0           #i:I
    .end local v1           #item:Lcom/android/internal/view/menu/MenuItemImpl;
    :goto_18
    return v0

    #@19
    .line 639
    .restart local v0       #i:I
    .restart local v1       #item:Lcom/android/internal/view/menu/MenuItemImpl;
    :cond_19
    add-int/lit8 v0, v0, 0x1

    #@1b
    goto :goto_8

    #@1c
    .line 647
    .end local v1           #item:Lcom/android/internal/view/menu/MenuItemImpl;
    :cond_1c
    const/4 v0, -0x1

    #@1d
    goto :goto_18
.end method

.method public findItem(I)Landroid/view/MenuItem;
    .registers 7
    .parameter "id"

    #@0
    .prologue
    .line 598
    invoke-virtual {p0}, Lcom/android/internal/view/menu/MenuBuilder;->size()I

    #@3
    move-result v3

    #@4
    .line 599
    .local v3, size:I
    const/4 v0, 0x0

    #@5
    .local v0, i:I
    :goto_5
    if-ge v0, v3, :cond_2b

    #@7
    .line 600
    iget-object v4, p0, Lcom/android/internal/view/menu/MenuBuilder;->mItems:Ljava/util/ArrayList;

    #@9
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@c
    move-result-object v1

    #@d
    check-cast v1, Lcom/android/internal/view/menu/MenuItemImpl;

    #@f
    .line 601
    .local v1, item:Lcom/android/internal/view/menu/MenuItemImpl;
    invoke-virtual {v1}, Lcom/android/internal/view/menu/MenuItemImpl;->getItemId()I

    #@12
    move-result v4

    #@13
    if-ne v4, p1, :cond_16

    #@15
    .line 612
    .end local v1           #item:Lcom/android/internal/view/menu/MenuItemImpl;
    :goto_15
    return-object v1

    #@16
    .line 603
    .restart local v1       #item:Lcom/android/internal/view/menu/MenuItemImpl;
    :cond_16
    invoke-virtual {v1}, Lcom/android/internal/view/menu/MenuItemImpl;->hasSubMenu()Z

    #@19
    move-result v4

    #@1a
    if-eqz v4, :cond_28

    #@1c
    .line 604
    invoke-virtual {v1}, Lcom/android/internal/view/menu/MenuItemImpl;->getSubMenu()Landroid/view/SubMenu;

    #@1f
    move-result-object v4

    #@20
    invoke-interface {v4, p1}, Landroid/view/SubMenu;->findItem(I)Landroid/view/MenuItem;

    #@23
    move-result-object v2

    #@24
    .line 606
    .local v2, possibleItem:Landroid/view/MenuItem;
    if-eqz v2, :cond_28

    #@26
    move-object v1, v2

    #@27
    .line 607
    goto :goto_15

    #@28
    .line 599
    .end local v2           #possibleItem:Landroid/view/MenuItem;
    :cond_28
    add-int/lit8 v0, v0, 0x1

    #@2a
    goto :goto_5

    #@2b
    .line 612
    .end local v1           #item:Lcom/android/internal/view/menu/MenuItemImpl;
    :cond_2b
    const/4 v1, 0x0

    #@2c
    goto :goto_15
.end method

.method public findItemIndex(I)I
    .registers 6
    .parameter "id"

    #@0
    .prologue
    .line 616
    invoke-virtual {p0}, Lcom/android/internal/view/menu/MenuBuilder;->size()I

    #@3
    move-result v2

    #@4
    .line 618
    .local v2, size:I
    const/4 v0, 0x0

    #@5
    .local v0, i:I
    :goto_5
    if-ge v0, v2, :cond_19

    #@7
    .line 619
    iget-object v3, p0, Lcom/android/internal/view/menu/MenuBuilder;->mItems:Ljava/util/ArrayList;

    #@9
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@c
    move-result-object v1

    #@d
    check-cast v1, Lcom/android/internal/view/menu/MenuItemImpl;

    #@f
    .line 620
    .local v1, item:Lcom/android/internal/view/menu/MenuItemImpl;
    invoke-virtual {v1}, Lcom/android/internal/view/menu/MenuItemImpl;->getItemId()I

    #@12
    move-result v3

    #@13
    if-ne v3, p1, :cond_16

    #@15
    .line 625
    .end local v0           #i:I
    .end local v1           #item:Lcom/android/internal/view/menu/MenuItemImpl;
    :goto_15
    return v0

    #@16
    .line 618
    .restart local v0       #i:I
    .restart local v1       #item:Lcom/android/internal/view/menu/MenuItemImpl;
    :cond_16
    add-int/lit8 v0, v0, 0x1

    #@18
    goto :goto_5

    #@19
    .line 625
    .end local v1           #item:Lcom/android/internal/view/menu/MenuItemImpl;
    :cond_19
    const/4 v0, -0x1

    #@1a
    goto :goto_15
.end method

.method findItemWithShortcutForKey(ILandroid/view/KeyEvent;)Lcom/android/internal/view/menu/MenuItemImpl;
    .registers 15
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    const/4 v11, 0x0

    #@2
    .line 824
    iget-object v2, p0, Lcom/android/internal/view/menu/MenuBuilder;->mTempShortcutItemList:Ljava/util/ArrayList;

    #@4
    .line 825
    .local v2, items:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/view/menu/MenuItemImpl;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    #@7
    .line 826
    invoke-virtual {p0, v2, p1, p2}, Lcom/android/internal/view/menu/MenuBuilder;->findItemsWithShortcutForKey(Ljava/util/List;ILandroid/view/KeyEvent;)V

    #@a
    .line 828
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    #@d
    move-result v9

    #@e
    if-eqz v9, :cond_11

    #@10
    .line 859
    :cond_10
    :goto_10
    return-object v8

    #@11
    .line 832
    :cond_11
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getMetaState()I

    #@14
    move-result v3

    #@15
    .line 833
    .local v3, metaState:I
    new-instance v4, Landroid/view/KeyCharacterMap$KeyData;

    #@17
    invoke-direct {v4}, Landroid/view/KeyCharacterMap$KeyData;-><init>()V

    #@1a
    .line 835
    .local v4, possibleChars:Landroid/view/KeyCharacterMap$KeyData;
    invoke-virtual {p2, v4}, Landroid/view/KeyEvent;->getKeyData(Landroid/view/KeyCharacterMap$KeyData;)Z

    #@1d
    .line 838
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@20
    move-result v7

    #@21
    .line 839
    .local v7, size:I
    const/4 v9, 0x1

    #@22
    if-ne v7, v9, :cond_2b

    #@24
    .line 840
    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@27
    move-result-object v8

    #@28
    check-cast v8, Lcom/android/internal/view/menu/MenuItemImpl;

    #@2a
    goto :goto_10

    #@2b
    .line 843
    :cond_2b
    invoke-virtual {p0}, Lcom/android/internal/view/menu/MenuBuilder;->isQwertyMode()Z

    #@2e
    move-result v5

    #@2f
    .line 846
    .local v5, qwerty:Z
    const/4 v0, 0x0

    #@30
    .local v0, i:I
    :goto_30
    if-ge v0, v7, :cond_10

    #@32
    .line 847
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@35
    move-result-object v1

    #@36
    check-cast v1, Lcom/android/internal/view/menu/MenuItemImpl;

    #@38
    .line 848
    .local v1, item:Lcom/android/internal/view/menu/MenuItemImpl;
    if-eqz v5, :cond_5f

    #@3a
    invoke-virtual {v1}, Lcom/android/internal/view/menu/MenuItemImpl;->getAlphabeticShortcut()C

    #@3d
    move-result v6

    #@3e
    .line 850
    .local v6, shortcutChar:C
    :goto_3e
    iget-object v9, v4, Landroid/view/KeyCharacterMap$KeyData;->meta:[C

    #@40
    aget-char v9, v9, v11

    #@42
    if-ne v6, v9, :cond_48

    #@44
    and-int/lit8 v9, v3, 0x2

    #@46
    if-eqz v9, :cond_5d

    #@48
    :cond_48
    iget-object v9, v4, Landroid/view/KeyCharacterMap$KeyData;->meta:[C

    #@4a
    const/4 v10, 0x2

    #@4b
    aget-char v9, v9, v10

    #@4d
    if-ne v6, v9, :cond_53

    #@4f
    and-int/lit8 v9, v3, 0x2

    #@51
    if-nez v9, :cond_5d

    #@53
    :cond_53
    if-eqz v5, :cond_64

    #@55
    const/16 v9, 0x8

    #@57
    if-ne v6, v9, :cond_64

    #@59
    const/16 v9, 0x43

    #@5b
    if-ne p1, v9, :cond_64

    #@5d
    :cond_5d
    move-object v8, v1

    #@5e
    .line 856
    goto :goto_10

    #@5f
    .line 848
    .end local v6           #shortcutChar:C
    :cond_5f
    invoke-virtual {v1}, Lcom/android/internal/view/menu/MenuItemImpl;->getNumericShortcut()C

    #@62
    move-result v6

    #@63
    goto :goto_3e

    #@64
    .line 846
    .restart local v6       #shortcutChar:C
    :cond_64
    add-int/lit8 v0, v0, 0x1

    #@66
    goto :goto_30
.end method

.method findItemsWithShortcutForKey(Ljava/util/List;ILandroid/view/KeyEvent;)V
    .registers 15
    .parameter
    .parameter "keyCode"
    .parameter "event"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/view/menu/MenuItemImpl;",
            ">;I",
            "Landroid/view/KeyEvent;",
            ")V"
        }
    .end annotation

    #@0
    .prologue
    .local p1, items:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/view/menu/MenuItemImpl;>;"
    const/16 v10, 0x43

    #@2
    .line 781
    invoke-virtual {p0}, Lcom/android/internal/view/menu/MenuBuilder;->isQwertyMode()Z

    #@5
    move-result v6

    #@6
    .line 782
    .local v6, qwerty:Z
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getMetaState()I

    #@9
    move-result v4

    #@a
    .line 783
    .local v4, metaState:I
    new-instance v5, Landroid/view/KeyCharacterMap$KeyData;

    #@c
    invoke-direct {v5}, Landroid/view/KeyCharacterMap$KeyData;-><init>()V

    #@f
    .line 785
    .local v5, possibleChars:Landroid/view/KeyCharacterMap$KeyData;
    invoke-virtual {p3, v5}, Landroid/view/KeyEvent;->getKeyData(Landroid/view/KeyCharacterMap$KeyData;)Z

    #@12
    move-result v2

    #@13
    .line 787
    .local v2, isKeyCodeMapped:Z
    if-nez v2, :cond_18

    #@15
    if-eq p2, v10, :cond_18

    #@17
    .line 809
    :cond_17
    return-void

    #@18
    .line 792
    :cond_18
    iget-object v8, p0, Lcom/android/internal/view/menu/MenuBuilder;->mItems:Ljava/util/ArrayList;

    #@1a
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    #@1d
    move-result v0

    #@1e
    .line 793
    .local v0, N:I
    const/4 v1, 0x0

    #@1f
    .local v1, i:I
    :goto_1f
    if-ge v1, v0, :cond_17

    #@21
    .line 794
    iget-object v8, p0, Lcom/android/internal/view/menu/MenuBuilder;->mItems:Ljava/util/ArrayList;

    #@23
    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@26
    move-result-object v3

    #@27
    check-cast v3, Lcom/android/internal/view/menu/MenuItemImpl;

    #@29
    .line 795
    .local v3, item:Lcom/android/internal/view/menu/MenuItemImpl;
    invoke-virtual {v3}, Lcom/android/internal/view/menu/MenuItemImpl;->hasSubMenu()Z

    #@2c
    move-result v8

    #@2d
    if-eqz v8, :cond_38

    #@2f
    .line 796
    invoke-virtual {v3}, Lcom/android/internal/view/menu/MenuItemImpl;->getSubMenu()Landroid/view/SubMenu;

    #@32
    move-result-object v8

    #@33
    check-cast v8, Lcom/android/internal/view/menu/MenuBuilder;

    #@35
    invoke-virtual {v8, p1, p2, p3}, Lcom/android/internal/view/menu/MenuBuilder;->findItemsWithShortcutForKey(Ljava/util/List;ILandroid/view/KeyEvent;)V

    #@38
    .line 798
    :cond_38
    if-eqz v6, :cond_66

    #@3a
    invoke-virtual {v3}, Lcom/android/internal/view/menu/MenuItemImpl;->getAlphabeticShortcut()C

    #@3d
    move-result v7

    #@3e
    .line 799
    .local v7, shortcutChar:C
    :goto_3e
    and-int/lit8 v8, v4, 0x5

    #@40
    if-nez v8, :cond_63

    #@42
    if-eqz v7, :cond_63

    #@44
    iget-object v8, v5, Landroid/view/KeyCharacterMap$KeyData;->meta:[C

    #@46
    const/4 v9, 0x0

    #@47
    aget-char v8, v8, v9

    #@49
    if-eq v7, v8, :cond_5a

    #@4b
    iget-object v8, v5, Landroid/view/KeyCharacterMap$KeyData;->meta:[C

    #@4d
    const/4 v9, 0x2

    #@4e
    aget-char v8, v8, v9

    #@50
    if-eq v7, v8, :cond_5a

    #@52
    if-eqz v6, :cond_63

    #@54
    const/16 v8, 0x8

    #@56
    if-ne v7, v8, :cond_63

    #@58
    if-ne p2, v10, :cond_63

    #@5a
    :cond_5a
    invoke-virtual {v3}, Lcom/android/internal/view/menu/MenuItemImpl;->isEnabled()Z

    #@5d
    move-result v8

    #@5e
    if-eqz v8, :cond_63

    #@60
    .line 806
    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@63
    .line 793
    :cond_63
    add-int/lit8 v1, v1, 0x1

    #@65
    goto :goto_1f

    #@66
    .line 798
    .end local v7           #shortcutChar:C
    :cond_66
    invoke-virtual {v3}, Lcom/android/internal/view/menu/MenuItemImpl;->getNumericShortcut()C

    #@69
    move-result v7

    #@6a
    goto :goto_3e
.end method

.method public flagActionItems()V
    .registers 11

    #@0
    .prologue
    .line 1037
    iget-boolean v8, p0, Lcom/android/internal/view/menu/MenuBuilder;->mIsActionItemsStale:Z

    #@2
    if-nez v8, :cond_5

    #@4
    .line 1073
    :goto_4
    return-void

    #@5
    .line 1042
    :cond_5
    const/4 v0, 0x0

    #@6
    .line 1043
    .local v0, flagged:Z
    iget-object v8, p0, Lcom/android/internal/view/menu/MenuBuilder;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@8
    invoke-virtual {v8}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    #@b
    move-result-object v2

    #@c
    .local v2, i$:Ljava/util/Iterator;
    :goto_c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@f
    move-result v8

    #@10
    if-eqz v8, :cond_2c

    #@12
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@15
    move-result-object v6

    #@16
    check-cast v6, Ljava/lang/ref/WeakReference;

    #@18
    .line 1044
    .local v6, ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Lcom/android/internal/view/menu/MenuPresenter;>;"
    invoke-virtual {v6}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@1b
    move-result-object v5

    #@1c
    check-cast v5, Lcom/android/internal/view/menu/MenuPresenter;

    #@1e
    .line 1045
    .local v5, presenter:Lcom/android/internal/view/menu/MenuPresenter;
    if-nez v5, :cond_26

    #@20
    .line 1046
    iget-object v8, p0, Lcom/android/internal/view/menu/MenuBuilder;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@22
    invoke-virtual {v8, v6}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    #@25
    goto :goto_c

    #@26
    .line 1048
    :cond_26
    invoke-interface {v5}, Lcom/android/internal/view/menu/MenuPresenter;->flagActionItems()Z

    #@29
    move-result v8

    #@2a
    or-int/2addr v0, v8

    #@2b
    goto :goto_c

    #@2c
    .line 1052
    .end local v5           #presenter:Lcom/android/internal/view/menu/MenuPresenter;
    .end local v6           #ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Lcom/android/internal/view/menu/MenuPresenter;>;"
    :cond_2c
    if-eqz v0, :cond_5d

    #@2e
    .line 1053
    iget-object v8, p0, Lcom/android/internal/view/menu/MenuBuilder;->mActionItems:Ljava/util/ArrayList;

    #@30
    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V

    #@33
    .line 1054
    iget-object v8, p0, Lcom/android/internal/view/menu/MenuBuilder;->mNonActionItems:Ljava/util/ArrayList;

    #@35
    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V

    #@38
    .line 1055
    invoke-virtual {p0}, Lcom/android/internal/view/menu/MenuBuilder;->getVisibleItems()Ljava/util/ArrayList;

    #@3b
    move-result-object v7

    #@3c
    .line 1056
    .local v7, visibleItems:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/view/menu/MenuItemImpl;>;"
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    #@3f
    move-result v4

    #@40
    .line 1057
    .local v4, itemsSize:I
    const/4 v1, 0x0

    #@41
    .local v1, i:I
    :goto_41
    if-ge v1, v4, :cond_70

    #@43
    .line 1058
    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@46
    move-result-object v3

    #@47
    check-cast v3, Lcom/android/internal/view/menu/MenuItemImpl;

    #@49
    .line 1059
    .local v3, item:Lcom/android/internal/view/menu/MenuItemImpl;
    invoke-virtual {v3}, Lcom/android/internal/view/menu/MenuItemImpl;->isActionButton()Z

    #@4c
    move-result v8

    #@4d
    if-eqz v8, :cond_57

    #@4f
    .line 1060
    iget-object v8, p0, Lcom/android/internal/view/menu/MenuBuilder;->mActionItems:Ljava/util/ArrayList;

    #@51
    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@54
    .line 1057
    :goto_54
    add-int/lit8 v1, v1, 0x1

    #@56
    goto :goto_41

    #@57
    .line 1062
    :cond_57
    iget-object v8, p0, Lcom/android/internal/view/menu/MenuBuilder;->mNonActionItems:Ljava/util/ArrayList;

    #@59
    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@5c
    goto :goto_54

    #@5d
    .line 1068
    .end local v1           #i:I
    .end local v3           #item:Lcom/android/internal/view/menu/MenuItemImpl;
    .end local v4           #itemsSize:I
    .end local v7           #visibleItems:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/view/menu/MenuItemImpl;>;"
    :cond_5d
    iget-object v8, p0, Lcom/android/internal/view/menu/MenuBuilder;->mActionItems:Ljava/util/ArrayList;

    #@5f
    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V

    #@62
    .line 1069
    iget-object v8, p0, Lcom/android/internal/view/menu/MenuBuilder;->mNonActionItems:Ljava/util/ArrayList;

    #@64
    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V

    #@67
    .line 1070
    iget-object v8, p0, Lcom/android/internal/view/menu/MenuBuilder;->mNonActionItems:Ljava/util/ArrayList;

    #@69
    invoke-virtual {p0}, Lcom/android/internal/view/menu/MenuBuilder;->getVisibleItems()Ljava/util/ArrayList;

    #@6c
    move-result-object v9

    #@6d
    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    #@70
    .line 1072
    :cond_70
    const/4 v8, 0x0

    #@71
    iput-boolean v8, p0, Lcom/android/internal/view/menu/MenuBuilder;->mIsActionItemsStale:Z

    #@73
    goto :goto_4
.end method

.method getActionItems()Ljava/util/ArrayList;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/view/menu/MenuItemImpl;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1076
    invoke-virtual {p0}, Lcom/android/internal/view/menu/MenuBuilder;->flagActionItems()V

    #@3
    .line 1077
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mActionItems:Ljava/util/ArrayList;

    #@5
    return-object v0
.end method

.method protected getActionViewStatesKey()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 376
    const-string v0, "android:menu:actionviewstates"

    #@2
    return-object v0
.end method

.method public getContext()Landroid/content/Context;
    .registers 2

    #@0
    .prologue
    .line 731
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method public getExpandedItem()Lcom/android/internal/view/menu/MenuItemImpl;
    .registers 2

    #@0
    .prologue
    .line 1280
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mExpandedItem:Lcom/android/internal/view/menu/MenuItemImpl;

    #@2
    return-object v0
.end method

.method public getHeaderIcon()Landroid/graphics/drawable/Drawable;
    .registers 2

    #@0
    .prologue
    .line 1201
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mHeaderIcon:Landroid/graphics/drawable/Drawable;

    #@2
    return-object v0
.end method

.method public getHeaderTitle()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 1197
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mHeaderTitle:Ljava/lang/CharSequence;

    #@2
    return-object v0
.end method

.method public getHeaderView()Landroid/view/View;
    .registers 2

    #@0
    .prologue
    .line 1205
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mHeaderView:Landroid/view/View;

    #@2
    return-object v0
.end method

.method public getItem(I)Landroid/view/MenuItem;
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 656
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mItems:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/view/MenuItem;

    #@8
    return-object v0
.end method

.method getNonActionItems()Ljava/util/ArrayList;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/view/menu/MenuItemImpl;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1081
    invoke-virtual {p0}, Lcom/android/internal/view/menu/MenuBuilder;->flagActionItems()V

    #@3
    .line 1082
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mNonActionItems:Ljava/util/ArrayList;

    #@5
    return-object v0
.end method

.method getOptionalIconsVisible()Z
    .registers 2

    #@0
    .prologue
    .line 1232
    iget-boolean v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mOptionalIconsVisible:Z

    #@2
    return v0
.end method

.method getResources()Landroid/content/res/Resources;
    .registers 2

    #@0
    .prologue
    .line 727
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mResources:Landroid/content/res/Resources;

    #@2
    return-object v0
.end method

.method public getRootMenu()Lcom/android/internal/view/menu/MenuBuilder;
    .registers 1

    #@0
    .prologue
    .line 1213
    return-object p0
.end method

.method getVisibleItems()Ljava/util/ArrayList;
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/view/menu/MenuItemImpl;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 994
    iget-boolean v3, p0, Lcom/android/internal/view/menu/MenuBuilder;->mIsVisibleItemsStale:Z

    #@2
    if-nez v3, :cond_7

    #@4
    iget-object v3, p0, Lcom/android/internal/view/menu/MenuBuilder;->mVisibleItems:Ljava/util/ArrayList;

    #@6
    .line 1009
    :goto_6
    return-object v3

    #@7
    .line 997
    :cond_7
    iget-object v3, p0, Lcom/android/internal/view/menu/MenuBuilder;->mVisibleItems:Ljava/util/ArrayList;

    #@9
    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    #@c
    .line 999
    iget-object v3, p0, Lcom/android/internal/view/menu/MenuBuilder;->mItems:Ljava/util/ArrayList;

    #@e
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@11
    move-result v2

    #@12
    .line 1001
    .local v2, itemsSize:I
    const/4 v0, 0x0

    #@13
    .local v0, i:I
    :goto_13
    if-ge v0, v2, :cond_2b

    #@15
    .line 1002
    iget-object v3, p0, Lcom/android/internal/view/menu/MenuBuilder;->mItems:Ljava/util/ArrayList;

    #@17
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1a
    move-result-object v1

    #@1b
    check-cast v1, Lcom/android/internal/view/menu/MenuItemImpl;

    #@1d
    .line 1003
    .local v1, item:Lcom/android/internal/view/menu/MenuItemImpl;
    invoke-virtual {v1}, Lcom/android/internal/view/menu/MenuItemImpl;->isVisible()Z

    #@20
    move-result v3

    #@21
    if-eqz v3, :cond_28

    #@23
    iget-object v3, p0, Lcom/android/internal/view/menu/MenuBuilder;->mVisibleItems:Ljava/util/ArrayList;

    #@25
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@28
    .line 1001
    :cond_28
    add-int/lit8 v0, v0, 0x1

    #@2a
    goto :goto_13

    #@2b
    .line 1006
    .end local v1           #item:Lcom/android/internal/view/menu/MenuItemImpl;
    :cond_2b
    const/4 v3, 0x0

    #@2c
    iput-boolean v3, p0, Lcom/android/internal/view/menu/MenuBuilder;->mIsVisibleItemsStale:Z

    #@2e
    .line 1007
    const/4 v3, 0x1

    #@2f
    iput-boolean v3, p0, Lcom/android/internal/view/menu/MenuBuilder;->mIsActionItemsStale:Z

    #@31
    .line 1009
    iget-object v3, p0, Lcom/android/internal/view/menu/MenuBuilder;->mVisibleItems:Ljava/util/ArrayList;

    #@33
    goto :goto_6
.end method

.method public hasNonActionItems()Z
    .registers 2

    #@0
    .prologue
    .line 1096
    invoke-virtual {p0}, Lcom/android/internal/view/menu/MenuBuilder;->flagActionItems()V

    #@3
    .line 1097
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mNonActionItems:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@8
    move-result v0

    #@9
    if-lez v0, :cond_d

    #@b
    .line 1098
    const/4 v0, 0x1

    #@c
    .line 1100
    :goto_c
    return v0

    #@d
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_c
.end method

.method public hasVisibleItems()Z
    .registers 5

    #@0
    .prologue
    .line 585
    invoke-virtual {p0}, Lcom/android/internal/view/menu/MenuBuilder;->size()I

    #@3
    move-result v2

    #@4
    .line 587
    .local v2, size:I
    const/4 v0, 0x0

    #@5
    .local v0, i:I
    :goto_5
    if-ge v0, v2, :cond_1a

    #@7
    .line 588
    iget-object v3, p0, Lcom/android/internal/view/menu/MenuBuilder;->mItems:Ljava/util/ArrayList;

    #@9
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@c
    move-result-object v1

    #@d
    check-cast v1, Lcom/android/internal/view/menu/MenuItemImpl;

    #@f
    .line 589
    .local v1, item:Lcom/android/internal/view/menu/MenuItemImpl;
    invoke-virtual {v1}, Lcom/android/internal/view/menu/MenuItemImpl;->isVisible()Z

    #@12
    move-result v3

    #@13
    if-eqz v3, :cond_17

    #@15
    .line 590
    const/4 v3, 0x1

    #@16
    .line 594
    .end local v1           #item:Lcom/android/internal/view/menu/MenuItemImpl;
    :goto_16
    return v3

    #@17
    .line 587
    .restart local v1       #item:Lcom/android/internal/view/menu/MenuItemImpl;
    :cond_17
    add-int/lit8 v0, v0, 0x1

    #@19
    goto :goto_5

    #@1a
    .line 594
    .end local v1           #item:Lcom/android/internal/view/menu/MenuItemImpl;
    :cond_1a
    const/4 v3, 0x0

    #@1b
    goto :goto_16
.end method

.method isQwertyMode()Z
    .registers 2

    #@0
    .prologue
    .line 694
    iget-boolean v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mQwertyMode:Z

    #@2
    return v0
.end method

.method public isShortcutKey(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    .line 660
    invoke-virtual {p0, p1, p2}, Lcom/android/internal/view/menu/MenuBuilder;->findItemWithShortcutForKey(ILandroid/view/KeyEvent;)Lcom/android/internal/view/menu/MenuItemImpl;

    #@3
    move-result-object v0

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public isShortcutsVisible()Z
    .registers 2

    #@0
    .prologue
    .line 723
    iget-boolean v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mShortcutsVisible:Z

    #@2
    return v0
.end method

.method onItemActionRequestChanged(Lcom/android/internal/view/menu/MenuItemImpl;)V
    .registers 3
    .parameter "item"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 989
    iput-boolean v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mIsActionItemsStale:Z

    #@3
    .line 990
    invoke-virtual {p0, v0}, Lcom/android/internal/view/menu/MenuBuilder;->onItemsChanged(Z)V

    #@6
    .line 991
    return-void
.end method

.method onItemVisibleChanged(Lcom/android/internal/view/menu/MenuItemImpl;)V
    .registers 3
    .parameter "item"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 979
    iput-boolean v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mIsVisibleItemsStale:Z

    #@3
    .line 980
    invoke-virtual {p0, v0}, Lcom/android/internal/view/menu/MenuBuilder;->onItemsChanged(Z)V

    #@6
    .line 981
    return-void
.end method

.method onItemsChanged(Z)V
    .registers 4
    .parameter "structureChanged"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 940
    iget-boolean v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mPreventDispatchingItemsChanged:Z

    #@3
    if-nez v0, :cond_f

    #@5
    .line 941
    if-eqz p1, :cond_b

    #@7
    .line 942
    iput-boolean v1, p0, Lcom/android/internal/view/menu/MenuBuilder;->mIsVisibleItemsStale:Z

    #@9
    .line 943
    iput-boolean v1, p0, Lcom/android/internal/view/menu/MenuBuilder;->mIsActionItemsStale:Z

    #@b
    .line 946
    :cond_b
    invoke-direct {p0, p1}, Lcom/android/internal/view/menu/MenuBuilder;->dispatchPresenterUpdate(Z)V

    #@e
    .line 950
    :goto_e
    return-void

    #@f
    .line 948
    :cond_f
    iput-boolean v1, p0, Lcom/android/internal/view/menu/MenuBuilder;->mItemsChangedWhileDispatchPrevented:Z

    #@11
    goto :goto_e
.end method

.method public performIdentifierAction(II)Z
    .registers 4
    .parameter "id"
    .parameter "flags"

    #@0
    .prologue
    .line 864
    invoke-virtual {p0, p1}, Lcom/android/internal/view/menu/MenuBuilder;->findItem(I)Landroid/view/MenuItem;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p0, v0, p2}, Lcom/android/internal/view/menu/MenuBuilder;->performItemAction(Landroid/view/MenuItem;I)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public performItemAction(Landroid/view/MenuItem;I)Z
    .registers 11
    .parameter "item"
    .parameter "flags"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v5, 0x1

    #@2
    .line 868
    move-object v1, p1

    #@3
    check-cast v1, Lcom/android/internal/view/menu/MenuItemImpl;

    #@5
    .line 870
    .local v1, itemImpl:Lcom/android/internal/view/menu/MenuItemImpl;
    if-eqz v1, :cond_d

    #@7
    invoke-virtual {v1}, Lcom/android/internal/view/menu/MenuItemImpl;->isEnabled()Z

    #@a
    move-result v7

    #@b
    if-nez v7, :cond_f

    #@d
    :cond_d
    move v0, v6

    #@e
    .line 900
    :cond_e
    :goto_e
    return v0

    #@f
    .line 874
    :cond_f
    invoke-virtual {v1}, Lcom/android/internal/view/menu/MenuItemImpl;->invoke()Z

    #@12
    move-result v0

    #@13
    .line 876
    .local v0, invoked:Z
    invoke-interface {p1}, Landroid/view/MenuItem;->getActionProvider()Landroid/view/ActionProvider;

    #@16
    move-result-object v2

    #@17
    .line 877
    .local v2, provider:Landroid/view/ActionProvider;
    if-eqz v2, :cond_31

    #@19
    invoke-virtual {v2}, Landroid/view/ActionProvider;->hasSubMenu()Z

    #@1c
    move-result v7

    #@1d
    if-eqz v7, :cond_31

    #@1f
    move v3, v5

    #@20
    .line 878
    .local v3, providerHasSubMenu:Z
    :goto_20
    invoke-virtual {v1}, Lcom/android/internal/view/menu/MenuItemImpl;->hasCollapsibleActionView()Z

    #@23
    move-result v7

    #@24
    if-eqz v7, :cond_33

    #@26
    .line 879
    invoke-virtual {v1}, Lcom/android/internal/view/menu/MenuItemImpl;->expandActionView()Z

    #@29
    move-result v6

    #@2a
    or-int/2addr v0, v6

    #@2b
    .line 880
    if-eqz v0, :cond_e

    #@2d
    invoke-virtual {p0, v5}, Lcom/android/internal/view/menu/MenuBuilder;->close(Z)V

    #@30
    goto :goto_e

    #@31
    .end local v3           #providerHasSubMenu:Z
    :cond_31
    move v3, v6

    #@32
    .line 877
    goto :goto_20

    #@33
    .line 881
    .restart local v3       #providerHasSubMenu:Z
    :cond_33
    invoke-virtual {v1}, Lcom/android/internal/view/menu/MenuItemImpl;->hasSubMenu()Z

    #@36
    move-result v7

    #@37
    if-nez v7, :cond_3b

    #@39
    if-eqz v3, :cond_66

    #@3b
    .line 882
    :cond_3b
    invoke-virtual {p0, v6}, Lcom/android/internal/view/menu/MenuBuilder;->close(Z)V

    #@3e
    .line 884
    invoke-virtual {v1}, Lcom/android/internal/view/menu/MenuItemImpl;->hasSubMenu()Z

    #@41
    move-result v6

    #@42
    if-nez v6, :cond_50

    #@44
    .line 885
    new-instance v6, Lcom/android/internal/view/menu/SubMenuBuilder;

    #@46
    invoke-virtual {p0}, Lcom/android/internal/view/menu/MenuBuilder;->getContext()Landroid/content/Context;

    #@49
    move-result-object v7

    #@4a
    invoke-direct {v6, v7, p0, v1}, Lcom/android/internal/view/menu/SubMenuBuilder;-><init>(Landroid/content/Context;Lcom/android/internal/view/menu/MenuBuilder;Lcom/android/internal/view/menu/MenuItemImpl;)V

    #@4d
    invoke-virtual {v1, v6}, Lcom/android/internal/view/menu/MenuItemImpl;->setSubMenu(Lcom/android/internal/view/menu/SubMenuBuilder;)V

    #@50
    .line 888
    :cond_50
    invoke-virtual {v1}, Lcom/android/internal/view/menu/MenuItemImpl;->getSubMenu()Landroid/view/SubMenu;

    #@53
    move-result-object v4

    #@54
    check-cast v4, Lcom/android/internal/view/menu/SubMenuBuilder;

    #@56
    .line 889
    .local v4, subMenu:Lcom/android/internal/view/menu/SubMenuBuilder;
    if-eqz v3, :cond_5b

    #@58
    .line 890
    invoke-virtual {v2, v4}, Landroid/view/ActionProvider;->onPrepareSubMenu(Landroid/view/SubMenu;)V

    #@5b
    .line 892
    :cond_5b
    invoke-direct {p0, v4}, Lcom/android/internal/view/menu/MenuBuilder;->dispatchSubMenuSelected(Lcom/android/internal/view/menu/SubMenuBuilder;)Z

    #@5e
    move-result v6

    #@5f
    or-int/2addr v0, v6

    #@60
    .line 893
    if-nez v0, :cond_e

    #@62
    invoke-virtual {p0, v5}, Lcom/android/internal/view/menu/MenuBuilder;->close(Z)V

    #@65
    goto :goto_e

    #@66
    .line 895
    .end local v4           #subMenu:Lcom/android/internal/view/menu/SubMenuBuilder;
    :cond_66
    and-int/lit8 v6, p2, 0x1

    #@68
    if-nez v6, :cond_e

    #@6a
    .line 896
    invoke-virtual {p0, v5}, Lcom/android/internal/view/menu/MenuBuilder;->close(Z)V

    #@6d
    goto :goto_e
.end method

.method public performShortcut(ILandroid/view/KeyEvent;I)Z
    .registers 7
    .parameter "keyCode"
    .parameter "event"
    .parameter "flags"

    #@0
    .prologue
    .line 759
    invoke-virtual {p0, p1, p2}, Lcom/android/internal/view/menu/MenuBuilder;->findItemWithShortcutForKey(ILandroid/view/KeyEvent;)Lcom/android/internal/view/menu/MenuItemImpl;

    #@3
    move-result-object v1

    #@4
    .line 761
    .local v1, item:Lcom/android/internal/view/menu/MenuItemImpl;
    const/4 v0, 0x0

    #@5
    .line 763
    .local v0, handled:Z
    if-eqz v1, :cond_b

    #@7
    .line 764
    invoke-virtual {p0, v1, p3}, Lcom/android/internal/view/menu/MenuBuilder;->performItemAction(Landroid/view/MenuItem;I)Z

    #@a
    move-result v0

    #@b
    .line 767
    :cond_b
    and-int/lit8 v2, p3, 0x2

    #@d
    if-eqz v2, :cond_13

    #@f
    .line 768
    const/4 v2, 0x1

    #@10
    invoke-virtual {p0, v2}, Lcom/android/internal/view/menu/MenuBuilder;->close(Z)V

    #@13
    .line 771
    :cond_13
    return v0
.end method

.method public removeGroup(I)V
    .registers 7
    .parameter "group"

    #@0
    .prologue
    .line 473
    invoke-virtual {p0, p1}, Lcom/android/internal/view/menu/MenuBuilder;->findGroupIndex(I)I

    #@3
    move-result v0

    #@4
    .line 475
    .local v0, i:I
    if-ltz v0, :cond_2c

    #@6
    .line 476
    iget-object v4, p0, Lcom/android/internal/view/menu/MenuBuilder;->mItems:Ljava/util/ArrayList;

    #@8
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@b
    move-result v4

    #@c
    sub-int v1, v4, v0

    #@e
    .line 477
    .local v1, maxRemovable:I
    const/4 v2, 0x0

    #@f
    .local v2, numRemoved:I
    move v3, v2

    #@10
    .line 478
    .end local v2           #numRemoved:I
    .local v3, numRemoved:I
    :goto_10
    add-int/lit8 v2, v3, 0x1

    #@12
    .end local v3           #numRemoved:I
    .restart local v2       #numRemoved:I
    if-ge v3, v1, :cond_28

    #@14
    iget-object v4, p0, Lcom/android/internal/view/menu/MenuBuilder;->mItems:Ljava/util/ArrayList;

    #@16
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@19
    move-result-object v4

    #@1a
    check-cast v4, Lcom/android/internal/view/menu/MenuItemImpl;

    #@1c
    invoke-virtual {v4}, Lcom/android/internal/view/menu/MenuItemImpl;->getGroupId()I

    #@1f
    move-result v4

    #@20
    if-ne v4, p1, :cond_28

    #@22
    .line 480
    const/4 v4, 0x0

    #@23
    invoke-direct {p0, v0, v4}, Lcom/android/internal/view/menu/MenuBuilder;->removeItemAtInt(IZ)V

    #@26
    move v3, v2

    #@27
    .end local v2           #numRemoved:I
    .restart local v3       #numRemoved:I
    goto :goto_10

    #@28
    .line 484
    .end local v3           #numRemoved:I
    .restart local v2       #numRemoved:I
    :cond_28
    const/4 v4, 0x1

    #@29
    invoke-virtual {p0, v4}, Lcom/android/internal/view/menu/MenuBuilder;->onItemsChanged(Z)V

    #@2c
    .line 486
    .end local v1           #maxRemovable:I
    .end local v2           #numRemoved:I
    :cond_2c
    return-void
.end method

.method public removeItem(I)V
    .registers 4
    .parameter "id"

    #@0
    .prologue
    .line 469
    invoke-virtual {p0, p1}, Lcom/android/internal/view/menu/MenuBuilder;->findItemIndex(I)I

    #@3
    move-result v0

    #@4
    const/4 v1, 0x1

    #@5
    invoke-direct {p0, v0, v1}, Lcom/android/internal/view/menu/MenuBuilder;->removeItemAtInt(IZ)V

    #@8
    .line 470
    return-void
.end method

.method public removeItemAt(I)V
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 507
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, p1, v0}, Lcom/android/internal/view/menu/MenuBuilder;->removeItemAtInt(IZ)V

    #@4
    .line 508
    return-void
.end method

.method public removeMenuPresenter(Lcom/android/internal/view/menu/MenuPresenter;)V
    .registers 6
    .parameter "presenter"

    #@0
    .prologue
    .line 227
    iget-object v3, p0, Lcom/android/internal/view/menu/MenuBuilder;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@2
    invoke-virtual {v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v0

    #@6
    .local v0, i$:Ljava/util/Iterator;
    :cond_6
    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@9
    move-result v3

    #@a
    if-eqz v3, :cond_22

    #@c
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f
    move-result-object v2

    #@10
    check-cast v2, Ljava/lang/ref/WeakReference;

    #@12
    .line 228
    .local v2, ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Lcom/android/internal/view/menu/MenuPresenter;>;"
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@15
    move-result-object v1

    #@16
    check-cast v1, Lcom/android/internal/view/menu/MenuPresenter;

    #@18
    .line 229
    .local v1, item:Lcom/android/internal/view/menu/MenuPresenter;
    if-eqz v1, :cond_1c

    #@1a
    if-ne v1, p1, :cond_6

    #@1c
    .line 230
    :cond_1c
    iget-object v3, p0, Lcom/android/internal/view/menu/MenuBuilder;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    #@1e
    invoke-virtual {v3, v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    #@21
    goto :goto_6

    #@22
    .line 233
    .end local v1           #item:Lcom/android/internal/view/menu/MenuPresenter;
    .end local v2           #ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Lcom/android/internal/view/menu/MenuPresenter;>;"
    :cond_22
    return-void
.end method

.method public restoreActionViewStates(Landroid/os/Bundle;)V
    .registers 12
    .parameter "states"

    #@0
    .prologue
    .line 346
    if-nez p1, :cond_3

    #@2
    .line 373
    :cond_2
    :goto_2
    return-void

    #@3
    .line 350
    :cond_3
    invoke-virtual {p0}, Lcom/android/internal/view/menu/MenuBuilder;->getActionViewStatesKey()Ljava/lang/String;

    #@6
    move-result-object v8

    #@7
    invoke-virtual {p1, v8}, Landroid/os/Bundle;->getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;

    #@a
    move-result-object v7

    #@b
    .line 353
    .local v7, viewStates:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Landroid/os/Parcelable;>;"
    invoke-virtual {p0}, Lcom/android/internal/view/menu/MenuBuilder;->size()I

    #@e
    move-result v3

    #@f
    .line 354
    .local v3, itemCount:I
    const/4 v1, 0x0

    #@10
    .local v1, i:I
    :goto_10
    if-ge v1, v3, :cond_38

    #@12
    .line 355
    invoke-virtual {p0, v1}, Lcom/android/internal/view/menu/MenuBuilder;->getItem(I)Landroid/view/MenuItem;

    #@15
    move-result-object v2

    #@16
    .line 356
    .local v2, item:Landroid/view/MenuItem;
    invoke-interface {v2}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    #@19
    move-result-object v6

    #@1a
    .line 357
    .local v6, v:Landroid/view/View;
    if-eqz v6, :cond_26

    #@1c
    invoke-virtual {v6}, Landroid/view/View;->getId()I

    #@1f
    move-result v8

    #@20
    const/4 v9, -0x1

    #@21
    if-eq v8, v9, :cond_26

    #@23
    .line 358
    invoke-virtual {v6, v7}, Landroid/view/View;->restoreHierarchyState(Landroid/util/SparseArray;)V

    #@26
    .line 360
    :cond_26
    invoke-interface {v2}, Landroid/view/MenuItem;->hasSubMenu()Z

    #@29
    move-result v8

    #@2a
    if-eqz v8, :cond_35

    #@2c
    .line 361
    invoke-interface {v2}, Landroid/view/MenuItem;->getSubMenu()Landroid/view/SubMenu;

    #@2f
    move-result-object v5

    #@30
    check-cast v5, Lcom/android/internal/view/menu/SubMenuBuilder;

    #@32
    .line 362
    .local v5, subMenu:Lcom/android/internal/view/menu/SubMenuBuilder;
    invoke-virtual {v5, p1}, Lcom/android/internal/view/menu/SubMenuBuilder;->restoreActionViewStates(Landroid/os/Bundle;)V

    #@35
    .line 354
    .end local v5           #subMenu:Lcom/android/internal/view/menu/SubMenuBuilder;
    :cond_35
    add-int/lit8 v1, v1, 0x1

    #@37
    goto :goto_10

    #@38
    .line 366
    .end local v2           #item:Landroid/view/MenuItem;
    .end local v6           #v:Landroid/view/View;
    :cond_38
    const-string v8, "android:menu:expandedactionview"

    #@3a
    invoke-virtual {p1, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@3d
    move-result v0

    #@3e
    .line 367
    .local v0, expandedId:I
    if-lez v0, :cond_2

    #@40
    .line 368
    invoke-virtual {p0, v0}, Lcom/android/internal/view/menu/MenuBuilder;->findItem(I)Landroid/view/MenuItem;

    #@43
    move-result-object v4

    #@44
    .line 369
    .local v4, itemToExpand:Landroid/view/MenuItem;
    if-eqz v4, :cond_2

    #@46
    .line 370
    invoke-interface {v4}, Landroid/view/MenuItem;->expandActionView()Z

    #@49
    goto :goto_2
.end method

.method public restorePresenterStates(Landroid/os/Bundle;)V
    .registers 2
    .parameter "state"

    #@0
    .prologue
    .line 315
    invoke-direct {p0, p1}, Lcom/android/internal/view/menu/MenuBuilder;->dispatchRestoreInstanceState(Landroid/os/Bundle;)V

    #@3
    .line 316
    return-void
.end method

.method public saveActionViewStates(Landroid/os/Bundle;)V
    .registers 10
    .parameter "outStates"

    #@0
    .prologue
    .line 319
    const/4 v5, 0x0

    #@1
    .line 321
    .local v5, viewStates:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Landroid/os/Parcelable;>;"
    invoke-virtual {p0}, Lcom/android/internal/view/menu/MenuBuilder;->size()I

    #@4
    move-result v2

    #@5
    .line 322
    .local v2, itemCount:I
    const/4 v0, 0x0

    #@6
    .local v0, i:I
    :goto_6
    if-ge v0, v2, :cond_44

    #@8
    .line 323
    invoke-virtual {p0, v0}, Lcom/android/internal/view/menu/MenuBuilder;->getItem(I)Landroid/view/MenuItem;

    #@b
    move-result-object v1

    #@c
    .line 324
    .local v1, item:Landroid/view/MenuItem;
    invoke-interface {v1}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    #@f
    move-result-object v4

    #@10
    .line 325
    .local v4, v:Landroid/view/View;
    if-eqz v4, :cond_32

    #@12
    invoke-virtual {v4}, Landroid/view/View;->getId()I

    #@15
    move-result v6

    #@16
    const/4 v7, -0x1

    #@17
    if-eq v6, v7, :cond_32

    #@19
    .line 326
    if-nez v5, :cond_20

    #@1b
    .line 327
    new-instance v5, Landroid/util/SparseArray;

    #@1d
    .end local v5           #viewStates:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Landroid/os/Parcelable;>;"
    invoke-direct {v5}, Landroid/util/SparseArray;-><init>()V

    #@20
    .line 329
    .restart local v5       #viewStates:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Landroid/os/Parcelable;>;"
    :cond_20
    invoke-virtual {v4, v5}, Landroid/view/View;->saveHierarchyState(Landroid/util/SparseArray;)V

    #@23
    .line 330
    invoke-interface {v1}, Landroid/view/MenuItem;->isActionViewExpanded()Z

    #@26
    move-result v6

    #@27
    if-eqz v6, :cond_32

    #@29
    .line 331
    const-string v6, "android:menu:expandedactionview"

    #@2b
    invoke-interface {v1}, Landroid/view/MenuItem;->getItemId()I

    #@2e
    move-result v7

    #@2f
    invoke-virtual {p1, v6, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@32
    .line 334
    :cond_32
    invoke-interface {v1}, Landroid/view/MenuItem;->hasSubMenu()Z

    #@35
    move-result v6

    #@36
    if-eqz v6, :cond_41

    #@38
    .line 335
    invoke-interface {v1}, Landroid/view/MenuItem;->getSubMenu()Landroid/view/SubMenu;

    #@3b
    move-result-object v3

    #@3c
    check-cast v3, Lcom/android/internal/view/menu/SubMenuBuilder;

    #@3e
    .line 336
    .local v3, subMenu:Lcom/android/internal/view/menu/SubMenuBuilder;
    invoke-virtual {v3, p1}, Lcom/android/internal/view/menu/SubMenuBuilder;->saveActionViewStates(Landroid/os/Bundle;)V

    #@41
    .line 322
    .end local v3           #subMenu:Lcom/android/internal/view/menu/SubMenuBuilder;
    :cond_41
    add-int/lit8 v0, v0, 0x1

    #@43
    goto :goto_6

    #@44
    .line 340
    .end local v1           #item:Landroid/view/MenuItem;
    .end local v4           #v:Landroid/view/View;
    :cond_44
    if-eqz v5, :cond_4d

    #@46
    .line 341
    invoke-virtual {p0}, Lcom/android/internal/view/menu/MenuBuilder;->getActionViewStatesKey()Ljava/lang/String;

    #@49
    move-result-object v6

    #@4a
    invoke-virtual {p1, v6, v5}, Landroid/os/Bundle;->putSparseParcelableArray(Ljava/lang/String;Landroid/util/SparseArray;)V

    #@4d
    .line 343
    :cond_4d
    return-void
.end method

.method public savePresenterStates(Landroid/os/Bundle;)V
    .registers 2
    .parameter "outState"

    #@0
    .prologue
    .line 311
    invoke-direct {p0, p1}, Lcom/android/internal/view/menu/MenuBuilder;->dispatchSaveInstanceState(Landroid/os/Bundle;)V

    #@3
    .line 312
    return-void
.end method

.method public setCallback(Lcom/android/internal/view/menu/MenuBuilder$Callback;)V
    .registers 2
    .parameter "cb"

    #@0
    .prologue
    .line 380
    iput-object p1, p0, Lcom/android/internal/view/menu/MenuBuilder;->mCallback:Lcom/android/internal/view/menu/MenuBuilder$Callback;

    #@2
    .line 381
    return-void
.end method

.method public setCurrentMenuInfo(Landroid/view/ContextMenu$ContextMenuInfo;)V
    .registers 2
    .parameter "menuInfo"

    #@0
    .prologue
    .line 1224
    iput-object p1, p0, Lcom/android/internal/view/menu/MenuBuilder;->mCurrentMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

    #@2
    .line 1225
    return-void
.end method

.method public setDefaultShowAsAction(I)Lcom/android/internal/view/menu/MenuBuilder;
    .registers 2
    .parameter "defaultShowAsAction"

    #@0
    .prologue
    .line 203
    iput p1, p0, Lcom/android/internal/view/menu/MenuBuilder;->mDefaultShowAsAction:I

    #@2
    .line 204
    return-object p0
.end method

.method setExclusiveItemChecked(Landroid/view/MenuItem;)V
    .registers 7
    .parameter "item"

    #@0
    .prologue
    .line 529
    invoke-interface {p1}, Landroid/view/MenuItem;->getGroupId()I

    #@3
    move-result v2

    #@4
    .line 531
    .local v2, group:I
    iget-object v4, p0, Lcom/android/internal/view/menu/MenuBuilder;->mItems:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@9
    move-result v0

    #@a
    .line 532
    .local v0, N:I
    const/4 v3, 0x0

    #@b
    .local v3, i:I
    :goto_b
    if-ge v3, v0, :cond_33

    #@d
    .line 533
    iget-object v4, p0, Lcom/android/internal/view/menu/MenuBuilder;->mItems:Ljava/util/ArrayList;

    #@f
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@12
    move-result-object v1

    #@13
    check-cast v1, Lcom/android/internal/view/menu/MenuItemImpl;

    #@15
    .line 534
    .local v1, curItem:Lcom/android/internal/view/menu/MenuItemImpl;
    invoke-virtual {v1}, Lcom/android/internal/view/menu/MenuItemImpl;->getGroupId()I

    #@18
    move-result v4

    #@19
    if-ne v4, v2, :cond_21

    #@1b
    .line 535
    invoke-virtual {v1}, Lcom/android/internal/view/menu/MenuItemImpl;->isExclusiveCheckable()Z

    #@1e
    move-result v4

    #@1f
    if-nez v4, :cond_24

    #@21
    .line 532
    :cond_21
    :goto_21
    add-int/lit8 v3, v3, 0x1

    #@23
    goto :goto_b

    #@24
    .line 536
    :cond_24
    invoke-virtual {v1}, Lcom/android/internal/view/menu/MenuItemImpl;->isCheckable()Z

    #@27
    move-result v4

    #@28
    if-eqz v4, :cond_21

    #@2a
    .line 539
    if-ne v1, p1, :cond_31

    #@2c
    const/4 v4, 0x1

    #@2d
    :goto_2d
    invoke-virtual {v1, v4}, Lcom/android/internal/view/menu/MenuItemImpl;->setCheckedInt(Z)V

    #@30
    goto :goto_21

    #@31
    :cond_31
    const/4 v4, 0x0

    #@32
    goto :goto_2d

    #@33
    .line 542
    .end local v1           #curItem:Lcom/android/internal/view/menu/MenuItemImpl;
    :cond_33
    return-void
.end method

.method public setGroupCheckable(IZZ)V
    .registers 8
    .parameter "group"
    .parameter "checkable"
    .parameter "exclusive"

    #@0
    .prologue
    .line 545
    iget-object v3, p0, Lcom/android/internal/view/menu/MenuBuilder;->mItems:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v0

    #@6
    .line 547
    .local v0, N:I
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    if-ge v1, v0, :cond_20

    #@9
    .line 548
    iget-object v3, p0, Lcom/android/internal/view/menu/MenuBuilder;->mItems:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v2

    #@f
    check-cast v2, Lcom/android/internal/view/menu/MenuItemImpl;

    #@11
    .line 549
    .local v2, item:Lcom/android/internal/view/menu/MenuItemImpl;
    invoke-virtual {v2}, Lcom/android/internal/view/menu/MenuItemImpl;->getGroupId()I

    #@14
    move-result v3

    #@15
    if-ne v3, p1, :cond_1d

    #@17
    .line 550
    invoke-virtual {v2, p3}, Lcom/android/internal/view/menu/MenuItemImpl;->setExclusiveCheckable(Z)V

    #@1a
    .line 551
    invoke-virtual {v2, p2}, Lcom/android/internal/view/menu/MenuItemImpl;->setCheckable(Z)Landroid/view/MenuItem;

    #@1d
    .line 547
    :cond_1d
    add-int/lit8 v1, v1, 0x1

    #@1f
    goto :goto_7

    #@20
    .line 554
    .end local v2           #item:Lcom/android/internal/view/menu/MenuItemImpl;
    :cond_20
    return-void
.end method

.method public setGroupEnabled(IZ)V
    .registers 7
    .parameter "group"
    .parameter "enabled"

    #@0
    .prologue
    .line 574
    iget-object v3, p0, Lcom/android/internal/view/menu/MenuBuilder;->mItems:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v0

    #@6
    .line 576
    .local v0, N:I
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    if-ge v1, v0, :cond_1d

    #@9
    .line 577
    iget-object v3, p0, Lcom/android/internal/view/menu/MenuBuilder;->mItems:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v2

    #@f
    check-cast v2, Lcom/android/internal/view/menu/MenuItemImpl;

    #@11
    .line 578
    .local v2, item:Lcom/android/internal/view/menu/MenuItemImpl;
    invoke-virtual {v2}, Lcom/android/internal/view/menu/MenuItemImpl;->getGroupId()I

    #@14
    move-result v3

    #@15
    if-ne v3, p1, :cond_1a

    #@17
    .line 579
    invoke-virtual {v2, p2}, Lcom/android/internal/view/menu/MenuItemImpl;->setEnabled(Z)Landroid/view/MenuItem;

    #@1a
    .line 576
    :cond_1a
    add-int/lit8 v1, v1, 0x1

    #@1c
    goto :goto_7

    #@1d
    .line 582
    .end local v2           #item:Lcom/android/internal/view/menu/MenuItemImpl;
    :cond_1d
    return-void
.end method

.method public setGroupVisible(IZ)V
    .registers 8
    .parameter "group"
    .parameter "visible"

    #@0
    .prologue
    .line 557
    iget-object v4, p0, Lcom/android/internal/view/menu/MenuBuilder;->mItems:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v0

    #@6
    .line 562
    .local v0, N:I
    const/4 v1, 0x0

    #@7
    .line 563
    .local v1, changedAtLeastOneItem:Z
    const/4 v2, 0x0

    #@8
    .local v2, i:I
    :goto_8
    if-ge v2, v0, :cond_22

    #@a
    .line 564
    iget-object v4, p0, Lcom/android/internal/view/menu/MenuBuilder;->mItems:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v3

    #@10
    check-cast v3, Lcom/android/internal/view/menu/MenuItemImpl;

    #@12
    .line 565
    .local v3, item:Lcom/android/internal/view/menu/MenuItemImpl;
    invoke-virtual {v3}, Lcom/android/internal/view/menu/MenuItemImpl;->getGroupId()I

    #@15
    move-result v4

    #@16
    if-ne v4, p1, :cond_1f

    #@18
    .line 566
    invoke-virtual {v3, p2}, Lcom/android/internal/view/menu/MenuItemImpl;->setVisibleInt(Z)Z

    #@1b
    move-result v4

    #@1c
    if-eqz v4, :cond_1f

    #@1e
    const/4 v1, 0x1

    #@1f
    .line 563
    :cond_1f
    add-int/lit8 v2, v2, 0x1

    #@21
    goto :goto_8

    #@22
    .line 570
    .end local v3           #item:Lcom/android/internal/view/menu/MenuItemImpl;
    :cond_22
    if-eqz v1, :cond_28

    #@24
    const/4 v4, 0x1

    #@25
    invoke-virtual {p0, v4}, Lcom/android/internal/view/menu/MenuBuilder;->onItemsChanged(Z)V

    #@28
    .line 571
    :cond_28
    return-void
.end method

.method protected setHeaderIconInt(I)Lcom/android/internal/view/menu/MenuBuilder;
    .registers 8
    .parameter "iconRes"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1180
    const/4 v1, 0x0

    #@2
    move-object v0, p0

    #@3
    move v3, p1

    #@4
    move-object v4, v2

    #@5
    move-object v5, v2

    #@6
    invoke-direct/range {v0 .. v5}, Lcom/android/internal/view/menu/MenuBuilder;->setHeaderInternal(ILjava/lang/CharSequence;ILandroid/graphics/drawable/Drawable;Landroid/view/View;)V

    #@9
    .line 1181
    return-object p0
.end method

.method protected setHeaderIconInt(Landroid/graphics/drawable/Drawable;)Lcom/android/internal/view/menu/MenuBuilder;
    .registers 8
    .parameter "icon"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 1168
    move-object v0, p0

    #@3
    move v3, v1

    #@4
    move-object v4, p1

    #@5
    move-object v5, v2

    #@6
    invoke-direct/range {v0 .. v5}, Lcom/android/internal/view/menu/MenuBuilder;->setHeaderInternal(ILjava/lang/CharSequence;ILandroid/graphics/drawable/Drawable;Landroid/view/View;)V

    #@9
    .line 1169
    return-object p0
.end method

.method protected setHeaderTitleInt(I)Lcom/android/internal/view/menu/MenuBuilder;
    .registers 8
    .parameter "titleRes"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1156
    const/4 v3, 0x0

    #@2
    move-object v0, p0

    #@3
    move v1, p1

    #@4
    move-object v4, v2

    #@5
    move-object v5, v2

    #@6
    invoke-direct/range {v0 .. v5}, Lcom/android/internal/view/menu/MenuBuilder;->setHeaderInternal(ILjava/lang/CharSequence;ILandroid/graphics/drawable/Drawable;Landroid/view/View;)V

    #@9
    .line 1157
    return-object p0
.end method

.method protected setHeaderTitleInt(Ljava/lang/CharSequence;)Lcom/android/internal/view/menu/MenuBuilder;
    .registers 8
    .parameter "title"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 1144
    move-object v0, p0

    #@3
    move-object v2, p1

    #@4
    move v3, v1

    #@5
    move-object v5, v4

    #@6
    invoke-direct/range {v0 .. v5}, Lcom/android/internal/view/menu/MenuBuilder;->setHeaderInternal(ILjava/lang/CharSequence;ILandroid/graphics/drawable/Drawable;Landroid/view/View;)V

    #@9
    .line 1145
    return-object p0
.end method

.method protected setHeaderViewInt(Landroid/view/View;)Lcom/android/internal/view/menu/MenuBuilder;
    .registers 8
    .parameter "view"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 1192
    move-object v0, p0

    #@3
    move v3, v1

    #@4
    move-object v4, v2

    #@5
    move-object v5, p1

    #@6
    invoke-direct/range {v0 .. v5}, Lcom/android/internal/view/menu/MenuBuilder;->setHeaderInternal(ILjava/lang/CharSequence;ILandroid/graphics/drawable/Drawable;Landroid/view/View;)V

    #@9
    .line 1193
    return-object p0
.end method

.method setOptionalIconsVisible(Z)V
    .registers 2
    .parameter "visible"

    #@0
    .prologue
    .line 1228
    iput-boolean p1, p0, Lcom/android/internal/view/menu/MenuBuilder;->mOptionalIconsVisible:Z

    #@2
    .line 1229
    return-void
.end method

.method public setQwertyMode(Z)V
    .registers 3
    .parameter "isQwerty"

    #@0
    .prologue
    .line 664
    iput-boolean p1, p0, Lcom/android/internal/view/menu/MenuBuilder;->mQwertyMode:Z

    #@2
    .line 666
    const/4 v0, 0x0

    #@3
    invoke-virtual {p0, v0}, Lcom/android/internal/view/menu/MenuBuilder;->onItemsChanged(Z)V

    #@6
    .line 667
    return-void
.end method

.method public setShortcutsVisible(Z)V
    .registers 3
    .parameter "shortcutsVisible"

    #@0
    .prologue
    .line 706
    iget-boolean v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mShortcutsVisible:Z

    #@2
    if-ne v0, p1, :cond_5

    #@4
    .line 710
    :goto_4
    return-void

    #@5
    .line 708
    :cond_5
    invoke-direct {p0, p1}, Lcom/android/internal/view/menu/MenuBuilder;->setShortcutsVisibleInner(Z)V

    #@8
    .line 709
    const/4 v0, 0x0

    #@9
    invoke-virtual {p0, v0}, Lcom/android/internal/view/menu/MenuBuilder;->onItemsChanged(Z)V

    #@c
    goto :goto_4
.end method

.method public size()I
    .registers 2

    #@0
    .prologue
    .line 651
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mItems:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public startDispatchingItemsChanged()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 965
    iput-boolean v1, p0, Lcom/android/internal/view/menu/MenuBuilder;->mPreventDispatchingItemsChanged:Z

    #@3
    .line 967
    iget-boolean v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mItemsChangedWhileDispatchPrevented:Z

    #@5
    if-eqz v0, :cond_d

    #@7
    .line 968
    iput-boolean v1, p0, Lcom/android/internal/view/menu/MenuBuilder;->mItemsChangedWhileDispatchPrevented:Z

    #@9
    .line 969
    const/4 v0, 0x1

    #@a
    invoke-virtual {p0, v0}, Lcom/android/internal/view/menu/MenuBuilder;->onItemsChanged(Z)V

    #@d
    .line 971
    :cond_d
    return-void
.end method

.method public stopDispatchingItemsChanged()V
    .registers 2

    #@0
    .prologue
    .line 958
    iget-boolean v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mPreventDispatchingItemsChanged:Z

    #@2
    if-nez v0, :cond_a

    #@4
    .line 959
    const/4 v0, 0x1

    #@5
    iput-boolean v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mPreventDispatchingItemsChanged:Z

    #@7
    .line 960
    const/4 v0, 0x0

    #@8
    iput-boolean v0, p0, Lcom/android/internal/view/menu/MenuBuilder;->mItemsChangedWhileDispatchPrevented:Z

    #@a
    .line 962
    :cond_a
    return-void
.end method
