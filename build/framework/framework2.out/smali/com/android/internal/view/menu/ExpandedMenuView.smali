.class public final Lcom/android/internal/view/menu/ExpandedMenuView;
.super Landroid/widget/ListView;
.source "ExpandedMenuView.java"

# interfaces
.implements Lcom/android/internal/view/menu/MenuBuilder$ItemInvoker;
.implements Lcom/android/internal/view/menu/MenuView;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field private mAnimations:I

.field final mMaxHeightMajor:Landroid/util/TypedValue;

.field final mMaxHeightMinor:Landroid/util/TypedValue;

.field private mMenu:Lcom/android/internal/view/menu/MenuBuilder;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 6
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 51
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 90
    new-instance v1, Landroid/util/TypedValue;

    #@6
    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    #@9
    iput-object v1, p0, Lcom/android/internal/view/menu/ExpandedMenuView;->mMaxHeightMajor:Landroid/util/TypedValue;

    #@b
    .line 91
    new-instance v1, Landroid/util/TypedValue;

    #@d
    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    #@10
    iput-object v1, p0, Lcom/android/internal/view/menu/ExpandedMenuView;->mMaxHeightMinor:Landroid/util/TypedValue;

    #@12
    .line 53
    sget-object v1, Lcom/android/internal/R$styleable;->MenuView:[I

    #@14
    invoke-virtual {p1, p2, v1, v2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@17
    move-result-object v0

    #@18
    .line 54
    .local v0, a:Landroid/content/res/TypedArray;
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@1b
    move-result v1

    #@1c
    iput v1, p0, Lcom/android/internal/view/menu/ExpandedMenuView;->mAnimations:I

    #@1e
    .line 55
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@21
    .line 57
    sget-object v1, Lcom/android/internal/R$styleable;->Window:[I

    #@23
    invoke-virtual {p1, v1}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    #@26
    move-result-object v0

    #@27
    .line 58
    const/16 v1, 0x17

    #@29
    iget-object v2, p0, Lcom/android/internal/view/menu/ExpandedMenuView;->mMaxHeightMajor:Landroid/util/TypedValue;

    #@2b
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    #@2e
    .line 59
    const/16 v1, 0x18

    #@30
    iget-object v2, p0, Lcom/android/internal/view/menu/ExpandedMenuView;->mMaxHeightMinor:Landroid/util/TypedValue;

    #@32
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    #@35
    .line 60
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@38
    .line 63
    invoke-virtual {p0, p0}, Lcom/android/internal/view/menu/ExpandedMenuView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    #@3b
    .line 64
    return-void
.end method


# virtual methods
.method public getWindowAnimations()I
    .registers 2

    #@0
    .prologue
    .line 87
    iget v0, p0, Lcom/android/internal/view/menu/ExpandedMenuView;->mAnimations:I

    #@2
    return v0
.end method

.method public initialize(Lcom/android/internal/view/menu/MenuBuilder;)V
    .registers 2
    .parameter "menu"

    #@0
    .prologue
    .line 67
    iput-object p1, p0, Lcom/android/internal/view/menu/ExpandedMenuView;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@2
    .line 68
    return-void
.end method

.method public invokeItem(Lcom/android/internal/view/menu/MenuItemImpl;)Z
    .registers 4
    .parameter "item"

    #@0
    .prologue
    .line 79
    iget-object v0, p0, Lcom/android/internal/view/menu/ExpandedMenuView;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-virtual {v0, p1, v1}, Lcom/android/internal/view/menu/MenuBuilder;->performItemAction(Landroid/view/MenuItem;I)Z

    #@6
    move-result v0

    #@7
    return v0
.end method

.method protected onDetachedFromWindow()V
    .registers 2

    #@0
    .prologue
    .line 72
    invoke-super {p0}, Landroid/widget/ListView;->onDetachedFromWindow()V

    #@3
    .line 75
    const/4 v0, 0x0

    #@4
    invoke-virtual {p0, v0}, Lcom/android/internal/view/menu/ExpandedMenuView;->setChildrenDrawingCacheEnabled(Z)V

    #@7
    .line 76
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 7
    .parameter "parent"
    .parameter "v"
    .parameter "position"
    .parameter "id"

    #@0
    .prologue
    .line 83
    invoke-virtual {p0}, Lcom/android/internal/view/menu/ExpandedMenuView;->getAdapter()Landroid/widget/ListAdapter;

    #@3
    move-result-object v0

    #@4
    invoke-interface {v0, p3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    #@7
    move-result-object v0

    #@8
    check-cast v0, Lcom/android/internal/view/menu/MenuItemImpl;

    #@a
    invoke-virtual {p0, v0}, Lcom/android/internal/view/menu/ExpandedMenuView;->invokeItem(Lcom/android/internal/view/menu/MenuItemImpl;)Z

    #@d
    .line 84
    return-void
.end method

.method protected onMeasure(II)V
    .registers 13
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    const/high16 v9, 0x4000

    #@2
    .line 95
    invoke-virtual {p0}, Lcom/android/internal/view/menu/ExpandedMenuView;->getContext()Landroid/content/Context;

    #@5
    move-result-object v7

    #@6
    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@9
    move-result-object v7

    #@a
    invoke-virtual {v7}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@d
    move-result-object v5

    #@e
    .line 96
    .local v5, metrics:Landroid/util/DisplayMetrics;
    iget v7, v5, Landroid/util/DisplayMetrics;->widthPixels:I

    #@10
    iget v8, v5, Landroid/util/DisplayMetrics;->heightPixels:I

    #@12
    if-ge v7, v8, :cond_42

    #@14
    const/4 v2, 0x1

    #@15
    .line 98
    .local v2, isPortrait:Z
    :goto_15
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@18
    move-result v1

    #@19
    .line 100
    .local v1, heightMode:I
    invoke-super {p0, p1, p2}, Landroid/widget/ListView;->onMeasure(II)V

    #@1c
    .line 102
    invoke-virtual {p0}, Lcom/android/internal/view/menu/ExpandedMenuView;->getMeasuredHeight()I

    #@1f
    move-result v0

    #@20
    .line 103
    .local v0, height:I
    const/4 v4, 0x0

    #@21
    .line 105
    .local v4, measure:Z
    if-eqz v2, :cond_44

    #@23
    iget-object v6, p0, Lcom/android/internal/view/menu/ExpandedMenuView;->mMaxHeightMinor:Landroid/util/TypedValue;

    #@25
    .line 107
    .local v6, tv:Landroid/util/TypedValue;
    :goto_25
    if-eq v1, v9, :cond_3c

    #@27
    iget v7, v6, Landroid/util/TypedValue;->type:I

    #@29
    if-eqz v7, :cond_3c

    #@2b
    .line 109
    iget v7, v6, Landroid/util/TypedValue;->type:I

    #@2d
    const/4 v8, 0x5

    #@2e
    if-ne v7, v8, :cond_47

    #@30
    .line 110
    invoke-virtual {v6, v5}, Landroid/util/TypedValue;->getDimension(Landroid/util/DisplayMetrics;)F

    #@33
    move-result v7

    #@34
    float-to-int v3, v7

    #@35
    .line 116
    .local v3, max:I
    :goto_35
    if-le v0, v3, :cond_3c

    #@37
    .line 117
    invoke-static {v3, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@3a
    move-result p2

    #@3b
    .line 118
    const/4 v4, 0x1

    #@3c
    .line 122
    .end local v3           #max:I
    :cond_3c
    if-eqz v4, :cond_41

    #@3e
    .line 123
    invoke-super {p0, p1, p2}, Landroid/widget/ListView;->onMeasure(II)V

    #@41
    .line 125
    :cond_41
    return-void

    #@42
    .line 96
    .end local v0           #height:I
    .end local v1           #heightMode:I
    .end local v2           #isPortrait:Z
    .end local v4           #measure:Z
    .end local v6           #tv:Landroid/util/TypedValue;
    :cond_42
    const/4 v2, 0x0

    #@43
    goto :goto_15

    #@44
    .line 105
    .restart local v0       #height:I
    .restart local v1       #heightMode:I
    .restart local v2       #isPortrait:Z
    .restart local v4       #measure:Z
    :cond_44
    iget-object v6, p0, Lcom/android/internal/view/menu/ExpandedMenuView;->mMaxHeightMajor:Landroid/util/TypedValue;

    #@46
    goto :goto_25

    #@47
    .line 111
    .restart local v6       #tv:Landroid/util/TypedValue;
    :cond_47
    iget v7, v6, Landroid/util/TypedValue;->type:I

    #@49
    const/4 v8, 0x6

    #@4a
    if-ne v7, v8, :cond_58

    #@4c
    .line 112
    iget v7, v5, Landroid/util/DisplayMetrics;->heightPixels:I

    #@4e
    int-to-float v7, v7

    #@4f
    iget v8, v5, Landroid/util/DisplayMetrics;->heightPixels:I

    #@51
    int-to-float v8, v8

    #@52
    invoke-virtual {v6, v7, v8}, Landroid/util/TypedValue;->getFraction(FF)F

    #@55
    move-result v7

    #@56
    float-to-int v3, v7

    #@57
    .restart local v3       #max:I
    goto :goto_35

    #@58
    .line 114
    .end local v3           #max:I
    :cond_58
    iget v3, v5, Landroid/util/DisplayMetrics;->heightPixels:I

    #@5a
    .restart local v3       #max:I
    goto :goto_35
.end method
