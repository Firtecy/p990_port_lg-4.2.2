.class public abstract Lcom/android/internal/view/BaseSurfaceHolder;
.super Ljava/lang/Object;
.source "BaseSurfaceHolder.java"

# interfaces
.implements Landroid/view/SurfaceHolder;


# static fields
.field static final DEBUG:Z = false

.field private static final TAG:Ljava/lang/String; = "BaseSurfaceHolder"


# instance fields
.field public final mCallbacks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/SurfaceHolder$Callback;",
            ">;"
        }
    .end annotation
.end field

.field mGottenCallbacks:[Landroid/view/SurfaceHolder$Callback;

.field mHaveGottenCallbacks:Z

.field mLastLockTime:J

.field protected mRequestedFormat:I

.field mRequestedHeight:I

.field mRequestedType:I

.field mRequestedWidth:I

.field public mSurface:Landroid/view/Surface;

.field final mSurfaceFrame:Landroid/graphics/Rect;

.field public final mSurfaceLock:Ljava/util/concurrent/locks/ReentrantLock;

.field mTmpDirty:Landroid/graphics/Rect;

.field mType:I


# direct methods
.method public constructor <init>()V
    .registers 4

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    .line 30
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 34
    new-instance v0, Ljava/util/ArrayList;

    #@6
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@9
    iput-object v0, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mCallbacks:Ljava/util/ArrayList;

    #@b
    .line 39
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    #@d
    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    #@10
    iput-object v0, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mSurfaceLock:Ljava/util/concurrent/locks/ReentrantLock;

    #@12
    .line 40
    new-instance v0, Landroid/view/Surface;

    #@14
    invoke-direct {v0}, Landroid/view/Surface;-><init>()V

    #@17
    iput-object v0, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mSurface:Landroid/view/Surface;

    #@19
    .line 42
    iput v2, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mRequestedWidth:I

    #@1b
    .line 43
    iput v2, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mRequestedHeight:I

    #@1d
    .line 45
    iput v2, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mRequestedFormat:I

    #@1f
    .line 46
    iput v2, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mRequestedType:I

    #@21
    .line 48
    const-wide/16 v0, 0x0

    #@23
    iput-wide v0, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mLastLockTime:J

    #@25
    .line 50
    iput v2, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mType:I

    #@27
    .line 51
    new-instance v0, Landroid/graphics/Rect;

    #@29
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@2c
    iput-object v0, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mSurfaceFrame:Landroid/graphics/Rect;

    #@2e
    return-void
.end method

.method private final internalLockCanvas(Landroid/graphics/Rect;)Landroid/graphics/Canvas;
    .registers 12
    .parameter "dirty"

    #@0
    .prologue
    .line 165
    iget v6, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mType:I

    #@2
    const/4 v7, 0x3

    #@3
    if-ne v6, v7, :cond_d

    #@5
    .line 166
    new-instance v6, Landroid/view/SurfaceHolder$BadSurfaceTypeException;

    #@7
    const-string v7, "Surface type is SURFACE_TYPE_PUSH_BUFFERS"

    #@9
    invoke-direct {v6, v7}, Landroid/view/SurfaceHolder$BadSurfaceTypeException;-><init>(Ljava/lang/String;)V

    #@c
    throw v6

    #@d
    .line 169
    :cond_d
    iget-object v6, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mSurfaceLock:Ljava/util/concurrent/locks/ReentrantLock;

    #@f
    invoke-virtual {v6}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    #@12
    .line 173
    const/4 v0, 0x0

    #@13
    .line 174
    .local v0, c:Landroid/graphics/Canvas;
    invoke-virtual {p0}, Lcom/android/internal/view/BaseSurfaceHolder;->onAllowLockCanvas()Z

    #@16
    move-result v6

    #@17
    if-eqz v6, :cond_35

    #@19
    .line 175
    if-nez p1, :cond_2f

    #@1b
    .line 176
    iget-object v6, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mTmpDirty:Landroid/graphics/Rect;

    #@1d
    if-nez v6, :cond_26

    #@1f
    .line 177
    new-instance v6, Landroid/graphics/Rect;

    #@21
    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    #@24
    iput-object v6, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mTmpDirty:Landroid/graphics/Rect;

    #@26
    .line 179
    :cond_26
    iget-object v6, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mTmpDirty:Landroid/graphics/Rect;

    #@28
    iget-object v7, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mSurfaceFrame:Landroid/graphics/Rect;

    #@2a
    invoke-virtual {v6, v7}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@2d
    .line 180
    iget-object p1, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mTmpDirty:Landroid/graphics/Rect;

    #@2f
    .line 184
    :cond_2f
    :try_start_2f
    iget-object v6, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mSurface:Landroid/view/Surface;

    #@31
    invoke-virtual {v6, p1}, Landroid/view/Surface;->lockCanvas(Landroid/graphics/Rect;)Landroid/graphics/Canvas;
    :try_end_34
    .catch Ljava/lang/Exception; {:try_start_2f .. :try_end_34} :catch_3e

    #@34
    move-result-object v0

    #@35
    .line 191
    :cond_35
    :goto_35
    if-eqz v0, :cond_47

    #@37
    .line 192
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@3a
    move-result-wide v6

    #@3b
    iput-wide v6, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mLastLockTime:J

    #@3d
    .line 211
    .end local v0           #c:Landroid/graphics/Canvas;
    :goto_3d
    return-object v0

    #@3e
    .line 185
    .restart local v0       #c:Landroid/graphics/Canvas;
    :catch_3e
    move-exception v1

    #@3f
    .line 186
    .local v1, e:Ljava/lang/Exception;
    const-string v6, "BaseSurfaceHolder"

    #@41
    const-string v7, "Exception locking surface"

    #@43
    invoke-static {v6, v7, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@46
    goto :goto_35

    #@47
    .line 199
    .end local v1           #e:Ljava/lang/Exception;
    :cond_47
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@4a
    move-result-wide v4

    #@4b
    .line 200
    .local v4, now:J
    iget-wide v6, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mLastLockTime:J

    #@4d
    const-wide/16 v8, 0x64

    #@4f
    add-long v2, v6, v8

    #@51
    .line 201
    .local v2, nextTime:J
    cmp-long v6, v2, v4

    #@53
    if-lez v6, :cond_5e

    #@55
    .line 203
    sub-long v6, v2, v4

    #@57
    :try_start_57
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_5a
    .catch Ljava/lang/InterruptedException; {:try_start_57 .. :try_end_5a} :catch_67

    #@5a
    .line 206
    :goto_5a
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@5d
    move-result-wide v4

    #@5e
    .line 208
    :cond_5e
    iput-wide v4, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mLastLockTime:J

    #@60
    .line 209
    iget-object v6, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mSurfaceLock:Ljava/util/concurrent/locks/ReentrantLock;

    #@62
    invoke-virtual {v6}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    #@65
    .line 211
    const/4 v0, 0x0

    #@66
    goto :goto_3d

    #@67
    .line 204
    :catch_67
    move-exception v6

    #@68
    goto :goto_5a
.end method


# virtual methods
.method public addCallback(Landroid/view/SurfaceHolder$Callback;)V
    .registers 4
    .parameter "callback"

    #@0
    .prologue
    .line 75
    iget-object v1, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mCallbacks:Ljava/util/ArrayList;

    #@2
    monitor-enter v1

    #@3
    .line 78
    :try_start_3
    iget-object v0, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mCallbacks:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@8
    move-result v0

    #@9
    if-nez v0, :cond_10

    #@b
    .line 79
    iget-object v0, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mCallbacks:Ljava/util/ArrayList;

    #@d
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@10
    .line 81
    :cond_10
    monitor-exit v1

    #@11
    .line 82
    return-void

    #@12
    .line 81
    :catchall_12
    move-exception v0

    #@13
    monitor-exit v1
    :try_end_14
    .catchall {:try_start_3 .. :try_end_14} :catchall_12

    #@14
    throw v0
.end method

.method public getCallbacks()[Landroid/view/SurfaceHolder$Callback;
    .registers 5

    #@0
    .prologue
    .line 91
    iget-boolean v1, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mHaveGottenCallbacks:Z

    #@2
    if-eqz v1, :cond_7

    #@4
    .line 92
    iget-object v1, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mGottenCallbacks:[Landroid/view/SurfaceHolder$Callback;

    #@6
    .line 108
    :goto_6
    return-object v1

    #@7
    .line 95
    :cond_7
    iget-object v2, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mCallbacks:Ljava/util/ArrayList;

    #@9
    monitor-enter v2

    #@a
    .line 96
    :try_start_a
    iget-object v1, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mCallbacks:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@f
    move-result v0

    #@10
    .line 97
    .local v0, N:I
    if-lez v0, :cond_2d

    #@12
    .line 98
    iget-object v1, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mGottenCallbacks:[Landroid/view/SurfaceHolder$Callback;

    #@14
    if-eqz v1, :cond_1b

    #@16
    iget-object v1, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mGottenCallbacks:[Landroid/view/SurfaceHolder$Callback;

    #@18
    array-length v1, v1

    #@19
    if-eq v1, v0, :cond_1f

    #@1b
    .line 99
    :cond_1b
    new-array v1, v0, [Landroid/view/SurfaceHolder$Callback;

    #@1d
    iput-object v1, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mGottenCallbacks:[Landroid/view/SurfaceHolder$Callback;

    #@1f
    .line 101
    :cond_1f
    iget-object v1, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mCallbacks:Ljava/util/ArrayList;

    #@21
    iget-object v3, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mGottenCallbacks:[Landroid/view/SurfaceHolder$Callback;

    #@23
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@26
    .line 105
    :goto_26
    const/4 v1, 0x1

    #@27
    iput-boolean v1, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mHaveGottenCallbacks:Z

    #@29
    .line 106
    monitor-exit v2
    :try_end_2a
    .catchall {:try_start_a .. :try_end_2a} :catchall_31

    #@2a
    .line 108
    iget-object v1, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mGottenCallbacks:[Landroid/view/SurfaceHolder$Callback;

    #@2c
    goto :goto_6

    #@2d
    .line 103
    :cond_2d
    const/4 v1, 0x0

    #@2e
    :try_start_2e
    iput-object v1, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mGottenCallbacks:[Landroid/view/SurfaceHolder$Callback;

    #@30
    goto :goto_26

    #@31
    .line 106
    .end local v0           #N:I
    :catchall_31
    move-exception v1

    #@32
    monitor-exit v2
    :try_end_33
    .catchall {:try_start_2e .. :try_end_33} :catchall_31

    #@33
    throw v1
.end method

.method public getRequestedFormat()I
    .registers 2

    #@0
    .prologue
    .line 67
    iget v0, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mRequestedFormat:I

    #@2
    return v0
.end method

.method public getRequestedHeight()I
    .registers 2

    #@0
    .prologue
    .line 63
    iget v0, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mRequestedHeight:I

    #@2
    return v0
.end method

.method public getRequestedType()I
    .registers 2

    #@0
    .prologue
    .line 71
    iget v0, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mRequestedType:I

    #@2
    return v0
.end method

.method public getRequestedWidth()I
    .registers 2

    #@0
    .prologue
    .line 59
    iget v0, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mRequestedWidth:I

    #@2
    return v0
.end method

.method public getSurface()Landroid/view/Surface;
    .registers 2

    #@0
    .prologue
    .line 220
    iget-object v0, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mSurface:Landroid/view/Surface;

    #@2
    return-object v0
.end method

.method public getSurfaceFrame()Landroid/graphics/Rect;
    .registers 2

    #@0
    .prologue
    .line 224
    iget-object v0, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mSurfaceFrame:Landroid/graphics/Rect;

    #@2
    return-object v0
.end method

.method public lockCanvas()Landroid/graphics/Canvas;
    .registers 2

    #@0
    .prologue
    .line 157
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, v0}, Lcom/android/internal/view/BaseSurfaceHolder;->internalLockCanvas(Landroid/graphics/Rect;)Landroid/graphics/Canvas;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public lockCanvas(Landroid/graphics/Rect;)Landroid/graphics/Canvas;
    .registers 3
    .parameter "dirty"

    #@0
    .prologue
    .line 161
    invoke-direct {p0, p1}, Lcom/android/internal/view/BaseSurfaceHolder;->internalLockCanvas(Landroid/graphics/Rect;)Landroid/graphics/Canvas;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public abstract onAllowLockCanvas()Z
.end method

.method public abstract onRelayoutContainer()V
.end method

.method public abstract onUpdateSurface()V
.end method

.method public removeCallback(Landroid/view/SurfaceHolder$Callback;)V
    .registers 4
    .parameter "callback"

    #@0
    .prologue
    .line 85
    iget-object v1, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mCallbacks:Ljava/util/ArrayList;

    #@2
    monitor-enter v1

    #@3
    .line 86
    :try_start_3
    iget-object v0, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mCallbacks:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@8
    .line 87
    monitor-exit v1

    #@9
    .line 88
    return-void

    #@a
    .line 87
    :catchall_a
    move-exception v0

    #@b
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method

.method public setFixedSize(II)V
    .registers 4
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 116
    iget v0, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mRequestedWidth:I

    #@2
    if-ne v0, p1, :cond_8

    #@4
    iget v0, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mRequestedHeight:I

    #@6
    if-eq v0, p2, :cond_f

    #@8
    .line 117
    :cond_8
    iput p1, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mRequestedWidth:I

    #@a
    .line 118
    iput p2, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mRequestedHeight:I

    #@c
    .line 119
    invoke-virtual {p0}, Lcom/android/internal/view/BaseSurfaceHolder;->onRelayoutContainer()V

    #@f
    .line 121
    :cond_f
    return-void
.end method

.method public setFormat(I)V
    .registers 3
    .parameter "format"

    #@0
    .prologue
    .line 131
    iget v0, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mRequestedFormat:I

    #@2
    if-eq v0, p1, :cond_9

    #@4
    .line 132
    iput p1, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mRequestedFormat:I

    #@6
    .line 133
    invoke-virtual {p0}, Lcom/android/internal/view/BaseSurfaceHolder;->onUpdateSurface()V

    #@9
    .line 135
    :cond_9
    return-void
.end method

.method public setSizeFromLayout()V
    .registers 3

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 124
    iget v0, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mRequestedWidth:I

    #@3
    if-ne v0, v1, :cond_9

    #@5
    iget v0, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mRequestedHeight:I

    #@7
    if-eq v0, v1, :cond_10

    #@9
    .line 125
    :cond_9
    iput v1, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mRequestedHeight:I

    #@b
    iput v1, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mRequestedWidth:I

    #@d
    .line 126
    invoke-virtual {p0}, Lcom/android/internal/view/BaseSurfaceHolder;->onRelayoutContainer()V

    #@10
    .line 128
    :cond_10
    return-void
.end method

.method public setSurfaceFrameSize(II)V
    .registers 5
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 228
    iget-object v0, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mSurfaceFrame:Landroid/graphics/Rect;

    #@3
    iput v1, v0, Landroid/graphics/Rect;->top:I

    #@5
    .line 229
    iget-object v0, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mSurfaceFrame:Landroid/graphics/Rect;

    #@7
    iput v1, v0, Landroid/graphics/Rect;->left:I

    #@9
    .line 230
    iget-object v0, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mSurfaceFrame:Landroid/graphics/Rect;

    #@b
    iput p1, v0, Landroid/graphics/Rect;->right:I

    #@d
    .line 231
    iget-object v0, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mSurfaceFrame:Landroid/graphics/Rect;

    #@f
    iput p2, v0, Landroid/graphics/Rect;->bottom:I

    #@11
    .line 232
    return-void
.end method

.method public setType(I)V
    .registers 3
    .parameter "type"

    #@0
    .prologue
    .line 138
    packed-switch p1, :pswitch_data_14

    #@3
    .line 145
    :goto_3
    packed-switch p1, :pswitch_data_1c

    #@6
    .line 154
    :cond_6
    :goto_6
    :pswitch_6
    return-void

    #@7
    .line 142
    :pswitch_7
    const/4 p1, 0x0

    #@8
    goto :goto_3

    #@9
    .line 148
    :pswitch_9
    iget v0, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mRequestedType:I

    #@b
    if-eq v0, p1, :cond_6

    #@d
    .line 149
    iput p1, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mRequestedType:I

    #@f
    .line 150
    invoke-virtual {p0}, Lcom/android/internal/view/BaseSurfaceHolder;->onUpdateSurface()V

    #@12
    goto :goto_6

    #@13
    .line 138
    nop

    #@14
    :pswitch_data_14
    .packed-switch 0x1
        :pswitch_7
        :pswitch_7
    .end packed-switch

    #@1c
    .line 145
    :pswitch_data_1c
    .packed-switch 0x0
        :pswitch_9
        :pswitch_6
        :pswitch_6
        :pswitch_9
    .end packed-switch
.end method

.method public ungetCallbacks()V
    .registers 2

    #@0
    .prologue
    .line 112
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mHaveGottenCallbacks:Z

    #@3
    .line 113
    return-void
.end method

.method public unlockCanvasAndPost(Landroid/graphics/Canvas;)V
    .registers 3
    .parameter "canvas"

    #@0
    .prologue
    .line 215
    iget-object v0, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mSurface:Landroid/view/Surface;

    #@2
    invoke-virtual {v0, p1}, Landroid/view/Surface;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    #@5
    .line 216
    iget-object v0, p0, Lcom/android/internal/view/BaseSurfaceHolder;->mSurfaceLock:Ljava/util/concurrent/locks/ReentrantLock;

    #@7
    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    #@a
    .line 217
    return-void
.end method
