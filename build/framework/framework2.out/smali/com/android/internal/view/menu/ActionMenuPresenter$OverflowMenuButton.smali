.class Lcom/android/internal/view/menu/ActionMenuPresenter$OverflowMenuButton;
.super Landroid/widget/ImageButton;
.source "ActionMenuPresenter.java"

# interfaces
.implements Lcom/android/internal/view/menu/ActionMenuView$ActionMenuChildView;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/view/menu/ActionMenuPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OverflowMenuButton"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/view/menu/ActionMenuPresenter;


# direct methods
.method public constructor <init>(Lcom/android/internal/view/menu/ActionMenuPresenter;Landroid/content/Context;)V
    .registers 6
    .parameter
    .parameter "context"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 555
    iput-object p1, p0, Lcom/android/internal/view/menu/ActionMenuPresenter$OverflowMenuButton;->this$0:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@3
    .line 556
    const/4 v0, 0x0

    #@4
    const v1, 0x10102f6

    #@7
    invoke-direct {p0, p2, v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@a
    .line 558
    invoke-virtual {p0, v2}, Lcom/android/internal/view/menu/ActionMenuPresenter$OverflowMenuButton;->setClickable(Z)V

    #@d
    .line 559
    invoke-virtual {p0, v2}, Lcom/android/internal/view/menu/ActionMenuPresenter$OverflowMenuButton;->setFocusable(Z)V

    #@10
    .line 560
    const/4 v0, 0x0

    #@11
    invoke-virtual {p0, v0}, Lcom/android/internal/view/menu/ActionMenuPresenter$OverflowMenuButton;->setVisibility(I)V

    #@14
    .line 561
    invoke-virtual {p0, v2}, Lcom/android/internal/view/menu/ActionMenuPresenter$OverflowMenuButton;->setEnabled(Z)V

    #@17
    .line 562
    return-void
.end method


# virtual methods
.method public needsDividerAfter()Z
    .registers 2

    #@0
    .prologue
    .line 580
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public needsDividerBefore()Z
    .registers 2

    #@0
    .prologue
    .line 576
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method protected onMeasure(II)V
    .registers 5
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    .line 585
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@3
    move-result v0

    #@4
    const/high16 v1, -0x8000

    #@6
    if-ne v0, v1, :cond_12

    #@8
    .line 587
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@b
    move-result v0

    #@c
    const/high16 v1, 0x4000

    #@e
    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@11
    move-result p2

    #@12
    .line 590
    :cond_12
    invoke-super {p0, p1, p2}, Landroid/widget/ImageButton;->onMeasure(II)V

    #@15
    .line 591
    return-void
.end method

.method public performClick()Z
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 566
    invoke-super {p0}, Landroid/widget/ImageButton;->performClick()Z

    #@4
    move-result v0

    #@5
    if-eqz v0, :cond_8

    #@7
    .line 572
    :goto_7
    return v1

    #@8
    .line 570
    :cond_8
    const/4 v0, 0x0

    #@9
    invoke-virtual {p0, v0}, Lcom/android/internal/view/menu/ActionMenuPresenter$OverflowMenuButton;->playSoundEffect(I)V

    #@c
    .line 571
    iget-object v0, p0, Lcom/android/internal/view/menu/ActionMenuPresenter$OverflowMenuButton;->this$0:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@e
    invoke-virtual {v0}, Lcom/android/internal/view/menu/ActionMenuPresenter;->showOverflowMenu()Z

    #@11
    goto :goto_7
.end method
