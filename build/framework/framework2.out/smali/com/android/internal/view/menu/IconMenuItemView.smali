.class public final Lcom/android/internal/view/menu/IconMenuItemView;
.super Landroid/widget/TextView;
.source "IconMenuItemView.java"

# interfaces
.implements Lcom/android/internal/view/menu/MenuView$ItemView;


# static fields
.field private static final NO_ALPHA:I = 0xff

.field private static sPrependShortcutLabel:Ljava/lang/String;


# instance fields
.field private mDisabledAlpha:F

.field private mIcon:Landroid/graphics/drawable/Drawable;

.field private mIconMenuView:Lcom/android/internal/view/menu/IconMenuView;

.field private mItemData:Lcom/android/internal/view/menu/MenuItemImpl;

.field private mItemInvoker:Lcom/android/internal/view/menu/MenuBuilder$ItemInvoker;

.field private mPositionIconAvailable:Landroid/graphics/Rect;

.field private mPositionIconOutput:Landroid/graphics/Rect;

.field private mShortcutCaption:Ljava/lang/String;

.field private mShortcutCaptionMode:Z

.field private mTextAppearance:I

.field private mTextAppearanceContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 86
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Lcom/android/internal/view/menu/IconMenuItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 87
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 7
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@3
    .line 52
    new-instance v1, Landroid/graphics/Rect;

    #@5
    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    #@8
    iput-object v1, p0, Lcom/android/internal/view/menu/IconMenuItemView;->mPositionIconAvailable:Landroid/graphics/Rect;

    #@a
    .line 53
    new-instance v1, Landroid/graphics/Rect;

    #@c
    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    #@f
    iput-object v1, p0, Lcom/android/internal/view/menu/IconMenuItemView;->mPositionIconOutput:Landroid/graphics/Rect;

    #@11
    .line 63
    sget-object v1, Lcom/android/internal/view/menu/IconMenuItemView;->sPrependShortcutLabel:Ljava/lang/String;

    #@13
    if-nez v1, :cond_22

    #@15
    .line 68
    invoke-virtual {p0}, Lcom/android/internal/view/menu/IconMenuItemView;->getResources()Landroid/content/res/Resources;

    #@18
    move-result-object v1

    #@19
    const v2, 0x10403c4

    #@1c
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    sput-object v1, Lcom/android/internal/view/menu/IconMenuItemView;->sPrependShortcutLabel:Ljava/lang/String;

    #@22
    .line 72
    :cond_22
    sget-object v1, Lcom/android/internal/R$styleable;->MenuView:[I

    #@24
    const/4 v2, 0x0

    #@25
    invoke-virtual {p1, p2, v1, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@28
    move-result-object v0

    #@29
    .line 76
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v1, 0x6

    #@2a
    const v2, 0x3f4ccccd

    #@2d
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    #@30
    move-result v1

    #@31
    iput v1, p0, Lcom/android/internal/view/menu/IconMenuItemView;->mDisabledAlpha:F

    #@33
    .line 78
    const/4 v1, 0x1

    #@34
    const/4 v2, -0x1

    #@35
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@38
    move-result v1

    #@39
    iput v1, p0, Lcom/android/internal/view/menu/IconMenuItemView;->mTextAppearance:I

    #@3b
    .line 80
    iput-object p1, p0, Lcom/android/internal/view/menu/IconMenuItemView;->mTextAppearanceContext:Landroid/content/Context;

    #@3d
    .line 82
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@40
    .line 83
    return-void
.end method

.method private positionIcon()V
    .registers 8

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 276
    iget-object v0, p0, Lcom/android/internal/view/menu/IconMenuItemView;->mIcon:Landroid/graphics/drawable/Drawable;

    #@3
    if-nez v0, :cond_6

    #@5
    .line 289
    :goto_5
    return-void

    #@6
    .line 281
    :cond_6
    iget-object v6, p0, Lcom/android/internal/view/menu/IconMenuItemView;->mPositionIconOutput:Landroid/graphics/Rect;

    #@8
    .line 282
    .local v6, tmpRect:Landroid/graphics/Rect;
    invoke-virtual {p0, v3, v6}, Lcom/android/internal/view/menu/IconMenuItemView;->getLineBounds(ILandroid/graphics/Rect;)I

    #@b
    .line 283
    iget-object v0, p0, Lcom/android/internal/view/menu/IconMenuItemView;->mPositionIconAvailable:Landroid/graphics/Rect;

    #@d
    invoke-virtual {p0}, Lcom/android/internal/view/menu/IconMenuItemView;->getWidth()I

    #@10
    move-result v1

    #@11
    iget v2, v6, Landroid/graphics/Rect;->top:I

    #@13
    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    #@16
    .line 284
    invoke-virtual {p0}, Lcom/android/internal/view/menu/IconMenuItemView;->getLayoutDirection()I

    #@19
    move-result v5

    #@1a
    .line 285
    .local v5, layoutDirection:I
    const v0, 0x800013

    #@1d
    iget-object v1, p0, Lcom/android/internal/view/menu/IconMenuItemView;->mIcon:Landroid/graphics/drawable/Drawable;

    #@1f
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@22
    move-result v1

    #@23
    iget-object v2, p0, Lcom/android/internal/view/menu/IconMenuItemView;->mIcon:Landroid/graphics/drawable/Drawable;

    #@25
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@28
    move-result v2

    #@29
    iget-object v3, p0, Lcom/android/internal/view/menu/IconMenuItemView;->mPositionIconAvailable:Landroid/graphics/Rect;

    #@2b
    iget-object v4, p0, Lcom/android/internal/view/menu/IconMenuItemView;->mPositionIconOutput:Landroid/graphics/Rect;

    #@2d
    invoke-static/range {v0 .. v5}, Landroid/view/Gravity;->apply(IIILandroid/graphics/Rect;Landroid/graphics/Rect;I)V

    #@30
    .line 288
    iget-object v0, p0, Lcom/android/internal/view/menu/IconMenuItemView;->mIcon:Landroid/graphics/drawable/Drawable;

    #@32
    iget-object v1, p0, Lcom/android/internal/view/menu/IconMenuItemView;->mPositionIconOutput:Landroid/graphics/Rect;

    #@34
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    #@37
    goto :goto_5
.end method


# virtual methods
.method protected drawableStateChanged()V
    .registers 5

    #@0
    .prologue
    .line 227
    invoke-super {p0}, Landroid/widget/TextView;->drawableStateChanged()V

    #@3
    .line 229
    iget-object v1, p0, Lcom/android/internal/view/menu/IconMenuItemView;->mItemData:Lcom/android/internal/view/menu/MenuItemImpl;

    #@5
    if-eqz v1, :cond_2d

    #@7
    iget-object v1, p0, Lcom/android/internal/view/menu/IconMenuItemView;->mIcon:Landroid/graphics/drawable/Drawable;

    #@9
    if-eqz v1, :cond_2d

    #@b
    .line 232
    iget-object v1, p0, Lcom/android/internal/view/menu/IconMenuItemView;->mItemData:Lcom/android/internal/view/menu/MenuItemImpl;

    #@d
    invoke-virtual {v1}, Lcom/android/internal/view/menu/MenuItemImpl;->isEnabled()Z

    #@10
    move-result v1

    #@11
    if-nez v1, :cond_2e

    #@13
    invoke-virtual {p0}, Lcom/android/internal/view/menu/IconMenuItemView;->isPressed()Z

    #@16
    move-result v1

    #@17
    if-nez v1, :cond_1f

    #@19
    invoke-virtual {p0}, Lcom/android/internal/view/menu/IconMenuItemView;->isFocused()Z

    #@1c
    move-result v1

    #@1d
    if-nez v1, :cond_2e

    #@1f
    :cond_1f
    const/4 v0, 0x1

    #@20
    .line 233
    .local v0, isInAlphaState:Z
    :goto_20
    iget-object v2, p0, Lcom/android/internal/view/menu/IconMenuItemView;->mIcon:Landroid/graphics/drawable/Drawable;

    #@22
    if-eqz v0, :cond_30

    #@24
    iget v1, p0, Lcom/android/internal/view/menu/IconMenuItemView;->mDisabledAlpha:F

    #@26
    const/high16 v3, 0x437f

    #@28
    mul-float/2addr v1, v3

    #@29
    float-to-int v1, v1

    #@2a
    :goto_2a
    invoke-virtual {v2, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    #@2d
    .line 235
    .end local v0           #isInAlphaState:Z
    :cond_2d
    return-void

    #@2e
    .line 232
    :cond_2e
    const/4 v0, 0x0

    #@2f
    goto :goto_20

    #@30
    .line 233
    .restart local v0       #isInAlphaState:Z
    :cond_30
    const/16 v1, 0xff

    #@32
    goto :goto_2a
.end method

.method public getItemData()Lcom/android/internal/view/menu/MenuItemImpl;
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$CapturedViewProperty;
        retrieveReturn = true
    .end annotation

    #@0
    .prologue
    .line 208
    iget-object v0, p0, Lcom/android/internal/view/menu/IconMenuItemView;->mItemData:Lcom/android/internal/view/menu/MenuItemImpl;

    #@2
    return-object v0
.end method

.method getTextAppropriateLayoutParams()Lcom/android/internal/view/menu/IconMenuView$LayoutParams;
    .registers 4

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 257
    invoke-virtual {p0}, Lcom/android/internal/view/menu/IconMenuItemView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@4
    move-result-object v0

    #@5
    check-cast v0, Lcom/android/internal/view/menu/IconMenuView$LayoutParams;

    #@7
    .line 258
    .local v0, lp:Lcom/android/internal/view/menu/IconMenuView$LayoutParams;
    if-nez v0, :cond_e

    #@9
    .line 260
    new-instance v0, Lcom/android/internal/view/menu/IconMenuView$LayoutParams;

    #@b
    .end local v0           #lp:Lcom/android/internal/view/menu/IconMenuView$LayoutParams;
    invoke-direct {v0, v1, v1}, Lcom/android/internal/view/menu/IconMenuView$LayoutParams;-><init>(II)V

    #@e
    .line 265
    .restart local v0       #lp:Lcom/android/internal/view/menu/IconMenuView$LayoutParams;
    :cond_e
    invoke-virtual {p0}, Lcom/android/internal/view/menu/IconMenuItemView;->getText()Ljava/lang/CharSequence;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {p0}, Lcom/android/internal/view/menu/IconMenuItemView;->getPaint()Landroid/text/TextPaint;

    #@15
    move-result-object v2

    #@16
    invoke-static {v1, v2}, Landroid/text/Layout;->getDesiredWidth(Ljava/lang/CharSequence;Landroid/text/TextPaint;)F

    #@19
    move-result v1

    #@1a
    float-to-int v1, v1

    #@1b
    iput v1, v0, Lcom/android/internal/view/menu/IconMenuView$LayoutParams;->desiredWidth:I

    #@1d
    .line 267
    return-object v0
.end method

.method public initialize(Lcom/android/internal/view/menu/MenuItemImpl;I)V
    .registers 5
    .parameter "itemData"
    .parameter "menuType"

    #@0
    .prologue
    .line 107
    iput-object p1, p0, Lcom/android/internal/view/menu/IconMenuItemView;->mItemData:Lcom/android/internal/view/menu/MenuItemImpl;

    #@2
    .line 109
    invoke-virtual {p1, p0}, Lcom/android/internal/view/menu/MenuItemImpl;->getTitleForItemView(Lcom/android/internal/view/menu/MenuView$ItemView;)Ljava/lang/CharSequence;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {p1}, Lcom/android/internal/view/menu/MenuItemImpl;->getIcon()Landroid/graphics/drawable/Drawable;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/view/menu/IconMenuItemView;->initialize(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;)V

    #@d
    .line 111
    invoke-virtual {p1}, Lcom/android/internal/view/menu/MenuItemImpl;->isVisible()Z

    #@10
    move-result v0

    #@11
    if-eqz v0, :cond_1f

    #@13
    const/4 v0, 0x0

    #@14
    :goto_14
    invoke-virtual {p0, v0}, Lcom/android/internal/view/menu/IconMenuItemView;->setVisibility(I)V

    #@17
    .line 112
    invoke-virtual {p1}, Lcom/android/internal/view/menu/MenuItemImpl;->isEnabled()Z

    #@1a
    move-result v0

    #@1b
    invoke-virtual {p0, v0}, Lcom/android/internal/view/menu/IconMenuItemView;->setEnabled(Z)V

    #@1e
    .line 113
    return-void

    #@1f
    .line 111
    :cond_1f
    const/16 v0, 0x8

    #@21
    goto :goto_14
.end method

.method initialize(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;)V
    .registers 5
    .parameter "title"
    .parameter "icon"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 95
    invoke-virtual {p0, v0}, Lcom/android/internal/view/menu/IconMenuItemView;->setClickable(Z)V

    #@4
    .line 96
    invoke-virtual {p0, v0}, Lcom/android/internal/view/menu/IconMenuItemView;->setFocusable(Z)V

    #@7
    .line 98
    iget v0, p0, Lcom/android/internal/view/menu/IconMenuItemView;->mTextAppearance:I

    #@9
    const/4 v1, -0x1

    #@a
    if-eq v0, v1, :cond_13

    #@c
    .line 99
    iget-object v0, p0, Lcom/android/internal/view/menu/IconMenuItemView;->mTextAppearanceContext:Landroid/content/Context;

    #@e
    iget v1, p0, Lcom/android/internal/view/menu/IconMenuItemView;->mTextAppearance:I

    #@10
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/view/menu/IconMenuItemView;->setTextAppearance(Landroid/content/Context;I)V

    #@13
    .line 102
    :cond_13
    invoke-virtual {p0, p1}, Lcom/android/internal/view/menu/IconMenuItemView;->setTitle(Ljava/lang/CharSequence;)V

    #@16
    .line 103
    invoke-virtual {p0, p2}, Lcom/android/internal/view/menu/IconMenuItemView;->setIcon(Landroid/graphics/drawable/Drawable;)V

    #@19
    .line 104
    return-void
.end method

.method protected onLayout(ZIIII)V
    .registers 6
    .parameter "changed"
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    .line 239
    invoke-super/range {p0 .. p5}, Landroid/widget/TextView;->onLayout(ZIIII)V

    #@3
    .line 241
    invoke-direct {p0}, Lcom/android/internal/view/menu/IconMenuItemView;->positionIcon()V

    #@6
    .line 242
    return-void
.end method

.method protected onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 6
    .parameter "text"
    .parameter "start"
    .parameter "before"
    .parameter "after"

    #@0
    .prologue
    .line 246
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/TextView;->onTextChanged(Ljava/lang/CharSequence;III)V

    #@3
    .line 249
    invoke-virtual {p0}, Lcom/android/internal/view/menu/IconMenuItemView;->getTextAppropriateLayoutParams()Lcom/android/internal/view/menu/IconMenuView$LayoutParams;

    #@6
    move-result-object v0

    #@7
    invoke-virtual {p0, v0}, Lcom/android/internal/view/menu/IconMenuItemView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@a
    .line 250
    return-void
.end method

.method public performClick()Z
    .registers 5

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 122
    invoke-super {p0}, Landroid/widget/TextView;->performClick()Z

    #@5
    move-result v2

    #@6
    if-eqz v2, :cond_9

    #@8
    .line 130
    :goto_8
    return v0

    #@9
    .line 126
    :cond_9
    iget-object v2, p0, Lcom/android/internal/view/menu/IconMenuItemView;->mItemInvoker:Lcom/android/internal/view/menu/MenuBuilder$ItemInvoker;

    #@b
    if-eqz v2, :cond_1b

    #@d
    iget-object v2, p0, Lcom/android/internal/view/menu/IconMenuItemView;->mItemInvoker:Lcom/android/internal/view/menu/MenuBuilder$ItemInvoker;

    #@f
    iget-object v3, p0, Lcom/android/internal/view/menu/IconMenuItemView;->mItemData:Lcom/android/internal/view/menu/MenuItemImpl;

    #@11
    invoke-interface {v2, v3}, Lcom/android/internal/view/menu/MenuBuilder$ItemInvoker;->invokeItem(Lcom/android/internal/view/menu/MenuItemImpl;)Z

    #@14
    move-result v2

    #@15
    if-eqz v2, :cond_1b

    #@17
    .line 127
    invoke-virtual {p0, v1}, Lcom/android/internal/view/menu/IconMenuItemView;->playSoundEffect(I)V

    #@1a
    goto :goto_8

    #@1b
    :cond_1b
    move v0, v1

    #@1c
    .line 130
    goto :goto_8
.end method

.method public prefersCondensedTitle()Z
    .registers 2

    #@0
    .prologue
    .line 310
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method setCaptionMode(Z)V
    .registers 4
    .parameter "shortcut"

    #@0
    .prologue
    .line 154
    iget-object v1, p0, Lcom/android/internal/view/menu/IconMenuItemView;->mItemData:Lcom/android/internal/view/menu/MenuItemImpl;

    #@2
    if-nez v1, :cond_5

    #@4
    .line 172
    :goto_4
    return-void

    #@5
    .line 158
    :cond_5
    if-eqz p1, :cond_2e

    #@7
    iget-object v1, p0, Lcom/android/internal/view/menu/IconMenuItemView;->mItemData:Lcom/android/internal/view/menu/MenuItemImpl;

    #@9
    invoke-virtual {v1}, Lcom/android/internal/view/menu/MenuItemImpl;->shouldShowShortcut()Z

    #@c
    move-result v1

    #@d
    if-eqz v1, :cond_2e

    #@f
    const/4 v1, 0x1

    #@10
    :goto_10
    iput-boolean v1, p0, Lcom/android/internal/view/menu/IconMenuItemView;->mShortcutCaptionMode:Z

    #@12
    .line 160
    iget-object v1, p0, Lcom/android/internal/view/menu/IconMenuItemView;->mItemData:Lcom/android/internal/view/menu/MenuItemImpl;

    #@14
    invoke-virtual {v1, p0}, Lcom/android/internal/view/menu/MenuItemImpl;->getTitleForItemView(Lcom/android/internal/view/menu/MenuView$ItemView;)Ljava/lang/CharSequence;

    #@17
    move-result-object v0

    #@18
    .line 162
    .local v0, text:Ljava/lang/CharSequence;
    iget-boolean v1, p0, Lcom/android/internal/view/menu/IconMenuItemView;->mShortcutCaptionMode:Z

    #@1a
    if-eqz v1, :cond_2a

    #@1c
    .line 164
    iget-object v1, p0, Lcom/android/internal/view/menu/IconMenuItemView;->mShortcutCaption:Ljava/lang/String;

    #@1e
    if-nez v1, :cond_28

    #@20
    .line 165
    iget-object v1, p0, Lcom/android/internal/view/menu/IconMenuItemView;->mItemData:Lcom/android/internal/view/menu/MenuItemImpl;

    #@22
    invoke-virtual {v1}, Lcom/android/internal/view/menu/MenuItemImpl;->getShortcutLabel()Ljava/lang/String;

    #@25
    move-result-object v1

    #@26
    iput-object v1, p0, Lcom/android/internal/view/menu/IconMenuItemView;->mShortcutCaption:Ljava/lang/String;

    #@28
    .line 168
    :cond_28
    iget-object v0, p0, Lcom/android/internal/view/menu/IconMenuItemView;->mShortcutCaption:Ljava/lang/String;

    #@2a
    .line 171
    :cond_2a
    invoke-virtual {p0, v0}, Lcom/android/internal/view/menu/IconMenuItemView;->setText(Ljava/lang/CharSequence;)V

    #@2d
    goto :goto_4

    #@2e
    .line 158
    .end local v0           #text:Ljava/lang/CharSequence;
    :cond_2e
    const/4 v1, 0x0

    #@2f
    goto :goto_10
.end method

.method public setCheckable(Z)V
    .registers 2
    .parameter "checkable"

    #@0
    .prologue
    .line 292
    return-void
.end method

.method public setChecked(Z)V
    .registers 2
    .parameter "checked"

    #@0
    .prologue
    .line 295
    return-void
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)V
    .registers 6
    .parameter "icon"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x0

    #@2
    .line 175
    iput-object p1, p0, Lcom/android/internal/view/menu/IconMenuItemView;->mIcon:Landroid/graphics/drawable/Drawable;

    #@4
    .line 177
    if-eqz p1, :cond_1d

    #@6
    .line 180
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@9
    move-result v0

    #@a
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@d
    move-result v1

    #@e
    invoke-virtual {p1, v3, v3, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@11
    .line 183
    invoke-virtual {p0, v2, p1, v2, v2}, Lcom/android/internal/view/menu/IconMenuItemView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    #@14
    .line 186
    const/16 v0, 0x51

    #@16
    invoke-virtual {p0, v0}, Lcom/android/internal/view/menu/IconMenuItemView;->setGravity(I)V

    #@19
    .line 193
    invoke-virtual {p0}, Lcom/android/internal/view/menu/IconMenuItemView;->requestLayout()V

    #@1c
    .line 200
    :goto_1c
    return-void

    #@1d
    .line 195
    :cond_1d
    invoke-virtual {p0, v2, v2, v2, v2}, Lcom/android/internal/view/menu/IconMenuItemView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    #@20
    .line 198
    const/16 v0, 0x11

    #@22
    invoke-virtual {p0, v0}, Lcom/android/internal/view/menu/IconMenuItemView;->setGravity(I)V

    #@25
    goto :goto_1c
.end method

.method setIconMenuView(Lcom/android/internal/view/menu/IconMenuView;)V
    .registers 2
    .parameter "iconMenuView"

    #@0
    .prologue
    .line 222
    iput-object p1, p0, Lcom/android/internal/view/menu/IconMenuItemView;->mIconMenuView:Lcom/android/internal/view/menu/IconMenuView;

    #@2
    .line 223
    return-void
.end method

.method public setItemData(Lcom/android/internal/view/menu/MenuItemImpl;)V
    .registers 2
    .parameter "data"

    #@0
    .prologue
    .line 116
    iput-object p1, p0, Lcom/android/internal/view/menu/IconMenuItemView;->mItemData:Lcom/android/internal/view/menu/MenuItemImpl;

    #@2
    .line 117
    return-void
.end method

.method public setItemInvoker(Lcom/android/internal/view/menu/MenuBuilder$ItemInvoker;)V
    .registers 2
    .parameter "itemInvoker"

    #@0
    .prologue
    .line 203
    iput-object p1, p0, Lcom/android/internal/view/menu/IconMenuItemView;->mItemInvoker:Lcom/android/internal/view/menu/MenuBuilder$ItemInvoker;

    #@2
    .line 204
    return-void
.end method

.method public setShortcut(ZC)V
    .registers 4
    .parameter "showShortcut"
    .parameter "shortcutKey"

    #@0
    .prologue
    .line 299
    iget-boolean v0, p0, Lcom/android/internal/view/menu/IconMenuItemView;->mShortcutCaptionMode:Z

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 304
    const/4 v0, 0x0

    #@5
    iput-object v0, p0, Lcom/android/internal/view/menu/IconMenuItemView;->mShortcutCaption:Ljava/lang/String;

    #@7
    .line 305
    const/4 v0, 0x1

    #@8
    invoke-virtual {p0, v0}, Lcom/android/internal/view/menu/IconMenuItemView;->setCaptionMode(Z)V

    #@b
    .line 307
    :cond_b
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .registers 3
    .parameter "title"

    #@0
    .prologue
    .line 136
    iget-boolean v0, p0, Lcom/android/internal/view/menu/IconMenuItemView;->mShortcutCaptionMode:Z

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 142
    const/4 v0, 0x1

    #@5
    invoke-virtual {p0, v0}, Lcom/android/internal/view/menu/IconMenuItemView;->setCaptionMode(Z)V

    #@8
    .line 147
    :cond_8
    :goto_8
    return-void

    #@9
    .line 144
    :cond_9
    if-eqz p1, :cond_8

    #@b
    .line 145
    invoke-virtual {p0, p1}, Lcom/android/internal/view/menu/IconMenuItemView;->setText(Ljava/lang/CharSequence;)V

    #@e
    goto :goto_8
.end method

.method public setVisibility(I)V
    .registers 3
    .parameter "v"

    #@0
    .prologue
    .line 213
    invoke-super {p0, p1}, Landroid/widget/TextView;->setVisibility(I)V

    #@3
    .line 215
    iget-object v0, p0, Lcom/android/internal/view/menu/IconMenuItemView;->mIconMenuView:Lcom/android/internal/view/menu/IconMenuView;

    #@5
    if-eqz v0, :cond_c

    #@7
    .line 217
    iget-object v0, p0, Lcom/android/internal/view/menu/IconMenuItemView;->mIconMenuView:Lcom/android/internal/view/menu/IconMenuView;

    #@9
    invoke-virtual {v0}, Lcom/android/internal/view/menu/IconMenuView;->markStaleChildren()V

    #@c
    .line 219
    :cond_c
    return-void
.end method

.method public showsIcon()Z
    .registers 2

    #@0
    .prologue
    .line 314
    const/4 v0, 0x1

    #@1
    return v0
.end method
