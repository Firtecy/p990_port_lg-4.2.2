.class public Lcom/android/internal/view/menu/ActionMenuView;
.super Landroid/widget/LinearLayout;
.source "ActionMenuView.java"

# interfaces
.implements Lcom/android/internal/view/menu/MenuBuilder$ItemInvoker;
.implements Lcom/android/internal/view/menu/MenuView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;,
        Lcom/android/internal/view/menu/ActionMenuView$ActionMenuChildView;
    }
.end annotation


# static fields
.field static final GENERATED_ITEM_PADDING:I = 0x4

.field static final MIN_CELL_SIZE:I = 0x38

.field private static final TAG:Ljava/lang/String; = "ActionMenuView"


# instance fields
.field private mAfterMeasured:Z

.field private mFormatItems:Z

.field private mFormatItemsWidth:I

.field private mGeneratedItemPadding:I

.field private mMaxItemHeight:I

.field private mMeasuredExtraWidth:I

.field private mMenu:Lcom/android/internal/view/menu/MenuBuilder;

.field private mMinCellSize:I

.field private mPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

.field private mReserveOverflow:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 54
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Lcom/android/internal/view/menu/ActionMenuView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 8
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 58
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 51
    iput-boolean v4, p0, Lcom/android/internal/view/menu/ActionMenuView;->mAfterMeasured:Z

    #@6
    .line 59
    invoke-virtual {p0, v4}, Lcom/android/internal/view/menu/ActionMenuView;->setBaselineAligned(Z)V

    #@9
    .line 60
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@10
    move-result-object v2

    #@11
    iget v1, v2, Landroid/util/DisplayMetrics;->density:F

    #@13
    .line 61
    .local v1, density:F
    const/high16 v2, 0x4260

    #@15
    mul-float/2addr v2, v1

    #@16
    float-to-int v2, v2

    #@17
    iput v2, p0, Lcom/android/internal/view/menu/ActionMenuView;->mMinCellSize:I

    #@19
    .line 62
    const/high16 v2, 0x4080

    #@1b
    mul-float/2addr v2, v1

    #@1c
    float-to-int v2, v2

    #@1d
    iput v2, p0, Lcom/android/internal/view/menu/ActionMenuView;->mGeneratedItemPadding:I

    #@1f
    .line 64
    sget-object v2, Lcom/android/internal/R$styleable;->ActionBar:[I

    #@21
    const v3, 0x10102ce

    #@24
    invoke-virtual {p1, p2, v2, v3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@27
    move-result-object v0

    #@28
    .line 66
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v2, 0x4

    #@29
    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@2c
    move-result v2

    #@2d
    iput v2, p0, Lcom/android/internal/view/menu/ActionMenuView;->mMaxItemHeight:I

    #@2f
    .line 67
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@32
    .line 68
    return-void
.end method

.method static measureChildForCells(Landroid/view/View;IIII)I
    .registers 18
    .parameter "child"
    .parameter "cellSize"
    .parameter "cellsRemaining"
    .parameter "parentHeightMeasureSpec"
    .parameter "parentHeightPadding"

    #@0
    .prologue
    .line 370
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@3
    move-result-object v8

    #@4
    check-cast v8, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;

    #@6
    .line 372
    .local v8, lp:Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;
    invoke-static/range {p3 .. p3}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@9
    move-result v11

    #@a
    sub-int v2, v11, p4

    #@c
    .line 374
    .local v2, childHeightSize:I
    invoke-static/range {p3 .. p3}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@f
    move-result v1

    #@10
    .line 375
    .local v1, childHeightMode:I
    invoke-static {v2, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@13
    move-result v3

    #@14
    .line 377
    .local v3, childHeightSpec:I
    instance-of v11, p0, Lcom/android/internal/view/menu/ActionMenuItemView;

    #@16
    if-eqz v11, :cond_5f

    #@18
    move-object v11, p0

    #@19
    check-cast v11, Lcom/android/internal/view/menu/ActionMenuItemView;

    #@1b
    move-object v7, v11

    #@1c
    .line 379
    .local v7, itemView:Lcom/android/internal/view/menu/ActionMenuItemView;
    :goto_1c
    if-eqz v7, :cond_61

    #@1e
    invoke-virtual {v7}, Lcom/android/internal/view/menu/ActionMenuItemView;->hasText()Z

    #@21
    move-result v11

    #@22
    if-eqz v11, :cond_61

    #@24
    const/4 v6, 0x1

    #@25
    .line 381
    .local v6, hasText:Z
    :goto_25
    const/4 v0, 0x0

    #@26
    .line 382
    .local v0, cellsUsed:I
    if-lez p2, :cond_4a

    #@28
    if-eqz v6, :cond_2d

    #@2a
    const/4 v11, 0x2

    #@2b
    if-lt p2, v11, :cond_4a

    #@2d
    .line 383
    :cond_2d
    mul-int v11, p1, p2

    #@2f
    const/high16 v12, -0x8000

    #@31
    invoke-static {v11, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@34
    move-result v4

    #@35
    .line 385
    .local v4, childWidthSpec:I
    invoke-virtual {p0, v4, v3}, Landroid/view/View;->measure(II)V

    #@38
    .line 387
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    #@3b
    move-result v9

    #@3c
    .line 388
    .local v9, measuredWidth:I
    div-int v0, v9, p1

    #@3e
    .line 389
    rem-int v11, v9, p1

    #@40
    if-eqz v11, :cond_44

    #@42
    add-int/lit8 v0, v0, 0x1

    #@44
    .line 390
    :cond_44
    if-eqz v6, :cond_4a

    #@46
    const/4 v11, 0x2

    #@47
    if-ge v0, v11, :cond_4a

    #@49
    const/4 v0, 0x2

    #@4a
    .line 395
    .end local v4           #childWidthSpec:I
    .end local v9           #measuredWidth:I
    :cond_4a
    iget-boolean v11, v8, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;->isOverflowButton:Z

    #@4c
    if-nez v11, :cond_63

    #@4e
    const/4 v5, 0x1

    #@4f
    .line 397
    .local v5, expandable:Z
    :goto_4f
    iput-boolean v5, v8, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;->expandable:Z

    #@51
    .line 399
    iput v0, v8, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;->cellsUsed:I

    #@53
    .line 400
    mul-int v10, v0, p1

    #@55
    .line 401
    .local v10, targetWidth:I
    const/high16 v11, 0x4000

    #@57
    invoke-static {v10, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@5a
    move-result v11

    #@5b
    invoke-virtual {p0, v11, v3}, Landroid/view/View;->measure(II)V

    #@5e
    .line 403
    return v0

    #@5f
    .line 377
    .end local v0           #cellsUsed:I
    .end local v5           #expandable:Z
    .end local v6           #hasText:Z
    .end local v7           #itemView:Lcom/android/internal/view/menu/ActionMenuItemView;
    .end local v10           #targetWidth:I
    :cond_5f
    const/4 v7, 0x0

    #@60
    goto :goto_1c

    #@61
    .line 379
    .restart local v7       #itemView:Lcom/android/internal/view/menu/ActionMenuItemView;
    :cond_61
    const/4 v6, 0x0

    #@62
    goto :goto_25

    #@63
    .line 395
    .restart local v0       #cellsUsed:I
    .restart local v6       #hasText:Z
    :cond_63
    const/4 v5, 0x0

    #@64
    goto :goto_4f
.end method

.method private onMeasureExactFormat(II)V
    .registers 45
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    .line 138
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@3
    move-result v17

    #@4
    .line 139
    .local v17, heightMode:I
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@7
    move-result v37

    #@8
    .line 140
    .local v37, widthSize:I
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@b
    move-result v19

    #@c
    .line 142
    .local v19, heightSize:I
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/view/menu/ActionMenuView;->getPaddingLeft()I

    #@f
    move-result v38

    #@10
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/view/menu/ActionMenuView;->getPaddingRight()I

    #@13
    move-result v39

    #@14
    add-int v36, v38, v39

    #@16
    .line 143
    .local v36, widthPadding:I
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/view/menu/ActionMenuView;->getPaddingTop()I

    #@19
    move-result v38

    #@1a
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/view/menu/ActionMenuView;->getPaddingBottom()I

    #@1d
    move-result v39

    #@1e
    add-int v18, v38, v39

    #@20
    .line 145
    .local v18, heightPadding:I
    const/high16 v38, 0x4000

    #@22
    move/from16 v0, v17

    #@24
    move/from16 v1, v38

    #@26
    if-ne v0, v1, :cond_50

    #@28
    sub-int v38, v19, v18

    #@2a
    const/high16 v39, 0x4000

    #@2c
    invoke-static/range {v38 .. v39}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@2f
    move-result v22

    #@30
    .line 150
    .local v22, itemHeightSpec:I
    :goto_30
    sub-int v37, v37, v36

    #@32
    .line 153
    move-object/from16 v0, p0

    #@34
    iget v0, v0, Lcom/android/internal/view/menu/ActionMenuView;->mMinCellSize:I

    #@36
    move/from16 v38, v0

    #@38
    div-int v4, v37, v38

    #@3a
    .line 154
    .local v4, cellCount:I
    move-object/from16 v0, p0

    #@3c
    iget v0, v0, Lcom/android/internal/view/menu/ActionMenuView;->mMinCellSize:I

    #@3e
    move/from16 v38, v0

    #@40
    rem-int v6, v37, v38

    #@42
    .line 156
    .local v6, cellSizeRemaining:I
    if-nez v4, :cond_63

    #@44
    .line 158
    const/16 v38, 0x0

    #@46
    move-object/from16 v0, p0

    #@48
    move/from16 v1, v37

    #@4a
    move/from16 v2, v38

    #@4c
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/view/menu/ActionMenuView;->setMeasuredDimension(II)V

    #@4f
    .line 353
    :goto_4f
    return-void

    #@50
    .line 145
    .end local v4           #cellCount:I
    .end local v6           #cellSizeRemaining:I
    .end local v22           #itemHeightSpec:I
    :cond_50
    move-object/from16 v0, p0

    #@52
    iget v0, v0, Lcom/android/internal/view/menu/ActionMenuView;->mMaxItemHeight:I

    #@54
    move/from16 v38, v0

    #@56
    sub-int v39, v19, v18

    #@58
    invoke-static/range {v38 .. v39}, Ljava/lang/Math;->min(II)I

    #@5b
    move-result v38

    #@5c
    const/high16 v39, -0x8000

    #@5e
    invoke-static/range {v38 .. v39}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@61
    move-result v22

    #@62
    goto :goto_30

    #@63
    .line 162
    .restart local v4       #cellCount:I
    .restart local v6       #cellSizeRemaining:I
    .restart local v22       #itemHeightSpec:I
    :cond_63
    move-object/from16 v0, p0

    #@65
    iget v0, v0, Lcom/android/internal/view/menu/ActionMenuView;->mMinCellSize:I

    #@67
    move/from16 v38, v0

    #@69
    div-int v39, v6, v4

    #@6b
    add-int v5, v38, v39

    #@6d
    .line 164
    .local v5, cellSize:I
    move v8, v4

    #@6e
    .line 165
    .local v8, cellsRemaining:I
    const/16 v25, 0x0

    #@70
    .line 166
    .local v25, maxChildHeight:I
    const/16 v24, 0x0

    #@72
    .line 167
    .local v24, maxCellsUsed:I
    const/4 v14, 0x0

    #@73
    .line 168
    .local v14, expandableItemCount:I
    const/16 v34, 0x0

    #@75
    .line 169
    .local v34, visibleItemCount:I
    const/16 v16, 0x0

    #@77
    .line 172
    .local v16, hasOverflow:Z
    const-wide/16 v32, 0x0

    #@79
    .line 174
    .local v32, smallestItemsAt:J
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/view/menu/ActionMenuView;->getChildCount()I

    #@7c
    move-result v12

    #@7d
    .line 175
    .local v12, childCount:I
    const/16 v20, 0x0

    #@7f
    .local v20, i:I
    :goto_7f
    move/from16 v0, v20

    #@81
    if-ge v0, v12, :cond_146

    #@83
    .line 176
    move-object/from16 v0, p0

    #@85
    move/from16 v1, v20

    #@87
    invoke-virtual {v0, v1}, Lcom/android/internal/view/menu/ActionMenuView;->getChildAt(I)Landroid/view/View;

    #@8a
    move-result-object v11

    #@8b
    .line 177
    .local v11, child:Landroid/view/View;
    invoke-virtual {v11}, Landroid/view/View;->getVisibility()I

    #@8e
    move-result v38

    #@8f
    const/16 v39, 0x8

    #@91
    move/from16 v0, v38

    #@93
    move/from16 v1, v39

    #@95
    if-ne v0, v1, :cond_9a

    #@97
    .line 175
    :cond_97
    :goto_97
    add-int/lit8 v20, v20, 0x1

    #@99
    goto :goto_7f

    #@9a
    .line 179
    :cond_9a
    instance-of v0, v11, Lcom/android/internal/view/menu/ActionMenuItemView;

    #@9c
    move/from16 v21, v0

    #@9e
    .line 180
    .local v21, isGeneratedItem:Z
    add-int/lit8 v34, v34, 0x1

    #@a0
    .line 182
    if-eqz v21, :cond_bd

    #@a2
    .line 185
    move-object/from16 v0, p0

    #@a4
    iget v0, v0, Lcom/android/internal/view/menu/ActionMenuView;->mGeneratedItemPadding:I

    #@a6
    move/from16 v38, v0

    #@a8
    const/16 v39, 0x0

    #@aa
    move-object/from16 v0, p0

    #@ac
    iget v0, v0, Lcom/android/internal/view/menu/ActionMenuView;->mGeneratedItemPadding:I

    #@ae
    move/from16 v40, v0

    #@b0
    const/16 v41, 0x0

    #@b2
    move/from16 v0, v38

    #@b4
    move/from16 v1, v39

    #@b6
    move/from16 v2, v40

    #@b8
    move/from16 v3, v41

    #@ba
    invoke-virtual {v11, v0, v1, v2, v3}, Landroid/view/View;->setPadding(IIII)V

    #@bd
    .line 188
    :cond_bd
    invoke-virtual {v11}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@c0
    move-result-object v23

    #@c1
    check-cast v23, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;

    #@c3
    .line 189
    .local v23, lp:Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;
    const/16 v38, 0x0

    #@c5
    move/from16 v0, v38

    #@c7
    move-object/from16 v1, v23

    #@c9
    iput-boolean v0, v1, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;->expanded:Z

    #@cb
    .line 190
    const/16 v38, 0x0

    #@cd
    move/from16 v0, v38

    #@cf
    move-object/from16 v1, v23

    #@d1
    iput v0, v1, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;->extraPixels:I

    #@d3
    .line 191
    const/16 v38, 0x0

    #@d5
    move/from16 v0, v38

    #@d7
    move-object/from16 v1, v23

    #@d9
    iput v0, v1, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;->cellsUsed:I

    #@db
    .line 192
    const/16 v38, 0x0

    #@dd
    move/from16 v0, v38

    #@df
    move-object/from16 v1, v23

    #@e1
    iput-boolean v0, v1, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;->expandable:Z

    #@e3
    .line 193
    const/16 v38, 0x0

    #@e5
    move/from16 v0, v38

    #@e7
    move-object/from16 v1, v23

    #@e9
    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@eb
    .line 194
    const/16 v38, 0x0

    #@ed
    move/from16 v0, v38

    #@ef
    move-object/from16 v1, v23

    #@f1
    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@f3
    .line 197
    move/from16 v0, v21

    #@f5
    move-object/from16 v1, v23

    #@f7
    iput-boolean v0, v1, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;->preventEdgeOffset:Z

    #@f9
    .line 201
    move-object/from16 v0, v23

    #@fb
    iget-boolean v0, v0, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;->isOverflowButton:Z

    #@fd
    move/from16 v38, v0

    #@ff
    if-eqz v38, :cond_144

    #@101
    const/4 v7, 0x1

    #@102
    .line 203
    .local v7, cellsAvailable:I
    :goto_102
    move/from16 v0, v22

    #@104
    move/from16 v1, v18

    #@106
    invoke-static {v11, v5, v7, v0, v1}, Lcom/android/internal/view/menu/ActionMenuView;->measureChildForCells(Landroid/view/View;IIII)I

    #@109
    move-result v9

    #@10a
    .line 206
    .local v9, cellsUsed:I
    move/from16 v0, v24

    #@10c
    invoke-static {v0, v9}, Ljava/lang/Math;->max(II)I

    #@10f
    move-result v24

    #@110
    .line 207
    move-object/from16 v0, v23

    #@112
    iget-boolean v0, v0, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;->expandable:Z

    #@114
    move/from16 v38, v0

    #@116
    if-eqz v38, :cond_11a

    #@118
    add-int/lit8 v14, v14, 0x1

    #@11a
    .line 208
    :cond_11a
    move-object/from16 v0, v23

    #@11c
    iget-boolean v0, v0, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;->isOverflowButton:Z

    #@11e
    move/from16 v38, v0

    #@120
    if-eqz v38, :cond_124

    #@122
    const/16 v16, 0x1

    #@124
    .line 210
    :cond_124
    sub-int/2addr v8, v9

    #@125
    .line 211
    invoke-virtual {v11}, Landroid/view/View;->getMeasuredHeight()I

    #@128
    move-result v38

    #@129
    move/from16 v0, v25

    #@12b
    move/from16 v1, v38

    #@12d
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@130
    move-result v25

    #@131
    .line 212
    const/16 v38, 0x1

    #@133
    move/from16 v0, v38

    #@135
    if-ne v9, v0, :cond_97

    #@137
    const/16 v38, 0x1

    #@139
    shl-int v38, v38, v20

    #@13b
    move/from16 v0, v38

    #@13d
    int-to-long v0, v0

    #@13e
    move-wide/from16 v38, v0

    #@140
    or-long v32, v32, v38

    #@142
    goto/16 :goto_97

    #@144
    .end local v7           #cellsAvailable:I
    .end local v9           #cellsUsed:I
    :cond_144
    move v7, v8

    #@145
    .line 201
    goto :goto_102

    #@146
    .line 217
    .end local v11           #child:Landroid/view/View;
    .end local v21           #isGeneratedItem:Z
    .end local v23           #lp:Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;
    :cond_146
    if-eqz v16, :cond_17d

    #@148
    const/16 v38, 0x2

    #@14a
    move/from16 v0, v34

    #@14c
    move/from16 v1, v38

    #@14e
    if-ne v0, v1, :cond_17d

    #@150
    const/4 v10, 0x1

    #@151
    .line 222
    .local v10, centerSingleExpandedItem:Z
    :goto_151
    const/16 v30, 0x0

    #@153
    .line 223
    .local v30, needsExpansion:Z
    :goto_153
    if-lez v14, :cond_1bd

    #@155
    if-lez v8, :cond_1bd

    #@157
    .line 224
    const v26, 0x7fffffff

    #@15a
    .line 225
    .local v26, minCells:I
    const-wide/16 v27, 0x0

    #@15c
    .line 226
    .local v27, minCellsAt:J
    const/16 v29, 0x0

    #@15e
    .line 227
    .local v29, minCellsItemCount:I
    const/16 v20, 0x0

    #@160
    :goto_160
    move/from16 v0, v20

    #@162
    if-ge v0, v12, :cond_1b7

    #@164
    .line 228
    move-object/from16 v0, p0

    #@166
    move/from16 v1, v20

    #@168
    invoke-virtual {v0, v1}, Lcom/android/internal/view/menu/ActionMenuView;->getChildAt(I)Landroid/view/View;

    #@16b
    move-result-object v11

    #@16c
    .line 229
    .restart local v11       #child:Landroid/view/View;
    invoke-virtual {v11}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@16f
    move-result-object v23

    #@170
    check-cast v23, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;

    #@172
    .line 232
    .restart local v23       #lp:Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;
    move-object/from16 v0, v23

    #@174
    iget-boolean v0, v0, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;->expandable:Z

    #@176
    move/from16 v38, v0

    #@178
    if-nez v38, :cond_17f

    #@17a
    .line 227
    :cond_17a
    :goto_17a
    add-int/lit8 v20, v20, 0x1

    #@17c
    goto :goto_160

    #@17d
    .line 217
    .end local v10           #centerSingleExpandedItem:Z
    .end local v11           #child:Landroid/view/View;
    .end local v23           #lp:Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;
    .end local v26           #minCells:I
    .end local v27           #minCellsAt:J
    .end local v29           #minCellsItemCount:I
    .end local v30           #needsExpansion:Z
    :cond_17d
    const/4 v10, 0x0

    #@17e
    goto :goto_151

    #@17f
    .line 235
    .restart local v10       #centerSingleExpandedItem:Z
    .restart local v11       #child:Landroid/view/View;
    .restart local v23       #lp:Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;
    .restart local v26       #minCells:I
    .restart local v27       #minCellsAt:J
    .restart local v29       #minCellsItemCount:I
    .restart local v30       #needsExpansion:Z
    :cond_17f
    move-object/from16 v0, v23

    #@181
    iget v0, v0, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;->cellsUsed:I

    #@183
    move/from16 v38, v0

    #@185
    move/from16 v0, v38

    #@187
    move/from16 v1, v26

    #@189
    if-ge v0, v1, :cond_19d

    #@18b
    .line 236
    move-object/from16 v0, v23

    #@18d
    iget v0, v0, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;->cellsUsed:I

    #@18f
    move/from16 v26, v0

    #@191
    .line 237
    const/16 v38, 0x1

    #@193
    shl-int v38, v38, v20

    #@195
    move/from16 v0, v38

    #@197
    int-to-long v0, v0

    #@198
    move-wide/from16 v27, v0

    #@19a
    .line 238
    const/16 v29, 0x1

    #@19c
    goto :goto_17a

    #@19d
    .line 239
    :cond_19d
    move-object/from16 v0, v23

    #@19f
    iget v0, v0, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;->cellsUsed:I

    #@1a1
    move/from16 v38, v0

    #@1a3
    move/from16 v0, v38

    #@1a5
    move/from16 v1, v26

    #@1a7
    if-ne v0, v1, :cond_17a

    #@1a9
    .line 240
    const/16 v38, 0x1

    #@1ab
    shl-int v38, v38, v20

    #@1ad
    move/from16 v0, v38

    #@1af
    int-to-long v0, v0

    #@1b0
    move-wide/from16 v38, v0

    #@1b2
    or-long v27, v27, v38

    #@1b4
    .line 241
    add-int/lit8 v29, v29, 0x1

    #@1b6
    goto :goto_17a

    #@1b7
    .line 246
    .end local v11           #child:Landroid/view/View;
    .end local v23           #lp:Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;
    :cond_1b7
    or-long v32, v32, v27

    #@1b9
    .line 248
    move/from16 v0, v29

    #@1bb
    if-le v0, v8, :cond_26b

    #@1bd
    .line 277
    .end local v26           #minCells:I
    .end local v27           #minCellsAt:J
    .end local v29           #minCellsItemCount:I
    :cond_1bd
    if-nez v16, :cond_2f6

    #@1bf
    const/16 v38, 0x1

    #@1c1
    move/from16 v0, v34

    #@1c3
    move/from16 v1, v38

    #@1c5
    if-ne v0, v1, :cond_2f6

    #@1c7
    const/16 v31, 0x1

    #@1c9
    .line 278
    .local v31, singleItem:Z
    :goto_1c9
    if-lez v8, :cond_376

    #@1cb
    const-wide/16 v38, 0x0

    #@1cd
    cmp-long v38, v32, v38

    #@1cf
    if-eqz v38, :cond_376

    #@1d1
    add-int/lit8 v38, v34, -0x1

    #@1d3
    move/from16 v0, v38

    #@1d5
    if-lt v8, v0, :cond_1e1

    #@1d7
    if-nez v31, :cond_1e1

    #@1d9
    const/16 v38, 0x1

    #@1db
    move/from16 v0, v24

    #@1dd
    move/from16 v1, v38

    #@1df
    if-le v0, v1, :cond_376

    #@1e1
    .line 280
    :cond_1e1
    invoke-static/range {v32 .. v33}, Ljava/lang/Long;->bitCount(J)I

    #@1e4
    move-result v38

    #@1e5
    move/from16 v0, v38

    #@1e7
    int-to-float v13, v0

    #@1e8
    .line 282
    .local v13, expandCount:F
    if-nez v31, :cond_23f

    #@1ea
    .line 284
    const-wide/16 v38, 0x1

    #@1ec
    and-long v38, v38, v32

    #@1ee
    const-wide/16 v40, 0x0

    #@1f0
    cmp-long v38, v38, v40

    #@1f2
    if-eqz v38, :cond_210

    #@1f4
    .line 285
    const/16 v38, 0x0

    #@1f6
    move-object/from16 v0, p0

    #@1f8
    move/from16 v1, v38

    #@1fa
    invoke-virtual {v0, v1}, Lcom/android/internal/view/menu/ActionMenuView;->getChildAt(I)Landroid/view/View;

    #@1fd
    move-result-object v38

    #@1fe
    invoke-virtual/range {v38 .. v38}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@201
    move-result-object v23

    #@202
    check-cast v23, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;

    #@204
    .line 286
    .restart local v23       #lp:Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;
    move-object/from16 v0, v23

    #@206
    iget-boolean v0, v0, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;->preventEdgeOffset:Z

    #@208
    move/from16 v38, v0

    #@20a
    if-nez v38, :cond_210

    #@20c
    const/high16 v38, 0x3f00

    #@20e
    sub-float v13, v13, v38

    #@210
    .line 288
    .end local v23           #lp:Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;
    :cond_210
    const/16 v38, 0x1

    #@212
    add-int/lit8 v39, v12, -0x1

    #@214
    shl-int v38, v38, v39

    #@216
    move/from16 v0, v38

    #@218
    int-to-long v0, v0

    #@219
    move-wide/from16 v38, v0

    #@21b
    and-long v38, v38, v32

    #@21d
    const-wide/16 v40, 0x0

    #@21f
    cmp-long v38, v38, v40

    #@221
    if-eqz v38, :cond_23f

    #@223
    .line 289
    add-int/lit8 v38, v12, -0x1

    #@225
    move-object/from16 v0, p0

    #@227
    move/from16 v1, v38

    #@229
    invoke-virtual {v0, v1}, Lcom/android/internal/view/menu/ActionMenuView;->getChildAt(I)Landroid/view/View;

    #@22c
    move-result-object v38

    #@22d
    invoke-virtual/range {v38 .. v38}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@230
    move-result-object v23

    #@231
    check-cast v23, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;

    #@233
    .line 290
    .restart local v23       #lp:Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;
    move-object/from16 v0, v23

    #@235
    iget-boolean v0, v0, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;->preventEdgeOffset:Z

    #@237
    move/from16 v38, v0

    #@239
    if-nez v38, :cond_23f

    #@23b
    const/high16 v38, 0x3f00

    #@23d
    sub-float v13, v13, v38

    #@23f
    .line 294
    .end local v23           #lp:Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;
    :cond_23f
    const/16 v38, 0x0

    #@241
    cmpl-float v38, v13, v38

    #@243
    if-lez v38, :cond_2fa

    #@245
    mul-int v38, v8, v5

    #@247
    move/from16 v0, v38

    #@249
    int-to-float v0, v0

    #@24a
    move/from16 v38, v0

    #@24c
    div-float v38, v38, v13

    #@24e
    move/from16 v0, v38

    #@250
    float-to-int v15, v0

    #@251
    .line 297
    .local v15, extraPixels:I
    :goto_251
    const/16 v20, 0x0

    #@253
    :goto_253
    move/from16 v0, v20

    #@255
    if-ge v0, v12, :cond_375

    #@257
    .line 298
    const/16 v38, 0x1

    #@259
    shl-int v38, v38, v20

    #@25b
    move/from16 v0, v38

    #@25d
    int-to-long v0, v0

    #@25e
    move-wide/from16 v38, v0

    #@260
    and-long v38, v38, v32

    #@262
    const-wide/16 v40, 0x0

    #@264
    cmp-long v38, v38, v40

    #@266
    if-nez v38, :cond_2fd

    #@268
    .line 297
    :cond_268
    :goto_268
    add-int/lit8 v20, v20, 0x1

    #@26a
    goto :goto_253

    #@26b
    .line 251
    .end local v13           #expandCount:F
    .end local v15           #extraPixels:I
    .end local v31           #singleItem:Z
    .restart local v26       #minCells:I
    .restart local v27       #minCellsAt:J
    .restart local v29       #minCellsItemCount:I
    :cond_26b
    add-int/lit8 v26, v26, 0x1

    #@26d
    .line 253
    const/16 v20, 0x0

    #@26f
    :goto_26f
    move/from16 v0, v20

    #@271
    if-ge v0, v12, :cond_2f2

    #@273
    .line 254
    move-object/from16 v0, p0

    #@275
    move/from16 v1, v20

    #@277
    invoke-virtual {v0, v1}, Lcom/android/internal/view/menu/ActionMenuView;->getChildAt(I)Landroid/view/View;

    #@27a
    move-result-object v11

    #@27b
    .line 255
    .restart local v11       #child:Landroid/view/View;
    invoke-virtual {v11}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@27e
    move-result-object v23

    #@27f
    check-cast v23, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;

    #@281
    .line 256
    .restart local v23       #lp:Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;
    const/16 v38, 0x1

    #@283
    shl-int v38, v38, v20

    #@285
    move/from16 v0, v38

    #@287
    int-to-long v0, v0

    #@288
    move-wide/from16 v38, v0

    #@28a
    and-long v38, v38, v27

    #@28c
    const-wide/16 v40, 0x0

    #@28e
    cmp-long v38, v38, v40

    #@290
    if-nez v38, :cond_2ac

    #@292
    .line 258
    move-object/from16 v0, v23

    #@294
    iget v0, v0, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;->cellsUsed:I

    #@296
    move/from16 v38, v0

    #@298
    move/from16 v0, v38

    #@29a
    move/from16 v1, v26

    #@29c
    if-ne v0, v1, :cond_2a9

    #@29e
    const/16 v38, 0x1

    #@2a0
    shl-int v38, v38, v20

    #@2a2
    move/from16 v0, v38

    #@2a4
    int-to-long v0, v0

    #@2a5
    move-wide/from16 v38, v0

    #@2a7
    or-long v32, v32, v38

    #@2a9
    .line 253
    :cond_2a9
    :goto_2a9
    add-int/lit8 v20, v20, 0x1

    #@2ab
    goto :goto_26f

    #@2ac
    .line 262
    :cond_2ac
    if-eqz v10, :cond_2d9

    #@2ae
    move-object/from16 v0, v23

    #@2b0
    iget-boolean v0, v0, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;->preventEdgeOffset:Z

    #@2b2
    move/from16 v38, v0

    #@2b4
    if-eqz v38, :cond_2d9

    #@2b6
    const/16 v38, 0x1

    #@2b8
    move/from16 v0, v38

    #@2ba
    if-ne v8, v0, :cond_2d9

    #@2bc
    .line 264
    move-object/from16 v0, p0

    #@2be
    iget v0, v0, Lcom/android/internal/view/menu/ActionMenuView;->mGeneratedItemPadding:I

    #@2c0
    move/from16 v38, v0

    #@2c2
    add-int v38, v38, v5

    #@2c4
    const/16 v39, 0x0

    #@2c6
    move-object/from16 v0, p0

    #@2c8
    iget v0, v0, Lcom/android/internal/view/menu/ActionMenuView;->mGeneratedItemPadding:I

    #@2ca
    move/from16 v40, v0

    #@2cc
    const/16 v41, 0x0

    #@2ce
    move/from16 v0, v38

    #@2d0
    move/from16 v1, v39

    #@2d2
    move/from16 v2, v40

    #@2d4
    move/from16 v3, v41

    #@2d6
    invoke-virtual {v11, v0, v1, v2, v3}, Landroid/view/View;->setPadding(IIII)V

    #@2d9
    .line 266
    :cond_2d9
    move-object/from16 v0, v23

    #@2db
    iget v0, v0, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;->cellsUsed:I

    #@2dd
    move/from16 v38, v0

    #@2df
    add-int/lit8 v38, v38, 0x1

    #@2e1
    move/from16 v0, v38

    #@2e3
    move-object/from16 v1, v23

    #@2e5
    iput v0, v1, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;->cellsUsed:I

    #@2e7
    .line 267
    const/16 v38, 0x1

    #@2e9
    move/from16 v0, v38

    #@2eb
    move-object/from16 v1, v23

    #@2ed
    iput-boolean v0, v1, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;->expanded:Z

    #@2ef
    .line 268
    add-int/lit8 v8, v8, -0x1

    #@2f1
    goto :goto_2a9

    #@2f2
    .line 271
    .end local v11           #child:Landroid/view/View;
    .end local v23           #lp:Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;
    :cond_2f2
    const/16 v30, 0x1

    #@2f4
    .line 272
    goto/16 :goto_153

    #@2f6
    .line 277
    .end local v26           #minCells:I
    .end local v27           #minCellsAt:J
    .end local v29           #minCellsItemCount:I
    :cond_2f6
    const/16 v31, 0x0

    #@2f8
    goto/16 :goto_1c9

    #@2fa
    .line 294
    .restart local v13       #expandCount:F
    .restart local v31       #singleItem:Z
    :cond_2fa
    const/4 v15, 0x0

    #@2fb
    goto/16 :goto_251

    #@2fd
    .line 300
    .restart local v15       #extraPixels:I
    :cond_2fd
    move-object/from16 v0, p0

    #@2ff
    move/from16 v1, v20

    #@301
    invoke-virtual {v0, v1}, Lcom/android/internal/view/menu/ActionMenuView;->getChildAt(I)Landroid/view/View;

    #@304
    move-result-object v11

    #@305
    .line 301
    .restart local v11       #child:Landroid/view/View;
    invoke-virtual {v11}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@308
    move-result-object v23

    #@309
    check-cast v23, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;

    #@30b
    .line 302
    .restart local v23       #lp:Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;
    instance-of v0, v11, Lcom/android/internal/view/menu/ActionMenuItemView;

    #@30d
    move/from16 v38, v0

    #@30f
    if-eqz v38, :cond_336

    #@311
    .line 304
    move-object/from16 v0, v23

    #@313
    iput v15, v0, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;->extraPixels:I

    #@315
    .line 305
    const/16 v38, 0x1

    #@317
    move/from16 v0, v38

    #@319
    move-object/from16 v1, v23

    #@31b
    iput-boolean v0, v1, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;->expanded:Z

    #@31d
    .line 306
    if-nez v20, :cond_332

    #@31f
    move-object/from16 v0, v23

    #@321
    iget-boolean v0, v0, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;->preventEdgeOffset:Z

    #@323
    move/from16 v38, v0

    #@325
    if-nez v38, :cond_332

    #@327
    .line 309
    neg-int v0, v15

    #@328
    move/from16 v38, v0

    #@32a
    div-int/lit8 v38, v38, 0x2

    #@32c
    move/from16 v0, v38

    #@32e
    move-object/from16 v1, v23

    #@330
    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@332
    .line 311
    :cond_332
    const/16 v30, 0x1

    #@334
    goto/16 :goto_268

    #@336
    .line 312
    :cond_336
    move-object/from16 v0, v23

    #@338
    iget-boolean v0, v0, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;->isOverflowButton:Z

    #@33a
    move/from16 v38, v0

    #@33c
    if-eqz v38, :cond_359

    #@33e
    .line 313
    move-object/from16 v0, v23

    #@340
    iput v15, v0, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;->extraPixels:I

    #@342
    .line 314
    const/16 v38, 0x1

    #@344
    move/from16 v0, v38

    #@346
    move-object/from16 v1, v23

    #@348
    iput-boolean v0, v1, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;->expanded:Z

    #@34a
    .line 315
    neg-int v0, v15

    #@34b
    move/from16 v38, v0

    #@34d
    div-int/lit8 v38, v38, 0x2

    #@34f
    move/from16 v0, v38

    #@351
    move-object/from16 v1, v23

    #@353
    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@355
    .line 316
    const/16 v30, 0x1

    #@357
    goto/16 :goto_268

    #@359
    .line 321
    :cond_359
    if-eqz v20, :cond_363

    #@35b
    .line 322
    div-int/lit8 v38, v15, 0x2

    #@35d
    move/from16 v0, v38

    #@35f
    move-object/from16 v1, v23

    #@361
    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@363
    .line 324
    :cond_363
    add-int/lit8 v38, v12, -0x1

    #@365
    move/from16 v0, v20

    #@367
    move/from16 v1, v38

    #@369
    if-eq v0, v1, :cond_268

    #@36b
    .line 325
    div-int/lit8 v38, v15, 0x2

    #@36d
    move/from16 v0, v38

    #@36f
    move-object/from16 v1, v23

    #@371
    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@373
    goto/16 :goto_268

    #@375
    .line 330
    .end local v11           #child:Landroid/view/View;
    .end local v23           #lp:Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;
    :cond_375
    const/4 v8, 0x0

    #@376
    .line 334
    .end local v13           #expandCount:F
    .end local v15           #extraPixels:I
    :cond_376
    if-eqz v30, :cond_3b9

    #@378
    .line 335
    const/16 v20, 0x0

    #@37a
    :goto_37a
    move/from16 v0, v20

    #@37c
    if-ge v0, v12, :cond_3b9

    #@37e
    .line 336
    move-object/from16 v0, p0

    #@380
    move/from16 v1, v20

    #@382
    invoke-virtual {v0, v1}, Lcom/android/internal/view/menu/ActionMenuView;->getChildAt(I)Landroid/view/View;

    #@385
    move-result-object v11

    #@386
    .line 337
    .restart local v11       #child:Landroid/view/View;
    invoke-virtual {v11}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@389
    move-result-object v23

    #@38a
    check-cast v23, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;

    #@38c
    .line 339
    .restart local v23       #lp:Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;
    move-object/from16 v0, v23

    #@38e
    iget-boolean v0, v0, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;->expanded:Z

    #@390
    move/from16 v38, v0

    #@392
    if-nez v38, :cond_397

    #@394
    .line 335
    :goto_394
    add-int/lit8 v20, v20, 0x1

    #@396
    goto :goto_37a

    #@397
    .line 341
    :cond_397
    move-object/from16 v0, v23

    #@399
    iget v0, v0, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;->cellsUsed:I

    #@39b
    move/from16 v38, v0

    #@39d
    mul-int v38, v38, v5

    #@39f
    move-object/from16 v0, v23

    #@3a1
    iget v0, v0, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;->extraPixels:I

    #@3a3
    move/from16 v39, v0

    #@3a5
    add-int v35, v38, v39

    #@3a7
    .line 342
    .local v35, width:I
    const/high16 v38, 0x4000

    #@3a9
    move/from16 v0, v35

    #@3ab
    move/from16 v1, v38

    #@3ad
    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@3b0
    move-result v38

    #@3b1
    move/from16 v0, v38

    #@3b3
    move/from16 v1, v22

    #@3b5
    invoke-virtual {v11, v0, v1}, Landroid/view/View;->measure(II)V

    #@3b8
    goto :goto_394

    #@3b9
    .line 347
    .end local v11           #child:Landroid/view/View;
    .end local v23           #lp:Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;
    .end local v35           #width:I
    :cond_3b9
    const/high16 v38, 0x4000

    #@3bb
    move/from16 v0, v17

    #@3bd
    move/from16 v1, v38

    #@3bf
    if-eq v0, v1, :cond_3c3

    #@3c1
    .line 348
    move/from16 v19, v25

    #@3c3
    .line 351
    :cond_3c3
    move-object/from16 v0, p0

    #@3c5
    move/from16 v1, v37

    #@3c7
    move/from16 v2, v19

    #@3c9
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/view/menu/ActionMenuView;->setMeasuredDimension(II)V

    #@3cc
    .line 352
    mul-int v38, v8, v5

    #@3ce
    move/from16 v0, v38

    #@3d0
    move-object/from16 v1, p0

    #@3d2
    iput v0, v1, Lcom/android/internal/view/menu/ActionMenuView;->mMeasuredExtraWidth:I

    #@3d4
    goto/16 :goto_4f
.end method


# virtual methods
.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .registers 3
    .parameter "p"

    #@0
    .prologue
    .line 557
    if-eqz p1, :cond_8

    #@2
    instance-of v0, p1, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 596
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method protected bridge synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .registers 2

    #@0
    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/android/internal/view/menu/ActionMenuView;->generateDefaultLayoutParams()Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method protected bridge synthetic generateDefaultLayoutParams()Landroid/widget/LinearLayout$LayoutParams;
    .registers 2

    #@0
    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/android/internal/view/menu/ActionMenuView;->generateDefaultLayoutParams()Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method protected generateDefaultLayoutParams()Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;
    .registers 3

    #@0
    .prologue
    const/4 v1, -0x2

    #@1
    .line 530
    new-instance v0, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;

    #@3
    invoke-direct {v0, v1, v1}, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;-><init>(II)V

    #@6
    .line 532
    .local v0, params:Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;
    const/16 v1, 0x10

    #@8
    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    #@a
    .line 533
    return-object v0
.end method

.method public bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 34
    invoke-virtual {p0, p1}, Lcom/android/internal/view/menu/ActionMenuView;->generateLayoutParams(Landroid/util/AttributeSet;)Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method protected bridge synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 34
    invoke-virtual {p0, p1}, Lcom/android/internal/view/menu/ActionMenuView;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/LinearLayout$LayoutParams;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 34
    invoke-virtual {p0, p1}, Lcom/android/internal/view/menu/ActionMenuView;->generateLayoutParams(Landroid/util/AttributeSet;)Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method protected bridge synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/widget/LinearLayout$LayoutParams;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 34
    invoke-virtual {p0, p1}, Lcom/android/internal/view/menu/ActionMenuView;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;
    .registers 4
    .parameter "attrs"

    #@0
    .prologue
    .line 538
    new-instance v0, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;

    #@2
    invoke-virtual {p0}, Lcom/android/internal/view/menu/ActionMenuView;->getContext()Landroid/content/Context;

    #@5
    move-result-object v1

    #@6
    invoke-direct {v0, v1, p1}, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@9
    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;
    .registers 4
    .parameter "p"

    #@0
    .prologue
    .line 543
    if-eqz p1, :cond_1c

    #@2
    .line 544
    instance-of v1, p1, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;

    #@4
    if-eqz v1, :cond_16

    #@6
    new-instance v0, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;

    #@8
    check-cast p1, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;

    #@a
    .end local p1
    invoke-direct {v0, p1}, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;-><init>(Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;)V

    #@d
    .line 547
    .local v0, result:Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;
    :goto_d
    iget v1, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    #@f
    if-gtz v1, :cond_15

    #@11
    .line 548
    const/16 v1, 0x10

    #@13
    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    #@15
    .line 552
    .end local v0           #result:Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;
    :cond_15
    :goto_15
    return-object v0

    #@16
    .line 544
    .restart local p1
    :cond_16
    new-instance v0, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;

    #@18
    invoke-direct {v0, p1}, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    #@1b
    goto :goto_d

    #@1c
    .line 552
    :cond_1c
    invoke-virtual {p0}, Lcom/android/internal/view/menu/ActionMenuView;->generateDefaultLayoutParams()Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;

    #@1f
    move-result-object v0

    #@20
    goto :goto_15
.end method

.method public generateOverflowButtonLayoutParams()Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;
    .registers 3

    #@0
    .prologue
    .line 561
    invoke-virtual {p0}, Lcom/android/internal/view/menu/ActionMenuView;->generateDefaultLayoutParams()Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;

    #@3
    move-result-object v0

    #@4
    .line 562
    .local v0, result:Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;
    const/4 v1, 0x1

    #@5
    iput-boolean v1, v0, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;->isOverflowButton:Z

    #@7
    .line 563
    return-object v0
.end method

.method public getWindowAnimations()I
    .registers 2

    #@0
    .prologue
    .line 571
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method protected hasDividerBeforeChildAt(I)Z
    .registers 6
    .parameter "childIndex"

    #@0
    .prologue
    .line 580
    if-nez p1, :cond_4

    #@2
    .line 581
    const/4 v2, 0x0

    #@3
    .line 592
    :cond_3
    :goto_3
    return v2

    #@4
    .line 583
    :cond_4
    add-int/lit8 v3, p1, -0x1

    #@6
    invoke-virtual {p0, v3}, Lcom/android/internal/view/menu/ActionMenuView;->getChildAt(I)Landroid/view/View;

    #@9
    move-result-object v1

    #@a
    .line 584
    .local v1, childBefore:Landroid/view/View;
    invoke-virtual {p0, p1}, Lcom/android/internal/view/menu/ActionMenuView;->getChildAt(I)Landroid/view/View;

    #@d
    move-result-object v0

    #@e
    .line 585
    .local v0, child:Landroid/view/View;
    const/4 v2, 0x0

    #@f
    .line 586
    .local v2, result:Z
    invoke-virtual {p0}, Lcom/android/internal/view/menu/ActionMenuView;->getChildCount()I

    #@12
    move-result v3

    #@13
    if-ge p1, v3, :cond_20

    #@15
    instance-of v3, v1, Lcom/android/internal/view/menu/ActionMenuView$ActionMenuChildView;

    #@17
    if-eqz v3, :cond_20

    #@19
    .line 587
    check-cast v1, Lcom/android/internal/view/menu/ActionMenuView$ActionMenuChildView;

    #@1b
    .end local v1           #childBefore:Landroid/view/View;
    invoke-interface {v1}, Lcom/android/internal/view/menu/ActionMenuView$ActionMenuChildView;->needsDividerAfter()Z

    #@1e
    move-result v3

    #@1f
    or-int/2addr v2, v3

    #@20
    .line 589
    :cond_20
    if-lez p1, :cond_3

    #@22
    instance-of v3, v0, Lcom/android/internal/view/menu/ActionMenuView$ActionMenuChildView;

    #@24
    if-eqz v3, :cond_3

    #@26
    .line 590
    check-cast v0, Lcom/android/internal/view/menu/ActionMenuView$ActionMenuChildView;

    #@28
    .end local v0           #child:Landroid/view/View;
    invoke-interface {v0}, Lcom/android/internal/view/menu/ActionMenuView$ActionMenuChildView;->needsDividerBefore()Z

    #@2b
    move-result v3

    #@2c
    or-int/2addr v2, v3

    #@2d
    goto :goto_3
.end method

.method public initialize(Lcom/android/internal/view/menu/MenuBuilder;)V
    .registers 2
    .parameter "menu"

    #@0
    .prologue
    .line 575
    iput-object p1, p0, Lcom/android/internal/view/menu/ActionMenuView;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@2
    .line 576
    return-void
.end method

.method public invokeItem(Lcom/android/internal/view/menu/MenuItemImpl;)Z
    .registers 4
    .parameter "item"

    #@0
    .prologue
    .line 567
    iget-object v0, p0, Lcom/android/internal/view/menu/ActionMenuView;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-virtual {v0, p1, v1}, Lcom/android/internal/view/menu/MenuBuilder;->performItemAction(Landroid/view/MenuItem;I)Z

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public isExpandedFormat()Z
    .registers 2

    #@0
    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/android/internal/view/menu/ActionMenuView;->mFormatItems:Z

    #@2
    return v0
.end method

.method public isOverflowReserved()Z
    .registers 2

    #@0
    .prologue
    .line 521
    iget-boolean v0, p0, Lcom/android/internal/view/menu/ActionMenuView;->mReserveOverflow:Z

    #@2
    return v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 4
    .parameter "newConfig"

    #@0
    .prologue
    .line 85
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    #@3
    .line 86
    iget-object v0, p0, Lcom/android/internal/view/menu/ActionMenuView;->mPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@5
    const/4 v1, 0x0

    #@6
    invoke-virtual {v0, v1}, Lcom/android/internal/view/menu/ActionMenuPresenter;->updateMenuView(Z)V

    #@9
    .line 88
    iget-object v0, p0, Lcom/android/internal/view/menu/ActionMenuView;->mPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@b
    if-eqz v0, :cond_1f

    #@d
    iget-object v0, p0, Lcom/android/internal/view/menu/ActionMenuView;->mPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@f
    invoke-virtual {v0}, Lcom/android/internal/view/menu/ActionMenuPresenter;->isOverflowMenuShowing()Z

    #@12
    move-result v0

    #@13
    if-eqz v0, :cond_1f

    #@15
    .line 89
    iget-object v0, p0, Lcom/android/internal/view/menu/ActionMenuView;->mPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@17
    invoke-virtual {v0}, Lcom/android/internal/view/menu/ActionMenuPresenter;->hideOverflowMenu()Z

    #@1a
    .line 90
    iget-object v0, p0, Lcom/android/internal/view/menu/ActionMenuView;->mPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@1c
    invoke-virtual {v0}, Lcom/android/internal/view/menu/ActionMenuPresenter;->showOverflowMenu()Z

    #@1f
    .line 93
    :cond_1f
    iget-boolean v0, p0, Lcom/android/internal/view/menu/ActionMenuView;->mAfterMeasured:Z

    #@21
    if-eqz v0, :cond_2b

    #@23
    .line 94
    new-instance v0, Lcom/android/internal/view/menu/ActionMenuView$1;

    #@25
    invoke-direct {v0, p0}, Lcom/android/internal/view/menu/ActionMenuView$1;-><init>(Lcom/android/internal/view/menu/ActionMenuView;)V

    #@28
    invoke-virtual {p0, v0}, Lcom/android/internal/view/menu/ActionMenuView;->post(Ljava/lang/Runnable;)Z

    #@2b
    .line 100
    :cond_2b
    return-void
.end method

.method public onDetachedFromWindow()V
    .registers 2

    #@0
    .prologue
    .line 516
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    #@3
    .line 517
    iget-object v0, p0, Lcom/android/internal/view/menu/ActionMenuView;->mPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@5
    invoke-virtual {v0}, Lcom/android/internal/view/menu/ActionMenuPresenter;->dismissPopupMenus()Z

    #@8
    .line 518
    return-void
.end method

.method protected onLayout(ZIIII)V
    .registers 38
    .parameter "changed"
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    .line 408
    const/16 v30, 0x0

    #@2
    move/from16 v0, v30

    #@4
    move-object/from16 v1, p0

    #@6
    iput-boolean v0, v1, Lcom/android/internal/view/menu/ActionMenuView;->mAfterMeasured:Z

    #@8
    .line 410
    move-object/from16 v0, p0

    #@a
    iget-boolean v0, v0, Lcom/android/internal/view/menu/ActionMenuView;->mFormatItems:Z

    #@c
    move/from16 v30, v0

    #@e
    if-nez v30, :cond_14

    #@10
    .line 411
    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    #@13
    .line 512
    :cond_13
    :goto_13
    return-void

    #@14
    .line 415
    :cond_14
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/view/menu/ActionMenuView;->getChildCount()I

    #@17
    move-result v6

    #@18
    .line 416
    .local v6, childCount:I
    add-int v30, p3, p5

    #@1a
    div-int/lit8 v15, v30, 0x2

    #@1c
    .line 417
    .local v15, midVertical:I
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/view/menu/ActionMenuView;->getDividerWidth()I

    #@1f
    move-result v7

    #@20
    .line 418
    .local v7, dividerWidth:I
    const/16 v18, 0x0

    #@22
    .line 419
    .local v18, overflowWidth:I
    const/16 v17, 0x0

    #@24
    .line 420
    .local v17, nonOverflowWidth:I
    const/16 v16, 0x0

    #@26
    .line 421
    .local v16, nonOverflowCount:I
    sub-int v30, p4, p2

    #@28
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/view/menu/ActionMenuView;->getPaddingRight()I

    #@2b
    move-result v31

    #@2c
    sub-int v30, v30, v31

    #@2e
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/view/menu/ActionMenuView;->getPaddingLeft()I

    #@31
    move-result v31

    #@32
    sub-int v29, v30, v31

    #@34
    .line 422
    .local v29, widthRemaining:I
    const/4 v8, 0x0

    #@35
    .line 423
    .local v8, hasOverflow:Z
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/view/menu/ActionMenuView;->isLayoutRtl()Z

    #@38
    move-result v11

    #@39
    .line 424
    .local v11, isLayoutRtl:Z
    const/4 v10, 0x0

    #@3a
    .local v10, i:I
    :goto_3a
    if-ge v10, v6, :cond_ce

    #@3c
    .line 425
    move-object/from16 v0, p0

    #@3e
    invoke-virtual {v0, v10}, Lcom/android/internal/view/menu/ActionMenuView;->getChildAt(I)Landroid/view/View;

    #@41
    move-result-object v27

    #@42
    .line 426
    .local v27, v:Landroid/view/View;
    invoke-virtual/range {v27 .. v27}, Landroid/view/View;->getVisibility()I

    #@45
    move-result v30

    #@46
    const/16 v31, 0x8

    #@48
    move/from16 v0, v30

    #@4a
    move/from16 v1, v31

    #@4c
    if-ne v0, v1, :cond_51

    #@4e
    .line 424
    :goto_4e
    add-int/lit8 v10, v10, 0x1

    #@50
    goto :goto_3a

    #@51
    .line 430
    :cond_51
    invoke-virtual/range {v27 .. v27}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@54
    move-result-object v19

    #@55
    check-cast v19, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;

    #@57
    .line 431
    .local v19, p:Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;
    move-object/from16 v0, v19

    #@59
    iget-boolean v0, v0, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;->isOverflowButton:Z

    #@5b
    move/from16 v30, v0

    #@5d
    if-eqz v30, :cond_a9

    #@5f
    .line 432
    invoke-virtual/range {v27 .. v27}, Landroid/view/View;->getMeasuredWidth()I

    #@62
    move-result v18

    #@63
    .line 433
    move-object/from16 v0, p0

    #@65
    invoke-virtual {v0, v10}, Lcom/android/internal/view/menu/ActionMenuView;->hasDividerBeforeChildAt(I)Z

    #@68
    move-result v30

    #@69
    if-eqz v30, :cond_6d

    #@6b
    .line 434
    add-int v18, v18, v7

    #@6d
    .line 437
    :cond_6d
    invoke-virtual/range {v27 .. v27}, Landroid/view/View;->getMeasuredHeight()I

    #@70
    move-result v9

    #@71
    .line 440
    .local v9, height:I
    if-eqz v11, :cond_94

    #@73
    .line 441
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/view/menu/ActionMenuView;->getPaddingLeft()I

    #@76
    move-result v30

    #@77
    move-object/from16 v0, v19

    #@79
    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@7b
    move/from16 v31, v0

    #@7d
    add-int v12, v30, v31

    #@7f
    .line 442
    .local v12, l:I
    add-int v20, v12, v18

    #@81
    .line 447
    .local v20, r:I
    :goto_81
    div-int/lit8 v30, v9, 0x2

    #@83
    sub-int v26, v15, v30

    #@85
    .line 448
    .local v26, t:I
    add-int v5, v26, v9

    #@87
    .line 449
    .local v5, b:I
    move-object/from16 v0, v27

    #@89
    move/from16 v1, v26

    #@8b
    move/from16 v2, v20

    #@8d
    invoke-virtual {v0, v12, v1, v2, v5}, Landroid/view/View;->layout(IIII)V

    #@90
    .line 451
    sub-int v29, v29, v18

    #@92
    .line 452
    const/4 v8, 0x1

    #@93
    .line 453
    goto :goto_4e

    #@94
    .line 444
    .end local v5           #b:I
    .end local v12           #l:I
    .end local v20           #r:I
    .end local v26           #t:I
    :cond_94
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/view/menu/ActionMenuView;->getWidth()I

    #@97
    move-result v30

    #@98
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/view/menu/ActionMenuView;->getPaddingRight()I

    #@9b
    move-result v31

    #@9c
    sub-int v30, v30, v31

    #@9e
    move-object/from16 v0, v19

    #@a0
    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@a2
    move/from16 v31, v0

    #@a4
    sub-int v20, v30, v31

    #@a6
    .line 445
    .restart local v20       #r:I
    sub-int v12, v20, v18

    #@a8
    .restart local v12       #l:I
    goto :goto_81

    #@a9
    .line 454
    .end local v9           #height:I
    .end local v12           #l:I
    .end local v20           #r:I
    :cond_a9
    invoke-virtual/range {v27 .. v27}, Landroid/view/View;->getMeasuredWidth()I

    #@ac
    move-result v30

    #@ad
    move-object/from16 v0, v19

    #@af
    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@b1
    move/from16 v31, v0

    #@b3
    add-int v30, v30, v31

    #@b5
    move-object/from16 v0, v19

    #@b7
    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@b9
    move/from16 v31, v0

    #@bb
    add-int v21, v30, v31

    #@bd
    .line 455
    .local v21, size:I
    add-int v17, v17, v21

    #@bf
    .line 456
    sub-int v29, v29, v21

    #@c1
    .line 457
    move-object/from16 v0, p0

    #@c3
    invoke-virtual {v0, v10}, Lcom/android/internal/view/menu/ActionMenuView;->hasDividerBeforeChildAt(I)Z

    #@c6
    move-result v30

    #@c7
    if-eqz v30, :cond_cb

    #@c9
    .line 458
    add-int v17, v17, v7

    #@cb
    .line 460
    :cond_cb
    add-int/lit8 v16, v16, 0x1

    #@cd
    goto :goto_4e

    #@ce
    .line 464
    .end local v19           #p:Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;
    .end local v21           #size:I
    .end local v27           #v:Landroid/view/View;
    :cond_ce
    const/16 v30, 0x1

    #@d0
    move/from16 v0, v30

    #@d2
    if-ne v6, v0, :cond_105

    #@d4
    if-nez v8, :cond_105

    #@d6
    .line 466
    const/16 v30, 0x0

    #@d8
    move-object/from16 v0, p0

    #@da
    move/from16 v1, v30

    #@dc
    invoke-virtual {v0, v1}, Lcom/android/internal/view/menu/ActionMenuView;->getChildAt(I)Landroid/view/View;

    #@df
    move-result-object v27

    #@e0
    .line 467
    .restart local v27       #v:Landroid/view/View;
    invoke-virtual/range {v27 .. v27}, Landroid/view/View;->getMeasuredWidth()I

    #@e3
    move-result v28

    #@e4
    .line 468
    .local v28, width:I
    invoke-virtual/range {v27 .. v27}, Landroid/view/View;->getMeasuredHeight()I

    #@e7
    move-result v9

    #@e8
    .line 469
    .restart local v9       #height:I
    sub-int v30, p4, p2

    #@ea
    div-int/lit8 v14, v30, 0x2

    #@ec
    .line 470
    .local v14, midHorizontal:I
    div-int/lit8 v30, v28, 0x2

    #@ee
    sub-int v12, v14, v30

    #@f0
    .line 471
    .restart local v12       #l:I
    div-int/lit8 v30, v9, 0x2

    #@f2
    sub-int v26, v15, v30

    #@f4
    .line 472
    .restart local v26       #t:I
    add-int v30, v12, v28

    #@f6
    add-int v31, v26, v9

    #@f8
    move-object/from16 v0, v27

    #@fa
    move/from16 v1, v26

    #@fc
    move/from16 v2, v30

    #@fe
    move/from16 v3, v31

    #@100
    invoke-virtual {v0, v12, v1, v2, v3}, Landroid/view/View;->layout(IIII)V

    #@103
    goto/16 :goto_13

    #@105
    .line 476
    .end local v9           #height:I
    .end local v12           #l:I
    .end local v14           #midHorizontal:I
    .end local v26           #t:I
    .end local v27           #v:Landroid/view/View;
    .end local v28           #width:I
    :cond_105
    if-eqz v8, :cond_149

    #@107
    const/16 v30, 0x0

    #@109
    :goto_109
    sub-int v22, v16, v30

    #@10b
    .line 477
    .local v22, spacerCount:I
    const/16 v31, 0x0

    #@10d
    if-lez v22, :cond_14c

    #@10f
    div-int v30, v29, v22

    #@111
    :goto_111
    move/from16 v0, v31

    #@113
    move/from16 v1, v30

    #@115
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@118
    move-result v23

    #@119
    .line 479
    .local v23, spacerSize:I
    if-eqz v11, :cond_17d

    #@11b
    .line 480
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/view/menu/ActionMenuView;->getWidth()I

    #@11e
    move-result v30

    #@11f
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/view/menu/ActionMenuView;->getPaddingRight()I

    #@122
    move-result v31

    #@123
    sub-int v25, v30, v31

    #@125
    .line 481
    .local v25, startRight:I
    const/4 v10, 0x0

    #@126
    :goto_126
    if-ge v10, v6, :cond_13

    #@128
    .line 482
    move-object/from16 v0, p0

    #@12a
    invoke-virtual {v0, v10}, Lcom/android/internal/view/menu/ActionMenuView;->getChildAt(I)Landroid/view/View;

    #@12d
    move-result-object v27

    #@12e
    .line 483
    .restart local v27       #v:Landroid/view/View;
    invoke-virtual/range {v27 .. v27}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@131
    move-result-object v13

    #@132
    check-cast v13, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;

    #@134
    .line 484
    .local v13, lp:Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;
    invoke-virtual/range {v27 .. v27}, Landroid/view/View;->getVisibility()I

    #@137
    move-result v30

    #@138
    const/16 v31, 0x8

    #@13a
    move/from16 v0, v30

    #@13c
    move/from16 v1, v31

    #@13e
    if-eq v0, v1, :cond_146

    #@140
    iget-boolean v0, v13, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;->isOverflowButton:Z

    #@142
    move/from16 v30, v0

    #@144
    if-eqz v30, :cond_14f

    #@146
    .line 481
    :cond_146
    :goto_146
    add-int/lit8 v10, v10, 0x1

    #@148
    goto :goto_126

    #@149
    .line 476
    .end local v13           #lp:Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;
    .end local v22           #spacerCount:I
    .end local v23           #spacerSize:I
    .end local v25           #startRight:I
    .end local v27           #v:Landroid/view/View;
    :cond_149
    const/16 v30, 0x1

    #@14b
    goto :goto_109

    #@14c
    .line 477
    .restart local v22       #spacerCount:I
    :cond_14c
    const/16 v30, 0x0

    #@14e
    goto :goto_111

    #@14f
    .line 488
    .restart local v13       #lp:Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;
    .restart local v23       #spacerSize:I
    .restart local v25       #startRight:I
    .restart local v27       #v:Landroid/view/View;
    :cond_14f
    iget v0, v13, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@151
    move/from16 v30, v0

    #@153
    sub-int v25, v25, v30

    #@155
    .line 489
    invoke-virtual/range {v27 .. v27}, Landroid/view/View;->getMeasuredWidth()I

    #@158
    move-result v28

    #@159
    .line 490
    .restart local v28       #width:I
    invoke-virtual/range {v27 .. v27}, Landroid/view/View;->getMeasuredHeight()I

    #@15c
    move-result v9

    #@15d
    .line 491
    .restart local v9       #height:I
    div-int/lit8 v30, v9, 0x2

    #@15f
    sub-int v26, v15, v30

    #@161
    .line 492
    .restart local v26       #t:I
    sub-int v30, v25, v28

    #@163
    add-int v31, v26, v9

    #@165
    move-object/from16 v0, v27

    #@167
    move/from16 v1, v30

    #@169
    move/from16 v2, v26

    #@16b
    move/from16 v3, v25

    #@16d
    move/from16 v4, v31

    #@16f
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->layout(IIII)V

    #@172
    .line 493
    iget v0, v13, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@174
    move/from16 v30, v0

    #@176
    add-int v30, v30, v28

    #@178
    add-int v30, v30, v23

    #@17a
    sub-int v25, v25, v30

    #@17c
    goto :goto_146

    #@17d
    .line 496
    .end local v9           #height:I
    .end local v13           #lp:Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;
    .end local v25           #startRight:I
    .end local v26           #t:I
    .end local v27           #v:Landroid/view/View;
    .end local v28           #width:I
    :cond_17d
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/view/menu/ActionMenuView;->getPaddingLeft()I

    #@180
    move-result v24

    #@181
    .line 497
    .local v24, startLeft:I
    const/4 v10, 0x0

    #@182
    :goto_182
    if-ge v10, v6, :cond_13

    #@184
    .line 498
    move-object/from16 v0, p0

    #@186
    invoke-virtual {v0, v10}, Lcom/android/internal/view/menu/ActionMenuView;->getChildAt(I)Landroid/view/View;

    #@189
    move-result-object v27

    #@18a
    .line 499
    .restart local v27       #v:Landroid/view/View;
    invoke-virtual/range {v27 .. v27}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@18d
    move-result-object v13

    #@18e
    check-cast v13, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;

    #@190
    .line 500
    .restart local v13       #lp:Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;
    invoke-virtual/range {v27 .. v27}, Landroid/view/View;->getVisibility()I

    #@193
    move-result v30

    #@194
    const/16 v31, 0x8

    #@196
    move/from16 v0, v30

    #@198
    move/from16 v1, v31

    #@19a
    if-eq v0, v1, :cond_1a2

    #@19c
    iget-boolean v0, v13, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;->isOverflowButton:Z

    #@19e
    move/from16 v30, v0

    #@1a0
    if-eqz v30, :cond_1a5

    #@1a2
    .line 497
    :cond_1a2
    :goto_1a2
    add-int/lit8 v10, v10, 0x1

    #@1a4
    goto :goto_182

    #@1a5
    .line 504
    :cond_1a5
    iget v0, v13, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@1a7
    move/from16 v30, v0

    #@1a9
    add-int v24, v24, v30

    #@1ab
    .line 505
    invoke-virtual/range {v27 .. v27}, Landroid/view/View;->getMeasuredWidth()I

    #@1ae
    move-result v28

    #@1af
    .line 506
    .restart local v28       #width:I
    invoke-virtual/range {v27 .. v27}, Landroid/view/View;->getMeasuredHeight()I

    #@1b2
    move-result v9

    #@1b3
    .line 507
    .restart local v9       #height:I
    div-int/lit8 v30, v9, 0x2

    #@1b5
    sub-int v26, v15, v30

    #@1b7
    .line 508
    .restart local v26       #t:I
    add-int v30, v24, v28

    #@1b9
    add-int v31, v26, v9

    #@1bb
    move-object/from16 v0, v27

    #@1bd
    move/from16 v1, v24

    #@1bf
    move/from16 v2, v26

    #@1c1
    move/from16 v3, v30

    #@1c3
    move/from16 v4, v31

    #@1c5
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->layout(IIII)V

    #@1c8
    .line 509
    iget v0, v13, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@1ca
    move/from16 v30, v0

    #@1cc
    add-int v30, v30, v28

    #@1ce
    add-int v30, v30, v23

    #@1d0
    add-int v24, v24, v30

    #@1d2
    goto :goto_1a2
.end method

.method protected onMeasure(II)V
    .registers 13
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v8, 0x0

    #@2
    .line 104
    iput-boolean v7, p0, Lcom/android/internal/view/menu/ActionMenuView;->mAfterMeasured:Z

    #@4
    .line 107
    iget-boolean v4, p0, Lcom/android/internal/view/menu/ActionMenuView;->mFormatItems:Z

    #@6
    .line 108
    .local v4, wasFormatted:Z
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@9
    move-result v6

    #@a
    const/high16 v9, 0x4000

    #@c
    if-ne v6, v9, :cond_36

    #@e
    move v6, v7

    #@f
    :goto_f
    iput-boolean v6, p0, Lcom/android/internal/view/menu/ActionMenuView;->mFormatItems:Z

    #@11
    .line 110
    iget-boolean v6, p0, Lcom/android/internal/view/menu/ActionMenuView;->mFormatItems:Z

    #@13
    if-eq v4, v6, :cond_17

    #@15
    .line 111
    iput v8, p0, Lcom/android/internal/view/menu/ActionMenuView;->mFormatItemsWidth:I

    #@17
    .line 116
    :cond_17
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@1a
    move-result v5

    #@1b
    .line 117
    .local v5, widthSize:I
    iget-boolean v6, p0, Lcom/android/internal/view/menu/ActionMenuView;->mFormatItems:Z

    #@1d
    if-eqz v6, :cond_2e

    #@1f
    iget-object v6, p0, Lcom/android/internal/view/menu/ActionMenuView;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@21
    if-eqz v6, :cond_2e

    #@23
    iget v6, p0, Lcom/android/internal/view/menu/ActionMenuView;->mFormatItemsWidth:I

    #@25
    if-eq v5, v6, :cond_2e

    #@27
    .line 118
    iput v5, p0, Lcom/android/internal/view/menu/ActionMenuView;->mFormatItemsWidth:I

    #@29
    .line 119
    iget-object v6, p0, Lcom/android/internal/view/menu/ActionMenuView;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@2b
    invoke-virtual {v6, v7}, Lcom/android/internal/view/menu/MenuBuilder;->onItemsChanged(Z)V

    #@2e
    .line 122
    :cond_2e
    iget-boolean v6, p0, Lcom/android/internal/view/menu/ActionMenuView;->mFormatItems:Z

    #@30
    if-eqz v6, :cond_38

    #@32
    .line 123
    invoke-direct {p0, p1, p2}, Lcom/android/internal/view/menu/ActionMenuView;->onMeasureExactFormat(II)V

    #@35
    .line 134
    :goto_35
    return-void

    #@36
    .end local v5           #widthSize:I
    :cond_36
    move v6, v8

    #@37
    .line 108
    goto :goto_f

    #@38
    .line 126
    .restart local v5       #widthSize:I
    :cond_38
    invoke-virtual {p0}, Lcom/android/internal/view/menu/ActionMenuView;->getChildCount()I

    #@3b
    move-result v1

    #@3c
    .line 127
    .local v1, childCount:I
    const/4 v2, 0x0

    #@3d
    .local v2, i:I
    :goto_3d
    if-ge v2, v1, :cond_50

    #@3f
    .line 128
    invoke-virtual {p0, v2}, Lcom/android/internal/view/menu/ActionMenuView;->getChildAt(I)Landroid/view/View;

    #@42
    move-result-object v0

    #@43
    .line 129
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@46
    move-result-object v3

    #@47
    check-cast v3, Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;

    #@49
    .line 130
    .local v3, lp:Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;
    iput v8, v3, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@4b
    iput v8, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@4d
    .line 127
    add-int/lit8 v2, v2, 0x1

    #@4f
    goto :goto_3d

    #@50
    .line 132
    .end local v0           #child:Landroid/view/View;
    .end local v3           #lp:Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;
    :cond_50
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    #@53
    goto :goto_35
.end method

.method public setMaxItemHeight(I)V
    .registers 2
    .parameter "maxItemHeight"

    #@0
    .prologue
    .line 79
    iput p1, p0, Lcom/android/internal/view/menu/ActionMenuView;->mMaxItemHeight:I

    #@2
    .line 80
    invoke-virtual {p0}, Lcom/android/internal/view/menu/ActionMenuView;->requestLayout()V

    #@5
    .line 81
    return-void
.end method

.method public setOverflowReserved(Z)V
    .registers 2
    .parameter "reserveOverflow"

    #@0
    .prologue
    .line 525
    iput-boolean p1, p0, Lcom/android/internal/view/menu/ActionMenuView;->mReserveOverflow:Z

    #@2
    .line 526
    return-void
.end method

.method public setPresenter(Lcom/android/internal/view/menu/ActionMenuPresenter;)V
    .registers 2
    .parameter "presenter"

    #@0
    .prologue
    .line 71
    iput-object p1, p0, Lcom/android/internal/view/menu/ActionMenuView;->mPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@2
    .line 72
    return-void
.end method
