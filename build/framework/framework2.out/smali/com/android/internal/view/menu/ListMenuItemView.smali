.class public Lcom/android/internal/view/menu/ListMenuItemView;
.super Landroid/widget/LinearLayout;
.source "ListMenuItemView.java"

# interfaces
.implements Lcom/android/internal/view/menu/MenuView$ItemView;


# static fields
.field private static final TAG:Ljava/lang/String; = "ListMenuItemView"


# instance fields
.field private mBackground:Landroid/graphics/drawable/Drawable;

.field private mCheckBox:Landroid/widget/CheckBox;

.field private mForceShowIcon:Z

.field private mIconView:Landroid/widget/ImageView;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mItemData:Lcom/android/internal/view/menu/MenuItemImpl;

.field private mMenuType:I

.field private mPreserveIconSpacing:Z

.field private mRadioButton:Landroid/widget/RadioButton;

.field private mShortcutView:Landroid/widget/TextView;

.field private mTextAppearance:I

.field private mTextAppearanceContext:Landroid/content/Context;

.field private mTitleView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 75
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Lcom/android/internal/view/menu/ListMenuItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 76
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 8
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 58
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 60
    sget-object v1, Lcom/android/internal/R$styleable;->MenuView:[I

    #@6
    invoke-virtual {p1, p2, v1, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@9
    move-result-object v0

    #@a
    .line 64
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v1, 0x5

    #@b
    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@e
    move-result-object v1

    #@f
    iput-object v1, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mBackground:Landroid/graphics/drawable/Drawable;

    #@11
    .line 65
    const/4 v1, 0x1

    #@12
    const/4 v2, -0x1

    #@13
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@16
    move-result v1

    #@17
    iput v1, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mTextAppearance:I

    #@19
    .line 67
    const/4 v1, 0x7

    #@1a
    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@1d
    move-result v1

    #@1e
    iput-boolean v1, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mPreserveIconSpacing:Z

    #@20
    .line 69
    iput-object p1, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mTextAppearanceContext:Landroid/content/Context;

    #@22
    .line 71
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@25
    .line 72
    return-void
.end method

.method private getInflater()Landroid/view/LayoutInflater;
    .registers 2

    #@0
    .prologue
    .line 267
    iget-object v0, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mInflater:Landroid/view/LayoutInflater;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 268
    iget-object v0, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mContext:Landroid/content/Context;

    #@6
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mInflater:Landroid/view/LayoutInflater;

    #@c
    .line 270
    :cond_c
    iget-object v0, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mInflater:Landroid/view/LayoutInflater;

    #@e
    return-object v0
.end method

.method private insertCheckBox()V
    .registers 4

    #@0
    .prologue
    .line 251
    invoke-direct {p0}, Lcom/android/internal/view/menu/ListMenuItemView;->getInflater()Landroid/view/LayoutInflater;

    #@3
    move-result-object v0

    #@4
    .line 252
    .local v0, inflater:Landroid/view/LayoutInflater;
    const v1, 0x109007e

    #@7
    const/4 v2, 0x0

    #@8
    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@b
    move-result-object v1

    #@c
    check-cast v1, Landroid/widget/CheckBox;

    #@e
    iput-object v1, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mCheckBox:Landroid/widget/CheckBox;

    #@10
    .line 255
    iget-object v1, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mCheckBox:Landroid/widget/CheckBox;

    #@12
    invoke-virtual {p0, v1}, Lcom/android/internal/view/menu/ListMenuItemView;->addView(Landroid/view/View;)V

    #@15
    .line 256
    return-void
.end method

.method private insertIconView()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 236
    invoke-direct {p0}, Lcom/android/internal/view/menu/ListMenuItemView;->getInflater()Landroid/view/LayoutInflater;

    #@4
    move-result-object v0

    #@5
    .line 237
    .local v0, inflater:Landroid/view/LayoutInflater;
    const v1, 0x109007f

    #@8
    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@b
    move-result-object v1

    #@c
    check-cast v1, Landroid/widget/ImageView;

    #@e
    iput-object v1, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mIconView:Landroid/widget/ImageView;

    #@10
    .line 239
    iget-object v1, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mIconView:Landroid/widget/ImageView;

    #@12
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/view/menu/ListMenuItemView;->addView(Landroid/view/View;I)V

    #@15
    .line 240
    return-void
.end method

.method private insertRadioButton()V
    .registers 4

    #@0
    .prologue
    .line 243
    invoke-direct {p0}, Lcom/android/internal/view/menu/ListMenuItemView;->getInflater()Landroid/view/LayoutInflater;

    #@3
    move-result-object v0

    #@4
    .line 244
    .local v0, inflater:Landroid/view/LayoutInflater;
    const v1, 0x1090081

    #@7
    const/4 v2, 0x0

    #@8
    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@b
    move-result-object v1

    #@c
    check-cast v1, Landroid/widget/RadioButton;

    #@e
    iput-object v1, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mRadioButton:Landroid/widget/RadioButton;

    #@10
    .line 247
    iget-object v1, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mRadioButton:Landroid/widget/RadioButton;

    #@12
    invoke-virtual {p0, v1}, Lcom/android/internal/view/menu/ListMenuItemView;->addView(Landroid/view/View;)V

    #@15
    .line 248
    return-void
.end method


# virtual methods
.method public getItemData()Lcom/android/internal/view/menu/MenuItemImpl;
    .registers 2

    #@0
    .prologue
    .line 121
    iget-object v0, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mItemData:Lcom/android/internal/view/menu/MenuItemImpl;

    #@2
    return-object v0
.end method

.method public initialize(Lcom/android/internal/view/menu/MenuItemImpl;I)V
    .registers 5
    .parameter "itemData"
    .parameter "menuType"

    #@0
    .prologue
    .line 94
    iput-object p1, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mItemData:Lcom/android/internal/view/menu/MenuItemImpl;

    #@2
    .line 95
    iput p2, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mMenuType:I

    #@4
    .line 97
    invoke-virtual {p1}, Lcom/android/internal/view/menu/MenuItemImpl;->isVisible()Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_36

    #@a
    const/4 v0, 0x0

    #@b
    :goto_b
    invoke-virtual {p0, v0}, Lcom/android/internal/view/menu/ListMenuItemView;->setVisibility(I)V

    #@e
    .line 99
    invoke-virtual {p1, p0}, Lcom/android/internal/view/menu/MenuItemImpl;->getTitleForItemView(Lcom/android/internal/view/menu/MenuView$ItemView;)Ljava/lang/CharSequence;

    #@11
    move-result-object v0

    #@12
    invoke-virtual {p0, v0}, Lcom/android/internal/view/menu/ListMenuItemView;->setTitle(Ljava/lang/CharSequence;)V

    #@15
    .line 100
    invoke-virtual {p1}, Lcom/android/internal/view/menu/MenuItemImpl;->isCheckable()Z

    #@18
    move-result v0

    #@19
    invoke-virtual {p0, v0}, Lcom/android/internal/view/menu/ListMenuItemView;->setCheckable(Z)V

    #@1c
    .line 101
    invoke-virtual {p1}, Lcom/android/internal/view/menu/MenuItemImpl;->shouldShowShortcut()Z

    #@1f
    move-result v0

    #@20
    invoke-virtual {p1}, Lcom/android/internal/view/menu/MenuItemImpl;->getShortcut()C

    #@23
    move-result v1

    #@24
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/view/menu/ListMenuItemView;->setShortcut(ZC)V

    #@27
    .line 102
    invoke-virtual {p1}, Lcom/android/internal/view/menu/MenuItemImpl;->getIcon()Landroid/graphics/drawable/Drawable;

    #@2a
    move-result-object v0

    #@2b
    invoke-virtual {p0, v0}, Lcom/android/internal/view/menu/ListMenuItemView;->setIcon(Landroid/graphics/drawable/Drawable;)V

    #@2e
    .line 103
    invoke-virtual {p1}, Lcom/android/internal/view/menu/MenuItemImpl;->isEnabled()Z

    #@31
    move-result v0

    #@32
    invoke-virtual {p0, v0}, Lcom/android/internal/view/menu/ListMenuItemView;->setEnabled(Z)V

    #@35
    .line 104
    return-void

    #@36
    .line 97
    :cond_36
    const/16 v0, 0x8

    #@38
    goto :goto_b
.end method

.method protected onFinishInflate()V
    .registers 4

    #@0
    .prologue
    .line 80
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    #@3
    .line 82
    iget-object v0, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mBackground:Landroid/graphics/drawable/Drawable;

    #@5
    invoke-virtual {p0, v0}, Lcom/android/internal/view/menu/ListMenuItemView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    #@8
    .line 84
    const v0, 0x1020016

    #@b
    invoke-virtual {p0, v0}, Lcom/android/internal/view/menu/ListMenuItemView;->findViewById(I)Landroid/view/View;

    #@e
    move-result-object v0

    #@f
    check-cast v0, Landroid/widget/TextView;

    #@11
    iput-object v0, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mTitleView:Landroid/widget/TextView;

    #@13
    .line 85
    iget v0, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mTextAppearance:I

    #@15
    const/4 v1, -0x1

    #@16
    if-eq v0, v1, :cond_21

    #@18
    .line 86
    iget-object v0, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mTitleView:Landroid/widget/TextView;

    #@1a
    iget-object v1, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mTextAppearanceContext:Landroid/content/Context;

    #@1c
    iget v2, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mTextAppearance:I

    #@1e
    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    #@21
    .line 90
    :cond_21
    const v0, 0x1020321

    #@24
    invoke-virtual {p0, v0}, Lcom/android/internal/view/menu/ListMenuItemView;->findViewById(I)Landroid/view/View;

    #@27
    move-result-object v0

    #@28
    check-cast v0, Landroid/widget/TextView;

    #@2a
    iput-object v0, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mShortcutView:Landroid/widget/TextView;

    #@2c
    .line 91
    return-void
.end method

.method protected onMeasure(II)V
    .registers 6
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    .line 224
    iget-object v2, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mIconView:Landroid/widget/ImageView;

    #@2
    if-eqz v2, :cond_20

    #@4
    iget-boolean v2, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mPreserveIconSpacing:Z

    #@6
    if-eqz v2, :cond_20

    #@8
    .line 226
    invoke-virtual {p0}, Lcom/android/internal/view/menu/ListMenuItemView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@b
    move-result-object v1

    #@c
    .line 227
    .local v1, lp:Landroid/view/ViewGroup$LayoutParams;
    iget-object v2, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mIconView:Landroid/widget/ImageView;

    #@e
    invoke-virtual {v2}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@11
    move-result-object v0

    #@12
    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    #@14
    .line 228
    .local v0, iconLp:Landroid/widget/LinearLayout$LayoutParams;
    iget v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@16
    if-lez v2, :cond_20

    #@18
    iget v2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@1a
    if-gtz v2, :cond_20

    #@1c
    .line 229
    iget v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@1e
    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@20
    .line 232
    .end local v0           #iconLp:Landroid/widget/LinearLayout$LayoutParams;
    .end local v1           #lp:Landroid/view/ViewGroup$LayoutParams;
    :cond_20
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    #@23
    .line 233
    return-void
.end method

.method public prefersCondensedTitle()Z
    .registers 2

    #@0
    .prologue
    .line 259
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public setCheckable(Z)V
    .registers 7
    .parameter "checkable"

    #@0
    .prologue
    const/16 v3, 0x8

    #@2
    .line 125
    if-nez p1, :cond_d

    #@4
    iget-object v4, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mRadioButton:Landroid/widget/RadioButton;

    #@6
    if-nez v4, :cond_d

    #@8
    iget-object v4, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mCheckBox:Landroid/widget/CheckBox;

    #@a
    if-nez v4, :cond_d

    #@c
    .line 164
    :cond_c
    :goto_c
    return-void

    #@d
    .line 134
    :cond_d
    iget-object v4, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mItemData:Lcom/android/internal/view/menu/MenuItemImpl;

    #@f
    invoke-virtual {v4}, Lcom/android/internal/view/menu/MenuItemImpl;->isExclusiveCheckable()Z

    #@12
    move-result v4

    #@13
    if-eqz v4, :cond_43

    #@15
    .line 135
    iget-object v4, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mRadioButton:Landroid/widget/RadioButton;

    #@17
    if-nez v4, :cond_1c

    #@19
    .line 136
    invoke-direct {p0}, Lcom/android/internal/view/menu/ListMenuItemView;->insertRadioButton()V

    #@1c
    .line 138
    :cond_1c
    iget-object v0, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mRadioButton:Landroid/widget/RadioButton;

    #@1e
    .line 139
    .local v0, compoundButton:Landroid/widget/CompoundButton;
    iget-object v2, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mCheckBox:Landroid/widget/CheckBox;

    #@20
    .line 148
    .local v2, otherCompoundButton:Landroid/widget/CompoundButton;
    :goto_20
    if-eqz p1, :cond_51

    #@22
    .line 149
    iget-object v4, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mItemData:Lcom/android/internal/view/menu/MenuItemImpl;

    #@24
    invoke-virtual {v4}, Lcom/android/internal/view/menu/MenuItemImpl;->isChecked()Z

    #@27
    move-result v4

    #@28
    invoke-virtual {v0, v4}, Landroid/widget/CompoundButton;->setChecked(Z)V

    #@2b
    .line 151
    if-eqz p1, :cond_4f

    #@2d
    const/4 v1, 0x0

    #@2e
    .line 152
    .local v1, newVisibility:I
    :goto_2e
    invoke-virtual {v0}, Landroid/widget/CompoundButton;->getVisibility()I

    #@31
    move-result v4

    #@32
    if-eq v4, v1, :cond_37

    #@34
    .line 153
    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setVisibility(I)V

    #@37
    .line 157
    :cond_37
    if-eqz v2, :cond_c

    #@39
    invoke-virtual {v2}, Landroid/widget/CompoundButton;->getVisibility()I

    #@3c
    move-result v4

    #@3d
    if-eq v4, v3, :cond_c

    #@3f
    .line 158
    invoke-virtual {v2, v3}, Landroid/widget/CompoundButton;->setVisibility(I)V

    #@42
    goto :goto_c

    #@43
    .line 141
    .end local v0           #compoundButton:Landroid/widget/CompoundButton;
    .end local v1           #newVisibility:I
    .end local v2           #otherCompoundButton:Landroid/widget/CompoundButton;
    :cond_43
    iget-object v4, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mCheckBox:Landroid/widget/CheckBox;

    #@45
    if-nez v4, :cond_4a

    #@47
    .line 142
    invoke-direct {p0}, Lcom/android/internal/view/menu/ListMenuItemView;->insertCheckBox()V

    #@4a
    .line 144
    :cond_4a
    iget-object v0, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mCheckBox:Landroid/widget/CheckBox;

    #@4c
    .line 145
    .restart local v0       #compoundButton:Landroid/widget/CompoundButton;
    iget-object v2, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mRadioButton:Landroid/widget/RadioButton;

    #@4e
    .restart local v2       #otherCompoundButton:Landroid/widget/CompoundButton;
    goto :goto_20

    #@4f
    :cond_4f
    move v1, v3

    #@50
    .line 151
    goto :goto_2e

    #@51
    .line 161
    :cond_51
    iget-object v4, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mCheckBox:Landroid/widget/CheckBox;

    #@53
    if-eqz v4, :cond_5a

    #@55
    iget-object v4, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mCheckBox:Landroid/widget/CheckBox;

    #@57
    invoke-virtual {v4, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    #@5a
    .line 162
    :cond_5a
    iget-object v4, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mRadioButton:Landroid/widget/RadioButton;

    #@5c
    if-eqz v4, :cond_c

    #@5e
    iget-object v4, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mRadioButton:Landroid/widget/RadioButton;

    #@60
    invoke-virtual {v4, v3}, Landroid/widget/RadioButton;->setVisibility(I)V

    #@63
    goto :goto_c
.end method

.method public setChecked(Z)V
    .registers 4
    .parameter "checked"

    #@0
    .prologue
    .line 169
    iget-object v1, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mItemData:Lcom/android/internal/view/menu/MenuItemImpl;

    #@2
    invoke-virtual {v1}, Lcom/android/internal/view/menu/MenuItemImpl;->isExclusiveCheckable()Z

    #@5
    move-result v1

    #@6
    if-eqz v1, :cond_15

    #@8
    .line 170
    iget-object v1, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mRadioButton:Landroid/widget/RadioButton;

    #@a
    if-nez v1, :cond_f

    #@c
    .line 171
    invoke-direct {p0}, Lcom/android/internal/view/menu/ListMenuItemView;->insertRadioButton()V

    #@f
    .line 173
    :cond_f
    iget-object v0, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mRadioButton:Landroid/widget/RadioButton;

    #@11
    .line 181
    .local v0, compoundButton:Landroid/widget/CompoundButton;
    :goto_11
    invoke-virtual {v0, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    #@14
    .line 182
    return-void

    #@15
    .line 175
    .end local v0           #compoundButton:Landroid/widget/CompoundButton;
    :cond_15
    iget-object v1, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mCheckBox:Landroid/widget/CheckBox;

    #@17
    if-nez v1, :cond_1c

    #@19
    .line 176
    invoke-direct {p0}, Lcom/android/internal/view/menu/ListMenuItemView;->insertCheckBox()V

    #@1c
    .line 178
    :cond_1c
    iget-object v0, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mCheckBox:Landroid/widget/CheckBox;

    #@1e
    .restart local v0       #compoundButton:Landroid/widget/CompoundButton;
    goto :goto_11
.end method

.method public setForceShowIcon(Z)V
    .registers 2
    .parameter "forceShow"

    #@0
    .prologue
    .line 107
    iput-boolean p1, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mForceShowIcon:Z

    #@2
    iput-boolean p1, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mPreserveIconSpacing:Z

    #@4
    .line 108
    return-void
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)V
    .registers 5
    .parameter "icon"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 198
    iget-object v2, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mItemData:Lcom/android/internal/view/menu/MenuItemImpl;

    #@3
    invoke-virtual {v2}, Lcom/android/internal/view/menu/MenuItemImpl;->shouldShowIcon()Z

    #@6
    move-result v2

    #@7
    if-nez v2, :cond_d

    #@9
    iget-boolean v2, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mForceShowIcon:Z

    #@b
    if-eqz v2, :cond_15

    #@d
    :cond_d
    const/4 v0, 0x1

    #@e
    .line 199
    .local v0, showIcon:Z
    :goto_e
    if-nez v0, :cond_17

    #@10
    iget-boolean v2, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mPreserveIconSpacing:Z

    #@12
    if-nez v2, :cond_17

    #@14
    .line 220
    .end local p1
    :cond_14
    :goto_14
    return-void

    #@15
    .end local v0           #showIcon:Z
    .restart local p1
    :cond_15
    move v0, v1

    #@16
    .line 198
    goto :goto_e

    #@17
    .line 203
    .restart local v0       #showIcon:Z
    :cond_17
    iget-object v2, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mIconView:Landroid/widget/ImageView;

    #@19
    if-nez v2, :cond_21

    #@1b
    if-nez p1, :cond_21

    #@1d
    iget-boolean v2, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mPreserveIconSpacing:Z

    #@1f
    if-eqz v2, :cond_14

    #@21
    .line 207
    :cond_21
    iget-object v2, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mIconView:Landroid/widget/ImageView;

    #@23
    if-nez v2, :cond_28

    #@25
    .line 208
    invoke-direct {p0}, Lcom/android/internal/view/menu/ListMenuItemView;->insertIconView()V

    #@28
    .line 211
    :cond_28
    if-nez p1, :cond_2e

    #@2a
    iget-boolean v2, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mPreserveIconSpacing:Z

    #@2c
    if-eqz v2, :cond_45

    #@2e
    .line 212
    :cond_2e
    iget-object v2, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mIconView:Landroid/widget/ImageView;

    #@30
    if-eqz v0, :cond_43

    #@32
    .end local p1
    :goto_32
    invoke-virtual {v2, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    #@35
    .line 214
    iget-object v2, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mIconView:Landroid/widget/ImageView;

    #@37
    invoke-virtual {v2}, Landroid/widget/ImageView;->getVisibility()I

    #@3a
    move-result v2

    #@3b
    if-eqz v2, :cond_14

    #@3d
    .line 215
    iget-object v2, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mIconView:Landroid/widget/ImageView;

    #@3f
    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    #@42
    goto :goto_14

    #@43
    .line 212
    .restart local p1
    :cond_43
    const/4 p1, 0x0

    #@44
    goto :goto_32

    #@45
    .line 218
    :cond_45
    iget-object v1, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mIconView:Landroid/widget/ImageView;

    #@47
    const/16 v2, 0x8

    #@49
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    #@4c
    goto :goto_14
.end method

.method public setShortcut(ZC)V
    .registers 6
    .parameter "showShortcut"
    .parameter "shortcutKey"

    #@0
    .prologue
    .line 185
    if-eqz p1, :cond_26

    #@2
    iget-object v1, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mItemData:Lcom/android/internal/view/menu/MenuItemImpl;

    #@4
    invoke-virtual {v1}, Lcom/android/internal/view/menu/MenuItemImpl;->shouldShowShortcut()Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_26

    #@a
    const/4 v0, 0x0

    #@b
    .line 188
    .local v0, newVisibility:I
    :goto_b
    if-nez v0, :cond_18

    #@d
    .line 189
    iget-object v1, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mShortcutView:Landroid/widget/TextView;

    #@f
    iget-object v2, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mItemData:Lcom/android/internal/view/menu/MenuItemImpl;

    #@11
    invoke-virtual {v2}, Lcom/android/internal/view/menu/MenuItemImpl;->getShortcutLabel()Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@18
    .line 192
    :cond_18
    iget-object v1, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mShortcutView:Landroid/widget/TextView;

    #@1a
    invoke-virtual {v1}, Landroid/widget/TextView;->getVisibility()I

    #@1d
    move-result v1

    #@1e
    if-eq v1, v0, :cond_25

    #@20
    .line 193
    iget-object v1, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mShortcutView:Landroid/widget/TextView;

    #@22
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    #@25
    .line 195
    :cond_25
    return-void

    #@26
    .line 185
    .end local v0           #newVisibility:I
    :cond_26
    const/16 v0, 0x8

    #@28
    goto :goto_b
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .registers 4
    .parameter "title"

    #@0
    .prologue
    const/16 v1, 0x8

    #@2
    .line 111
    if-eqz p1, :cond_18

    #@4
    .line 112
    iget-object v0, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mTitleView:Landroid/widget/TextView;

    #@6
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@9
    .line 114
    iget-object v0, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mTitleView:Landroid/widget/TextView;

    #@b
    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    #@e
    move-result v0

    #@f
    if-eqz v0, :cond_17

    #@11
    iget-object v0, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mTitleView:Landroid/widget/TextView;

    #@13
    const/4 v1, 0x0

    #@14
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    #@17
    .line 118
    :cond_17
    :goto_17
    return-void

    #@18
    .line 116
    :cond_18
    iget-object v0, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mTitleView:Landroid/widget/TextView;

    #@1a
    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    #@1d
    move-result v0

    #@1e
    if-eq v0, v1, :cond_17

    #@20
    iget-object v0, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mTitleView:Landroid/widget/TextView;

    #@22
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    #@25
    goto :goto_17
.end method

.method public showsIcon()Z
    .registers 2

    #@0
    .prologue
    .line 263
    iget-boolean v0, p0, Lcom/android/internal/view/menu/ListMenuItemView;->mForceShowIcon:Z

    #@2
    return v0
.end method
