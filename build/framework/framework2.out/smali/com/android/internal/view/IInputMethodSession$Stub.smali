.class public abstract Lcom/android/internal/view/IInputMethodSession$Stub;
.super Landroid/os/Binder;
.source "IInputMethodSession.java"

# interfaces
.implements Lcom/android/internal/view/IInputMethodSession;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/view/IInputMethodSession;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/view/IInputMethodSession$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.android.internal.view.IInputMethodSession"

.field static final TRANSACTION_appPrivateCommand:I = 0xa

.field static final TRANSACTION_dispatchGenericMotionEvent:I = 0x9

.field static final TRANSACTION_dispatchKeyEvent:I = 0x7

.field static final TRANSACTION_dispatchTrackballEvent:I = 0x8

.field static final TRANSACTION_displayCompletions:I = 0x6

.field static final TRANSACTION_finishInput:I = 0x1

.field static final TRANSACTION_finishSession:I = 0xc

.field static final TRANSACTION_toggleSoftInput:I = 0xb

.field static final TRANSACTION_updateCursor:I = 0x5

.field static final TRANSACTION_updateExtractedText:I = 0x2

.field static final TRANSACTION_updateSelection:I = 0x3

.field static final TRANSACTION_viewClicked:I = 0x4


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 18
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 19
    const-string v0, "com.android.internal.view.IInputMethodSession"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/android/internal/view/IInputMethodSession$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 20
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/android/internal/view/IInputMethodSession;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 27
    if-nez p0, :cond_4

    #@2
    .line 28
    const/4 v0, 0x0

    #@3
    .line 34
    :goto_3
    return-object v0

    #@4
    .line 30
    :cond_4
    const-string v1, "com.android.internal.view.IInputMethodSession"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 31
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/android/internal/view/IInputMethodSession;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 32
    check-cast v0, Lcom/android/internal/view/IInputMethodSession;

    #@12
    goto :goto_3

    #@13
    .line 34
    :cond_13
    new-instance v0, Lcom/android/internal/view/IInputMethodSession$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/android/internal/view/IInputMethodSession$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 38
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 13
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    .line 42
    sparse-switch p1, :sswitch_data_140

    #@4
    .line 200
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v7

    #@8
    :goto_8
    return v7

    #@9
    .line 46
    :sswitch_9
    const-string v0, "com.android.internal.view.IInputMethodSession"

    #@b
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    goto :goto_8

    #@f
    .line 51
    :sswitch_f
    const-string v0, "com.android.internal.view.IInputMethodSession"

    #@11
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 52
    invoke-virtual {p0}, Lcom/android/internal/view/IInputMethodSession$Stub;->finishInput()V

    #@17
    goto :goto_8

    #@18
    .line 57
    :sswitch_18
    const-string v0, "com.android.internal.view.IInputMethodSession"

    #@1a
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1d
    .line 59
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@20
    move-result v1

    #@21
    .line 61
    .local v1, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@24
    move-result v0

    #@25
    if-eqz v0, :cond_33

    #@27
    .line 62
    sget-object v0, Landroid/view/inputmethod/ExtractedText;->CREATOR:Landroid/os/Parcelable$Creator;

    #@29
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@2c
    move-result-object v2

    #@2d
    check-cast v2, Landroid/view/inputmethod/ExtractedText;

    #@2f
    .line 67
    .local v2, _arg1:Landroid/view/inputmethod/ExtractedText;
    :goto_2f
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/view/IInputMethodSession$Stub;->updateExtractedText(ILandroid/view/inputmethod/ExtractedText;)V

    #@32
    goto :goto_8

    #@33
    .line 65
    .end local v2           #_arg1:Landroid/view/inputmethod/ExtractedText;
    :cond_33
    const/4 v2, 0x0

    #@34
    .restart local v2       #_arg1:Landroid/view/inputmethod/ExtractedText;
    goto :goto_2f

    #@35
    .line 72
    .end local v1           #_arg0:I
    .end local v2           #_arg1:Landroid/view/inputmethod/ExtractedText;
    :sswitch_35
    const-string v0, "com.android.internal.view.IInputMethodSession"

    #@37
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3a
    .line 74
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@3d
    move-result v1

    #@3e
    .line 76
    .restart local v1       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@41
    move-result v2

    #@42
    .line 78
    .local v2, _arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@45
    move-result v3

    #@46
    .line 80
    .local v3, _arg2:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@49
    move-result v4

    #@4a
    .line 82
    .local v4, _arg3:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@4d
    move-result v5

    #@4e
    .line 84
    .local v5, _arg4:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@51
    move-result v6

    #@52
    .local v6, _arg5:I
    move-object v0, p0

    #@53
    .line 85
    invoke-virtual/range {v0 .. v6}, Lcom/android/internal/view/IInputMethodSession$Stub;->updateSelection(IIIIII)V

    #@56
    goto :goto_8

    #@57
    .line 90
    .end local v1           #_arg0:I
    .end local v2           #_arg1:I
    .end local v3           #_arg2:I
    .end local v4           #_arg3:I
    .end local v5           #_arg4:I
    .end local v6           #_arg5:I
    :sswitch_57
    const-string v0, "com.android.internal.view.IInputMethodSession"

    #@59
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@5c
    .line 92
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@5f
    move-result v0

    #@60
    if-eqz v0, :cond_67

    #@62
    move v1, v7

    #@63
    .line 93
    .local v1, _arg0:Z
    :goto_63
    invoke-virtual {p0, v1}, Lcom/android/internal/view/IInputMethodSession$Stub;->viewClicked(Z)V

    #@66
    goto :goto_8

    #@67
    .line 92
    .end local v1           #_arg0:Z
    :cond_67
    const/4 v1, 0x0

    #@68
    goto :goto_63

    #@69
    .line 98
    :sswitch_69
    const-string v0, "com.android.internal.view.IInputMethodSession"

    #@6b
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@6e
    .line 100
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@71
    move-result v0

    #@72
    if-eqz v0, :cond_80

    #@74
    .line 101
    sget-object v0, Landroid/graphics/Rect;->CREATOR:Landroid/os/Parcelable$Creator;

    #@76
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@79
    move-result-object v1

    #@7a
    check-cast v1, Landroid/graphics/Rect;

    #@7c
    .line 106
    .local v1, _arg0:Landroid/graphics/Rect;
    :goto_7c
    invoke-virtual {p0, v1}, Lcom/android/internal/view/IInputMethodSession$Stub;->updateCursor(Landroid/graphics/Rect;)V

    #@7f
    goto :goto_8

    #@80
    .line 104
    .end local v1           #_arg0:Landroid/graphics/Rect;
    :cond_80
    const/4 v1, 0x0

    #@81
    .restart local v1       #_arg0:Landroid/graphics/Rect;
    goto :goto_7c

    #@82
    .line 111
    .end local v1           #_arg0:Landroid/graphics/Rect;
    :sswitch_82
    const-string v0, "com.android.internal.view.IInputMethodSession"

    #@84
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@87
    .line 113
    sget-object v0, Landroid/view/inputmethod/CompletionInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@89
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    #@8c
    move-result-object v1

    #@8d
    check-cast v1, [Landroid/view/inputmethod/CompletionInfo;

    #@8f
    .line 114
    .local v1, _arg0:[Landroid/view/inputmethod/CompletionInfo;
    invoke-virtual {p0, v1}, Lcom/android/internal/view/IInputMethodSession$Stub;->displayCompletions([Landroid/view/inputmethod/CompletionInfo;)V

    #@92
    goto/16 :goto_8

    #@94
    .line 119
    .end local v1           #_arg0:[Landroid/view/inputmethod/CompletionInfo;
    :sswitch_94
    const-string v0, "com.android.internal.view.IInputMethodSession"

    #@96
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@99
    .line 121
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@9c
    move-result v1

    #@9d
    .line 123
    .local v1, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@a0
    move-result v0

    #@a1
    if-eqz v0, :cond_b8

    #@a3
    .line 124
    sget-object v0, Landroid/view/KeyEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@a5
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@a8
    move-result-object v2

    #@a9
    check-cast v2, Landroid/view/KeyEvent;

    #@ab
    .line 130
    .local v2, _arg1:Landroid/view/KeyEvent;
    :goto_ab
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@ae
    move-result-object v0

    #@af
    invoke-static {v0}, Lcom/android/internal/view/IInputMethodCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/view/IInputMethodCallback;

    #@b2
    move-result-object v3

    #@b3
    .line 131
    .local v3, _arg2:Lcom/android/internal/view/IInputMethodCallback;
    invoke-virtual {p0, v1, v2, v3}, Lcom/android/internal/view/IInputMethodSession$Stub;->dispatchKeyEvent(ILandroid/view/KeyEvent;Lcom/android/internal/view/IInputMethodCallback;)V

    #@b6
    goto/16 :goto_8

    #@b8
    .line 127
    .end local v2           #_arg1:Landroid/view/KeyEvent;
    .end local v3           #_arg2:Lcom/android/internal/view/IInputMethodCallback;
    :cond_b8
    const/4 v2, 0x0

    #@b9
    .restart local v2       #_arg1:Landroid/view/KeyEvent;
    goto :goto_ab

    #@ba
    .line 136
    .end local v1           #_arg0:I
    .end local v2           #_arg1:Landroid/view/KeyEvent;
    :sswitch_ba
    const-string v0, "com.android.internal.view.IInputMethodSession"

    #@bc
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@bf
    .line 138
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@c2
    move-result v1

    #@c3
    .line 140
    .restart local v1       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@c6
    move-result v0

    #@c7
    if-eqz v0, :cond_de

    #@c9
    .line 141
    sget-object v0, Landroid/view/MotionEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@cb
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@ce
    move-result-object v2

    #@cf
    check-cast v2, Landroid/view/MotionEvent;

    #@d1
    .line 147
    .local v2, _arg1:Landroid/view/MotionEvent;
    :goto_d1
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@d4
    move-result-object v0

    #@d5
    invoke-static {v0}, Lcom/android/internal/view/IInputMethodCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/view/IInputMethodCallback;

    #@d8
    move-result-object v3

    #@d9
    .line 148
    .restart local v3       #_arg2:Lcom/android/internal/view/IInputMethodCallback;
    invoke-virtual {p0, v1, v2, v3}, Lcom/android/internal/view/IInputMethodSession$Stub;->dispatchTrackballEvent(ILandroid/view/MotionEvent;Lcom/android/internal/view/IInputMethodCallback;)V

    #@dc
    goto/16 :goto_8

    #@de
    .line 144
    .end local v2           #_arg1:Landroid/view/MotionEvent;
    .end local v3           #_arg2:Lcom/android/internal/view/IInputMethodCallback;
    :cond_de
    const/4 v2, 0x0

    #@df
    .restart local v2       #_arg1:Landroid/view/MotionEvent;
    goto :goto_d1

    #@e0
    .line 153
    .end local v1           #_arg0:I
    .end local v2           #_arg1:Landroid/view/MotionEvent;
    :sswitch_e0
    const-string v0, "com.android.internal.view.IInputMethodSession"

    #@e2
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@e5
    .line 155
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@e8
    move-result v1

    #@e9
    .line 157
    .restart local v1       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@ec
    move-result v0

    #@ed
    if-eqz v0, :cond_104

    #@ef
    .line 158
    sget-object v0, Landroid/view/MotionEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@f1
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@f4
    move-result-object v2

    #@f5
    check-cast v2, Landroid/view/MotionEvent;

    #@f7
    .line 164
    .restart local v2       #_arg1:Landroid/view/MotionEvent;
    :goto_f7
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@fa
    move-result-object v0

    #@fb
    invoke-static {v0}, Lcom/android/internal/view/IInputMethodCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/view/IInputMethodCallback;

    #@fe
    move-result-object v3

    #@ff
    .line 165
    .restart local v3       #_arg2:Lcom/android/internal/view/IInputMethodCallback;
    invoke-virtual {p0, v1, v2, v3}, Lcom/android/internal/view/IInputMethodSession$Stub;->dispatchGenericMotionEvent(ILandroid/view/MotionEvent;Lcom/android/internal/view/IInputMethodCallback;)V

    #@102
    goto/16 :goto_8

    #@104
    .line 161
    .end local v2           #_arg1:Landroid/view/MotionEvent;
    .end local v3           #_arg2:Lcom/android/internal/view/IInputMethodCallback;
    :cond_104
    const/4 v2, 0x0

    #@105
    .restart local v2       #_arg1:Landroid/view/MotionEvent;
    goto :goto_f7

    #@106
    .line 170
    .end local v1           #_arg0:I
    .end local v2           #_arg1:Landroid/view/MotionEvent;
    :sswitch_106
    const-string v0, "com.android.internal.view.IInputMethodSession"

    #@108
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@10b
    .line 172
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@10e
    move-result-object v1

    #@10f
    .line 174
    .local v1, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@112
    move-result v0

    #@113
    if-eqz v0, :cond_122

    #@115
    .line 175
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@117
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@11a
    move-result-object v2

    #@11b
    check-cast v2, Landroid/os/Bundle;

    #@11d
    .line 180
    .local v2, _arg1:Landroid/os/Bundle;
    :goto_11d
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/view/IInputMethodSession$Stub;->appPrivateCommand(Ljava/lang/String;Landroid/os/Bundle;)V

    #@120
    goto/16 :goto_8

    #@122
    .line 178
    .end local v2           #_arg1:Landroid/os/Bundle;
    :cond_122
    const/4 v2, 0x0

    #@123
    .restart local v2       #_arg1:Landroid/os/Bundle;
    goto :goto_11d

    #@124
    .line 185
    .end local v1           #_arg0:Ljava/lang/String;
    .end local v2           #_arg1:Landroid/os/Bundle;
    :sswitch_124
    const-string v0, "com.android.internal.view.IInputMethodSession"

    #@126
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@129
    .line 187
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@12c
    move-result v1

    #@12d
    .line 189
    .local v1, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@130
    move-result v2

    #@131
    .line 190
    .local v2, _arg1:I
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/view/IInputMethodSession$Stub;->toggleSoftInput(II)V

    #@134
    goto/16 :goto_8

    #@136
    .line 195
    .end local v1           #_arg0:I
    .end local v2           #_arg1:I
    :sswitch_136
    const-string v0, "com.android.internal.view.IInputMethodSession"

    #@138
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@13b
    .line 196
    invoke-virtual {p0}, Lcom/android/internal/view/IInputMethodSession$Stub;->finishSession()V

    #@13e
    goto/16 :goto_8

    #@140
    .line 42
    :sswitch_data_140
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_18
        0x3 -> :sswitch_35
        0x4 -> :sswitch_57
        0x5 -> :sswitch_69
        0x6 -> :sswitch_82
        0x7 -> :sswitch_94
        0x8 -> :sswitch_ba
        0x9 -> :sswitch_e0
        0xa -> :sswitch_106
        0xb -> :sswitch_124
        0xc -> :sswitch_136
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
