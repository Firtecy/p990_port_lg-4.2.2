.class final Lcom/android/internal/view/RotationPolicy$1;
.super Ljava/lang/Object;
.source "RotationPolicy.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/view/RotationPolicy;->setRotationLock(Landroid/content/Context;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$enabled:Z


# direct methods
.method constructor <init>(Z)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 82
    iput-boolean p1, p0, Lcom/android/internal/view/RotationPolicy$1;->val$enabled:Z

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public run()V
    .registers 5

    #@0
    .prologue
    .line 86
    :try_start_0
    invoke-static {}, Landroid/view/WindowManagerGlobal;->getWindowManagerService()Landroid/view/IWindowManager;

    #@3
    move-result-object v1

    #@4
    .line 87
    .local v1, wm:Landroid/view/IWindowManager;
    iget-boolean v2, p0, Lcom/android/internal/view/RotationPolicy$1;->val$enabled:Z

    #@6
    if-eqz v2, :cond_d

    #@8
    .line 88
    const/4 v2, -0x1

    #@9
    invoke-interface {v1, v2}, Landroid/view/IWindowManager;->freezeRotation(I)V

    #@c
    .line 95
    .end local v1           #wm:Landroid/view/IWindowManager;
    :goto_c
    return-void

    #@d
    .line 90
    .restart local v1       #wm:Landroid/view/IWindowManager;
    :cond_d
    invoke-interface {v1}, Landroid/view/IWindowManager;->thawRotation()V
    :try_end_10
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_10} :catch_11

    #@10
    goto :goto_c

    #@11
    .line 92
    .end local v1           #wm:Landroid/view/IWindowManager;
    :catch_11
    move-exception v0

    #@12
    .line 93
    .local v0, exc:Landroid/os/RemoteException;
    const-string v2, "RotationPolicy"

    #@14
    const-string v3, "Unable to save auto-rotate setting"

    #@16
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    goto :goto_c
.end method
