.class public Lcom/android/internal/view/menu/ActionMenuPresenter;
.super Lcom/android/internal/view/menu/BaseMenuPresenter;
.source "ActionMenuPresenter.java"

# interfaces
.implements Landroid/view/ActionProvider$SubUiVisibilityListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/view/menu/ActionMenuPresenter$1;,
        Lcom/android/internal/view/menu/ActionMenuPresenter$OpenOverflowRunnable;,
        Lcom/android/internal/view/menu/ActionMenuPresenter$PopupPresenterCallback;,
        Lcom/android/internal/view/menu/ActionMenuPresenter$ActionButtonSubmenu;,
        Lcom/android/internal/view/menu/ActionMenuPresenter$OverflowPopup;,
        Lcom/android/internal/view/menu/ActionMenuPresenter$OverflowMenuButton;,
        Lcom/android/internal/view/menu/ActionMenuPresenter$SavedState;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "ActionMenuPresenter"


# instance fields
.field private final mActionButtonGroups:Landroid/util/SparseBooleanArray;

.field private mActionButtonPopup:Lcom/android/internal/view/menu/ActionMenuPresenter$ActionButtonSubmenu;

.field private mActionItemWidthLimit:I

.field private mExpandedActionViewsExclusive:Z

.field private mMaxItems:I

.field private mMaxItemsSet:Z

.field private mMinCellSize:I

.field mOpenSubMenuId:I

.field private mOverflowButton:Landroid/view/View;

.field private mOverflowPopup:Lcom/android/internal/view/menu/ActionMenuPresenter$OverflowPopup;

.field final mPopupPresenterCallback:Lcom/android/internal/view/menu/ActionMenuPresenter$PopupPresenterCallback;

.field private mPostedOpenRunnable:Lcom/android/internal/view/menu/ActionMenuPresenter$OpenOverflowRunnable;

.field private mReserveOverflow:Z

.field private mReserveOverflowSet:Z

.field private mScrapActionButtonView:Landroid/view/View;

.field private mStrictWidthLimit:Z

.field private mWidthLimit:I

.field private mWidthLimitSet:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 72
    const v0, 0x109001c

    #@3
    const v1, 0x109001b

    #@6
    invoke-direct {p0, p1, v0, v1}, Lcom/android/internal/view/menu/BaseMenuPresenter;-><init>(Landroid/content/Context;II)V

    #@9
    .line 59
    new-instance v0, Landroid/util/SparseBooleanArray;

    #@b
    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    #@e
    iput-object v0, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mActionButtonGroups:Landroid/util/SparseBooleanArray;

    #@10
    .line 68
    new-instance v0, Lcom/android/internal/view/menu/ActionMenuPresenter$PopupPresenterCallback;

    #@12
    const/4 v1, 0x0

    #@13
    invoke-direct {v0, p0, v1}, Lcom/android/internal/view/menu/ActionMenuPresenter$PopupPresenterCallback;-><init>(Lcom/android/internal/view/menu/ActionMenuPresenter;Lcom/android/internal/view/menu/ActionMenuPresenter$1;)V

    #@16
    iput-object v0, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mPopupPresenterCallback:Lcom/android/internal/view/menu/ActionMenuPresenter$PopupPresenterCallback;

    #@18
    .line 74
    return-void
.end method

.method static synthetic access$102(Lcom/android/internal/view/menu/ActionMenuPresenter;Lcom/android/internal/view/menu/ActionMenuPresenter$OverflowPopup;)Lcom/android/internal/view/menu/ActionMenuPresenter$OverflowPopup;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 41
    iput-object p1, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mOverflowPopup:Lcom/android/internal/view/menu/ActionMenuPresenter$OverflowPopup;

    #@2
    return-object p1
.end method

.method static synthetic access$200(Lcom/android/internal/view/menu/ActionMenuPresenter;)Landroid/view/View;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 41
    iget-object v0, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mOverflowButton:Landroid/view/View;

    #@2
    return-object v0
.end method

.method static synthetic access$302(Lcom/android/internal/view/menu/ActionMenuPresenter;Lcom/android/internal/view/menu/ActionMenuPresenter$ActionButtonSubmenu;)Lcom/android/internal/view/menu/ActionMenuPresenter$ActionButtonSubmenu;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 41
    iput-object p1, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mActionButtonPopup:Lcom/android/internal/view/menu/ActionMenuPresenter$ActionButtonSubmenu;

    #@2
    return-object p1
.end method

.method static synthetic access$402(Lcom/android/internal/view/menu/ActionMenuPresenter;Lcom/android/internal/view/menu/ActionMenuPresenter$OpenOverflowRunnable;)Lcom/android/internal/view/menu/ActionMenuPresenter$OpenOverflowRunnable;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 41
    iput-object p1, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mPostedOpenRunnable:Lcom/android/internal/view/menu/ActionMenuPresenter$OpenOverflowRunnable;

    #@2
    return-object p1
.end method

.method private findViewForItem(Landroid/view/MenuItem;)Landroid/view/View;
    .registers 8
    .parameter "item"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 261
    iget-object v3, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mMenuView:Lcom/android/internal/view/menu/MenuView;

    #@3
    check-cast v3, Landroid/view/ViewGroup;

    #@5
    .line 262
    .local v3, parent:Landroid/view/ViewGroup;
    if-nez v3, :cond_9

    #@7
    move-object v0, v5

    #@8
    .line 272
    :cond_8
    :goto_8
    return-object v0

    #@9
    .line 264
    :cond_9
    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    #@c
    move-result v1

    #@d
    .line 265
    .local v1, count:I
    const/4 v2, 0x0

    #@e
    .local v2, i:I
    :goto_e
    if-ge v2, v1, :cond_24

    #@10
    .line 266
    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    #@13
    move-result-object v0

    #@14
    .line 267
    .local v0, child:Landroid/view/View;
    instance-of v4, v0, Lcom/android/internal/view/menu/MenuView$ItemView;

    #@16
    if-eqz v4, :cond_21

    #@18
    move-object v4, v0

    #@19
    check-cast v4, Lcom/android/internal/view/menu/MenuView$ItemView;

    #@1b
    invoke-interface {v4}, Lcom/android/internal/view/menu/MenuView$ItemView;->getItemData()Lcom/android/internal/view/menu/MenuItemImpl;

    #@1e
    move-result-object v4

    #@1f
    if-eq v4, p1, :cond_8

    #@21
    .line 265
    :cond_21
    add-int/lit8 v2, v2, 0x1

    #@23
    goto :goto_e

    #@24
    .end local v0           #child:Landroid/view/View;
    :cond_24
    move-object v0, v5

    #@25
    .line 272
    goto :goto_8
.end method


# virtual methods
.method public bindItemView(Lcom/android/internal/view/menu/MenuItemImpl;Lcom/android/internal/view/menu/MenuView$ItemView;)V
    .registers 6
    .parameter "item"
    .parameter "itemView"

    #@0
    .prologue
    .line 174
    const/4 v2, 0x0

    #@1
    invoke-interface {p2, p1, v2}, Lcom/android/internal/view/menu/MenuView$ItemView;->initialize(Lcom/android/internal/view/menu/MenuItemImpl;I)V

    #@4
    .line 176
    iget-object v1, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mMenuView:Lcom/android/internal/view/menu/MenuView;

    #@6
    check-cast v1, Lcom/android/internal/view/menu/ActionMenuView;

    #@8
    .local v1, menuView:Lcom/android/internal/view/menu/ActionMenuView;
    move-object v0, p2

    #@9
    .line 177
    check-cast v0, Lcom/android/internal/view/menu/ActionMenuItemView;

    #@b
    .line 178
    .local v0, actionItemView:Lcom/android/internal/view/menu/ActionMenuItemView;
    invoke-virtual {v0, v1}, Lcom/android/internal/view/menu/ActionMenuItemView;->setItemInvoker(Lcom/android/internal/view/menu/MenuBuilder$ItemInvoker;)V

    #@e
    .line 179
    return-void
.end method

.method public dismissPopupMenus()Z
    .registers 3

    #@0
    .prologue
    .line 321
    invoke-virtual {p0}, Lcom/android/internal/view/menu/ActionMenuPresenter;->hideOverflowMenu()Z

    #@3
    move-result v0

    #@4
    .line 322
    .local v0, result:Z
    invoke-virtual {p0}, Lcom/android/internal/view/menu/ActionMenuPresenter;->hideSubMenus()Z

    #@7
    move-result v1

    #@8
    or-int/2addr v0, v1

    #@9
    .line 323
    return v0
.end method

.method public filterLeftoverView(Landroid/view/ViewGroup;I)Z
    .registers 5
    .parameter "parent"
    .parameter "childIndex"

    #@0
    .prologue
    .line 235
    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    #@3
    move-result-object v0

    #@4
    iget-object v1, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mOverflowButton:Landroid/view/View;

    #@6
    if-ne v0, v1, :cond_a

    #@8
    const/4 v0, 0x0

    #@9
    .line 236
    :goto_9
    return v0

    #@a
    :cond_a
    invoke-super {p0, p1, p2}, Lcom/android/internal/view/menu/BaseMenuPresenter;->filterLeftoverView(Landroid/view/ViewGroup;I)Z

    #@d
    move-result v0

    #@e
    goto :goto_9
.end method

.method public flagActionItems()Z
    .registers 30

    #@0
    .prologue
    .line 354
    move-object/from16 v0, p0

    #@2
    iget-object v0, v0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@4
    move-object/from16 v27, v0

    #@6
    invoke-virtual/range {v27 .. v27}, Lcom/android/internal/view/menu/MenuBuilder;->getVisibleItems()Ljava/util/ArrayList;

    #@9
    move-result-object v25

    #@a
    .line 355
    .local v25, visibleItems:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/view/menu/MenuItemImpl;>;"
    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->size()I

    #@d
    move-result v15

    #@e
    .line 356
    .local v15, itemsSize:I
    move-object/from16 v0, p0

    #@10
    iget v0, v0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mMaxItems:I

    #@12
    move/from16 v17, v0

    #@14
    .line 357
    .local v17, maxActions:I
    move-object/from16 v0, p0

    #@16
    iget v0, v0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mActionItemWidthLimit:I

    #@18
    move/from16 v26, v0

    #@1a
    .line 358
    .local v26, widthLimit:I
    const/16 v27, 0x0

    #@1c
    const/16 v28, 0x0

    #@1e
    invoke-static/range {v27 .. v28}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@21
    move-result v20

    #@22
    .line 359
    .local v20, querySpec:I
    move-object/from16 v0, p0

    #@24
    iget-object v0, v0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mMenuView:Lcom/android/internal/view/menu/MenuView;

    #@26
    move-object/from16 v19, v0

    #@28
    check-cast v19, Landroid/view/ViewGroup;

    #@2a
    .line 361
    .local v19, parent:Landroid/view/ViewGroup;
    const/16 v22, 0x0

    #@2c
    .line 362
    .local v22, requiredItems:I
    const/16 v21, 0x0

    #@2e
    .line 363
    .local v21, requestedItems:I
    const/4 v8, 0x0

    #@2f
    .line 364
    .local v8, firstActionWidth:I
    const/4 v10, 0x0

    #@30
    .line 365
    .local v10, hasOverflow:Z
    const/4 v11, 0x0

    #@31
    .local v11, i:I
    :goto_31
    if-ge v11, v15, :cond_61

    #@33
    .line 366
    move-object/from16 v0, v25

    #@35
    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@38
    move-result-object v14

    #@39
    check-cast v14, Lcom/android/internal/view/menu/MenuItemImpl;

    #@3b
    .line 367
    .local v14, item:Lcom/android/internal/view/menu/MenuItemImpl;
    invoke-virtual {v14}, Lcom/android/internal/view/menu/MenuItemImpl;->requiresActionButton()Z

    #@3e
    move-result v27

    #@3f
    if-eqz v27, :cond_56

    #@41
    .line 368
    add-int/lit8 v22, v22, 0x1

    #@43
    .line 374
    :goto_43
    move-object/from16 v0, p0

    #@45
    iget-boolean v0, v0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mExpandedActionViewsExclusive:Z

    #@47
    move/from16 v27, v0

    #@49
    if-eqz v27, :cond_53

    #@4b
    invoke-virtual {v14}, Lcom/android/internal/view/menu/MenuItemImpl;->isActionViewExpanded()Z

    #@4e
    move-result v27

    #@4f
    if-eqz v27, :cond_53

    #@51
    .line 377
    const/16 v17, 0x0

    #@53
    .line 365
    :cond_53
    add-int/lit8 v11, v11, 0x1

    #@55
    goto :goto_31

    #@56
    .line 369
    :cond_56
    invoke-virtual {v14}, Lcom/android/internal/view/menu/MenuItemImpl;->requestsActionButton()Z

    #@59
    move-result v27

    #@5a
    if-eqz v27, :cond_5f

    #@5c
    .line 370
    add-int/lit8 v21, v21, 0x1

    #@5e
    goto :goto_43

    #@5f
    .line 372
    :cond_5f
    const/4 v10, 0x1

    #@60
    goto :goto_43

    #@61
    .line 382
    .end local v14           #item:Lcom/android/internal/view/menu/MenuItemImpl;
    :cond_61
    move-object/from16 v0, p0

    #@63
    iget-boolean v0, v0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mReserveOverflow:Z

    #@65
    move/from16 v27, v0

    #@67
    if-eqz v27, :cond_75

    #@69
    if-nez v10, :cond_73

    #@6b
    add-int v27, v22, v21

    #@6d
    move/from16 v0, v27

    #@6f
    move/from16 v1, v17

    #@71
    if-le v0, v1, :cond_75

    #@73
    .line 384
    :cond_73
    add-int/lit8 v17, v17, -0x1

    #@75
    .line 386
    :cond_75
    sub-int v17, v17, v22

    #@77
    .line 388
    move-object/from16 v0, p0

    #@79
    iget-object v0, v0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mActionButtonGroups:Landroid/util/SparseBooleanArray;

    #@7b
    move-object/from16 v23, v0

    #@7d
    .line 389
    .local v23, seenGroups:Landroid/util/SparseBooleanArray;
    invoke-virtual/range {v23 .. v23}, Landroid/util/SparseBooleanArray;->clear()V

    #@80
    .line 391
    const/4 v4, 0x0

    #@81
    .line 392
    .local v4, cellSize:I
    const/4 v7, 0x0

    #@82
    .line 393
    .local v7, cellsRemaining:I
    move-object/from16 v0, p0

    #@84
    iget-boolean v0, v0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mStrictWidthLimit:Z

    #@86
    move/from16 v27, v0

    #@88
    if-eqz v27, :cond_a4

    #@8a
    .line 394
    move-object/from16 v0, p0

    #@8c
    iget v0, v0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mMinCellSize:I

    #@8e
    move/from16 v27, v0

    #@90
    div-int v7, v26, v27

    #@92
    .line 395
    move-object/from16 v0, p0

    #@94
    iget v0, v0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mMinCellSize:I

    #@96
    move/from16 v27, v0

    #@98
    rem-int v5, v26, v27

    #@9a
    .line 396
    .local v5, cellSizeRemaining:I
    move-object/from16 v0, p0

    #@9c
    iget v0, v0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mMinCellSize:I

    #@9e
    move/from16 v27, v0

    #@a0
    div-int v28, v5, v7

    #@a2
    add-int v4, v27, v28

    #@a4
    .line 400
    .end local v5           #cellSizeRemaining:I
    :cond_a4
    const/4 v11, 0x0

    #@a5
    :goto_a5
    if-ge v11, v15, :cond_1f6

    #@a7
    .line 401
    move-object/from16 v0, v25

    #@a9
    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@ac
    move-result-object v14

    #@ad
    check-cast v14, Lcom/android/internal/view/menu/MenuItemImpl;

    #@af
    .line 403
    .restart local v14       #item:Lcom/android/internal/view/menu/MenuItemImpl;
    invoke-virtual {v14}, Lcom/android/internal/view/menu/MenuItemImpl;->requiresActionButton()Z

    #@b2
    move-result v27

    #@b3
    if-eqz v27, :cond_116

    #@b5
    .line 404
    move-object/from16 v0, p0

    #@b7
    iget-object v0, v0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mScrapActionButtonView:Landroid/view/View;

    #@b9
    move-object/from16 v27, v0

    #@bb
    move-object/from16 v0, p0

    #@bd
    move-object/from16 v1, v27

    #@bf
    move-object/from16 v2, v19

    #@c1
    invoke-virtual {v0, v14, v1, v2}, Lcom/android/internal/view/menu/ActionMenuPresenter;->getItemView(Lcom/android/internal/view/menu/MenuItemImpl;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    #@c4
    move-result-object v24

    #@c5
    .line 405
    .local v24, v:Landroid/view/View;
    move-object/from16 v0, p0

    #@c7
    iget-object v0, v0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mScrapActionButtonView:Landroid/view/View;

    #@c9
    move-object/from16 v27, v0

    #@cb
    if-nez v27, :cond_d3

    #@cd
    .line 406
    move-object/from16 v0, v24

    #@cf
    move-object/from16 v1, p0

    #@d1
    iput-object v0, v1, Lcom/android/internal/view/menu/ActionMenuPresenter;->mScrapActionButtonView:Landroid/view/View;

    #@d3
    .line 408
    :cond_d3
    move-object/from16 v0, p0

    #@d5
    iget-boolean v0, v0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mStrictWidthLimit:Z

    #@d7
    move/from16 v27, v0

    #@d9
    if-eqz v27, :cond_10c

    #@db
    .line 409
    const/16 v27, 0x0

    #@dd
    move-object/from16 v0, v24

    #@df
    move/from16 v1, v20

    #@e1
    move/from16 v2, v27

    #@e3
    invoke-static {v0, v4, v7, v1, v2}, Lcom/android/internal/view/menu/ActionMenuView;->measureChildForCells(Landroid/view/View;IIII)I

    #@e6
    move-result v27

    #@e7
    sub-int v7, v7, v27

    #@e9
    .line 414
    :goto_e9
    invoke-virtual/range {v24 .. v24}, Landroid/view/View;->getMeasuredWidth()I

    #@ec
    move-result v18

    #@ed
    .line 415
    .local v18, measuredWidth:I
    sub-int v26, v26, v18

    #@ef
    .line 416
    if-nez v8, :cond_f3

    #@f1
    .line 417
    move/from16 v8, v18

    #@f3
    .line 419
    :cond_f3
    invoke-virtual {v14}, Lcom/android/internal/view/menu/MenuItemImpl;->getGroupId()I

    #@f6
    move-result v9

    #@f7
    .line 420
    .local v9, groupId:I
    if-eqz v9, :cond_102

    #@f9
    .line 421
    const/16 v27, 0x1

    #@fb
    move-object/from16 v0, v23

    #@fd
    move/from16 v1, v27

    #@ff
    invoke-virtual {v0, v9, v1}, Landroid/util/SparseBooleanArray;->put(IZ)V

    #@102
    .line 423
    :cond_102
    const/16 v27, 0x1

    #@104
    move/from16 v0, v27

    #@106
    invoke-virtual {v14, v0}, Lcom/android/internal/view/menu/MenuItemImpl;->setIsActionButton(Z)V

    #@109
    .line 400
    .end local v9           #groupId:I
    .end local v18           #measuredWidth:I
    .end local v24           #v:Landroid/view/View;
    :goto_109
    add-int/lit8 v11, v11, 0x1

    #@10b
    goto :goto_a5

    #@10c
    .line 412
    .restart local v24       #v:Landroid/view/View;
    :cond_10c
    move-object/from16 v0, v24

    #@10e
    move/from16 v1, v20

    #@110
    move/from16 v2, v20

    #@112
    invoke-virtual {v0, v1, v2}, Landroid/view/View;->measure(II)V

    #@115
    goto :goto_e9

    #@116
    .line 424
    .end local v24           #v:Landroid/view/View;
    :cond_116
    invoke-virtual {v14}, Lcom/android/internal/view/menu/MenuItemImpl;->requestsActionButton()Z

    #@119
    move-result v27

    #@11a
    if-eqz v27, :cond_1ed

    #@11c
    .line 427
    invoke-virtual {v14}, Lcom/android/internal/view/menu/MenuItemImpl;->getGroupId()I

    #@11f
    move-result v9

    #@120
    .line 428
    .restart local v9       #groupId:I
    move-object/from16 v0, v23

    #@122
    invoke-virtual {v0, v9}, Landroid/util/SparseBooleanArray;->get(I)Z

    #@125
    move-result v12

    #@126
    .line 429
    .local v12, inGroup:Z
    if-gtz v17, :cond_12a

    #@128
    if-eqz v12, :cond_19d

    #@12a
    :cond_12a
    if-lez v26, :cond_19d

    #@12c
    move-object/from16 v0, p0

    #@12e
    iget-boolean v0, v0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mStrictWidthLimit:Z

    #@130
    move/from16 v27, v0

    #@132
    if-eqz v27, :cond_136

    #@134
    if-lez v7, :cond_19d

    #@136
    :cond_136
    const/4 v13, 0x1

    #@137
    .line 432
    .local v13, isAction:Z
    :goto_137
    if-eqz v13, :cond_187

    #@139
    .line 433
    move-object/from16 v0, p0

    #@13b
    iget-object v0, v0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mScrapActionButtonView:Landroid/view/View;

    #@13d
    move-object/from16 v27, v0

    #@13f
    move-object/from16 v0, p0

    #@141
    move-object/from16 v1, v27

    #@143
    move-object/from16 v2, v19

    #@145
    invoke-virtual {v0, v14, v1, v2}, Lcom/android/internal/view/menu/ActionMenuPresenter;->getItemView(Lcom/android/internal/view/menu/MenuItemImpl;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    #@148
    move-result-object v24

    #@149
    .line 434
    .restart local v24       #v:Landroid/view/View;
    move-object/from16 v0, p0

    #@14b
    iget-object v0, v0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mScrapActionButtonView:Landroid/view/View;

    #@14d
    move-object/from16 v27, v0

    #@14f
    if-nez v27, :cond_157

    #@151
    .line 435
    move-object/from16 v0, v24

    #@153
    move-object/from16 v1, p0

    #@155
    iput-object v0, v1, Lcom/android/internal/view/menu/ActionMenuPresenter;->mScrapActionButtonView:Landroid/view/View;

    #@157
    .line 437
    :cond_157
    move-object/from16 v0, p0

    #@159
    iget-boolean v0, v0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mStrictWidthLimit:Z

    #@15b
    move/from16 v27, v0

    #@15d
    if-eqz v27, :cond_19f

    #@15f
    .line 438
    const/16 v27, 0x0

    #@161
    move-object/from16 v0, v24

    #@163
    move/from16 v1, v20

    #@165
    move/from16 v2, v27

    #@167
    invoke-static {v0, v4, v7, v1, v2}, Lcom/android/internal/view/menu/ActionMenuView;->measureChildForCells(Landroid/view/View;IIII)I

    #@16a
    move-result v6

    #@16b
    .line 440
    .local v6, cells:I
    sub-int/2addr v7, v6

    #@16c
    .line 441
    if-nez v6, :cond_16f

    #@16e
    .line 442
    const/4 v13, 0x0

    #@16f
    .line 447
    .end local v6           #cells:I
    :cond_16f
    :goto_16f
    invoke-virtual/range {v24 .. v24}, Landroid/view/View;->getMeasuredWidth()I

    #@172
    move-result v18

    #@173
    .line 448
    .restart local v18       #measuredWidth:I
    sub-int v26, v26, v18

    #@175
    .line 449
    if-nez v8, :cond_179

    #@177
    .line 450
    move/from16 v8, v18

    #@179
    .line 453
    :cond_179
    move-object/from16 v0, p0

    #@17b
    iget-boolean v0, v0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mStrictWidthLimit:Z

    #@17d
    move/from16 v27, v0

    #@17f
    if-eqz v27, :cond_1ac

    #@181
    .line 454
    if-ltz v26, :cond_1a9

    #@183
    const/16 v27, 0x1

    #@185
    :goto_185
    and-int v13, v13, v27

    #@187
    .line 461
    .end local v18           #measuredWidth:I
    .end local v24           #v:Landroid/view/View;
    :cond_187
    :goto_187
    if-eqz v13, :cond_1b8

    #@189
    if-eqz v9, :cond_1b8

    #@18b
    .line 462
    const/16 v27, 0x1

    #@18d
    move-object/from16 v0, v23

    #@18f
    move/from16 v1, v27

    #@191
    invoke-virtual {v0, v9, v1}, Landroid/util/SparseBooleanArray;->put(IZ)V

    #@194
    .line 476
    :cond_194
    if-eqz v13, :cond_198

    #@196
    add-int/lit8 v17, v17, -0x1

    #@198
    .line 478
    :cond_198
    invoke-virtual {v14, v13}, Lcom/android/internal/view/menu/MenuItemImpl;->setIsActionButton(Z)V

    #@19b
    goto/16 :goto_109

    #@19d
    .line 429
    .end local v13           #isAction:Z
    :cond_19d
    const/4 v13, 0x0

    #@19e
    goto :goto_137

    #@19f
    .line 445
    .restart local v13       #isAction:Z
    .restart local v24       #v:Landroid/view/View;
    :cond_19f
    move-object/from16 v0, v24

    #@1a1
    move/from16 v1, v20

    #@1a3
    move/from16 v2, v20

    #@1a5
    invoke-virtual {v0, v1, v2}, Landroid/view/View;->measure(II)V

    #@1a8
    goto :goto_16f

    #@1a9
    .line 454
    .restart local v18       #measuredWidth:I
    :cond_1a9
    const/16 v27, 0x0

    #@1ab
    goto :goto_185

    #@1ac
    .line 457
    :cond_1ac
    add-int v27, v26, v8

    #@1ae
    if-lez v27, :cond_1b5

    #@1b0
    const/16 v27, 0x1

    #@1b2
    :goto_1b2
    and-int v13, v13, v27

    #@1b4
    goto :goto_187

    #@1b5
    :cond_1b5
    const/16 v27, 0x0

    #@1b7
    goto :goto_1b2

    #@1b8
    .line 463
    .end local v18           #measuredWidth:I
    .end local v24           #v:Landroid/view/View;
    :cond_1b8
    if-eqz v12, :cond_194

    #@1ba
    .line 465
    const/16 v27, 0x0

    #@1bc
    move-object/from16 v0, v23

    #@1be
    move/from16 v1, v27

    #@1c0
    invoke-virtual {v0, v9, v1}, Landroid/util/SparseBooleanArray;->put(IZ)V

    #@1c3
    .line 466
    const/16 v16, 0x0

    #@1c5
    .local v16, j:I
    :goto_1c5
    move/from16 v0, v16

    #@1c7
    if-ge v0, v11, :cond_194

    #@1c9
    .line 467
    move-object/from16 v0, v25

    #@1cb
    move/from16 v1, v16

    #@1cd
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1d0
    move-result-object v3

    #@1d1
    check-cast v3, Lcom/android/internal/view/menu/MenuItemImpl;

    #@1d3
    .line 468
    .local v3, areYouMyGroupie:Lcom/android/internal/view/menu/MenuItemImpl;
    invoke-virtual {v3}, Lcom/android/internal/view/menu/MenuItemImpl;->getGroupId()I

    #@1d6
    move-result v27

    #@1d7
    move/from16 v0, v27

    #@1d9
    if-ne v0, v9, :cond_1ea

    #@1db
    .line 470
    invoke-virtual {v3}, Lcom/android/internal/view/menu/MenuItemImpl;->isActionButton()Z

    #@1de
    move-result v27

    #@1df
    if-eqz v27, :cond_1e3

    #@1e1
    add-int/lit8 v17, v17, 0x1

    #@1e3
    .line 471
    :cond_1e3
    const/16 v27, 0x0

    #@1e5
    move/from16 v0, v27

    #@1e7
    invoke-virtual {v3, v0}, Lcom/android/internal/view/menu/MenuItemImpl;->setIsActionButton(Z)V

    #@1ea
    .line 466
    :cond_1ea
    add-int/lit8 v16, v16, 0x1

    #@1ec
    goto :goto_1c5

    #@1ed
    .line 481
    .end local v3           #areYouMyGroupie:Lcom/android/internal/view/menu/MenuItemImpl;
    .end local v9           #groupId:I
    .end local v12           #inGroup:Z
    .end local v13           #isAction:Z
    .end local v16           #j:I
    :cond_1ed
    const/16 v27, 0x0

    #@1ef
    move/from16 v0, v27

    #@1f1
    invoke-virtual {v14, v0}, Lcom/android/internal/view/menu/MenuItemImpl;->setIsActionButton(Z)V

    #@1f4
    goto/16 :goto_109

    #@1f6
    .line 484
    .end local v14           #item:Lcom/android/internal/view/menu/MenuItemImpl;
    :cond_1f6
    const/16 v27, 0x1

    #@1f8
    return v27
.end method

.method public getItemView(Lcom/android/internal/view/menu/MenuItemImpl;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 8
    .parameter "item"
    .parameter "convertView"
    .parameter "parent"

    #@0
    .prologue
    .line 155
    invoke-virtual {p1}, Lcom/android/internal/view/menu/MenuItemImpl;->getActionView()Landroid/view/View;

    #@3
    move-result-object v0

    #@4
    .line 156
    .local v0, actionView:Landroid/view/View;
    if-eqz v0, :cond_c

    #@6
    invoke-virtual {p1}, Lcom/android/internal/view/menu/MenuItemImpl;->hasCollapsibleActionView()Z

    #@9
    move-result v3

    #@a
    if-eqz v3, :cond_15

    #@c
    .line 157
    :cond_c
    instance-of v3, p2, Lcom/android/internal/view/menu/ActionMenuItemView;

    #@e
    if-nez v3, :cond_11

    #@10
    .line 158
    const/4 p2, 0x0

    #@11
    .line 160
    :cond_11
    invoke-super {p0, p1, p2, p3}, Lcom/android/internal/view/menu/BaseMenuPresenter;->getItemView(Lcom/android/internal/view/menu/MenuItemImpl;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    #@14
    move-result-object v0

    #@15
    .line 162
    :cond_15
    invoke-virtual {p1}, Lcom/android/internal/view/menu/MenuItemImpl;->isActionViewExpanded()Z

    #@18
    move-result v3

    #@19
    if-eqz v3, :cond_35

    #@1b
    const/16 v3, 0x8

    #@1d
    :goto_1d
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    #@20
    move-object v2, p3

    #@21
    .line 164
    check-cast v2, Lcom/android/internal/view/menu/ActionMenuView;

    #@23
    .line 165
    .local v2, menuParent:Lcom/android/internal/view/menu/ActionMenuView;
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@26
    move-result-object v1

    #@27
    .line 166
    .local v1, lp:Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {v2, v1}, Lcom/android/internal/view/menu/ActionMenuView;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    #@2a
    move-result v3

    #@2b
    if-nez v3, :cond_34

    #@2d
    .line 167
    invoke-virtual {v2, v1}, Lcom/android/internal/view/menu/ActionMenuView;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;

    #@30
    move-result-object v3

    #@31
    invoke-virtual {v0, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@34
    .line 169
    :cond_34
    return-object v0

    #@35
    .line 162
    .end local v1           #lp:Landroid/view/ViewGroup$LayoutParams;
    .end local v2           #menuParent:Lcom/android/internal/view/menu/ActionMenuView;
    :cond_35
    const/4 v3, 0x0

    #@36
    goto :goto_1d
.end method

.method public getMenuView(Landroid/view/ViewGroup;)Lcom/android/internal/view/menu/MenuView;
    .registers 4
    .parameter "root"

    #@0
    .prologue
    .line 148
    invoke-super {p0, p1}, Lcom/android/internal/view/menu/BaseMenuPresenter;->getMenuView(Landroid/view/ViewGroup;)Lcom/android/internal/view/menu/MenuView;

    #@3
    move-result-object v0

    #@4
    .local v0, result:Lcom/android/internal/view/menu/MenuView;
    move-object v1, v0

    #@5
    .line 149
    check-cast v1, Lcom/android/internal/view/menu/ActionMenuView;

    #@7
    invoke-virtual {v1, p0}, Lcom/android/internal/view/menu/ActionMenuView;->setPresenter(Lcom/android/internal/view/menu/ActionMenuPresenter;)V

    #@a
    .line 150
    return-object v0
.end method

.method public hideOverflowMenu()Z
    .registers 5

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 302
    iget-object v1, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mPostedOpenRunnable:Lcom/android/internal/view/menu/ActionMenuPresenter$OpenOverflowRunnable;

    #@3
    if-eqz v1, :cond_17

    #@5
    iget-object v1, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mMenuView:Lcom/android/internal/view/menu/MenuView;

    #@7
    if-eqz v1, :cond_17

    #@9
    .line 303
    iget-object v1, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mMenuView:Lcom/android/internal/view/menu/MenuView;

    #@b
    check-cast v1, Landroid/view/View;

    #@d
    iget-object v3, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mPostedOpenRunnable:Lcom/android/internal/view/menu/ActionMenuPresenter$OpenOverflowRunnable;

    #@f
    invoke-virtual {v1, v3}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@12
    .line 304
    const/4 v1, 0x0

    #@13
    iput-object v1, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mPostedOpenRunnable:Lcom/android/internal/view/menu/ActionMenuPresenter$OpenOverflowRunnable;

    #@15
    move v1, v2

    #@16
    .line 313
    :goto_16
    return v1

    #@17
    .line 308
    :cond_17
    iget-object v0, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mOverflowPopup:Lcom/android/internal/view/menu/ActionMenuPresenter$OverflowPopup;

    #@19
    .line 309
    .local v0, popup:Lcom/android/internal/view/menu/MenuPopupHelper;
    if-eqz v0, :cond_20

    #@1b
    .line 310
    invoke-virtual {v0}, Lcom/android/internal/view/menu/MenuPopupHelper;->dismiss()V

    #@1e
    move v1, v2

    #@1f
    .line 311
    goto :goto_16

    #@20
    .line 313
    :cond_20
    const/4 v1, 0x0

    #@21
    goto :goto_16
.end method

.method public hideSubMenus()Z
    .registers 2

    #@0
    .prologue
    .line 332
    iget-object v0, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mActionButtonPopup:Lcom/android/internal/view/menu/ActionMenuPresenter$ActionButtonSubmenu;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 333
    iget-object v0, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mActionButtonPopup:Lcom/android/internal/view/menu/ActionMenuPresenter$ActionButtonSubmenu;

    #@6
    invoke-virtual {v0}, Lcom/android/internal/view/menu/ActionMenuPresenter$ActionButtonSubmenu;->dismiss()V

    #@9
    .line 334
    const/4 v0, 0x1

    #@a
    .line 336
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public initForMenu(Landroid/content/Context;Lcom/android/internal/view/menu/MenuBuilder;)V
    .registers 11
    .parameter "context"
    .parameter "menu"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v6, 0x0

    #@2
    .line 78
    invoke-super {p0, p1, p2}, Lcom/android/internal/view/menu/BaseMenuPresenter;->initForMenu(Landroid/content/Context;Lcom/android/internal/view/menu/MenuBuilder;)V

    #@5
    .line 80
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@8
    move-result-object v1

    #@9
    .line 82
    .local v1, res:Landroid/content/res/Resources;
    invoke-static {p1}, Lcom/android/internal/view/ActionBarPolicy;->get(Landroid/content/Context;)Lcom/android/internal/view/ActionBarPolicy;

    #@c
    move-result-object v0

    #@d
    .line 83
    .local v0, abp:Lcom/android/internal/view/ActionBarPolicy;
    iget-boolean v4, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mReserveOverflowSet:Z

    #@f
    if-nez v4, :cond_17

    #@11
    .line 84
    invoke-virtual {v0}, Lcom/android/internal/view/ActionBarPolicy;->showsOverflowMenuButton()Z

    #@14
    move-result v4

    #@15
    iput-boolean v4, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mReserveOverflow:Z

    #@17
    .line 87
    :cond_17
    iget-boolean v4, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mWidthLimitSet:Z

    #@19
    if-nez v4, :cond_21

    #@1b
    .line 88
    invoke-virtual {v0}, Lcom/android/internal/view/ActionBarPolicy;->getEmbeddedMenuWidthLimit()I

    #@1e
    move-result v4

    #@1f
    iput v4, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mWidthLimit:I

    #@21
    .line 92
    :cond_21
    iget-boolean v4, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mMaxItemsSet:Z

    #@23
    if-nez v4, :cond_2b

    #@25
    .line 93
    invoke-virtual {v0}, Lcom/android/internal/view/ActionBarPolicy;->getMaxActionButtons()I

    #@28
    move-result v4

    #@29
    iput v4, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mMaxItems:I

    #@2b
    .line 96
    :cond_2b
    iget v3, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mWidthLimit:I

    #@2d
    .line 97
    .local v3, width:I
    iget-boolean v4, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mReserveOverflow:Z

    #@2f
    if-eqz v4, :cond_5f

    #@31
    .line 98
    iget-object v4, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mOverflowButton:Landroid/view/View;

    #@33
    if-nez v4, :cond_47

    #@35
    .line 99
    new-instance v4, Lcom/android/internal/view/menu/ActionMenuPresenter$OverflowMenuButton;

    #@37
    iget-object v5, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mSystemContext:Landroid/content/Context;

    #@39
    invoke-direct {v4, p0, v5}, Lcom/android/internal/view/menu/ActionMenuPresenter$OverflowMenuButton;-><init>(Lcom/android/internal/view/menu/ActionMenuPresenter;Landroid/content/Context;)V

    #@3c
    iput-object v4, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mOverflowButton:Landroid/view/View;

    #@3e
    .line 100
    invoke-static {v6, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@41
    move-result v2

    #@42
    .line 101
    .local v2, spec:I
    iget-object v4, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mOverflowButton:Landroid/view/View;

    #@44
    invoke-virtual {v4, v2, v2}, Landroid/view/View;->measure(II)V

    #@47
    .line 103
    .end local v2           #spec:I
    :cond_47
    iget-object v4, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mOverflowButton:Landroid/view/View;

    #@49
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    #@4c
    move-result v4

    #@4d
    sub-int/2addr v3, v4

    #@4e
    .line 108
    :goto_4e
    iput v3, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mActionItemWidthLimit:I

    #@50
    .line 110
    const/high16 v4, 0x4260

    #@52
    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@55
    move-result-object v5

    #@56
    iget v5, v5, Landroid/util/DisplayMetrics;->density:F

    #@58
    mul-float/2addr v4, v5

    #@59
    float-to-int v4, v4

    #@5a
    iput v4, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mMinCellSize:I

    #@5c
    .line 113
    iput-object v7, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mScrapActionButtonView:Landroid/view/View;

    #@5e
    .line 114
    return-void

    #@5f
    .line 105
    :cond_5f
    iput-object v7, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mOverflowButton:Landroid/view/View;

    #@61
    goto :goto_4e
.end method

.method public isOverflowMenuShowing()Z
    .registers 2

    #@0
    .prologue
    .line 343
    iget-object v0, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mOverflowPopup:Lcom/android/internal/view/menu/ActionMenuPresenter$OverflowPopup;

    #@2
    if-eqz v0, :cond_e

    #@4
    iget-object v0, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mOverflowPopup:Lcom/android/internal/view/menu/ActionMenuPresenter$OverflowPopup;

    #@6
    invoke-virtual {v0}, Lcom/android/internal/view/menu/ActionMenuPresenter$OverflowPopup;->isShowing()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public isOverflowReserved()Z
    .registers 2

    #@0
    .prologue
    .line 350
    iget-boolean v0, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mReserveOverflow:Z

    #@2
    return v0
.end method

.method public onCloseMenu(Lcom/android/internal/view/menu/MenuBuilder;Z)V
    .registers 3
    .parameter "menu"
    .parameter "allMenusAreClosing"

    #@0
    .prologue
    .line 489
    invoke-virtual {p0}, Lcom/android/internal/view/menu/ActionMenuPresenter;->dismissPopupMenus()Z

    #@3
    .line 490
    invoke-super {p0, p1, p2}, Lcom/android/internal/view/menu/BaseMenuPresenter;->onCloseMenu(Lcom/android/internal/view/menu/MenuBuilder;Z)V

    #@6
    .line 491
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 4
    .parameter "newConfig"

    #@0
    .prologue
    .line 117
    iget-boolean v0, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mMaxItemsSet:Z

    #@2
    if-nez v0, :cond_13

    #@4
    .line 118
    iget-object v0, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mContext:Landroid/content/Context;

    #@6
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@9
    move-result-object v0

    #@a
    const v1, 0x10e003b

    #@d
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    #@10
    move-result v0

    #@11
    iput v0, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mMaxItems:I

    #@13
    .line 121
    :cond_13
    iget-object v0, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@15
    if-eqz v0, :cond_1d

    #@17
    .line 122
    iget-object v0, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@19
    const/4 v1, 0x1

    #@1a
    invoke-virtual {v0, v1}, Lcom/android/internal/view/menu/MenuBuilder;->onItemsChanged(Z)V

    #@1d
    .line 124
    :cond_1d
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .registers 7
    .parameter "state"

    #@0
    .prologue
    .line 502
    move-object v1, p1

    #@1
    check-cast v1, Lcom/android/internal/view/menu/ActionMenuPresenter$SavedState;

    #@3
    .line 503
    .local v1, saved:Lcom/android/internal/view/menu/ActionMenuPresenter$SavedState;
    iget v3, v1, Lcom/android/internal/view/menu/ActionMenuPresenter$SavedState;->openSubMenuId:I

    #@5
    if-lez v3, :cond_1c

    #@7
    .line 504
    iget-object v3, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@9
    iget v4, v1, Lcom/android/internal/view/menu/ActionMenuPresenter$SavedState;->openSubMenuId:I

    #@b
    invoke-virtual {v3, v4}, Lcom/android/internal/view/menu/MenuBuilder;->findItem(I)Landroid/view/MenuItem;

    #@e
    move-result-object v0

    #@f
    .line 505
    .local v0, item:Landroid/view/MenuItem;
    if-eqz v0, :cond_1c

    #@11
    .line 506
    invoke-interface {v0}, Landroid/view/MenuItem;->getSubMenu()Landroid/view/SubMenu;

    #@14
    move-result-object v2

    #@15
    check-cast v2, Lcom/android/internal/view/menu/SubMenuBuilder;

    #@17
    .line 507
    .local v2, subMenu:Lcom/android/internal/view/menu/SubMenuBuilder;
    if-eqz v2, :cond_1c

    #@19
    invoke-virtual {p0, v2}, Lcom/android/internal/view/menu/ActionMenuPresenter;->onSubMenuSelected(Lcom/android/internal/view/menu/SubMenuBuilder;)Z

    #@1c
    .line 510
    .end local v0           #item:Landroid/view/MenuItem;
    .end local v2           #subMenu:Lcom/android/internal/view/menu/SubMenuBuilder;
    :cond_1c
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .registers 3

    #@0
    .prologue
    .line 495
    new-instance v0, Lcom/android/internal/view/menu/ActionMenuPresenter$SavedState;

    #@2
    invoke-direct {v0}, Lcom/android/internal/view/menu/ActionMenuPresenter$SavedState;-><init>()V

    #@5
    .line 496
    .local v0, state:Lcom/android/internal/view/menu/ActionMenuPresenter$SavedState;
    iget v1, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mOpenSubMenuId:I

    #@7
    iput v1, v0, Lcom/android/internal/view/menu/ActionMenuPresenter$SavedState;->openSubMenuId:I

    #@9
    .line 497
    return-object v0
.end method

.method public onSubMenuSelected(Lcom/android/internal/view/menu/SubMenuBuilder;)Z
    .registers 7
    .parameter "subMenu"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 240
    invoke-virtual {p1}, Lcom/android/internal/view/menu/SubMenuBuilder;->hasVisibleItems()Z

    #@4
    move-result v3

    #@5
    if-nez v3, :cond_8

    #@7
    .line 257
    :cond_7
    :goto_7
    return v2

    #@8
    .line 242
    :cond_8
    move-object v1, p1

    #@9
    .line 243
    .local v1, topSubMenu:Lcom/android/internal/view/menu/SubMenuBuilder;
    :goto_9
    invoke-virtual {v1}, Lcom/android/internal/view/menu/SubMenuBuilder;->getParentMenu()Landroid/view/Menu;

    #@c
    move-result-object v3

    #@d
    iget-object v4, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@f
    if-eq v3, v4, :cond_18

    #@11
    .line 244
    invoke-virtual {v1}, Lcom/android/internal/view/menu/SubMenuBuilder;->getParentMenu()Landroid/view/Menu;

    #@14
    move-result-object v1

    #@15
    .end local v1           #topSubMenu:Lcom/android/internal/view/menu/SubMenuBuilder;
    check-cast v1, Lcom/android/internal/view/menu/SubMenuBuilder;

    #@17
    .restart local v1       #topSubMenu:Lcom/android/internal/view/menu/SubMenuBuilder;
    goto :goto_9

    #@18
    .line 246
    :cond_18
    invoke-virtual {v1}, Lcom/android/internal/view/menu/SubMenuBuilder;->getItem()Landroid/view/MenuItem;

    #@1b
    move-result-object v3

    #@1c
    invoke-direct {p0, v3}, Lcom/android/internal/view/menu/ActionMenuPresenter;->findViewForItem(Landroid/view/MenuItem;)Landroid/view/View;

    #@1f
    move-result-object v0

    #@20
    .line 247
    .local v0, anchor:Landroid/view/View;
    if-nez v0, :cond_28

    #@22
    .line 248
    iget-object v3, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mOverflowButton:Landroid/view/View;

    #@24
    if-eqz v3, :cond_7

    #@26
    .line 249
    iget-object v0, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mOverflowButton:Landroid/view/View;

    #@28
    .line 252
    :cond_28
    invoke-virtual {p1}, Lcom/android/internal/view/menu/SubMenuBuilder;->getItem()Landroid/view/MenuItem;

    #@2b
    move-result-object v2

    #@2c
    invoke-interface {v2}, Landroid/view/MenuItem;->getItemId()I

    #@2f
    move-result v2

    #@30
    iput v2, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mOpenSubMenuId:I

    #@32
    .line 253
    new-instance v2, Lcom/android/internal/view/menu/ActionMenuPresenter$ActionButtonSubmenu;

    #@34
    iget-object v3, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mContext:Landroid/content/Context;

    #@36
    invoke-direct {v2, p0, v3, p1}, Lcom/android/internal/view/menu/ActionMenuPresenter$ActionButtonSubmenu;-><init>(Lcom/android/internal/view/menu/ActionMenuPresenter;Landroid/content/Context;Lcom/android/internal/view/menu/SubMenuBuilder;)V

    #@39
    iput-object v2, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mActionButtonPopup:Lcom/android/internal/view/menu/ActionMenuPresenter$ActionButtonSubmenu;

    #@3b
    .line 254
    iget-object v2, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mActionButtonPopup:Lcom/android/internal/view/menu/ActionMenuPresenter$ActionButtonSubmenu;

    #@3d
    invoke-virtual {v2, v0}, Lcom/android/internal/view/menu/ActionMenuPresenter$ActionButtonSubmenu;->setAnchorView(Landroid/view/View;)V

    #@40
    .line 255
    iget-object v2, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mActionButtonPopup:Lcom/android/internal/view/menu/ActionMenuPresenter$ActionButtonSubmenu;

    #@42
    invoke-virtual {v2}, Lcom/android/internal/view/menu/ActionMenuPresenter$ActionButtonSubmenu;->show()V

    #@45
    .line 256
    invoke-super {p0, p1}, Lcom/android/internal/view/menu/BaseMenuPresenter;->onSubMenuSelected(Lcom/android/internal/view/menu/SubMenuBuilder;)Z

    #@48
    .line 257
    const/4 v2, 0x1

    #@49
    goto :goto_7
.end method

.method public onSubUiVisibilityChanged(Z)V
    .registers 4
    .parameter "isVisible"

    #@0
    .prologue
    .line 514
    if-eqz p1, :cond_7

    #@2
    .line 516
    const/4 v0, 0x0

    #@3
    invoke-super {p0, v0}, Lcom/android/internal/view/menu/BaseMenuPresenter;->onSubMenuSelected(Lcom/android/internal/view/menu/SubMenuBuilder;)Z

    #@6
    .line 520
    :goto_6
    return-void

    #@7
    .line 518
    :cond_7
    iget-object v0, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@9
    const/4 v1, 0x0

    #@a
    invoke-virtual {v0, v1}, Lcom/android/internal/view/menu/MenuBuilder;->close(Z)V

    #@d
    goto :goto_6
.end method

.method public setExpandedActionViewsExclusive(Z)V
    .registers 2
    .parameter "isExclusive"

    #@0
    .prologue
    .line 143
    iput-boolean p1, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mExpandedActionViewsExclusive:Z

    #@2
    .line 144
    return-void
.end method

.method public setItemLimit(I)V
    .registers 3
    .parameter "itemCount"

    #@0
    .prologue
    .line 138
    iput p1, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mMaxItems:I

    #@2
    .line 139
    const/4 v0, 0x1

    #@3
    iput-boolean v0, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mMaxItemsSet:Z

    #@5
    .line 140
    return-void
.end method

.method public setReserveOverflow(Z)V
    .registers 3
    .parameter "reserveOverflow"

    #@0
    .prologue
    .line 133
    iput-boolean p1, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mReserveOverflow:Z

    #@2
    .line 134
    const/4 v0, 0x1

    #@3
    iput-boolean v0, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mReserveOverflowSet:Z

    #@5
    .line 135
    return-void
.end method

.method public setWidthLimit(IZ)V
    .registers 4
    .parameter "width"
    .parameter "strict"

    #@0
    .prologue
    .line 127
    iput p1, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mWidthLimit:I

    #@2
    .line 128
    iput-boolean p2, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mStrictWidthLimit:Z

    #@4
    .line 129
    const/4 v0, 0x1

    #@5
    iput-boolean v0, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mWidthLimitSet:Z

    #@7
    .line 130
    return-void
.end method

.method public shouldIncludeItem(ILcom/android/internal/view/menu/MenuItemImpl;)Z
    .registers 4
    .parameter "childIndex"
    .parameter "item"

    #@0
    .prologue
    .line 183
    invoke-virtual {p2}, Lcom/android/internal/view/menu/MenuItemImpl;->isActionButton()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public showOverflowMenu()Z
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 280
    iget-boolean v1, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mReserveOverflow:Z

    #@3
    if-eqz v1, :cond_44

    #@5
    invoke-virtual {p0}, Lcom/android/internal/view/menu/ActionMenuPresenter;->isOverflowMenuShowing()Z

    #@8
    move-result v1

    #@9
    if-nez v1, :cond_44

    #@b
    iget-object v1, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@d
    if-eqz v1, :cond_44

    #@f
    iget-object v1, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mMenuView:Lcom/android/internal/view/menu/MenuView;

    #@11
    if-eqz v1, :cond_44

    #@13
    iget-object v1, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mPostedOpenRunnable:Lcom/android/internal/view/menu/ActionMenuPresenter$OpenOverflowRunnable;

    #@15
    if-nez v1, :cond_44

    #@17
    iget-object v1, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@19
    invoke-virtual {v1}, Lcom/android/internal/view/menu/MenuBuilder;->getNonActionItems()Ljava/util/ArrayList;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    #@20
    move-result v1

    #@21
    if-nez v1, :cond_44

    #@23
    .line 282
    new-instance v0, Lcom/android/internal/view/menu/ActionMenuPresenter$OverflowPopup;

    #@25
    iget-object v2, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mContext:Landroid/content/Context;

    #@27
    iget-object v3, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@29
    iget-object v4, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mOverflowButton:Landroid/view/View;

    #@2b
    move-object v1, p0

    #@2c
    invoke-direct/range {v0 .. v5}, Lcom/android/internal/view/menu/ActionMenuPresenter$OverflowPopup;-><init>(Lcom/android/internal/view/menu/ActionMenuPresenter;Landroid/content/Context;Lcom/android/internal/view/menu/MenuBuilder;Landroid/view/View;Z)V

    #@2f
    .line 283
    .local v0, popup:Lcom/android/internal/view/menu/ActionMenuPresenter$OverflowPopup;
    new-instance v1, Lcom/android/internal/view/menu/ActionMenuPresenter$OpenOverflowRunnable;

    #@31
    invoke-direct {v1, p0, v0}, Lcom/android/internal/view/menu/ActionMenuPresenter$OpenOverflowRunnable;-><init>(Lcom/android/internal/view/menu/ActionMenuPresenter;Lcom/android/internal/view/menu/ActionMenuPresenter$OverflowPopup;)V

    #@34
    iput-object v1, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mPostedOpenRunnable:Lcom/android/internal/view/menu/ActionMenuPresenter$OpenOverflowRunnable;

    #@36
    .line 285
    iget-object v1, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mMenuView:Lcom/android/internal/view/menu/MenuView;

    #@38
    check-cast v1, Landroid/view/View;

    #@3a
    iget-object v2, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mPostedOpenRunnable:Lcom/android/internal/view/menu/ActionMenuPresenter$OpenOverflowRunnable;

    #@3c
    invoke-virtual {v1, v2}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    #@3f
    .line 289
    const/4 v1, 0x0

    #@40
    invoke-super {p0, v1}, Lcom/android/internal/view/menu/BaseMenuPresenter;->onSubMenuSelected(Lcom/android/internal/view/menu/SubMenuBuilder;)Z

    #@43
    .line 293
    .end local v0           #popup:Lcom/android/internal/view/menu/ActionMenuPresenter$OverflowPopup;
    :goto_43
    return v5

    #@44
    :cond_44
    const/4 v5, 0x0

    #@45
    goto :goto_43
.end method

.method public updateMenuView(Z)V
    .registers 13
    .parameter "cleared"

    #@0
    .prologue
    const/4 v9, 0x1

    #@1
    const/4 v10, 0x0

    #@2
    .line 188
    invoke-super {p0, p1}, Lcom/android/internal/view/menu/BaseMenuPresenter;->updateMenuView(Z)V

    #@5
    .line 190
    iget-object v8, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@7
    if-eqz v8, :cond_28

    #@9
    .line 191
    iget-object v8, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@b
    invoke-virtual {v8}, Lcom/android/internal/view/menu/MenuBuilder;->getActionItems()Ljava/util/ArrayList;

    #@e
    move-result-object v0

    #@f
    .line 192
    .local v0, actionItems:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/view/menu/MenuItemImpl;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@12
    move-result v1

    #@13
    .line 193
    .local v1, count:I
    const/4 v3, 0x0

    #@14
    .local v3, i:I
    :goto_14
    if-ge v3, v1, :cond_28

    #@16
    .line 194
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@19
    move-result-object v8

    #@1a
    check-cast v8, Lcom/android/internal/view/menu/MenuItemImpl;

    #@1c
    invoke-virtual {v8}, Lcom/android/internal/view/menu/MenuItemImpl;->getActionProvider()Landroid/view/ActionProvider;

    #@1f
    move-result-object v7

    #@20
    .line 195
    .local v7, provider:Landroid/view/ActionProvider;
    if-eqz v7, :cond_25

    #@22
    .line 196
    invoke-virtual {v7, p0}, Landroid/view/ActionProvider;->setSubUiVisibilityListener(Landroid/view/ActionProvider$SubUiVisibilityListener;)V

    #@25
    .line 193
    :cond_25
    add-int/lit8 v3, v3, 0x1

    #@27
    goto :goto_14

    #@28
    .line 201
    .end local v0           #actionItems:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/view/menu/MenuItemImpl;>;"
    .end local v1           #count:I
    .end local v3           #i:I
    .end local v7           #provider:Landroid/view/ActionProvider;
    :cond_28
    iget-object v8, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@2a
    if-eqz v8, :cond_85

    #@2c
    iget-object v8, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@2e
    invoke-virtual {v8}, Lcom/android/internal/view/menu/MenuBuilder;->getNonActionItems()Ljava/util/ArrayList;

    #@31
    move-result-object v5

    #@32
    .line 204
    .local v5, nonActionItems:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/view/menu/MenuItemImpl;>;"
    :goto_32
    const/4 v2, 0x0

    #@33
    .line 205
    .local v2, hasOverflow:Z
    iget-boolean v8, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mReserveOverflow:Z

    #@35
    if-eqz v8, :cond_4c

    #@37
    if-eqz v5, :cond_4c

    #@39
    .line 206
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@3c
    move-result v1

    #@3d
    .line 207
    .restart local v1       #count:I
    if-ne v1, v9, :cond_89

    #@3f
    .line 208
    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@42
    move-result-object v8

    #@43
    check-cast v8, Lcom/android/internal/view/menu/MenuItemImpl;

    #@45
    invoke-virtual {v8}, Lcom/android/internal/view/menu/MenuItemImpl;->isActionViewExpanded()Z

    #@48
    move-result v8

    #@49
    if-nez v8, :cond_87

    #@4b
    move v2, v9

    #@4c
    .line 214
    .end local v1           #count:I
    :cond_4c
    :goto_4c
    if-eqz v2, :cond_8f

    #@4e
    .line 215
    iget-object v8, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mOverflowButton:Landroid/view/View;

    #@50
    if-nez v8, :cond_5b

    #@52
    .line 216
    new-instance v8, Lcom/android/internal/view/menu/ActionMenuPresenter$OverflowMenuButton;

    #@54
    iget-object v9, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mSystemContext:Landroid/content/Context;

    #@56
    invoke-direct {v8, p0, v9}, Lcom/android/internal/view/menu/ActionMenuPresenter$OverflowMenuButton;-><init>(Lcom/android/internal/view/menu/ActionMenuPresenter;Landroid/content/Context;)V

    #@59
    iput-object v8, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mOverflowButton:Landroid/view/View;

    #@5b
    .line 218
    :cond_5b
    iget-object v8, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mOverflowButton:Landroid/view/View;

    #@5d
    invoke-virtual {v8}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    #@60
    move-result-object v6

    #@61
    check-cast v6, Landroid/view/ViewGroup;

    #@63
    .line 219
    .local v6, parent:Landroid/view/ViewGroup;
    iget-object v8, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mMenuView:Lcom/android/internal/view/menu/MenuView;

    #@65
    if-eq v6, v8, :cond_7b

    #@67
    .line 220
    if-eqz v6, :cond_6e

    #@69
    .line 221
    iget-object v8, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mOverflowButton:Landroid/view/View;

    #@6b
    invoke-virtual {v6, v8}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    #@6e
    .line 223
    :cond_6e
    iget-object v4, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mMenuView:Lcom/android/internal/view/menu/MenuView;

    #@70
    check-cast v4, Lcom/android/internal/view/menu/ActionMenuView;

    #@72
    .line 224
    .local v4, menuView:Lcom/android/internal/view/menu/ActionMenuView;
    iget-object v8, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mOverflowButton:Landroid/view/View;

    #@74
    invoke-virtual {v4}, Lcom/android/internal/view/menu/ActionMenuView;->generateOverflowButtonLayoutParams()Lcom/android/internal/view/menu/ActionMenuView$LayoutParams;

    #@77
    move-result-object v9

    #@78
    invoke-virtual {v4, v8, v9}, Lcom/android/internal/view/menu/ActionMenuView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@7b
    .line 230
    .end local v4           #menuView:Lcom/android/internal/view/menu/ActionMenuView;
    .end local v6           #parent:Landroid/view/ViewGroup;
    :cond_7b
    :goto_7b
    iget-object v8, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mMenuView:Lcom/android/internal/view/menu/MenuView;

    #@7d
    check-cast v8, Lcom/android/internal/view/menu/ActionMenuView;

    #@7f
    iget-boolean v9, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mReserveOverflow:Z

    #@81
    invoke-virtual {v8, v9}, Lcom/android/internal/view/menu/ActionMenuView;->setOverflowReserved(Z)V

    #@84
    .line 231
    return-void

    #@85
    .line 201
    .end local v2           #hasOverflow:Z
    .end local v5           #nonActionItems:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/view/menu/MenuItemImpl;>;"
    :cond_85
    const/4 v5, 0x0

    #@86
    goto :goto_32

    #@87
    .restart local v1       #count:I
    .restart local v2       #hasOverflow:Z
    .restart local v5       #nonActionItems:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/view/menu/MenuItemImpl;>;"
    :cond_87
    move v2, v10

    #@88
    .line 208
    goto :goto_4c

    #@89
    .line 210
    :cond_89
    if-lez v1, :cond_8d

    #@8b
    move v2, v9

    #@8c
    :goto_8c
    goto :goto_4c

    #@8d
    :cond_8d
    move v2, v10

    #@8e
    goto :goto_8c

    #@8f
    .line 226
    .end local v1           #count:I
    :cond_8f
    iget-object v8, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mOverflowButton:Landroid/view/View;

    #@91
    if-eqz v8, :cond_7b

    #@93
    iget-object v8, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mOverflowButton:Landroid/view/View;

    #@95
    invoke-virtual {v8}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    #@98
    move-result-object v8

    #@99
    iget-object v9, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mMenuView:Lcom/android/internal/view/menu/MenuView;

    #@9b
    if-ne v8, v9, :cond_7b

    #@9d
    .line 227
    iget-object v8, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mMenuView:Lcom/android/internal/view/menu/MenuView;

    #@9f
    check-cast v8, Landroid/view/ViewGroup;

    #@a1
    iget-object v9, p0, Lcom/android/internal/view/menu/ActionMenuPresenter;->mOverflowButton:Landroid/view/View;

    #@a3
    invoke-virtual {v8, v9}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    #@a6
    goto :goto_7b
.end method
