.class public abstract Lcom/android/internal/view/RotationPolicy$RotationPolicyListener;
.super Ljava/lang/Object;
.source "RotationPolicy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/view/RotationPolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "RotationPolicyListener"
.end annotation


# instance fields
.field final mObserver:Landroid/database/ContentObserver;


# direct methods
.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    .line 160
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 161
    new-instance v0, Lcom/android/internal/view/RotationPolicy$RotationPolicyListener$1;

    #@5
    new-instance v1, Landroid/os/Handler;

    #@7
    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    #@a
    invoke-direct {v0, p0, v1}, Lcom/android/internal/view/RotationPolicy$RotationPolicyListener$1;-><init>(Lcom/android/internal/view/RotationPolicy$RotationPolicyListener;Landroid/os/Handler;)V

    #@d
    iput-object v0, p0, Lcom/android/internal/view/RotationPolicy$RotationPolicyListener;->mObserver:Landroid/database/ContentObserver;

    #@f
    return-void
.end method


# virtual methods
.method public abstract onChange()V
.end method
