.class public final Lcom/android/internal/view/menu/MenuItemImpl;
.super Ljava/lang/Object;
.source "MenuItemImpl.java"

# interfaces
.implements Landroid/view/MenuItem;


# static fields
.field private static final CHECKABLE:I = 0x1

.field private static final CHECKED:I = 0x2

.field private static final ENABLED:I = 0x10

.field private static final EXCLUSIVE:I = 0x4

.field private static final HIDDEN:I = 0x8

.field private static final IS_ACTION:I = 0x20

.field static final NO_ICON:I = 0x0

.field private static final SHOW_AS_ACTION_MASK:I = 0x3

.field private static final TAG:Ljava/lang/String; = "MenuItemImpl"

.field private static sDeleteShortcutLabel:Ljava/lang/String;

.field private static sEnterShortcutLabel:Ljava/lang/String;

.field private static sPrependShortcutLabel:Ljava/lang/String;

.field private static sSpaceShortcutLabel:Ljava/lang/String;


# instance fields
.field private mActionProvider:Landroid/view/ActionProvider;

.field private mActionView:Landroid/view/View;

.field private final mCategoryOrder:I

.field private mClickListener:Landroid/view/MenuItem$OnMenuItemClickListener;

.field private mFlags:I

.field private final mGroup:I

.field private mIconDrawable:Landroid/graphics/drawable/Drawable;

.field private mIconResId:I

.field private final mId:I

.field private mIntent:Landroid/content/Intent;

.field private mIsActionViewExpanded:Z

.field private mItemCallback:Ljava/lang/Runnable;

.field private mMenu:Lcom/android/internal/view/menu/MenuBuilder;

.field private mMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

.field private mOnActionExpandListener:Landroid/view/MenuItem$OnActionExpandListener;

.field private final mOrdering:I

.field private mShortcutAlphabeticChar:C

.field private mShortcutNumericChar:C

.field private mShowAsAction:I

.field private mSubMenu:Lcom/android/internal/view/menu/SubMenuBuilder;

.field private mTitle:Ljava/lang/CharSequence;

.field private mTitleCondensed:Ljava/lang/CharSequence;


# direct methods
.method constructor <init>(Lcom/android/internal/view/menu/MenuBuilder;IIIILjava/lang/CharSequence;I)V
    .registers 10
    .parameter "menu"
    .parameter "group"
    .parameter "id"
    .parameter "categoryOrder"
    .parameter "ordering"
    .parameter "title"
    .parameter "showAsAction"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 115
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 62
    iput v1, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mIconResId:I

    #@6
    .line 72
    const/16 v0, 0x10

    #@8
    iput v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mFlags:I

    #@a
    .line 80
    iput v1, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mShowAsAction:I

    #@c
    .line 85
    iput-boolean v1, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mIsActionViewExpanded:Z

    #@e
    .line 117
    sget-object v0, Lcom/android/internal/view/menu/MenuItemImpl;->sPrependShortcutLabel:Ljava/lang/String;

    #@10
    if-nez v0, :cond_56

    #@12
    .line 119
    invoke-virtual {p1}, Lcom/android/internal/view/menu/MenuBuilder;->getContext()Landroid/content/Context;

    #@15
    move-result-object v0

    #@16
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@19
    move-result-object v0

    #@1a
    const v1, 0x10403c4

    #@1d
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@20
    move-result-object v0

    #@21
    sput-object v0, Lcom/android/internal/view/menu/MenuItemImpl;->sPrependShortcutLabel:Ljava/lang/String;

    #@23
    .line 121
    invoke-virtual {p1}, Lcom/android/internal/view/menu/MenuBuilder;->getContext()Landroid/content/Context;

    #@26
    move-result-object v0

    #@27
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@2a
    move-result-object v0

    #@2b
    const v1, 0x10403c6

    #@2e
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@31
    move-result-object v0

    #@32
    sput-object v0, Lcom/android/internal/view/menu/MenuItemImpl;->sEnterShortcutLabel:Ljava/lang/String;

    #@34
    .line 123
    invoke-virtual {p1}, Lcom/android/internal/view/menu/MenuBuilder;->getContext()Landroid/content/Context;

    #@37
    move-result-object v0

    #@38
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@3b
    move-result-object v0

    #@3c
    const v1, 0x10403c7

    #@3f
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@42
    move-result-object v0

    #@43
    sput-object v0, Lcom/android/internal/view/menu/MenuItemImpl;->sDeleteShortcutLabel:Ljava/lang/String;

    #@45
    .line 125
    invoke-virtual {p1}, Lcom/android/internal/view/menu/MenuBuilder;->getContext()Landroid/content/Context;

    #@48
    move-result-object v0

    #@49
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@4c
    move-result-object v0

    #@4d
    const v1, 0x10403c5

    #@50
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@53
    move-result-object v0

    #@54
    sput-object v0, Lcom/android/internal/view/menu/MenuItemImpl;->sSpaceShortcutLabel:Ljava/lang/String;

    #@56
    .line 129
    :cond_56
    iput-object p1, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@58
    .line 130
    iput p3, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mId:I

    #@5a
    .line 131
    iput p2, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mGroup:I

    #@5c
    .line 132
    iput p4, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mCategoryOrder:I

    #@5e
    .line 133
    iput p5, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mOrdering:I

    #@60
    .line 134
    iput-object p6, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mTitle:Ljava/lang/CharSequence;

    #@62
    .line 135
    iput p7, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mShowAsAction:I

    #@64
    .line 136
    return-void
.end method

.method static synthetic access$000(Lcom/android/internal/view/menu/MenuItemImpl;)Lcom/android/internal/view/menu/MenuBuilder;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 38
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@2
    return-object v0
.end method


# virtual methods
.method public actionFormatChanged()V
    .registers 2

    #@0
    .prologue
    .line 508
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@2
    invoke-virtual {v0, p0}, Lcom/android/internal/view/menu/MenuBuilder;->onItemActionRequestChanged(Lcom/android/internal/view/menu/MenuItemImpl;)V

    #@5
    .line 509
    return-void
.end method

.method public collapseActionView()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 628
    iget v1, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mShowAsAction:I

    #@3
    and-int/lit8 v1, v1, 0x8

    #@5
    if-nez v1, :cond_8

    #@7
    .line 641
    :cond_7
    :goto_7
    return v0

    #@8
    .line 631
    :cond_8
    iget-object v1, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mActionView:Landroid/view/View;

    #@a
    if-nez v1, :cond_e

    #@c
    .line 633
    const/4 v0, 0x1

    #@d
    goto :goto_7

    #@e
    .line 636
    :cond_e
    iget-object v1, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mOnActionExpandListener:Landroid/view/MenuItem$OnActionExpandListener;

    #@10
    if-eqz v1, :cond_1a

    #@12
    iget-object v1, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mOnActionExpandListener:Landroid/view/MenuItem$OnActionExpandListener;

    #@14
    invoke-interface {v1, p0}, Landroid/view/MenuItem$OnActionExpandListener;->onMenuItemActionCollapse(Landroid/view/MenuItem;)Z

    #@17
    move-result v1

    #@18
    if-eqz v1, :cond_7

    #@1a
    .line 638
    :cond_1a
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@1c
    invoke-virtual {v0, p0}, Lcom/android/internal/view/menu/MenuBuilder;->collapseItemActionView(Lcom/android/internal/view/menu/MenuItemImpl;)Z

    #@1f
    move-result v0

    #@20
    goto :goto_7
.end method

.method public expandActionView()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 614
    iget v1, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mShowAsAction:I

    #@3
    and-int/lit8 v1, v1, 0x8

    #@5
    if-eqz v1, :cond_b

    #@7
    iget-object v1, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mActionView:Landroid/view/View;

    #@9
    if-nez v1, :cond_c

    #@b
    .line 623
    :cond_b
    :goto_b
    return v0

    #@c
    .line 618
    :cond_c
    iget-object v1, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mOnActionExpandListener:Landroid/view/MenuItem$OnActionExpandListener;

    #@e
    if-eqz v1, :cond_18

    #@10
    iget-object v1, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mOnActionExpandListener:Landroid/view/MenuItem$OnActionExpandListener;

    #@12
    invoke-interface {v1, p0}, Landroid/view/MenuItem$OnActionExpandListener;->onMenuItemActionExpand(Landroid/view/MenuItem;)Z

    #@15
    move-result v1

    #@16
    if-eqz v1, :cond_b

    #@18
    .line 620
    :cond_18
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@1a
    invoke-virtual {v0, p0}, Lcom/android/internal/view/menu/MenuBuilder;->expandItemActionView(Lcom/android/internal/view/menu/MenuItemImpl;)Z

    #@1d
    move-result v0

    #@1e
    goto :goto_b
.end method

.method public getActionProvider()Landroid/view/ActionProvider;
    .registers 2

    #@0
    .prologue
    .line 588
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mActionProvider:Landroid/view/ActionProvider;

    #@2
    return-object v0
.end method

.method public getActionView()Landroid/view/View;
    .registers 2

    #@0
    .prologue
    .line 577
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mActionView:Landroid/view/View;

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 578
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mActionView:Landroid/view/View;

    #@6
    .line 583
    :goto_6
    return-object v0

    #@7
    .line 579
    :cond_7
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mActionProvider:Landroid/view/ActionProvider;

    #@9
    if-eqz v0, :cond_16

    #@b
    .line 580
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mActionProvider:Landroid/view/ActionProvider;

    #@d
    invoke-virtual {v0, p0}, Landroid/view/ActionProvider;->onCreateActionView(Landroid/view/MenuItem;)Landroid/view/View;

    #@10
    move-result-object v0

    #@11
    iput-object v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mActionView:Landroid/view/View;

    #@13
    .line 581
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mActionView:Landroid/view/View;

    #@15
    goto :goto_6

    #@16
    .line 583
    :cond_16
    const/4 v0, 0x0

    #@17
    goto :goto_6
.end method

.method public getAlphabeticShortcut()C
    .registers 2

    #@0
    .prologue
    .line 226
    iget-char v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mShortcutAlphabeticChar:C

    #@2
    return v0
.end method

.method getCallback()Ljava/lang/Runnable;
    .registers 2

    #@0
    .prologue
    .line 217
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mItemCallback:Ljava/lang/Runnable;

    #@2
    return-object v0
.end method

.method public getGroupId()I
    .registers 2

    #@0
    .prologue
    .line 191
    iget v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mGroup:I

    #@2
    return v0
.end method

.method public getIcon()Landroid/graphics/drawable/Drawable;
    .registers 4

    #@0
    .prologue
    .line 380
    iget-object v1, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mIconDrawable:Landroid/graphics/drawable/Drawable;

    #@2
    if-eqz v1, :cond_7

    #@4
    .line 381
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mIconDrawable:Landroid/graphics/drawable/Drawable;

    #@6
    .line 391
    :goto_6
    return-object v0

    #@7
    .line 384
    :cond_7
    iget v1, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mIconResId:I

    #@9
    if-eqz v1, :cond_1d

    #@b
    .line 385
    iget-object v1, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@d
    invoke-virtual {v1}, Lcom/android/internal/view/menu/MenuBuilder;->getResources()Landroid/content/res/Resources;

    #@10
    move-result-object v1

    #@11
    iget v2, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mIconResId:I

    #@13
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@16
    move-result-object v0

    #@17
    .line 386
    .local v0, icon:Landroid/graphics/drawable/Drawable;
    const/4 v1, 0x0

    #@18
    iput v1, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mIconResId:I

    #@1a
    .line 387
    iput-object v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mIconDrawable:Landroid/graphics/drawable/Drawable;

    #@1c
    goto :goto_6

    #@1d
    .line 391
    .end local v0           #icon:Landroid/graphics/drawable/Drawable;
    :cond_1d
    const/4 v0, 0x0

    #@1e
    goto :goto_6
.end method

.method public getIntent()Landroid/content/Intent;
    .registers 2

    #@0
    .prologue
    .line 208
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mIntent:Landroid/content/Intent;

    #@2
    return-object v0
.end method

.method public getItemId()I
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$CapturedViewProperty;
    .end annotation

    #@0
    .prologue
    .line 196
    iget v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mId:I

    #@2
    return v0
.end method

.method public getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;
    .registers 2

    #@0
    .prologue
    .line 504
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

    #@2
    return-object v0
.end method

.method public getNumericShortcut()C
    .registers 2

    #@0
    .prologue
    .line 240
    iget-char v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mShortcutNumericChar:C

    #@2
    return v0
.end method

.method public getOrder()I
    .registers 2

    #@0
    .prologue
    .line 200
    iget v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mCategoryOrder:I

    #@2
    return v0
.end method

.method public getOrdering()I
    .registers 2

    #@0
    .prologue
    .line 204
    iget v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mOrdering:I

    #@2
    return v0
.end method

.method getShortcut()C
    .registers 2

    #@0
    .prologue
    .line 266
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/view/menu/MenuBuilder;->isQwertyMode()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_b

    #@8
    iget-char v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mShortcutAlphabeticChar:C

    #@a
    :goto_a
    return v0

    #@b
    :cond_b
    iget-char v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mShortcutNumericChar:C

    #@d
    goto :goto_a
.end method

.method getShortcutLabel()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 276
    invoke-virtual {p0}, Lcom/android/internal/view/menu/MenuItemImpl;->getShortcut()C

    #@3
    move-result v1

    #@4
    .line 277
    .local v1, shortcut:C
    if-nez v1, :cond_9

    #@6
    .line 278
    const-string v2, ""

    #@8
    .line 301
    :goto_8
    return-object v2

    #@9
    .line 281
    :cond_9
    new-instance v0, Ljava/lang/StringBuilder;

    #@b
    sget-object v2, Lcom/android/internal/view/menu/MenuItemImpl;->sPrependShortcutLabel:Ljava/lang/String;

    #@d
    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@10
    .line 282
    .local v0, sb:Ljava/lang/StringBuilder;
    sparse-switch v1, :sswitch_data_2e

    #@13
    .line 297
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@16
    .line 301
    :goto_16
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v2

    #@1a
    goto :goto_8

    #@1b
    .line 285
    :sswitch_1b
    sget-object v2, Lcom/android/internal/view/menu/MenuItemImpl;->sEnterShortcutLabel:Ljava/lang/String;

    #@1d
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    goto :goto_16

    #@21
    .line 289
    :sswitch_21
    sget-object v2, Lcom/android/internal/view/menu/MenuItemImpl;->sDeleteShortcutLabel:Ljava/lang/String;

    #@23
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    goto :goto_16

    #@27
    .line 293
    :sswitch_27
    sget-object v2, Lcom/android/internal/view/menu/MenuItemImpl;->sSpaceShortcutLabel:Ljava/lang/String;

    #@29
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    goto :goto_16

    #@2d
    .line 282
    nop

    #@2e
    :sswitch_data_2e
    .sparse-switch
        0x8 -> :sswitch_21
        0xa -> :sswitch_1b
        0x20 -> :sswitch_27
    .end sparse-switch
.end method

.method public getSubMenu()Landroid/view/SubMenu;
    .registers 2

    #@0
    .prologue
    .line 315
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mSubMenu:Lcom/android/internal/view/menu/SubMenuBuilder;

    #@2
    return-object v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$CapturedViewProperty;
    .end annotation

    #@0
    .prologue
    .line 330
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mTitle:Ljava/lang/CharSequence;

    #@2
    return-object v0
.end method

.method public getTitleCondensed()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 363
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mTitleCondensed:Ljava/lang/CharSequence;

    #@2
    if-eqz v0, :cond_7

    #@4
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mTitleCondensed:Ljava/lang/CharSequence;

    #@6
    :goto_6
    return-object v0

    #@7
    :cond_7
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mTitle:Ljava/lang/CharSequence;

    #@9
    goto :goto_6
.end method

.method getTitleForItemView(Lcom/android/internal/view/menu/MenuView$ItemView;)Ljava/lang/CharSequence;
    .registers 3
    .parameter "itemView"

    #@0
    .prologue
    .line 341
    if-eqz p1, :cond_d

    #@2
    invoke-interface {p1}, Lcom/android/internal/view/menu/MenuView$ItemView;->prefersCondensedTitle()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_d

    #@8
    invoke-virtual {p0}, Lcom/android/internal/view/menu/MenuItemImpl;->getTitleCondensed()Ljava/lang/CharSequence;

    #@b
    move-result-object v0

    #@c
    :goto_c
    return-object v0

    #@d
    :cond_d
    invoke-virtual {p0}, Lcom/android/internal/view/menu/MenuItemImpl;->getTitle()Ljava/lang/CharSequence;

    #@10
    move-result-object v0

    #@11
    goto :goto_c
.end method

.method public hasCollapsibleActionView()Z
    .registers 2

    #@0
    .prologue
    .line 651
    iget v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mShowAsAction:I

    #@2
    and-int/lit8 v0, v0, 0x8

    #@4
    if-eqz v0, :cond_c

    #@6
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mActionView:Landroid/view/View;

    #@8
    if-eqz v0, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method public hasSubMenu()Z
    .registers 2

    #@0
    .prologue
    .line 319
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mSubMenu:Lcom/android/internal/view/menu/SubMenuBuilder;

    #@2
    if-eqz v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public invoke()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 144
    iget-object v2, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mClickListener:Landroid/view/MenuItem$OnMenuItemClickListener;

    #@3
    if-eqz v2, :cond_e

    #@5
    iget-object v2, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mClickListener:Landroid/view/MenuItem$OnMenuItemClickListener;

    #@7
    invoke-interface {v2, p0}, Landroid/view/MenuItem$OnMenuItemClickListener;->onMenuItemClick(Landroid/view/MenuItem;)Z

    #@a
    move-result v2

    #@b
    if-eqz v2, :cond_e

    #@d
    .line 171
    :cond_d
    :goto_d
    return v1

    #@e
    .line 149
    :cond_e
    iget-object v2, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@10
    iget-object v3, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@12
    invoke-virtual {v3}, Lcom/android/internal/view/menu/MenuBuilder;->getRootMenu()Lcom/android/internal/view/menu/MenuBuilder;

    #@15
    move-result-object v3

    #@16
    invoke-virtual {v2, v3, p0}, Lcom/android/internal/view/menu/MenuBuilder;->dispatchMenuItemSelected(Lcom/android/internal/view/menu/MenuBuilder;Landroid/view/MenuItem;)Z

    #@19
    move-result v2

    #@1a
    if-nez v2, :cond_d

    #@1c
    .line 153
    iget-object v2, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mItemCallback:Ljava/lang/Runnable;

    #@1e
    if-eqz v2, :cond_26

    #@20
    .line 154
    iget-object v2, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mItemCallback:Ljava/lang/Runnable;

    #@22
    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    #@25
    goto :goto_d

    #@26
    .line 158
    :cond_26
    iget-object v2, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mIntent:Landroid/content/Intent;

    #@28
    if-eqz v2, :cond_3e

    #@2a
    .line 160
    :try_start_2a
    iget-object v2, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@2c
    invoke-virtual {v2}, Lcom/android/internal/view/menu/MenuBuilder;->getContext()Landroid/content/Context;

    #@2f
    move-result-object v2

    #@30
    iget-object v3, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mIntent:Landroid/content/Intent;

    #@32
    invoke-virtual {v2, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_35
    .catch Landroid/content/ActivityNotFoundException; {:try_start_2a .. :try_end_35} :catch_36

    #@35
    goto :goto_d

    #@36
    .line 162
    :catch_36
    move-exception v0

    #@37
    .line 163
    .local v0, e:Landroid/content/ActivityNotFoundException;
    const-string v2, "MenuItemImpl"

    #@39
    const-string v3, "Can\'t find activity to handle intent; ignoring"

    #@3b
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@3e
    .line 167
    .end local v0           #e:Landroid/content/ActivityNotFoundException;
    :cond_3e
    iget-object v2, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mActionProvider:Landroid/view/ActionProvider;

    #@40
    if-eqz v2, :cond_4a

    #@42
    iget-object v2, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mActionProvider:Landroid/view/ActionProvider;

    #@44
    invoke-virtual {v2}, Landroid/view/ActionProvider;->onPerformDefaultAction()Z

    #@47
    move-result v2

    #@48
    if-nez v2, :cond_d

    #@4a
    .line 171
    :cond_4a
    const/4 v1, 0x0

    #@4b
    goto :goto_d
.end method

.method public isActionButton()Z
    .registers 3

    #@0
    .prologue
    .line 519
    iget v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mFlags:I

    #@2
    and-int/lit8 v0, v0, 0x20

    #@4
    const/16 v1, 0x20

    #@6
    if-ne v0, v1, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public isActionViewExpanded()Z
    .registers 2

    #@0
    .prologue
    .line 660
    iget-boolean v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mIsActionViewExpanded:Z

    #@2
    return v0
.end method

.method public isCheckable()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 413
    iget v1, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mFlags:I

    #@3
    and-int/lit8 v1, v1, 0x1

    #@5
    if-ne v1, v0, :cond_8

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public isChecked()Z
    .registers 3

    #@0
    .prologue
    .line 435
    iget v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mFlags:I

    #@2
    and-int/lit8 v0, v0, 0x2

    #@4
    const/4 v1, 0x2

    #@5
    if-ne v0, v1, :cond_9

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public isEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 175
    iget v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mFlags:I

    #@2
    and-int/lit8 v0, v0, 0x10

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public isExclusiveCheckable()Z
    .registers 2

    #@0
    .prologue
    .line 431
    iget v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mFlags:I

    #@2
    and-int/lit8 v0, v0, 0x4

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public isVisible()Z
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 459
    iget-object v2, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mActionProvider:Landroid/view/ActionProvider;

    #@4
    if-eqz v2, :cond_1f

    #@6
    iget-object v2, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mActionProvider:Landroid/view/ActionProvider;

    #@8
    invoke-virtual {v2}, Landroid/view/ActionProvider;->overridesItemVisibility()Z

    #@b
    move-result v2

    #@c
    if-eqz v2, :cond_1f

    #@e
    .line 460
    iget v2, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mFlags:I

    #@10
    and-int/lit8 v2, v2, 0x8

    #@12
    if-nez v2, :cond_1d

    #@14
    iget-object v2, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mActionProvider:Landroid/view/ActionProvider;

    #@16
    invoke-virtual {v2}, Landroid/view/ActionProvider;->isVisible()Z

    #@19
    move-result v2

    #@1a
    if-eqz v2, :cond_1d

    #@1c
    .line 462
    :cond_1c
    :goto_1c
    return v0

    #@1d
    :cond_1d
    move v0, v1

    #@1e
    .line 460
    goto :goto_1c

    #@1f
    .line 462
    :cond_1f
    iget v2, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mFlags:I

    #@21
    and-int/lit8 v2, v2, 0x8

    #@23
    if-eqz v2, :cond_1c

    #@25
    move v0, v1

    #@26
    goto :goto_1c
.end method

.method public requestsActionButton()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 523
    iget v1, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mShowAsAction:I

    #@3
    and-int/lit8 v1, v1, 0x1

    #@5
    if-ne v1, v0, :cond_8

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public requiresActionButton()Z
    .registers 3

    #@0
    .prologue
    .line 527
    iget v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mShowAsAction:I

    #@2
    and-int/lit8 v0, v0, 0x2

    #@4
    const/4 v1, 0x2

    #@5
    if-ne v0, v1, :cond_9

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public setActionProvider(Landroid/view/ActionProvider;)Landroid/view/MenuItem;
    .registers 4
    .parameter "actionProvider"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 592
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mActionProvider:Landroid/view/ActionProvider;

    #@3
    if-eqz v0, :cond_a

    #@5
    .line 593
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mActionProvider:Landroid/view/ActionProvider;

    #@7
    invoke-virtual {v0, v1}, Landroid/view/ActionProvider;->setVisibilityListener(Landroid/view/ActionProvider$VisibilityListener;)V

    #@a
    .line 595
    :cond_a
    iput-object v1, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mActionView:Landroid/view/View;

    #@c
    .line 596
    iput-object p1, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mActionProvider:Landroid/view/ActionProvider;

    #@e
    .line 597
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@10
    const/4 v1, 0x1

    #@11
    invoke-virtual {v0, v1}, Lcom/android/internal/view/menu/MenuBuilder;->onItemsChanged(Z)V

    #@14
    .line 598
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mActionProvider:Landroid/view/ActionProvider;

    #@16
    new-instance v1, Lcom/android/internal/view/menu/MenuItemImpl$1;

    #@18
    invoke-direct {v1, p0}, Lcom/android/internal/view/menu/MenuItemImpl$1;-><init>(Lcom/android/internal/view/menu/MenuItemImpl;)V

    #@1b
    invoke-virtual {v0, v1}, Landroid/view/ActionProvider;->setVisibilityListener(Landroid/view/ActionProvider$VisibilityListener;)V

    #@1e
    .line 603
    return-object p0
.end method

.method public setActionView(I)Landroid/view/MenuItem;
    .registers 6
    .parameter "resId"

    #@0
    .prologue
    .line 570
    iget-object v2, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@2
    invoke-virtual {v2}, Lcom/android/internal/view/menu/MenuBuilder;->getContext()Landroid/content/Context;

    #@5
    move-result-object v0

    #@6
    .line 571
    .local v0, context:Landroid/content/Context;
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    #@9
    move-result-object v1

    #@a
    .line 572
    .local v1, inflater:Landroid/view/LayoutInflater;
    new-instance v2, Landroid/widget/LinearLayout;

    #@c
    invoke-direct {v2, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    #@f
    const/4 v3, 0x0

    #@10
    invoke-virtual {v1, p1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {p0, v2}, Lcom/android/internal/view/menu/MenuItemImpl;->setActionView(Landroid/view/View;)Landroid/view/MenuItem;

    #@17
    .line 573
    return-object p0
.end method

.method public setActionView(Landroid/view/View;)Landroid/view/MenuItem;
    .registers 4
    .parameter "view"

    #@0
    .prologue
    .line 560
    iput-object p1, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mActionView:Landroid/view/View;

    #@2
    .line 561
    const/4 v0, 0x0

    #@3
    iput-object v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mActionProvider:Landroid/view/ActionProvider;

    #@5
    .line 562
    if-eqz p1, :cond_17

    #@7
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    #@a
    move-result v0

    #@b
    const/4 v1, -0x1

    #@c
    if-ne v0, v1, :cond_17

    #@e
    iget v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mId:I

    #@10
    if-lez v0, :cond_17

    #@12
    .line 563
    iget v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mId:I

    #@14
    invoke-virtual {p1, v0}, Landroid/view/View;->setId(I)V

    #@17
    .line 565
    :cond_17
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@19
    invoke-virtual {v0, p0}, Lcom/android/internal/view/menu/MenuBuilder;->onItemActionRequestChanged(Lcom/android/internal/view/menu/MenuItemImpl;)V

    #@1c
    .line 566
    return-object p0
.end method

.method public setActionViewExpanded(Z)V
    .registers 4
    .parameter "isExpanded"

    #@0
    .prologue
    .line 655
    iput-boolean p1, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mIsActionViewExpanded:Z

    #@2
    .line 656
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@4
    const/4 v1, 0x0

    #@5
    invoke-virtual {v0, v1}, Lcom/android/internal/view/menu/MenuBuilder;->onItemsChanged(Z)V

    #@8
    .line 657
    return-void
.end method

.method public setAlphabeticShortcut(C)Landroid/view/MenuItem;
    .registers 4
    .parameter "alphaChar"

    #@0
    .prologue
    .line 230
    iget-char v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mShortcutAlphabeticChar:C

    #@2
    if-ne v0, p1, :cond_5

    #@4
    .line 236
    :goto_4
    return-object p0

    #@5
    .line 232
    :cond_5
    invoke-static {p1}, Ljava/lang/Character;->toLowerCase(C)C

    #@8
    move-result v0

    #@9
    iput-char v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mShortcutAlphabeticChar:C

    #@b
    .line 234
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@d
    const/4 v1, 0x0

    #@e
    invoke-virtual {v0, v1}, Lcom/android/internal/view/menu/MenuBuilder;->onItemsChanged(Z)V

    #@11
    goto :goto_4
.end method

.method public setCallback(Ljava/lang/Runnable;)Landroid/view/MenuItem;
    .registers 2
    .parameter "callback"

    #@0
    .prologue
    .line 221
    iput-object p1, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mItemCallback:Ljava/lang/Runnable;

    #@2
    .line 222
    return-object p0
.end method

.method public setCheckable(Z)Landroid/view/MenuItem;
    .registers 6
    .parameter "checkable"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 417
    iget v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mFlags:I

    #@3
    .line 418
    .local v0, oldFlags:I
    iget v1, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mFlags:I

    #@5
    and-int/lit8 v3, v1, -0x2

    #@7
    if-eqz p1, :cond_17

    #@9
    const/4 v1, 0x1

    #@a
    :goto_a
    or-int/2addr v1, v3

    #@b
    iput v1, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mFlags:I

    #@d
    .line 419
    iget v1, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mFlags:I

    #@f
    if-eq v0, v1, :cond_16

    #@11
    .line 420
    iget-object v1, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@13
    invoke-virtual {v1, v2}, Lcom/android/internal/view/menu/MenuBuilder;->onItemsChanged(Z)V

    #@16
    .line 423
    :cond_16
    return-object p0

    #@17
    :cond_17
    move v1, v2

    #@18
    .line 418
    goto :goto_a
.end method

.method public setChecked(Z)Landroid/view/MenuItem;
    .registers 3
    .parameter "checked"

    #@0
    .prologue
    .line 439
    iget v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mFlags:I

    #@2
    and-int/lit8 v0, v0, 0x4

    #@4
    if-eqz v0, :cond_c

    #@6
    .line 442
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@8
    invoke-virtual {v0, p0}, Lcom/android/internal/view/menu/MenuBuilder;->setExclusiveItemChecked(Landroid/view/MenuItem;)V

    #@b
    .line 447
    :goto_b
    return-object p0

    #@c
    .line 444
    :cond_c
    invoke-virtual {p0, p1}, Lcom/android/internal/view/menu/MenuItemImpl;->setCheckedInt(Z)V

    #@f
    goto :goto_b
.end method

.method setCheckedInt(Z)V
    .registers 6
    .parameter "checked"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 451
    iget v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mFlags:I

    #@3
    .line 452
    .local v0, oldFlags:I
    iget v1, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mFlags:I

    #@5
    and-int/lit8 v3, v1, -0x3

    #@7
    if-eqz p1, :cond_17

    #@9
    const/4 v1, 0x2

    #@a
    :goto_a
    or-int/2addr v1, v3

    #@b
    iput v1, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mFlags:I

    #@d
    .line 453
    iget v1, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mFlags:I

    #@f
    if-eq v0, v1, :cond_16

    #@11
    .line 454
    iget-object v1, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@13
    invoke-virtual {v1, v2}, Lcom/android/internal/view/menu/MenuBuilder;->onItemsChanged(Z)V

    #@16
    .line 456
    :cond_16
    return-void

    #@17
    :cond_17
    move v1, v2

    #@18
    .line 452
    goto :goto_a
.end method

.method public setEnabled(Z)Landroid/view/MenuItem;
    .registers 4
    .parameter "enabled"

    #@0
    .prologue
    .line 179
    if-eqz p1, :cond_f

    #@2
    .line 180
    iget v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mFlags:I

    #@4
    or-int/lit8 v0, v0, 0x10

    #@6
    iput v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mFlags:I

    #@8
    .line 185
    :goto_8
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@a
    const/4 v1, 0x0

    #@b
    invoke-virtual {v0, v1}, Lcom/android/internal/view/menu/MenuBuilder;->onItemsChanged(Z)V

    #@e
    .line 187
    return-object p0

    #@f
    .line 182
    :cond_f
    iget v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mFlags:I

    #@11
    and-int/lit8 v0, v0, -0x11

    #@13
    iput v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mFlags:I

    #@15
    goto :goto_8
.end method

.method public setExclusiveCheckable(Z)V
    .registers 4
    .parameter "exclusive"

    #@0
    .prologue
    .line 427
    iget v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mFlags:I

    #@2
    and-int/lit8 v1, v0, -0x5

    #@4
    if-eqz p1, :cond_b

    #@6
    const/4 v0, 0x4

    #@7
    :goto_7
    or-int/2addr v0, v1

    #@8
    iput v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mFlags:I

    #@a
    .line 428
    return-void

    #@b
    .line 427
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_7
.end method

.method public setIcon(I)Landroid/view/MenuItem;
    .registers 4
    .parameter "iconResId"

    #@0
    .prologue
    .line 403
    const/4 v0, 0x0

    #@1
    iput-object v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mIconDrawable:Landroid/graphics/drawable/Drawable;

    #@3
    .line 404
    iput p1, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mIconResId:I

    #@5
    .line 407
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@7
    const/4 v1, 0x0

    #@8
    invoke-virtual {v0, v1}, Lcom/android/internal/view/menu/MenuBuilder;->onItemsChanged(Z)V

    #@b
    .line 409
    return-object p0
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;
    .registers 4
    .parameter "icon"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 395
    iput v1, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mIconResId:I

    #@3
    .line 396
    iput-object p1, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mIconDrawable:Landroid/graphics/drawable/Drawable;

    #@5
    .line 397
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@7
    invoke-virtual {v0, v1}, Lcom/android/internal/view/menu/MenuBuilder;->onItemsChanged(Z)V

    #@a
    .line 399
    return-object p0
.end method

.method public setIntent(Landroid/content/Intent;)Landroid/view/MenuItem;
    .registers 2
    .parameter "intent"

    #@0
    .prologue
    .line 212
    iput-object p1, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mIntent:Landroid/content/Intent;

    #@2
    .line 213
    return-object p0
.end method

.method public setIsActionButton(Z)V
    .registers 3
    .parameter "isActionButton"

    #@0
    .prologue
    .line 531
    if-eqz p1, :cond_9

    #@2
    .line 532
    iget v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mFlags:I

    #@4
    or-int/lit8 v0, v0, 0x20

    #@6
    iput v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mFlags:I

    #@8
    .line 536
    :goto_8
    return-void

    #@9
    .line 534
    :cond_9
    iget v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mFlags:I

    #@b
    and-int/lit8 v0, v0, -0x21

    #@d
    iput v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mFlags:I

    #@f
    goto :goto_8
.end method

.method setMenuInfo(Landroid/view/ContextMenu$ContextMenuInfo;)V
    .registers 2
    .parameter "menuInfo"

    #@0
    .prologue
    .line 500
    iput-object p1, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

    #@2
    .line 501
    return-void
.end method

.method public setNumericShortcut(C)Landroid/view/MenuItem;
    .registers 4
    .parameter "numericChar"

    #@0
    .prologue
    .line 244
    iget-char v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mShortcutNumericChar:C

    #@2
    if-ne v0, p1, :cond_5

    #@4
    .line 250
    :goto_4
    return-object p0

    #@5
    .line 246
    :cond_5
    iput-char p1, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mShortcutNumericChar:C

    #@7
    .line 248
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@9
    const/4 v1, 0x0

    #@a
    invoke-virtual {v0, v1}, Lcom/android/internal/view/menu/MenuBuilder;->onItemsChanged(Z)V

    #@d
    goto :goto_4
.end method

.method public setOnActionExpandListener(Landroid/view/MenuItem$OnActionExpandListener;)Landroid/view/MenuItem;
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 646
    iput-object p1, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mOnActionExpandListener:Landroid/view/MenuItem$OnActionExpandListener;

    #@2
    .line 647
    return-object p0
.end method

.method public setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;
    .registers 2
    .parameter "clickListener"

    #@0
    .prologue
    .line 490
    iput-object p1, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mClickListener:Landroid/view/MenuItem$OnMenuItemClickListener;

    #@2
    .line 491
    return-object p0
.end method

.method public setShortcut(CC)Landroid/view/MenuItem;
    .registers 5
    .parameter "numericChar"
    .parameter "alphaChar"

    #@0
    .prologue
    .line 254
    iput-char p1, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mShortcutNumericChar:C

    #@2
    .line 255
    invoke-static {p2}, Ljava/lang/Character;->toLowerCase(C)C

    #@5
    move-result v0

    #@6
    iput-char v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mShortcutAlphabeticChar:C

    #@8
    .line 257
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@a
    const/4 v1, 0x0

    #@b
    invoke-virtual {v0, v1}, Lcom/android/internal/view/menu/MenuBuilder;->onItemsChanged(Z)V

    #@e
    .line 259
    return-object p0
.end method

.method public setShowAsAction(I)V
    .registers 4
    .parameter "actionEnum"

    #@0
    .prologue
    .line 543
    and-int/lit8 v0, p1, 0x3

    #@2
    packed-switch v0, :pswitch_data_16

    #@5
    .line 552
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@7
    const-string v1, "SHOW_AS_ACTION_ALWAYS, SHOW_AS_ACTION_IF_ROOM, and SHOW_AS_ACTION_NEVER are mutually exclusive."

    #@9
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@c
    throw v0

    #@d
    .line 555
    :pswitch_d
    iput p1, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mShowAsAction:I

    #@f
    .line 556
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@11
    invoke-virtual {v0, p0}, Lcom/android/internal/view/menu/MenuBuilder;->onItemActionRequestChanged(Lcom/android/internal/view/menu/MenuItemImpl;)V

    #@14
    .line 557
    return-void

    #@15
    .line 543
    nop

    #@16
    :pswitch_data_16
    .packed-switch 0x0
        :pswitch_d
        :pswitch_d
        :pswitch_d
    .end packed-switch
.end method

.method public setShowAsActionFlags(I)Landroid/view/MenuItem;
    .registers 2
    .parameter "actionEnum"

    #@0
    .prologue
    .line 608
    invoke-virtual {p0, p1}, Lcom/android/internal/view/menu/MenuItemImpl;->setShowAsAction(I)V

    #@3
    .line 609
    return-object p0
.end method

.method setSubMenu(Lcom/android/internal/view/menu/SubMenuBuilder;)V
    .registers 3
    .parameter "subMenu"

    #@0
    .prologue
    .line 323
    iput-object p1, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mSubMenu:Lcom/android/internal/view/menu/SubMenuBuilder;

    #@2
    .line 325
    invoke-virtual {p0}, Lcom/android/internal/view/menu/MenuItemImpl;->getTitle()Ljava/lang/CharSequence;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {p1, v0}, Lcom/android/internal/view/menu/SubMenuBuilder;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/SubMenu;

    #@9
    .line 326
    return-void
.end method

.method public setTitle(I)Landroid/view/MenuItem;
    .registers 3
    .parameter "title"

    #@0
    .prologue
    .line 359
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/view/menu/MenuBuilder;->getContext()Landroid/content/Context;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    invoke-virtual {p0, v0}, Lcom/android/internal/view/menu/MenuItemImpl;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    #@d
    move-result-object v0

    #@e
    return-object v0
.end method

.method public setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .registers 4
    .parameter "title"

    #@0
    .prologue
    .line 347
    iput-object p1, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mTitle:Ljava/lang/CharSequence;

    #@2
    .line 349
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@4
    const/4 v1, 0x0

    #@5
    invoke-virtual {v0, v1}, Lcom/android/internal/view/menu/MenuBuilder;->onItemsChanged(Z)V

    #@8
    .line 351
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mSubMenu:Lcom/android/internal/view/menu/SubMenuBuilder;

    #@a
    if-eqz v0, :cond_11

    #@c
    .line 352
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mSubMenu:Lcom/android/internal/view/menu/SubMenuBuilder;

    #@e
    invoke-virtual {v0, p1}, Lcom/android/internal/view/menu/SubMenuBuilder;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/SubMenu;

    #@11
    .line 355
    :cond_11
    return-object p0
.end method

.method public setTitleCondensed(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .registers 4
    .parameter "title"

    #@0
    .prologue
    .line 367
    iput-object p1, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mTitleCondensed:Ljava/lang/CharSequence;

    #@2
    .line 370
    if-nez p1, :cond_6

    #@4
    .line 371
    iget-object p1, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mTitle:Ljava/lang/CharSequence;

    #@6
    .line 374
    :cond_6
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@8
    const/4 v1, 0x0

    #@9
    invoke-virtual {v0, v1}, Lcom/android/internal/view/menu/MenuBuilder;->onItemsChanged(Z)V

    #@c
    .line 376
    return-object p0
.end method

.method public setVisible(Z)Landroid/view/MenuItem;
    .registers 3
    .parameter "shown"

    #@0
    .prologue
    .line 484
    invoke-virtual {p0, p1}, Lcom/android/internal/view/menu/MenuItemImpl;->setVisibleInt(Z)Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_b

    #@6
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@8
    invoke-virtual {v0, p0}, Lcom/android/internal/view/menu/MenuBuilder;->onItemVisibleChanged(Lcom/android/internal/view/menu/MenuItemImpl;)V

    #@b
    .line 486
    :cond_b
    return-object p0
.end method

.method setVisibleInt(Z)Z
    .registers 6
    .parameter "shown"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 475
    iget v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mFlags:I

    #@3
    .line 476
    .local v0, oldFlags:I
    iget v1, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mFlags:I

    #@5
    and-int/lit8 v3, v1, -0x9

    #@7
    if-eqz p1, :cond_13

    #@9
    move v1, v2

    #@a
    :goto_a
    or-int/2addr v1, v3

    #@b
    iput v1, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mFlags:I

    #@d
    .line 477
    iget v1, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mFlags:I

    #@f
    if-eq v0, v1, :cond_12

    #@11
    const/4 v2, 0x1

    #@12
    :cond_12
    return v2

    #@13
    .line 476
    :cond_13
    const/16 v1, 0x8

    #@15
    goto :goto_a
.end method

.method public shouldShowIcon()Z
    .registers 2

    #@0
    .prologue
    .line 515
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/view/menu/MenuBuilder;->getOptionalIconsVisible()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method shouldShowShortcut()Z
    .registers 2

    #@0
    .prologue
    .line 311
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/view/menu/MenuBuilder;->isShortcutsVisible()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_10

    #@8
    invoke-virtual {p0}, Lcom/android/internal/view/menu/MenuItemImpl;->getShortcut()C

    #@b
    move-result v0

    #@c
    if-eqz v0, :cond_10

    #@e
    const/4 v0, 0x1

    #@f
    :goto_f
    return v0

    #@10
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_f
.end method

.method public showsTextAsAction()Z
    .registers 3

    #@0
    .prologue
    .line 539
    iget v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mShowAsAction:I

    #@2
    and-int/lit8 v0, v0, 0x4

    #@4
    const/4 v1, 0x4

    #@5
    if-ne v0, v1, :cond_9

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 496
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuItemImpl;->mTitle:Ljava/lang/CharSequence;

    #@2
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method
