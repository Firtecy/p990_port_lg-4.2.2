.class public final Lcom/android/internal/view/RotationPolicy;
.super Ljava/lang/Object;
.source "RotationPolicy.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/view/RotationPolicy$RotationPolicyListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "RotationPolicy"


# direct methods
.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 39
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 40
    return-void
.end method

.method public static isRotationLockToggleSupported(Landroid/content/Context;)Z
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 51
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@7
    move-result-object v0

    #@8
    iget v0, v0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    #@a
    const/16 v1, 0x258

    #@c
    if-lt v0, v1, :cond_10

    #@e
    const/4 v0, 0x1

    #@f
    :goto_f
    return v0

    #@10
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_f
.end method

.method public static isRotationLockToggleVisible(Landroid/content/Context;)Z
    .registers 5
    .parameter "context"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 58
    invoke-static {p0}, Lcom/android/internal/view/RotationPolicy;->isRotationLockToggleSupported(Landroid/content/Context;)Z

    #@4
    move-result v1

    #@5
    if-eqz v1, :cond_15

    #@7
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@a
    move-result-object v1

    #@b
    const-string v2, "hide_rotation_lock_toggle_for_accessibility"

    #@d
    const/4 v3, -0x2

    #@e
    invoke-static {v1, v2, v0, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    #@11
    move-result v1

    #@12
    if-nez v1, :cond_15

    #@14
    const/4 v0, 0x1

    #@15
    :cond_15
    return v0
.end method

.method public static isRotationLocked(Landroid/content/Context;)Z
    .registers 5
    .parameter "context"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 68
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@4
    move-result-object v1

    #@5
    const-string v2, "accelerometer_rotation"

    #@7
    const/4 v3, -0x2

    #@8
    invoke-static {v1, v2, v0, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    #@b
    move-result v1

    #@c
    if-nez v1, :cond_f

    #@e
    const/4 v0, 0x1

    #@f
    :cond_f
    return v0
.end method

.method public static registerRotationPolicyListener(Landroid/content/Context;Lcom/android/internal/view/RotationPolicy$RotationPolicyListener;)V
    .registers 3
    .parameter "context"
    .parameter "listener"

    #@0
    .prologue
    .line 132
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@3
    move-result v0

    #@4
    invoke-static {p0, p1, v0}, Lcom/android/internal/view/RotationPolicy;->registerRotationPolicyListener(Landroid/content/Context;Lcom/android/internal/view/RotationPolicy$RotationPolicyListener;I)V

    #@7
    .line 133
    return-void
.end method

.method public static registerRotationPolicyListener(Landroid/content/Context;Lcom/android/internal/view/RotationPolicy$RotationPolicyListener;I)V
    .registers 7
    .parameter "context"
    .parameter "listener"
    .parameter "userHandle"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 141
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@4
    move-result-object v0

    #@5
    const-string v1, "accelerometer_rotation"

    #@7
    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@a
    move-result-object v1

    #@b
    iget-object v2, p1, Lcom/android/internal/view/RotationPolicy$RotationPolicyListener;->mObserver:Landroid/database/ContentObserver;

    #@d
    invoke-virtual {v0, v1, v3, v2, p2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    #@10
    .line 144
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@13
    move-result-object v0

    #@14
    const-string v1, "hide_rotation_lock_toggle_for_accessibility"

    #@16
    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@19
    move-result-object v1

    #@1a
    iget-object v2, p1, Lcom/android/internal/view/RotationPolicy$RotationPolicyListener;->mObserver:Landroid/database/ContentObserver;

    #@1c
    invoke-virtual {v0, v1, v3, v2, p2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    #@1f
    .line 147
    return-void
.end method

.method public static setRotationLock(Landroid/content/Context;Z)V
    .registers 6
    .parameter "context"
    .parameter "enabled"

    #@0
    .prologue
    .line 78
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@3
    move-result-object v0

    #@4
    const-string v1, "hide_rotation_lock_toggle_for_accessibility"

    #@6
    const/4 v2, 0x0

    #@7
    const/4 v3, -0x2

    #@8
    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    #@b
    .line 82
    new-instance v0, Lcom/android/internal/view/RotationPolicy$1;

    #@d
    invoke-direct {v0, p1}, Lcom/android/internal/view/RotationPolicy$1;-><init>(Z)V

    #@10
    invoke-static {v0}, Landroid/os/AsyncTask;->execute(Ljava/lang/Runnable;)V

    #@13
    .line 97
    return-void
.end method

.method public static setRotationLockForAccessibility(Landroid/content/Context;Z)V
    .registers 6
    .parameter "context"
    .parameter "enabled"

    #@0
    .prologue
    .line 106
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@3
    move-result-object v1

    #@4
    const-string v2, "hide_rotation_lock_toggle_for_accessibility"

    #@6
    if-eqz p1, :cond_16

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    const/4 v3, -0x2

    #@a
    invoke-static {v1, v2, v0, v3}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    #@d
    .line 110
    new-instance v0, Lcom/android/internal/view/RotationPolicy$2;

    #@f
    invoke-direct {v0, p1}, Lcom/android/internal/view/RotationPolicy$2;-><init>(Z)V

    #@12
    invoke-static {v0}, Landroid/os/AsyncTask;->execute(Ljava/lang/Runnable;)V

    #@15
    .line 125
    return-void

    #@16
    .line 106
    :cond_16
    const/4 v0, 0x0

    #@17
    goto :goto_9
.end method

.method public static unregisterRotationPolicyListener(Landroid/content/Context;Lcom/android/internal/view/RotationPolicy$RotationPolicyListener;)V
    .registers 4
    .parameter "context"
    .parameter "listener"

    #@0
    .prologue
    .line 154
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@3
    move-result-object v0

    #@4
    iget-object v1, p1, Lcom/android/internal/view/RotationPolicy$RotationPolicyListener;->mObserver:Landroid/database/ContentObserver;

    #@6
    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    #@9
    .line 155
    return-void
.end method
