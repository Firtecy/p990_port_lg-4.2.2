.class public Lcom/android/internal/view/ActionBarPolicy;
.super Ljava/lang/Object;
.source "ActionBarPolicy.java"


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 38
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 39
    iput-object p1, p0, Lcom/android/internal/view/ActionBarPolicy;->mContext:Landroid/content/Context;

    #@5
    .line 40
    return-void
.end method

.method public static get(Landroid/content/Context;)Lcom/android/internal/view/ActionBarPolicy;
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 35
    new-instance v0, Lcom/android/internal/view/ActionBarPolicy;

    #@2
    invoke-direct {v0, p0}, Lcom/android/internal/view/ActionBarPolicy;-><init>(Landroid/content/Context;)V

    #@5
    return-object v0
.end method


# virtual methods
.method public enableHomeButtonByDefault()Z
    .registers 3

    #@0
    .prologue
    .line 82
    iget-object v0, p0, Lcom/android/internal/view/ActionBarPolicy;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    #@5
    move-result-object v0

    #@6
    iget v0, v0, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@8
    const/16 v1, 0xe

    #@a
    if-ge v0, v1, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public getEmbeddedMenuWidthLimit()I
    .registers 2

    #@0
    .prologue
    .line 51
    iget-object v0, p0, Lcom/android/internal/view/ActionBarPolicy;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@9
    move-result-object v0

    #@a
    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    #@c
    div-int/lit8 v0, v0, 0x2

    #@e
    return v0
.end method

.method public getMaxActionButtons()I
    .registers 3

    #@0
    .prologue
    .line 43
    iget-object v0, p0, Lcom/android/internal/view/ActionBarPolicy;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@5
    move-result-object v0

    #@6
    const v1, 0x10e003b

    #@9
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    #@c
    move-result v0

    #@d
    return v0
.end method

.method public getStackedTabMaxWidth()I
    .registers 7

    #@0
    .prologue
    .line 88
    const/4 v2, 0x0

    #@1
    .line 89
    .local v2, useExpandedStackedTabs:Z
    iget-object v3, p0, Lcom/android/internal/view/ActionBarPolicy;->mContext:Landroid/content/Context;

    #@3
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@6
    move-result-object v1

    #@7
    .line 91
    .local v1, rcs:Landroid/content/res/Resources;
    if-eqz v1, :cond_23

    #@9
    .line 92
    const-string v3, "com.lge.action_bar_stacked_tabs_expanded"

    #@b
    const-string v4, "bool"

    #@d
    iget-object v5, p0, Lcom/android/internal/view/ActionBarPolicy;->mContext:Landroid/content/Context;

    #@f
    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@12
    move-result-object v5

    #@13
    invoke-virtual {v1, v3, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    #@16
    move-result v0

    #@17
    .line 96
    .local v0, id:I
    if-eqz v0, :cond_23

    #@19
    .line 97
    iget-object v3, p0, Lcom/android/internal/view/ActionBarPolicy;->mContext:Landroid/content/Context;

    #@1b
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@22
    move-result v2

    #@23
    .line 101
    .end local v0           #id:I
    :cond_23
    if-eqz v2, :cond_33

    #@25
    .line 102
    iget-object v3, p0, Lcom/android/internal/view/ActionBarPolicy;->mContext:Landroid/content/Context;

    #@27
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@2a
    move-result-object v3

    #@2b
    const v4, 0x20c0047

    #@2e
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@31
    move-result v3

    #@32
    .line 105
    :goto_32
    return v3

    #@33
    :cond_33
    iget-object v3, p0, Lcom/android/internal/view/ActionBarPolicy;->mContext:Landroid/content/Context;

    #@35
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@38
    move-result-object v3

    #@39
    const v4, 0x1050056

    #@3c
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@3f
    move-result v3

    #@40
    goto :goto_32
.end method

.method public getTabContainerHeight()I
    .registers 9

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 66
    iget-object v3, p0, Lcom/android/internal/view/ActionBarPolicy;->mContext:Landroid/content/Context;

    #@3
    const/4 v4, 0x0

    #@4
    sget-object v5, Lcom/android/internal/R$styleable;->ActionBar:[I

    #@6
    const v6, 0x10102ce

    #@9
    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@c
    move-result-object v0

    #@d
    .line 68
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v3, 0x4

    #@e
    invoke-virtual {v0, v3, v7}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    #@11
    move-result v1

    #@12
    .line 69
    .local v1, height:I
    iget-object v3, p0, Lcom/android/internal/view/ActionBarPolicy;->mContext:Landroid/content/Context;

    #@14
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@17
    move-result-object v2

    #@18
    .line 70
    .local v2, r:Landroid/content/res/Resources;
    invoke-virtual {p0}, Lcom/android/internal/view/ActionBarPolicy;->hasEmbeddedTabs()Z

    #@1b
    move-result v3

    #@1c
    if-nez v3, :cond_29

    #@1e
    .line 72
    const v3, 0x1050055

    #@21
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@24
    move-result v3

    #@25
    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    #@28
    move-result v1

    #@29
    .line 75
    :cond_29
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@2c
    .line 76
    return v1
.end method

.method public hasEmbeddedTabs()Z
    .registers 4

    #@0
    .prologue
    .line 55
    iget-object v1, p0, Lcom/android/internal/view/ActionBarPolicy;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    #@5
    move-result-object v1

    #@6
    iget v0, v1, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@8
    .line 56
    .local v0, targetSdk:I
    const/16 v1, 0x10

    #@a
    if-lt v0, v1, :cond_1a

    #@c
    .line 57
    iget-object v1, p0, Lcom/android/internal/view/ActionBarPolicy;->mContext:Landroid/content/Context;

    #@e
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@11
    move-result-object v1

    #@12
    const v2, 0x1110004

    #@15
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@18
    move-result v1

    #@19
    .line 62
    :goto_19
    return v1

    #@1a
    :cond_1a
    iget-object v1, p0, Lcom/android/internal/view/ActionBarPolicy;->mContext:Landroid/content/Context;

    #@1c
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@1f
    move-result-object v1

    #@20
    const v2, 0x1110005

    #@23
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@26
    move-result v1

    #@27
    goto :goto_19
.end method

.method public showsOverflowMenuButton()Z
    .registers 2

    #@0
    .prologue
    .line 47
    iget-object v0, p0, Lcom/android/internal/view/ActionBarPolicy;->mContext:Landroid/content/Context;

    #@2
    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method
