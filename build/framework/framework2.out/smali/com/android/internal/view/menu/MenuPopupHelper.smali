.class public Lcom/android/internal/view/menu/MenuPopupHelper;
.super Ljava/lang/Object;
.source "MenuPopupHelper.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/view/View$OnKeyListener;
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
.implements Landroid/widget/PopupWindow$OnDismissListener;
.implements Landroid/view/View$OnAttachStateChangeListener;
.implements Lcom/android/internal/view/menu/MenuPresenter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/view/menu/MenuPopupHelper$MenuAdapter;
    }
.end annotation


# static fields
.field static final ITEM_LAYOUT:I = 0x109009e

.field private static final TAG:Ljava/lang/String; = "MenuPopupHelper"


# instance fields
.field private mAdapter:Lcom/android/internal/view/menu/MenuPopupHelper$MenuAdapter;

.field private mAnchorView:Landroid/view/View;

.field private mContext:Landroid/content/Context;

.field mForceShowIcon:Z

.field private mInflater:Landroid/view/LayoutInflater;

.field private mMeasureParent:Landroid/view/ViewGroup;

.field private mMenu:Lcom/android/internal/view/menu/MenuBuilder;

.field private mOverflowOnly:Z

.field private mPopup:Landroid/widget/ListPopupWindow;

.field private mPopupMaxWidth:I

.field private mPresenterCallback:Lcom/android/internal/view/menu/MenuPresenter$Callback;

.field private mTreeObserver:Landroid/view/ViewTreeObserver;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/internal/view/menu/MenuBuilder;)V
    .registers 5
    .parameter "context"
    .parameter "menu"

    #@0
    .prologue
    .line 68
    const/4 v0, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    invoke-direct {p0, p1, p2, v0, v1}, Lcom/android/internal/view/menu/MenuPopupHelper;-><init>(Landroid/content/Context;Lcom/android/internal/view/menu/MenuBuilder;Landroid/view/View;Z)V

    #@5
    .line 69
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/internal/view/menu/MenuBuilder;Landroid/view/View;)V
    .registers 5
    .parameter "context"
    .parameter "menu"
    .parameter "anchorView"

    #@0
    .prologue
    .line 72
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/android/internal/view/menu/MenuPopupHelper;-><init>(Landroid/content/Context;Lcom/android/internal/view/menu/MenuBuilder;Landroid/view/View;Z)V

    #@4
    .line 73
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/internal/view/menu/MenuBuilder;Landroid/view/View;Z)V
    .registers 8
    .parameter "context"
    .parameter "menu"
    .parameter "anchorView"
    .parameter "overflowOnly"

    #@0
    .prologue
    .line 76
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 77
    iput-object p1, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mContext:Landroid/content/Context;

    #@5
    .line 78
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    #@8
    move-result-object v1

    #@9
    iput-object v1, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mInflater:Landroid/view/LayoutInflater;

    #@b
    .line 79
    iput-object p2, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@d
    .line 80
    iput-boolean p4, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mOverflowOnly:Z

    #@f
    .line 82
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@12
    move-result-object v0

    #@13
    .line 83
    .local v0, res:Landroid/content/res/Resources;
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@16
    move-result-object v1

    #@17
    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    #@19
    div-int/lit8 v1, v1, 0x2

    #@1b
    const v2, 0x1050007

    #@1e
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@21
    move-result v2

    #@22
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    #@25
    move-result v1

    #@26
    iput v1, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mPopupMaxWidth:I

    #@28
    .line 86
    iput-object p3, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mAnchorView:Landroid/view/View;

    #@2a
    .line 88
    invoke-virtual {p2, p0}, Lcom/android/internal/view/menu/MenuBuilder;->addMenuPresenter(Lcom/android/internal/view/menu/MenuPresenter;)V

    #@2d
    .line 89
    return-void
.end method

.method static synthetic access$100(Lcom/android/internal/view/menu/MenuPopupHelper;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mOverflowOnly:Z

    #@2
    return v0
.end method

.method static synthetic access$200(Lcom/android/internal/view/menu/MenuPopupHelper;)Landroid/view/LayoutInflater;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 43
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mInflater:Landroid/view/LayoutInflater;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/android/internal/view/menu/MenuPopupHelper;)Lcom/android/internal/view/menu/MenuBuilder;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 43
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@2
    return-object v0
.end method

.method private measureContentWidth(Landroid/widget/ListAdapter;)I
    .registers 12
    .parameter "adapter"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 169
    const/4 v6, 0x0

    #@2
    .line 170
    .local v6, width:I
    const/4 v4, 0x0

    #@3
    .line 171
    .local v4, itemView:Landroid/view/View;
    const/4 v3, 0x0

    #@4
    .line 172
    .local v3, itemType:I
    invoke-static {v8, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@7
    move-result v7

    #@8
    .line 174
    .local v7, widthMeasureSpec:I
    invoke-static {v8, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@b
    move-result v1

    #@c
    .line 176
    .local v1, heightMeasureSpec:I
    invoke-interface {p1}, Landroid/widget/ListAdapter;->getCount()I

    #@f
    move-result v0

    #@10
    .line 177
    .local v0, count:I
    const/4 v2, 0x0

    #@11
    .local v2, i:I
    :goto_11
    if-ge v2, v0, :cond_3c

    #@13
    .line 178
    invoke-interface {p1, v2}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    #@16
    move-result v5

    #@17
    .line 179
    .local v5, positionType:I
    if-eq v5, v3, :cond_1b

    #@19
    .line 180
    move v3, v5

    #@1a
    .line 181
    const/4 v4, 0x0

    #@1b
    .line 183
    :cond_1b
    iget-object v8, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mMeasureParent:Landroid/view/ViewGroup;

    #@1d
    if-nez v8, :cond_28

    #@1f
    .line 184
    new-instance v8, Landroid/widget/FrameLayout;

    #@21
    iget-object v9, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mContext:Landroid/content/Context;

    #@23
    invoke-direct {v8, v9}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    #@26
    iput-object v8, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mMeasureParent:Landroid/view/ViewGroup;

    #@28
    .line 186
    :cond_28
    iget-object v8, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mMeasureParent:Landroid/view/ViewGroup;

    #@2a
    invoke-interface {p1, v2, v4, v8}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    #@2d
    move-result-object v4

    #@2e
    .line 187
    invoke-virtual {v4, v7, v1}, Landroid/view/View;->measure(II)V

    #@31
    .line 188
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    #@34
    move-result v8

    #@35
    invoke-static {v6, v8}, Ljava/lang/Math;->max(II)I

    #@38
    move-result v6

    #@39
    .line 177
    add-int/lit8 v2, v2, 0x1

    #@3b
    goto :goto_11

    #@3c
    .line 190
    .end local v5           #positionType:I
    :cond_3c
    return v6
.end method


# virtual methods
.method public collapseItemActionView(Lcom/android/internal/view/menu/MenuBuilder;Lcom/android/internal/view/menu/MenuItemImpl;)Z
    .registers 4
    .parameter "menu"
    .parameter "item"

    #@0
    .prologue
    .line 287
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public dismiss()V
    .registers 2

    #@0
    .prologue
    .line 133
    invoke-virtual {p0}, Lcom/android/internal/view/menu/MenuPopupHelper;->isShowing()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_b

    #@6
    .line 134
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mPopup:Landroid/widget/ListPopupWindow;

    #@8
    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->dismiss()V

    #@b
    .line 136
    :cond_b
    return-void
.end method

.method public expandItemActionView(Lcom/android/internal/view/menu/MenuBuilder;Lcom/android/internal/view/menu/MenuItemImpl;)Z
    .registers 4
    .parameter "menu"
    .parameter "item"

    #@0
    .prologue
    .line 283
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public flagActionItems()Z
    .registers 2

    #@0
    .prologue
    .line 279
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getId()I
    .registers 2

    #@0
    .prologue
    .line 292
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getMenuView(Landroid/view/ViewGroup;)Lcom/android/internal/view/menu/MenuView;
    .registers 4
    .parameter "root"

    #@0
    .prologue
    .line 226
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@2
    const-string v1, "MenuPopupHelpers manage their own views"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@7
    throw v0
.end method

.method public initForMenu(Landroid/content/Context;Lcom/android/internal/view/menu/MenuBuilder;)V
    .registers 3
    .parameter "context"
    .parameter "menu"

    #@0
    .prologue
    .line 222
    return-void
.end method

.method public isShowing()Z
    .registers 2

    #@0
    .prologue
    .line 150
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mPopup:Landroid/widget/ListPopupWindow;

    #@2
    if-eqz v0, :cond_e

    #@4
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mPopup:Landroid/widget/ListPopupWindow;

    #@6
    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->isShowing()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public onCloseMenu(Lcom/android/internal/view/menu/MenuBuilder;Z)V
    .registers 4
    .parameter "menu"
    .parameter "allMenusAreClosing"

    #@0
    .prologue
    .line 269
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@2
    if-eq p1, v0, :cond_5

    #@4
    .line 275
    :cond_4
    :goto_4
    return-void

    #@5
    .line 271
    :cond_5
    invoke-virtual {p0}, Lcom/android/internal/view/menu/MenuPopupHelper;->dismiss()V

    #@8
    .line 272
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mPresenterCallback:Lcom/android/internal/view/menu/MenuPresenter$Callback;

    #@a
    if-eqz v0, :cond_4

    #@c
    .line 273
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mPresenterCallback:Lcom/android/internal/view/menu/MenuPresenter$Callback;

    #@e
    invoke-interface {v0, p1, p2}, Lcom/android/internal/view/menu/MenuPresenter$Callback;->onCloseMenu(Lcom/android/internal/view/menu/MenuBuilder;Z)V

    #@11
    goto :goto_4
.end method

.method public onDismiss()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 139
    iput-object v1, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mPopup:Landroid/widget/ListPopupWindow;

    #@3
    .line 140
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@5
    invoke-virtual {v0}, Lcom/android/internal/view/menu/MenuBuilder;->close()V

    #@8
    .line 141
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mTreeObserver:Landroid/view/ViewTreeObserver;

    #@a
    if-eqz v0, :cond_23

    #@c
    .line 142
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mTreeObserver:Landroid/view/ViewTreeObserver;

    #@e
    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    #@11
    move-result v0

    #@12
    if-nez v0, :cond_1c

    #@14
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mAnchorView:Landroid/view/View;

    #@16
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    #@19
    move-result-object v0

    #@1a
    iput-object v0, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mTreeObserver:Landroid/view/ViewTreeObserver;

    #@1c
    .line 143
    :cond_1c
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mTreeObserver:Landroid/view/ViewTreeObserver;

    #@1e
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    #@21
    .line 144
    iput-object v1, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mTreeObserver:Landroid/view/ViewTreeObserver;

    #@23
    .line 146
    :cond_23
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mAnchorView:Landroid/view/View;

    #@25
    invoke-virtual {v0, p0}, Landroid/view/View;->removeOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    #@28
    .line 147
    return-void
.end method

.method public onGlobalLayout()V
    .registers 3

    #@0
    .prologue
    .line 195
    invoke-virtual {p0}, Lcom/android/internal/view/menu/MenuPopupHelper;->isShowing()Z

    #@3
    move-result v1

    #@4
    if-eqz v1, :cond_13

    #@6
    .line 196
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mAnchorView:Landroid/view/View;

    #@8
    .line 197
    .local v0, anchor:Landroid/view/View;
    if-eqz v0, :cond_10

    #@a
    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    #@d
    move-result v1

    #@e
    if-nez v1, :cond_14

    #@10
    .line 198
    :cond_10
    invoke-virtual {p0}, Lcom/android/internal/view/menu/MenuPopupHelper;->dismiss()V

    #@13
    .line 204
    .end local v0           #anchor:Landroid/view/View;
    :cond_13
    :goto_13
    return-void

    #@14
    .line 199
    .restart local v0       #anchor:Landroid/view/View;
    :cond_14
    invoke-virtual {p0}, Lcom/android/internal/view/menu/MenuPopupHelper;->isShowing()Z

    #@17
    move-result v1

    #@18
    if-eqz v1, :cond_13

    #@1a
    .line 201
    iget-object v1, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mPopup:Landroid/widget/ListPopupWindow;

    #@1c
    invoke-virtual {v1}, Landroid/widget/ListPopupWindow;->show()V

    #@1f
    goto :goto_13
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 10
    .parameter
    .parameter "view"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    #@0
    .prologue
    .line 155
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mAdapter:Lcom/android/internal/view/menu/MenuPopupHelper$MenuAdapter;

    #@2
    .line 156
    .local v0, adapter:Lcom/android/internal/view/menu/MenuPopupHelper$MenuAdapter;
    invoke-static {v0}, Lcom/android/internal/view/menu/MenuPopupHelper$MenuAdapter;->access$000(Lcom/android/internal/view/menu/MenuPopupHelper$MenuAdapter;)Lcom/android/internal/view/menu/MenuBuilder;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v0, p3}, Lcom/android/internal/view/menu/MenuPopupHelper$MenuAdapter;->getItem(I)Lcom/android/internal/view/menu/MenuItemImpl;

    #@9
    move-result-object v2

    #@a
    const/4 v3, 0x0

    #@b
    invoke-virtual {v1, v2, v3}, Lcom/android/internal/view/menu/MenuBuilder;->performItemAction(Landroid/view/MenuItem;I)Z

    #@e
    .line 157
    return-void
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .registers 6
    .parameter "v"
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 160
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    #@4
    move-result v1

    #@5
    if-ne v1, v0, :cond_f

    #@7
    const/16 v1, 0x52

    #@9
    if-ne p2, v1, :cond_f

    #@b
    .line 161
    invoke-virtual {p0}, Lcom/android/internal/view/menu/MenuPopupHelper;->dismiss()V

    #@e
    .line 164
    :goto_e
    return v0

    #@f
    :cond_f
    const/4 v0, 0x0

    #@10
    goto :goto_e
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .registers 2
    .parameter "state"

    #@0
    .prologue
    .line 302
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .registers 2

    #@0
    .prologue
    .line 297
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public onSubMenuSelected(Lcom/android/internal/view/menu/SubMenuBuilder;)Z
    .registers 10
    .parameter "subMenu"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 241
    invoke-virtual {p1}, Lcom/android/internal/view/menu/SubMenuBuilder;->hasVisibleItems()Z

    #@4
    move-result v6

    #@5
    if-eqz v6, :cond_41

    #@7
    .line 242
    new-instance v4, Lcom/android/internal/view/menu/MenuPopupHelper;

    #@9
    iget-object v6, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mContext:Landroid/content/Context;

    #@b
    iget-object v7, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mAnchorView:Landroid/view/View;

    #@d
    invoke-direct {v4, v6, p1, v7, v5}, Lcom/android/internal/view/menu/MenuPopupHelper;-><init>(Landroid/content/Context;Lcom/android/internal/view/menu/MenuBuilder;Landroid/view/View;Z)V

    #@10
    .line 243
    .local v4, subPopup:Lcom/android/internal/view/menu/MenuPopupHelper;
    iget-object v6, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mPresenterCallback:Lcom/android/internal/view/menu/MenuPresenter$Callback;

    #@12
    invoke-virtual {v4, v6}, Lcom/android/internal/view/menu/MenuPopupHelper;->setCallback(Lcom/android/internal/view/menu/MenuPresenter$Callback;)V

    #@15
    .line 245
    const/4 v3, 0x0

    #@16
    .line 246
    .local v3, preserveIconSpacing:Z
    invoke-virtual {p1}, Lcom/android/internal/view/menu/SubMenuBuilder;->size()I

    #@19
    move-result v1

    #@1a
    .line 247
    .local v1, count:I
    const/4 v2, 0x0

    #@1b
    .local v2, i:I
    :goto_1b
    if-ge v2, v1, :cond_2e

    #@1d
    .line 248
    invoke-virtual {p1, v2}, Lcom/android/internal/view/menu/SubMenuBuilder;->getItem(I)Landroid/view/MenuItem;

    #@20
    move-result-object v0

    #@21
    .line 249
    .local v0, childItem:Landroid/view/MenuItem;
    invoke-interface {v0}, Landroid/view/MenuItem;->isVisible()Z

    #@24
    move-result v6

    #@25
    if-eqz v6, :cond_42

    #@27
    invoke-interface {v0}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    #@2a
    move-result-object v6

    #@2b
    if-eqz v6, :cond_42

    #@2d
    .line 250
    const/4 v3, 0x1

    #@2e
    .line 254
    .end local v0           #childItem:Landroid/view/MenuItem;
    :cond_2e
    invoke-virtual {v4, v3}, Lcom/android/internal/view/menu/MenuPopupHelper;->setForceShowIcon(Z)V

    #@31
    .line 256
    invoke-virtual {v4}, Lcom/android/internal/view/menu/MenuPopupHelper;->tryShow()Z

    #@34
    move-result v6

    #@35
    if-eqz v6, :cond_41

    #@37
    .line 257
    iget-object v5, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mPresenterCallback:Lcom/android/internal/view/menu/MenuPresenter$Callback;

    #@39
    if-eqz v5, :cond_40

    #@3b
    .line 258
    iget-object v5, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mPresenterCallback:Lcom/android/internal/view/menu/MenuPresenter$Callback;

    #@3d
    invoke-interface {v5, p1}, Lcom/android/internal/view/menu/MenuPresenter$Callback;->onOpenSubMenu(Lcom/android/internal/view/menu/MenuBuilder;)Z

    #@40
    .line 260
    :cond_40
    const/4 v5, 0x1

    #@41
    .line 263
    .end local v1           #count:I
    .end local v2           #i:I
    .end local v3           #preserveIconSpacing:Z
    .end local v4           #subPopup:Lcom/android/internal/view/menu/MenuPopupHelper;
    :cond_41
    return v5

    #@42
    .line 247
    .restart local v0       #childItem:Landroid/view/MenuItem;
    .restart local v1       #count:I
    .restart local v2       #i:I
    .restart local v3       #preserveIconSpacing:Z
    .restart local v4       #subPopup:Lcom/android/internal/view/menu/MenuPopupHelper;
    :cond_42
    add-int/lit8 v2, v2, 0x1

    #@44
    goto :goto_1b
.end method

.method public onViewAttachedToWindow(Landroid/view/View;)V
    .registers 2
    .parameter "v"

    #@0
    .prologue
    .line 208
    return-void
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .registers 3
    .parameter "v"

    #@0
    .prologue
    .line 212
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mTreeObserver:Landroid/view/ViewTreeObserver;

    #@2
    if-eqz v0, :cond_17

    #@4
    .line 213
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mTreeObserver:Landroid/view/ViewTreeObserver;

    #@6
    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_12

    #@c
    invoke-virtual {p1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    #@f
    move-result-object v0

    #@10
    iput-object v0, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mTreeObserver:Landroid/view/ViewTreeObserver;

    #@12
    .line 214
    :cond_12
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mTreeObserver:Landroid/view/ViewTreeObserver;

    #@14
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    #@17
    .line 216
    :cond_17
    invoke-virtual {p1, p0}, Landroid/view/View;->removeOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    #@1a
    .line 217
    return-void
.end method

.method public setAnchorView(Landroid/view/View;)V
    .registers 2
    .parameter "anchor"

    #@0
    .prologue
    .line 92
    iput-object p1, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mAnchorView:Landroid/view/View;

    #@2
    .line 93
    return-void
.end method

.method public setCallback(Lcom/android/internal/view/menu/MenuPresenter$Callback;)V
    .registers 2
    .parameter "cb"

    #@0
    .prologue
    .line 236
    iput-object p1, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mPresenterCallback:Lcom/android/internal/view/menu/MenuPresenter$Callback;

    #@2
    .line 237
    return-void
.end method

.method public setForceShowIcon(Z)V
    .registers 2
    .parameter "forceShow"

    #@0
    .prologue
    .line 96
    iput-boolean p1, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mForceShowIcon:Z

    #@2
    .line 97
    return-void
.end method

.method public show()V
    .registers 3

    #@0
    .prologue
    .line 100
    invoke-virtual {p0}, Lcom/android/internal/view/menu/MenuPopupHelper;->tryShow()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_e

    #@6
    .line 101
    new-instance v0, Ljava/lang/IllegalStateException;

    #@8
    const-string v1, "MenuPopupHelper cannot be used without an anchor"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 103
    :cond_e
    return-void
.end method

.method public tryShow()Z
    .registers 8

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    .line 106
    new-instance v3, Landroid/widget/ListPopupWindow;

    #@4
    iget-object v4, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mContext:Landroid/content/Context;

    #@6
    const/4 v5, 0x0

    #@7
    const v6, 0x1010300

    #@a
    invoke-direct {v3, v4, v5, v6}, Landroid/widget/ListPopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@d
    iput-object v3, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mPopup:Landroid/widget/ListPopupWindow;

    #@f
    .line 107
    iget-object v3, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mPopup:Landroid/widget/ListPopupWindow;

    #@11
    invoke-virtual {v3, p0}, Landroid/widget/ListPopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    #@14
    .line 108
    iget-object v3, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mPopup:Landroid/widget/ListPopupWindow;

    #@16
    invoke-virtual {v3, p0}, Landroid/widget/ListPopupWindow;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    #@19
    .line 110
    new-instance v3, Lcom/android/internal/view/menu/MenuPopupHelper$MenuAdapter;

    #@1b
    iget-object v4, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@1d
    invoke-direct {v3, p0, v4}, Lcom/android/internal/view/menu/MenuPopupHelper$MenuAdapter;-><init>(Lcom/android/internal/view/menu/MenuPopupHelper;Lcom/android/internal/view/menu/MenuBuilder;)V

    #@20
    iput-object v3, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mAdapter:Lcom/android/internal/view/menu/MenuPopupHelper$MenuAdapter;

    #@22
    .line 111
    iget-object v3, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mPopup:Landroid/widget/ListPopupWindow;

    #@24
    iget-object v4, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mAdapter:Lcom/android/internal/view/menu/MenuPopupHelper$MenuAdapter;

    #@26
    invoke-virtual {v3, v4}, Landroid/widget/ListPopupWindow;->setAdapter(Landroid/widget/ListAdapter;)V

    #@29
    .line 112
    iget-object v3, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mPopup:Landroid/widget/ListPopupWindow;

    #@2b
    invoke-virtual {v3, v2}, Landroid/widget/ListPopupWindow;->setModal(Z)V

    #@2e
    .line 114
    iget-object v1, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mAnchorView:Landroid/view/View;

    #@30
    .line 115
    .local v1, anchor:Landroid/view/View;
    if-eqz v1, :cond_72

    #@32
    .line 116
    iget-object v3, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mTreeObserver:Landroid/view/ViewTreeObserver;

    #@34
    if-nez v3, :cond_37

    #@36
    move v0, v2

    #@37
    .line 117
    .local v0, addGlobalListener:Z
    :cond_37
    invoke-virtual {v1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    #@3a
    move-result-object v3

    #@3b
    iput-object v3, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mTreeObserver:Landroid/view/ViewTreeObserver;

    #@3d
    .line 118
    if-eqz v0, :cond_44

    #@3f
    iget-object v3, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mTreeObserver:Landroid/view/ViewTreeObserver;

    #@41
    invoke-virtual {v3, p0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    #@44
    .line 119
    :cond_44
    invoke-virtual {v1, p0}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    #@47
    .line 120
    iget-object v3, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mPopup:Landroid/widget/ListPopupWindow;

    #@49
    invoke-virtual {v3, v1}, Landroid/widget/ListPopupWindow;->setAnchorView(Landroid/view/View;)V

    #@4c
    .line 125
    iget-object v3, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mPopup:Landroid/widget/ListPopupWindow;

    #@4e
    iget-object v4, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mAdapter:Lcom/android/internal/view/menu/MenuPopupHelper$MenuAdapter;

    #@50
    invoke-direct {p0, v4}, Lcom/android/internal/view/menu/MenuPopupHelper;->measureContentWidth(Landroid/widget/ListAdapter;)I

    #@53
    move-result v4

    #@54
    iget v5, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mPopupMaxWidth:I

    #@56
    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    #@59
    move-result v4

    #@5a
    invoke-virtual {v3, v4}, Landroid/widget/ListPopupWindow;->setContentWidth(I)V

    #@5d
    .line 126
    iget-object v3, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mPopup:Landroid/widget/ListPopupWindow;

    #@5f
    const/4 v4, 0x2

    #@60
    invoke-virtual {v3, v4}, Landroid/widget/ListPopupWindow;->setInputMethodMode(I)V

    #@63
    .line 127
    iget-object v3, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mPopup:Landroid/widget/ListPopupWindow;

    #@65
    invoke-virtual {v3}, Landroid/widget/ListPopupWindow;->show()V

    #@68
    .line 128
    iget-object v3, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mPopup:Landroid/widget/ListPopupWindow;

    #@6a
    invoke-virtual {v3}, Landroid/widget/ListPopupWindow;->getListView()Landroid/widget/ListView;

    #@6d
    move-result-object v3

    #@6e
    invoke-virtual {v3, p0}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    #@71
    .line 129
    .end local v0           #addGlobalListener:Z
    :goto_71
    return v2

    #@72
    :cond_72
    move v2, v0

    #@73
    .line 122
    goto :goto_71
.end method

.method public updateMenuView(Z)V
    .registers 3
    .parameter "cleared"

    #@0
    .prologue
    .line 231
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mAdapter:Lcom/android/internal/view/menu/MenuPopupHelper$MenuAdapter;

    #@2
    if-eqz v0, :cond_9

    #@4
    iget-object v0, p0, Lcom/android/internal/view/menu/MenuPopupHelper;->mAdapter:Lcom/android/internal/view/menu/MenuPopupHelper$MenuAdapter;

    #@6
    invoke-virtual {v0}, Lcom/android/internal/view/menu/MenuPopupHelper$MenuAdapter;->notifyDataSetChanged()V

    #@9
    .line 232
    :cond_9
    return-void
.end method
