.class public abstract Lcom/android/internal/view/IInputContextCallback$Stub;
.super Landroid/os/Binder;
.source "IInputContextCallback.java"

# interfaces
.implements Lcom/android/internal/view/IInputContextCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/view/IInputContextCallback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/view/IInputContextCallback$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.android.internal.view.IInputContextCallback"

.field static final TRANSACTION_setCursorCapsMode:I = 0x3

.field static final TRANSACTION_setExtractedText:I = 0x4

.field static final TRANSACTION_setSelectedText:I = 0x5

.field static final TRANSACTION_setTextAfterCursor:I = 0x2

.field static final TRANSACTION_setTextBeforeCursor:I = 0x1


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 17
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 18
    const-string v0, "com.android.internal.view.IInputContextCallback"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/android/internal/view/IInputContextCallback$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 19
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/android/internal/view/IInputContextCallback;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 26
    if-nez p0, :cond_4

    #@2
    .line 27
    const/4 v0, 0x0

    #@3
    .line 33
    :goto_3
    return-object v0

    #@4
    .line 29
    :cond_4
    const-string v1, "com.android.internal.view.IInputContextCallback"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 30
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/android/internal/view/IInputContextCallback;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 31
    check-cast v0, Lcom/android/internal/view/IInputContextCallback;

    #@12
    goto :goto_3

    #@13
    .line 33
    :cond_13
    new-instance v0, Lcom/android/internal/view/IInputContextCallback$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/android/internal/view/IInputContextCallback$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 37
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 9
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 41
    sparse-switch p1, :sswitch_data_96

    #@4
    .line 119
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v2

    #@8
    :goto_8
    return v2

    #@9
    .line 45
    :sswitch_9
    const-string v3, "com.android.internal.view.IInputContextCallback"

    #@b
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    goto :goto_8

    #@f
    .line 50
    :sswitch_f
    const-string v3, "com.android.internal.view.IInputContextCallback"

    #@11
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 52
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@17
    move-result v3

    #@18
    if-eqz v3, :cond_2a

    #@1a
    .line 53
    sget-object v3, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    #@1c
    invoke-interface {v3, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1f
    move-result-object v0

    #@20
    check-cast v0, Ljava/lang/CharSequence;

    #@22
    .line 59
    .local v0, _arg0:Ljava/lang/CharSequence;
    :goto_22
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@25
    move-result v1

    #@26
    .line 60
    .local v1, _arg1:I
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/view/IInputContextCallback$Stub;->setTextBeforeCursor(Ljava/lang/CharSequence;I)V

    #@29
    goto :goto_8

    #@2a
    .line 56
    .end local v0           #_arg0:Ljava/lang/CharSequence;
    .end local v1           #_arg1:I
    :cond_2a
    const/4 v0, 0x0

    #@2b
    .restart local v0       #_arg0:Ljava/lang/CharSequence;
    goto :goto_22

    #@2c
    .line 65
    .end local v0           #_arg0:Ljava/lang/CharSequence;
    :sswitch_2c
    const-string v3, "com.android.internal.view.IInputContextCallback"

    #@2e
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@31
    .line 67
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@34
    move-result v3

    #@35
    if-eqz v3, :cond_47

    #@37
    .line 68
    sget-object v3, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    #@39
    invoke-interface {v3, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@3c
    move-result-object v0

    #@3d
    check-cast v0, Ljava/lang/CharSequence;

    #@3f
    .line 74
    .restart local v0       #_arg0:Ljava/lang/CharSequence;
    :goto_3f
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@42
    move-result v1

    #@43
    .line 75
    .restart local v1       #_arg1:I
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/view/IInputContextCallback$Stub;->setTextAfterCursor(Ljava/lang/CharSequence;I)V

    #@46
    goto :goto_8

    #@47
    .line 71
    .end local v0           #_arg0:Ljava/lang/CharSequence;
    .end local v1           #_arg1:I
    :cond_47
    const/4 v0, 0x0

    #@48
    .restart local v0       #_arg0:Ljava/lang/CharSequence;
    goto :goto_3f

    #@49
    .line 80
    .end local v0           #_arg0:Ljava/lang/CharSequence;
    :sswitch_49
    const-string v3, "com.android.internal.view.IInputContextCallback"

    #@4b
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4e
    .line 82
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@51
    move-result v0

    #@52
    .line 84
    .local v0, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@55
    move-result v1

    #@56
    .line 85
    .restart local v1       #_arg1:I
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/view/IInputContextCallback$Stub;->setCursorCapsMode(II)V

    #@59
    goto :goto_8

    #@5a
    .line 90
    .end local v0           #_arg0:I
    .end local v1           #_arg1:I
    :sswitch_5a
    const-string v3, "com.android.internal.view.IInputContextCallback"

    #@5c
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@5f
    .line 92
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@62
    move-result v3

    #@63
    if-eqz v3, :cond_75

    #@65
    .line 93
    sget-object v3, Landroid/view/inputmethod/ExtractedText;->CREATOR:Landroid/os/Parcelable$Creator;

    #@67
    invoke-interface {v3, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@6a
    move-result-object v0

    #@6b
    check-cast v0, Landroid/view/inputmethod/ExtractedText;

    #@6d
    .line 99
    .local v0, _arg0:Landroid/view/inputmethod/ExtractedText;
    :goto_6d
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@70
    move-result v1

    #@71
    .line 100
    .restart local v1       #_arg1:I
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/view/IInputContextCallback$Stub;->setExtractedText(Landroid/view/inputmethod/ExtractedText;I)V

    #@74
    goto :goto_8

    #@75
    .line 96
    .end local v0           #_arg0:Landroid/view/inputmethod/ExtractedText;
    .end local v1           #_arg1:I
    :cond_75
    const/4 v0, 0x0

    #@76
    .restart local v0       #_arg0:Landroid/view/inputmethod/ExtractedText;
    goto :goto_6d

    #@77
    .line 105
    .end local v0           #_arg0:Landroid/view/inputmethod/ExtractedText;
    :sswitch_77
    const-string v3, "com.android.internal.view.IInputContextCallback"

    #@79
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@7c
    .line 107
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@7f
    move-result v3

    #@80
    if-eqz v3, :cond_93

    #@82
    .line 108
    sget-object v3, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    #@84
    invoke-interface {v3, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@87
    move-result-object v0

    #@88
    check-cast v0, Ljava/lang/CharSequence;

    #@8a
    .line 114
    .local v0, _arg0:Ljava/lang/CharSequence;
    :goto_8a
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@8d
    move-result v1

    #@8e
    .line 115
    .restart local v1       #_arg1:I
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/view/IInputContextCallback$Stub;->setSelectedText(Ljava/lang/CharSequence;I)V

    #@91
    goto/16 :goto_8

    #@93
    .line 111
    .end local v0           #_arg0:Ljava/lang/CharSequence;
    .end local v1           #_arg1:I
    :cond_93
    const/4 v0, 0x0

    #@94
    .restart local v0       #_arg0:Ljava/lang/CharSequence;
    goto :goto_8a

    #@95
    .line 41
    nop

    #@96
    :sswitch_data_96
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_2c
        0x3 -> :sswitch_49
        0x4 -> :sswitch_5a
        0x5 -> :sswitch_77
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
