.class Lcom/android/internal/view/IInputMethodManager$Stub$Proxy;
.super Ljava/lang/Object;
.source "IInputMethodManager.java"

# interfaces
.implements Lcom/android/internal/view/IInputMethodManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/view/IInputMethodManager$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 440
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 441
    iput-object p1, p0, Lcom/android/internal/view/IInputMethodManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 442
    return-void
.end method


# virtual methods
.method public addClient(Lcom/android/internal/view/IInputMethodClient;Lcom/android/internal/view/IInputContext;II)V
    .registers 10
    .parameter "client"
    .parameter "inputContext"
    .parameter "uid"
    .parameter "pid"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 555
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 556
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 558
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "com.android.internal.view.IInputMethodManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 559
    if-eqz p1, :cond_37

    #@10
    invoke-interface {p1}, Lcom/android/internal/view/IInputMethodClient;->asBinder()Landroid/os/IBinder;

    #@13
    move-result-object v3

    #@14
    :goto_14
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@17
    .line 560
    if-eqz p2, :cond_1d

    #@19
    invoke-interface {p2}, Lcom/android/internal/view/IInputContext;->asBinder()Landroid/os/IBinder;

    #@1c
    move-result-object v2

    #@1d
    :cond_1d
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@20
    .line 561
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@23
    .line 562
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@26
    .line 563
    iget-object v2, p0, Lcom/android/internal/view/IInputMethodManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@28
    const/4 v3, 0x6

    #@29
    const/4 v4, 0x0

    #@2a
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@2d
    .line 564
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_30
    .catchall {:try_start_9 .. :try_end_30} :catchall_39

    #@30
    .line 567
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@33
    .line 568
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@36
    .line 570
    return-void

    #@37
    :cond_37
    move-object v3, v2

    #@38
    .line 559
    goto :goto_14

    #@39
    .line 567
    :catchall_39
    move-exception v2

    #@3a
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3d
    .line 568
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@40
    throw v2
.end method

.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 445
    iget-object v0, p0, Lcom/android/internal/view/IInputMethodManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public finishInput(Lcom/android/internal/view/IInputMethodClient;)V
    .registers 7
    .parameter "client"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 620
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 621
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 623
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.android.internal.view.IInputMethodManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 624
    if-eqz p1, :cond_28

    #@f
    invoke-interface {p1}, Lcom/android/internal/view/IInputMethodClient;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v2

    #@13
    :goto_13
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 625
    iget-object v2, p0, Lcom/android/internal/view/IInputMethodManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/16 v3, 0x9

    #@1a
    const/4 v4, 0x0

    #@1b
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 626
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_21
    .catchall {:try_start_8 .. :try_end_21} :catchall_2a

    #@21
    .line 629
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 630
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 632
    return-void

    #@28
    .line 624
    :cond_28
    const/4 v2, 0x0

    #@29
    goto :goto_13

    #@2a
    .line 629
    :catchall_2a
    move-exception v2

    #@2b
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 630
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@31
    throw v2
.end method

.method public getCurrentInputMethodSubtype()Landroid/view/inputmethod/InputMethodSubtype;
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 902
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 903
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 906
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.android.internal.view.IInputMethodManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 907
    iget-object v3, p0, Lcom/android/internal/view/IInputMethodManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0x17

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 908
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 909
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@1b
    move-result v3

    #@1c
    if-eqz v3, :cond_2d

    #@1e
    .line 910
    sget-object v3, Landroid/view/inputmethod/InputMethodSubtype;->CREATOR:Landroid/os/Parcelable$Creator;

    #@20
    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@23
    move-result-object v2

    #@24
    check-cast v2, Landroid/view/inputmethod/InputMethodSubtype;
    :try_end_26
    .catchall {:try_start_8 .. :try_end_26} :catchall_2f

    #@26
    .line 917
    .local v2, _result:Landroid/view/inputmethod/InputMethodSubtype;
    :goto_26
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 918
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 920
    return-object v2

    #@2d
    .line 913
    .end local v2           #_result:Landroid/view/inputmethod/InputMethodSubtype;
    :cond_2d
    const/4 v2, 0x0

    #@2e
    .restart local v2       #_result:Landroid/view/inputmethod/InputMethodSubtype;
    goto :goto_26

    #@2f
    .line 917
    .end local v2           #_result:Landroid/view/inputmethod/InputMethodSubtype;
    :catchall_2f
    move-exception v3

    #@30
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@33
    .line 918
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@36
    throw v3
.end method

.method public getEnabledInputMethodList()Ljava/util/List;
    .registers 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/view/inputmethod/InputMethodInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 470
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 471
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 474
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.android.internal.view.IInputMethodManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 475
    iget-object v3, p0, Lcom/android/internal/view/IInputMethodManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v4, 0x2

    #@10
    const/4 v5, 0x0

    #@11
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 476
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@17
    .line 477
    sget-object v3, Landroid/view/inputmethod/InputMethodInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@19
    invoke-virtual {v1, v3}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;
    :try_end_1c
    .catchall {:try_start_8 .. :try_end_1c} :catchall_24

    #@1c
    move-result-object v2

    #@1d
    .line 480
    .local v2, _result:Ljava/util/List;,"Ljava/util/List<Landroid/view/inputmethod/InputMethodInfo;>;"
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@20
    .line 481
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 483
    return-object v2

    #@24
    .line 480
    .end local v2           #_result:Ljava/util/List;,"Ljava/util/List<Landroid/view/inputmethod/InputMethodInfo;>;"
    :catchall_24
    move-exception v3

    #@25
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 481
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    throw v3
.end method

.method public getEnabledInputMethodSubtypeList(Landroid/view/inputmethod/InputMethodInfo;Z)Ljava/util/List;
    .registers 9
    .parameter "imi"
    .parameter "allowsImplicitlySelectedSubtypes"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/inputmethod/InputMethodInfo;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Landroid/view/inputmethod/InputMethodSubtype;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 487
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 488
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 491
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v5, "com.android.internal.view.IInputMethodManager"

    #@c
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 492
    if-eqz p1, :cond_35

    #@11
    .line 493
    const/4 v5, 0x1

    #@12
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 494
    const/4 v5, 0x0

    #@16
    invoke-virtual {p1, v0, v5}, Landroid/view/inputmethod/InputMethodInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@19
    .line 499
    :goto_19
    if-eqz p2, :cond_42

    #@1b
    :goto_1b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@1e
    .line 500
    iget-object v3, p0, Lcom/android/internal/view/IInputMethodManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@20
    const/4 v4, 0x3

    #@21
    const/4 v5, 0x0

    #@22
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@25
    .line 501
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@28
    .line 502
    sget-object v3, Landroid/view/inputmethod/InputMethodSubtype;->CREATOR:Landroid/os/Parcelable$Creator;

    #@2a
    invoke-virtual {v1, v3}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;
    :try_end_2d
    .catchall {:try_start_a .. :try_end_2d} :catchall_3a

    #@2d
    move-result-object v2

    #@2e
    .line 505
    .local v2, _result:Ljava/util/List;,"Ljava/util/List<Landroid/view/inputmethod/InputMethodSubtype;>;"
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@31
    .line 506
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@34
    .line 508
    return-object v2

    #@35
    .line 497
    .end local v2           #_result:Ljava/util/List;,"Ljava/util/List<Landroid/view/inputmethod/InputMethodSubtype;>;"
    :cond_35
    const/4 v5, 0x0

    #@36
    :try_start_36
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_39
    .catchall {:try_start_36 .. :try_end_39} :catchall_3a

    #@39
    goto :goto_19

    #@3a
    .line 505
    :catchall_3a
    move-exception v3

    #@3b
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3e
    .line 506
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@41
    throw v3

    #@42
    :cond_42
    move v3, v4

    #@43
    .line 499
    goto :goto_1b
.end method

.method public getInputMethodList()Ljava/util/List;
    .registers 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/view/inputmethod/InputMethodInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 453
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 454
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 457
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.android.internal.view.IInputMethodManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 458
    iget-object v3, p0, Lcom/android/internal/view/IInputMethodManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v4, 0x1

    #@10
    const/4 v5, 0x0

    #@11
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 459
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@17
    .line 460
    sget-object v3, Landroid/view/inputmethod/InputMethodInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@19
    invoke-virtual {v1, v3}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;
    :try_end_1c
    .catchall {:try_start_8 .. :try_end_1c} :catchall_24

    #@1c
    move-result-object v2

    #@1d
    .line 463
    .local v2, _result:Ljava/util/List;,"Ljava/util/List<Landroid/view/inputmethod/InputMethodInfo;>;"
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@20
    .line 464
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 466
    return-object v2

    #@24
    .line 463
    .end local v2           #_result:Ljava/util/List;,"Ljava/util/List<Landroid/view/inputmethod/InputMethodInfo;>;"
    :catchall_24
    move-exception v3

    #@25
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 464
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    throw v3
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 449
    const-string v0, "com.android.internal.view.IInputMethodManager"

    #@2
    return-object v0
.end method

.method public getLastInputMethodSubtype()Landroid/view/inputmethod/InputMethodSubtype;
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 512
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 513
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 516
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.android.internal.view.IInputMethodManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 517
    iget-object v3, p0, Lcom/android/internal/view/IInputMethodManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v4, 0x4

    #@10
    const/4 v5, 0x0

    #@11
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 518
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@17
    .line 519
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@1a
    move-result v3

    #@1b
    if-eqz v3, :cond_2c

    #@1d
    .line 520
    sget-object v3, Landroid/view/inputmethod/InputMethodSubtype;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1f
    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@22
    move-result-object v2

    #@23
    check-cast v2, Landroid/view/inputmethod/InputMethodSubtype;
    :try_end_25
    .catchall {:try_start_8 .. :try_end_25} :catchall_2e

    #@25
    .line 527
    .local v2, _result:Landroid/view/inputmethod/InputMethodSubtype;
    :goto_25
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 528
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 530
    return-object v2

    #@2c
    .line 523
    .end local v2           #_result:Landroid/view/inputmethod/InputMethodSubtype;
    :cond_2c
    const/4 v2, 0x0

    #@2d
    .restart local v2       #_result:Landroid/view/inputmethod/InputMethodSubtype;
    goto :goto_25

    #@2e
    .line 527
    .end local v2           #_result:Landroid/view/inputmethod/InputMethodSubtype;
    :catchall_2e
    move-exception v3

    #@2f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@32
    .line 528
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@35
    throw v3
.end method

.method public getShortcutInputMethodsAndSubtypes()Ljava/util/List;
    .registers 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 537
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 538
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 541
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v4, "com.android.internal.view.IInputMethodManager"

    #@a
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 542
    iget-object v4, p0, Lcom/android/internal/view/IInputMethodManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v5, 0x5

    #@10
    const/4 v6, 0x0

    #@11
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 543
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@17
    .line 544
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@1a
    move-result-object v4

    #@1b
    invoke-virtual {v4}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    #@1e
    move-result-object v3

    #@1f
    .line 545
    .local v3, cl:Ljava/lang/ClassLoader;
    invoke-virtual {v1, v3}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;
    :try_end_22
    .catchall {:try_start_8 .. :try_end_22} :catchall_2a

    #@22
    move-result-object v2

    #@23
    .line 548
    .local v2, _result:Ljava/util/List;
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 549
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 551
    return-object v2

    #@2a
    .line 548
    .end local v2           #_result:Ljava/util/List;
    .end local v3           #cl:Ljava/lang/ClassLoader;
    :catchall_2a
    move-exception v4

    #@2b
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 549
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@31
    throw v4
.end method

.method public hideMySoftInput(Landroid/os/IBinder;I)V
    .registers 8
    .parameter "token"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 795
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 796
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 798
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.android.internal.view.IInputMethodManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 799
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@10
    .line 800
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 801
    iget-object v2, p0, Lcom/android/internal/view/IInputMethodManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/16 v3, 0x11

    #@17
    const/4 v4, 0x0

    #@18
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1b
    .line 802
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1e
    .catchall {:try_start_8 .. :try_end_1e} :catchall_25

    #@1e
    .line 805
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 806
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 808
    return-void

    #@25
    .line 805
    :catchall_25
    move-exception v2

    #@26
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 806
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    throw v2
.end method

.method public hideSoftInput(Lcom/android/internal/view/IInputMethodClient;ILandroid/os/ResultReceiver;)Z
    .registers 11
    .parameter "client"
    .parameter "flags"
    .parameter "resultReceiver"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 661
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 662
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 665
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "com.android.internal.view.IInputMethodManager"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 666
    if-eqz p1, :cond_3d

    #@11
    invoke-interface {p1}, Lcom/android/internal/view/IInputMethodClient;->asBinder()Landroid/os/IBinder;

    #@14
    move-result-object v4

    #@15
    :goto_15
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@18
    .line 667
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@1b
    .line 668
    if-eqz p3, :cond_3f

    #@1d
    .line 669
    const/4 v4, 0x1

    #@1e
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@21
    .line 670
    const/4 v4, 0x0

    #@22
    invoke-virtual {p3, v0, v4}, Landroid/os/ResultReceiver;->writeToParcel(Landroid/os/Parcel;I)V

    #@25
    .line 675
    :goto_25
    iget-object v4, p0, Lcom/android/internal/view/IInputMethodManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@27
    const/16 v5, 0xb

    #@29
    const/4 v6, 0x0

    #@2a
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@2d
    .line 676
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@30
    .line 677
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_33
    .catchall {:try_start_a .. :try_end_33} :catchall_44

    #@33
    move-result v4

    #@34
    if-eqz v4, :cond_4c

    #@36
    .line 680
    .local v2, _result:Z
    :goto_36
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@39
    .line 681
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3c
    .line 683
    return v2

    #@3d
    .line 666
    .end local v2           #_result:Z
    :cond_3d
    const/4 v4, 0x0

    #@3e
    goto :goto_15

    #@3f
    .line 673
    :cond_3f
    const/4 v4, 0x0

    #@40
    :try_start_40
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_43
    .catchall {:try_start_40 .. :try_end_43} :catchall_44

    #@43
    goto :goto_25

    #@44
    .line 680
    :catchall_44
    move-exception v3

    #@45
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@48
    .line 681
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@4b
    throw v3

    #@4c
    :cond_4c
    move v2, v3

    #@4d
    .line 677
    goto :goto_36
.end method

.method public notifySuggestionPicked(Landroid/text/style/SuggestionSpan;Ljava/lang/String;I)Z
    .registers 11
    .parameter "span"
    .parameter "originalString"
    .parameter "index"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 876
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 877
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 880
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "com.android.internal.view.IInputMethodManager"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 881
    if-eqz p1, :cond_37

    #@11
    .line 882
    const/4 v4, 0x1

    #@12
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 883
    const/4 v4, 0x0

    #@16
    invoke-virtual {p1, v0, v4}, Landroid/text/style/SuggestionSpan;->writeToParcel(Landroid/os/Parcel;I)V

    #@19
    .line 888
    :goto_19
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1c
    .line 889
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@1f
    .line 890
    iget-object v4, p0, Lcom/android/internal/view/IInputMethodManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@21
    const/16 v5, 0x16

    #@23
    const/4 v6, 0x0

    #@24
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@27
    .line 891
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@2a
    .line 892
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_2d
    .catchall {:try_start_a .. :try_end_2d} :catchall_3c

    #@2d
    move-result v4

    #@2e
    if-eqz v4, :cond_44

    #@30
    .line 895
    .local v2, _result:Z
    :goto_30
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@33
    .line 896
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@36
    .line 898
    return v2

    #@37
    .line 886
    .end local v2           #_result:Z
    :cond_37
    const/4 v4, 0x0

    #@38
    :try_start_38
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_3b
    .catchall {:try_start_38 .. :try_end_3b} :catchall_3c

    #@3b
    goto :goto_19

    #@3c
    .line 895
    :catchall_3c
    move-exception v3

    #@3d
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@40
    .line 896
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@43
    throw v3

    #@44
    :cond_44
    move v2, v3

    #@45
    .line 892
    goto :goto_30
.end method

.method public registerSuggestionSpansForNotification([Landroid/text/style/SuggestionSpan;)V
    .registers 7
    .parameter "spans"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 861
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 862
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 864
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.android.internal.view.IInputMethodManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 865
    const/4 v2, 0x0

    #@e
    invoke-virtual {v0, p1, v2}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    #@11
    .line 866
    iget-object v2, p0, Lcom/android/internal/view/IInputMethodManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@13
    const/16 v3, 0x15

    #@15
    const/4 v4, 0x0

    #@16
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@19
    .line 867
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1c
    .catchall {:try_start_8 .. :try_end_1c} :catchall_23

    #@1c
    .line 870
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1f
    .line 871
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 873
    return-void

    #@23
    .line 870
    :catchall_23
    move-exception v2

    #@24
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 871
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    throw v2
.end method

.method public removeClient(Lcom/android/internal/view/IInputMethodClient;)V
    .registers 7
    .parameter "client"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 573
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 574
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 576
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.android.internal.view.IInputMethodManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 577
    if-eqz p1, :cond_27

    #@f
    invoke-interface {p1}, Lcom/android/internal/view/IInputMethodClient;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v2

    #@13
    :goto_13
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 578
    iget-object v2, p0, Lcom/android/internal/view/IInputMethodManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/4 v3, 0x7

    #@19
    const/4 v4, 0x0

    #@1a
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1d
    .line 579
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_20
    .catchall {:try_start_8 .. :try_end_20} :catchall_29

    #@20
    .line 582
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 583
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 585
    return-void

    #@27
    .line 577
    :cond_27
    const/4 v2, 0x0

    #@28
    goto :goto_13

    #@29
    .line 582
    :catchall_29
    move-exception v2

    #@2a
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 583
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@30
    throw v2
.end method

.method public setAdditionalInputMethodSubtypes(Ljava/lang/String;[Landroid/view/inputmethod/InputMethodSubtype;)V
    .registers 8
    .parameter "id"
    .parameter "subtypes"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1004
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1006
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "com.android.internal.view.IInputMethodManager"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 1007
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@c
    .line 1008
    const/4 v1, 0x0

    #@d
    invoke-virtual {v0, p2, v1}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    #@10
    .line 1009
    iget-object v1, p0, Lcom/android/internal/view/IInputMethodManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v2, 0x1c

    #@14
    const/4 v3, 0x0

    #@15
    const/4 v4, 0x1

    #@16
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_19
    .catchall {:try_start_4 .. :try_end_19} :catchall_1d

    #@19
    .line 1012
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1c
    .line 1014
    return-void

    #@1d
    .line 1012
    :catchall_1d
    move-exception v1

    #@1e
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    throw v1
.end method

.method public setCurrentInputMethodSubtype(Landroid/view/inputmethod/InputMethodSubtype;)Z
    .registers 9
    .parameter "subtype"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 924
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 925
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 928
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "com.android.internal.view.IInputMethodManager"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 929
    if-eqz p1, :cond_31

    #@11
    .line 930
    const/4 v4, 0x1

    #@12
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 931
    const/4 v4, 0x0

    #@16
    invoke-virtual {p1, v0, v4}, Landroid/view/inputmethod/InputMethodSubtype;->writeToParcel(Landroid/os/Parcel;I)V

    #@19
    .line 936
    :goto_19
    iget-object v4, p0, Lcom/android/internal/view/IInputMethodManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1b
    const/16 v5, 0x18

    #@1d
    const/4 v6, 0x0

    #@1e
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@21
    .line 937
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@24
    .line 938
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_27
    .catchall {:try_start_a .. :try_end_27} :catchall_36

    #@27
    move-result v4

    #@28
    if-eqz v4, :cond_3e

    #@2a
    .line 941
    .local v2, _result:Z
    :goto_2a
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 942
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@30
    .line 944
    return v2

    #@31
    .line 934
    .end local v2           #_result:Z
    :cond_31
    const/4 v4, 0x0

    #@32
    :try_start_32
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_35
    .catchall {:try_start_32 .. :try_end_35} :catchall_36

    #@35
    goto :goto_19

    #@36
    .line 941
    :catchall_36
    move-exception v3

    #@37
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3a
    .line 942
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3d
    throw v3

    #@3e
    :cond_3e
    move v2, v3

    #@3f
    .line 938
    goto :goto_2a
.end method

.method public setImeWindowStatus(Landroid/os/IBinder;II)V
    .registers 9
    .parameter "token"
    .parameter "vis"
    .parameter "backDisposition"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 844
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 845
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 847
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.android.internal.view.IInputMethodManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 848
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@10
    .line 849
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 850
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 851
    iget-object v2, p0, Lcom/android/internal/view/IInputMethodManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/16 v3, 0x14

    #@1a
    const/4 v4, 0x0

    #@1b
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 852
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_21
    .catchall {:try_start_8 .. :try_end_21} :catchall_28

    #@21
    .line 855
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 856
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 858
    return-void

    #@28
    .line 855
    :catchall_28
    move-exception v2

    #@29
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 856
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2f
    throw v2
.end method

.method public setInputMethod(Landroid/os/IBinder;Ljava/lang/String;)V
    .registers 8
    .parameter "token"
    .parameter "id"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 756
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 757
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 759
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.android.internal.view.IInputMethodManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 760
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@10
    .line 761
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@13
    .line 762
    iget-object v2, p0, Lcom/android/internal/view/IInputMethodManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/16 v3, 0xf

    #@17
    const/4 v4, 0x0

    #@18
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1b
    .line 763
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1e
    .catchall {:try_start_8 .. :try_end_1e} :catchall_25

    #@1e
    .line 766
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 767
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 769
    return-void

    #@25
    .line 766
    :catchall_25
    move-exception v2

    #@26
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 767
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    throw v2
.end method

.method public setInputMethodAndSubtype(Landroid/os/IBinder;Ljava/lang/String;Landroid/view/inputmethod/InputMethodSubtype;)V
    .registers 9
    .parameter "token"
    .parameter "id"
    .parameter "subtype"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 772
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 773
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 775
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.android.internal.view.IInputMethodManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 776
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@10
    .line 777
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@13
    .line 778
    if-eqz p3, :cond_2f

    #@15
    .line 779
    const/4 v2, 0x1

    #@16
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 780
    const/4 v2, 0x0

    #@1a
    invoke-virtual {p3, v0, v2}, Landroid/view/inputmethod/InputMethodSubtype;->writeToParcel(Landroid/os/Parcel;I)V

    #@1d
    .line 785
    :goto_1d
    iget-object v2, p0, Lcom/android/internal/view/IInputMethodManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1f
    const/16 v3, 0x10

    #@21
    const/4 v4, 0x0

    #@22
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@25
    .line 786
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_28
    .catchall {:try_start_8 .. :try_end_28} :catchall_34

    #@28
    .line 789
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 790
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 792
    return-void

    #@2f
    .line 783
    :cond_2f
    const/4 v2, 0x0

    #@30
    :try_start_30
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_33
    .catchall {:try_start_30 .. :try_end_33} :catchall_34

    #@33
    goto :goto_1d

    #@34
    .line 789
    :catchall_34
    move-exception v2

    #@35
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@38
    .line 790
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3b
    throw v2
.end method

.method public setInputMethodEnabled(Ljava/lang/String;Z)Z
    .registers 10
    .parameter "id"
    .parameter "enabled"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 985
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 986
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 989
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "com.android.internal.view.IInputMethodManager"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 990
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@12
    .line 991
    if-eqz p2, :cond_30

    #@14
    move v4, v2

    #@15
    :goto_15
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@18
    .line 992
    iget-object v4, p0, Lcom/android/internal/view/IInputMethodManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1a
    const/16 v5, 0x1b

    #@1c
    const/4 v6, 0x0

    #@1d
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@20
    .line 993
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@23
    .line 994
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_26
    .catchall {:try_start_a .. :try_end_26} :catchall_34

    #@26
    move-result v4

    #@27
    if-eqz v4, :cond_32

    #@29
    .line 997
    .local v2, _result:Z
    :goto_29
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 998
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2f
    .line 1000
    return v2

    #@30
    .end local v2           #_result:Z
    :cond_30
    move v4, v3

    #@31
    .line 991
    goto :goto_15

    #@32
    :cond_32
    move v2, v3

    #@33
    .line 994
    goto :goto_29

    #@34
    .line 997
    :catchall_34
    move-exception v3

    #@35
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@38
    .line 998
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3b
    throw v3
.end method

.method public showInputMethodAndSubtypeEnablerFromClient(Lcom/android/internal/view/IInputMethodClient;Ljava/lang/String;)V
    .registers 8
    .parameter "client"
    .parameter "topId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 740
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 741
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 743
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.android.internal.view.IInputMethodManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 744
    if-eqz p1, :cond_2b

    #@f
    invoke-interface {p1}, Lcom/android/internal/view/IInputMethodClient;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v2

    #@13
    :goto_13
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 745
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@19
    .line 746
    iget-object v2, p0, Lcom/android/internal/view/IInputMethodManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1b
    const/16 v3, 0xe

    #@1d
    const/4 v4, 0x0

    #@1e
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@21
    .line 747
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_24
    .catchall {:try_start_8 .. :try_end_24} :catchall_2d

    #@24
    .line 750
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 751
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 753
    return-void

    #@2b
    .line 744
    :cond_2b
    const/4 v2, 0x0

    #@2c
    goto :goto_13

    #@2d
    .line 750
    :catchall_2d
    move-exception v2

    #@2e
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@31
    .line 751
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@34
    throw v2
.end method

.method public showInputMethodPickerFromClient(Lcom/android/internal/view/IInputMethodClient;)V
    .registers 7
    .parameter "client"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 725
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 726
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 728
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.android.internal.view.IInputMethodManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 729
    if-eqz p1, :cond_28

    #@f
    invoke-interface {p1}, Lcom/android/internal/view/IInputMethodClient;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v2

    #@13
    :goto_13
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 730
    iget-object v2, p0, Lcom/android/internal/view/IInputMethodManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/16 v3, 0xd

    #@1a
    const/4 v4, 0x0

    #@1b
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 731
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_21
    .catchall {:try_start_8 .. :try_end_21} :catchall_2a

    #@21
    .line 734
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 735
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 737
    return-void

    #@28
    .line 729
    :cond_28
    const/4 v2, 0x0

    #@29
    goto :goto_13

    #@2a
    .line 734
    :catchall_2a
    move-exception v2

    #@2b
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 735
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@31
    throw v2
.end method

.method public showMySoftInput(Landroid/os/IBinder;I)V
    .registers 8
    .parameter "token"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 811
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 812
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 814
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.android.internal.view.IInputMethodManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 815
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@10
    .line 816
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 817
    iget-object v2, p0, Lcom/android/internal/view/IInputMethodManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/16 v3, 0x12

    #@17
    const/4 v4, 0x0

    #@18
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1b
    .line 818
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1e
    .catchall {:try_start_8 .. :try_end_1e} :catchall_25

    #@1e
    .line 821
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 822
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 824
    return-void

    #@25
    .line 821
    :catchall_25
    move-exception v2

    #@26
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 822
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    throw v2
.end method

.method public showSoftInput(Lcom/android/internal/view/IInputMethodClient;ILandroid/os/ResultReceiver;)Z
    .registers 11
    .parameter "client"
    .parameter "flags"
    .parameter "resultReceiver"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 635
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 636
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 639
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "com.android.internal.view.IInputMethodManager"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 640
    if-eqz p1, :cond_3d

    #@11
    invoke-interface {p1}, Lcom/android/internal/view/IInputMethodClient;->asBinder()Landroid/os/IBinder;

    #@14
    move-result-object v4

    #@15
    :goto_15
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@18
    .line 641
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@1b
    .line 642
    if-eqz p3, :cond_3f

    #@1d
    .line 643
    const/4 v4, 0x1

    #@1e
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@21
    .line 644
    const/4 v4, 0x0

    #@22
    invoke-virtual {p3, v0, v4}, Landroid/os/ResultReceiver;->writeToParcel(Landroid/os/Parcel;I)V

    #@25
    .line 649
    :goto_25
    iget-object v4, p0, Lcom/android/internal/view/IInputMethodManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@27
    const/16 v5, 0xa

    #@29
    const/4 v6, 0x0

    #@2a
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@2d
    .line 650
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@30
    .line 651
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_33
    .catchall {:try_start_a .. :try_end_33} :catchall_44

    #@33
    move-result v4

    #@34
    if-eqz v4, :cond_4c

    #@36
    .line 654
    .local v2, _result:Z
    :goto_36
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@39
    .line 655
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3c
    .line 657
    return v2

    #@3d
    .line 640
    .end local v2           #_result:Z
    :cond_3d
    const/4 v4, 0x0

    #@3e
    goto :goto_15

    #@3f
    .line 647
    :cond_3f
    const/4 v4, 0x0

    #@40
    :try_start_40
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_43
    .catchall {:try_start_40 .. :try_end_43} :catchall_44

    #@43
    goto :goto_25

    #@44
    .line 654
    :catchall_44
    move-exception v3

    #@45
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@48
    .line 655
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@4b
    throw v3

    #@4c
    :cond_4c
    move v2, v3

    #@4d
    .line 651
    goto :goto_36
.end method

.method public startInput(Lcom/android/internal/view/IInputMethodClient;Lcom/android/internal/view/IInputContext;Landroid/view/inputmethod/EditorInfo;I)Lcom/android/internal/view/InputBindResult;
    .registers 11
    .parameter "client"
    .parameter "inputContext"
    .parameter "attribute"
    .parameter "controlFlags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 588
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 589
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 592
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v4, "com.android.internal.view.IInputMethodManager"

    #@b
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 593
    if-eqz p1, :cond_4d

    #@10
    invoke-interface {p1}, Lcom/android/internal/view/IInputMethodClient;->asBinder()Landroid/os/IBinder;

    #@13
    move-result-object v4

    #@14
    :goto_14
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@17
    .line 594
    if-eqz p2, :cond_1d

    #@19
    invoke-interface {p2}, Lcom/android/internal/view/IInputContext;->asBinder()Landroid/os/IBinder;

    #@1c
    move-result-object v3

    #@1d
    :cond_1d
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@20
    .line 595
    if-eqz p3, :cond_4f

    #@22
    .line 596
    const/4 v3, 0x1

    #@23
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@26
    .line 597
    const/4 v3, 0x0

    #@27
    invoke-virtual {p3, v0, v3}, Landroid/view/inputmethod/EditorInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@2a
    .line 602
    :goto_2a
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@2d
    .line 603
    iget-object v3, p0, Lcom/android/internal/view/IInputMethodManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2f
    const/16 v4, 0x8

    #@31
    const/4 v5, 0x0

    #@32
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@35
    .line 604
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@38
    .line 605
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@3b
    move-result v3

    #@3c
    if-eqz v3, :cond_5c

    #@3e
    .line 606
    sget-object v3, Lcom/android/internal/view/InputBindResult;->CREATOR:Landroid/os/Parcelable$Creator;

    #@40
    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@43
    move-result-object v2

    #@44
    check-cast v2, Lcom/android/internal/view/InputBindResult;
    :try_end_46
    .catchall {:try_start_9 .. :try_end_46} :catchall_54

    #@46
    .line 613
    .local v2, _result:Lcom/android/internal/view/InputBindResult;
    :goto_46
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@49
    .line 614
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@4c
    .line 616
    return-object v2

    #@4d
    .end local v2           #_result:Lcom/android/internal/view/InputBindResult;
    :cond_4d
    move-object v4, v3

    #@4e
    .line 593
    goto :goto_14

    #@4f
    .line 600
    :cond_4f
    const/4 v3, 0x0

    #@50
    :try_start_50
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_53
    .catchall {:try_start_50 .. :try_end_53} :catchall_54

    #@53
    goto :goto_2a

    #@54
    .line 613
    :catchall_54
    move-exception v3

    #@55
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@58
    .line 614
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@5b
    throw v3

    #@5c
    .line 609
    :cond_5c
    const/4 v2, 0x0

    #@5d
    .restart local v2       #_result:Lcom/android/internal/view/InputBindResult;
    goto :goto_46
.end method

.method public switchToLastInputMethod(Landroid/os/IBinder;)Z
    .registers 8
    .parameter "token"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 948
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 949
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 952
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "com.android.internal.view.IInputMethodManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 953
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@11
    .line 954
    iget-object v3, p0, Lcom/android/internal/view/IInputMethodManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@13
    const/16 v4, 0x19

    #@15
    const/4 v5, 0x0

    #@16
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@19
    .line 955
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1c
    .line 956
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1f
    .catchall {:try_start_9 .. :try_end_1f} :catchall_2a

    #@1f
    move-result v3

    #@20
    if-eqz v3, :cond_23

    #@22
    const/4 v2, 0x1

    #@23
    .line 959
    .local v2, _result:Z
    :cond_23
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 960
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 962
    return v2

    #@2a
    .line 959
    .end local v2           #_result:Z
    :catchall_2a
    move-exception v3

    #@2b
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 960
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@31
    throw v3
.end method

.method public switchToNextInputMethod(Landroid/os/IBinder;Z)Z
    .registers 10
    .parameter "token"
    .parameter "onlyCurrentIme"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 966
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 967
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 970
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "com.android.internal.view.IInputMethodManager"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 971
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@12
    .line 972
    if-eqz p2, :cond_30

    #@14
    move v4, v2

    #@15
    :goto_15
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@18
    .line 973
    iget-object v4, p0, Lcom/android/internal/view/IInputMethodManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1a
    const/16 v5, 0x1a

    #@1c
    const/4 v6, 0x0

    #@1d
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@20
    .line 974
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@23
    .line 975
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_26
    .catchall {:try_start_a .. :try_end_26} :catchall_34

    #@26
    move-result v4

    #@27
    if-eqz v4, :cond_32

    #@29
    .line 978
    .local v2, _result:Z
    :goto_29
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 979
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2f
    .line 981
    return v2

    #@30
    .end local v2           #_result:Z
    :cond_30
    move v4, v3

    #@31
    .line 972
    goto :goto_15

    #@32
    :cond_32
    move v2, v3

    #@33
    .line 975
    goto :goto_29

    #@34
    .line 978
    :catchall_34
    move-exception v3

    #@35
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@38
    .line 979
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3b
    throw v3
.end method

.method public updateStatusIcon(Landroid/os/IBinder;Ljava/lang/String;I)V
    .registers 9
    .parameter "token"
    .parameter "packageName"
    .parameter "iconId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 827
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 828
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 830
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.android.internal.view.IInputMethodManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 831
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@10
    .line 832
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@13
    .line 833
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 834
    iget-object v2, p0, Lcom/android/internal/view/IInputMethodManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/16 v3, 0x13

    #@1a
    const/4 v4, 0x0

    #@1b
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 835
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_21
    .catchall {:try_start_8 .. :try_end_21} :catchall_28

    #@21
    .line 838
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 839
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 841
    return-void

    #@28
    .line 838
    :catchall_28
    move-exception v2

    #@29
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 839
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2f
    throw v2
.end method

.method public windowGainedFocus(Lcom/android/internal/view/IInputMethodClient;Landroid/os/IBinder;IIILandroid/view/inputmethod/EditorInfo;Lcom/android/internal/view/IInputContext;)Lcom/android/internal/view/InputBindResult;
    .registers 14
    .parameter "client"
    .parameter "windowToken"
    .parameter "controlFlags"
    .parameter "softInputMode"
    .parameter "windowFlags"
    .parameter "attribute"
    .parameter "inputContext"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 690
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 691
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 694
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v4, "com.android.internal.view.IInputMethodManager"

    #@b
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 695
    if-eqz p1, :cond_56

    #@10
    invoke-interface {p1}, Lcom/android/internal/view/IInputMethodClient;->asBinder()Landroid/os/IBinder;

    #@13
    move-result-object v4

    #@14
    :goto_14
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@17
    .line 696
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@1a
    .line 697
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@1d
    .line 698
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@20
    .line 699
    invoke-virtual {v0, p5}, Landroid/os/Parcel;->writeInt(I)V

    #@23
    .line 700
    if-eqz p6, :cond_58

    #@25
    .line 701
    const/4 v4, 0x1

    #@26
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@29
    .line 702
    const/4 v4, 0x0

    #@2a
    invoke-virtual {p6, v0, v4}, Landroid/view/inputmethod/EditorInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@2d
    .line 707
    :goto_2d
    if-eqz p7, :cond_33

    #@2f
    invoke-interface {p7}, Lcom/android/internal/view/IInputContext;->asBinder()Landroid/os/IBinder;

    #@32
    move-result-object v3

    #@33
    :cond_33
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@36
    .line 708
    iget-object v3, p0, Lcom/android/internal/view/IInputMethodManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@38
    const/16 v4, 0xc

    #@3a
    const/4 v5, 0x0

    #@3b
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@3e
    .line 709
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@41
    .line 710
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@44
    move-result v3

    #@45
    if-eqz v3, :cond_65

    #@47
    .line 711
    sget-object v3, Lcom/android/internal/view/InputBindResult;->CREATOR:Landroid/os/Parcelable$Creator;

    #@49
    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@4c
    move-result-object v2

    #@4d
    check-cast v2, Lcom/android/internal/view/InputBindResult;
    :try_end_4f
    .catchall {:try_start_9 .. :try_end_4f} :catchall_5d

    #@4f
    .line 718
    .local v2, _result:Lcom/android/internal/view/InputBindResult;
    :goto_4f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@52
    .line 719
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@55
    .line 721
    return-object v2

    #@56
    .end local v2           #_result:Lcom/android/internal/view/InputBindResult;
    :cond_56
    move-object v4, v3

    #@57
    .line 695
    goto :goto_14

    #@58
    .line 705
    :cond_58
    const/4 v4, 0x0

    #@59
    :try_start_59
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_5c
    .catchall {:try_start_59 .. :try_end_5c} :catchall_5d

    #@5c
    goto :goto_2d

    #@5d
    .line 718
    :catchall_5d
    move-exception v3

    #@5e
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@61
    .line 719
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@64
    throw v3

    #@65
    .line 714
    :cond_65
    const/4 v2, 0x0

    #@66
    .restart local v2       #_result:Lcom/android/internal/view/InputBindResult;
    goto :goto_4f
.end method
