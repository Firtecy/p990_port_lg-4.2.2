.class public abstract Lcom/android/internal/view/IInputMethodClient$Stub;
.super Landroid/os/Binder;
.source "IInputMethodClient.java"

# interfaces
.implements Lcom/android/internal/view/IInputMethodClient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/view/IInputMethodClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/view/IInputMethodClient$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.android.internal.view.IInputMethodClient"

.field static final TRANSACTION_onBindMethod:I = 0x2

.field static final TRANSACTION_onUnbindMethod:I = 0x3

.field static final TRANSACTION_setActive:I = 0x4

.field static final TRANSACTION_setScreenOnOff:I = 0x5

.field static final TRANSACTION_setUsingInputMethod:I = 0x1


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 18
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 19
    const-string v0, "com.android.internal.view.IInputMethodClient"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/android/internal/view/IInputMethodClient$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 20
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/android/internal/view/IInputMethodClient;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 27
    if-nez p0, :cond_4

    #@2
    .line 28
    const/4 v0, 0x0

    #@3
    .line 34
    :goto_3
    return-object v0

    #@4
    .line 30
    :cond_4
    const-string v1, "com.android.internal.view.IInputMethodClient"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 31
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/android/internal/view/IInputMethodClient;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 32
    check-cast v0, Lcom/android/internal/view/IInputMethodClient;

    #@12
    goto :goto_3

    #@13
    .line 34
    :cond_13
    new-instance v0, Lcom/android/internal/view/IInputMethodClient$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/android/internal/view/IInputMethodClient$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 38
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 8
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    .line 42
    sparse-switch p1, :sswitch_data_66

    #@5
    .line 95
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v1

    #@9
    :goto_9
    return v1

    #@a
    .line 46
    :sswitch_a
    const-string v2, "com.android.internal.view.IInputMethodClient"

    #@c
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 51
    :sswitch_10
    const-string v2, "com.android.internal.view.IInputMethodClient"

    #@12
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 53
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@18
    move-result v2

    #@19
    if-eqz v2, :cond_1c

    #@1b
    move v0, v1

    #@1c
    .line 54
    .local v0, _arg0:Z
    :cond_1c
    invoke-virtual {p0, v0}, Lcom/android/internal/view/IInputMethodClient$Stub;->setUsingInputMethod(Z)V

    #@1f
    goto :goto_9

    #@20
    .line 59
    .end local v0           #_arg0:Z
    :sswitch_20
    const-string v2, "com.android.internal.view.IInputMethodClient"

    #@22
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@25
    .line 61
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@28
    move-result v2

    #@29
    if-eqz v2, :cond_37

    #@2b
    .line 62
    sget-object v2, Lcom/android/internal/view/InputBindResult;->CREATOR:Landroid/os/Parcelable$Creator;

    #@2d
    invoke-interface {v2, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@30
    move-result-object v0

    #@31
    check-cast v0, Lcom/android/internal/view/InputBindResult;

    #@33
    .line 67
    .local v0, _arg0:Lcom/android/internal/view/InputBindResult;
    :goto_33
    invoke-virtual {p0, v0}, Lcom/android/internal/view/IInputMethodClient$Stub;->onBindMethod(Lcom/android/internal/view/InputBindResult;)V

    #@36
    goto :goto_9

    #@37
    .line 65
    .end local v0           #_arg0:Lcom/android/internal/view/InputBindResult;
    :cond_37
    const/4 v0, 0x0

    #@38
    .restart local v0       #_arg0:Lcom/android/internal/view/InputBindResult;
    goto :goto_33

    #@39
    .line 72
    .end local v0           #_arg0:Lcom/android/internal/view/InputBindResult;
    :sswitch_39
    const-string v2, "com.android.internal.view.IInputMethodClient"

    #@3b
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3e
    .line 74
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@41
    move-result v0

    #@42
    .line 75
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Lcom/android/internal/view/IInputMethodClient$Stub;->onUnbindMethod(I)V

    #@45
    goto :goto_9

    #@46
    .line 80
    .end local v0           #_arg0:I
    :sswitch_46
    const-string v2, "com.android.internal.view.IInputMethodClient"

    #@48
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4b
    .line 82
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@4e
    move-result v2

    #@4f
    if-eqz v2, :cond_52

    #@51
    move v0, v1

    #@52
    .line 83
    .local v0, _arg0:Z
    :cond_52
    invoke-virtual {p0, v0}, Lcom/android/internal/view/IInputMethodClient$Stub;->setActive(Z)V

    #@55
    goto :goto_9

    #@56
    .line 88
    .end local v0           #_arg0:Z
    :sswitch_56
    const-string v2, "com.android.internal.view.IInputMethodClient"

    #@58
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@5b
    .line 90
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@5e
    move-result v2

    #@5f
    if-eqz v2, :cond_62

    #@61
    move v0, v1

    #@62
    .line 91
    .restart local v0       #_arg0:Z
    :cond_62
    invoke-virtual {p0, v0}, Lcom/android/internal/view/IInputMethodClient$Stub;->setScreenOnOff(Z)V

    #@65
    goto :goto_9

    #@66
    .line 42
    :sswitch_data_66
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_20
        0x3 -> :sswitch_39
        0x4 -> :sswitch_46
        0x5 -> :sswitch_56
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
