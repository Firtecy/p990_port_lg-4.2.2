.class public abstract Lcom/android/internal/view/IInputContext$Stub;
.super Landroid/os/Binder;
.source "IInputContext.java"

# interfaces
.implements Lcom/android/internal/view/IInputContext;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/view/IInputContext;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/view/IInputContext$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.android.internal.view.IInputContext"

.field static final TRANSACTION_beginBatchEdit:I = 0xe

.field static final TRANSACTION_clearMetaKeyStates:I = 0x12

.field static final TRANSACTION_commitCompletion:I = 0x9

.field static final TRANSACTION_commitCorrection:I = 0xa

.field static final TRANSACTION_commitText:I = 0x8

.field static final TRANSACTION_deleteSurroundingText:I = 0x5

.field static final TRANSACTION_endBatchEdit:I = 0xf

.field static final TRANSACTION_finishComposingText:I = 0x7

.field static final TRANSACTION_getCursorCapsMode:I = 0x3

.field static final TRANSACTION_getExtractedText:I = 0x4

.field static final TRANSACTION_getSelectedText:I = 0x15

.field static final TRANSACTION_getTextAfterCursor:I = 0x2

.field static final TRANSACTION_getTextBeforeCursor:I = 0x1

.field static final TRANSACTION_performContextMenuAction:I = 0xd

.field static final TRANSACTION_performEditorAction:I = 0xc

.field static final TRANSACTION_performPrivateCommand:I = 0x13

.field static final TRANSACTION_reportFullscreenMode:I = 0x10

.field static final TRANSACTION_sendKeyEvent:I = 0x11

.field static final TRANSACTION_setComposingRegion:I = 0x14

.field static final TRANSACTION_setComposingText:I = 0x6

.field static final TRANSACTION_setSelection:I = 0xb


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 19
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 20
    const-string v0, "com.android.internal.view.IInputContext"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/android/internal/view/IInputContext$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 21
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/android/internal/view/IInputContext;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 28
    if-nez p0, :cond_4

    #@2
    .line 29
    const/4 v0, 0x0

    #@3
    .line 35
    :goto_3
    return-object v0

    #@4
    .line 31
    :cond_4
    const-string v1, "com.android.internal.view.IInputContext"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 32
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/android/internal/view/IInputContext;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 33
    check-cast v0, Lcom/android/internal/view/IInputContext;

    #@12
    goto :goto_3

    #@13
    .line 35
    :cond_13
    new-instance v0, Lcom/android/internal/view/IInputContext$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/android/internal/view/IInputContext$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 39
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 11
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 43
    sparse-switch p1, :sswitch_data_1de

    #@4
    .line 286
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v4

    #@8
    :goto_8
    return v4

    #@9
    .line 47
    :sswitch_9
    const-string v5, "com.android.internal.view.IInputContext"

    #@b
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    goto :goto_8

    #@f
    .line 52
    :sswitch_f
    const-string v5, "com.android.internal.view.IInputContext"

    #@11
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 54
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@17
    move-result v0

    #@18
    .line 56
    .local v0, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1b
    move-result v1

    #@1c
    .line 58
    .local v1, _arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1f
    move-result v2

    #@20
    .line 60
    .local v2, _arg2:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@23
    move-result-object v5

    #@24
    invoke-static {v5}, Lcom/android/internal/view/IInputContextCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/view/IInputContextCallback;

    #@27
    move-result-object v3

    #@28
    .line 61
    .local v3, _arg3:Lcom/android/internal/view/IInputContextCallback;
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/android/internal/view/IInputContext$Stub;->getTextBeforeCursor(IIILcom/android/internal/view/IInputContextCallback;)V

    #@2b
    goto :goto_8

    #@2c
    .line 66
    .end local v0           #_arg0:I
    .end local v1           #_arg1:I
    .end local v2           #_arg2:I
    .end local v3           #_arg3:Lcom/android/internal/view/IInputContextCallback;
    :sswitch_2c
    const-string v5, "com.android.internal.view.IInputContext"

    #@2e
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@31
    .line 68
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@34
    move-result v0

    #@35
    .line 70
    .restart local v0       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@38
    move-result v1

    #@39
    .line 72
    .restart local v1       #_arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@3c
    move-result v2

    #@3d
    .line 74
    .restart local v2       #_arg2:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@40
    move-result-object v5

    #@41
    invoke-static {v5}, Lcom/android/internal/view/IInputContextCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/view/IInputContextCallback;

    #@44
    move-result-object v3

    #@45
    .line 75
    .restart local v3       #_arg3:Lcom/android/internal/view/IInputContextCallback;
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/android/internal/view/IInputContext$Stub;->getTextAfterCursor(IIILcom/android/internal/view/IInputContextCallback;)V

    #@48
    goto :goto_8

    #@49
    .line 80
    .end local v0           #_arg0:I
    .end local v1           #_arg1:I
    .end local v2           #_arg2:I
    .end local v3           #_arg3:Lcom/android/internal/view/IInputContextCallback;
    :sswitch_49
    const-string v5, "com.android.internal.view.IInputContext"

    #@4b
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4e
    .line 82
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@51
    move-result v0

    #@52
    .line 84
    .restart local v0       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@55
    move-result v1

    #@56
    .line 86
    .restart local v1       #_arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@59
    move-result-object v5

    #@5a
    invoke-static {v5}, Lcom/android/internal/view/IInputContextCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/view/IInputContextCallback;

    #@5d
    move-result-object v2

    #@5e
    .line 87
    .local v2, _arg2:Lcom/android/internal/view/IInputContextCallback;
    invoke-virtual {p0, v0, v1, v2}, Lcom/android/internal/view/IInputContext$Stub;->getCursorCapsMode(IILcom/android/internal/view/IInputContextCallback;)V

    #@61
    goto :goto_8

    #@62
    .line 92
    .end local v0           #_arg0:I
    .end local v1           #_arg1:I
    .end local v2           #_arg2:Lcom/android/internal/view/IInputContextCallback;
    :sswitch_62
    const-string v5, "com.android.internal.view.IInputContext"

    #@64
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@67
    .line 94
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@6a
    move-result v5

    #@6b
    if-eqz v5, :cond_89

    #@6d
    .line 95
    sget-object v5, Landroid/view/inputmethod/ExtractedTextRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    #@6f
    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@72
    move-result-object v0

    #@73
    check-cast v0, Landroid/view/inputmethod/ExtractedTextRequest;

    #@75
    .line 101
    .local v0, _arg0:Landroid/view/inputmethod/ExtractedTextRequest;
    :goto_75
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@78
    move-result v1

    #@79
    .line 103
    .restart local v1       #_arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@7c
    move-result v2

    #@7d
    .line 105
    .local v2, _arg2:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@80
    move-result-object v5

    #@81
    invoke-static {v5}, Lcom/android/internal/view/IInputContextCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/view/IInputContextCallback;

    #@84
    move-result-object v3

    #@85
    .line 106
    .restart local v3       #_arg3:Lcom/android/internal/view/IInputContextCallback;
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/android/internal/view/IInputContext$Stub;->getExtractedText(Landroid/view/inputmethod/ExtractedTextRequest;IILcom/android/internal/view/IInputContextCallback;)V

    #@88
    goto :goto_8

    #@89
    .line 98
    .end local v0           #_arg0:Landroid/view/inputmethod/ExtractedTextRequest;
    .end local v1           #_arg1:I
    .end local v2           #_arg2:I
    .end local v3           #_arg3:Lcom/android/internal/view/IInputContextCallback;
    :cond_89
    const/4 v0, 0x0

    #@8a
    .restart local v0       #_arg0:Landroid/view/inputmethod/ExtractedTextRequest;
    goto :goto_75

    #@8b
    .line 111
    .end local v0           #_arg0:Landroid/view/inputmethod/ExtractedTextRequest;
    :sswitch_8b
    const-string v5, "com.android.internal.view.IInputContext"

    #@8d
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@90
    .line 113
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@93
    move-result v0

    #@94
    .line 115
    .local v0, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@97
    move-result v1

    #@98
    .line 116
    .restart local v1       #_arg1:I
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/view/IInputContext$Stub;->deleteSurroundingText(II)V

    #@9b
    goto/16 :goto_8

    #@9d
    .line 121
    .end local v0           #_arg0:I
    .end local v1           #_arg1:I
    :sswitch_9d
    const-string v5, "com.android.internal.view.IInputContext"

    #@9f
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a2
    .line 123
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@a5
    move-result v5

    #@a6
    if-eqz v5, :cond_b9

    #@a8
    .line 124
    sget-object v5, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    #@aa
    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@ad
    move-result-object v0

    #@ae
    check-cast v0, Ljava/lang/CharSequence;

    #@b0
    .line 130
    .local v0, _arg0:Ljava/lang/CharSequence;
    :goto_b0
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@b3
    move-result v1

    #@b4
    .line 131
    .restart local v1       #_arg1:I
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/view/IInputContext$Stub;->setComposingText(Ljava/lang/CharSequence;I)V

    #@b7
    goto/16 :goto_8

    #@b9
    .line 127
    .end local v0           #_arg0:Ljava/lang/CharSequence;
    .end local v1           #_arg1:I
    :cond_b9
    const/4 v0, 0x0

    #@ba
    .restart local v0       #_arg0:Ljava/lang/CharSequence;
    goto :goto_b0

    #@bb
    .line 136
    .end local v0           #_arg0:Ljava/lang/CharSequence;
    :sswitch_bb
    const-string v5, "com.android.internal.view.IInputContext"

    #@bd
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@c0
    .line 137
    invoke-virtual {p0}, Lcom/android/internal/view/IInputContext$Stub;->finishComposingText()V

    #@c3
    goto/16 :goto_8

    #@c5
    .line 142
    :sswitch_c5
    const-string v5, "com.android.internal.view.IInputContext"

    #@c7
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@ca
    .line 144
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@cd
    move-result v5

    #@ce
    if-eqz v5, :cond_e1

    #@d0
    .line 145
    sget-object v5, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    #@d2
    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@d5
    move-result-object v0

    #@d6
    check-cast v0, Ljava/lang/CharSequence;

    #@d8
    .line 151
    .restart local v0       #_arg0:Ljava/lang/CharSequence;
    :goto_d8
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@db
    move-result v1

    #@dc
    .line 152
    .restart local v1       #_arg1:I
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/view/IInputContext$Stub;->commitText(Ljava/lang/CharSequence;I)V

    #@df
    goto/16 :goto_8

    #@e1
    .line 148
    .end local v0           #_arg0:Ljava/lang/CharSequence;
    .end local v1           #_arg1:I
    :cond_e1
    const/4 v0, 0x0

    #@e2
    .restart local v0       #_arg0:Ljava/lang/CharSequence;
    goto :goto_d8

    #@e3
    .line 157
    .end local v0           #_arg0:Ljava/lang/CharSequence;
    :sswitch_e3
    const-string v5, "com.android.internal.view.IInputContext"

    #@e5
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@e8
    .line 159
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@eb
    move-result v5

    #@ec
    if-eqz v5, :cond_fb

    #@ee
    .line 160
    sget-object v5, Landroid/view/inputmethod/CompletionInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@f0
    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@f3
    move-result-object v0

    #@f4
    check-cast v0, Landroid/view/inputmethod/CompletionInfo;

    #@f6
    .line 165
    .local v0, _arg0:Landroid/view/inputmethod/CompletionInfo;
    :goto_f6
    invoke-virtual {p0, v0}, Lcom/android/internal/view/IInputContext$Stub;->commitCompletion(Landroid/view/inputmethod/CompletionInfo;)V

    #@f9
    goto/16 :goto_8

    #@fb
    .line 163
    .end local v0           #_arg0:Landroid/view/inputmethod/CompletionInfo;
    :cond_fb
    const/4 v0, 0x0

    #@fc
    .restart local v0       #_arg0:Landroid/view/inputmethod/CompletionInfo;
    goto :goto_f6

    #@fd
    .line 170
    .end local v0           #_arg0:Landroid/view/inputmethod/CompletionInfo;
    :sswitch_fd
    const-string v5, "com.android.internal.view.IInputContext"

    #@ff
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@102
    .line 172
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@105
    move-result v5

    #@106
    if-eqz v5, :cond_115

    #@108
    .line 173
    sget-object v5, Landroid/view/inputmethod/CorrectionInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@10a
    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@10d
    move-result-object v0

    #@10e
    check-cast v0, Landroid/view/inputmethod/CorrectionInfo;

    #@110
    .line 178
    .local v0, _arg0:Landroid/view/inputmethod/CorrectionInfo;
    :goto_110
    invoke-virtual {p0, v0}, Lcom/android/internal/view/IInputContext$Stub;->commitCorrection(Landroid/view/inputmethod/CorrectionInfo;)V

    #@113
    goto/16 :goto_8

    #@115
    .line 176
    .end local v0           #_arg0:Landroid/view/inputmethod/CorrectionInfo;
    :cond_115
    const/4 v0, 0x0

    #@116
    .restart local v0       #_arg0:Landroid/view/inputmethod/CorrectionInfo;
    goto :goto_110

    #@117
    .line 183
    .end local v0           #_arg0:Landroid/view/inputmethod/CorrectionInfo;
    :sswitch_117
    const-string v5, "com.android.internal.view.IInputContext"

    #@119
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@11c
    .line 185
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@11f
    move-result v0

    #@120
    .line 187
    .local v0, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@123
    move-result v1

    #@124
    .line 188
    .restart local v1       #_arg1:I
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/view/IInputContext$Stub;->setSelection(II)V

    #@127
    goto/16 :goto_8

    #@129
    .line 193
    .end local v0           #_arg0:I
    .end local v1           #_arg1:I
    :sswitch_129
    const-string v5, "com.android.internal.view.IInputContext"

    #@12b
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@12e
    .line 195
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@131
    move-result v0

    #@132
    .line 196
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/android/internal/view/IInputContext$Stub;->performEditorAction(I)V

    #@135
    goto/16 :goto_8

    #@137
    .line 201
    .end local v0           #_arg0:I
    :sswitch_137
    const-string v5, "com.android.internal.view.IInputContext"

    #@139
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@13c
    .line 203
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@13f
    move-result v0

    #@140
    .line 204
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/android/internal/view/IInputContext$Stub;->performContextMenuAction(I)V

    #@143
    goto/16 :goto_8

    #@145
    .line 209
    .end local v0           #_arg0:I
    :sswitch_145
    const-string v5, "com.android.internal.view.IInputContext"

    #@147
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14a
    .line 210
    invoke-virtual {p0}, Lcom/android/internal/view/IInputContext$Stub;->beginBatchEdit()V

    #@14d
    goto/16 :goto_8

    #@14f
    .line 215
    :sswitch_14f
    const-string v5, "com.android.internal.view.IInputContext"

    #@151
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@154
    .line 216
    invoke-virtual {p0}, Lcom/android/internal/view/IInputContext$Stub;->endBatchEdit()V

    #@157
    goto/16 :goto_8

    #@159
    .line 221
    :sswitch_159
    const-string v5, "com.android.internal.view.IInputContext"

    #@15b
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15e
    .line 223
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@161
    move-result v5

    #@162
    if-eqz v5, :cond_16a

    #@164
    move v0, v4

    #@165
    .line 224
    .local v0, _arg0:Z
    :goto_165
    invoke-virtual {p0, v0}, Lcom/android/internal/view/IInputContext$Stub;->reportFullscreenMode(Z)V

    #@168
    goto/16 :goto_8

    #@16a
    .line 223
    .end local v0           #_arg0:Z
    :cond_16a
    const/4 v0, 0x0

    #@16b
    goto :goto_165

    #@16c
    .line 229
    :sswitch_16c
    const-string v5, "com.android.internal.view.IInputContext"

    #@16e
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@171
    .line 231
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@174
    move-result v5

    #@175
    if-eqz v5, :cond_184

    #@177
    .line 232
    sget-object v5, Landroid/view/KeyEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@179
    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@17c
    move-result-object v0

    #@17d
    check-cast v0, Landroid/view/KeyEvent;

    #@17f
    .line 237
    .local v0, _arg0:Landroid/view/KeyEvent;
    :goto_17f
    invoke-virtual {p0, v0}, Lcom/android/internal/view/IInputContext$Stub;->sendKeyEvent(Landroid/view/KeyEvent;)V

    #@182
    goto/16 :goto_8

    #@184
    .line 235
    .end local v0           #_arg0:Landroid/view/KeyEvent;
    :cond_184
    const/4 v0, 0x0

    #@185
    .restart local v0       #_arg0:Landroid/view/KeyEvent;
    goto :goto_17f

    #@186
    .line 242
    .end local v0           #_arg0:Landroid/view/KeyEvent;
    :sswitch_186
    const-string v5, "com.android.internal.view.IInputContext"

    #@188
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@18b
    .line 244
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@18e
    move-result v0

    #@18f
    .line 245
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Lcom/android/internal/view/IInputContext$Stub;->clearMetaKeyStates(I)V

    #@192
    goto/16 :goto_8

    #@194
    .line 250
    .end local v0           #_arg0:I
    :sswitch_194
    const-string v5, "com.android.internal.view.IInputContext"

    #@196
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@199
    .line 252
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@19c
    move-result-object v0

    #@19d
    .line 254
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1a0
    move-result v5

    #@1a1
    if-eqz v5, :cond_1b0

    #@1a3
    .line 255
    sget-object v5, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1a5
    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1a8
    move-result-object v1

    #@1a9
    check-cast v1, Landroid/os/Bundle;

    #@1ab
    .line 260
    .local v1, _arg1:Landroid/os/Bundle;
    :goto_1ab
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/view/IInputContext$Stub;->performPrivateCommand(Ljava/lang/String;Landroid/os/Bundle;)V

    #@1ae
    goto/16 :goto_8

    #@1b0
    .line 258
    .end local v1           #_arg1:Landroid/os/Bundle;
    :cond_1b0
    const/4 v1, 0x0

    #@1b1
    .restart local v1       #_arg1:Landroid/os/Bundle;
    goto :goto_1ab

    #@1b2
    .line 265
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v1           #_arg1:Landroid/os/Bundle;
    :sswitch_1b2
    const-string v5, "com.android.internal.view.IInputContext"

    #@1b4
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1b7
    .line 267
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1ba
    move-result v0

    #@1bb
    .line 269
    .local v0, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1be
    move-result v1

    #@1bf
    .line 270
    .local v1, _arg1:I
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/view/IInputContext$Stub;->setComposingRegion(II)V

    #@1c2
    goto/16 :goto_8

    #@1c4
    .line 275
    .end local v0           #_arg0:I
    .end local v1           #_arg1:I
    :sswitch_1c4
    const-string v5, "com.android.internal.view.IInputContext"

    #@1c6
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1c9
    .line 277
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1cc
    move-result v0

    #@1cd
    .line 279
    .restart local v0       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1d0
    move-result v1

    #@1d1
    .line 281
    .restart local v1       #_arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@1d4
    move-result-object v5

    #@1d5
    invoke-static {v5}, Lcom/android/internal/view/IInputContextCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/view/IInputContextCallback;

    #@1d8
    move-result-object v2

    #@1d9
    .line 282
    .local v2, _arg2:Lcom/android/internal/view/IInputContextCallback;
    invoke-virtual {p0, v0, v1, v2}, Lcom/android/internal/view/IInputContext$Stub;->getSelectedText(IILcom/android/internal/view/IInputContextCallback;)V

    #@1dc
    goto/16 :goto_8

    #@1de
    .line 43
    :sswitch_data_1de
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_2c
        0x3 -> :sswitch_49
        0x4 -> :sswitch_62
        0x5 -> :sswitch_8b
        0x6 -> :sswitch_9d
        0x7 -> :sswitch_bb
        0x8 -> :sswitch_c5
        0x9 -> :sswitch_e3
        0xa -> :sswitch_fd
        0xb -> :sswitch_117
        0xc -> :sswitch_129
        0xd -> :sswitch_137
        0xe -> :sswitch_145
        0xf -> :sswitch_14f
        0x10 -> :sswitch_159
        0x11 -> :sswitch_16c
        0x12 -> :sswitch_186
        0x13 -> :sswitch_194
        0x14 -> :sswitch_1b2
        0x15 -> :sswitch_1c4
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
