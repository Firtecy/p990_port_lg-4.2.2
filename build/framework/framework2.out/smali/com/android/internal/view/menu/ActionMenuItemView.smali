.class public Lcom/android/internal/view/menu/ActionMenuItemView;
.super Landroid/widget/TextView;
.source "ActionMenuItemView.java"

# interfaces
.implements Lcom/android/internal/view/menu/MenuView$ItemView;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Lcom/android/internal/view/menu/ActionMenuView$ActionMenuChildView;


# static fields
.field private static final MAX_ICON_SIZE:I = 0x40

.field private static final TAG:Ljava/lang/String; = "ActionMenuItemView"


# instance fields
.field private mAllowTextWithIcon:Z

.field private mExpandedFormat:Z

.field private mIcon:Landroid/graphics/drawable/Drawable;

.field private mItemData:Lcom/android/internal/view/menu/MenuItemImpl;

.field private mItemInvoker:Lcom/android/internal/view/menu/MenuBuilder$ItemInvoker;

.field private mMaxIconSize:I

.field private mMinWidth:I

.field private mSavedPaddingLeft:I

.field private mTitle:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 58
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Lcom/android/internal/view/menu/ActionMenuItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 62
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Lcom/android/internal/view/menu/ActionMenuItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 9
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 66
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 67
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@7
    move-result-object v2

    #@8
    .line 68
    .local v2, res:Landroid/content/res/Resources;
    const v3, 0x111003e

    #@b
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@e
    move-result v3

    #@f
    iput-boolean v3, p0, Lcom/android/internal/view/menu/ActionMenuItemView;->mAllowTextWithIcon:Z

    #@11
    .line 70
    sget-object v3, Lcom/android/internal/R$styleable;->ActionMenuItemView:[I

    #@13
    invoke-virtual {p1, p2, v3, v4, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@16
    move-result-object v0

    #@17
    .line 72
    .local v0, a:Landroid/content/res/TypedArray;
    invoke-virtual {v0, v4, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@1a
    move-result v3

    #@1b
    iput v3, p0, Lcom/android/internal/view/menu/ActionMenuItemView;->mMinWidth:I

    #@1d
    .line 74
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@20
    .line 76
    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@23
    move-result-object v3

    #@24
    iget v1, v3, Landroid/util/DisplayMetrics;->density:F

    #@26
    .line 77
    .local v1, density:F
    const/high16 v3, 0x4280

    #@28
    mul-float/2addr v3, v1

    #@29
    const/high16 v4, 0x3f00

    #@2b
    add-float/2addr v3, v4

    #@2c
    float-to-int v3, v3

    #@2d
    iput v3, p0, Lcom/android/internal/view/menu/ActionMenuItemView;->mMaxIconSize:I

    #@2f
    .line 79
    invoke-virtual {p0, p0}, Lcom/android/internal/view/menu/ActionMenuItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@32
    .line 80
    invoke-virtual {p0, p0}, Lcom/android/internal/view/menu/ActionMenuItemView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    #@35
    .line 82
    const/4 v3, -0x1

    #@36
    iput v3, p0, Lcom/android/internal/view/menu/ActionMenuItemView;->mSavedPaddingLeft:I

    #@38
    .line 83
    return-void
.end method

.method private updateTextButtonVisibility()V
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 147
    iget-object v3, p0, Lcom/android/internal/view/menu/ActionMenuItemView;->mTitle:Ljava/lang/CharSequence;

    #@4
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@7
    move-result v3

    #@8
    if-nez v3, :cond_29

    #@a
    move v0, v1

    #@b
    .line 148
    .local v0, visible:Z
    :goto_b
    iget-object v3, p0, Lcom/android/internal/view/menu/ActionMenuItemView;->mIcon:Landroid/graphics/drawable/Drawable;

    #@d
    if-eqz v3, :cond_1f

    #@f
    iget-object v3, p0, Lcom/android/internal/view/menu/ActionMenuItemView;->mItemData:Lcom/android/internal/view/menu/MenuItemImpl;

    #@11
    invoke-virtual {v3}, Lcom/android/internal/view/menu/MenuItemImpl;->showsTextAsAction()Z

    #@14
    move-result v3

    #@15
    if-eqz v3, :cond_20

    #@17
    iget-boolean v3, p0, Lcom/android/internal/view/menu/ActionMenuItemView;->mAllowTextWithIcon:Z

    #@19
    if-nez v3, :cond_1f

    #@1b
    iget-boolean v3, p0, Lcom/android/internal/view/menu/ActionMenuItemView;->mExpandedFormat:Z

    #@1d
    if-eqz v3, :cond_20

    #@1f
    :cond_1f
    move v2, v1

    #@20
    :cond_20
    and-int/2addr v0, v2

    #@21
    .line 151
    if-eqz v0, :cond_2b

    #@23
    iget-object v1, p0, Lcom/android/internal/view/menu/ActionMenuItemView;->mTitle:Ljava/lang/CharSequence;

    #@25
    :goto_25
    invoke-virtual {p0, v1}, Lcom/android/internal/view/menu/ActionMenuItemView;->setText(Ljava/lang/CharSequence;)V

    #@28
    .line 152
    return-void

    #@29
    .end local v0           #visible:Z
    :cond_29
    move v0, v2

    #@2a
    .line 147
    goto :goto_b

    #@2b
    .line 151
    .restart local v0       #visible:Z
    :cond_2b
    const/4 v1, 0x0

    #@2c
    goto :goto_25
.end method


# virtual methods
.method public dispatchHoverEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 211
    invoke-virtual {p0, p1}, Lcom/android/internal/view/menu/ActionMenuItemView;->onHoverEvent(Landroid/view/MotionEvent;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 195
    invoke-virtual {p0, p1}, Lcom/android/internal/view/menu/ActionMenuItemView;->onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 196
    const/4 v0, 0x1

    #@4
    return v0
.end method

.method public getItemData()Lcom/android/internal/view/menu/MenuItemImpl;
    .registers 2

    #@0
    .prologue
    .line 101
    iget-object v0, p0, Lcom/android/internal/view/menu/ActionMenuItemView;->mItemData:Lcom/android/internal/view/menu/MenuItemImpl;

    #@2
    return-object v0
.end method

.method public hasText()Z
    .registers 2

    #@0
    .prologue
    .line 179
    invoke-virtual {p0}, Lcom/android/internal/view/menu/ActionMenuItemView;->getText()Ljava/lang/CharSequence;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method public initialize(Lcom/android/internal/view/menu/MenuItemImpl;I)V
    .registers 4
    .parameter "itemData"
    .parameter "menuType"

    #@0
    .prologue
    .line 105
    iput-object p1, p0, Lcom/android/internal/view/menu/ActionMenuItemView;->mItemData:Lcom/android/internal/view/menu/MenuItemImpl;

    #@2
    .line 107
    invoke-virtual {p1}, Lcom/android/internal/view/menu/MenuItemImpl;->getIcon()Landroid/graphics/drawable/Drawable;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {p0, v0}, Lcom/android/internal/view/menu/ActionMenuItemView;->setIcon(Landroid/graphics/drawable/Drawable;)V

    #@9
    .line 108
    invoke-virtual {p1, p0}, Lcom/android/internal/view/menu/MenuItemImpl;->getTitleForItemView(Lcom/android/internal/view/menu/MenuView$ItemView;)Ljava/lang/CharSequence;

    #@c
    move-result-object v0

    #@d
    invoke-virtual {p0, v0}, Lcom/android/internal/view/menu/ActionMenuItemView;->setTitle(Ljava/lang/CharSequence;)V

    #@10
    .line 109
    invoke-virtual {p1}, Lcom/android/internal/view/menu/MenuItemImpl;->getItemId()I

    #@13
    move-result v0

    #@14
    invoke-virtual {p0, v0}, Lcom/android/internal/view/menu/ActionMenuItemView;->setId(I)V

    #@17
    .line 111
    invoke-virtual {p1}, Lcom/android/internal/view/menu/MenuItemImpl;->isVisible()Z

    #@1a
    move-result v0

    #@1b
    if-eqz v0, :cond_29

    #@1d
    const/4 v0, 0x0

    #@1e
    :goto_1e
    invoke-virtual {p0, v0}, Lcom/android/internal/view/menu/ActionMenuItemView;->setVisibility(I)V

    #@21
    .line 112
    invoke-virtual {p1}, Lcom/android/internal/view/menu/MenuItemImpl;->isEnabled()Z

    #@24
    move-result v0

    #@25
    invoke-virtual {p0, v0}, Lcom/android/internal/view/menu/ActionMenuItemView;->setEnabled(Z)V

    #@28
    .line 113
    return-void

    #@29
    .line 111
    :cond_29
    const/16 v0, 0x8

    #@2b
    goto :goto_1e
.end method

.method public needsDividerAfter()Z
    .registers 2

    #@0
    .prologue
    .line 223
    invoke-virtual {p0}, Lcom/android/internal/view/menu/ActionMenuItemView;->hasText()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public needsDividerBefore()Z
    .registers 2

    #@0
    .prologue
    .line 219
    invoke-virtual {p0}, Lcom/android/internal/view/menu/ActionMenuItemView;->hasText()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_10

    #@6
    iget-object v0, p0, Lcom/android/internal/view/menu/ActionMenuItemView;->mItemData:Lcom/android/internal/view/menu/MenuItemImpl;

    #@8
    invoke-virtual {v0}, Lcom/android/internal/view/menu/MenuItemImpl;->getIcon()Landroid/graphics/drawable/Drawable;

    #@b
    move-result-object v0

    #@c
    if-nez v0, :cond_10

    #@e
    const/4 v0, 0x1

    #@f
    :goto_f
    return v0

    #@10
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_f
.end method

.method public onClick(Landroid/view/View;)V
    .registers 4
    .parameter "v"

    #@0
    .prologue
    .line 116
    iget-object v0, p0, Lcom/android/internal/view/menu/ActionMenuItemView;->mItemInvoker:Lcom/android/internal/view/menu/MenuBuilder$ItemInvoker;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 117
    iget-object v0, p0, Lcom/android/internal/view/menu/ActionMenuItemView;->mItemInvoker:Lcom/android/internal/view/menu/MenuBuilder$ItemInvoker;

    #@6
    iget-object v1, p0, Lcom/android/internal/view/menu/ActionMenuItemView;->mItemData:Lcom/android/internal/view/menu/MenuItemImpl;

    #@8
    invoke-interface {v0, v1}, Lcom/android/internal/view/menu/MenuBuilder$ItemInvoker;->invokeItem(Lcom/android/internal/view/menu/MenuItemImpl;)Z

    #@b
    .line 119
    :cond_b
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 4
    .parameter "newConfig"

    #@0
    .prologue
    .line 87
    invoke-super {p0, p1}, Landroid/widget/TextView;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    #@3
    .line 89
    invoke-virtual {p0}, Lcom/android/internal/view/menu/ActionMenuItemView;->getContext()Landroid/content/Context;

    #@6
    move-result-object v0

    #@7
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@a
    move-result-object v0

    #@b
    const v1, 0x111003e

    #@e
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@11
    move-result v0

    #@12
    iput-boolean v0, p0, Lcom/android/internal/view/menu/ActionMenuItemView;->mAllowTextWithIcon:Z

    #@14
    .line 91
    invoke-direct {p0}, Lcom/android/internal/view/menu/ActionMenuItemView;->updateTextButtonVisibility()V

    #@17
    .line 92
    return-void
.end method

.method public onLongClick(Landroid/view/View;)Z
    .registers 15
    .parameter "v"

    #@0
    .prologue
    const v12, 0x800035

    #@3
    const/4 v9, 0x1

    #@4
    const/4 v8, 0x0

    #@5
    .line 228
    invoke-virtual {p0}, Lcom/android/internal/view/menu/ActionMenuItemView;->hasText()Z

    #@8
    move-result v10

    #@9
    if-eqz v10, :cond_c

    #@b
    .line 260
    :goto_b
    return v8

    #@c
    .line 233
    :cond_c
    const/4 v10, 0x2

    #@d
    new-array v5, v10, [I

    #@f
    .line 234
    .local v5, screenPos:[I
    new-instance v2, Landroid/graphics/Rect;

    #@11
    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    #@14
    .line 235
    .local v2, displayFrame:Landroid/graphics/Rect;
    invoke-virtual {p0, v5}, Lcom/android/internal/view/menu/ActionMenuItemView;->getLocationOnScreen([I)V

    #@17
    .line 236
    invoke-virtual {p0, v2}, Lcom/android/internal/view/menu/ActionMenuItemView;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    #@1a
    .line 238
    invoke-virtual {p0}, Lcom/android/internal/view/menu/ActionMenuItemView;->getContext()Landroid/content/Context;

    #@1d
    move-result-object v1

    #@1e
    .line 239
    .local v1, context:Landroid/content/Context;
    invoke-virtual {p0}, Lcom/android/internal/view/menu/ActionMenuItemView;->getWidth()I

    #@21
    move-result v7

    #@22
    .line 240
    .local v7, width:I
    invoke-virtual {p0}, Lcom/android/internal/view/menu/ActionMenuItemView;->getHeight()I

    #@25
    move-result v3

    #@26
    .line 241
    .local v3, height:I
    aget v10, v5, v9

    #@28
    div-int/lit8 v11, v3, 0x2

    #@2a
    add-int v4, v10, v11

    #@2c
    .line 242
    .local v4, midy:I
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@2f
    move-result-object v10

    #@30
    invoke-virtual {v10}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@33
    move-result-object v10

    #@34
    iget v6, v10, Landroid/util/DisplayMetrics;->widthPixels:I

    #@36
    .line 244
    .local v6, screenWidth:I
    iget-object v10, p0, Lcom/android/internal/view/menu/ActionMenuItemView;->mItemData:Lcom/android/internal/view/menu/MenuItemImpl;

    #@38
    invoke-virtual {v10}, Lcom/android/internal/view/menu/MenuItemImpl;->getTitle()Ljava/lang/CharSequence;

    #@3b
    move-result-object v10

    #@3c
    invoke-static {v1, v10, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@3f
    move-result-object v0

    #@40
    .line 245
    .local v0, cheatSheet:Landroid/widget/Toast;
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    #@43
    move-result v10

    #@44
    if-ge v4, v10, :cond_64

    #@46
    .line 247
    invoke-virtual {p0}, Lcom/android/internal/view/menu/ActionMenuItemView;->isLayoutRtl()Z

    #@49
    move-result v10

    #@4a
    if-eqz v10, :cond_59

    #@4c
    .line 248
    aget v8, v5, v8

    #@4e
    div-int/lit8 v10, v7, 0x2

    #@50
    add-int/2addr v8, v10

    #@51
    invoke-virtual {v0, v12, v8, v3}, Landroid/widget/Toast;->setGravity(III)V

    #@54
    .line 259
    :goto_54
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    #@57
    move v8, v9

    #@58
    .line 260
    goto :goto_b

    #@59
    .line 252
    :cond_59
    aget v8, v5, v8

    #@5b
    sub-int v8, v6, v8

    #@5d
    div-int/lit8 v10, v7, 0x2

    #@5f
    sub-int/2addr v8, v10

    #@60
    invoke-virtual {v0, v12, v8, v3}, Landroid/widget/Toast;->setGravity(III)V

    #@63
    goto :goto_54

    #@64
    .line 257
    :cond_64
    const/16 v10, 0x51

    #@66
    invoke-virtual {v0, v10, v8, v3}, Landroid/widget/Toast;->setGravity(III)V

    #@69
    goto :goto_54
.end method

.method protected onMeasure(II)V
    .registers 16
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    const/high16 v12, -0x8000

    #@2
    const/high16 v11, 0x4000

    #@4
    .line 265
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@7
    move-result v7

    #@8
    if-ne v7, v12, :cond_12

    #@a
    .line 267
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@d
    move-result v7

    #@e
    invoke-static {v7, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@11
    move-result p2

    #@12
    .line 270
    :cond_12
    invoke-virtual {p0}, Lcom/android/internal/view/menu/ActionMenuItemView;->hasText()Z

    #@15
    move-result v3

    #@16
    .line 271
    .local v3, textVisible:Z
    if-eqz v3, :cond_2d

    #@18
    iget v7, p0, Lcom/android/internal/view/menu/ActionMenuItemView;->mSavedPaddingLeft:I

    #@1a
    if-ltz v7, :cond_2d

    #@1c
    .line 272
    iget v7, p0, Lcom/android/internal/view/menu/ActionMenuItemView;->mSavedPaddingLeft:I

    #@1e
    invoke-virtual {p0}, Lcom/android/internal/view/menu/ActionMenuItemView;->getPaddingTop()I

    #@21
    move-result v8

    #@22
    invoke-virtual {p0}, Lcom/android/internal/view/menu/ActionMenuItemView;->getPaddingRight()I

    #@25
    move-result v9

    #@26
    invoke-virtual {p0}, Lcom/android/internal/view/menu/ActionMenuItemView;->getPaddingBottom()I

    #@29
    move-result v10

    #@2a
    invoke-super {p0, v7, v8, v9, v10}, Landroid/widget/TextView;->setPadding(IIII)V

    #@2d
    .line 276
    :cond_2d
    invoke-super {p0, p1, p2}, Landroid/widget/TextView;->onMeasure(II)V

    #@30
    .line 278
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@33
    move-result v5

    #@34
    .line 279
    .local v5, widthMode:I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@37
    move-result v6

    #@38
    .line 280
    .local v6, widthSize:I
    invoke-virtual {p0}, Lcom/android/internal/view/menu/ActionMenuItemView;->getMeasuredWidth()I

    #@3b
    move-result v1

    #@3c
    .line 281
    .local v1, oldMeasuredWidth:I
    if-ne v5, v12, :cond_7b

    #@3e
    iget v7, p0, Lcom/android/internal/view/menu/ActionMenuItemView;->mMinWidth:I

    #@40
    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    #@43
    move-result v2

    #@44
    .line 284
    .local v2, targetWidth:I
    :goto_44
    if-eq v5, v11, :cond_53

    #@46
    iget v7, p0, Lcom/android/internal/view/menu/ActionMenuItemView;->mMinWidth:I

    #@48
    if-lez v7, :cond_53

    #@4a
    if-ge v1, v2, :cond_53

    #@4c
    .line 286
    invoke-static {v2, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@4f
    move-result v7

    #@50
    invoke-super {p0, v7, p2}, Landroid/widget/TextView;->onMeasure(II)V

    #@53
    .line 290
    :cond_53
    if-nez v3, :cond_7a

    #@55
    iget-object v7, p0, Lcom/android/internal/view/menu/ActionMenuItemView;->mIcon:Landroid/graphics/drawable/Drawable;

    #@57
    if-eqz v7, :cond_7a

    #@59
    .line 293
    invoke-virtual {p0}, Lcom/android/internal/view/menu/ActionMenuItemView;->getMeasuredWidth()I

    #@5c
    move-result v4

    #@5d
    .line 294
    .local v4, w:I
    iget-object v7, p0, Lcom/android/internal/view/menu/ActionMenuItemView;->mIcon:Landroid/graphics/drawable/Drawable;

    #@5f
    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    #@62
    move-result-object v7

    #@63
    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    #@66
    move-result v0

    #@67
    .line 295
    .local v0, dw:I
    sub-int v7, v4, v0

    #@69
    div-int/lit8 v7, v7, 0x2

    #@6b
    invoke-virtual {p0}, Lcom/android/internal/view/menu/ActionMenuItemView;->getPaddingTop()I

    #@6e
    move-result v8

    #@6f
    invoke-virtual {p0}, Lcom/android/internal/view/menu/ActionMenuItemView;->getPaddingRight()I

    #@72
    move-result v9

    #@73
    invoke-virtual {p0}, Lcom/android/internal/view/menu/ActionMenuItemView;->getPaddingBottom()I

    #@76
    move-result v10

    #@77
    invoke-super {p0, v7, v8, v9, v10}, Landroid/widget/TextView;->setPadding(IIII)V

    #@7a
    .line 297
    .end local v0           #dw:I
    .end local v4           #w:I
    :cond_7a
    return-void

    #@7b
    .line 281
    .end local v2           #targetWidth:I
    :cond_7b
    iget v2, p0, Lcom/android/internal/view/menu/ActionMenuItemView;->mMinWidth:I

    #@7d
    goto :goto_44
.end method

.method public onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 4
    .parameter "event"

    #@0
    .prologue
    .line 201
    invoke-super {p0, p1}, Landroid/widget/TextView;->onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@3
    .line 202
    invoke-virtual {p0}, Lcom/android/internal/view/menu/ActionMenuItemView;->getContentDescription()Ljava/lang/CharSequence;

    #@6
    move-result-object v0

    #@7
    .line 203
    .local v0, cdesc:Ljava/lang/CharSequence;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@a
    move-result v1

    #@b
    if-nez v1, :cond_14

    #@d
    .line 204
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    #@10
    move-result-object v1

    #@11
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@14
    .line 206
    :cond_14
    return-void
.end method

.method public prefersCondensedTitle()Z
    .registers 2

    #@0
    .prologue
    .line 126
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public setCheckable(Z)V
    .registers 2
    .parameter "checkable"

    #@0
    .prologue
    .line 131
    return-void
.end method

.method public setChecked(Z)V
    .registers 2
    .parameter "checked"

    #@0
    .prologue
    .line 135
    return-void
.end method

.method public setExpandedFormat(Z)V
    .registers 3
    .parameter "expandedFormat"

    #@0
    .prologue
    .line 138
    iget-boolean v0, p0, Lcom/android/internal/view/menu/ActionMenuItemView;->mExpandedFormat:Z

    #@2
    if-eq v0, p1, :cond_f

    #@4
    .line 139
    iput-boolean p1, p0, Lcom/android/internal/view/menu/ActionMenuItemView;->mExpandedFormat:Z

    #@6
    .line 140
    iget-object v0, p0, Lcom/android/internal/view/menu/ActionMenuItemView;->mItemData:Lcom/android/internal/view/menu/MenuItemImpl;

    #@8
    if-eqz v0, :cond_f

    #@a
    .line 141
    iget-object v0, p0, Lcom/android/internal/view/menu/ActionMenuItemView;->mItemData:Lcom/android/internal/view/menu/MenuItemImpl;

    #@c
    invoke-virtual {v0}, Lcom/android/internal/view/menu/MenuItemImpl;->actionFormatChanged()V

    #@f
    .line 144
    :cond_f
    return-void
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)V
    .registers 9
    .parameter "icon"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v5, 0x0

    #@2
    .line 155
    iput-object p1, p0, Lcom/android/internal/view/menu/ActionMenuItemView;->mIcon:Landroid/graphics/drawable/Drawable;

    #@4
    .line 157
    if-eqz p1, :cond_2f

    #@6
    .line 158
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@9
    move-result v2

    #@a
    .line 159
    .local v2, width:I
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@d
    move-result v0

    #@e
    .line 160
    .local v0, height:I
    iget v3, p0, Lcom/android/internal/view/menu/ActionMenuItemView;->mMaxIconSize:I

    #@10
    if-le v2, v3, :cond_1d

    #@12
    .line 161
    iget v3, p0, Lcom/android/internal/view/menu/ActionMenuItemView;->mMaxIconSize:I

    #@14
    int-to-float v3, v3

    #@15
    int-to-float v4, v2

    #@16
    div-float v1, v3, v4

    #@18
    .line 162
    .local v1, scale:F
    iget v2, p0, Lcom/android/internal/view/menu/ActionMenuItemView;->mMaxIconSize:I

    #@1a
    .line 163
    int-to-float v3, v0

    #@1b
    mul-float/2addr v3, v1

    #@1c
    float-to-int v0, v3

    #@1d
    .line 165
    .end local v1           #scale:F
    :cond_1d
    iget v3, p0, Lcom/android/internal/view/menu/ActionMenuItemView;->mMaxIconSize:I

    #@1f
    if-le v0, v3, :cond_2c

    #@21
    .line 166
    iget v3, p0, Lcom/android/internal/view/menu/ActionMenuItemView;->mMaxIconSize:I

    #@23
    int-to-float v3, v3

    #@24
    int-to-float v4, v0

    #@25
    div-float v1, v3, v4

    #@27
    .line 167
    .restart local v1       #scale:F
    iget v0, p0, Lcom/android/internal/view/menu/ActionMenuItemView;->mMaxIconSize:I

    #@29
    .line 168
    int-to-float v3, v2

    #@2a
    mul-float/2addr v3, v1

    #@2b
    float-to-int v2, v3

    #@2c
    .line 170
    .end local v1           #scale:F
    :cond_2c
    invoke-virtual {p1, v6, v6, v2, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@2f
    .line 173
    .end local v0           #height:I
    .end local v2           #width:I
    :cond_2f
    invoke-virtual {p0, p1, v5, v5, v5}, Lcom/android/internal/view/menu/ActionMenuItemView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    #@32
    .line 175
    invoke-direct {p0}, Lcom/android/internal/view/menu/ActionMenuItemView;->updateTextButtonVisibility()V

    #@35
    .line 176
    return-void
.end method

.method public setItemInvoker(Lcom/android/internal/view/menu/MenuBuilder$ItemInvoker;)V
    .registers 2
    .parameter "invoker"

    #@0
    .prologue
    .line 122
    iput-object p1, p0, Lcom/android/internal/view/menu/ActionMenuItemView;->mItemInvoker:Lcom/android/internal/view/menu/MenuBuilder$ItemInvoker;

    #@2
    .line 123
    return-void
.end method

.method public setPadding(IIII)V
    .registers 5
    .parameter "l"
    .parameter "t"
    .parameter "r"
    .parameter "b"

    #@0
    .prologue
    .line 96
    iput p1, p0, Lcom/android/internal/view/menu/ActionMenuItemView;->mSavedPaddingLeft:I

    #@2
    .line 97
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/TextView;->setPadding(IIII)V

    #@5
    .line 98
    return-void
.end method

.method public setShortcut(ZC)V
    .registers 3
    .parameter "showShortcut"
    .parameter "shortcutKey"

    #@0
    .prologue
    .line 184
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .registers 3
    .parameter "title"

    #@0
    .prologue
    .line 187
    iput-object p1, p0, Lcom/android/internal/view/menu/ActionMenuItemView;->mTitle:Ljava/lang/CharSequence;

    #@2
    .line 189
    iget-object v0, p0, Lcom/android/internal/view/menu/ActionMenuItemView;->mTitle:Ljava/lang/CharSequence;

    #@4
    invoke-virtual {p0, v0}, Lcom/android/internal/view/menu/ActionMenuItemView;->setContentDescription(Ljava/lang/CharSequence;)V

    #@7
    .line 190
    invoke-direct {p0}, Lcom/android/internal/view/menu/ActionMenuItemView;->updateTextButtonVisibility()V

    #@a
    .line 191
    return-void
.end method

.method public showsIcon()Z
    .registers 2

    #@0
    .prologue
    .line 215
    const/4 v0, 0x1

    #@1
    return v0
.end method
