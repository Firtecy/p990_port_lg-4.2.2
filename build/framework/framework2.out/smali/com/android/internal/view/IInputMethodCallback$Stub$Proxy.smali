.class Lcom/android/internal/view/IInputMethodCallback$Stub$Proxy;
.super Ljava/lang/Object;
.source "IInputMethodCallback.java"

# interfaces
.implements Lcom/android/internal/view/IInputMethodCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/view/IInputMethodCallback$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 75
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 76
    iput-object p1, p0, Lcom/android/internal/view/IInputMethodCallback$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 77
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 80
    iget-object v0, p0, Lcom/android/internal/view/IInputMethodCallback$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public finishedEvent(IZ)V
    .registers 8
    .parameter "seq"
    .parameter "handled"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 88
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 90
    .local v0, _data:Landroid/os/Parcel;
    :try_start_5
    const-string v2, "com.android.internal.view.IInputMethodCallback"

    #@7
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@a
    .line 91
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@d
    .line 92
    if-eqz p2, :cond_1e

    #@f
    :goto_f
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@12
    .line 93
    iget-object v1, p0, Lcom/android/internal/view/IInputMethodCallback$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@14
    const/4 v2, 0x1

    #@15
    const/4 v3, 0x0

    #@16
    const/4 v4, 0x1

    #@17
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_1a
    .catchall {:try_start_5 .. :try_end_1a} :catchall_20

    #@1a
    .line 96
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1d
    .line 98
    return-void

    #@1e
    .line 92
    :cond_1e
    const/4 v1, 0x0

    #@1f
    goto :goto_f

    #@20
    .line 96
    :catchall_20
    move-exception v1

    #@21
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@24
    throw v1
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 84
    const-string v0, "com.android.internal.view.IInputMethodCallback"

    #@2
    return-object v0
.end method

.method public sessionCreated(Lcom/android/internal/view/IInputMethodSession;)V
    .registers 7
    .parameter "session"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 101
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 103
    .local v0, _data:Landroid/os/Parcel;
    :try_start_5
    const-string v2, "com.android.internal.view.IInputMethodCallback"

    #@7
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@a
    .line 104
    if-eqz p1, :cond_10

    #@c
    invoke-interface {p1}, Lcom/android/internal/view/IInputMethodSession;->asBinder()Landroid/os/IBinder;

    #@f
    move-result-object v1

    #@10
    :cond_10
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@13
    .line 105
    iget-object v1, p0, Lcom/android/internal/view/IInputMethodCallback$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/4 v2, 0x2

    #@16
    const/4 v3, 0x0

    #@17
    const/4 v4, 0x1

    #@18
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_1b
    .catchall {:try_start_5 .. :try_end_1b} :catchall_1f

    #@1b
    .line 108
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 110
    return-void

    #@1f
    .line 108
    :catchall_1f
    move-exception v1

    #@20
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@23
    throw v1
.end method
