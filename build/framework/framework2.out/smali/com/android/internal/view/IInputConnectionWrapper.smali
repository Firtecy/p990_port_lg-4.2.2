.class public Lcom/android/internal/view/IInputConnectionWrapper;
.super Lcom/android/internal/view/IInputContext$Stub;
.source "IInputConnectionWrapper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/view/IInputConnectionWrapper$MyHandler;,
        Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;
    }
.end annotation


# static fields
.field private static final DO_BEGIN_BATCH_EDIT:I = 0x5a

.field private static final DO_CLEAR_META_KEY_STATES:I = 0x82

.field private static final DO_COMMIT_COMPLETION:I = 0x37

.field private static final DO_COMMIT_CORRECTION:I = 0x38

.field private static final DO_COMMIT_TEXT:I = 0x32

.field private static final DO_DELETE_SURROUNDING_TEXT:I = 0x50

.field private static final DO_END_BATCH_EDIT:I = 0x5f

.field private static final DO_FINISH_COMPOSING_TEXT:I = 0x41

.field private static final DO_GET_CURSOR_CAPS_MODE:I = 0x1e

.field private static final DO_GET_EXTRACTED_TEXT:I = 0x28

.field private static final DO_GET_SELECTED_TEXT:I = 0x19

.field private static final DO_GET_TEXT_AFTER_CURSOR:I = 0xa

.field private static final DO_GET_TEXT_BEFORE_CURSOR:I = 0x14

.field private static final DO_PERFORM_CONTEXT_MENU_ACTION:I = 0x3b

.field private static final DO_PERFORM_EDITOR_ACTION:I = 0x3a

.field private static final DO_PERFORM_PRIVATE_COMMAND:I = 0x78

.field private static final DO_REPORT_FULLSCREEN_MODE:I = 0x64

.field private static final DO_SEND_KEY_EVENT:I = 0x46

.field private static final DO_SET_COMPOSING_REGION:I = 0x3f

.field private static final DO_SET_COMPOSING_TEXT:I = 0x3c

.field private static final DO_SET_SELECTION:I = 0x39

.field static final TAG:Ljava/lang/String; = "IInputConnectionWrapper"


# instance fields
.field private mH:Landroid/os/Handler;

.field private mInputConnection:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/inputmethod/InputConnection;",
            ">;"
        }
    .end annotation
.end field

.field private mMainLooper:Landroid/os/Looper;


# direct methods
.method public constructor <init>(Landroid/os/Looper;Landroid/view/inputmethod/InputConnection;)V
    .registers 5
    .parameter "mainLooper"
    .parameter "conn"

    #@0
    .prologue
    .line 81
    invoke-direct {p0}, Lcom/android/internal/view/IInputContext$Stub;-><init>()V

    #@3
    .line 82
    new-instance v0, Ljava/lang/ref/WeakReference;

    #@5
    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@8
    iput-object v0, p0, Lcom/android/internal/view/IInputConnectionWrapper;->mInputConnection:Ljava/lang/ref/WeakReference;

    #@a
    .line 83
    iput-object p1, p0, Lcom/android/internal/view/IInputConnectionWrapper;->mMainLooper:Landroid/os/Looper;

    #@c
    .line 84
    new-instance v0, Lcom/android/internal/view/IInputConnectionWrapper$MyHandler;

    #@e
    iget-object v1, p0, Lcom/android/internal/view/IInputConnectionWrapper;->mMainLooper:Landroid/os/Looper;

    #@10
    invoke-direct {v0, p0, v1}, Lcom/android/internal/view/IInputConnectionWrapper$MyHandler;-><init>(Lcom/android/internal/view/IInputConnectionWrapper;Landroid/os/Looper;)V

    #@13
    iput-object v0, p0, Lcom/android/internal/view/IInputConnectionWrapper;->mH:Landroid/os/Handler;

    #@15
    .line 85
    return-void
.end method


# virtual methods
.method public beginBatchEdit()V
    .registers 2

    #@0
    .prologue
    .line 163
    const/16 v0, 0x5a

    #@2
    invoke-virtual {p0, v0}, Lcom/android/internal/view/IInputConnectionWrapper;->obtainMessage(I)Landroid/os/Message;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {p0, v0}, Lcom/android/internal/view/IInputConnectionWrapper;->dispatchMessage(Landroid/os/Message;)V

    #@9
    .line 164
    return-void
.end method

.method public clearMetaKeyStates(I)V
    .registers 4
    .parameter "states"

    #@0
    .prologue
    .line 154
    const/16 v0, 0x82

    #@2
    const/4 v1, 0x0

    #@3
    invoke-virtual {p0, v0, p1, v1}, Lcom/android/internal/view/IInputConnectionWrapper;->obtainMessageII(III)Landroid/os/Message;

    #@6
    move-result-object v0

    #@7
    invoke-virtual {p0, v0}, Lcom/android/internal/view/IInputConnectionWrapper;->dispatchMessage(Landroid/os/Message;)V

    #@a
    .line 155
    return-void
.end method

.method public commitCompletion(Landroid/view/inputmethod/CompletionInfo;)V
    .registers 3
    .parameter "text"

    #@0
    .prologue
    .line 118
    const/16 v0, 0x37

    #@2
    invoke-virtual {p0, v0, p1}, Lcom/android/internal/view/IInputConnectionWrapper;->obtainMessageO(ILjava/lang/Object;)Landroid/os/Message;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {p0, v0}, Lcom/android/internal/view/IInputConnectionWrapper;->dispatchMessage(Landroid/os/Message;)V

    #@9
    .line 119
    return-void
.end method

.method public commitCorrection(Landroid/view/inputmethod/CorrectionInfo;)V
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 122
    const/16 v0, 0x38

    #@2
    invoke-virtual {p0, v0, p1}, Lcom/android/internal/view/IInputConnectionWrapper;->obtainMessageO(ILjava/lang/Object;)Landroid/os/Message;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {p0, v0}, Lcom/android/internal/view/IInputConnectionWrapper;->dispatchMessage(Landroid/os/Message;)V

    #@9
    .line 123
    return-void
.end method

.method public commitText(Ljava/lang/CharSequence;I)V
    .registers 4
    .parameter "text"
    .parameter "newCursorPosition"

    #@0
    .prologue
    .line 114
    const/16 v0, 0x32

    #@2
    invoke-virtual {p0, v0, p2, p1}, Lcom/android/internal/view/IInputConnectionWrapper;->obtainMessageIO(IILjava/lang/Object;)Landroid/os/Message;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {p0, v0}, Lcom/android/internal/view/IInputConnectionWrapper;->dispatchMessage(Landroid/os/Message;)V

    #@9
    .line 115
    return-void
.end method

.method public deleteSurroundingText(II)V
    .registers 4
    .parameter "leftLength"
    .parameter "rightLength"

    #@0
    .prologue
    .line 158
    const/16 v0, 0x50

    #@2
    invoke-virtual {p0, v0, p1, p2}, Lcom/android/internal/view/IInputConnectionWrapper;->obtainMessageII(III)Landroid/os/Message;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {p0, v0}, Lcom/android/internal/view/IInputConnectionWrapper;->dispatchMessage(Landroid/os/Message;)V

    #@9
    .line 160
    return-void
.end method

.method dispatchMessage(Landroid/os/Message;)V
    .registers 4
    .parameter "msg"

    #@0
    .prologue
    .line 182
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@3
    move-result-object v0

    #@4
    iget-object v1, p0, Lcom/android/internal/view/IInputConnectionWrapper;->mMainLooper:Landroid/os/Looper;

    #@6
    if-ne v0, v1, :cond_f

    #@8
    .line 183
    invoke-virtual {p0, p1}, Lcom/android/internal/view/IInputConnectionWrapper;->executeMessage(Landroid/os/Message;)V

    #@b
    .line 184
    invoke-virtual {p1}, Landroid/os/Message;->recycle()V

    #@e
    .line 189
    :goto_e
    return-void

    #@f
    .line 188
    :cond_f
    iget-object v0, p0, Lcom/android/internal/view/IInputConnectionWrapper;->mH:Landroid/os/Handler;

    #@11
    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@14
    goto :goto_e
.end method

.method public endBatchEdit()V
    .registers 2

    #@0
    .prologue
    .line 167
    const/16 v0, 0x5f

    #@2
    invoke-virtual {p0, v0}, Lcom/android/internal/view/IInputConnectionWrapper;->obtainMessage(I)Landroid/os/Message;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {p0, v0}, Lcom/android/internal/view/IInputConnectionWrapper;->dispatchMessage(Landroid/os/Message;)V

    #@9
    .line 168
    return-void
.end method

.method executeMessage(Landroid/os/Message;)V
    .registers 8
    .parameter "msg"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    .line 192
    iget v5, p1, Landroid/os/Message;->what:I

    #@4
    sparse-switch v5, :sswitch_data_384

    #@7
    .line 430
    const-string v3, "IInputConnectionWrapper"

    #@9
    new-instance v4, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v5, "Unhandled message code: "

    #@10
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v4

    #@14
    iget v5, p1, Landroid/os/Message;->what:I

    #@16
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19
    move-result-object v4

    #@1a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v4

    #@1e
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    .line 431
    :goto_21
    return-void

    #@22
    .line 194
    :sswitch_22
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@24
    check-cast v0, Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;

    #@26
    .line 196
    .local v0, args:Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;
    :try_start_26
    iget-object v3, p0, Lcom/android/internal/view/IInputConnectionWrapper;->mInputConnection:Ljava/lang/ref/WeakReference;

    #@28
    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@2b
    move-result-object v2

    #@2c
    check-cast v2, Landroid/view/inputmethod/InputConnection;

    #@2e
    .line 197
    .local v2, ic:Landroid/view/inputmethod/InputConnection;
    if-eqz v2, :cond_36

    #@30
    invoke-virtual {p0}, Lcom/android/internal/view/IInputConnectionWrapper;->isActive()Z

    #@33
    move-result v3

    #@34
    if-nez v3, :cond_4f

    #@36
    .line 198
    :cond_36
    const-string v3, "IInputConnectionWrapper"

    #@38
    const-string v4, "getTextAfterCursor on inactive InputConnection"

    #@3a
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3d
    .line 199
    iget-object v3, v0, Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;->callback:Lcom/android/internal/view/IInputContextCallback;

    #@3f
    const/4 v4, 0x0

    #@40
    iget v5, v0, Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;->seq:I

    #@42
    invoke-interface {v3, v4, v5}, Lcom/android/internal/view/IInputContextCallback;->setTextAfterCursor(Ljava/lang/CharSequence;I)V
    :try_end_45
    .catch Landroid/os/RemoteException; {:try_start_26 .. :try_end_45} :catch_46

    #@45
    goto :goto_21

    #@46
    .line 204
    .end local v2           #ic:Landroid/view/inputmethod/InputConnection;
    :catch_46
    move-exception v1

    #@47
    .line 205
    .local v1, e:Landroid/os/RemoteException;
    const-string v3, "IInputConnectionWrapper"

    #@49
    const-string v4, "Got RemoteException calling setTextAfterCursor"

    #@4b
    invoke-static {v3, v4, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@4e
    goto :goto_21

    #@4f
    .line 202
    .end local v1           #e:Landroid/os/RemoteException;
    .restart local v2       #ic:Landroid/view/inputmethod/InputConnection;
    :cond_4f
    :try_start_4f
    iget-object v3, v0, Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;->callback:Lcom/android/internal/view/IInputContextCallback;

    #@51
    iget v4, p1, Landroid/os/Message;->arg1:I

    #@53
    iget v5, p1, Landroid/os/Message;->arg2:I

    #@55
    invoke-interface {v2, v4, v5}, Landroid/view/inputmethod/InputConnection;->getTextAfterCursor(II)Ljava/lang/CharSequence;

    #@58
    move-result-object v4

    #@59
    iget v5, v0, Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;->seq:I

    #@5b
    invoke-interface {v3, v4, v5}, Lcom/android/internal/view/IInputContextCallback;->setTextAfterCursor(Ljava/lang/CharSequence;I)V
    :try_end_5e
    .catch Landroid/os/RemoteException; {:try_start_4f .. :try_end_5e} :catch_46

    #@5e
    goto :goto_21

    #@5f
    .line 210
    .end local v0           #args:Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;
    .end local v2           #ic:Landroid/view/inputmethod/InputConnection;
    :sswitch_5f
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@61
    check-cast v0, Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;

    #@63
    .line 212
    .restart local v0       #args:Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;
    :try_start_63
    iget-object v3, p0, Lcom/android/internal/view/IInputConnectionWrapper;->mInputConnection:Ljava/lang/ref/WeakReference;

    #@65
    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@68
    move-result-object v2

    #@69
    check-cast v2, Landroid/view/inputmethod/InputConnection;

    #@6b
    .line 213
    .restart local v2       #ic:Landroid/view/inputmethod/InputConnection;
    if-eqz v2, :cond_73

    #@6d
    invoke-virtual {p0}, Lcom/android/internal/view/IInputConnectionWrapper;->isActive()Z

    #@70
    move-result v3

    #@71
    if-nez v3, :cond_8c

    #@73
    .line 214
    :cond_73
    const-string v3, "IInputConnectionWrapper"

    #@75
    const-string v4, "getTextBeforeCursor on inactive InputConnection"

    #@77
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7a
    .line 215
    iget-object v3, v0, Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;->callback:Lcom/android/internal/view/IInputContextCallback;

    #@7c
    const/4 v4, 0x0

    #@7d
    iget v5, v0, Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;->seq:I

    #@7f
    invoke-interface {v3, v4, v5}, Lcom/android/internal/view/IInputContextCallback;->setTextBeforeCursor(Ljava/lang/CharSequence;I)V
    :try_end_82
    .catch Landroid/os/RemoteException; {:try_start_63 .. :try_end_82} :catch_83

    #@82
    goto :goto_21

    #@83
    .line 220
    .end local v2           #ic:Landroid/view/inputmethod/InputConnection;
    :catch_83
    move-exception v1

    #@84
    .line 221
    .restart local v1       #e:Landroid/os/RemoteException;
    const-string v3, "IInputConnectionWrapper"

    #@86
    const-string v4, "Got RemoteException calling setTextBeforeCursor"

    #@88
    invoke-static {v3, v4, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@8b
    goto :goto_21

    #@8c
    .line 218
    .end local v1           #e:Landroid/os/RemoteException;
    .restart local v2       #ic:Landroid/view/inputmethod/InputConnection;
    :cond_8c
    :try_start_8c
    iget-object v3, v0, Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;->callback:Lcom/android/internal/view/IInputContextCallback;

    #@8e
    iget v4, p1, Landroid/os/Message;->arg1:I

    #@90
    iget v5, p1, Landroid/os/Message;->arg2:I

    #@92
    invoke-interface {v2, v4, v5}, Landroid/view/inputmethod/InputConnection;->getTextBeforeCursor(II)Ljava/lang/CharSequence;

    #@95
    move-result-object v4

    #@96
    iget v5, v0, Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;->seq:I

    #@98
    invoke-interface {v3, v4, v5}, Lcom/android/internal/view/IInputContextCallback;->setTextBeforeCursor(Ljava/lang/CharSequence;I)V
    :try_end_9b
    .catch Landroid/os/RemoteException; {:try_start_8c .. :try_end_9b} :catch_83

    #@9b
    goto :goto_21

    #@9c
    .line 226
    .end local v0           #args:Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;
    .end local v2           #ic:Landroid/view/inputmethod/InputConnection;
    :sswitch_9c
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@9e
    check-cast v0, Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;

    #@a0
    .line 228
    .restart local v0       #args:Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;
    :try_start_a0
    iget-object v3, p0, Lcom/android/internal/view/IInputConnectionWrapper;->mInputConnection:Ljava/lang/ref/WeakReference;

    #@a2
    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@a5
    move-result-object v2

    #@a6
    check-cast v2, Landroid/view/inputmethod/InputConnection;

    #@a8
    .line 229
    .restart local v2       #ic:Landroid/view/inputmethod/InputConnection;
    if-eqz v2, :cond_b0

    #@aa
    invoke-virtual {p0}, Lcom/android/internal/view/IInputConnectionWrapper;->isActive()Z

    #@ad
    move-result v3

    #@ae
    if-nez v3, :cond_cb

    #@b0
    .line 230
    :cond_b0
    const-string v3, "IInputConnectionWrapper"

    #@b2
    const-string v4, "getSelectedText on inactive InputConnection"

    #@b4
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@b7
    .line 231
    iget-object v3, v0, Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;->callback:Lcom/android/internal/view/IInputContextCallback;

    #@b9
    const/4 v4, 0x0

    #@ba
    iget v5, v0, Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;->seq:I

    #@bc
    invoke-interface {v3, v4, v5}, Lcom/android/internal/view/IInputContextCallback;->setSelectedText(Ljava/lang/CharSequence;I)V
    :try_end_bf
    .catch Landroid/os/RemoteException; {:try_start_a0 .. :try_end_bf} :catch_c1

    #@bf
    goto/16 :goto_21

    #@c1
    .line 236
    .end local v2           #ic:Landroid/view/inputmethod/InputConnection;
    :catch_c1
    move-exception v1

    #@c2
    .line 237
    .restart local v1       #e:Landroid/os/RemoteException;
    const-string v3, "IInputConnectionWrapper"

    #@c4
    const-string v4, "Got RemoteException calling setSelectedText"

    #@c6
    invoke-static {v3, v4, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@c9
    goto/16 :goto_21

    #@cb
    .line 234
    .end local v1           #e:Landroid/os/RemoteException;
    .restart local v2       #ic:Landroid/view/inputmethod/InputConnection;
    :cond_cb
    :try_start_cb
    iget-object v3, v0, Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;->callback:Lcom/android/internal/view/IInputContextCallback;

    #@cd
    iget v4, p1, Landroid/os/Message;->arg1:I

    #@cf
    invoke-interface {v2, v4}, Landroid/view/inputmethod/InputConnection;->getSelectedText(I)Ljava/lang/CharSequence;

    #@d2
    move-result-object v4

    #@d3
    iget v5, v0, Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;->seq:I

    #@d5
    invoke-interface {v3, v4, v5}, Lcom/android/internal/view/IInputContextCallback;->setSelectedText(Ljava/lang/CharSequence;I)V
    :try_end_d8
    .catch Landroid/os/RemoteException; {:try_start_cb .. :try_end_d8} :catch_c1

    #@d8
    goto/16 :goto_21

    #@da
    .line 242
    .end local v0           #args:Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;
    .end local v2           #ic:Landroid/view/inputmethod/InputConnection;
    :sswitch_da
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@dc
    check-cast v0, Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;

    #@de
    .line 244
    .restart local v0       #args:Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;
    :try_start_de
    iget-object v3, p0, Lcom/android/internal/view/IInputConnectionWrapper;->mInputConnection:Ljava/lang/ref/WeakReference;

    #@e0
    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@e3
    move-result-object v2

    #@e4
    check-cast v2, Landroid/view/inputmethod/InputConnection;

    #@e6
    .line 245
    .restart local v2       #ic:Landroid/view/inputmethod/InputConnection;
    if-eqz v2, :cond_ee

    #@e8
    invoke-virtual {p0}, Lcom/android/internal/view/IInputConnectionWrapper;->isActive()Z

    #@eb
    move-result v3

    #@ec
    if-nez v3, :cond_109

    #@ee
    .line 246
    :cond_ee
    const-string v3, "IInputConnectionWrapper"

    #@f0
    const-string v4, "getCursorCapsMode on inactive InputConnection"

    #@f2
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@f5
    .line 247
    iget-object v3, v0, Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;->callback:Lcom/android/internal/view/IInputContextCallback;

    #@f7
    const/4 v4, 0x0

    #@f8
    iget v5, v0, Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;->seq:I

    #@fa
    invoke-interface {v3, v4, v5}, Lcom/android/internal/view/IInputContextCallback;->setCursorCapsMode(II)V
    :try_end_fd
    .catch Landroid/os/RemoteException; {:try_start_de .. :try_end_fd} :catch_ff

    #@fd
    goto/16 :goto_21

    #@ff
    .line 252
    .end local v2           #ic:Landroid/view/inputmethod/InputConnection;
    :catch_ff
    move-exception v1

    #@100
    .line 253
    .restart local v1       #e:Landroid/os/RemoteException;
    const-string v3, "IInputConnectionWrapper"

    #@102
    const-string v4, "Got RemoteException calling setCursorCapsMode"

    #@104
    invoke-static {v3, v4, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@107
    goto/16 :goto_21

    #@109
    .line 250
    .end local v1           #e:Landroid/os/RemoteException;
    .restart local v2       #ic:Landroid/view/inputmethod/InputConnection;
    :cond_109
    :try_start_109
    iget-object v3, v0, Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;->callback:Lcom/android/internal/view/IInputContextCallback;

    #@10b
    iget v4, p1, Landroid/os/Message;->arg1:I

    #@10d
    invoke-interface {v2, v4}, Landroid/view/inputmethod/InputConnection;->getCursorCapsMode(I)I

    #@110
    move-result v4

    #@111
    iget v5, v0, Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;->seq:I

    #@113
    invoke-interface {v3, v4, v5}, Lcom/android/internal/view/IInputContextCallback;->setCursorCapsMode(II)V
    :try_end_116
    .catch Landroid/os/RemoteException; {:try_start_109 .. :try_end_116} :catch_ff

    #@116
    goto/16 :goto_21

    #@118
    .line 258
    .end local v0           #args:Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;
    .end local v2           #ic:Landroid/view/inputmethod/InputConnection;
    :sswitch_118
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@11a
    check-cast v0, Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;

    #@11c
    .line 260
    .restart local v0       #args:Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;
    :try_start_11c
    iget-object v3, p0, Lcom/android/internal/view/IInputConnectionWrapper;->mInputConnection:Ljava/lang/ref/WeakReference;

    #@11e
    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@121
    move-result-object v2

    #@122
    check-cast v2, Landroid/view/inputmethod/InputConnection;

    #@124
    .line 261
    .restart local v2       #ic:Landroid/view/inputmethod/InputConnection;
    if-eqz v2, :cond_12c

    #@126
    invoke-virtual {p0}, Lcom/android/internal/view/IInputConnectionWrapper;->isActive()Z

    #@129
    move-result v3

    #@12a
    if-nez v3, :cond_147

    #@12c
    .line 262
    :cond_12c
    const-string v3, "IInputConnectionWrapper"

    #@12e
    const-string v4, "getExtractedText on inactive InputConnection"

    #@130
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@133
    .line 263
    iget-object v3, v0, Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;->callback:Lcom/android/internal/view/IInputContextCallback;

    #@135
    const/4 v4, 0x0

    #@136
    iget v5, v0, Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;->seq:I

    #@138
    invoke-interface {v3, v4, v5}, Lcom/android/internal/view/IInputContextCallback;->setExtractedText(Landroid/view/inputmethod/ExtractedText;I)V
    :try_end_13b
    .catch Landroid/os/RemoteException; {:try_start_11c .. :try_end_13b} :catch_13d

    #@13b
    goto/16 :goto_21

    #@13d
    .line 268
    .end local v2           #ic:Landroid/view/inputmethod/InputConnection;
    :catch_13d
    move-exception v1

    #@13e
    .line 269
    .restart local v1       #e:Landroid/os/RemoteException;
    const-string v3, "IInputConnectionWrapper"

    #@140
    const-string v4, "Got RemoteException calling setExtractedText"

    #@142
    invoke-static {v3, v4, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@145
    goto/16 :goto_21

    #@147
    .line 266
    .end local v1           #e:Landroid/os/RemoteException;
    .restart local v2       #ic:Landroid/view/inputmethod/InputConnection;
    :cond_147
    :try_start_147
    iget-object v4, v0, Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;->callback:Lcom/android/internal/view/IInputContextCallback;

    #@149
    iget-object v3, v0, Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;->arg1:Ljava/lang/Object;

    #@14b
    check-cast v3, Landroid/view/inputmethod/ExtractedTextRequest;

    #@14d
    iget v5, p1, Landroid/os/Message;->arg1:I

    #@14f
    invoke-interface {v2, v3, v5}, Landroid/view/inputmethod/InputConnection;->getExtractedText(Landroid/view/inputmethod/ExtractedTextRequest;I)Landroid/view/inputmethod/ExtractedText;

    #@152
    move-result-object v3

    #@153
    iget v5, v0, Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;->seq:I

    #@155
    invoke-interface {v4, v3, v5}, Lcom/android/internal/view/IInputContextCallback;->setExtractedText(Landroid/view/inputmethod/ExtractedText;I)V
    :try_end_158
    .catch Landroid/os/RemoteException; {:try_start_147 .. :try_end_158} :catch_13d

    #@158
    goto/16 :goto_21

    #@15a
    .line 274
    .end local v0           #args:Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;
    .end local v2           #ic:Landroid/view/inputmethod/InputConnection;
    :sswitch_15a
    iget-object v3, p0, Lcom/android/internal/view/IInputConnectionWrapper;->mInputConnection:Ljava/lang/ref/WeakReference;

    #@15c
    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@15f
    move-result-object v2

    #@160
    check-cast v2, Landroid/view/inputmethod/InputConnection;

    #@162
    .line 275
    .restart local v2       #ic:Landroid/view/inputmethod/InputConnection;
    if-eqz v2, :cond_16a

    #@164
    invoke-virtual {p0}, Lcom/android/internal/view/IInputConnectionWrapper;->isActive()Z

    #@167
    move-result v3

    #@168
    if-nez v3, :cond_173

    #@16a
    .line 276
    :cond_16a
    const-string v3, "IInputConnectionWrapper"

    #@16c
    const-string v4, "commitText on inactive InputConnection"

    #@16e
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@171
    goto/16 :goto_21

    #@173
    .line 279
    :cond_173
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@175
    check-cast v3, Ljava/lang/CharSequence;

    #@177
    iget v4, p1, Landroid/os/Message;->arg1:I

    #@179
    invoke-interface {v2, v3, v4}, Landroid/view/inputmethod/InputConnection;->commitText(Ljava/lang/CharSequence;I)Z

    #@17c
    goto/16 :goto_21

    #@17e
    .line 283
    .end local v2           #ic:Landroid/view/inputmethod/InputConnection;
    :sswitch_17e
    iget-object v3, p0, Lcom/android/internal/view/IInputConnectionWrapper;->mInputConnection:Ljava/lang/ref/WeakReference;

    #@180
    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@183
    move-result-object v2

    #@184
    check-cast v2, Landroid/view/inputmethod/InputConnection;

    #@186
    .line 284
    .restart local v2       #ic:Landroid/view/inputmethod/InputConnection;
    if-eqz v2, :cond_18e

    #@188
    invoke-virtual {p0}, Lcom/android/internal/view/IInputConnectionWrapper;->isActive()Z

    #@18b
    move-result v3

    #@18c
    if-nez v3, :cond_197

    #@18e
    .line 285
    :cond_18e
    const-string v3, "IInputConnectionWrapper"

    #@190
    const-string v4, "setSelection on inactive InputConnection"

    #@192
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@195
    goto/16 :goto_21

    #@197
    .line 288
    :cond_197
    iget v3, p1, Landroid/os/Message;->arg1:I

    #@199
    iget v4, p1, Landroid/os/Message;->arg2:I

    #@19b
    invoke-interface {v2, v3, v4}, Landroid/view/inputmethod/InputConnection;->setSelection(II)Z

    #@19e
    goto/16 :goto_21

    #@1a0
    .line 292
    .end local v2           #ic:Landroid/view/inputmethod/InputConnection;
    :sswitch_1a0
    iget-object v3, p0, Lcom/android/internal/view/IInputConnectionWrapper;->mInputConnection:Ljava/lang/ref/WeakReference;

    #@1a2
    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@1a5
    move-result-object v2

    #@1a6
    check-cast v2, Landroid/view/inputmethod/InputConnection;

    #@1a8
    .line 293
    .restart local v2       #ic:Landroid/view/inputmethod/InputConnection;
    if-eqz v2, :cond_1b0

    #@1aa
    invoke-virtual {p0}, Lcom/android/internal/view/IInputConnectionWrapper;->isActive()Z

    #@1ad
    move-result v3

    #@1ae
    if-nez v3, :cond_1b9

    #@1b0
    .line 294
    :cond_1b0
    const-string v3, "IInputConnectionWrapper"

    #@1b2
    const-string v4, "performEditorAction on inactive InputConnection"

    #@1b4
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1b7
    goto/16 :goto_21

    #@1b9
    .line 297
    :cond_1b9
    iget v3, p1, Landroid/os/Message;->arg1:I

    #@1bb
    invoke-interface {v2, v3}, Landroid/view/inputmethod/InputConnection;->performEditorAction(I)Z

    #@1be
    goto/16 :goto_21

    #@1c0
    .line 301
    .end local v2           #ic:Landroid/view/inputmethod/InputConnection;
    :sswitch_1c0
    iget-object v3, p0, Lcom/android/internal/view/IInputConnectionWrapper;->mInputConnection:Ljava/lang/ref/WeakReference;

    #@1c2
    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@1c5
    move-result-object v2

    #@1c6
    check-cast v2, Landroid/view/inputmethod/InputConnection;

    #@1c8
    .line 302
    .restart local v2       #ic:Landroid/view/inputmethod/InputConnection;
    if-eqz v2, :cond_1d0

    #@1ca
    invoke-virtual {p0}, Lcom/android/internal/view/IInputConnectionWrapper;->isActive()Z

    #@1cd
    move-result v3

    #@1ce
    if-nez v3, :cond_1d9

    #@1d0
    .line 303
    :cond_1d0
    const-string v3, "IInputConnectionWrapper"

    #@1d2
    const-string v4, "performContextMenuAction on inactive InputConnection"

    #@1d4
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1d7
    goto/16 :goto_21

    #@1d9
    .line 306
    :cond_1d9
    iget v3, p1, Landroid/os/Message;->arg1:I

    #@1db
    invoke-interface {v2, v3}, Landroid/view/inputmethod/InputConnection;->performContextMenuAction(I)Z

    #@1de
    goto/16 :goto_21

    #@1e0
    .line 310
    .end local v2           #ic:Landroid/view/inputmethod/InputConnection;
    :sswitch_1e0
    iget-object v3, p0, Lcom/android/internal/view/IInputConnectionWrapper;->mInputConnection:Ljava/lang/ref/WeakReference;

    #@1e2
    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@1e5
    move-result-object v2

    #@1e6
    check-cast v2, Landroid/view/inputmethod/InputConnection;

    #@1e8
    .line 311
    .restart local v2       #ic:Landroid/view/inputmethod/InputConnection;
    if-eqz v2, :cond_1f0

    #@1ea
    invoke-virtual {p0}, Lcom/android/internal/view/IInputConnectionWrapper;->isActive()Z

    #@1ed
    move-result v3

    #@1ee
    if-nez v3, :cond_1f9

    #@1f0
    .line 312
    :cond_1f0
    const-string v3, "IInputConnectionWrapper"

    #@1f2
    const-string v4, "commitCompletion on inactive InputConnection"

    #@1f4
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1f7
    goto/16 :goto_21

    #@1f9
    .line 315
    :cond_1f9
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1fb
    check-cast v3, Landroid/view/inputmethod/CompletionInfo;

    #@1fd
    invoke-interface {v2, v3}, Landroid/view/inputmethod/InputConnection;->commitCompletion(Landroid/view/inputmethod/CompletionInfo;)Z

    #@200
    goto/16 :goto_21

    #@202
    .line 319
    .end local v2           #ic:Landroid/view/inputmethod/InputConnection;
    :sswitch_202
    iget-object v3, p0, Lcom/android/internal/view/IInputConnectionWrapper;->mInputConnection:Ljava/lang/ref/WeakReference;

    #@204
    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@207
    move-result-object v2

    #@208
    check-cast v2, Landroid/view/inputmethod/InputConnection;

    #@20a
    .line 320
    .restart local v2       #ic:Landroid/view/inputmethod/InputConnection;
    if-eqz v2, :cond_212

    #@20c
    invoke-virtual {p0}, Lcom/android/internal/view/IInputConnectionWrapper;->isActive()Z

    #@20f
    move-result v3

    #@210
    if-nez v3, :cond_21b

    #@212
    .line 321
    :cond_212
    const-string v3, "IInputConnectionWrapper"

    #@214
    const-string v4, "commitCorrection on inactive InputConnection"

    #@216
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@219
    goto/16 :goto_21

    #@21b
    .line 324
    :cond_21b
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@21d
    check-cast v3, Landroid/view/inputmethod/CorrectionInfo;

    #@21f
    invoke-interface {v2, v3}, Landroid/view/inputmethod/InputConnection;->commitCorrection(Landroid/view/inputmethod/CorrectionInfo;)Z

    #@222
    goto/16 :goto_21

    #@224
    .line 328
    .end local v2           #ic:Landroid/view/inputmethod/InputConnection;
    :sswitch_224
    iget-object v3, p0, Lcom/android/internal/view/IInputConnectionWrapper;->mInputConnection:Ljava/lang/ref/WeakReference;

    #@226
    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@229
    move-result-object v2

    #@22a
    check-cast v2, Landroid/view/inputmethod/InputConnection;

    #@22c
    .line 329
    .restart local v2       #ic:Landroid/view/inputmethod/InputConnection;
    if-eqz v2, :cond_234

    #@22e
    invoke-virtual {p0}, Lcom/android/internal/view/IInputConnectionWrapper;->isActive()Z

    #@231
    move-result v3

    #@232
    if-nez v3, :cond_23d

    #@234
    .line 330
    :cond_234
    const-string v3, "IInputConnectionWrapper"

    #@236
    const-string v4, "setComposingText on inactive InputConnection"

    #@238
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@23b
    goto/16 :goto_21

    #@23d
    .line 333
    :cond_23d
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@23f
    check-cast v3, Ljava/lang/CharSequence;

    #@241
    iget v4, p1, Landroid/os/Message;->arg1:I

    #@243
    invoke-interface {v2, v3, v4}, Landroid/view/inputmethod/InputConnection;->setComposingText(Ljava/lang/CharSequence;I)Z

    #@246
    goto/16 :goto_21

    #@248
    .line 337
    .end local v2           #ic:Landroid/view/inputmethod/InputConnection;
    :sswitch_248
    iget-object v3, p0, Lcom/android/internal/view/IInputConnectionWrapper;->mInputConnection:Ljava/lang/ref/WeakReference;

    #@24a
    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@24d
    move-result-object v2

    #@24e
    check-cast v2, Landroid/view/inputmethod/InputConnection;

    #@250
    .line 338
    .restart local v2       #ic:Landroid/view/inputmethod/InputConnection;
    if-eqz v2, :cond_258

    #@252
    invoke-virtual {p0}, Lcom/android/internal/view/IInputConnectionWrapper;->isActive()Z

    #@255
    move-result v3

    #@256
    if-nez v3, :cond_261

    #@258
    .line 339
    :cond_258
    const-string v3, "IInputConnectionWrapper"

    #@25a
    const-string v4, "setComposingRegion on inactive InputConnection"

    #@25c
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@25f
    goto/16 :goto_21

    #@261
    .line 342
    :cond_261
    iget v3, p1, Landroid/os/Message;->arg1:I

    #@263
    iget v4, p1, Landroid/os/Message;->arg2:I

    #@265
    invoke-interface {v2, v3, v4}, Landroid/view/inputmethod/InputConnection;->setComposingRegion(II)Z

    #@268
    goto/16 :goto_21

    #@26a
    .line 346
    .end local v2           #ic:Landroid/view/inputmethod/InputConnection;
    :sswitch_26a
    iget-object v3, p0, Lcom/android/internal/view/IInputConnectionWrapper;->mInputConnection:Ljava/lang/ref/WeakReference;

    #@26c
    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@26f
    move-result-object v2

    #@270
    check-cast v2, Landroid/view/inputmethod/InputConnection;

    #@272
    .line 351
    .restart local v2       #ic:Landroid/view/inputmethod/InputConnection;
    if-nez v2, :cond_27d

    #@274
    .line 352
    const-string v3, "IInputConnectionWrapper"

    #@276
    const-string v4, "finishComposingText on inactive InputConnection"

    #@278
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@27b
    goto/16 :goto_21

    #@27d
    .line 355
    :cond_27d
    invoke-interface {v2}, Landroid/view/inputmethod/InputConnection;->finishComposingText()Z

    #@280
    goto/16 :goto_21

    #@282
    .line 359
    .end local v2           #ic:Landroid/view/inputmethod/InputConnection;
    :sswitch_282
    iget-object v3, p0, Lcom/android/internal/view/IInputConnectionWrapper;->mInputConnection:Ljava/lang/ref/WeakReference;

    #@284
    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@287
    move-result-object v2

    #@288
    check-cast v2, Landroid/view/inputmethod/InputConnection;

    #@28a
    .line 360
    .restart local v2       #ic:Landroid/view/inputmethod/InputConnection;
    if-eqz v2, :cond_292

    #@28c
    invoke-virtual {p0}, Lcom/android/internal/view/IInputConnectionWrapper;->isActive()Z

    #@28f
    move-result v3

    #@290
    if-nez v3, :cond_29b

    #@292
    .line 361
    :cond_292
    const-string v3, "IInputConnectionWrapper"

    #@294
    const-string v4, "sendKeyEvent on inactive InputConnection"

    #@296
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@299
    goto/16 :goto_21

    #@29b
    .line 364
    :cond_29b
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@29d
    check-cast v3, Landroid/view/KeyEvent;

    #@29f
    invoke-interface {v2, v3}, Landroid/view/inputmethod/InputConnection;->sendKeyEvent(Landroid/view/KeyEvent;)Z

    #@2a2
    goto/16 :goto_21

    #@2a4
    .line 368
    .end local v2           #ic:Landroid/view/inputmethod/InputConnection;
    :sswitch_2a4
    iget-object v3, p0, Lcom/android/internal/view/IInputConnectionWrapper;->mInputConnection:Ljava/lang/ref/WeakReference;

    #@2a6
    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@2a9
    move-result-object v2

    #@2aa
    check-cast v2, Landroid/view/inputmethod/InputConnection;

    #@2ac
    .line 369
    .restart local v2       #ic:Landroid/view/inputmethod/InputConnection;
    if-eqz v2, :cond_2b4

    #@2ae
    invoke-virtual {p0}, Lcom/android/internal/view/IInputConnectionWrapper;->isActive()Z

    #@2b1
    move-result v3

    #@2b2
    if-nez v3, :cond_2bd

    #@2b4
    .line 370
    :cond_2b4
    const-string v3, "IInputConnectionWrapper"

    #@2b6
    const-string v4, "clearMetaKeyStates on inactive InputConnection"

    #@2b8
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2bb
    goto/16 :goto_21

    #@2bd
    .line 373
    :cond_2bd
    iget v3, p1, Landroid/os/Message;->arg1:I

    #@2bf
    invoke-interface {v2, v3}, Landroid/view/inputmethod/InputConnection;->clearMetaKeyStates(I)Z

    #@2c2
    goto/16 :goto_21

    #@2c4
    .line 377
    .end local v2           #ic:Landroid/view/inputmethod/InputConnection;
    :sswitch_2c4
    iget-object v3, p0, Lcom/android/internal/view/IInputConnectionWrapper;->mInputConnection:Ljava/lang/ref/WeakReference;

    #@2c6
    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@2c9
    move-result-object v2

    #@2ca
    check-cast v2, Landroid/view/inputmethod/InputConnection;

    #@2cc
    .line 378
    .restart local v2       #ic:Landroid/view/inputmethod/InputConnection;
    if-eqz v2, :cond_2d4

    #@2ce
    invoke-virtual {p0}, Lcom/android/internal/view/IInputConnectionWrapper;->isActive()Z

    #@2d1
    move-result v3

    #@2d2
    if-nez v3, :cond_2dd

    #@2d4
    .line 379
    :cond_2d4
    const-string v3, "IInputConnectionWrapper"

    #@2d6
    const-string v4, "deleteSurroundingText on inactive InputConnection"

    #@2d8
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2db
    goto/16 :goto_21

    #@2dd
    .line 382
    :cond_2dd
    iget v3, p1, Landroid/os/Message;->arg1:I

    #@2df
    iget v4, p1, Landroid/os/Message;->arg2:I

    #@2e1
    invoke-interface {v2, v3, v4}, Landroid/view/inputmethod/InputConnection;->deleteSurroundingText(II)Z

    #@2e4
    goto/16 :goto_21

    #@2e6
    .line 386
    .end local v2           #ic:Landroid/view/inputmethod/InputConnection;
    :sswitch_2e6
    iget-object v3, p0, Lcom/android/internal/view/IInputConnectionWrapper;->mInputConnection:Ljava/lang/ref/WeakReference;

    #@2e8
    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@2eb
    move-result-object v2

    #@2ec
    check-cast v2, Landroid/view/inputmethod/InputConnection;

    #@2ee
    .line 387
    .restart local v2       #ic:Landroid/view/inputmethod/InputConnection;
    if-eqz v2, :cond_2f6

    #@2f0
    invoke-virtual {p0}, Lcom/android/internal/view/IInputConnectionWrapper;->isActive()Z

    #@2f3
    move-result v3

    #@2f4
    if-nez v3, :cond_2ff

    #@2f6
    .line 388
    :cond_2f6
    const-string v3, "IInputConnectionWrapper"

    #@2f8
    const-string v4, "beginBatchEdit on inactive InputConnection"

    #@2fa
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2fd
    goto/16 :goto_21

    #@2ff
    .line 391
    :cond_2ff
    invoke-interface {v2}, Landroid/view/inputmethod/InputConnection;->beginBatchEdit()Z

    #@302
    goto/16 :goto_21

    #@304
    .line 395
    .end local v2           #ic:Landroid/view/inputmethod/InputConnection;
    :sswitch_304
    iget-object v3, p0, Lcom/android/internal/view/IInputConnectionWrapper;->mInputConnection:Ljava/lang/ref/WeakReference;

    #@306
    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@309
    move-result-object v2

    #@30a
    check-cast v2, Landroid/view/inputmethod/InputConnection;

    #@30c
    .line 396
    .restart local v2       #ic:Landroid/view/inputmethod/InputConnection;
    if-eqz v2, :cond_314

    #@30e
    invoke-virtual {p0}, Lcom/android/internal/view/IInputConnectionWrapper;->isActive()Z

    #@311
    move-result v3

    #@312
    if-nez v3, :cond_31d

    #@314
    .line 397
    :cond_314
    const-string v3, "IInputConnectionWrapper"

    #@316
    const-string v4, "endBatchEdit on inactive InputConnection"

    #@318
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@31b
    goto/16 :goto_21

    #@31d
    .line 400
    :cond_31d
    invoke-interface {v2}, Landroid/view/inputmethod/InputConnection;->endBatchEdit()Z

    #@320
    goto/16 :goto_21

    #@322
    .line 404
    .end local v2           #ic:Landroid/view/inputmethod/InputConnection;
    :sswitch_322
    iget-object v5, p0, Lcom/android/internal/view/IInputConnectionWrapper;->mInputConnection:Ljava/lang/ref/WeakReference;

    #@324
    invoke-virtual {v5}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@327
    move-result-object v2

    #@328
    check-cast v2, Landroid/view/inputmethod/InputConnection;

    #@32a
    .line 405
    .restart local v2       #ic:Landroid/view/inputmethod/InputConnection;
    if-eqz v2, :cond_332

    #@32c
    invoke-virtual {p0}, Lcom/android/internal/view/IInputConnectionWrapper;->isActive()Z

    #@32f
    move-result v5

    #@330
    if-nez v5, :cond_33b

    #@332
    .line 406
    :cond_332
    const-string v3, "IInputConnectionWrapper"

    #@334
    const-string v4, "showStatusIcon on inactive InputConnection"

    #@336
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@339
    goto/16 :goto_21

    #@33b
    .line 409
    :cond_33b
    iget v5, p1, Landroid/os/Message;->arg1:I

    #@33d
    if-ne v5, v3, :cond_344

    #@33f
    :goto_33f
    invoke-interface {v2, v3}, Landroid/view/inputmethod/InputConnection;->reportFullscreenMode(Z)Z

    #@342
    goto/16 :goto_21

    #@344
    :cond_344
    move v3, v4

    #@345
    goto :goto_33f

    #@346
    .line 413
    .end local v2           #ic:Landroid/view/inputmethod/InputConnection;
    :sswitch_346
    iget-object v4, p0, Lcom/android/internal/view/IInputConnectionWrapper;->mInputConnection:Ljava/lang/ref/WeakReference;

    #@348
    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@34b
    move-result-object v2

    #@34c
    check-cast v2, Landroid/view/inputmethod/InputConnection;

    #@34e
    .line 414
    .restart local v2       #ic:Landroid/view/inputmethod/InputConnection;
    if-eqz v2, :cond_356

    #@350
    invoke-virtual {p0}, Lcom/android/internal/view/IInputConnectionWrapper;->isActive()Z

    #@353
    move-result v4

    #@354
    if-nez v4, :cond_35f

    #@356
    .line 415
    :cond_356
    const-string v3, "IInputConnectionWrapper"

    #@358
    const-string v4, "performPrivateCommand on inactive InputConnection"

    #@35a
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@35d
    goto/16 :goto_21

    #@35f
    .line 418
    :cond_35f
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@361
    check-cast v0, Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;

    #@363
    .line 419
    .restart local v0       #args:Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;
    sget-boolean v4, Lcom/lge/config/ConfigBuildFlags;->CAPP_BUBBLE_POPUP:Z

    #@365
    if-eqz v4, :cond_376

    #@367
    .line 420
    iget-object v4, v0, Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;->arg1:Ljava/lang/Object;

    #@369
    const-string v5, "ACTION_SHOWING_BUBBLE_POPUP"

    #@36b
    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@36e
    move-result v4

    #@36f
    if-eqz v4, :cond_376

    #@371
    .line 421
    invoke-static {v3}, Landroid/widget/BubblePopupHelper;->setShowingAnyBubblePopup(Z)V

    #@374
    goto/16 :goto_21

    #@376
    .line 425
    :cond_376
    iget-object v3, v0, Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;->arg1:Ljava/lang/Object;

    #@378
    check-cast v3, Ljava/lang/String;

    #@37a
    iget-object v4, v0, Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;->arg2:Ljava/lang/Object;

    #@37c
    check-cast v4, Landroid/os/Bundle;

    #@37e
    invoke-interface {v2, v3, v4}, Landroid/view/inputmethod/InputConnection;->performPrivateCommand(Ljava/lang/String;Landroid/os/Bundle;)Z

    #@381
    goto/16 :goto_21

    #@383
    .line 192
    nop

    #@384
    :sswitch_data_384
    .sparse-switch
        0xa -> :sswitch_22
        0x14 -> :sswitch_5f
        0x19 -> :sswitch_9c
        0x1e -> :sswitch_da
        0x28 -> :sswitch_118
        0x32 -> :sswitch_15a
        0x37 -> :sswitch_1e0
        0x38 -> :sswitch_202
        0x39 -> :sswitch_17e
        0x3a -> :sswitch_1a0
        0x3b -> :sswitch_1c0
        0x3c -> :sswitch_224
        0x3f -> :sswitch_248
        0x41 -> :sswitch_26a
        0x46 -> :sswitch_282
        0x50 -> :sswitch_2c4
        0x5a -> :sswitch_2e6
        0x5f -> :sswitch_304
        0x64 -> :sswitch_322
        0x78 -> :sswitch_346
        0x82 -> :sswitch_2a4
    .end sparse-switch
.end method

.method public finishComposingText()V
    .registers 2

    #@0
    .prologue
    .line 146
    const/16 v0, 0x41

    #@2
    invoke-virtual {p0, v0}, Lcom/android/internal/view/IInputConnectionWrapper;->obtainMessage(I)Landroid/os/Message;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {p0, v0}, Lcom/android/internal/view/IInputConnectionWrapper;->dispatchMessage(Landroid/os/Message;)V

    #@9
    .line 147
    return-void
.end method

.method public getCursorCapsMode(IILcom/android/internal/view/IInputContextCallback;)V
    .registers 5
    .parameter "reqModes"
    .parameter "seq"
    .parameter "callback"

    #@0
    .prologue
    .line 104
    const/16 v0, 0x1e

    #@2
    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/android/internal/view/IInputConnectionWrapper;->obtainMessageISC(IIILcom/android/internal/view/IInputContextCallback;)Landroid/os/Message;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {p0, v0}, Lcom/android/internal/view/IInputConnectionWrapper;->dispatchMessage(Landroid/os/Message;)V

    #@9
    .line 105
    return-void
.end method

.method public getExtractedText(Landroid/view/inputmethod/ExtractedTextRequest;IILcom/android/internal/view/IInputContextCallback;)V
    .registers 11
    .parameter "request"
    .parameter "flags"
    .parameter "seq"
    .parameter "callback"

    #@0
    .prologue
    .line 109
    const/16 v1, 0x28

    #@2
    move-object v0, p0

    #@3
    move v2, p2

    #@4
    move-object v3, p1

    #@5
    move v4, p3

    #@6
    move-object v5, p4

    #@7
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/view/IInputConnectionWrapper;->obtainMessageIOSC(IILjava/lang/Object;ILcom/android/internal/view/IInputContextCallback;)Landroid/os/Message;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {p0, v0}, Lcom/android/internal/view/IInputConnectionWrapper;->dispatchMessage(Landroid/os/Message;)V

    #@e
    .line 111
    return-void
.end method

.method public getSelectedText(IILcom/android/internal/view/IInputContextCallback;)V
    .registers 5
    .parameter "flags"
    .parameter "seq"
    .parameter "callback"

    #@0
    .prologue
    .line 100
    const/16 v0, 0x19

    #@2
    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/android/internal/view/IInputConnectionWrapper;->obtainMessageISC(IIILcom/android/internal/view/IInputContextCallback;)Landroid/os/Message;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {p0, v0}, Lcom/android/internal/view/IInputConnectionWrapper;->dispatchMessage(Landroid/os/Message;)V

    #@9
    .line 101
    return-void
.end method

.method public getTextAfterCursor(IIILcom/android/internal/view/IInputContextCallback;)V
    .registers 11
    .parameter "length"
    .parameter "flags"
    .parameter "seq"
    .parameter "callback"

    #@0
    .prologue
    .line 92
    const/16 v1, 0xa

    #@2
    move-object v0, p0

    #@3
    move v2, p1

    #@4
    move v3, p2

    #@5
    move v4, p3

    #@6
    move-object v5, p4

    #@7
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/view/IInputConnectionWrapper;->obtainMessageIISC(IIIILcom/android/internal/view/IInputContextCallback;)Landroid/os/Message;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {p0, v0}, Lcom/android/internal/view/IInputConnectionWrapper;->dispatchMessage(Landroid/os/Message;)V

    #@e
    .line 93
    return-void
.end method

.method public getTextBeforeCursor(IIILcom/android/internal/view/IInputContextCallback;)V
    .registers 11
    .parameter "length"
    .parameter "flags"
    .parameter "seq"
    .parameter "callback"

    #@0
    .prologue
    .line 96
    const/16 v1, 0x14

    #@2
    move-object v0, p0

    #@3
    move v2, p1

    #@4
    move v3, p2

    #@5
    move v4, p3

    #@6
    move-object v5, p4

    #@7
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/view/IInputConnectionWrapper;->obtainMessageIISC(IIIILcom/android/internal/view/IInputContextCallback;)Landroid/os/Message;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {p0, v0}, Lcom/android/internal/view/IInputConnectionWrapper;->dispatchMessage(Landroid/os/Message;)V

    #@e
    .line 97
    return-void
.end method

.method public isActive()Z
    .registers 2

    #@0
    .prologue
    .line 88
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method obtainMessage(I)Landroid/os/Message;
    .registers 3
    .parameter "what"

    #@0
    .prologue
    .line 434
    iget-object v0, p0, Lcom/android/internal/view/IInputConnectionWrapper;->mH:Landroid/os/Handler;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method obtainMessageII(III)Landroid/os/Message;
    .registers 5
    .parameter "what"
    .parameter "arg1"
    .parameter "arg2"

    #@0
    .prologue
    .line 438
    iget-object v0, p0, Lcom/android/internal/view/IInputConnectionWrapper;->mH:Landroid/os/Handler;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method obtainMessageIISC(IIIILcom/android/internal/view/IInputContextCallback;)Landroid/os/Message;
    .registers 8
    .parameter "what"
    .parameter "arg1"
    .parameter "arg2"
    .parameter "seq"
    .parameter "callback"

    #@0
    .prologue
    .line 453
    new-instance v0, Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;

    #@2
    invoke-direct {v0}, Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;-><init>()V

    #@5
    .line 454
    .local v0, args:Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;
    iput-object p5, v0, Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;->callback:Lcom/android/internal/view/IInputContextCallback;

    #@7
    .line 455
    iput p4, v0, Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;->seq:I

    #@9
    .line 456
    iget-object v1, p0, Lcom/android/internal/view/IInputConnectionWrapper;->mH:Landroid/os/Handler;

    #@b
    invoke-virtual {v1, p1, p2, p3, v0}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@e
    move-result-object v1

    #@f
    return-object v1
.end method

.method obtainMessageIO(IILjava/lang/Object;)Landroid/os/Message;
    .registers 6
    .parameter "what"
    .parameter "arg1"
    .parameter "arg2"

    #@0
    .prologue
    .line 469
    iget-object v0, p0, Lcom/android/internal/view/IInputConnectionWrapper;->mH:Landroid/os/Handler;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-virtual {v0, p1, p2, v1, p3}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method obtainMessageIOSC(IILjava/lang/Object;ILcom/android/internal/view/IInputContextCallback;)Landroid/os/Message;
    .registers 9
    .parameter "what"
    .parameter "arg1"
    .parameter "arg2"
    .parameter "seq"
    .parameter "callback"

    #@0
    .prologue
    .line 461
    new-instance v0, Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;

    #@2
    invoke-direct {v0}, Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;-><init>()V

    #@5
    .line 462
    .local v0, args:Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;
    iput-object p3, v0, Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;->arg1:Ljava/lang/Object;

    #@7
    .line 463
    iput-object p5, v0, Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;->callback:Lcom/android/internal/view/IInputContextCallback;

    #@9
    .line 464
    iput p4, v0, Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;->seq:I

    #@b
    .line 465
    iget-object v1, p0, Lcom/android/internal/view/IInputConnectionWrapper;->mH:Landroid/os/Handler;

    #@d
    const/4 v2, 0x0

    #@e
    invoke-virtual {v1, p1, p2, v2, v0}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@11
    move-result-object v1

    #@12
    return-object v1
.end method

.method obtainMessageISC(IIILcom/android/internal/view/IInputContextCallback;)Landroid/os/Message;
    .registers 8
    .parameter "what"
    .parameter "arg1"
    .parameter "seq"
    .parameter "callback"

    #@0
    .prologue
    .line 446
    new-instance v0, Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;

    #@2
    invoke-direct {v0}, Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;-><init>()V

    #@5
    .line 447
    .local v0, args:Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;
    iput-object p4, v0, Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;->callback:Lcom/android/internal/view/IInputContextCallback;

    #@7
    .line 448
    iput p3, v0, Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;->seq:I

    #@9
    .line 449
    iget-object v1, p0, Lcom/android/internal/view/IInputConnectionWrapper;->mH:Landroid/os/Handler;

    #@b
    const/4 v2, 0x0

    #@c
    invoke-virtual {v1, p1, p2, v2, v0}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@f
    move-result-object v1

    #@10
    return-object v1
.end method

.method obtainMessageO(ILjava/lang/Object;)Landroid/os/Message;
    .registers 5
    .parameter "what"
    .parameter "arg1"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 442
    iget-object v0, p0, Lcom/android/internal/view/IInputConnectionWrapper;->mH:Landroid/os/Handler;

    #@3
    invoke-virtual {v0, p1, v1, v1, p2}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method obtainMessageOO(ILjava/lang/Object;Ljava/lang/Object;)Landroid/os/Message;
    .registers 7
    .parameter "what"
    .parameter "arg1"
    .parameter "arg2"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 473
    new-instance v0, Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;

    #@3
    invoke-direct {v0}, Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;-><init>()V

    #@6
    .line 474
    .local v0, args:Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;
    iput-object p2, v0, Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;->arg1:Ljava/lang/Object;

    #@8
    .line 475
    iput-object p3, v0, Lcom/android/internal/view/IInputConnectionWrapper$SomeArgs;->arg2:Ljava/lang/Object;

    #@a
    .line 476
    iget-object v1, p0, Lcom/android/internal/view/IInputConnectionWrapper;->mH:Landroid/os/Handler;

    #@c
    invoke-virtual {v1, p1, v2, v2, v0}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@f
    move-result-object v1

    #@10
    return-object v1
.end method

.method public performContextMenuAction(I)V
    .registers 4
    .parameter "id"

    #@0
    .prologue
    .line 134
    const/16 v0, 0x3b

    #@2
    const/4 v1, 0x0

    #@3
    invoke-virtual {p0, v0, p1, v1}, Lcom/android/internal/view/IInputConnectionWrapper;->obtainMessageII(III)Landroid/os/Message;

    #@6
    move-result-object v0

    #@7
    invoke-virtual {p0, v0}, Lcom/android/internal/view/IInputConnectionWrapper;->dispatchMessage(Landroid/os/Message;)V

    #@a
    .line 135
    return-void
.end method

.method public performEditorAction(I)V
    .registers 4
    .parameter "id"

    #@0
    .prologue
    .line 130
    const/16 v0, 0x3a

    #@2
    const/4 v1, 0x0

    #@3
    invoke-virtual {p0, v0, p1, v1}, Lcom/android/internal/view/IInputConnectionWrapper;->obtainMessageII(III)Landroid/os/Message;

    #@6
    move-result-object v0

    #@7
    invoke-virtual {p0, v0}, Lcom/android/internal/view/IInputConnectionWrapper;->dispatchMessage(Landroid/os/Message;)V

    #@a
    .line 131
    return-void
.end method

.method public performPrivateCommand(Ljava/lang/String;Landroid/os/Bundle;)V
    .registers 4
    .parameter "action"
    .parameter "data"

    #@0
    .prologue
    .line 175
    const/16 v0, 0x78

    #@2
    invoke-virtual {p0, v0, p1, p2}, Lcom/android/internal/view/IInputConnectionWrapper;->obtainMessageOO(ILjava/lang/Object;Ljava/lang/Object;)Landroid/os/Message;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {p0, v0}, Lcom/android/internal/view/IInputConnectionWrapper;->dispatchMessage(Landroid/os/Message;)V

    #@9
    .line 176
    return-void
.end method

.method public reportFullscreenMode(Z)V
    .registers 5
    .parameter "enabled"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 171
    const/16 v2, 0x64

    #@3
    if-eqz p1, :cond_e

    #@5
    const/4 v0, 0x1

    #@6
    :goto_6
    invoke-virtual {p0, v2, v0, v1}, Lcom/android/internal/view/IInputConnectionWrapper;->obtainMessageII(III)Landroid/os/Message;

    #@9
    move-result-object v0

    #@a
    invoke-virtual {p0, v0}, Lcom/android/internal/view/IInputConnectionWrapper;->dispatchMessage(Landroid/os/Message;)V

    #@d
    .line 172
    return-void

    #@e
    :cond_e
    move v0, v1

    #@f
    .line 171
    goto :goto_6
.end method

.method public sendKeyEvent(Landroid/view/KeyEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 150
    const/16 v0, 0x46

    #@2
    invoke-virtual {p0, v0, p1}, Lcom/android/internal/view/IInputConnectionWrapper;->obtainMessageO(ILjava/lang/Object;)Landroid/os/Message;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {p0, v0}, Lcom/android/internal/view/IInputConnectionWrapper;->dispatchMessage(Landroid/os/Message;)V

    #@9
    .line 151
    return-void
.end method

.method public setComposingRegion(II)V
    .registers 4
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 138
    const/16 v0, 0x3f

    #@2
    invoke-virtual {p0, v0, p1, p2}, Lcom/android/internal/view/IInputConnectionWrapper;->obtainMessageII(III)Landroid/os/Message;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {p0, v0}, Lcom/android/internal/view/IInputConnectionWrapper;->dispatchMessage(Landroid/os/Message;)V

    #@9
    .line 139
    return-void
.end method

.method public setComposingText(Ljava/lang/CharSequence;I)V
    .registers 4
    .parameter "text"
    .parameter "newCursorPosition"

    #@0
    .prologue
    .line 142
    const/16 v0, 0x3c

    #@2
    invoke-virtual {p0, v0, p2, p1}, Lcom/android/internal/view/IInputConnectionWrapper;->obtainMessageIO(IILjava/lang/Object;)Landroid/os/Message;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {p0, v0}, Lcom/android/internal/view/IInputConnectionWrapper;->dispatchMessage(Landroid/os/Message;)V

    #@9
    .line 143
    return-void
.end method

.method public setSelection(II)V
    .registers 4
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 126
    const/16 v0, 0x39

    #@2
    invoke-virtual {p0, v0, p1, p2}, Lcom/android/internal/view/IInputConnectionWrapper;->obtainMessageII(III)Landroid/os/Message;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {p0, v0}, Lcom/android/internal/view/IInputConnectionWrapper;->dispatchMessage(Landroid/os/Message;)V

    #@9
    .line 127
    return-void
.end method
