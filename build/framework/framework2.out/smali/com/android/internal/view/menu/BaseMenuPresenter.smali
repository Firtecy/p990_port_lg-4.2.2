.class public abstract Lcom/android/internal/view/menu/BaseMenuPresenter;
.super Ljava/lang/Object;
.source "BaseMenuPresenter.java"

# interfaces
.implements Lcom/android/internal/view/menu/MenuPresenter;


# instance fields
.field private mCallback:Lcom/android/internal/view/menu/MenuPresenter$Callback;

.field protected mContext:Landroid/content/Context;

.field private mId:I

.field protected mInflater:Landroid/view/LayoutInflater;

.field private mItemLayoutRes:I

.field protected mMenu:Lcom/android/internal/view/menu/MenuBuilder;

.field private mMenuLayoutRes:I

.field protected mMenuView:Lcom/android/internal/view/menu/MenuView;

.field protected mSystemContext:Landroid/content/Context;

.field protected mSystemInflater:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;II)V
    .registers 5
    .parameter "context"
    .parameter "menuLayoutRes"
    .parameter "itemLayoutRes"

    #@0
    .prologue
    .line 53
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 54
    iput-object p1, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mSystemContext:Landroid/content/Context;

    #@5
    .line 55
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    #@8
    move-result-object v0

    #@9
    iput-object v0, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mSystemInflater:Landroid/view/LayoutInflater;

    #@b
    .line 56
    iput p2, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mMenuLayoutRes:I

    #@d
    .line 57
    iput p3, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mItemLayoutRes:I

    #@f
    .line 58
    return-void
.end method


# virtual methods
.method protected addItemView(Landroid/view/View;I)V
    .registers 5
    .parameter "itemView"
    .parameter "childIndex"

    #@0
    .prologue
    .line 125
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    #@3
    move-result-object v0

    #@4
    check-cast v0, Landroid/view/ViewGroup;

    #@6
    .line 126
    .local v0, currentParent:Landroid/view/ViewGroup;
    if-eqz v0, :cond_b

    #@8
    .line 127
    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    #@b
    .line 129
    :cond_b
    iget-object v1, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mMenuView:Lcom/android/internal/view/menu/MenuView;

    #@d
    check-cast v1, Landroid/view/ViewGroup;

    #@f
    invoke-virtual {v1, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    #@12
    .line 130
    return-void
.end method

.method public abstract bindItemView(Lcom/android/internal/view/menu/MenuItemImpl;Lcom/android/internal/view/menu/MenuView$ItemView;)V
.end method

.method public collapseItemActionView(Lcom/android/internal/view/menu/MenuBuilder;Lcom/android/internal/view/menu/MenuItemImpl;)Z
    .registers 4
    .parameter "menu"
    .parameter "item"

    #@0
    .prologue
    .line 224
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public createItemView(Landroid/view/ViewGroup;)Lcom/android/internal/view/menu/MenuView$ItemView;
    .registers 5
    .parameter "parent"

    #@0
    .prologue
    .line 153
    iget-object v0, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mSystemInflater:Landroid/view/LayoutInflater;

    #@2
    iget v1, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mItemLayoutRes:I

    #@4
    const/4 v2, 0x0

    #@5
    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Lcom/android/internal/view/menu/MenuView$ItemView;

    #@b
    return-object v0
.end method

.method public expandItemActionView(Lcom/android/internal/view/menu/MenuBuilder;Lcom/android/internal/view/menu/MenuItemImpl;)Z
    .registers 4
    .parameter "menu"
    .parameter "item"

    #@0
    .prologue
    .line 220
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method protected filterLeftoverView(Landroid/view/ViewGroup;I)Z
    .registers 4
    .parameter "parent"
    .parameter "childIndex"

    #@0
    .prologue
    .line 139
    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->removeViewAt(I)V

    #@3
    .line 140
    const/4 v0, 0x1

    #@4
    return v0
.end method

.method public flagActionItems()Z
    .registers 2

    #@0
    .prologue
    .line 216
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getId()I
    .registers 2

    #@0
    .prologue
    .line 228
    iget v0, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mId:I

    #@2
    return v0
.end method

.method public getItemView(Lcom/android/internal/view/menu/MenuItemImpl;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 5
    .parameter "item"
    .parameter "convertView"
    .parameter "parent"

    #@0
    .prologue
    .line 177
    invoke-virtual {p0, p3}, Lcom/android/internal/view/menu/BaseMenuPresenter;->createItemView(Landroid/view/ViewGroup;)Lcom/android/internal/view/menu/MenuView$ItemView;

    #@3
    move-result-object v0

    #@4
    .line 179
    .local v0, itemView:Lcom/android/internal/view/menu/MenuView$ItemView;
    invoke-virtual {p0, p1, v0}, Lcom/android/internal/view/menu/BaseMenuPresenter;->bindItemView(Lcom/android/internal/view/menu/MenuItemImpl;Lcom/android/internal/view/menu/MenuView$ItemView;)V

    #@7
    .line 180
    check-cast v0, Landroid/view/View;

    #@9
    .end local v0           #itemView:Lcom/android/internal/view/menu/MenuView$ItemView;
    return-object v0
.end method

.method public getMenuView(Landroid/view/ViewGroup;)Lcom/android/internal/view/menu/MenuView;
    .registers 5
    .parameter "root"

    #@0
    .prologue
    .line 69
    iget-object v0, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mMenuView:Lcom/android/internal/view/menu/MenuView;

    #@2
    if-nez v0, :cond_1c

    #@4
    .line 70
    iget-object v0, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mSystemInflater:Landroid/view/LayoutInflater;

    #@6
    iget v1, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mMenuLayoutRes:I

    #@8
    const/4 v2, 0x0

    #@9
    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@c
    move-result-object v0

    #@d
    check-cast v0, Lcom/android/internal/view/menu/MenuView;

    #@f
    iput-object v0, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mMenuView:Lcom/android/internal/view/menu/MenuView;

    #@11
    .line 71
    iget-object v0, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mMenuView:Lcom/android/internal/view/menu/MenuView;

    #@13
    iget-object v1, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@15
    invoke-interface {v0, v1}, Lcom/android/internal/view/menu/MenuView;->initialize(Lcom/android/internal/view/menu/MenuBuilder;)V

    #@18
    .line 72
    const/4 v0, 0x1

    #@19
    invoke-virtual {p0, v0}, Lcom/android/internal/view/menu/BaseMenuPresenter;->updateMenuView(Z)V

    #@1c
    .line 75
    :cond_1c
    iget-object v0, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mMenuView:Lcom/android/internal/view/menu/MenuView;

    #@1e
    return-object v0
.end method

.method public initForMenu(Landroid/content/Context;Lcom/android/internal/view/menu/MenuBuilder;)V
    .registers 4
    .parameter "context"
    .parameter "menu"

    #@0
    .prologue
    .line 62
    iput-object p1, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mContext:Landroid/content/Context;

    #@2
    .line 63
    iget-object v0, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mContext:Landroid/content/Context;

    #@4
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    #@7
    move-result-object v0

    #@8
    iput-object v0, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mInflater:Landroid/view/LayoutInflater;

    #@a
    .line 64
    iput-object p2, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@c
    .line 65
    return-void
.end method

.method public onCloseMenu(Lcom/android/internal/view/menu/MenuBuilder;Z)V
    .registers 4
    .parameter "menu"
    .parameter "allMenusAreClosing"

    #@0
    .prologue
    .line 203
    iget-object v0, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mCallback:Lcom/android/internal/view/menu/MenuPresenter$Callback;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 204
    iget-object v0, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mCallback:Lcom/android/internal/view/menu/MenuPresenter$Callback;

    #@6
    invoke-interface {v0, p1, p2}, Lcom/android/internal/view/menu/MenuPresenter$Callback;->onCloseMenu(Lcom/android/internal/view/menu/MenuBuilder;Z)V

    #@9
    .line 206
    :cond_9
    return-void
.end method

.method public onSubMenuSelected(Lcom/android/internal/view/menu/SubMenuBuilder;)Z
    .registers 3
    .parameter "menu"

    #@0
    .prologue
    .line 209
    iget-object v0, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mCallback:Lcom/android/internal/view/menu/MenuPresenter$Callback;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 210
    iget-object v0, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mCallback:Lcom/android/internal/view/menu/MenuPresenter$Callback;

    #@6
    invoke-interface {v0, p1}, Lcom/android/internal/view/menu/MenuPresenter$Callback;->onOpenSubMenu(Lcom/android/internal/view/menu/MenuBuilder;)Z

    #@9
    move-result v0

    #@a
    .line 212
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public setCallback(Lcom/android/internal/view/menu/MenuPresenter$Callback;)V
    .registers 2
    .parameter "cb"

    #@0
    .prologue
    .line 144
    iput-object p1, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mCallback:Lcom/android/internal/view/menu/MenuPresenter$Callback;

    #@2
    .line 145
    return-void
.end method

.method public setId(I)V
    .registers 2
    .parameter "id"

    #@0
    .prologue
    .line 232
    iput p1, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mId:I

    #@2
    .line 233
    return-void
.end method

.method public shouldIncludeItem(ILcom/android/internal/view/menu/MenuItemImpl;)Z
    .registers 4
    .parameter "childIndex"
    .parameter "item"

    #@0
    .prologue
    .line 199
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public updateMenuView(Z)V
    .registers 12
    .parameter "cleared"

    #@0
    .prologue
    .line 82
    iget-object v7, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mMenuView:Lcom/android/internal/view/menu/MenuView;

    #@2
    check-cast v7, Landroid/view/ViewGroup;

    #@4
    .line 83
    .local v7, parent:Landroid/view/ViewGroup;
    if-nez v7, :cond_7

    #@6
    .line 116
    :cond_6
    return-void

    #@7
    .line 85
    :cond_7
    const/4 v0, 0x0

    #@8
    .line 86
    .local v0, childIndex:I
    iget-object v9, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@a
    if-eqz v9, :cond_52

    #@c
    .line 87
    iget-object v9, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@e
    invoke-virtual {v9}, Lcom/android/internal/view/menu/MenuBuilder;->flagActionItems()V

    #@11
    .line 88
    iget-object v9, p0, Lcom/android/internal/view/menu/BaseMenuPresenter;->mMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@13
    invoke-virtual {v9}, Lcom/android/internal/view/menu/MenuBuilder;->getVisibleItems()Ljava/util/ArrayList;

    #@16
    move-result-object v8

    #@17
    .line 89
    .local v8, visibleItems:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/view/menu/MenuItemImpl;>;"
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    #@1a
    move-result v4

    #@1b
    .line 90
    .local v4, itemCount:I
    const/4 v2, 0x0

    #@1c
    .local v2, i:I
    :goto_1c
    if-ge v2, v4, :cond_52

    #@1e
    .line 91
    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@21
    move-result-object v3

    #@22
    check-cast v3, Lcom/android/internal/view/menu/MenuItemImpl;

    #@24
    .line 92
    .local v3, item:Lcom/android/internal/view/menu/MenuItemImpl;
    invoke-virtual {p0, v0, v3}, Lcom/android/internal/view/menu/BaseMenuPresenter;->shouldIncludeItem(ILcom/android/internal/view/menu/MenuItemImpl;)Z

    #@27
    move-result v9

    #@28
    if-eqz v9, :cond_4d

    #@2a
    .line 93
    invoke-virtual {v7, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    #@2d
    move-result-object v1

    #@2e
    .line 94
    .local v1, convertView:Landroid/view/View;
    instance-of v9, v1, Lcom/android/internal/view/menu/MenuView$ItemView;

    #@30
    if-eqz v9, :cond_50

    #@32
    move-object v9, v1

    #@33
    check-cast v9, Lcom/android/internal/view/menu/MenuView$ItemView;

    #@35
    invoke-interface {v9}, Lcom/android/internal/view/menu/MenuView$ItemView;->getItemData()Lcom/android/internal/view/menu/MenuItemImpl;

    #@38
    move-result-object v6

    #@39
    .line 96
    .local v6, oldItem:Lcom/android/internal/view/menu/MenuItemImpl;
    :goto_39
    invoke-virtual {p0, v3, v1, v7}, Lcom/android/internal/view/menu/BaseMenuPresenter;->getItemView(Lcom/android/internal/view/menu/MenuItemImpl;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    #@3c
    move-result-object v5

    #@3d
    .line 97
    .local v5, itemView:Landroid/view/View;
    if-eq v3, v6, :cond_46

    #@3f
    .line 99
    const/4 v9, 0x0

    #@40
    invoke-virtual {v5, v9}, Landroid/view/View;->setPressed(Z)V

    #@43
    .line 100
    invoke-virtual {v5}, Landroid/view/View;->jumpDrawablesToCurrentState()V

    #@46
    .line 102
    :cond_46
    if-eq v5, v1, :cond_4b

    #@48
    .line 103
    invoke-virtual {p0, v5, v0}, Lcom/android/internal/view/menu/BaseMenuPresenter;->addItemView(Landroid/view/View;I)V

    #@4b
    .line 105
    :cond_4b
    add-int/lit8 v0, v0, 0x1

    #@4d
    .line 90
    .end local v1           #convertView:Landroid/view/View;
    .end local v5           #itemView:Landroid/view/View;
    .end local v6           #oldItem:Lcom/android/internal/view/menu/MenuItemImpl;
    :cond_4d
    add-int/lit8 v2, v2, 0x1

    #@4f
    goto :goto_1c

    #@50
    .line 94
    .restart local v1       #convertView:Landroid/view/View;
    :cond_50
    const/4 v6, 0x0

    #@51
    goto :goto_39

    #@52
    .line 111
    .end local v1           #convertView:Landroid/view/View;
    .end local v2           #i:I
    .end local v3           #item:Lcom/android/internal/view/menu/MenuItemImpl;
    .end local v4           #itemCount:I
    .end local v8           #visibleItems:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/view/menu/MenuItemImpl;>;"
    :cond_52
    :goto_52
    invoke-virtual {v7}, Landroid/view/ViewGroup;->getChildCount()I

    #@55
    move-result v9

    #@56
    if-ge v0, v9, :cond_6

    #@58
    .line 112
    invoke-virtual {p0, v7, v0}, Lcom/android/internal/view/menu/BaseMenuPresenter;->filterLeftoverView(Landroid/view/ViewGroup;I)Z

    #@5b
    move-result v9

    #@5c
    if-nez v9, :cond_52

    #@5e
    .line 113
    add-int/lit8 v0, v0, 0x1

    #@60
    goto :goto_52
.end method
