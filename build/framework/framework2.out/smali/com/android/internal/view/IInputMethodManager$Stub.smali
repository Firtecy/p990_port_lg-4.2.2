.class public abstract Lcom/android/internal/view/IInputMethodManager$Stub;
.super Landroid/os/Binder;
.source "IInputMethodManager.java"

# interfaces
.implements Lcom/android/internal/view/IInputMethodManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/view/IInputMethodManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/view/IInputMethodManager$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.android.internal.view.IInputMethodManager"

.field static final TRANSACTION_addClient:I = 0x6

.field static final TRANSACTION_finishInput:I = 0x9

.field static final TRANSACTION_getCurrentInputMethodSubtype:I = 0x17

.field static final TRANSACTION_getEnabledInputMethodList:I = 0x2

.field static final TRANSACTION_getEnabledInputMethodSubtypeList:I = 0x3

.field static final TRANSACTION_getInputMethodList:I = 0x1

.field static final TRANSACTION_getLastInputMethodSubtype:I = 0x4

.field static final TRANSACTION_getShortcutInputMethodsAndSubtypes:I = 0x5

.field static final TRANSACTION_hideMySoftInput:I = 0x11

.field static final TRANSACTION_hideSoftInput:I = 0xb

.field static final TRANSACTION_notifySuggestionPicked:I = 0x16

.field static final TRANSACTION_registerSuggestionSpansForNotification:I = 0x15

.field static final TRANSACTION_removeClient:I = 0x7

.field static final TRANSACTION_setAdditionalInputMethodSubtypes:I = 0x1c

.field static final TRANSACTION_setCurrentInputMethodSubtype:I = 0x18

.field static final TRANSACTION_setImeWindowStatus:I = 0x14

.field static final TRANSACTION_setInputMethod:I = 0xf

.field static final TRANSACTION_setInputMethodAndSubtype:I = 0x10

.field static final TRANSACTION_setInputMethodEnabled:I = 0x1b

.field static final TRANSACTION_showInputMethodAndSubtypeEnablerFromClient:I = 0xe

.field static final TRANSACTION_showInputMethodPickerFromClient:I = 0xd

.field static final TRANSACTION_showMySoftInput:I = 0x12

.field static final TRANSACTION_showSoftInput:I = 0xa

.field static final TRANSACTION_startInput:I = 0x8

.field static final TRANSACTION_switchToLastInputMethod:I = 0x19

.field static final TRANSACTION_switchToNextInputMethod:I = 0x1a

.field static final TRANSACTION_updateStatusIcon:I = 0x13

.field static final TRANSACTION_windowGainedFocus:I = 0xc


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 18
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 19
    const-string v0, "com.android.internal.view.IInputMethodManager"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/android/internal/view/IInputMethodManager$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 20
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/android/internal/view/IInputMethodManager;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 27
    if-nez p0, :cond_4

    #@2
    .line 28
    const/4 v0, 0x0

    #@3
    .line 34
    :goto_3
    return-object v0

    #@4
    .line 30
    :cond_4
    const-string v1, "com.android.internal.view.IInputMethodManager"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 31
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/android/internal/view/IInputMethodManager;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 32
    check-cast v0, Lcom/android/internal/view/IInputMethodManager;

    #@12
    goto :goto_3

    #@13
    .line 34
    :cond_13
    new-instance v0, Lcom/android/internal/view/IInputMethodManager$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/android/internal/view/IInputMethodManager$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 38
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 16
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 42
    sparse-switch p1, :sswitch_data_3a4

    #@3
    .line 434
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@6
    move-result v0

    #@7
    :goto_7
    return v0

    #@8
    .line 46
    :sswitch_8
    const-string v0, "com.android.internal.view.IInputMethodManager"

    #@a
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@d
    .line 47
    const/4 v0, 0x1

    #@e
    goto :goto_7

    #@f
    .line 51
    :sswitch_f
    const-string v0, "com.android.internal.view.IInputMethodManager"

    #@11
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 52
    invoke-virtual {p0}, Lcom/android/internal/view/IInputMethodManager$Stub;->getInputMethodList()Ljava/util/List;

    #@17
    move-result-object v9

    #@18
    .line 53
    .local v9, _result:Ljava/util/List;,"Ljava/util/List<Landroid/view/inputmethod/InputMethodInfo;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1b
    .line 54
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@1e
    .line 55
    const/4 v0, 0x1

    #@1f
    goto :goto_7

    #@20
    .line 59
    .end local v9           #_result:Ljava/util/List;,"Ljava/util/List<Landroid/view/inputmethod/InputMethodInfo;>;"
    :sswitch_20
    const-string v0, "com.android.internal.view.IInputMethodManager"

    #@22
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@25
    .line 60
    invoke-virtual {p0}, Lcom/android/internal/view/IInputMethodManager$Stub;->getEnabledInputMethodList()Ljava/util/List;

    #@28
    move-result-object v9

    #@29
    .line 61
    .restart local v9       #_result:Ljava/util/List;,"Ljava/util/List<Landroid/view/inputmethod/InputMethodInfo;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2c
    .line 62
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@2f
    .line 63
    const/4 v0, 0x1

    #@30
    goto :goto_7

    #@31
    .line 67
    .end local v9           #_result:Ljava/util/List;,"Ljava/util/List<Landroid/view/inputmethod/InputMethodInfo;>;"
    :sswitch_31
    const-string v0, "com.android.internal.view.IInputMethodManager"

    #@33
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@36
    .line 69
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@39
    move-result v0

    #@3a
    if-eqz v0, :cond_57

    #@3c
    .line 70
    sget-object v0, Landroid/view/inputmethod/InputMethodInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@3e
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@41
    move-result-object v1

    #@42
    check-cast v1, Landroid/view/inputmethod/InputMethodInfo;

    #@44
    .line 76
    .local v1, _arg0:Landroid/view/inputmethod/InputMethodInfo;
    :goto_44
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@47
    move-result v0

    #@48
    if-eqz v0, :cond_59

    #@4a
    const/4 v2, 0x1

    #@4b
    .line 77
    .local v2, _arg1:Z
    :goto_4b
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/view/IInputMethodManager$Stub;->getEnabledInputMethodSubtypeList(Landroid/view/inputmethod/InputMethodInfo;Z)Ljava/util/List;

    #@4e
    move-result-object v10

    #@4f
    .line 78
    .local v10, _result:Ljava/util/List;,"Ljava/util/List<Landroid/view/inputmethod/InputMethodSubtype;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@52
    .line 79
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@55
    .line 80
    const/4 v0, 0x1

    #@56
    goto :goto_7

    #@57
    .line 73
    .end local v1           #_arg0:Landroid/view/inputmethod/InputMethodInfo;
    .end local v2           #_arg1:Z
    .end local v10           #_result:Ljava/util/List;,"Ljava/util/List<Landroid/view/inputmethod/InputMethodSubtype;>;"
    :cond_57
    const/4 v1, 0x0

    #@58
    .restart local v1       #_arg0:Landroid/view/inputmethod/InputMethodInfo;
    goto :goto_44

    #@59
    .line 76
    :cond_59
    const/4 v2, 0x0

    #@5a
    goto :goto_4b

    #@5b
    .line 84
    .end local v1           #_arg0:Landroid/view/inputmethod/InputMethodInfo;
    :sswitch_5b
    const-string v0, "com.android.internal.view.IInputMethodManager"

    #@5d
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@60
    .line 85
    invoke-virtual {p0}, Lcom/android/internal/view/IInputMethodManager$Stub;->getLastInputMethodSubtype()Landroid/view/inputmethod/InputMethodSubtype;

    #@63
    move-result-object v8

    #@64
    .line 86
    .local v8, _result:Landroid/view/inputmethod/InputMethodSubtype;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@67
    .line 87
    if-eqz v8, :cond_73

    #@69
    .line 88
    const/4 v0, 0x1

    #@6a
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@6d
    .line 89
    const/4 v0, 0x1

    #@6e
    invoke-virtual {v8, p3, v0}, Landroid/view/inputmethod/InputMethodSubtype;->writeToParcel(Landroid/os/Parcel;I)V

    #@71
    .line 94
    :goto_71
    const/4 v0, 0x1

    #@72
    goto :goto_7

    #@73
    .line 92
    :cond_73
    const/4 v0, 0x0

    #@74
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@77
    goto :goto_71

    #@78
    .line 98
    .end local v8           #_result:Landroid/view/inputmethod/InputMethodSubtype;
    :sswitch_78
    const-string v0, "com.android.internal.view.IInputMethodManager"

    #@7a
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@7d
    .line 99
    invoke-virtual {p0}, Lcom/android/internal/view/IInputMethodManager$Stub;->getShortcutInputMethodsAndSubtypes()Ljava/util/List;

    #@80
    move-result-object v8

    #@81
    .line 100
    .local v8, _result:Ljava/util/List;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@84
    .line 101
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    #@87
    .line 102
    const/4 v0, 0x1

    #@88
    goto/16 :goto_7

    #@8a
    .line 106
    .end local v8           #_result:Ljava/util/List;
    :sswitch_8a
    const-string v0, "com.android.internal.view.IInputMethodManager"

    #@8c
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@8f
    .line 108
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@92
    move-result-object v0

    #@93
    invoke-static {v0}, Lcom/android/internal/view/IInputMethodClient$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/view/IInputMethodClient;

    #@96
    move-result-object v1

    #@97
    .line 110
    .local v1, _arg0:Lcom/android/internal/view/IInputMethodClient;
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@9a
    move-result-object v0

    #@9b
    invoke-static {v0}, Lcom/android/internal/view/IInputContext$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/view/IInputContext;

    #@9e
    move-result-object v2

    #@9f
    .line 112
    .local v2, _arg1:Lcom/android/internal/view/IInputContext;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@a2
    move-result v3

    #@a3
    .line 114
    .local v3, _arg2:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@a6
    move-result v4

    #@a7
    .line 115
    .local v4, _arg3:I
    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/android/internal/view/IInputMethodManager$Stub;->addClient(Lcom/android/internal/view/IInputMethodClient;Lcom/android/internal/view/IInputContext;II)V

    #@aa
    .line 116
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@ad
    .line 117
    const/4 v0, 0x1

    #@ae
    goto/16 :goto_7

    #@b0
    .line 121
    .end local v1           #_arg0:Lcom/android/internal/view/IInputMethodClient;
    .end local v2           #_arg1:Lcom/android/internal/view/IInputContext;
    .end local v3           #_arg2:I
    .end local v4           #_arg3:I
    :sswitch_b0
    const-string v0, "com.android.internal.view.IInputMethodManager"

    #@b2
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@b5
    .line 123
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@b8
    move-result-object v0

    #@b9
    invoke-static {v0}, Lcom/android/internal/view/IInputMethodClient$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/view/IInputMethodClient;

    #@bc
    move-result-object v1

    #@bd
    .line 124
    .restart local v1       #_arg0:Lcom/android/internal/view/IInputMethodClient;
    invoke-virtual {p0, v1}, Lcom/android/internal/view/IInputMethodManager$Stub;->removeClient(Lcom/android/internal/view/IInputMethodClient;)V

    #@c0
    .line 125
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@c3
    .line 126
    const/4 v0, 0x1

    #@c4
    goto/16 :goto_7

    #@c6
    .line 130
    .end local v1           #_arg0:Lcom/android/internal/view/IInputMethodClient;
    :sswitch_c6
    const-string v0, "com.android.internal.view.IInputMethodManager"

    #@c8
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@cb
    .line 132
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@ce
    move-result-object v0

    #@cf
    invoke-static {v0}, Lcom/android/internal/view/IInputMethodClient$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/view/IInputMethodClient;

    #@d2
    move-result-object v1

    #@d3
    .line 134
    .restart local v1       #_arg0:Lcom/android/internal/view/IInputMethodClient;
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@d6
    move-result-object v0

    #@d7
    invoke-static {v0}, Lcom/android/internal/view/IInputContext$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/view/IInputContext;

    #@da
    move-result-object v2

    #@db
    .line 136
    .restart local v2       #_arg1:Lcom/android/internal/view/IInputContext;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@de
    move-result v0

    #@df
    if-eqz v0, :cond_101

    #@e1
    .line 137
    sget-object v0, Landroid/view/inputmethod/EditorInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@e3
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@e6
    move-result-object v3

    #@e7
    check-cast v3, Landroid/view/inputmethod/EditorInfo;

    #@e9
    .line 143
    .local v3, _arg2:Landroid/view/inputmethod/EditorInfo;
    :goto_e9
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@ec
    move-result v4

    #@ed
    .line 144
    .restart local v4       #_arg3:I
    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/android/internal/view/IInputMethodManager$Stub;->startInput(Lcom/android/internal/view/IInputMethodClient;Lcom/android/internal/view/IInputContext;Landroid/view/inputmethod/EditorInfo;I)Lcom/android/internal/view/InputBindResult;

    #@f0
    move-result-object v8

    #@f1
    .line 145
    .local v8, _result:Lcom/android/internal/view/InputBindResult;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@f4
    .line 146
    if-eqz v8, :cond_103

    #@f6
    .line 147
    const/4 v0, 0x1

    #@f7
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@fa
    .line 148
    const/4 v0, 0x1

    #@fb
    invoke-virtual {v8, p3, v0}, Lcom/android/internal/view/InputBindResult;->writeToParcel(Landroid/os/Parcel;I)V

    #@fe
    .line 153
    :goto_fe
    const/4 v0, 0x1

    #@ff
    goto/16 :goto_7

    #@101
    .line 140
    .end local v3           #_arg2:Landroid/view/inputmethod/EditorInfo;
    .end local v4           #_arg3:I
    .end local v8           #_result:Lcom/android/internal/view/InputBindResult;
    :cond_101
    const/4 v3, 0x0

    #@102
    .restart local v3       #_arg2:Landroid/view/inputmethod/EditorInfo;
    goto :goto_e9

    #@103
    .line 151
    .restart local v4       #_arg3:I
    .restart local v8       #_result:Lcom/android/internal/view/InputBindResult;
    :cond_103
    const/4 v0, 0x0

    #@104
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@107
    goto :goto_fe

    #@108
    .line 157
    .end local v1           #_arg0:Lcom/android/internal/view/IInputMethodClient;
    .end local v2           #_arg1:Lcom/android/internal/view/IInputContext;
    .end local v3           #_arg2:Landroid/view/inputmethod/EditorInfo;
    .end local v4           #_arg3:I
    .end local v8           #_result:Lcom/android/internal/view/InputBindResult;
    :sswitch_108
    const-string v0, "com.android.internal.view.IInputMethodManager"

    #@10a
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@10d
    .line 159
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@110
    move-result-object v0

    #@111
    invoke-static {v0}, Lcom/android/internal/view/IInputMethodClient$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/view/IInputMethodClient;

    #@114
    move-result-object v1

    #@115
    .line 160
    .restart local v1       #_arg0:Lcom/android/internal/view/IInputMethodClient;
    invoke-virtual {p0, v1}, Lcom/android/internal/view/IInputMethodManager$Stub;->finishInput(Lcom/android/internal/view/IInputMethodClient;)V

    #@118
    .line 161
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@11b
    .line 162
    const/4 v0, 0x1

    #@11c
    goto/16 :goto_7

    #@11e
    .line 166
    .end local v1           #_arg0:Lcom/android/internal/view/IInputMethodClient;
    :sswitch_11e
    const-string v0, "com.android.internal.view.IInputMethodManager"

    #@120
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@123
    .line 168
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@126
    move-result-object v0

    #@127
    invoke-static {v0}, Lcom/android/internal/view/IInputMethodClient$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/view/IInputMethodClient;

    #@12a
    move-result-object v1

    #@12b
    .line 170
    .restart local v1       #_arg0:Lcom/android/internal/view/IInputMethodClient;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@12e
    move-result v2

    #@12f
    .line 172
    .local v2, _arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@132
    move-result v0

    #@133
    if-eqz v0, :cond_14d

    #@135
    .line 173
    sget-object v0, Landroid/os/ResultReceiver;->CREATOR:Landroid/os/Parcelable$Creator;

    #@137
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@13a
    move-result-object v3

    #@13b
    check-cast v3, Landroid/os/ResultReceiver;

    #@13d
    .line 178
    .local v3, _arg2:Landroid/os/ResultReceiver;
    :goto_13d
    invoke-virtual {p0, v1, v2, v3}, Lcom/android/internal/view/IInputMethodManager$Stub;->showSoftInput(Lcom/android/internal/view/IInputMethodClient;ILandroid/os/ResultReceiver;)Z

    #@140
    move-result v8

    #@141
    .line 179
    .local v8, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@144
    .line 180
    if-eqz v8, :cond_14f

    #@146
    const/4 v0, 0x1

    #@147
    :goto_147
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@14a
    .line 181
    const/4 v0, 0x1

    #@14b
    goto/16 :goto_7

    #@14d
    .line 176
    .end local v3           #_arg2:Landroid/os/ResultReceiver;
    .end local v8           #_result:Z
    :cond_14d
    const/4 v3, 0x0

    #@14e
    .restart local v3       #_arg2:Landroid/os/ResultReceiver;
    goto :goto_13d

    #@14f
    .line 180
    .restart local v8       #_result:Z
    :cond_14f
    const/4 v0, 0x0

    #@150
    goto :goto_147

    #@151
    .line 185
    .end local v1           #_arg0:Lcom/android/internal/view/IInputMethodClient;
    .end local v2           #_arg1:I
    .end local v3           #_arg2:Landroid/os/ResultReceiver;
    .end local v8           #_result:Z
    :sswitch_151
    const-string v0, "com.android.internal.view.IInputMethodManager"

    #@153
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@156
    .line 187
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@159
    move-result-object v0

    #@15a
    invoke-static {v0}, Lcom/android/internal/view/IInputMethodClient$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/view/IInputMethodClient;

    #@15d
    move-result-object v1

    #@15e
    .line 189
    .restart local v1       #_arg0:Lcom/android/internal/view/IInputMethodClient;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@161
    move-result v2

    #@162
    .line 191
    .restart local v2       #_arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@165
    move-result v0

    #@166
    if-eqz v0, :cond_180

    #@168
    .line 192
    sget-object v0, Landroid/os/ResultReceiver;->CREATOR:Landroid/os/Parcelable$Creator;

    #@16a
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@16d
    move-result-object v3

    #@16e
    check-cast v3, Landroid/os/ResultReceiver;

    #@170
    .line 197
    .restart local v3       #_arg2:Landroid/os/ResultReceiver;
    :goto_170
    invoke-virtual {p0, v1, v2, v3}, Lcom/android/internal/view/IInputMethodManager$Stub;->hideSoftInput(Lcom/android/internal/view/IInputMethodClient;ILandroid/os/ResultReceiver;)Z

    #@173
    move-result v8

    #@174
    .line 198
    .restart local v8       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@177
    .line 199
    if-eqz v8, :cond_182

    #@179
    const/4 v0, 0x1

    #@17a
    :goto_17a
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@17d
    .line 200
    const/4 v0, 0x1

    #@17e
    goto/16 :goto_7

    #@180
    .line 195
    .end local v3           #_arg2:Landroid/os/ResultReceiver;
    .end local v8           #_result:Z
    :cond_180
    const/4 v3, 0x0

    #@181
    .restart local v3       #_arg2:Landroid/os/ResultReceiver;
    goto :goto_170

    #@182
    .line 199
    .restart local v8       #_result:Z
    :cond_182
    const/4 v0, 0x0

    #@183
    goto :goto_17a

    #@184
    .line 204
    .end local v1           #_arg0:Lcom/android/internal/view/IInputMethodClient;
    .end local v2           #_arg1:I
    .end local v3           #_arg2:Landroid/os/ResultReceiver;
    .end local v8           #_result:Z
    :sswitch_184
    const-string v0, "com.android.internal.view.IInputMethodManager"

    #@186
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@189
    .line 206
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@18c
    move-result-object v0

    #@18d
    invoke-static {v0}, Lcom/android/internal/view/IInputMethodClient$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/view/IInputMethodClient;

    #@190
    move-result-object v1

    #@191
    .line 208
    .restart local v1       #_arg0:Lcom/android/internal/view/IInputMethodClient;
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@194
    move-result-object v2

    #@195
    .line 210
    .local v2, _arg1:Landroid/os/IBinder;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@198
    move-result v3

    #@199
    .line 212
    .local v3, _arg2:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@19c
    move-result v4

    #@19d
    .line 214
    .restart local v4       #_arg3:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1a0
    move-result v5

    #@1a1
    .line 216
    .local v5, _arg4:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1a4
    move-result v0

    #@1a5
    if-eqz v0, :cond_1cc

    #@1a7
    .line 217
    sget-object v0, Landroid/view/inputmethod/EditorInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1a9
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1ac
    move-result-object v6

    #@1ad
    check-cast v6, Landroid/view/inputmethod/EditorInfo;

    #@1af
    .line 223
    .local v6, _arg5:Landroid/view/inputmethod/EditorInfo;
    :goto_1af
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@1b2
    move-result-object v0

    #@1b3
    invoke-static {v0}, Lcom/android/internal/view/IInputContext$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/view/IInputContext;

    #@1b6
    move-result-object v7

    #@1b7
    .local v7, _arg6:Lcom/android/internal/view/IInputContext;
    move-object v0, p0

    #@1b8
    .line 224
    invoke-virtual/range {v0 .. v7}, Lcom/android/internal/view/IInputMethodManager$Stub;->windowGainedFocus(Lcom/android/internal/view/IInputMethodClient;Landroid/os/IBinder;IIILandroid/view/inputmethod/EditorInfo;Lcom/android/internal/view/IInputContext;)Lcom/android/internal/view/InputBindResult;

    #@1bb
    move-result-object v8

    #@1bc
    .line 225
    .local v8, _result:Lcom/android/internal/view/InputBindResult;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1bf
    .line 226
    if-eqz v8, :cond_1ce

    #@1c1
    .line 227
    const/4 v0, 0x1

    #@1c2
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@1c5
    .line 228
    const/4 v0, 0x1

    #@1c6
    invoke-virtual {v8, p3, v0}, Lcom/android/internal/view/InputBindResult;->writeToParcel(Landroid/os/Parcel;I)V

    #@1c9
    .line 233
    :goto_1c9
    const/4 v0, 0x1

    #@1ca
    goto/16 :goto_7

    #@1cc
    .line 220
    .end local v6           #_arg5:Landroid/view/inputmethod/EditorInfo;
    .end local v7           #_arg6:Lcom/android/internal/view/IInputContext;
    .end local v8           #_result:Lcom/android/internal/view/InputBindResult;
    :cond_1cc
    const/4 v6, 0x0

    #@1cd
    .restart local v6       #_arg5:Landroid/view/inputmethod/EditorInfo;
    goto :goto_1af

    #@1ce
    .line 231
    .restart local v7       #_arg6:Lcom/android/internal/view/IInputContext;
    .restart local v8       #_result:Lcom/android/internal/view/InputBindResult;
    :cond_1ce
    const/4 v0, 0x0

    #@1cf
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@1d2
    goto :goto_1c9

    #@1d3
    .line 237
    .end local v1           #_arg0:Lcom/android/internal/view/IInputMethodClient;
    .end local v2           #_arg1:Landroid/os/IBinder;
    .end local v3           #_arg2:I
    .end local v4           #_arg3:I
    .end local v5           #_arg4:I
    .end local v6           #_arg5:Landroid/view/inputmethod/EditorInfo;
    .end local v7           #_arg6:Lcom/android/internal/view/IInputContext;
    .end local v8           #_result:Lcom/android/internal/view/InputBindResult;
    :sswitch_1d3
    const-string v0, "com.android.internal.view.IInputMethodManager"

    #@1d5
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1d8
    .line 239
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@1db
    move-result-object v0

    #@1dc
    invoke-static {v0}, Lcom/android/internal/view/IInputMethodClient$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/view/IInputMethodClient;

    #@1df
    move-result-object v1

    #@1e0
    .line 240
    .restart local v1       #_arg0:Lcom/android/internal/view/IInputMethodClient;
    invoke-virtual {p0, v1}, Lcom/android/internal/view/IInputMethodManager$Stub;->showInputMethodPickerFromClient(Lcom/android/internal/view/IInputMethodClient;)V

    #@1e3
    .line 241
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1e6
    .line 242
    const/4 v0, 0x1

    #@1e7
    goto/16 :goto_7

    #@1e9
    .line 246
    .end local v1           #_arg0:Lcom/android/internal/view/IInputMethodClient;
    :sswitch_1e9
    const-string v0, "com.android.internal.view.IInputMethodManager"

    #@1eb
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1ee
    .line 248
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@1f1
    move-result-object v0

    #@1f2
    invoke-static {v0}, Lcom/android/internal/view/IInputMethodClient$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/view/IInputMethodClient;

    #@1f5
    move-result-object v1

    #@1f6
    .line 250
    .restart local v1       #_arg0:Lcom/android/internal/view/IInputMethodClient;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1f9
    move-result-object v2

    #@1fa
    .line 251
    .local v2, _arg1:Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/view/IInputMethodManager$Stub;->showInputMethodAndSubtypeEnablerFromClient(Lcom/android/internal/view/IInputMethodClient;Ljava/lang/String;)V

    #@1fd
    .line 252
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@200
    .line 253
    const/4 v0, 0x1

    #@201
    goto/16 :goto_7

    #@203
    .line 257
    .end local v1           #_arg0:Lcom/android/internal/view/IInputMethodClient;
    .end local v2           #_arg1:Ljava/lang/String;
    :sswitch_203
    const-string v0, "com.android.internal.view.IInputMethodManager"

    #@205
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@208
    .line 259
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@20b
    move-result-object v1

    #@20c
    .line 261
    .local v1, _arg0:Landroid/os/IBinder;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@20f
    move-result-object v2

    #@210
    .line 262
    .restart local v2       #_arg1:Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/view/IInputMethodManager$Stub;->setInputMethod(Landroid/os/IBinder;Ljava/lang/String;)V

    #@213
    .line 263
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@216
    .line 264
    const/4 v0, 0x1

    #@217
    goto/16 :goto_7

    #@219
    .line 268
    .end local v1           #_arg0:Landroid/os/IBinder;
    .end local v2           #_arg1:Ljava/lang/String;
    :sswitch_219
    const-string v0, "com.android.internal.view.IInputMethodManager"

    #@21b
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@21e
    .line 270
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@221
    move-result-object v1

    #@222
    .line 272
    .restart local v1       #_arg0:Landroid/os/IBinder;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@225
    move-result-object v2

    #@226
    .line 274
    .restart local v2       #_arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@229
    move-result v0

    #@22a
    if-eqz v0, :cond_23d

    #@22c
    .line 275
    sget-object v0, Landroid/view/inputmethod/InputMethodSubtype;->CREATOR:Landroid/os/Parcelable$Creator;

    #@22e
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@231
    move-result-object v3

    #@232
    check-cast v3, Landroid/view/inputmethod/InputMethodSubtype;

    #@234
    .line 280
    .local v3, _arg2:Landroid/view/inputmethod/InputMethodSubtype;
    :goto_234
    invoke-virtual {p0, v1, v2, v3}, Lcom/android/internal/view/IInputMethodManager$Stub;->setInputMethodAndSubtype(Landroid/os/IBinder;Ljava/lang/String;Landroid/view/inputmethod/InputMethodSubtype;)V

    #@237
    .line 281
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@23a
    .line 282
    const/4 v0, 0x1

    #@23b
    goto/16 :goto_7

    #@23d
    .line 278
    .end local v3           #_arg2:Landroid/view/inputmethod/InputMethodSubtype;
    :cond_23d
    const/4 v3, 0x0

    #@23e
    .restart local v3       #_arg2:Landroid/view/inputmethod/InputMethodSubtype;
    goto :goto_234

    #@23f
    .line 286
    .end local v1           #_arg0:Landroid/os/IBinder;
    .end local v2           #_arg1:Ljava/lang/String;
    .end local v3           #_arg2:Landroid/view/inputmethod/InputMethodSubtype;
    :sswitch_23f
    const-string v0, "com.android.internal.view.IInputMethodManager"

    #@241
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@244
    .line 288
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@247
    move-result-object v1

    #@248
    .line 290
    .restart local v1       #_arg0:Landroid/os/IBinder;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@24b
    move-result v2

    #@24c
    .line 291
    .local v2, _arg1:I
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/view/IInputMethodManager$Stub;->hideMySoftInput(Landroid/os/IBinder;I)V

    #@24f
    .line 292
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@252
    .line 293
    const/4 v0, 0x1

    #@253
    goto/16 :goto_7

    #@255
    .line 297
    .end local v1           #_arg0:Landroid/os/IBinder;
    .end local v2           #_arg1:I
    :sswitch_255
    const-string v0, "com.android.internal.view.IInputMethodManager"

    #@257
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@25a
    .line 299
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@25d
    move-result-object v1

    #@25e
    .line 301
    .restart local v1       #_arg0:Landroid/os/IBinder;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@261
    move-result v2

    #@262
    .line 302
    .restart local v2       #_arg1:I
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/view/IInputMethodManager$Stub;->showMySoftInput(Landroid/os/IBinder;I)V

    #@265
    .line 303
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@268
    .line 304
    const/4 v0, 0x1

    #@269
    goto/16 :goto_7

    #@26b
    .line 308
    .end local v1           #_arg0:Landroid/os/IBinder;
    .end local v2           #_arg1:I
    :sswitch_26b
    const-string v0, "com.android.internal.view.IInputMethodManager"

    #@26d
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@270
    .line 310
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@273
    move-result-object v1

    #@274
    .line 312
    .restart local v1       #_arg0:Landroid/os/IBinder;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@277
    move-result-object v2

    #@278
    .line 314
    .local v2, _arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@27b
    move-result v3

    #@27c
    .line 315
    .local v3, _arg2:I
    invoke-virtual {p0, v1, v2, v3}, Lcom/android/internal/view/IInputMethodManager$Stub;->updateStatusIcon(Landroid/os/IBinder;Ljava/lang/String;I)V

    #@27f
    .line 316
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@282
    .line 317
    const/4 v0, 0x1

    #@283
    goto/16 :goto_7

    #@285
    .line 321
    .end local v1           #_arg0:Landroid/os/IBinder;
    .end local v2           #_arg1:Ljava/lang/String;
    .end local v3           #_arg2:I
    :sswitch_285
    const-string v0, "com.android.internal.view.IInputMethodManager"

    #@287
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@28a
    .line 323
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@28d
    move-result-object v1

    #@28e
    .line 325
    .restart local v1       #_arg0:Landroid/os/IBinder;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@291
    move-result v2

    #@292
    .line 327
    .local v2, _arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@295
    move-result v3

    #@296
    .line 328
    .restart local v3       #_arg2:I
    invoke-virtual {p0, v1, v2, v3}, Lcom/android/internal/view/IInputMethodManager$Stub;->setImeWindowStatus(Landroid/os/IBinder;II)V

    #@299
    .line 329
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@29c
    .line 330
    const/4 v0, 0x1

    #@29d
    goto/16 :goto_7

    #@29f
    .line 334
    .end local v1           #_arg0:Landroid/os/IBinder;
    .end local v2           #_arg1:I
    .end local v3           #_arg2:I
    :sswitch_29f
    const-string v0, "com.android.internal.view.IInputMethodManager"

    #@2a1
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2a4
    .line 336
    sget-object v0, Landroid/text/style/SuggestionSpan;->CREATOR:Landroid/os/Parcelable$Creator;

    #@2a6
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    #@2a9
    move-result-object v1

    #@2aa
    check-cast v1, [Landroid/text/style/SuggestionSpan;

    #@2ac
    .line 337
    .local v1, _arg0:[Landroid/text/style/SuggestionSpan;
    invoke-virtual {p0, v1}, Lcom/android/internal/view/IInputMethodManager$Stub;->registerSuggestionSpansForNotification([Landroid/text/style/SuggestionSpan;)V

    #@2af
    .line 338
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2b2
    .line 339
    const/4 v0, 0x1

    #@2b3
    goto/16 :goto_7

    #@2b5
    .line 343
    .end local v1           #_arg0:[Landroid/text/style/SuggestionSpan;
    :sswitch_2b5
    const-string v0, "com.android.internal.view.IInputMethodManager"

    #@2b7
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2ba
    .line 345
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2bd
    move-result v0

    #@2be
    if-eqz v0, :cond_2e0

    #@2c0
    .line 346
    sget-object v0, Landroid/text/style/SuggestionSpan;->CREATOR:Landroid/os/Parcelable$Creator;

    #@2c2
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@2c5
    move-result-object v1

    #@2c6
    check-cast v1, Landroid/text/style/SuggestionSpan;

    #@2c8
    .line 352
    .local v1, _arg0:Landroid/text/style/SuggestionSpan;
    :goto_2c8
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2cb
    move-result-object v2

    #@2cc
    .line 354
    .local v2, _arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2cf
    move-result v3

    #@2d0
    .line 355
    .restart local v3       #_arg2:I
    invoke-virtual {p0, v1, v2, v3}, Lcom/android/internal/view/IInputMethodManager$Stub;->notifySuggestionPicked(Landroid/text/style/SuggestionSpan;Ljava/lang/String;I)Z

    #@2d3
    move-result v8

    #@2d4
    .line 356
    .local v8, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2d7
    .line 357
    if-eqz v8, :cond_2e2

    #@2d9
    const/4 v0, 0x1

    #@2da
    :goto_2da
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@2dd
    .line 358
    const/4 v0, 0x1

    #@2de
    goto/16 :goto_7

    #@2e0
    .line 349
    .end local v1           #_arg0:Landroid/text/style/SuggestionSpan;
    .end local v2           #_arg1:Ljava/lang/String;
    .end local v3           #_arg2:I
    .end local v8           #_result:Z
    :cond_2e0
    const/4 v1, 0x0

    #@2e1
    .restart local v1       #_arg0:Landroid/text/style/SuggestionSpan;
    goto :goto_2c8

    #@2e2
    .line 357
    .restart local v2       #_arg1:Ljava/lang/String;
    .restart local v3       #_arg2:I
    .restart local v8       #_result:Z
    :cond_2e2
    const/4 v0, 0x0

    #@2e3
    goto :goto_2da

    #@2e4
    .line 362
    .end local v1           #_arg0:Landroid/text/style/SuggestionSpan;
    .end local v2           #_arg1:Ljava/lang/String;
    .end local v3           #_arg2:I
    .end local v8           #_result:Z
    :sswitch_2e4
    const-string v0, "com.android.internal.view.IInputMethodManager"

    #@2e6
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2e9
    .line 363
    invoke-virtual {p0}, Lcom/android/internal/view/IInputMethodManager$Stub;->getCurrentInputMethodSubtype()Landroid/view/inputmethod/InputMethodSubtype;

    #@2ec
    move-result-object v8

    #@2ed
    .line 364
    .local v8, _result:Landroid/view/inputmethod/InputMethodSubtype;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2f0
    .line 365
    if-eqz v8, :cond_2fd

    #@2f2
    .line 366
    const/4 v0, 0x1

    #@2f3
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@2f6
    .line 367
    const/4 v0, 0x1

    #@2f7
    invoke-virtual {v8, p3, v0}, Landroid/view/inputmethod/InputMethodSubtype;->writeToParcel(Landroid/os/Parcel;I)V

    #@2fa
    .line 372
    :goto_2fa
    const/4 v0, 0x1

    #@2fb
    goto/16 :goto_7

    #@2fd
    .line 370
    :cond_2fd
    const/4 v0, 0x0

    #@2fe
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@301
    goto :goto_2fa

    #@302
    .line 376
    .end local v8           #_result:Landroid/view/inputmethod/InputMethodSubtype;
    :sswitch_302
    const-string v0, "com.android.internal.view.IInputMethodManager"

    #@304
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@307
    .line 378
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@30a
    move-result v0

    #@30b
    if-eqz v0, :cond_325

    #@30d
    .line 379
    sget-object v0, Landroid/view/inputmethod/InputMethodSubtype;->CREATOR:Landroid/os/Parcelable$Creator;

    #@30f
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@312
    move-result-object v1

    #@313
    check-cast v1, Landroid/view/inputmethod/InputMethodSubtype;

    #@315
    .line 384
    .local v1, _arg0:Landroid/view/inputmethod/InputMethodSubtype;
    :goto_315
    invoke-virtual {p0, v1}, Lcom/android/internal/view/IInputMethodManager$Stub;->setCurrentInputMethodSubtype(Landroid/view/inputmethod/InputMethodSubtype;)Z

    #@318
    move-result v8

    #@319
    .line 385
    .local v8, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@31c
    .line 386
    if-eqz v8, :cond_327

    #@31e
    const/4 v0, 0x1

    #@31f
    :goto_31f
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@322
    .line 387
    const/4 v0, 0x1

    #@323
    goto/16 :goto_7

    #@325
    .line 382
    .end local v1           #_arg0:Landroid/view/inputmethod/InputMethodSubtype;
    .end local v8           #_result:Z
    :cond_325
    const/4 v1, 0x0

    #@326
    .restart local v1       #_arg0:Landroid/view/inputmethod/InputMethodSubtype;
    goto :goto_315

    #@327
    .line 386
    .restart local v8       #_result:Z
    :cond_327
    const/4 v0, 0x0

    #@328
    goto :goto_31f

    #@329
    .line 391
    .end local v1           #_arg0:Landroid/view/inputmethod/InputMethodSubtype;
    .end local v8           #_result:Z
    :sswitch_329
    const-string v0, "com.android.internal.view.IInputMethodManager"

    #@32b
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@32e
    .line 393
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@331
    move-result-object v1

    #@332
    .line 394
    .local v1, _arg0:Landroid/os/IBinder;
    invoke-virtual {p0, v1}, Lcom/android/internal/view/IInputMethodManager$Stub;->switchToLastInputMethod(Landroid/os/IBinder;)Z

    #@335
    move-result v8

    #@336
    .line 395
    .restart local v8       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@339
    .line 396
    if-eqz v8, :cond_342

    #@33b
    const/4 v0, 0x1

    #@33c
    :goto_33c
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@33f
    .line 397
    const/4 v0, 0x1

    #@340
    goto/16 :goto_7

    #@342
    .line 396
    :cond_342
    const/4 v0, 0x0

    #@343
    goto :goto_33c

    #@344
    .line 401
    .end local v1           #_arg0:Landroid/os/IBinder;
    .end local v8           #_result:Z
    :sswitch_344
    const-string v0, "com.android.internal.view.IInputMethodManager"

    #@346
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@349
    .line 403
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@34c
    move-result-object v1

    #@34d
    .line 405
    .restart local v1       #_arg0:Landroid/os/IBinder;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@350
    move-result v0

    #@351
    if-eqz v0, :cond_364

    #@353
    const/4 v2, 0x1

    #@354
    .line 406
    .local v2, _arg1:Z
    :goto_354
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/view/IInputMethodManager$Stub;->switchToNextInputMethod(Landroid/os/IBinder;Z)Z

    #@357
    move-result v8

    #@358
    .line 407
    .restart local v8       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@35b
    .line 408
    if-eqz v8, :cond_366

    #@35d
    const/4 v0, 0x1

    #@35e
    :goto_35e
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@361
    .line 409
    const/4 v0, 0x1

    #@362
    goto/16 :goto_7

    #@364
    .line 405
    .end local v2           #_arg1:Z
    .end local v8           #_result:Z
    :cond_364
    const/4 v2, 0x0

    #@365
    goto :goto_354

    #@366
    .line 408
    .restart local v2       #_arg1:Z
    .restart local v8       #_result:Z
    :cond_366
    const/4 v0, 0x0

    #@367
    goto :goto_35e

    #@368
    .line 413
    .end local v1           #_arg0:Landroid/os/IBinder;
    .end local v2           #_arg1:Z
    .end local v8           #_result:Z
    :sswitch_368
    const-string v0, "com.android.internal.view.IInputMethodManager"

    #@36a
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@36d
    .line 415
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@370
    move-result-object v1

    #@371
    .line 417
    .local v1, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@374
    move-result v0

    #@375
    if-eqz v0, :cond_388

    #@377
    const/4 v2, 0x1

    #@378
    .line 418
    .restart local v2       #_arg1:Z
    :goto_378
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/view/IInputMethodManager$Stub;->setInputMethodEnabled(Ljava/lang/String;Z)Z

    #@37b
    move-result v8

    #@37c
    .line 419
    .restart local v8       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@37f
    .line 420
    if-eqz v8, :cond_38a

    #@381
    const/4 v0, 0x1

    #@382
    :goto_382
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@385
    .line 421
    const/4 v0, 0x1

    #@386
    goto/16 :goto_7

    #@388
    .line 417
    .end local v2           #_arg1:Z
    .end local v8           #_result:Z
    :cond_388
    const/4 v2, 0x0

    #@389
    goto :goto_378

    #@38a
    .line 420
    .restart local v2       #_arg1:Z
    .restart local v8       #_result:Z
    :cond_38a
    const/4 v0, 0x0

    #@38b
    goto :goto_382

    #@38c
    .line 425
    .end local v1           #_arg0:Ljava/lang/String;
    .end local v2           #_arg1:Z
    .end local v8           #_result:Z
    :sswitch_38c
    const-string v0, "com.android.internal.view.IInputMethodManager"

    #@38e
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@391
    .line 427
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@394
    move-result-object v1

    #@395
    .line 429
    .restart local v1       #_arg0:Ljava/lang/String;
    sget-object v0, Landroid/view/inputmethod/InputMethodSubtype;->CREATOR:Landroid/os/Parcelable$Creator;

    #@397
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    #@39a
    move-result-object v2

    #@39b
    check-cast v2, [Landroid/view/inputmethod/InputMethodSubtype;

    #@39d
    .line 430
    .local v2, _arg1:[Landroid/view/inputmethod/InputMethodSubtype;
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/view/IInputMethodManager$Stub;->setAdditionalInputMethodSubtypes(Ljava/lang/String;[Landroid/view/inputmethod/InputMethodSubtype;)V

    #@3a0
    .line 431
    const/4 v0, 0x1

    #@3a1
    goto/16 :goto_7

    #@3a3
    .line 42
    nop

    #@3a4
    :sswitch_data_3a4
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_20
        0x3 -> :sswitch_31
        0x4 -> :sswitch_5b
        0x5 -> :sswitch_78
        0x6 -> :sswitch_8a
        0x7 -> :sswitch_b0
        0x8 -> :sswitch_c6
        0x9 -> :sswitch_108
        0xa -> :sswitch_11e
        0xb -> :sswitch_151
        0xc -> :sswitch_184
        0xd -> :sswitch_1d3
        0xe -> :sswitch_1e9
        0xf -> :sswitch_203
        0x10 -> :sswitch_219
        0x11 -> :sswitch_23f
        0x12 -> :sswitch_255
        0x13 -> :sswitch_26b
        0x14 -> :sswitch_285
        0x15 -> :sswitch_29f
        0x16 -> :sswitch_2b5
        0x17 -> :sswitch_2e4
        0x18 -> :sswitch_302
        0x19 -> :sswitch_329
        0x1a -> :sswitch_344
        0x1b -> :sswitch_368
        0x1c -> :sswitch_38c
        0x5f4e5446 -> :sswitch_8
    .end sparse-switch
.end method
