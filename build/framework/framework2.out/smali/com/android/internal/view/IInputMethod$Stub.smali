.class public abstract Lcom/android/internal/view/IInputMethod$Stub;
.super Landroid/os/Binder;
.source "IInputMethod.java"

# interfaces
.implements Lcom/android/internal/view/IInputMethod;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/view/IInputMethod;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/view/IInputMethod$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.android.internal.view.IInputMethod"

.field static final TRANSACTION_attachToken:I = 0x1

.field static final TRANSACTION_bindInput:I = 0x2

.field static final TRANSACTION_changeInputMethodSubtype:I = 0xb

.field static final TRANSACTION_createSession:I = 0x6

.field static final TRANSACTION_hideSoftInput:I = 0xa

.field static final TRANSACTION_restartInput:I = 0x5

.field static final TRANSACTION_revokeSession:I = 0x8

.field static final TRANSACTION_setSessionEnabled:I = 0x7

.field static final TRANSACTION_showSoftInput:I = 0x9

.field static final TRANSACTION_startInput:I = 0x4

.field static final TRANSACTION_unbindInput:I = 0x3


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 19
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 20
    const-string v0, "com.android.internal.view.IInputMethod"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/android/internal/view/IInputMethod$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 21
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/android/internal/view/IInputMethod;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 28
    if-nez p0, :cond_4

    #@2
    .line 29
    const/4 v0, 0x0

    #@3
    .line 35
    :goto_3
    return-object v0

    #@4
    .line 31
    :cond_4
    const-string v1, "com.android.internal.view.IInputMethod"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 32
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/android/internal/view/IInputMethod;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 33
    check-cast v0, Lcom/android/internal/view/IInputMethod;

    #@12
    goto :goto_3

    #@13
    .line 35
    :cond_13
    new-instance v0, Lcom/android/internal/view/IInputMethod$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/android/internal/view/IInputMethod$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 39
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 9
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 43
    sparse-switch p1, :sswitch_data_116

    #@4
    .line 177
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v2

    #@8
    :goto_8
    return v2

    #@9
    .line 47
    :sswitch_9
    const-string v3, "com.android.internal.view.IInputMethod"

    #@b
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    goto :goto_8

    #@f
    .line 52
    :sswitch_f
    const-string v3, "com.android.internal.view.IInputMethod"

    #@11
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 54
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@17
    move-result-object v0

    #@18
    .line 55
    .local v0, _arg0:Landroid/os/IBinder;
    invoke-virtual {p0, v0}, Lcom/android/internal/view/IInputMethod$Stub;->attachToken(Landroid/os/IBinder;)V

    #@1b
    goto :goto_8

    #@1c
    .line 60
    .end local v0           #_arg0:Landroid/os/IBinder;
    :sswitch_1c
    const-string v3, "com.android.internal.view.IInputMethod"

    #@1e
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@21
    .line 62
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@24
    move-result v3

    #@25
    if-eqz v3, :cond_33

    #@27
    .line 63
    sget-object v3, Landroid/view/inputmethod/InputBinding;->CREATOR:Landroid/os/Parcelable$Creator;

    #@29
    invoke-interface {v3, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@2c
    move-result-object v0

    #@2d
    check-cast v0, Landroid/view/inputmethod/InputBinding;

    #@2f
    .line 68
    .local v0, _arg0:Landroid/view/inputmethod/InputBinding;
    :goto_2f
    invoke-virtual {p0, v0}, Lcom/android/internal/view/IInputMethod$Stub;->bindInput(Landroid/view/inputmethod/InputBinding;)V

    #@32
    goto :goto_8

    #@33
    .line 66
    .end local v0           #_arg0:Landroid/view/inputmethod/InputBinding;
    :cond_33
    const/4 v0, 0x0

    #@34
    .restart local v0       #_arg0:Landroid/view/inputmethod/InputBinding;
    goto :goto_2f

    #@35
    .line 73
    .end local v0           #_arg0:Landroid/view/inputmethod/InputBinding;
    :sswitch_35
    const-string v3, "com.android.internal.view.IInputMethod"

    #@37
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3a
    .line 74
    invoke-virtual {p0}, Lcom/android/internal/view/IInputMethod$Stub;->unbindInput()V

    #@3d
    goto :goto_8

    #@3e
    .line 79
    :sswitch_3e
    const-string v3, "com.android.internal.view.IInputMethod"

    #@40
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@43
    .line 81
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@46
    move-result-object v3

    #@47
    invoke-static {v3}, Lcom/android/internal/view/IInputContext$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/view/IInputContext;

    #@4a
    move-result-object v0

    #@4b
    .line 83
    .local v0, _arg0:Lcom/android/internal/view/IInputContext;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@4e
    move-result v3

    #@4f
    if-eqz v3, :cond_5d

    #@51
    .line 84
    sget-object v3, Landroid/view/inputmethod/EditorInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@53
    invoke-interface {v3, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@56
    move-result-object v1

    #@57
    check-cast v1, Landroid/view/inputmethod/EditorInfo;

    #@59
    .line 89
    .local v1, _arg1:Landroid/view/inputmethod/EditorInfo;
    :goto_59
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/view/IInputMethod$Stub;->startInput(Lcom/android/internal/view/IInputContext;Landroid/view/inputmethod/EditorInfo;)V

    #@5c
    goto :goto_8

    #@5d
    .line 87
    .end local v1           #_arg1:Landroid/view/inputmethod/EditorInfo;
    :cond_5d
    const/4 v1, 0x0

    #@5e
    .restart local v1       #_arg1:Landroid/view/inputmethod/EditorInfo;
    goto :goto_59

    #@5f
    .line 94
    .end local v0           #_arg0:Lcom/android/internal/view/IInputContext;
    .end local v1           #_arg1:Landroid/view/inputmethod/EditorInfo;
    :sswitch_5f
    const-string v3, "com.android.internal.view.IInputMethod"

    #@61
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@64
    .line 96
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@67
    move-result-object v3

    #@68
    invoke-static {v3}, Lcom/android/internal/view/IInputContext$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/view/IInputContext;

    #@6b
    move-result-object v0

    #@6c
    .line 98
    .restart local v0       #_arg0:Lcom/android/internal/view/IInputContext;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@6f
    move-result v3

    #@70
    if-eqz v3, :cond_7e

    #@72
    .line 99
    sget-object v3, Landroid/view/inputmethod/EditorInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@74
    invoke-interface {v3, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@77
    move-result-object v1

    #@78
    check-cast v1, Landroid/view/inputmethod/EditorInfo;

    #@7a
    .line 104
    .restart local v1       #_arg1:Landroid/view/inputmethod/EditorInfo;
    :goto_7a
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/view/IInputMethod$Stub;->restartInput(Lcom/android/internal/view/IInputContext;Landroid/view/inputmethod/EditorInfo;)V

    #@7d
    goto :goto_8

    #@7e
    .line 102
    .end local v1           #_arg1:Landroid/view/inputmethod/EditorInfo;
    :cond_7e
    const/4 v1, 0x0

    #@7f
    .restart local v1       #_arg1:Landroid/view/inputmethod/EditorInfo;
    goto :goto_7a

    #@80
    .line 109
    .end local v0           #_arg0:Lcom/android/internal/view/IInputContext;
    .end local v1           #_arg1:Landroid/view/inputmethod/EditorInfo;
    :sswitch_80
    const-string v3, "com.android.internal.view.IInputMethod"

    #@82
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@85
    .line 111
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@88
    move-result-object v3

    #@89
    invoke-static {v3}, Lcom/android/internal/view/IInputMethodCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/view/IInputMethodCallback;

    #@8c
    move-result-object v0

    #@8d
    .line 112
    .local v0, _arg0:Lcom/android/internal/view/IInputMethodCallback;
    invoke-virtual {p0, v0}, Lcom/android/internal/view/IInputMethod$Stub;->createSession(Lcom/android/internal/view/IInputMethodCallback;)V

    #@90
    goto/16 :goto_8

    #@92
    .line 117
    .end local v0           #_arg0:Lcom/android/internal/view/IInputMethodCallback;
    :sswitch_92
    const-string v3, "com.android.internal.view.IInputMethod"

    #@94
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@97
    .line 119
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@9a
    move-result-object v3

    #@9b
    invoke-static {v3}, Lcom/android/internal/view/IInputMethodSession$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/view/IInputMethodSession;

    #@9e
    move-result-object v0

    #@9f
    .line 121
    .local v0, _arg0:Lcom/android/internal/view/IInputMethodSession;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@a2
    move-result v3

    #@a3
    if-eqz v3, :cond_ab

    #@a5
    move v1, v2

    #@a6
    .line 122
    .local v1, _arg1:Z
    :goto_a6
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/view/IInputMethod$Stub;->setSessionEnabled(Lcom/android/internal/view/IInputMethodSession;Z)V

    #@a9
    goto/16 :goto_8

    #@ab
    .line 121
    .end local v1           #_arg1:Z
    :cond_ab
    const/4 v1, 0x0

    #@ac
    goto :goto_a6

    #@ad
    .line 127
    .end local v0           #_arg0:Lcom/android/internal/view/IInputMethodSession;
    :sswitch_ad
    const-string v3, "com.android.internal.view.IInputMethod"

    #@af
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@b2
    .line 129
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@b5
    move-result-object v3

    #@b6
    invoke-static {v3}, Lcom/android/internal/view/IInputMethodSession$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/view/IInputMethodSession;

    #@b9
    move-result-object v0

    #@ba
    .line 130
    .restart local v0       #_arg0:Lcom/android/internal/view/IInputMethodSession;
    invoke-virtual {p0, v0}, Lcom/android/internal/view/IInputMethod$Stub;->revokeSession(Lcom/android/internal/view/IInputMethodSession;)V

    #@bd
    goto/16 :goto_8

    #@bf
    .line 135
    .end local v0           #_arg0:Lcom/android/internal/view/IInputMethodSession;
    :sswitch_bf
    const-string v3, "com.android.internal.view.IInputMethod"

    #@c1
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@c4
    .line 137
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@c7
    move-result v0

    #@c8
    .line 139
    .local v0, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@cb
    move-result v3

    #@cc
    if-eqz v3, :cond_db

    #@ce
    .line 140
    sget-object v3, Landroid/os/ResultReceiver;->CREATOR:Landroid/os/Parcelable$Creator;

    #@d0
    invoke-interface {v3, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@d3
    move-result-object v1

    #@d4
    check-cast v1, Landroid/os/ResultReceiver;

    #@d6
    .line 145
    .local v1, _arg1:Landroid/os/ResultReceiver;
    :goto_d6
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/view/IInputMethod$Stub;->showSoftInput(ILandroid/os/ResultReceiver;)V

    #@d9
    goto/16 :goto_8

    #@db
    .line 143
    .end local v1           #_arg1:Landroid/os/ResultReceiver;
    :cond_db
    const/4 v1, 0x0

    #@dc
    .restart local v1       #_arg1:Landroid/os/ResultReceiver;
    goto :goto_d6

    #@dd
    .line 150
    .end local v0           #_arg0:I
    .end local v1           #_arg1:Landroid/os/ResultReceiver;
    :sswitch_dd
    const-string v3, "com.android.internal.view.IInputMethod"

    #@df
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@e2
    .line 152
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@e5
    move-result v0

    #@e6
    .line 154
    .restart local v0       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@e9
    move-result v3

    #@ea
    if-eqz v3, :cond_f9

    #@ec
    .line 155
    sget-object v3, Landroid/os/ResultReceiver;->CREATOR:Landroid/os/Parcelable$Creator;

    #@ee
    invoke-interface {v3, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@f1
    move-result-object v1

    #@f2
    check-cast v1, Landroid/os/ResultReceiver;

    #@f4
    .line 160
    .restart local v1       #_arg1:Landroid/os/ResultReceiver;
    :goto_f4
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/view/IInputMethod$Stub;->hideSoftInput(ILandroid/os/ResultReceiver;)V

    #@f7
    goto/16 :goto_8

    #@f9
    .line 158
    .end local v1           #_arg1:Landroid/os/ResultReceiver;
    :cond_f9
    const/4 v1, 0x0

    #@fa
    .restart local v1       #_arg1:Landroid/os/ResultReceiver;
    goto :goto_f4

    #@fb
    .line 165
    .end local v0           #_arg0:I
    .end local v1           #_arg1:Landroid/os/ResultReceiver;
    :sswitch_fb
    const-string v3, "com.android.internal.view.IInputMethod"

    #@fd
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@100
    .line 167
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@103
    move-result v3

    #@104
    if-eqz v3, :cond_113

    #@106
    .line 168
    sget-object v3, Landroid/view/inputmethod/InputMethodSubtype;->CREATOR:Landroid/os/Parcelable$Creator;

    #@108
    invoke-interface {v3, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@10b
    move-result-object v0

    #@10c
    check-cast v0, Landroid/view/inputmethod/InputMethodSubtype;

    #@10e
    .line 173
    .local v0, _arg0:Landroid/view/inputmethod/InputMethodSubtype;
    :goto_10e
    invoke-virtual {p0, v0}, Lcom/android/internal/view/IInputMethod$Stub;->changeInputMethodSubtype(Landroid/view/inputmethod/InputMethodSubtype;)V

    #@111
    goto/16 :goto_8

    #@113
    .line 171
    .end local v0           #_arg0:Landroid/view/inputmethod/InputMethodSubtype;
    :cond_113
    const/4 v0, 0x0

    #@114
    .restart local v0       #_arg0:Landroid/view/inputmethod/InputMethodSubtype;
    goto :goto_10e

    #@115
    .line 43
    nop

    #@116
    :sswitch_data_116
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_1c
        0x3 -> :sswitch_35
        0x4 -> :sswitch_3e
        0x5 -> :sswitch_5f
        0x6 -> :sswitch_80
        0x7 -> :sswitch_92
        0x8 -> :sswitch_ad
        0x9 -> :sswitch_bf
        0xa -> :sswitch_dd
        0xb -> :sswitch_fb
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
