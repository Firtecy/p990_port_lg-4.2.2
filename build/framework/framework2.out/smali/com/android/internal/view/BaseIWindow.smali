.class public Lcom/android/internal/view/BaseIWindow;
.super Landroid/view/IWindow$Stub;
.source "BaseIWindow.java"


# instance fields
.field public mSeq:I

.field private mSession:Landroid/view/IWindowSession;


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 28
    invoke-direct {p0}, Landroid/view/IWindow$Stub;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public closeSystemDialogs(Ljava/lang/String;)V
    .registers 2
    .parameter "reason"

    #@0
    .prologue
    .line 73
    return-void
.end method

.method public dispatchAppVisibility(Z)V
    .registers 2
    .parameter "visible"

    #@0
    .prologue
    .line 53
    return-void
.end method

.method public dispatchDragEvent(Landroid/view/DragEvent;)V
    .registers 2
    .parameter "event"

    #@0
    .prologue
    .line 87
    return-void
.end method

.method public dispatchGetNewSurface()V
    .registers 1

    #@0
    .prologue
    .line 57
    return-void
.end method

.method public dispatchScreenState(Z)V
    .registers 2
    .parameter "on"

    #@0
    .prologue
    .line 61
    return-void
.end method

.method public dispatchSystemUiVisibilityChanged(IIII)V
    .registers 5
    .parameter "seq"
    .parameter "globalUi"
    .parameter "localValue"
    .parameter "localChanges"

    #@0
    .prologue
    .line 92
    iput p1, p0, Lcom/android/internal/view/BaseIWindow;->mSeq:I

    #@2
    .line 93
    return-void
.end method

.method public dispatchWallpaperCommand(Ljava/lang/String;IIILandroid/os/Bundle;Z)V
    .registers 10
    .parameter "action"
    .parameter "x"
    .parameter "y"
    .parameter "z"
    .parameter "extras"
    .parameter "sync"

    #@0
    .prologue
    .line 98
    if-eqz p6, :cond_c

    #@2
    .line 100
    :try_start_2
    iget-object v0, p0, Lcom/android/internal/view/BaseIWindow;->mSession:Landroid/view/IWindowSession;

    #@4
    invoke-virtual {p0}, Lcom/android/internal/view/BaseIWindow;->asBinder()Landroid/os/IBinder;

    #@7
    move-result-object v1

    #@8
    const/4 v2, 0x0

    #@9
    invoke-interface {v0, v1, v2}, Landroid/view/IWindowSession;->wallpaperCommandComplete(Landroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_c
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_c} :catch_d

    #@c
    .line 104
    :cond_c
    :goto_c
    return-void

    #@d
    .line 101
    :catch_d
    move-exception v0

    #@e
    goto :goto_c
.end method

.method public dispatchWallpaperOffsets(FFFFZ)V
    .registers 8
    .parameter "x"
    .parameter "y"
    .parameter "xStep"
    .parameter "yStep"
    .parameter "sync"

    #@0
    .prologue
    .line 77
    if-eqz p5, :cond_b

    #@2
    .line 79
    :try_start_2
    iget-object v0, p0, Lcom/android/internal/view/BaseIWindow;->mSession:Landroid/view/IWindowSession;

    #@4
    invoke-virtual {p0}, Lcom/android/internal/view/BaseIWindow;->asBinder()Landroid/os/IBinder;

    #@7
    move-result-object v1

    #@8
    invoke-interface {v0, v1}, Landroid/view/IWindowSession;->wallpaperOffsetsComplete(Landroid/os/IBinder;)V
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_b} :catch_c

    #@b
    .line 83
    :cond_b
    :goto_b
    return-void

    #@c
    .line 80
    :catch_c
    move-exception v0

    #@d
    goto :goto_b
.end method

.method public doneAnimating()V
    .registers 1

    #@0
    .prologue
    .line 108
    return-void
.end method

.method public executeCommand(Ljava/lang/String;Ljava/lang/String;Landroid/os/ParcelFileDescriptor;)V
    .registers 4
    .parameter "command"
    .parameter "parameters"
    .parameter "out"

    #@0
    .prologue
    .line 69
    return-void
.end method

.method public moved(II)V
    .registers 3
    .parameter "newX"
    .parameter "newY"

    #@0
    .prologue
    .line 49
    return-void
.end method

.method public resized(Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;ZLandroid/content/res/Configuration;)V
    .registers 7
    .parameter "frame"
    .parameter "contentInsets"
    .parameter "visibleInsets"
    .parameter "reportDraw"
    .parameter "newConfig"

    #@0
    .prologue
    .line 39
    if-eqz p4, :cond_7

    #@2
    .line 41
    :try_start_2
    iget-object v0, p0, Lcom/android/internal/view/BaseIWindow;->mSession:Landroid/view/IWindowSession;

    #@4
    invoke-interface {v0, p0}, Landroid/view/IWindowSession;->finishDrawing(Landroid/view/IWindow;)V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_7} :catch_8

    #@7
    .line 45
    :cond_7
    :goto_7
    return-void

    #@8
    .line 42
    :catch_8
    move-exception v0

    #@9
    goto :goto_7
.end method

.method public setSession(Landroid/view/IWindowSession;)V
    .registers 2
    .parameter "session"

    #@0
    .prologue
    .line 33
    iput-object p1, p0, Lcom/android/internal/view/BaseIWindow;->mSession:Landroid/view/IWindowSession;

    #@2
    .line 34
    return-void
.end method

.method public windowFocusChanged(ZZ)V
    .registers 3
    .parameter "hasFocus"
    .parameter "touchEnabled"

    #@0
    .prologue
    .line 65
    return-void
.end method
