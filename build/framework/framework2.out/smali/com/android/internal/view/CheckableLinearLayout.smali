.class public Lcom/android/internal/view/CheckableLinearLayout;
.super Landroid/widget/LinearLayout;
.source "CheckableLinearLayout.java"

# interfaces
.implements Landroid/widget/Checkable;


# instance fields
.field private mCheckBox:Landroid/widget/CheckBox;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 31
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    #@3
    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@3
    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@3
    .line 43
    return-void
.end method


# virtual methods
.method public isChecked()Z
    .registers 2

    #@0
    .prologue
    .line 58
    iget-object v0, p0, Lcom/android/internal/view/CheckableLinearLayout;->mCheckBox:Landroid/widget/CheckBox;

    #@2
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method protected onFinishInflate()V
    .registers 2

    #@0
    .prologue
    .line 47
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    #@3
    .line 48
    const v0, 0x102032e

    #@6
    invoke-virtual {p0, v0}, Lcom/android/internal/view/CheckableLinearLayout;->findViewById(I)Landroid/view/View;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Landroid/widget/CheckBox;

    #@c
    iput-object v0, p0, Lcom/android/internal/view/CheckableLinearLayout;->mCheckBox:Landroid/widget/CheckBox;

    #@e
    .line 49
    return-void
.end method

.method public setChecked(Z)V
    .registers 3
    .parameter "checked"

    #@0
    .prologue
    .line 53
    iget-object v0, p0, Lcom/android/internal/view/CheckableLinearLayout;->mCheckBox:Landroid/widget/CheckBox;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    #@5
    .line 54
    return-void
.end method

.method public toggle()V
    .registers 2

    #@0
    .prologue
    .line 63
    iget-object v0, p0, Lcom/android/internal/view/CheckableLinearLayout;->mCheckBox:Landroid/widget/CheckBox;

    #@2
    invoke-virtual {v0}, Landroid/widget/CheckBox;->toggle()V

    #@5
    .line 64
    return-void
.end method
