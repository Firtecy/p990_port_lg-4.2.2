.class Lcom/android/internal/view/IInputMethodSession$Stub$Proxy;
.super Ljava/lang/Object;
.source "IInputMethodSession.java"

# interfaces
.implements Lcom/android/internal/view/IInputMethodSession;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/view/IInputMethodSession$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 206
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 207
    iput-object p1, p0, Lcom/android/internal/view/IInputMethodSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 208
    return-void
.end method


# virtual methods
.method public appPrivateCommand(Ljava/lang/String;Landroid/os/Bundle;)V
    .registers 8
    .parameter "action"
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 368
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 370
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "com.android.internal.view.IInputMethodSession"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 371
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@c
    .line 372
    if-eqz p2, :cond_23

    #@e
    .line 373
    const/4 v1, 0x1

    #@f
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@12
    .line 374
    const/4 v1, 0x0

    #@13
    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    #@16
    .line 379
    :goto_16
    iget-object v1, p0, Lcom/android/internal/view/IInputMethodSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/16 v2, 0xa

    #@1a
    const/4 v3, 0x0

    #@1b
    const/4 v4, 0x1

    #@1c
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_1f
    .catchall {:try_start_4 .. :try_end_1f} :catchall_28

    #@1f
    .line 382
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 384
    return-void

    #@23
    .line 377
    :cond_23
    const/4 v1, 0x0

    #@24
    :try_start_24
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_27
    .catchall {:try_start_24 .. :try_end_27} :catchall_28

    #@27
    goto :goto_16

    #@28
    .line 382
    :catchall_28
    move-exception v1

    #@29
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    throw v1
.end method

.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 211
    iget-object v0, p0, Lcom/android/internal/view/IInputMethodSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public dispatchGenericMotionEvent(ILandroid/view/MotionEvent;Lcom/android/internal/view/IInputMethodCallback;)V
    .registers 9
    .parameter "seq"
    .parameter "event"
    .parameter "callback"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 348
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 350
    .local v0, _data:Landroid/os/Parcel;
    :try_start_5
    const-string v2, "com.android.internal.view.IInputMethodSession"

    #@7
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@a
    .line 351
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@d
    .line 352
    if-eqz p2, :cond_2d

    #@f
    .line 353
    const/4 v2, 0x1

    #@10
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 354
    const/4 v2, 0x0

    #@14
    invoke-virtual {p2, v0, v2}, Landroid/view/MotionEvent;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 359
    :goto_17
    if-eqz p3, :cond_1d

    #@19
    invoke-interface {p3}, Lcom/android/internal/view/IInputMethodCallback;->asBinder()Landroid/os/IBinder;

    #@1c
    move-result-object v1

    #@1d
    :cond_1d
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@20
    .line 360
    iget-object v1, p0, Lcom/android/internal/view/IInputMethodSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@22
    const/16 v2, 0x9

    #@24
    const/4 v3, 0x0

    #@25
    const/4 v4, 0x1

    #@26
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_29
    .catchall {:try_start_5 .. :try_end_29} :catchall_32

    #@29
    .line 363
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 365
    return-void

    #@2d
    .line 357
    :cond_2d
    const/4 v2, 0x0

    #@2e
    :try_start_2e
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_31
    .catchall {:try_start_2e .. :try_end_31} :catchall_32

    #@31
    goto :goto_17

    #@32
    .line 363
    :catchall_32
    move-exception v1

    #@33
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@36
    throw v1
.end method

.method public dispatchKeyEvent(ILandroid/view/KeyEvent;Lcom/android/internal/view/IInputMethodCallback;)V
    .registers 9
    .parameter "seq"
    .parameter "event"
    .parameter "callback"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 308
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 310
    .local v0, _data:Landroid/os/Parcel;
    :try_start_5
    const-string v2, "com.android.internal.view.IInputMethodSession"

    #@7
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@a
    .line 311
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@d
    .line 312
    if-eqz p2, :cond_2c

    #@f
    .line 313
    const/4 v2, 0x1

    #@10
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 314
    const/4 v2, 0x0

    #@14
    invoke-virtual {p2, v0, v2}, Landroid/view/KeyEvent;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 319
    :goto_17
    if-eqz p3, :cond_1d

    #@19
    invoke-interface {p3}, Lcom/android/internal/view/IInputMethodCallback;->asBinder()Landroid/os/IBinder;

    #@1c
    move-result-object v1

    #@1d
    :cond_1d
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@20
    .line 320
    iget-object v1, p0, Lcom/android/internal/view/IInputMethodSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@22
    const/4 v2, 0x7

    #@23
    const/4 v3, 0x0

    #@24
    const/4 v4, 0x1

    #@25
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_28
    .catchall {:try_start_5 .. :try_end_28} :catchall_31

    #@28
    .line 323
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 325
    return-void

    #@2c
    .line 317
    :cond_2c
    const/4 v2, 0x0

    #@2d
    :try_start_2d
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_30
    .catchall {:try_start_2d .. :try_end_30} :catchall_31

    #@30
    goto :goto_17

    #@31
    .line 323
    :catchall_31
    move-exception v1

    #@32
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@35
    throw v1
.end method

.method public dispatchTrackballEvent(ILandroid/view/MotionEvent;Lcom/android/internal/view/IInputMethodCallback;)V
    .registers 9
    .parameter "seq"
    .parameter "event"
    .parameter "callback"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 328
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 330
    .local v0, _data:Landroid/os/Parcel;
    :try_start_5
    const-string v2, "com.android.internal.view.IInputMethodSession"

    #@7
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@a
    .line 331
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@d
    .line 332
    if-eqz p2, :cond_2d

    #@f
    .line 333
    const/4 v2, 0x1

    #@10
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 334
    const/4 v2, 0x0

    #@14
    invoke-virtual {p2, v0, v2}, Landroid/view/MotionEvent;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 339
    :goto_17
    if-eqz p3, :cond_1d

    #@19
    invoke-interface {p3}, Lcom/android/internal/view/IInputMethodCallback;->asBinder()Landroid/os/IBinder;

    #@1c
    move-result-object v1

    #@1d
    :cond_1d
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@20
    .line 340
    iget-object v1, p0, Lcom/android/internal/view/IInputMethodSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@22
    const/16 v2, 0x8

    #@24
    const/4 v3, 0x0

    #@25
    const/4 v4, 0x1

    #@26
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_29
    .catchall {:try_start_5 .. :try_end_29} :catchall_32

    #@29
    .line 343
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 345
    return-void

    #@2d
    .line 337
    :cond_2d
    const/4 v2, 0x0

    #@2e
    :try_start_2e
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_31
    .catchall {:try_start_2e .. :try_end_31} :catchall_32

    #@31
    goto :goto_17

    #@32
    .line 343
    :catchall_32
    move-exception v1

    #@33
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@36
    throw v1
.end method

.method public displayCompletions([Landroid/view/inputmethod/CompletionInfo;)V
    .registers 7
    .parameter "completions"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 296
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 298
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "com.android.internal.view.IInputMethodSession"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 299
    const/4 v1, 0x0

    #@a
    invoke-virtual {v0, p1, v1}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    #@d
    .line 300
    iget-object v1, p0, Lcom/android/internal/view/IInputMethodSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v2, 0x6

    #@10
    const/4 v3, 0x0

    #@11
    const/4 v4, 0x1

    #@12
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_15
    .catchall {:try_start_4 .. :try_end_15} :catchall_19

    #@15
    .line 303
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@18
    .line 305
    return-void

    #@19
    .line 303
    :catchall_19
    move-exception v1

    #@1a
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1d
    throw v1
.end method

.method public finishInput()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 219
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 221
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "com.android.internal.view.IInputMethodSession"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 222
    iget-object v1, p0, Lcom/android/internal/view/IInputMethodSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@b
    const/4 v2, 0x1

    #@c
    const/4 v3, 0x0

    #@d
    const/4 v4, 0x1

    #@e
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_11
    .catchall {:try_start_4 .. :try_end_11} :catchall_15

    #@11
    .line 225
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@14
    .line 227
    return-void

    #@15
    .line 225
    :catchall_15
    move-exception v1

    #@16
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@19
    throw v1
.end method

.method public finishSession()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 400
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 402
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "com.android.internal.view.IInputMethodSession"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 403
    iget-object v1, p0, Lcom/android/internal/view/IInputMethodSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@b
    const/16 v2, 0xc

    #@d
    const/4 v3, 0x0

    #@e
    const/4 v4, 0x1

    #@f
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_12
    .catchall {:try_start_4 .. :try_end_12} :catchall_16

    #@12
    .line 406
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@15
    .line 408
    return-void

    #@16
    .line 406
    :catchall_16
    move-exception v1

    #@17
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1a
    throw v1
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 215
    const-string v0, "com.android.internal.view.IInputMethodSession"

    #@2
    return-object v0
.end method

.method public toggleSoftInput(II)V
    .registers 8
    .parameter "showFlags"
    .parameter "hideFlags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 387
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 389
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "com.android.internal.view.IInputMethodSession"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 390
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@c
    .line 391
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 392
    iget-object v1, p0, Lcom/android/internal/view/IInputMethodSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@11
    const/16 v2, 0xb

    #@13
    const/4 v3, 0x0

    #@14
    const/4 v4, 0x1

    #@15
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_18
    .catchall {:try_start_4 .. :try_end_18} :catchall_1c

    #@18
    .line 395
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1b
    .line 397
    return-void

    #@1c
    .line 395
    :catchall_1c
    move-exception v1

    #@1d
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@20
    throw v1
.end method

.method public updateCursor(Landroid/graphics/Rect;)V
    .registers 7
    .parameter "newCursor"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 278
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 280
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "com.android.internal.view.IInputMethodSession"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 281
    if-eqz p1, :cond_1f

    #@b
    .line 282
    const/4 v1, 0x1

    #@c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 283
    const/4 v1, 0x0

    #@10
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Rect;->writeToParcel(Landroid/os/Parcel;I)V

    #@13
    .line 288
    :goto_13
    iget-object v1, p0, Lcom/android/internal/view/IInputMethodSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/4 v2, 0x5

    #@16
    const/4 v3, 0x0

    #@17
    const/4 v4, 0x1

    #@18
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_1b
    .catchall {:try_start_4 .. :try_end_1b} :catchall_24

    #@1b
    .line 291
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 293
    return-void

    #@1f
    .line 286
    :cond_1f
    const/4 v1, 0x0

    #@20
    :try_start_20
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_23
    .catchall {:try_start_20 .. :try_end_23} :catchall_24

    #@23
    goto :goto_13

    #@24
    .line 291
    :catchall_24
    move-exception v1

    #@25
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    throw v1
.end method

.method public updateExtractedText(ILandroid/view/inputmethod/ExtractedText;)V
    .registers 8
    .parameter "token"
    .parameter "text"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 230
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 232
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "com.android.internal.view.IInputMethodSession"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 233
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@c
    .line 234
    if-eqz p2, :cond_22

    #@e
    .line 235
    const/4 v1, 0x1

    #@f
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@12
    .line 236
    const/4 v1, 0x0

    #@13
    invoke-virtual {p2, v0, v1}, Landroid/view/inputmethod/ExtractedText;->writeToParcel(Landroid/os/Parcel;I)V

    #@16
    .line 241
    :goto_16
    iget-object v1, p0, Lcom/android/internal/view/IInputMethodSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/4 v2, 0x2

    #@19
    const/4 v3, 0x0

    #@1a
    const/4 v4, 0x1

    #@1b
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_1e
    .catchall {:try_start_4 .. :try_end_1e} :catchall_27

    #@1e
    .line 244
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 246
    return-void

    #@22
    .line 239
    :cond_22
    const/4 v1, 0x0

    #@23
    :try_start_23
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_26
    .catchall {:try_start_23 .. :try_end_26} :catchall_27

    #@26
    goto :goto_16

    #@27
    .line 244
    :catchall_27
    move-exception v1

    #@28
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    throw v1
.end method

.method public updateSelection(IIIIII)V
    .registers 12
    .parameter "oldSelStart"
    .parameter "oldSelEnd"
    .parameter "newSelStart"
    .parameter "newSelEnd"
    .parameter "candidatesStart"
    .parameter "candidatesEnd"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 249
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 251
    .local v0, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v1, "com.android.internal.view.IInputMethodSession"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 252
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@c
    .line 253
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 254
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@12
    .line 255
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 256
    invoke-virtual {v0, p5}, Landroid/os/Parcel;->writeInt(I)V

    #@18
    .line 257
    invoke-virtual {v0, p6}, Landroid/os/Parcel;->writeInt(I)V

    #@1b
    .line 258
    iget-object v1, p0, Lcom/android/internal/view/IInputMethodSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1d
    const/4 v2, 0x3

    #@1e
    const/4 v3, 0x0

    #@1f
    const/4 v4, 0x1

    #@20
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_23
    .catchall {:try_start_4 .. :try_end_23} :catchall_27

    #@23
    .line 261
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 263
    return-void

    #@27
    .line 261
    :catchall_27
    move-exception v1

    #@28
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    throw v1
.end method

.method public viewClicked(Z)V
    .registers 7
    .parameter "focusChanged"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 266
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 268
    .local v0, _data:Landroid/os/Parcel;
    :try_start_5
    const-string v2, "com.android.internal.view.IInputMethodSession"

    #@7
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@a
    .line 269
    if-eqz p1, :cond_1b

    #@c
    :goto_c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 270
    iget-object v1, p0, Lcom/android/internal/view/IInputMethodSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@11
    const/4 v2, 0x4

    #@12
    const/4 v3, 0x0

    #@13
    const/4 v4, 0x1

    #@14
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_17
    .catchall {:try_start_5 .. :try_end_17} :catchall_1d

    #@17
    .line 273
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1a
    .line 275
    return-void

    #@1b
    .line 269
    :cond_1b
    const/4 v1, 0x0

    #@1c
    goto :goto_c

    #@1d
    .line 273
    :catchall_1d
    move-exception v1

    #@1e
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    throw v1
.end method
