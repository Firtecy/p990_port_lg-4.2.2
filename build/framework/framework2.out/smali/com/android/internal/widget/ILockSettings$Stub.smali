.class public abstract Lcom/android/internal/widget/ILockSettings$Stub;
.super Landroid/os/Binder;
.source "ILockSettings.java"

# interfaces
.implements Lcom/android/internal/widget/ILockSettings;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/widget/ILockSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/widget/ILockSettings$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.android.internal.widget.ILockSettings"

.field static final TRANSACTION_checkPassword:I = 0xc

.field static final TRANSACTION_checkPattern:I = 0x9

.field static final TRANSACTION_checkPatternKidsMode:I = 0xa

.field static final TRANSACTION_getBoolean:I = 0x4

.field static final TRANSACTION_getLong:I = 0x5

.field static final TRANSACTION_getString:I = 0x6

.field static final TRANSACTION_havePassword:I = 0xf

.field static final TRANSACTION_havePattern:I = 0xd

.field static final TRANSACTION_havePatternKidsMode:I = 0xe

.field static final TRANSACTION_removeUser:I = 0x10

.field static final TRANSACTION_setBoolean:I = 0x1

.field static final TRANSACTION_setLockPassword:I = 0xb

.field static final TRANSACTION_setLockPattern:I = 0x7

.field static final TRANSACTION_setLockPatternKidsMode:I = 0x8

.field static final TRANSACTION_setLong:I = 0x2

.field static final TRANSACTION_setString:I = 0x3


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 15
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 16
    const-string v0, "com.android.internal.widget.ILockSettings"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/android/internal/widget/ILockSettings$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 17
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/android/internal/widget/ILockSettings;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 24
    if-nez p0, :cond_4

    #@2
    .line 25
    const/4 v0, 0x0

    #@3
    .line 31
    :goto_3
    return-object v0

    #@4
    .line 27
    :cond_4
    const-string v1, "com.android.internal.widget.ILockSettings"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 28
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/android/internal/widget/ILockSettings;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 29
    check-cast v0, Lcom/android/internal/widget/ILockSettings;

    #@12
    goto :goto_3

    #@13
    .line 31
    :cond_13
    new-instance v0, Lcom/android/internal/widget/ILockSettings$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/android/internal/widget/ILockSettings$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 35
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 14
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v6, 0x1

    #@2
    .line 39
    sparse-switch p1, :sswitch_data_1a8

    #@5
    .line 236
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v6

    #@9
    :goto_9
    return v6

    #@a
    .line 43
    :sswitch_a
    const-string v7, "com.android.internal.widget.ILockSettings"

    #@c
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 48
    :sswitch_10
    const-string v8, "com.android.internal.widget.ILockSettings"

    #@12
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 50
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    .line 52
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1c
    move-result v8

    #@1d
    if-eqz v8, :cond_2b

    #@1f
    move v1, v6

    #@20
    .line 54
    .local v1, _arg1:Z
    :goto_20
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@23
    move-result v3

    #@24
    .line 55
    .local v3, _arg2:I
    invoke-virtual {p0, v0, v1, v3}, Lcom/android/internal/widget/ILockSettings$Stub;->setBoolean(Ljava/lang/String;ZI)V

    #@27
    .line 56
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2a
    goto :goto_9

    #@2b
    .end local v1           #_arg1:Z
    .end local v3           #_arg2:I
    :cond_2b
    move v1, v7

    #@2c
    .line 52
    goto :goto_20

    #@2d
    .line 61
    .end local v0           #_arg0:Ljava/lang/String;
    :sswitch_2d
    const-string v7, "com.android.internal.widget.ILockSettings"

    #@2f
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@32
    .line 63
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@35
    move-result-object v0

    #@36
    .line 65
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    #@39
    move-result-wide v1

    #@3a
    .line 67
    .local v1, _arg1:J
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@3d
    move-result v3

    #@3e
    .line 68
    .restart local v3       #_arg2:I
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/android/internal/widget/ILockSettings$Stub;->setLong(Ljava/lang/String;JI)V

    #@41
    .line 69
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@44
    goto :goto_9

    #@45
    .line 74
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v1           #_arg1:J
    .end local v3           #_arg2:I
    :sswitch_45
    const-string v7, "com.android.internal.widget.ILockSettings"

    #@47
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4a
    .line 76
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@4d
    move-result-object v0

    #@4e
    .line 78
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@51
    move-result-object v1

    #@52
    .line 80
    .local v1, _arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@55
    move-result v3

    #@56
    .line 81
    .restart local v3       #_arg2:I
    invoke-virtual {p0, v0, v1, v3}, Lcom/android/internal/widget/ILockSettings$Stub;->setString(Ljava/lang/String;Ljava/lang/String;I)V

    #@59
    .line 82
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@5c
    goto :goto_9

    #@5d
    .line 87
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v1           #_arg1:Ljava/lang/String;
    .end local v3           #_arg2:I
    :sswitch_5d
    const-string v8, "com.android.internal.widget.ILockSettings"

    #@5f
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@62
    .line 89
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@65
    move-result-object v0

    #@66
    .line 91
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@69
    move-result v8

    #@6a
    if-eqz v8, :cond_7f

    #@6c
    move v1, v6

    #@6d
    .line 93
    .local v1, _arg1:Z
    :goto_6d
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@70
    move-result v3

    #@71
    .line 94
    .restart local v3       #_arg2:I
    invoke-virtual {p0, v0, v1, v3}, Lcom/android/internal/widget/ILockSettings$Stub;->getBoolean(Ljava/lang/String;ZI)Z

    #@74
    move-result v4

    #@75
    .line 95
    .local v4, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@78
    .line 96
    if-eqz v4, :cond_7b

    #@7a
    move v7, v6

    #@7b
    :cond_7b
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@7e
    goto :goto_9

    #@7f
    .end local v1           #_arg1:Z
    .end local v3           #_arg2:I
    .end local v4           #_result:Z
    :cond_7f
    move v1, v7

    #@80
    .line 91
    goto :goto_6d

    #@81
    .line 101
    .end local v0           #_arg0:Ljava/lang/String;
    :sswitch_81
    const-string v7, "com.android.internal.widget.ILockSettings"

    #@83
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@86
    .line 103
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@89
    move-result-object v0

    #@8a
    .line 105
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    #@8d
    move-result-wide v1

    #@8e
    .line 107
    .local v1, _arg1:J
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@91
    move-result v3

    #@92
    .line 108
    .restart local v3       #_arg2:I
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/android/internal/widget/ILockSettings$Stub;->getLong(Ljava/lang/String;JI)J

    #@95
    move-result-wide v4

    #@96
    .line 109
    .local v4, _result:J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@99
    .line 110
    invoke-virtual {p3, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    #@9c
    goto/16 :goto_9

    #@9e
    .line 115
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v1           #_arg1:J
    .end local v3           #_arg2:I
    .end local v4           #_result:J
    :sswitch_9e
    const-string v7, "com.android.internal.widget.ILockSettings"

    #@a0
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a3
    .line 117
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@a6
    move-result-object v0

    #@a7
    .line 119
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@aa
    move-result-object v1

    #@ab
    .line 121
    .local v1, _arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@ae
    move-result v3

    #@af
    .line 122
    .restart local v3       #_arg2:I
    invoke-virtual {p0, v0, v1, v3}, Lcom/android/internal/widget/ILockSettings$Stub;->getString(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    #@b2
    move-result-object v4

    #@b3
    .line 123
    .local v4, _result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@b6
    .line 124
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@b9
    goto/16 :goto_9

    #@bb
    .line 129
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v1           #_arg1:Ljava/lang/String;
    .end local v3           #_arg2:I
    .end local v4           #_result:Ljava/lang/String;
    :sswitch_bb
    const-string v7, "com.android.internal.widget.ILockSettings"

    #@bd
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@c0
    .line 131
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@c3
    move-result-object v0

    #@c4
    .line 133
    .local v0, _arg0:[B
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@c7
    move-result v1

    #@c8
    .line 134
    .local v1, _arg1:I
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/widget/ILockSettings$Stub;->setLockPattern([BI)V

    #@cb
    .line 135
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@ce
    goto/16 :goto_9

    #@d0
    .line 140
    .end local v0           #_arg0:[B
    .end local v1           #_arg1:I
    :sswitch_d0
    const-string v7, "com.android.internal.widget.ILockSettings"

    #@d2
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@d5
    .line 142
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@d8
    move-result-object v0

    #@d9
    .line 144
    .restart local v0       #_arg0:[B
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@dc
    move-result v1

    #@dd
    .line 145
    .restart local v1       #_arg1:I
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/widget/ILockSettings$Stub;->setLockPatternKidsMode([BI)V

    #@e0
    .line 146
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@e3
    goto/16 :goto_9

    #@e5
    .line 151
    .end local v0           #_arg0:[B
    .end local v1           #_arg1:I
    :sswitch_e5
    const-string v8, "com.android.internal.widget.ILockSettings"

    #@e7
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@ea
    .line 153
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@ed
    move-result-object v0

    #@ee
    .line 155
    .restart local v0       #_arg0:[B
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@f1
    move-result v1

    #@f2
    .line 156
    .restart local v1       #_arg1:I
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/widget/ILockSettings$Stub;->checkPattern([BI)Z

    #@f5
    move-result v4

    #@f6
    .line 157
    .local v4, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@f9
    .line 158
    if-eqz v4, :cond_fc

    #@fb
    move v7, v6

    #@fc
    :cond_fc
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@ff
    goto/16 :goto_9

    #@101
    .line 163
    .end local v0           #_arg0:[B
    .end local v1           #_arg1:I
    .end local v4           #_result:Z
    :sswitch_101
    const-string v8, "com.android.internal.widget.ILockSettings"

    #@103
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@106
    .line 165
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@109
    move-result-object v0

    #@10a
    .line 167
    .restart local v0       #_arg0:[B
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@10d
    move-result v1

    #@10e
    .line 168
    .restart local v1       #_arg1:I
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/widget/ILockSettings$Stub;->checkPatternKidsMode([BI)Z

    #@111
    move-result v4

    #@112
    .line 169
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@115
    .line 170
    if-eqz v4, :cond_118

    #@117
    move v7, v6

    #@118
    :cond_118
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@11b
    goto/16 :goto_9

    #@11d
    .line 175
    .end local v0           #_arg0:[B
    .end local v1           #_arg1:I
    .end local v4           #_result:Z
    :sswitch_11d
    const-string v7, "com.android.internal.widget.ILockSettings"

    #@11f
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@122
    .line 177
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@125
    move-result-object v0

    #@126
    .line 179
    .restart local v0       #_arg0:[B
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@129
    move-result v1

    #@12a
    .line 180
    .restart local v1       #_arg1:I
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/widget/ILockSettings$Stub;->setLockPassword([BI)V

    #@12d
    .line 181
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@130
    goto/16 :goto_9

    #@132
    .line 186
    .end local v0           #_arg0:[B
    .end local v1           #_arg1:I
    :sswitch_132
    const-string v8, "com.android.internal.widget.ILockSettings"

    #@134
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@137
    .line 188
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@13a
    move-result-object v0

    #@13b
    .line 190
    .restart local v0       #_arg0:[B
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@13e
    move-result v1

    #@13f
    .line 191
    .restart local v1       #_arg1:I
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/widget/ILockSettings$Stub;->checkPassword([BI)Z

    #@142
    move-result v4

    #@143
    .line 192
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@146
    .line 193
    if-eqz v4, :cond_149

    #@148
    move v7, v6

    #@149
    :cond_149
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@14c
    goto/16 :goto_9

    #@14e
    .line 198
    .end local v0           #_arg0:[B
    .end local v1           #_arg1:I
    .end local v4           #_result:Z
    :sswitch_14e
    const-string v8, "com.android.internal.widget.ILockSettings"

    #@150
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@153
    .line 200
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@156
    move-result v0

    #@157
    .line 201
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/ILockSettings$Stub;->havePattern(I)Z

    #@15a
    move-result v4

    #@15b
    .line 202
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@15e
    .line 203
    if-eqz v4, :cond_161

    #@160
    move v7, v6

    #@161
    :cond_161
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@164
    goto/16 :goto_9

    #@166
    .line 208
    .end local v0           #_arg0:I
    .end local v4           #_result:Z
    :sswitch_166
    const-string v8, "com.android.internal.widget.ILockSettings"

    #@168
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@16b
    .line 210
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@16e
    move-result v0

    #@16f
    .line 211
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/ILockSettings$Stub;->havePatternKidsMode(I)Z

    #@172
    move-result v4

    #@173
    .line 212
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@176
    .line 213
    if-eqz v4, :cond_179

    #@178
    move v7, v6

    #@179
    :cond_179
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@17c
    goto/16 :goto_9

    #@17e
    .line 218
    .end local v0           #_arg0:I
    .end local v4           #_result:Z
    :sswitch_17e
    const-string v8, "com.android.internal.widget.ILockSettings"

    #@180
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@183
    .line 220
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@186
    move-result v0

    #@187
    .line 221
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/ILockSettings$Stub;->havePassword(I)Z

    #@18a
    move-result v4

    #@18b
    .line 222
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@18e
    .line 223
    if-eqz v4, :cond_191

    #@190
    move v7, v6

    #@191
    :cond_191
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@194
    goto/16 :goto_9

    #@196
    .line 228
    .end local v0           #_arg0:I
    .end local v4           #_result:Z
    :sswitch_196
    const-string v7, "com.android.internal.widget.ILockSettings"

    #@198
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@19b
    .line 230
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@19e
    move-result v0

    #@19f
    .line 231
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/ILockSettings$Stub;->removeUser(I)V

    #@1a2
    .line 232
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1a5
    goto/16 :goto_9

    #@1a7
    .line 39
    nop

    #@1a8
    :sswitch_data_1a8
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_2d
        0x3 -> :sswitch_45
        0x4 -> :sswitch_5d
        0x5 -> :sswitch_81
        0x6 -> :sswitch_9e
        0x7 -> :sswitch_bb
        0x8 -> :sswitch_d0
        0x9 -> :sswitch_e5
        0xa -> :sswitch_101
        0xb -> :sswitch_11d
        0xc -> :sswitch_132
        0xd -> :sswitch_14e
        0xe -> :sswitch_166
        0xf -> :sswitch_17e
        0x10 -> :sswitch_196
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
