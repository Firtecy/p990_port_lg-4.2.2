.class public Lcom/android/internal/widget/TextProgressBar;
.super Landroid/widget/RelativeLayout;
.source "TextProgressBar.java"

# interfaces
.implements Landroid/widget/Chronometer$OnChronometerTickListener;


# annotations
.annotation runtime Landroid/widget/RemoteViews$RemoteView;
.end annotation


# static fields
.field static final CHRONOMETER_ID:I = 0x1020014

.field static final PROGRESSBAR_ID:I = 0x102000d

.field public static final TAG:Ljava/lang/String; = "TextProgressBar"


# instance fields
.field mChronometer:Landroid/widget/Chronometer;

.field mChronometerFollow:Z

.field mChronometerGravity:I

.field mDuration:I

.field mDurationBase:J

.field mProgressBar:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 5
    .parameter "context"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/4 v2, 0x0

    #@2
    .line 72
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    #@5
    .line 54
    iput-object v0, p0, Lcom/android/internal/widget/TextProgressBar;->mChronometer:Landroid/widget/Chronometer;

    #@7
    .line 55
    iput-object v0, p0, Lcom/android/internal/widget/TextProgressBar;->mProgressBar:Landroid/widget/ProgressBar;

    #@9
    .line 57
    const-wide/16 v0, -0x1

    #@b
    iput-wide v0, p0, Lcom/android/internal/widget/TextProgressBar;->mDurationBase:J

    #@d
    .line 58
    const/4 v0, -0x1

    #@e
    iput v0, p0, Lcom/android/internal/widget/TextProgressBar;->mDuration:I

    #@10
    .line 60
    iput-boolean v2, p0, Lcom/android/internal/widget/TextProgressBar;->mChronometerFollow:Z

    #@12
    .line 61
    iput v2, p0, Lcom/android/internal/widget/TextProgressBar;->mChronometerGravity:I

    #@14
    .line 73
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 6
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/4 v2, 0x0

    #@2
    .line 68
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@5
    .line 54
    iput-object v0, p0, Lcom/android/internal/widget/TextProgressBar;->mChronometer:Landroid/widget/Chronometer;

    #@7
    .line 55
    iput-object v0, p0, Lcom/android/internal/widget/TextProgressBar;->mProgressBar:Landroid/widget/ProgressBar;

    #@9
    .line 57
    const-wide/16 v0, -0x1

    #@b
    iput-wide v0, p0, Lcom/android/internal/widget/TextProgressBar;->mDurationBase:J

    #@d
    .line 58
    const/4 v0, -0x1

    #@e
    iput v0, p0, Lcom/android/internal/widget/TextProgressBar;->mDuration:I

    #@10
    .line 60
    iput-boolean v2, p0, Lcom/android/internal/widget/TextProgressBar;->mChronometerFollow:Z

    #@12
    .line 61
    iput v2, p0, Lcom/android/internal/widget/TextProgressBar;->mChronometerGravity:I

    #@14
    .line 69
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 7
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/4 v2, 0x0

    #@2
    .line 64
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@5
    .line 54
    iput-object v0, p0, Lcom/android/internal/widget/TextProgressBar;->mChronometer:Landroid/widget/Chronometer;

    #@7
    .line 55
    iput-object v0, p0, Lcom/android/internal/widget/TextProgressBar;->mProgressBar:Landroid/widget/ProgressBar;

    #@9
    .line 57
    const-wide/16 v0, -0x1

    #@b
    iput-wide v0, p0, Lcom/android/internal/widget/TextProgressBar;->mDurationBase:J

    #@d
    .line 58
    const/4 v0, -0x1

    #@e
    iput v0, p0, Lcom/android/internal/widget/TextProgressBar;->mDuration:I

    #@10
    .line 60
    iput-boolean v2, p0, Lcom/android/internal/widget/TextProgressBar;->mChronometerFollow:Z

    #@12
    .line 61
    iput v2, p0, Lcom/android/internal/widget/TextProgressBar;->mChronometerGravity:I

    #@14
    .line 65
    return-void
.end method


# virtual methods
.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .registers 7
    .parameter "child"
    .parameter "index"
    .parameter "params"

    #@0
    .prologue
    .line 80
    invoke-super {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    #@3
    .line 82
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    #@6
    move-result v0

    #@7
    .line 83
    .local v0, childId:I
    const v1, 0x1020014

    #@a
    if-ne v0, v1, :cond_30

    #@c
    instance-of v1, p1, Landroid/widget/Chronometer;

    #@e
    if-eqz v1, :cond_30

    #@10
    .line 84
    check-cast p1, Landroid/widget/Chronometer;

    #@12
    .end local p1
    iput-object p1, p0, Lcom/android/internal/widget/TextProgressBar;->mChronometer:Landroid/widget/Chronometer;

    #@14
    .line 85
    iget-object v1, p0, Lcom/android/internal/widget/TextProgressBar;->mChronometer:Landroid/widget/Chronometer;

    #@16
    invoke-virtual {v1, p0}, Landroid/widget/Chronometer;->setOnChronometerTickListener(Landroid/widget/Chronometer$OnChronometerTickListener;)V

    #@19
    .line 88
    iget v1, p3, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@1b
    const/4 v2, -0x2

    #@1c
    if-ne v1, v2, :cond_2e

    #@1e
    const/4 v1, 0x1

    #@1f
    :goto_1f
    iput-boolean v1, p0, Lcom/android/internal/widget/TextProgressBar;->mChronometerFollow:Z

    #@21
    .line 89
    iget-object v1, p0, Lcom/android/internal/widget/TextProgressBar;->mChronometer:Landroid/widget/Chronometer;

    #@23
    invoke-virtual {v1}, Landroid/widget/Chronometer;->getGravity()I

    #@26
    move-result v1

    #@27
    const v2, 0x800007

    #@2a
    and-int/2addr v1, v2

    #@2b
    iput v1, p0, Lcom/android/internal/widget/TextProgressBar;->mChronometerGravity:I

    #@2d
    .line 95
    :cond_2d
    :goto_2d
    return-void

    #@2e
    .line 88
    :cond_2e
    const/4 v1, 0x0

    #@2f
    goto :goto_1f

    #@30
    .line 92
    .restart local p1
    :cond_30
    const v1, 0x102000d

    #@33
    if-ne v0, v1, :cond_2d

    #@35
    instance-of v1, p1, Landroid/widget/ProgressBar;

    #@37
    if-eqz v1, :cond_2d

    #@39
    .line 93
    check-cast p1, Landroid/widget/ProgressBar;

    #@3b
    .end local p1
    iput-object p1, p0, Lcom/android/internal/widget/TextProgressBar;->mProgressBar:Landroid/widget/ProgressBar;

    #@3d
    goto :goto_2d
.end method

.method public onChronometerTick(Landroid/widget/Chronometer;)V
    .registers 14
    .parameter "chronometer"

    #@0
    .prologue
    .line 130
    iget-object v9, p0, Lcom/android/internal/widget/TextProgressBar;->mProgressBar:Landroid/widget/ProgressBar;

    #@2
    if-nez v9, :cond_c

    #@4
    .line 131
    new-instance v9, Ljava/lang/RuntimeException;

    #@6
    const-string v10, "Expecting child ProgressBar with id \'android.R.id.progress\'"

    #@8
    invoke-direct {v9, v10}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@b
    throw v9

    #@c
    .line 136
    :cond_c
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@f
    move-result-wide v3

    #@10
    .line 137
    .local v3, now:J
    iget-wide v9, p0, Lcom/android/internal/widget/TextProgressBar;->mDurationBase:J

    #@12
    cmp-long v9, v3, v9

    #@14
    if-ltz v9, :cond_1b

    #@16
    .line 138
    iget-object v9, p0, Lcom/android/internal/widget/TextProgressBar;->mChronometer:Landroid/widget/Chronometer;

    #@18
    invoke-virtual {v9}, Landroid/widget/Chronometer;->stop()V

    #@1b
    .line 142
    :cond_1b
    iget-wide v9, p0, Lcom/android/internal/widget/TextProgressBar;->mDurationBase:J

    #@1d
    sub-long/2addr v9, v3

    #@1e
    long-to-int v6, v9

    #@1f
    .line 143
    .local v6, remaining:I
    iget-object v9, p0, Lcom/android/internal/widget/TextProgressBar;->mProgressBar:Landroid/widget/ProgressBar;

    #@21
    iget v10, p0, Lcom/android/internal/widget/TextProgressBar;->mDuration:I

    #@23
    sub-int/2addr v10, v6

    #@24
    invoke-virtual {v9, v10}, Landroid/widget/ProgressBar;->setProgress(I)V

    #@27
    .line 146
    iget-boolean v9, p0, Lcom/android/internal/widget/TextProgressBar;->mChronometerFollow:Z

    #@29
    if-eqz v9, :cond_7d

    #@2b
    .line 150
    iget-object v9, p0, Lcom/android/internal/widget/TextProgressBar;->mProgressBar:Landroid/widget/ProgressBar;

    #@2d
    invoke-virtual {v9}, Landroid/widget/ProgressBar;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@30
    move-result-object v5

    #@31
    check-cast v5, Landroid/widget/RelativeLayout$LayoutParams;

    #@33
    .line 151
    .local v5, params:Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v9, p0, Lcom/android/internal/widget/TextProgressBar;->mProgressBar:Landroid/widget/ProgressBar;

    #@35
    invoke-virtual {v9}, Landroid/widget/ProgressBar;->getWidth()I

    #@38
    move-result v9

    #@39
    iget v10, v5, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@3b
    iget v11, v5, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@3d
    add-int/2addr v10, v11

    #@3e
    sub-int v1, v9, v10

    #@40
    .line 152
    .local v1, contentWidth:I
    iget-object v9, p0, Lcom/android/internal/widget/TextProgressBar;->mProgressBar:Landroid/widget/ProgressBar;

    #@42
    invoke-virtual {v9}, Landroid/widget/ProgressBar;->getProgress()I

    #@45
    move-result v9

    #@46
    mul-int/2addr v9, v1

    #@47
    iget-object v10, p0, Lcom/android/internal/widget/TextProgressBar;->mProgressBar:Landroid/widget/ProgressBar;

    #@49
    invoke-virtual {v10}, Landroid/widget/ProgressBar;->getMax()I

    #@4c
    move-result v10

    #@4d
    div-int/2addr v9, v10

    #@4e
    iget v10, v5, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@50
    add-int v2, v9, v10

    #@52
    .line 156
    .local v2, leadingEdge:I
    const/4 v0, 0x0

    #@53
    .line 157
    .local v0, adjustLeft:I
    iget-object v9, p0, Lcom/android/internal/widget/TextProgressBar;->mChronometer:Landroid/widget/Chronometer;

    #@55
    invoke-virtual {v9}, Landroid/widget/Chronometer;->getWidth()I

    #@58
    move-result v8

    #@59
    .line 158
    .local v8, textWidth:I
    iget v9, p0, Lcom/android/internal/widget/TextProgressBar;->mChronometerGravity:I

    #@5b
    const v10, 0x800005

    #@5e
    if-ne v9, v10, :cond_7e

    #@60
    .line 159
    neg-int v0, v8

    #@61
    .line 165
    :cond_61
    :goto_61
    add-int/2addr v2, v0

    #@62
    .line 166
    iget v9, v5, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@64
    sub-int v9, v1, v9

    #@66
    sub-int v7, v9, v8

    #@68
    .line 167
    .local v7, rightLimit:I
    iget v9, v5, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@6a
    if-ge v2, v9, :cond_87

    #@6c
    .line 168
    iget v2, v5, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@6e
    .line 173
    :cond_6e
    :goto_6e
    iget-object v9, p0, Lcom/android/internal/widget/TextProgressBar;->mChronometer:Landroid/widget/Chronometer;

    #@70
    invoke-virtual {v9}, Landroid/widget/Chronometer;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@73
    move-result-object v5

    #@74
    .end local v5           #params:Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v5, Landroid/widget/RelativeLayout$LayoutParams;

    #@76
    .line 174
    .restart local v5       #params:Landroid/widget/RelativeLayout$LayoutParams;
    iput v2, v5, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@78
    .line 177
    iget-object v9, p0, Lcom/android/internal/widget/TextProgressBar;->mChronometer:Landroid/widget/Chronometer;

    #@7a
    invoke-virtual {v9}, Landroid/widget/Chronometer;->requestLayout()V

    #@7d
    .line 180
    .end local v0           #adjustLeft:I
    .end local v1           #contentWidth:I
    .end local v2           #leadingEdge:I
    .end local v5           #params:Landroid/widget/RelativeLayout$LayoutParams;
    .end local v7           #rightLimit:I
    .end local v8           #textWidth:I
    :cond_7d
    return-void

    #@7e
    .line 160
    .restart local v0       #adjustLeft:I
    .restart local v1       #contentWidth:I
    .restart local v2       #leadingEdge:I
    .restart local v5       #params:Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v8       #textWidth:I
    :cond_7e
    iget v9, p0, Lcom/android/internal/widget/TextProgressBar;->mChronometerGravity:I

    #@80
    const/4 v10, 0x1

    #@81
    if-ne v9, v10, :cond_61

    #@83
    .line 161
    div-int/lit8 v9, v8, 0x2

    #@85
    neg-int v0, v9

    #@86
    goto :goto_61

    #@87
    .line 169
    .restart local v7       #rightLimit:I
    :cond_87
    if-le v2, v7, :cond_6e

    #@89
    .line 170
    move v2, v7

    #@8a
    goto :goto_6e
.end method

.method public setDurationBase(J)V
    .registers 5
    .parameter "durationBase"
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    #@0
    .prologue
    .line 110
    iput-wide p1, p0, Lcom/android/internal/widget/TextProgressBar;->mDurationBase:J

    #@2
    .line 112
    iget-object v0, p0, Lcom/android/internal/widget/TextProgressBar;->mProgressBar:Landroid/widget/ProgressBar;

    #@4
    if-eqz v0, :cond_a

    #@6
    iget-object v0, p0, Lcom/android/internal/widget/TextProgressBar;->mChronometer:Landroid/widget/Chronometer;

    #@8
    if-nez v0, :cond_12

    #@a
    .line 113
    :cond_a
    new-instance v0, Ljava/lang/RuntimeException;

    #@c
    const-string v1, "Expecting child ProgressBar with id \'android.R.id.progress\' and Chronometer id \'android.R.id.text1\'"

    #@e
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@11
    throw v0

    #@12
    .line 118
    :cond_12
    iget-object v0, p0, Lcom/android/internal/widget/TextProgressBar;->mChronometer:Landroid/widget/Chronometer;

    #@14
    invoke-virtual {v0}, Landroid/widget/Chronometer;->getBase()J

    #@17
    move-result-wide v0

    #@18
    sub-long v0, p1, v0

    #@1a
    long-to-int v0, v0

    #@1b
    iput v0, p0, Lcom/android/internal/widget/TextProgressBar;->mDuration:I

    #@1d
    .line 119
    iget v0, p0, Lcom/android/internal/widget/TextProgressBar;->mDuration:I

    #@1f
    if-gtz v0, :cond_24

    #@21
    .line 120
    const/4 v0, 0x1

    #@22
    iput v0, p0, Lcom/android/internal/widget/TextProgressBar;->mDuration:I

    #@24
    .line 122
    :cond_24
    iget-object v0, p0, Lcom/android/internal/widget/TextProgressBar;->mProgressBar:Landroid/widget/ProgressBar;

    #@26
    iget v1, p0, Lcom/android/internal/widget/TextProgressBar;->mDuration:I

    #@28
    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    #@2b
    .line 123
    return-void
.end method
