.class final Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;
.super Ljava/lang/Object;
.source "PointerLocationView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/widget/PointerLocationView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "FasterStringBuilder"
.end annotation


# instance fields
.field private mChars:[C

.field private mLength:I


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 778
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 779
    const/16 v0, 0x40

    #@5
    new-array v0, v0, [C

    #@7
    iput-object v0, p0, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->mChars:[C

    #@9
    .line 780
    return-void
.end method

.method private reserve(I)I
    .registers 10
    .parameter "length"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 868
    iget v5, p0, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->mLength:I

    #@3
    .line 869
    .local v5, oldLength:I
    iget v6, p0, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->mLength:I

    #@5
    add-int v2, v6, p1

    #@7
    .line 870
    .local v2, newLength:I
    iget-object v4, p0, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->mChars:[C

    #@9
    .line 871
    .local v4, oldChars:[C
    array-length v3, v4

    #@a
    .line 872
    .local v3, oldCapacity:I
    if-le v2, v3, :cond_15

    #@c
    .line 873
    mul-int/lit8 v0, v3, 0x2

    #@e
    .line 874
    .local v0, newCapacity:I
    new-array v1, v0, [C

    #@10
    .line 875
    .local v1, newChars:[C
    invoke-static {v4, v7, v1, v7, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@13
    .line 876
    iput-object v1, p0, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->mChars:[C

    #@15
    .line 878
    .end local v0           #newCapacity:I
    .end local v1           #newChars:[C
    :cond_15
    return v5
.end method


# virtual methods
.method public append(FI)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;
    .registers 9
    .parameter "value"
    .parameter "precision"

    #@0
    .prologue
    .line 844
    const/4 v1, 0x1

    #@1
    .line 845
    .local v1, scale:I
    const/4 v0, 0x0

    #@2
    .local v0, i:I
    :goto_2
    if-ge v0, p2, :cond_9

    #@4
    .line 846
    mul-int/lit8 v1, v1, 0xa

    #@6
    .line 845
    add-int/lit8 v0, v0, 0x1

    #@8
    goto :goto_2

    #@9
    .line 848
    :cond_9
    int-to-float v2, v1

    #@a
    mul-float/2addr v2, p1

    #@b
    float-to-double v2, v2

    #@c
    invoke-static {v2, v3}, Ljava/lang/Math;->rint(D)D

    #@f
    move-result-wide v2

    #@10
    int-to-double v4, v1

    #@11
    div-double/2addr v2, v4

    #@12
    double-to-float p1, v2

    #@13
    .line 850
    float-to-int v2, p1

    #@14
    invoke-virtual {p0, v2}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(I)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@17
    .line 852
    if-eqz p2, :cond_30

    #@19
    .line 853
    const-string v2, "."

    #@1b
    invoke-virtual {p0, v2}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(Ljava/lang/String;)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@1e
    .line 854
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    #@21
    move-result p1

    #@22
    .line 855
    float-to-double v2, p1

    #@23
    float-to-double v4, p1

    #@24
    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    #@27
    move-result-wide v4

    #@28
    sub-double/2addr v2, v4

    #@29
    double-to-float p1, v2

    #@2a
    .line 856
    int-to-float v2, v1

    #@2b
    mul-float/2addr v2, p1

    #@2c
    float-to-int v2, v2

    #@2d
    invoke-virtual {p0, v2, p2}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(II)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@30
    .line 859
    :cond_30
    return-object p0
.end method

.method public append(I)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 796
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(II)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public append(II)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;
    .registers 12
    .parameter "value"
    .parameter "zeroPadWidth"

    #@0
    .prologue
    const/16 v8, 0x30

    #@2
    .line 800
    if-gez p1, :cond_10

    #@4
    const/4 v5, 0x1

    #@5
    .line 801
    .local v5, negative:Z
    :goto_5
    if-eqz v5, :cond_12

    #@7
    .line 802
    neg-int p1, p1

    #@8
    .line 803
    if-gez p1, :cond_12

    #@a
    .line 804
    const-string v7, "-2147483648"

    #@c
    invoke-virtual {p0, v7}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(Ljava/lang/String;)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@f
    .line 840
    :goto_f
    return-object p0

    #@10
    .line 800
    .end local v5           #negative:Z
    :cond_10
    const/4 v5, 0x0

    #@11
    goto :goto_5

    #@12
    .line 809
    .restart local v5       #negative:Z
    :cond_12
    const/16 v7, 0xb

    #@14
    invoke-direct {p0, v7}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->reserve(I)I

    #@17
    move-result v3

    #@18
    .line 810
    .local v3, index:I
    iget-object v0, p0, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->mChars:[C

    #@1a
    .line 812
    .local v0, chars:[C
    if-nez p1, :cond_27

    #@1c
    .line 813
    add-int/lit8 v4, v3, 0x1

    #@1e
    .end local v3           #index:I
    .local v4, index:I
    aput-char v8, v0, v3

    #@20
    .line 814
    iget v7, p0, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->mLength:I

    #@22
    add-int/lit8 v7, v7, 0x1

    #@24
    iput v7, p0, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->mLength:I

    #@26
    goto :goto_f

    #@27
    .line 818
    .end local v4           #index:I
    .restart local v3       #index:I
    :cond_27
    if-eqz v5, :cond_30

    #@29
    .line 819
    add-int/lit8 v4, v3, 0x1

    #@2b
    .end local v3           #index:I
    .restart local v4       #index:I
    const/16 v7, 0x2d

    #@2d
    aput-char v7, v0, v3

    #@2f
    move v3, v4

    #@30
    .line 822
    .end local v4           #index:I
    .restart local v3       #index:I
    :cond_30
    const v2, 0x3b9aca00

    #@33
    .line 823
    .local v2, divisor:I
    const/16 v6, 0xa

    #@35
    .local v6, numberWidth:I
    move v4, v3

    #@36
    .line 824
    .end local v3           #index:I
    .restart local v4       #index:I
    :cond_36
    :goto_36
    if-ge p1, v2, :cond_44

    #@38
    .line 825
    div-int/lit8 v2, v2, 0xa

    #@3a
    .line 826
    add-int/lit8 v6, v6, -0x1

    #@3c
    .line 827
    if-ge v6, p2, :cond_36

    #@3e
    .line 828
    add-int/lit8 v3, v4, 0x1

    #@40
    .end local v4           #index:I
    .restart local v3       #index:I
    aput-char v8, v0, v4

    #@42
    move v4, v3

    #@43
    .end local v3           #index:I
    .restart local v4       #index:I
    goto :goto_36

    #@44
    :cond_44
    move v3, v4

    #@45
    .line 833
    .end local v4           #index:I
    .restart local v3       #index:I
    div-int v1, p1, v2

    #@47
    .line 834
    .local v1, digit:I
    mul-int v7, v1, v2

    #@49
    sub-int/2addr p1, v7

    #@4a
    .line 835
    div-int/lit8 v2, v2, 0xa

    #@4c
    .line 836
    add-int/lit8 v4, v3, 0x1

    #@4e
    .end local v3           #index:I
    .restart local v4       #index:I
    add-int/lit8 v7, v1, 0x30

    #@50
    int-to-char v7, v7

    #@51
    aput-char v7, v0, v3

    #@53
    .line 837
    if-nez v2, :cond_44

    #@55
    .line 839
    iput v4, p0, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->mLength:I

    #@57
    goto :goto_f
.end method

.method public append(Ljava/lang/String;)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;
    .registers 6
    .parameter "value"

    #@0
    .prologue
    .line 788
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@3
    move-result v1

    #@4
    .line 789
    .local v1, valueLength:I
    invoke-direct {p0, v1}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->reserve(I)I

    #@7
    move-result v0

    #@8
    .line 790
    .local v0, index:I
    const/4 v2, 0x0

    #@9
    iget-object v3, p0, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->mChars:[C

    #@b
    invoke-virtual {p1, v2, v1, v3, v0}, Ljava/lang/String;->getChars(II[CI)V

    #@e
    .line 791
    iget v2, p0, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->mLength:I

    #@10
    add-int/2addr v2, v1

    #@11
    iput v2, p0, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->mLength:I

    #@13
    .line 792
    return-object p0
.end method

.method public clear()Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;
    .registers 2

    #@0
    .prologue
    .line 783
    const/4 v0, 0x0

    #@1
    iput v0, p0, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->mLength:I

    #@3
    .line 784
    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 864
    new-instance v0, Ljava/lang/String;

    #@2
    iget-object v1, p0, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->mChars:[C

    #@4
    const/4 v2, 0x0

    #@5
    iget v3, p0, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->mLength:I

    #@7
    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([CII)V

    #@a
    return-object v0
.end method
