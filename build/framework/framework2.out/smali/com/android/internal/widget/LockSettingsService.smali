.class public Lcom/android/internal/widget/LockSettingsService;
.super Lcom/android/internal/widget/ILockSettings$Stub;
.source "LockSettingsService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/widget/LockSettingsService$DatabaseHelper;
    }
.end annotation


# static fields
.field private static final COLUMNS_FOR_QUERY:[Ljava/lang/String; = null

.field private static final COLUMN_KEY:Ljava/lang/String; = "name"

.field private static final COLUMN_USERID:Ljava/lang/String; = "user"

.field private static final COLUMN_VALUE:Ljava/lang/String; = "value"

.field private static final LOCK_PASSWORD_FILE:Ljava/lang/String; = "password.key"

.field private static final LOCK_PATTERN_FILE:Ljava/lang/String; = "gesture.key"

.field private static final LOCK_PATTERN_KIDS_MODE_FILE:Ljava/lang/String; = "gesture_kids.key"

.field private static final SYSTEM_DIRECTORY:Ljava/lang/String; = "/system/"

.field private static final TABLE:Ljava/lang/String; = "locksettings"

.field private static final TAG:Ljava/lang/String; = "LockSettingsService"

.field private static final VALID_SETTINGS:[Ljava/lang/String;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mOpenHelper:Lcom/android/internal/widget/LockSettingsService$DatabaseHelper;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 57
    new-array v0, v3, [Ljava/lang/String;

    #@4
    const-string v1, "value"

    #@6
    aput-object v1, v0, v2

    #@8
    sput-object v0, Lcom/android/internal/widget/LockSettingsService;->COLUMNS_FOR_QUERY:[Ljava/lang/String;

    #@a
    .line 449
    const/16 v0, 0x10

    #@c
    new-array v0, v0, [Ljava/lang/String;

    #@e
    const-string v1, "lockscreen.lockedoutpermanently"

    #@10
    aput-object v1, v0, v2

    #@12
    const-string v1, "lockscreen.lockoutattemptdeadline"

    #@14
    aput-object v1, v0, v3

    #@16
    const/4 v1, 0x2

    #@17
    const-string v2, "lockscreen.patterneverchosen"

    #@19
    aput-object v2, v0, v1

    #@1b
    const/4 v1, 0x3

    #@1c
    const-string v2, "lockscreen.password_type"

    #@1e
    aput-object v2, v0, v1

    #@20
    const/4 v1, 0x4

    #@21
    const-string v2, "lockscreen.password_type_alternate"

    #@23
    aput-object v2, v0, v1

    #@25
    const/4 v1, 0x5

    #@26
    const-string v2, "lockscreen.password_salt"

    #@28
    aput-object v2, v0, v1

    #@2a
    const/4 v1, 0x6

    #@2b
    const-string v2, "lockscreen.disabled"

    #@2d
    aput-object v2, v0, v1

    #@2f
    const/4 v1, 0x7

    #@30
    const-string v2, "lockscreen.options"

    #@32
    aput-object v2, v0, v1

    #@34
    const/16 v1, 0x8

    #@36
    const-string v2, "lockscreen.biometric_weak_fallback"

    #@38
    aput-object v2, v0, v1

    #@3a
    const/16 v1, 0x9

    #@3c
    const-string v2, "lockscreen.biometricweakeverchosen"

    #@3e
    aput-object v2, v0, v1

    #@40
    const/16 v1, 0xa

    #@42
    const-string v2, "lockscreen.power_button_instantly_locks"

    #@44
    aput-object v2, v0, v1

    #@46
    const/16 v1, 0xb

    #@48
    const-string v2, "lockscreen.passwordhistory"

    #@4a
    aput-object v2, v0, v1

    #@4c
    const/16 v1, 0xc

    #@4e
    const-string v2, "lock_pattern_autolock"

    #@50
    aput-object v2, v0, v1

    #@52
    const/16 v1, 0xd

    #@54
    const-string v2, "lock_biometric_weak_flags"

    #@56
    aput-object v2, v0, v1

    #@58
    const/16 v1, 0xe

    #@5a
    const-string v2, "lock_pattern_visible_pattern"

    #@5c
    aput-object v2, v0, v1

    #@5e
    const/16 v1, 0xf

    #@60
    const-string v2, "lock_pattern_tactile_feedback_enabled"

    #@62
    aput-object v2, v0, v1

    #@64
    sput-object v0, Lcom/android/internal/widget/LockSettingsService;->VALID_SETTINGS:[Ljava/lang/String;

    #@66
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 70
    invoke-direct {p0}, Lcom/android/internal/widget/ILockSettings$Stub;-><init>()V

    #@3
    .line 71
    iput-object p1, p0, Lcom/android/internal/widget/LockSettingsService;->mContext:Landroid/content/Context;

    #@5
    .line 73
    new-instance v0, Lcom/android/internal/widget/LockSettingsService$DatabaseHelper;

    #@7
    iget-object v1, p0, Lcom/android/internal/widget/LockSettingsService;->mContext:Landroid/content/Context;

    #@9
    invoke-direct {v0, p0, v1}, Lcom/android/internal/widget/LockSettingsService$DatabaseHelper;-><init>(Lcom/android/internal/widget/LockSettingsService;Landroid/content/Context;)V

    #@c
    iput-object v0, p0, Lcom/android/internal/widget/LockSettingsService;->mOpenHelper:Lcom/android/internal/widget/LockSettingsService$DatabaseHelper;

    #@e
    .line 74
    return-void
.end method

.method static synthetic access$000(Lcom/android/internal/widget/LockSettingsService;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;I)V
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    #@0
    .prologue
    .line 47
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/internal/widget/LockSettingsService;->writeToDb(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method private static final checkPasswordReadPermission(I)V
    .registers 5
    .parameter "userId"

    #@0
    .prologue
    .line 111
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@3
    move-result v0

    #@4
    .line 112
    .local v0, callingUid:I
    invoke-static {v0}, Landroid/os/UserHandle;->getAppId(I)I

    #@7
    move-result v1

    #@8
    const/16 v2, 0x3e8

    #@a
    if-eq v1, v2, :cond_2b

    #@c
    .line 113
    new-instance v1, Ljava/lang/SecurityException;

    #@e
    new-instance v2, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v3, "uid="

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    const-string v3, " not authorized to read lock password"

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v2

    #@27
    invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@2a
    throw v1

    #@2b
    .line 116
    :cond_2b
    return-void
.end method

.method private static final checkReadPermission(I)V
    .registers 5
    .parameter "userId"

    #@0
    .prologue
    .line 119
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@3
    move-result v0

    #@4
    .line 120
    .local v0, callingUid:I
    invoke-static {v0}, Landroid/os/UserHandle;->getAppId(I)I

    #@7
    move-result v1

    #@8
    const/16 v2, 0x3e8

    #@a
    if-eq v1, v2, :cond_35

    #@c
    invoke-static {v0}, Landroid/os/UserHandle;->getUserId(I)I

    #@f
    move-result v1

    #@10
    if-eq v1, p0, :cond_35

    #@12
    .line 122
    new-instance v1, Ljava/lang/SecurityException;

    #@14
    new-instance v2, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v3, "uid="

    #@1b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    const-string v3, " not authorized to read settings of user "

    #@25
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v2

    #@29
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v2

    #@2d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v2

    #@31
    invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@34
    throw v1

    #@35
    .line 125
    :cond_35
    return-void
.end method

.method private static final checkWritePermission(I)V
    .registers 5
    .parameter "userId"

    #@0
    .prologue
    .line 103
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@3
    move-result v0

    #@4
    .line 104
    .local v0, callingUid:I
    invoke-static {v0}, Landroid/os/UserHandle;->getAppId(I)I

    #@7
    move-result v1

    #@8
    const/16 v2, 0x3e8

    #@a
    if-eq v1, v2, :cond_2b

    #@c
    .line 105
    new-instance v1, Ljava/lang/SecurityException;

    #@e
    new-instance v2, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v3, "uid="

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    const-string v3, " not authorized to write lock settings"

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v2

    #@27
    invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@2a
    throw v1

    #@2b
    .line 108
    :cond_2b
    return-void
.end method

.method private getLockPasswordFilename(I)Ljava/lang/String;
    .registers 6
    .parameter "userId"

    #@0
    .prologue
    .line 201
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    #@8
    move-result-object v2

    #@9
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, "/system/"

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v0

    #@1b
    .line 204
    .local v0, dataSystemDirectory:Ljava/lang/String;
    if-nez p1, :cond_31

    #@1d
    .line 206
    new-instance v1, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v1

    #@26
    const-string v2, "password.key"

    #@28
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v1

    #@2c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v1

    #@30
    .line 208
    :goto_30
    return-object v1

    #@31
    :cond_31
    new-instance v1, Ljava/io/File;

    #@33
    invoke-static {p1}, Landroid/os/Environment;->getUserSystemDirectory(I)Ljava/io/File;

    #@36
    move-result-object v2

    #@37
    const-string v3, "password.key"

    #@39
    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@3c
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@3f
    move-result-object v1

    #@40
    goto :goto_30
.end method

.method private getLockPatternFilename(I)Ljava/lang/String;
    .registers 6
    .parameter "userId"

    #@0
    .prologue
    .line 173
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    #@8
    move-result-object v2

    #@9
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, "/system/"

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v0

    #@1b
    .line 176
    .local v0, dataSystemDirectory:Ljava/lang/String;
    if-nez p1, :cond_31

    #@1d
    .line 178
    new-instance v1, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v1

    #@26
    const-string v2, "gesture.key"

    #@28
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v1

    #@2c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v1

    #@30
    .line 180
    :goto_30
    return-object v1

    #@31
    :cond_31
    new-instance v1, Ljava/io/File;

    #@33
    invoke-static {p1}, Landroid/os/Environment;->getUserSystemDirectory(I)Ljava/io/File;

    #@36
    move-result-object v2

    #@37
    const-string v3, "gesture.key"

    #@39
    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@3c
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@3f
    move-result-object v1

    #@40
    goto :goto_30
.end method

.method private getLockPatternKidsModeFilename(I)Ljava/lang/String;
    .registers 6
    .parameter "userId"

    #@0
    .prologue
    .line 187
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    #@8
    move-result-object v2

    #@9
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, "/system/"

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v0

    #@1b
    .line 190
    .local v0, dataSystemDirectory:Ljava/lang/String;
    if-nez p1, :cond_31

    #@1d
    .line 192
    new-instance v1, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v1

    #@26
    const-string v2, "gesture_kids.key"

    #@28
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v1

    #@2c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v1

    #@30
    .line 194
    :goto_30
    return-object v1

    #@31
    :cond_31
    new-instance v1, Ljava/io/File;

    #@33
    invoke-static {p1}, Landroid/os/Environment;->getUserSystemDirectory(I)Ljava/io/File;

    #@36
    move-result-object v2

    #@37
    const-string v3, "gesture_kids.key"

    #@39
    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@3c
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@3f
    move-result-object v1

    #@40
    goto :goto_30
.end method

.method private migrateOldData()V
    .registers 11

    #@0
    .prologue
    .line 82
    :try_start_0
    const-string v7, "migrated"

    #@2
    const/4 v8, 0x0

    #@3
    const/4 v9, 0x0

    #@4
    invoke-virtual {p0, v7, v8, v9}, Lcom/android/internal/widget/LockSettingsService;->getString(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    #@7
    move-result-object v7

    #@8
    if-eqz v7, :cond_b

    #@a
    .line 100
    :goto_a
    return-void

    #@b
    .line 87
    :cond_b
    iget-object v7, p0, Lcom/android/internal/widget/LockSettingsService;->mContext:Landroid/content/Context;

    #@d
    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@10
    move-result-object v1

    #@11
    .line 88
    .local v1, cr:Landroid/content/ContentResolver;
    sget-object v0, Lcom/android/internal/widget/LockSettingsService;->VALID_SETTINGS:[Ljava/lang/String;

    #@13
    .local v0, arr$:[Ljava/lang/String;
    array-length v3, v0

    #@14
    .local v3, len$:I
    const/4 v2, 0x0

    #@15
    .local v2, i$:I
    :goto_15
    if-ge v2, v3, :cond_26

    #@17
    aget-object v5, v0, v2

    #@19
    .line 89
    .local v5, validSetting:Ljava/lang/String;
    invoke-static {v1, v5}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@1c
    move-result-object v6

    #@1d
    .line 90
    .local v6, value:Ljava/lang/String;
    if-eqz v6, :cond_23

    #@1f
    .line 91
    const/4 v7, 0x0

    #@20
    invoke-virtual {p0, v5, v6, v7}, Lcom/android/internal/widget/LockSettingsService;->setString(Ljava/lang/String;Ljava/lang/String;I)V

    #@23
    .line 88
    :cond_23
    add-int/lit8 v2, v2, 0x1

    #@25
    goto :goto_15

    #@26
    .line 95
    .end local v5           #validSetting:Ljava/lang/String;
    .end local v6           #value:Ljava/lang/String;
    :cond_26
    const-string v7, "migrated"

    #@28
    const-string v8, "true"

    #@2a
    const/4 v9, 0x0

    #@2b
    invoke-virtual {p0, v7, v8, v9}, Lcom/android/internal/widget/LockSettingsService;->setString(Ljava/lang/String;Ljava/lang/String;I)V

    #@2e
    .line 96
    const-string v7, "LockSettingsService"

    #@30
    const-string v8, "Migrated lock settings to new location"

    #@32
    invoke-static {v7, v8}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_35
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_35} :catch_36

    #@35
    goto :goto_a

    #@36
    .line 97
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #cr:Landroid/content/ContentResolver;
    .end local v2           #i$:I
    .end local v3           #len$:I
    :catch_36
    move-exception v4

    #@37
    .line 98
    .local v4, re:Landroid/os/RemoteException;
    const-string v7, "LockSettingsService"

    #@39
    const-string v8, "Unable to migrate old data"

    #@3b
    invoke-static {v7, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    goto :goto_a
.end method

.method private readFromDb(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .registers 15
    .parameter "key"
    .parameter "defaultValue"
    .parameter "userId"

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    const/4 v5, 0x0

    #@2
    .line 394
    move-object v9, p2

    #@3
    .line 395
    .local v9, result:Ljava/lang/String;
    iget-object v1, p0, Lcom/android/internal/widget/LockSettingsService;->mOpenHelper:Lcom/android/internal/widget/LockSettingsService$DatabaseHelper;

    #@5
    invoke-virtual {v1}, Lcom/android/internal/widget/LockSettingsService$DatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    #@8
    move-result-object v0

    #@9
    .line 396
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "locksettings"

    #@b
    sget-object v2, Lcom/android/internal/widget/LockSettingsService;->COLUMNS_FOR_QUERY:[Ljava/lang/String;

    #@d
    const-string v3, "user=? AND name=?"

    #@f
    const/4 v4, 0x2

    #@10
    new-array v4, v4, [Ljava/lang/String;

    #@12
    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@15
    move-result-object v6

    #@16
    aput-object v6, v4, v10

    #@18
    const/4 v6, 0x1

    #@19
    aput-object p1, v4, v6

    #@1b
    move-object v6, v5

    #@1c
    move-object v7, v5

    #@1d
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@20
    move-result-object v8

    #@21
    .local v8, cursor:Landroid/database/Cursor;
    if-eqz v8, :cond_30

    #@23
    .line 400
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    #@26
    move-result v1

    #@27
    if-eqz v1, :cond_2d

    #@29
    .line 401
    invoke-interface {v8, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@2c
    move-result-object v9

    #@2d
    .line 403
    :cond_2d
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@30
    .line 405
    :cond_30
    return-object v9
.end method

.method private writeFile(Ljava/lang/String;[B)V
    .registers 8
    .parameter "name"
    .parameter "hash"

    #@0
    .prologue
    .line 358
    :try_start_0
    new-instance v1, Ljava/io/RandomAccessFile;

    #@2
    const-string v2, "rw"

    #@4
    invoke-direct {v1, p1, v2}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 360
    .local v1, raf:Ljava/io/RandomAccessFile;
    if-eqz p2, :cond_c

    #@9
    array-length v2, p2

    #@a
    if-nez v2, :cond_15

    #@c
    .line 361
    :cond_c
    const-wide/16 v2, 0x0

    #@e
    invoke-virtual {v1, v2, v3}, Ljava/io/RandomAccessFile;->setLength(J)V

    #@11
    .line 365
    :goto_11
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V

    #@14
    .line 369
    .end local v1           #raf:Ljava/io/RandomAccessFile;
    :goto_14
    return-void

    #@15
    .line 363
    .restart local v1       #raf:Ljava/io/RandomAccessFile;
    :cond_15
    const/4 v2, 0x0

    #@16
    array-length v3, p2

    #@17
    invoke-virtual {v1, p2, v2, v3}, Ljava/io/RandomAccessFile;->write([BII)V
    :try_end_1a
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_1a} :catch_1b

    #@1a
    goto :goto_11

    #@1b
    .line 366
    .end local v1           #raf:Ljava/io/RandomAccessFile;
    :catch_1b
    move-exception v0

    #@1c
    .line 367
    .local v0, ioe:Ljava/io/IOException;
    const-string v2, "LockSettingsService"

    #@1e
    new-instance v3, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v4, "Error writing to file "

    #@25
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v3

    #@29
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v3

    #@2d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v3

    #@31
    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    goto :goto_14
.end method

.method private writeToDb(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;I)V
    .registers 11
    .parameter "db"
    .parameter "key"
    .parameter "value"
    .parameter "userId"

    #@0
    .prologue
    .line 376
    new-instance v0, Landroid/content/ContentValues;

    #@2
    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    #@5
    .line 377
    .local v0, cv:Landroid/content/ContentValues;
    const-string v1, "name"

    #@7
    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 378
    const-string v1, "user"

    #@c
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@13
    .line 379
    const-string v1, "value"

    #@15
    invoke-virtual {v0, v1, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@18
    .line 381
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    #@1b
    .line 383
    :try_start_1b
    const-string v1, "locksettings"

    #@1d
    const-string v2, "name=? AND user=?"

    #@1f
    const/4 v3, 0x2

    #@20
    new-array v3, v3, [Ljava/lang/String;

    #@22
    const/4 v4, 0x0

    #@23
    aput-object p2, v3, v4

    #@25
    const/4 v4, 0x1

    #@26
    invoke-static {p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@29
    move-result-object v5

    #@2a
    aput-object v5, v3, v4

    #@2c
    invoke-virtual {p1, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    #@2f
    .line 385
    const-string v1, "locksettings"

    #@31
    const/4 v2, 0x0

    #@32
    invoke-virtual {p1, v1, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    #@35
    .line 386
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_38
    .catchall {:try_start_1b .. :try_end_38} :catchall_3c

    #@38
    .line 388
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@3b
    .line 390
    return-void

    #@3c
    .line 388
    :catchall_3c
    move-exception v1

    #@3d
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@40
    throw v1
.end method

.method private writeToDb(Ljava/lang/String;Ljava/lang/String;I)V
    .registers 5
    .parameter "key"
    .parameter "value"
    .parameter "userId"

    #@0
    .prologue
    .line 372
    iget-object v0, p0, Lcom/android/internal/widget/LockSettingsService;->mOpenHelper:Lcom/android/internal/widget/LockSettingsService$DatabaseHelper;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/widget/LockSettingsService$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    #@5
    move-result-object v0

    #@6
    invoke-direct {p0, v0, p1, p2, p3}, Lcom/android/internal/widget/LockSettingsService;->writeToDb(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;I)V

    #@9
    .line 373
    return-void
.end method


# virtual methods
.method public checkPassword([BI)Z
    .registers 12
    .parameter "hash"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 310
    invoke-static {p2}, Lcom/android/internal/widget/LockSettingsService;->checkPasswordReadPermission(I)V

    #@4
    .line 314
    :try_start_4
    new-instance v3, Ljava/io/RandomAccessFile;

    #@6
    invoke-direct {p0, p2}, Lcom/android/internal/widget/LockSettingsService;->getLockPasswordFilename(I)Ljava/lang/String;

    #@9
    move-result-object v6

    #@a
    const-string v7, "r"

    #@c
    invoke-direct {v3, v6, v7}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@f
    .line 315
    .local v3, raf:Ljava/io/RandomAccessFile;
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->length()J

    #@12
    move-result-wide v6

    #@13
    long-to-int v6, v6

    #@14
    new-array v4, v6, [B

    #@16
    .line 316
    .local v4, stored:[B
    const/4 v6, 0x0

    #@17
    array-length v7, v4

    #@18
    invoke-virtual {v3, v4, v6, v7}, Ljava/io/RandomAccessFile;->read([BII)I

    #@1b
    move-result v1

    #@1c
    .line 317
    .local v1, got:I
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V

    #@1f
    .line 318
    if-gtz v1, :cond_22

    #@21
    .line 328
    .end local v1           #got:I
    .end local v3           #raf:Ljava/io/RandomAccessFile;
    .end local v4           #stored:[B
    :goto_21
    return v5

    #@22
    .line 322
    .restart local v1       #got:I
    .restart local v3       #raf:Ljava/io/RandomAccessFile;
    .restart local v4       #stored:[B
    :cond_22
    invoke-static {v4, p1}, Ljava/util/Arrays;->equals([B[B)Z
    :try_end_25
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_25} :catch_27
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_25} :catch_41

    #@25
    move-result v5

    #@26
    goto :goto_21

    #@27
    .line 323
    .end local v1           #got:I
    .end local v3           #raf:Ljava/io/RandomAccessFile;
    .end local v4           #stored:[B
    :catch_27
    move-exception v0

    #@28
    .line 324
    .local v0, fnfe:Ljava/io/FileNotFoundException;
    const-string v6, "LockSettingsService"

    #@2a
    new-instance v7, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v8, "Cannot read file "

    #@31
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v7

    #@35
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v7

    #@39
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v7

    #@3d
    invoke-static {v6, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    goto :goto_21

    #@41
    .line 326
    .end local v0           #fnfe:Ljava/io/FileNotFoundException;
    :catch_41
    move-exception v2

    #@42
    .line 327
    .local v2, ioe:Ljava/io/IOException;
    const-string v6, "LockSettingsService"

    #@44
    new-instance v7, Ljava/lang/StringBuilder;

    #@46
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@49
    const-string v8, "Cannot read file "

    #@4b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v7

    #@4f
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v7

    #@53
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@56
    move-result-object v7

    #@57
    invoke-static {v6, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5a
    goto :goto_21
.end method

.method public checkPattern([BI)Z
    .registers 12
    .parameter "hash"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 255
    invoke-static {p2}, Lcom/android/internal/widget/LockSettingsService;->checkPasswordReadPermission(I)V

    #@4
    .line 258
    :try_start_4
    new-instance v3, Ljava/io/RandomAccessFile;

    #@6
    invoke-direct {p0, p2}, Lcom/android/internal/widget/LockSettingsService;->getLockPatternFilename(I)Ljava/lang/String;

    #@9
    move-result-object v6

    #@a
    const-string v7, "r"

    #@c
    invoke-direct {v3, v6, v7}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@f
    .line 259
    .local v3, raf:Ljava/io/RandomAccessFile;
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->length()J

    #@12
    move-result-wide v6

    #@13
    long-to-int v6, v6

    #@14
    new-array v4, v6, [B

    #@16
    .line 260
    .local v4, stored:[B
    const/4 v6, 0x0

    #@17
    array-length v7, v4

    #@18
    invoke-virtual {v3, v4, v6, v7}, Ljava/io/RandomAccessFile;->read([BII)I

    #@1b
    move-result v1

    #@1c
    .line 261
    .local v1, got:I
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V

    #@1f
    .line 262
    if-gtz v1, :cond_22

    #@21
    .line 272
    .end local v1           #got:I
    .end local v3           #raf:Ljava/io/RandomAccessFile;
    .end local v4           #stored:[B
    :goto_21
    return v5

    #@22
    .line 266
    .restart local v1       #got:I
    .restart local v3       #raf:Ljava/io/RandomAccessFile;
    .restart local v4       #stored:[B
    :cond_22
    invoke-static {v4, p1}, Ljava/util/Arrays;->equals([B[B)Z
    :try_end_25
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_25} :catch_27
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_25} :catch_41

    #@25
    move-result v5

    #@26
    goto :goto_21

    #@27
    .line 267
    .end local v1           #got:I
    .end local v3           #raf:Ljava/io/RandomAccessFile;
    .end local v4           #stored:[B
    :catch_27
    move-exception v0

    #@28
    .line 268
    .local v0, fnfe:Ljava/io/FileNotFoundException;
    const-string v6, "LockSettingsService"

    #@2a
    new-instance v7, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v8, "Cannot read file "

    #@31
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v7

    #@35
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v7

    #@39
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v7

    #@3d
    invoke-static {v6, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    goto :goto_21

    #@41
    .line 270
    .end local v0           #fnfe:Ljava/io/FileNotFoundException;
    :catch_41
    move-exception v2

    #@42
    .line 271
    .local v2, ioe:Ljava/io/IOException;
    const-string v6, "LockSettingsService"

    #@44
    new-instance v7, Ljava/lang/StringBuilder;

    #@46
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@49
    const-string v8, "Cannot read file "

    #@4b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v7

    #@4f
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v7

    #@53
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@56
    move-result-object v7

    #@57
    invoke-static {v6, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5a
    goto :goto_21
.end method

.method public checkPatternKidsMode([BI)Z
    .registers 12
    .parameter "hash"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 279
    invoke-static {p2}, Lcom/android/internal/widget/LockSettingsService;->checkPasswordReadPermission(I)V

    #@4
    .line 282
    :try_start_4
    new-instance v3, Ljava/io/RandomAccessFile;

    #@6
    invoke-direct {p0, p2}, Lcom/android/internal/widget/LockSettingsService;->getLockPatternKidsModeFilename(I)Ljava/lang/String;

    #@9
    move-result-object v6

    #@a
    const-string v7, "r"

    #@c
    invoke-direct {v3, v6, v7}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@f
    .line 283
    .local v3, raf:Ljava/io/RandomAccessFile;
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->length()J

    #@12
    move-result-wide v6

    #@13
    long-to-int v6, v6

    #@14
    new-array v4, v6, [B

    #@16
    .line 284
    .local v4, stored:[B
    const/4 v6, 0x0

    #@17
    array-length v7, v4

    #@18
    invoke-virtual {v3, v4, v6, v7}, Ljava/io/RandomAccessFile;->read([BII)I

    #@1b
    move-result v1

    #@1c
    .line 285
    .local v1, got:I
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V

    #@1f
    .line 286
    if-gtz v1, :cond_22

    #@21
    .line 296
    .end local v1           #got:I
    .end local v3           #raf:Ljava/io/RandomAccessFile;
    .end local v4           #stored:[B
    :goto_21
    return v5

    #@22
    .line 290
    .restart local v1       #got:I
    .restart local v3       #raf:Ljava/io/RandomAccessFile;
    .restart local v4       #stored:[B
    :cond_22
    invoke-static {v4, p1}, Ljava/util/Arrays;->equals([B[B)Z
    :try_end_25
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_25} :catch_27
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_25} :catch_41

    #@25
    move-result v5

    #@26
    goto :goto_21

    #@27
    .line 291
    .end local v1           #got:I
    .end local v3           #raf:Ljava/io/RandomAccessFile;
    .end local v4           #stored:[B
    :catch_27
    move-exception v0

    #@28
    .line 292
    .local v0, fnfe:Ljava/io/FileNotFoundException;
    const-string v6, "LockSettingsService"

    #@2a
    new-instance v7, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v8, "Cannot read file "

    #@31
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v7

    #@35
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v7

    #@39
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v7

    #@3d
    invoke-static {v6, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    goto :goto_21

    #@41
    .line 294
    .end local v0           #fnfe:Ljava/io/FileNotFoundException;
    :catch_41
    move-exception v2

    #@42
    .line 295
    .local v2, ioe:Ljava/io/IOException;
    const-string v6, "LockSettingsService"

    #@44
    new-instance v7, Ljava/lang/StringBuilder;

    #@46
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@49
    const-string v8, "Cannot read file "

    #@4b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v7

    #@4f
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v7

    #@53
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@56
    move-result-object v7

    #@57
    invoke-static {v6, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5a
    goto :goto_21
.end method

.method public getBoolean(Ljava/lang/String;ZI)Z
    .registers 6
    .parameter "key"
    .parameter "defaultValue"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 152
    const/4 v1, 0x0

    #@1
    invoke-direct {p0, p1, v1, p3}, Lcom/android/internal/widget/LockSettingsService;->readFromDb(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    #@4
    move-result-object v0

    #@5
    .line 153
    .local v0, value:Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@8
    move-result v1

    #@9
    if-eqz v1, :cond_c

    #@b
    .end local p2
    :goto_b
    return p2

    #@c
    .restart local p2
    :cond_c
    const-string v1, "1"

    #@e
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11
    move-result v1

    #@12
    if-nez v1, :cond_1c

    #@14
    const-string v1, "true"

    #@16
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@19
    move-result v1

    #@1a
    if-eqz v1, :cond_1e

    #@1c
    :cond_1c
    const/4 p2, 0x1

    #@1d
    goto :goto_b

    #@1e
    :cond_1e
    const/4 p2, 0x0

    #@1f
    goto :goto_b
.end method

.method public getLong(Ljava/lang/String;JI)J
    .registers 7
    .parameter "key"
    .parameter "defaultValue"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 161
    const/4 v1, 0x0

    #@1
    invoke-direct {p0, p1, v1, p4}, Lcom/android/internal/widget/LockSettingsService;->readFromDb(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    #@4
    move-result-object v0

    #@5
    .line 162
    .local v0, value:Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@8
    move-result v1

    #@9
    if-eqz v1, :cond_c

    #@b
    .end local p2
    :goto_b
    return-wide p2

    #@c
    .restart local p2
    :cond_c
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@f
    move-result-wide p2

    #@10
    goto :goto_b
.end method

.method public getString(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .registers 5
    .parameter "key"
    .parameter "defaultValue"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 169
    invoke-direct {p0, p1, p2, p3}, Lcom/android/internal/widget/LockSettingsService;->readFromDb(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public havePassword(I)Z
    .registers 6
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 217
    new-instance v0, Ljava/io/File;

    #@2
    invoke-direct {p0, p1}, Lcom/android/internal/widget/LockSettingsService;->getLockPasswordFilename(I)Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@9
    invoke-virtual {v0}, Ljava/io/File;->length()J

    #@c
    move-result-wide v0

    #@d
    const-wide/16 v2, 0x0

    #@f
    cmp-long v0, v0, v2

    #@11
    if-lez v0, :cond_15

    #@13
    const/4 v0, 0x1

    #@14
    :goto_14
    return v0

    #@15
    :cond_15
    const/4 v0, 0x0

    #@16
    goto :goto_14
.end method

.method public havePattern(I)Z
    .registers 6
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 224
    new-instance v0, Ljava/io/File;

    #@2
    invoke-direct {p0, p1}, Lcom/android/internal/widget/LockSettingsService;->getLockPatternFilename(I)Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@9
    invoke-virtual {v0}, Ljava/io/File;->length()J

    #@c
    move-result-wide v0

    #@d
    const-wide/16 v2, 0x0

    #@f
    cmp-long v0, v0, v2

    #@11
    if-lez v0, :cond_15

    #@13
    const/4 v0, 0x1

    #@14
    :goto_14
    return v0

    #@15
    :cond_15
    const/4 v0, 0x0

    #@16
    goto :goto_14
.end method

.method public havePatternKidsMode(I)Z
    .registers 6
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 233
    new-instance v0, Ljava/io/File;

    #@2
    invoke-direct {p0, p1}, Lcom/android/internal/widget/LockSettingsService;->getLockPatternKidsModeFilename(I)Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@9
    invoke-virtual {v0}, Ljava/io/File;->length()J

    #@c
    move-result-wide v0

    #@d
    const-wide/16 v2, 0x0

    #@f
    cmp-long v0, v0, v2

    #@11
    if-lez v0, :cond_15

    #@13
    const/4 v0, 0x1

    #@14
    :goto_14
    return v0

    #@15
    :cond_15
    const/4 v0, 0x0

    #@16
    goto :goto_14
.end method

.method public removeUser(I)V
    .registers 7
    .parameter "userId"

    #@0
    .prologue
    .line 334
    invoke-static {p1}, Lcom/android/internal/widget/LockSettingsService;->checkWritePermission(I)V

    #@3
    .line 336
    iget-object v2, p0, Lcom/android/internal/widget/LockSettingsService;->mOpenHelper:Lcom/android/internal/widget/LockSettingsService$DatabaseHelper;

    #@5
    invoke-virtual {v2}, Lcom/android/internal/widget/LockSettingsService$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    #@8
    move-result-object v0

    #@9
    .line 338
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    :try_start_9
    new-instance v1, Ljava/io/File;

    #@b
    invoke-direct {p0, p1}, Lcom/android/internal/widget/LockSettingsService;->getLockPasswordFilename(I)Ljava/lang/String;

    #@e
    move-result-object v2

    #@f
    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@12
    .line 339
    .local v1, file:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    #@15
    move-result v2

    #@16
    if-eqz v2, :cond_1b

    #@18
    .line 340
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    #@1b
    .line 342
    :cond_1b
    new-instance v1, Ljava/io/File;

    #@1d
    .end local v1           #file:Ljava/io/File;
    invoke-direct {p0, p1}, Lcom/android/internal/widget/LockSettingsService;->getLockPatternFilename(I)Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@24
    .line 343
    .restart local v1       #file:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    #@27
    move-result v2

    #@28
    if-eqz v2, :cond_2d

    #@2a
    .line 344
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    #@2d
    .line 347
    :cond_2d
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    #@30
    .line 348
    const-string v2, "locksettings"

    #@32
    new-instance v3, Ljava/lang/StringBuilder;

    #@34
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@37
    const-string v4, "user=\'"

    #@39
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v3

    #@3d
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40
    move-result-object v3

    #@41
    const-string v4, "\'"

    #@43
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v3

    #@47
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v3

    #@4b
    const/4 v4, 0x0

    #@4c
    invoke-virtual {v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    #@4f
    .line 349
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_52
    .catchall {:try_start_9 .. :try_end_52} :catchall_56

    #@52
    .line 351
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@55
    .line 353
    return-void

    #@56
    .line 351
    .end local v1           #file:Ljava/io/File;
    :catchall_56
    move-exception v2

    #@57
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    #@5a
    throw v2
.end method

.method public setBoolean(Ljava/lang/String;ZI)V
    .registers 5
    .parameter "key"
    .parameter "value"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 129
    invoke-static {p3}, Lcom/android/internal/widget/LockSettingsService;->checkWritePermission(I)V

    #@3
    .line 131
    if-eqz p2, :cond_b

    #@5
    const-string v0, "1"

    #@7
    :goto_7
    invoke-direct {p0, p1, v0, p3}, Lcom/android/internal/widget/LockSettingsService;->writeToDb(Ljava/lang/String;Ljava/lang/String;I)V

    #@a
    .line 132
    return-void

    #@b
    .line 131
    :cond_b
    const-string v0, "0"

    #@d
    goto :goto_7
.end method

.method public setLockPassword([BI)V
    .registers 4
    .parameter "hash"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 303
    invoke-static {p2}, Lcom/android/internal/widget/LockSettingsService;->checkWritePermission(I)V

    #@3
    .line 305
    invoke-direct {p0, p2}, Lcom/android/internal/widget/LockSettingsService;->getLockPasswordFilename(I)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    invoke-direct {p0, v0, p1}, Lcom/android/internal/widget/LockSettingsService;->writeFile(Ljava/lang/String;[B)V

    #@a
    .line 306
    return-void
.end method

.method public setLockPattern([BI)V
    .registers 4
    .parameter "hash"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 239
    invoke-static {p2}, Lcom/android/internal/widget/LockSettingsService;->checkWritePermission(I)V

    #@3
    .line 241
    invoke-direct {p0, p2}, Lcom/android/internal/widget/LockSettingsService;->getLockPatternFilename(I)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    invoke-direct {p0, v0, p1}, Lcom/android/internal/widget/LockSettingsService;->writeFile(Ljava/lang/String;[B)V

    #@a
    .line 242
    return-void
.end method

.method public setLockPatternKidsMode([BI)V
    .registers 4
    .parameter "hash"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 247
    invoke-static {p2}, Lcom/android/internal/widget/LockSettingsService;->checkWritePermission(I)V

    #@3
    .line 249
    invoke-direct {p0, p2}, Lcom/android/internal/widget/LockSettingsService;->getLockPatternKidsModeFilename(I)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    invoke-direct {p0, v0, p1}, Lcom/android/internal/widget/LockSettingsService;->writeFile(Ljava/lang/String;[B)V

    #@a
    .line 250
    return-void
.end method

.method public setLong(Ljava/lang/String;JI)V
    .registers 6
    .parameter "key"
    .parameter "value"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 136
    invoke-static {p4}, Lcom/android/internal/widget/LockSettingsService;->checkWritePermission(I)V

    #@3
    .line 138
    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    invoke-direct {p0, p1, v0, p4}, Lcom/android/internal/widget/LockSettingsService;->writeToDb(Ljava/lang/String;Ljava/lang/String;I)V

    #@a
    .line 139
    return-void
.end method

.method public setString(Ljava/lang/String;Ljava/lang/String;I)V
    .registers 4
    .parameter "key"
    .parameter "value"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 143
    invoke-static {p3}, Lcom/android/internal/widget/LockSettingsService;->checkWritePermission(I)V

    #@3
    .line 145
    invoke-direct {p0, p1, p2, p3}, Lcom/android/internal/widget/LockSettingsService;->writeToDb(Ljava/lang/String;Ljava/lang/String;I)V

    #@6
    .line 146
    return-void
.end method

.method public systemReady()V
    .registers 1

    #@0
    .prologue
    .line 77
    invoke-direct {p0}, Lcom/android/internal/widget/LockSettingsService;->migrateOldData()V

    #@3
    .line 78
    return-void
.end method
