.class public Lcom/android/internal/widget/PasswordEntryKeyboardHelper;
.super Ljava/lang/Object;
.source "PasswordEntryKeyboardHelper.java"

# interfaces
.implements Landroid/inputmethodservice/KeyboardView$OnKeyboardActionListener;


# static fields
.field public static final KEYBOARD_MODE_ALPHA:I = 0x0

.field public static final KEYBOARD_MODE_NUMERIC:I = 0x1

.field private static final KEYBOARD_STATE_CAPSLOCK:I = 0x2

.field private static final KEYBOARD_STATE_NORMAL:I = 0x0

.field private static final KEYBOARD_STATE_SHIFTED:I = 0x1

.field private static final NUMERIC:I = 0x0

.field private static final QWERTY:I = 0x1

.field private static final QWERTY_SHIFTED:I = 0x2

.field private static final SYMBOLS:I = 0x3

.field private static final SYMBOLS_SHIFTED:I = 0x4

.field private static final TAG:Ljava/lang/String; = "PasswordEntryKeyboardHelper"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mEnableHaptics:Z

.field private mKeyboardMode:I

.field private mKeyboardState:I

.field private final mKeyboardView:Landroid/inputmethodservice/KeyboardView;

.field mLayouts:[I

.field private mNumericKeyboard:Lcom/android/internal/widget/PasswordEntryKeyboard;

.field private mQwertyKeyboard:Lcom/android/internal/widget/PasswordEntryKeyboard;

.field private mQwertyKeyboardShifted:Lcom/android/internal/widget/PasswordEntryKeyboard;

.field private mSymbolsKeyboard:Lcom/android/internal/widget/PasswordEntryKeyboard;

.field private mSymbolsKeyboardShifted:Lcom/android/internal/widget/PasswordEntryKeyboard;

.field private final mTargetView:Landroid/view/View;

.field private mUsingScreenWidth:Z

.field private mVibratePattern:[J


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/inputmethodservice/KeyboardView;Landroid/view/View;)V
    .registers 10
    .parameter "context"
    .parameter "keyboardView"
    .parameter "targetView"

    #@0
    .prologue
    .line 76
    const/4 v4, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    move-object v0, p0

    #@3
    move-object v1, p1

    #@4
    move-object v2, p2

    #@5
    move-object v3, p3

    #@6
    invoke-direct/range {v0 .. v5}, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;-><init>(Landroid/content/Context;Landroid/inputmethodservice/KeyboardView;Landroid/view/View;Z[I)V

    #@9
    .line 77
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/inputmethodservice/KeyboardView;Landroid/view/View;Z)V
    .registers 11
    .parameter "context"
    .parameter "keyboardView"
    .parameter "targetView"
    .parameter "useFullScreenWidth"

    #@0
    .prologue
    .line 81
    const/4 v5, 0x0

    #@1
    move-object v0, p0

    #@2
    move-object v1, p1

    #@3
    move-object v2, p2

    #@4
    move-object v3, p3

    #@5
    move v4, p4

    #@6
    invoke-direct/range {v0 .. v5}, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;-><init>(Landroid/content/Context;Landroid/inputmethodservice/KeyboardView;Landroid/view/View;Z[I)V

    #@9
    .line 82
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/inputmethodservice/KeyboardView;Landroid/view/View;Z[I)V
    .registers 9
    .parameter "context"
    .parameter "keyboardView"
    .parameter "targetView"
    .parameter "useFullScreenWidth"
    .parameter "layouts"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 85
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 46
    iput v1, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mKeyboardMode:I

    #@6
    .line 47
    iput v1, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mKeyboardState:I

    #@8
    .line 57
    iput-boolean v1, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mEnableHaptics:Z

    #@a
    .line 65
    const/4 v1, 0x5

    #@b
    new-array v1, v1, [I

    #@d
    fill-array-data v1, :array_42

    #@10
    iput-object v1, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mLayouts:[I

    #@12
    .line 86
    iput-object p1, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mContext:Landroid/content/Context;

    #@14
    .line 87
    iput-object p3, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mTargetView:Landroid/view/View;

    #@16
    .line 88
    iput-object p2, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mKeyboardView:Landroid/inputmethodservice/KeyboardView;

    #@18
    .line 89
    iget-object v1, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mKeyboardView:Landroid/inputmethodservice/KeyboardView;

    #@1a
    invoke-virtual {v1, p0}, Landroid/inputmethodservice/KeyboardView;->setOnKeyboardActionListener(Landroid/inputmethodservice/KeyboardView$OnKeyboardActionListener;)V

    #@1d
    .line 90
    iput-boolean p4, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mUsingScreenWidth:Z

    #@1f
    .line 91
    if-eqz p5, :cond_3e

    #@21
    .line 92
    array-length v1, p5

    #@22
    iget-object v2, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mLayouts:[I

    #@24
    array-length v2, v2

    #@25
    if-eq v1, v2, :cond_2f

    #@27
    .line 93
    new-instance v1, Ljava/lang/RuntimeException;

    #@29
    const-string v2, "Wrong number of layouts"

    #@2b
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@2e
    throw v1

    #@2f
    .line 95
    :cond_2f
    const/4 v0, 0x0

    #@30
    .local v0, i:I
    :goto_30
    iget-object v1, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mLayouts:[I

    #@32
    array-length v1, v1

    #@33
    if-ge v0, v1, :cond_3e

    #@35
    .line 96
    iget-object v1, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mLayouts:[I

    #@37
    aget v2, p5, v0

    #@39
    aput v2, v1, v0

    #@3b
    .line 95
    add-int/lit8 v0, v0, 0x1

    #@3d
    goto :goto_30

    #@3e
    .line 99
    .end local v0           #i:I
    :cond_3e
    invoke-virtual {p0}, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->createKeyboards()V

    #@41
    .line 100
    return-void

    #@42
    .line 65
    :array_42
    .array-data 0x4
        0x6t 0x0t 0xft 0x1t
        0x8t 0x0t 0xft 0x1t
        0x9t 0x0t 0xft 0x1t
        0xat 0x0t 0xft 0x1t
        0xbt 0x0t 0xft 0x1t
    .end array-data
.end method

.method private createKeyboardsWithDefaultWidth()V
    .registers 7

    #@0
    .prologue
    const v5, 0x10203c9

    #@3
    const/4 v4, 0x1

    #@4
    .line 140
    new-instance v0, Lcom/android/internal/widget/PasswordEntryKeyboard;

    #@6
    iget-object v1, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mContext:Landroid/content/Context;

    #@8
    iget-object v2, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mLayouts:[I

    #@a
    const/4 v3, 0x0

    #@b
    aget v2, v2, v3

    #@d
    invoke-direct {v0, v1, v2}, Lcom/android/internal/widget/PasswordEntryKeyboard;-><init>(Landroid/content/Context;I)V

    #@10
    iput-object v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mNumericKeyboard:Lcom/android/internal/widget/PasswordEntryKeyboard;

    #@12
    .line 141
    new-instance v0, Lcom/android/internal/widget/PasswordEntryKeyboard;

    #@14
    iget-object v1, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mContext:Landroid/content/Context;

    #@16
    iget-object v2, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mLayouts:[I

    #@18
    aget v2, v2, v4

    #@1a
    invoke-direct {v0, v1, v2, v5}, Lcom/android/internal/widget/PasswordEntryKeyboard;-><init>(Landroid/content/Context;II)V

    #@1d
    iput-object v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mQwertyKeyboard:Lcom/android/internal/widget/PasswordEntryKeyboard;

    #@1f
    .line 142
    iget-object v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mQwertyKeyboard:Lcom/android/internal/widget/PasswordEntryKeyboard;

    #@21
    invoke-virtual {v0}, Lcom/android/internal/widget/PasswordEntryKeyboard;->enableShiftLock()V

    #@24
    .line 144
    new-instance v0, Lcom/android/internal/widget/PasswordEntryKeyboard;

    #@26
    iget-object v1, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mContext:Landroid/content/Context;

    #@28
    iget-object v2, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mLayouts:[I

    #@2a
    const/4 v3, 0x2

    #@2b
    aget v2, v2, v3

    #@2d
    invoke-direct {v0, v1, v2, v5}, Lcom/android/internal/widget/PasswordEntryKeyboard;-><init>(Landroid/content/Context;II)V

    #@30
    iput-object v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mQwertyKeyboardShifted:Lcom/android/internal/widget/PasswordEntryKeyboard;

    #@32
    .line 146
    iget-object v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mQwertyKeyboardShifted:Lcom/android/internal/widget/PasswordEntryKeyboard;

    #@34
    invoke-virtual {v0}, Lcom/android/internal/widget/PasswordEntryKeyboard;->enableShiftLock()V

    #@37
    .line 147
    iget-object v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mQwertyKeyboardShifted:Lcom/android/internal/widget/PasswordEntryKeyboard;

    #@39
    invoke-virtual {v0, v4}, Lcom/android/internal/widget/PasswordEntryKeyboard;->setShifted(Z)Z

    #@3c
    .line 149
    new-instance v0, Lcom/android/internal/widget/PasswordEntryKeyboard;

    #@3e
    iget-object v1, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mContext:Landroid/content/Context;

    #@40
    iget-object v2, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mLayouts:[I

    #@42
    const/4 v3, 0x3

    #@43
    aget v2, v2, v3

    #@45
    invoke-direct {v0, v1, v2}, Lcom/android/internal/widget/PasswordEntryKeyboard;-><init>(Landroid/content/Context;I)V

    #@48
    iput-object v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mSymbolsKeyboard:Lcom/android/internal/widget/PasswordEntryKeyboard;

    #@4a
    .line 150
    iget-object v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mSymbolsKeyboard:Lcom/android/internal/widget/PasswordEntryKeyboard;

    #@4c
    invoke-virtual {v0}, Lcom/android/internal/widget/PasswordEntryKeyboard;->enableShiftLock()V

    #@4f
    .line 152
    new-instance v0, Lcom/android/internal/widget/PasswordEntryKeyboard;

    #@51
    iget-object v1, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mContext:Landroid/content/Context;

    #@53
    iget-object v2, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mLayouts:[I

    #@55
    const/4 v3, 0x4

    #@56
    aget v2, v2, v3

    #@58
    invoke-direct {v0, v1, v2}, Lcom/android/internal/widget/PasswordEntryKeyboard;-><init>(Landroid/content/Context;I)V

    #@5b
    iput-object v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mSymbolsKeyboardShifted:Lcom/android/internal/widget/PasswordEntryKeyboard;

    #@5d
    .line 153
    iget-object v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mSymbolsKeyboardShifted:Lcom/android/internal/widget/PasswordEntryKeyboard;

    #@5f
    invoke-virtual {v0}, Lcom/android/internal/widget/PasswordEntryKeyboard;->enableShiftLock()V

    #@62
    .line 154
    iget-object v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mSymbolsKeyboardShifted:Lcom/android/internal/widget/PasswordEntryKeyboard;

    #@64
    invoke-virtual {v0, v4}, Lcom/android/internal/widget/PasswordEntryKeyboard;->setShifted(Z)Z

    #@67
    .line 155
    return-void
.end method

.method private createKeyboardsWithSpecificSize(II)V
    .registers 10
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    const v3, 0x10203c9

    #@3
    const/4 v6, 0x1

    #@4
    .line 120
    new-instance v0, Lcom/android/internal/widget/PasswordEntryKeyboard;

    #@6
    iget-object v1, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mContext:Landroid/content/Context;

    #@8
    iget-object v2, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mLayouts:[I

    #@a
    const/4 v4, 0x0

    #@b
    aget v2, v2, v4

    #@d
    invoke-direct {v0, v1, v2, p1, p2}, Lcom/android/internal/widget/PasswordEntryKeyboard;-><init>(Landroid/content/Context;III)V

    #@10
    iput-object v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mNumericKeyboard:Lcom/android/internal/widget/PasswordEntryKeyboard;

    #@12
    .line 121
    new-instance v0, Lcom/android/internal/widget/PasswordEntryKeyboard;

    #@14
    iget-object v1, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mContext:Landroid/content/Context;

    #@16
    iget-object v2, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mLayouts:[I

    #@18
    aget v2, v2, v6

    #@1a
    move v4, p1

    #@1b
    move v5, p2

    #@1c
    invoke-direct/range {v0 .. v5}, Lcom/android/internal/widget/PasswordEntryKeyboard;-><init>(Landroid/content/Context;IIII)V

    #@1f
    iput-object v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mQwertyKeyboard:Lcom/android/internal/widget/PasswordEntryKeyboard;

    #@21
    .line 123
    iget-object v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mQwertyKeyboard:Lcom/android/internal/widget/PasswordEntryKeyboard;

    #@23
    invoke-virtual {v0}, Lcom/android/internal/widget/PasswordEntryKeyboard;->enableShiftLock()V

    #@26
    .line 125
    new-instance v0, Lcom/android/internal/widget/PasswordEntryKeyboard;

    #@28
    iget-object v1, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mContext:Landroid/content/Context;

    #@2a
    iget-object v2, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mLayouts:[I

    #@2c
    const/4 v4, 0x2

    #@2d
    aget v2, v2, v4

    #@2f
    move v4, p1

    #@30
    move v5, p2

    #@31
    invoke-direct/range {v0 .. v5}, Lcom/android/internal/widget/PasswordEntryKeyboard;-><init>(Landroid/content/Context;IIII)V

    #@34
    iput-object v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mQwertyKeyboardShifted:Lcom/android/internal/widget/PasswordEntryKeyboard;

    #@36
    .line 127
    iget-object v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mQwertyKeyboardShifted:Lcom/android/internal/widget/PasswordEntryKeyboard;

    #@38
    invoke-virtual {v0}, Lcom/android/internal/widget/PasswordEntryKeyboard;->enableShiftLock()V

    #@3b
    .line 128
    iget-object v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mQwertyKeyboardShifted:Lcom/android/internal/widget/PasswordEntryKeyboard;

    #@3d
    invoke-virtual {v0, v6}, Lcom/android/internal/widget/PasswordEntryKeyboard;->setShifted(Z)Z

    #@40
    .line 130
    new-instance v0, Lcom/android/internal/widget/PasswordEntryKeyboard;

    #@42
    iget-object v1, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mContext:Landroid/content/Context;

    #@44
    iget-object v2, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mLayouts:[I

    #@46
    const/4 v3, 0x3

    #@47
    aget v2, v2, v3

    #@49
    invoke-direct {v0, v1, v2, p1, p2}, Lcom/android/internal/widget/PasswordEntryKeyboard;-><init>(Landroid/content/Context;III)V

    #@4c
    iput-object v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mSymbolsKeyboard:Lcom/android/internal/widget/PasswordEntryKeyboard;

    #@4e
    .line 131
    iget-object v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mSymbolsKeyboard:Lcom/android/internal/widget/PasswordEntryKeyboard;

    #@50
    invoke-virtual {v0}, Lcom/android/internal/widget/PasswordEntryKeyboard;->enableShiftLock()V

    #@53
    .line 133
    new-instance v0, Lcom/android/internal/widget/PasswordEntryKeyboard;

    #@55
    iget-object v1, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mContext:Landroid/content/Context;

    #@57
    iget-object v2, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mLayouts:[I

    #@59
    const/4 v3, 0x4

    #@5a
    aget v2, v2, v3

    #@5c
    invoke-direct {v0, v1, v2, p1, p2}, Lcom/android/internal/widget/PasswordEntryKeyboard;-><init>(Landroid/content/Context;III)V

    #@5f
    iput-object v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mSymbolsKeyboardShifted:Lcom/android/internal/widget/PasswordEntryKeyboard;

    #@61
    .line 135
    iget-object v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mSymbolsKeyboardShifted:Lcom/android/internal/widget/PasswordEntryKeyboard;

    #@63
    invoke-virtual {v0}, Lcom/android/internal/widget/PasswordEntryKeyboard;->enableShiftLock()V

    #@66
    .line 136
    iget-object v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mSymbolsKeyboardShifted:Lcom/android/internal/widget/PasswordEntryKeyboard;

    #@68
    invoke-virtual {v0, v6}, Lcom/android/internal/widget/PasswordEntryKeyboard;->setShifted(Z)Z

    #@6b
    .line 137
    return-void
.end method

.method private handleCharacter(I[I)V
    .registers 4
    .parameter "primaryCode"
    .parameter "keyCodes"

    #@0
    .prologue
    .line 297
    iget-object v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mKeyboardView:Landroid/inputmethodservice/KeyboardView;

    #@2
    invoke-virtual {v0}, Landroid/inputmethodservice/KeyboardView;->isShifted()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_14

    #@8
    const/16 v0, 0x20

    #@a
    if-eq p1, v0, :cond_14

    #@c
    const/16 v0, 0xa

    #@e
    if-eq p1, v0, :cond_14

    #@10
    .line 298
    invoke-static {p1}, Ljava/lang/Character;->toUpperCase(I)I

    #@13
    move-result p1

    #@14
    .line 300
    :cond_14
    invoke-direct {p0, p1}, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->sendKeyEventsToTarget(I)V

    #@17
    .line 301
    return-void
.end method

.method private handleClose()V
    .registers 1

    #@0
    .prologue
    .line 305
    return-void
.end method

.method private handleModeChange()V
    .registers 4

    #@0
    .prologue
    .line 250
    iget-object v2, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mKeyboardView:Landroid/inputmethodservice/KeyboardView;

    #@2
    invoke-virtual {v2}, Landroid/inputmethodservice/KeyboardView;->getKeyboard()Landroid/inputmethodservice/Keyboard;

    #@5
    move-result-object v0

    #@6
    .line 251
    .local v0, current:Landroid/inputmethodservice/Keyboard;
    const/4 v1, 0x0

    #@7
    .line 252
    .local v1, next:Landroid/inputmethodservice/Keyboard;
    iget-object v2, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mQwertyKeyboard:Lcom/android/internal/widget/PasswordEntryKeyboard;

    #@9
    if-eq v0, v2, :cond_f

    #@b
    iget-object v2, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mQwertyKeyboardShifted:Lcom/android/internal/widget/PasswordEntryKeyboard;

    #@d
    if-ne v0, v2, :cond_1c

    #@f
    .line 253
    :cond_f
    iget-object v1, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mSymbolsKeyboard:Lcom/android/internal/widget/PasswordEntryKeyboard;

    #@11
    .line 257
    :cond_11
    :goto_11
    if-eqz v1, :cond_1b

    #@13
    .line 258
    iget-object v2, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mKeyboardView:Landroid/inputmethodservice/KeyboardView;

    #@15
    invoke-virtual {v2, v1}, Landroid/inputmethodservice/KeyboardView;->setKeyboard(Landroid/inputmethodservice/Keyboard;)V

    #@18
    .line 259
    const/4 v2, 0x0

    #@19
    iput v2, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mKeyboardState:I

    #@1b
    .line 261
    :cond_1b
    return-void

    #@1c
    .line 254
    :cond_1c
    iget-object v2, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mSymbolsKeyboard:Lcom/android/internal/widget/PasswordEntryKeyboard;

    #@1e
    if-eq v0, v2, :cond_24

    #@20
    iget-object v2, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mSymbolsKeyboardShifted:Lcom/android/internal/widget/PasswordEntryKeyboard;

    #@22
    if-ne v0, v2, :cond_11

    #@24
    .line 255
    :cond_24
    iget-object v1, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mQwertyKeyboard:Lcom/android/internal/widget/PasswordEntryKeyboard;

    #@26
    goto :goto_11
.end method

.method private handleShift()V
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x2

    #@1
    const/4 v3, 0x0

    #@2
    const/4 v4, 0x1

    #@3
    .line 269
    iget-object v5, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mKeyboardView:Landroid/inputmethodservice/KeyboardView;

    #@5
    if-nez v5, :cond_8

    #@7
    .line 293
    :cond_7
    :goto_7
    return-void

    #@8
    .line 272
    :cond_8
    iget-object v5, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mKeyboardView:Landroid/inputmethodservice/KeyboardView;

    #@a
    invoke-virtual {v5}, Landroid/inputmethodservice/KeyboardView;->getKeyboard()Landroid/inputmethodservice/Keyboard;

    #@d
    move-result-object v0

    #@e
    .line 273
    .local v0, current:Landroid/inputmethodservice/Keyboard;
    const/4 v2, 0x0

    #@f
    .line 274
    .local v2, next:Lcom/android/internal/widget/PasswordEntryKeyboard;
    iget-object v5, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mQwertyKeyboard:Lcom/android/internal/widget/PasswordEntryKeyboard;

    #@11
    if-eq v0, v5, :cond_17

    #@13
    iget-object v5, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mQwertyKeyboardShifted:Lcom/android/internal/widget/PasswordEntryKeyboard;

    #@15
    if-ne v0, v5, :cond_40

    #@17
    :cond_17
    move v1, v4

    #@18
    .line 276
    .local v1, isAlphaMode:Z
    :goto_18
    iget v5, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mKeyboardState:I

    #@1a
    if-nez v5, :cond_47

    #@1c
    .line 277
    if-eqz v1, :cond_42

    #@1e
    move v5, v4

    #@1f
    :goto_1f
    iput v5, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mKeyboardState:I

    #@21
    .line 278
    if-eqz v1, :cond_44

    #@23
    iget-object v2, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mQwertyKeyboardShifted:Lcom/android/internal/widget/PasswordEntryKeyboard;

    #@25
    .line 286
    :cond_25
    :goto_25
    if-eqz v2, :cond_7

    #@27
    .line 287
    if-eq v2, v0, :cond_2e

    #@29
    .line 288
    iget-object v5, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mKeyboardView:Landroid/inputmethodservice/KeyboardView;

    #@2b
    invoke-virtual {v5, v2}, Landroid/inputmethodservice/KeyboardView;->setKeyboard(Landroid/inputmethodservice/Keyboard;)V

    #@2e
    .line 290
    :cond_2e
    iget v5, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mKeyboardState:I

    #@30
    if-ne v5, v6, :cond_63

    #@32
    move v5, v4

    #@33
    :goto_33
    invoke-virtual {v2, v5}, Lcom/android/internal/widget/PasswordEntryKeyboard;->setShiftLocked(Z)V

    #@36
    .line 291
    iget-object v5, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mKeyboardView:Landroid/inputmethodservice/KeyboardView;

    #@38
    iget v6, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mKeyboardState:I

    #@3a
    if-eqz v6, :cond_65

    #@3c
    :goto_3c
    invoke-virtual {v5, v4}, Landroid/inputmethodservice/KeyboardView;->setShifted(Z)Z

    #@3f
    goto :goto_7

    #@40
    .end local v1           #isAlphaMode:Z
    :cond_40
    move v1, v3

    #@41
    .line 274
    goto :goto_18

    #@42
    .restart local v1       #isAlphaMode:Z
    :cond_42
    move v5, v6

    #@43
    .line 277
    goto :goto_1f

    #@44
    .line 278
    :cond_44
    iget-object v2, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mSymbolsKeyboardShifted:Lcom/android/internal/widget/PasswordEntryKeyboard;

    #@46
    goto :goto_25

    #@47
    .line 279
    :cond_47
    iget v5, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mKeyboardState:I

    #@49
    if-ne v5, v4, :cond_55

    #@4b
    .line 280
    iput v6, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mKeyboardState:I

    #@4d
    .line 281
    if-eqz v1, :cond_52

    #@4f
    iget-object v2, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mQwertyKeyboardShifted:Lcom/android/internal/widget/PasswordEntryKeyboard;

    #@51
    :goto_51
    goto :goto_25

    #@52
    :cond_52
    iget-object v2, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mSymbolsKeyboardShifted:Lcom/android/internal/widget/PasswordEntryKeyboard;

    #@54
    goto :goto_51

    #@55
    .line 282
    :cond_55
    iget v5, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mKeyboardState:I

    #@57
    if-ne v5, v6, :cond_25

    #@59
    .line 283
    iput v3, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mKeyboardState:I

    #@5b
    .line 284
    if-eqz v1, :cond_60

    #@5d
    iget-object v2, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mQwertyKeyboard:Lcom/android/internal/widget/PasswordEntryKeyboard;

    #@5f
    :goto_5f
    goto :goto_25

    #@60
    :cond_60
    iget-object v2, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mSymbolsKeyboard:Lcom/android/internal/widget/PasswordEntryKeyboard;

    #@62
    goto :goto_5f

    #@63
    :cond_63
    move v5, v3

    #@64
    .line 290
    goto :goto_33

    #@65
    :cond_65
    move v4, v3

    #@66
    .line 291
    goto :goto_3c
.end method

.method private performHapticFeedback()V
    .registers 4

    #@0
    .prologue
    .line 312
    iget-boolean v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mEnableHaptics:Z

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 313
    iget-object v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mKeyboardView:Landroid/inputmethodservice/KeyboardView;

    #@6
    const/4 v1, 0x1

    #@7
    const/4 v2, 0x3

    #@8
    invoke-virtual {v0, v1, v2}, Landroid/inputmethodservice/KeyboardView;->performHapticFeedback(II)Z

    #@b
    .line 317
    :cond_b
    return-void
.end method

.method private sendKeyEventsToTarget(I)V
    .registers 11
    .parameter "character"

    #@0
    .prologue
    .line 178
    iget-object v5, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mTargetView:Landroid/view/View;

    #@2
    invoke-virtual {v5}, Landroid/view/View;->getViewRootImpl()Landroid/view/ViewRootImpl;

    #@5
    move-result-object v4

    #@6
    .line 179
    .local v4, viewRootImpl:Landroid/view/ViewRootImpl;
    const/4 v5, -0x1

    #@7
    invoke-static {v5}, Landroid/view/KeyCharacterMap;->load(I)Landroid/view/KeyCharacterMap;

    #@a
    move-result-object v5

    #@b
    const/4 v6, 0x1

    #@c
    new-array v6, v6, [C

    #@e
    const/4 v7, 0x0

    #@f
    int-to-char v8, p1

    #@10
    aput-char v8, v6, v7

    #@12
    invoke-virtual {v5, v6}, Landroid/view/KeyCharacterMap;->getEvents([C)[Landroid/view/KeyEvent;

    #@15
    move-result-object v2

    #@16
    .line 181
    .local v2, events:[Landroid/view/KeyEvent;
    if-eqz v2, :cond_30

    #@18
    .line 182
    array-length v0, v2

    #@19
    .line 183
    .local v0, N:I
    const/4 v3, 0x0

    #@1a
    .local v3, i:I
    :goto_1a
    if-ge v3, v0, :cond_30

    #@1c
    .line 184
    aget-object v1, v2, v3

    #@1e
    .line 185
    .local v1, event:Landroid/view/KeyEvent;
    invoke-virtual {v1}, Landroid/view/KeyEvent;->getFlags()I

    #@21
    move-result v5

    #@22
    or-int/lit8 v5, v5, 0x2

    #@24
    or-int/lit8 v5, v5, 0x4

    #@26
    invoke-static {v1, v5}, Landroid/view/KeyEvent;->changeFlags(Landroid/view/KeyEvent;I)Landroid/view/KeyEvent;

    #@29
    move-result-object v1

    #@2a
    .line 187
    invoke-virtual {v4, v1}, Landroid/view/ViewRootImpl;->dispatchKey(Landroid/view/KeyEvent;)V

    #@2d
    .line 183
    add-int/lit8 v3, v3, 0x1

    #@2f
    goto :goto_1a

    #@30
    .line 190
    .end local v0           #N:I
    .end local v1           #event:Landroid/view/KeyEvent;
    .end local v3           #i:I
    :cond_30
    return-void
.end method


# virtual methods
.method public createKeyboards()V
    .registers 4

    #@0
    .prologue
    .line 103
    iget-object v1, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mKeyboardView:Landroid/inputmethodservice/KeyboardView;

    #@2
    invoke-virtual {v1}, Landroid/inputmethodservice/KeyboardView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@5
    move-result-object v0

    #@6
    .line 104
    .local v0, lp:Landroid/view/ViewGroup$LayoutParams;
    iget-boolean v1, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mUsingScreenWidth:Z

    #@8
    if-nez v1, :cond_f

    #@a
    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@c
    const/4 v2, -0x1

    #@d
    if-ne v1, v2, :cond_13

    #@f
    .line 105
    :cond_f
    invoke-direct {p0}, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->createKeyboardsWithDefaultWidth()V

    #@12
    .line 109
    :goto_12
    return-void

    #@13
    .line 107
    :cond_13
    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@15
    iget v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@17
    invoke-direct {p0, v1, v2}, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->createKeyboardsWithSpecificSize(II)V

    #@1a
    goto :goto_12
.end method

.method public handleBackspace()V
    .registers 2

    #@0
    .prologue
    .line 264
    const/16 v0, 0x43

    #@2
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->sendDownUpKeyEvents(I)V

    #@5
    .line 265
    invoke-direct {p0}, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->performHapticFeedback()V

    #@8
    .line 266
    return-void
.end method

.method public isAlpha()Z
    .registers 2

    #@0
    .prologue
    .line 116
    iget v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mKeyboardMode:I

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public onKey(I[I)V
    .registers 5
    .parameter "primaryCode"
    .parameter "keyCodes"

    #@0
    .prologue
    .line 206
    const/4 v0, -0x5

    #@1
    if-ne p1, v0, :cond_7

    #@3
    .line 207
    invoke-virtual {p0}, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->handleBackspace()V

    #@6
    .line 224
    :cond_6
    :goto_6
    return-void

    #@7
    .line 208
    :cond_7
    const/4 v0, -0x1

    #@8
    if-ne p1, v0, :cond_e

    #@a
    .line 209
    invoke-direct {p0}, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->handleShift()V

    #@d
    goto :goto_6

    #@e
    .line 210
    :cond_e
    const/4 v0, -0x3

    #@f
    if-ne p1, v0, :cond_15

    #@11
    .line 211
    invoke-direct {p0}, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->handleClose()V

    #@14
    goto :goto_6

    #@15
    .line 213
    :cond_15
    const/4 v0, -0x2

    #@16
    if-ne p1, v0, :cond_20

    #@18
    iget-object v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mKeyboardView:Landroid/inputmethodservice/KeyboardView;

    #@1a
    if-eqz v0, :cond_20

    #@1c
    .line 214
    invoke-direct {p0}, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->handleModeChange()V

    #@1f
    goto :goto_6

    #@20
    .line 216
    :cond_20
    invoke-direct {p0, p1, p2}, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->handleCharacter(I[I)V

    #@23
    .line 218
    iget v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mKeyboardState:I

    #@25
    const/4 v1, 0x1

    #@26
    if-ne v0, v1, :cond_6

    #@28
    .line 220
    const/4 v0, 0x2

    #@29
    iput v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mKeyboardState:I

    #@2b
    .line 221
    invoke-direct {p0}, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->handleShift()V

    #@2e
    goto :goto_6
.end method

.method public onPress(I)V
    .registers 2
    .parameter "primaryCode"

    #@0
    .prologue
    .line 308
    invoke-direct {p0}, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->performHapticFeedback()V

    #@3
    .line 309
    return-void
.end method

.method public onRelease(I)V
    .registers 2
    .parameter "primaryCode"

    #@0
    .prologue
    .line 321
    return-void
.end method

.method public onText(Ljava/lang/CharSequence;)V
    .registers 2
    .parameter "text"

    #@0
    .prologue
    .line 325
    return-void
.end method

.method public sendDownUpKeyEvents(I)V
    .registers 15
    .parameter "keyEventCode"

    #@0
    .prologue
    .line 193
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@3
    move-result-wide v1

    #@4
    .line 194
    .local v1, eventTime:J
    iget-object v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mTargetView:Landroid/view/View;

    #@6
    invoke-virtual {v0}, Landroid/view/View;->getViewRootImpl()Landroid/view/ViewRootImpl;

    #@9
    move-result-object v12

    #@a
    .line 195
    .local v12, viewRootImpl:Landroid/view/ViewRootImpl;
    new-instance v0, Landroid/view/KeyEvent;

    #@c
    const/4 v5, 0x0

    #@d
    const/4 v7, 0x0

    #@e
    const/4 v8, 0x0

    #@f
    const/4 v9, -0x1

    #@10
    const/4 v10, 0x0

    #@11
    const/4 v11, 0x6

    #@12
    move-wide v3, v1

    #@13
    move v6, p1

    #@14
    invoke-direct/range {v0 .. v11}, Landroid/view/KeyEvent;-><init>(JJIIIIIII)V

    #@17
    invoke-virtual {v12, v0}, Landroid/view/ViewRootImpl;->dispatchKeyFromIme(Landroid/view/KeyEvent;)V

    #@1a
    .line 199
    new-instance v0, Landroid/view/KeyEvent;

    #@1c
    const/4 v5, 0x1

    #@1d
    const/4 v7, 0x0

    #@1e
    const/4 v8, 0x0

    #@1f
    const/4 v9, -0x1

    #@20
    const/4 v10, 0x0

    #@21
    const/4 v11, 0x6

    #@22
    move-wide v3, v1

    #@23
    move v6, p1

    #@24
    invoke-direct/range {v0 .. v11}, Landroid/view/KeyEvent;-><init>(JJIIIIIII)V

    #@27
    invoke-virtual {v12, v0}, Landroid/view/ViewRootImpl;->dispatchKeyFromIme(Landroid/view/KeyEvent;)V

    #@2a
    .line 203
    return-void
.end method

.method public setEnableHaptics(Z)V
    .registers 2
    .parameter "enabled"

    #@0
    .prologue
    .line 112
    iput-boolean p1, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mEnableHaptics:Z

    #@2
    .line 113
    return-void
.end method

.method public setKeyboardMode(I)V
    .registers 7
    .parameter "mode"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 158
    packed-switch p1, :pswitch_data_3a

    #@5
    .line 174
    :goto_5
    iput p1, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mKeyboardMode:I

    #@7
    .line 175
    return-void

    #@8
    .line 160
    :pswitch_8
    iget-object v3, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mKeyboardView:Landroid/inputmethodservice/KeyboardView;

    #@a
    iget-object v4, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mQwertyKeyboard:Lcom/android/internal/widget/PasswordEntryKeyboard;

    #@c
    invoke-virtual {v3, v4}, Landroid/inputmethodservice/KeyboardView;->setKeyboard(Landroid/inputmethodservice/Keyboard;)V

    #@f
    .line 161
    iput v2, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mKeyboardState:I

    #@11
    .line 162
    iget-object v3, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mContext:Landroid/content/Context;

    #@13
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@16
    move-result-object v3

    #@17
    const-string v4, "show_password"

    #@19
    invoke-static {v3, v4, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@1c
    move-result v3

    #@1d
    if-eqz v3, :cond_28

    #@1f
    .line 165
    .local v1, visiblePassword:Z
    :goto_1f
    const/4 v0, 0x0

    #@20
    .line 166
    .local v0, enablePreview:Z
    iget-object v3, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mKeyboardView:Landroid/inputmethodservice/KeyboardView;

    #@22
    if-eqz v1, :cond_24

    #@24
    :cond_24
    invoke-virtual {v3, v2}, Landroid/inputmethodservice/KeyboardView;->setPreviewEnabled(Z)V

    #@27
    goto :goto_5

    #@28
    .end local v0           #enablePreview:Z
    .end local v1           #visiblePassword:Z
    :cond_28
    move v1, v2

    #@29
    .line 162
    goto :goto_1f

    #@2a
    .line 169
    :pswitch_2a
    iget-object v3, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mKeyboardView:Landroid/inputmethodservice/KeyboardView;

    #@2c
    iget-object v4, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mNumericKeyboard:Lcom/android/internal/widget/PasswordEntryKeyboard;

    #@2e
    invoke-virtual {v3, v4}, Landroid/inputmethodservice/KeyboardView;->setKeyboard(Landroid/inputmethodservice/Keyboard;)V

    #@31
    .line 170
    iput v2, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mKeyboardState:I

    #@33
    .line 171
    iget-object v3, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mKeyboardView:Landroid/inputmethodservice/KeyboardView;

    #@35
    invoke-virtual {v3, v2}, Landroid/inputmethodservice/KeyboardView;->setPreviewEnabled(Z)V

    #@38
    goto :goto_5

    #@39
    .line 158
    nop

    #@3a
    :pswitch_data_3a
    .packed-switch 0x0
        :pswitch_8
        :pswitch_2a
    .end packed-switch
.end method

.method public setVibratePattern(I)V
    .registers 8
    .parameter "id"

    #@0
    .prologue
    .line 231
    const/4 v2, 0x0

    #@1
    .line 233
    .local v2, tmpArray:[I
    :try_start_1
    iget-object v3, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mContext:Landroid/content/Context;

    #@3
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@6
    move-result-object v3

    #@7
    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->getIntArray(I)[I
    :try_end_a
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_a} :catch_11

    #@a
    move-result-object v2

    #@b
    .line 239
    :cond_b
    :goto_b
    if-nez v2, :cond_1c

    #@d
    .line 240
    const/4 v3, 0x0

    #@e
    iput-object v3, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mVibratePattern:[J

    #@10
    .line 247
    :cond_10
    return-void

    #@11
    .line 234
    :catch_11
    move-exception v0

    #@12
    .line 235
    .local v0, e:Landroid/content/res/Resources$NotFoundException;
    if-eqz p1, :cond_b

    #@14
    .line 236
    const-string v3, "PasswordEntryKeyboardHelper"

    #@16
    const-string v4, "Vibrate pattern missing"

    #@18
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1b
    goto :goto_b

    #@1c
    .line 243
    .end local v0           #e:Landroid/content/res/Resources$NotFoundException;
    :cond_1c
    array-length v3, v2

    #@1d
    new-array v3, v3, [J

    #@1f
    iput-object v3, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mVibratePattern:[J

    #@21
    .line 244
    const/4 v1, 0x0

    #@22
    .local v1, i:I
    :goto_22
    array-length v3, v2

    #@23
    if-ge v1, v3, :cond_10

    #@25
    .line 245
    iget-object v3, p0, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->mVibratePattern:[J

    #@27
    aget v4, v2, v1

    #@29
    int-to-long v4, v4

    #@2a
    aput-wide v4, v3, v1

    #@2c
    .line 244
    add-int/lit8 v1, v1, 0x1

    #@2e
    goto :goto_22
.end method

.method public swipeDown()V
    .registers 1

    #@0
    .prologue
    .line 329
    return-void
.end method

.method public swipeLeft()V
    .registers 1

    #@0
    .prologue
    .line 333
    return-void
.end method

.method public swipeRight()V
    .registers 1

    #@0
    .prologue
    .line 337
    return-void
.end method

.method public swipeUp()V
    .registers 1

    #@0
    .prologue
    .line 341
    return-void
.end method
