.class public Lcom/android/internal/widget/DigitalClock;
.super Landroid/widget/RelativeLayout;
.source "DigitalClock.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/widget/DigitalClock$FormatChangeObserver;,
        Lcom/android/internal/widget/DigitalClock$AmPm;,
        Lcom/android/internal/widget/DigitalClock$TimeChangedReceiver;
    }
.end annotation


# static fields
.field private static final M12:Ljava/lang/String; = "h:mm"

.field private static final M24:Ljava/lang/String; = "kk:mm"

.field private static final SYSTEM:Ljava/lang/String; = "/system/fonts/"

.field private static final SYSTEM_FONT_TIME_BACKGROUND:Ljava/lang/String; = "/system/fonts/AndroidClock.ttf"

.field private static final SYSTEM_FONT_TIME_FOREGROUND:Ljava/lang/String; = "/system/fonts/AndroidClock_Highlight.ttf"

.field private static final sBackgroundFont:Landroid/graphics/Typeface;

.field private static final sForegroundFont:Landroid/graphics/Typeface;


# instance fields
.field private mAmPm:Lcom/android/internal/widget/DigitalClock$AmPm;

.field private mAttached:I

.field private mCalendar:Ljava/util/Calendar;

.field private mFormat:Ljava/lang/String;

.field private mFormatChangeObserver:Landroid/database/ContentObserver;

.field private final mHandler:Landroid/os/Handler;

.field private mIntentReceiver:Landroid/content/BroadcastReceiver;

.field private mTimeDisplayBackground:Landroid/widget/TextView;

.field private mTimeDisplayForeground:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 66
    const-string v0, "/system/fonts/AndroidClock.ttf"

    #@2
    invoke-static {v0}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/android/internal/widget/DigitalClock;->sBackgroundFont:Landroid/graphics/Typeface;

    #@8
    .line 67
    const-string v0, "/system/fonts/AndroidClock_Highlight.ttf"

    #@a
    invoke-static {v0}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    #@d
    move-result-object v0

    #@e
    sput-object v0, Lcom/android/internal/widget/DigitalClock;->sForegroundFont:Landroid/graphics/Typeface;

    #@10
    .line 68
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 158
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Lcom/android/internal/widget/DigitalClock;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 159
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 162
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@3
    .line 56
    const/4 v0, 0x0

    #@4
    iput v0, p0, Lcom/android/internal/widget/DigitalClock;->mAttached:I

    #@6
    .line 59
    new-instance v0, Landroid/os/Handler;

    #@8
    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    #@b
    iput-object v0, p0, Lcom/android/internal/widget/DigitalClock;->mHandler:Landroid/os/Handler;

    #@d
    .line 163
    return-void
.end method

.method static synthetic access$002(Lcom/android/internal/widget/DigitalClock;Ljava/util/Calendar;)Ljava/util/Calendar;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 42
    iput-object p1, p0, Lcom/android/internal/widget/DigitalClock;->mCalendar:Ljava/util/Calendar;

    #@2
    return-object p1
.end method

.method static synthetic access$100(Lcom/android/internal/widget/DigitalClock;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 42
    iget-object v0, p0, Lcom/android/internal/widget/DigitalClock;->mHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Lcom/android/internal/widget/DigitalClock;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 42
    invoke-direct {p0}, Lcom/android/internal/widget/DigitalClock;->setDateFormat()V

    #@3
    return-void
.end method

.method private setDateFormat()V
    .registers 4

    #@0
    .prologue
    .line 241
    invoke-virtual {p0}, Lcom/android/internal/widget/DigitalClock;->getContext()Landroid/content/Context;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_1c

    #@a
    const-string v0, "kk:mm"

    #@c
    :goto_c
    iput-object v0, p0, Lcom/android/internal/widget/DigitalClock;->mFormat:Ljava/lang/String;

    #@e
    .line 243
    iget-object v0, p0, Lcom/android/internal/widget/DigitalClock;->mAmPm:Lcom/android/internal/widget/DigitalClock$AmPm;

    #@10
    iget-object v1, p0, Lcom/android/internal/widget/DigitalClock;->mFormat:Ljava/lang/String;

    #@12
    const-string v2, "h:mm"

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@17
    move-result v1

    #@18
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/DigitalClock$AmPm;->setShowAmPm(Z)V

    #@1b
    .line 244
    return-void

    #@1c
    .line 241
    :cond_1c
    const-string v0, "h:mm"

    #@1e
    goto :goto_c
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .registers 6

    #@0
    .prologue
    .line 184
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onAttachedToWindow()V

    #@3
    .line 186
    iget v1, p0, Lcom/android/internal/widget/DigitalClock;->mAttached:I

    #@5
    add-int/lit8 v1, v1, 0x1

    #@7
    iput v1, p0, Lcom/android/internal/widget/DigitalClock;->mAttached:I

    #@9
    .line 189
    iget-object v1, p0, Lcom/android/internal/widget/DigitalClock;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@b
    if-nez v1, :cond_2f

    #@d
    .line 190
    new-instance v1, Lcom/android/internal/widget/DigitalClock$TimeChangedReceiver;

    #@f
    invoke-direct {v1, p0}, Lcom/android/internal/widget/DigitalClock$TimeChangedReceiver;-><init>(Lcom/android/internal/widget/DigitalClock;)V

    #@12
    iput-object v1, p0, Lcom/android/internal/widget/DigitalClock;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@14
    .line 191
    new-instance v0, Landroid/content/IntentFilter;

    #@16
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@19
    .line 192
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.TIME_TICK"

    #@1b
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1e
    .line 193
    const-string v1, "android.intent.action.TIME_SET"

    #@20
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@23
    .line 194
    const-string v1, "android.intent.action.TIMEZONE_CHANGED"

    #@25
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@28
    .line 195
    iget-object v1, p0, Lcom/android/internal/widget/DigitalClock;->mContext:Landroid/content/Context;

    #@2a
    iget-object v2, p0, Lcom/android/internal/widget/DigitalClock;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@2c
    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@2f
    .line 199
    .end local v0           #filter:Landroid/content/IntentFilter;
    :cond_2f
    iget-object v1, p0, Lcom/android/internal/widget/DigitalClock;->mFormatChangeObserver:Landroid/database/ContentObserver;

    #@31
    if-nez v1, :cond_48

    #@33
    .line 200
    new-instance v1, Lcom/android/internal/widget/DigitalClock$FormatChangeObserver;

    #@35
    invoke-direct {v1, p0}, Lcom/android/internal/widget/DigitalClock$FormatChangeObserver;-><init>(Lcom/android/internal/widget/DigitalClock;)V

    #@38
    iput-object v1, p0, Lcom/android/internal/widget/DigitalClock;->mFormatChangeObserver:Landroid/database/ContentObserver;

    #@3a
    .line 201
    iget-object v1, p0, Lcom/android/internal/widget/DigitalClock;->mContext:Landroid/content/Context;

    #@3c
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@3f
    move-result-object v1

    #@40
    sget-object v2, Landroid/provider/Settings$System;->CONTENT_URI:Landroid/net/Uri;

    #@42
    const/4 v3, 0x1

    #@43
    iget-object v4, p0, Lcom/android/internal/widget/DigitalClock;->mFormatChangeObserver:Landroid/database/ContentObserver;

    #@45
    invoke-virtual {v1, v2, v3, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@48
    .line 205
    :cond_48
    invoke-virtual {p0}, Lcom/android/internal/widget/DigitalClock;->updateTime()V

    #@4b
    .line 206
    return-void
.end method

.method protected onDetachedFromWindow()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 210
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    #@4
    .line 212
    iget v0, p0, Lcom/android/internal/widget/DigitalClock;->mAttached:I

    #@6
    add-int/lit8 v0, v0, -0x1

    #@8
    iput v0, p0, Lcom/android/internal/widget/DigitalClock;->mAttached:I

    #@a
    .line 214
    iget-object v0, p0, Lcom/android/internal/widget/DigitalClock;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@c
    if-eqz v0, :cond_15

    #@e
    .line 215
    iget-object v0, p0, Lcom/android/internal/widget/DigitalClock;->mContext:Landroid/content/Context;

    #@10
    iget-object v1, p0, Lcom/android/internal/widget/DigitalClock;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@12
    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@15
    .line 217
    :cond_15
    iget-object v0, p0, Lcom/android/internal/widget/DigitalClock;->mFormatChangeObserver:Landroid/database/ContentObserver;

    #@17
    if-eqz v0, :cond_24

    #@19
    .line 218
    iget-object v0, p0, Lcom/android/internal/widget/DigitalClock;->mContext:Landroid/content/Context;

    #@1b
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1e
    move-result-object v0

    #@1f
    iget-object v1, p0, Lcom/android/internal/widget/DigitalClock;->mFormatChangeObserver:Landroid/database/ContentObserver;

    #@21
    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    #@24
    .line 222
    :cond_24
    iput-object v2, p0, Lcom/android/internal/widget/DigitalClock;->mFormatChangeObserver:Landroid/database/ContentObserver;

    #@26
    .line 223
    iput-object v2, p0, Lcom/android/internal/widget/DigitalClock;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@28
    .line 224
    return-void
.end method

.method protected onFinishInflate()V
    .registers 3

    #@0
    .prologue
    .line 167
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    #@3
    .line 170
    const v0, 0x10202f2

    #@6
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/DigitalClock;->findViewById(I)Landroid/view/View;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Landroid/widget/TextView;

    #@c
    iput-object v0, p0, Lcom/android/internal/widget/DigitalClock;->mTimeDisplayBackground:Landroid/widget/TextView;

    #@e
    .line 171
    iget-object v0, p0, Lcom/android/internal/widget/DigitalClock;->mTimeDisplayBackground:Landroid/widget/TextView;

    #@10
    sget-object v1, Lcom/android/internal/widget/DigitalClock;->sBackgroundFont:Landroid/graphics/Typeface;

    #@12
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    #@15
    .line 172
    iget-object v0, p0, Lcom/android/internal/widget/DigitalClock;->mTimeDisplayBackground:Landroid/widget/TextView;

    #@17
    const/4 v1, 0x4

    #@18
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    #@1b
    .line 174
    const v0, 0x10202f3

    #@1e
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/DigitalClock;->findViewById(I)Landroid/view/View;

    #@21
    move-result-object v0

    #@22
    check-cast v0, Landroid/widget/TextView;

    #@24
    iput-object v0, p0, Lcom/android/internal/widget/DigitalClock;->mTimeDisplayForeground:Landroid/widget/TextView;

    #@26
    .line 175
    iget-object v0, p0, Lcom/android/internal/widget/DigitalClock;->mTimeDisplayForeground:Landroid/widget/TextView;

    #@28
    sget-object v1, Lcom/android/internal/widget/DigitalClock;->sForegroundFont:Landroid/graphics/Typeface;

    #@2a
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    #@2d
    .line 176
    new-instance v0, Lcom/android/internal/widget/DigitalClock$AmPm;

    #@2f
    const/4 v1, 0x0

    #@30
    invoke-direct {v0, p0, v1}, Lcom/android/internal/widget/DigitalClock$AmPm;-><init>(Landroid/view/View;Landroid/graphics/Typeface;)V

    #@33
    iput-object v0, p0, Lcom/android/internal/widget/DigitalClock;->mAmPm:Lcom/android/internal/widget/DigitalClock$AmPm;

    #@35
    .line 177
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    #@38
    move-result-object v0

    #@39
    iput-object v0, p0, Lcom/android/internal/widget/DigitalClock;->mCalendar:Ljava/util/Calendar;

    #@3b
    .line 179
    invoke-direct {p0}, Lcom/android/internal/widget/DigitalClock;->setDateFormat()V

    #@3e
    .line 180
    return-void
.end method

.method public updateTime()V
    .registers 5

    #@0
    .prologue
    .line 232
    iget-object v1, p0, Lcom/android/internal/widget/DigitalClock;->mCalendar:Ljava/util/Calendar;

    #@2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@5
    move-result-wide v2

    #@6
    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    #@9
    .line 234
    iget-object v1, p0, Lcom/android/internal/widget/DigitalClock;->mFormat:Ljava/lang/String;

    #@b
    iget-object v2, p0, Lcom/android/internal/widget/DigitalClock;->mCalendar:Ljava/util/Calendar;

    #@d
    invoke-static {v1, v2}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    #@10
    move-result-object v0

    #@11
    .line 235
    .local v0, newTime:Ljava/lang/CharSequence;
    iget-object v1, p0, Lcom/android/internal/widget/DigitalClock;->mTimeDisplayBackground:Landroid/widget/TextView;

    #@13
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@16
    .line 236
    iget-object v1, p0, Lcom/android/internal/widget/DigitalClock;->mTimeDisplayForeground:Landroid/widget/TextView;

    #@18
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@1b
    .line 237
    iget-object v2, p0, Lcom/android/internal/widget/DigitalClock;->mAmPm:Lcom/android/internal/widget/DigitalClock$AmPm;

    #@1d
    iget-object v1, p0, Lcom/android/internal/widget/DigitalClock;->mCalendar:Ljava/util/Calendar;

    #@1f
    const/16 v3, 0x9

    #@21
    invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I

    #@24
    move-result v1

    #@25
    if-nez v1, :cond_2c

    #@27
    const/4 v1, 0x1

    #@28
    :goto_28
    invoke-virtual {v2, v1}, Lcom/android/internal/widget/DigitalClock$AmPm;->setIsMorning(Z)V

    #@2b
    .line 238
    return-void

    #@2c
    .line 237
    :cond_2c
    const/4 v1, 0x0

    #@2d
    goto :goto_28
.end method

.method updateTime(Ljava/util/Calendar;)V
    .registers 2
    .parameter "c"

    #@0
    .prologue
    .line 227
    iput-object p1, p0, Lcom/android/internal/widget/DigitalClock;->mCalendar:Ljava/util/Calendar;

    #@2
    .line 228
    invoke-virtual {p0}, Lcom/android/internal/widget/DigitalClock;->updateTime()V

    #@5
    .line 229
    return-void
.end method
