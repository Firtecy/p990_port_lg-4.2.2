.class Lcom/android/internal/widget/ScrollingTabContainerView$TabClickListener;
.super Ljava/lang/Object;
.source "ScrollingTabContainerView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/widget/ScrollingTabContainerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TabClickListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/widget/ScrollingTabContainerView;


# direct methods
.method private constructor <init>(Lcom/android/internal/widget/ScrollingTabContainerView;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 530
    iput-object p1, p0, Lcom/android/internal/widget/ScrollingTabContainerView$TabClickListener;->this$0:Lcom/android/internal/widget/ScrollingTabContainerView;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/internal/widget/ScrollingTabContainerView;Lcom/android/internal/widget/ScrollingTabContainerView$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 530
    invoke-direct {p0, p1}, Lcom/android/internal/widget/ScrollingTabContainerView$TabClickListener;-><init>(Lcom/android/internal/widget/ScrollingTabContainerView;)V

    #@3
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 7
    .parameter "view"

    #@0
    .prologue
    .line 532
    move-object v3, p1

    #@1
    check-cast v3, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;

    #@3
    .line 533
    .local v3, tabView:Lcom/android/internal/widget/ScrollingTabContainerView$TabView;
    invoke-virtual {v3}, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->getTab()Landroid/app/ActionBar$Tab;

    #@6
    move-result-object v4

    #@7
    invoke-virtual {v4}, Landroid/app/ActionBar$Tab;->select()V

    #@a
    .line 534
    iget-object v4, p0, Lcom/android/internal/widget/ScrollingTabContainerView$TabClickListener;->this$0:Lcom/android/internal/widget/ScrollingTabContainerView;

    #@c
    iget-object v4, v4, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabLayout:Landroid/widget/LinearLayout;

    #@e
    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getChildCount()I

    #@11
    move-result v2

    #@12
    .line 535
    .local v2, tabCount:I
    const/4 v1, 0x0

    #@13
    .local v1, i:I
    :goto_13
    if-ge v1, v2, :cond_28

    #@15
    .line 536
    iget-object v4, p0, Lcom/android/internal/widget/ScrollingTabContainerView$TabClickListener;->this$0:Lcom/android/internal/widget/ScrollingTabContainerView;

    #@17
    iget-object v4, v4, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabLayout:Landroid/widget/LinearLayout;

    #@19
    invoke-virtual {v4, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    #@1c
    move-result-object v0

    #@1d
    .line 537
    .local v0, child:Landroid/view/View;
    if-ne v0, p1, :cond_26

    #@1f
    const/4 v4, 0x1

    #@20
    :goto_20
    invoke-virtual {v0, v4}, Landroid/view/View;->setSelected(Z)V

    #@23
    .line 535
    add-int/lit8 v1, v1, 0x1

    #@25
    goto :goto_13

    #@26
    .line 537
    :cond_26
    const/4 v4, 0x0

    #@27
    goto :goto_20

    #@28
    .line 539
    .end local v0           #child:Landroid/view/View;
    :cond_28
    return-void
.end method
