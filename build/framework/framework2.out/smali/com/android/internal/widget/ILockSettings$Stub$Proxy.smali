.class Lcom/android/internal/widget/ILockSettings$Stub$Proxy;
.super Ljava/lang/Object;
.source "ILockSettings.java"

# interfaces
.implements Lcom/android/internal/widget/ILockSettings;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/widget/ILockSettings$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 242
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 243
    iput-object p1, p0, Lcom/android/internal/widget/ILockSettings$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 244
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 247
    iget-object v0, p0, Lcom/android/internal/widget/ILockSettings$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public checkPassword([BI)Z
    .registers 9
    .parameter "hash"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 452
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 453
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 456
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "com.android.internal.widget.ILockSettings"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 457
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeByteArray([B)V

    #@11
    .line 458
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 459
    iget-object v3, p0, Lcom/android/internal/widget/ILockSettings$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@16
    const/16 v4, 0xc

    #@18
    const/4 v5, 0x0

    #@19
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1c
    .line 460
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1f
    .line 461
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_22
    .catchall {:try_start_9 .. :try_end_22} :catchall_2d

    #@22
    move-result v3

    #@23
    if-eqz v3, :cond_26

    #@25
    const/4 v2, 0x1

    #@26
    .line 464
    .local v2, _result:Z
    :cond_26
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 465
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 467
    return v2

    #@2d
    .line 464
    .end local v2           #_result:Z
    :catchall_2d
    move-exception v3

    #@2e
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@31
    .line 465
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@34
    throw v3
.end method

.method public checkPattern([BI)Z
    .registers 9
    .parameter "hash"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 398
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 399
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 402
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "com.android.internal.widget.ILockSettings"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 403
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeByteArray([B)V

    #@11
    .line 404
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 405
    iget-object v3, p0, Lcom/android/internal/widget/ILockSettings$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@16
    const/16 v4, 0x9

    #@18
    const/4 v5, 0x0

    #@19
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1c
    .line 406
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1f
    .line 407
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_22
    .catchall {:try_start_9 .. :try_end_22} :catchall_2d

    #@22
    move-result v3

    #@23
    if-eqz v3, :cond_26

    #@25
    const/4 v2, 0x1

    #@26
    .line 410
    .local v2, _result:Z
    :cond_26
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 411
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 413
    return v2

    #@2d
    .line 410
    .end local v2           #_result:Z
    :catchall_2d
    move-exception v3

    #@2e
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@31
    .line 411
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@34
    throw v3
.end method

.method public checkPatternKidsMode([BI)Z
    .registers 9
    .parameter "hash"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 417
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 418
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 421
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "com.android.internal.widget.ILockSettings"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 422
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeByteArray([B)V

    #@11
    .line 423
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 424
    iget-object v3, p0, Lcom/android/internal/widget/ILockSettings$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@16
    const/16 v4, 0xa

    #@18
    const/4 v5, 0x0

    #@19
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1c
    .line 425
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1f
    .line 426
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_22
    .catchall {:try_start_9 .. :try_end_22} :catchall_2d

    #@22
    move-result v3

    #@23
    if-eqz v3, :cond_26

    #@25
    const/4 v2, 0x1

    #@26
    .line 429
    .local v2, _result:Z
    :cond_26
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 430
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 432
    return v2

    #@2d
    .line 429
    .end local v2           #_result:Z
    :catchall_2d
    move-exception v3

    #@2e
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@31
    .line 430
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@34
    throw v3
.end method

.method public getBoolean(Ljava/lang/String;ZI)Z
    .registers 11
    .parameter "key"
    .parameter "defaultValue"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 306
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 307
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 310
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "com.android.internal.widget.ILockSettings"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 311
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@12
    .line 312
    if-eqz p2, :cond_32

    #@14
    move v4, v2

    #@15
    :goto_15
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@18
    .line 313
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@1b
    .line 314
    iget-object v4, p0, Lcom/android/internal/widget/ILockSettings$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1d
    const/4 v5, 0x4

    #@1e
    const/4 v6, 0x0

    #@1f
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@22
    .line 315
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@25
    .line 316
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_28
    .catchall {:try_start_a .. :try_end_28} :catchall_36

    #@28
    move-result v4

    #@29
    if-eqz v4, :cond_34

    #@2b
    .line 319
    .local v2, _result:Z
    :goto_2b
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 320
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@31
    .line 322
    return v2

    #@32
    .end local v2           #_result:Z
    :cond_32
    move v4, v3

    #@33
    .line 312
    goto :goto_15

    #@34
    :cond_34
    move v2, v3

    #@35
    .line 316
    goto :goto_2b

    #@36
    .line 319
    :catchall_36
    move-exception v3

    #@37
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3a
    .line 320
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3d
    throw v3
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 251
    const-string v0, "com.android.internal.widget.ILockSettings"

    #@2
    return-object v0
.end method

.method public getLong(Ljava/lang/String;JI)J
    .registers 12
    .parameter "key"
    .parameter "defaultValue"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 326
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 327
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 330
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v4, "com.android.internal.widget.ILockSettings"

    #@a
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 331
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 332
    invoke-virtual {v0, p2, p3}, Landroid/os/Parcel;->writeLong(J)V

    #@13
    .line 333
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 334
    iget-object v4, p0, Lcom/android/internal/widget/ILockSettings$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/4 v5, 0x5

    #@19
    const/4 v6, 0x0

    #@1a
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1d
    .line 335
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@20
    .line 336
    invoke-virtual {v1}, Landroid/os/Parcel;->readLong()J
    :try_end_23
    .catchall {:try_start_8 .. :try_end_23} :catchall_2b

    #@23
    move-result-wide v2

    #@24
    .line 339
    .local v2, _result:J
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 340
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 342
    return-wide v2

    #@2b
    .line 339
    .end local v2           #_result:J
    :catchall_2b
    move-exception v4

    #@2c
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2f
    .line 340
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@32
    throw v4
.end method

.method public getString(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .registers 10
    .parameter "key"
    .parameter "defaultValue"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 346
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 347
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 350
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.android.internal.widget.ILockSettings"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 351
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 352
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@13
    .line 353
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 354
    iget-object v3, p0, Lcom/android/internal/widget/ILockSettings$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/4 v4, 0x6

    #@19
    const/4 v5, 0x0

    #@1a
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1d
    .line 355
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@20
    .line 356
    invoke-virtual {v1}, Landroid/os/Parcel;->readString()Ljava/lang/String;
    :try_end_23
    .catchall {:try_start_8 .. :try_end_23} :catchall_2b

    #@23
    move-result-object v2

    #@24
    .line 359
    .local v2, _result:Ljava/lang/String;
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 360
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 362
    return-object v2

    #@2b
    .line 359
    .end local v2           #_result:Ljava/lang/String;
    :catchall_2b
    move-exception v3

    #@2c
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2f
    .line 360
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@32
    throw v3
.end method

.method public havePassword(I)Z
    .registers 8
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 507
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 508
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 511
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "com.android.internal.widget.ILockSettings"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 512
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 513
    iget-object v3, p0, Lcom/android/internal/widget/ILockSettings$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@13
    const/16 v4, 0xf

    #@15
    const/4 v5, 0x0

    #@16
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@19
    .line 514
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1c
    .line 515
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1f
    .catchall {:try_start_9 .. :try_end_1f} :catchall_2a

    #@1f
    move-result v3

    #@20
    if-eqz v3, :cond_23

    #@22
    const/4 v2, 0x1

    #@23
    .line 518
    .local v2, _result:Z
    :cond_23
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 519
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 521
    return v2

    #@2a
    .line 518
    .end local v2           #_result:Z
    :catchall_2a
    move-exception v3

    #@2b
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 519
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@31
    throw v3
.end method

.method public havePattern(I)Z
    .registers 8
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 471
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 472
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 475
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "com.android.internal.widget.ILockSettings"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 476
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 477
    iget-object v3, p0, Lcom/android/internal/widget/ILockSettings$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@13
    const/16 v4, 0xd

    #@15
    const/4 v5, 0x0

    #@16
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@19
    .line 478
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1c
    .line 479
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1f
    .catchall {:try_start_9 .. :try_end_1f} :catchall_2a

    #@1f
    move-result v3

    #@20
    if-eqz v3, :cond_23

    #@22
    const/4 v2, 0x1

    #@23
    .line 482
    .local v2, _result:Z
    :cond_23
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 483
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 485
    return v2

    #@2a
    .line 482
    .end local v2           #_result:Z
    :catchall_2a
    move-exception v3

    #@2b
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 483
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@31
    throw v3
.end method

.method public havePatternKidsMode(I)Z
    .registers 8
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 489
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 490
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 493
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "com.android.internal.widget.ILockSettings"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 494
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 495
    iget-object v3, p0, Lcom/android/internal/widget/ILockSettings$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@13
    const/16 v4, 0xe

    #@15
    const/4 v5, 0x0

    #@16
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@19
    .line 496
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1c
    .line 497
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1f
    .catchall {:try_start_9 .. :try_end_1f} :catchall_2a

    #@1f
    move-result v3

    #@20
    if-eqz v3, :cond_23

    #@22
    const/4 v2, 0x1

    #@23
    .line 500
    .local v2, _result:Z
    :cond_23
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 501
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 503
    return v2

    #@2a
    .line 500
    .end local v2           #_result:Z
    :catchall_2a
    move-exception v3

    #@2b
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 501
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@31
    throw v3
.end method

.method public removeUser(I)V
    .registers 7
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 525
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 526
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 528
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.android.internal.widget.ILockSettings"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 529
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 530
    iget-object v2, p0, Lcom/android/internal/widget/ILockSettings$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v3, 0x10

    #@14
    const/4 v4, 0x0

    #@15
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 531
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1b
    .catchall {:try_start_8 .. :try_end_1b} :catchall_22

    #@1b
    .line 534
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 535
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 537
    return-void

    #@22
    .line 534
    :catchall_22
    move-exception v2

    #@23
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 535
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    throw v2
.end method

.method public setBoolean(Ljava/lang/String;ZI)V
    .registers 9
    .parameter "key"
    .parameter "value"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 255
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 256
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 258
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "com.android.internal.widget.ILockSettings"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 259
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@12
    .line 260
    if-eqz p2, :cond_2b

    #@14
    :goto_14
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@17
    .line 261
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@1a
    .line 262
    iget-object v2, p0, Lcom/android/internal/widget/ILockSettings$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1c
    const/4 v3, 0x1

    #@1d
    const/4 v4, 0x0

    #@1e
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@21
    .line 263
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_24
    .catchall {:try_start_a .. :try_end_24} :catchall_2d

    #@24
    .line 266
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 267
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 269
    return-void

    #@2b
    :cond_2b
    move v2, v3

    #@2c
    .line 260
    goto :goto_14

    #@2d
    .line 266
    :catchall_2d
    move-exception v2

    #@2e
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@31
    .line 267
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@34
    throw v2
.end method

.method public setLockPassword([BI)V
    .registers 8
    .parameter "hash"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 436
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 437
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 439
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.android.internal.widget.ILockSettings"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 440
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeByteArray([B)V

    #@10
    .line 441
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 442
    iget-object v2, p0, Lcom/android/internal/widget/ILockSettings$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/16 v3, 0xb

    #@17
    const/4 v4, 0x0

    #@18
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1b
    .line 443
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1e
    .catchall {:try_start_8 .. :try_end_1e} :catchall_25

    #@1e
    .line 446
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 447
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 449
    return-void

    #@25
    .line 446
    :catchall_25
    move-exception v2

    #@26
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 447
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    throw v2
.end method

.method public setLockPattern([BI)V
    .registers 8
    .parameter "hash"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 366
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 367
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 369
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.android.internal.widget.ILockSettings"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 370
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeByteArray([B)V

    #@10
    .line 371
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 372
    iget-object v2, p0, Lcom/android/internal/widget/ILockSettings$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/4 v3, 0x7

    #@16
    const/4 v4, 0x0

    #@17
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1a
    .line 373
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1d
    .catchall {:try_start_8 .. :try_end_1d} :catchall_24

    #@1d
    .line 376
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@20
    .line 377
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 379
    return-void

    #@24
    .line 376
    :catchall_24
    move-exception v2

    #@25
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 377
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    throw v2
.end method

.method public setLockPatternKidsMode([BI)V
    .registers 8
    .parameter "hash"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 382
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 383
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 385
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.android.internal.widget.ILockSettings"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 386
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeByteArray([B)V

    #@10
    .line 387
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 388
    iget-object v2, p0, Lcom/android/internal/widget/ILockSettings$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/16 v3, 0x8

    #@17
    const/4 v4, 0x0

    #@18
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1b
    .line 389
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1e
    .catchall {:try_start_8 .. :try_end_1e} :catchall_25

    #@1e
    .line 392
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 393
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 395
    return-void

    #@25
    .line 392
    :catchall_25
    move-exception v2

    #@26
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 393
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    throw v2
.end method

.method public setLong(Ljava/lang/String;JI)V
    .registers 10
    .parameter "key"
    .parameter "value"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 272
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 273
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 275
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.android.internal.widget.ILockSettings"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 276
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 277
    invoke-virtual {v0, p2, p3}, Landroid/os/Parcel;->writeLong(J)V

    #@13
    .line 278
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 279
    iget-object v2, p0, Lcom/android/internal/widget/ILockSettings$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/4 v3, 0x2

    #@19
    const/4 v4, 0x0

    #@1a
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1d
    .line 280
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_20
    .catchall {:try_start_8 .. :try_end_20} :catchall_27

    #@20
    .line 283
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 284
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 286
    return-void

    #@27
    .line 283
    :catchall_27
    move-exception v2

    #@28
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 284
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    throw v2
.end method

.method public setString(Ljava/lang/String;Ljava/lang/String;I)V
    .registers 9
    .parameter "key"
    .parameter "value"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 289
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 290
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 292
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.android.internal.widget.ILockSettings"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 293
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 294
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@13
    .line 295
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 296
    iget-object v2, p0, Lcom/android/internal/widget/ILockSettings$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/4 v3, 0x3

    #@19
    const/4 v4, 0x0

    #@1a
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1d
    .line 297
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_20
    .catchall {:try_start_8 .. :try_end_20} :catchall_27

    #@20
    .line 300
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 301
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 303
    return-void

    #@27
    .line 300
    :catchall_27
    move-exception v2

    #@28
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 301
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    throw v2
.end method
