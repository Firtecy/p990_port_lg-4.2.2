.class Lcom/android/internal/widget/SlidingTab$Slider;
.super Ljava/lang/Object;
.source "SlidingTab.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/widget/SlidingTab;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Slider"
.end annotation


# static fields
.field public static final ALIGN_BOTTOM:I = 0x3

.field public static final ALIGN_LEFT:I = 0x0

.field public static final ALIGN_RIGHT:I = 0x1

.field public static final ALIGN_TOP:I = 0x2

.field public static final ALIGN_UNKNOWN:I = 0x4

.field private static final STATE_ACTIVE:I = 0x2

.field private static final STATE_NORMAL:I = 0x0

.field private static final STATE_PRESSED:I = 0x1


# instance fields
.field private alignment:I

.field private alignment_value:I

.field private currentState:I

.field private final tab:Landroid/widget/ImageView;

.field private final target:Landroid/widget/ImageView;

.field private final text:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Landroid/view/ViewGroup;III)V
    .registers 10
    .parameter "parent"
    .parameter "tabId"
    .parameter "barId"
    .parameter "targetId"

    #@0
    .prologue
    const/4 v4, 0x4

    #@1
    const/4 v3, -0x2

    #@2
    .line 190
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 178
    const/4 v0, 0x0

    #@6
    iput v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->currentState:I

    #@8
    .line 179
    iput v4, p0, Lcom/android/internal/widget/SlidingTab$Slider;->alignment:I

    #@a
    .line 192
    new-instance v0, Landroid/widget/ImageView;

    #@c
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    #@f
    move-result-object v1

    #@10
    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    #@13
    iput-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    #@15
    .line 193
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    #@17
    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    #@1a
    .line 194
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    #@1c
    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    #@1e
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    #@21
    .line 195
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    #@23
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    #@25
    invoke-direct {v1, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    #@28
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@2b
    .line 199
    new-instance v0, Landroid/widget/TextView;

    #@2d
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    #@30
    move-result-object v1

    #@31
    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    #@34
    iput-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    #@36
    .line 200
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    #@38
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    #@3a
    const/4 v2, -0x1

    #@3b
    invoke-direct {v1, v3, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    #@3e
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@41
    .line 202
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    #@43
    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setBackgroundResource(I)V

    #@46
    .line 203
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    #@48
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    #@4b
    move-result-object v1

    #@4c
    const v2, 0x1030216

    #@4f
    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    #@52
    .line 207
    new-instance v0, Landroid/widget/ImageView;

    #@54
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    #@57
    move-result-object v1

    #@58
    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    #@5b
    iput-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->target:Landroid/widget/ImageView;

    #@5d
    .line 208
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->target:Landroid/widget/ImageView;

    #@5f
    invoke-virtual {v0, p4}, Landroid/widget/ImageView;->setImageResource(I)V

    #@62
    .line 209
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->target:Landroid/widget/ImageView;

    #@64
    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    #@66
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    #@69
    .line 210
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->target:Landroid/widget/ImageView;

    #@6b
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    #@6d
    invoke-direct {v1, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    #@70
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@73
    .line 212
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->target:Landroid/widget/ImageView;

    #@75
    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    #@78
    .line 214
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->target:Landroid/widget/ImageView;

    #@7a
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    #@7d
    .line 215
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    #@7f
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    #@82
    .line 216
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    #@84
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    #@87
    .line 217
    return-void
.end method

.method static synthetic access$100(Lcom/android/internal/widget/SlidingTab$Slider;)Landroid/widget/ImageView;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 158
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Lcom/android/internal/widget/SlidingTab$Slider;)Landroid/widget/TextView;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 158
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    #@2
    return-object v0
.end method


# virtual methods
.method public getTabHeight()I
    .registers 2

    #@0
    .prologue
    .line 417
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    #@2
    invoke-virtual {v0}, Landroid/widget/ImageView;->getMeasuredHeight()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getTabWidth()I
    .registers 2

    #@0
    .prologue
    .line 409
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    #@2
    invoke-virtual {v0}, Landroid/widget/ImageView;->getMeasuredWidth()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method hide()V
    .registers 9

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    const/4 v1, 0x0

    #@3
    .line 236
    iget v5, p0, Lcom/android/internal/widget/SlidingTab$Slider;->alignment:I

    #@5
    if-eqz v5, :cond_b

    #@7
    iget v5, p0, Lcom/android/internal/widget/SlidingTab$Slider;->alignment:I

    #@9
    if-ne v5, v4, :cond_3e

    #@b
    :cond_b
    move v2, v4

    #@c
    .line 237
    .local v2, horiz:Z
    :goto_c
    if-eqz v2, :cond_4b

    #@e
    iget v5, p0, Lcom/android/internal/widget/SlidingTab$Slider;->alignment:I

    #@10
    if-nez v5, :cond_40

    #@12
    iget v5, p0, Lcom/android/internal/widget/SlidingTab$Slider;->alignment_value:I

    #@14
    iget-object v6, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    #@16
    invoke-virtual {v6}, Landroid/widget/ImageView;->getRight()I

    #@19
    move-result v6

    #@1a
    sub-int v0, v5, v6

    #@1c
    .line 239
    .local v0, dx:I
    :goto_1c
    if-eqz v2, :cond_4d

    #@1e
    .line 242
    .local v1, dy:I
    :goto_1e
    new-instance v3, Landroid/view/animation/TranslateAnimation;

    #@20
    int-to-float v5, v0

    #@21
    int-to-float v6, v1

    #@22
    invoke-direct {v3, v7, v5, v7, v6}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    #@25
    .line 243
    .local v3, trans:Landroid/view/animation/Animation;
    const-wide/16 v5, 0xfa

    #@27
    invoke-virtual {v3, v5, v6}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    #@2a
    .line 244
    invoke-virtual {v3, v4}, Landroid/view/animation/TranslateAnimation;->setFillAfter(Z)V

    #@2d
    .line 245
    iget-object v4, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    #@2f
    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    #@32
    .line 246
    iget-object v4, p0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    #@34
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    #@37
    .line 247
    iget-object v4, p0, Lcom/android/internal/widget/SlidingTab$Slider;->target:Landroid/widget/ImageView;

    #@39
    const/4 v5, 0x4

    #@3a
    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    #@3d
    .line 248
    return-void

    #@3e
    .end local v0           #dx:I
    .end local v1           #dy:I
    .end local v2           #horiz:Z
    .end local v3           #trans:Landroid/view/animation/Animation;
    :cond_3e
    move v2, v1

    #@3f
    .line 236
    goto :goto_c

    #@40
    .line 237
    .restart local v2       #horiz:Z
    :cond_40
    iget v5, p0, Lcom/android/internal/widget/SlidingTab$Slider;->alignment_value:I

    #@42
    iget-object v6, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    #@44
    invoke-virtual {v6}, Landroid/widget/ImageView;->getLeft()I

    #@47
    move-result v6

    #@48
    sub-int v0, v5, v6

    #@4a
    goto :goto_1c

    #@4b
    :cond_4b
    move v0, v1

    #@4c
    goto :goto_1c

    #@4d
    .line 239
    .restart local v0       #dx:I
    :cond_4d
    iget v5, p0, Lcom/android/internal/widget/SlidingTab$Slider;->alignment:I

    #@4f
    const/4 v6, 0x2

    #@50
    if-ne v5, v6, :cond_5d

    #@52
    iget v5, p0, Lcom/android/internal/widget/SlidingTab$Slider;->alignment_value:I

    #@54
    iget-object v6, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    #@56
    invoke-virtual {v6}, Landroid/widget/ImageView;->getBottom()I

    #@59
    move-result v6

    #@5a
    sub-int v1, v5, v6

    #@5c
    goto :goto_1e

    #@5d
    :cond_5d
    iget v5, p0, Lcom/android/internal/widget/SlidingTab$Slider;->alignment_value:I

    #@5f
    iget-object v6, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    #@61
    invoke-virtual {v6}, Landroid/widget/ImageView;->getTop()I

    #@64
    move-result v6

    #@65
    sub-int v1, v5, v6

    #@67
    goto :goto_1e
.end method

.method public hideTarget()V
    .registers 3

    #@0
    .prologue
    .line 433
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->target:Landroid/widget/ImageView;

    #@2
    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    #@5
    .line 434
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->target:Landroid/widget/ImageView;

    #@7
    const/4 v1, 0x4

    #@8
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    #@b
    .line 435
    return-void
.end method

.method layout(IIIII)V
    .registers 32
    .parameter "l"
    .parameter "t"
    .parameter "r"
    .parameter "b"
    .parameter "alignment"

    #@0
    .prologue
    .line 336
    move/from16 v0, p5

    #@2
    move-object/from16 v1, p0

    #@4
    iput v0, v1, Lcom/android/internal/widget/SlidingTab$Slider;->alignment:I

    #@6
    .line 337
    move-object/from16 v0, p0

    #@8
    iget-object v0, v0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    #@a
    move-object/from16 v23, v0

    #@c
    invoke-virtual/range {v23 .. v23}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    #@f
    move-result-object v14

    #@10
    .line 338
    .local v14, tabBackground:Landroid/graphics/drawable/Drawable;
    invoke-virtual {v14}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@13
    move-result v7

    #@14
    .line 339
    .local v7, handleWidth:I
    invoke-virtual {v14}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@17
    move-result v6

    #@18
    .line 340
    .local v6, handleHeight:I
    move-object/from16 v0, p0

    #@1a
    iget-object v0, v0, Lcom/android/internal/widget/SlidingTab$Slider;->target:Landroid/widget/ImageView;

    #@1c
    move-object/from16 v23, v0

    #@1e
    invoke-virtual/range {v23 .. v23}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    #@21
    move-result-object v16

    #@22
    .line 341
    .local v16, targetDrawable:Landroid/graphics/drawable/Drawable;
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@25
    move-result v21

    #@26
    .line 342
    .local v21, targetWidth:I
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@29
    move-result v17

    #@2a
    .line 343
    .local v17, targetHeight:I
    sub-int v11, p3, p1

    #@2c
    .line 344
    .local v11, parentWidth:I
    sub-int v10, p4, p2

    #@2e
    .line 346
    .local v10, parentHeight:I
    const v23, 0x3f2aaaab

    #@31
    int-to-float v0, v11

    #@32
    move/from16 v24, v0

    #@34
    mul-float v23, v23, v24

    #@36
    move/from16 v0, v23

    #@38
    float-to-int v0, v0

    #@39
    move/from16 v23, v0

    #@3b
    sub-int v23, v23, v21

    #@3d
    div-int/lit8 v24, v7, 0x2

    #@3f
    add-int v9, v23, v24

    #@41
    .line 347
    .local v9, leftTarget:I
    const v23, 0x3eaaaaaa

    #@44
    int-to-float v0, v11

    #@45
    move/from16 v24, v0

    #@47
    mul-float v23, v23, v24

    #@49
    move/from16 v0, v23

    #@4b
    float-to-int v0, v0

    #@4c
    move/from16 v23, v0

    #@4e
    div-int/lit8 v24, v7, 0x2

    #@50
    sub-int v13, v23, v24

    #@52
    .line 348
    .local v13, rightTarget:I
    sub-int v23, v11, v7

    #@54
    div-int/lit8 v8, v23, 0x2

    #@56
    .line 349
    .local v8, left:I
    add-int v12, v8, v7

    #@58
    .line 351
    .local v12, right:I
    if-eqz p5, :cond_62

    #@5a
    const/16 v23, 0x1

    #@5c
    move/from16 v0, p5

    #@5e
    move/from16 v1, v23

    #@60
    if-ne v0, v1, :cond_100

    #@62
    .line 353
    :cond_62
    sub-int v23, v10, v17

    #@64
    div-int/lit8 v20, v23, 0x2

    #@66
    .line 354
    .local v20, targetTop:I
    add-int v15, v20, v17

    #@68
    .line 355
    .local v15, targetBottom:I
    sub-int v23, v10, v6

    #@6a
    div-int/lit8 v22, v23, 0x2

    #@6c
    .line 356
    .local v22, top:I
    add-int v23, v10, v6

    #@6e
    div-int/lit8 v5, v23, 0x2

    #@70
    .line 357
    .local v5, bottom:I
    if-nez p5, :cond_bb

    #@72
    .line 358
    move-object/from16 v0, p0

    #@74
    iget-object v0, v0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    #@76
    move-object/from16 v23, v0

    #@78
    const/16 v24, 0x0

    #@7a
    move-object/from16 v0, v23

    #@7c
    move/from16 v1, v24

    #@7e
    move/from16 v2, v22

    #@80
    invoke-virtual {v0, v1, v2, v7, v5}, Landroid/widget/ImageView;->layout(IIII)V

    #@83
    .line 359
    move-object/from16 v0, p0

    #@85
    iget-object v0, v0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    #@87
    move-object/from16 v23, v0

    #@89
    rsub-int/lit8 v24, v11, 0x0

    #@8b
    const/16 v25, 0x0

    #@8d
    move-object/from16 v0, v23

    #@8f
    move/from16 v1, v24

    #@91
    move/from16 v2, v22

    #@93
    move/from16 v3, v25

    #@95
    invoke-virtual {v0, v1, v2, v3, v5}, Landroid/widget/TextView;->layout(IIII)V

    #@98
    .line 360
    move-object/from16 v0, p0

    #@9a
    iget-object v0, v0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    #@9c
    move-object/from16 v23, v0

    #@9e
    const/16 v24, 0x5

    #@a0
    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setGravity(I)V

    #@a3
    .line 361
    move-object/from16 v0, p0

    #@a5
    iget-object v0, v0, Lcom/android/internal/widget/SlidingTab$Slider;->target:Landroid/widget/ImageView;

    #@a7
    move-object/from16 v23, v0

    #@a9
    add-int v24, v9, v21

    #@ab
    move-object/from16 v0, v23

    #@ad
    move/from16 v1, v20

    #@af
    move/from16 v2, v24

    #@b1
    invoke-virtual {v0, v9, v1, v2, v15}, Landroid/widget/ImageView;->layout(IIII)V

    #@b4
    .line 362
    move/from16 v0, p1

    #@b6
    move-object/from16 v1, p0

    #@b8
    iput v0, v1, Lcom/android/internal/widget/SlidingTab$Slider;->alignment_value:I

    #@ba
    .line 388
    .end local v15           #targetBottom:I
    .end local v20           #targetTop:I
    :goto_ba
    return-void

    #@bb
    .line 364
    .restart local v15       #targetBottom:I
    .restart local v20       #targetTop:I
    :cond_bb
    move-object/from16 v0, p0

    #@bd
    iget-object v0, v0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    #@bf
    move-object/from16 v23, v0

    #@c1
    sub-int v24, v11, v7

    #@c3
    move-object/from16 v0, v23

    #@c5
    move/from16 v1, v24

    #@c7
    move/from16 v2, v22

    #@c9
    invoke-virtual {v0, v1, v2, v11, v5}, Landroid/widget/ImageView;->layout(IIII)V

    #@cc
    .line 365
    move-object/from16 v0, p0

    #@ce
    iget-object v0, v0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    #@d0
    move-object/from16 v23, v0

    #@d2
    add-int v24, v11, v11

    #@d4
    move-object/from16 v0, v23

    #@d6
    move/from16 v1, v22

    #@d8
    move/from16 v2, v24

    #@da
    invoke-virtual {v0, v11, v1, v2, v5}, Landroid/widget/TextView;->layout(IIII)V

    #@dd
    .line 366
    move-object/from16 v0, p0

    #@df
    iget-object v0, v0, Lcom/android/internal/widget/SlidingTab$Slider;->target:Landroid/widget/ImageView;

    #@e1
    move-object/from16 v23, v0

    #@e3
    add-int v24, v13, v21

    #@e5
    move-object/from16 v0, v23

    #@e7
    move/from16 v1, v20

    #@e9
    move/from16 v2, v24

    #@eb
    invoke-virtual {v0, v13, v1, v2, v15}, Landroid/widget/ImageView;->layout(IIII)V

    #@ee
    .line 367
    move-object/from16 v0, p0

    #@f0
    iget-object v0, v0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    #@f2
    move-object/from16 v23, v0

    #@f4
    const/16 v24, 0x30

    #@f6
    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setGravity(I)V

    #@f9
    .line 368
    move/from16 v0, p3

    #@fb
    move-object/from16 v1, p0

    #@fd
    iput v0, v1, Lcom/android/internal/widget/SlidingTab$Slider;->alignment_value:I

    #@ff
    goto :goto_ba

    #@100
    .line 372
    .end local v5           #bottom:I
    .end local v15           #targetBottom:I
    .end local v20           #targetTop:I
    .end local v22           #top:I
    :cond_100
    sub-int v23, v11, v21

    #@102
    div-int/lit8 v18, v23, 0x2

    #@104
    .line 373
    .local v18, targetLeft:I
    add-int v23, v11, v21

    #@106
    div-int/lit8 v19, v23, 0x2

    #@108
    .line 374
    .local v19, targetRight:I
    const v23, 0x3f2aaaab

    #@10b
    int-to-float v0, v10

    #@10c
    move/from16 v24, v0

    #@10e
    mul-float v23, v23, v24

    #@110
    move/from16 v0, v23

    #@112
    float-to-int v0, v0

    #@113
    move/from16 v23, v0

    #@115
    div-int/lit8 v24, v6, 0x2

    #@117
    add-int v23, v23, v24

    #@119
    sub-int v22, v23, v17

    #@11b
    .line 375
    .restart local v22       #top:I
    const v23, 0x3eaaaaaa

    #@11e
    int-to-float v0, v10

    #@11f
    move/from16 v24, v0

    #@121
    mul-float v23, v23, v24

    #@123
    move/from16 v0, v23

    #@125
    float-to-int v0, v0

    #@126
    move/from16 v23, v0

    #@128
    div-int/lit8 v24, v6, 0x2

    #@12a
    sub-int v5, v23, v24

    #@12c
    .line 376
    .restart local v5       #bottom:I
    const/16 v23, 0x2

    #@12e
    move/from16 v0, p5

    #@130
    move/from16 v1, v23

    #@132
    if-ne v0, v1, :cond_173

    #@134
    .line 377
    move-object/from16 v0, p0

    #@136
    iget-object v0, v0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    #@138
    move-object/from16 v23, v0

    #@13a
    const/16 v24, 0x0

    #@13c
    move-object/from16 v0, v23

    #@13e
    move/from16 v1, v24

    #@140
    invoke-virtual {v0, v8, v1, v12, v6}, Landroid/widget/ImageView;->layout(IIII)V

    #@143
    .line 378
    move-object/from16 v0, p0

    #@145
    iget-object v0, v0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    #@147
    move-object/from16 v23, v0

    #@149
    rsub-int/lit8 v24, v10, 0x0

    #@14b
    const/16 v25, 0x0

    #@14d
    move-object/from16 v0, v23

    #@14f
    move/from16 v1, v24

    #@151
    move/from16 v2, v25

    #@153
    invoke-virtual {v0, v8, v1, v12, v2}, Landroid/widget/TextView;->layout(IIII)V

    #@156
    .line 379
    move-object/from16 v0, p0

    #@158
    iget-object v0, v0, Lcom/android/internal/widget/SlidingTab$Slider;->target:Landroid/widget/ImageView;

    #@15a
    move-object/from16 v23, v0

    #@15c
    add-int v24, v22, v17

    #@15e
    move-object/from16 v0, v23

    #@160
    move/from16 v1, v18

    #@162
    move/from16 v2, v22

    #@164
    move/from16 v3, v19

    #@166
    move/from16 v4, v24

    #@168
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/ImageView;->layout(IIII)V

    #@16b
    .line 380
    move/from16 v0, p2

    #@16d
    move-object/from16 v1, p0

    #@16f
    iput v0, v1, Lcom/android/internal/widget/SlidingTab$Slider;->alignment_value:I

    #@171
    goto/16 :goto_ba

    #@173
    .line 382
    :cond_173
    move-object/from16 v0, p0

    #@175
    iget-object v0, v0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    #@177
    move-object/from16 v23, v0

    #@179
    sub-int v24, v10, v6

    #@17b
    move-object/from16 v0, v23

    #@17d
    move/from16 v1, v24

    #@17f
    invoke-virtual {v0, v8, v1, v12, v10}, Landroid/widget/ImageView;->layout(IIII)V

    #@182
    .line 383
    move-object/from16 v0, p0

    #@184
    iget-object v0, v0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    #@186
    move-object/from16 v23, v0

    #@188
    add-int v24, v10, v10

    #@18a
    move-object/from16 v0, v23

    #@18c
    move/from16 v1, v24

    #@18e
    invoke-virtual {v0, v8, v10, v12, v1}, Landroid/widget/TextView;->layout(IIII)V

    #@191
    .line 384
    move-object/from16 v0, p0

    #@193
    iget-object v0, v0, Lcom/android/internal/widget/SlidingTab$Slider;->target:Landroid/widget/ImageView;

    #@195
    move-object/from16 v23, v0

    #@197
    add-int v24, v5, v17

    #@199
    move-object/from16 v0, v23

    #@19b
    move/from16 v1, v18

    #@19d
    move/from16 v2, v19

    #@19f
    move/from16 v3, v24

    #@1a1
    invoke-virtual {v0, v1, v5, v2, v3}, Landroid/widget/ImageView;->layout(IIII)V

    #@1a4
    .line 385
    move/from16 v0, p4

    #@1a6
    move-object/from16 v1, p0

    #@1a8
    iput v0, v1, Lcom/android/internal/widget/SlidingTab$Slider;->alignment_value:I

    #@1aa
    goto/16 :goto_ba
.end method

.method public measure()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 398
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    #@3
    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@6
    move-result v1

    #@7
    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@a
    move-result v2

    #@b
    invoke-virtual {v0, v1, v2}, Landroid/widget/ImageView;->measure(II)V

    #@e
    .line 400
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    #@10
    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@13
    move-result v1

    #@14
    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@17
    move-result v2

    #@18
    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->measure(II)V

    #@1b
    .line 402
    return-void
.end method

.method reset(Z)V
    .registers 11
    .parameter "animate"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v8, 0x0

    #@2
    const/4 v4, 0x0

    #@3
    .line 292
    invoke-virtual {p0, v4}, Lcom/android/internal/widget/SlidingTab$Slider;->setState(I)V

    #@6
    .line 293
    iget-object v5, p0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    #@8
    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setVisibility(I)V

    #@b
    .line 294
    iget-object v5, p0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    #@d
    iget-object v6, p0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    #@f
    invoke-virtual {v6}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@12
    move-result-object v6

    #@13
    const v7, 0x1030216

    #@16
    invoke-virtual {v5, v6, v7}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    #@19
    .line 295
    iget-object v5, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    #@1b
    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    #@1e
    .line 296
    iget-object v5, p0, Lcom/android/internal/widget/SlidingTab$Slider;->target:Landroid/widget/ImageView;

    #@20
    const/4 v6, 0x4

    #@21
    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    #@24
    .line 297
    iget v5, p0, Lcom/android/internal/widget/SlidingTab$Slider;->alignment:I

    #@26
    if-eqz v5, :cond_2c

    #@28
    iget v5, p0, Lcom/android/internal/widget/SlidingTab$Slider;->alignment:I

    #@2a
    if-ne v5, v2, :cond_5b

    #@2c
    .line 298
    .local v2, horiz:Z
    :cond_2c
    :goto_2c
    if-eqz v2, :cond_68

    #@2e
    iget v5, p0, Lcom/android/internal/widget/SlidingTab$Slider;->alignment:I

    #@30
    if-nez v5, :cond_5d

    #@32
    iget v5, p0, Lcom/android/internal/widget/SlidingTab$Slider;->alignment_value:I

    #@34
    iget-object v6, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    #@36
    invoke-virtual {v6}, Landroid/widget/ImageView;->getLeft()I

    #@39
    move-result v6

    #@3a
    sub-int v0, v5, v6

    #@3c
    .line 300
    .local v0, dx:I
    :goto_3c
    if-eqz v2, :cond_6a

    #@3e
    move v1, v4

    #@3f
    .line 302
    .local v1, dy:I
    :goto_3f
    if-eqz p1, :cond_85

    #@41
    .line 303
    new-instance v3, Landroid/view/animation/TranslateAnimation;

    #@43
    int-to-float v5, v0

    #@44
    int-to-float v6, v1

    #@45
    invoke-direct {v3, v8, v5, v8, v6}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    #@48
    .line 304
    .local v3, trans:Landroid/view/animation/TranslateAnimation;
    const-wide/16 v5, 0xfa

    #@4a
    invoke-virtual {v3, v5, v6}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    #@4d
    .line 305
    invoke-virtual {v3, v4}, Landroid/view/animation/TranslateAnimation;->setFillAfter(Z)V

    #@50
    .line 306
    iget-object v4, p0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    #@52
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    #@55
    .line 307
    iget-object v4, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    #@57
    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    #@5a
    .line 320
    .end local v3           #trans:Landroid/view/animation/TranslateAnimation;
    :goto_5a
    return-void

    #@5b
    .end local v0           #dx:I
    .end local v1           #dy:I
    .end local v2           #horiz:Z
    :cond_5b
    move v2, v4

    #@5c
    .line 297
    goto :goto_2c

    #@5d
    .line 298
    .restart local v2       #horiz:Z
    :cond_5d
    iget v5, p0, Lcom/android/internal/widget/SlidingTab$Slider;->alignment_value:I

    #@5f
    iget-object v6, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    #@61
    invoke-virtual {v6}, Landroid/widget/ImageView;->getRight()I

    #@64
    move-result v6

    #@65
    sub-int v0, v5, v6

    #@67
    goto :goto_3c

    #@68
    :cond_68
    move v0, v4

    #@69
    goto :goto_3c

    #@6a
    .line 300
    .restart local v0       #dx:I
    :cond_6a
    iget v5, p0, Lcom/android/internal/widget/SlidingTab$Slider;->alignment:I

    #@6c
    const/4 v6, 0x2

    #@6d
    if-ne v5, v6, :cond_7a

    #@6f
    iget v5, p0, Lcom/android/internal/widget/SlidingTab$Slider;->alignment_value:I

    #@71
    iget-object v6, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    #@73
    invoke-virtual {v6}, Landroid/widget/ImageView;->getTop()I

    #@76
    move-result v6

    #@77
    sub-int v1, v5, v6

    #@79
    goto :goto_3f

    #@7a
    :cond_7a
    iget v5, p0, Lcom/android/internal/widget/SlidingTab$Slider;->alignment_value:I

    #@7c
    iget-object v6, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    #@7e
    invoke-virtual {v6}, Landroid/widget/ImageView;->getBottom()I

    #@81
    move-result v6

    #@82
    sub-int v1, v5, v6

    #@84
    goto :goto_3f

    #@85
    .line 309
    .restart local v1       #dy:I
    :cond_85
    if-eqz v2, :cond_a1

    #@87
    .line 310
    iget-object v4, p0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    #@89
    invoke-virtual {v4, v0}, Landroid/widget/TextView;->offsetLeftAndRight(I)V

    #@8c
    .line 311
    iget-object v4, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    #@8e
    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->offsetLeftAndRight(I)V

    #@91
    .line 316
    :goto_91
    iget-object v4, p0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    #@93
    invoke-virtual {v4}, Landroid/widget/TextView;->clearAnimation()V

    #@96
    .line 317
    iget-object v4, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    #@98
    invoke-virtual {v4}, Landroid/widget/ImageView;->clearAnimation()V

    #@9b
    .line 318
    iget-object v4, p0, Lcom/android/internal/widget/SlidingTab$Slider;->target:Landroid/widget/ImageView;

    #@9d
    invoke-virtual {v4}, Landroid/widget/ImageView;->clearAnimation()V

    #@a0
    goto :goto_5a

    #@a1
    .line 313
    :cond_a1
    iget-object v4, p0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    #@a3
    invoke-virtual {v4, v1}, Landroid/widget/TextView;->offsetTopAndBottom(I)V

    #@a6
    .line 314
    iget-object v4, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    #@a8
    invoke-virtual {v4, v1}, Landroid/widget/ImageView;->offsetTopAndBottom(I)V

    #@ab
    goto :goto_91
.end method

.method setBarBackgroundResource(I)V
    .registers 3
    .parameter "barId"

    #@0
    .prologue
    .line 228
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    #@5
    .line 229
    return-void
.end method

.method setHintText(I)V
    .registers 3
    .parameter "resId"

    #@0
    .prologue
    .line 232
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    #@5
    .line 233
    return-void
.end method

.method setIcon(I)V
    .registers 3
    .parameter "iconId"

    #@0
    .prologue
    .line 220
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    #@5
    .line 221
    return-void
.end method

.method setState(I)V
    .registers 7
    .parameter "state"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    .line 267
    iget-object v4, p0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    #@4
    if-ne p1, v2, :cond_57

    #@6
    move v1, v2

    #@7
    :goto_7
    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setPressed(Z)V

    #@a
    .line 268
    iget-object v4, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    #@c
    if-ne p1, v2, :cond_59

    #@e
    move v1, v2

    #@f
    :goto_f
    invoke-virtual {v4, v1}, Landroid/widget/ImageView;->setPressed(Z)V

    #@12
    .line 269
    const/4 v1, 0x2

    #@13
    if-ne p1, v1, :cond_5b

    #@15
    .line 270
    new-array v0, v2, [I

    #@17
    const v1, 0x10100a2

    #@1a
    aput v1, v0, v3

    #@1c
    .line 271
    .local v0, activeState:[I
    iget-object v1, p0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    #@1e
    invoke-virtual {v1}, Landroid/widget/TextView;->getBackground()Landroid/graphics/drawable/Drawable;

    #@21
    move-result-object v1

    #@22
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    #@25
    move-result v1

    #@26
    if-eqz v1, :cond_31

    #@28
    .line 272
    iget-object v1, p0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    #@2a
    invoke-virtual {v1}, Landroid/widget/TextView;->getBackground()Landroid/graphics/drawable/Drawable;

    #@2d
    move-result-object v1

    #@2e
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@31
    .line 274
    :cond_31
    iget-object v1, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    #@33
    invoke-virtual {v1}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    #@36
    move-result-object v1

    #@37
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    #@3a
    move-result v1

    #@3b
    if-eqz v1, :cond_46

    #@3d
    .line 275
    iget-object v1, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    #@3f
    invoke-virtual {v1}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    #@42
    move-result-object v1

    #@43
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@46
    .line 277
    :cond_46
    iget-object v1, p0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    #@48
    iget-object v2, p0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    #@4a
    invoke-virtual {v2}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@4d
    move-result-object v2

    #@4e
    const v3, 0x1030217

    #@51
    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    #@54
    .line 281
    .end local v0           #activeState:[I
    :goto_54
    iput p1, p0, Lcom/android/internal/widget/SlidingTab$Slider;->currentState:I

    #@56
    .line 282
    return-void

    #@57
    :cond_57
    move v1, v3

    #@58
    .line 267
    goto :goto_7

    #@59
    :cond_59
    move v1, v3

    #@5a
    .line 268
    goto :goto_f

    #@5b
    .line 279
    :cond_5b
    iget-object v1, p0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    #@5d
    iget-object v2, p0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    #@5f
    invoke-virtual {v2}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    #@62
    move-result-object v2

    #@63
    const v3, 0x1030216

    #@66
    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    #@69
    goto :goto_54
.end method

.method setTabBackgroundResource(I)V
    .registers 3
    .parameter "tabId"

    #@0
    .prologue
    .line 224
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    #@5
    .line 225
    return-void
.end method

.method setTarget(I)V
    .registers 3
    .parameter "targetId"

    #@0
    .prologue
    .line 323
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->target:Landroid/widget/ImageView;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    #@5
    .line 324
    return-void
.end method

.method show(Z)V
    .registers 9
    .parameter "animate"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    const/4 v1, 0x0

    #@3
    .line 251
    iget-object v4, p0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    #@5
    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setVisibility(I)V

    #@8
    .line 252
    iget-object v4, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    #@a
    invoke-virtual {v4, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    #@d
    .line 254
    if-eqz p1, :cond_3d

    #@f
    .line 255
    iget v4, p0, Lcom/android/internal/widget/SlidingTab$Slider;->alignment:I

    #@11
    if-eqz v4, :cond_17

    #@13
    iget v4, p0, Lcom/android/internal/widget/SlidingTab$Slider;->alignment:I

    #@15
    if-ne v4, v2, :cond_3e

    #@17
    .line 256
    .local v2, horiz:Z
    :cond_17
    :goto_17
    if-eqz v2, :cond_48

    #@19
    iget v4, p0, Lcom/android/internal/widget/SlidingTab$Slider;->alignment:I

    #@1b
    if-nez v4, :cond_40

    #@1d
    iget-object v4, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    #@1f
    invoke-virtual {v4}, Landroid/widget/ImageView;->getWidth()I

    #@22
    move-result v0

    #@23
    .line 257
    .local v0, dx:I
    :goto_23
    if-eqz v2, :cond_4a

    #@25
    .line 259
    .local v1, dy:I
    :goto_25
    new-instance v3, Landroid/view/animation/TranslateAnimation;

    #@27
    neg-int v4, v0

    #@28
    int-to-float v4, v4

    #@29
    neg-int v5, v1

    #@2a
    int-to-float v5, v5

    #@2b
    invoke-direct {v3, v4, v6, v5, v6}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    #@2e
    .line 260
    .local v3, trans:Landroid/view/animation/Animation;
    const-wide/16 v4, 0xfa

    #@30
    invoke-virtual {v3, v4, v5}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    #@33
    .line 261
    iget-object v4, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    #@35
    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    #@38
    .line 262
    iget-object v4, p0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    #@3a
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    #@3d
    .line 264
    .end local v0           #dx:I
    .end local v1           #dy:I
    .end local v2           #horiz:Z
    .end local v3           #trans:Landroid/view/animation/Animation;
    :cond_3d
    return-void

    #@3e
    :cond_3e
    move v2, v1

    #@3f
    .line 255
    goto :goto_17

    #@40
    .line 256
    .restart local v2       #horiz:Z
    :cond_40
    iget-object v4, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    #@42
    invoke-virtual {v4}, Landroid/widget/ImageView;->getWidth()I

    #@45
    move-result v4

    #@46
    neg-int v0, v4

    #@47
    goto :goto_23

    #@48
    :cond_48
    move v0, v1

    #@49
    goto :goto_23

    #@4a
    .line 257
    .restart local v0       #dx:I
    :cond_4a
    iget v4, p0, Lcom/android/internal/widget/SlidingTab$Slider;->alignment:I

    #@4c
    const/4 v5, 0x2

    #@4d
    if-ne v4, v5, :cond_56

    #@4f
    iget-object v4, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    #@51
    invoke-virtual {v4}, Landroid/widget/ImageView;->getHeight()I

    #@54
    move-result v1

    #@55
    goto :goto_25

    #@56
    :cond_56
    iget-object v4, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    #@58
    invoke-virtual {v4}, Landroid/widget/ImageView;->getHeight()I

    #@5b
    move-result v4

    #@5c
    neg-int v1, v4

    #@5d
    goto :goto_25
.end method

.method showTarget()V
    .registers 4

    #@0
    .prologue
    .line 285
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    #@2
    const/4 v1, 0x0

    #@3
    const/high16 v2, 0x3f80

    #@5
    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    #@8
    .line 286
    .local v0, alphaAnim:Landroid/view/animation/AlphaAnimation;
    const-wide/16 v1, 0x1f4

    #@a
    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    #@d
    .line 287
    iget-object v1, p0, Lcom/android/internal/widget/SlidingTab$Slider;->target:Landroid/widget/ImageView;

    #@f
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    #@12
    .line 288
    iget-object v1, p0, Lcom/android/internal/widget/SlidingTab$Slider;->target:Landroid/widget/ImageView;

    #@14
    const/4 v2, 0x0

    #@15
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    #@18
    .line 289
    return-void
.end method

.method public startAnimation(Landroid/view/animation/Animation;Landroid/view/animation/Animation;)V
    .registers 4
    .parameter "anim1"
    .parameter "anim2"

    #@0
    .prologue
    .line 428
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    #@5
    .line 429
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    #@7
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    #@a
    .line 430
    return-void
.end method

.method public updateDrawableStates()V
    .registers 2

    #@0
    .prologue
    .line 391
    iget v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->currentState:I

    #@2
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/SlidingTab$Slider;->setState(I)V

    #@5
    .line 392
    return-void
.end method
