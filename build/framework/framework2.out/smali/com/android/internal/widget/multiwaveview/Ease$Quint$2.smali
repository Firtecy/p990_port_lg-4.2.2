.class final Lcom/android/internal/widget/multiwaveview/Ease$Quint$2;
.super Ljava/lang/Object;
.source "Ease.java"

# interfaces
.implements Landroid/animation/TimeInterpolator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/widget/multiwaveview/Ease$Quint;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 100
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .registers 4
    .parameter "input"

    #@0
    .prologue
    const/high16 v1, 0x3f80

    #@2
    .line 102
    div-float v0, p1, v1

    #@4
    sub-float p1, v0, v1

    #@6
    mul-float v0, p1, p1

    #@8
    mul-float/2addr v0, p1

    #@9
    mul-float/2addr v0, p1

    #@a
    mul-float/2addr v0, p1

    #@b
    add-float/2addr v0, v1

    #@c
    mul-float/2addr v0, v1

    #@d
    const/4 v1, 0x0

    #@e
    add-float/2addr v0, v1

    #@f
    return v0
.end method
