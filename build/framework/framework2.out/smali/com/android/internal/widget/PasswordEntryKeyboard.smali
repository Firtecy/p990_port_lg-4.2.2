.class public Lcom/android/internal/widget/PasswordEntryKeyboard;
.super Landroid/inputmethodservice/Keyboard;
.source "PasswordEntryKeyboard.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/widget/PasswordEntryKeyboard$LatinKey;
    }
.end annotation


# static fields
.field public static final KEYCODE_SPACE:I = 0x20

.field private static final SHIFT_LOCKED:I = 0x2

.field private static final SHIFT_OFF:I = 0x0

.field private static final SHIFT_ON:I = 0x1

.field static sSpacebarVerticalCorrection:I


# instance fields
.field private mEnterKey:Landroid/inputmethodservice/Keyboard$Key;

.field private mF1Key:Landroid/inputmethodservice/Keyboard$Key;

.field private mOldShiftIcons:[Landroid/graphics/drawable/Drawable;

.field private mShiftIcon:Landroid/graphics/drawable/Drawable;

.field private mShiftKeys:[Landroid/inputmethodservice/Keyboard$Key;

.field private mShiftLockIcon:Landroid/graphics/drawable/Drawable;

.field private mShiftState:I

.field private mSpaceKey:Landroid/inputmethodservice/Keyboard$Key;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .registers 4
    .parameter "context"
    .parameter "xmlLayoutResId"

    #@0
    .prologue
    .line 56
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Lcom/android/internal/widget/PasswordEntryKeyboard;-><init>(Landroid/content/Context;II)V

    #@4
    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;II)V
    .registers 9
    .parameter "context"
    .parameter "xmlLayoutResId"
    .parameter "mode"

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    const/4 v3, 0x1

    #@2
    const/4 v2, 0x0

    #@3
    const/4 v1, 0x0

    #@4
    .line 64
    invoke-direct {p0, p1, p2, p3}, Landroid/inputmethodservice/Keyboard;-><init>(Landroid/content/Context;II)V

    #@7
    .line 45
    new-array v0, v4, [Landroid/graphics/drawable/Drawable;

    #@9
    aput-object v1, v0, v2

    #@b
    aput-object v1, v0, v3

    #@d
    iput-object v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboard;->mOldShiftIcons:[Landroid/graphics/drawable/Drawable;

    #@f
    .line 46
    new-array v0, v4, [Landroid/inputmethodservice/Keyboard$Key;

    #@11
    aput-object v1, v0, v2

    #@13
    aput-object v1, v0, v3

    #@15
    iput-object v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboard;->mShiftKeys:[Landroid/inputmethodservice/Keyboard$Key;

    #@17
    .line 51
    iput v2, p0, Lcom/android/internal/widget/PasswordEntryKeyboard;->mShiftState:I

    #@19
    .line 65
    invoke-direct {p0, p1}, Lcom/android/internal/widget/PasswordEntryKeyboard;->init(Landroid/content/Context;)V

    #@1c
    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;III)V
    .registers 11
    .parameter "context"
    .parameter "xmlLayoutResId"
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 60
    const/4 v3, 0x0

    #@1
    move-object v0, p0

    #@2
    move-object v1, p1

    #@3
    move v2, p2

    #@4
    move v4, p3

    #@5
    move v5, p4

    #@6
    invoke-direct/range {v0 .. v5}, Lcom/android/internal/widget/PasswordEntryKeyboard;-><init>(Landroid/content/Context;IIII)V

    #@9
    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;IIII)V
    .registers 11
    .parameter "context"
    .parameter "xmlLayoutResId"
    .parameter "mode"
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    const/4 v3, 0x1

    #@2
    const/4 v2, 0x0

    #@3
    const/4 v1, 0x0

    #@4
    .line 70
    invoke-direct/range {p0 .. p5}, Landroid/inputmethodservice/Keyboard;-><init>(Landroid/content/Context;IIII)V

    #@7
    .line 45
    new-array v0, v4, [Landroid/graphics/drawable/Drawable;

    #@9
    aput-object v1, v0, v2

    #@b
    aput-object v1, v0, v3

    #@d
    iput-object v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboard;->mOldShiftIcons:[Landroid/graphics/drawable/Drawable;

    #@f
    .line 46
    new-array v0, v4, [Landroid/inputmethodservice/Keyboard$Key;

    #@11
    aput-object v1, v0, v2

    #@13
    aput-object v1, v0, v3

    #@15
    iput-object v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboard;->mShiftKeys:[Landroid/inputmethodservice/Keyboard$Key;

    #@17
    .line 51
    iput v2, p0, Lcom/android/internal/widget/PasswordEntryKeyboard;->mShiftState:I

    #@19
    .line 71
    invoke-direct {p0, p1}, Lcom/android/internal/widget/PasswordEntryKeyboard;->init(Landroid/content/Context;)V

    #@1c
    .line 72
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/CharSequence;II)V
    .registers 11
    .parameter "context"
    .parameter "layoutTemplateResId"
    .parameter "characters"
    .parameter "columns"
    .parameter "horizontalPadding"

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    const/4 v3, 0x1

    #@2
    const/4 v2, 0x0

    #@3
    const/4 v1, 0x0

    #@4
    .line 84
    invoke-direct/range {p0 .. p5}, Landroid/inputmethodservice/Keyboard;-><init>(Landroid/content/Context;ILjava/lang/CharSequence;II)V

    #@7
    .line 45
    new-array v0, v4, [Landroid/graphics/drawable/Drawable;

    #@9
    aput-object v1, v0, v2

    #@b
    aput-object v1, v0, v3

    #@d
    iput-object v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboard;->mOldShiftIcons:[Landroid/graphics/drawable/Drawable;

    #@f
    .line 46
    new-array v0, v4, [Landroid/inputmethodservice/Keyboard$Key;

    #@11
    aput-object v1, v0, v2

    #@13
    aput-object v1, v0, v3

    #@15
    iput-object v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboard;->mShiftKeys:[Landroid/inputmethodservice/Keyboard$Key;

    #@17
    .line 51
    iput v2, p0, Lcom/android/internal/widget/PasswordEntryKeyboard;->mShiftState:I

    #@19
    .line 85
    return-void
.end method

.method private init(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 75
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@3
    move-result-object v0

    #@4
    .line 76
    .local v0, res:Landroid/content/res/Resources;
    const v1, 0x10805ad

    #@7
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@a
    move-result-object v1

    #@b
    iput-object v1, p0, Lcom/android/internal/widget/PasswordEntryKeyboard;->mShiftIcon:Landroid/graphics/drawable/Drawable;

    #@d
    .line 77
    const v1, 0x10805ae

    #@10
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@13
    move-result-object v1

    #@14
    iput-object v1, p0, Lcom/android/internal/widget/PasswordEntryKeyboard;->mShiftLockIcon:Landroid/graphics/drawable/Drawable;

    #@16
    .line 78
    const v1, 0x105001b

    #@19
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    #@1c
    move-result v1

    #@1d
    sput v1, Lcom/android/internal/widget/PasswordEntryKeyboard;->sSpacebarVerticalCorrection:I

    #@1f
    .line 80
    return-void
.end method


# virtual methods
.method protected createKeyFromXml(Landroid/content/res/Resources;Landroid/inputmethodservice/Keyboard$Row;IILandroid/content/res/XmlResourceParser;)Landroid/inputmethodservice/Keyboard$Key;
    .registers 14
    .parameter "res"
    .parameter "parent"
    .parameter "x"
    .parameter "y"
    .parameter "parser"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 90
    new-instance v0, Lcom/android/internal/widget/PasswordEntryKeyboard$LatinKey;

    #@3
    move-object v1, p1

    #@4
    move-object v2, p2

    #@5
    move v3, p3

    #@6
    move v4, p4

    #@7
    move-object v5, p5

    #@8
    invoke-direct/range {v0 .. v5}, Lcom/android/internal/widget/PasswordEntryKeyboard$LatinKey;-><init>(Landroid/content/res/Resources;Landroid/inputmethodservice/Keyboard$Row;IILandroid/content/res/XmlResourceParser;)V

    #@b
    .line 91
    .local v0, key:Lcom/android/internal/widget/PasswordEntryKeyboard$LatinKey;
    iget-object v1, v0, Lcom/android/internal/widget/PasswordEntryKeyboard$LatinKey;->codes:[I

    #@d
    aget v6, v1, v7

    #@f
    .line 92
    .local v6, code:I
    if-ltz v6, :cond_24

    #@11
    const/16 v1, 0xa

    #@13
    if-eq v6, v1, :cond_24

    #@15
    const/16 v1, 0x20

    #@17
    if-lt v6, v1, :cond_1d

    #@19
    const/16 v1, 0x7f

    #@1b
    if-le v6, v1, :cond_24

    #@1d
    .line 94
    :cond_1d
    const-string v1, " "

    #@1f
    iput-object v1, v0, Landroid/inputmethodservice/Keyboard$Key;->label:Ljava/lang/CharSequence;

    #@21
    .line 95
    invoke-virtual {v0, v7}, Lcom/android/internal/widget/PasswordEntryKeyboard$LatinKey;->setEnabled(Z)V

    #@24
    .line 97
    :cond_24
    iget-object v1, v0, Lcom/android/internal/widget/PasswordEntryKeyboard$LatinKey;->codes:[I

    #@26
    aget v1, v1, v7

    #@28
    sparse-switch v1, :sswitch_data_36

    #@2b
    .line 108
    :goto_2b
    return-object v0

    #@2c
    .line 99
    :sswitch_2c
    iput-object v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboard;->mEnterKey:Landroid/inputmethodservice/Keyboard$Key;

    #@2e
    goto :goto_2b

    #@2f
    .line 102
    :sswitch_2f
    iput-object v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboard;->mF1Key:Landroid/inputmethodservice/Keyboard$Key;

    #@31
    goto :goto_2b

    #@32
    .line 105
    :sswitch_32
    iput-object v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboard;->mSpaceKey:Landroid/inputmethodservice/Keyboard$Key;

    #@34
    goto :goto_2b

    #@35
    .line 97
    nop

    #@36
    :sswitch_data_36
    .sparse-switch
        -0x67 -> :sswitch_2f
        0xa -> :sswitch_2c
        0x20 -> :sswitch_32
    .end sparse-switch
.end method

.method enableShiftLock()V
    .registers 8

    #@0
    .prologue
    .line 143
    const/4 v1, 0x0

    #@1
    .line 144
    .local v1, i:I
    invoke-virtual {p0}, Lcom/android/internal/widget/PasswordEntryKeyboard;->getShiftKeyIndices()[I

    #@4
    move-result-object v0

    #@5
    .local v0, arr$:[I
    array-length v4, v0

    #@6
    .local v4, len$:I
    const/4 v2, 0x0

    #@7
    .local v2, i$:I
    :goto_7
    if-ge v2, v4, :cond_40

    #@9
    aget v3, v0, v2

    #@b
    .line 145
    .local v3, index:I
    if-ltz v3, :cond_3d

    #@d
    iget-object v5, p0, Lcom/android/internal/widget/PasswordEntryKeyboard;->mShiftKeys:[Landroid/inputmethodservice/Keyboard$Key;

    #@f
    array-length v5, v5

    #@10
    if-ge v1, v5, :cond_3d

    #@12
    .line 146
    iget-object v6, p0, Lcom/android/internal/widget/PasswordEntryKeyboard;->mShiftKeys:[Landroid/inputmethodservice/Keyboard$Key;

    #@14
    invoke-virtual {p0}, Lcom/android/internal/widget/PasswordEntryKeyboard;->getKeys()Ljava/util/List;

    #@17
    move-result-object v5

    #@18
    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@1b
    move-result-object v5

    #@1c
    check-cast v5, Landroid/inputmethodservice/Keyboard$Key;

    #@1e
    aput-object v5, v6, v1

    #@20
    .line 147
    iget-object v5, p0, Lcom/android/internal/widget/PasswordEntryKeyboard;->mShiftKeys:[Landroid/inputmethodservice/Keyboard$Key;

    #@22
    aget-object v5, v5, v1

    #@24
    instance-of v5, v5, Lcom/android/internal/widget/PasswordEntryKeyboard$LatinKey;

    #@26
    if-eqz v5, :cond_31

    #@28
    .line 148
    iget-object v5, p0, Lcom/android/internal/widget/PasswordEntryKeyboard;->mShiftKeys:[Landroid/inputmethodservice/Keyboard$Key;

    #@2a
    aget-object v5, v5, v1

    #@2c
    check-cast v5, Lcom/android/internal/widget/PasswordEntryKeyboard$LatinKey;

    #@2e
    invoke-virtual {v5}, Lcom/android/internal/widget/PasswordEntryKeyboard$LatinKey;->enableShiftLock()V

    #@31
    .line 150
    :cond_31
    iget-object v5, p0, Lcom/android/internal/widget/PasswordEntryKeyboard;->mOldShiftIcons:[Landroid/graphics/drawable/Drawable;

    #@33
    iget-object v6, p0, Lcom/android/internal/widget/PasswordEntryKeyboard;->mShiftKeys:[Landroid/inputmethodservice/Keyboard$Key;

    #@35
    aget-object v6, v6, v1

    #@37
    iget-object v6, v6, Landroid/inputmethodservice/Keyboard$Key;->icon:Landroid/graphics/drawable/Drawable;

    #@39
    aput-object v6, v5, v1

    #@3b
    .line 151
    add-int/lit8 v1, v1, 0x1

    #@3d
    .line 144
    :cond_3d
    add-int/lit8 v2, v2, 0x1

    #@3f
    goto :goto_7

    #@40
    .line 154
    .end local v3           #index:I
    :cond_40
    return-void
.end method

.method public isShifted()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 212
    iget-object v1, p0, Lcom/android/internal/widget/PasswordEntryKeyboard;->mShiftKeys:[Landroid/inputmethodservice/Keyboard$Key;

    #@3
    aget-object v1, v1, v0

    #@5
    if-eqz v1, :cond_d

    #@7
    .line 213
    iget v1, p0, Lcom/android/internal/widget/PasswordEntryKeyboard;->mShiftState:I

    #@9
    if-eqz v1, :cond_c

    #@b
    const/4 v0, 0x1

    #@c
    .line 215
    :cond_c
    :goto_c
    return v0

    #@d
    :cond_d
    invoke-super {p0}, Landroid/inputmethodservice/Keyboard;->isShifted()Z

    #@10
    move-result v0

    #@11
    goto :goto_c
.end method

.method setEnterKeyResources(Landroid/content/res/Resources;III)V
    .registers 9
    .parameter "res"
    .parameter "previewId"
    .parameter "iconId"
    .parameter "labelId"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v3, 0x0

    #@2
    .line 119
    iget-object v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboard;->mEnterKey:Landroid/inputmethodservice/Keyboard$Key;

    #@4
    if-eqz v0, :cond_47

    #@6
    .line 121
    iget-object v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboard;->mEnterKey:Landroid/inputmethodservice/Keyboard$Key;

    #@8
    iput-object v1, v0, Landroid/inputmethodservice/Keyboard$Key;->popupCharacters:Ljava/lang/CharSequence;

    #@a
    .line 122
    iget-object v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboard;->mEnterKey:Landroid/inputmethodservice/Keyboard$Key;

    #@c
    iput v3, v0, Landroid/inputmethodservice/Keyboard$Key;->popupResId:I

    #@e
    .line 123
    iget-object v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboard;->mEnterKey:Landroid/inputmethodservice/Keyboard$Key;

    #@10
    iput-object v1, v0, Landroid/inputmethodservice/Keyboard$Key;->text:Ljava/lang/CharSequence;

    #@12
    .line 125
    iget-object v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboard;->mEnterKey:Landroid/inputmethodservice/Keyboard$Key;

    #@14
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@17
    move-result-object v1

    #@18
    iput-object v1, v0, Landroid/inputmethodservice/Keyboard$Key;->iconPreview:Landroid/graphics/drawable/Drawable;

    #@1a
    .line 126
    iget-object v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboard;->mEnterKey:Landroid/inputmethodservice/Keyboard$Key;

    #@1c
    invoke-virtual {p1, p3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@1f
    move-result-object v1

    #@20
    iput-object v1, v0, Landroid/inputmethodservice/Keyboard$Key;->icon:Landroid/graphics/drawable/Drawable;

    #@22
    .line 127
    iget-object v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboard;->mEnterKey:Landroid/inputmethodservice/Keyboard$Key;

    #@24
    invoke-virtual {p1, p4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@27
    move-result-object v1

    #@28
    iput-object v1, v0, Landroid/inputmethodservice/Keyboard$Key;->label:Ljava/lang/CharSequence;

    #@2a
    .line 130
    iget-object v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboard;->mEnterKey:Landroid/inputmethodservice/Keyboard$Key;

    #@2c
    iget-object v0, v0, Landroid/inputmethodservice/Keyboard$Key;->iconPreview:Landroid/graphics/drawable/Drawable;

    #@2e
    if-eqz v0, :cond_47

    #@30
    .line 131
    iget-object v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboard;->mEnterKey:Landroid/inputmethodservice/Keyboard$Key;

    #@32
    iget-object v0, v0, Landroid/inputmethodservice/Keyboard$Key;->iconPreview:Landroid/graphics/drawable/Drawable;

    #@34
    iget-object v1, p0, Lcom/android/internal/widget/PasswordEntryKeyboard;->mEnterKey:Landroid/inputmethodservice/Keyboard$Key;

    #@36
    iget-object v1, v1, Landroid/inputmethodservice/Keyboard$Key;->iconPreview:Landroid/graphics/drawable/Drawable;

    #@38
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@3b
    move-result v1

    #@3c
    iget-object v2, p0, Lcom/android/internal/widget/PasswordEntryKeyboard;->mEnterKey:Landroid/inputmethodservice/Keyboard$Key;

    #@3e
    iget-object v2, v2, Landroid/inputmethodservice/Keyboard$Key;->iconPreview:Landroid/graphics/drawable/Drawable;

    #@40
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@43
    move-result v2

    #@44
    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@47
    .line 136
    :cond_47
    return-void
.end method

.method setShiftLocked(Z)V
    .registers 7
    .parameter "shiftLocked"

    #@0
    .prologue
    .line 164
    iget-object v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboard;->mShiftKeys:[Landroid/inputmethodservice/Keyboard$Key;

    #@2
    .local v0, arr$:[Landroid/inputmethodservice/Keyboard$Key;
    array-length v2, v0

    #@3
    .local v2, len$:I
    const/4 v1, 0x0

    #@4
    .local v1, i$:I
    :goto_4
    if-ge v1, v2, :cond_13

    #@6
    aget-object v3, v0, v1

    #@8
    .line 165
    .local v3, shiftKey:Landroid/inputmethodservice/Keyboard$Key;
    if-eqz v3, :cond_10

    #@a
    .line 166
    iput-boolean p1, v3, Landroid/inputmethodservice/Keyboard$Key;->on:Z

    #@c
    .line 167
    iget-object v4, p0, Lcom/android/internal/widget/PasswordEntryKeyboard;->mShiftLockIcon:Landroid/graphics/drawable/Drawable;

    #@e
    iput-object v4, v3, Landroid/inputmethodservice/Keyboard$Key;->icon:Landroid/graphics/drawable/Drawable;

    #@10
    .line 164
    :cond_10
    add-int/lit8 v1, v1, 0x1

    #@12
    goto :goto_4

    #@13
    .line 170
    .end local v3           #shiftKey:Landroid/inputmethodservice/Keyboard$Key;
    :cond_13
    if-eqz p1, :cond_19

    #@15
    const/4 v4, 0x2

    #@16
    :goto_16
    iput v4, p0, Lcom/android/internal/widget/PasswordEntryKeyboard;->mShiftState:I

    #@18
    .line 171
    return-void

    #@19
    .line 170
    :cond_19
    const/4 v4, 0x1

    #@1a
    goto :goto_16
.end method

.method public setShifted(Z)Z
    .registers 7
    .parameter "shiftState"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 182
    const/4 v1, 0x0

    #@3
    .line 183
    .local v1, shiftChanged:Z
    if-nez p1, :cond_2f

    #@5
    .line 184
    iget v4, p0, Lcom/android/internal/widget/PasswordEntryKeyboard;->mShiftState:I

    #@7
    if-eqz v4, :cond_2d

    #@9
    move v1, v2

    #@a
    .line 185
    :goto_a
    iput v3, p0, Lcom/android/internal/widget/PasswordEntryKeyboard;->mShiftState:I

    #@c
    .line 190
    :cond_c
    :goto_c
    const/4 v0, 0x0

    #@d
    .local v0, i:I
    :goto_d
    iget-object v2, p0, Lcom/android/internal/widget/PasswordEntryKeyboard;->mShiftKeys:[Landroid/inputmethodservice/Keyboard$Key;

    #@f
    array-length v2, v2

    #@10
    if-ge v0, v2, :cond_50

    #@12
    .line 191
    iget-object v2, p0, Lcom/android/internal/widget/PasswordEntryKeyboard;->mShiftKeys:[Landroid/inputmethodservice/Keyboard$Key;

    #@14
    aget-object v2, v2, v0

    #@16
    if-eqz v2, :cond_2a

    #@18
    .line 192
    if-nez p1, :cond_3d

    #@1a
    .line 193
    iget-object v2, p0, Lcom/android/internal/widget/PasswordEntryKeyboard;->mShiftKeys:[Landroid/inputmethodservice/Keyboard$Key;

    #@1c
    aget-object v2, v2, v0

    #@1e
    iput-boolean v3, v2, Landroid/inputmethodservice/Keyboard$Key;->on:Z

    #@20
    .line 194
    iget-object v2, p0, Lcom/android/internal/widget/PasswordEntryKeyboard;->mShiftKeys:[Landroid/inputmethodservice/Keyboard$Key;

    #@22
    aget-object v2, v2, v0

    #@24
    iget-object v4, p0, Lcom/android/internal/widget/PasswordEntryKeyboard;->mOldShiftIcons:[Landroid/graphics/drawable/Drawable;

    #@26
    aget-object v4, v4, v0

    #@28
    iput-object v4, v2, Landroid/inputmethodservice/Keyboard$Key;->icon:Landroid/graphics/drawable/Drawable;

    #@2a
    .line 190
    :cond_2a
    :goto_2a
    add-int/lit8 v0, v0, 0x1

    #@2c
    goto :goto_d

    #@2d
    .end local v0           #i:I
    :cond_2d
    move v1, v3

    #@2e
    .line 184
    goto :goto_a

    #@2f
    .line 186
    :cond_2f
    iget v4, p0, Lcom/android/internal/widget/PasswordEntryKeyboard;->mShiftState:I

    #@31
    if-nez v4, :cond_c

    #@33
    .line 187
    iget v4, p0, Lcom/android/internal/widget/PasswordEntryKeyboard;->mShiftState:I

    #@35
    if-nez v4, :cond_3b

    #@37
    move v1, v2

    #@38
    .line 188
    :goto_38
    iput v2, p0, Lcom/android/internal/widget/PasswordEntryKeyboard;->mShiftState:I

    #@3a
    goto :goto_c

    #@3b
    :cond_3b
    move v1, v3

    #@3c
    .line 187
    goto :goto_38

    #@3d
    .line 195
    .restart local v0       #i:I
    :cond_3d
    iget v2, p0, Lcom/android/internal/widget/PasswordEntryKeyboard;->mShiftState:I

    #@3f
    if-nez v2, :cond_2a

    #@41
    .line 196
    iget-object v2, p0, Lcom/android/internal/widget/PasswordEntryKeyboard;->mShiftKeys:[Landroid/inputmethodservice/Keyboard$Key;

    #@43
    aget-object v2, v2, v0

    #@45
    iput-boolean v3, v2, Landroid/inputmethodservice/Keyboard$Key;->on:Z

    #@47
    .line 197
    iget-object v2, p0, Lcom/android/internal/widget/PasswordEntryKeyboard;->mShiftKeys:[Landroid/inputmethodservice/Keyboard$Key;

    #@49
    aget-object v2, v2, v0

    #@4b
    iget-object v4, p0, Lcom/android/internal/widget/PasswordEntryKeyboard;->mShiftIcon:Landroid/graphics/drawable/Drawable;

    #@4d
    iput-object v4, v2, Landroid/inputmethodservice/Keyboard$Key;->icon:Landroid/graphics/drawable/Drawable;

    #@4f
    goto :goto_2a

    #@50
    .line 203
    :cond_50
    return v1
.end method
