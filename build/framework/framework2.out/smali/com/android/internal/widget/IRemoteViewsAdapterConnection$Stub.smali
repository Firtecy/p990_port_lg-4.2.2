.class public abstract Lcom/android/internal/widget/IRemoteViewsAdapterConnection$Stub;
.super Landroid/os/Binder;
.source "IRemoteViewsAdapterConnection.java"

# interfaces
.implements Lcom/android/internal/widget/IRemoteViewsAdapterConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/widget/IRemoteViewsAdapterConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/widget/IRemoteViewsAdapterConnection$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.android.internal.widget.IRemoteViewsAdapterConnection"

.field static final TRANSACTION_onServiceConnected:I = 0x1

.field static final TRANSACTION_onServiceDisconnected:I = 0x2


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 15
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 16
    const-string v0, "com.android.internal.widget.IRemoteViewsAdapterConnection"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/android/internal/widget/IRemoteViewsAdapterConnection$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 17
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/android/internal/widget/IRemoteViewsAdapterConnection;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 24
    if-nez p0, :cond_4

    #@2
    .line 25
    const/4 v0, 0x0

    #@3
    .line 31
    :goto_3
    return-object v0

    #@4
    .line 27
    :cond_4
    const-string v1, "com.android.internal.widget.IRemoteViewsAdapterConnection"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 28
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/android/internal/widget/IRemoteViewsAdapterConnection;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 29
    check-cast v0, Lcom/android/internal/widget/IRemoteViewsAdapterConnection;

    #@12
    goto :goto_3

    #@13
    .line 31
    :cond_13
    new-instance v0, Lcom/android/internal/widget/IRemoteViewsAdapterConnection$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/android/internal/widget/IRemoteViewsAdapterConnection$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 35
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 8
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 39
    sparse-switch p1, :sswitch_data_2c

    #@4
    .line 63
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v1

    #@8
    :goto_8
    return v1

    #@9
    .line 43
    :sswitch_9
    const-string v2, "com.android.internal.widget.IRemoteViewsAdapterConnection"

    #@b
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    goto :goto_8

    #@f
    .line 48
    :sswitch_f
    const-string v2, "com.android.internal.widget.IRemoteViewsAdapterConnection"

    #@11
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 50
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@17
    move-result-object v0

    #@18
    .line 51
    .local v0, _arg0:Landroid/os/IBinder;
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/IRemoteViewsAdapterConnection$Stub;->onServiceConnected(Landroid/os/IBinder;)V

    #@1b
    .line 52
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1e
    goto :goto_8

    #@1f
    .line 57
    .end local v0           #_arg0:Landroid/os/IBinder;
    :sswitch_1f
    const-string v2, "com.android.internal.widget.IRemoteViewsAdapterConnection"

    #@21
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@24
    .line 58
    invoke-virtual {p0}, Lcom/android/internal/widget/IRemoteViewsAdapterConnection$Stub;->onServiceDisconnected()V

    #@27
    .line 59
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2a
    goto :goto_8

    #@2b
    .line 39
    nop

    #@2c
    :sswitch_data_2c
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_1f
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
