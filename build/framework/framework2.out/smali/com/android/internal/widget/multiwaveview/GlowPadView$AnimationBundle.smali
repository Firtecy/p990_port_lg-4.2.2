.class Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;
.super Ljava/util/ArrayList;
.source "GlowPadView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/widget/multiwaveview/GlowPadView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AnimationBundle"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/ArrayList",
        "<",
        "Lcom/android/internal/widget/multiwaveview/Tweener;",
        ">;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = -0x57b2878d90ed8b98L


# instance fields
.field private mSuspended:Z

.field final synthetic this$0:Lcom/android/internal/widget/multiwaveview/GlowPadView;


# direct methods
.method private constructor <init>(Lcom/android/internal/widget/multiwaveview/GlowPadView;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 127
    iput-object p1, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;->this$0:Lcom/android/internal/widget/multiwaveview/GlowPadView;

    #@2
    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/internal/widget/multiwaveview/GlowPadView;Lcom/android/internal/widget/multiwaveview/GlowPadView$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 127
    invoke-direct {p0, p1}, Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;-><init>(Lcom/android/internal/widget/multiwaveview/GlowPadView;)V

    #@3
    return-void
.end method


# virtual methods
.method public cancel()V
    .registers 5

    #@0
    .prologue
    .line 141
    invoke-virtual {p0}, Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;->size()I

    #@3
    move-result v1

    #@4
    .line 142
    .local v1, count:I
    const/4 v2, 0x0

    #@5
    .local v2, i:I
    :goto_5
    if-ge v2, v1, :cond_15

    #@7
    .line 143
    invoke-virtual {p0, v2}, Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;->get(I)Ljava/lang/Object;

    #@a
    move-result-object v0

    #@b
    check-cast v0, Lcom/android/internal/widget/multiwaveview/Tweener;

    #@d
    .line 144
    .local v0, anim:Lcom/android/internal/widget/multiwaveview/Tweener;
    iget-object v3, v0, Lcom/android/internal/widget/multiwaveview/Tweener;->animator:Landroid/animation/ObjectAnimator;

    #@f
    invoke-virtual {v3}, Landroid/animation/ObjectAnimator;->cancel()V

    #@12
    .line 142
    add-int/lit8 v2, v2, 0x1

    #@14
    goto :goto_5

    #@15
    .line 146
    .end local v0           #anim:Lcom/android/internal/widget/multiwaveview/Tweener;
    :cond_15
    invoke-virtual {p0}, Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;->clear()V

    #@18
    .line 147
    return-void
.end method

.method public setSuspended(Z)V
    .registers 2
    .parameter "suspend"

    #@0
    .prologue
    .line 159
    iput-boolean p1, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;->mSuspended:Z

    #@2
    .line 160
    return-void
.end method

.method public start()V
    .registers 5

    #@0
    .prologue
    .line 132
    iget-boolean v3, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;->mSuspended:Z

    #@2
    if-eqz v3, :cond_5

    #@4
    .line 138
    :cond_4
    return-void

    #@5
    .line 133
    :cond_5
    invoke-virtual {p0}, Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;->size()I

    #@8
    move-result v1

    #@9
    .line 134
    .local v1, count:I
    const/4 v2, 0x0

    #@a
    .local v2, i:I
    :goto_a
    if-ge v2, v1, :cond_4

    #@c
    .line 135
    invoke-virtual {p0, v2}, Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Lcom/android/internal/widget/multiwaveview/Tweener;

    #@12
    .line 136
    .local v0, anim:Lcom/android/internal/widget/multiwaveview/Tweener;
    iget-object v3, v0, Lcom/android/internal/widget/multiwaveview/Tweener;->animator:Landroid/animation/ObjectAnimator;

    #@14
    invoke-virtual {v3}, Landroid/animation/ObjectAnimator;->start()V

    #@17
    .line 134
    add-int/lit8 v2, v2, 0x1

    #@19
    goto :goto_a
.end method

.method public stop()V
    .registers 5

    #@0
    .prologue
    .line 150
    invoke-virtual {p0}, Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;->size()I

    #@3
    move-result v1

    #@4
    .line 151
    .local v1, count:I
    const/4 v2, 0x0

    #@5
    .local v2, i:I
    :goto_5
    if-ge v2, v1, :cond_15

    #@7
    .line 152
    invoke-virtual {p0, v2}, Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;->get(I)Ljava/lang/Object;

    #@a
    move-result-object v0

    #@b
    check-cast v0, Lcom/android/internal/widget/multiwaveview/Tweener;

    #@d
    .line 153
    .local v0, anim:Lcom/android/internal/widget/multiwaveview/Tweener;
    iget-object v3, v0, Lcom/android/internal/widget/multiwaveview/Tweener;->animator:Landroid/animation/ObjectAnimator;

    #@f
    invoke-virtual {v3}, Landroid/animation/ObjectAnimator;->end()V

    #@12
    .line 151
    add-int/lit8 v2, v2, 0x1

    #@14
    goto :goto_5

    #@15
    .line 155
    .end local v0           #anim:Lcom/android/internal/widget/multiwaveview/Tweener;
    :cond_15
    invoke-virtual {p0}, Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;->clear()V

    #@18
    .line 156
    return-void
.end method
