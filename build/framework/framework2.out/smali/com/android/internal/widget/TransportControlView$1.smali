.class Lcom/android/internal/widget/TransportControlView$1;
.super Landroid/os/Handler;
.source "TransportControlView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/widget/TransportControlView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/widget/TransportControlView;


# direct methods
.method constructor <init>(Lcom/android/internal/widget/TransportControlView;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 90
    iput-object p1, p0, Lcom/android/internal/widget/TransportControlView$1;->this$0:Lcom/android/internal/widget/TransportControlView;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 4
    .parameter "msg"

    #@0
    .prologue
    .line 93
    iget v0, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v0, :pswitch_data_ac

    #@5
    .line 129
    :cond_5
    :goto_5
    return-void

    #@6
    .line 95
    :pswitch_6
    iget-object v0, p0, Lcom/android/internal/widget/TransportControlView$1;->this$0:Lcom/android/internal/widget/TransportControlView;

    #@8
    invoke-static {v0}, Lcom/android/internal/widget/TransportControlView;->access$000(Lcom/android/internal/widget/TransportControlView;)I

    #@b
    move-result v0

    #@c
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@e
    if-ne v0, v1, :cond_5

    #@10
    iget-object v0, p0, Lcom/android/internal/widget/TransportControlView$1;->this$0:Lcom/android/internal/widget/TransportControlView;

    #@12
    iget v1, p1, Landroid/os/Message;->arg2:I

    #@14
    invoke-static {v0, v1}, Lcom/android/internal/widget/TransportControlView;->access$100(Lcom/android/internal/widget/TransportControlView;I)V

    #@17
    goto :goto_5

    #@18
    .line 99
    :pswitch_18
    iget-object v0, p0, Lcom/android/internal/widget/TransportControlView$1;->this$0:Lcom/android/internal/widget/TransportControlView;

    #@1a
    invoke-static {v0}, Lcom/android/internal/widget/TransportControlView;->access$000(Lcom/android/internal/widget/TransportControlView;)I

    #@1d
    move-result v0

    #@1e
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@20
    if-ne v0, v1, :cond_5

    #@22
    iget-object v1, p0, Lcom/android/internal/widget/TransportControlView$1;->this$0:Lcom/android/internal/widget/TransportControlView;

    #@24
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@26
    check-cast v0, Landroid/os/Bundle;

    #@28
    invoke-static {v1, v0}, Lcom/android/internal/widget/TransportControlView;->access$200(Lcom/android/internal/widget/TransportControlView;Landroid/os/Bundle;)V

    #@2b
    goto :goto_5

    #@2c
    .line 103
    :pswitch_2c
    iget-object v0, p0, Lcom/android/internal/widget/TransportControlView$1;->this$0:Lcom/android/internal/widget/TransportControlView;

    #@2e
    invoke-static {v0}, Lcom/android/internal/widget/TransportControlView;->access$000(Lcom/android/internal/widget/TransportControlView;)I

    #@31
    move-result v0

    #@32
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@34
    if-ne v0, v1, :cond_5

    #@36
    iget-object v0, p0, Lcom/android/internal/widget/TransportControlView$1;->this$0:Lcom/android/internal/widget/TransportControlView;

    #@38
    iget v1, p1, Landroid/os/Message;->arg2:I

    #@3a
    invoke-static {v0, v1}, Lcom/android/internal/widget/TransportControlView;->access$300(Lcom/android/internal/widget/TransportControlView;I)V

    #@3d
    goto :goto_5

    #@3e
    .line 107
    :pswitch_3e
    iget-object v0, p0, Lcom/android/internal/widget/TransportControlView$1;->this$0:Lcom/android/internal/widget/TransportControlView;

    #@40
    invoke-static {v0}, Lcom/android/internal/widget/TransportControlView;->access$000(Lcom/android/internal/widget/TransportControlView;)I

    #@43
    move-result v0

    #@44
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@46
    if-ne v0, v1, :cond_5

    #@48
    .line 108
    iget-object v0, p0, Lcom/android/internal/widget/TransportControlView$1;->this$0:Lcom/android/internal/widget/TransportControlView;

    #@4a
    invoke-static {v0}, Lcom/android/internal/widget/TransportControlView;->access$400(Lcom/android/internal/widget/TransportControlView;)Lcom/android/internal/widget/TransportControlView$Metadata;

    #@4d
    move-result-object v0

    #@4e
    invoke-static {v0}, Lcom/android/internal/widget/TransportControlView$Metadata;->access$500(Lcom/android/internal/widget/TransportControlView$Metadata;)Landroid/graphics/Bitmap;

    #@51
    move-result-object v0

    #@52
    if-eqz v0, :cond_61

    #@54
    .line 109
    iget-object v0, p0, Lcom/android/internal/widget/TransportControlView$1;->this$0:Lcom/android/internal/widget/TransportControlView;

    #@56
    invoke-static {v0}, Lcom/android/internal/widget/TransportControlView;->access$400(Lcom/android/internal/widget/TransportControlView;)Lcom/android/internal/widget/TransportControlView$Metadata;

    #@59
    move-result-object v0

    #@5a
    invoke-static {v0}, Lcom/android/internal/widget/TransportControlView$Metadata;->access$500(Lcom/android/internal/widget/TransportControlView$Metadata;)Landroid/graphics/Bitmap;

    #@5d
    move-result-object v0

    #@5e
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    #@61
    .line 111
    :cond_61
    iget-object v0, p0, Lcom/android/internal/widget/TransportControlView$1;->this$0:Lcom/android/internal/widget/TransportControlView;

    #@63
    invoke-static {v0}, Lcom/android/internal/widget/TransportControlView;->access$400(Lcom/android/internal/widget/TransportControlView;)Lcom/android/internal/widget/TransportControlView$Metadata;

    #@66
    move-result-object v1

    #@67
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@69
    check-cast v0, Landroid/graphics/Bitmap;

    #@6b
    invoke-static {v1, v0}, Lcom/android/internal/widget/TransportControlView$Metadata;->access$502(Lcom/android/internal/widget/TransportControlView$Metadata;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    #@6e
    .line 112
    iget-object v0, p0, Lcom/android/internal/widget/TransportControlView$1;->this$0:Lcom/android/internal/widget/TransportControlView;

    #@70
    invoke-static {v0}, Lcom/android/internal/widget/TransportControlView;->access$600(Lcom/android/internal/widget/TransportControlView;)Landroid/widget/ImageView;

    #@73
    move-result-object v0

    #@74
    iget-object v1, p0, Lcom/android/internal/widget/TransportControlView$1;->this$0:Lcom/android/internal/widget/TransportControlView;

    #@76
    invoke-static {v1}, Lcom/android/internal/widget/TransportControlView;->access$400(Lcom/android/internal/widget/TransportControlView;)Lcom/android/internal/widget/TransportControlView$Metadata;

    #@79
    move-result-object v1

    #@7a
    invoke-static {v1}, Lcom/android/internal/widget/TransportControlView$Metadata;->access$500(Lcom/android/internal/widget/TransportControlView$Metadata;)Landroid/graphics/Bitmap;

    #@7d
    move-result-object v1

    #@7e
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    #@81
    goto :goto_5

    #@82
    .line 117
    :pswitch_82
    iget v0, p1, Landroid/os/Message;->arg2:I

    #@84
    if-eqz v0, :cond_99

    #@86
    .line 119
    iget-object v0, p0, Lcom/android/internal/widget/TransportControlView$1;->this$0:Lcom/android/internal/widget/TransportControlView;

    #@88
    invoke-static {v0}, Lcom/android/internal/widget/TransportControlView;->access$700(Lcom/android/internal/widget/TransportControlView;)Lcom/android/internal/widget/LockScreenWidgetCallback;

    #@8b
    move-result-object v0

    #@8c
    if-eqz v0, :cond_99

    #@8e
    .line 120
    iget-object v0, p0, Lcom/android/internal/widget/TransportControlView$1;->this$0:Lcom/android/internal/widget/TransportControlView;

    #@90
    invoke-static {v0}, Lcom/android/internal/widget/TransportControlView;->access$700(Lcom/android/internal/widget/TransportControlView;)Lcom/android/internal/widget/LockScreenWidgetCallback;

    #@93
    move-result-object v0

    #@94
    iget-object v1, p0, Lcom/android/internal/widget/TransportControlView$1;->this$0:Lcom/android/internal/widget/TransportControlView;

    #@96
    invoke-interface {v0, v1}, Lcom/android/internal/widget/LockScreenWidgetCallback;->requestHide(Landroid/view/View;)V

    #@99
    .line 124
    :cond_99
    iget-object v0, p0, Lcom/android/internal/widget/TransportControlView$1;->this$0:Lcom/android/internal/widget/TransportControlView;

    #@9b
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@9d
    invoke-static {v0, v1}, Lcom/android/internal/widget/TransportControlView;->access$002(Lcom/android/internal/widget/TransportControlView;I)I

    #@a0
    .line 125
    iget-object v1, p0, Lcom/android/internal/widget/TransportControlView$1;->this$0:Lcom/android/internal/widget/TransportControlView;

    #@a2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@a4
    check-cast v0, Landroid/app/PendingIntent;

    #@a6
    invoke-static {v1, v0}, Lcom/android/internal/widget/TransportControlView;->access$802(Lcom/android/internal/widget/TransportControlView;Landroid/app/PendingIntent;)Landroid/app/PendingIntent;

    #@a9
    goto/16 :goto_5

    #@ab
    .line 93
    nop

    #@ac
    :pswitch_data_ac
    .packed-switch 0x64
        :pswitch_6
        :pswitch_18
        :pswitch_2c
        :pswitch_3e
        :pswitch_82
    .end packed-switch
.end method
