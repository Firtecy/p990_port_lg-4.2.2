.class public Lcom/android/internal/widget/LockPatternView$Cell;
.super Ljava/lang/Object;
.source "LockPatternView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/widget/LockPatternView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Cell"
.end annotation


# static fields
.field static sCells:[[Lcom/android/internal/widget/LockPatternView$Cell;


# instance fields
.field column:I

.field row:I


# direct methods
.method static constructor <clinit>()V
    .registers 5

    #@0
    .prologue
    const/4 v4, 0x3

    #@1
    .line 140
    filled-new-array {v4, v4}, [I

    #@4
    move-result-object v2

    #@5
    const-class v3, Lcom/android/internal/widget/LockPatternView$Cell;

    #@7
    invoke-static {v3, v2}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    #@a
    move-result-object v2

    #@b
    check-cast v2, [[Lcom/android/internal/widget/LockPatternView$Cell;

    #@d
    sput-object v2, Lcom/android/internal/widget/LockPatternView$Cell;->sCells:[[Lcom/android/internal/widget/LockPatternView$Cell;

    #@f
    .line 142
    const/4 v0, 0x0

    #@10
    .local v0, i:I
    :goto_10
    if-ge v0, v4, :cond_26

    #@12
    .line 143
    const/4 v1, 0x0

    #@13
    .local v1, j:I
    :goto_13
    if-ge v1, v4, :cond_23

    #@15
    .line 144
    sget-object v2, Lcom/android/internal/widget/LockPatternView$Cell;->sCells:[[Lcom/android/internal/widget/LockPatternView$Cell;

    #@17
    aget-object v2, v2, v0

    #@19
    new-instance v3, Lcom/android/internal/widget/LockPatternView$Cell;

    #@1b
    invoke-direct {v3, v0, v1}, Lcom/android/internal/widget/LockPatternView$Cell;-><init>(II)V

    #@1e
    aput-object v3, v2, v1

    #@20
    .line 143
    add-int/lit8 v1, v1, 0x1

    #@22
    goto :goto_13

    #@23
    .line 142
    :cond_23
    add-int/lit8 v0, v0, 0x1

    #@25
    goto :goto_10

    #@26
    .line 147
    .end local v1           #j:I
    :cond_26
    return-void
.end method

.method private constructor <init>(II)V
    .registers 3
    .parameter "row"
    .parameter "column"

    #@0
    .prologue
    .line 153
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 154
    invoke-static {p1, p2}, Lcom/android/internal/widget/LockPatternView$Cell;->checkRange(II)V

    #@6
    .line 155
    iput p1, p0, Lcom/android/internal/widget/LockPatternView$Cell;->row:I

    #@8
    .line 156
    iput p2, p0, Lcom/android/internal/widget/LockPatternView$Cell;->column:I

    #@a
    .line 157
    return-void
.end method

.method private static checkRange(II)V
    .registers 4
    .parameter "row"
    .parameter "column"

    #@0
    .prologue
    const/4 v0, 0x2

    #@1
    .line 177
    if-ltz p0, :cond_5

    #@3
    if-le p0, v0, :cond_d

    #@5
    .line 178
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@7
    const-string v1, "row must be in range 0-2"

    #@9
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@c
    throw v0

    #@d
    .line 180
    :cond_d
    if-ltz p1, :cond_11

    #@f
    if-le p1, v0, :cond_19

    #@11
    .line 181
    :cond_11
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@13
    const-string v1, "column must be in range 0-2"

    #@15
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@18
    throw v0

    #@19
    .line 183
    :cond_19
    return-void
.end method

.method public static declared-synchronized of(II)Lcom/android/internal/widget/LockPatternView$Cell;
    .registers 4
    .parameter "row"
    .parameter "column"

    #@0
    .prologue
    .line 172
    const-class v1, Lcom/android/internal/widget/LockPatternView$Cell;

    #@2
    monitor-enter v1

    #@3
    :try_start_3
    invoke-static {p0, p1}, Lcom/android/internal/widget/LockPatternView$Cell;->checkRange(II)V

    #@6
    .line 173
    sget-object v0, Lcom/android/internal/widget/LockPatternView$Cell;->sCells:[[Lcom/android/internal/widget/LockPatternView$Cell;

    #@8
    aget-object v0, v0, p0

    #@a
    aget-object v0, v0, p1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_e

    #@c
    monitor-exit v1

    #@d
    return-object v0

    #@e
    .line 172
    :catchall_e
    move-exception v0

    #@f
    monitor-exit v1

    #@10
    throw v0
.end method


# virtual methods
.method public getColumn()I
    .registers 2

    #@0
    .prologue
    .line 164
    iget v0, p0, Lcom/android/internal/widget/LockPatternView$Cell;->column:I

    #@2
    return v0
.end method

.method public getRow()I
    .registers 2

    #@0
    .prologue
    .line 160
    iget v0, p0, Lcom/android/internal/widget/LockPatternView$Cell;->row:I

    #@2
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 186
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "(row="

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget v1, p0, Lcom/android/internal/widget/LockPatternView$Cell;->row:I

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, ",clmn="

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget v1, p0, Lcom/android/internal/widget/LockPatternView$Cell;->column:I

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, ")"

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v0

    #@27
    return-object v0
.end method
