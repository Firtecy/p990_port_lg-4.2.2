.class Lcom/android/internal/widget/TransportControlView$Metadata;
.super Ljava/lang/Object;
.source "TransportControlView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/widget/TransportControlView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Metadata"
.end annotation


# instance fields
.field private albumTitle:Ljava/lang/String;

.field private artist:Ljava/lang/String;

.field private bitmap:Landroid/graphics/Bitmap;

.field final synthetic this$0:Lcom/android/internal/widget/TransportControlView;

.field private trackTitle:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/android/internal/widget/TransportControlView;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 252
    iput-object p1, p0, Lcom/android/internal/widget/TransportControlView$Metadata;->this$0:Lcom/android/internal/widget/TransportControlView;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method

.method static synthetic access$1000(Lcom/android/internal/widget/TransportControlView$Metadata;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 252
    iget-object v0, p0, Lcom/android/internal/widget/TransportControlView$Metadata;->trackTitle:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$1002(Lcom/android/internal/widget/TransportControlView$Metadata;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 252
    iput-object p1, p0, Lcom/android/internal/widget/TransportControlView$Metadata;->trackTitle:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$1100(Lcom/android/internal/widget/TransportControlView$Metadata;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 252
    iget-object v0, p0, Lcom/android/internal/widget/TransportControlView$Metadata;->albumTitle:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$1102(Lcom/android/internal/widget/TransportControlView$Metadata;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 252
    iput-object p1, p0, Lcom/android/internal/widget/TransportControlView$Metadata;->albumTitle:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$500(Lcom/android/internal/widget/TransportControlView$Metadata;)Landroid/graphics/Bitmap;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 252
    iget-object v0, p0, Lcom/android/internal/widget/TransportControlView$Metadata;->bitmap:Landroid/graphics/Bitmap;

    #@2
    return-object v0
.end method

.method static synthetic access$502(Lcom/android/internal/widget/TransportControlView$Metadata;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 252
    iput-object p1, p0, Lcom/android/internal/widget/TransportControlView$Metadata;->bitmap:Landroid/graphics/Bitmap;

    #@2
    return-object p1
.end method

.method static synthetic access$900(Lcom/android/internal/widget/TransportControlView$Metadata;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 252
    iget-object v0, p0, Lcom/android/internal/widget/TransportControlView$Metadata;->artist:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$902(Lcom/android/internal/widget/TransportControlView$Metadata;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 252
    iput-object p1, p0, Lcom/android/internal/widget/TransportControlView$Metadata;->artist:Ljava/lang/String;

    #@2
    return-object p1
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 259
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "Metadata[artist="

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget-object v1, p0, Lcom/android/internal/widget/TransportControlView$Metadata;->artist:Ljava/lang/String;

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, " trackTitle="

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget-object v1, p0, Lcom/android/internal/widget/TransportControlView$Metadata;->trackTitle:Ljava/lang/String;

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, " albumTitle="

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    iget-object v1, p0, Lcom/android/internal/widget/TransportControlView$Metadata;->albumTitle:Ljava/lang/String;

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    const-string v1, "]"

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v0

    #@33
    return-object v0
.end method
