.class public Lcom/android/internal/widget/multiwaveview/PointCloud;
.super Ljava/lang/Object;
.source "PointCloud.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/widget/multiwaveview/PointCloud$Point;,
        Lcom/android/internal/widget/multiwaveview/PointCloud$GlowManager;,
        Lcom/android/internal/widget/multiwaveview/PointCloud$WaveManager;
    }
.end annotation


# static fields
.field private static final INNER_POINTS:I = 0x8

.field private static final MAX_POINT_SIZE:F = 4.0f

.field private static final MIN_POINT_SIZE:F = 2.0f

.field private static final PI:F = 3.1415927f

.field private static final TAG:Ljava/lang/String; = "PointCloud"


# instance fields
.field glowManager:Lcom/android/internal/widget/multiwaveview/PointCloud$GlowManager;

.field private mCenterX:F

.field private mCenterY:F

.field private mDrawable:Landroid/graphics/drawable/Drawable;

.field private mOuterRadius:F

.field private mPaint:Landroid/graphics/Paint;

.field private mPointCloud:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/widget/multiwaveview/PointCloud$Point;",
            ">;"
        }
    .end annotation
.end field

.field private mScale:F

.field waveManager:Lcom/android/internal/widget/multiwaveview/PointCloud$WaveManager;


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;)V
    .registers 6
    .parameter "drawable"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/16 v1, 0xff

    #@3
    const/4 v2, 0x1

    #@4
    .line 118
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@7
    .line 33
    new-instance v0, Ljava/util/ArrayList;

    #@9
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@c
    iput-object v0, p0, Lcom/android/internal/widget/multiwaveview/PointCloud;->mPointCloud:Ljava/util/ArrayList;

    #@e
    .line 38
    const/high16 v0, 0x3f80

    #@10
    iput v0, p0, Lcom/android/internal/widget/multiwaveview/PointCloud;->mScale:F

    #@12
    .line 42
    new-instance v0, Lcom/android/internal/widget/multiwaveview/PointCloud$WaveManager;

    #@14
    invoke-direct {v0, p0}, Lcom/android/internal/widget/multiwaveview/PointCloud$WaveManager;-><init>(Lcom/android/internal/widget/multiwaveview/PointCloud;)V

    #@17
    iput-object v0, p0, Lcom/android/internal/widget/multiwaveview/PointCloud;->waveManager:Lcom/android/internal/widget/multiwaveview/PointCloud$WaveManager;

    #@19
    .line 43
    new-instance v0, Lcom/android/internal/widget/multiwaveview/PointCloud$GlowManager;

    #@1b
    invoke-direct {v0, p0}, Lcom/android/internal/widget/multiwaveview/PointCloud$GlowManager;-><init>(Lcom/android/internal/widget/multiwaveview/PointCloud;)V

    #@1e
    iput-object v0, p0, Lcom/android/internal/widget/multiwaveview/PointCloud;->glowManager:Lcom/android/internal/widget/multiwaveview/PointCloud$GlowManager;

    #@20
    .line 119
    new-instance v0, Landroid/graphics/Paint;

    #@22
    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    #@25
    iput-object v0, p0, Lcom/android/internal/widget/multiwaveview/PointCloud;->mPaint:Landroid/graphics/Paint;

    #@27
    .line 120
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/PointCloud;->mPaint:Landroid/graphics/Paint;

    #@29
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    #@2c
    .line 121
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/PointCloud;->mPaint:Landroid/graphics/Paint;

    #@2e
    invoke-static {v1, v1, v1}, Landroid/graphics/Color;->rgb(III)I

    #@31
    move-result v1

    #@32
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    #@35
    .line 122
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/PointCloud;->mPaint:Landroid/graphics/Paint;

    #@37
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    #@3a
    .line 123
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/PointCloud;->mPaint:Landroid/graphics/Paint;

    #@3c
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setDither(Z)V

    #@3f
    .line 125
    iput-object p1, p0, Lcom/android/internal/widget/multiwaveview/PointCloud;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@41
    .line 126
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/PointCloud;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@43
    if-eqz v0, :cond_50

    #@45
    .line 127
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@48
    move-result v0

    #@49
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@4c
    move-result v1

    #@4d
    invoke-virtual {p1, v3, v3, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@50
    .line 129
    :cond_50
    return-void
.end method

.method private static hypot(FF)F
    .registers 4
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 171
    mul-float v0, p0, p0

    #@2
    mul-float v1, p1, p1

    #@4
    add-float/2addr v0, v1

    #@5
    invoke-static {v0}, Landroid/util/FloatMath;->sqrt(F)F

    #@8
    move-result v0

    #@9
    return v0
.end method

.method private interp(FFF)F
    .registers 5
    .parameter "min"
    .parameter "max"
    .parameter "f"

    #@0
    .prologue
    .line 200
    sub-float v0, p2, p1

    #@2
    mul-float/2addr v0, p3

    #@3
    add-float/2addr v0, p1

    #@4
    return v0
.end method

.method private static max(FF)F
    .registers 3
    .parameter "a"
    .parameter "b"

    #@0
    .prologue
    .line 175
    cmpl-float v0, p0, p1

    #@2
    if-lez v0, :cond_5

    #@4
    .end local p0
    :goto_4
    return p0

    #@5
    .restart local p0
    :cond_5
    move p0, p1

    #@6
    goto :goto_4
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .registers 16
    .parameter "canvas"

    #@0
    .prologue
    .line 204
    iget-object v6, p0, Lcom/android/internal/widget/multiwaveview/PointCloud;->mPointCloud:Ljava/util/ArrayList;

    #@2
    .line 205
    .local v6, points:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/widget/multiwaveview/PointCloud$Point;>;"
    const/4 v10, 0x1

    #@3
    invoke-virtual {p1, v10}, Landroid/graphics/Canvas;->save(I)I

    #@6
    .line 206
    iget v10, p0, Lcom/android/internal/widget/multiwaveview/PointCloud;->mScale:F

    #@8
    iget v11, p0, Lcom/android/internal/widget/multiwaveview/PointCloud;->mScale:F

    #@a
    iget v12, p0, Lcom/android/internal/widget/multiwaveview/PointCloud;->mCenterX:F

    #@c
    iget v13, p0, Lcom/android/internal/widget/multiwaveview/PointCloud;->mCenterY:F

    #@e
    invoke-virtual {p1, v10, v11, v12, v13}, Landroid/graphics/Canvas;->scale(FFFF)V

    #@11
    .line 207
    const/4 v3, 0x0

    #@12
    .local v3, i:I
    :goto_12
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@15
    move-result v10

    #@16
    if-ge v3, v10, :cond_85

    #@18
    .line 208
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1b
    move-result-object v4

    #@1c
    check-cast v4, Lcom/android/internal/widget/multiwaveview/PointCloud$Point;

    #@1e
    .line 209
    .local v4, point:Lcom/android/internal/widget/multiwaveview/PointCloud$Point;
    const/high16 v10, 0x4080

    #@20
    const/high16 v11, 0x4000

    #@22
    iget v12, v4, Lcom/android/internal/widget/multiwaveview/PointCloud$Point;->radius:F

    #@24
    iget v13, p0, Lcom/android/internal/widget/multiwaveview/PointCloud;->mOuterRadius:F

    #@26
    div-float/2addr v12, v13

    #@27
    invoke-direct {p0, v10, v11, v12}, Lcom/android/internal/widget/multiwaveview/PointCloud;->interp(FFF)F

    #@2a
    move-result v5

    #@2b
    .line 211
    .local v5, pointSize:F
    iget v10, v4, Lcom/android/internal/widget/multiwaveview/PointCloud$Point;->x:F

    #@2d
    iget v11, p0, Lcom/android/internal/widget/multiwaveview/PointCloud;->mCenterX:F

    #@2f
    add-float v7, v10, v11

    #@31
    .line 212
    .local v7, px:F
    iget v10, v4, Lcom/android/internal/widget/multiwaveview/PointCloud$Point;->y:F

    #@33
    iget v11, p0, Lcom/android/internal/widget/multiwaveview/PointCloud;->mCenterY:F

    #@35
    add-float v8, v10, v11

    #@37
    .line 213
    .local v8, py:F
    invoke-virtual {p0, v4}, Lcom/android/internal/widget/multiwaveview/PointCloud;->getAlphaForPoint(Lcom/android/internal/widget/multiwaveview/PointCloud$Point;)I

    #@3a
    move-result v0

    #@3b
    .line 215
    .local v0, alpha:I
    if-nez v0, :cond_40

    #@3d
    .line 207
    :goto_3d
    add-int/lit8 v3, v3, 0x1

    #@3f
    goto :goto_12

    #@40
    .line 217
    :cond_40
    iget-object v10, p0, Lcom/android/internal/widget/multiwaveview/PointCloud;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@42
    if-eqz v10, :cond_7a

    #@44
    .line 218
    const/4 v10, 0x1

    #@45
    invoke-virtual {p1, v10}, Landroid/graphics/Canvas;->save(I)I

    #@48
    .line 219
    iget-object v10, p0, Lcom/android/internal/widget/multiwaveview/PointCloud;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@4a
    invoke-virtual {v10}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@4d
    move-result v10

    #@4e
    int-to-float v10, v10

    #@4f
    const/high16 v11, 0x3f00

    #@51
    mul-float v1, v10, v11

    #@53
    .line 220
    .local v1, cx:F
    iget-object v10, p0, Lcom/android/internal/widget/multiwaveview/PointCloud;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@55
    invoke-virtual {v10}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@58
    move-result v10

    #@59
    int-to-float v10, v10

    #@5a
    const/high16 v11, 0x3f00

    #@5c
    mul-float v2, v10, v11

    #@5e
    .line 221
    .local v2, cy:F
    const/high16 v10, 0x4080

    #@60
    div-float v9, v5, v10

    #@62
    .line 222
    .local v9, s:F
    invoke-virtual {p1, v9, v9, v7, v8}, Landroid/graphics/Canvas;->scale(FFFF)V

    #@65
    .line 223
    sub-float v10, v7, v1

    #@67
    sub-float v11, v8, v2

    #@69
    invoke-virtual {p1, v10, v11}, Landroid/graphics/Canvas;->translate(FF)V

    #@6c
    .line 224
    iget-object v10, p0, Lcom/android/internal/widget/multiwaveview/PointCloud;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@6e
    invoke-virtual {v10, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    #@71
    .line 225
    iget-object v10, p0, Lcom/android/internal/widget/multiwaveview/PointCloud;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@73
    invoke-virtual {v10, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@76
    .line 226
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    #@79
    goto :goto_3d

    #@7a
    .line 228
    .end local v1           #cx:F
    .end local v2           #cy:F
    .end local v9           #s:F
    :cond_7a
    iget-object v10, p0, Lcom/android/internal/widget/multiwaveview/PointCloud;->mPaint:Landroid/graphics/Paint;

    #@7c
    invoke-virtual {v10, v0}, Landroid/graphics/Paint;->setAlpha(I)V

    #@7f
    .line 229
    iget-object v10, p0, Lcom/android/internal/widget/multiwaveview/PointCloud;->mPaint:Landroid/graphics/Paint;

    #@81
    invoke-virtual {p1, v7, v8, v5, v10}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    #@84
    goto :goto_3d

    #@85
    .line 232
    .end local v0           #alpha:I
    .end local v4           #point:Lcom/android/internal/widget/multiwaveview/PointCloud$Point;
    .end local v5           #pointSize:F
    .end local v7           #px:F
    .end local v8           #py:F
    :cond_85
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    #@88
    .line 233
    return-void
.end method

.method public getAlphaForPoint(Lcom/android/internal/widget/multiwaveview/PointCloud$Point;)I
    .registers 15
    .parameter "point"

    #@0
    .prologue
    const v12, 0x3f490fdb

    #@3
    const/4 v11, 0x0

    #@4
    .line 180
    iget-object v6, p0, Lcom/android/internal/widget/multiwaveview/PointCloud;->glowManager:Lcom/android/internal/widget/multiwaveview/PointCloud$GlowManager;

    #@6
    invoke-static {v6}, Lcom/android/internal/widget/multiwaveview/PointCloud$GlowManager;->access$000(Lcom/android/internal/widget/multiwaveview/PointCloud$GlowManager;)F

    #@9
    move-result v6

    #@a
    iget v7, p1, Lcom/android/internal/widget/multiwaveview/PointCloud$Point;->x:F

    #@c
    sub-float/2addr v6, v7

    #@d
    iget-object v7, p0, Lcom/android/internal/widget/multiwaveview/PointCloud;->glowManager:Lcom/android/internal/widget/multiwaveview/PointCloud$GlowManager;

    #@f
    invoke-static {v7}, Lcom/android/internal/widget/multiwaveview/PointCloud$GlowManager;->access$100(Lcom/android/internal/widget/multiwaveview/PointCloud$GlowManager;)F

    #@12
    move-result v7

    #@13
    iget v8, p1, Lcom/android/internal/widget/multiwaveview/PointCloud$Point;->y:F

    #@15
    sub-float/2addr v7, v8

    #@16
    invoke-static {v6, v7}, Lcom/android/internal/widget/multiwaveview/PointCloud;->hypot(FF)F

    #@19
    move-result v3

    #@1a
    .line 181
    .local v3, glowDistance:F
    const/4 v2, 0x0

    #@1b
    .line 182
    .local v2, glowAlpha:F
    iget-object v6, p0, Lcom/android/internal/widget/multiwaveview/PointCloud;->glowManager:Lcom/android/internal/widget/multiwaveview/PointCloud$GlowManager;

    #@1d
    invoke-static {v6}, Lcom/android/internal/widget/multiwaveview/PointCloud$GlowManager;->access$200(Lcom/android/internal/widget/multiwaveview/PointCloud$GlowManager;)F

    #@20
    move-result v6

    #@21
    cmpg-float v6, v3, v6

    #@23
    if-gez v6, :cond_46

    #@25
    .line 183
    mul-float v6, v12, v3

    #@27
    iget-object v7, p0, Lcom/android/internal/widget/multiwaveview/PointCloud;->glowManager:Lcom/android/internal/widget/multiwaveview/PointCloud$GlowManager;

    #@29
    invoke-static {v7}, Lcom/android/internal/widget/multiwaveview/PointCloud$GlowManager;->access$200(Lcom/android/internal/widget/multiwaveview/PointCloud$GlowManager;)F

    #@2c
    move-result v7

    #@2d
    div-float/2addr v6, v7

    #@2e
    invoke-static {v6}, Landroid/util/FloatMath;->cos(F)F

    #@31
    move-result v0

    #@32
    .line 184
    .local v0, cosf:F
    iget-object v6, p0, Lcom/android/internal/widget/multiwaveview/PointCloud;->glowManager:Lcom/android/internal/widget/multiwaveview/PointCloud$GlowManager;

    #@34
    invoke-static {v6}, Lcom/android/internal/widget/multiwaveview/PointCloud$GlowManager;->access$300(Lcom/android/internal/widget/multiwaveview/PointCloud$GlowManager;)F

    #@37
    move-result v6

    #@38
    float-to-double v7, v0

    #@39
    const-wide/high16 v9, 0x4024

    #@3b
    invoke-static {v7, v8, v9, v10}, Ljava/lang/Math;->pow(DD)D

    #@3e
    move-result-wide v7

    #@3f
    double-to-float v7, v7

    #@40
    invoke-static {v11, v7}, Lcom/android/internal/widget/multiwaveview/PointCloud;->max(FF)F

    #@43
    move-result v7

    #@44
    mul-float v2, v6, v7

    #@46
    .line 188
    .end local v0           #cosf:F
    :cond_46
    iget v6, p1, Lcom/android/internal/widget/multiwaveview/PointCloud$Point;->x:F

    #@48
    iget v7, p1, Lcom/android/internal/widget/multiwaveview/PointCloud$Point;->y:F

    #@4a
    invoke-static {v6, v7}, Lcom/android/internal/widget/multiwaveview/PointCloud;->hypot(FF)F

    #@4d
    move-result v4

    #@4e
    .line 189
    .local v4, radius:F
    iget-object v6, p0, Lcom/android/internal/widget/multiwaveview/PointCloud;->waveManager:Lcom/android/internal/widget/multiwaveview/PointCloud$WaveManager;

    #@50
    invoke-static {v6}, Lcom/android/internal/widget/multiwaveview/PointCloud$WaveManager;->access$400(Lcom/android/internal/widget/multiwaveview/PointCloud$WaveManager;)F

    #@53
    move-result v6

    #@54
    sub-float v1, v4, v6

    #@56
    .line 190
    .local v1, distanceToWaveRing:F
    const/4 v5, 0x0

    #@57
    .line 191
    .local v5, waveAlpha:F
    iget-object v6, p0, Lcom/android/internal/widget/multiwaveview/PointCloud;->waveManager:Lcom/android/internal/widget/multiwaveview/PointCloud$WaveManager;

    #@59
    invoke-static {v6}, Lcom/android/internal/widget/multiwaveview/PointCloud$WaveManager;->access$500(Lcom/android/internal/widget/multiwaveview/PointCloud$WaveManager;)F

    #@5c
    move-result v6

    #@5d
    const/high16 v7, 0x3f00

    #@5f
    mul-float/2addr v6, v7

    #@60
    cmpg-float v6, v1, v6

    #@62
    if-gez v6, :cond_89

    #@64
    cmpg-float v6, v1, v11

    #@66
    if-gez v6, :cond_89

    #@68
    .line 192
    mul-float v6, v12, v1

    #@6a
    iget-object v7, p0, Lcom/android/internal/widget/multiwaveview/PointCloud;->waveManager:Lcom/android/internal/widget/multiwaveview/PointCloud$WaveManager;

    #@6c
    invoke-static {v7}, Lcom/android/internal/widget/multiwaveview/PointCloud$WaveManager;->access$500(Lcom/android/internal/widget/multiwaveview/PointCloud$WaveManager;)F

    #@6f
    move-result v7

    #@70
    div-float/2addr v6, v7

    #@71
    invoke-static {v6}, Landroid/util/FloatMath;->cos(F)F

    #@74
    move-result v0

    #@75
    .line 193
    .restart local v0       #cosf:F
    iget-object v6, p0, Lcom/android/internal/widget/multiwaveview/PointCloud;->waveManager:Lcom/android/internal/widget/multiwaveview/PointCloud$WaveManager;

    #@77
    invoke-static {v6}, Lcom/android/internal/widget/multiwaveview/PointCloud$WaveManager;->access$600(Lcom/android/internal/widget/multiwaveview/PointCloud$WaveManager;)F

    #@7a
    move-result v6

    #@7b
    float-to-double v7, v0

    #@7c
    const-wide/high16 v9, 0x4034

    #@7e
    invoke-static {v7, v8, v9, v10}, Ljava/lang/Math;->pow(DD)D

    #@81
    move-result-wide v7

    #@82
    double-to-float v7, v7

    #@83
    invoke-static {v11, v7}, Lcom/android/internal/widget/multiwaveview/PointCloud;->max(FF)F

    #@86
    move-result v7

    #@87
    mul-float v5, v6, v7

    #@89
    .line 196
    .end local v0           #cosf:F
    :cond_89
    invoke-static {v2, v5}, Lcom/android/internal/widget/multiwaveview/PointCloud;->max(FF)F

    #@8c
    move-result v6

    #@8d
    const/high16 v7, 0x437f

    #@8f
    mul-float/2addr v6, v7

    #@90
    float-to-int v6, v6

    #@91
    return v6
.end method

.method public getScale()F
    .registers 2

    #@0
    .prologue
    .line 167
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/PointCloud;->mScale:F

    #@2
    return v0
.end method

.method public makePointCloud(FF)V
    .registers 20
    .parameter "innerRadius"
    .parameter "outerRadius"

    #@0
    .prologue
    .line 137
    const/4 v15, 0x0

    #@1
    cmpl-float v15, p1, v15

    #@3
    if-nez v15, :cond_d

    #@5
    .line 138
    const-string v15, "PointCloud"

    #@7
    const-string v16, "Must specify an inner radius"

    #@9
    invoke-static/range {v15 .. v16}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 160
    :cond_c
    return-void

    #@d
    .line 141
    :cond_d
    move/from16 v0, p2

    #@f
    move-object/from16 v1, p0

    #@11
    iput v0, v1, Lcom/android/internal/widget/multiwaveview/PointCloud;->mOuterRadius:F

    #@13
    .line 142
    move-object/from16 v0, p0

    #@15
    iget-object v15, v0, Lcom/android/internal/widget/multiwaveview/PointCloud;->mPointCloud:Ljava/util/ArrayList;

    #@17
    invoke-virtual {v15}, Ljava/util/ArrayList;->clear()V

    #@1a
    .line 143
    sub-float v10, p2, p1

    #@1c
    .line 144
    .local v10, pointAreaRadius:F
    const v15, 0x40c90fdb

    #@1f
    mul-float v15, v15, p1

    #@21
    const/high16 v16, 0x4100

    #@23
    div-float v7, v15, v16

    #@25
    .line 145
    .local v7, ds:F
    div-float v15, v10, v7

    #@27
    invoke-static {v15}, Ljava/lang/Math;->round(F)I

    #@2a
    move-result v3

    #@2b
    .line 146
    .local v3, bands:I
    int-to-float v15, v3

    #@2c
    div-float v6, v10, v15

    #@2e
    .line 147
    .local v6, dr:F
    move/from16 v12, p1

    #@30
    .line 148
    .local v12, r:F
    const/4 v2, 0x0

    #@31
    .local v2, b:I
    :goto_31
    if-gt v2, v3, :cond_c

    #@33
    .line 149
    const v15, 0x40c90fdb

    #@36
    mul-float v4, v15, v12

    #@38
    .line 150
    .local v4, circumference:F
    div-float v15, v4, v7

    #@3a
    float-to-int v11, v15

    #@3b
    .line 151
    .local v11, pointsInBand:I
    const v8, 0x3fc90fdb

    #@3e
    .line 152
    .local v8, eta:F
    const v15, 0x40c90fdb

    #@41
    int-to-float v0, v11

    #@42
    move/from16 v16, v0

    #@44
    div-float v5, v15, v16

    #@46
    .line 153
    .local v5, dEta:F
    const/4 v9, 0x0

    #@47
    .local v9, i:I
    :goto_47
    if-ge v9, v11, :cond_69

    #@49
    .line 154
    invoke-static {v8}, Landroid/util/FloatMath;->cos(F)F

    #@4c
    move-result v15

    #@4d
    mul-float v13, v12, v15

    #@4f
    .line 155
    .local v13, x:F
    invoke-static {v8}, Landroid/util/FloatMath;->sin(F)F

    #@52
    move-result v15

    #@53
    mul-float v14, v12, v15

    #@55
    .line 156
    .local v14, y:F
    add-float/2addr v8, v5

    #@56
    .line 157
    move-object/from16 v0, p0

    #@58
    iget-object v15, v0, Lcom/android/internal/widget/multiwaveview/PointCloud;->mPointCloud:Ljava/util/ArrayList;

    #@5a
    new-instance v16, Lcom/android/internal/widget/multiwaveview/PointCloud$Point;

    #@5c
    move-object/from16 v0, v16

    #@5e
    move-object/from16 v1, p0

    #@60
    invoke-direct {v0, v1, v13, v14, v12}, Lcom/android/internal/widget/multiwaveview/PointCloud$Point;-><init>(Lcom/android/internal/widget/multiwaveview/PointCloud;FFF)V

    #@63
    invoke-virtual/range {v15 .. v16}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@66
    .line 153
    add-int/lit8 v9, v9, 0x1

    #@68
    goto :goto_47

    #@69
    .line 148
    .end local v13           #x:F
    .end local v14           #y:F
    :cond_69
    add-int/lit8 v2, v2, 0x1

    #@6b
    add-float/2addr v12, v6

    #@6c
    goto :goto_31
.end method

.method public setCenter(FF)V
    .registers 3
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 132
    iput p1, p0, Lcom/android/internal/widget/multiwaveview/PointCloud;->mCenterX:F

    #@2
    .line 133
    iput p2, p0, Lcom/android/internal/widget/multiwaveview/PointCloud;->mCenterY:F

    #@4
    .line 134
    return-void
.end method

.method public setScale(F)V
    .registers 2
    .parameter "scale"

    #@0
    .prologue
    .line 163
    iput p1, p0, Lcom/android/internal/widget/multiwaveview/PointCloud;->mScale:F

    #@2
    .line 164
    return-void
.end method
