.class public Lcom/android/internal/widget/PointerLocationView;
.super Landroid/view/View;
.source "PointerLocationView.java"

# interfaces
.implements Landroid/hardware/input/InputManager$InputDeviceListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;,
        Lcom/android/internal/widget/PointerLocationView$PointerState;
    }
.end annotation


# static fields
.field private static final ALT_STRATEGY_PROPERY_KEY:Ljava/lang/String; = "debug.velocitytracker.alt"

.field private static final TAG:Ljava/lang/String; = "Pointer"


# instance fields
.field private final ESTIMATE_FUTURE_POINTS:I

.field private final ESTIMATE_INTERVAL:F

.field private final ESTIMATE_PAST_POINTS:I

.field private mActivePointerId:I

.field private final mAltVelocity:Landroid/view/VelocityTracker;

.field private mCurDown:Z

.field private mCurNumPointers:I

.field private mHeaderBottom:I

.field private final mIm:Landroid/hardware/input/InputManager;

.field private mMaxNumPointers:I

.field private final mPaint:Landroid/graphics/Paint;

.field private final mPathPaint:Landroid/graphics/Paint;

.field private final mPointers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/widget/PointerLocationView$PointerState;",
            ">;"
        }
    .end annotation
.end field

.field private mPrintCoords:Z

.field private mReusableOvalRect:Landroid/graphics/RectF;

.field private final mTargetPaint:Landroid/graphics/Paint;

.field private final mTempCoords:Landroid/view/MotionEvent$PointerCoords;

.field private final mText:Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

.field private final mTextBackgroundPaint:Landroid/graphics/Paint;

.field private final mTextLevelPaint:Landroid/graphics/Paint;

.field private final mTextMetrics:Landroid/graphics/Paint$FontMetricsInt;

.field private final mTextPaint:Landroid/graphics/Paint;

.field private final mVC:Landroid/view/ViewConfiguration;

.field private final mVelocity:Landroid/view/VelocityTracker;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 11
    .parameter "c"

    #@0
    .prologue
    const/16 v8, 0xc0

    #@2
    const/4 v7, 0x1

    #@3
    const/16 v6, 0xff

    #@5
    const/4 v5, 0x0

    #@6
    .line 148
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    #@9
    .line 118
    const/4 v2, 0x4

    #@a
    iput v2, p0, Lcom/android/internal/widget/PointerLocationView;->ESTIMATE_PAST_POINTS:I

    #@c
    .line 119
    const/4 v2, 0x2

    #@d
    iput v2, p0, Lcom/android/internal/widget/PointerLocationView;->ESTIMATE_FUTURE_POINTS:I

    #@f
    .line 120
    const v2, 0x3ca3d70a

    #@12
    iput v2, p0, Lcom/android/internal/widget/PointerLocationView;->ESTIMATE_INTERVAL:F

    #@14
    .line 131
    new-instance v2, Landroid/graphics/Paint$FontMetricsInt;

    #@16
    invoke-direct {v2}, Landroid/graphics/Paint$FontMetricsInt;-><init>()V

    #@19
    iput-object v2, p0, Lcom/android/internal/widget/PointerLocationView;->mTextMetrics:Landroid/graphics/Paint$FontMetricsInt;

    #@1b
    .line 137
    new-instance v2, Ljava/util/ArrayList;

    #@1d
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@20
    iput-object v2, p0, Lcom/android/internal/widget/PointerLocationView;->mPointers:Ljava/util/ArrayList;

    #@22
    .line 138
    new-instance v2, Landroid/view/MotionEvent$PointerCoords;

    #@24
    invoke-direct {v2}, Landroid/view/MotionEvent$PointerCoords;-><init>()V

    #@27
    iput-object v2, p0, Lcom/android/internal/widget/PointerLocationView;->mTempCoords:Landroid/view/MotionEvent$PointerCoords;

    #@29
    .line 143
    new-instance v2, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@2b
    invoke-direct {v2}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;-><init>()V

    #@2e
    iput-object v2, p0, Lcom/android/internal/widget/PointerLocationView;->mText:Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@30
    .line 145
    iput-boolean v7, p0, Lcom/android/internal/widget/PointerLocationView;->mPrintCoords:Z

    #@32
    .line 214
    new-instance v2, Landroid/graphics/RectF;

    #@34
    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    #@37
    iput-object v2, p0, Lcom/android/internal/widget/PointerLocationView;->mReusableOvalRect:Landroid/graphics/RectF;

    #@39
    .line 149
    invoke-virtual {p0, v7}, Lcom/android/internal/widget/PointerLocationView;->setFocusableInTouchMode(Z)V

    #@3c
    .line 151
    const-string v2, "input"

    #@3e
    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@41
    move-result-object v2

    #@42
    check-cast v2, Landroid/hardware/input/InputManager;

    #@44
    iput-object v2, p0, Lcom/android/internal/widget/PointerLocationView;->mIm:Landroid/hardware/input/InputManager;

    #@46
    .line 153
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    #@49
    move-result-object v2

    #@4a
    iput-object v2, p0, Lcom/android/internal/widget/PointerLocationView;->mVC:Landroid/view/ViewConfiguration;

    #@4c
    .line 154
    new-instance v2, Landroid/graphics/Paint;

    #@4e
    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    #@51
    iput-object v2, p0, Lcom/android/internal/widget/PointerLocationView;->mTextPaint:Landroid/graphics/Paint;

    #@53
    .line 155
    iget-object v2, p0, Lcom/android/internal/widget/PointerLocationView;->mTextPaint:Landroid/graphics/Paint;

    #@55
    invoke-virtual {v2, v7}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    #@58
    .line 156
    iget-object v2, p0, Lcom/android/internal/widget/PointerLocationView;->mTextPaint:Landroid/graphics/Paint;

    #@5a
    const/high16 v3, 0x4120

    #@5c
    invoke-virtual {p0}, Lcom/android/internal/widget/PointerLocationView;->getResources()Landroid/content/res/Resources;

    #@5f
    move-result-object v4

    #@60
    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@63
    move-result-object v4

    #@64
    iget v4, v4, Landroid/util/DisplayMetrics;->density:F

    #@66
    mul-float/2addr v3, v4

    #@67
    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    #@6a
    .line 158
    iget-object v2, p0, Lcom/android/internal/widget/PointerLocationView;->mTextPaint:Landroid/graphics/Paint;

    #@6c
    invoke-virtual {v2, v6, v5, v5, v5}, Landroid/graphics/Paint;->setARGB(IIII)V

    #@6f
    .line 159
    new-instance v2, Landroid/graphics/Paint;

    #@71
    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    #@74
    iput-object v2, p0, Lcom/android/internal/widget/PointerLocationView;->mTextBackgroundPaint:Landroid/graphics/Paint;

    #@76
    .line 160
    iget-object v2, p0, Lcom/android/internal/widget/PointerLocationView;->mTextBackgroundPaint:Landroid/graphics/Paint;

    #@78
    invoke-virtual {v2, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    #@7b
    .line 161
    iget-object v2, p0, Lcom/android/internal/widget/PointerLocationView;->mTextBackgroundPaint:Landroid/graphics/Paint;

    #@7d
    const/16 v3, 0x80

    #@7f
    invoke-virtual {v2, v3, v6, v6, v6}, Landroid/graphics/Paint;->setARGB(IIII)V

    #@82
    .line 162
    new-instance v2, Landroid/graphics/Paint;

    #@84
    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    #@87
    iput-object v2, p0, Lcom/android/internal/widget/PointerLocationView;->mTextLevelPaint:Landroid/graphics/Paint;

    #@89
    .line 163
    iget-object v2, p0, Lcom/android/internal/widget/PointerLocationView;->mTextLevelPaint:Landroid/graphics/Paint;

    #@8b
    invoke-virtual {v2, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    #@8e
    .line 164
    iget-object v2, p0, Lcom/android/internal/widget/PointerLocationView;->mTextLevelPaint:Landroid/graphics/Paint;

    #@90
    invoke-virtual {v2, v8, v6, v5, v5}, Landroid/graphics/Paint;->setARGB(IIII)V

    #@93
    .line 165
    new-instance v2, Landroid/graphics/Paint;

    #@95
    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    #@98
    iput-object v2, p0, Lcom/android/internal/widget/PointerLocationView;->mPaint:Landroid/graphics/Paint;

    #@9a
    .line 166
    iget-object v2, p0, Lcom/android/internal/widget/PointerLocationView;->mPaint:Landroid/graphics/Paint;

    #@9c
    invoke-virtual {v2, v7}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    #@9f
    .line 167
    iget-object v2, p0, Lcom/android/internal/widget/PointerLocationView;->mPaint:Landroid/graphics/Paint;

    #@a1
    invoke-virtual {v2, v6, v6, v6, v6}, Landroid/graphics/Paint;->setARGB(IIII)V

    #@a4
    .line 168
    iget-object v2, p0, Lcom/android/internal/widget/PointerLocationView;->mPaint:Landroid/graphics/Paint;

    #@a6
    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    #@a8
    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    #@ab
    .line 169
    iget-object v2, p0, Lcom/android/internal/widget/PointerLocationView;->mPaint:Landroid/graphics/Paint;

    #@ad
    const/high16 v3, 0x4000

    #@af
    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    #@b2
    .line 170
    new-instance v2, Landroid/graphics/Paint;

    #@b4
    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    #@b7
    iput-object v2, p0, Lcom/android/internal/widget/PointerLocationView;->mTargetPaint:Landroid/graphics/Paint;

    #@b9
    .line 171
    iget-object v2, p0, Lcom/android/internal/widget/PointerLocationView;->mTargetPaint:Landroid/graphics/Paint;

    #@bb
    invoke-virtual {v2, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    #@be
    .line 172
    iget-object v2, p0, Lcom/android/internal/widget/PointerLocationView;->mTargetPaint:Landroid/graphics/Paint;

    #@c0
    invoke-virtual {v2, v6, v5, v5, v8}, Landroid/graphics/Paint;->setARGB(IIII)V

    #@c3
    .line 173
    new-instance v2, Landroid/graphics/Paint;

    #@c5
    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    #@c8
    iput-object v2, p0, Lcom/android/internal/widget/PointerLocationView;->mPathPaint:Landroid/graphics/Paint;

    #@ca
    .line 174
    iget-object v2, p0, Lcom/android/internal/widget/PointerLocationView;->mPathPaint:Landroid/graphics/Paint;

    #@cc
    invoke-virtual {v2, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    #@cf
    .line 175
    iget-object v2, p0, Lcom/android/internal/widget/PointerLocationView;->mPathPaint:Landroid/graphics/Paint;

    #@d1
    const/16 v3, 0x60

    #@d3
    invoke-virtual {v2, v6, v5, v3, v6}, Landroid/graphics/Paint;->setARGB(IIII)V

    #@d6
    .line 176
    iget-object v2, p0, Lcom/android/internal/widget/PointerLocationView;->mPaint:Landroid/graphics/Paint;

    #@d8
    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    #@da
    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    #@dd
    .line 177
    iget-object v2, p0, Lcom/android/internal/widget/PointerLocationView;->mPaint:Landroid/graphics/Paint;

    #@df
    const/high16 v3, 0x3f80

    #@e1
    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    #@e4
    .line 179
    new-instance v1, Lcom/android/internal/widget/PointerLocationView$PointerState;

    #@e6
    invoke-direct {v1}, Lcom/android/internal/widget/PointerLocationView$PointerState;-><init>()V

    #@e9
    .line 180
    .local v1, ps:Lcom/android/internal/widget/PointerLocationView$PointerState;
    iget-object v2, p0, Lcom/android/internal/widget/PointerLocationView;->mPointers:Ljava/util/ArrayList;

    #@eb
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@ee
    .line 181
    iput v5, p0, Lcom/android/internal/widget/PointerLocationView;->mActivePointerId:I

    #@f0
    .line 183
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    #@f3
    move-result-object v2

    #@f4
    iput-object v2, p0, Lcom/android/internal/widget/PointerLocationView;->mVelocity:Landroid/view/VelocityTracker;

    #@f6
    .line 185
    const-string v2, "debug.velocitytracker.alt"

    #@f8
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@fb
    move-result-object v0

    #@fc
    .line 186
    .local v0, altStrategy:Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@ff
    move-result v2

    #@100
    if-eqz v2, :cond_121

    #@102
    .line 187
    const-string v2, "Pointer"

    #@104
    new-instance v3, Ljava/lang/StringBuilder;

    #@106
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@109
    const-string v4, "Comparing default velocity tracker strategy with "

    #@10b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10e
    move-result-object v3

    #@10f
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@112
    move-result-object v3

    #@113
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@116
    move-result-object v3

    #@117
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@11a
    .line 188
    invoke-static {v0}, Landroid/view/VelocityTracker;->obtain(Ljava/lang/String;)Landroid/view/VelocityTracker;

    #@11d
    move-result-object v2

    #@11e
    iput-object v2, p0, Lcom/android/internal/widget/PointerLocationView;->mAltVelocity:Landroid/view/VelocityTracker;

    #@120
    .line 192
    :goto_120
    return-void

    #@121
    .line 190
    :cond_121
    const/4 v2, 0x0

    #@122
    iput-object v2, p0, Lcom/android/internal/widget/PointerLocationView;->mAltVelocity:Landroid/view/VelocityTracker;

    #@124
    goto :goto_120
.end method

.method private drawOval(Landroid/graphics/Canvas;FFFFFLandroid/graphics/Paint;)V
    .registers 13
    .parameter "canvas"
    .parameter "x"
    .parameter "y"
    .parameter "major"
    .parameter "minor"
    .parameter "angle"
    .parameter "paint"

    #@0
    .prologue
    const/high16 v4, 0x4000

    #@2
    .line 217
    const/4 v0, 0x1

    #@3
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->save(I)I

    #@6
    .line 218
    const/high16 v0, 0x4334

    #@8
    mul-float/2addr v0, p6

    #@9
    float-to-double v0, v0

    #@a
    const-wide v2, 0x400921fb54442d18L

    #@f
    div-double/2addr v0, v2

    #@10
    double-to-float v0, v0

    #@11
    invoke-virtual {p1, v0, p2, p3}, Landroid/graphics/Canvas;->rotate(FFF)V

    #@14
    .line 219
    iget-object v0, p0, Lcom/android/internal/widget/PointerLocationView;->mReusableOvalRect:Landroid/graphics/RectF;

    #@16
    div-float v1, p5, v4

    #@18
    sub-float v1, p2, v1

    #@1a
    iput v1, v0, Landroid/graphics/RectF;->left:F

    #@1c
    .line 220
    iget-object v0, p0, Lcom/android/internal/widget/PointerLocationView;->mReusableOvalRect:Landroid/graphics/RectF;

    #@1e
    div-float v1, p5, v4

    #@20
    add-float/2addr v1, p2

    #@21
    iput v1, v0, Landroid/graphics/RectF;->right:F

    #@23
    .line 221
    iget-object v0, p0, Lcom/android/internal/widget/PointerLocationView;->mReusableOvalRect:Landroid/graphics/RectF;

    #@25
    div-float v1, p4, v4

    #@27
    sub-float v1, p3, v1

    #@29
    iput v1, v0, Landroid/graphics/RectF;->top:F

    #@2b
    .line 222
    iget-object v0, p0, Lcom/android/internal/widget/PointerLocationView;->mReusableOvalRect:Landroid/graphics/RectF;

    #@2d
    div-float v1, p4, v4

    #@2f
    add-float/2addr v1, p3

    #@30
    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    #@32
    .line 223
    iget-object v0, p0, Lcom/android/internal/widget/PointerLocationView;->mReusableOvalRect:Landroid/graphics/RectF;

    #@34
    invoke-virtual {p1, v0, p7}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    #@37
    .line 224
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    #@3a
    .line 225
    return-void
.end method

.method private logCoords(Ljava/lang/String;IILandroid/view/MotionEvent$PointerCoords;III)V
    .registers 15
    .parameter "type"
    .parameter "action"
    .parameter "index"
    .parameter "coords"
    .parameter "id"
    .parameter "toolType"
    .parameter "buttonState"

    #@0
    .prologue
    .line 458
    and-int/lit16 v1, p2, 0xff

    #@2
    packed-switch v1, :pswitch_data_16e

    #@5
    .line 503
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    .line 507
    .local v0, prefix:Ljava/lang/String;
    :goto_9
    const-string v1, "Pointer"

    #@b
    iget-object v2, p0, Lcom/android/internal/widget/PointerLocationView;->mText:Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@d
    invoke-virtual {v2}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->clear()Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2, p1}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(Ljava/lang/String;)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@14
    move-result-object v2

    #@15
    const-string v3, " id "

    #@17
    invoke-virtual {v2, v3}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(Ljava/lang/String;)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@1a
    move-result-object v2

    #@1b
    add-int/lit8 v3, p5, 0x1

    #@1d
    invoke-virtual {v2, v3}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(I)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@20
    move-result-object v2

    #@21
    const-string v3, ": "

    #@23
    invoke-virtual {v2, v3}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(Ljava/lang/String;)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@26
    move-result-object v2

    #@27
    invoke-virtual {v2, v0}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(Ljava/lang/String;)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@2a
    move-result-object v2

    #@2b
    const-string v3, " ("

    #@2d
    invoke-virtual {v2, v3}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(Ljava/lang/String;)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@30
    move-result-object v2

    #@31
    iget v3, p4, Landroid/view/MotionEvent$PointerCoords;->x:F

    #@33
    const/4 v4, 0x3

    #@34
    invoke-virtual {v2, v3, v4}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(FI)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@37
    move-result-object v2

    #@38
    const-string v3, ", "

    #@3a
    invoke-virtual {v2, v3}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(Ljava/lang/String;)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@3d
    move-result-object v2

    #@3e
    iget v3, p4, Landroid/view/MotionEvent$PointerCoords;->y:F

    #@40
    const/4 v4, 0x3

    #@41
    invoke-virtual {v2, v3, v4}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(FI)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@44
    move-result-object v2

    #@45
    const-string v3, ") Pressure="

    #@47
    invoke-virtual {v2, v3}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(Ljava/lang/String;)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@4a
    move-result-object v2

    #@4b
    iget v3, p4, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    #@4d
    const/4 v4, 0x3

    #@4e
    invoke-virtual {v2, v3, v4}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(FI)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@51
    move-result-object v2

    #@52
    const-string v3, " Size="

    #@54
    invoke-virtual {v2, v3}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(Ljava/lang/String;)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@57
    move-result-object v2

    #@58
    iget v3, p4, Landroid/view/MotionEvent$PointerCoords;->size:F

    #@5a
    const/4 v4, 0x3

    #@5b
    invoke-virtual {v2, v3, v4}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(FI)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@5e
    move-result-object v2

    #@5f
    const-string v3, " TouchMajor="

    #@61
    invoke-virtual {v2, v3}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(Ljava/lang/String;)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@64
    move-result-object v2

    #@65
    iget v3, p4, Landroid/view/MotionEvent$PointerCoords;->touchMajor:F

    #@67
    const/4 v4, 0x3

    #@68
    invoke-virtual {v2, v3, v4}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(FI)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@6b
    move-result-object v2

    #@6c
    const-string v3, " TouchMinor="

    #@6e
    invoke-virtual {v2, v3}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(Ljava/lang/String;)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@71
    move-result-object v2

    #@72
    iget v3, p4, Landroid/view/MotionEvent$PointerCoords;->touchMinor:F

    #@74
    const/4 v4, 0x3

    #@75
    invoke-virtual {v2, v3, v4}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(FI)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@78
    move-result-object v2

    #@79
    const-string v3, " ToolMajor="

    #@7b
    invoke-virtual {v2, v3}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(Ljava/lang/String;)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@7e
    move-result-object v2

    #@7f
    iget v3, p4, Landroid/view/MotionEvent$PointerCoords;->toolMajor:F

    #@81
    const/4 v4, 0x3

    #@82
    invoke-virtual {v2, v3, v4}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(FI)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@85
    move-result-object v2

    #@86
    const-string v3, " ToolMinor="

    #@88
    invoke-virtual {v2, v3}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(Ljava/lang/String;)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@8b
    move-result-object v2

    #@8c
    iget v3, p4, Landroid/view/MotionEvent$PointerCoords;->toolMinor:F

    #@8e
    const/4 v4, 0x3

    #@8f
    invoke-virtual {v2, v3, v4}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(FI)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@92
    move-result-object v2

    #@93
    const-string v3, " Orientation="

    #@95
    invoke-virtual {v2, v3}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(Ljava/lang/String;)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@98
    move-result-object v2

    #@99
    iget v3, p4, Landroid/view/MotionEvent$PointerCoords;->orientation:F

    #@9b
    const/high16 v4, 0x4334

    #@9d
    mul-float/2addr v3, v4

    #@9e
    float-to-double v3, v3

    #@9f
    const-wide v5, 0x400921fb54442d18L

    #@a4
    div-double/2addr v3, v5

    #@a5
    double-to-float v3, v3

    #@a6
    const/4 v4, 0x1

    #@a7
    invoke-virtual {v2, v3, v4}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(FI)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@aa
    move-result-object v2

    #@ab
    const-string v3, "deg"

    #@ad
    invoke-virtual {v2, v3}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(Ljava/lang/String;)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@b0
    move-result-object v2

    #@b1
    const-string v3, " Tilt="

    #@b3
    invoke-virtual {v2, v3}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(Ljava/lang/String;)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@b6
    move-result-object v2

    #@b7
    const/16 v3, 0x19

    #@b9
    invoke-virtual {p4, v3}, Landroid/view/MotionEvent$PointerCoords;->getAxisValue(I)F

    #@bc
    move-result v3

    #@bd
    const/high16 v4, 0x4334

    #@bf
    mul-float/2addr v3, v4

    #@c0
    float-to-double v3, v3

    #@c1
    const-wide v5, 0x400921fb54442d18L

    #@c6
    div-double/2addr v3, v5

    #@c7
    double-to-float v3, v3

    #@c8
    const/4 v4, 0x1

    #@c9
    invoke-virtual {v2, v3, v4}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(FI)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@cc
    move-result-object v2

    #@cd
    const-string v3, "deg"

    #@cf
    invoke-virtual {v2, v3}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(Ljava/lang/String;)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@d2
    move-result-object v2

    #@d3
    const-string v3, " Distance="

    #@d5
    invoke-virtual {v2, v3}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(Ljava/lang/String;)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@d8
    move-result-object v2

    #@d9
    const/16 v3, 0x18

    #@db
    invoke-virtual {p4, v3}, Landroid/view/MotionEvent$PointerCoords;->getAxisValue(I)F

    #@de
    move-result v3

    #@df
    const/4 v4, 0x1

    #@e0
    invoke-virtual {v2, v3, v4}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(FI)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@e3
    move-result-object v2

    #@e4
    const-string v3, " VScroll="

    #@e6
    invoke-virtual {v2, v3}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(Ljava/lang/String;)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@e9
    move-result-object v2

    #@ea
    const/16 v3, 0x9

    #@ec
    invoke-virtual {p4, v3}, Landroid/view/MotionEvent$PointerCoords;->getAxisValue(I)F

    #@ef
    move-result v3

    #@f0
    const/4 v4, 0x1

    #@f1
    invoke-virtual {v2, v3, v4}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(FI)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@f4
    move-result-object v2

    #@f5
    const-string v3, " HScroll="

    #@f7
    invoke-virtual {v2, v3}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(Ljava/lang/String;)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@fa
    move-result-object v2

    #@fb
    const/16 v3, 0xa

    #@fd
    invoke-virtual {p4, v3}, Landroid/view/MotionEvent$PointerCoords;->getAxisValue(I)F

    #@100
    move-result v3

    #@101
    const/4 v4, 0x1

    #@102
    invoke-virtual {v2, v3, v4}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(FI)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@105
    move-result-object v2

    #@106
    const-string v3, " ToolType="

    #@108
    invoke-virtual {v2, v3}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(Ljava/lang/String;)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@10b
    move-result-object v2

    #@10c
    invoke-static {p6}, Landroid/view/MotionEvent;->toolTypeToString(I)Ljava/lang/String;

    #@10f
    move-result-object v3

    #@110
    invoke-virtual {v2, v3}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(Ljava/lang/String;)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@113
    move-result-object v2

    #@114
    const-string v3, " ButtonState="

    #@116
    invoke-virtual {v2, v3}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(Ljava/lang/String;)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@119
    move-result-object v2

    #@11a
    invoke-static {p7}, Landroid/view/MotionEvent;->buttonStateToString(I)Ljava/lang/String;

    #@11d
    move-result-object v3

    #@11e
    invoke-virtual {v2, v3}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(Ljava/lang/String;)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@121
    move-result-object v2

    #@122
    invoke-virtual {v2}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->toString()Ljava/lang/String;

    #@125
    move-result-object v2

    #@126
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@129
    .line 529
    return-void

    #@12a
    .line 460
    .end local v0           #prefix:Ljava/lang/String;
    :pswitch_12a
    const-string v0, "DOWN"

    #@12c
    .line 461
    .restart local v0       #prefix:Ljava/lang/String;
    goto/16 :goto_9

    #@12e
    .line 463
    .end local v0           #prefix:Ljava/lang/String;
    :pswitch_12e
    const-string v0, "UP"

    #@130
    .line 464
    .restart local v0       #prefix:Ljava/lang/String;
    goto/16 :goto_9

    #@132
    .line 466
    .end local v0           #prefix:Ljava/lang/String;
    :pswitch_132
    const-string v0, "MOVE"

    #@134
    .line 467
    .restart local v0       #prefix:Ljava/lang/String;
    goto/16 :goto_9

    #@136
    .line 469
    .end local v0           #prefix:Ljava/lang/String;
    :pswitch_136
    const-string v0, "CANCEL"

    #@138
    .line 470
    .restart local v0       #prefix:Ljava/lang/String;
    goto/16 :goto_9

    #@13a
    .line 472
    .end local v0           #prefix:Ljava/lang/String;
    :pswitch_13a
    const-string v0, "OUTSIDE"

    #@13c
    .line 473
    .restart local v0       #prefix:Ljava/lang/String;
    goto/16 :goto_9

    #@13e
    .line 475
    .end local v0           #prefix:Ljava/lang/String;
    :pswitch_13e
    const v1, 0xff00

    #@141
    and-int/2addr v1, p2

    #@142
    shr-int/lit8 v1, v1, 0x8

    #@144
    if-ne p3, v1, :cond_14a

    #@146
    .line 477
    const-string v0, "DOWN"

    #@148
    .restart local v0       #prefix:Ljava/lang/String;
    goto/16 :goto_9

    #@14a
    .line 479
    .end local v0           #prefix:Ljava/lang/String;
    :cond_14a
    const-string v0, "MOVE"

    #@14c
    .line 481
    .restart local v0       #prefix:Ljava/lang/String;
    goto/16 :goto_9

    #@14e
    .line 483
    .end local v0           #prefix:Ljava/lang/String;
    :pswitch_14e
    const v1, 0xff00

    #@151
    and-int/2addr v1, p2

    #@152
    shr-int/lit8 v1, v1, 0x8

    #@154
    if-ne p3, v1, :cond_15a

    #@156
    .line 485
    const-string v0, "UP"

    #@158
    .restart local v0       #prefix:Ljava/lang/String;
    goto/16 :goto_9

    #@15a
    .line 487
    .end local v0           #prefix:Ljava/lang/String;
    :cond_15a
    const-string v0, "MOVE"

    #@15c
    .line 489
    .restart local v0       #prefix:Ljava/lang/String;
    goto/16 :goto_9

    #@15e
    .line 491
    .end local v0           #prefix:Ljava/lang/String;
    :pswitch_15e
    const-string v0, "HOVER MOVE"

    #@160
    .line 492
    .restart local v0       #prefix:Ljava/lang/String;
    goto/16 :goto_9

    #@162
    .line 494
    .end local v0           #prefix:Ljava/lang/String;
    :pswitch_162
    const-string v0, "HOVER ENTER"

    #@164
    .line 495
    .restart local v0       #prefix:Ljava/lang/String;
    goto/16 :goto_9

    #@166
    .line 497
    .end local v0           #prefix:Ljava/lang/String;
    :pswitch_166
    const-string v0, "HOVER EXIT"

    #@168
    .line 498
    .restart local v0       #prefix:Ljava/lang/String;
    goto/16 :goto_9

    #@16a
    .line 500
    .end local v0           #prefix:Ljava/lang/String;
    :pswitch_16a
    const-string v0, "SCROLL"

    #@16c
    .line 501
    .restart local v0       #prefix:Ljava/lang/String;
    goto/16 :goto_9

    #@16e
    .line 458
    :pswitch_data_16e
    .packed-switch 0x0
        :pswitch_12a
        :pswitch_12e
        :pswitch_132
        :pswitch_136
        :pswitch_13a
        :pswitch_13e
        :pswitch_14e
        :pswitch_15e
        :pswitch_16a
        :pswitch_162
        :pswitch_166
    .end packed-switch
.end method

.method private logInputDeviceState(ILjava/lang/String;)V
    .registers 7
    .parameter "deviceId"
    .parameter "state"

    #@0
    .prologue
    .line 761
    iget-object v1, p0, Lcom/android/internal/widget/PointerLocationView;->mIm:Landroid/hardware/input/InputManager;

    #@2
    invoke-virtual {v1, p1}, Landroid/hardware/input/InputManager;->getInputDevice(I)Landroid/view/InputDevice;

    #@5
    move-result-object v0

    #@6
    .line 762
    .local v0, device:Landroid/view/InputDevice;
    if-eqz v0, :cond_25

    #@8
    .line 763
    const-string v1, "Pointer"

    #@a
    new-instance v2, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    const-string v3, ": "

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 767
    :goto_24
    return-void

    #@25
    .line 765
    :cond_25
    const-string v1, "Pointer"

    #@27
    new-instance v2, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v2

    #@30
    const-string v3, ": "

    #@32
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v2

    #@36
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@39
    move-result-object v2

    #@3a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d
    move-result-object v2

    #@3e
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@41
    goto :goto_24
.end method

.method private logInputDevices()V
    .registers 5

    #@0
    .prologue
    .line 754
    invoke-static {}, Landroid/view/InputDevice;->getDeviceIds()[I

    #@3
    move-result-object v0

    #@4
    .line 755
    .local v0, deviceIds:[I
    const/4 v1, 0x0

    #@5
    .local v1, i:I
    :goto_5
    array-length v2, v0

    #@6
    if-ge v1, v2, :cond_12

    #@8
    .line 756
    aget v2, v0, v1

    #@a
    const-string v3, "Device Enumerated"

    #@c
    invoke-direct {p0, v2, v3}, Lcom/android/internal/widget/PointerLocationView;->logInputDeviceState(ILjava/lang/String;)V

    #@f
    .line 755
    add-int/lit8 v1, v1, 0x1

    #@11
    goto :goto_5

    #@12
    .line 758
    :cond_12
    return-void
.end method

.method private logMotionEvent(Ljava/lang/String;Landroid/view/MotionEvent;)V
    .registers 14
    .parameter "type"
    .parameter "event"

    #@0
    .prologue
    .line 436
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    #@3
    move-result v2

    #@4
    .line 437
    .local v2, action:I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getHistorySize()I

    #@7
    move-result v8

    #@8
    .line 438
    .local v8, N:I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getPointerCount()I

    #@b
    move-result v9

    #@c
    .line 439
    .local v9, NI:I
    const/4 v10, 0x0

    #@d
    .local v10, historyPos:I
    :goto_d
    if-ge v10, v8, :cond_30

    #@f
    .line 440
    const/4 v3, 0x0

    #@10
    .local v3, i:I
    :goto_10
    if-ge v3, v9, :cond_2d

    #@12
    .line 441
    invoke-virtual {p2, v3}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@15
    move-result v5

    #@16
    .line 442
    .local v5, id:I
    iget-object v0, p0, Lcom/android/internal/widget/PointerLocationView;->mTempCoords:Landroid/view/MotionEvent$PointerCoords;

    #@18
    invoke-virtual {p2, v3, v10, v0}, Landroid/view/MotionEvent;->getHistoricalPointerCoords(IILandroid/view/MotionEvent$PointerCoords;)V

    #@1b
    .line 443
    iget-object v4, p0, Lcom/android/internal/widget/PointerLocationView;->mTempCoords:Landroid/view/MotionEvent$PointerCoords;

    #@1d
    invoke-virtual {p2, v3}, Landroid/view/MotionEvent;->getToolType(I)I

    #@20
    move-result v6

    #@21
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getButtonState()I

    #@24
    move-result v7

    #@25
    move-object v0, p0

    #@26
    move-object v1, p1

    #@27
    invoke-direct/range {v0 .. v7}, Lcom/android/internal/widget/PointerLocationView;->logCoords(Ljava/lang/String;IILandroid/view/MotionEvent$PointerCoords;III)V

    #@2a
    .line 440
    add-int/lit8 v3, v3, 0x1

    #@2c
    goto :goto_10

    #@2d
    .line 439
    .end local v5           #id:I
    :cond_2d
    add-int/lit8 v10, v10, 0x1

    #@2f
    goto :goto_d

    #@30
    .line 447
    .end local v3           #i:I
    :cond_30
    const/4 v3, 0x0

    #@31
    .restart local v3       #i:I
    :goto_31
    if-ge v3, v9, :cond_4e

    #@33
    .line 448
    invoke-virtual {p2, v3}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@36
    move-result v5

    #@37
    .line 449
    .restart local v5       #id:I
    iget-object v0, p0, Lcom/android/internal/widget/PointerLocationView;->mTempCoords:Landroid/view/MotionEvent$PointerCoords;

    #@39
    invoke-virtual {p2, v3, v0}, Landroid/view/MotionEvent;->getPointerCoords(ILandroid/view/MotionEvent$PointerCoords;)V

    #@3c
    .line 450
    iget-object v4, p0, Lcom/android/internal/widget/PointerLocationView;->mTempCoords:Landroid/view/MotionEvent$PointerCoords;

    #@3e
    invoke-virtual {p2, v3}, Landroid/view/MotionEvent;->getToolType(I)I

    #@41
    move-result v6

    #@42
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getButtonState()I

    #@45
    move-result v7

    #@46
    move-object v0, p0

    #@47
    move-object v1, p1

    #@48
    invoke-direct/range {v0 .. v7}, Lcom/android/internal/widget/PointerLocationView;->logCoords(Ljava/lang/String;IILandroid/view/MotionEvent$PointerCoords;III)V

    #@4b
    .line 447
    add-int/lit8 v3, v3, 0x1

    #@4d
    goto :goto_31

    #@4e
    .line 453
    .end local v5           #id:I
    :cond_4e
    return-void
.end method

.method private static shouldLogKey(I)Z
    .registers 3
    .parameter "keyCode"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 704
    packed-switch p0, :pswitch_data_14

    #@4
    .line 712
    invoke-static {p0}, Landroid/view/KeyEvent;->isGamepadButton(I)Z

    #@7
    move-result v1

    #@8
    if-nez v1, :cond_10

    #@a
    invoke-static {p0}, Landroid/view/KeyEvent;->isModifierKey(I)Z

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_11

    #@10
    :cond_10
    :goto_10
    :pswitch_10
    return v0

    #@11
    :cond_11
    const/4 v0, 0x0

    #@12
    goto :goto_10

    #@13
    .line 704
    nop

    #@14
    :pswitch_data_14
    .packed-switch 0x13
        :pswitch_10
        :pswitch_10
        :pswitch_10
        :pswitch_10
        :pswitch_10
    .end packed-switch
.end method


# virtual methods
.method public addPointerEvent(Landroid/view/MotionEvent;)V
    .registers 18
    .parameter "event"

    #@0
    .prologue
    .line 532
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    #@3
    move-result v3

    #@4
    .line 533
    .local v3, action:I
    move-object/from16 v0, p0

    #@6
    iget-object v1, v0, Lcom/android/internal/widget/PointerLocationView;->mPointers:Ljava/util/ArrayList;

    #@8
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@b
    move-result v11

    #@c
    .line 535
    .local v11, NP:I
    if-eqz v3, :cond_13

    #@e
    and-int/lit16 v1, v3, 0xff

    #@10
    const/4 v2, 0x5

    #@11
    if-ne v1, v2, :cond_b4

    #@13
    .line 537
    :cond_13
    const v1, 0xff00

    #@16
    and-int/2addr v1, v3

    #@17
    shr-int/lit8 v13, v1, 0x8

    #@19
    .line 539
    .local v13, index:I
    if-nez v3, :cond_55

    #@1b
    .line 540
    const/4 v14, 0x0

    #@1c
    .local v14, p:I
    :goto_1c
    if-ge v14, v11, :cond_32

    #@1e
    .line 541
    move-object/from16 v0, p0

    #@20
    iget-object v1, v0, Lcom/android/internal/widget/PointerLocationView;->mPointers:Ljava/util/ArrayList;

    #@22
    invoke-virtual {v1, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@25
    move-result-object v15

    #@26
    check-cast v15, Lcom/android/internal/widget/PointerLocationView$PointerState;

    #@28
    .line 542
    .local v15, ps:Lcom/android/internal/widget/PointerLocationView$PointerState;
    invoke-virtual {v15}, Lcom/android/internal/widget/PointerLocationView$PointerState;->clearTrace()V

    #@2b
    .line 543
    const/4 v1, 0x0

    #@2c
    invoke-static {v15, v1}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$102(Lcom/android/internal/widget/PointerLocationView$PointerState;Z)Z

    #@2f
    .line 540
    add-int/lit8 v14, v14, 0x1

    #@31
    goto :goto_1c

    #@32
    .line 545
    .end local v15           #ps:Lcom/android/internal/widget/PointerLocationView$PointerState;
    :cond_32
    const/4 v1, 0x1

    #@33
    move-object/from16 v0, p0

    #@35
    iput-boolean v1, v0, Lcom/android/internal/widget/PointerLocationView;->mCurDown:Z

    #@37
    .line 546
    const/4 v1, 0x0

    #@38
    move-object/from16 v0, p0

    #@3a
    iput v1, v0, Lcom/android/internal/widget/PointerLocationView;->mCurNumPointers:I

    #@3c
    .line 547
    const/4 v1, 0x0

    #@3d
    move-object/from16 v0, p0

    #@3f
    iput v1, v0, Lcom/android/internal/widget/PointerLocationView;->mMaxNumPointers:I

    #@41
    .line 548
    move-object/from16 v0, p0

    #@43
    iget-object v1, v0, Lcom/android/internal/widget/PointerLocationView;->mVelocity:Landroid/view/VelocityTracker;

    #@45
    invoke-virtual {v1}, Landroid/view/VelocityTracker;->clear()V

    #@48
    .line 549
    move-object/from16 v0, p0

    #@4a
    iget-object v1, v0, Lcom/android/internal/widget/PointerLocationView;->mAltVelocity:Landroid/view/VelocityTracker;

    #@4c
    if-eqz v1, :cond_55

    #@4e
    .line 550
    move-object/from16 v0, p0

    #@50
    iget-object v1, v0, Lcom/android/internal/widget/PointerLocationView;->mAltVelocity:Landroid/view/VelocityTracker;

    #@52
    invoke-virtual {v1}, Landroid/view/VelocityTracker;->clear()V

    #@55
    .line 554
    .end local v14           #p:I
    :cond_55
    move-object/from16 v0, p0

    #@57
    iget v1, v0, Lcom/android/internal/widget/PointerLocationView;->mCurNumPointers:I

    #@59
    add-int/lit8 v1, v1, 0x1

    #@5b
    move-object/from16 v0, p0

    #@5d
    iput v1, v0, Lcom/android/internal/widget/PointerLocationView;->mCurNumPointers:I

    #@5f
    .line 555
    move-object/from16 v0, p0

    #@61
    iget v1, v0, Lcom/android/internal/widget/PointerLocationView;->mMaxNumPointers:I

    #@63
    move-object/from16 v0, p0

    #@65
    iget v2, v0, Lcom/android/internal/widget/PointerLocationView;->mCurNumPointers:I

    #@67
    if-ge v1, v2, :cond_71

    #@69
    .line 556
    move-object/from16 v0, p0

    #@6b
    iget v1, v0, Lcom/android/internal/widget/PointerLocationView;->mCurNumPointers:I

    #@6d
    move-object/from16 v0, p0

    #@6f
    iput v1, v0, Lcom/android/internal/widget/PointerLocationView;->mMaxNumPointers:I

    #@71
    .line 559
    :cond_71
    move-object/from16 v0, p1

    #@73
    invoke-virtual {v0, v13}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@76
    move-result v6

    #@77
    .line 560
    .local v6, id:I
    :goto_77
    if-gt v11, v6, :cond_88

    #@79
    .line 561
    new-instance v15, Lcom/android/internal/widget/PointerLocationView$PointerState;

    #@7b
    invoke-direct {v15}, Lcom/android/internal/widget/PointerLocationView$PointerState;-><init>()V

    #@7e
    .line 562
    .restart local v15       #ps:Lcom/android/internal/widget/PointerLocationView$PointerState;
    move-object/from16 v0, p0

    #@80
    iget-object v1, v0, Lcom/android/internal/widget/PointerLocationView;->mPointers:Ljava/util/ArrayList;

    #@82
    invoke-virtual {v1, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@85
    .line 563
    add-int/lit8 v11, v11, 0x1

    #@87
    .line 564
    goto :goto_77

    #@88
    .line 566
    .end local v15           #ps:Lcom/android/internal/widget/PointerLocationView$PointerState;
    :cond_88
    move-object/from16 v0, p0

    #@8a
    iget v1, v0, Lcom/android/internal/widget/PointerLocationView;->mActivePointerId:I

    #@8c
    if-ltz v1, :cond_a2

    #@8e
    move-object/from16 v0, p0

    #@90
    iget-object v1, v0, Lcom/android/internal/widget/PointerLocationView;->mPointers:Ljava/util/ArrayList;

    #@92
    move-object/from16 v0, p0

    #@94
    iget v2, v0, Lcom/android/internal/widget/PointerLocationView;->mActivePointerId:I

    #@96
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@99
    move-result-object v1

    #@9a
    check-cast v1, Lcom/android/internal/widget/PointerLocationView$PointerState;

    #@9c
    invoke-static {v1}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$100(Lcom/android/internal/widget/PointerLocationView$PointerState;)Z

    #@9f
    move-result v1

    #@a0
    if-nez v1, :cond_a6

    #@a2
    .line 568
    :cond_a2
    move-object/from16 v0, p0

    #@a4
    iput v6, v0, Lcom/android/internal/widget/PointerLocationView;->mActivePointerId:I

    #@a6
    .line 571
    :cond_a6
    move-object/from16 v0, p0

    #@a8
    iget-object v1, v0, Lcom/android/internal/widget/PointerLocationView;->mPointers:Ljava/util/ArrayList;

    #@aa
    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@ad
    move-result-object v15

    #@ae
    check-cast v15, Lcom/android/internal/widget/PointerLocationView$PointerState;

    #@b0
    .line 572
    .restart local v15       #ps:Lcom/android/internal/widget/PointerLocationView$PointerState;
    const/4 v1, 0x1

    #@b1
    invoke-static {v15, v1}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$102(Lcom/android/internal/widget/PointerLocationView$PointerState;Z)Z

    #@b4
    .line 575
    .end local v6           #id:I
    .end local v13           #index:I
    .end local v15           #ps:Lcom/android/internal/widget/PointerLocationView$PointerState;
    :cond_b4
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    #@b7
    move-result v10

    #@b8
    .line 577
    .local v10, NI:I
    move-object/from16 v0, p0

    #@ba
    iget-object v1, v0, Lcom/android/internal/widget/PointerLocationView;->mVelocity:Landroid/view/VelocityTracker;

    #@bc
    move-object/from16 v0, p1

    #@be
    invoke-virtual {v1, v0}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    #@c1
    .line 578
    move-object/from16 v0, p0

    #@c3
    iget-object v1, v0, Lcom/android/internal/widget/PointerLocationView;->mVelocity:Landroid/view/VelocityTracker;

    #@c5
    const/4 v2, 0x1

    #@c6
    invoke-virtual {v1, v2}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    #@c9
    .line 579
    move-object/from16 v0, p0

    #@cb
    iget-object v1, v0, Lcom/android/internal/widget/PointerLocationView;->mAltVelocity:Landroid/view/VelocityTracker;

    #@cd
    if-eqz v1, :cond_e0

    #@cf
    .line 580
    move-object/from16 v0, p0

    #@d1
    iget-object v1, v0, Lcom/android/internal/widget/PointerLocationView;->mAltVelocity:Landroid/view/VelocityTracker;

    #@d3
    move-object/from16 v0, p1

    #@d5
    invoke-virtual {v1, v0}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    #@d8
    .line 581
    move-object/from16 v0, p0

    #@da
    iget-object v1, v0, Lcom/android/internal/widget/PointerLocationView;->mAltVelocity:Landroid/view/VelocityTracker;

    #@dc
    const/4 v2, 0x1

    #@dd
    invoke-virtual {v1, v2}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    #@e0
    .line 584
    :cond_e0
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getHistorySize()I

    #@e3
    move-result v9

    #@e4
    .line 585
    .local v9, N:I
    const/4 v12, 0x0

    #@e5
    .local v12, historyPos:I
    :goto_e5
    if-ge v12, v9, :cond_13f

    #@e7
    .line 586
    const/4 v4, 0x0

    #@e8
    .local v4, i:I
    :goto_e8
    if-ge v4, v10, :cond_13c

    #@ea
    .line 587
    move-object/from16 v0, p1

    #@ec
    invoke-virtual {v0, v4}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@ef
    move-result v6

    #@f0
    .line 588
    .restart local v6       #id:I
    move-object/from16 v0, p0

    #@f2
    iget-boolean v1, v0, Lcom/android/internal/widget/PointerLocationView;->mCurDown:Z

    #@f4
    if-eqz v1, :cond_135

    #@f6
    move-object/from16 v0, p0

    #@f8
    iget-object v1, v0, Lcom/android/internal/widget/PointerLocationView;->mPointers:Ljava/util/ArrayList;

    #@fa
    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@fd
    move-result-object v1

    #@fe
    check-cast v1, Lcom/android/internal/widget/PointerLocationView$PointerState;

    #@100
    move-object v15, v1

    #@101
    .line 589
    .restart local v15       #ps:Lcom/android/internal/widget/PointerLocationView$PointerState;
    :goto_101
    if-eqz v15, :cond_137

    #@103
    invoke-static {v15}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$200(Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/MotionEvent$PointerCoords;

    #@106
    move-result-object v5

    #@107
    .line 590
    .local v5, coords:Landroid/view/MotionEvent$PointerCoords;
    :goto_107
    move-object/from16 v0, p1

    #@109
    invoke-virtual {v0, v4, v12, v5}, Landroid/view/MotionEvent;->getHistoricalPointerCoords(IILandroid/view/MotionEvent$PointerCoords;)V

    #@10c
    .line 591
    move-object/from16 v0, p0

    #@10e
    iget-boolean v1, v0, Lcom/android/internal/widget/PointerLocationView;->mPrintCoords:Z

    #@110
    if-eqz v1, :cond_123

    #@112
    .line 592
    const-string v2, "Pointer"

    #@114
    move-object/from16 v0, p1

    #@116
    invoke-virtual {v0, v4}, Landroid/view/MotionEvent;->getToolType(I)I

    #@119
    move-result v7

    #@11a
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getButtonState()I

    #@11d
    move-result v8

    #@11e
    move-object/from16 v1, p0

    #@120
    invoke-direct/range {v1 .. v8}, Lcom/android/internal/widget/PointerLocationView;->logCoords(Ljava/lang/String;IILandroid/view/MotionEvent$PointerCoords;III)V

    #@123
    .line 595
    :cond_123
    if-eqz v15, :cond_132

    #@125
    .line 598
    iget v1, v5, Landroid/view/MotionEvent$PointerCoords;->x:F

    #@127
    iget v2, v5, Landroid/view/MotionEvent$PointerCoords;->y:F

    #@129
    move-object/from16 v0, p1

    #@12b
    invoke-virtual {v0, v4}, Landroid/view/MotionEvent;->getToolType(I)I

    #@12e
    move-result v7

    #@12f
    invoke-virtual {v15, v1, v2, v7}, Lcom/android/internal/widget/PointerLocationView$PointerState;->addTraceAndToolType(FFI)V

    #@132
    .line 586
    :cond_132
    add-int/lit8 v4, v4, 0x1

    #@134
    goto :goto_e8

    #@135
    .line 588
    .end local v5           #coords:Landroid/view/MotionEvent$PointerCoords;
    .end local v15           #ps:Lcom/android/internal/widget/PointerLocationView$PointerState;
    :cond_135
    const/4 v15, 0x0

    #@136
    goto :goto_101

    #@137
    .line 589
    .restart local v15       #ps:Lcom/android/internal/widget/PointerLocationView$PointerState;
    :cond_137
    move-object/from16 v0, p0

    #@139
    iget-object v5, v0, Lcom/android/internal/widget/PointerLocationView;->mTempCoords:Landroid/view/MotionEvent$PointerCoords;

    #@13b
    goto :goto_107

    #@13c
    .line 585
    .end local v6           #id:I
    .end local v15           #ps:Lcom/android/internal/widget/PointerLocationView$PointerState;
    :cond_13c
    add-int/lit8 v12, v12, 0x1

    #@13e
    goto :goto_e5

    #@13f
    .line 602
    .end local v4           #i:I
    :cond_13f
    const/4 v4, 0x0

    #@140
    .restart local v4       #i:I
    :goto_140
    if-ge v4, v10, :cond_1dd

    #@142
    .line 603
    move-object/from16 v0, p1

    #@144
    invoke-virtual {v0, v4}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@147
    move-result v6

    #@148
    .line 604
    .restart local v6       #id:I
    move-object/from16 v0, p0

    #@14a
    iget-boolean v1, v0, Lcom/android/internal/widget/PointerLocationView;->mCurDown:Z

    #@14c
    if-eqz v1, :cond_1d6

    #@14e
    move-object/from16 v0, p0

    #@150
    iget-object v1, v0, Lcom/android/internal/widget/PointerLocationView;->mPointers:Ljava/util/ArrayList;

    #@152
    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@155
    move-result-object v1

    #@156
    check-cast v1, Lcom/android/internal/widget/PointerLocationView$PointerState;

    #@158
    move-object v15, v1

    #@159
    .line 605
    .restart local v15       #ps:Lcom/android/internal/widget/PointerLocationView$PointerState;
    :goto_159
    if-eqz v15, :cond_1d8

    #@15b
    invoke-static {v15}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$200(Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/MotionEvent$PointerCoords;

    #@15e
    move-result-object v5

    #@15f
    .line 606
    .restart local v5       #coords:Landroid/view/MotionEvent$PointerCoords;
    :goto_15f
    move-object/from16 v0, p1

    #@161
    invoke-virtual {v0, v4, v5}, Landroid/view/MotionEvent;->getPointerCoords(ILandroid/view/MotionEvent$PointerCoords;)V

    #@164
    .line 607
    move-object/from16 v0, p0

    #@166
    iget-boolean v1, v0, Lcom/android/internal/widget/PointerLocationView;->mPrintCoords:Z

    #@168
    if-eqz v1, :cond_17b

    #@16a
    .line 608
    const-string v2, "Pointer"

    #@16c
    move-object/from16 v0, p1

    #@16e
    invoke-virtual {v0, v4}, Landroid/view/MotionEvent;->getToolType(I)I

    #@171
    move-result v7

    #@172
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getButtonState()I

    #@175
    move-result v8

    #@176
    move-object/from16 v1, p0

    #@178
    invoke-direct/range {v1 .. v8}, Lcom/android/internal/widget/PointerLocationView;->logCoords(Ljava/lang/String;IILandroid/view/MotionEvent$PointerCoords;III)V

    #@17b
    .line 611
    :cond_17b
    if-eqz v15, :cond_1d2

    #@17d
    .line 614
    iget v1, v5, Landroid/view/MotionEvent$PointerCoords;->x:F

    #@17f
    iget v2, v5, Landroid/view/MotionEvent$PointerCoords;->y:F

    #@181
    move-object/from16 v0, p1

    #@183
    invoke-virtual {v0, v4}, Landroid/view/MotionEvent;->getToolType(I)I

    #@186
    move-result v7

    #@187
    invoke-virtual {v15, v1, v2, v7}, Lcom/android/internal/widget/PointerLocationView$PointerState;->addTraceAndToolType(FFI)V

    #@18a
    .line 615
    move-object/from16 v0, p0

    #@18c
    iget-object v1, v0, Lcom/android/internal/widget/PointerLocationView;->mVelocity:Landroid/view/VelocityTracker;

    #@18e
    invoke-virtual {v1, v6}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    #@191
    move-result v1

    #@192
    invoke-static {v15, v1}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$502(Lcom/android/internal/widget/PointerLocationView$PointerState;F)F

    #@195
    .line 616
    move-object/from16 v0, p0

    #@197
    iget-object v1, v0, Lcom/android/internal/widget/PointerLocationView;->mVelocity:Landroid/view/VelocityTracker;

    #@199
    invoke-virtual {v1, v6}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    #@19c
    move-result v1

    #@19d
    invoke-static {v15, v1}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$602(Lcom/android/internal/widget/PointerLocationView$PointerState;F)F

    #@1a0
    .line 617
    move-object/from16 v0, p0

    #@1a2
    iget-object v1, v0, Lcom/android/internal/widget/PointerLocationView;->mVelocity:Landroid/view/VelocityTracker;

    #@1a4
    invoke-static {v15}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$800(Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/VelocityTracker$Estimator;

    #@1a7
    move-result-object v2

    #@1a8
    invoke-virtual {v1, v6, v2}, Landroid/view/VelocityTracker;->getEstimator(ILandroid/view/VelocityTracker$Estimator;)Z

    #@1ab
    .line 618
    move-object/from16 v0, p0

    #@1ad
    iget-object v1, v0, Lcom/android/internal/widget/PointerLocationView;->mAltVelocity:Landroid/view/VelocityTracker;

    #@1af
    if-eqz v1, :cond_1d2

    #@1b1
    .line 619
    move-object/from16 v0, p0

    #@1b3
    iget-object v1, v0, Lcom/android/internal/widget/PointerLocationView;->mAltVelocity:Landroid/view/VelocityTracker;

    #@1b5
    invoke-virtual {v1, v6}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    #@1b8
    move-result v1

    #@1b9
    invoke-static {v15, v1}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$1002(Lcom/android/internal/widget/PointerLocationView$PointerState;F)F

    #@1bc
    .line 620
    move-object/from16 v0, p0

    #@1be
    iget-object v1, v0, Lcom/android/internal/widget/PointerLocationView;->mAltVelocity:Landroid/view/VelocityTracker;

    #@1c0
    invoke-virtual {v1, v6}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    #@1c3
    move-result v1

    #@1c4
    invoke-static {v15, v1}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$1102(Lcom/android/internal/widget/PointerLocationView$PointerState;F)F

    #@1c7
    .line 621
    move-object/from16 v0, p0

    #@1c9
    iget-object v1, v0, Lcom/android/internal/widget/PointerLocationView;->mAltVelocity:Landroid/view/VelocityTracker;

    #@1cb
    invoke-static {v15}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$900(Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/VelocityTracker$Estimator;

    #@1ce
    move-result-object v2

    #@1cf
    invoke-virtual {v1, v6, v2}, Landroid/view/VelocityTracker;->getEstimator(ILandroid/view/VelocityTracker$Estimator;)Z

    #@1d2
    .line 602
    :cond_1d2
    add-int/lit8 v4, v4, 0x1

    #@1d4
    goto/16 :goto_140

    #@1d6
    .line 604
    .end local v5           #coords:Landroid/view/MotionEvent$PointerCoords;
    .end local v15           #ps:Lcom/android/internal/widget/PointerLocationView$PointerState;
    :cond_1d6
    const/4 v15, 0x0

    #@1d7
    goto :goto_159

    #@1d8
    .line 605
    .restart local v15       #ps:Lcom/android/internal/widget/PointerLocationView$PointerState;
    :cond_1d8
    move-object/from16 v0, p0

    #@1da
    iget-object v5, v0, Lcom/android/internal/widget/PointerLocationView;->mTempCoords:Landroid/view/MotionEvent$PointerCoords;

    #@1dc
    goto :goto_15f

    #@1dd
    .line 627
    .end local v6           #id:I
    .end local v15           #ps:Lcom/android/internal/widget/PointerLocationView$PointerState;
    :cond_1dd
    const/4 v1, 0x1

    #@1de
    if-eq v3, v1, :cond_1e8

    #@1e0
    const/4 v1, 0x3

    #@1e1
    if-eq v3, v1, :cond_1e8

    #@1e3
    and-int/lit16 v1, v3, 0xff

    #@1e5
    const/4 v2, 0x6

    #@1e6
    if-ne v1, v2, :cond_212

    #@1e8
    .line 630
    :cond_1e8
    const v1, 0xff00

    #@1eb
    and-int/2addr v1, v3

    #@1ec
    shr-int/lit8 v13, v1, 0x8

    #@1ee
    .line 633
    .restart local v13       #index:I
    move-object/from16 v0, p1

    #@1f0
    invoke-virtual {v0, v13}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@1f3
    move-result v6

    #@1f4
    .line 634
    .restart local v6       #id:I
    move-object/from16 v0, p0

    #@1f6
    iget-object v1, v0, Lcom/android/internal/widget/PointerLocationView;->mPointers:Ljava/util/ArrayList;

    #@1f8
    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1fb
    move-result-object v15

    #@1fc
    check-cast v15, Lcom/android/internal/widget/PointerLocationView$PointerState;

    #@1fe
    .line 635
    .restart local v15       #ps:Lcom/android/internal/widget/PointerLocationView$PointerState;
    const/4 v1, 0x0

    #@1ff
    invoke-static {v15, v1}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$102(Lcom/android/internal/widget/PointerLocationView$PointerState;Z)Z

    #@202
    .line 637
    const/4 v1, 0x1

    #@203
    if-eq v3, v1, :cond_208

    #@205
    const/4 v1, 0x3

    #@206
    if-ne v3, v1, :cond_216

    #@208
    .line 639
    :cond_208
    const/4 v1, 0x0

    #@209
    move-object/from16 v0, p0

    #@20b
    iput-boolean v1, v0, Lcom/android/internal/widget/PointerLocationView;->mCurDown:Z

    #@20d
    .line 640
    const/4 v1, 0x0

    #@20e
    move-object/from16 v0, p0

    #@210
    iput v1, v0, Lcom/android/internal/widget/PointerLocationView;->mCurNumPointers:I

    #@212
    .line 652
    .end local v6           #id:I
    .end local v13           #index:I
    .end local v15           #ps:Lcom/android/internal/widget/PointerLocationView$PointerState;
    :cond_212
    :goto_212
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/widget/PointerLocationView;->invalidate()V

    #@215
    .line 653
    return-void

    #@216
    .line 642
    .restart local v6       #id:I
    .restart local v13       #index:I
    .restart local v15       #ps:Lcom/android/internal/widget/PointerLocationView$PointerState;
    :cond_216
    move-object/from16 v0, p0

    #@218
    iget v1, v0, Lcom/android/internal/widget/PointerLocationView;->mCurNumPointers:I

    #@21a
    add-int/lit8 v1, v1, -0x1

    #@21c
    move-object/from16 v0, p0

    #@21e
    iput v1, v0, Lcom/android/internal/widget/PointerLocationView;->mCurNumPointers:I

    #@220
    .line 643
    move-object/from16 v0, p0

    #@222
    iget v1, v0, Lcom/android/internal/widget/PointerLocationView;->mActivePointerId:I

    #@224
    if-ne v1, v6, :cond_233

    #@226
    .line 644
    if-nez v13, :cond_23c

    #@228
    const/4 v1, 0x1

    #@229
    :goto_229
    move-object/from16 v0, p1

    #@22b
    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@22e
    move-result v1

    #@22f
    move-object/from16 v0, p0

    #@231
    iput v1, v0, Lcom/android/internal/widget/PointerLocationView;->mActivePointerId:I

    #@233
    .line 648
    :cond_233
    const/high16 v1, 0x7fc0

    #@235
    const/high16 v2, 0x7fc0

    #@237
    const/4 v7, -0x1

    #@238
    invoke-virtual {v15, v1, v2, v7}, Lcom/android/internal/widget/PointerLocationView$PointerState;->addTraceAndToolType(FFI)V

    #@23b
    goto :goto_212

    #@23c
    .line 644
    :cond_23c
    const/4 v1, 0x0

    #@23d
    goto :goto_229
.end method

.method protected onAttachedToWindow()V
    .registers 3

    #@0
    .prologue
    .line 725
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    #@3
    .line 727
    iget-object v0, p0, Lcom/android/internal/widget/PointerLocationView;->mIm:Landroid/hardware/input/InputManager;

    #@5
    invoke-virtual {p0}, Lcom/android/internal/widget/PointerLocationView;->getHandler()Landroid/os/Handler;

    #@8
    move-result-object v1

    #@9
    invoke-virtual {v0, p0, v1}, Landroid/hardware/input/InputManager;->registerInputDeviceListener(Landroid/hardware/input/InputManager$InputDeviceListener;Landroid/os/Handler;)V

    #@c
    .line 728
    invoke-direct {p0}, Lcom/android/internal/widget/PointerLocationView;->logInputDevices()V

    #@f
    .line 729
    return-void
.end method

.method protected onDetachedFromWindow()V
    .registers 2

    #@0
    .prologue
    .line 733
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    #@3
    .line 735
    iget-object v0, p0, Lcom/android/internal/widget/PointerLocationView;->mIm:Landroid/hardware/input/InputManager;

    #@5
    invoke-virtual {v0, p0}, Landroid/hardware/input/InputManager;->unregisterInputDeviceListener(Landroid/hardware/input/InputManager$InputDeviceListener;)V

    #@8
    .line 736
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 40
    .parameter "canvas"

    #@0
    .prologue
    .line 229
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/widget/PointerLocationView;->getWidth()I

    #@3
    move-result v35

    #@4
    .line 230
    .local v35, w:I
    div-int/lit8 v28, v35, 0x7

    #@6
    .line 231
    .local v28, itemW:I
    move-object/from16 v0, p0

    #@8
    iget-object v2, v0, Lcom/android/internal/widget/PointerLocationView;->mTextMetrics:Landroid/graphics/Paint$FontMetricsInt;

    #@a
    iget v2, v2, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    #@c
    neg-int v2, v2

    #@d
    add-int/lit8 v21, v2, 0x1

    #@f
    .line 232
    .local v21, base:I
    move-object/from16 v0, p0

    #@11
    iget v0, v0, Lcom/android/internal/widget/PointerLocationView;->mHeaderBottom:I

    #@13
    move/from16 v22, v0

    #@15
    .line 234
    .local v22, bottom:I
    move-object/from16 v0, p0

    #@17
    iget-object v2, v0, Lcom/android/internal/widget/PointerLocationView;->mPointers:Ljava/util/ArrayList;

    #@19
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@1c
    move-result v19

    #@1d
    .line 237
    .local v19, NP:I
    move-object/from16 v0, p0

    #@1f
    iget v2, v0, Lcom/android/internal/widget/PointerLocationView;->mActivePointerId:I

    #@21
    if-ltz v2, :cond_258

    #@23
    .line 238
    move-object/from16 v0, p0

    #@25
    iget-object v2, v0, Lcom/android/internal/widget/PointerLocationView;->mPointers:Ljava/util/ArrayList;

    #@27
    move-object/from16 v0, p0

    #@29
    iget v7, v0, Lcom/android/internal/widget/PointerLocationView;->mActivePointerId:I

    #@2b
    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@2e
    move-result-object v33

    #@2f
    check-cast v33, Lcom/android/internal/widget/PointerLocationView$PointerState;

    #@31
    .line 240
    .local v33, ps:Lcom/android/internal/widget/PointerLocationView$PointerState;
    const/4 v3, 0x0

    #@32
    const/4 v4, 0x0

    #@33
    add-int/lit8 v2, v28, -0x1

    #@35
    int-to-float v5, v2

    #@36
    move/from16 v0, v22

    #@38
    int-to-float v6, v0

    #@39
    move-object/from16 v0, p0

    #@3b
    iget-object v7, v0, Lcom/android/internal/widget/PointerLocationView;->mTextBackgroundPaint:Landroid/graphics/Paint;

    #@3d
    move-object/from16 v2, p1

    #@3f
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    #@42
    .line 241
    move-object/from16 v0, p0

    #@44
    iget-object v2, v0, Lcom/android/internal/widget/PointerLocationView;->mText:Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@46
    invoke-virtual {v2}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->clear()Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@49
    move-result-object v2

    #@4a
    const-string v7, "P: "

    #@4c
    invoke-virtual {v2, v7}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(Ljava/lang/String;)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@4f
    move-result-object v2

    #@50
    move-object/from16 v0, p0

    #@52
    iget v7, v0, Lcom/android/internal/widget/PointerLocationView;->mCurNumPointers:I

    #@54
    invoke-virtual {v2, v7}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(I)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@57
    move-result-object v2

    #@58
    const-string v7, " / "

    #@5a
    invoke-virtual {v2, v7}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(Ljava/lang/String;)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@5d
    move-result-object v2

    #@5e
    move-object/from16 v0, p0

    #@60
    iget v7, v0, Lcom/android/internal/widget/PointerLocationView;->mMaxNumPointers:I

    #@62
    invoke-virtual {v2, v7}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(I)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@65
    move-result-object v2

    #@66
    invoke-virtual {v2}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->toString()Ljava/lang/String;

    #@69
    move-result-object v2

    #@6a
    const/high16 v7, 0x3f80

    #@6c
    move/from16 v0, v21

    #@6e
    int-to-float v10, v0

    #@6f
    move-object/from16 v0, p0

    #@71
    iget-object v11, v0, Lcom/android/internal/widget/PointerLocationView;->mTextPaint:Landroid/graphics/Paint;

    #@73
    move-object/from16 v0, p1

    #@75
    invoke-virtual {v0, v2, v7, v10, v11}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    #@78
    .line 246
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$000(Lcom/android/internal/widget/PointerLocationView$PointerState;)I

    #@7b
    move-result v18

    #@7c
    .line 247
    .local v18, N:I
    move-object/from16 v0, p0

    #@7e
    iget-boolean v2, v0, Lcom/android/internal/widget/PointerLocationView;->mCurDown:Z

    #@80
    if-eqz v2, :cond_88

    #@82
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$100(Lcom/android/internal/widget/PointerLocationView$PointerState;)Z

    #@85
    move-result v2

    #@86
    if-nez v2, :cond_8a

    #@88
    :cond_88
    if-nez v18, :cond_2a4

    #@8a
    .line 248
    :cond_8a
    move/from16 v0, v28

    #@8c
    int-to-float v3, v0

    #@8d
    const/4 v4, 0x0

    #@8e
    mul-int/lit8 v2, v28, 0x2

    #@90
    add-int/lit8 v2, v2, -0x1

    #@92
    int-to-float v5, v2

    #@93
    move/from16 v0, v22

    #@95
    int-to-float v6, v0

    #@96
    move-object/from16 v0, p0

    #@98
    iget-object v7, v0, Lcom/android/internal/widget/PointerLocationView;->mTextBackgroundPaint:Landroid/graphics/Paint;

    #@9a
    move-object/from16 v2, p1

    #@9c
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    #@9f
    .line 249
    move-object/from16 v0, p0

    #@a1
    iget-object v2, v0, Lcom/android/internal/widget/PointerLocationView;->mText:Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@a3
    invoke-virtual {v2}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->clear()Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@a6
    move-result-object v2

    #@a7
    const-string v7, "X: "

    #@a9
    invoke-virtual {v2, v7}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(Ljava/lang/String;)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@ac
    move-result-object v2

    #@ad
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$200(Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/MotionEvent$PointerCoords;

    #@b0
    move-result-object v7

    #@b1
    iget v7, v7, Landroid/view/MotionEvent$PointerCoords;->x:F

    #@b3
    const/4 v10, 0x1

    #@b4
    invoke-virtual {v2, v7, v10}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(FI)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@b7
    move-result-object v2

    #@b8
    invoke-virtual {v2}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->toString()Ljava/lang/String;

    #@bb
    move-result-object v2

    #@bc
    add-int/lit8 v7, v28, 0x1

    #@be
    int-to-float v7, v7

    #@bf
    move/from16 v0, v21

    #@c1
    int-to-float v10, v0

    #@c2
    move-object/from16 v0, p0

    #@c4
    iget-object v11, v0, Lcom/android/internal/widget/PointerLocationView;->mTextPaint:Landroid/graphics/Paint;

    #@c6
    move-object/from16 v0, p1

    #@c8
    invoke-virtual {v0, v2, v7, v10, v11}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    #@cb
    .line 252
    mul-int/lit8 v2, v28, 0x2

    #@cd
    int-to-float v3, v2

    #@ce
    const/4 v4, 0x0

    #@cf
    mul-int/lit8 v2, v28, 0x3

    #@d1
    add-int/lit8 v2, v2, -0x1

    #@d3
    int-to-float v5, v2

    #@d4
    move/from16 v0, v22

    #@d6
    int-to-float v6, v0

    #@d7
    move-object/from16 v0, p0

    #@d9
    iget-object v7, v0, Lcom/android/internal/widget/PointerLocationView;->mTextBackgroundPaint:Landroid/graphics/Paint;

    #@db
    move-object/from16 v2, p1

    #@dd
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    #@e0
    .line 253
    move-object/from16 v0, p0

    #@e2
    iget-object v2, v0, Lcom/android/internal/widget/PointerLocationView;->mText:Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@e4
    invoke-virtual {v2}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->clear()Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@e7
    move-result-object v2

    #@e8
    const-string v7, "Y: "

    #@ea
    invoke-virtual {v2, v7}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(Ljava/lang/String;)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@ed
    move-result-object v2

    #@ee
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$200(Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/MotionEvent$PointerCoords;

    #@f1
    move-result-object v7

    #@f2
    iget v7, v7, Landroid/view/MotionEvent$PointerCoords;->y:F

    #@f4
    const/4 v10, 0x1

    #@f5
    invoke-virtual {v2, v7, v10}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(FI)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@f8
    move-result-object v2

    #@f9
    invoke-virtual {v2}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->toString()Ljava/lang/String;

    #@fc
    move-result-object v2

    #@fd
    mul-int/lit8 v7, v28, 0x2

    #@ff
    add-int/lit8 v7, v7, 0x1

    #@101
    int-to-float v7, v7

    #@102
    move/from16 v0, v21

    #@104
    int-to-float v10, v0

    #@105
    move-object/from16 v0, p0

    #@107
    iget-object v11, v0, Lcom/android/internal/widget/PointerLocationView;->mTextPaint:Landroid/graphics/Paint;

    #@109
    move-object/from16 v0, p1

    #@10b
    invoke-virtual {v0, v2, v7, v10, v11}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    #@10e
    .line 273
    :goto_10e
    mul-int/lit8 v2, v28, 0x3

    #@110
    int-to-float v3, v2

    #@111
    const/4 v4, 0x0

    #@112
    mul-int/lit8 v2, v28, 0x4

    #@114
    add-int/lit8 v2, v2, -0x1

    #@116
    int-to-float v5, v2

    #@117
    move/from16 v0, v22

    #@119
    int-to-float v6, v0

    #@11a
    move-object/from16 v0, p0

    #@11c
    iget-object v7, v0, Lcom/android/internal/widget/PointerLocationView;->mTextBackgroundPaint:Landroid/graphics/Paint;

    #@11e
    move-object/from16 v2, p1

    #@120
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    #@123
    .line 274
    move-object/from16 v0, p0

    #@125
    iget-object v2, v0, Lcom/android/internal/widget/PointerLocationView;->mText:Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@127
    invoke-virtual {v2}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->clear()Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@12a
    move-result-object v2

    #@12b
    const-string v7, "Xv: "

    #@12d
    invoke-virtual {v2, v7}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(Ljava/lang/String;)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@130
    move-result-object v2

    #@131
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$500(Lcom/android/internal/widget/PointerLocationView$PointerState;)F

    #@134
    move-result v7

    #@135
    const/4 v10, 0x3

    #@136
    invoke-virtual {v2, v7, v10}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(FI)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@139
    move-result-object v2

    #@13a
    invoke-virtual {v2}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->toString()Ljava/lang/String;

    #@13d
    move-result-object v2

    #@13e
    mul-int/lit8 v7, v28, 0x3

    #@140
    add-int/lit8 v7, v7, 0x1

    #@142
    int-to-float v7, v7

    #@143
    move/from16 v0, v21

    #@145
    int-to-float v10, v0

    #@146
    move-object/from16 v0, p0

    #@148
    iget-object v11, v0, Lcom/android/internal/widget/PointerLocationView;->mTextPaint:Landroid/graphics/Paint;

    #@14a
    move-object/from16 v0, p1

    #@14c
    invoke-virtual {v0, v2, v7, v10, v11}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    #@14f
    .line 278
    mul-int/lit8 v2, v28, 0x4

    #@151
    int-to-float v3, v2

    #@152
    const/4 v4, 0x0

    #@153
    mul-int/lit8 v2, v28, 0x5

    #@155
    add-int/lit8 v2, v2, -0x1

    #@157
    int-to-float v5, v2

    #@158
    move/from16 v0, v22

    #@15a
    int-to-float v6, v0

    #@15b
    move-object/from16 v0, p0

    #@15d
    iget-object v7, v0, Lcom/android/internal/widget/PointerLocationView;->mTextBackgroundPaint:Landroid/graphics/Paint;

    #@15f
    move-object/from16 v2, p1

    #@161
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    #@164
    .line 279
    move-object/from16 v0, p0

    #@166
    iget-object v2, v0, Lcom/android/internal/widget/PointerLocationView;->mText:Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@168
    invoke-virtual {v2}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->clear()Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@16b
    move-result-object v2

    #@16c
    const-string v7, "Yv: "

    #@16e
    invoke-virtual {v2, v7}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(Ljava/lang/String;)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@171
    move-result-object v2

    #@172
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$600(Lcom/android/internal/widget/PointerLocationView$PointerState;)F

    #@175
    move-result v7

    #@176
    const/4 v10, 0x3

    #@177
    invoke-virtual {v2, v7, v10}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(FI)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@17a
    move-result-object v2

    #@17b
    invoke-virtual {v2}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->toString()Ljava/lang/String;

    #@17e
    move-result-object v2

    #@17f
    mul-int/lit8 v7, v28, 0x4

    #@181
    add-int/lit8 v7, v7, 0x1

    #@183
    int-to-float v7, v7

    #@184
    move/from16 v0, v21

    #@186
    int-to-float v10, v0

    #@187
    move-object/from16 v0, p0

    #@189
    iget-object v11, v0, Lcom/android/internal/widget/PointerLocationView;->mTextPaint:Landroid/graphics/Paint;

    #@18b
    move-object/from16 v0, p1

    #@18d
    invoke-virtual {v0, v2, v7, v10, v11}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    #@190
    .line 283
    mul-int/lit8 v2, v28, 0x5

    #@192
    int-to-float v3, v2

    #@193
    const/4 v4, 0x0

    #@194
    mul-int/lit8 v2, v28, 0x6

    #@196
    add-int/lit8 v2, v2, -0x1

    #@198
    int-to-float v5, v2

    #@199
    move/from16 v0, v22

    #@19b
    int-to-float v6, v0

    #@19c
    move-object/from16 v0, p0

    #@19e
    iget-object v7, v0, Lcom/android/internal/widget/PointerLocationView;->mTextBackgroundPaint:Landroid/graphics/Paint;

    #@1a0
    move-object/from16 v2, p1

    #@1a2
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    #@1a5
    .line 284
    mul-int/lit8 v2, v28, 0x5

    #@1a7
    int-to-float v3, v2

    #@1a8
    const/4 v4, 0x0

    #@1a9
    mul-int/lit8 v2, v28, 0x5

    #@1ab
    int-to-float v2, v2

    #@1ac
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$200(Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/MotionEvent$PointerCoords;

    #@1af
    move-result-object v7

    #@1b0
    iget v7, v7, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    #@1b2
    move/from16 v0, v28

    #@1b4
    int-to-float v10, v0

    #@1b5
    mul-float/2addr v7, v10

    #@1b6
    add-float/2addr v2, v7

    #@1b7
    const/high16 v7, 0x3f80

    #@1b9
    sub-float v5, v2, v7

    #@1bb
    move/from16 v0, v22

    #@1bd
    int-to-float v6, v0

    #@1be
    move-object/from16 v0, p0

    #@1c0
    iget-object v7, v0, Lcom/android/internal/widget/PointerLocationView;->mTextLevelPaint:Landroid/graphics/Paint;

    #@1c2
    move-object/from16 v2, p1

    #@1c4
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    #@1c7
    .line 286
    move-object/from16 v0, p0

    #@1c9
    iget-object v2, v0, Lcom/android/internal/widget/PointerLocationView;->mText:Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@1cb
    invoke-virtual {v2}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->clear()Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@1ce
    move-result-object v2

    #@1cf
    const-string v7, "Prs: "

    #@1d1
    invoke-virtual {v2, v7}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(Ljava/lang/String;)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@1d4
    move-result-object v2

    #@1d5
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$200(Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/MotionEvent$PointerCoords;

    #@1d8
    move-result-object v7

    #@1d9
    iget v7, v7, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    #@1db
    const/4 v10, 0x2

    #@1dc
    invoke-virtual {v2, v7, v10}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(FI)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@1df
    move-result-object v2

    #@1e0
    invoke-virtual {v2}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->toString()Ljava/lang/String;

    #@1e3
    move-result-object v2

    #@1e4
    mul-int/lit8 v7, v28, 0x5

    #@1e6
    add-int/lit8 v7, v7, 0x1

    #@1e8
    int-to-float v7, v7

    #@1e9
    move/from16 v0, v21

    #@1eb
    int-to-float v10, v0

    #@1ec
    move-object/from16 v0, p0

    #@1ee
    iget-object v11, v0, Lcom/android/internal/widget/PointerLocationView;->mTextPaint:Landroid/graphics/Paint;

    #@1f0
    move-object/from16 v0, p1

    #@1f2
    invoke-virtual {v0, v2, v7, v10, v11}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    #@1f5
    .line 290
    mul-int/lit8 v2, v28, 0x6

    #@1f7
    int-to-float v3, v2

    #@1f8
    const/4 v4, 0x0

    #@1f9
    move/from16 v0, v35

    #@1fb
    int-to-float v5, v0

    #@1fc
    move/from16 v0, v22

    #@1fe
    int-to-float v6, v0

    #@1ff
    move-object/from16 v0, p0

    #@201
    iget-object v7, v0, Lcom/android/internal/widget/PointerLocationView;->mTextBackgroundPaint:Landroid/graphics/Paint;

    #@203
    move-object/from16 v2, p1

    #@205
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    #@208
    .line 291
    mul-int/lit8 v2, v28, 0x6

    #@20a
    int-to-float v3, v2

    #@20b
    const/4 v4, 0x0

    #@20c
    mul-int/lit8 v2, v28, 0x6

    #@20e
    int-to-float v2, v2

    #@20f
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$200(Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/MotionEvent$PointerCoords;

    #@212
    move-result-object v7

    #@213
    iget v7, v7, Landroid/view/MotionEvent$PointerCoords;->size:F

    #@215
    move/from16 v0, v28

    #@217
    int-to-float v10, v0

    #@218
    mul-float/2addr v7, v10

    #@219
    add-float/2addr v2, v7

    #@21a
    const/high16 v7, 0x3f80

    #@21c
    sub-float v5, v2, v7

    #@21e
    move/from16 v0, v22

    #@220
    int-to-float v6, v0

    #@221
    move-object/from16 v0, p0

    #@223
    iget-object v7, v0, Lcom/android/internal/widget/PointerLocationView;->mTextLevelPaint:Landroid/graphics/Paint;

    #@225
    move-object/from16 v2, p1

    #@227
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    #@22a
    .line 293
    move-object/from16 v0, p0

    #@22c
    iget-object v2, v0, Lcom/android/internal/widget/PointerLocationView;->mText:Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@22e
    invoke-virtual {v2}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->clear()Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@231
    move-result-object v2

    #@232
    const-string v7, "Size: "

    #@234
    invoke-virtual {v2, v7}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(Ljava/lang/String;)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@237
    move-result-object v2

    #@238
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$200(Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/MotionEvent$PointerCoords;

    #@23b
    move-result-object v7

    #@23c
    iget v7, v7, Landroid/view/MotionEvent$PointerCoords;->size:F

    #@23e
    const/4 v10, 0x2

    #@23f
    invoke-virtual {v2, v7, v10}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(FI)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@242
    move-result-object v2

    #@243
    invoke-virtual {v2}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->toString()Ljava/lang/String;

    #@246
    move-result-object v2

    #@247
    mul-int/lit8 v7, v28, 0x6

    #@249
    add-int/lit8 v7, v7, 0x1

    #@24b
    int-to-float v7, v7

    #@24c
    move/from16 v0, v21

    #@24e
    int-to-float v10, v0

    #@24f
    move-object/from16 v0, p0

    #@251
    iget-object v11, v0, Lcom/android/internal/widget/PointerLocationView;->mTextPaint:Landroid/graphics/Paint;

    #@253
    move-object/from16 v0, p1

    #@255
    invoke-virtual {v0, v2, v7, v10, v11}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    #@258
    .line 299
    .end local v18           #N:I
    .end local v33           #ps:Lcom/android/internal/widget/PointerLocationView$PointerState;
    :cond_258
    const/16 v31, 0x0

    #@25a
    .local v31, p:I
    :goto_25a
    move/from16 v0, v31

    #@25c
    move/from16 v1, v19

    #@25e
    if-ge v0, v1, :cond_6a7

    #@260
    .line 300
    move-object/from16 v0, p0

    #@262
    iget-object v2, v0, Lcom/android/internal/widget/PointerLocationView;->mPointers:Ljava/util/ArrayList;

    #@264
    move/from16 v0, v31

    #@266
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@269
    move-result-object v33

    #@26a
    check-cast v33, Lcom/android/internal/widget/PointerLocationView$PointerState;

    #@26c
    .line 303
    .restart local v33       #ps:Lcom/android/internal/widget/PointerLocationView$PointerState;
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$000(Lcom/android/internal/widget/PointerLocationView$PointerState;)I

    #@26f
    move-result v18

    #@270
    .line 304
    .restart local v18       #N:I
    const/4 v3, 0x0

    #@271
    .local v3, lastX:F
    const/4 v4, 0x0

    #@272
    .line 305
    .local v4, lastY:F
    const/16 v26, 0x0

    #@274
    .line 306
    .local v26, haveLast:Z
    const/16 v23, 0x0

    #@276
    .line 307
    .local v23, drawn:Z
    move-object/from16 v0, p0

    #@278
    iget-object v2, v0, Lcom/android/internal/widget/PointerLocationView;->mPaint:Landroid/graphics/Paint;

    #@27a
    const/16 v7, 0xff

    #@27c
    const/16 v10, 0x80

    #@27e
    const/16 v11, 0xff

    #@280
    const/16 v12, 0xff

    #@282
    invoke-virtual {v2, v7, v10, v11, v12}, Landroid/graphics/Paint;->setARGB(IIII)V

    #@285
    .line 309
    const/16 v27, 0x0

    #@287
    .local v27, i:I
    :goto_287
    move/from16 v0, v27

    #@289
    move/from16 v1, v18

    #@28b
    if-ge v0, v1, :cond_3c5

    #@28d
    .line 310
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$300(Lcom/android/internal/widget/PointerLocationView$PointerState;)[F

    #@290
    move-result-object v2

    #@291
    aget v5, v2, v27

    #@293
    .line 311
    .local v5, x:F
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$400(Lcom/android/internal/widget/PointerLocationView$PointerState;)[F

    #@296
    move-result-object v2

    #@297
    aget v6, v2, v27

    #@299
    .line 312
    .local v6, y:F
    invoke-static {v5}, Ljava/lang/Float;->isNaN(F)Z

    #@29c
    move-result v2

    #@29d
    if-eqz v2, :cond_371

    #@29f
    .line 313
    const/16 v26, 0x0

    #@2a1
    .line 309
    :goto_2a1
    add-int/lit8 v27, v27, 0x1

    #@2a3
    goto :goto_287

    #@2a4
    .line 257
    .end local v3           #lastX:F
    .end local v4           #lastY:F
    .end local v5           #x:F
    .end local v6           #y:F
    .end local v23           #drawn:Z
    .end local v26           #haveLast:Z
    .end local v27           #i:I
    .end local v31           #p:I
    :cond_2a4
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$300(Lcom/android/internal/widget/PointerLocationView$PointerState;)[F

    #@2a7
    move-result-object v2

    #@2a8
    add-int/lit8 v7, v18, -0x1

    #@2aa
    aget v2, v2, v7

    #@2ac
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$300(Lcom/android/internal/widget/PointerLocationView$PointerState;)[F

    #@2af
    move-result-object v7

    #@2b0
    const/4 v10, 0x0

    #@2b1
    aget v7, v7, v10

    #@2b3
    sub-float v24, v2, v7

    #@2b5
    .line 258
    .local v24, dx:F
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$400(Lcom/android/internal/widget/PointerLocationView$PointerState;)[F

    #@2b8
    move-result-object v2

    #@2b9
    add-int/lit8 v7, v18, -0x1

    #@2bb
    aget v2, v2, v7

    #@2bd
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$400(Lcom/android/internal/widget/PointerLocationView$PointerState;)[F

    #@2c0
    move-result-object v7

    #@2c1
    const/4 v10, 0x0

    #@2c2
    aget v7, v7, v10

    #@2c4
    sub-float v25, v2, v7

    #@2c6
    .line 259
    .local v25, dy:F
    move/from16 v0, v28

    #@2c8
    int-to-float v3, v0

    #@2c9
    const/4 v4, 0x0

    #@2ca
    mul-int/lit8 v2, v28, 0x2

    #@2cc
    add-int/lit8 v2, v2, -0x1

    #@2ce
    int-to-float v5, v2

    #@2cf
    move/from16 v0, v22

    #@2d1
    int-to-float v6, v0

    #@2d2
    invoke-static/range {v24 .. v24}, Ljava/lang/Math;->abs(F)F

    #@2d5
    move-result v2

    #@2d6
    move-object/from16 v0, p0

    #@2d8
    iget-object v7, v0, Lcom/android/internal/widget/PointerLocationView;->mVC:Landroid/view/ViewConfiguration;

    #@2da
    invoke-virtual {v7}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    #@2dd
    move-result v7

    #@2de
    int-to-float v7, v7

    #@2df
    cmpg-float v2, v2, v7

    #@2e1
    if-gez v2, :cond_366

    #@2e3
    move-object/from16 v0, p0

    #@2e5
    iget-object v7, v0, Lcom/android/internal/widget/PointerLocationView;->mTextBackgroundPaint:Landroid/graphics/Paint;

    #@2e7
    :goto_2e7
    move-object/from16 v2, p1

    #@2e9
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    #@2ec
    .line 262
    move-object/from16 v0, p0

    #@2ee
    iget-object v2, v0, Lcom/android/internal/widget/PointerLocationView;->mText:Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@2f0
    invoke-virtual {v2}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->clear()Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@2f3
    move-result-object v2

    #@2f4
    const-string v7, "dX: "

    #@2f6
    invoke-virtual {v2, v7}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(Ljava/lang/String;)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@2f9
    move-result-object v2

    #@2fa
    const/4 v7, 0x1

    #@2fb
    move/from16 v0, v24

    #@2fd
    invoke-virtual {v2, v0, v7}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(FI)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@300
    move-result-object v2

    #@301
    invoke-virtual {v2}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->toString()Ljava/lang/String;

    #@304
    move-result-object v2

    #@305
    add-int/lit8 v7, v28, 0x1

    #@307
    int-to-float v7, v7

    #@308
    move/from16 v0, v21

    #@30a
    int-to-float v10, v0

    #@30b
    move-object/from16 v0, p0

    #@30d
    iget-object v11, v0, Lcom/android/internal/widget/PointerLocationView;->mTextPaint:Landroid/graphics/Paint;

    #@30f
    move-object/from16 v0, p1

    #@311
    invoke-virtual {v0, v2, v7, v10, v11}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    #@314
    .line 265
    mul-int/lit8 v2, v28, 0x2

    #@316
    int-to-float v3, v2

    #@317
    const/4 v4, 0x0

    #@318
    mul-int/lit8 v2, v28, 0x3

    #@31a
    add-int/lit8 v2, v2, -0x1

    #@31c
    int-to-float v5, v2

    #@31d
    move/from16 v0, v22

    #@31f
    int-to-float v6, v0

    #@320
    invoke-static/range {v25 .. v25}, Ljava/lang/Math;->abs(F)F

    #@323
    move-result v2

    #@324
    move-object/from16 v0, p0

    #@326
    iget-object v7, v0, Lcom/android/internal/widget/PointerLocationView;->mVC:Landroid/view/ViewConfiguration;

    #@328
    invoke-virtual {v7}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    #@32b
    move-result v7

    #@32c
    int-to-float v7, v7

    #@32d
    cmpg-float v2, v2, v7

    #@32f
    if-gez v2, :cond_36c

    #@331
    move-object/from16 v0, p0

    #@333
    iget-object v7, v0, Lcom/android/internal/widget/PointerLocationView;->mTextBackgroundPaint:Landroid/graphics/Paint;

    #@335
    :goto_335
    move-object/from16 v2, p1

    #@337
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    #@33a
    .line 268
    move-object/from16 v0, p0

    #@33c
    iget-object v2, v0, Lcom/android/internal/widget/PointerLocationView;->mText:Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@33e
    invoke-virtual {v2}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->clear()Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@341
    move-result-object v2

    #@342
    const-string v7, "dY: "

    #@344
    invoke-virtual {v2, v7}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(Ljava/lang/String;)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@347
    move-result-object v2

    #@348
    const/4 v7, 0x1

    #@349
    move/from16 v0, v25

    #@34b
    invoke-virtual {v2, v0, v7}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->append(FI)Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;

    #@34e
    move-result-object v2

    #@34f
    invoke-virtual {v2}, Lcom/android/internal/widget/PointerLocationView$FasterStringBuilder;->toString()Ljava/lang/String;

    #@352
    move-result-object v2

    #@353
    mul-int/lit8 v7, v28, 0x2

    #@355
    add-int/lit8 v7, v7, 0x1

    #@357
    int-to-float v7, v7

    #@358
    move/from16 v0, v21

    #@35a
    int-to-float v10, v0

    #@35b
    move-object/from16 v0, p0

    #@35d
    iget-object v11, v0, Lcom/android/internal/widget/PointerLocationView;->mTextPaint:Landroid/graphics/Paint;

    #@35f
    move-object/from16 v0, p1

    #@361
    invoke-virtual {v0, v2, v7, v10, v11}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    #@364
    goto/16 :goto_10e

    #@366
    .line 259
    :cond_366
    move-object/from16 v0, p0

    #@368
    iget-object v7, v0, Lcom/android/internal/widget/PointerLocationView;->mTextLevelPaint:Landroid/graphics/Paint;

    #@36a
    goto/16 :goto_2e7

    #@36c
    .line 265
    :cond_36c
    move-object/from16 v0, p0

    #@36e
    iget-object v7, v0, Lcom/android/internal/widget/PointerLocationView;->mTextLevelPaint:Landroid/graphics/Paint;

    #@370
    goto :goto_335

    #@371
    .line 316
    .end local v24           #dx:F
    .end local v25           #dy:F
    .restart local v3       #lastX:F
    .restart local v4       #lastY:F
    .restart local v5       #x:F
    .restart local v6       #y:F
    .restart local v23       #drawn:Z
    .restart local v26       #haveLast:Z
    .restart local v27       #i:I
    .restart local v31       #p:I
    :cond_371
    if-eqz v26, :cond_3a7

    #@373
    .line 318
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$700(Lcom/android/internal/widget/PointerLocationView$PointerState;)[I

    #@376
    move-result-object v2

    #@377
    aget v2, v2, v27

    #@379
    const/4 v7, 0x5

    #@37a
    if-ne v2, v7, :cond_3ad

    #@37c
    .line 319
    move-object/from16 v0, p0

    #@37e
    iget-object v2, v0, Lcom/android/internal/widget/PointerLocationView;->mPathPaint:Landroid/graphics/Paint;

    #@380
    const/16 v7, 0xff

    #@382
    const/16 v10, 0xff

    #@384
    const/4 v11, 0x0

    #@385
    const/16 v12, 0x60

    #@387
    invoke-virtual {v2, v7, v10, v11, v12}, Landroid/graphics/Paint;->setARGB(IIII)V

    #@38a
    .line 320
    move-object/from16 v0, p0

    #@38c
    iget-object v2, v0, Lcom/android/internal/widget/PointerLocationView;->mPathPaint:Landroid/graphics/Paint;

    #@38e
    const/high16 v7, 0x40a0

    #@390
    invoke-virtual {v2, v7}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    #@393
    .line 326
    :goto_393
    move-object/from16 v0, p0

    #@395
    iget-object v7, v0, Lcom/android/internal/widget/PointerLocationView;->mPathPaint:Landroid/graphics/Paint;

    #@397
    move-object/from16 v2, p1

    #@399
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    #@39c
    .line 327
    move-object/from16 v0, p0

    #@39e
    iget-object v2, v0, Lcom/android/internal/widget/PointerLocationView;->mPaint:Landroid/graphics/Paint;

    #@3a0
    move-object/from16 v0, p1

    #@3a2
    invoke-virtual {v0, v3, v4, v2}, Landroid/graphics/Canvas;->drawPoint(FFLandroid/graphics/Paint;)V

    #@3a5
    .line 328
    const/16 v23, 0x1

    #@3a7
    .line 330
    :cond_3a7
    move v3, v5

    #@3a8
    .line 331
    move v4, v6

    #@3a9
    .line 332
    const/16 v26, 0x1

    #@3ab
    goto/16 :goto_2a1

    #@3ad
    .line 322
    :cond_3ad
    move-object/from16 v0, p0

    #@3af
    iget-object v2, v0, Lcom/android/internal/widget/PointerLocationView;->mPathPaint:Landroid/graphics/Paint;

    #@3b1
    const/16 v7, 0xff

    #@3b3
    const/4 v10, 0x0

    #@3b4
    const/16 v11, 0x60

    #@3b6
    const/16 v12, 0xff

    #@3b8
    invoke-virtual {v2, v7, v10, v11, v12}, Landroid/graphics/Paint;->setARGB(IIII)V

    #@3bb
    .line 323
    move-object/from16 v0, p0

    #@3bd
    iget-object v2, v0, Lcom/android/internal/widget/PointerLocationView;->mPathPaint:Landroid/graphics/Paint;

    #@3bf
    const/high16 v7, 0x3f80

    #@3c1
    invoke-virtual {v2, v7}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    #@3c4
    goto :goto_393

    #@3c5
    .line 335
    .end local v5           #x:F
    .end local v6           #y:F
    :cond_3c5
    if-eqz v23, :cond_4db

    #@3c7
    .line 337
    move-object/from16 v0, p0

    #@3c9
    iget-object v2, v0, Lcom/android/internal/widget/PointerLocationView;->mPaint:Landroid/graphics/Paint;

    #@3cb
    const/16 v7, 0x80

    #@3cd
    const/16 v10, 0x80

    #@3cf
    const/4 v11, 0x0

    #@3d0
    const/16 v12, 0x80

    #@3d2
    invoke-virtual {v2, v7, v10, v11, v12}, Landroid/graphics/Paint;->setARGB(IIII)V

    #@3d5
    .line 338
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$800(Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/VelocityTracker$Estimator;

    #@3d8
    move-result-object v2

    #@3d9
    const v7, -0x425c28f6

    #@3dc
    invoke-virtual {v2, v7}, Landroid/view/VelocityTracker$Estimator;->estimateX(F)F

    #@3df
    move-result v8

    #@3e0
    .line 339
    .local v8, lx:F
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$800(Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/VelocityTracker$Estimator;

    #@3e3
    move-result-object v2

    #@3e4
    const v7, -0x425c28f6

    #@3e7
    invoke-virtual {v2, v7}, Landroid/view/VelocityTracker$Estimator;->estimateY(F)F

    #@3ea
    move-result v9

    #@3eb
    .line 340
    .local v9, ly:F
    const/16 v27, -0x3

    #@3ed
    :goto_3ed
    const/4 v2, 0x2

    #@3ee
    move/from16 v0, v27

    #@3f0
    if-gt v0, v2, :cond_420

    #@3f2
    .line 341
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$800(Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/VelocityTracker$Estimator;

    #@3f5
    move-result-object v2

    #@3f6
    move/from16 v0, v27

    #@3f8
    int-to-float v7, v0

    #@3f9
    const v10, 0x3ca3d70a

    #@3fc
    mul-float/2addr v7, v10

    #@3fd
    invoke-virtual {v2, v7}, Landroid/view/VelocityTracker$Estimator;->estimateX(F)F

    #@400
    move-result v5

    #@401
    .line 342
    .restart local v5       #x:F
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$800(Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/VelocityTracker$Estimator;

    #@404
    move-result-object v2

    #@405
    move/from16 v0, v27

    #@407
    int-to-float v7, v0

    #@408
    const v10, 0x3ca3d70a

    #@40b
    mul-float/2addr v7, v10

    #@40c
    invoke-virtual {v2, v7}, Landroid/view/VelocityTracker$Estimator;->estimateY(F)F

    #@40f
    move-result v6

    #@410
    .line 343
    .restart local v6       #y:F
    move-object/from16 v0, p0

    #@412
    iget-object v12, v0, Lcom/android/internal/widget/PointerLocationView;->mPaint:Landroid/graphics/Paint;

    #@414
    move-object/from16 v7, p1

    #@416
    move v10, v5

    #@417
    move v11, v6

    #@418
    invoke-virtual/range {v7 .. v12}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    #@41b
    .line 344
    move v8, v5

    #@41c
    .line 345
    move v9, v6

    #@41d
    .line 340
    add-int/lit8 v27, v27, 0x1

    #@41f
    goto :goto_3ed

    #@420
    .line 349
    .end local v5           #x:F
    .end local v6           #y:F
    :cond_420
    move-object/from16 v0, p0

    #@422
    iget-object v2, v0, Lcom/android/internal/widget/PointerLocationView;->mPaint:Landroid/graphics/Paint;

    #@424
    const/16 v7, 0xff

    #@426
    const/16 v10, 0xff

    #@428
    const/16 v11, 0x40

    #@42a
    const/16 v12, 0x80

    #@42c
    invoke-virtual {v2, v7, v10, v11, v12}, Landroid/graphics/Paint;->setARGB(IIII)V

    #@42f
    .line 350
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$500(Lcom/android/internal/widget/PointerLocationView$PointerState;)F

    #@432
    move-result v2

    #@433
    const/high16 v7, 0x4180

    #@435
    mul-float v36, v2, v7

    #@437
    .line 351
    .local v36, xVel:F
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$600(Lcom/android/internal/widget/PointerLocationView$PointerState;)F

    #@43a
    move-result v2

    #@43b
    const/high16 v7, 0x4180

    #@43d
    mul-float v37, v2, v7

    #@43f
    .line 352
    .local v37, yVel:F
    add-float v13, v3, v36

    #@441
    add-float v14, v4, v37

    #@443
    move-object/from16 v0, p0

    #@445
    iget-object v15, v0, Lcom/android/internal/widget/PointerLocationView;->mPaint:Landroid/graphics/Paint;

    #@447
    move-object/from16 v10, p1

    #@449
    move v11, v3

    #@44a
    move v12, v4

    #@44b
    invoke-virtual/range {v10 .. v15}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    #@44e
    .line 355
    move-object/from16 v0, p0

    #@450
    iget-object v2, v0, Lcom/android/internal/widget/PointerLocationView;->mAltVelocity:Landroid/view/VelocityTracker;

    #@452
    if-eqz v2, :cond_4db

    #@454
    .line 356
    move-object/from16 v0, p0

    #@456
    iget-object v2, v0, Lcom/android/internal/widget/PointerLocationView;->mPaint:Landroid/graphics/Paint;

    #@458
    const/16 v7, 0x80

    #@45a
    const/4 v10, 0x0

    #@45b
    const/16 v11, 0x80

    #@45d
    const/16 v12, 0x80

    #@45f
    invoke-virtual {v2, v7, v10, v11, v12}, Landroid/graphics/Paint;->setARGB(IIII)V

    #@462
    .line 357
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$900(Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/VelocityTracker$Estimator;

    #@465
    move-result-object v2

    #@466
    const v7, -0x425c28f6

    #@469
    invoke-virtual {v2, v7}, Landroid/view/VelocityTracker$Estimator;->estimateX(F)F

    #@46c
    move-result v8

    #@46d
    .line 358
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$900(Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/VelocityTracker$Estimator;

    #@470
    move-result-object v2

    #@471
    const v7, -0x425c28f6

    #@474
    invoke-virtual {v2, v7}, Landroid/view/VelocityTracker$Estimator;->estimateY(F)F

    #@477
    move-result v9

    #@478
    .line 359
    const/16 v27, -0x3

    #@47a
    :goto_47a
    const/4 v2, 0x2

    #@47b
    move/from16 v0, v27

    #@47d
    if-gt v0, v2, :cond_4ad

    #@47f
    .line 360
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$900(Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/VelocityTracker$Estimator;

    #@482
    move-result-object v2

    #@483
    move/from16 v0, v27

    #@485
    int-to-float v7, v0

    #@486
    const v10, 0x3ca3d70a

    #@489
    mul-float/2addr v7, v10

    #@48a
    invoke-virtual {v2, v7}, Landroid/view/VelocityTracker$Estimator;->estimateX(F)F

    #@48d
    move-result v5

    #@48e
    .line 361
    .restart local v5       #x:F
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$900(Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/VelocityTracker$Estimator;

    #@491
    move-result-object v2

    #@492
    move/from16 v0, v27

    #@494
    int-to-float v7, v0

    #@495
    const v10, 0x3ca3d70a

    #@498
    mul-float/2addr v7, v10

    #@499
    invoke-virtual {v2, v7}, Landroid/view/VelocityTracker$Estimator;->estimateY(F)F

    #@49c
    move-result v6

    #@49d
    .line 362
    .restart local v6       #y:F
    move-object/from16 v0, p0

    #@49f
    iget-object v12, v0, Lcom/android/internal/widget/PointerLocationView;->mPaint:Landroid/graphics/Paint;

    #@4a1
    move-object/from16 v7, p1

    #@4a3
    move v10, v5

    #@4a4
    move v11, v6

    #@4a5
    invoke-virtual/range {v7 .. v12}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    #@4a8
    .line 363
    move v8, v5

    #@4a9
    .line 364
    move v9, v6

    #@4aa
    .line 359
    add-int/lit8 v27, v27, 0x1

    #@4ac
    goto :goto_47a

    #@4ad
    .line 367
    .end local v5           #x:F
    .end local v6           #y:F
    :cond_4ad
    move-object/from16 v0, p0

    #@4af
    iget-object v2, v0, Lcom/android/internal/widget/PointerLocationView;->mPaint:Landroid/graphics/Paint;

    #@4b1
    const/16 v7, 0xff

    #@4b3
    const/16 v10, 0x40

    #@4b5
    const/16 v11, 0xff

    #@4b7
    const/16 v12, 0x80

    #@4b9
    invoke-virtual {v2, v7, v10, v11, v12}, Landroid/graphics/Paint;->setARGB(IIII)V

    #@4bc
    .line 368
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$1000(Lcom/android/internal/widget/PointerLocationView$PointerState;)F

    #@4bf
    move-result v2

    #@4c0
    const/high16 v7, 0x4180

    #@4c2
    mul-float v36, v2, v7

    #@4c4
    .line 369
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$1100(Lcom/android/internal/widget/PointerLocationView$PointerState;)F

    #@4c7
    move-result v2

    #@4c8
    const/high16 v7, 0x4180

    #@4ca
    mul-float v37, v2, v7

    #@4cc
    .line 370
    add-float v13, v3, v36

    #@4ce
    add-float v14, v4, v37

    #@4d0
    move-object/from16 v0, p0

    #@4d2
    iget-object v15, v0, Lcom/android/internal/widget/PointerLocationView;->mPaint:Landroid/graphics/Paint;

    #@4d4
    move-object/from16 v10, p1

    #@4d6
    move v11, v3

    #@4d7
    move v12, v4

    #@4d8
    invoke-virtual/range {v10 .. v15}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    #@4db
    .line 374
    .end local v8           #lx:F
    .end local v9           #ly:F
    .end local v36           #xVel:F
    .end local v37           #yVel:F
    :cond_4db
    move-object/from16 v0, p0

    #@4dd
    iget-boolean v2, v0, Lcom/android/internal/widget/PointerLocationView;->mCurDown:Z

    #@4df
    if-eqz v2, :cond_679

    #@4e1
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$100(Lcom/android/internal/widget/PointerLocationView$PointerState;)Z

    #@4e4
    move-result v2

    #@4e5
    if-eqz v2, :cond_679

    #@4e7
    .line 376
    const/4 v11, 0x0

    #@4e8
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$200(Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/MotionEvent$PointerCoords;

    #@4eb
    move-result-object v2

    #@4ec
    iget v12, v2, Landroid/view/MotionEvent$PointerCoords;->y:F

    #@4ee
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/widget/PointerLocationView;->getWidth()I

    #@4f1
    move-result v2

    #@4f2
    int-to-float v13, v2

    #@4f3
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$200(Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/MotionEvent$PointerCoords;

    #@4f6
    move-result-object v2

    #@4f7
    iget v14, v2, Landroid/view/MotionEvent$PointerCoords;->y:F

    #@4f9
    move-object/from16 v0, p0

    #@4fb
    iget-object v15, v0, Lcom/android/internal/widget/PointerLocationView;->mTargetPaint:Landroid/graphics/Paint;

    #@4fd
    move-object/from16 v10, p1

    #@4ff
    invoke-virtual/range {v10 .. v15}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    #@502
    .line 377
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$200(Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/MotionEvent$PointerCoords;

    #@505
    move-result-object v2

    #@506
    iget v11, v2, Landroid/view/MotionEvent$PointerCoords;->x:F

    #@508
    const/4 v12, 0x0

    #@509
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$200(Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/MotionEvent$PointerCoords;

    #@50c
    move-result-object v2

    #@50d
    iget v13, v2, Landroid/view/MotionEvent$PointerCoords;->x:F

    #@50f
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/widget/PointerLocationView;->getHeight()I

    #@512
    move-result v2

    #@513
    int-to-float v14, v2

    #@514
    move-object/from16 v0, p0

    #@516
    iget-object v15, v0, Lcom/android/internal/widget/PointerLocationView;->mTargetPaint:Landroid/graphics/Paint;

    #@518
    move-object/from16 v10, p1

    #@51a
    invoke-virtual/range {v10 .. v15}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    #@51d
    .line 380
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$200(Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/MotionEvent$PointerCoords;

    #@520
    move-result-object v2

    #@521
    iget v2, v2, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    #@523
    const/high16 v7, 0x437f

    #@525
    mul-float/2addr v2, v7

    #@526
    float-to-int v0, v2

    #@527
    move/from16 v32, v0

    #@529
    .line 381
    .local v32, pressureLevel:I
    move-object/from16 v0, p0

    #@52b
    iget-object v2, v0, Lcom/android/internal/widget/PointerLocationView;->mPaint:Landroid/graphics/Paint;

    #@52d
    const/16 v7, 0xff

    #@52f
    const/16 v10, 0xff

    #@531
    move/from16 v0, v32

    #@533
    rsub-int v11, v0, 0xff

    #@535
    move/from16 v0, v32

    #@537
    invoke-virtual {v2, v7, v0, v10, v11}, Landroid/graphics/Paint;->setARGB(IIII)V

    #@53a
    .line 382
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$200(Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/MotionEvent$PointerCoords;

    #@53d
    move-result-object v2

    #@53e
    iget v2, v2, Landroid/view/MotionEvent$PointerCoords;->x:F

    #@540
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$200(Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/MotionEvent$PointerCoords;

    #@543
    move-result-object v7

    #@544
    iget v7, v7, Landroid/view/MotionEvent$PointerCoords;->y:F

    #@546
    move-object/from16 v0, p0

    #@548
    iget-object v10, v0, Lcom/android/internal/widget/PointerLocationView;->mPaint:Landroid/graphics/Paint;

    #@54a
    move-object/from16 v0, p1

    #@54c
    invoke-virtual {v0, v2, v7, v10}, Landroid/graphics/Canvas;->drawPoint(FFLandroid/graphics/Paint;)V

    #@54f
    .line 385
    move-object/from16 v0, p0

    #@551
    iget-object v2, v0, Lcom/android/internal/widget/PointerLocationView;->mPaint:Landroid/graphics/Paint;

    #@553
    const/16 v7, 0xff

    #@555
    move/from16 v0, v32

    #@557
    rsub-int v10, v0, 0xff

    #@559
    const/16 v11, 0x80

    #@55b
    move/from16 v0, v32

    #@55d
    invoke-virtual {v2, v7, v0, v10, v11}, Landroid/graphics/Paint;->setARGB(IIII)V

    #@560
    .line 386
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$200(Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/MotionEvent$PointerCoords;

    #@563
    move-result-object v2

    #@564
    iget v12, v2, Landroid/view/MotionEvent$PointerCoords;->x:F

    #@566
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$200(Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/MotionEvent$PointerCoords;

    #@569
    move-result-object v2

    #@56a
    iget v13, v2, Landroid/view/MotionEvent$PointerCoords;->y:F

    #@56c
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$200(Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/MotionEvent$PointerCoords;

    #@56f
    move-result-object v2

    #@570
    iget v14, v2, Landroid/view/MotionEvent$PointerCoords;->touchMajor:F

    #@572
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$200(Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/MotionEvent$PointerCoords;

    #@575
    move-result-object v2

    #@576
    iget v15, v2, Landroid/view/MotionEvent$PointerCoords;->touchMinor:F

    #@578
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$200(Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/MotionEvent$PointerCoords;

    #@57b
    move-result-object v2

    #@57c
    iget v0, v2, Landroid/view/MotionEvent$PointerCoords;->orientation:F

    #@57e
    move/from16 v16, v0

    #@580
    move-object/from16 v0, p0

    #@582
    iget-object v0, v0, Lcom/android/internal/widget/PointerLocationView;->mPaint:Landroid/graphics/Paint;

    #@584
    move-object/from16 v17, v0

    #@586
    move-object/from16 v10, p0

    #@588
    move-object/from16 v11, p1

    #@58a
    invoke-direct/range {v10 .. v17}, Lcom/android/internal/widget/PointerLocationView;->drawOval(Landroid/graphics/Canvas;FFFFFLandroid/graphics/Paint;)V

    #@58d
    .line 390
    move-object/from16 v0, p0

    #@58f
    iget-object v2, v0, Lcom/android/internal/widget/PointerLocationView;->mPaint:Landroid/graphics/Paint;

    #@591
    const/16 v7, 0xff

    #@593
    const/16 v10, 0x80

    #@595
    move/from16 v0, v32

    #@597
    rsub-int v11, v0, 0xff

    #@599
    move/from16 v0, v32

    #@59b
    invoke-virtual {v2, v7, v0, v10, v11}, Landroid/graphics/Paint;->setARGB(IIII)V

    #@59e
    .line 391
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$200(Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/MotionEvent$PointerCoords;

    #@5a1
    move-result-object v2

    #@5a2
    iget v12, v2, Landroid/view/MotionEvent$PointerCoords;->x:F

    #@5a4
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$200(Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/MotionEvent$PointerCoords;

    #@5a7
    move-result-object v2

    #@5a8
    iget v13, v2, Landroid/view/MotionEvent$PointerCoords;->y:F

    #@5aa
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$200(Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/MotionEvent$PointerCoords;

    #@5ad
    move-result-object v2

    #@5ae
    iget v14, v2, Landroid/view/MotionEvent$PointerCoords;->toolMajor:F

    #@5b0
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$200(Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/MotionEvent$PointerCoords;

    #@5b3
    move-result-object v2

    #@5b4
    iget v15, v2, Landroid/view/MotionEvent$PointerCoords;->toolMinor:F

    #@5b6
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$200(Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/MotionEvent$PointerCoords;

    #@5b9
    move-result-object v2

    #@5ba
    iget v0, v2, Landroid/view/MotionEvent$PointerCoords;->orientation:F

    #@5bc
    move/from16 v16, v0

    #@5be
    move-object/from16 v0, p0

    #@5c0
    iget-object v0, v0, Lcom/android/internal/widget/PointerLocationView;->mPaint:Landroid/graphics/Paint;

    #@5c2
    move-object/from16 v17, v0

    #@5c4
    move-object/from16 v10, p0

    #@5c6
    move-object/from16 v11, p1

    #@5c8
    invoke-direct/range {v10 .. v17}, Lcom/android/internal/widget/PointerLocationView;->drawOval(Landroid/graphics/Canvas;FFFFFLandroid/graphics/Paint;)V

    #@5cb
    .line 395
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$200(Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/MotionEvent$PointerCoords;

    #@5ce
    move-result-object v2

    #@5cf
    iget v2, v2, Landroid/view/MotionEvent$PointerCoords;->toolMajor:F

    #@5d1
    const v7, 0x3f333333

    #@5d4
    mul-float v20, v2, v7

    #@5d6
    .line 396
    .local v20, arrowSize:F
    const/high16 v2, 0x41a0

    #@5d8
    cmpg-float v2, v20, v2

    #@5da
    if-gez v2, :cond_5de

    #@5dc
    .line 397
    const/high16 v20, 0x41a0

    #@5de
    .line 399
    :cond_5de
    move-object/from16 v0, p0

    #@5e0
    iget-object v2, v0, Lcom/android/internal/widget/PointerLocationView;->mPaint:Landroid/graphics/Paint;

    #@5e2
    const/16 v7, 0xff

    #@5e4
    const/16 v10, 0xff

    #@5e6
    const/4 v11, 0x0

    #@5e7
    move/from16 v0, v32

    #@5e9
    invoke-virtual {v2, v7, v0, v10, v11}, Landroid/graphics/Paint;->setARGB(IIII)V

    #@5ec
    .line 400
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$200(Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/MotionEvent$PointerCoords;

    #@5ef
    move-result-object v2

    #@5f0
    iget v2, v2, Landroid/view/MotionEvent$PointerCoords;->orientation:F

    #@5f2
    float-to-double v10, v2

    #@5f3
    invoke-static {v10, v11}, Ljava/lang/Math;->sin(D)D

    #@5f6
    move-result-wide v10

    #@5f7
    move/from16 v0, v20

    #@5f9
    float-to-double v12, v0

    #@5fa
    mul-double/2addr v10, v12

    #@5fb
    double-to-float v0, v10

    #@5fc
    move/from16 v29, v0

    #@5fe
    .line 402
    .local v29, orientationVectorX:F
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$200(Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/MotionEvent$PointerCoords;

    #@601
    move-result-object v2

    #@602
    iget v2, v2, Landroid/view/MotionEvent$PointerCoords;->orientation:F

    #@604
    float-to-double v10, v2

    #@605
    invoke-static {v10, v11}, Ljava/lang/Math;->cos(D)D

    #@608
    move-result-wide v10

    #@609
    neg-double v10, v10

    #@60a
    move/from16 v0, v20

    #@60c
    float-to-double v12, v0

    #@60d
    mul-double/2addr v10, v12

    #@60e
    double-to-float v0, v10

    #@60f
    move/from16 v30, v0

    #@611
    .line 407
    .local v30, orientationVectorY:F
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$700(Lcom/android/internal/widget/PointerLocationView$PointerState;)[I

    #@614
    move-result-object v2

    #@615
    const/4 v7, 0x0

    #@616
    aget v2, v2, v7

    #@618
    const/4 v7, 0x2

    #@619
    if-eq v2, v7, :cond_625

    #@61b
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$700(Lcom/android/internal/widget/PointerLocationView$PointerState;)[I

    #@61e
    move-result-object v2

    #@61f
    const/4 v7, 0x0

    #@620
    aget v2, v2, v7

    #@622
    const/4 v7, 0x4

    #@623
    if-ne v2, v7, :cond_67d

    #@625
    .line 410
    :cond_625
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$200(Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/MotionEvent$PointerCoords;

    #@628
    move-result-object v2

    #@629
    iget v11, v2, Landroid/view/MotionEvent$PointerCoords;->x:F

    #@62b
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$200(Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/MotionEvent$PointerCoords;

    #@62e
    move-result-object v2

    #@62f
    iget v12, v2, Landroid/view/MotionEvent$PointerCoords;->y:F

    #@631
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$200(Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/MotionEvent$PointerCoords;

    #@634
    move-result-object v2

    #@635
    iget v2, v2, Landroid/view/MotionEvent$PointerCoords;->x:F

    #@637
    add-float v13, v2, v29

    #@639
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$200(Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/MotionEvent$PointerCoords;

    #@63c
    move-result-object v2

    #@63d
    iget v2, v2, Landroid/view/MotionEvent$PointerCoords;->y:F

    #@63f
    add-float v14, v2, v30

    #@641
    move-object/from16 v0, p0

    #@643
    iget-object v15, v0, Lcom/android/internal/widget/PointerLocationView;->mPaint:Landroid/graphics/Paint;

    #@645
    move-object/from16 v10, p1

    #@647
    invoke-virtual/range {v10 .. v15}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    #@64a
    .line 425
    :goto_64a
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$200(Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/MotionEvent$PointerCoords;

    #@64d
    move-result-object v2

    #@64e
    const/16 v7, 0x19

    #@650
    invoke-virtual {v2, v7}, Landroid/view/MotionEvent$PointerCoords;->getAxisValue(I)F

    #@653
    move-result v2

    #@654
    float-to-double v10, v2

    #@655
    invoke-static {v10, v11}, Ljava/lang/Math;->sin(D)D

    #@658
    move-result-wide v10

    #@659
    double-to-float v0, v10

    #@65a
    move/from16 v34, v0

    #@65c
    .line 427
    .local v34, tiltScale:F
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$200(Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/MotionEvent$PointerCoords;

    #@65f
    move-result-object v2

    #@660
    iget v2, v2, Landroid/view/MotionEvent$PointerCoords;->x:F

    #@662
    mul-float v7, v29, v34

    #@664
    add-float/2addr v2, v7

    #@665
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$200(Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/MotionEvent$PointerCoords;

    #@668
    move-result-object v7

    #@669
    iget v7, v7, Landroid/view/MotionEvent$PointerCoords;->y:F

    #@66b
    mul-float v10, v30, v34

    #@66d
    add-float/2addr v7, v10

    #@66e
    const/high16 v10, 0x4040

    #@670
    move-object/from16 v0, p0

    #@672
    iget-object v11, v0, Lcom/android/internal/widget/PointerLocationView;->mPaint:Landroid/graphics/Paint;

    #@674
    move-object/from16 v0, p1

    #@676
    invoke-virtual {v0, v2, v7, v10, v11}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    #@679
    .line 299
    .end local v20           #arrowSize:F
    .end local v29           #orientationVectorX:F
    .end local v30           #orientationVectorY:F
    .end local v32           #pressureLevel:I
    .end local v34           #tiltScale:F
    :cond_679
    add-int/lit8 v31, v31, 0x1

    #@67b
    goto/16 :goto_25a

    #@67d
    .line 416
    .restart local v20       #arrowSize:F
    .restart local v29       #orientationVectorX:F
    .restart local v30       #orientationVectorY:F
    .restart local v32       #pressureLevel:I
    :cond_67d
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$200(Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/MotionEvent$PointerCoords;

    #@680
    move-result-object v2

    #@681
    iget v2, v2, Landroid/view/MotionEvent$PointerCoords;->x:F

    #@683
    sub-float v11, v2, v29

    #@685
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$200(Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/MotionEvent$PointerCoords;

    #@688
    move-result-object v2

    #@689
    iget v2, v2, Landroid/view/MotionEvent$PointerCoords;->y:F

    #@68b
    sub-float v12, v2, v30

    #@68d
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$200(Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/MotionEvent$PointerCoords;

    #@690
    move-result-object v2

    #@691
    iget v2, v2, Landroid/view/MotionEvent$PointerCoords;->x:F

    #@693
    add-float v13, v2, v29

    #@695
    invoke-static/range {v33 .. v33}, Lcom/android/internal/widget/PointerLocationView$PointerState;->access$200(Lcom/android/internal/widget/PointerLocationView$PointerState;)Landroid/view/MotionEvent$PointerCoords;

    #@698
    move-result-object v2

    #@699
    iget v2, v2, Landroid/view/MotionEvent$PointerCoords;->y:F

    #@69b
    add-float v14, v2, v30

    #@69d
    move-object/from16 v0, p0

    #@69f
    iget-object v15, v0, Lcom/android/internal/widget/PointerLocationView;->mPaint:Landroid/graphics/Paint;

    #@6a1
    move-object/from16 v10, p1

    #@6a3
    invoke-virtual/range {v10 .. v15}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    #@6a6
    goto :goto_64a

    #@6a7
    .line 433
    .end local v3           #lastX:F
    .end local v4           #lastY:F
    .end local v18           #N:I
    .end local v20           #arrowSize:F
    .end local v23           #drawn:Z
    .end local v26           #haveLast:Z
    .end local v27           #i:I
    .end local v29           #orientationVectorX:F
    .end local v30           #orientationVectorY:F
    .end local v32           #pressureLevel:I
    .end local v33           #ps:Lcom/android/internal/widget/PointerLocationView$PointerState;
    :cond_6a7
    return-void
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .registers 4
    .parameter "event"

    #@0
    .prologue
    .line 667
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSource()I

    #@3
    move-result v0

    #@4
    .line 668
    .local v0, source:I
    and-int/lit8 v1, v0, 0x2

    #@6
    if-eqz v1, :cond_d

    #@8
    .line 669
    invoke-virtual {p0, p1}, Lcom/android/internal/widget/PointerLocationView;->addPointerEvent(Landroid/view/MotionEvent;)V

    #@b
    .line 677
    :goto_b
    const/4 v1, 0x1

    #@c
    return v1

    #@d
    .line 670
    :cond_d
    and-int/lit8 v1, v0, 0x10

    #@f
    if-eqz v1, :cond_17

    #@11
    .line 671
    const-string v1, "Joystick"

    #@13
    invoke-direct {p0, v1, p1}, Lcom/android/internal/widget/PointerLocationView;->logMotionEvent(Ljava/lang/String;Landroid/view/MotionEvent;)V

    #@16
    goto :goto_b

    #@17
    .line 672
    :cond_17
    and-int/lit8 v1, v0, 0x8

    #@19
    if-eqz v1, :cond_21

    #@1b
    .line 673
    const-string v1, "Position"

    #@1d
    invoke-direct {p0, v1, p1}, Lcom/android/internal/widget/PointerLocationView;->logMotionEvent(Ljava/lang/String;Landroid/view/MotionEvent;)V

    #@20
    goto :goto_b

    #@21
    .line 675
    :cond_21
    const-string v1, "Generic"

    #@23
    invoke-direct {p0, v1, p1}, Lcom/android/internal/widget/PointerLocationView;->logMotionEvent(Ljava/lang/String;Landroid/view/MotionEvent;)V

    #@26
    goto :goto_b
.end method

.method public onInputDeviceAdded(I)V
    .registers 3
    .parameter "deviceId"

    #@0
    .prologue
    .line 740
    const-string v0, "Device Added"

    #@2
    invoke-direct {p0, p1, v0}, Lcom/android/internal/widget/PointerLocationView;->logInputDeviceState(ILjava/lang/String;)V

    #@5
    .line 741
    return-void
.end method

.method public onInputDeviceChanged(I)V
    .registers 3
    .parameter "deviceId"

    #@0
    .prologue
    .line 745
    const-string v0, "Device Changed"

    #@2
    invoke-direct {p0, p1, v0}, Lcom/android/internal/widget/PointerLocationView;->logInputDeviceState(ILjava/lang/String;)V

    #@5
    .line 746
    return-void
.end method

.method public onInputDeviceRemoved(I)V
    .registers 3
    .parameter "deviceId"

    #@0
    .prologue
    .line 750
    const-string v0, "Device Removed"

    #@2
    invoke-direct {p0, p1, v0}, Lcom/android/internal/widget/PointerLocationView;->logInputDeviceState(ILjava/lang/String;)V

    #@5
    .line 751
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 7
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    .line 682
    invoke-static {p1}, Lcom/android/internal/widget/PointerLocationView;->shouldLogKey(I)Z

    #@3
    move-result v1

    #@4
    if-eqz v1, :cond_49

    #@6
    .line 683
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    #@9
    move-result v0

    #@a
    .line 684
    .local v0, repeatCount:I
    if-nez v0, :cond_26

    #@c
    .line 685
    const-string v1, "Pointer"

    #@e
    new-instance v2, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v3, "Key Down: "

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 689
    :goto_24
    const/4 v1, 0x1

    #@25
    .line 691
    .end local v0           #repeatCount:I
    :goto_25
    return v1

    #@26
    .line 687
    .restart local v0       #repeatCount:I
    :cond_26
    const-string v1, "Pointer"

    #@28
    new-instance v2, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v3, "Key Repeat #"

    #@2f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    const-string v3, ": "

    #@39
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v2

    #@3d
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v2

    #@41
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v2

    #@45
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    goto :goto_24

    #@49
    .line 691
    .end local v0           #repeatCount:I
    :cond_49
    invoke-super {p0, p1, p2}, Landroid/view/View;->onKeyDown(ILandroid/view/KeyEvent;)Z

    #@4c
    move-result v1

    #@4d
    goto :goto_25
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .registers 6
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    .line 696
    invoke-static {p1}, Lcom/android/internal/widget/PointerLocationView;->shouldLogKey(I)Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_20

    #@6
    .line 697
    const-string v0, "Pointer"

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "Key Up: "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 698
    const/4 v0, 0x1

    #@1f
    .line 700
    :goto_1f
    return v0

    #@20
    :cond_20
    invoke-super {p0, p1, p2}, Landroid/view/View;->onKeyUp(ILandroid/view/KeyEvent;)Z

    #@23
    move-result v0

    #@24
    goto :goto_1f
.end method

.method protected onMeasure(II)V
    .registers 5
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    .line 200
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    #@3
    .line 201
    iget-object v0, p0, Lcom/android/internal/widget/PointerLocationView;->mTextPaint:Landroid/graphics/Paint;

    #@5
    iget-object v1, p0, Lcom/android/internal/widget/PointerLocationView;->mTextMetrics:Landroid/graphics/Paint$FontMetricsInt;

    #@7
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->getFontMetricsInt(Landroid/graphics/Paint$FontMetricsInt;)I

    #@a
    .line 202
    iget-object v0, p0, Lcom/android/internal/widget/PointerLocationView;->mTextMetrics:Landroid/graphics/Paint$FontMetricsInt;

    #@c
    iget v0, v0, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    #@e
    neg-int v0, v0

    #@f
    iget-object v1, p0, Lcom/android/internal/widget/PointerLocationView;->mTextMetrics:Landroid/graphics/Paint$FontMetricsInt;

    #@11
    iget v1, v1, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    #@13
    add-int/2addr v0, v1

    #@14
    add-int/lit8 v0, v0, 0x2

    #@16
    iput v0, p0, Lcom/android/internal/widget/PointerLocationView;->mHeaderBottom:I

    #@18
    .line 210
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 657
    invoke-virtual {p0, p1}, Lcom/android/internal/widget/PointerLocationView;->addPointerEvent(Landroid/view/MotionEvent;)V

    #@3
    .line 659
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@6
    move-result v0

    #@7
    if-nez v0, :cond_12

    #@9
    invoke-virtual {p0}, Lcom/android/internal/widget/PointerLocationView;->isFocused()Z

    #@c
    move-result v0

    #@d
    if-nez v0, :cond_12

    #@f
    .line 660
    invoke-virtual {p0}, Lcom/android/internal/widget/PointerLocationView;->requestFocus()Z

    #@12
    .line 662
    :cond_12
    const/4 v0, 0x1

    #@13
    return v0
.end method

.method public onTrackballEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 719
    const-string v0, "Trackball"

    #@2
    invoke-direct {p0, v0, p1}, Lcom/android/internal/widget/PointerLocationView;->logMotionEvent(Ljava/lang/String;Landroid/view/MotionEvent;)V

    #@5
    .line 720
    const/4 v0, 0x1

    #@6
    return v0
.end method

.method public setPrintCoords(Z)V
    .registers 2
    .parameter "state"

    #@0
    .prologue
    .line 195
    iput-boolean p1, p0, Lcom/android/internal/widget/PointerLocationView;->mPrintCoords:Z

    #@2
    .line 196
    return-void
.end method
