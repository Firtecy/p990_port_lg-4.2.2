.class public Lcom/android/internal/widget/multiwaveview/PointCloud$WaveManager;
.super Ljava/lang/Object;
.source "PointCloud.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/widget/multiwaveview/PointCloud;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "WaveManager"
.end annotation


# instance fields
.field private alpha:F

.field private radius:F

.field final synthetic this$0:Lcom/android/internal/widget/multiwaveview/PointCloud;

.field private width:F


# direct methods
.method public constructor <init>(Lcom/android/internal/widget/multiwaveview/PointCloud;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 46
    iput-object p1, p0, Lcom/android/internal/widget/multiwaveview/PointCloud$WaveManager;->this$0:Lcom/android/internal/widget/multiwaveview/PointCloud;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 47
    const/high16 v0, 0x4248

    #@7
    iput v0, p0, Lcom/android/internal/widget/multiwaveview/PointCloud$WaveManager;->radius:F

    #@9
    .line 48
    const/high16 v0, 0x4348

    #@b
    iput v0, p0, Lcom/android/internal/widget/multiwaveview/PointCloud$WaveManager;->width:F

    #@d
    .line 49
    const/4 v0, 0x0

    #@e
    iput v0, p0, Lcom/android/internal/widget/multiwaveview/PointCloud$WaveManager;->alpha:F

    #@10
    return-void
.end method

.method static synthetic access$400(Lcom/android/internal/widget/multiwaveview/PointCloud$WaveManager;)F
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 46
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/PointCloud$WaveManager;->radius:F

    #@2
    return v0
.end method

.method static synthetic access$500(Lcom/android/internal/widget/multiwaveview/PointCloud$WaveManager;)F
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 46
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/PointCloud$WaveManager;->width:F

    #@2
    return v0
.end method

.method static synthetic access$600(Lcom/android/internal/widget/multiwaveview/PointCloud$WaveManager;)F
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 46
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/PointCloud$WaveManager;->alpha:F

    #@2
    return v0
.end method


# virtual methods
.method public getAlpha()F
    .registers 2

    #@0
    .prologue
    .line 63
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/PointCloud$WaveManager;->alpha:F

    #@2
    return v0
.end method

.method public getRadius()F
    .registers 2

    #@0
    .prologue
    .line 55
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/PointCloud$WaveManager;->radius:F

    #@2
    return v0
.end method

.method public setAlpha(F)V
    .registers 2
    .parameter "a"

    #@0
    .prologue
    .line 59
    iput p1, p0, Lcom/android/internal/widget/multiwaveview/PointCloud$WaveManager;->alpha:F

    #@2
    .line 60
    return-void
.end method

.method public setRadius(F)V
    .registers 2
    .parameter "r"

    #@0
    .prologue
    .line 51
    iput p1, p0, Lcom/android/internal/widget/multiwaveview/PointCloud$WaveManager;->radius:F

    #@2
    .line 52
    return-void
.end method
