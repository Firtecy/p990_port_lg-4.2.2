.class public Lcom/android/internal/widget/ScrollingTabContainerView$TabView;
.super Landroid/widget/LinearLayout;
.source "ScrollingTabContainerView.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/widget/ScrollingTabContainerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "TabView"
.end annotation


# instance fields
.field private mCustomView:Landroid/view/View;

.field private mIconView:Landroid/widget/ImageView;

.field private mTab:Landroid/app/ActionBar$Tab;

.field private mTextView:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/android/internal/widget/ScrollingTabContainerView;


# direct methods
.method public constructor <init>(Lcom/android/internal/widget/ScrollingTabContainerView;Landroid/content/Context;Landroid/app/ActionBar$Tab;Z)V
    .registers 7
    .parameter
    .parameter "context"
    .parameter "tab"
    .parameter "forList"

    #@0
    .prologue
    .line 379
    iput-object p1, p0, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->this$0:Lcom/android/internal/widget/ScrollingTabContainerView;

    #@2
    .line 380
    const/4 v0, 0x0

    #@3
    const v1, 0x10102f3

    #@6
    invoke-direct {p0, p2, v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@9
    .line 381
    iput-object p3, p0, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->mTab:Landroid/app/ActionBar$Tab;

    #@b
    .line 383
    if-eqz p4, :cond_13

    #@d
    .line 384
    const v0, 0x800013

    #@10
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->setGravity(I)V

    #@13
    .line 387
    :cond_13
    invoke-virtual {p0}, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->update()V

    #@16
    .line 388
    return-void
.end method


# virtual methods
.method public bindTab(Landroid/app/ActionBar$Tab;)V
    .registers 2
    .parameter "tab"

    #@0
    .prologue
    .line 391
    iput-object p1, p0, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->mTab:Landroid/app/ActionBar$Tab;

    #@2
    .line 392
    invoke-virtual {p0}, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->update()V

    #@5
    .line 393
    return-void
.end method

.method public getTab()Landroid/app/ActionBar$Tab;
    .registers 2

    #@0
    .prologue
    .line 499
    iget-object v0, p0, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->mTab:Landroid/app/ActionBar$Tab;

    #@2
    return-object v0
.end method

.method public onLongClick(Landroid/view/View;)Z
    .registers 11
    .parameter "v"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 480
    const/4 v6, 0x2

    #@2
    new-array v3, v6, [I

    #@4
    .line 481
    .local v3, screenPos:[I
    invoke-virtual {p0, v3}, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->getLocationOnScreen([I)V

    #@7
    .line 483
    invoke-virtual {p0}, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->getContext()Landroid/content/Context;

    #@a
    move-result-object v1

    #@b
    .line 484
    .local v1, context:Landroid/content/Context;
    invoke-virtual {p0}, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->getWidth()I

    #@e
    move-result v5

    #@f
    .line 485
    .local v5, width:I
    invoke-virtual {p0}, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->getHeight()I

    #@12
    move-result v2

    #@13
    .line 486
    .local v2, height:I
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@16
    move-result-object v6

    #@17
    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@1a
    move-result-object v6

    #@1b
    iget v4, v6, Landroid/util/DisplayMetrics;->widthPixels:I

    #@1d
    .line 488
    .local v4, screenWidth:I
    iget-object v6, p0, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->mTab:Landroid/app/ActionBar$Tab;

    #@1f
    invoke-virtual {v6}, Landroid/app/ActionBar$Tab;->getContentDescription()Ljava/lang/CharSequence;

    #@22
    move-result-object v6

    #@23
    invoke-static {v1, v6, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@26
    move-result-object v0

    #@27
    .line 491
    .local v0, cheatSheet:Landroid/widget/Toast;
    const/16 v6, 0x31

    #@29
    aget v7, v3, v7

    #@2b
    div-int/lit8 v8, v5, 0x2

    #@2d
    add-int/2addr v7, v8

    #@2e
    div-int/lit8 v8, v4, 0x2

    #@30
    sub-int/2addr v7, v8

    #@31
    invoke-virtual {v0, v6, v7, v2}, Landroid/widget/Toast;->setGravity(III)V

    #@34
    .line 494
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    #@37
    .line 495
    const/4 v6, 0x1

    #@38
    return v6
.end method

.method public onMeasure(II)V
    .registers 5
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    .line 397
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    #@3
    .line 400
    iget-object v0, p0, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->this$0:Lcom/android/internal/widget/ScrollingTabContainerView;

    #@5
    iget v0, v0, Lcom/android/internal/widget/ScrollingTabContainerView;->mMaxTabWidth:I

    #@7
    if-lez v0, :cond_20

    #@9
    invoke-virtual {p0}, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->getMeasuredWidth()I

    #@c
    move-result v0

    #@d
    iget-object v1, p0, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->this$0:Lcom/android/internal/widget/ScrollingTabContainerView;

    #@f
    iget v1, v1, Lcom/android/internal/widget/ScrollingTabContainerView;->mMaxTabWidth:I

    #@11
    if-le v0, v1, :cond_20

    #@13
    .line 401
    iget-object v0, p0, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->this$0:Lcom/android/internal/widget/ScrollingTabContainerView;

    #@15
    iget v0, v0, Lcom/android/internal/widget/ScrollingTabContainerView;->mMaxTabWidth:I

    #@17
    const/high16 v1, 0x4000

    #@19
    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@1c
    move-result v0

    #@1d
    invoke-super {p0, v0, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    #@20
    .line 404
    :cond_20
    return-void
.end method

.method public update()V
    .registers 16

    #@0
    .prologue
    const/16 v14, 0x10

    #@2
    const/16 v11, 0x8

    #@4
    const/4 v13, -0x2

    #@5
    const/4 v9, 0x0

    #@6
    const/4 v12, 0x0

    #@7
    .line 407
    iget-object v6, p0, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->mTab:Landroid/app/ActionBar$Tab;

    #@9
    .line 408
    .local v6, tab:Landroid/app/ActionBar$Tab;
    invoke-virtual {v6}, Landroid/app/ActionBar$Tab;->getCustomView()Landroid/view/View;

    #@c
    move-result-object v0

    #@d
    .line 409
    .local v0, custom:Landroid/view/View;
    if-eqz v0, :cond_39

    #@f
    .line 410
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    #@12
    move-result-object v1

    #@13
    .line 411
    .local v1, customParent:Landroid/view/ViewParent;
    if-eq v1, p0, :cond_1f

    #@15
    .line 412
    if-eqz v1, :cond_1c

    #@17
    check-cast v1, Landroid/view/ViewGroup;

    #@19
    .end local v1           #customParent:Landroid/view/ViewParent;
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    #@1c
    .line 413
    :cond_1c
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->addView(Landroid/view/View;)V

    #@1f
    .line 415
    :cond_1f
    iput-object v0, p0, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->mCustomView:Landroid/view/View;

    #@21
    .line 416
    iget-object v9, p0, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->mTextView:Landroid/widget/TextView;

    #@23
    if-eqz v9, :cond_2a

    #@25
    iget-object v9, p0, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->mTextView:Landroid/widget/TextView;

    #@27
    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setVisibility(I)V

    #@2a
    .line 417
    :cond_2a
    iget-object v9, p0, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->mIconView:Landroid/widget/ImageView;

    #@2c
    if-eqz v9, :cond_38

    #@2e
    .line 418
    iget-object v9, p0, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->mIconView:Landroid/widget/ImageView;

    #@30
    invoke-virtual {v9, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    #@33
    .line 419
    iget-object v9, p0, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->mIconView:Landroid/widget/ImageView;

    #@35
    invoke-virtual {v9, v12}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    #@38
    .line 477
    :cond_38
    :goto_38
    return-void

    #@39
    .line 422
    :cond_39
    iget-object v10, p0, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->mCustomView:Landroid/view/View;

    #@3b
    if-eqz v10, :cond_44

    #@3d
    .line 423
    iget-object v10, p0, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->mCustomView:Landroid/view/View;

    #@3f
    invoke-virtual {p0, v10}, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->removeView(Landroid/view/View;)V

    #@42
    .line 424
    iput-object v12, p0, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->mCustomView:Landroid/view/View;

    #@44
    .line 427
    :cond_44
    invoke-virtual {v6}, Landroid/app/ActionBar$Tab;->getIcon()Landroid/graphics/drawable/Drawable;

    #@47
    move-result-object v3

    #@48
    .line 428
    .local v3, icon:Landroid/graphics/drawable/Drawable;
    invoke-virtual {v6}, Landroid/app/ActionBar$Tab;->getText()Ljava/lang/CharSequence;

    #@4b
    move-result-object v7

    #@4c
    .line 430
    .local v7, text:Ljava/lang/CharSequence;
    if-eqz v3, :cond_c4

    #@4e
    .line 431
    iget-object v10, p0, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->mIconView:Landroid/widget/ImageView;

    #@50
    if-nez v10, :cond_6a

    #@52
    .line 432
    new-instance v4, Landroid/widget/ImageView;

    #@54
    invoke-virtual {p0}, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->getContext()Landroid/content/Context;

    #@57
    move-result-object v10

    #@58
    invoke-direct {v4, v10}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    #@5b
    .line 433
    .local v4, iconView:Landroid/widget/ImageView;
    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    #@5d
    invoke-direct {v5, v13, v13}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    #@60
    .line 435
    .local v5, lp:Landroid/widget/LinearLayout$LayoutParams;
    iput v14, v5, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    #@62
    .line 436
    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@65
    .line 437
    invoke-virtual {p0, v4, v9}, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->addView(Landroid/view/View;I)V

    #@68
    .line 438
    iput-object v4, p0, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->mIconView:Landroid/widget/ImageView;

    #@6a
    .line 440
    .end local v4           #iconView:Landroid/widget/ImageView;
    .end local v5           #lp:Landroid/widget/LinearLayout$LayoutParams;
    :cond_6a
    iget-object v10, p0, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->mIconView:Landroid/widget/ImageView;

    #@6c
    invoke-virtual {v10, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    #@6f
    .line 441
    iget-object v10, p0, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->mIconView:Landroid/widget/ImageView;

    #@71
    invoke-virtual {v10, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    #@74
    .line 447
    :cond_74
    :goto_74
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@77
    move-result v10

    #@78
    if-nez v10, :cond_d3

    #@7a
    const/4 v2, 0x1

    #@7b
    .line 448
    .local v2, hasText:Z
    :goto_7b
    if-eqz v2, :cond_d5

    #@7d
    .line 449
    iget-object v10, p0, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->mTextView:Landroid/widget/TextView;

    #@7f
    if-nez v10, :cond_9c

    #@81
    .line 450
    new-instance v8, Landroid/widget/TextView;

    #@83
    invoke-virtual {p0}, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->getContext()Landroid/content/Context;

    #@86
    move-result-object v10

    #@87
    const v11, 0x10102f5

    #@8a
    invoke-direct {v8, v10, v12, v11}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@8d
    .line 452
    .local v8, textView:Landroid/widget/TextView;
    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    #@8f
    invoke-direct {v5, v13, v13}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    #@92
    .line 454
    .restart local v5       #lp:Landroid/widget/LinearLayout$LayoutParams;
    iput v14, v5, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    #@94
    .line 455
    invoke-virtual {v8, v5}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@97
    .line 456
    invoke-virtual {p0, v8}, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->addView(Landroid/view/View;)V

    #@9a
    .line 457
    iput-object v8, p0, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->mTextView:Landroid/widget/TextView;

    #@9c
    .line 459
    .end local v5           #lp:Landroid/widget/LinearLayout$LayoutParams;
    .end local v8           #textView:Landroid/widget/TextView;
    :cond_9c
    iget-object v10, p0, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->mTextView:Landroid/widget/TextView;

    #@9e
    invoke-virtual {v10, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@a1
    .line 460
    iget-object v10, p0, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->mTextView:Landroid/widget/TextView;

    #@a3
    invoke-virtual {v10, v9}, Landroid/widget/TextView;->setVisibility(I)V

    #@a6
    .line 466
    :cond_a6
    :goto_a6
    iget-object v10, p0, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->mIconView:Landroid/widget/ImageView;

    #@a8
    if-eqz v10, :cond_b3

    #@aa
    .line 467
    iget-object v10, p0, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->mIconView:Landroid/widget/ImageView;

    #@ac
    invoke-virtual {v6}, Landroid/app/ActionBar$Tab;->getContentDescription()Ljava/lang/CharSequence;

    #@af
    move-result-object v11

    #@b0
    invoke-virtual {v10, v11}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    #@b3
    .line 470
    :cond_b3
    if-nez v2, :cond_e4

    #@b5
    invoke-virtual {v6}, Landroid/app/ActionBar$Tab;->getContentDescription()Ljava/lang/CharSequence;

    #@b8
    move-result-object v10

    #@b9
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@bc
    move-result v10

    #@bd
    if-nez v10, :cond_e4

    #@bf
    .line 471
    invoke-virtual {p0, p0}, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    #@c2
    goto/16 :goto_38

    #@c4
    .line 442
    .end local v2           #hasText:Z
    :cond_c4
    iget-object v10, p0, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->mIconView:Landroid/widget/ImageView;

    #@c6
    if-eqz v10, :cond_74

    #@c8
    .line 443
    iget-object v10, p0, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->mIconView:Landroid/widget/ImageView;

    #@ca
    invoke-virtual {v10, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    #@cd
    .line 444
    iget-object v10, p0, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->mIconView:Landroid/widget/ImageView;

    #@cf
    invoke-virtual {v10, v12}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    #@d2
    goto :goto_74

    #@d3
    :cond_d3
    move v2, v9

    #@d4
    .line 447
    goto :goto_7b

    #@d5
    .line 461
    .restart local v2       #hasText:Z
    :cond_d5
    iget-object v10, p0, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->mTextView:Landroid/widget/TextView;

    #@d7
    if-eqz v10, :cond_a6

    #@d9
    .line 462
    iget-object v10, p0, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->mTextView:Landroid/widget/TextView;

    #@db
    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setVisibility(I)V

    #@de
    .line 463
    iget-object v10, p0, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->mTextView:Landroid/widget/TextView;

    #@e0
    invoke-virtual {v10, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@e3
    goto :goto_a6

    #@e4
    .line 473
    :cond_e4
    invoke-virtual {p0, v12}, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    #@e7
    .line 474
    invoke-virtual {p0, v9}, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->setLongClickable(Z)V

    #@ea
    goto/16 :goto_38
.end method
