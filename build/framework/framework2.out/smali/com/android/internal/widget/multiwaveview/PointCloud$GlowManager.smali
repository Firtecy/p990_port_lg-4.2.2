.class public Lcom/android/internal/widget/multiwaveview/PointCloud$GlowManager;
.super Ljava/lang/Object;
.source "PointCloud.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/widget/multiwaveview/PointCloud;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "GlowManager"
.end annotation


# instance fields
.field private alpha:F

.field private radius:F

.field final synthetic this$0:Lcom/android/internal/widget/multiwaveview/PointCloud;

.field private x:F

.field private y:F


# direct methods
.method public constructor <init>(Lcom/android/internal/widget/multiwaveview/PointCloud;)V
    .registers 3
    .parameter

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 67
    iput-object p1, p0, Lcom/android/internal/widget/multiwaveview/PointCloud$GlowManager;->this$0:Lcom/android/internal/widget/multiwaveview/PointCloud;

    #@3
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 70
    iput v0, p0, Lcom/android/internal/widget/multiwaveview/PointCloud$GlowManager;->radius:F

    #@8
    .line 71
    iput v0, p0, Lcom/android/internal/widget/multiwaveview/PointCloud$GlowManager;->alpha:F

    #@a
    return-void
.end method

.method static synthetic access$000(Lcom/android/internal/widget/multiwaveview/PointCloud$GlowManager;)F
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 67
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/PointCloud$GlowManager;->x:F

    #@2
    return v0
.end method

.method static synthetic access$100(Lcom/android/internal/widget/multiwaveview/PointCloud$GlowManager;)F
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 67
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/PointCloud$GlowManager;->y:F

    #@2
    return v0
.end method

.method static synthetic access$200(Lcom/android/internal/widget/multiwaveview/PointCloud$GlowManager;)F
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 67
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/PointCloud$GlowManager;->radius:F

    #@2
    return v0
.end method

.method static synthetic access$300(Lcom/android/internal/widget/multiwaveview/PointCloud$GlowManager;)F
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 67
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/PointCloud$GlowManager;->alpha:F

    #@2
    return v0
.end method


# virtual methods
.method public getAlpha()F
    .registers 2

    #@0
    .prologue
    .line 94
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/PointCloud$GlowManager;->alpha:F

    #@2
    return v0
.end method

.method public getRadius()F
    .registers 2

    #@0
    .prologue
    .line 102
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/PointCloud$GlowManager;->radius:F

    #@2
    return v0
.end method

.method public getX()F
    .registers 2

    #@0
    .prologue
    .line 78
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/PointCloud$GlowManager;->x:F

    #@2
    return v0
.end method

.method public getY()F
    .registers 2

    #@0
    .prologue
    .line 86
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/PointCloud$GlowManager;->y:F

    #@2
    return v0
.end method

.method public setAlpha(F)V
    .registers 2
    .parameter "a"

    #@0
    .prologue
    .line 90
    iput p1, p0, Lcom/android/internal/widget/multiwaveview/PointCloud$GlowManager;->alpha:F

    #@2
    .line 91
    return-void
.end method

.method public setRadius(F)V
    .registers 2
    .parameter "r"

    #@0
    .prologue
    .line 98
    iput p1, p0, Lcom/android/internal/widget/multiwaveview/PointCloud$GlowManager;->radius:F

    #@2
    .line 99
    return-void
.end method

.method public setX(F)V
    .registers 2
    .parameter "x1"

    #@0
    .prologue
    .line 74
    iput p1, p0, Lcom/android/internal/widget/multiwaveview/PointCloud$GlowManager;->x:F

    #@2
    .line 75
    return-void
.end method

.method public setY(F)V
    .registers 2
    .parameter "y1"

    #@0
    .prologue
    .line 82
    iput p1, p0, Lcom/android/internal/widget/multiwaveview/PointCloud$GlowManager;->y:F

    #@2
    .line 83
    return-void
.end method
