.class public Lcom/android/internal/widget/EditableInputConnection;
.super Landroid/view/inputmethod/BaseInputConnection;
.source "EditableInputConnection.java"


# static fields
.field private static final DEBUG:Z = false

.field private static final TAG:Ljava/lang/String; = "EditableInputConnection"


# instance fields
.field private mBatchEditNesting:I

.field private final mTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/widget/TextView;)V
    .registers 3
    .parameter "textview"

    #@0
    .prologue
    .line 44
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, p1, v0}, Landroid/view/inputmethod/BaseInputConnection;-><init>(Landroid/view/View;Z)V

    #@4
    .line 45
    iput-object p1, p0, Lcom/android/internal/widget/EditableInputConnection;->mTextView:Landroid/widget/TextView;

    #@6
    .line 46
    return-void
.end method


# virtual methods
.method public beginBatchEdit()Z
    .registers 2

    #@0
    .prologue
    .line 59
    monitor-enter p0

    #@1
    .line 60
    :try_start_1
    iget v0, p0, Lcom/android/internal/widget/EditableInputConnection;->mBatchEditNesting:I

    #@3
    if-ltz v0, :cond_13

    #@5
    .line 61
    iget-object v0, p0, Lcom/android/internal/widget/EditableInputConnection;->mTextView:Landroid/widget/TextView;

    #@7
    invoke-virtual {v0}, Landroid/widget/TextView;->beginBatchEdit()V

    #@a
    .line 62
    iget v0, p0, Lcom/android/internal/widget/EditableInputConnection;->mBatchEditNesting:I

    #@c
    add-int/lit8 v0, v0, 0x1

    #@e
    iput v0, p0, Lcom/android/internal/widget/EditableInputConnection;->mBatchEditNesting:I

    #@10
    .line 63
    const/4 v0, 0x1

    #@11
    monitor-exit p0

    #@12
    .line 66
    :goto_12
    return v0

    #@13
    .line 65
    :cond_13
    monitor-exit p0

    #@14
    .line 66
    const/4 v0, 0x0

    #@15
    goto :goto_12

    #@16
    .line 65
    :catchall_16
    move-exception v0

    #@17
    monitor-exit p0
    :try_end_18
    .catchall {:try_start_1 .. :try_end_18} :catchall_16

    #@18
    throw v0
.end method

.method public clearMetaKeyStates(I)Z
    .registers 5
    .parameter "states"

    #@0
    .prologue
    .line 100
    invoke-virtual {p0}, Lcom/android/internal/widget/EditableInputConnection;->getEditable()Landroid/text/Editable;

    #@3
    move-result-object v0

    #@4
    .line 101
    .local v0, content:Landroid/text/Editable;
    if-nez v0, :cond_8

    #@6
    const/4 v2, 0x0

    #@7
    .line 111
    :goto_7
    return v2

    #@8
    .line 102
    :cond_8
    iget-object v2, p0, Lcom/android/internal/widget/EditableInputConnection;->mTextView:Landroid/widget/TextView;

    #@a
    invoke-virtual {v2}, Landroid/widget/TextView;->getKeyListener()Landroid/text/method/KeyListener;

    #@d
    move-result-object v1

    #@e
    .line 103
    .local v1, kl:Landroid/text/method/KeyListener;
    if-eqz v1, :cond_15

    #@10
    .line 105
    :try_start_10
    iget-object v2, p0, Lcom/android/internal/widget/EditableInputConnection;->mTextView:Landroid/widget/TextView;

    #@12
    invoke-interface {v1, v2, v0, p1}, Landroid/text/method/KeyListener;->clearMetaKeyState(Landroid/view/View;Landroid/text/Editable;I)V
    :try_end_15
    .catch Ljava/lang/AbstractMethodError; {:try_start_10 .. :try_end_15} :catch_17

    #@15
    .line 111
    :cond_15
    :goto_15
    const/4 v2, 0x1

    #@16
    goto :goto_7

    #@17
    .line 106
    :catch_17
    move-exception v2

    #@18
    goto :goto_15
.end method

.method public commitCompletion(Landroid/view/inputmethod/CompletionInfo;)Z
    .registers 3
    .parameter "text"

    #@0
    .prologue
    .line 117
    iget-object v0, p0, Lcom/android/internal/widget/EditableInputConnection;->mTextView:Landroid/widget/TextView;

    #@2
    invoke-virtual {v0}, Landroid/widget/TextView;->beginBatchEdit()V

    #@5
    .line 118
    iget-object v0, p0, Lcom/android/internal/widget/EditableInputConnection;->mTextView:Landroid/widget/TextView;

    #@7
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->onCommitCompletion(Landroid/view/inputmethod/CompletionInfo;)V

    #@a
    .line 119
    iget-object v0, p0, Lcom/android/internal/widget/EditableInputConnection;->mTextView:Landroid/widget/TextView;

    #@c
    invoke-virtual {v0}, Landroid/widget/TextView;->endBatchEdit()V

    #@f
    .line 120
    const/4 v0, 0x1

    #@10
    return v0
.end method

.method public commitCorrection(Landroid/view/inputmethod/CorrectionInfo;)Z
    .registers 3
    .parameter "correctionInfo"

    #@0
    .prologue
    .line 129
    iget-object v0, p0, Lcom/android/internal/widget/EditableInputConnection;->mTextView:Landroid/widget/TextView;

    #@2
    invoke-virtual {v0}, Landroid/widget/TextView;->beginBatchEdit()V

    #@5
    .line 130
    iget-object v0, p0, Lcom/android/internal/widget/EditableInputConnection;->mTextView:Landroid/widget/TextView;

    #@7
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->onCommitCorrection(Landroid/view/inputmethod/CorrectionInfo;)V

    #@a
    .line 131
    iget-object v0, p0, Lcom/android/internal/widget/EditableInputConnection;->mTextView:Landroid/widget/TextView;

    #@c
    invoke-virtual {v0}, Landroid/widget/TextView;->endBatchEdit()V

    #@f
    .line 132
    const/4 v0, 0x1

    #@10
    return v0
.end method

.method public commitText(Ljava/lang/CharSequence;I)Z
    .registers 9
    .parameter "text"
    .parameter "newCursorPosition"

    #@0
    .prologue
    .line 173
    iget-object v3, p0, Lcom/android/internal/widget/EditableInputConnection;->mTextView:Landroid/widget/TextView;

    #@2
    if-nez v3, :cond_9

    #@4
    .line 174
    invoke-super {p0, p1, p2}, Landroid/view/inputmethod/BaseInputConnection;->commitText(Ljava/lang/CharSequence;I)Z

    #@7
    move-result v2

    #@8
    .line 186
    :goto_8
    return v2

    #@9
    .line 176
    :cond_9
    instance-of v3, p1, Landroid/text/Spanned;

    #@b
    if-eqz v3, :cond_22

    #@d
    move-object v0, p1

    #@e
    .line 177
    check-cast v0, Landroid/text/Spanned;

    #@10
    .line 178
    .local v0, spanned:Landroid/text/Spanned;
    const/4 v3, 0x0

    #@11
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@14
    move-result v4

    #@15
    const-class v5, Landroid/text/style/SuggestionSpan;

    #@17
    invoke-interface {v0, v3, v4, v5}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@1a
    move-result-object v1

    #@1b
    check-cast v1, [Landroid/text/style/SuggestionSpan;

    #@1d
    .line 179
    .local v1, spans:[Landroid/text/style/SuggestionSpan;
    iget-object v3, p0, Lcom/android/internal/widget/EditableInputConnection;->mIMM:Landroid/view/inputmethod/InputMethodManager;

    #@1f
    invoke-virtual {v3, v1}, Landroid/view/inputmethod/InputMethodManager;->registerSuggestionSpansForNotification([Landroid/text/style/SuggestionSpan;)V

    #@22
    .line 182
    .end local v0           #spanned:Landroid/text/Spanned;
    .end local v1           #spans:[Landroid/text/style/SuggestionSpan;
    :cond_22
    iget-object v3, p0, Lcom/android/internal/widget/EditableInputConnection;->mTextView:Landroid/widget/TextView;

    #@24
    invoke-virtual {v3}, Landroid/widget/TextView;->resetErrorChangedFlag()V

    #@27
    .line 183
    invoke-super {p0, p1, p2}, Landroid/view/inputmethod/BaseInputConnection;->commitText(Ljava/lang/CharSequence;I)Z

    #@2a
    move-result v2

    #@2b
    .line 184
    .local v2, success:Z
    iget-object v3, p0, Lcom/android/internal/widget/EditableInputConnection;->mTextView:Landroid/widget/TextView;

    #@2d
    invoke-virtual {v3}, Landroid/widget/TextView;->hideErrorIfUnchanged()V

    #@30
    goto :goto_8
.end method

.method public endBatchEdit()Z
    .registers 2

    #@0
    .prologue
    .line 71
    monitor-enter p0

    #@1
    .line 72
    :try_start_1
    iget v0, p0, Lcom/android/internal/widget/EditableInputConnection;->mBatchEditNesting:I

    #@3
    if-lez v0, :cond_13

    #@5
    .line 77
    iget-object v0, p0, Lcom/android/internal/widget/EditableInputConnection;->mTextView:Landroid/widget/TextView;

    #@7
    invoke-virtual {v0}, Landroid/widget/TextView;->endBatchEdit()V

    #@a
    .line 78
    iget v0, p0, Lcom/android/internal/widget/EditableInputConnection;->mBatchEditNesting:I

    #@c
    add-int/lit8 v0, v0, -0x1

    #@e
    iput v0, p0, Lcom/android/internal/widget/EditableInputConnection;->mBatchEditNesting:I

    #@10
    .line 79
    const/4 v0, 0x1

    #@11
    monitor-exit p0

    #@12
    .line 82
    :goto_12
    return v0

    #@13
    .line 81
    :cond_13
    monitor-exit p0

    #@14
    .line 82
    const/4 v0, 0x0

    #@15
    goto :goto_12

    #@16
    .line 81
    :catchall_16
    move-exception v0

    #@17
    monitor-exit p0
    :try_end_18
    .catchall {:try_start_1 .. :try_end_18} :catchall_16

    #@18
    throw v0
.end method

.method public getEditable()Landroid/text/Editable;
    .registers 3

    #@0
    .prologue
    .line 50
    iget-object v0, p0, Lcom/android/internal/widget/EditableInputConnection;->mTextView:Landroid/widget/TextView;

    #@2
    .line 51
    .local v0, tv:Landroid/widget/TextView;
    if-eqz v0, :cond_9

    #@4
    .line 52
    invoke-virtual {v0}, Landroid/widget/TextView;->getEditableText()Landroid/text/Editable;

    #@7
    move-result-object v1

    #@8
    .line 54
    :goto_8
    return-object v1

    #@9
    :cond_9
    const/4 v1, 0x0

    #@a
    goto :goto_8
.end method

.method public getExtractedText(Landroid/view/inputmethod/ExtractedTextRequest;I)Landroid/view/inputmethod/ExtractedText;
    .registers 5
    .parameter "request"
    .parameter "flags"

    #@0
    .prologue
    .line 153
    iget-object v1, p0, Lcom/android/internal/widget/EditableInputConnection;->mTextView:Landroid/widget/TextView;

    #@2
    if-eqz v1, :cond_1b

    #@4
    .line 154
    new-instance v0, Landroid/view/inputmethod/ExtractedText;

    #@6
    invoke-direct {v0}, Landroid/view/inputmethod/ExtractedText;-><init>()V

    #@9
    .line 155
    .local v0, et:Landroid/view/inputmethod/ExtractedText;
    iget-object v1, p0, Lcom/android/internal/widget/EditableInputConnection;->mTextView:Landroid/widget/TextView;

    #@b
    invoke-virtual {v1, p1, v0}, Landroid/widget/TextView;->extractText(Landroid/view/inputmethod/ExtractedTextRequest;Landroid/view/inputmethod/ExtractedText;)Z

    #@e
    move-result v1

    #@f
    if-eqz v1, :cond_1b

    #@11
    .line 156
    and-int/lit8 v1, p2, 0x1

    #@13
    if-eqz v1, :cond_1a

    #@15
    .line 157
    iget-object v1, p0, Lcom/android/internal/widget/EditableInputConnection;->mTextView:Landroid/widget/TextView;

    #@17
    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setExtracting(Landroid/view/inputmethod/ExtractedTextRequest;)V

    #@1a
    .line 162
    .end local v0           #et:Landroid/view/inputmethod/ExtractedText;
    :cond_1a
    :goto_1a
    return-object v0

    #@1b
    :cond_1b
    const/4 v0, 0x0

    #@1c
    goto :goto_1a
.end method

.method public performContextMenuAction(I)Z
    .registers 3
    .parameter "id"

    #@0
    .prologue
    .line 145
    iget-object v0, p0, Lcom/android/internal/widget/EditableInputConnection;->mTextView:Landroid/widget/TextView;

    #@2
    invoke-virtual {v0}, Landroid/widget/TextView;->beginBatchEdit()V

    #@5
    .line 146
    iget-object v0, p0, Lcom/android/internal/widget/EditableInputConnection;->mTextView:Landroid/widget/TextView;

    #@7
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->onTextContextMenuItem(I)Z

    #@a
    .line 147
    iget-object v0, p0, Lcom/android/internal/widget/EditableInputConnection;->mTextView:Landroid/widget/TextView;

    #@c
    invoke-virtual {v0}, Landroid/widget/TextView;->endBatchEdit()V

    #@f
    .line 148
    const/4 v0, 0x1

    #@10
    return v0
.end method

.method public performEditorAction(I)Z
    .registers 3
    .parameter "actionCode"

    #@0
    .prologue
    .line 138
    iget-object v0, p0, Lcom/android/internal/widget/EditableInputConnection;->mTextView:Landroid/widget/TextView;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->onEditorAction(I)V

    #@5
    .line 139
    const/4 v0, 0x1

    #@6
    return v0
.end method

.method public performPrivateCommand(Ljava/lang/String;Landroid/os/Bundle;)Z
    .registers 4
    .parameter "action"
    .parameter "data"

    #@0
    .prologue
    .line 167
    iget-object v0, p0, Lcom/android/internal/widget/EditableInputConnection;->mTextView:Landroid/widget/TextView;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/widget/TextView;->onPrivateIMECommand(Ljava/lang/String;Landroid/os/Bundle;)Z

    #@5
    .line 168
    const/4 v0, 0x1

    #@6
    return v0
.end method

.method protected reportFinish()V
    .registers 2

    #@0
    .prologue
    .line 87
    invoke-super {p0}, Landroid/view/inputmethod/BaseInputConnection;->reportFinish()V

    #@3
    .line 89
    monitor-enter p0

    #@4
    .line 90
    :goto_4
    :try_start_4
    iget v0, p0, Lcom/android/internal/widget/EditableInputConnection;->mBatchEditNesting:I

    #@6
    if-lez v0, :cond_f

    #@8
    .line 91
    invoke-virtual {p0}, Lcom/android/internal/widget/EditableInputConnection;->endBatchEdit()Z

    #@b
    goto :goto_4

    #@c
    .line 95
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0
    :try_end_e
    .catchall {:try_start_4 .. :try_end_e} :catchall_c

    #@e
    throw v0

    #@f
    .line 94
    :cond_f
    const/4 v0, -0x1

    #@10
    :try_start_10
    iput v0, p0, Lcom/android/internal/widget/EditableInputConnection;->mBatchEditNesting:I

    #@12
    .line 95
    monitor-exit p0
    :try_end_13
    .catchall {:try_start_10 .. :try_end_13} :catchall_c

    #@13
    .line 96
    return-void
.end method

.method public setComposingText(Ljava/lang/CharSequence;I)Z
    .registers 5
    .parameter "text"
    .parameter "newCursorPosition"

    #@0
    .prologue
    .line 191
    iget-object v1, p0, Lcom/android/internal/widget/EditableInputConnection;->mTextView:Landroid/widget/TextView;

    #@2
    if-nez v1, :cond_9

    #@4
    .line 192
    invoke-super {p0, p1, p2}, Landroid/view/inputmethod/BaseInputConnection;->setComposingText(Ljava/lang/CharSequence;I)Z

    #@7
    move-result v0

    #@8
    .line 198
    :goto_8
    return v0

    #@9
    .line 194
    :cond_9
    iget-object v1, p0, Lcom/android/internal/widget/EditableInputConnection;->mTextView:Landroid/widget/TextView;

    #@b
    invoke-virtual {v1}, Landroid/widget/TextView;->resetErrorChangedFlag()V

    #@e
    .line 195
    invoke-super {p0, p1, p2}, Landroid/view/inputmethod/BaseInputConnection;->setComposingText(Ljava/lang/CharSequence;I)Z

    #@11
    move-result v0

    #@12
    .line 196
    .local v0, success:Z
    iget-object v1, p0, Lcom/android/internal/widget/EditableInputConnection;->mTextView:Landroid/widget/TextView;

    #@14
    invoke-virtual {v1}, Landroid/widget/TextView;->hideErrorIfUnchanged()V

    #@17
    goto :goto_8
.end method
