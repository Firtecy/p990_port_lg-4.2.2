.class Lcom/android/internal/widget/WaveView$2;
.super Ljava/lang/Object;
.source "WaveView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/widget/WaveView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/widget/WaveView;


# direct methods
.method constructor <init>(Lcom/android/internal/widget/WaveView;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 431
    iput-object p1, p0, Lcom/android/internal/widget/WaveView$2;->this$0:Lcom/android/internal/widget/WaveView;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public run()V
    .registers 14

    #@0
    .prologue
    .line 433
    iget-object v1, p0, Lcom/android/internal/widget/WaveView$2;->this$0:Lcom/android/internal/widget/WaveView;

    #@2
    invoke-static {v1}, Lcom/android/internal/widget/WaveView;->access$100(Lcom/android/internal/widget/WaveView;)F

    #@5
    move-result v1

    #@6
    iget-object v2, p0, Lcom/android/internal/widget/WaveView$2;->this$0:Lcom/android/internal/widget/WaveView;

    #@8
    invoke-static {v2}, Lcom/android/internal/widget/WaveView;->access$200(Lcom/android/internal/widget/WaveView;)F

    #@b
    move-result v2

    #@c
    sub-float/2addr v1, v2

    #@d
    float-to-double v8, v1

    #@e
    .line 434
    .local v8, distX:D
    iget-object v1, p0, Lcom/android/internal/widget/WaveView$2;->this$0:Lcom/android/internal/widget/WaveView;

    #@10
    invoke-static {v1}, Lcom/android/internal/widget/WaveView;->access$300(Lcom/android/internal/widget/WaveView;)F

    #@13
    move-result v1

    #@14
    iget-object v2, p0, Lcom/android/internal/widget/WaveView$2;->this$0:Lcom/android/internal/widget/WaveView;

    #@16
    invoke-static {v2}, Lcom/android/internal/widget/WaveView;->access$400(Lcom/android/internal/widget/WaveView;)F

    #@19
    move-result v2

    #@1a
    sub-float/2addr v1, v2

    #@1b
    float-to-double v10, v1

    #@1c
    .line 435
    .local v10, distY:D
    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->hypot(DD)D

    #@1f
    move-result-wide v1

    #@20
    invoke-static {v1, v2}, Ljava/lang/Math;->ceil(D)D

    #@23
    move-result-wide v1

    #@24
    double-to-int v12, v1

    #@25
    .line 436
    .local v12, dragDistance:I
    iget-object v1, p0, Lcom/android/internal/widget/WaveView$2;->this$0:Lcom/android/internal/widget/WaveView;

    #@27
    invoke-static {v1}, Lcom/android/internal/widget/WaveView;->access$000(Lcom/android/internal/widget/WaveView;)I

    #@2a
    move-result v1

    #@2b
    const/4 v2, 0x3

    #@2c
    if-ne v1, v2, :cond_101

    #@2e
    iget-object v1, p0, Lcom/android/internal/widget/WaveView$2;->this$0:Lcom/android/internal/widget/WaveView;

    #@30
    invoke-static {v1}, Lcom/android/internal/widget/WaveView;->access$500(Lcom/android/internal/widget/WaveView;)I

    #@33
    move-result v1

    #@34
    if-ge v12, v1, :cond_101

    #@36
    iget-object v1, p0, Lcom/android/internal/widget/WaveView$2;->this$0:Lcom/android/internal/widget/WaveView;

    #@38
    invoke-static {v1}, Lcom/android/internal/widget/WaveView;->access$600(Lcom/android/internal/widget/WaveView;)J

    #@3b
    move-result-wide v1

    #@3c
    const-wide/16 v3, 0x64

    #@3e
    cmp-long v1, v1, v3

    #@40
    if-ltz v1, :cond_101

    #@42
    .line 438
    iget-object v1, p0, Lcom/android/internal/widget/WaveView$2;->this$0:Lcom/android/internal/widget/WaveView;

    #@44
    const-wide/16 v2, 0x7d0

    #@46
    iget-object v4, p0, Lcom/android/internal/widget/WaveView$2;->this$0:Lcom/android/internal/widget/WaveView;

    #@48
    invoke-static {v4}, Lcom/android/internal/widget/WaveView;->access$600(Lcom/android/internal/widget/WaveView;)J

    #@4b
    move-result-wide v4

    #@4c
    const-wide/16 v6, 0xf

    #@4e
    add-long/2addr v4, v6

    #@4f
    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    #@52
    move-result-wide v2

    #@53
    invoke-static {v1, v2, v3}, Lcom/android/internal/widget/WaveView;->access$602(Lcom/android/internal/widget/WaveView;J)J

    #@56
    .line 440
    iget-object v1, p0, Lcom/android/internal/widget/WaveView$2;->this$0:Lcom/android/internal/widget/WaveView;

    #@58
    invoke-static {v1}, Lcom/android/internal/widget/WaveView;->access$800(Lcom/android/internal/widget/WaveView;)Ljava/util/ArrayList;

    #@5b
    move-result-object v1

    #@5c
    iget-object v2, p0, Lcom/android/internal/widget/WaveView$2;->this$0:Lcom/android/internal/widget/WaveView;

    #@5e
    invoke-static {v2}, Lcom/android/internal/widget/WaveView;->access$700(Lcom/android/internal/widget/WaveView;)I

    #@61
    move-result v2

    #@62
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@65
    move-result-object v0

    #@66
    check-cast v0, Lcom/android/internal/widget/DrawableHolder;

    #@68
    .line 441
    .local v0, wave:Lcom/android/internal/widget/DrawableHolder;
    const/4 v1, 0x0

    #@69
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/DrawableHolder;->setAlpha(F)V

    #@6c
    .line 442
    const v1, 0x3e4ccccd

    #@6f
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/DrawableHolder;->setScaleX(F)V

    #@72
    .line 443
    const v1, 0x3e4ccccd

    #@75
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/DrawableHolder;->setScaleY(F)V

    #@78
    .line 444
    iget-object v1, p0, Lcom/android/internal/widget/WaveView$2;->this$0:Lcom/android/internal/widget/WaveView;

    #@7a
    invoke-static {v1}, Lcom/android/internal/widget/WaveView;->access$100(Lcom/android/internal/widget/WaveView;)F

    #@7d
    move-result v1

    #@7e
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/DrawableHolder;->setX(F)V

    #@81
    .line 445
    iget-object v1, p0, Lcom/android/internal/widget/WaveView$2;->this$0:Lcom/android/internal/widget/WaveView;

    #@83
    invoke-static {v1}, Lcom/android/internal/widget/WaveView;->access$300(Lcom/android/internal/widget/WaveView;)F

    #@86
    move-result v1

    #@87
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/DrawableHolder;->setY(F)V

    #@8a
    .line 447
    const-wide/16 v1, 0x7d0

    #@8c
    const-wide/16 v3, 0x0

    #@8e
    const-string v5, "x"

    #@90
    iget-object v6, p0, Lcom/android/internal/widget/WaveView$2;->this$0:Lcom/android/internal/widget/WaveView;

    #@92
    invoke-static {v6}, Lcom/android/internal/widget/WaveView;->access$200(Lcom/android/internal/widget/WaveView;)F

    #@95
    move-result v6

    #@96
    const/4 v7, 0x1

    #@97
    invoke-virtual/range {v0 .. v7}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@9a
    .line 448
    const-wide/16 v1, 0x7d0

    #@9c
    const-wide/16 v3, 0x0

    #@9e
    const-string v5, "y"

    #@a0
    iget-object v6, p0, Lcom/android/internal/widget/WaveView$2;->this$0:Lcom/android/internal/widget/WaveView;

    #@a2
    invoke-static {v6}, Lcom/android/internal/widget/WaveView;->access$400(Lcom/android/internal/widget/WaveView;)F

    #@a5
    move-result v6

    #@a6
    const/4 v7, 0x1

    #@a7
    invoke-virtual/range {v0 .. v7}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@aa
    .line 449
    const-wide/16 v1, 0x535

    #@ac
    const-wide/16 v3, 0x0

    #@ae
    const-string v5, "alpha"

    #@b0
    const/high16 v6, 0x3f80

    #@b2
    const/4 v7, 0x1

    #@b3
    invoke-virtual/range {v0 .. v7}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@b6
    .line 450
    const-wide/16 v1, 0x7d0

    #@b8
    const-wide/16 v3, 0x0

    #@ba
    const-string v5, "scaleX"

    #@bc
    const/high16 v6, 0x3f80

    #@be
    const/4 v7, 0x1

    #@bf
    invoke-virtual/range {v0 .. v7}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@c2
    .line 451
    const-wide/16 v1, 0x7d0

    #@c4
    const-wide/16 v3, 0x0

    #@c6
    const-string v5, "scaleY"

    #@c8
    const/high16 v6, 0x3f80

    #@ca
    const/4 v7, 0x1

    #@cb
    invoke-virtual/range {v0 .. v7}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@ce
    .line 453
    const-wide/16 v1, 0x3e8

    #@d0
    const-wide/16 v3, 0x514

    #@d2
    const-string v5, "alpha"

    #@d4
    const/4 v6, 0x0

    #@d5
    const/4 v7, 0x0

    #@d6
    invoke-virtual/range {v0 .. v7}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@d9
    .line 454
    iget-object v1, p0, Lcom/android/internal/widget/WaveView$2;->this$0:Lcom/android/internal/widget/WaveView;

    #@db
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/DrawableHolder;->startAnimations(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    #@de
    .line 456
    iget-object v1, p0, Lcom/android/internal/widget/WaveView$2;->this$0:Lcom/android/internal/widget/WaveView;

    #@e0
    iget-object v2, p0, Lcom/android/internal/widget/WaveView$2;->this$0:Lcom/android/internal/widget/WaveView;

    #@e2
    invoke-static {v2}, Lcom/android/internal/widget/WaveView;->access$700(Lcom/android/internal/widget/WaveView;)I

    #@e5
    move-result v2

    #@e6
    add-int/lit8 v2, v2, 0x1

    #@e8
    iget-object v3, p0, Lcom/android/internal/widget/WaveView$2;->this$0:Lcom/android/internal/widget/WaveView;

    #@ea
    invoke-static {v3}, Lcom/android/internal/widget/WaveView;->access$900(Lcom/android/internal/widget/WaveView;)I

    #@ed
    move-result v3

    #@ee
    rem-int/2addr v2, v3

    #@ef
    invoke-static {v1, v2}, Lcom/android/internal/widget/WaveView;->access$702(Lcom/android/internal/widget/WaveView;I)I

    #@f2
    .line 461
    .end local v0           #wave:Lcom/android/internal/widget/DrawableHolder;
    :goto_f2
    iget-object v1, p0, Lcom/android/internal/widget/WaveView$2;->this$0:Lcom/android/internal/widget/WaveView;

    #@f4
    invoke-static {v1}, Lcom/android/internal/widget/WaveView;->access$1000(Lcom/android/internal/widget/WaveView;)Z

    #@f7
    move-result v1

    #@f8
    if-eqz v1, :cond_109

    #@fa
    .line 463
    iget-object v1, p0, Lcom/android/internal/widget/WaveView$2;->this$0:Lcom/android/internal/widget/WaveView;

    #@fc
    const/4 v2, 0x0

    #@fd
    invoke-static {v1, v2}, Lcom/android/internal/widget/WaveView;->access$1102(Lcom/android/internal/widget/WaveView;Z)Z

    #@100
    .line 467
    :goto_100
    return-void

    #@101
    .line 459
    :cond_101
    iget-object v1, p0, Lcom/android/internal/widget/WaveView$2;->this$0:Lcom/android/internal/widget/WaveView;

    #@103
    const-wide/16 v2, 0xc

    #@105
    invoke-static {v1, v2, v3}, Lcom/android/internal/widget/WaveView;->access$614(Lcom/android/internal/widget/WaveView;J)J

    #@108
    goto :goto_f2

    #@109
    .line 465
    :cond_109
    iget-object v1, p0, Lcom/android/internal/widget/WaveView$2;->this$0:Lcom/android/internal/widget/WaveView;

    #@10b
    iget-object v2, p0, Lcom/android/internal/widget/WaveView$2;->this$0:Lcom/android/internal/widget/WaveView;

    #@10d
    invoke-static {v2}, Lcom/android/internal/widget/WaveView;->access$1200(Lcom/android/internal/widget/WaveView;)Ljava/lang/Runnable;

    #@110
    move-result-object v2

    #@111
    iget-object v3, p0, Lcom/android/internal/widget/WaveView$2;->this$0:Lcom/android/internal/widget/WaveView;

    #@113
    invoke-static {v3}, Lcom/android/internal/widget/WaveView;->access$600(Lcom/android/internal/widget/WaveView;)J

    #@116
    move-result-wide v3

    #@117
    invoke-virtual {v1, v2, v3, v4}, Lcom/android/internal/widget/WaveView;->postDelayed(Ljava/lang/Runnable;J)Z

    #@11a
    goto :goto_100
.end method
