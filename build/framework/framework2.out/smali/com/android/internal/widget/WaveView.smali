.class public Lcom/android/internal/widget/WaveView;
.super Landroid/view/View;
.source "WaveView.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/widget/WaveView$OnTriggerListener;
    }
.end annotation


# static fields
.field private static final DBG:Z = false

.field private static final DELAY_INCREMENT:J = 0xfL

.field private static final DELAY_INCREMENT2:J = 0xcL

.field private static final DURATION:J = 0x12cL

.field private static final FINAL_DELAY:J = 0xc8L

.field private static final FINAL_DURATION:J = 0xc8L

.field private static final GRAB_HANDLE_RADIUS_SCALE_ACCESSIBILITY_DISABLED:F = 0.5f

.field private static final GRAB_HANDLE_RADIUS_SCALE_ACCESSIBILITY_ENABLED:F = 1.0f

.field private static final RESET_TIMEOUT:J = 0xbb8L

.field private static final RING_DELAY:J = 0x514L

.field private static final SHORT_DELAY:J = 0x64L

.field private static final STATE_ATTEMPTING:I = 0x3

.field private static final STATE_READY:I = 0x1

.field private static final STATE_RESET_LOCK:I = 0x0

.field private static final STATE_START_ATTEMPT:I = 0x2

.field private static final STATE_UNLOCK_ATTEMPT:I = 0x4

.field private static final STATE_UNLOCK_SUCCESS:I = 0x5

.field private static final TAG:Ljava/lang/String; = "WaveView"

.field private static final VIBRATE_LONG:J = 0x14L

.field private static final VIBRATE_SHORT:J = 0x14L

.field private static final WAVE_COUNT:I = 0x14

.field private static final WAVE_DELAY:J = 0x64L

.field private static final WAVE_DURATION:J = 0x7d0L


# instance fields
.field private final mAddWaveAction:Ljava/lang/Runnable;

.field private mCurrentWave:I

.field private mDrawables:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/widget/DrawableHolder;",
            ">;"
        }
    .end annotation
.end field

.field private mFingerDown:Z

.field private mFinishWaves:Z

.field private mGrabbedState:I

.field private mLightWaves:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/widget/DrawableHolder;",
            ">;"
        }
    .end annotation
.end field

.field private mLockCenterX:F

.field private mLockCenterY:F

.field private mLockState:I

.field private final mLockTimerActions:Ljava/lang/Runnable;

.field private mMouseX:F

.field private mMouseY:F

.field private mOnTriggerListener:Lcom/android/internal/widget/WaveView$OnTriggerListener;

.field private mRingRadius:F

.field private mSnapRadius:I

.field private mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

.field private mUnlockHalo:Lcom/android/internal/widget/DrawableHolder;

.field private mUnlockRing:Lcom/android/internal/widget/DrawableHolder;

.field private mVibrator:Landroid/os/Vibrator;

.field private mWaveCount:I

.field private mWaveTimerDelay:J

.field private mWavesRunning:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 107
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Lcom/android/internal/widget/WaveView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 108
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 7
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    const/16 v3, 0x14

    #@2
    const/4 v2, 0x0

    #@3
    .line 111
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@6
    .line 86
    new-instance v0, Ljava/util/ArrayList;

    #@8
    const/4 v1, 0x3

    #@9
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    #@c
    iput-object v0, p0, Lcom/android/internal/widget/WaveView;->mDrawables:Ljava/util/ArrayList;

    #@e
    .line 87
    new-instance v0, Ljava/util/ArrayList;

    #@10
    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    #@13
    iput-object v0, p0, Lcom/android/internal/widget/WaveView;->mLightWaves:Ljava/util/ArrayList;

    #@15
    .line 88
    iput-boolean v2, p0, Lcom/android/internal/widget/WaveView;->mFingerDown:Z

    #@17
    .line 89
    const/high16 v0, 0x4336

    #@19
    iput v0, p0, Lcom/android/internal/widget/WaveView;->mRingRadius:F

    #@1b
    .line 90
    const/16 v0, 0x88

    #@1d
    iput v0, p0, Lcom/android/internal/widget/WaveView;->mSnapRadius:I

    #@1f
    .line 91
    iput v3, p0, Lcom/android/internal/widget/WaveView;->mWaveCount:I

    #@21
    .line 92
    const-wide/16 v0, 0x64

    #@23
    iput-wide v0, p0, Lcom/android/internal/widget/WaveView;->mWaveTimerDelay:J

    #@25
    .line 93
    iput v2, p0, Lcom/android/internal/widget/WaveView;->mCurrentWave:I

    #@27
    .line 101
    iput v2, p0, Lcom/android/internal/widget/WaveView;->mLockState:I

    #@29
    .line 102
    iput v2, p0, Lcom/android/internal/widget/WaveView;->mGrabbedState:I

    #@2b
    .line 414
    new-instance v0, Lcom/android/internal/widget/WaveView$1;

    #@2d
    invoke-direct {v0, p0}, Lcom/android/internal/widget/WaveView$1;-><init>(Lcom/android/internal/widget/WaveView;)V

    #@30
    iput-object v0, p0, Lcom/android/internal/widget/WaveView;->mLockTimerActions:Ljava/lang/Runnable;

    #@32
    .line 431
    new-instance v0, Lcom/android/internal/widget/WaveView$2;

    #@34
    invoke-direct {v0, p0}, Lcom/android/internal/widget/WaveView$2;-><init>(Lcom/android/internal/widget/WaveView;)V

    #@37
    iput-object v0, p0, Lcom/android/internal/widget/WaveView;->mAddWaveAction:Ljava/lang/Runnable;

    #@39
    .line 117
    invoke-direct {p0}, Lcom/android/internal/widget/WaveView;->initDrawables()V

    #@3c
    .line 118
    return-void
.end method

.method static synthetic access$000(Lcom/android/internal/widget/WaveView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 45
    iget v0, p0, Lcom/android/internal/widget/WaveView;->mLockState:I

    #@2
    return v0
.end method

.method static synthetic access$002(Lcom/android/internal/widget/WaveView;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 45
    iput p1, p0, Lcom/android/internal/widget/WaveView;->mLockState:I

    #@2
    return p1
.end method

.method static synthetic access$100(Lcom/android/internal/widget/WaveView;)F
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 45
    iget v0, p0, Lcom/android/internal/widget/WaveView;->mMouseX:F

    #@2
    return v0
.end method

.method static synthetic access$1000(Lcom/android/internal/widget/WaveView;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/android/internal/widget/WaveView;->mFinishWaves:Z

    #@2
    return v0
.end method

.method static synthetic access$1102(Lcom/android/internal/widget/WaveView;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/android/internal/widget/WaveView;->mWavesRunning:Z

    #@2
    return p1
.end method

.method static synthetic access$1200(Lcom/android/internal/widget/WaveView;)Ljava/lang/Runnable;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 45
    iget-object v0, p0, Lcom/android/internal/widget/WaveView;->mAddWaveAction:Ljava/lang/Runnable;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Lcom/android/internal/widget/WaveView;)F
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 45
    iget v0, p0, Lcom/android/internal/widget/WaveView;->mLockCenterX:F

    #@2
    return v0
.end method

.method static synthetic access$300(Lcom/android/internal/widget/WaveView;)F
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 45
    iget v0, p0, Lcom/android/internal/widget/WaveView;->mMouseY:F

    #@2
    return v0
.end method

.method static synthetic access$400(Lcom/android/internal/widget/WaveView;)F
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 45
    iget v0, p0, Lcom/android/internal/widget/WaveView;->mLockCenterY:F

    #@2
    return v0
.end method

.method static synthetic access$500(Lcom/android/internal/widget/WaveView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 45
    iget v0, p0, Lcom/android/internal/widget/WaveView;->mSnapRadius:I

    #@2
    return v0
.end method

.method static synthetic access$600(Lcom/android/internal/widget/WaveView;)J
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 45
    iget-wide v0, p0, Lcom/android/internal/widget/WaveView;->mWaveTimerDelay:J

    #@2
    return-wide v0
.end method

.method static synthetic access$602(Lcom/android/internal/widget/WaveView;J)J
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 45
    iput-wide p1, p0, Lcom/android/internal/widget/WaveView;->mWaveTimerDelay:J

    #@2
    return-wide p1
.end method

.method static synthetic access$614(Lcom/android/internal/widget/WaveView;J)J
    .registers 5
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 45
    iget-wide v0, p0, Lcom/android/internal/widget/WaveView;->mWaveTimerDelay:J

    #@2
    add-long/2addr v0, p1

    #@3
    iput-wide v0, p0, Lcom/android/internal/widget/WaveView;->mWaveTimerDelay:J

    #@5
    return-wide v0
.end method

.method static synthetic access$700(Lcom/android/internal/widget/WaveView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 45
    iget v0, p0, Lcom/android/internal/widget/WaveView;->mCurrentWave:I

    #@2
    return v0
.end method

.method static synthetic access$702(Lcom/android/internal/widget/WaveView;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 45
    iput p1, p0, Lcom/android/internal/widget/WaveView;->mCurrentWave:I

    #@2
    return p1
.end method

.method static synthetic access$800(Lcom/android/internal/widget/WaveView;)Ljava/util/ArrayList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 45
    iget-object v0, p0, Lcom/android/internal/widget/WaveView;->mLightWaves:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method static synthetic access$900(Lcom/android/internal/widget/WaveView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 45
    iget v0, p0, Lcom/android/internal/widget/WaveView;->mWaveCount:I

    #@2
    return v0
.end method

.method private announceUnlockHandle()V
    .registers 3

    #@0
    .prologue
    .line 569
    iget-object v0, p0, Lcom/android/internal/widget/WaveView;->mContext:Landroid/content/Context;

    #@2
    const v1, 0x1040507

    #@5
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/WaveView;->setContentDescription(Ljava/lang/CharSequence;)V

    #@c
    .line 570
    const/16 v0, 0x8

    #@e
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/WaveView;->sendAccessibilityEvent(I)V

    #@11
    .line 571
    const/4 v0, 0x0

    #@12
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/WaveView;->setContentDescription(Ljava/lang/CharSequence;)V

    #@15
    .line 572
    return-void
.end method

.method private dispatchTriggerEvent(I)V
    .registers 4
    .parameter "whichHandle"

    #@0
    .prologue
    .line 604
    const-wide/16 v0, 0x14

    #@2
    invoke-direct {p0, v0, v1}, Lcom/android/internal/widget/WaveView;->vibrate(J)V

    #@5
    .line 605
    iget-object v0, p0, Lcom/android/internal/widget/WaveView;->mOnTriggerListener:Lcom/android/internal/widget/WaveView$OnTriggerListener;

    #@7
    if-eqz v0, :cond_e

    #@9
    .line 606
    iget-object v0, p0, Lcom/android/internal/widget/WaveView;->mOnTriggerListener:Lcom/android/internal/widget/WaveView$OnTriggerListener;

    #@b
    invoke-interface {v0, p0, p1}, Lcom/android/internal/widget/WaveView$OnTriggerListener;->onTrigger(Landroid/view/View;I)V

    #@e
    .line 608
    :cond_e
    return-void
.end method

.method private getScaledGrabHandleRadius()F
    .registers 3

    #@0
    .prologue
    .line 558
    iget-object v0, p0, Lcom/android/internal/widget/WaveView;->mContext:Landroid/content/Context;

    #@2
    invoke-static {v0}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_17

    #@c
    .line 559
    const/high16 v0, 0x3f80

    #@e
    iget-object v1, p0, Lcom/android/internal/widget/WaveView;->mUnlockHalo:Lcom/android/internal/widget/DrawableHolder;

    #@10
    invoke-virtual {v1}, Lcom/android/internal/widget/DrawableHolder;->getWidth()I

    #@13
    move-result v1

    #@14
    int-to-float v1, v1

    #@15
    mul-float/2addr v0, v1

    #@16
    .line 561
    :goto_16
    return v0

    #@17
    :cond_17
    const/high16 v0, 0x3f00

    #@19
    iget-object v1, p0, Lcom/android/internal/widget/WaveView;->mUnlockHalo:Lcom/android/internal/widget/DrawableHolder;

    #@1b
    invoke-virtual {v1}, Lcom/android/internal/widget/DrawableHolder;->getWidth()I

    #@1e
    move-result v1

    #@1f
    int-to-float v1, v1

    #@20
    mul-float/2addr v0, v1

    #@21
    goto :goto_16
.end method

.method private initDrawables()V
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const v5, 0x3dcccccd

    #@4
    .line 168
    new-instance v3, Lcom/android/internal/widget/DrawableHolder;

    #@6
    const v4, 0x1080611

    #@9
    invoke-virtual {p0, v4}, Lcom/android/internal/widget/WaveView;->createDrawable(I)Landroid/graphics/drawable/BitmapDrawable;

    #@c
    move-result-object v4

    #@d
    invoke-direct {v3, v4}, Lcom/android/internal/widget/DrawableHolder;-><init>(Landroid/graphics/drawable/BitmapDrawable;)V

    #@10
    iput-object v3, p0, Lcom/android/internal/widget/WaveView;->mUnlockRing:Lcom/android/internal/widget/DrawableHolder;

    #@12
    .line 169
    iget-object v3, p0, Lcom/android/internal/widget/WaveView;->mUnlockRing:Lcom/android/internal/widget/DrawableHolder;

    #@14
    iget v4, p0, Lcom/android/internal/widget/WaveView;->mLockCenterX:F

    #@16
    invoke-virtual {v3, v4}, Lcom/android/internal/widget/DrawableHolder;->setX(F)V

    #@19
    .line 170
    iget-object v3, p0, Lcom/android/internal/widget/WaveView;->mUnlockRing:Lcom/android/internal/widget/DrawableHolder;

    #@1b
    iget v4, p0, Lcom/android/internal/widget/WaveView;->mLockCenterY:F

    #@1d
    invoke-virtual {v3, v4}, Lcom/android/internal/widget/DrawableHolder;->setY(F)V

    #@20
    .line 171
    iget-object v3, p0, Lcom/android/internal/widget/WaveView;->mUnlockRing:Lcom/android/internal/widget/DrawableHolder;

    #@22
    invoke-virtual {v3, v5}, Lcom/android/internal/widget/DrawableHolder;->setScaleX(F)V

    #@25
    .line 172
    iget-object v3, p0, Lcom/android/internal/widget/WaveView;->mUnlockRing:Lcom/android/internal/widget/DrawableHolder;

    #@27
    invoke-virtual {v3, v5}, Lcom/android/internal/widget/DrawableHolder;->setScaleY(F)V

    #@2a
    .line 173
    iget-object v3, p0, Lcom/android/internal/widget/WaveView;->mUnlockRing:Lcom/android/internal/widget/DrawableHolder;

    #@2c
    invoke-virtual {v3, v6}, Lcom/android/internal/widget/DrawableHolder;->setAlpha(F)V

    #@2f
    .line 174
    iget-object v3, p0, Lcom/android/internal/widget/WaveView;->mDrawables:Ljava/util/ArrayList;

    #@31
    iget-object v4, p0, Lcom/android/internal/widget/WaveView;->mUnlockRing:Lcom/android/internal/widget/DrawableHolder;

    #@33
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@36
    .line 176
    new-instance v3, Lcom/android/internal/widget/DrawableHolder;

    #@38
    const v4, 0x108060f

    #@3b
    invoke-virtual {p0, v4}, Lcom/android/internal/widget/WaveView;->createDrawable(I)Landroid/graphics/drawable/BitmapDrawable;

    #@3e
    move-result-object v4

    #@3f
    invoke-direct {v3, v4}, Lcom/android/internal/widget/DrawableHolder;-><init>(Landroid/graphics/drawable/BitmapDrawable;)V

    #@42
    iput-object v3, p0, Lcom/android/internal/widget/WaveView;->mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

    #@44
    .line 177
    iget-object v3, p0, Lcom/android/internal/widget/WaveView;->mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

    #@46
    iget v4, p0, Lcom/android/internal/widget/WaveView;->mLockCenterX:F

    #@48
    invoke-virtual {v3, v4}, Lcom/android/internal/widget/DrawableHolder;->setX(F)V

    #@4b
    .line 178
    iget-object v3, p0, Lcom/android/internal/widget/WaveView;->mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

    #@4d
    iget v4, p0, Lcom/android/internal/widget/WaveView;->mLockCenterY:F

    #@4f
    invoke-virtual {v3, v4}, Lcom/android/internal/widget/DrawableHolder;->setY(F)V

    #@52
    .line 179
    iget-object v3, p0, Lcom/android/internal/widget/WaveView;->mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

    #@54
    invoke-virtual {v3, v5}, Lcom/android/internal/widget/DrawableHolder;->setScaleX(F)V

    #@57
    .line 180
    iget-object v3, p0, Lcom/android/internal/widget/WaveView;->mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

    #@59
    invoke-virtual {v3, v5}, Lcom/android/internal/widget/DrawableHolder;->setScaleY(F)V

    #@5c
    .line 181
    iget-object v3, p0, Lcom/android/internal/widget/WaveView;->mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

    #@5e
    invoke-virtual {v3, v6}, Lcom/android/internal/widget/DrawableHolder;->setAlpha(F)V

    #@61
    .line 182
    iget-object v3, p0, Lcom/android/internal/widget/WaveView;->mDrawables:Ljava/util/ArrayList;

    #@63
    iget-object v4, p0, Lcom/android/internal/widget/WaveView;->mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

    #@65
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@68
    .line 184
    new-instance v3, Lcom/android/internal/widget/DrawableHolder;

    #@6a
    const v4, 0x1080610

    #@6d
    invoke-virtual {p0, v4}, Lcom/android/internal/widget/WaveView;->createDrawable(I)Landroid/graphics/drawable/BitmapDrawable;

    #@70
    move-result-object v4

    #@71
    invoke-direct {v3, v4}, Lcom/android/internal/widget/DrawableHolder;-><init>(Landroid/graphics/drawable/BitmapDrawable;)V

    #@74
    iput-object v3, p0, Lcom/android/internal/widget/WaveView;->mUnlockHalo:Lcom/android/internal/widget/DrawableHolder;

    #@76
    .line 185
    iget-object v3, p0, Lcom/android/internal/widget/WaveView;->mUnlockHalo:Lcom/android/internal/widget/DrawableHolder;

    #@78
    iget v4, p0, Lcom/android/internal/widget/WaveView;->mLockCenterX:F

    #@7a
    invoke-virtual {v3, v4}, Lcom/android/internal/widget/DrawableHolder;->setX(F)V

    #@7d
    .line 186
    iget-object v3, p0, Lcom/android/internal/widget/WaveView;->mUnlockHalo:Lcom/android/internal/widget/DrawableHolder;

    #@7f
    iget v4, p0, Lcom/android/internal/widget/WaveView;->mLockCenterY:F

    #@81
    invoke-virtual {v3, v4}, Lcom/android/internal/widget/DrawableHolder;->setY(F)V

    #@84
    .line 187
    iget-object v3, p0, Lcom/android/internal/widget/WaveView;->mUnlockHalo:Lcom/android/internal/widget/DrawableHolder;

    #@86
    invoke-virtual {v3, v5}, Lcom/android/internal/widget/DrawableHolder;->setScaleX(F)V

    #@89
    .line 188
    iget-object v3, p0, Lcom/android/internal/widget/WaveView;->mUnlockHalo:Lcom/android/internal/widget/DrawableHolder;

    #@8b
    invoke-virtual {v3, v5}, Lcom/android/internal/widget/DrawableHolder;->setScaleY(F)V

    #@8e
    .line 189
    iget-object v3, p0, Lcom/android/internal/widget/WaveView;->mUnlockHalo:Lcom/android/internal/widget/DrawableHolder;

    #@90
    invoke-virtual {v3, v6}, Lcom/android/internal/widget/DrawableHolder;->setAlpha(F)V

    #@93
    .line 190
    iget-object v3, p0, Lcom/android/internal/widget/WaveView;->mDrawables:Ljava/util/ArrayList;

    #@95
    iget-object v4, p0, Lcom/android/internal/widget/WaveView;->mUnlockHalo:Lcom/android/internal/widget/DrawableHolder;

    #@97
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@9a
    .line 192
    const v3, 0x1080612

    #@9d
    invoke-virtual {p0, v3}, Lcom/android/internal/widget/WaveView;->createDrawable(I)Landroid/graphics/drawable/BitmapDrawable;

    #@a0
    move-result-object v2

    #@a1
    .line 193
    .local v2, wave:Landroid/graphics/drawable/BitmapDrawable;
    const/4 v1, 0x0

    #@a2
    .local v1, i:I
    :goto_a2
    iget v3, p0, Lcom/android/internal/widget/WaveView;->mWaveCount:I

    #@a4
    if-ge v1, v3, :cond_b6

    #@a6
    .line 194
    new-instance v0, Lcom/android/internal/widget/DrawableHolder;

    #@a8
    invoke-direct {v0, v2}, Lcom/android/internal/widget/DrawableHolder;-><init>(Landroid/graphics/drawable/BitmapDrawable;)V

    #@ab
    .line 195
    .local v0, holder:Lcom/android/internal/widget/DrawableHolder;
    iget-object v3, p0, Lcom/android/internal/widget/WaveView;->mLightWaves:Ljava/util/ArrayList;

    #@ad
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@b0
    .line 196
    invoke-virtual {v0, v6}, Lcom/android/internal/widget/DrawableHolder;->setAlpha(F)V

    #@b3
    .line 193
    add-int/lit8 v1, v1, 0x1

    #@b5
    goto :goto_a2

    #@b6
    .line 198
    .end local v0           #holder:Lcom/android/internal/widget/DrawableHolder;
    :cond_b6
    return-void
.end method

.method private setGrabbedState(I)V
    .registers 4
    .parameter "newState"

    #@0
    .prologue
    .line 615
    iget v0, p0, Lcom/android/internal/widget/WaveView;->mGrabbedState:I

    #@2
    if-eq p1, v0, :cond_11

    #@4
    .line 616
    iput p1, p0, Lcom/android/internal/widget/WaveView;->mGrabbedState:I

    #@6
    .line 617
    iget-object v0, p0, Lcom/android/internal/widget/WaveView;->mOnTriggerListener:Lcom/android/internal/widget/WaveView$OnTriggerListener;

    #@8
    if-eqz v0, :cond_11

    #@a
    .line 618
    iget-object v0, p0, Lcom/android/internal/widget/WaveView;->mOnTriggerListener:Lcom/android/internal/widget/WaveView$OnTriggerListener;

    #@c
    iget v1, p0, Lcom/android/internal/widget/WaveView;->mGrabbedState:I

    #@e
    invoke-interface {v0, p0, v1}, Lcom/android/internal/widget/WaveView$OnTriggerListener;->onGrabbedStateChange(Landroid/view/View;I)V

    #@11
    .line 621
    :cond_11
    return-void
.end method

.method private tryTransitionToStartAttemptState(Landroid/view/MotionEvent;)V
    .registers 9
    .parameter "event"

    #@0
    .prologue
    .line 539
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@3
    move-result v3

    #@4
    iget-object v4, p0, Lcom/android/internal/widget/WaveView;->mUnlockHalo:Lcom/android/internal/widget/DrawableHolder;

    #@6
    invoke-virtual {v4}, Lcom/android/internal/widget/DrawableHolder;->getX()F

    #@9
    move-result v4

    #@a
    sub-float v1, v3, v4

    #@c
    .line 540
    .local v1, dx:F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@f
    move-result v3

    #@10
    iget-object v4, p0, Lcom/android/internal/widget/WaveView;->mUnlockHalo:Lcom/android/internal/widget/DrawableHolder;

    #@12
    invoke-virtual {v4}, Lcom/android/internal/widget/DrawableHolder;->getY()F

    #@15
    move-result v4

    #@16
    sub-float v2, v3, v4

    #@18
    .line 541
    .local v2, dy:F
    float-to-double v3, v1

    #@19
    float-to-double v5, v2

    #@1a
    invoke-static {v3, v4, v5, v6}, Ljava/lang/Math;->hypot(DD)D

    #@1d
    move-result-wide v3

    #@1e
    double-to-float v0, v3

    #@1f
    .line 542
    .local v0, dist:F
    invoke-direct {p0}, Lcom/android/internal/widget/WaveView;->getScaledGrabHandleRadius()F

    #@22
    move-result v3

    #@23
    cmpg-float v3, v0, v3

    #@25
    if-gtz v3, :cond_43

    #@27
    .line 543
    const/16 v3, 0xa

    #@29
    invoke-direct {p0, v3}, Lcom/android/internal/widget/WaveView;->setGrabbedState(I)V

    #@2c
    .line 544
    iget v3, p0, Lcom/android/internal/widget/WaveView;->mLockState:I

    #@2e
    const/4 v4, 0x1

    #@2f
    if-ne v3, v4, :cond_43

    #@31
    .line 545
    const/4 v3, 0x2

    #@32
    iput v3, p0, Lcom/android/internal/widget/WaveView;->mLockState:I

    #@34
    .line 546
    iget-object v3, p0, Lcom/android/internal/widget/WaveView;->mContext:Landroid/content/Context;

    #@36
    invoke-static {v3}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    #@39
    move-result-object v3

    #@3a
    invoke-virtual {v3}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    #@3d
    move-result v3

    #@3e
    if-eqz v3, :cond_43

    #@40
    .line 547
    invoke-direct {p0}, Lcom/android/internal/widget/WaveView;->announceUnlockHandle()V

    #@43
    .line 551
    :cond_43
    return-void
.end method

.method private declared-synchronized vibrate(J)V
    .registers 8
    .parameter "duration"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 578
    monitor-enter p0

    #@2
    :try_start_2
    iget-object v1, p0, Lcom/android/internal/widget/WaveView;->mContext:Landroid/content/Context;

    #@4
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7
    move-result-object v1

    #@8
    const-string v2, "haptic_feedback_enabled"

    #@a
    const/4 v3, 0x1

    #@b
    const/4 v4, -0x2

    #@c
    invoke-static {v1, v2, v3, v4}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    #@f
    move-result v1

    #@10
    if-eqz v1, :cond_2d

    #@12
    .line 581
    .local v0, hapticEnabled:Z
    :goto_12
    if-eqz v0, :cond_2b

    #@14
    .line 582
    iget-object v1, p0, Lcom/android/internal/widget/WaveView;->mVibrator:Landroid/os/Vibrator;

    #@16
    if-nez v1, :cond_26

    #@18
    .line 583
    invoke-virtual {p0}, Lcom/android/internal/widget/WaveView;->getContext()Landroid/content/Context;

    #@1b
    move-result-object v1

    #@1c
    const-string v2, "vibrator"

    #@1e
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@21
    move-result-object v1

    #@22
    check-cast v1, Landroid/os/Vibrator;

    #@24
    iput-object v1, p0, Lcom/android/internal/widget/WaveView;->mVibrator:Landroid/os/Vibrator;

    #@26
    .line 586
    :cond_26
    iget-object v1, p0, Lcom/android/internal/widget/WaveView;->mVibrator:Landroid/os/Vibrator;

    #@28
    invoke-virtual {v1, p1, p2}, Landroid/os/Vibrator;->vibrate(J)V
    :try_end_2b
    .catchall {:try_start_2 .. :try_end_2b} :catchall_2f

    #@2b
    .line 588
    :cond_2b
    monitor-exit p0

    #@2c
    return-void

    #@2d
    .line 578
    .end local v0           #hapticEnabled:Z
    :cond_2d
    const/4 v0, 0x0

    #@2e
    goto :goto_12

    #@2f
    :catchall_2f
    move-exception v1

    #@30
    monitor-exit p0

    #@31
    throw v1
.end method

.method private waveUpdateFrame(FFZ)V
    .registers 38
    .parameter "mouseX"
    .parameter "mouseY"
    .parameter "fingerDown"

    #@0
    .prologue
    .line 201
    move-object/from16 v0, p0

    #@2
    iget v4, v0, Lcom/android/internal/widget/WaveView;->mLockCenterX:F

    #@4
    sub-float v4, p1, v4

    #@6
    float-to-double v0, v4

    #@7
    move-wide/from16 v25, v0

    #@9
    .line 202
    .local v25, distX:D
    move-object/from16 v0, p0

    #@b
    iget v4, v0, Lcom/android/internal/widget/WaveView;->mLockCenterY:F

    #@d
    sub-float v4, p2, v4

    #@f
    float-to-double v0, v4

    #@10
    move-wide/from16 v27, v0

    #@12
    .line 203
    .local v27, distY:D
    invoke-static/range {v25 .. v28}, Ljava/lang/Math;->hypot(DD)D

    #@15
    move-result-wide v4

    #@16
    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    #@19
    move-result-wide v4

    #@1a
    double-to-int v0, v4

    #@1b
    move/from16 v29, v0

    #@1d
    .line 204
    .local v29, dragDistance:I
    invoke-static/range {v25 .. v28}, Ljava/lang/Math;->atan2(DD)D

    #@20
    move-result-wide v32

    #@21
    .line 205
    .local v32, touchA:D
    move-object/from16 v0, p0

    #@23
    iget v4, v0, Lcom/android/internal/widget/WaveView;->mLockCenterX:F

    #@25
    float-to-double v4, v4

    #@26
    move-object/from16 v0, p0

    #@28
    iget v8, v0, Lcom/android/internal/widget/WaveView;->mRingRadius:F

    #@2a
    float-to-double v10, v8

    #@2b
    invoke-static/range {v32 .. v33}, Ljava/lang/Math;->sin(D)D

    #@2e
    move-result-wide v12

    #@2f
    mul-double/2addr v10, v12

    #@30
    add-double/2addr v4, v10

    #@31
    double-to-float v9, v4

    #@32
    .line 206
    .local v9, ringX:F
    move-object/from16 v0, p0

    #@34
    iget v4, v0, Lcom/android/internal/widget/WaveView;->mLockCenterY:F

    #@36
    float-to-double v4, v4

    #@37
    move-object/from16 v0, p0

    #@39
    iget v8, v0, Lcom/android/internal/widget/WaveView;->mRingRadius:F

    #@3b
    float-to-double v10, v8

    #@3c
    invoke-static/range {v32 .. v33}, Ljava/lang/Math;->cos(D)D

    #@3f
    move-result-wide v12

    #@40
    mul-double/2addr v10, v12

    #@41
    add-double/2addr v4, v10

    #@42
    double-to-float v0, v4

    #@43
    move/from16 v16, v0

    #@45
    .line 208
    .local v16, ringY:F
    move-object/from16 v0, p0

    #@47
    iget v4, v0, Lcom/android/internal/widget/WaveView;->mLockState:I

    #@49
    packed-switch v4, :pswitch_data_6a0

    #@4c
    .line 392
    .end local v9           #ringX:F
    :goto_4c
    move-object/from16 v0, p0

    #@4e
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

    #@50
    move-object/from16 v0, p0

    #@52
    invoke-virtual {v4, v0}, Lcom/android/internal/widget/DrawableHolder;->startAnimations(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    #@55
    .line 393
    move-object/from16 v0, p0

    #@57
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mUnlockHalo:Lcom/android/internal/widget/DrawableHolder;

    #@59
    move-object/from16 v0, p0

    #@5b
    invoke-virtual {v4, v0}, Lcom/android/internal/widget/DrawableHolder;->startAnimations(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    #@5e
    .line 394
    move-object/from16 v0, p0

    #@60
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mUnlockRing:Lcom/android/internal/widget/DrawableHolder;

    #@62
    move-object/from16 v0, p0

    #@64
    invoke-virtual {v4, v0}, Lcom/android/internal/widget/DrawableHolder;->startAnimations(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    #@67
    .line 395
    return-void

    #@68
    .line 211
    .restart local v9       #ringX:F
    :pswitch_68
    const-wide/16 v4, 0x64

    #@6a
    move-object/from16 v0, p0

    #@6c
    iput-wide v4, v0, Lcom/android/internal/widget/WaveView;->mWaveTimerDelay:J

    #@6e
    .line 212
    const/16 v30, 0x0

    #@70
    .end local v9           #ringX:F
    .local v30, i:I
    :goto_70
    move-object/from16 v0, p0

    #@72
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mLightWaves:Ljava/util/ArrayList;

    #@74
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@77
    move-result v4

    #@78
    move/from16 v0, v30

    #@7a
    if-ge v0, v4, :cond_96

    #@7c
    .line 213
    move-object/from16 v0, p0

    #@7e
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mLightWaves:Ljava/util/ArrayList;

    #@80
    move/from16 v0, v30

    #@82
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@85
    move-result-object v2

    #@86
    check-cast v2, Lcom/android/internal/widget/DrawableHolder;

    #@88
    .line 214
    .local v2, holder:Lcom/android/internal/widget/DrawableHolder;
    const-wide/16 v3, 0x12c

    #@8a
    const-wide/16 v5, 0x0

    #@8c
    const-string v7, "alpha"

    #@8e
    const/4 v8, 0x0

    #@8f
    const/4 v9, 0x0

    #@90
    invoke-virtual/range {v2 .. v9}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@93
    .line 212
    add-int/lit8 v30, v30, 0x1

    #@95
    goto :goto_70

    #@96
    .line 216
    .end local v2           #holder:Lcom/android/internal/widget/DrawableHolder;
    :cond_96
    const/16 v30, 0x0

    #@98
    :goto_98
    move-object/from16 v0, p0

    #@9a
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mLightWaves:Ljava/util/ArrayList;

    #@9c
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@9f
    move-result v4

    #@a0
    move/from16 v0, v30

    #@a2
    if-ge v0, v4, :cond_b8

    #@a4
    .line 217
    move-object/from16 v0, p0

    #@a6
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mLightWaves:Ljava/util/ArrayList;

    #@a8
    move/from16 v0, v30

    #@aa
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@ad
    move-result-object v4

    #@ae
    check-cast v4, Lcom/android/internal/widget/DrawableHolder;

    #@b0
    move-object/from16 v0, p0

    #@b2
    invoke-virtual {v4, v0}, Lcom/android/internal/widget/DrawableHolder;->startAnimations(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    #@b5
    .line 216
    add-int/lit8 v30, v30, 0x1

    #@b7
    goto :goto_98

    #@b8
    .line 220
    :cond_b8
    move-object/from16 v0, p0

    #@ba
    iget-object v3, v0, Lcom/android/internal/widget/WaveView;->mUnlockRing:Lcom/android/internal/widget/DrawableHolder;

    #@bc
    const-wide/16 v4, 0x12c

    #@be
    const-wide/16 v6, 0x0

    #@c0
    const-string v8, "x"

    #@c2
    move-object/from16 v0, p0

    #@c4
    iget v9, v0, Lcom/android/internal/widget/WaveView;->mLockCenterX:F

    #@c6
    const/4 v10, 0x1

    #@c7
    invoke-virtual/range {v3 .. v10}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@ca
    .line 221
    move-object/from16 v0, p0

    #@cc
    iget-object v3, v0, Lcom/android/internal/widget/WaveView;->mUnlockRing:Lcom/android/internal/widget/DrawableHolder;

    #@ce
    const-wide/16 v4, 0x12c

    #@d0
    const-wide/16 v6, 0x0

    #@d2
    const-string v8, "y"

    #@d4
    move-object/from16 v0, p0

    #@d6
    iget v9, v0, Lcom/android/internal/widget/WaveView;->mLockCenterY:F

    #@d8
    const/4 v10, 0x1

    #@d9
    invoke-virtual/range {v3 .. v10}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@dc
    .line 222
    move-object/from16 v0, p0

    #@de
    iget-object v3, v0, Lcom/android/internal/widget/WaveView;->mUnlockRing:Lcom/android/internal/widget/DrawableHolder;

    #@e0
    const-wide/16 v4, 0x12c

    #@e2
    const-wide/16 v6, 0x0

    #@e4
    const-string v8, "scaleX"

    #@e6
    const v9, 0x3dcccccd

    #@e9
    const/4 v10, 0x1

    #@ea
    invoke-virtual/range {v3 .. v10}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@ed
    .line 223
    move-object/from16 v0, p0

    #@ef
    iget-object v3, v0, Lcom/android/internal/widget/WaveView;->mUnlockRing:Lcom/android/internal/widget/DrawableHolder;

    #@f1
    const-wide/16 v4, 0x12c

    #@f3
    const-wide/16 v6, 0x0

    #@f5
    const-string v8, "scaleY"

    #@f7
    const v9, 0x3dcccccd

    #@fa
    const/4 v10, 0x1

    #@fb
    invoke-virtual/range {v3 .. v10}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@fe
    .line 224
    move-object/from16 v0, p0

    #@100
    iget-object v3, v0, Lcom/android/internal/widget/WaveView;->mUnlockRing:Lcom/android/internal/widget/DrawableHolder;

    #@102
    const-wide/16 v4, 0x12c

    #@104
    const-wide/16 v6, 0x0

    #@106
    const-string v8, "alpha"

    #@108
    const/4 v9, 0x0

    #@109
    const/4 v10, 0x1

    #@10a
    invoke-virtual/range {v3 .. v10}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@10d
    .line 226
    move-object/from16 v0, p0

    #@10f
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

    #@111
    const-string v5, "x"

    #@113
    invoke-virtual {v4, v5}, Lcom/android/internal/widget/DrawableHolder;->removeAnimationFor(Ljava/lang/String;)V

    #@116
    .line 227
    move-object/from16 v0, p0

    #@118
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

    #@11a
    const-string v5, "y"

    #@11c
    invoke-virtual {v4, v5}, Lcom/android/internal/widget/DrawableHolder;->removeAnimationFor(Ljava/lang/String;)V

    #@11f
    .line 228
    move-object/from16 v0, p0

    #@121
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

    #@123
    const-string v5, "scaleX"

    #@125
    invoke-virtual {v4, v5}, Lcom/android/internal/widget/DrawableHolder;->removeAnimationFor(Ljava/lang/String;)V

    #@128
    .line 229
    move-object/from16 v0, p0

    #@12a
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

    #@12c
    const-string v5, "scaleY"

    #@12e
    invoke-virtual {v4, v5}, Lcom/android/internal/widget/DrawableHolder;->removeAnimationFor(Ljava/lang/String;)V

    #@131
    .line 230
    move-object/from16 v0, p0

    #@133
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

    #@135
    const-string v5, "alpha"

    #@137
    invoke-virtual {v4, v5}, Lcom/android/internal/widget/DrawableHolder;->removeAnimationFor(Ljava/lang/String;)V

    #@13a
    .line 231
    move-object/from16 v0, p0

    #@13c
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

    #@13e
    move-object/from16 v0, p0

    #@140
    iget v5, v0, Lcom/android/internal/widget/WaveView;->mLockCenterX:F

    #@142
    invoke-virtual {v4, v5}, Lcom/android/internal/widget/DrawableHolder;->setX(F)V

    #@145
    .line 232
    move-object/from16 v0, p0

    #@147
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

    #@149
    move-object/from16 v0, p0

    #@14b
    iget v5, v0, Lcom/android/internal/widget/WaveView;->mLockCenterY:F

    #@14d
    invoke-virtual {v4, v5}, Lcom/android/internal/widget/DrawableHolder;->setY(F)V

    #@150
    .line 233
    move-object/from16 v0, p0

    #@152
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

    #@154
    const v5, 0x3dcccccd

    #@157
    invoke-virtual {v4, v5}, Lcom/android/internal/widget/DrawableHolder;->setScaleX(F)V

    #@15a
    .line 234
    move-object/from16 v0, p0

    #@15c
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

    #@15e
    const v5, 0x3dcccccd

    #@161
    invoke-virtual {v4, v5}, Lcom/android/internal/widget/DrawableHolder;->setScaleY(F)V

    #@164
    .line 235
    move-object/from16 v0, p0

    #@166
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

    #@168
    const/4 v5, 0x0

    #@169
    invoke-virtual {v4, v5}, Lcom/android/internal/widget/DrawableHolder;->setAlpha(F)V

    #@16c
    .line 236
    move-object/from16 v0, p0

    #@16e
    iget-object v3, v0, Lcom/android/internal/widget/WaveView;->mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

    #@170
    const-wide/16 v4, 0x12c

    #@172
    const-wide/16 v6, 0x64

    #@174
    const-string v8, "scaleX"

    #@176
    const/high16 v9, 0x3f80

    #@178
    const/4 v10, 0x1

    #@179
    invoke-virtual/range {v3 .. v10}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@17c
    .line 237
    move-object/from16 v0, p0

    #@17e
    iget-object v3, v0, Lcom/android/internal/widget/WaveView;->mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

    #@180
    const-wide/16 v4, 0x12c

    #@182
    const-wide/16 v6, 0x64

    #@184
    const-string v8, "scaleY"

    #@186
    const/high16 v9, 0x3f80

    #@188
    const/4 v10, 0x1

    #@189
    invoke-virtual/range {v3 .. v10}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@18c
    .line 238
    move-object/from16 v0, p0

    #@18e
    iget-object v3, v0, Lcom/android/internal/widget/WaveView;->mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

    #@190
    const-wide/16 v4, 0x12c

    #@192
    const-wide/16 v6, 0x64

    #@194
    const-string v8, "alpha"

    #@196
    const/high16 v9, 0x3f80

    #@198
    const/4 v10, 0x1

    #@199
    invoke-virtual/range {v3 .. v10}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@19c
    .line 240
    move-object/from16 v0, p0

    #@19e
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mUnlockHalo:Lcom/android/internal/widget/DrawableHolder;

    #@1a0
    const-string v5, "x"

    #@1a2
    invoke-virtual {v4, v5}, Lcom/android/internal/widget/DrawableHolder;->removeAnimationFor(Ljava/lang/String;)V

    #@1a5
    .line 241
    move-object/from16 v0, p0

    #@1a7
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mUnlockHalo:Lcom/android/internal/widget/DrawableHolder;

    #@1a9
    const-string v5, "y"

    #@1ab
    invoke-virtual {v4, v5}, Lcom/android/internal/widget/DrawableHolder;->removeAnimationFor(Ljava/lang/String;)V

    #@1ae
    .line 242
    move-object/from16 v0, p0

    #@1b0
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mUnlockHalo:Lcom/android/internal/widget/DrawableHolder;

    #@1b2
    const-string v5, "scaleX"

    #@1b4
    invoke-virtual {v4, v5}, Lcom/android/internal/widget/DrawableHolder;->removeAnimationFor(Ljava/lang/String;)V

    #@1b7
    .line 243
    move-object/from16 v0, p0

    #@1b9
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mUnlockHalo:Lcom/android/internal/widget/DrawableHolder;

    #@1bb
    const-string v5, "scaleY"

    #@1bd
    invoke-virtual {v4, v5}, Lcom/android/internal/widget/DrawableHolder;->removeAnimationFor(Ljava/lang/String;)V

    #@1c0
    .line 244
    move-object/from16 v0, p0

    #@1c2
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mUnlockHalo:Lcom/android/internal/widget/DrawableHolder;

    #@1c4
    const-string v5, "alpha"

    #@1c6
    invoke-virtual {v4, v5}, Lcom/android/internal/widget/DrawableHolder;->removeAnimationFor(Ljava/lang/String;)V

    #@1c9
    .line 245
    move-object/from16 v0, p0

    #@1cb
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mUnlockHalo:Lcom/android/internal/widget/DrawableHolder;

    #@1cd
    move-object/from16 v0, p0

    #@1cf
    iget v5, v0, Lcom/android/internal/widget/WaveView;->mLockCenterX:F

    #@1d1
    invoke-virtual {v4, v5}, Lcom/android/internal/widget/DrawableHolder;->setX(F)V

    #@1d4
    .line 246
    move-object/from16 v0, p0

    #@1d6
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mUnlockHalo:Lcom/android/internal/widget/DrawableHolder;

    #@1d8
    move-object/from16 v0, p0

    #@1da
    iget v5, v0, Lcom/android/internal/widget/WaveView;->mLockCenterY:F

    #@1dc
    invoke-virtual {v4, v5}, Lcom/android/internal/widget/DrawableHolder;->setY(F)V

    #@1df
    .line 247
    move-object/from16 v0, p0

    #@1e1
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mUnlockHalo:Lcom/android/internal/widget/DrawableHolder;

    #@1e3
    const v5, 0x3dcccccd

    #@1e6
    invoke-virtual {v4, v5}, Lcom/android/internal/widget/DrawableHolder;->setScaleX(F)V

    #@1e9
    .line 248
    move-object/from16 v0, p0

    #@1eb
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mUnlockHalo:Lcom/android/internal/widget/DrawableHolder;

    #@1ed
    const v5, 0x3dcccccd

    #@1f0
    invoke-virtual {v4, v5}, Lcom/android/internal/widget/DrawableHolder;->setScaleY(F)V

    #@1f3
    .line 249
    move-object/from16 v0, p0

    #@1f5
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mUnlockHalo:Lcom/android/internal/widget/DrawableHolder;

    #@1f7
    const/4 v5, 0x0

    #@1f8
    invoke-virtual {v4, v5}, Lcom/android/internal/widget/DrawableHolder;->setAlpha(F)V

    #@1fb
    .line 250
    move-object/from16 v0, p0

    #@1fd
    iget-object v3, v0, Lcom/android/internal/widget/WaveView;->mUnlockHalo:Lcom/android/internal/widget/DrawableHolder;

    #@1ff
    const-wide/16 v4, 0x12c

    #@201
    const-wide/16 v6, 0x64

    #@203
    const-string v8, "x"

    #@205
    move-object/from16 v0, p0

    #@207
    iget v9, v0, Lcom/android/internal/widget/WaveView;->mLockCenterX:F

    #@209
    const/4 v10, 0x1

    #@20a
    invoke-virtual/range {v3 .. v10}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@20d
    .line 251
    move-object/from16 v0, p0

    #@20f
    iget-object v3, v0, Lcom/android/internal/widget/WaveView;->mUnlockHalo:Lcom/android/internal/widget/DrawableHolder;

    #@211
    const-wide/16 v4, 0x12c

    #@213
    const-wide/16 v6, 0x64

    #@215
    const-string v8, "y"

    #@217
    move-object/from16 v0, p0

    #@219
    iget v9, v0, Lcom/android/internal/widget/WaveView;->mLockCenterY:F

    #@21b
    const/4 v10, 0x1

    #@21c
    invoke-virtual/range {v3 .. v10}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@21f
    .line 252
    move-object/from16 v0, p0

    #@221
    iget-object v3, v0, Lcom/android/internal/widget/WaveView;->mUnlockHalo:Lcom/android/internal/widget/DrawableHolder;

    #@223
    const-wide/16 v4, 0x12c

    #@225
    const-wide/16 v6, 0x64

    #@227
    const-string v8, "scaleX"

    #@229
    const/high16 v9, 0x3f80

    #@22b
    const/4 v10, 0x1

    #@22c
    invoke-virtual/range {v3 .. v10}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@22f
    .line 253
    move-object/from16 v0, p0

    #@231
    iget-object v3, v0, Lcom/android/internal/widget/WaveView;->mUnlockHalo:Lcom/android/internal/widget/DrawableHolder;

    #@233
    const-wide/16 v4, 0x12c

    #@235
    const-wide/16 v6, 0x64

    #@237
    const-string v8, "scaleY"

    #@239
    const/high16 v9, 0x3f80

    #@23b
    const/4 v10, 0x1

    #@23c
    invoke-virtual/range {v3 .. v10}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@23f
    .line 254
    move-object/from16 v0, p0

    #@241
    iget-object v3, v0, Lcom/android/internal/widget/WaveView;->mUnlockHalo:Lcom/android/internal/widget/DrawableHolder;

    #@243
    const-wide/16 v4, 0x12c

    #@245
    const-wide/16 v6, 0x64

    #@247
    const-string v8, "alpha"

    #@249
    const/high16 v9, 0x3f80

    #@24b
    const/4 v10, 0x1

    #@24c
    invoke-virtual/range {v3 .. v10}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@24f
    .line 256
    move-object/from16 v0, p0

    #@251
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mLockTimerActions:Ljava/lang/Runnable;

    #@253
    move-object/from16 v0, p0

    #@255
    invoke-virtual {v0, v4}, Lcom/android/internal/widget/WaveView;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@258
    .line 258
    const/4 v4, 0x1

    #@259
    move-object/from16 v0, p0

    #@25b
    iput v4, v0, Lcom/android/internal/widget/WaveView;->mLockState:I

    #@25d
    goto/16 :goto_4c

    #@25f
    .line 263
    .end local v30           #i:I
    .restart local v9       #ringX:F
    :pswitch_25f
    const-wide/16 v4, 0x64

    #@261
    move-object/from16 v0, p0

    #@263
    iput-wide v4, v0, Lcom/android/internal/widget/WaveView;->mWaveTimerDelay:J

    #@265
    goto/16 :goto_4c

    #@267
    .line 268
    :pswitch_267
    move-object/from16 v0, p0

    #@269
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

    #@26b
    const-string v5, "x"

    #@26d
    invoke-virtual {v4, v5}, Lcom/android/internal/widget/DrawableHolder;->removeAnimationFor(Ljava/lang/String;)V

    #@270
    .line 269
    move-object/from16 v0, p0

    #@272
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

    #@274
    const-string v5, "y"

    #@276
    invoke-virtual {v4, v5}, Lcom/android/internal/widget/DrawableHolder;->removeAnimationFor(Ljava/lang/String;)V

    #@279
    .line 270
    move-object/from16 v0, p0

    #@27b
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

    #@27d
    const-string v5, "scaleX"

    #@27f
    invoke-virtual {v4, v5}, Lcom/android/internal/widget/DrawableHolder;->removeAnimationFor(Ljava/lang/String;)V

    #@282
    .line 271
    move-object/from16 v0, p0

    #@284
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

    #@286
    const-string v5, "scaleY"

    #@288
    invoke-virtual {v4, v5}, Lcom/android/internal/widget/DrawableHolder;->removeAnimationFor(Ljava/lang/String;)V

    #@28b
    .line 272
    move-object/from16 v0, p0

    #@28d
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

    #@28f
    const-string v5, "alpha"

    #@291
    invoke-virtual {v4, v5}, Lcom/android/internal/widget/DrawableHolder;->removeAnimationFor(Ljava/lang/String;)V

    #@294
    .line 273
    move-object/from16 v0, p0

    #@296
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

    #@298
    move-object/from16 v0, p0

    #@29a
    iget v5, v0, Lcom/android/internal/widget/WaveView;->mLockCenterX:F

    #@29c
    const/high16 v8, 0x4336

    #@29e
    add-float/2addr v5, v8

    #@29f
    invoke-virtual {v4, v5}, Lcom/android/internal/widget/DrawableHolder;->setX(F)V

    #@2a2
    .line 274
    move-object/from16 v0, p0

    #@2a4
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

    #@2a6
    move-object/from16 v0, p0

    #@2a8
    iget v5, v0, Lcom/android/internal/widget/WaveView;->mLockCenterY:F

    #@2aa
    invoke-virtual {v4, v5}, Lcom/android/internal/widget/DrawableHolder;->setY(F)V

    #@2ad
    .line 275
    move-object/from16 v0, p0

    #@2af
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

    #@2b1
    const v5, 0x3dcccccd

    #@2b4
    invoke-virtual {v4, v5}, Lcom/android/internal/widget/DrawableHolder;->setScaleX(F)V

    #@2b7
    .line 276
    move-object/from16 v0, p0

    #@2b9
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

    #@2bb
    const v5, 0x3dcccccd

    #@2be
    invoke-virtual {v4, v5}, Lcom/android/internal/widget/DrawableHolder;->setScaleY(F)V

    #@2c1
    .line 277
    move-object/from16 v0, p0

    #@2c3
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

    #@2c5
    const/4 v5, 0x0

    #@2c6
    invoke-virtual {v4, v5}, Lcom/android/internal/widget/DrawableHolder;->setAlpha(F)V

    #@2c9
    .line 279
    move-object/from16 v0, p0

    #@2cb
    iget-object v3, v0, Lcom/android/internal/widget/WaveView;->mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

    #@2cd
    const-wide/16 v4, 0x12c

    #@2cf
    const-wide/16 v6, 0x64

    #@2d1
    const-string v8, "scaleX"

    #@2d3
    const/high16 v9, 0x3f80

    #@2d5
    const/4 v10, 0x0

    #@2d6
    invoke-virtual/range {v3 .. v10}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@2d9
    .line 280
    .end local v9           #ringX:F
    move-object/from16 v0, p0

    #@2db
    iget-object v3, v0, Lcom/android/internal/widget/WaveView;->mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

    #@2dd
    const-wide/16 v4, 0x12c

    #@2df
    const-wide/16 v6, 0x64

    #@2e1
    const-string v8, "scaleY"

    #@2e3
    const/high16 v9, 0x3f80

    #@2e5
    const/4 v10, 0x0

    #@2e6
    invoke-virtual/range {v3 .. v10}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@2e9
    .line 281
    move-object/from16 v0, p0

    #@2eb
    iget-object v3, v0, Lcom/android/internal/widget/WaveView;->mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

    #@2ed
    const-wide/16 v4, 0x12c

    #@2ef
    const-wide/16 v6, 0x64

    #@2f1
    const-string v8, "alpha"

    #@2f3
    const/high16 v9, 0x3f80

    #@2f5
    const/4 v10, 0x0

    #@2f6
    invoke-virtual/range {v3 .. v10}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@2f9
    .line 283
    move-object/from16 v0, p0

    #@2fb
    iget-object v3, v0, Lcom/android/internal/widget/WaveView;->mUnlockRing:Lcom/android/internal/widget/DrawableHolder;

    #@2fd
    const-wide/16 v4, 0x12c

    #@2ff
    const-wide/16 v6, 0x0

    #@301
    const-string v8, "scaleX"

    #@303
    const/high16 v9, 0x3f80

    #@305
    const/4 v10, 0x1

    #@306
    invoke-virtual/range {v3 .. v10}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@309
    .line 284
    move-object/from16 v0, p0

    #@30b
    iget-object v3, v0, Lcom/android/internal/widget/WaveView;->mUnlockRing:Lcom/android/internal/widget/DrawableHolder;

    #@30d
    const-wide/16 v4, 0x12c

    #@30f
    const-wide/16 v6, 0x0

    #@311
    const-string v8, "scaleY"

    #@313
    const/high16 v9, 0x3f80

    #@315
    const/4 v10, 0x1

    #@316
    invoke-virtual/range {v3 .. v10}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@319
    .line 285
    move-object/from16 v0, p0

    #@31b
    iget-object v3, v0, Lcom/android/internal/widget/WaveView;->mUnlockRing:Lcom/android/internal/widget/DrawableHolder;

    #@31d
    const-wide/16 v4, 0x12c

    #@31f
    const-wide/16 v6, 0x0

    #@321
    const-string v8, "alpha"

    #@323
    const/high16 v9, 0x3f80

    #@325
    const/4 v10, 0x1

    #@326
    invoke-virtual/range {v3 .. v10}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@329
    .line 287
    const/4 v4, 0x3

    #@32a
    move-object/from16 v0, p0

    #@32c
    iput v4, v0, Lcom/android/internal/widget/WaveView;->mLockState:I

    #@32e
    goto/16 :goto_4c

    #@330
    .line 292
    .restart local v9       #ringX:F
    :pswitch_330
    move-object/from16 v0, p0

    #@332
    iget v4, v0, Lcom/android/internal/widget/WaveView;->mSnapRadius:I

    #@334
    move/from16 v0, v29

    #@336
    if-le v0, v4, :cond_39e

    #@338
    .line 293
    const/4 v4, 0x1

    #@339
    move-object/from16 v0, p0

    #@33b
    iput-boolean v4, v0, Lcom/android/internal/widget/WaveView;->mFinishWaves:Z

    #@33d
    .line 294
    if-eqz p3, :cond_397

    #@33f
    .line 295
    move-object/from16 v0, p0

    #@341
    iget-object v3, v0, Lcom/android/internal/widget/WaveView;->mUnlockHalo:Lcom/android/internal/widget/DrawableHolder;

    #@343
    const-wide/16 v4, 0x0

    #@345
    const-wide/16 v6, 0x0

    #@347
    const-string v8, "x"

    #@349
    const/4 v10, 0x1

    #@34a
    invoke-virtual/range {v3 .. v10}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@34d
    .line 296
    move-object/from16 v0, p0

    #@34f
    iget-object v10, v0, Lcom/android/internal/widget/WaveView;->mUnlockHalo:Lcom/android/internal/widget/DrawableHolder;

    #@351
    const-wide/16 v11, 0x0

    #@353
    const-wide/16 v13, 0x0

    #@355
    const-string v15, "y"

    #@357
    const/16 v17, 0x1

    #@359
    invoke-virtual/range {v10 .. v17}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@35c
    .line 297
    move-object/from16 v0, p0

    #@35e
    iget-object v0, v0, Lcom/android/internal/widget/WaveView;->mUnlockHalo:Lcom/android/internal/widget/DrawableHolder;

    #@360
    move-object/from16 v17, v0

    #@362
    const-wide/16 v18, 0x0

    #@364
    const-wide/16 v20, 0x0

    #@366
    const-string v22, "scaleX"

    #@368
    const/high16 v23, 0x3f80

    #@36a
    const/16 v24, 0x1

    #@36c
    invoke-virtual/range {v17 .. v24}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@36f
    .line 298
    move-object/from16 v0, p0

    #@371
    iget-object v0, v0, Lcom/android/internal/widget/WaveView;->mUnlockHalo:Lcom/android/internal/widget/DrawableHolder;

    #@373
    move-object/from16 v17, v0

    #@375
    const-wide/16 v18, 0x0

    #@377
    const-wide/16 v20, 0x0

    #@379
    const-string v22, "scaleY"

    #@37b
    const/high16 v23, 0x3f80

    #@37d
    const/16 v24, 0x1

    #@37f
    invoke-virtual/range {v17 .. v24}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@382
    .line 299
    move-object/from16 v0, p0

    #@384
    iget-object v0, v0, Lcom/android/internal/widget/WaveView;->mUnlockHalo:Lcom/android/internal/widget/DrawableHolder;

    #@386
    move-object/from16 v17, v0

    #@388
    const-wide/16 v18, 0x0

    #@38a
    const-wide/16 v20, 0x0

    #@38c
    const-string v22, "alpha"

    #@38e
    const/high16 v23, 0x3f80

    #@390
    const/16 v24, 0x1

    #@392
    invoke-virtual/range {v17 .. v24}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@395
    goto/16 :goto_4c

    #@397
    .line 302
    :cond_397
    const/4 v4, 0x4

    #@398
    move-object/from16 v0, p0

    #@39a
    iput v4, v0, Lcom/android/internal/widget/WaveView;->mLockState:I

    #@39c
    goto/16 :goto_4c

    #@39e
    .line 306
    :cond_39e
    move-object/from16 v0, p0

    #@3a0
    iget-boolean v4, v0, Lcom/android/internal/widget/WaveView;->mWavesRunning:Z

    #@3a2
    if-nez v4, :cond_3bb

    #@3a4
    .line 307
    const/4 v4, 0x1

    #@3a5
    move-object/from16 v0, p0

    #@3a7
    iput-boolean v4, v0, Lcom/android/internal/widget/WaveView;->mWavesRunning:Z

    #@3a9
    .line 308
    const/4 v4, 0x0

    #@3aa
    move-object/from16 v0, p0

    #@3ac
    iput-boolean v4, v0, Lcom/android/internal/widget/WaveView;->mFinishWaves:Z

    #@3ae
    .line 310
    move-object/from16 v0, p0

    #@3b0
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mAddWaveAction:Ljava/lang/Runnable;

    #@3b2
    move-object/from16 v0, p0

    #@3b4
    iget-wide v10, v0, Lcom/android/internal/widget/WaveView;->mWaveTimerDelay:J

    #@3b6
    move-object/from16 v0, p0

    #@3b8
    invoke-virtual {v0, v4, v10, v11}, Lcom/android/internal/widget/WaveView;->postDelayed(Ljava/lang/Runnable;J)Z

    #@3bb
    .line 312
    :cond_3bb
    move-object/from16 v0, p0

    #@3bd
    iget-object v0, v0, Lcom/android/internal/widget/WaveView;->mUnlockHalo:Lcom/android/internal/widget/DrawableHolder;

    #@3bf
    move-object/from16 v17, v0

    #@3c1
    const-wide/16 v18, 0x0

    #@3c3
    const-wide/16 v20, 0x0

    #@3c5
    const-string v22, "x"

    #@3c7
    const/16 v24, 0x1

    #@3c9
    move/from16 v23, p1

    #@3cb
    invoke-virtual/range {v17 .. v24}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@3ce
    .line 313
    move-object/from16 v0, p0

    #@3d0
    iget-object v0, v0, Lcom/android/internal/widget/WaveView;->mUnlockHalo:Lcom/android/internal/widget/DrawableHolder;

    #@3d2
    move-object/from16 v17, v0

    #@3d4
    const-wide/16 v18, 0x0

    #@3d6
    const-wide/16 v20, 0x0

    #@3d8
    const-string v22, "y"

    #@3da
    const/16 v24, 0x1

    #@3dc
    move/from16 v23, p2

    #@3de
    invoke-virtual/range {v17 .. v24}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@3e1
    .line 314
    move-object/from16 v0, p0

    #@3e3
    iget-object v0, v0, Lcom/android/internal/widget/WaveView;->mUnlockHalo:Lcom/android/internal/widget/DrawableHolder;

    #@3e5
    move-object/from16 v17, v0

    #@3e7
    const-wide/16 v18, 0x0

    #@3e9
    const-wide/16 v20, 0x0

    #@3eb
    const-string v22, "scaleX"

    #@3ed
    const/high16 v23, 0x3f80

    #@3ef
    const/16 v24, 0x1

    #@3f1
    invoke-virtual/range {v17 .. v24}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@3f4
    .line 315
    move-object/from16 v0, p0

    #@3f6
    iget-object v0, v0, Lcom/android/internal/widget/WaveView;->mUnlockHalo:Lcom/android/internal/widget/DrawableHolder;

    #@3f8
    move-object/from16 v17, v0

    #@3fa
    const-wide/16 v18, 0x0

    #@3fc
    const-wide/16 v20, 0x0

    #@3fe
    const-string v22, "scaleY"

    #@400
    const/high16 v23, 0x3f80

    #@402
    const/16 v24, 0x1

    #@404
    invoke-virtual/range {v17 .. v24}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@407
    .line 316
    move-object/from16 v0, p0

    #@409
    iget-object v0, v0, Lcom/android/internal/widget/WaveView;->mUnlockHalo:Lcom/android/internal/widget/DrawableHolder;

    #@40b
    move-object/from16 v17, v0

    #@40d
    const-wide/16 v18, 0x0

    #@40f
    const-wide/16 v20, 0x0

    #@411
    const-string v22, "alpha"

    #@413
    const/high16 v23, 0x3f80

    #@415
    const/16 v24, 0x1

    #@417
    invoke-virtual/range {v17 .. v24}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@41a
    goto/16 :goto_4c

    #@41c
    .line 322
    :pswitch_41c
    move-object/from16 v0, p0

    #@41e
    iget v4, v0, Lcom/android/internal/widget/WaveView;->mSnapRadius:I

    #@420
    move/from16 v0, v29

    #@422
    if-le v0, v4, :cond_68e

    #@424
    .line 323
    const/16 v31, 0x0

    #@426
    .local v31, n:I
    :goto_426
    move-object/from16 v0, p0

    #@428
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mLightWaves:Ljava/util/ArrayList;

    #@42a
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@42d
    move-result v4

    #@42e
    move/from16 v0, v31

    #@430
    if-ge v0, v4, :cond_492

    #@432
    .line 324
    move-object/from16 v0, p0

    #@434
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mLightWaves:Ljava/util/ArrayList;

    #@436
    move/from16 v0, v31

    #@438
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@43b
    move-result-object v3

    #@43c
    check-cast v3, Lcom/android/internal/widget/DrawableHolder;

    #@43e
    .line 325
    .local v3, wave:Lcom/android/internal/widget/DrawableHolder;
    const-wide/16 v4, 0x3e8

    #@440
    add-int/lit8 v8, v31, 0x6

    #@442
    move-object/from16 v0, p0

    #@444
    iget v10, v0, Lcom/android/internal/widget/WaveView;->mCurrentWave:I

    #@446
    sub-int/2addr v8, v10

    #@447
    int-to-long v10, v8

    #@448
    mul-long/2addr v4, v10

    #@449
    const-wide/16 v10, 0xa

    #@44b
    div-long v6, v4, v10

    #@44d
    .line 326
    .local v6, delay:J
    const-wide/16 v4, 0xc8

    #@44f
    const-string v8, "x"

    #@451
    const/4 v10, 0x1

    #@452
    invoke-virtual/range {v3 .. v10}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@455
    .line 327
    const-wide/16 v11, 0xc8

    #@457
    const-string v15, "y"

    #@459
    const/16 v17, 0x1

    #@45b
    move-object v10, v3

    #@45c
    move-wide v13, v6

    #@45d
    invoke-virtual/range {v10 .. v17}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@460
    .line 328
    const-wide/16 v18, 0xc8

    #@462
    const-string v22, "scaleX"

    #@464
    const v23, 0x3dcccccd

    #@467
    const/16 v24, 0x1

    #@469
    move-object/from16 v17, v3

    #@46b
    move-wide/from16 v20, v6

    #@46d
    invoke-virtual/range {v17 .. v24}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@470
    .line 329
    const-wide/16 v18, 0xc8

    #@472
    const-string v22, "scaleY"

    #@474
    const v23, 0x3dcccccd

    #@477
    const/16 v24, 0x1

    #@479
    move-object/from16 v17, v3

    #@47b
    move-wide/from16 v20, v6

    #@47d
    invoke-virtual/range {v17 .. v24}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@480
    .line 330
    const-wide/16 v18, 0xc8

    #@482
    const-string v22, "alpha"

    #@484
    const/16 v23, 0x0

    #@486
    const/16 v24, 0x1

    #@488
    move-object/from16 v17, v3

    #@48a
    move-wide/from16 v20, v6

    #@48c
    invoke-virtual/range {v17 .. v24}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@48f
    .line 323
    add-int/lit8 v31, v31, 0x1

    #@491
    goto :goto_426

    #@492
    .line 332
    .end local v3           #wave:Lcom/android/internal/widget/DrawableHolder;
    .end local v6           #delay:J
    :cond_492
    const/16 v30, 0x0

    #@494
    .restart local v30       #i:I
    :goto_494
    move-object/from16 v0, p0

    #@496
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mLightWaves:Ljava/util/ArrayList;

    #@498
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@49b
    move-result v4

    #@49c
    move/from16 v0, v30

    #@49e
    if-ge v0, v4, :cond_4b4

    #@4a0
    .line 333
    move-object/from16 v0, p0

    #@4a2
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mLightWaves:Ljava/util/ArrayList;

    #@4a4
    move/from16 v0, v30

    #@4a6
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@4a9
    move-result-object v4

    #@4aa
    check-cast v4, Lcom/android/internal/widget/DrawableHolder;

    #@4ac
    move-object/from16 v0, p0

    #@4ae
    invoke-virtual {v4, v0}, Lcom/android/internal/widget/DrawableHolder;->startAnimations(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    #@4b1
    .line 332
    add-int/lit8 v30, v30, 0x1

    #@4b3
    goto :goto_494

    #@4b4
    .line 336
    :cond_4b4
    move-object/from16 v0, p0

    #@4b6
    iget-object v0, v0, Lcom/android/internal/widget/WaveView;->mUnlockRing:Lcom/android/internal/widget/DrawableHolder;

    #@4b8
    move-object/from16 v17, v0

    #@4ba
    const-wide/16 v18, 0xc8

    #@4bc
    const-wide/16 v20, 0x0

    #@4be
    const-string v22, "x"

    #@4c0
    const/16 v24, 0x0

    #@4c2
    move/from16 v23, v9

    #@4c4
    invoke-virtual/range {v17 .. v24}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@4c7
    .line 337
    move-object/from16 v0, p0

    #@4c9
    iget-object v10, v0, Lcom/android/internal/widget/WaveView;->mUnlockRing:Lcom/android/internal/widget/DrawableHolder;

    #@4cb
    const-wide/16 v11, 0xc8

    #@4cd
    const-wide/16 v13, 0x0

    #@4cf
    const-string v15, "y"

    #@4d1
    const/16 v17, 0x0

    #@4d3
    invoke-virtual/range {v10 .. v17}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@4d6
    .line 338
    move-object/from16 v0, p0

    #@4d8
    iget-object v0, v0, Lcom/android/internal/widget/WaveView;->mUnlockRing:Lcom/android/internal/widget/DrawableHolder;

    #@4da
    move-object/from16 v17, v0

    #@4dc
    const-wide/16 v18, 0xc8

    #@4de
    const-wide/16 v20, 0x0

    #@4e0
    const-string v22, "scaleX"

    #@4e2
    const v23, 0x3dcccccd

    #@4e5
    const/16 v24, 0x0

    #@4e7
    invoke-virtual/range {v17 .. v24}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@4ea
    .line 339
    move-object/from16 v0, p0

    #@4ec
    iget-object v0, v0, Lcom/android/internal/widget/WaveView;->mUnlockRing:Lcom/android/internal/widget/DrawableHolder;

    #@4ee
    move-object/from16 v17, v0

    #@4f0
    const-wide/16 v18, 0xc8

    #@4f2
    const-wide/16 v20, 0x0

    #@4f4
    const-string v22, "scaleY"

    #@4f6
    const v23, 0x3dcccccd

    #@4f9
    const/16 v24, 0x0

    #@4fb
    invoke-virtual/range {v17 .. v24}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@4fe
    .line 340
    move-object/from16 v0, p0

    #@500
    iget-object v0, v0, Lcom/android/internal/widget/WaveView;->mUnlockRing:Lcom/android/internal/widget/DrawableHolder;

    #@502
    move-object/from16 v17, v0

    #@504
    const-wide/16 v18, 0xc8

    #@506
    const-wide/16 v20, 0x0

    #@508
    const-string v22, "alpha"

    #@50a
    const/16 v23, 0x0

    #@50c
    const/16 v24, 0x0

    #@50e
    invoke-virtual/range {v17 .. v24}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@511
    .line 342
    move-object/from16 v0, p0

    #@513
    iget-object v0, v0, Lcom/android/internal/widget/WaveView;->mUnlockRing:Lcom/android/internal/widget/DrawableHolder;

    #@515
    move-object/from16 v17, v0

    #@517
    const-wide/16 v18, 0xc8

    #@519
    const-wide/16 v20, 0xc8

    #@51b
    const-string v22, "alpha"

    #@51d
    const/16 v23, 0x0

    #@51f
    const/16 v24, 0x0

    #@521
    invoke-virtual/range {v17 .. v24}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@524
    .line 344
    move-object/from16 v0, p0

    #@526
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

    #@528
    const-string v5, "x"

    #@52a
    invoke-virtual {v4, v5}, Lcom/android/internal/widget/DrawableHolder;->removeAnimationFor(Ljava/lang/String;)V

    #@52d
    .line 345
    move-object/from16 v0, p0

    #@52f
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

    #@531
    const-string v5, "y"

    #@533
    invoke-virtual {v4, v5}, Lcom/android/internal/widget/DrawableHolder;->removeAnimationFor(Ljava/lang/String;)V

    #@536
    .line 346
    move-object/from16 v0, p0

    #@538
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

    #@53a
    const-string v5, "scaleX"

    #@53c
    invoke-virtual {v4, v5}, Lcom/android/internal/widget/DrawableHolder;->removeAnimationFor(Ljava/lang/String;)V

    #@53f
    .line 347
    move-object/from16 v0, p0

    #@541
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

    #@543
    const-string v5, "scaleY"

    #@545
    invoke-virtual {v4, v5}, Lcom/android/internal/widget/DrawableHolder;->removeAnimationFor(Ljava/lang/String;)V

    #@548
    .line 348
    move-object/from16 v0, p0

    #@54a
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

    #@54c
    const-string v5, "alpha"

    #@54e
    invoke-virtual {v4, v5}, Lcom/android/internal/widget/DrawableHolder;->removeAnimationFor(Ljava/lang/String;)V

    #@551
    .line 349
    move-object/from16 v0, p0

    #@553
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

    #@555
    invoke-virtual {v4, v9}, Lcom/android/internal/widget/DrawableHolder;->setX(F)V

    #@558
    .line 350
    move-object/from16 v0, p0

    #@55a
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

    #@55c
    move/from16 v0, v16

    #@55e
    invoke-virtual {v4, v0}, Lcom/android/internal/widget/DrawableHolder;->setY(F)V

    #@561
    .line 351
    move-object/from16 v0, p0

    #@563
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

    #@565
    const v5, 0x3dcccccd

    #@568
    invoke-virtual {v4, v5}, Lcom/android/internal/widget/DrawableHolder;->setScaleX(F)V

    #@56b
    .line 352
    move-object/from16 v0, p0

    #@56d
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

    #@56f
    const v5, 0x3dcccccd

    #@572
    invoke-virtual {v4, v5}, Lcom/android/internal/widget/DrawableHolder;->setScaleY(F)V

    #@575
    .line 353
    move-object/from16 v0, p0

    #@577
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

    #@579
    const/4 v5, 0x0

    #@57a
    invoke-virtual {v4, v5}, Lcom/android/internal/widget/DrawableHolder;->setAlpha(F)V

    #@57d
    .line 355
    move-object/from16 v0, p0

    #@57f
    iget-object v0, v0, Lcom/android/internal/widget/WaveView;->mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

    #@581
    move-object/from16 v17, v0

    #@583
    const-wide/16 v18, 0xc8

    #@585
    const-wide/16 v20, 0x0

    #@587
    const-string v22, "x"

    #@589
    const/16 v24, 0x1

    #@58b
    move/from16 v23, v9

    #@58d
    invoke-virtual/range {v17 .. v24}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@590
    .line 356
    move-object/from16 v0, p0

    #@592
    iget-object v10, v0, Lcom/android/internal/widget/WaveView;->mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

    #@594
    const-wide/16 v11, 0xc8

    #@596
    const-wide/16 v13, 0x0

    #@598
    const-string v15, "y"

    #@59a
    const/16 v17, 0x1

    #@59c
    invoke-virtual/range {v10 .. v17}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@59f
    .line 357
    move-object/from16 v0, p0

    #@5a1
    iget-object v0, v0, Lcom/android/internal/widget/WaveView;->mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

    #@5a3
    move-object/from16 v17, v0

    #@5a5
    const-wide/16 v18, 0xc8

    #@5a7
    const-wide/16 v20, 0x0

    #@5a9
    const-string v22, "scaleX"

    #@5ab
    const/high16 v23, 0x3f80

    #@5ad
    const/16 v24, 0x1

    #@5af
    invoke-virtual/range {v17 .. v24}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@5b2
    .line 358
    move-object/from16 v0, p0

    #@5b4
    iget-object v0, v0, Lcom/android/internal/widget/WaveView;->mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

    #@5b6
    move-object/from16 v17, v0

    #@5b8
    const-wide/16 v18, 0xc8

    #@5ba
    const-wide/16 v20, 0x0

    #@5bc
    const-string v22, "scaleY"

    #@5be
    const/high16 v23, 0x3f80

    #@5c0
    const/16 v24, 0x1

    #@5c2
    invoke-virtual/range {v17 .. v24}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@5c5
    .line 359
    move-object/from16 v0, p0

    #@5c7
    iget-object v0, v0, Lcom/android/internal/widget/WaveView;->mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

    #@5c9
    move-object/from16 v17, v0

    #@5cb
    const-wide/16 v18, 0xc8

    #@5cd
    const-wide/16 v20, 0x0

    #@5cf
    const-string v22, "alpha"

    #@5d1
    const/high16 v23, 0x3f80

    #@5d3
    const/16 v24, 0x1

    #@5d5
    invoke-virtual/range {v17 .. v24}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@5d8
    .line 361
    move-object/from16 v0, p0

    #@5da
    iget-object v0, v0, Lcom/android/internal/widget/WaveView;->mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

    #@5dc
    move-object/from16 v17, v0

    #@5de
    const-wide/16 v18, 0xc8

    #@5e0
    const-wide/16 v20, 0xc8

    #@5e2
    const-string v22, "scaleX"

    #@5e4
    const/high16 v23, 0x4040

    #@5e6
    const/16 v24, 0x0

    #@5e8
    invoke-virtual/range {v17 .. v24}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@5eb
    .line 362
    move-object/from16 v0, p0

    #@5ed
    iget-object v0, v0, Lcom/android/internal/widget/WaveView;->mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

    #@5ef
    move-object/from16 v17, v0

    #@5f1
    const-wide/16 v18, 0xc8

    #@5f3
    const-wide/16 v20, 0xc8

    #@5f5
    const-string v22, "scaleY"

    #@5f7
    const/high16 v23, 0x4040

    #@5f9
    const/16 v24, 0x0

    #@5fb
    invoke-virtual/range {v17 .. v24}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@5fe
    .line 363
    move-object/from16 v0, p0

    #@600
    iget-object v0, v0, Lcom/android/internal/widget/WaveView;->mUnlockDefault:Lcom/android/internal/widget/DrawableHolder;

    #@602
    move-object/from16 v17, v0

    #@604
    const-wide/16 v18, 0xc8

    #@606
    const-wide/16 v20, 0xc8

    #@608
    const-string v22, "alpha"

    #@60a
    const/16 v23, 0x0

    #@60c
    const/16 v24, 0x0

    #@60e
    invoke-virtual/range {v17 .. v24}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@611
    .line 365
    move-object/from16 v0, p0

    #@613
    iget-object v0, v0, Lcom/android/internal/widget/WaveView;->mUnlockHalo:Lcom/android/internal/widget/DrawableHolder;

    #@615
    move-object/from16 v17, v0

    #@617
    const-wide/16 v18, 0xc8

    #@619
    const-wide/16 v20, 0x0

    #@61b
    const-string v22, "x"

    #@61d
    const/16 v24, 0x0

    #@61f
    move/from16 v23, v9

    #@621
    invoke-virtual/range {v17 .. v24}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@624
    .line 366
    move-object/from16 v0, p0

    #@626
    iget-object v10, v0, Lcom/android/internal/widget/WaveView;->mUnlockHalo:Lcom/android/internal/widget/DrawableHolder;

    #@628
    const-wide/16 v11, 0xc8

    #@62a
    const-wide/16 v13, 0x0

    #@62c
    const-string v15, "y"

    #@62e
    const/16 v17, 0x0

    #@630
    invoke-virtual/range {v10 .. v17}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@633
    .line 368
    move-object/from16 v0, p0

    #@635
    iget-object v0, v0, Lcom/android/internal/widget/WaveView;->mUnlockHalo:Lcom/android/internal/widget/DrawableHolder;

    #@637
    move-object/from16 v17, v0

    #@639
    const-wide/16 v18, 0xc8

    #@63b
    const-wide/16 v20, 0xc8

    #@63d
    const-string v22, "scaleX"

    #@63f
    const/high16 v23, 0x4040

    #@641
    const/16 v24, 0x0

    #@643
    invoke-virtual/range {v17 .. v24}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@646
    .line 369
    move-object/from16 v0, p0

    #@648
    iget-object v0, v0, Lcom/android/internal/widget/WaveView;->mUnlockHalo:Lcom/android/internal/widget/DrawableHolder;

    #@64a
    move-object/from16 v17, v0

    #@64c
    const-wide/16 v18, 0xc8

    #@64e
    const-wide/16 v20, 0xc8

    #@650
    const-string v22, "scaleY"

    #@652
    const/high16 v23, 0x4040

    #@654
    const/16 v24, 0x0

    #@656
    invoke-virtual/range {v17 .. v24}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@659
    .line 370
    move-object/from16 v0, p0

    #@65b
    iget-object v0, v0, Lcom/android/internal/widget/WaveView;->mUnlockHalo:Lcom/android/internal/widget/DrawableHolder;

    #@65d
    move-object/from16 v17, v0

    #@65f
    const-wide/16 v18, 0xc8

    #@661
    const-wide/16 v20, 0xc8

    #@663
    const-string v22, "alpha"

    #@665
    const/16 v23, 0x0

    #@667
    const/16 v24, 0x0

    #@669
    invoke-virtual/range {v17 .. v24}, Lcom/android/internal/widget/DrawableHolder;->addAnimTo(JJLjava/lang/String;FZ)Landroid/animation/ObjectAnimator;

    #@66c
    .line 372
    move-object/from16 v0, p0

    #@66e
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mLockTimerActions:Ljava/lang/Runnable;

    #@670
    move-object/from16 v0, p0

    #@672
    invoke-virtual {v0, v4}, Lcom/android/internal/widget/WaveView;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@675
    .line 374
    move-object/from16 v0, p0

    #@677
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mLockTimerActions:Ljava/lang/Runnable;

    #@679
    const-wide/16 v10, 0xbb8

    #@67b
    move-object/from16 v0, p0

    #@67d
    invoke-virtual {v0, v4, v10, v11}, Lcom/android/internal/widget/WaveView;->postDelayed(Ljava/lang/Runnable;J)Z

    #@680
    .line 376
    const/16 v4, 0xa

    #@682
    move-object/from16 v0, p0

    #@684
    invoke-direct {v0, v4}, Lcom/android/internal/widget/WaveView;->dispatchTriggerEvent(I)V

    #@687
    .line 377
    const/4 v4, 0x5

    #@688
    move-object/from16 v0, p0

    #@68a
    iput v4, v0, Lcom/android/internal/widget/WaveView;->mLockState:I

    #@68c
    goto/16 :goto_4c

    #@68e
    .line 379
    .end local v30           #i:I
    .end local v31           #n:I
    :cond_68e
    const/4 v4, 0x0

    #@68f
    move-object/from16 v0, p0

    #@691
    iput v4, v0, Lcom/android/internal/widget/WaveView;->mLockState:I

    #@693
    goto/16 :goto_4c

    #@695
    .line 385
    :pswitch_695
    move-object/from16 v0, p0

    #@697
    iget-object v4, v0, Lcom/android/internal/widget/WaveView;->mAddWaveAction:Ljava/lang/Runnable;

    #@699
    move-object/from16 v0, p0

    #@69b
    invoke-virtual {v0, v4}, Lcom/android/internal/widget/WaveView;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@69e
    goto/16 :goto_4c

    #@6a0
    .line 208
    :pswitch_data_6a0
    .packed-switch 0x0
        :pswitch_68
        :pswitch_25f
        :pswitch_267
        :pswitch_330
        :pswitch_41c
        :pswitch_695
    .end packed-switch
.end method


# virtual methods
.method createDrawable(I)Landroid/graphics/drawable/BitmapDrawable;
    .registers 5
    .parameter "resId"

    #@0
    .prologue
    .line 398
    invoke-virtual {p0}, Lcom/android/internal/widget/WaveView;->getResources()Landroid/content/res/Resources;

    #@3
    move-result-object v1

    #@4
    .line 399
    .local v1, res:Landroid/content/res/Resources;
    invoke-static {v1, p1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    #@7
    move-result-object v0

    #@8
    .line 400
    .local v0, bitmap:Landroid/graphics/Bitmap;
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    #@a
    invoke-direct {v2, v1, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    #@d
    return-object v2
.end method

.method protected getSuggestedMinimumHeight()I
    .registers 3

    #@0
    .prologue
    .line 136
    iget-object v0, p0, Lcom/android/internal/widget/WaveView;->mUnlockRing:Lcom/android/internal/widget/DrawableHolder;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/widget/DrawableHolder;->getHeight()I

    #@5
    move-result v0

    #@6
    iget-object v1, p0, Lcom/android/internal/widget/WaveView;->mUnlockHalo:Lcom/android/internal/widget/DrawableHolder;

    #@8
    invoke-virtual {v1}, Lcom/android/internal/widget/DrawableHolder;->getHeight()I

    #@b
    move-result v1

    #@c
    add-int/2addr v0, v1

    #@d
    return v0
.end method

.method protected getSuggestedMinimumWidth()I
    .registers 3

    #@0
    .prologue
    .line 130
    iget-object v0, p0, Lcom/android/internal/widget/WaveView;->mUnlockRing:Lcom/android/internal/widget/DrawableHolder;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/widget/DrawableHolder;->getWidth()I

    #@5
    move-result v0

    #@6
    iget-object v1, p0, Lcom/android/internal/widget/WaveView;->mUnlockHalo:Lcom/android/internal/widget/DrawableHolder;

    #@8
    invoke-virtual {v1}, Lcom/android/internal/widget/DrawableHolder;->getWidth()I

    #@b
    move-result v1

    #@c
    add-int/2addr v0, v1

    #@d
    return v0
.end method

.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .registers 2
    .parameter "animation"

    #@0
    .prologue
    .line 650
    invoke-virtual {p0}, Lcom/android/internal/widget/WaveView;->invalidate()V

    #@3
    .line 651
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 6
    .parameter "canvas"

    #@0
    .prologue
    .line 405
    iget v1, p0, Lcom/android/internal/widget/WaveView;->mMouseX:F

    #@2
    iget v2, p0, Lcom/android/internal/widget/WaveView;->mMouseY:F

    #@4
    iget-boolean v3, p0, Lcom/android/internal/widget/WaveView;->mFingerDown:Z

    #@6
    invoke-direct {p0, v1, v2, v3}, Lcom/android/internal/widget/WaveView;->waveUpdateFrame(FFZ)V

    #@9
    .line 406
    const/4 v0, 0x0

    #@a
    .local v0, i:I
    :goto_a
    iget-object v1, p0, Lcom/android/internal/widget/WaveView;->mDrawables:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@f
    move-result v1

    #@10
    if-ge v0, v1, :cond_20

    #@12
    .line 407
    iget-object v1, p0, Lcom/android/internal/widget/WaveView;->mDrawables:Ljava/util/ArrayList;

    #@14
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@17
    move-result-object v1

    #@18
    check-cast v1, Lcom/android/internal/widget/DrawableHolder;

    #@1a
    invoke-virtual {v1, p1}, Lcom/android/internal/widget/DrawableHolder;->draw(Landroid/graphics/Canvas;)V

    #@1d
    .line 406
    add-int/lit8 v0, v0, 0x1

    #@1f
    goto :goto_a

    #@20
    .line 409
    :cond_20
    const/4 v0, 0x0

    #@21
    :goto_21
    iget-object v1, p0, Lcom/android/internal/widget/WaveView;->mLightWaves:Ljava/util/ArrayList;

    #@23
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@26
    move-result v1

    #@27
    if-ge v0, v1, :cond_37

    #@29
    .line 410
    iget-object v1, p0, Lcom/android/internal/widget/WaveView;->mLightWaves:Ljava/util/ArrayList;

    #@2b
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@2e
    move-result-object v1

    #@2f
    check-cast v1, Lcom/android/internal/widget/DrawableHolder;

    #@31
    invoke-virtual {v1, p1}, Lcom/android/internal/widget/DrawableHolder;->draw(Landroid/graphics/Canvas;)V

    #@34
    .line 409
    add-int/lit8 v0, v0, 0x1

    #@36
    goto :goto_21

    #@37
    .line 412
    :cond_37
    return-void
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .registers 4
    .parameter "event"

    #@0
    .prologue
    .line 472
    iget-object v1, p0, Lcom/android/internal/widget/WaveView;->mContext:Landroid/content/Context;

    #@2
    invoke-static {v1}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_19

    #@c
    .line 473
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@f
    move-result v0

    #@10
    .line 474
    .local v0, action:I
    packed-switch v0, :pswitch_data_2e

    #@13
    .line 485
    :goto_13
    :pswitch_13
    invoke-virtual {p0, p1}, Lcom/android/internal/widget/WaveView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    #@16
    .line 486
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->setAction(I)V

    #@19
    .line 488
    .end local v0           #action:I
    :cond_19
    invoke-super {p0, p1}, Landroid/view/View;->onHoverEvent(Landroid/view/MotionEvent;)Z

    #@1c
    move-result v1

    #@1d
    return v1

    #@1e
    .line 476
    .restart local v0       #action:I
    :pswitch_1e
    const/4 v1, 0x0

    #@1f
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->setAction(I)V

    #@22
    goto :goto_13

    #@23
    .line 479
    :pswitch_23
    const/4 v1, 0x2

    #@24
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->setAction(I)V

    #@27
    goto :goto_13

    #@28
    .line 482
    :pswitch_28
    const/4 v1, 0x1

    #@29
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->setAction(I)V

    #@2c
    goto :goto_13

    #@2d
    .line 474
    nop

    #@2e
    :pswitch_data_2e
    .packed-switch 0x7
        :pswitch_23
        :pswitch_13
        :pswitch_1e
        :pswitch_28
    .end packed-switch
.end method

.method protected onMeasure(II)V
    .registers 12
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    const/high16 v8, 0x4000

    #@2
    const/high16 v7, -0x8000

    #@4
    .line 141
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@7
    move-result v4

    #@8
    .line 142
    .local v4, widthSpecMode:I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@b
    move-result v1

    #@c
    .line 143
    .local v1, heightSpecMode:I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@f
    move-result v5

    #@10
    .line 144
    .local v5, widthSpecSize:I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@13
    move-result v2

    #@14
    .line 148
    .local v2, heightSpecSize:I
    if-ne v4, v7, :cond_2c

    #@16
    .line 149
    invoke-virtual {p0}, Lcom/android/internal/widget/WaveView;->getSuggestedMinimumWidth()I

    #@19
    move-result v6

    #@1a
    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    #@1d
    move-result v3

    #@1e
    .line 156
    .local v3, width:I
    :goto_1e
    if-ne v1, v7, :cond_35

    #@20
    .line 157
    invoke-virtual {p0}, Lcom/android/internal/widget/WaveView;->getSuggestedMinimumWidth()I

    #@23
    move-result v6

    #@24
    invoke-static {v2, v6}, Ljava/lang/Math;->min(II)I

    #@27
    move-result v0

    #@28
    .line 164
    .local v0, height:I
    :goto_28
    invoke-virtual {p0, v3, v0}, Lcom/android/internal/widget/WaveView;->setMeasuredDimension(II)V

    #@2b
    .line 165
    return-void

    #@2c
    .line 150
    .end local v0           #height:I
    .end local v3           #width:I
    :cond_2c
    if-ne v4, v8, :cond_30

    #@2e
    .line 151
    move v3, v5

    #@2f
    .restart local v3       #width:I
    goto :goto_1e

    #@30
    .line 153
    .end local v3           #width:I
    :cond_30
    invoke-virtual {p0}, Lcom/android/internal/widget/WaveView;->getSuggestedMinimumWidth()I

    #@33
    move-result v3

    #@34
    .restart local v3       #width:I
    goto :goto_1e

    #@35
    .line 158
    :cond_35
    if-ne v1, v8, :cond_39

    #@37
    .line 159
    move v0, v2

    #@38
    .restart local v0       #height:I
    goto :goto_28

    #@39
    .line 161
    .end local v0           #height:I
    :cond_39
    invoke-virtual {p0}, Lcom/android/internal/widget/WaveView;->getSuggestedMinimumHeight()I

    #@3c
    move-result v0

    #@3d
    .restart local v0       #height:I
    goto :goto_28
.end method

.method protected onSizeChanged(IIII)V
    .registers 7
    .parameter "w"
    .parameter "h"
    .parameter "oldw"
    .parameter "oldh"

    #@0
    .prologue
    const/high16 v1, 0x3f00

    #@2
    .line 122
    int-to-float v0, p1

    #@3
    mul-float/2addr v0, v1

    #@4
    iput v0, p0, Lcom/android/internal/widget/WaveView;->mLockCenterX:F

    #@6
    .line 123
    int-to-float v0, p2

    #@7
    mul-float/2addr v0, v1

    #@8
    iput v0, p0, Lcom/android/internal/widget/WaveView;->mLockCenterY:F

    #@a
    .line 124
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    #@d
    .line 125
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 9
    .parameter "event"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 493
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@5
    move-result v0

    #@6
    .line 494
    .local v0, action:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@9
    move-result v3

    #@a
    iput v3, p0, Lcom/android/internal/widget/WaveView;->mMouseX:F

    #@c
    .line 495
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@f
    move-result v3

    #@10
    iput v3, p0, Lcom/android/internal/widget/WaveView;->mMouseY:F

    #@12
    .line 496
    const/4 v1, 0x0

    #@13
    .line 497
    .local v1, handled:Z
    packed-switch v0, :pswitch_data_4e

    #@16
    .line 529
    :goto_16
    invoke-virtual {p0}, Lcom/android/internal/widget/WaveView;->invalidate()V

    #@19
    .line 530
    if-eqz v1, :cond_48

    #@1b
    :goto_1b
    return v2

    #@1c
    .line 499
    :pswitch_1c
    iget-object v3, p0, Lcom/android/internal/widget/WaveView;->mLockTimerActions:Ljava/lang/Runnable;

    #@1e
    invoke-virtual {p0, v3}, Lcom/android/internal/widget/WaveView;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@21
    .line 500
    iput-boolean v2, p0, Lcom/android/internal/widget/WaveView;->mFingerDown:Z

    #@23
    .line 501
    invoke-direct {p0, p1}, Lcom/android/internal/widget/WaveView;->tryTransitionToStartAttemptState(Landroid/view/MotionEvent;)V

    #@26
    .line 502
    const/4 v1, 0x1

    #@27
    .line 503
    goto :goto_16

    #@28
    .line 506
    :pswitch_28
    invoke-direct {p0, p1}, Lcom/android/internal/widget/WaveView;->tryTransitionToStartAttemptState(Landroid/view/MotionEvent;)V

    #@2b
    .line 507
    const/4 v1, 0x1

    #@2c
    .line 508
    goto :goto_16

    #@2d
    .line 512
    :pswitch_2d
    iput-boolean v6, p0, Lcom/android/internal/widget/WaveView;->mFingerDown:Z

    #@2f
    .line 513
    iget-object v3, p0, Lcom/android/internal/widget/WaveView;->mLockTimerActions:Ljava/lang/Runnable;

    #@31
    const-wide/16 v4, 0xbb8

    #@33
    invoke-virtual {p0, v3, v4, v5}, Lcom/android/internal/widget/WaveView;->postDelayed(Ljava/lang/Runnable;J)Z

    #@36
    .line 514
    invoke-direct {p0, v6}, Lcom/android/internal/widget/WaveView;->setGrabbedState(I)V

    #@39
    .line 520
    iget v3, p0, Lcom/android/internal/widget/WaveView;->mMouseX:F

    #@3b
    iget v4, p0, Lcom/android/internal/widget/WaveView;->mMouseY:F

    #@3d
    iget-boolean v5, p0, Lcom/android/internal/widget/WaveView;->mFingerDown:Z

    #@3f
    invoke-direct {p0, v3, v4, v5}, Lcom/android/internal/widget/WaveView;->waveUpdateFrame(FFZ)V

    #@42
    .line 521
    const/4 v1, 0x1

    #@43
    .line 522
    goto :goto_16

    #@44
    .line 525
    :pswitch_44
    iput-boolean v6, p0, Lcom/android/internal/widget/WaveView;->mFingerDown:Z

    #@46
    .line 526
    const/4 v1, 0x1

    #@47
    goto :goto_16

    #@48
    .line 530
    :cond_48
    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    #@4b
    move-result v2

    #@4c
    goto :goto_1b

    #@4d
    .line 497
    nop

    #@4e
    :pswitch_data_4e
    .packed-switch 0x0
        :pswitch_1c
        :pswitch_2d
        :pswitch_28
        :pswitch_44
    .end packed-switch
.end method

.method public reset()V
    .registers 2

    #@0
    .prologue
    .line 655
    const/4 v0, 0x0

    #@1
    iput v0, p0, Lcom/android/internal/widget/WaveView;->mLockState:I

    #@3
    .line 656
    invoke-virtual {p0}, Lcom/android/internal/widget/WaveView;->invalidate()V

    #@6
    .line 657
    return-void
.end method

.method public setOnTriggerListener(Lcom/android/internal/widget/WaveView$OnTriggerListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 596
    iput-object p1, p0, Lcom/android/internal/widget/WaveView;->mOnTriggerListener:Lcom/android/internal/widget/WaveView$OnTriggerListener;

    #@2
    .line 597
    return-void
.end method
