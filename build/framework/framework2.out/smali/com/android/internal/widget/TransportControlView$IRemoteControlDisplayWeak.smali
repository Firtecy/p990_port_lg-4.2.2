.class Lcom/android/internal/widget/TransportControlView$IRemoteControlDisplayWeak;
.super Landroid/media/IRemoteControlDisplay$Stub;
.source "TransportControlView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/widget/TransportControlView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "IRemoteControlDisplayWeak"
.end annotation


# instance fields
.field private mLocalHandler:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/os/Handler;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/os/Handler;)V
    .registers 3
    .parameter "handler"

    #@0
    .prologue
    .line 142
    invoke-direct {p0}, Landroid/media/IRemoteControlDisplay$Stub;-><init>()V

    #@3
    .line 143
    new-instance v0, Ljava/lang/ref/WeakReference;

    #@5
    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@8
    iput-object v0, p0, Lcom/android/internal/widget/TransportControlView$IRemoteControlDisplayWeak;->mLocalHandler:Ljava/lang/ref/WeakReference;

    #@a
    .line 144
    return-void
.end method


# virtual methods
.method public setAllMetadata(ILandroid/os/Bundle;Landroid/graphics/Bitmap;)V
    .registers 7
    .parameter "generationId"
    .parameter "metadata"
    .parameter "bitmap"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 176
    iget-object v1, p0, Lcom/android/internal/widget/TransportControlView$IRemoteControlDisplayWeak;->mLocalHandler:Ljava/lang/ref/WeakReference;

    #@3
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@6
    move-result-object v0

    #@7
    check-cast v0, Landroid/os/Handler;

    #@9
    .line 177
    .local v0, handler:Landroid/os/Handler;
    if-eqz v0, :cond_1d

    #@b
    .line 178
    const/16 v1, 0x65

    #@d
    invoke-virtual {v0, v1, p1, v2, p2}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    #@14
    .line 179
    const/16 v1, 0x67

    #@16
    invoke-virtual {v0, v1, p1, v2, p3}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    #@1d
    .line 181
    :cond_1d
    return-void
.end method

.method public setArtwork(ILandroid/graphics/Bitmap;)V
    .registers 6
    .parameter "generationId"
    .parameter "bitmap"

    #@0
    .prologue
    .line 169
    iget-object v1, p0, Lcom/android/internal/widget/TransportControlView$IRemoteControlDisplayWeak;->mLocalHandler:Ljava/lang/ref/WeakReference;

    #@2
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/os/Handler;

    #@8
    .line 170
    .local v0, handler:Landroid/os/Handler;
    if-eqz v0, :cond_14

    #@a
    .line 171
    const/16 v1, 0x67

    #@c
    const/4 v2, 0x0

    #@d
    invoke-virtual {v0, v1, p1, v2, p2}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    #@14
    .line 173
    :cond_14
    return-void
.end method

.method public setCurrentClientId(ILandroid/app/PendingIntent;Z)V
    .registers 7
    .parameter "clientGeneration"
    .parameter "mediaIntent"
    .parameter "clearing"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 185
    iget-object v1, p0, Lcom/android/internal/widget/TransportControlView$IRemoteControlDisplayWeak;->mLocalHandler:Ljava/lang/ref/WeakReference;

    #@2
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/os/Handler;

    #@8
    .line 186
    .local v0, handler:Landroid/os/Handler;
    if-eqz v0, :cond_16

    #@a
    .line 187
    const/16 v2, 0x68

    #@c
    if-eqz p3, :cond_17

    #@e
    const/4 v1, 0x1

    #@f
    :goto_f
    invoke-virtual {v0, v2, p1, v1, p2}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    #@16
    .line 190
    :cond_16
    return-void

    #@17
    .line 187
    :cond_17
    const/4 v1, 0x0

    #@18
    goto :goto_f
.end method

.method public setMetadata(ILandroid/os/Bundle;)V
    .registers 6
    .parameter "generationId"
    .parameter "metadata"

    #@0
    .prologue
    .line 154
    iget-object v1, p0, Lcom/android/internal/widget/TransportControlView$IRemoteControlDisplayWeak;->mLocalHandler:Ljava/lang/ref/WeakReference;

    #@2
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/os/Handler;

    #@8
    .line 155
    .local v0, handler:Landroid/os/Handler;
    if-eqz v0, :cond_14

    #@a
    .line 156
    const/16 v1, 0x65

    #@c
    const/4 v2, 0x0

    #@d
    invoke-virtual {v0, v1, p1, v2, p2}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    #@14
    .line 158
    :cond_14
    return-void
.end method

.method public setPlaybackState(IIJ)V
    .registers 7
    .parameter "generationId"
    .parameter "state"
    .parameter "stateChangeTimeMs"

    #@0
    .prologue
    .line 147
    iget-object v1, p0, Lcom/android/internal/widget/TransportControlView$IRemoteControlDisplayWeak;->mLocalHandler:Ljava/lang/ref/WeakReference;

    #@2
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/os/Handler;

    #@8
    .line 148
    .local v0, handler:Landroid/os/Handler;
    if-eqz v0, :cond_13

    #@a
    .line 149
    const/16 v1, 0x64

    #@c
    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    #@13
    .line 151
    :cond_13
    return-void
.end method

.method public setTransportControlFlags(II)V
    .registers 5
    .parameter "generationId"
    .parameter "flags"

    #@0
    .prologue
    .line 161
    iget-object v1, p0, Lcom/android/internal/widget/TransportControlView$IRemoteControlDisplayWeak;->mLocalHandler:Ljava/lang/ref/WeakReference;

    #@2
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/os/Handler;

    #@8
    .line 162
    .local v0, handler:Landroid/os/Handler;
    if-eqz v0, :cond_13

    #@a
    .line 163
    const/16 v1, 0x66

    #@c
    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    #@13
    .line 166
    :cond_13
    return-void
.end method
