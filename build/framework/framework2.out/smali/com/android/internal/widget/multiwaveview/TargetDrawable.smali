.class public Lcom/android/internal/widget/multiwaveview/TargetDrawable;
.super Ljava/lang/Object;
.source "TargetDrawable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/widget/multiwaveview/TargetDrawable$DrawableWithAlpha;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field public static final STATE_ACTIVE:[I = null

.field public static final STATE_FOCUSED:[I = null

.field public static final STATE_INACTIVE:[I = null

.field private static final TAG:Ljava/lang/String; = "TargetDrawable"


# instance fields
.field private mAlpha:F

.field private mDrawable:Landroid/graphics/drawable/Drawable;

.field private mEnabled:Z

.field private mPositionX:F

.field private mPositionY:F

.field private final mResourceId:I

.field private mScaleX:F

.field private mScaleY:F

.field private mTranslationX:F

.field private mTranslationY:F


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    const/4 v1, 0x2

    #@1
    .line 30
    new-array v0, v1, [I

    #@3
    fill-array-data v0, :array_18

    #@6
    sput-object v0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->STATE_ACTIVE:[I

    #@8
    .line 32
    new-array v0, v1, [I

    #@a
    fill-array-data v0, :array_20

    #@d
    sput-object v0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->STATE_INACTIVE:[I

    #@f
    .line 34
    const/4 v0, 0x3

    #@10
    new-array v0, v0, [I

    #@12
    fill-array-data v0, :array_28

    #@15
    sput-object v0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->STATE_FOCUSED:[I

    #@17
    return-void

    #@18
    .line 30
    :array_18
    .array-data 0x4
        0x9et 0x0t 0x1t 0x1t
        0xa2t 0x0t 0x1t 0x1t
    .end array-data

    #@20
    .line 32
    :array_20
    .array-data 0x4
        0x9et 0x0t 0x1t 0x1t
        0x5et 0xfft 0xfet 0xfet
    .end array-data

    #@28
    .line 34
    :array_28
    .array-data 0x4
        0x9et 0x0t 0x1t 0x1t
        0x5et 0xfft 0xfet 0xfet
        0x9ct 0x0t 0x1t 0x1t
    .end array-data
.end method

.method public constructor <init>(Landroid/content/res/Resources;I)V
    .registers 5
    .parameter "res"
    .parameter "resId"

    #@0
    .prologue
    const/high16 v1, 0x3f80

    #@2
    const/4 v0, 0x0

    #@3
    .line 79
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 38
    iput v0, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mTranslationX:F

    #@8
    .line 39
    iput v0, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mTranslationY:F

    #@a
    .line 40
    iput v0, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mPositionX:F

    #@c
    .line 41
    iput v0, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mPositionY:F

    #@e
    .line 42
    iput v1, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mScaleX:F

    #@10
    .line 43
    iput v1, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mScaleY:F

    #@12
    .line 44
    iput v1, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mAlpha:F

    #@14
    .line 46
    const/4 v0, 0x1

    #@15
    iput-boolean v0, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mEnabled:Z

    #@17
    .line 80
    iput p2, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mResourceId:I

    #@19
    .line 81
    invoke-virtual {p0, p1, p2}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->setDrawable(Landroid/content/res/Resources;I)V

    #@1c
    .line 82
    return-void
.end method

.method public constructor <init>(Lcom/android/internal/widget/multiwaveview/TargetDrawable;)V
    .registers 4
    .parameter "other"

    #@0
    .prologue
    const/high16 v1, 0x3f80

    #@2
    const/4 v0, 0x0

    #@3
    .line 94
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 38
    iput v0, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mTranslationX:F

    #@8
    .line 39
    iput v0, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mTranslationY:F

    #@a
    .line 40
    iput v0, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mPositionX:F

    #@c
    .line 41
    iput v0, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mPositionY:F

    #@e
    .line 42
    iput v1, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mScaleX:F

    #@10
    .line 43
    iput v1, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mScaleY:F

    #@12
    .line 44
    iput v1, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mAlpha:F

    #@14
    .line 46
    const/4 v0, 0x1

    #@15
    iput-boolean v0, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mEnabled:Z

    #@17
    .line 95
    iget v0, p1, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mResourceId:I

    #@19
    iput v0, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mResourceId:I

    #@1b
    .line 97
    iget-object v0, p1, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@1d
    if-eqz v0, :cond_30

    #@1f
    iget-object v0, p1, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@21
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    #@24
    move-result-object v0

    #@25
    :goto_25
    iput-object v0, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@27
    .line 98
    invoke-direct {p0}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->resizeDrawables()V

    #@2a
    .line 99
    sget-object v0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->STATE_INACTIVE:[I

    #@2c
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->setState([I)V

    #@2f
    .line 100
    return-void

    #@30
    .line 97
    :cond_30
    const/4 v0, 0x0

    #@31
    goto :goto_25
.end method

.method private resizeDrawables()V
    .registers 10

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 152
    iget-object v5, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@3
    instance-of v5, v5, Landroid/graphics/drawable/StateListDrawable;

    #@5
    if-eqz v5, :cond_3f

    #@7
    .line 153
    iget-object v1, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@9
    check-cast v1, Landroid/graphics/drawable/StateListDrawable;

    #@b
    .line 154
    .local v1, d:Landroid/graphics/drawable/StateListDrawable;
    const/4 v4, 0x0

    #@c
    .line 155
    .local v4, maxWidth:I
    const/4 v3, 0x0

    #@d
    .line 156
    .local v3, maxHeight:I
    const/4 v2, 0x0

    #@e
    .local v2, i:I
    :goto_e
    invoke-virtual {v1}, Landroid/graphics/drawable/StateListDrawable;->getStateCount()I

    #@11
    move-result v5

    #@12
    if-ge v2, v5, :cond_2b

    #@14
    .line 157
    invoke-virtual {v1, v2}, Landroid/graphics/drawable/StateListDrawable;->getStateDrawable(I)Landroid/graphics/drawable/Drawable;

    #@17
    move-result-object v0

    #@18
    .line 158
    .local v0, childDrawable:Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@1b
    move-result v5

    #@1c
    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    #@1f
    move-result v4

    #@20
    .line 159
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@23
    move-result v5

    #@24
    invoke-static {v3, v5}, Ljava/lang/Math;->max(II)I

    #@27
    move-result v3

    #@28
    .line 156
    add-int/lit8 v2, v2, 0x1

    #@2a
    goto :goto_e

    #@2b
    .line 163
    .end local v0           #childDrawable:Landroid/graphics/drawable/Drawable;
    :cond_2b
    invoke-virtual {v1, v8, v8, v4, v3}, Landroid/graphics/drawable/StateListDrawable;->setBounds(IIII)V

    #@2e
    .line 164
    const/4 v2, 0x0

    #@2f
    :goto_2f
    invoke-virtual {v1}, Landroid/graphics/drawable/StateListDrawable;->getStateCount()I

    #@32
    move-result v5

    #@33
    if-ge v2, v5, :cond_54

    #@35
    .line 165
    invoke-virtual {v1, v2}, Landroid/graphics/drawable/StateListDrawable;->getStateDrawable(I)Landroid/graphics/drawable/Drawable;

    #@38
    move-result-object v0

    #@39
    .line 168
    .restart local v0       #childDrawable:Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0, v8, v8, v4, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@3c
    .line 164
    add-int/lit8 v2, v2, 0x1

    #@3e
    goto :goto_2f

    #@3f
    .line 170
    .end local v0           #childDrawable:Landroid/graphics/drawable/Drawable;
    .end local v1           #d:Landroid/graphics/drawable/StateListDrawable;
    .end local v2           #i:I
    .end local v3           #maxHeight:I
    .end local v4           #maxWidth:I
    :cond_3f
    iget-object v5, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@41
    if-eqz v5, :cond_54

    #@43
    .line 171
    iget-object v5, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@45
    iget-object v6, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@47
    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@4a
    move-result v6

    #@4b
    iget-object v7, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@4d
    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@50
    move-result v7

    #@51
    invoke-virtual {v5, v8, v8, v6, v7}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@54
    .line 174
    :cond_54
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .registers 7
    .parameter "canvas"

    #@0
    .prologue
    const/high16 v4, -0x4100

    #@2
    .line 241
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@4
    if-eqz v0, :cond_a

    #@6
    iget-boolean v0, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mEnabled:Z

    #@8
    if-nez v0, :cond_b

    #@a
    .line 251
    :cond_a
    :goto_a
    return-void

    #@b
    .line 244
    :cond_b
    const/4 v0, 0x1

    #@c
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->save(I)I

    #@f
    .line 245
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mScaleX:F

    #@11
    iget v1, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mScaleY:F

    #@13
    iget v2, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mPositionX:F

    #@15
    iget v3, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mPositionY:F

    #@17
    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->scale(FFFF)V

    #@1a
    .line 246
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mTranslationX:F

    #@1c
    iget v1, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mPositionX:F

    #@1e
    add-float/2addr v0, v1

    #@1f
    iget v1, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mTranslationY:F

    #@21
    iget v2, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mPositionY:F

    #@23
    add-float/2addr v1, v2

    #@24
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    #@27
    .line 247
    invoke-virtual {p0}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->getWidth()I

    #@2a
    move-result v0

    #@2b
    int-to-float v0, v0

    #@2c
    mul-float/2addr v0, v4

    #@2d
    invoke-virtual {p0}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->getHeight()I

    #@30
    move-result v1

    #@31
    int-to-float v1, v1

    #@32
    mul-float/2addr v1, v4

    #@33
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    #@36
    .line 248
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@38
    iget v1, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mAlpha:F

    #@3a
    const/high16 v2, 0x437f

    #@3c
    mul-float/2addr v1, v2

    #@3d
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    #@40
    move-result v1

    #@41
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    #@44
    .line 249
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@46
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@49
    .line 250
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    #@4c
    goto :goto_a
.end method

.method public getAlpha()F
    .registers 2

    #@0
    .prologue
    .line 213
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mAlpha:F

    #@2
    return v0
.end method

.method public getHeight()I
    .registers 2

    #@0
    .prologue
    .line 237
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@6
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@9
    move-result v0

    #@a
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public getPositionX()F
    .registers 2

    #@0
    .prologue
    .line 225
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mPositionX:F

    #@2
    return v0
.end method

.method public getPositionY()F
    .registers 2

    #@0
    .prologue
    .line 229
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mPositionY:F

    #@2
    return v0
.end method

.method public getResourceId()I
    .registers 2

    #@0
    .prologue
    .line 258
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mResourceId:I

    #@2
    return v0
.end method

.method public getScaleX()F
    .registers 2

    #@0
    .prologue
    .line 205
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mScaleX:F

    #@2
    return v0
.end method

.method public getScaleY()F
    .registers 2

    #@0
    .prologue
    .line 209
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mScaleY:F

    #@2
    return v0
.end method

.method public getWidth()I
    .registers 2

    #@0
    .prologue
    .line 233
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@6
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@9
    move-result v0

    #@a
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public getX()F
    .registers 2

    #@0
    .prologue
    .line 197
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mTranslationX:F

    #@2
    return v0
.end method

.method public getY()F
    .registers 2

    #@0
    .prologue
    .line 201
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mTranslationY:F

    #@2
    return v0
.end method

.method public hasState([I)Z
    .registers 6
    .parameter "state"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 110
    iget-object v2, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@3
    instance-of v2, v2, Landroid/graphics/drawable/StateListDrawable;

    #@5
    if-eqz v2, :cond_13

    #@7
    .line 111
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@9
    check-cast v0, Landroid/graphics/drawable/StateListDrawable;

    #@b
    .line 113
    .local v0, d:Landroid/graphics/drawable/StateListDrawable;
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/StateListDrawable;->getStateDrawableIndex([I)I

    #@e
    move-result v2

    #@f
    const/4 v3, -0x1

    #@10
    if-eq v2, v3, :cond_13

    #@12
    const/4 v1, 0x1

    #@13
    .line 115
    .end local v0           #d:Landroid/graphics/drawable/StateListDrawable;
    :cond_13
    return v1
.end method

.method public isActive()Z
    .registers 6

    #@0
    .prologue
    .line 124
    iget-object v3, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@2
    instance-of v3, v3, Landroid/graphics/drawable/StateListDrawable;

    #@4
    if-eqz v3, :cond_1e

    #@6
    .line 125
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@8
    check-cast v0, Landroid/graphics/drawable/StateListDrawable;

    #@a
    .line 126
    .local v0, d:Landroid/graphics/drawable/StateListDrawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/StateListDrawable;->getState()[I

    #@d
    move-result-object v2

    #@e
    .line 127
    .local v2, states:[I
    const/4 v1, 0x0

    #@f
    .local v1, i:I
    :goto_f
    array-length v3, v2

    #@10
    if-ge v1, v3, :cond_1e

    #@12
    .line 128
    aget v3, v2, v1

    #@14
    const v4, 0x101009c

    #@17
    if-ne v3, v4, :cond_1b

    #@19
    .line 129
    const/4 v3, 0x1

    #@1a
    .line 133
    .end local v0           #d:Landroid/graphics/drawable/StateListDrawable;
    .end local v1           #i:I
    .end local v2           #states:[I
    :goto_1a
    return v3

    #@1b
    .line 127
    .restart local v0       #d:Landroid/graphics/drawable/StateListDrawable;
    .restart local v1       #i:I
    .restart local v2       #states:[I
    :cond_1b
    add-int/lit8 v1, v1, 0x1

    #@1d
    goto :goto_f

    #@1e
    .line 133
    .end local v0           #d:Landroid/graphics/drawable/StateListDrawable;
    .end local v1           #i:I
    .end local v2           #states:[I
    :cond_1e
    const/4 v3, 0x0

    #@1f
    goto :goto_1a
.end method

.method public isEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 143
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@2
    if-eqz v0, :cond_a

    #@4
    iget-boolean v0, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mEnabled:Z

    #@6
    if-eqz v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public setAlpha(F)V
    .registers 2
    .parameter "alpha"

    #@0
    .prologue
    .line 193
    iput p1, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mAlpha:F

    #@2
    .line 194
    return-void
.end method

.method public setDrawable(Landroid/content/res/Resources;I)V
    .registers 5
    .parameter "res"
    .parameter "resId"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 87
    if-nez p2, :cond_15

    #@3
    move-object v0, v1

    #@4
    .line 89
    .local v0, drawable:Landroid/graphics/drawable/Drawable;
    :goto_4
    if-eqz v0, :cond_a

    #@6
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    #@9
    move-result-object v1

    #@a
    :cond_a
    iput-object v1, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@c
    .line 90
    invoke-direct {p0}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->resizeDrawables()V

    #@f
    .line 91
    sget-object v1, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->STATE_INACTIVE:[I

    #@11
    invoke-virtual {p0, v1}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->setState([I)V

    #@14
    .line 92
    return-void

    #@15
    .line 87
    .end local v0           #drawable:Landroid/graphics/drawable/Drawable;
    :cond_15
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@18
    move-result-object v0

    #@19
    goto :goto_4
.end method

.method public setEnabled(Z)V
    .registers 2
    .parameter "enabled"

    #@0
    .prologue
    .line 254
    iput-boolean p1, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mEnabled:Z

    #@2
    .line 255
    return-void
.end method

.method public setPositionX(F)V
    .registers 2
    .parameter "x"

    #@0
    .prologue
    .line 217
    iput p1, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mPositionX:F

    #@2
    .line 218
    return-void
.end method

.method public setPositionY(F)V
    .registers 2
    .parameter "y"

    #@0
    .prologue
    .line 221
    iput p1, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mPositionY:F

    #@2
    .line 222
    return-void
.end method

.method public setScaleX(F)V
    .registers 2
    .parameter "x"

    #@0
    .prologue
    .line 185
    iput p1, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mScaleX:F

    #@2
    .line 186
    return-void
.end method

.method public setScaleY(F)V
    .registers 2
    .parameter "y"

    #@0
    .prologue
    .line 189
    iput p1, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mScaleY:F

    #@2
    .line 190
    return-void
.end method

.method public setState([I)V
    .registers 4
    .parameter "state"

    #@0
    .prologue
    .line 103
    iget-object v1, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@2
    instance-of v1, v1, Landroid/graphics/drawable/StateListDrawable;

    #@4
    if-eqz v1, :cond_d

    #@6
    .line 104
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@8
    check-cast v0, Landroid/graphics/drawable/StateListDrawable;

    #@a
    .line 105
    .local v0, d:Landroid/graphics/drawable/StateListDrawable;
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/StateListDrawable;->setState([I)Z

    #@d
    .line 107
    .end local v0           #d:Landroid/graphics/drawable/StateListDrawable;
    :cond_d
    return-void
.end method

.method public setX(F)V
    .registers 2
    .parameter "x"

    #@0
    .prologue
    .line 177
    iput p1, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mTranslationX:F

    #@2
    .line 178
    return-void
.end method

.method public setY(F)V
    .registers 2
    .parameter "y"

    #@0
    .prologue
    .line 181
    iput p1, p0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->mTranslationY:F

    #@2
    .line 182
    return-void
.end method
