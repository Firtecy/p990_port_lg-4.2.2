.class final Lcom/android/internal/widget/multiwaveview/Ease$Cubic$3;
.super Ljava/lang/Object;
.source "Ease.java"

# interfaces
.implements Landroid/animation/TimeInterpolator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/widget/multiwaveview/Ease$Cubic;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 45
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .registers 6
    .parameter "input"

    #@0
    .prologue
    const/high16 v3, 0x4000

    #@2
    const/4 v2, 0x0

    #@3
    const/high16 v1, 0x3f00

    #@5
    .line 47
    div-float/2addr p1, v1

    #@6
    const/high16 v0, 0x3f80

    #@8
    cmpg-float v0, p1, v0

    #@a
    if-gez v0, :cond_12

    #@c
    mul-float v0, v1, p1

    #@e
    mul-float/2addr v0, p1

    #@f
    mul-float/2addr v0, p1

    #@10
    add-float/2addr v0, v2

    #@11
    :goto_11
    return v0

    #@12
    :cond_12
    sub-float/2addr p1, v3

    #@13
    mul-float v0, p1, p1

    #@15
    mul-float/2addr v0, p1

    #@16
    add-float/2addr v0, v3

    #@17
    mul-float/2addr v0, v1

    #@18
    add-float/2addr v0, v2

    #@19
    goto :goto_11
.end method
