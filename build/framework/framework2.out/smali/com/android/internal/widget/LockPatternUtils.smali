.class public Lcom/android/internal/widget/LockPatternUtils;
.super Ljava/lang/Object;
.source "LockPatternUtils.java"


# static fields
.field private static final BACKUP_PIN_FILE:Ljava/lang/String; = "/system/backuppin.dat"

.field public static final BIOMETRIC_WEAK_EVER_CHOSEN_KEY:Ljava/lang/String; = "lockscreen.biometricweakeverchosen"

.field protected static final DISABLE_LOCKSCREEN_KEY:Ljava/lang/String; = "lockscreen.disabled"

.field public static final FAILED_ATTEMPTS_BEFORE_RESET:I = 0x14

.field public static final FAILED_ATTEMPTS_BEFORE_TIMEOUT:I = 0x5

.field public static final FAILED_ATTEMPTS_BEFORE_WIPE_GRACE:I = 0x5

.field public static final FAILED_ATTEMPT_COUNTDOWN_INTERVAL_MS:J = 0x3e8L

.field public static final FAILED_ATTEMPT_TIMEOUT_MS:J = 0x7530L

.field public static final FLAG_BIOMETRIC_WEAK_LIVELINESS:I = 0x1

.field public static final ID_DEFAULT_STATUS_WIDGET:I = -0x2

.field public static final KEYGUARD_SHOW_APPWIDGET:Ljava/lang/String; = "showappwidget"

.field public static final KEYGUARD_SHOW_SECURITY_CHALLENGE:Ljava/lang/String; = "showsecuritychallenge"

.field public static final KEYGUARD_SHOW_USER_SWITCHER:Ljava/lang/String; = "showuserswitcher"

.field protected static final LOCKOUT_ATTEMPT_DEADLINE:Ljava/lang/String; = "lockscreen.lockoutattemptdeadline"

.field protected static final LOCKOUT_PERMANENT_KEY:Ljava/lang/String; = "lockscreen.lockedoutpermanently"

.field public static final LOCKSCREEN_BIOMETRIC_WEAK_FALLBACK:Ljava/lang/String; = "lockscreen.biometric_weak_fallback"

.field protected static final LOCKSCREEN_OPTIONS:Ljava/lang/String; = "lockscreen.options"

.field public static final LOCKSCREEN_POWER_BUTTON_INSTANTLY_LOCKS:Ljava/lang/String; = "lockscreen.power_button_instantly_locks"

.field protected static final LOCK_PASSWORD_SALT_KEY:Ljava/lang/String; = "lockscreen.password_salt"

.field public static final MIN_LOCK_PATTERN_SIZE:I = 0x4

.field public static final MIN_PATTERN_REGISTER_FAIL:I = 0x4

.field public static final PASSWORD_EXPIRE_MODE:Ljava/lang/String; = "PASSWORD_EXPIRE"

.field protected static final PASSWORD_HISTORY_KEY:Ljava/lang/String; = "lockscreen.passwordhistory"

.field public static final PASSWORD_TYPE_ALTERNATE_KEY:Ljava/lang/String; = "lockscreen.password_type_alternate"

.field public static final PASSWORD_TYPE_KEY:Ljava/lang/String; = "lockscreen.password_type"

.field protected static final PATTERN_EVER_CHOSEN_KEY:Ljava/lang/String; = "lockscreen.patterneverchosen"

.field private static final TAG:Ljava/lang/String; = "LockPatternUtils"

.field private static final carrier:Ljava/lang/String;

.field private static volatile sCurrentUserId:I


# instance fields
.field private final mContentResolver:Landroid/content/ContentResolver;

.field private final mContext:Landroid/content/Context;

.field private mDevicePolicyManager:Landroid/app/admin/DevicePolicyManager;

.field private mHiddenPatternDisabled:Z

.field private mLockSettingsService:Lcom/android/internal/widget/ILockSettings;

.field private sLGPasswordFilename:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 75
    const-string v0, "ro.build.target_operator"

    #@2
    const-string v1, "COM"

    #@4
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    sput-object v0, Lcom/android/internal/widget/LockPatternUtils;->carrier:Ljava/lang/String;

    #@a
    .line 169
    const/16 v0, -0x2710

    #@c
    sput v0, Lcom/android/internal/widget/LockPatternUtils;->sCurrentUserId:I

    #@e
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 190
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 518
    const/4 v0, 0x0

    #@4
    iput-boolean v0, p0, Lcom/android/internal/widget/LockPatternUtils;->mHiddenPatternDisabled:Z

    #@6
    .line 191
    iput-object p1, p0, Lcom/android/internal/widget/LockPatternUtils;->mContext:Landroid/content/Context;

    #@8
    .line 192
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@b
    move-result-object v0

    #@c
    iput-object v0, p0, Lcom/android/internal/widget/LockPatternUtils;->mContentResolver:Landroid/content/ContentResolver;

    #@e
    .line 196
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_LOCKSCREEN:Z

    #@10
    if-eqz v0, :cond_15

    #@12
    .line 197
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternUtils;->initFilename()V

    #@15
    .line 201
    :cond_15
    return-void
.end method

.method private static combineStrings([ILjava/lang/String;)Ljava/lang/String;
    .registers 9
    .parameter "list"
    .parameter "separator"

    #@0
    .prologue
    .line 1188
    array-length v1, p0

    #@1
    .line 1190
    .local v1, listLength:I
    packed-switch v1, :pswitch_data_4e

    #@4
    .line 1199
    const/4 v4, 0x0

    #@5
    .line 1200
    .local v4, strLength:I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@8
    move-result v3

    #@9
    .line 1202
    .local v3, separatorLength:I
    array-length v6, p0

    #@a
    new-array v5, v6, [Ljava/lang/String;

    #@c
    .line 1203
    .local v5, stringList:[Ljava/lang/String;
    const/4 v0, 0x0

    #@d
    .local v0, i:I
    :goto_d
    if-ge v0, v1, :cond_31

    #@f
    .line 1204
    aget v6, p0, v0

    #@11
    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@14
    move-result-object v6

    #@15
    aput-object v6, v5, v0

    #@17
    .line 1205
    aget-object v6, v5, v0

    #@19
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    #@1c
    move-result v6

    #@1d
    add-int/2addr v4, v6

    #@1e
    .line 1206
    add-int/lit8 v6, v1, -0x1

    #@20
    if-ge v0, v6, :cond_23

    #@22
    .line 1207
    add-int/2addr v4, v3

    #@23
    .line 1203
    :cond_23
    add-int/lit8 v0, v0, 0x1

    #@25
    goto :goto_d

    #@26
    .line 1192
    .end local v0           #i:I
    .end local v3           #separatorLength:I
    .end local v4           #strLength:I
    .end local v5           #stringList:[Ljava/lang/String;
    :pswitch_26
    const-string v6, ""

    #@28
    .line 1220
    :goto_28
    return-object v6

    #@29
    .line 1195
    :pswitch_29
    const/4 v6, 0x0

    #@2a
    aget v6, p0, v6

    #@2c
    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@2f
    move-result-object v6

    #@30
    goto :goto_28

    #@31
    .line 1211
    .restart local v0       #i:I
    .restart local v3       #separatorLength:I
    .restart local v4       #strLength:I
    .restart local v5       #stringList:[Ljava/lang/String;
    :cond_31
    new-instance v2, Ljava/lang/StringBuilder;

    #@33
    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    #@36
    .line 1213
    .local v2, sb:Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    #@37
    :goto_37
    if-ge v0, v1, :cond_48

    #@39
    .line 1214
    aget v6, p0, v0

    #@3b
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3e
    .line 1215
    add-int/lit8 v6, v1, -0x1

    #@40
    if-ge v0, v6, :cond_45

    #@42
    .line 1216
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    .line 1213
    :cond_45
    add-int/lit8 v0, v0, 0x1

    #@47
    goto :goto_37

    #@48
    .line 1220
    :cond_48
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v6

    #@4c
    goto :goto_28

    #@4d
    .line 1190
    nop

    #@4e
    :pswitch_data_4e
    .packed-switch 0x0
        :pswitch_26
        :pswitch_29
    .end packed-switch
.end method

.method public static computePasswordQuality(Ljava/lang/String;)I
    .registers 6
    .parameter "password"

    #@0
    .prologue
    .line 639
    const/4 v0, 0x0

    #@1
    .line 640
    .local v0, hasDigit:Z
    const/4 v1, 0x0

    #@2
    .line 641
    .local v1, hasNonDigit:Z
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@5
    move-result v3

    #@6
    .line 642
    .local v3, len:I
    const/4 v2, 0x0

    #@7
    .local v2, i:I
    :goto_7
    if-ge v2, v3, :cond_19

    #@9
    .line 643
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    #@c
    move-result v4

    #@d
    invoke-static {v4}, Ljava/lang/Character;->isDigit(C)Z

    #@10
    move-result v4

    #@11
    if-eqz v4, :cond_17

    #@13
    .line 644
    const/4 v0, 0x1

    #@14
    .line 642
    :goto_14
    add-int/lit8 v2, v2, 0x1

    #@16
    goto :goto_7

    #@17
    .line 646
    :cond_17
    const/4 v1, 0x1

    #@18
    goto :goto_14

    #@19
    .line 650
    :cond_19
    if-eqz v1, :cond_20

    #@1b
    if-eqz v0, :cond_20

    #@1d
    .line 651
    const/high16 v4, 0x5

    #@1f
    .line 659
    :goto_1f
    return v4

    #@20
    .line 653
    :cond_20
    if-eqz v1, :cond_25

    #@22
    .line 654
    const/high16 v4, 0x4

    #@24
    goto :goto_1f

    #@25
    .line 656
    :cond_25
    if-eqz v0, :cond_2a

    #@27
    .line 657
    const/high16 v4, 0x2

    #@29
    goto :goto_1f

    #@2a
    .line 659
    :cond_2a
    const/4 v4, 0x0

    #@2b
    goto :goto_1f
.end method

.method private finishBiometricWeak()V
    .registers 4

    #@0
    .prologue
    .line 1462
    const-string v1, "lockscreen.biometricweakeverchosen"

    #@2
    const/4 v2, 0x1

    #@3
    invoke-direct {p0, v1, v2}, Lcom/android/internal/widget/LockPatternUtils;->setBoolean(Ljava/lang/String;Z)V

    #@6
    .line 1466
    new-instance v0, Landroid/content/Intent;

    #@8
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    #@b
    .line 1467
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "com.android.facelock"

    #@d
    const-string v2, "com.android.facelock.SetupEndScreen"

    #@f
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@12
    .line 1469
    iget-object v1, p0, Lcom/android/internal/widget/LockPatternUtils;->mContext:Landroid/content/Context;

    #@14
    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@17
    .line 1470
    return-void
.end method

.method private getBoolean(Ljava/lang/String;Z)Z
    .registers 6
    .parameter "secureSettingKey"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 1149
    :try_start_0
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternUtils;->getLockSettings()Lcom/android/internal/widget/ILockSettings;

    #@3
    move-result-object v1

    #@4
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentOrCallingUserId()I

    #@7
    move-result v2

    #@8
    invoke-interface {v1, p1, p2, v2}, Lcom/android/internal/widget/ILockSettings;->getBoolean(Ljava/lang/String;ZI)Z
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_b} :catch_d

    #@b
    move-result p2

    #@c
    .line 1152
    .end local p2
    :goto_c
    return p2

    #@d
    .line 1151
    .restart local p2
    :catch_d
    move-exception v0

    #@e
    .line 1152
    .local v0, re:Landroid/os/RemoteException;
    goto :goto_c
.end method

.method private getCurrentOrCallingUserId()I
    .registers 3

    #@0
    .prologue
    .line 295
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@3
    move-result v0

    #@4
    .line 296
    .local v0, callingUid:I
    const/16 v1, 0x3e8

    #@6
    if-ne v0, v1, :cond_d

    #@8
    .line 299
    invoke-virtual {p0}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentUser()I

    #@b
    move-result v1

    #@c
    .line 301
    :goto_c
    return v1

    #@d
    :cond_d
    invoke-static {v0}, Landroid/os/UserHandle;->getUserId(I)I

    #@10
    move-result v1

    #@11
    goto :goto_c
.end method

.method private getLockSettings()Lcom/android/internal/widget/ILockSettings;
    .registers 2

    #@0
    .prologue
    .line 204
    iget-object v0, p0, Lcom/android/internal/widget/LockPatternUtils;->mLockSettingsService:Lcom/android/internal/widget/ILockSettings;

    #@2
    if-nez v0, :cond_10

    #@4
    .line 205
    const-string v0, "lock_settings"

    #@6
    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@9
    move-result-object v0

    #@a
    invoke-static {v0}, Lcom/android/internal/widget/ILockSettings$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/widget/ILockSettings;

    #@d
    move-result-object v0

    #@e
    iput-object v0, p0, Lcom/android/internal/widget/LockPatternUtils;->mLockSettingsService:Lcom/android/internal/widget/ILockSettings;

    #@10
    .line 208
    :cond_10
    iget-object v0, p0, Lcom/android/internal/widget/LockPatternUtils;->mLockSettingsService:Lcom/android/internal/widget/ILockSettings;

    #@12
    return-object v0
.end method

.method private getLong(Ljava/lang/String;J)J
    .registers 7
    .parameter "secureSettingKey"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 1298
    :try_start_0
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternUtils;->getLockSettings()Lcom/android/internal/widget/ILockSettings;

    #@3
    move-result-object v1

    #@4
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentOrCallingUserId()I

    #@7
    move-result v2

    #@8
    invoke-interface {v1, p1, p2, p3, v2}, Lcom/android/internal/widget/ILockSettings;->getLong(Ljava/lang/String;JI)J
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_b} :catch_d

    #@b
    move-result-wide p2

    #@c
    .line 1301
    .end local p2
    :goto_c
    return-wide p2

    #@d
    .line 1300
    .restart local p2
    :catch_d
    move-exception v0

    #@e
    .line 1301
    .local v0, re:Landroid/os/RemoteException;
    goto :goto_c
.end method

.method private getSalt()Ljava/lang/String;
    .registers 7

    #@0
    .prologue
    const-wide/16 v4, 0x0

    #@2
    .line 908
    const-string v3, "lockscreen.password_salt"

    #@4
    invoke-direct {p0, v3, v4, v5}, Lcom/android/internal/widget/LockPatternUtils;->getLong(Ljava/lang/String;J)J

    #@7
    move-result-wide v1

    #@8
    .line 909
    .local v1, salt:J
    cmp-long v3, v1, v4

    #@a
    if-nez v3, :cond_22

    #@c
    .line 911
    :try_start_c
    const-string v3, "SHA1PRNG"

    #@e
    invoke-static {v3}, Ljava/security/SecureRandom;->getInstance(Ljava/lang/String;)Ljava/security/SecureRandom;

    #@11
    move-result-object v3

    #@12
    invoke-virtual {v3}, Ljava/security/SecureRandom;->nextLong()J

    #@15
    move-result-wide v1

    #@16
    .line 912
    const-string v3, "lockscreen.password_salt"

    #@18
    invoke-direct {p0, v3, v1, v2}, Lcom/android/internal/widget/LockPatternUtils;->setLong(Ljava/lang/String;J)V

    #@1b
    .line 913
    const-string v3, "LockPatternUtils"

    #@1d
    const-string v4, "Initialized lock password salt"

    #@1f
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_22
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_c .. :try_end_22} :catch_27

    #@22
    .line 919
    :cond_22
    invoke-static {v1, v2}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    #@25
    move-result-object v3

    #@26
    return-object v3

    #@27
    .line 914
    :catch_27
    move-exception v0

    #@28
    .line 916
    .local v0, e:Ljava/security/NoSuchAlgorithmException;
    new-instance v3, Ljava/lang/IllegalStateException;

    #@2a
    const-string v4, "Couldn\'t get SecureRandom number"

    #@2c
    invoke-direct {v3, v4, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@2f
    throw v3
.end method

.method private getString(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "secureSettingKey"

    #@0
    .prologue
    .line 1319
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentOrCallingUserId()I

    #@3
    move-result v0

    #@4
    invoke-direct {p0, p1, v0}, Lcom/android/internal/widget/LockPatternUtils;->getString(Ljava/lang/String;I)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method private getString(Ljava/lang/String;I)Ljava/lang/String;
    .registers 7
    .parameter "secureSettingKey"
    .parameter "userHandle"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1324
    :try_start_1
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternUtils;->getLockSettings()Lcom/android/internal/widget/ILockSettings;

    #@4
    move-result-object v2

    #@5
    const/4 v3, 0x0

    #@6
    invoke-interface {v2, p1, v3, p2}, Lcom/android/internal/widget/ILockSettings;->getString(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_9} :catch_b

    #@9
    move-result-object v1

    #@a
    .line 1326
    :goto_a
    return-object v1

    #@b
    .line 1325
    :catch_b
    move-exception v0

    #@c
    .line 1326
    .local v0, re:Landroid/os/RemoteException;
    goto :goto_a
.end method

.method private initFilename()V
    .registers 3

    #@0
    .prologue
    .line 1475
    iget-object v0, p0, Lcom/android/internal/widget/LockPatternUtils;->sLGPasswordFilename:Ljava/lang/String;

    #@2
    if-nez v0, :cond_21

    #@4
    .line 1476
    new-instance v0, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    const-string v1, "/system/backuppin.dat"

    #@17
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v0

    #@1b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v0

    #@1f
    iput-object v0, p0, Lcom/android/internal/widget/LockPatternUtils;->sLGPasswordFilename:Ljava/lang/String;

    #@21
    .line 1477
    :cond_21
    return-void
.end method

.method public static isSafeModeEnabled()Z
    .registers 1

    #@0
    .prologue
    .line 1554
    :try_start_0
    const-string v0, "window"

    #@2
    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@5
    move-result-object v0

    #@6
    invoke-static {v0}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    #@9
    move-result-object v0

    #@a
    invoke-interface {v0}, Landroid/view/IWindowManager;->isSafeModeEnabled()Z
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_d} :catch_f

    #@d
    move-result v0

    #@e
    .line 1559
    :goto_e
    return v0

    #@f
    .line 1556
    :catch_f
    move-exception v0

    #@10
    .line 1559
    const/4 v0, 0x0

    #@11
    goto :goto_e
.end method

.method private static patternToHash(Ljava/util/List;)[B
    .registers 10
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/widget/LockPatternView$Cell;",
            ">;)[B"
        }
    .end annotation

    #@0
    .prologue
    .line 888
    .local p0, pattern:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/widget/LockPatternView$Cell;>;"
    if-nez p0, :cond_4

    #@2
    .line 889
    const/4 v1, 0x0

    #@3
    .line 903
    :goto_3
    return-object v1

    #@4
    .line 892
    :cond_4
    invoke-interface {p0}, Ljava/util/List;->size()I

    #@7
    move-result v5

    #@8
    .line 893
    .local v5, patternSize:I
    new-array v6, v5, [B

    #@a
    .line 894
    .local v6, res:[B
    const/4 v2, 0x0

    #@b
    .local v2, i:I
    :goto_b
    if-ge v2, v5, :cond_24

    #@d
    .line 895
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@10
    move-result-object v0

    #@11
    check-cast v0, Lcom/android/internal/widget/LockPatternView$Cell;

    #@13
    .line 896
    .local v0, cell:Lcom/android/internal/widget/LockPatternView$Cell;
    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternView$Cell;->getRow()I

    #@16
    move-result v7

    #@17
    mul-int/lit8 v7, v7, 0x3

    #@19
    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternView$Cell;->getColumn()I

    #@1c
    move-result v8

    #@1d
    add-int/2addr v7, v8

    #@1e
    int-to-byte v7, v7

    #@1f
    aput-byte v7, v6, v2

    #@21
    .line 894
    add-int/lit8 v2, v2, 0x1

    #@23
    goto :goto_b

    #@24
    .line 899
    .end local v0           #cell:Lcom/android/internal/widget/LockPatternView$Cell;
    :cond_24
    :try_start_24
    const-string v7, "SHA-1"

    #@26
    invoke-static {v7}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    #@29
    move-result-object v3

    #@2a
    .line 900
    .local v3, md:Ljava/security/MessageDigest;
    invoke-virtual {v3, v6}, Ljava/security/MessageDigest;->digest([B)[B
    :try_end_2d
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_24 .. :try_end_2d} :catch_2f

    #@2d
    move-result-object v1

    #@2e
    .line 901
    .local v1, hash:[B
    goto :goto_3

    #@2f
    .line 902
    .end local v1           #hash:[B
    .end local v3           #md:Ljava/security/MessageDigest;
    :catch_2f
    move-exception v4

    #@30
    .local v4, nsa:Ljava/security/NoSuchAlgorithmException;
    move-object v1, v6

    #@31
    .line 903
    goto :goto_3
.end method

.method public static patternToString(Ljava/util/List;)Ljava/lang/String;
    .registers 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/widget/LockPatternView$Cell;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    #@0
    .prologue
    .line 867
    .local p0, pattern:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/widget/LockPatternView$Cell;>;"
    if-nez p0, :cond_5

    #@2
    .line 868
    const-string v4, ""

    #@4
    .line 877
    :goto_4
    return-object v4

    #@5
    .line 870
    :cond_5
    invoke-interface {p0}, Ljava/util/List;->size()I

    #@8
    move-result v2

    #@9
    .line 872
    .local v2, patternSize:I
    new-array v3, v2, [B

    #@b
    .line 873
    .local v3, res:[B
    const/4 v1, 0x0

    #@c
    .local v1, i:I
    :goto_c
    if-ge v1, v2, :cond_25

    #@e
    .line 874
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@11
    move-result-object v0

    #@12
    check-cast v0, Lcom/android/internal/widget/LockPatternView$Cell;

    #@14
    .line 875
    .local v0, cell:Lcom/android/internal/widget/LockPatternView$Cell;
    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternView$Cell;->getRow()I

    #@17
    move-result v4

    #@18
    mul-int/lit8 v4, v4, 0x3

    #@1a
    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternView$Cell;->getColumn()I

    #@1d
    move-result v5

    #@1e
    add-int/2addr v4, v5

    #@1f
    int-to-byte v4, v4

    #@20
    aput-byte v4, v3, v1

    #@22
    .line 873
    add-int/lit8 v1, v1, 0x1

    #@24
    goto :goto_c

    #@25
    .line 877
    .end local v0           #cell:Lcom/android/internal/widget/LockPatternView$Cell;
    :cond_25
    new-instance v4, Ljava/lang/String;

    #@27
    invoke-direct {v4, v3}, Ljava/lang/String;-><init>([B)V

    #@2a
    goto :goto_4
.end method

.method private setBoolean(Ljava/lang/String;Z)V
    .registers 7
    .parameter "secureSettingKey"
    .parameter "enabled"

    #@0
    .prologue
    .line 1158
    :try_start_0
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternUtils;->getLockSettings()Lcom/android/internal/widget/ILockSettings;

    #@3
    move-result-object v1

    #@4
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentOrCallingUserId()I

    #@7
    move-result v2

    #@8
    invoke-interface {v1, p1, p2, v2}, Lcom/android/internal/widget/ILockSettings;->setBoolean(Ljava/lang/String;ZI)V
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_b} :catch_c

    #@b
    .line 1163
    :goto_b
    return-void

    #@c
    .line 1159
    :catch_c
    move-exception v0

    #@d
    .line 1161
    .local v0, re:Landroid/os/RemoteException;
    const-string v1, "LockPatternUtils"

    #@f
    new-instance v2, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v3, "Couldn\'t write boolean "

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v2

    #@22
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v2

    #@26
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    goto :goto_b
.end method

.method private setLong(Ljava/lang/String;J)V
    .registers 5
    .parameter "secureSettingKey"
    .parameter "value"

    #@0
    .prologue
    .line 1306
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentOrCallingUserId()I

    #@3
    move-result v0

    #@4
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/android/internal/widget/LockPatternUtils;->setLong(Ljava/lang/String;JI)V

    #@7
    .line 1307
    return-void
.end method

.method private setLong(Ljava/lang/String;JI)V
    .registers 9
    .parameter "secureSettingKey"
    .parameter "value"
    .parameter "userHandle"

    #@0
    .prologue
    .line 1311
    :try_start_0
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternUtils;->getLockSettings()Lcom/android/internal/widget/ILockSettings;

    #@3
    move-result-object v1

    #@4
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentOrCallingUserId()I

    #@7
    move-result v2

    #@8
    invoke-interface {v1, p1, p2, p3, v2}, Lcom/android/internal/widget/ILockSettings;->setLong(Ljava/lang/String;JI)V
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_b} :catch_c

    #@b
    .line 1316
    :goto_b
    return-void

    #@c
    .line 1312
    :catch_c
    move-exception v0

    #@d
    .line 1314
    .local v0, re:Landroid/os/RemoteException;
    const-string v1, "LockPatternUtils"

    #@f
    new-instance v2, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v3, "Couldn\'t write long "

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v2

    #@22
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v2

    #@26
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    goto :goto_b
.end method

.method private setString(Ljava/lang/String;Ljava/lang/String;I)V
    .registers 8
    .parameter "secureSettingKey"
    .parameter "value"
    .parameter "userHandle"

    #@0
    .prologue
    .line 1332
    :try_start_0
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternUtils;->getLockSettings()Lcom/android/internal/widget/ILockSettings;

    #@3
    move-result-object v1

    #@4
    invoke-interface {v1, p1, p2, p3}, Lcom/android/internal/widget/ILockSettings;->setString(Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_8

    #@7
    .line 1337
    :goto_7
    return-void

    #@8
    .line 1333
    :catch_8
    move-exception v0

    #@9
    .line 1335
    .local v0, re:Landroid/os/RemoteException;
    const-string v1, "LockPatternUtils"

    #@b
    new-instance v2, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v3, "Couldn\'t write string "

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    goto :goto_7
.end method

.method public static stringToPattern(Ljava/lang/String;)Ljava/util/List;
    .registers 7
    .parameter "string"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/widget/LockPatternView$Cell;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 851
    invoke-static {}, Lcom/google/android/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    #@3
    move-result-object v3

    #@4
    .line 853
    .local v3, result:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/widget/LockPatternView$Cell;>;"
    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    #@7
    move-result-object v1

    #@8
    .line 854
    .local v1, bytes:[B
    const/4 v2, 0x0

    #@9
    .local v2, i:I
    :goto_9
    array-length v4, v1

    #@a
    if-ge v2, v4, :cond_1c

    #@c
    .line 855
    aget-byte v0, v1, v2

    #@e
    .line 856
    .local v0, b:B
    div-int/lit8 v4, v0, 0x3

    #@10
    rem-int/lit8 v5, v0, 0x3

    #@12
    invoke-static {v4, v5}, Lcom/android/internal/widget/LockPatternView$Cell;->of(II)Lcom/android/internal/widget/LockPatternView$Cell;

    #@15
    move-result-object v4

    #@16
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@19
    .line 854
    add-int/lit8 v2, v2, 0x1

    #@1b
    goto :goto_9

    #@1c
    .line 858
    .end local v0           #b:B
    :cond_1c
    return-object v3
.end method

.method private static toHex([B)Ljava/lang/String;
    .registers 7
    .parameter "ary"

    #@0
    .prologue
    .line 947
    const-string v0, "0123456789ABCDEF"

    #@2
    .line 948
    .local v0, hex:Ljava/lang/String;
    const-string v2, ""

    #@4
    .line 949
    .local v2, ret:Ljava/lang/String;
    const/4 v1, 0x0

    #@5
    .local v1, i:I
    :goto_5
    array-length v3, p0

    #@6
    if-ge v1, v3, :cond_43

    #@8
    .line 950
    new-instance v3, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    const-string v4, "0123456789ABCDEF"

    #@13
    aget-byte v5, p0, v1

    #@15
    shr-int/lit8 v5, v5, 0x4

    #@17
    and-int/lit8 v5, v5, 0xf

    #@19
    invoke-virtual {v4, v5}, Ljava/lang/String;->charAt(I)C

    #@1c
    move-result v4

    #@1d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v2

    #@25
    .line 951
    new-instance v3, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v3

    #@2e
    const-string v4, "0123456789ABCDEF"

    #@30
    aget-byte v5, p0, v1

    #@32
    and-int/lit8 v5, v5, 0xf

    #@34
    invoke-virtual {v4, v5}, Ljava/lang/String;->charAt(I)C

    #@37
    move-result v4

    #@38
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v3

    #@3c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v2

    #@40
    .line 949
    add-int/lit8 v1, v1, 0x1

    #@42
    goto :goto_5

    #@43
    .line 953
    :cond_43
    return-object v2
.end method

.method private updateEncryptionPassword(Ljava/lang/String;)V
    .registers 8
    .parameter "password"

    #@0
    .prologue
    .line 664
    invoke-virtual {p0}, Lcom/android/internal/widget/LockPatternUtils;->getDevicePolicyManager()Landroid/app/admin/DevicePolicyManager;

    #@3
    move-result-object v0

    #@4
    .line 665
    .local v0, dpm:Landroid/app/admin/DevicePolicyManager;
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentOrCallingUserId()I

    #@7
    move-result v4

    #@8
    invoke-virtual {v0, v4}, Landroid/app/admin/DevicePolicyManager;->getStorageEncryptionStatus(I)I

    #@b
    move-result v4

    #@c
    const/4 v5, 0x3

    #@d
    if-eq v4, v5, :cond_10

    #@f
    .line 682
    :goto_f
    return-void

    #@10
    .line 670
    :cond_10
    const-string v4, "mount"

    #@12
    invoke-static {v4}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@15
    move-result-object v3

    #@16
    .line 671
    .local v3, service:Landroid/os/IBinder;
    if-nez v3, :cond_20

    #@18
    .line 672
    const-string v4, "LockPatternUtils"

    #@1a
    const-string v5, "Could not find the mount service to update the encryption password"

    #@1c
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    goto :goto_f

    #@20
    .line 676
    :cond_20
    invoke-static {v3}, Landroid/os/storage/IMountService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/storage/IMountService;

    #@23
    move-result-object v2

    #@24
    .line 678
    .local v2, mountService:Landroid/os/storage/IMountService;
    :try_start_24
    invoke-interface {v2, p1}, Landroid/os/storage/IMountService;->changeEncryptionPassword(Ljava/lang/String;)I
    :try_end_27
    .catch Landroid/os/RemoteException; {:try_start_24 .. :try_end_27} :catch_28

    #@27
    goto :goto_f

    #@28
    .line 679
    :catch_28
    move-exception v1

    #@29
    .line 680
    .local v1, e:Landroid/os/RemoteException;
    const-string v4, "LockPatternUtils"

    #@2b
    const-string v5, "Error changing encryption password"

    #@2d
    invoke-static {v4, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@30
    goto :goto_f
.end method

.method private writeAppWidgets([I)V
    .registers 6
    .parameter "appWidgetIds"

    #@0
    .prologue
    .line 1243
    iget-object v0, p0, Lcom/android/internal/widget/LockPatternUtils;->mContentResolver:Landroid/content/ContentResolver;

    #@2
    const-string v1, "lock_screen_appwidget_ids"

    #@4
    const-string v2, ","

    #@6
    invoke-static {p1, v2}, Lcom/android/internal/widget/LockPatternUtils;->combineStrings([ILjava/lang/String;)Ljava/lang/String;

    #@9
    move-result-object v2

    #@a
    const/4 v3, -0x2

    #@b
    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$Secure;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    #@e
    .line 1247
    return-void
.end method


# virtual methods
.method public ResetLockoutAttemptDeadline()V
    .registers 6

    #@0
    .prologue
    .line 1092
    const-wide/16 v0, 0x0

    #@2
    .line 1093
    .local v0, deadline:J
    const-string v2, "lockscreen.lockoutattemptdeadline"

    #@4
    const-wide/16 v3, 0x0

    #@6
    invoke-direct {p0, v2, v3, v4}, Lcom/android/internal/widget/LockPatternUtils;->setLong(Ljava/lang/String;J)V

    #@9
    .line 1094
    return-void
.end method

.method public addAppWidget(II)Z
    .registers 9
    .parameter "widgetId"
    .parameter "index"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1251
    invoke-virtual {p0}, Lcom/android/internal/widget/LockPatternUtils;->getAppWidgets()[I

    #@4
    move-result-object v3

    #@5
    .line 1252
    .local v3, widgets:[I
    if-nez v3, :cond_8

    #@7
    .line 1270
    :cond_7
    :goto_7
    return v4

    #@8
    .line 1255
    :cond_8
    if-ltz p2, :cond_7

    #@a
    array-length v5, v3

    #@b
    if-gt p2, v5, :cond_7

    #@d
    .line 1258
    array-length v4, v3

    #@e
    add-int/lit8 v4, v4, 0x1

    #@10
    new-array v2, v4, [I

    #@12
    .line 1259
    .local v2, newWidgets:[I
    const/4 v0, 0x0

    #@13
    .local v0, i:I
    const/4 v1, 0x0

    #@14
    .local v1, j:I
    :goto_14
    array-length v4, v2

    #@15
    if-ge v0, v4, :cond_29

    #@17
    .line 1260
    if-ne p2, v0, :cond_1d

    #@19
    .line 1261
    aput p1, v2, v0

    #@1b
    .line 1262
    add-int/lit8 v0, v0, 0x1

    #@1d
    .line 1264
    :cond_1d
    array-length v4, v2

    #@1e
    if-ge v0, v4, :cond_26

    #@20
    .line 1265
    aget v4, v3, v1

    #@22
    aput v4, v2, v0

    #@24
    .line 1266
    add-int/lit8 v1, v1, 0x1

    #@26
    .line 1259
    :cond_26
    add-int/lit8 v0, v0, 0x1

    #@28
    goto :goto_14

    #@29
    .line 1269
    :cond_29
    invoke-direct {p0, v2}, Lcom/android/internal/widget/LockPatternUtils;->writeAppWidgets([I)V

    #@2c
    .line 1270
    const/4 v4, 0x1

    #@2d
    goto :goto_7
.end method

.method public checkBackupPinFile()Z
    .registers 11

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 1508
    const/4 v3, 0x0

    #@2
    .line 1510
    .local v3, raf:Ljava/io/RandomAccessFile;
    :try_start_2
    new-instance v4, Ljava/io/RandomAccessFile;

    #@4
    iget-object v6, p0, Lcom/android/internal/widget/LockPatternUtils;->sLGPasswordFilename:Ljava/lang/String;

    #@6
    const-string v7, "r"

    #@8
    invoke-direct {v4, v6, v7}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_b
    .catchall {:try_start_2 .. :try_end_b} :catchall_6b
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_b} :catch_3d
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_b} :catch_54

    #@b
    .line 1511
    .end local v3           #raf:Ljava/io/RandomAccessFile;
    .local v4, raf:Ljava/io/RandomAccessFile;
    :try_start_b
    invoke-virtual {v4}, Ljava/io/RandomAccessFile;->length()J

    #@e
    move-result-wide v6

    #@f
    const-wide/16 v8, 0x0

    #@11
    cmp-long v6, v6, v8

    #@13
    if-gtz v6, :cond_2c

    #@15
    .line 1512
    const-string v6, "LockPatternUtils"

    #@17
    const-string v7, "Backup PIN checkBackupPinFile : file length below zero"

    #@19
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1c
    .catchall {:try_start_b .. :try_end_1c} :catchall_7b
    .catch Ljava/io/FileNotFoundException; {:try_start_b .. :try_end_1c} :catch_81
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_1c} :catch_7e

    #@1c
    .line 1523
    if-eqz v4, :cond_21

    #@1e
    .line 1524
    :try_start_1e
    invoke-virtual {v4}, Ljava/io/RandomAccessFile;->close()V
    :try_end_21
    .catch Ljava/io/IOException; {:try_start_1e .. :try_end_21} :catch_23

    #@21
    :cond_21
    :goto_21
    move-object v3, v4

    #@22
    .line 1529
    .end local v4           #raf:Ljava/io/RandomAccessFile;
    .restart local v3       #raf:Ljava/io/RandomAccessFile;
    :cond_22
    :goto_22
    return v5

    #@23
    .line 1525
    .end local v3           #raf:Ljava/io/RandomAccessFile;
    .restart local v4       #raf:Ljava/io/RandomAccessFile;
    :catch_23
    move-exception v0

    #@24
    .line 1526
    .local v0, e:Ljava/io/IOException;
    const-string v6, "LockPatternUtils"

    #@26
    const-string v7, "Backup PIN checkBackupPinFile : IO exception at finally"

    #@28
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    goto :goto_21

    #@2c
    .line 1523
    .end local v0           #e:Ljava/io/IOException;
    :cond_2c
    if-eqz v4, :cond_31

    #@2e
    .line 1524
    :try_start_2e
    invoke-virtual {v4}, Ljava/io/RandomAccessFile;->close()V
    :try_end_31
    .catch Ljava/io/IOException; {:try_start_2e .. :try_end_31} :catch_34

    #@31
    .line 1529
    :cond_31
    :goto_31
    const/4 v5, 0x1

    #@32
    move-object v3, v4

    #@33
    .end local v4           #raf:Ljava/io/RandomAccessFile;
    .restart local v3       #raf:Ljava/io/RandomAccessFile;
    goto :goto_22

    #@34
    .line 1525
    .end local v3           #raf:Ljava/io/RandomAccessFile;
    .restart local v4       #raf:Ljava/io/RandomAccessFile;
    :catch_34
    move-exception v0

    #@35
    .line 1526
    .restart local v0       #e:Ljava/io/IOException;
    const-string v5, "LockPatternUtils"

    #@37
    const-string v6, "Backup PIN checkBackupPinFile : IO exception at finally"

    #@39
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3c
    goto :goto_31

    #@3d
    .line 1515
    .end local v0           #e:Ljava/io/IOException;
    .end local v4           #raf:Ljava/io/RandomAccessFile;
    .restart local v3       #raf:Ljava/io/RandomAccessFile;
    :catch_3d
    move-exception v1

    #@3e
    .line 1516
    .local v1, fnfe:Ljava/io/FileNotFoundException;
    :goto_3e
    :try_start_3e
    const-string v6, "LockPatternUtils"

    #@40
    const-string v7, "Backup PIN checkBackupPinFile : file not found exception"

    #@42
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_45
    .catchall {:try_start_3e .. :try_end_45} :catchall_6b

    #@45
    .line 1523
    if-eqz v3, :cond_22

    #@47
    .line 1524
    :try_start_47
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_4a
    .catch Ljava/io/IOException; {:try_start_47 .. :try_end_4a} :catch_4b

    #@4a
    goto :goto_22

    #@4b
    .line 1525
    :catch_4b
    move-exception v0

    #@4c
    .line 1526
    .restart local v0       #e:Ljava/io/IOException;
    const-string v6, "LockPatternUtils"

    #@4e
    const-string v7, "Backup PIN checkBackupPinFile : IO exception at finally"

    #@50
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@53
    goto :goto_22

    #@54
    .line 1518
    .end local v0           #e:Ljava/io/IOException;
    .end local v1           #fnfe:Ljava/io/FileNotFoundException;
    :catch_54
    move-exception v2

    #@55
    .line 1519
    .local v2, ioe:Ljava/io/IOException;
    :goto_55
    :try_start_55
    const-string v6, "LockPatternUtils"

    #@57
    const-string v7, "Backup PIN checkBackupPinFile : IO exception at catch"

    #@59
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5c
    .catchall {:try_start_55 .. :try_end_5c} :catchall_6b

    #@5c
    .line 1523
    if-eqz v3, :cond_22

    #@5e
    .line 1524
    :try_start_5e
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_61
    .catch Ljava/io/IOException; {:try_start_5e .. :try_end_61} :catch_62

    #@61
    goto :goto_22

    #@62
    .line 1525
    :catch_62
    move-exception v0

    #@63
    .line 1526
    .restart local v0       #e:Ljava/io/IOException;
    const-string v6, "LockPatternUtils"

    #@65
    const-string v7, "Backup PIN checkBackupPinFile : IO exception at finally"

    #@67
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6a
    goto :goto_22

    #@6b
    .line 1522
    .end local v0           #e:Ljava/io/IOException;
    .end local v2           #ioe:Ljava/io/IOException;
    :catchall_6b
    move-exception v5

    #@6c
    .line 1523
    :goto_6c
    if-eqz v3, :cond_71

    #@6e
    .line 1524
    :try_start_6e
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_71
    .catch Ljava/io/IOException; {:try_start_6e .. :try_end_71} :catch_72

    #@71
    .line 1527
    :cond_71
    :goto_71
    throw v5

    #@72
    .line 1525
    :catch_72
    move-exception v0

    #@73
    .line 1526
    .restart local v0       #e:Ljava/io/IOException;
    const-string v6, "LockPatternUtils"

    #@75
    const-string v7, "Backup PIN checkBackupPinFile : IO exception at finally"

    #@77
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7a
    goto :goto_71

    #@7b
    .line 1522
    .end local v0           #e:Ljava/io/IOException;
    .end local v3           #raf:Ljava/io/RandomAccessFile;
    .restart local v4       #raf:Ljava/io/RandomAccessFile;
    :catchall_7b
    move-exception v5

    #@7c
    move-object v3, v4

    #@7d
    .end local v4           #raf:Ljava/io/RandomAccessFile;
    .restart local v3       #raf:Ljava/io/RandomAccessFile;
    goto :goto_6c

    #@7e
    .line 1518
    .end local v3           #raf:Ljava/io/RandomAccessFile;
    .restart local v4       #raf:Ljava/io/RandomAccessFile;
    :catch_7e
    move-exception v2

    #@7f
    move-object v3, v4

    #@80
    .end local v4           #raf:Ljava/io/RandomAccessFile;
    .restart local v3       #raf:Ljava/io/RandomAccessFile;
    goto :goto_55

    #@81
    .line 1515
    .end local v3           #raf:Ljava/io/RandomAccessFile;
    .restart local v4       #raf:Ljava/io/RandomAccessFile;
    :catch_81
    move-exception v1

    #@82
    move-object v3, v4

    #@83
    .end local v4           #raf:Ljava/io/RandomAccessFile;
    .restart local v3       #raf:Ljava/io/RandomAccessFile;
    goto :goto_3e
.end method

.method public checkPassword(Ljava/lang/String;)Z
    .registers 7
    .parameter "password"

    #@0
    .prologue
    .line 343
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentOrCallingUserId()I

    #@3
    move-result v2

    #@4
    .line 345
    .local v2, userId:I
    :try_start_4
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternUtils;->getLockSettings()Lcom/android/internal/widget/ILockSettings;

    #@7
    move-result-object v3

    #@8
    invoke-virtual {p0, p1}, Lcom/android/internal/widget/LockPatternUtils;->passwordToHash(Ljava/lang/String;)[B

    #@b
    move-result-object v4

    #@c
    invoke-interface {v3, v4, v2}, Lcom/android/internal/widget/ILockSettings;->checkPassword([BI)Z

    #@f
    move-result v0

    #@10
    .line 347
    .local v0, matched:Z
    if-eqz v0, :cond_1b

    #@12
    if-nez v2, :cond_1b

    #@14
    .line 348
    invoke-static {}, Landroid/security/KeyStore;->getInstance()Landroid/security/KeyStore;

    #@17
    move-result-object v3

    #@18
    invoke-virtual {v3, p1}, Landroid/security/KeyStore;->password(Ljava/lang/String;)Z
    :try_end_1b
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_1b} :catch_1c

    #@1b
    .line 352
    .end local v0           #matched:Z
    :cond_1b
    :goto_1b
    return v0

    #@1c
    .line 351
    :catch_1c
    move-exception v1

    #@1d
    .line 352
    .local v1, re:Landroid/os/RemoteException;
    const/4 v0, 0x1

    #@1e
    goto :goto_1b
.end method

.method public checkPasswordHistory(Ljava/lang/String;)Z
    .registers 9
    .parameter "password"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 364
    new-instance v2, Ljava/lang/String;

    #@3
    invoke-virtual {p0, p1}, Lcom/android/internal/widget/LockPatternUtils;->passwordToHash(Ljava/lang/String;)[B

    #@6
    move-result-object v6

    #@7
    invoke-direct {v2, v6}, Ljava/lang/String;-><init>([B)V

    #@a
    .line 365
    .local v2, passwordHashString:Ljava/lang/String;
    const-string v6, "lockscreen.passwordhistory"

    #@c
    invoke-direct {p0, v6}, Lcom/android/internal/widget/LockPatternUtils;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@f
    move-result-object v3

    #@10
    .line 366
    .local v3, passwordHistory:Ljava/lang/String;
    if-nez v3, :cond_13

    #@12
    .line 380
    :cond_12
    :goto_12
    return v5

    #@13
    .line 370
    :cond_13
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@16
    move-result v1

    #@17
    .line 371
    .local v1, passwordHashLength:I
    invoke-virtual {p0}, Lcom/android/internal/widget/LockPatternUtils;->getRequestedPasswordHistoryLength()I

    #@1a
    move-result v4

    #@1b
    .line 372
    .local v4, passwordHistoryLength:I
    if-eqz v4, :cond_12

    #@1d
    .line 375
    mul-int v6, v1, v4

    #@1f
    add-int/2addr v6, v4

    #@20
    add-int/lit8 v0, v6, -0x1

    #@22
    .line 377
    .local v0, neededPasswordHistoryLength:I
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@25
    move-result v6

    #@26
    if-le v6, v0, :cond_2c

    #@28
    .line 378
    invoke-virtual {v3, v5, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@2b
    move-result-object v3

    #@2c
    .line 380
    :cond_2c
    invoke-virtual {v3, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@2f
    move-result v5

    #@30
    goto :goto_12
.end method

.method public checkPattern(Ljava/util/List;)Z
    .registers 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/widget/LockPatternView$Cell;",
            ">;)Z"
        }
    .end annotation

    #@0
    .prologue
    .line 312
    .local p1, pattern:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/widget/LockPatternView$Cell;>;"
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentOrCallingUserId()I

    #@3
    move-result v2

    #@4
    .line 314
    .local v2, userId:I
    :try_start_4
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternUtils;->getLockSettings()Lcom/android/internal/widget/ILockSettings;

    #@7
    move-result-object v3

    #@8
    invoke-static {p1}, Lcom/android/internal/widget/LockPatternUtils;->patternToHash(Ljava/util/List;)[B

    #@b
    move-result-object v4

    #@c
    invoke-interface {v3, v4, v2}, Lcom/android/internal/widget/ILockSettings;->checkPattern([BI)Z

    #@f
    move-result v0

    #@10
    .line 315
    .local v0, matched:Z
    if-eqz v0, :cond_1f

    #@12
    if-nez v2, :cond_1f

    #@14
    .line 316
    invoke-static {}, Landroid/security/KeyStore;->getInstance()Landroid/security/KeyStore;

    #@17
    move-result-object v3

    #@18
    invoke-static {p1}, Lcom/android/internal/widget/LockPatternUtils;->patternToString(Ljava/util/List;)Ljava/lang/String;

    #@1b
    move-result-object v4

    #@1c
    invoke-virtual {v3, v4}, Landroid/security/KeyStore;->password(Ljava/lang/String;)Z
    :try_end_1f
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_1f} :catch_20

    #@1f
    .line 320
    .end local v0           #matched:Z
    :cond_1f
    :goto_1f
    return v0

    #@20
    .line 319
    :catch_20
    move-exception v1

    #@21
    .line 320
    .local v1, re:Landroid/os/RemoteException;
    const/4 v0, 0x1

    #@22
    goto :goto_1f
.end method

.method public checkPatternKidsMode(Ljava/util/List;)Z
    .registers 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/widget/LockPatternView$Cell;",
            ">;)Z"
        }
    .end annotation

    #@0
    .prologue
    .line 326
    .local p1, pattern:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/widget/LockPatternView$Cell;>;"
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentOrCallingUserId()I

    #@3
    move-result v2

    #@4
    .line 328
    .local v2, userId:I
    :try_start_4
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternUtils;->getLockSettings()Lcom/android/internal/widget/ILockSettings;

    #@7
    move-result-object v3

    #@8
    invoke-static {p1}, Lcom/android/internal/widget/LockPatternUtils;->patternToHash(Ljava/util/List;)[B

    #@b
    move-result-object v4

    #@c
    invoke-interface {v3, v4, v2}, Lcom/android/internal/widget/ILockSettings;->checkPatternKidsMode([BI)Z
    :try_end_f
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_f} :catch_11

    #@f
    move-result v0

    #@10
    .line 331
    :goto_10
    return v0

    #@11
    .line 330
    :catch_11
    move-exception v1

    #@12
    .line 331
    .local v1, re:Landroid/os/RemoteException;
    const/4 v0, 0x1

    #@13
    goto :goto_10
.end method

.method public clearLock(Z)V
    .registers 6
    .parameter "isFallback"

    #@0
    .prologue
    const-wide/16 v2, 0x0

    #@2
    const/4 v1, 0x0

    #@3
    .line 487
    if-nez p1, :cond_8

    #@5
    invoke-virtual {p0}, Lcom/android/internal/widget/LockPatternUtils;->deleteGallery()V

    #@8
    .line 488
    :cond_8
    const/high16 v0, 0x1

    #@a
    invoke-virtual {p0, v1, v0}, Lcom/android/internal/widget/LockPatternUtils;->saveLockPassword(Ljava/lang/String;I)V

    #@d
    .line 489
    const/4 v0, 0x0

    #@e
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/LockPatternUtils;->setLockPatternEnabled(Z)V

    #@11
    .line 490
    invoke-virtual {p0, v1}, Lcom/android/internal/widget/LockPatternUtils;->saveLockPattern(Ljava/util/List;)V

    #@14
    .line 491
    const-string v0, "lockscreen.password_type"

    #@16
    invoke-direct {p0, v0, v2, v3}, Lcom/android/internal/widget/LockPatternUtils;->setLong(Ljava/lang/String;J)V

    #@19
    .line 492
    const-string v0, "lockscreen.password_type_alternate"

    #@1b
    invoke-direct {p0, v0, v2, v3}, Lcom/android/internal/widget/LockPatternUtils;->setLong(Ljava/lang/String;J)V

    #@1e
    .line 494
    const-string v0, "SPR"

    #@20
    sget-object v1, Lcom/android/internal/widget/LockPatternUtils;->carrier:Ljava/lang/String;

    #@22
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@25
    move-result v0

    #@26
    if-nez v0, :cond_3c

    #@28
    const-string v0, "BM"

    #@2a
    sget-object v1, Lcom/android/internal/widget/LockPatternUtils;->carrier:Ljava/lang/String;

    #@2c
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2f
    move-result v0

    #@30
    if-nez v0, :cond_3c

    #@32
    const-string v0, "TRF"

    #@34
    sget-object v1, Lcom/android/internal/widget/LockPatternUtils;->carrier:Ljava/lang/String;

    #@36
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@39
    move-result v0

    #@3a
    if-eqz v0, :cond_4c

    #@3c
    :cond_3c
    invoke-virtual {p0}, Lcom/android/internal/widget/LockPatternUtils;->isOMADM()Z

    #@3f
    move-result v0

    #@40
    if-eqz v0, :cond_4c

    #@42
    .line 495
    const-string v0, "LockPatternUtils"

    #@44
    const-string v1, "clearLock() : OMADM"

    #@46
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@49
    .line 496
    invoke-virtual {p0}, Lcom/android/internal/widget/LockPatternUtils;->setOMADMPolicy()V

    #@4c
    .line 499
    :cond_4c
    return-void
.end method

.method public clearOMADM(Z)V
    .registers 5
    .parameter "isFallback"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 504
    const-string v0, "LockPatternUtils"

    #@3
    const-string v1, "clearOMADM()"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 505
    if-nez p1, :cond_14

    #@a
    .line 506
    const-string v0, "LockPatternUtils"

    #@c
    const-string v1, "deleteGallery()"

    #@e
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 507
    invoke-virtual {p0}, Lcom/android/internal/widget/LockPatternUtils;->deleteGallery()V

    #@14
    .line 509
    :cond_14
    const/high16 v0, 0x1

    #@16
    invoke-virtual {p0, v2, v0}, Lcom/android/internal/widget/LockPatternUtils;->saveLockPassword(Ljava/lang/String;I)V

    #@19
    .line 510
    const/4 v0, 0x0

    #@1a
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/LockPatternUtils;->setLockPatternEnabled(Z)V

    #@1d
    .line 511
    invoke-virtual {p0, v2}, Lcom/android/internal/widget/LockPatternUtils;->saveLockPattern(Ljava/util/List;)V

    #@20
    .line 512
    invoke-virtual {p0}, Lcom/android/internal/widget/LockPatternUtils;->reportSuccessfulPasswordAttempt()V

    #@23
    .line 514
    const-string v0, "lockscreen.password_type"

    #@25
    const-wide/32 v1, 0x10000

    #@28
    invoke-direct {p0, v0, v1, v2}, Lcom/android/internal/widget/LockPatternUtils;->setLong(Ljava/lang/String;J)V

    #@2b
    .line 515
    const-string v0, "lockscreen.password_type_alternate"

    #@2d
    const-wide/16 v1, 0x0

    #@2f
    invoke-direct {p0, v0, v1, v2}, Lcom/android/internal/widget/LockPatternUtils;->setLong(Ljava/lang/String;J)V

    #@32
    .line 516
    return-void
.end method

.method deleteGallery()V
    .registers 4

    #@0
    .prologue
    .line 562
    invoke-virtual {p0}, Lcom/android/internal/widget/LockPatternUtils;->usingBiometricWeak()Z

    #@3
    move-result v1

    #@4
    if-eqz v1, :cond_1c

    #@6
    .line 563
    new-instance v1, Landroid/content/Intent;

    #@8
    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    #@b
    const-string v2, "com.android.facelock.DELETE_GALLERY"

    #@d
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@10
    move-result-object v0

    #@11
    .line 564
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "deleteGallery"

    #@13
    const/4 v2, 0x1

    #@14
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@17
    .line 565
    iget-object v1, p0, Lcom/android/internal/widget/LockPatternUtils;->mContext:Landroid/content/Context;

    #@19
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@1c
    .line 567
    .end local v0           #intent:Landroid/content/Intent;
    :cond_1c
    return-void
.end method

.method public deleteTempGallery()V
    .registers 4

    #@0
    .prologue
    .line 553
    new-instance v1, Landroid/content/Intent;

    #@2
    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    #@5
    const-string v2, "com.android.facelock.DELETE_GALLERY"

    #@7
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@a
    move-result-object v0

    #@b
    .line 554
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "deleteTempGallery"

    #@d
    const/4 v2, 0x1

    #@e
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@11
    .line 555
    iget-object v1, p0, Lcom/android/internal/widget/LockPatternUtils;->mContext:Landroid/content/Context;

    #@13
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@16
    .line 556
    return-void
.end method

.method public getActivePasswordQuality()I
    .registers 6

    #@0
    .prologue
    .line 442
    const/4 v0, 0x0

    #@1
    .line 445
    .local v0, activePasswordQuality:I
    const-string v2, "lockscreen.password_type"

    #@3
    const-wide/32 v3, 0x10000

    #@6
    invoke-direct {p0, v2, v3, v4}, Lcom/android/internal/widget/LockPatternUtils;->getLong(Ljava/lang/String;J)J

    #@9
    move-result-wide v2

    #@a
    long-to-int v1, v2

    #@b
    .line 447
    .local v1, quality:I
    sparse-switch v1, :sswitch_data_46

    #@e
    .line 480
    :cond_e
    :goto_e
    return v0

    #@f
    .line 449
    :sswitch_f
    invoke-virtual {p0}, Lcom/android/internal/widget/LockPatternUtils;->isLockPatternEnabled()Z

    #@12
    move-result v2

    #@13
    if-eqz v2, :cond_e

    #@15
    .line 450
    const/high16 v0, 0x1

    #@17
    goto :goto_e

    #@18
    .line 454
    :sswitch_18
    invoke-virtual {p0}, Lcom/android/internal/widget/LockPatternUtils;->isBiometricWeakInstalled()Z

    #@1b
    move-result v2

    #@1c
    if-eqz v2, :cond_e

    #@1e
    .line 455
    const v0, 0x8000

    #@21
    goto :goto_e

    #@22
    .line 459
    :sswitch_22
    invoke-virtual {p0}, Lcom/android/internal/widget/LockPatternUtils;->isLockPasswordEnabled()Z

    #@25
    move-result v2

    #@26
    if-eqz v2, :cond_e

    #@28
    .line 460
    const/high16 v0, 0x2

    #@2a
    goto :goto_e

    #@2b
    .line 464
    :sswitch_2b
    invoke-virtual {p0}, Lcom/android/internal/widget/LockPatternUtils;->isLockPasswordEnabled()Z

    #@2e
    move-result v2

    #@2f
    if-eqz v2, :cond_e

    #@31
    .line 465
    const/high16 v0, 0x4

    #@33
    goto :goto_e

    #@34
    .line 469
    :sswitch_34
    invoke-virtual {p0}, Lcom/android/internal/widget/LockPatternUtils;->isLockPasswordEnabled()Z

    #@37
    move-result v2

    #@38
    if-eqz v2, :cond_e

    #@3a
    .line 470
    const/high16 v0, 0x5

    #@3c
    goto :goto_e

    #@3d
    .line 474
    :sswitch_3d
    invoke-virtual {p0}, Lcom/android/internal/widget/LockPatternUtils;->isLockPasswordEnabled()Z

    #@40
    move-result v2

    #@41
    if-eqz v2, :cond_e

    #@43
    .line 475
    const/high16 v0, 0x6

    #@45
    goto :goto_e

    #@46
    .line 447
    :sswitch_data_46
    .sparse-switch
        0x8000 -> :sswitch_18
        0x10000 -> :sswitch_f
        0x20000 -> :sswitch_22
        0x40000 -> :sswitch_2b
        0x50000 -> :sswitch_34
        0x60000 -> :sswitch_3d
    .end sparse-switch
.end method

.method public getAppWidgets()[I
    .registers 11

    #@0
    .prologue
    .line 1166
    iget-object v7, p0, Lcom/android/internal/widget/LockPatternUtils;->mContentResolver:Landroid/content/ContentResolver;

    #@2
    const-string v8, "lock_screen_appwidget_ids"

    #@4
    const/4 v9, -0x2

    #@5
    invoke-static {v7, v8, v9}, Landroid/provider/Settings$Secure;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    #@8
    move-result-object v1

    #@9
    .line 1169
    .local v1, appWidgetIdString:Ljava/lang/String;
    const-string v4, ","

    #@b
    .line 1170
    .local v4, delims:Ljava/lang/String;
    if-eqz v1, :cond_48

    #@d
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@10
    move-result v7

    #@11
    if-lez v7, :cond_48

    #@13
    .line 1171
    invoke-virtual {v1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@16
    move-result-object v3

    #@17
    .line 1172
    .local v3, appWidgetStringIds:[Ljava/lang/String;
    array-length v7, v3

    #@18
    new-array v2, v7, [I

    #@1a
    .line 1173
    .local v2, appWidgetIds:[I
    const/4 v6, 0x0

    #@1b
    .local v6, i:I
    :goto_1b
    array-length v7, v3

    #@1c
    if-ge v6, v7, :cond_47

    #@1e
    .line 1174
    aget-object v0, v3, v6

    #@20
    .line 1176
    .local v0, appWidget:Ljava/lang/String;
    :try_start_20
    invoke-static {v0}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    #@23
    move-result-object v7

    #@24
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    #@27
    move-result v7

    #@28
    aput v7, v2, v6
    :try_end_2a
    .catch Ljava/lang/NumberFormatException; {:try_start_20 .. :try_end_2a} :catch_2d

    #@2a
    .line 1173
    add-int/lit8 v6, v6, 0x1

    #@2c
    goto :goto_1b

    #@2d
    .line 1177
    :catch_2d
    move-exception v5

    #@2e
    .line 1178
    .local v5, e:Ljava/lang/NumberFormatException;
    const-string v7, "LockPatternUtils"

    #@30
    new-instance v8, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    const-string v9, "Error when parsing widget id "

    #@37
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v8

    #@3b
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v8

    #@3f
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v8

    #@43
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@46
    .line 1179
    const/4 v2, 0x0

    #@47
    .line 1184
    .end local v0           #appWidget:Ljava/lang/String;
    .end local v2           #appWidgetIds:[I
    .end local v3           #appWidgetStringIds:[Ljava/lang/String;
    .end local v5           #e:Ljava/lang/NumberFormatException;
    .end local v6           #i:I
    :cond_47
    :goto_47
    return-object v2

    #@48
    :cond_48
    const/4 v7, 0x0

    #@49
    new-array v2, v7, [I

    #@4b
    goto :goto_47
.end method

.method public getCurrentUser()I
    .registers 4

    #@0
    .prologue
    .line 275
    sget v1, Lcom/android/internal/widget/LockPatternUtils;->sCurrentUserId:I

    #@2
    const/16 v2, -0x2710

    #@4
    if-eq v1, v2, :cond_9

    #@6
    .line 277
    sget v1, Lcom/android/internal/widget/LockPatternUtils;->sCurrentUserId:I

    #@8
    .line 282
    :goto_8
    return v1

    #@9
    .line 280
    :cond_9
    :try_start_9
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@c
    move-result-object v1

    #@d
    invoke-interface {v1}, Landroid/app/IActivityManager;->getCurrentUser()Landroid/content/pm/UserInfo;

    #@10
    move-result-object v1

    #@11
    iget v1, v1, Landroid/content/pm/UserInfo;->id:I
    :try_end_13
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_13} :catch_14

    #@13
    goto :goto_8

    #@14
    .line 281
    :catch_14
    move-exception v0

    #@15
    .line 282
    .local v0, re:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@16
    goto :goto_8
.end method

.method public getDevicePolicyManager()Landroid/app/admin/DevicePolicyManager;
    .registers 5

    #@0
    .prologue
    .line 176
    iget-object v0, p0, Lcom/android/internal/widget/LockPatternUtils;->mDevicePolicyManager:Landroid/app/admin/DevicePolicyManager;

    #@2
    if-nez v0, :cond_22

    #@4
    .line 177
    iget-object v0, p0, Lcom/android/internal/widget/LockPatternUtils;->mContext:Landroid/content/Context;

    #@6
    const-string v1, "device_policy"

    #@8
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@b
    move-result-object v0

    #@c
    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    #@e
    iput-object v0, p0, Lcom/android/internal/widget/LockPatternUtils;->mDevicePolicyManager:Landroid/app/admin/DevicePolicyManager;

    #@10
    .line 179
    iget-object v0, p0, Lcom/android/internal/widget/LockPatternUtils;->mDevicePolicyManager:Landroid/app/admin/DevicePolicyManager;

    #@12
    if-nez v0, :cond_22

    #@14
    .line 180
    const-string v0, "LockPatternUtils"

    #@16
    const-string v1, "Can\'t get DevicePolicyManagerService: is it running?"

    #@18
    new-instance v2, Ljava/lang/IllegalStateException;

    #@1a
    const-string v3, "Stack trace:"

    #@1c
    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@1f
    invoke-static {v0, v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@22
    .line 184
    :cond_22
    iget-object v0, p0, Lcom/android/internal/widget/LockPatternUtils;->mDevicePolicyManager:Landroid/app/admin/DevicePolicyManager;

    #@24
    return-object v0
.end method

.method public getFallbackAppWidgetId()I
    .registers 5

    #@0
    .prologue
    .line 1235
    iget-object v0, p0, Lcom/android/internal/widget/LockPatternUtils;->mContentResolver:Landroid/content/ContentResolver;

    #@2
    const-string v1, "lock_screen_fallback_appwidget_id"

    #@4
    const/4 v2, 0x0

    #@5
    const/4 v3, -0x2

    #@6
    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public getIntentForClockAppWidget()Landroid/content/Intent;
    .registers 4

    #@0
    .prologue
    .line 1564
    new-instance v0, Landroid/content/Intent;

    #@2
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    #@5
    .line 1565
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "com.android.deskclock"

    #@7
    const-string v2, "com.android.deskclock.DeskClock"

    #@9
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@c
    .line 1566
    return-object v0
.end method

.method public getKeyguardStoredPasswordQuality()I
    .registers 6

    #@0
    .prologue
    const-wide/32 v3, 0x10000

    #@3
    .line 824
    const-string v1, "lockscreen.password_type"

    #@5
    invoke-direct {p0, v1, v3, v4}, Lcom/android/internal/widget/LockPatternUtils;->getLong(Ljava/lang/String;J)J

    #@8
    move-result-wide v1

    #@9
    long-to-int v0, v1

    #@a
    .line 828
    .local v0, quality:I
    const v1, 0x8000

    #@d
    if-ne v0, v1, :cond_16

    #@f
    .line 829
    const-string v1, "lockscreen.password_type_alternate"

    #@11
    invoke-direct {p0, v1, v3, v4}, Lcom/android/internal/widget/LockPatternUtils;->getLong(Ljava/lang/String;J)J

    #@14
    move-result-wide v1

    #@15
    long-to-int v0, v1

    #@16
    .line 833
    :cond_16
    return v0
.end method

.method public getLockoutAttemptDeadline()J
    .registers 9

    #@0
    .prologue
    const-wide/16 v4, 0x0

    #@2
    .line 1081
    const-string v6, "lockscreen.lockoutattemptdeadline"

    #@4
    invoke-direct {p0, v6, v4, v5}, Lcom/android/internal/widget/LockPatternUtils;->getLong(Ljava/lang/String;J)J

    #@7
    move-result-wide v0

    #@8
    .line 1082
    .local v0, deadline:J
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@b
    move-result-wide v2

    #@c
    .line 1083
    .local v2, now:J
    cmp-long v6, v0, v2

    #@e
    if-ltz v6, :cond_17

    #@10
    const-wide/16 v6, 0x7530

    #@12
    add-long/2addr v6, v2

    #@13
    cmp-long v6, v0, v6

    #@15
    if-lez v6, :cond_18

    #@17
    :cond_17
    move-wide v0, v4

    #@18
    .line 1086
    .end local v0           #deadline:J
    :cond_18
    return-wide v0
.end method

.method public getNextAlarm()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 1139
    iget-object v1, p0, Lcom/android/internal/widget/LockPatternUtils;->mContentResolver:Landroid/content/ContentResolver;

    #@2
    const-string v2, "next_alarm_formatted"

    #@4
    const/4 v3, -0x2

    #@5
    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    .line 1141
    .local v0, nextAlarm:Ljava/lang/String;
    if-eqz v0, :cond_11

    #@b
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@e
    move-result v1

    #@f
    if-eqz v1, :cond_12

    #@11
    .line 1142
    :cond_11
    const/4 v0, 0x0

    #@12
    .line 1144
    .end local v0           #nextAlarm:Ljava/lang/String;
    :cond_12
    return-object v0
.end method

.method public getOMADMLockCode()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 1578
    const-string v0, "LockPatternUtils"

    #@2
    const-string v1, "OMADM getOMADM LockCode"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1579
    iget-object v0, p0, Lcom/android/internal/widget/LockPatternUtils;->mContext:Landroid/content/Context;

    #@9
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@c
    move-result-object v0

    #@d
    const-string v1, "lg_omadm_lwmo_lock_code"

    #@f
    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    return-object v0
.end method

.method public getPowerButtonInstantlyLocks()Z
    .registers 3

    #@0
    .prologue
    .line 1549
    const-string v0, "lockscreen.power_button_instantly_locks"

    #@2
    const/4 v1, 0x1

    #@3
    invoke-direct {p0, v0, v1}, Lcom/android/internal/widget/LockPatternUtils;->getBoolean(Ljava/lang/String;Z)Z

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public getRequestedMinimumPasswordLength()I
    .registers 4

    #@0
    .prologue
    .line 212
    invoke-virtual {p0}, Lcom/android/internal/widget/LockPatternUtils;->getDevicePolicyManager()Landroid/app/admin/DevicePolicyManager;

    #@3
    move-result-object v0

    #@4
    const/4 v1, 0x0

    #@5
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentOrCallingUserId()I

    #@8
    move-result v2

    #@9
    invoke-virtual {v0, v1, v2}, Landroid/app/admin/DevicePolicyManager;->getPasswordMinimumLength(Landroid/content/ComponentName;I)I

    #@c
    move-result v0

    #@d
    return v0
.end method

.method public getRequestedPasswordHistoryLength()I
    .registers 4

    #@0
    .prologue
    .line 224
    invoke-virtual {p0}, Lcom/android/internal/widget/LockPatternUtils;->getDevicePolicyManager()Landroid/app/admin/DevicePolicyManager;

    #@3
    move-result-object v0

    #@4
    const/4 v1, 0x0

    #@5
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentOrCallingUserId()I

    #@8
    move-result v2

    #@9
    invoke-virtual {v0, v1, v2}, Landroid/app/admin/DevicePolicyManager;->getPasswordHistoryLength(Landroid/content/ComponentName;I)I

    #@c
    move-result v0

    #@d
    return v0
.end method

.method public getRequestedPasswordMinimumLetters()I
    .registers 4

    #@0
    .prologue
    .line 228
    invoke-virtual {p0}, Lcom/android/internal/widget/LockPatternUtils;->getDevicePolicyManager()Landroid/app/admin/DevicePolicyManager;

    #@3
    move-result-object v0

    #@4
    const/4 v1, 0x0

    #@5
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentOrCallingUserId()I

    #@8
    move-result v2

    #@9
    invoke-virtual {v0, v1, v2}, Landroid/app/admin/DevicePolicyManager;->getPasswordMinimumLetters(Landroid/content/ComponentName;I)I

    #@c
    move-result v0

    #@d
    return v0
.end method

.method public getRequestedPasswordMinimumLowerCase()I
    .registers 4

    #@0
    .prologue
    .line 238
    invoke-virtual {p0}, Lcom/android/internal/widget/LockPatternUtils;->getDevicePolicyManager()Landroid/app/admin/DevicePolicyManager;

    #@3
    move-result-object v0

    #@4
    const/4 v1, 0x0

    #@5
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentOrCallingUserId()I

    #@8
    move-result v2

    #@9
    invoke-virtual {v0, v1, v2}, Landroid/app/admin/DevicePolicyManager;->getPasswordMinimumLowerCase(Landroid/content/ComponentName;I)I

    #@c
    move-result v0

    #@d
    return v0
.end method

.method public getRequestedPasswordMinimumNonLetter()I
    .registers 4

    #@0
    .prologue
    .line 253
    invoke-virtual {p0}, Lcom/android/internal/widget/LockPatternUtils;->getDevicePolicyManager()Landroid/app/admin/DevicePolicyManager;

    #@3
    move-result-object v0

    #@4
    const/4 v1, 0x0

    #@5
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentOrCallingUserId()I

    #@8
    move-result v2

    #@9
    invoke-virtual {v0, v1, v2}, Landroid/app/admin/DevicePolicyManager;->getPasswordMinimumNonLetter(Landroid/content/ComponentName;I)I

    #@c
    move-result v0

    #@d
    return v0
.end method

.method public getRequestedPasswordMinimumNumeric()I
    .registers 4

    #@0
    .prologue
    .line 243
    invoke-virtual {p0}, Lcom/android/internal/widget/LockPatternUtils;->getDevicePolicyManager()Landroid/app/admin/DevicePolicyManager;

    #@3
    move-result-object v0

    #@4
    const/4 v1, 0x0

    #@5
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentOrCallingUserId()I

    #@8
    move-result v2

    #@9
    invoke-virtual {v0, v1, v2}, Landroid/app/admin/DevicePolicyManager;->getPasswordMinimumNumeric(Landroid/content/ComponentName;I)I

    #@c
    move-result v0

    #@d
    return v0
.end method

.method public getRequestedPasswordMinimumSymbols()I
    .registers 4

    #@0
    .prologue
    .line 248
    invoke-virtual {p0}, Lcom/android/internal/widget/LockPatternUtils;->getDevicePolicyManager()Landroid/app/admin/DevicePolicyManager;

    #@3
    move-result-object v0

    #@4
    const/4 v1, 0x0

    #@5
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentOrCallingUserId()I

    #@8
    move-result v2

    #@9
    invoke-virtual {v0, v1, v2}, Landroid/app/admin/DevicePolicyManager;->getPasswordMinimumSymbols(Landroid/content/ComponentName;I)I

    #@c
    move-result v0

    #@d
    return v0
.end method

.method public getRequestedPasswordMinimumUpperCase()I
    .registers 4

    #@0
    .prologue
    .line 233
    invoke-virtual {p0}, Lcom/android/internal/widget/LockPatternUtils;->getDevicePolicyManager()Landroid/app/admin/DevicePolicyManager;

    #@3
    move-result-object v0

    #@4
    const/4 v1, 0x0

    #@5
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentOrCallingUserId()I

    #@8
    move-result v2

    #@9
    invoke-virtual {v0, v1, v2}, Landroid/app/admin/DevicePolicyManager;->getPasswordMinimumUpperCase(Landroid/content/ComponentName;I)I

    #@c
    move-result v0

    #@d
    return v0
.end method

.method public getRequestedPasswordQuality()I
    .registers 4

    #@0
    .prologue
    .line 220
    invoke-virtual {p0}, Lcom/android/internal/widget/LockPatternUtils;->getDevicePolicyManager()Landroid/app/admin/DevicePolicyManager;

    #@3
    move-result-object v0

    #@4
    const/4 v1, 0x0

    #@5
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentOrCallingUserId()I

    #@8
    move-result v2

    #@9
    invoke-virtual {v0, v1, v2}, Landroid/app/admin/DevicePolicyManager;->getPasswordQuality(Landroid/content/ComponentName;I)I

    #@c
    move-result v0

    #@d
    return v0
.end method

.method public isBiometricWeakEverChosen()Z
    .registers 3

    #@0
    .prologue
    .line 434
    const-string v0, "lockscreen.biometricweakeverchosen"

    #@2
    const/4 v1, 0x0

    #@3
    invoke-direct {p0, v0, v1}, Lcom/android/internal/widget/LockPatternUtils;->getBoolean(Ljava/lang/String;Z)Z

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public isBiometricWeakInstalled()Z
    .registers 8

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 994
    iget-object v4, p0, Lcom/android/internal/widget/LockPatternUtils;->mContext:Landroid/content/Context;

    #@4
    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@7
    move-result-object v1

    #@8
    .line 996
    .local v1, pm:Landroid/content/pm/PackageManager;
    :try_start_8
    const-string v4, "com.android.facelock"

    #@a
    const/4 v5, 0x1

    #@b
    invoke-virtual {v1, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_e
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_8 .. :try_end_e} :catch_17

    #@e
    .line 1002
    const-string v4, "android.hardware.camera.front"

    #@10
    invoke-virtual {v1, v4}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    #@13
    move-result v4

    #@14
    if-nez v4, :cond_19

    #@16
    .line 1010
    :cond_16
    :goto_16
    return v2

    #@17
    .line 997
    :catch_17
    move-exception v0

    #@18
    .line 998
    .local v0, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    goto :goto_16

    #@19
    .line 1005
    .end local v0           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_19
    invoke-virtual {p0}, Lcom/android/internal/widget/LockPatternUtils;->getDevicePolicyManager()Landroid/app/admin/DevicePolicyManager;

    #@1c
    move-result-object v4

    #@1d
    const/4 v5, 0x0

    #@1e
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentOrCallingUserId()I

    #@21
    move-result v6

    #@22
    invoke-virtual {v4, v5, v6}, Landroid/app/admin/DevicePolicyManager;->getCameraDisabled(Landroid/content/ComponentName;I)Z

    #@25
    move-result v4

    #@26
    if-nez v4, :cond_16

    #@28
    move v2, v3

    #@29
    .line 1010
    goto :goto_16
.end method

.method public isBiometricWeakLivelinessEnabled()Z
    .registers 7

    #@0
    .prologue
    const-wide/16 v4, 0x0

    #@2
    .line 1031
    const-string v2, "lock_biometric_weak_flags"

    #@4
    invoke-direct {p0, v2, v4, v5}, Lcom/android/internal/widget/LockPatternUtils;->getLong(Ljava/lang/String;J)J

    #@7
    move-result-wide v0

    #@8
    .line 1032
    .local v0, currentFlag:J
    const-wide/16 v2, 0x1

    #@a
    and-long/2addr v2, v0

    #@b
    cmp-long v2, v2, v4

    #@d
    if-eqz v2, :cond_11

    #@f
    const/4 v2, 0x1

    #@10
    :goto_10
    return v2

    #@11
    :cond_11
    const/4 v2, 0x0

    #@12
    goto :goto_10
.end method

.method public isEmergencyCallCapable()Z
    .registers 3

    #@0
    .prologue
    .line 1120
    iget-object v0, p0, Lcom/android/internal/widget/LockPatternUtils;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@5
    move-result-object v0

    #@6
    const v1, 0x1110030

    #@9
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@c
    move-result v0

    #@d
    return v0
.end method

.method public isEmergencyCallEnabledWhileSimLocked()Z
    .registers 3

    #@0
    .prologue
    .line 1130
    iget-object v0, p0, Lcom/android/internal/widget/LockPatternUtils;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@5
    move-result-object v0

    #@6
    const v1, 0x1110029

    #@9
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@c
    move-result v0

    #@d
    return v0
.end method

.method public isHiddenPatternDisabled()Z
    .registers 2

    #@0
    .prologue
    .line 525
    iget-boolean v0, p0, Lcom/android/internal/widget/LockPatternUtils;->mHiddenPatternDisabled:Z

    #@2
    return v0
.end method

.method public isLockPasswordEnabled()Z
    .registers 16

    #@0
    .prologue
    const-wide/32 v13, 0x40000

    #@3
    const-wide/32 v11, 0x20000

    #@6
    const-wide/16 v9, 0x0

    #@8
    const/4 v7, 0x1

    #@9
    const/4 v6, 0x0

    #@a
    .line 960
    const-string v8, "lockscreen.password_type"

    #@c
    invoke-direct {p0, v8, v9, v10}, Lcom/android/internal/widget/LockPatternUtils;->getLong(Ljava/lang/String;J)J

    #@f
    move-result-wide v3

    #@10
    .line 961
    .local v3, mode:J
    const-string v8, "lockscreen.password_type_alternate"

    #@12
    invoke-direct {p0, v8, v9, v10}, Lcom/android/internal/widget/LockPatternUtils;->getLong(Ljava/lang/String;J)J

    #@15
    move-result-wide v1

    #@16
    .line 962
    .local v1, backupMode:J
    cmp-long v8, v3, v13

    #@18
    if-eqz v8, :cond_2c

    #@1a
    cmp-long v8, v3, v11

    #@1c
    if-eqz v8, :cond_2c

    #@1e
    const-wide/32 v8, 0x50000

    #@21
    cmp-long v8, v3, v8

    #@23
    if-eqz v8, :cond_2c

    #@25
    const-wide/32 v8, 0x60000

    #@28
    cmp-long v8, v3, v8

    #@2a
    if-nez v8, :cond_55

    #@2c
    :cond_2c
    move v5, v7

    #@2d
    .line 966
    .local v5, passwordEnabled:Z
    :goto_2d
    cmp-long v8, v1, v13

    #@2f
    if-eqz v8, :cond_43

    #@31
    cmp-long v8, v1, v11

    #@33
    if-eqz v8, :cond_43

    #@35
    const-wide/32 v8, 0x50000

    #@38
    cmp-long v8, v1, v8

    #@3a
    if-eqz v8, :cond_43

    #@3c
    const-wide/32 v8, 0x60000

    #@3f
    cmp-long v8, v1, v8

    #@41
    if-nez v8, :cond_57

    #@43
    :cond_43
    move v0, v7

    #@44
    .line 971
    .local v0, backupEnabled:Z
    :goto_44
    invoke-virtual {p0}, Lcom/android/internal/widget/LockPatternUtils;->savedPasswordExists()Z

    #@47
    move-result v8

    #@48
    if-eqz v8, :cond_59

    #@4a
    if-nez v5, :cond_54

    #@4c
    invoke-virtual {p0}, Lcom/android/internal/widget/LockPatternUtils;->usingBiometricWeak()Z

    #@4f
    move-result v8

    #@50
    if-eqz v8, :cond_59

    #@52
    if-eqz v0, :cond_59

    #@54
    :cond_54
    :goto_54
    return v7

    #@55
    .end local v0           #backupEnabled:Z
    .end local v5           #passwordEnabled:Z
    :cond_55
    move v5, v6

    #@56
    .line 962
    goto :goto_2d

    #@57
    .restart local v5       #passwordEnabled:Z
    :cond_57
    move v0, v6

    #@58
    .line 966
    goto :goto_44

    #@59
    .restart local v0       #backupEnabled:Z
    :cond_59
    move v7, v6

    #@5a
    .line 971
    goto :goto_54
.end method

.method public isLockPattern()Z
    .registers 8

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 1359
    invoke-virtual {p0}, Lcom/android/internal/widget/LockPatternUtils;->getKeyguardStoredPasswordQuality()I

    #@5
    move-result v5

    #@6
    int-to-long v2, v5

    #@7
    .line 1360
    .local v2, mode:J
    const-wide/32 v5, 0x10000

    #@a
    cmp-long v5, v2, v5

    #@c
    if-nez v5, :cond_1e

    #@e
    move v1, v0

    #@f
    .line 1362
    .local v1, isPatternMode:Z
    :goto_f
    if-eqz v1, :cond_20

    #@11
    invoke-virtual {p0}, Lcom/android/internal/widget/LockPatternUtils;->isLockPatternEnabled()Z

    #@14
    move-result v5

    #@15
    if-eqz v5, :cond_20

    #@17
    invoke-virtual {p0}, Lcom/android/internal/widget/LockPatternUtils;->savedPatternExists()Z

    #@1a
    move-result v5

    #@1b
    if-eqz v5, :cond_20

    #@1d
    .line 1363
    .local v0, isPattern:Z
    :goto_1d
    return v0

    #@1e
    .end local v0           #isPattern:Z
    .end local v1           #isPatternMode:Z
    :cond_1e
    move v1, v4

    #@1f
    .line 1360
    goto :goto_f

    #@20
    .restart local v1       #isPatternMode:Z
    :cond_20
    move v0, v4

    #@21
    .line 1362
    goto :goto_1d
.end method

.method public isLockPatternEnabled()Z
    .registers 8

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    const-wide/32 v5, 0x10000

    #@5
    .line 979
    const-string v3, "lockscreen.password_type_alternate"

    #@7
    invoke-direct {p0, v3, v5, v6}, Lcom/android/internal/widget/LockPatternUtils;->getLong(Ljava/lang/String;J)J

    #@a
    move-result-wide v3

    #@b
    cmp-long v3, v3, v5

    #@d
    if-nez v3, :cond_2b

    #@f
    move v0, v1

    #@10
    .line 983
    .local v0, backupEnabled:Z
    :goto_10
    const-string v3, "lock_pattern_autolock"

    #@12
    invoke-direct {p0, v3, v2}, Lcom/android/internal/widget/LockPatternUtils;->getBoolean(Ljava/lang/String;Z)Z

    #@15
    move-result v3

    #@16
    if-eqz v3, :cond_2d

    #@18
    const-string v3, "lockscreen.password_type"

    #@1a
    invoke-direct {p0, v3, v5, v6}, Lcom/android/internal/widget/LockPatternUtils;->getLong(Ljava/lang/String;J)J

    #@1d
    move-result-wide v3

    #@1e
    cmp-long v3, v3, v5

    #@20
    if-eqz v3, :cond_2a

    #@22
    invoke-virtual {p0}, Lcom/android/internal/widget/LockPatternUtils;->usingBiometricWeak()Z

    #@25
    move-result v3

    #@26
    if-eqz v3, :cond_2d

    #@28
    if-eqz v0, :cond_2d

    #@2a
    :cond_2a
    :goto_2a
    return v1

    #@2b
    .end local v0           #backupEnabled:Z
    :cond_2b
    move v0, v2

    #@2c
    .line 979
    goto :goto_10

    #@2d
    .restart local v0       #backupEnabled:Z
    :cond_2d
    move v1, v2

    #@2e
    .line 983
    goto :goto_2a
.end method

.method public isLockScreenDisabled()Z
    .registers 5

    #@0
    .prologue
    const-wide/16 v2, 0x0

    #@2
    .line 546
    invoke-virtual {p0}, Lcom/android/internal/widget/LockPatternUtils;->isSecure()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_14

    #@8
    const-string v0, "lockscreen.disabled"

    #@a
    invoke-direct {p0, v0, v2, v3}, Lcom/android/internal/widget/LockPatternUtils;->getLong(Ljava/lang/String;J)J

    #@d
    move-result-wide v0

    #@e
    cmp-long v0, v0, v2

    #@10
    if-eqz v0, :cond_14

    #@12
    const/4 v0, 0x1

    #@13
    :goto_13
    return v0

    #@14
    :cond_14
    const/4 v0, 0x0

    #@15
    goto :goto_13
.end method

.method public isOMADM()Z
    .registers 5

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 1583
    iget-object v2, p0, Lcom/android/internal/widget/LockPatternUtils;->mContext:Landroid/content/Context;

    #@4
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7
    move-result-object v2

    #@8
    const-string v3, "lg_omadm_lwmo_lock_state"

    #@a
    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@d
    move-result v2

    #@e
    if-ne v2, v0, :cond_18

    #@10
    .line 1584
    const-string v1, "LockPatternUtils"

    #@12
    const-string v2, "OMADM Lock is ON"

    #@14
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@17
    .line 1588
    :goto_17
    return v0

    #@18
    .line 1587
    :cond_18
    const-string v0, "LockPatternUtils"

    #@1a
    const-string v2, "OMADM Lock is OFF"

    #@1c
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    move v0, v1

    #@20
    .line 1588
    goto :goto_17
.end method

.method public isPatternEverChosen()Z
    .registers 3

    #@0
    .prologue
    .line 424
    const-string v0, "lockscreen.patterneverchosen"

    #@2
    const/4 v1, 0x0

    #@3
    invoke-direct {p0, v0, v1}, Lcom/android/internal/widget/LockPatternUtils;->getBoolean(Ljava/lang/String;Z)Z

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public isPermanentlyLocked()Z
    .registers 3

    #@0
    .prologue
    .line 1104
    const-string v0, "lockscreen.lockedoutpermanently"

    #@2
    const/4 v1, 0x0

    #@3
    invoke-direct {p0, v0, v1}, Lcom/android/internal/widget/LockPatternUtils;->getBoolean(Ljava/lang/String;Z)Z

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public isPukUnlockScreenEnable()Z
    .registers 3

    #@0
    .prologue
    .line 1125
    iget-object v0, p0, Lcom/android/internal/widget/LockPatternUtils;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@5
    move-result-object v0

    #@6
    const v1, 0x1110028

    #@9
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@c
    move-result v0

    #@d
    return v0
.end method

.method public isSecure()Z
    .registers 9

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    .line 1340
    invoke-virtual {p0}, Lcom/android/internal/widget/LockPatternUtils;->isOMADM()Z

    #@5
    move-result v6

    #@6
    if-nez v6, :cond_4f

    #@8
    .line 1341
    invoke-virtual {p0}, Lcom/android/internal/widget/LockPatternUtils;->getKeyguardStoredPasswordQuality()I

    #@b
    move-result v6

    #@c
    int-to-long v2, v6

    #@d
    .line 1342
    .local v2, mode:J
    const-wide/32 v6, 0x10000

    #@10
    cmp-long v6, v2, v6

    #@12
    if-nez v6, :cond_49

    #@14
    move v1, v4

    #@15
    .line 1343
    .local v1, isPattern:Z
    :goto_15
    const-wide/32 v6, 0x20000

    #@18
    cmp-long v6, v2, v6

    #@1a
    if-eqz v6, :cond_31

    #@1c
    const-wide/32 v6, 0x40000

    #@1f
    cmp-long v6, v2, v6

    #@21
    if-eqz v6, :cond_31

    #@23
    const-wide/32 v6, 0x50000

    #@26
    cmp-long v6, v2, v6

    #@28
    if-eqz v6, :cond_31

    #@2a
    const-wide/32 v6, 0x60000

    #@2d
    cmp-long v6, v2, v6

    #@2f
    if-nez v6, :cond_4b

    #@31
    :cond_31
    move v0, v4

    #@32
    .line 1347
    .local v0, isPassword:Z
    :goto_32
    if-eqz v1, :cond_40

    #@34
    invoke-virtual {p0}, Lcom/android/internal/widget/LockPatternUtils;->isLockPatternEnabled()Z

    #@37
    move-result v6

    #@38
    if-eqz v6, :cond_40

    #@3a
    invoke-virtual {p0}, Lcom/android/internal/widget/LockPatternUtils;->savedPatternExists()Z

    #@3d
    move-result v6

    #@3e
    if-nez v6, :cond_48

    #@40
    :cond_40
    if-eqz v0, :cond_4d

    #@42
    invoke-virtual {p0}, Lcom/android/internal/widget/LockPatternUtils;->savedPasswordExists()Z

    #@45
    move-result v6

    #@46
    if-eqz v6, :cond_4d

    #@48
    .line 1352
    .end local v0           #isPassword:Z
    .end local v1           #isPattern:Z
    .end local v2           #mode:J
    :cond_48
    :goto_48
    return v4

    #@49
    .restart local v2       #mode:J
    :cond_49
    move v1, v5

    #@4a
    .line 1342
    goto :goto_15

    #@4b
    .restart local v1       #isPattern:Z
    :cond_4b
    move v0, v5

    #@4c
    .line 1343
    goto :goto_32

    #@4d
    .restart local v0       #isPassword:Z
    :cond_4d
    move v4, v5

    #@4e
    .line 1347
    goto :goto_48

    #@4f
    .line 1351
    .end local v0           #isPassword:Z
    .end local v1           #isPattern:Z
    .end local v2           #mode:J
    :cond_4f
    const-string v5, "LockPatternUtils"

    #@51
    const-string v6, "OMADM isSecure"

    #@53
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@56
    goto :goto_48
.end method

.method public isTactileFeedbackEnabled()Z
    .registers 5

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 1060
    iget-object v1, p0, Lcom/android/internal/widget/LockPatternUtils;->mContentResolver:Landroid/content/ContentResolver;

    #@3
    const-string v2, "haptic_feedback_enabled"

    #@5
    const/4 v3, -0x2

    #@6
    invoke-static {v1, v2, v0, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_d

    #@c
    :goto_c
    return v0

    #@d
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_c
.end method

.method public isVisiblePatternEnabled()Z
    .registers 3

    #@0
    .prologue
    .line 1046
    const-string v0, "lock_pattern_visible_pattern"

    #@2
    const/4 v1, 0x0

    #@3
    invoke-direct {p0, v0, v1}, Lcom/android/internal/widget/LockPatternUtils;->getBoolean(Ljava/lang/String;Z)Z

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public passwordToHash(Ljava/lang/String;)[B
    .registers 11
    .parameter "password"

    #@0
    .prologue
    .line 930
    if-nez p1, :cond_4

    #@2
    .line 931
    const/4 v2, 0x0

    #@3
    .line 943
    :goto_3
    return-object v2

    #@4
    .line 933
    :cond_4
    const/4 v0, 0x0

    #@5
    .line 934
    .local v0, algo:Ljava/lang/String;
    const/4 v2, 0x0

    #@6
    .line 936
    .local v2, hashed:[B
    :try_start_6
    new-instance v6, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v6

    #@f
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternUtils;->getSalt()Ljava/lang/String;

    #@12
    move-result-object v7

    #@13
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v6

    #@17
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v6

    #@1b
    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    #@1e
    move-result-object v4

    #@1f
    .line 937
    .local v4, saltedPassword:[B
    const-string v0, "SHA-1"

    #@21
    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    #@24
    move-result-object v6

    #@25
    invoke-virtual {v6, v4}, Ljava/security/MessageDigest;->digest([B)[B

    #@28
    move-result-object v5

    #@29
    .line 938
    .local v5, sha1:[B
    const-string v0, "MD5"

    #@2b
    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    #@2e
    move-result-object v6

    #@2f
    invoke-virtual {v6, v4}, Ljava/security/MessageDigest;->digest([B)[B

    #@32
    move-result-object v3

    #@33
    .line 939
    .local v3, md5:[B
    new-instance v6, Ljava/lang/StringBuilder;

    #@35
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    invoke-static {v5}, Lcom/android/internal/widget/LockPatternUtils;->toHex([B)Ljava/lang/String;

    #@3b
    move-result-object v7

    #@3c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v6

    #@40
    invoke-static {v3}, Lcom/android/internal/widget/LockPatternUtils;->toHex([B)Ljava/lang/String;

    #@43
    move-result-object v7

    #@44
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v6

    #@48
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v6

    #@4c
    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B
    :try_end_4f
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_6 .. :try_end_4f} :catch_51

    #@4f
    move-result-object v2

    #@50
    goto :goto_3

    #@51
    .line 940
    .end local v3           #md5:[B
    .end local v4           #saltedPassword:[B
    .end local v5           #sha1:[B
    :catch_51
    move-exception v1

    #@52
    .line 941
    .local v1, e:Ljava/security/NoSuchAlgorithmException;
    const-string v6, "LockPatternUtils"

    #@54
    new-instance v7, Ljava/lang/StringBuilder;

    #@56
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@59
    const-string v8, "Failed to encode string because of missing algorithm: "

    #@5b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v7

    #@5f
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v7

    #@63
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@66
    move-result-object v7

    #@67
    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@6a
    goto :goto_3
.end method

.method public readBackupPin(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "password"

    #@0
    .prologue
    .line 1533
    invoke-virtual {p0, p1}, Lcom/android/internal/widget/LockPatternUtils;->readBackupPinFile(Ljava/lang/String;)Z

    #@3
    move-result v0

    #@4
    .line 1534
    .local v0, check:Z
    if-eqz v0, :cond_7

    #@6
    .line 1538
    .end local p1
    :goto_6
    return-object p1

    #@7
    .restart local p1
    :cond_7
    const/4 p1, 0x0

    #@8
    goto :goto_6
.end method

.method public readBackupPinFile(Ljava/lang/String;)Z
    .registers 12
    .parameter "password"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 1480
    const/4 v4, 0x0

    #@2
    .line 1482
    .local v4, raf:Ljava/io/RandomAccessFile;
    :try_start_2
    new-instance v5, Ljava/io/RandomAccessFile;

    #@4
    iget-object v8, p0, Lcom/android/internal/widget/LockPatternUtils;->sLGPasswordFilename:Ljava/lang/String;

    #@6
    const-string v9, "r"

    #@8
    invoke-direct {v5, v8, v9}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_b
    .catchall {:try_start_2 .. :try_end_b} :catchall_77
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_b} :catch_49
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_b} :catch_60

    #@b
    .line 1483
    .end local v4           #raf:Ljava/io/RandomAccessFile;
    .local v5, raf:Ljava/io/RandomAccessFile;
    :try_start_b
    invoke-virtual {v5}, Ljava/io/RandomAccessFile;->length()J

    #@e
    move-result-wide v8

    #@f
    long-to-int v8, v8

    #@10
    new-array v6, v8, [B

    #@12
    .line 1484
    .local v6, stored:[B
    const/4 v8, 0x0

    #@13
    array-length v9, v6

    #@14
    invoke-virtual {v5, v6, v8, v9}, Ljava/io/RandomAccessFile;->read([BII)I

    #@17
    move-result v2

    #@18
    .line 1486
    .local v2, got:I
    if-gtz v2, :cond_31

    #@1a
    .line 1487
    const-string v8, "LockPatternUtils"

    #@1c
    const-string v9, "Backup PIN readBackupPinFile : file length below zero"

    #@1e
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_21
    .catchall {:try_start_b .. :try_end_21} :catchall_87
    .catch Ljava/io/FileNotFoundException; {:try_start_b .. :try_end_21} :catch_8d
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_21} :catch_8a

    #@21
    .line 1499
    if-eqz v5, :cond_26

    #@23
    .line 1500
    :try_start_23
    invoke-virtual {v5}, Ljava/io/RandomAccessFile;->close()V
    :try_end_26
    .catch Ljava/io/IOException; {:try_start_23 .. :try_end_26} :catch_28

    #@26
    :cond_26
    :goto_26
    move-object v4, v5

    #@27
    .line 1503
    .end local v2           #got:I
    .end local v5           #raf:Ljava/io/RandomAccessFile;
    .end local v6           #stored:[B
    .restart local v4       #raf:Ljava/io/RandomAccessFile;
    :cond_27
    :goto_27
    return v7

    #@28
    .line 1501
    .end local v4           #raf:Ljava/io/RandomAccessFile;
    .restart local v2       #got:I
    .restart local v5       #raf:Ljava/io/RandomAccessFile;
    .restart local v6       #stored:[B
    :catch_28
    move-exception v0

    #@29
    .line 1502
    .local v0, e:Ljava/io/IOException;
    const-string v8, "LockPatternUtils"

    #@2b
    const-string v9, "Backup PIN readBackupPinFile : IO exception at finally"

    #@2d
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    goto :goto_26

    #@31
    .line 1490
    .end local v0           #e:Ljava/io/IOException;
    :cond_31
    :try_start_31
    invoke-virtual {p0, p1}, Lcom/android/internal/widget/LockPatternUtils;->passwordToHash(Ljava/lang/String;)[B

    #@34
    move-result-object v8

    #@35
    invoke-static {v6, v8}, Ljava/util/Arrays;->equals([B[B)Z
    :try_end_38
    .catchall {:try_start_31 .. :try_end_38} :catchall_87
    .catch Ljava/io/FileNotFoundException; {:try_start_31 .. :try_end_38} :catch_8d
    .catch Ljava/io/IOException; {:try_start_31 .. :try_end_38} :catch_8a

    #@38
    move-result v7

    #@39
    .line 1499
    if-eqz v5, :cond_3e

    #@3b
    .line 1500
    :try_start_3b
    invoke-virtual {v5}, Ljava/io/RandomAccessFile;->close()V
    :try_end_3e
    .catch Ljava/io/IOException; {:try_start_3b .. :try_end_3e} :catch_40

    #@3e
    :cond_3e
    :goto_3e
    move-object v4, v5

    #@3f
    .line 1503
    .end local v5           #raf:Ljava/io/RandomAccessFile;
    .restart local v4       #raf:Ljava/io/RandomAccessFile;
    goto :goto_27

    #@40
    .line 1501
    .end local v4           #raf:Ljava/io/RandomAccessFile;
    .restart local v5       #raf:Ljava/io/RandomAccessFile;
    :catch_40
    move-exception v0

    #@41
    .line 1502
    .restart local v0       #e:Ljava/io/IOException;
    const-string v8, "LockPatternUtils"

    #@43
    const-string v9, "Backup PIN readBackupPinFile : IO exception at finally"

    #@45
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    goto :goto_3e

    #@49
    .line 1491
    .end local v0           #e:Ljava/io/IOException;
    .end local v2           #got:I
    .end local v5           #raf:Ljava/io/RandomAccessFile;
    .end local v6           #stored:[B
    .restart local v4       #raf:Ljava/io/RandomAccessFile;
    :catch_49
    move-exception v1

    #@4a
    .line 1492
    .local v1, fnfe:Ljava/io/FileNotFoundException;
    :goto_4a
    :try_start_4a
    const-string v8, "LockPatternUtils"

    #@4c
    const-string v9, "Backup PIN readBackupPinFile : file not found exception"

    #@4e
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_51
    .catchall {:try_start_4a .. :try_end_51} :catchall_77

    #@51
    .line 1499
    if-eqz v4, :cond_27

    #@53
    .line 1500
    :try_start_53
    invoke-virtual {v4}, Ljava/io/RandomAccessFile;->close()V
    :try_end_56
    .catch Ljava/io/IOException; {:try_start_53 .. :try_end_56} :catch_57

    #@56
    goto :goto_27

    #@57
    .line 1501
    :catch_57
    move-exception v0

    #@58
    .line 1502
    .restart local v0       #e:Ljava/io/IOException;
    const-string v8, "LockPatternUtils"

    #@5a
    const-string v9, "Backup PIN readBackupPinFile : IO exception at finally"

    #@5c
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5f
    goto :goto_27

    #@60
    .line 1494
    .end local v0           #e:Ljava/io/IOException;
    .end local v1           #fnfe:Ljava/io/FileNotFoundException;
    :catch_60
    move-exception v3

    #@61
    .line 1495
    .local v3, ioe:Ljava/io/IOException;
    :goto_61
    :try_start_61
    const-string v8, "LockPatternUtils"

    #@63
    const-string v9, "Backup PIN readBackupPinFile : IO exception at catch"

    #@65
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_68
    .catchall {:try_start_61 .. :try_end_68} :catchall_77

    #@68
    .line 1499
    if-eqz v4, :cond_27

    #@6a
    .line 1500
    :try_start_6a
    invoke-virtual {v4}, Ljava/io/RandomAccessFile;->close()V
    :try_end_6d
    .catch Ljava/io/IOException; {:try_start_6a .. :try_end_6d} :catch_6e

    #@6d
    goto :goto_27

    #@6e
    .line 1501
    :catch_6e
    move-exception v0

    #@6f
    .line 1502
    .restart local v0       #e:Ljava/io/IOException;
    const-string v8, "LockPatternUtils"

    #@71
    const-string v9, "Backup PIN readBackupPinFile : IO exception at finally"

    #@73
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@76
    goto :goto_27

    #@77
    .line 1498
    .end local v0           #e:Ljava/io/IOException;
    .end local v3           #ioe:Ljava/io/IOException;
    :catchall_77
    move-exception v7

    #@78
    .line 1499
    :goto_78
    if-eqz v4, :cond_7d

    #@7a
    .line 1500
    :try_start_7a
    invoke-virtual {v4}, Ljava/io/RandomAccessFile;->close()V
    :try_end_7d
    .catch Ljava/io/IOException; {:try_start_7a .. :try_end_7d} :catch_7e

    #@7d
    .line 1503
    :cond_7d
    :goto_7d
    throw v7

    #@7e
    .line 1501
    :catch_7e
    move-exception v0

    #@7f
    .line 1502
    .restart local v0       #e:Ljava/io/IOException;
    const-string v8, "LockPatternUtils"

    #@81
    const-string v9, "Backup PIN readBackupPinFile : IO exception at finally"

    #@83
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@86
    goto :goto_7d

    #@87
    .line 1498
    .end local v0           #e:Ljava/io/IOException;
    .end local v4           #raf:Ljava/io/RandomAccessFile;
    .restart local v5       #raf:Ljava/io/RandomAccessFile;
    :catchall_87
    move-exception v7

    #@88
    move-object v4, v5

    #@89
    .end local v5           #raf:Ljava/io/RandomAccessFile;
    .restart local v4       #raf:Ljava/io/RandomAccessFile;
    goto :goto_78

    #@8a
    .line 1494
    .end local v4           #raf:Ljava/io/RandomAccessFile;
    .restart local v5       #raf:Ljava/io/RandomAccessFile;
    :catch_8a
    move-exception v3

    #@8b
    move-object v4, v5

    #@8c
    .end local v5           #raf:Ljava/io/RandomAccessFile;
    .restart local v4       #raf:Ljava/io/RandomAccessFile;
    goto :goto_61

    #@8d
    .line 1491
    .end local v4           #raf:Ljava/io/RandomAccessFile;
    .restart local v5       #raf:Ljava/io/RandomAccessFile;
    :catch_8d
    move-exception v1

    #@8e
    move-object v4, v5

    #@8f
    .end local v5           #raf:Ljava/io/RandomAccessFile;
    .restart local v4       #raf:Ljava/io/RandomAccessFile;
    goto :goto_4a
.end method

.method public removeAppWidget(I)Z
    .registers 8
    .parameter "widgetId"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1274
    invoke-virtual {p0}, Lcom/android/internal/widget/LockPatternUtils;->getAppWidgets()[I

    #@4
    move-result-object v3

    #@5
    .line 1276
    .local v3, widgets:[I
    array-length v5, v3

    #@6
    if-nez v5, :cond_9

    #@8
    .line 1293
    :cond_8
    :goto_8
    return v4

    #@9
    .line 1280
    :cond_9
    array-length v5, v3

    #@a
    add-int/lit8 v5, v5, -0x1

    #@c
    new-array v2, v5, [I

    #@e
    .line 1281
    .local v2, newWidgets:[I
    const/4 v0, 0x0

    #@f
    .local v0, i:I
    const/4 v1, 0x0

    #@10
    .local v1, j:I
    :goto_10
    array-length v5, v3

    #@11
    if-ge v0, v5, :cond_24

    #@13
    .line 1282
    aget v5, v3, v0

    #@15
    if-ne v5, p1, :cond_1a

    #@17
    .line 1281
    :goto_17
    add-int/lit8 v0, v0, 0x1

    #@19
    goto :goto_10

    #@1a
    .line 1284
    :cond_1a
    array-length v5, v2

    #@1b
    if-ge v1, v5, :cond_8

    #@1d
    .line 1288
    aget v5, v3, v0

    #@1f
    aput v5, v2, v1

    #@21
    .line 1289
    add-int/lit8 v1, v1, 0x1

    #@23
    goto :goto_17

    #@24
    .line 1292
    :cond_24
    invoke-direct {p0, v2}, Lcom/android/internal/widget/LockPatternUtils;->writeAppWidgets([I)V

    #@27
    .line 1293
    const/4 v4, 0x1

    #@28
    goto :goto_8
.end method

.method public removeUser(I)V
    .registers 6
    .parameter "userId"

    #@0
    .prologue
    .line 288
    :try_start_0
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternUtils;->getLockSettings()Lcom/android/internal/widget/ILockSettings;

    #@3
    move-result-object v1

    #@4
    invoke-interface {v1, p1}, Lcom/android/internal/widget/ILockSettings;->removeUser(I)V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_8

    #@7
    .line 292
    :goto_7
    return-void

    #@8
    .line 289
    :catch_8
    move-exception v0

    #@9
    .line 290
    .local v0, re:Landroid/os/RemoteException;
    const-string v1, "LockPatternUtils"

    #@b
    new-instance v2, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v3, "Couldn\'t remove lock settings for user "

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    goto :goto_7
.end method

.method public reportFailedPasswordAttempt()V
    .registers 3

    #@0
    .prologue
    .line 263
    invoke-virtual {p0}, Lcom/android/internal/widget/LockPatternUtils;->getDevicePolicyManager()Landroid/app/admin/DevicePolicyManager;

    #@3
    move-result-object v0

    #@4
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentOrCallingUserId()I

    #@7
    move-result v1

    #@8
    invoke-virtual {v0, v1}, Landroid/app/admin/DevicePolicyManager;->reportFailedPasswordAttempt(I)V

    #@b
    .line 264
    return-void
.end method

.method public reportSuccessfulPasswordAttempt()V
    .registers 3

    #@0
    .prologue
    .line 267
    invoke-virtual {p0}, Lcom/android/internal/widget/LockPatternUtils;->getDevicePolicyManager()Landroid/app/admin/DevicePolicyManager;

    #@3
    move-result-object v0

    #@4
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentOrCallingUserId()I

    #@7
    move-result v1

    #@8
    invoke-virtual {v0, v1}, Landroid/app/admin/DevicePolicyManager;->reportSuccessfulPasswordAttempt(I)V

    #@b
    .line 268
    return-void
.end method

.method public resumeCall()Z
    .registers 3

    #@0
    .prologue
    .line 1450
    const-string v1, "phone"

    #@2
    invoke-static {v1}, Landroid/os/ServiceManager;->checkService(Ljava/lang/String;)Landroid/os/IBinder;

    #@5
    move-result-object v1

    #@6
    invoke-static {v1}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    #@9
    move-result-object v0

    #@a
    .line 1452
    .local v0, phone:Lcom/android/internal/telephony/ITelephony;
    if-eqz v0, :cond_15

    #@c
    :try_start_c
    invoke-interface {v0}, Lcom/android/internal/telephony/ITelephony;->showCallScreen()Z
    :try_end_f
    .catch Landroid/os/RemoteException; {:try_start_c .. :try_end_f} :catch_14

    #@f
    move-result v1

    #@10
    if-eqz v1, :cond_15

    #@12
    .line 1453
    const/4 v1, 0x1

    #@13
    .line 1458
    :goto_13
    return v1

    #@14
    .line 1455
    :catch_14
    move-exception v1

    #@15
    .line 1458
    :cond_15
    const/4 v1, 0x0

    #@16
    goto :goto_13
.end method

.method public saveLockPassword(Ljava/lang/String;I)V
    .registers 5
    .parameter "password"
    .parameter "quality"

    #@0
    .prologue
    .line 692
    const/4 v0, 0x0

    #@1
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentOrCallingUserId()I

    #@4
    move-result v1

    #@5
    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/android/internal/widget/LockPatternUtils;->saveLockPassword(Ljava/lang/String;IZI)V

    #@8
    .line 693
    return-void
.end method

.method public saveLockPassword(Ljava/lang/String;IZ)V
    .registers 5
    .parameter "password"
    .parameter "quality"
    .parameter "isFallback"

    #@0
    .prologue
    .line 704
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentOrCallingUserId()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/android/internal/widget/LockPatternUtils;->saveLockPassword(Ljava/lang/String;IZI)V

    #@7
    .line 705
    return-void
.end method

.method public saveLockPassword(Ljava/lang/String;IZI)V
    .registers 35
    .parameter "password"
    .parameter "quality"
    .parameter "isFallback"
    .parameter "userHandle"

    #@0
    .prologue
    .line 718
    invoke-virtual/range {p0 .. p1}, Lcom/android/internal/widget/LockPatternUtils;->passwordToHash(Ljava/lang/String;)[B

    #@3
    move-result-object v24

    #@4
    .line 720
    .local v24, hash:[B
    :try_start_4
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/widget/LockPatternUtils;->getLockSettings()Lcom/android/internal/widget/ILockSettings;

    #@7
    move-result-object v4

    #@8
    move-object/from16 v0, v24

    #@a
    move/from16 v1, p4

    #@c
    invoke-interface {v4, v0, v1}, Lcom/android/internal/widget/ILockSettings;->setLockPassword([BI)V

    #@f
    .line 721
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/widget/LockPatternUtils;->getDevicePolicyManager()Landroid/app/admin/DevicePolicyManager;

    #@12
    move-result-object v3

    #@13
    .line 722
    .local v3, dpm:Landroid/app/admin/DevicePolicyManager;
    invoke-static {}, Landroid/security/KeyStore;->getInstance()Landroid/security/KeyStore;

    #@16
    move-result-object v26

    #@17
    .line 723
    .local v26, keyStore:Landroid/security/KeyStore;
    if-eqz p1, :cond_172

    #@19
    .line 724
    if-nez p4, :cond_25

    #@1b
    .line 726
    invoke-direct/range {p0 .. p1}, Lcom/android/internal/widget/LockPatternUtils;->updateEncryptionPassword(Ljava/lang/String;)V

    #@1e
    .line 729
    move-object/from16 v0, v26

    #@20
    move-object/from16 v1, p1

    #@22
    invoke-virtual {v0, v1}, Landroid/security/KeyStore;->password(Ljava/lang/String;)Z

    #@25
    .line 732
    :cond_25
    invoke-static/range {p1 .. p1}, Lcom/android/internal/widget/LockPatternUtils;->computePasswordQuality(Ljava/lang/String;)I

    #@28
    move-result v23

    #@29
    .line 733
    .local v23, computedQuality:I
    if-nez p3, :cond_fa

    #@2b
    .line 734
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/widget/LockPatternUtils;->deleteGallery()V

    #@2e
    .line 735
    const-string v4, "lockscreen.password_type"

    #@30
    move/from16 v0, p2

    #@32
    move/from16 v1, v23

    #@34
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@37
    move-result v5

    #@38
    int-to-long v12, v5

    #@39
    move-object/from16 v0, p0

    #@3b
    move/from16 v1, p4

    #@3d
    invoke-direct {v0, v4, v12, v13, v1}, Lcom/android/internal/widget/LockPatternUtils;->setLong(Ljava/lang/String;JI)V

    #@40
    .line 736
    if-eqz v23, :cond_ca

    #@42
    .line 737
    const/4 v6, 0x0

    #@43
    .line 738
    .local v6, letters:I
    const/4 v7, 0x0

    #@44
    .line 739
    .local v7, uppercase:I
    const/4 v8, 0x0

    #@45
    .line 740
    .local v8, lowercase:I
    const/4 v9, 0x0

    #@46
    .line 741
    .local v9, numbers:I
    const/4 v10, 0x0

    #@47
    .line 742
    .local v10, symbols:I
    const/4 v11, 0x0

    #@48
    .line 743
    .local v11, nonletter:I
    const/16 v25, 0x0

    #@4a
    .local v25, i:I
    :goto_4a
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    #@4d
    move-result v4

    #@4e
    move/from16 v0, v25

    #@50
    if-ge v0, v4, :cond_94

    #@52
    .line 744
    move-object/from16 v0, p1

    #@54
    move/from16 v1, v25

    #@56
    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    #@59
    move-result v22

    #@5a
    .line 745
    .local v22, c:C
    const/16 v4, 0x41

    #@5c
    move/from16 v0, v22

    #@5e
    if-lt v0, v4, :cond_6d

    #@60
    const/16 v4, 0x5a

    #@62
    move/from16 v0, v22

    #@64
    if-gt v0, v4, :cond_6d

    #@66
    .line 746
    add-int/lit8 v6, v6, 0x1

    #@68
    .line 747
    add-int/lit8 v7, v7, 0x1

    #@6a
    .line 743
    :goto_6a
    add-int/lit8 v25, v25, 0x1

    #@6c
    goto :goto_4a

    #@6d
    .line 748
    :cond_6d
    const/16 v4, 0x61

    #@6f
    move/from16 v0, v22

    #@71
    if-lt v0, v4, :cond_7e

    #@73
    const/16 v4, 0x7a

    #@75
    move/from16 v0, v22

    #@77
    if-gt v0, v4, :cond_7e

    #@79
    .line 749
    add-int/lit8 v6, v6, 0x1

    #@7b
    .line 750
    add-int/lit8 v8, v8, 0x1

    #@7d
    goto :goto_6a

    #@7e
    .line 751
    :cond_7e
    const/16 v4, 0x30

    #@80
    move/from16 v0, v22

    #@82
    if-lt v0, v4, :cond_8f

    #@84
    const/16 v4, 0x39

    #@86
    move/from16 v0, v22

    #@88
    if-gt v0, v4, :cond_8f

    #@8a
    .line 752
    add-int/lit8 v9, v9, 0x1

    #@8c
    .line 753
    add-int/lit8 v11, v11, 0x1

    #@8e
    goto :goto_6a

    #@8f
    .line 755
    :cond_8f
    add-int/lit8 v10, v10, 0x1

    #@91
    .line 756
    add-int/lit8 v11, v11, 0x1

    #@93
    goto :goto_6a

    #@94
    .line 759
    .end local v22           #c:C
    :cond_94
    move/from16 v0, p2

    #@96
    move/from16 v1, v23

    #@98
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@9b
    move-result v4

    #@9c
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    #@9f
    move-result v5

    #@a0
    move/from16 v12, p4

    #@a2
    invoke-virtual/range {v3 .. v12}, Landroid/app/admin/DevicePolicyManager;->setActivePasswordState(IIIIIIIII)V

    #@a5
    .line 785
    .end local v6           #letters:I
    .end local v7           #uppercase:I
    .end local v8           #lowercase:I
    .end local v9           #numbers:I
    .end local v10           #symbols:I
    .end local v11           #nonletter:I
    .end local v25           #i:I
    :goto_a5
    const-string v4, "lockscreen.passwordhistory"

    #@a7
    move-object/from16 v0, p0

    #@a9
    move/from16 v1, p4

    #@ab
    invoke-direct {v0, v4, v1}, Lcom/android/internal/widget/LockPatternUtils;->getString(Ljava/lang/String;I)Ljava/lang/String;

    #@ae
    move-result-object v27

    #@af
    .line 786
    .local v27, passwordHistory:Ljava/lang/String;
    if-nez v27, :cond_b6

    #@b1
    .line 787
    new-instance v27, Ljava/lang/String;

    #@b3
    .end local v27           #passwordHistory:Ljava/lang/String;
    invoke-direct/range {v27 .. v27}, Ljava/lang/String;-><init>()V

    #@b6
    .line 789
    .restart local v27       #passwordHistory:Ljava/lang/String;
    :cond_b6
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/widget/LockPatternUtils;->getRequestedPasswordHistoryLength()I

    #@b9
    move-result v28

    #@ba
    .line 790
    .local v28, passwordHistoryLength:I
    if-nez v28, :cond_138

    #@bc
    .line 791
    const-string v27, ""

    #@be
    .line 800
    :goto_be
    const-string v4, "lockscreen.passwordhistory"

    #@c0
    move-object/from16 v0, p0

    #@c2
    move-object/from16 v1, v27

    #@c4
    move/from16 v2, p4

    #@c6
    invoke-direct {v0, v4, v1, v2}, Lcom/android/internal/widget/LockPatternUtils;->setString(Ljava/lang/String;Ljava/lang/String;I)V

    #@c9
    .line 815
    .end local v3           #dpm:Landroid/app/admin/DevicePolicyManager;
    .end local v23           #computedQuality:I
    .end local v26           #keyStore:Landroid/security/KeyStore;
    .end local v27           #passwordHistory:Ljava/lang/String;
    .end local v28           #passwordHistoryLength:I
    :goto_c9
    return-void

    #@ca
    .line 764
    .restart local v3       #dpm:Landroid/app/admin/DevicePolicyManager;
    .restart local v23       #computedQuality:I
    .restart local v26       #keyStore:Landroid/security/KeyStore;
    :cond_ca
    const/4 v13, 0x0

    #@cb
    const/4 v14, 0x0

    #@cc
    const/4 v15, 0x0

    #@cd
    const/16 v16, 0x0

    #@cf
    const/16 v17, 0x0

    #@d1
    const/16 v18, 0x0

    #@d3
    const/16 v19, 0x0

    #@d5
    const/16 v20, 0x0

    #@d7
    move-object v12, v3

    #@d8
    move/from16 v21, p4

    #@da
    invoke-virtual/range {v12 .. v21}, Landroid/app/admin/DevicePolicyManager;->setActivePasswordState(IIIIIIIII)V
    :try_end_dd
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_dd} :catch_de

    #@dd
    goto :goto_a5

    #@de
    .line 811
    .end local v3           #dpm:Landroid/app/admin/DevicePolicyManager;
    .end local v23           #computedQuality:I
    .end local v26           #keyStore:Landroid/security/KeyStore;
    :catch_de
    move-exception v29

    #@df
    .line 813
    .local v29, re:Landroid/os/RemoteException;
    const-string v4, "LockPatternUtils"

    #@e1
    new-instance v5, Ljava/lang/StringBuilder;

    #@e3
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@e6
    const-string v12, "Unable to save lock password "

    #@e8
    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@eb
    move-result-object v5

    #@ec
    move-object/from16 v0, v29

    #@ee
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@f1
    move-result-object v5

    #@f2
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f5
    move-result-object v5

    #@f6
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@f9
    goto :goto_c9

    #@fa
    .line 770
    .end local v29           #re:Landroid/os/RemoteException;
    .restart local v3       #dpm:Landroid/app/admin/DevicePolicyManager;
    .restart local v23       #computedQuality:I
    .restart local v26       #keyStore:Landroid/security/KeyStore;
    :cond_fa
    :try_start_fa
    const-string v4, "lockscreen.password_type"

    #@fc
    const-wide/32 v12, 0x8000

    #@ff
    move-object/from16 v0, p0

    #@101
    move/from16 v1, p4

    #@103
    invoke-direct {v0, v4, v12, v13, v1}, Lcom/android/internal/widget/LockPatternUtils;->setLong(Ljava/lang/String;JI)V

    #@106
    .line 772
    const-string v4, "lockscreen.password_type_alternate"

    #@108
    move/from16 v0, p2

    #@10a
    move/from16 v1, v23

    #@10c
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@10f
    move-result v5

    #@110
    int-to-long v12, v5

    #@111
    move-object/from16 v0, p0

    #@113
    move/from16 v1, p4

    #@115
    invoke-direct {v0, v4, v12, v13, v1}, Lcom/android/internal/widget/LockPatternUtils;->setLong(Ljava/lang/String;JI)V

    #@118
    .line 775
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/widget/LockPatternUtils;->isOMADM()Z

    #@11b
    move-result v4

    #@11c
    if-nez v4, :cond_121

    #@11e
    .line 776
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/widget/LockPatternUtils;->finishBiometricWeak()V

    #@121
    .line 779
    :cond_121
    const v13, 0x8000

    #@124
    const/4 v14, 0x0

    #@125
    const/4 v15, 0x0

    #@126
    const/16 v16, 0x0

    #@128
    const/16 v17, 0x0

    #@12a
    const/16 v18, 0x0

    #@12c
    const/16 v19, 0x0

    #@12e
    const/16 v20, 0x0

    #@130
    move-object v12, v3

    #@131
    move/from16 v21, p4

    #@133
    invoke-virtual/range {v12 .. v21}, Landroid/app/admin/DevicePolicyManager;->setActivePasswordState(IIIIIIIII)V

    #@136
    goto/16 :goto_a5

    #@138
    .line 793
    .restart local v27       #passwordHistory:Ljava/lang/String;
    .restart local v28       #passwordHistoryLength:I
    :cond_138
    new-instance v4, Ljava/lang/StringBuilder;

    #@13a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@13d
    new-instance v5, Ljava/lang/String;

    #@13f
    move-object/from16 v0, v24

    #@141
    invoke-direct {v5, v0}, Ljava/lang/String;-><init>([B)V

    #@144
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@147
    move-result-object v4

    #@148
    const-string v5, ","

    #@14a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14d
    move-result-object v4

    #@14e
    move-object/from16 v0, v27

    #@150
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@153
    move-result-object v4

    #@154
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@157
    move-result-object v27

    #@158
    .line 796
    const/4 v4, 0x0

    #@159
    move-object/from16 v0, v24

    #@15b
    array-length v5, v0

    #@15c
    mul-int v5, v5, v28

    #@15e
    add-int v5, v5, v28

    #@160
    add-int/lit8 v5, v5, -0x1

    #@162
    invoke-virtual/range {v27 .. v27}, Ljava/lang/String;->length()I

    #@165
    move-result v12

    #@166
    invoke-static {v5, v12}, Ljava/lang/Math;->min(II)I

    #@169
    move-result v5

    #@16a
    move-object/from16 v0, v27

    #@16c
    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@16f
    move-result-object v27

    #@170
    goto/16 :goto_be

    #@172
    .line 804
    .end local v23           #computedQuality:I
    .end local v27           #passwordHistory:Ljava/lang/String;
    .end local v28           #passwordHistoryLength:I
    :cond_172
    invoke-virtual/range {v26 .. v26}, Landroid/security/KeyStore;->isEmpty()Z

    #@175
    move-result v4

    #@176
    if-eqz v4, :cond_17b

    #@178
    .line 805
    invoke-virtual/range {v26 .. v26}, Landroid/security/KeyStore;->reset()Z

    #@17b
    .line 807
    :cond_17b
    const/4 v13, 0x0

    #@17c
    const/4 v14, 0x0

    #@17d
    const/4 v15, 0x0

    #@17e
    const/16 v16, 0x0

    #@180
    const/16 v17, 0x0

    #@182
    const/16 v18, 0x0

    #@184
    const/16 v19, 0x0

    #@186
    const/16 v20, 0x0

    #@188
    move-object v12, v3

    #@189
    move/from16 v21, p4

    #@18b
    invoke-virtual/range {v12 .. v21}, Landroid/app/admin/DevicePolicyManager;->setActivePasswordState(IIIIIIIII)V
    :try_end_18e
    .catch Landroid/os/RemoteException; {:try_start_fa .. :try_end_18e} :catch_de

    #@18e
    goto/16 :goto_c9
.end method

.method public saveLockPattern(Ljava/util/List;)V
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/widget/LockPatternView$Cell;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 574
    .local p1, pattern:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/widget/LockPatternView$Cell;>;"
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0}, Lcom/android/internal/widget/LockPatternUtils;->saveLockPattern(Ljava/util/List;Z)V

    #@4
    .line 575
    return-void
.end method

.method public saveLockPattern(Ljava/util/List;Z)V
    .registers 16
    .parameter
    .parameter "isFallback"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/widget/LockPatternView$Cell;",
            ">;Z)V"
        }
    .end annotation

    #@0
    .prologue
    .line 590
    .local p1, pattern:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/widget/LockPatternView$Cell;>;"
    invoke-static {p1}, Lcom/android/internal/widget/LockPatternUtils;->patternToHash(Ljava/util/List;)[B

    #@3
    move-result-object v10

    #@4
    .line 592
    .local v10, hash:[B
    :try_start_4
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternUtils;->getLockSettings()Lcom/android/internal/widget/ILockSettings;

    #@7
    move-result-object v1

    #@8
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentOrCallingUserId()I

    #@b
    move-result v2

    #@c
    invoke-interface {v1, v10, v2}, Lcom/android/internal/widget/ILockSettings;->setLockPattern([BI)V

    #@f
    .line 593
    invoke-virtual {p0}, Lcom/android/internal/widget/LockPatternUtils;->getDevicePolicyManager()Landroid/app/admin/DevicePolicyManager;

    #@12
    move-result-object v0

    #@13
    .line 594
    .local v0, dpm:Landroid/app/admin/DevicePolicyManager;
    invoke-static {}, Landroid/security/KeyStore;->getInstance()Landroid/security/KeyStore;

    #@16
    move-result-object v11

    #@17
    .line 595
    .local v11, keyStore:Landroid/security/KeyStore;
    if-eqz p1, :cond_86

    #@19
    .line 596
    invoke-static {p1}, Lcom/android/internal/widget/LockPatternUtils;->patternToString(Ljava/util/List;)Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v11, v1}, Landroid/security/KeyStore;->password(Ljava/lang/String;)Z

    #@20
    .line 597
    const-string v1, "lockscreen.patterneverchosen"

    #@22
    const/4 v2, 0x1

    #@23
    invoke-direct {p0, v1, v2}, Lcom/android/internal/widget/LockPatternUtils;->setBoolean(Ljava/lang/String;Z)V

    #@26
    .line 598
    if-nez p2, :cond_47

    #@28
    .line 599
    invoke-virtual {p0}, Lcom/android/internal/widget/LockPatternUtils;->deleteGallery()V

    #@2b
    .line 600
    const-string v1, "lockscreen.password_type"

    #@2d
    const-wide/32 v2, 0x10000

    #@30
    invoke-direct {p0, v1, v2, v3}, Lcom/android/internal/widget/LockPatternUtils;->setLong(Ljava/lang/String;J)V

    #@33
    .line 601
    const/high16 v1, 0x1

    #@35
    invoke-interface {p1}, Ljava/util/List;->size()I

    #@38
    move-result v2

    #@39
    const/4 v3, 0x0

    #@3a
    const/4 v4, 0x0

    #@3b
    const/4 v5, 0x0

    #@3c
    const/4 v6, 0x0

    #@3d
    const/4 v7, 0x0

    #@3e
    const/4 v8, 0x0

    #@3f
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentOrCallingUserId()I

    #@42
    move-result v9

    #@43
    invoke-virtual/range {v0 .. v9}, Landroid/app/admin/DevicePolicyManager;->setActivePasswordState(IIIIIIIII)V

    #@46
    .line 621
    .end local v0           #dpm:Landroid/app/admin/DevicePolicyManager;
    .end local v11           #keyStore:Landroid/security/KeyStore;
    :goto_46
    return-void

    #@47
    .line 604
    .restart local v0       #dpm:Landroid/app/admin/DevicePolicyManager;
    .restart local v11       #keyStore:Landroid/security/KeyStore;
    :cond_47
    const-string v1, "lockscreen.password_type"

    #@49
    const-wide/32 v2, 0x8000

    #@4c
    invoke-direct {p0, v1, v2, v3}, Lcom/android/internal/widget/LockPatternUtils;->setLong(Ljava/lang/String;J)V

    #@4f
    .line 605
    const-string v1, "lockscreen.password_type_alternate"

    #@51
    const-wide/32 v2, 0x10000

    #@54
    invoke-direct {p0, v1, v2, v3}, Lcom/android/internal/widget/LockPatternUtils;->setLong(Ljava/lang/String;J)V

    #@57
    .line 607
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternUtils;->finishBiometricWeak()V

    #@5a
    .line 608
    const v1, 0x8000

    #@5d
    const/4 v2, 0x0

    #@5e
    const/4 v3, 0x0

    #@5f
    const/4 v4, 0x0

    #@60
    const/4 v5, 0x0

    #@61
    const/4 v6, 0x0

    #@62
    const/4 v7, 0x0

    #@63
    const/4 v8, 0x0

    #@64
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentOrCallingUserId()I

    #@67
    move-result v9

    #@68
    invoke-virtual/range {v0 .. v9}, Landroid/app/admin/DevicePolicyManager;->setActivePasswordState(IIIIIIIII)V
    :try_end_6b
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_6b} :catch_6c

    #@6b
    goto :goto_46

    #@6c
    .line 618
    .end local v0           #dpm:Landroid/app/admin/DevicePolicyManager;
    .end local v11           #keyStore:Landroid/security/KeyStore;
    :catch_6c
    move-exception v12

    #@6d
    .line 619
    .local v12, re:Landroid/os/RemoteException;
    const-string v1, "LockPatternUtils"

    #@6f
    new-instance v2, Ljava/lang/StringBuilder;

    #@71
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@74
    const-string v3, "Couldn\'t save lock pattern "

    #@76
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v2

    #@7a
    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v2

    #@7e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@81
    move-result-object v2

    #@82
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@85
    goto :goto_46

    #@86
    .line 612
    .end local v12           #re:Landroid/os/RemoteException;
    .restart local v0       #dpm:Landroid/app/admin/DevicePolicyManager;
    .restart local v11       #keyStore:Landroid/security/KeyStore;
    :cond_86
    :try_start_86
    invoke-virtual {v11}, Landroid/security/KeyStore;->isEmpty()Z

    #@89
    move-result v1

    #@8a
    if-eqz v1, :cond_8f

    #@8c
    .line 613
    invoke-virtual {v11}, Landroid/security/KeyStore;->reset()Z

    #@8f
    .line 615
    :cond_8f
    const/4 v1, 0x0

    #@90
    const/4 v2, 0x0

    #@91
    const/4 v3, 0x0

    #@92
    const/4 v4, 0x0

    #@93
    const/4 v5, 0x0

    #@94
    const/4 v6, 0x0

    #@95
    const/4 v7, 0x0

    #@96
    const/4 v8, 0x0

    #@97
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentOrCallingUserId()I

    #@9a
    move-result v9

    #@9b
    invoke-virtual/range {v0 .. v9}, Landroid/app/admin/DevicePolicyManager;->setActivePasswordState(IIIIIIIII)V
    :try_end_9e
    .catch Landroid/os/RemoteException; {:try_start_86 .. :try_end_9e} :catch_6c

    #@9e
    goto :goto_46
.end method

.method public saveLockPatternKidsMode(Ljava/util/List;)V
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/widget/LockPatternView$Cell;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 579
    .local p1, pattern:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/widget/LockPatternView$Cell;>;"
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0}, Lcom/android/internal/widget/LockPatternUtils;->saveLockPatternKidsMode(Ljava/util/List;Z)V

    #@4
    .line 580
    return-void
.end method

.method public saveLockPatternKidsMode(Ljava/util/List;Z)V
    .registers 8
    .parameter
    .parameter "isFallback"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/widget/LockPatternView$Cell;",
            ">;Z)V"
        }
    .end annotation

    #@0
    .prologue
    .line 626
    .local p1, pattern:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/widget/LockPatternView$Cell;>;"
    invoke-static {p1}, Lcom/android/internal/widget/LockPatternUtils;->patternToHash(Ljava/util/List;)[B

    #@3
    move-result-object v0

    #@4
    .line 628
    .local v0, hash:[B
    :try_start_4
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternUtils;->getLockSettings()Lcom/android/internal/widget/ILockSettings;

    #@7
    move-result-object v2

    #@8
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentOrCallingUserId()I

    #@b
    move-result v3

    #@c
    invoke-interface {v2, v0, v3}, Lcom/android/internal/widget/ILockSettings;->setLockPatternKidsMode([BI)V
    :try_end_f
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_f} :catch_10

    #@f
    .line 632
    :goto_f
    return-void

    #@10
    .line 629
    :catch_10
    move-exception v1

    #@11
    .line 630
    .local v1, re:Landroid/os/RemoteException;
    const-string v2, "LockPatternUtils"

    #@13
    new-instance v3, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v4, "Couldn\'t save lock pattern kids mode"

    #@1a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v3

    #@22
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v3

    #@26
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    goto :goto_f
.end method

.method public savedPasswordExists()Z
    .registers 4

    #@0
    .prologue
    .line 411
    :try_start_0
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternUtils;->getLockSettings()Lcom/android/internal/widget/ILockSettings;

    #@3
    move-result-object v1

    #@4
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentOrCallingUserId()I

    #@7
    move-result v2

    #@8
    invoke-interface {v1, v2}, Lcom/android/internal/widget/ILockSettings;->havePassword(I)Z
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_b} :catch_d

    #@b
    move-result v1

    #@c
    .line 413
    :goto_c
    return v1

    #@d
    .line 412
    :catch_d
    move-exception v0

    #@e
    .line 413
    .local v0, re:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@f
    goto :goto_c
.end method

.method public savedPatternExists()Z
    .registers 4

    #@0
    .prologue
    .line 389
    :try_start_0
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternUtils;->getLockSettings()Lcom/android/internal/widget/ILockSettings;

    #@3
    move-result-object v1

    #@4
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentOrCallingUserId()I

    #@7
    move-result v2

    #@8
    invoke-interface {v1, v2}, Lcom/android/internal/widget/ILockSettings;->havePattern(I)Z
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_b} :catch_d

    #@b
    move-result v1

    #@c
    .line 391
    :goto_c
    return v1

    #@d
    .line 390
    :catch_d
    move-exception v0

    #@e
    .line 391
    .local v0, re:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@f
    goto :goto_c
.end method

.method public savedPatternKidsModeExists()Z
    .registers 4

    #@0
    .prologue
    .line 398
    :try_start_0
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternUtils;->getLockSettings()Lcom/android/internal/widget/ILockSettings;

    #@3
    move-result-object v1

    #@4
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternUtils;->getCurrentOrCallingUserId()I

    #@7
    move-result v2

    #@8
    invoke-interface {v1, v2}, Lcom/android/internal/widget/ILockSettings;->havePatternKidsMode(I)Z
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_b} :catch_d

    #@b
    move-result v1

    #@c
    .line 400
    :goto_c
    return v1

    #@d
    .line 399
    :catch_d
    move-exception v0

    #@e
    .line 400
    .local v0, re:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@f
    goto :goto_c
.end method

.method public setBiometricWeakLivelinessEnabled(Z)V
    .registers 9
    .parameter "enabled"

    #@0
    .prologue
    .line 1017
    const-string v4, "lock_biometric_weak_flags"

    #@2
    const-wide/16 v5, 0x0

    #@4
    invoke-direct {p0, v4, v5, v6}, Lcom/android/internal/widget/LockPatternUtils;->getLong(Ljava/lang/String;J)J

    #@7
    move-result-wide v0

    #@8
    .line 1019
    .local v0, currentFlag:J
    if-eqz p1, :cond_14

    #@a
    .line 1020
    const-wide/16 v4, 0x1

    #@c
    or-long v2, v0, v4

    #@e
    .line 1024
    .local v2, newFlag:J
    :goto_e
    const-string v4, "lock_biometric_weak_flags"

    #@10
    invoke-direct {p0, v4, v2, v3}, Lcom/android/internal/widget/LockPatternUtils;->setLong(Ljava/lang/String;J)V

    #@13
    .line 1025
    return-void

    #@14
    .line 1022
    .end local v2           #newFlag:J
    :cond_14
    const-wide/16 v4, -0x2

    #@16
    and-long v2, v0, v4

    #@18
    .restart local v2       #newFlag:J
    goto :goto_e
.end method

.method public setCurrentUser(I)V
    .registers 2
    .parameter "userId"

    #@0
    .prologue
    .line 271
    sput p1, Lcom/android/internal/widget/LockPatternUtils;->sCurrentUserId:I

    #@2
    .line 272
    return-void
.end method

.method public setHiddenPatternDisabled(Z)V
    .registers 2
    .parameter "disable"

    #@0
    .prologue
    .line 521
    iput-boolean p1, p0, Lcom/android/internal/widget/LockPatternUtils;->mHiddenPatternDisabled:Z

    #@2
    .line 522
    return-void
.end method

.method public setLockPatternEnabled(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 1039
    const-string v0, "lock_pattern_autolock"

    #@2
    invoke-direct {p0, v0, p1}, Lcom/android/internal/widget/LockPatternUtils;->setBoolean(Ljava/lang/String;Z)V

    #@5
    .line 1040
    return-void
.end method

.method public setLockScreenDisabled(Z)V
    .registers 5
    .parameter "disable"

    #@0
    .prologue
    .line 536
    const-string v2, "lockscreen.disabled"

    #@2
    if-eqz p1, :cond_a

    #@4
    const-wide/16 v0, 0x1

    #@6
    :goto_6
    invoke-direct {p0, v2, v0, v1}, Lcom/android/internal/widget/LockPatternUtils;->setLong(Ljava/lang/String;J)V

    #@9
    .line 537
    return-void

    #@a
    .line 536
    :cond_a
    const-wide/16 v0, 0x0

    #@c
    goto :goto_6
.end method

.method public setLockoutAttemptDeadline()J
    .registers 7

    #@0
    .prologue
    .line 1070
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@3
    move-result-wide v2

    #@4
    const-wide/16 v4, 0x7530

    #@6
    add-long v0, v2, v4

    #@8
    .line 1071
    .local v0, deadline:J
    const-string v2, "lockscreen.lockoutattemptdeadline"

    #@a
    invoke-direct {p0, v2, v0, v1}, Lcom/android/internal/widget/LockPatternUtils;->setLong(Ljava/lang/String;J)V

    #@d
    .line 1072
    return-wide v0
.end method

.method public setOMADMPolicy()V
    .registers 4

    #@0
    .prologue
    .line 1572
    const-string v0, "LockPatternUtils"

    #@2
    const-string v1, "OMADM setOMADMPolicy"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1573
    iget-object v0, p0, Lcom/android/internal/widget/LockPatternUtils;->mContext:Landroid/content/Context;

    #@9
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@c
    move-result-object v0

    #@d
    const-string v1, "lg_omadm_lwmo_lock_code"

    #@f
    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    const/high16 v1, 0x2

    #@15
    const/4 v2, 0x1

    #@16
    invoke-virtual {p0, v0, v1, v2}, Lcom/android/internal/widget/LockPatternUtils;->saveLockPassword(Ljava/lang/String;IZ)V

    #@19
    .line 1575
    return-void
.end method

.method public setPermanentlyLocked(Z)V
    .registers 3
    .parameter "locked"

    #@0
    .prologue
    .line 1116
    const-string v0, "lockscreen.lockedoutpermanently"

    #@2
    invoke-direct {p0, v0, p1}, Lcom/android/internal/widget/LockPatternUtils;->setBoolean(Ljava/lang/String;Z)V

    #@5
    .line 1117
    return-void
.end method

.method public setPolicy()V
    .registers 4

    #@0
    .prologue
    .line 1593
    const-string v0, "LockPatternUtils"

    #@2
    const-string v1, "OMADM setPolicy"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1594
    const-string v0, "lockscreen.password_type"

    #@9
    const-wide/16 v1, 0x0

    #@b
    invoke-direct {p0, v0, v1, v2}, Lcom/android/internal/widget/LockPatternUtils;->setLong(Ljava/lang/String;J)V

    #@e
    .line 1595
    return-void
.end method

.method public setPowerButtonInstantlyLocks(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 1545
    const-string v0, "lockscreen.power_button_instantly_locks"

    #@2
    invoke-direct {p0, v0, p1}, Lcom/android/internal/widget/LockPatternUtils;->setBoolean(Ljava/lang/String;Z)V

    #@5
    .line 1546
    return-void
.end method

.method public setVisiblePatternEnabled(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 1053
    const-string v0, "lock_pattern_visible_pattern"

    #@2
    invoke-direct {p0, v0, p1}, Lcom/android/internal/widget/LockPatternUtils;->setBoolean(Ljava/lang/String;Z)V

    #@5
    .line 1054
    return-void
.end method

.method public updateEmergencyCallButtonState(Landroid/widget/Button;IZ)V
    .registers 10
    .parameter "button"
    .parameter "phoneState"
    .parameter "shown"

    #@0
    .prologue
    .line 1425
    const/4 v4, 0x0

    #@1
    const/4 v5, 0x1

    #@2
    move-object v0, p0

    #@3
    move-object v1, p1

    #@4
    move v2, p2

    #@5
    move v3, p3

    #@6
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/widget/LockPatternUtils;->updateEmergencyCallButtonState(Landroid/widget/Button;IZZZ)V

    #@9
    .line 1426
    return-void
.end method

.method public updateEmergencyCallButtonState(Landroid/widget/Button;IZZZ)V
    .registers 14
    .parameter "button"
    .parameter "phoneState"
    .parameter "shown"
    .parameter "upperCase"
    .parameter "showIcon"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 1387
    invoke-virtual {p0}, Lcom/android/internal/widget/LockPatternUtils;->isEmergencyCallCapable()Z

    #@4
    move-result v6

    #@5
    if-eqz v6, :cond_34

    #@7
    if-eqz p3, :cond_34

    #@9
    .line 1388
    invoke-virtual {p1, v5}, Landroid/widget/Button;->setVisibility(I)V

    #@c
    .line 1395
    const/4 v6, 0x2

    #@d
    if-ne p2, v6, :cond_3c

    #@f
    .line 1397
    const v3, 0x1040311

    #@12
    .line 1398
    .local v3, textId:I
    if-eqz p5, :cond_3a

    #@14
    const v2, 0x1080084

    #@17
    .line 1399
    .local v2, phoneCallIcon:I
    :goto_17
    invoke-virtual {p1, v2, v5, v5, v5}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    #@1a
    .line 1409
    .end local v2           #phoneCallIcon:I
    :goto_1a
    if-eqz p4, :cond_5e

    #@1c
    .line 1410
    iget-object v5, p0, Lcom/android/internal/widget/LockPatternUtils;->mContext:Landroid/content/Context;

    #@1e
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@21
    move-result-object v5

    #@22
    invoke-virtual {v5, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@25
    move-result-object v1

    #@26
    .line 1411
    .local v1, original:Ljava/lang/CharSequence;
    if-eqz v1, :cond_5c

    #@28
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@2b
    move-result-object v5

    #@2c
    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    #@2f
    move-result-object v4

    #@30
    .line 1412
    .local v4, upper:Ljava/lang/String;
    :goto_30
    invoke-virtual {p1, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    #@33
    .line 1416
    .end local v1           #original:Ljava/lang/CharSequence;
    .end local v3           #textId:I
    .end local v4           #upper:Ljava/lang/String;
    :goto_33
    return-void

    #@34
    .line 1390
    :cond_34
    const/16 v5, 0x8

    #@36
    invoke-virtual {p1, v5}, Landroid/widget/Button;->setVisibility(I)V

    #@39
    goto :goto_33

    #@3a
    .restart local v3       #textId:I
    :cond_3a
    move v2, v5

    #@3b
    .line 1398
    goto :goto_17

    #@3c
    .line 1401
    .end local v3           #textId:I
    :cond_3c
    sget-boolean v6, Lcom/lge/config/ConfigBuildFlags;->CAPP_NEXTI_LOCKSCREEN:Z

    #@3e
    if-nez v6, :cond_4a

    #@40
    const-string v6, "TEL"

    #@42
    sget-object v7, Lcom/android/internal/widget/LockPatternUtils;->carrier:Ljava/lang/String;

    #@44
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@47
    move-result v6

    #@48
    if-eqz v6, :cond_56

    #@4a
    .line 1402
    :cond_4a
    const v3, 0x1040310

    #@4d
    .line 1406
    .restart local v3       #textId:I
    :goto_4d
    if-eqz p5, :cond_5a

    #@4f
    const v0, 0x10802cf

    #@52
    .line 1407
    .local v0, emergencyIcon:I
    :goto_52
    invoke-virtual {p1, v0, v5, v5, v5}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    #@55
    goto :goto_1a

    #@56
    .line 1404
    .end local v0           #emergencyIcon:I
    .end local v3           #textId:I
    :cond_56
    const v3, 0x209013d

    #@59
    .restart local v3       #textId:I
    goto :goto_4d

    #@5a
    :cond_5a
    move v0, v5

    #@5b
    .line 1406
    goto :goto_52

    #@5c
    .line 1411
    .restart local v1       #original:Ljava/lang/CharSequence;
    :cond_5c
    const/4 v4, 0x0

    #@5d
    goto :goto_30

    #@5e
    .line 1414
    .end local v1           #original:Ljava/lang/CharSequence;
    :cond_5e
    invoke-virtual {p1, v3}, Landroid/widget/Button;->setText(I)V

    #@61
    goto :goto_33
.end method

.method public updateEmergencyCallButtonState(Landroid/widget/Button;ZII)V
    .registers 7
    .parameter "button"
    .parameter "shown"
    .parameter "textId"
    .parameter "iconId"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1431
    invoke-virtual {p0}, Lcom/android/internal/widget/LockPatternUtils;->isEmergencyCallCapable()Z

    #@4
    move-result v0

    #@5
    if-eqz v0, :cond_13

    #@7
    if-eqz p2, :cond_13

    #@9
    .line 1432
    invoke-virtual {p1, v1}, Landroid/widget/Button;->setVisibility(I)V

    #@c
    .line 1438
    invoke-virtual {p1, p4, v1, v1, v1}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    #@f
    .line 1439
    invoke-virtual {p1, p3}, Landroid/widget/Button;->setText(I)V

    #@12
    .line 1440
    :goto_12
    return-void

    #@13
    .line 1434
    :cond_13
    const/16 v0, 0x8

    #@15
    invoke-virtual {p1, v0}, Landroid/widget/Button;->setVisibility(I)V

    #@18
    goto :goto_12
.end method

.method public usingBiometricWeak()Z
    .registers 5

    #@0
    .prologue
    .line 840
    const-string v1, "lockscreen.password_type"

    #@2
    const-wide/32 v2, 0x10000

    #@5
    invoke-direct {p0, v1, v2, v3}, Lcom/android/internal/widget/LockPatternUtils;->getLong(Ljava/lang/String;J)J

    #@8
    move-result-wide v1

    #@9
    long-to-int v0, v1

    #@a
    .line 842
    .local v0, quality:I
    const v1, 0x8000

    #@d
    if-ne v0, v1, :cond_11

    #@f
    const/4 v1, 0x1

    #@10
    :goto_10
    return v1

    #@11
    :cond_11
    const/4 v1, 0x0

    #@12
    goto :goto_10
.end method

.method public writeFallbackAppWidgetId(I)V
    .registers 5
    .parameter "appWidgetId"

    #@0
    .prologue
    .line 1226
    iget-object v0, p0, Lcom/android/internal/widget/LockPatternUtils;->mContentResolver:Landroid/content/ContentResolver;

    #@2
    const-string v1, "lock_screen_fallback_appwidget_id"

    #@4
    const/4 v2, -0x2

    #@5
    invoke-static {v0, v1, p1, v2}, Landroid/provider/Settings$Secure;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    #@8
    .line 1230
    return-void
.end method
