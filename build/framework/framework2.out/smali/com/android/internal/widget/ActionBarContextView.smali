.class public Lcom/android/internal/widget/ActionBarContextView;
.super Lcom/android/internal/widget/AbsActionBarView;
.source "ActionBarContextView.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# static fields
.field private static final ANIMATE_IDLE:I = 0x0

.field private static final ANIMATE_IN:I = 0x1

.field private static final ANIMATE_OUT:I = 0x2

.field private static final TAG:Ljava/lang/String; = "ActionBarContextView"


# instance fields
.field private mAnimateInOnLayout:Z

.field private mAnimationMode:I

.field private mClose:Landroid/view/View;

.field private mCurrentAnimation:Landroid/animation/Animator;

.field private mCustomView:Landroid/view/View;

.field private mSplitBackground:Landroid/graphics/drawable/Drawable;

.field private mSubtitle:Ljava/lang/CharSequence;

.field private mSubtitleStyleRes:I

.field private mSubtitleView:Landroid/widget/TextView;

.field private mTitle:Ljava/lang/CharSequence;

.field private mTitleLayout:Landroid/widget/LinearLayout;

.field private mTitleOptional:Z

.field private mTitleStyleRes:I

.field private mTitleView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 70
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Lcom/android/internal/widget/ActionBarContextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 71
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 74
    const v0, 0x1010394

    #@3
    invoke-direct {p0, p1, p2, v0}, Lcom/android/internal/widget/ActionBarContextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@6
    .line 75
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 7
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 78
    invoke-direct {p0, p1, p2, p3}, Lcom/android/internal/widget/AbsActionBarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@4
    .line 80
    sget-object v1, Lcom/android/internal/R$styleable;->ActionMode:[I

    #@6
    invoke-virtual {p1, p2, v1, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@9
    move-result-object v0

    #@a
    .line 81
    .local v0, a:Landroid/content/res/TypedArray;
    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {p0, v1}, Lcom/android/internal/widget/ActionBarContextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    #@11
    .line 83
    const/4 v1, 0x2

    #@12
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@15
    move-result v1

    #@16
    iput v1, p0, Lcom/android/internal/widget/ActionBarContextView;->mTitleStyleRes:I

    #@18
    .line 85
    const/4 v1, 0x3

    #@19
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@1c
    move-result v1

    #@1d
    iput v1, p0, Lcom/android/internal/widget/ActionBarContextView;->mSubtitleStyleRes:I

    #@1f
    .line 88
    const/4 v1, 0x1

    #@20
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    #@23
    move-result v1

    #@24
    iput v1, p0, Lcom/android/internal/widget/AbsActionBarView;->mContentHeight:I

    #@26
    .line 91
    const/4 v1, 0x4

    #@27
    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@2a
    move-result-object v1

    #@2b
    iput-object v1, p0, Lcom/android/internal/widget/ActionBarContextView;->mSplitBackground:Landroid/graphics/drawable/Drawable;

    #@2d
    .line 94
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@30
    .line 95
    return-void
.end method

.method private finishAnimation()V
    .registers 3

    #@0
    .prologue
    .line 268
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarContextView;->mCurrentAnimation:Landroid/animation/Animator;

    #@2
    .line 269
    .local v0, a:Landroid/animation/Animator;
    if-eqz v0, :cond_a

    #@4
    .line 270
    const/4 v1, 0x0

    #@5
    iput-object v1, p0, Lcom/android/internal/widget/ActionBarContextView;->mCurrentAnimation:Landroid/animation/Animator;

    #@7
    .line 271
    invoke-virtual {v0}, Landroid/animation/Animator;->end()V

    #@a
    .line 273
    :cond_a
    return-void
.end method

.method private initTitle()V
    .registers 10

    #@0
    .prologue
    const/16 v6, 0x8

    #@2
    const/4 v4, 0x1

    #@3
    const/4 v5, 0x0

    #@4
    .line 177
    iget-object v3, p0, Lcom/android/internal/widget/ActionBarContextView;->mTitleLayout:Landroid/widget/LinearLayout;

    #@6
    if-nez v3, :cond_58

    #@8
    .line 178
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarContextView;->getContext()Landroid/content/Context;

    #@b
    move-result-object v3

    #@c
    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    #@f
    move-result-object v2

    #@10
    .line 179
    .local v2, inflater:Landroid/view/LayoutInflater;
    const v3, 0x1090019

    #@13
    invoke-virtual {v2, v3, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@16
    .line 180
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarContextView;->getChildCount()I

    #@19
    move-result v3

    #@1a
    add-int/lit8 v3, v3, -0x1

    #@1c
    invoke-virtual {p0, v3}, Lcom/android/internal/widget/ActionBarContextView;->getChildAt(I)Landroid/view/View;

    #@1f
    move-result-object v3

    #@20
    check-cast v3, Landroid/widget/LinearLayout;

    #@22
    iput-object v3, p0, Lcom/android/internal/widget/ActionBarContextView;->mTitleLayout:Landroid/widget/LinearLayout;

    #@24
    .line 181
    iget-object v3, p0, Lcom/android/internal/widget/ActionBarContextView;->mTitleLayout:Landroid/widget/LinearLayout;

    #@26
    const v7, 0x102025c

    #@29
    invoke-virtual {v3, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    #@2c
    move-result-object v3

    #@2d
    check-cast v3, Landroid/widget/TextView;

    #@2f
    iput-object v3, p0, Lcom/android/internal/widget/ActionBarContextView;->mTitleView:Landroid/widget/TextView;

    #@31
    .line 182
    iget-object v3, p0, Lcom/android/internal/widget/ActionBarContextView;->mTitleLayout:Landroid/widget/LinearLayout;

    #@33
    const v7, 0x102025d

    #@36
    invoke-virtual {v3, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    #@39
    move-result-object v3

    #@3a
    check-cast v3, Landroid/widget/TextView;

    #@3c
    iput-object v3, p0, Lcom/android/internal/widget/ActionBarContextView;->mSubtitleView:Landroid/widget/TextView;

    #@3e
    .line 183
    iget v3, p0, Lcom/android/internal/widget/ActionBarContextView;->mTitleStyleRes:I

    #@40
    if-eqz v3, :cond_4b

    #@42
    .line 184
    iget-object v3, p0, Lcom/android/internal/widget/ActionBarContextView;->mTitleView:Landroid/widget/TextView;

    #@44
    iget-object v7, p0, Lcom/android/internal/widget/ActionBarContextView;->mContext:Landroid/content/Context;

    #@46
    iget v8, p0, Lcom/android/internal/widget/ActionBarContextView;->mTitleStyleRes:I

    #@48
    invoke-virtual {v3, v7, v8}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    #@4b
    .line 186
    :cond_4b
    iget v3, p0, Lcom/android/internal/widget/ActionBarContextView;->mSubtitleStyleRes:I

    #@4d
    if-eqz v3, :cond_58

    #@4f
    .line 187
    iget-object v3, p0, Lcom/android/internal/widget/ActionBarContextView;->mSubtitleView:Landroid/widget/TextView;

    #@51
    iget-object v7, p0, Lcom/android/internal/widget/ActionBarContextView;->mContext:Landroid/content/Context;

    #@53
    iget v8, p0, Lcom/android/internal/widget/ActionBarContextView;->mSubtitleStyleRes:I

    #@55
    invoke-virtual {v3, v7, v8}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    #@58
    .line 191
    .end local v2           #inflater:Landroid/view/LayoutInflater;
    :cond_58
    iget-object v3, p0, Lcom/android/internal/widget/ActionBarContextView;->mTitleView:Landroid/widget/TextView;

    #@5a
    iget-object v7, p0, Lcom/android/internal/widget/ActionBarContextView;->mTitle:Ljava/lang/CharSequence;

    #@5c
    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@5f
    .line 192
    iget-object v3, p0, Lcom/android/internal/widget/ActionBarContextView;->mSubtitleView:Landroid/widget/TextView;

    #@61
    iget-object v7, p0, Lcom/android/internal/widget/ActionBarContextView;->mSubtitle:Ljava/lang/CharSequence;

    #@63
    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@66
    .line 194
    iget-object v3, p0, Lcom/android/internal/widget/ActionBarContextView;->mTitle:Ljava/lang/CharSequence;

    #@68
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@6b
    move-result v3

    #@6c
    if-nez v3, :cond_98

    #@6e
    move v1, v4

    #@6f
    .line 195
    .local v1, hasTitle:Z
    :goto_6f
    iget-object v3, p0, Lcom/android/internal/widget/ActionBarContextView;->mSubtitle:Ljava/lang/CharSequence;

    #@71
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@74
    move-result v3

    #@75
    if-nez v3, :cond_9a

    #@77
    move v0, v4

    #@78
    .line 196
    .local v0, hasSubtitle:Z
    :goto_78
    iget-object v4, p0, Lcom/android/internal/widget/ActionBarContextView;->mSubtitleView:Landroid/widget/TextView;

    #@7a
    if-eqz v0, :cond_9c

    #@7c
    move v3, v5

    #@7d
    :goto_7d
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setVisibility(I)V

    #@80
    .line 197
    iget-object v3, p0, Lcom/android/internal/widget/ActionBarContextView;->mTitleLayout:Landroid/widget/LinearLayout;

    #@82
    if-nez v1, :cond_86

    #@84
    if-eqz v0, :cond_87

    #@86
    :cond_86
    move v6, v5

    #@87
    :cond_87
    invoke-virtual {v3, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    #@8a
    .line 198
    iget-object v3, p0, Lcom/android/internal/widget/ActionBarContextView;->mTitleLayout:Landroid/widget/LinearLayout;

    #@8c
    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getParent()Landroid/view/ViewParent;

    #@8f
    move-result-object v3

    #@90
    if-nez v3, :cond_97

    #@92
    .line 199
    iget-object v3, p0, Lcom/android/internal/widget/ActionBarContextView;->mTitleLayout:Landroid/widget/LinearLayout;

    #@94
    invoke-virtual {p0, v3}, Lcom/android/internal/widget/ActionBarContextView;->addView(Landroid/view/View;)V

    #@97
    .line 201
    :cond_97
    return-void

    #@98
    .end local v0           #hasSubtitle:Z
    .end local v1           #hasTitle:Z
    :cond_98
    move v1, v5

    #@99
    .line 194
    goto :goto_6f

    #@9a
    .restart local v1       #hasTitle:Z
    :cond_9a
    move v0, v5

    #@9b
    .line 195
    goto :goto_78

    #@9c
    .restart local v0       #hasSubtitle:Z
    :cond_9c
    move v3, v6

    #@9d
    .line 196
    goto :goto_7d
.end method

.method private makeInAnimation()Landroid/animation/Animator;
    .registers 14

    #@0
    .prologue
    const/4 v12, 0x0

    #@1
    .line 403
    iget-object v9, p0, Lcom/android/internal/widget/ActionBarContextView;->mClose:Landroid/view/View;

    #@3
    iget-object v8, p0, Lcom/android/internal/widget/ActionBarContextView;->mClose:Landroid/view/View;

    #@5
    invoke-virtual {v8}, Landroid/view/View;->getWidth()I

    #@8
    move-result v8

    #@9
    neg-int v10, v8

    #@a
    iget-object v8, p0, Lcom/android/internal/widget/ActionBarContextView;->mClose:Landroid/view/View;

    #@c
    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@f
    move-result-object v8

    #@10
    check-cast v8, Landroid/view/ViewGroup$MarginLayoutParams;

    #@12
    iget v8, v8, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@14
    sub-int v8, v10, v8

    #@16
    int-to-float v8, v8

    #@17
    invoke-virtual {v9, v8}, Landroid/view/View;->setTranslationX(F)V

    #@1a
    .line 405
    iget-object v8, p0, Lcom/android/internal/widget/ActionBarContextView;->mClose:Landroid/view/View;

    #@1c
    const-string v9, "translationX"

    #@1e
    const/4 v10, 0x1

    #@1f
    new-array v10, v10, [F

    #@21
    const/4 v11, 0x0

    #@22
    aput v12, v10, v11

    #@24
    invoke-static {v8, v9, v10}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    #@27
    move-result-object v2

    #@28
    .line 406
    .local v2, buttonAnimator:Landroid/animation/ObjectAnimator;
    const-wide/16 v8, 0xc8

    #@2a
    invoke-virtual {v2, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    #@2d
    .line 407
    invoke-virtual {v2, p0}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    #@30
    .line 408
    new-instance v8, Landroid/view/animation/DecelerateInterpolator;

    #@32
    invoke-direct {v8}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    #@35
    invoke-virtual {v2, v8}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    #@38
    .line 410
    new-instance v7, Landroid/animation/AnimatorSet;

    #@3a
    invoke-direct {v7}, Landroid/animation/AnimatorSet;-><init>()V

    #@3d
    .line 411
    .local v7, set:Landroid/animation/AnimatorSet;
    invoke-virtual {v7, v2}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    #@40
    move-result-object v1

    #@41
    .line 413
    .local v1, b:Landroid/animation/AnimatorSet$Builder;
    iget-object v8, p0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@43
    if-eqz v8, :cond_74

    #@45
    .line 414
    iget-object v8, p0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@47
    invoke-virtual {v8}, Lcom/android/internal/view/menu/ActionMenuView;->getChildCount()I

    #@4a
    move-result v4

    #@4b
    .line 415
    .local v4, count:I
    if-lez v4, :cond_74

    #@4d
    .line 416
    add-int/lit8 v5, v4, -0x1

    #@4f
    .local v5, i:I
    const/4 v6, 0x0

    #@50
    .local v6, j:I
    :goto_50
    if-ltz v5, :cond_74

    #@52
    .line 417
    iget-object v8, p0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@54
    invoke-virtual {v8, v5}, Lcom/android/internal/view/menu/ActionMenuView;->getChildAt(I)Landroid/view/View;

    #@57
    move-result-object v3

    #@58
    .line 418
    .local v3, child:Landroid/view/View;
    invoke-virtual {v3, v12}, Landroid/view/View;->setScaleY(F)V

    #@5b
    .line 419
    const-string v8, "scaleY"

    #@5d
    const/4 v9, 0x2

    #@5e
    new-array v9, v9, [F

    #@60
    fill-array-data v9, :array_76

    #@63
    invoke-static {v3, v8, v9}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    #@66
    move-result-object v0

    #@67
    .line 420
    .local v0, a:Landroid/animation/ObjectAnimator;
    const-wide/16 v8, 0x12c

    #@69
    invoke-virtual {v0, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    #@6c
    .line 421
    invoke-virtual {v1, v0}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    #@6f
    .line 416
    add-int/lit8 v5, v5, -0x1

    #@71
    add-int/lit8 v6, v6, 0x1

    #@73
    goto :goto_50

    #@74
    .line 426
    .end local v0           #a:Landroid/animation/ObjectAnimator;
    .end local v3           #child:Landroid/view/View;
    .end local v4           #count:I
    .end local v5           #i:I
    .end local v6           #j:I
    :cond_74
    return-object v7

    #@75
    .line 419
    nop

    #@76
    :array_76
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
    .end array-data
.end method

.method private makeOutAnimation()Landroid/animation/Animator;
    .registers 16

    #@0
    .prologue
    const/4 v14, 0x1

    #@1
    const/4 v13, 0x0

    #@2
    const/4 v12, 0x0

    #@3
    .line 430
    iget-object v8, p0, Lcom/android/internal/widget/ActionBarContextView;->mClose:Landroid/view/View;

    #@5
    const-string v9, "translationX"

    #@7
    new-array v10, v14, [F

    #@9
    iget-object v7, p0, Lcom/android/internal/widget/ActionBarContextView;->mClose:Landroid/view/View;

    #@b
    invoke-virtual {v7}, Landroid/view/View;->getWidth()I

    #@e
    move-result v7

    #@f
    neg-int v11, v7

    #@10
    iget-object v7, p0, Lcom/android/internal/widget/ActionBarContextView;->mClose:Landroid/view/View;

    #@12
    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@15
    move-result-object v7

    #@16
    check-cast v7, Landroid/view/ViewGroup$MarginLayoutParams;

    #@18
    iget v7, v7, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@1a
    sub-int v7, v11, v7

    #@1c
    int-to-float v7, v7

    #@1d
    aput v7, v10, v13

    #@1f
    invoke-static {v8, v9, v10}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    #@22
    move-result-object v2

    #@23
    .line 432
    .local v2, buttonAnimator:Landroid/animation/ObjectAnimator;
    const-wide/16 v7, 0xc8

    #@25
    invoke-virtual {v2, v7, v8}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    #@28
    .line 433
    invoke-virtual {v2, p0}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    #@2b
    .line 434
    new-instance v7, Landroid/view/animation/DecelerateInterpolator;

    #@2d
    invoke-direct {v7}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    #@30
    invoke-virtual {v2, v7}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    #@33
    .line 436
    new-instance v6, Landroid/animation/AnimatorSet;

    #@35
    invoke-direct {v6}, Landroid/animation/AnimatorSet;-><init>()V

    #@38
    .line 437
    .local v6, set:Landroid/animation/AnimatorSet;
    invoke-virtual {v6, v2}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    #@3b
    move-result-object v1

    #@3c
    .line 439
    .local v1, b:Landroid/animation/AnimatorSet$Builder;
    iget-object v7, p0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@3e
    if-eqz v7, :cond_69

    #@40
    .line 440
    iget-object v7, p0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@42
    invoke-virtual {v7}, Lcom/android/internal/view/menu/ActionMenuView;->getChildCount()I

    #@45
    move-result v4

    #@46
    .line 441
    .local v4, count:I
    if-lez v4, :cond_69

    #@48
    .line 442
    const/4 v5, 0x0

    #@49
    .local v5, i:I
    :goto_49
    if-gez v5, :cond_69

    #@4b
    .line 443
    iget-object v7, p0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@4d
    invoke-virtual {v7, v5}, Lcom/android/internal/view/menu/ActionMenuView;->getChildAt(I)Landroid/view/View;

    #@50
    move-result-object v3

    #@51
    .line 444
    .local v3, child:Landroid/view/View;
    invoke-virtual {v3, v12}, Landroid/view/View;->setScaleY(F)V

    #@54
    .line 445
    const-string v7, "scaleY"

    #@56
    new-array v8, v14, [F

    #@58
    aput v12, v8, v13

    #@5a
    invoke-static {v3, v7, v8}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    #@5d
    move-result-object v0

    #@5e
    .line 446
    .local v0, a:Landroid/animation/ObjectAnimator;
    const-wide/16 v7, 0x12c

    #@60
    invoke-virtual {v0, v7, v8}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    #@63
    .line 447
    invoke-virtual {v1, v0}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    #@66
    .line 442
    add-int/lit8 v5, v5, 0x1

    #@68
    goto :goto_49

    #@69
    .line 452
    .end local v0           #a:Landroid/animation/ObjectAnimator;
    .end local v3           #child:Landroid/view/View;
    .end local v4           #count:I
    .end local v5           #i:I
    :cond_69
    return-object v6
.end method


# virtual methods
.method public closeMode()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x2

    #@1
    .line 252
    iget v0, p0, Lcom/android/internal/widget/ActionBarContextView;->mAnimationMode:I

    #@3
    if-ne v0, v1, :cond_6

    #@5
    .line 265
    :goto_5
    return-void

    #@6
    .line 256
    :cond_6
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarContextView;->mClose:Landroid/view/View;

    #@8
    if-nez v0, :cond_e

    #@a
    .line 257
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarContextView;->killMode()V

    #@d
    goto :goto_5

    #@e
    .line 261
    :cond_e
    invoke-direct {p0}, Lcom/android/internal/widget/ActionBarContextView;->finishAnimation()V

    #@11
    .line 262
    iput v1, p0, Lcom/android/internal/widget/ActionBarContextView;->mAnimationMode:I

    #@13
    .line 263
    invoke-direct {p0}, Lcom/android/internal/widget/ActionBarContextView;->makeOutAnimation()Landroid/animation/Animator;

    #@16
    move-result-object v0

    #@17
    iput-object v0, p0, Lcom/android/internal/widget/ActionBarContextView;->mCurrentAnimation:Landroid/animation/Animator;

    #@19
    .line 264
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarContextView;->mCurrentAnimation:Landroid/animation/Animator;

    #@1b
    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    #@1e
    goto :goto_5
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .registers 4

    #@0
    .prologue
    .line 314
    new-instance v0, Landroid/view/ViewGroup$MarginLayoutParams;

    #@2
    const/4 v1, -0x1

    #@3
    const/4 v2, -0x2

    #@4
    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    #@7
    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .registers 4
    .parameter "attrs"

    #@0
    .prologue
    .line 319
    new-instance v0, Landroid/view/ViewGroup$MarginLayoutParams;

    #@2
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarContextView;->getContext()Landroid/content/Context;

    #@5
    move-result-object v1

    #@6
    invoke-direct {v0, v1, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@9
    return-object v0
.end method

.method public getSubtitle()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 173
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarContextView;->mSubtitle:Ljava/lang/CharSequence;

    #@2
    return-object v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 169
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarContextView;->mTitle:Ljava/lang/CharSequence;

    #@2
    return-object v0
.end method

.method public hideOverflowMenu()Z
    .registers 2

    #@0
    .prologue
    .line 296
    iget-object v0, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 297
    iget-object v0, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@6
    invoke-virtual {v0}, Lcom/android/internal/view/menu/ActionMenuPresenter;->hideOverflowMenu()Z

    #@9
    move-result v0

    #@a
    .line 299
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public initForMode(Landroid/view/ActionMode;)V
    .registers 10
    .parameter "mode"

    #@0
    .prologue
    const/4 v7, -0x1

    #@1
    const/4 v6, 0x1

    #@2
    .line 204
    iget-object v4, p0, Lcom/android/internal/widget/ActionBarContextView;->mClose:Landroid/view/View;

    #@4
    if-nez v4, :cond_70

    #@6
    .line 205
    iget-object v4, p0, Lcom/android/internal/widget/ActionBarContextView;->mContext:Landroid/content/Context;

    #@8
    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    #@b
    move-result-object v1

    #@c
    .line 206
    .local v1, inflater:Landroid/view/LayoutInflater;
    const v4, 0x109001e

    #@f
    const/4 v5, 0x0

    #@10
    invoke-virtual {v1, v4, p0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@13
    move-result-object v4

    #@14
    iput-object v4, p0, Lcom/android/internal/widget/ActionBarContextView;->mClose:Landroid/view/View;

    #@16
    .line 207
    iget-object v4, p0, Lcom/android/internal/widget/ActionBarContextView;->mClose:Landroid/view/View;

    #@18
    invoke-virtual {p0, v4}, Lcom/android/internal/widget/ActionBarContextView;->addView(Landroid/view/View;)V

    #@1b
    .line 212
    .end local v1           #inflater:Landroid/view/LayoutInflater;
    :cond_1b
    :goto_1b
    iget-object v4, p0, Lcom/android/internal/widget/ActionBarContextView;->mClose:Landroid/view/View;

    #@1d
    const v5, 0x102025e

    #@20
    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@23
    move-result-object v0

    #@24
    .line 213
    .local v0, closeButton:Landroid/view/View;
    new-instance v4, Lcom/android/internal/widget/ActionBarContextView$1;

    #@26
    invoke-direct {v4, p0, p1}, Lcom/android/internal/widget/ActionBarContextView$1;-><init>(Lcom/android/internal/widget/ActionBarContextView;Landroid/view/ActionMode;)V

    #@29
    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@2c
    .line 219
    invoke-virtual {p1}, Landroid/view/ActionMode;->getMenu()Landroid/view/Menu;

    #@2f
    move-result-object v3

    #@30
    check-cast v3, Lcom/android/internal/view/menu/MenuBuilder;

    #@32
    .line 220
    .local v3, menu:Lcom/android/internal/view/menu/MenuBuilder;
    iget-object v4, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@34
    if-eqz v4, :cond_3b

    #@36
    .line 221
    iget-object v4, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@38
    invoke-virtual {v4}, Lcom/android/internal/view/menu/ActionMenuPresenter;->dismissPopupMenus()Z

    #@3b
    .line 223
    :cond_3b
    new-instance v4, Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@3d
    iget-object v5, p0, Lcom/android/internal/widget/ActionBarContextView;->mContext:Landroid/content/Context;

    #@3f
    invoke-direct {v4, v5}, Lcom/android/internal/view/menu/ActionMenuPresenter;-><init>(Landroid/content/Context;)V

    #@42
    iput-object v4, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@44
    .line 224
    iget-object v4, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@46
    invoke-virtual {v4, v6}, Lcom/android/internal/view/menu/ActionMenuPresenter;->setReserveOverflow(Z)V

    #@49
    .line 226
    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    #@4b
    const/4 v4, -0x2

    #@4c
    invoke-direct {v2, v4, v7}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    #@4f
    .line 228
    .local v2, layoutParams:Landroid/view/ViewGroup$LayoutParams;
    iget-boolean v4, p0, Lcom/android/internal/widget/AbsActionBarView;->mSplitActionBar:Z

    #@51
    if-nez v4, :cond_7e

    #@53
    .line 229
    iget-object v4, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@55
    invoke-virtual {v3, v4}, Lcom/android/internal/view/menu/MenuBuilder;->addMenuPresenter(Lcom/android/internal/view/menu/MenuPresenter;)V

    #@58
    .line 230
    iget-object v4, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@5a
    invoke-virtual {v4, p0}, Lcom/android/internal/view/menu/ActionMenuPresenter;->getMenuView(Landroid/view/ViewGroup;)Lcom/android/internal/view/menu/MenuView;

    #@5d
    move-result-object v4

    #@5e
    check-cast v4, Lcom/android/internal/view/menu/ActionMenuView;

    #@60
    iput-object v4, p0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@62
    .line 231
    iget-object v4, p0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@64
    const/4 v5, 0x0

    #@65
    invoke-virtual {v4, v5}, Lcom/android/internal/view/menu/ActionMenuView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    #@68
    .line 232
    iget-object v4, p0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@6a
    invoke-virtual {p0, v4, v2}, Lcom/android/internal/widget/ActionBarContextView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@6d
    .line 248
    :goto_6d
    iput-boolean v6, p0, Lcom/android/internal/widget/ActionBarContextView;->mAnimateInOnLayout:Z

    #@6f
    .line 249
    return-void

    #@70
    .line 208
    .end local v0           #closeButton:Landroid/view/View;
    .end local v2           #layoutParams:Landroid/view/ViewGroup$LayoutParams;
    .end local v3           #menu:Lcom/android/internal/view/menu/MenuBuilder;
    :cond_70
    iget-object v4, p0, Lcom/android/internal/widget/ActionBarContextView;->mClose:Landroid/view/View;

    #@72
    invoke-virtual {v4}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    #@75
    move-result-object v4

    #@76
    if-nez v4, :cond_1b

    #@78
    .line 209
    iget-object v4, p0, Lcom/android/internal/widget/ActionBarContextView;->mClose:Landroid/view/View;

    #@7a
    invoke-virtual {p0, v4}, Lcom/android/internal/widget/ActionBarContextView;->addView(Landroid/view/View;)V

    #@7d
    goto :goto_1b

    #@7e
    .line 235
    .restart local v0       #closeButton:Landroid/view/View;
    .restart local v2       #layoutParams:Landroid/view/ViewGroup$LayoutParams;
    .restart local v3       #menu:Lcom/android/internal/view/menu/MenuBuilder;
    :cond_7e
    iget-object v4, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@80
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarContextView;->getContext()Landroid/content/Context;

    #@83
    move-result-object v5

    #@84
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@87
    move-result-object v5

    #@88
    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@8b
    move-result-object v5

    #@8c
    iget v5, v5, Landroid/util/DisplayMetrics;->widthPixels:I

    #@8e
    invoke-virtual {v4, v5, v6}, Lcom/android/internal/view/menu/ActionMenuPresenter;->setWidthLimit(IZ)V

    #@91
    .line 238
    iget-object v4, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@93
    const v5, 0x7fffffff

    #@96
    invoke-virtual {v4, v5}, Lcom/android/internal/view/menu/ActionMenuPresenter;->setItemLimit(I)V

    #@99
    .line 240
    iput v7, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@9b
    .line 241
    iget v4, p0, Lcom/android/internal/widget/AbsActionBarView;->mContentHeight:I

    #@9d
    iput v4, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@9f
    .line 242
    iget-object v4, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@a1
    invoke-virtual {v3, v4}, Lcom/android/internal/view/menu/MenuBuilder;->addMenuPresenter(Lcom/android/internal/view/menu/MenuPresenter;)V

    #@a4
    .line 243
    iget-object v4, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@a6
    invoke-virtual {v4, p0}, Lcom/android/internal/view/menu/ActionMenuPresenter;->getMenuView(Landroid/view/ViewGroup;)Lcom/android/internal/view/menu/MenuView;

    #@a9
    move-result-object v4

    #@aa
    check-cast v4, Lcom/android/internal/view/menu/ActionMenuView;

    #@ac
    iput-object v4, p0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@ae
    .line 244
    iget-object v4, p0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@b0
    iget-object v5, p0, Lcom/android/internal/widget/ActionBarContextView;->mSplitBackground:Landroid/graphics/drawable/Drawable;

    #@b2
    invoke-virtual {v4, v5}, Lcom/android/internal/view/menu/ActionMenuView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    #@b5
    .line 245
    iget-object v4, p0, Lcom/android/internal/widget/AbsActionBarView;->mSplitView:Lcom/android/internal/widget/ActionBarContainer;

    #@b7
    iget-object v5, p0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@b9
    invoke-virtual {v4, v5, v2}, Lcom/android/internal/widget/ActionBarContainer;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@bc
    goto :goto_6d
.end method

.method public isOverflowMenuShowing()Z
    .registers 2

    #@0
    .prologue
    .line 304
    iget-object v0, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 305
    iget-object v0, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@6
    invoke-virtual {v0}, Lcom/android/internal/view/menu/ActionMenuPresenter;->isOverflowMenuShowing()Z

    #@9
    move-result v0

    #@a
    .line 307
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public isTitleOptional()Z
    .registers 2

    #@0
    .prologue
    .line 539
    iget-boolean v0, p0, Lcom/android/internal/widget/ActionBarContextView;->mTitleOptional:Z

    #@2
    return v0
.end method

.method public killMode()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 276
    invoke-direct {p0}, Lcom/android/internal/widget/ActionBarContextView;->finishAnimation()V

    #@4
    .line 277
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarContextView;->removeAllViews()V

    #@7
    .line 278
    iget-object v0, p0, Lcom/android/internal/widget/AbsActionBarView;->mSplitView:Lcom/android/internal/widget/ActionBarContainer;

    #@9
    if-eqz v0, :cond_12

    #@b
    .line 279
    iget-object v0, p0, Lcom/android/internal/widget/AbsActionBarView;->mSplitView:Lcom/android/internal/widget/ActionBarContainer;

    #@d
    iget-object v1, p0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@f
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/ActionBarContainer;->removeView(Landroid/view/View;)V

    #@12
    .line 281
    :cond_12
    iput-object v2, p0, Lcom/android/internal/widget/ActionBarContextView;->mCustomView:Landroid/view/View;

    #@14
    .line 282
    iput-object v2, p0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@16
    .line 283
    const/4 v0, 0x0

    #@17
    iput-boolean v0, p0, Lcom/android/internal/widget/ActionBarContextView;->mAnimateInOnLayout:Z

    #@19
    .line 284
    return-void
.end method

.method public onAnimationCancel(Landroid/animation/Animator;)V
    .registers 2
    .parameter "animation"

    #@0
    .prologue
    .line 507
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .registers 4
    .parameter "animation"

    #@0
    .prologue
    .line 499
    iget v0, p0, Lcom/android/internal/widget/ActionBarContextView;->mAnimationMode:I

    #@2
    const/4 v1, 0x2

    #@3
    if-ne v0, v1, :cond_8

    #@5
    .line 500
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarContextView;->killMode()V

    #@8
    .line 502
    :cond_8
    const/4 v0, 0x0

    #@9
    iput v0, p0, Lcom/android/internal/widget/ActionBarContextView;->mAnimationMode:I

    #@b
    .line 503
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .registers 2
    .parameter "animation"

    #@0
    .prologue
    .line 511
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .registers 2
    .parameter "animation"

    #@0
    .prologue
    .line 495
    return-void
.end method

.method public onDetachedFromWindow()V
    .registers 2

    #@0
    .prologue
    .line 99
    invoke-super {p0}, Lcom/android/internal/widget/AbsActionBarView;->onDetachedFromWindow()V

    #@3
    .line 100
    iget-object v0, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@5
    if-eqz v0, :cond_11

    #@7
    .line 101
    iget-object v0, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@9
    invoke-virtual {v0}, Lcom/android/internal/view/menu/ActionMenuPresenter;->hideOverflowMenu()Z

    #@c
    .line 102
    iget-object v0, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@e
    invoke-virtual {v0}, Lcom/android/internal/view/menu/ActionMenuPresenter;->hideSubMenus()Z

    #@11
    .line 104
    :cond_11
    return-void
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 4
    .parameter "event"

    #@0
    .prologue
    .line 520
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    #@3
    move-result v0

    #@4
    const/16 v1, 0x20

    #@6
    if-ne v0, v1, :cond_27

    #@8
    .line 522
    invoke-virtual {p1, p0}, Landroid/view/accessibility/AccessibilityEvent;->setSource(Landroid/view/View;)V

    #@b
    .line 523
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@16
    .line 524
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarContextView;->getContext()Landroid/content/Context;

    #@19
    move-result-object v0

    #@1a
    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@1d
    move-result-object v0

    #@1e
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setPackageName(Ljava/lang/CharSequence;)V

    #@21
    .line 525
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarContextView;->mTitle:Ljava/lang/CharSequence;

    #@23
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setContentDescription(Ljava/lang/CharSequence;)V

    #@26
    .line 529
    :goto_26
    return-void

    #@27
    .line 527
    :cond_27
    invoke-super {p0, p1}, Lcom/android/internal/widget/AbsActionBarView;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@2a
    goto :goto_26
.end method

.method protected onLayout(ZIIII)V
    .registers 21
    .parameter "changed"
    .parameter "l"
    .parameter "t"
    .parameter "r"
    .parameter "b"

    #@0
    .prologue
    .line 457
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarContextView;->isLayoutRtl()Z

    #@3
    move-result v5

    #@4
    .line 458
    .local v5, isLayoutRtl:Z
    if-eqz v5, :cond_a1

    #@6
    sub-int v0, p4, p2

    #@8
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarContextView;->getPaddingRight()I

    #@b
    move-result v1

    #@c
    sub-int v2, v0, v1

    #@e
    .line 459
    .local v2, x:I
    :goto_e
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarContextView;->getPaddingTop()I

    #@11
    move-result v3

    #@12
    .line 460
    .local v3, y:I
    sub-int v0, p5, p3

    #@14
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarContextView;->getPaddingTop()I

    #@17
    move-result v1

    #@18
    sub-int/2addr v0, v1

    #@19
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarContextView;->getPaddingBottom()I

    #@1c
    move-result v1

    #@1d
    sub-int v4, v0, v1

    #@1f
    .line 462
    .local v4, contentHeight:I
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarContextView;->mClose:Landroid/view/View;

    #@21
    if-eqz v0, :cond_62

    #@23
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarContextView;->mClose:Landroid/view/View;

    #@25
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    #@28
    move-result v0

    #@29
    const/16 v1, 0x8

    #@2b
    if-eq v0, v1, :cond_62

    #@2d
    .line 463
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarContextView;->mClose:Landroid/view/View;

    #@2f
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@32
    move-result-object v13

    #@33
    check-cast v13, Landroid/view/ViewGroup$MarginLayoutParams;

    #@35
    .line 464
    .local v13, lp:Landroid/view/ViewGroup$MarginLayoutParams;
    if-eqz v5, :cond_a7

    #@37
    iget v14, v13, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@39
    .line 465
    .local v14, startMargin:I
    :goto_39
    if-eqz v5, :cond_aa

    #@3b
    iget v12, v13, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@3d
    .line 466
    .local v12, endMargin:I
    :goto_3d
    invoke-static {v2, v14, v5}, Lcom/android/internal/widget/ActionBarContextView;->next(IIZ)I

    #@40
    move-result v2

    #@41
    .line 467
    iget-object v1, p0, Lcom/android/internal/widget/ActionBarContextView;->mClose:Landroid/view/View;

    #@43
    move-object v0, p0

    #@44
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/widget/ActionBarContextView;->positionChild(Landroid/view/View;IIIZ)I

    #@47
    move-result v0

    #@48
    add-int/2addr v2, v0

    #@49
    .line 468
    invoke-static {v2, v12, v5}, Lcom/android/internal/widget/ActionBarContextView;->next(IIZ)I

    #@4c
    move-result v2

    #@4d
    .line 470
    iget-boolean v0, p0, Lcom/android/internal/widget/ActionBarContextView;->mAnimateInOnLayout:Z

    #@4f
    if-eqz v0, :cond_62

    #@51
    .line 471
    const/4 v0, 0x1

    #@52
    iput v0, p0, Lcom/android/internal/widget/ActionBarContextView;->mAnimationMode:I

    #@54
    .line 472
    invoke-direct {p0}, Lcom/android/internal/widget/ActionBarContextView;->makeInAnimation()Landroid/animation/Animator;

    #@57
    move-result-object v0

    #@58
    iput-object v0, p0, Lcom/android/internal/widget/ActionBarContextView;->mCurrentAnimation:Landroid/animation/Animator;

    #@5a
    .line 473
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarContextView;->mCurrentAnimation:Landroid/animation/Animator;

    #@5c
    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    #@5f
    .line 474
    const/4 v0, 0x0

    #@60
    iput-boolean v0, p0, Lcom/android/internal/widget/ActionBarContextView;->mAnimateInOnLayout:Z

    #@62
    .line 478
    .end local v12           #endMargin:I
    .end local v13           #lp:Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v14           #startMargin:I
    :cond_62
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarContextView;->mTitleLayout:Landroid/widget/LinearLayout;

    #@64
    if-eqz v0, :cond_7c

    #@66
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarContextView;->mCustomView:Landroid/view/View;

    #@68
    if-nez v0, :cond_7c

    #@6a
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarContextView;->mTitleLayout:Landroid/widget/LinearLayout;

    #@6c
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    #@6f
    move-result v0

    #@70
    const/16 v1, 0x8

    #@72
    if-eq v0, v1, :cond_7c

    #@74
    .line 479
    iget-object v1, p0, Lcom/android/internal/widget/ActionBarContextView;->mTitleLayout:Landroid/widget/LinearLayout;

    #@76
    move-object v0, p0

    #@77
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/widget/ActionBarContextView;->positionChild(Landroid/view/View;IIIZ)I

    #@7a
    move-result v0

    #@7b
    add-int/2addr v2, v0

    #@7c
    .line 482
    :cond_7c
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarContextView;->mCustomView:Landroid/view/View;

    #@7e
    if-eqz v0, :cond_88

    #@80
    .line 483
    iget-object v1, p0, Lcom/android/internal/widget/ActionBarContextView;->mCustomView:Landroid/view/View;

    #@82
    move-object v0, p0

    #@83
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/widget/ActionBarContextView;->positionChild(Landroid/view/View;IIIZ)I

    #@86
    move-result v0

    #@87
    add-int/2addr v2, v0

    #@88
    .line 486
    :cond_88
    if-eqz v5, :cond_ad

    #@8a
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarContextView;->getPaddingLeft()I

    #@8d
    move-result v2

    #@8e
    .line 488
    :goto_8e
    iget-object v0, p0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@90
    if-eqz v0, :cond_a0

    #@92
    .line 489
    iget-object v7, p0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@94
    if-nez v5, :cond_b6

    #@96
    const/4 v11, 0x1

    #@97
    :goto_97
    move-object v6, p0

    #@98
    move v8, v2

    #@99
    move v9, v3

    #@9a
    move v10, v4

    #@9b
    invoke-virtual/range {v6 .. v11}, Lcom/android/internal/widget/ActionBarContextView;->positionChild(Landroid/view/View;IIIZ)I

    #@9e
    move-result v0

    #@9f
    add-int/2addr v2, v0

    #@a0
    .line 491
    :cond_a0
    return-void

    #@a1
    .line 458
    .end local v2           #x:I
    .end local v3           #y:I
    .end local v4           #contentHeight:I
    :cond_a1
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarContextView;->getPaddingLeft()I

    #@a4
    move-result v2

    #@a5
    goto/16 :goto_e

    #@a7
    .line 464
    .restart local v2       #x:I
    .restart local v3       #y:I
    .restart local v4       #contentHeight:I
    .restart local v13       #lp:Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_a7
    iget v14, v13, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@a9
    goto :goto_39

    #@aa
    .line 465
    .restart local v14       #startMargin:I
    :cond_aa
    iget v12, v13, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@ac
    goto :goto_3d

    #@ad
    .line 486
    .end local v13           #lp:Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v14           #startMargin:I
    :cond_ad
    sub-int v0, p4, p2

    #@af
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarContextView;->getPaddingRight()I

    #@b2
    move-result v1

    #@b3
    sub-int v2, v0, v1

    #@b5
    goto :goto_8e

    #@b6
    .line 489
    :cond_b6
    const/4 v11, 0x0

    #@b7
    goto :goto_97
.end method

.method protected onMeasure(II)V
    .registers 30
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    .line 324
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@3
    move-result v23

    #@4
    .line 325
    .local v23, widthMode:I
    const/high16 v24, 0x4000

    #@6
    move/from16 v0, v23

    #@8
    move/from16 v1, v24

    #@a
    if-eq v0, v1, :cond_33

    #@c
    .line 326
    new-instance v24, Ljava/lang/IllegalStateException;

    #@e
    new-instance v25, Ljava/lang/StringBuilder;

    #@10
    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@16
    move-result-object v26

    #@17
    invoke-virtual/range {v26 .. v26}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@1a
    move-result-object v26

    #@1b
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v25

    #@1f
    const-string v26, " can only be used "

    #@21
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v25

    #@25
    const-string v26, "with android:layout_width=\"match_parent\" (or fill_parent)"

    #@27
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v25

    #@2b
    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v25

    #@2f
    invoke-direct/range {v24 .. v25}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@32
    throw v24

    #@33
    .line 330
    :cond_33
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@36
    move-result v12

    #@37
    .line 331
    .local v12, heightMode:I
    if-nez v12, :cond_60

    #@39
    .line 332
    new-instance v24, Ljava/lang/IllegalStateException;

    #@3b
    new-instance v25, Ljava/lang/StringBuilder;

    #@3d
    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    #@40
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@43
    move-result-object v26

    #@44
    invoke-virtual/range {v26 .. v26}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@47
    move-result-object v26

    #@48
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v25

    #@4c
    const-string v26, " can only be used "

    #@4e
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v25

    #@52
    const-string v26, "with android:layout_height=\"wrap_content\""

    #@54
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v25

    #@58
    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5b
    move-result-object v25

    #@5c
    invoke-direct/range {v24 .. v25}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@5f
    throw v24

    #@60
    .line 336
    :cond_60
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@63
    move-result v5

    #@64
    .line 338
    .local v5, contentWidth:I
    move-object/from16 v0, p0

    #@66
    iget v0, v0, Lcom/android/internal/widget/AbsActionBarView;->mContentHeight:I

    #@68
    move/from16 v24, v0

    #@6a
    if-lez v24, :cond_1c5

    #@6c
    move-object/from16 v0, p0

    #@6e
    iget v15, v0, Lcom/android/internal/widget/AbsActionBarView;->mContentHeight:I

    #@70
    .line 341
    .local v15, maxHeight:I
    :goto_70
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/widget/ActionBarContextView;->getPaddingTop()I

    #@73
    move-result v24

    #@74
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/widget/ActionBarContextView;->getPaddingBottom()I

    #@77
    move-result v25

    #@78
    add-int v22, v24, v25

    #@7a
    .line 342
    .local v22, verticalPadding:I
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/widget/ActionBarContextView;->getPaddingLeft()I

    #@7d
    move-result v24

    #@7e
    sub-int v24, v5, v24

    #@80
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/widget/ActionBarContextView;->getPaddingRight()I

    #@83
    move-result v25

    #@84
    sub-int v3, v24, v25

    #@86
    .line 343
    .local v3, availableWidth:I
    sub-int v11, v15, v22

    #@88
    .line 344
    .local v11, height:I
    const/high16 v24, -0x8000

    #@8a
    move/from16 v0, v24

    #@8c
    invoke-static {v11, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@8f
    move-result v4

    #@90
    .line 346
    .local v4, childSpecHeight:I
    move-object/from16 v0, p0

    #@92
    iget-object v0, v0, Lcom/android/internal/widget/ActionBarContextView;->mClose:Landroid/view/View;

    #@94
    move-object/from16 v24, v0

    #@96
    if-eqz v24, :cond_c2

    #@98
    .line 347
    move-object/from16 v0, p0

    #@9a
    iget-object v0, v0, Lcom/android/internal/widget/ActionBarContextView;->mClose:Landroid/view/View;

    #@9c
    move-object/from16 v24, v0

    #@9e
    const/16 v25, 0x0

    #@a0
    move-object/from16 v0, p0

    #@a2
    move-object/from16 v1, v24

    #@a4
    move/from16 v2, v25

    #@a6
    invoke-virtual {v0, v1, v3, v4, v2}, Lcom/android/internal/widget/ActionBarContextView;->measureChildView(Landroid/view/View;III)I

    #@a9
    move-result v3

    #@aa
    .line 348
    move-object/from16 v0, p0

    #@ac
    iget-object v0, v0, Lcom/android/internal/widget/ActionBarContextView;->mClose:Landroid/view/View;

    #@ae
    move-object/from16 v24, v0

    #@b0
    invoke-virtual/range {v24 .. v24}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@b3
    move-result-object v14

    #@b4
    check-cast v14, Landroid/view/ViewGroup$MarginLayoutParams;

    #@b6
    .line 349
    .local v14, lp:Landroid/view/ViewGroup$MarginLayoutParams;
    iget v0, v14, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@b8
    move/from16 v24, v0

    #@ba
    iget v0, v14, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@bc
    move/from16 v25, v0

    #@be
    add-int v24, v24, v25

    #@c0
    sub-int v3, v3, v24

    #@c2
    .line 352
    .end local v14           #lp:Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_c2
    move-object/from16 v0, p0

    #@c4
    iget-object v0, v0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@c6
    move-object/from16 v24, v0

    #@c8
    if-eqz v24, :cond_ec

    #@ca
    move-object/from16 v0, p0

    #@cc
    iget-object v0, v0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@ce
    move-object/from16 v24, v0

    #@d0
    invoke-virtual/range {v24 .. v24}, Lcom/android/internal/view/menu/ActionMenuView;->getParent()Landroid/view/ViewParent;

    #@d3
    move-result-object v24

    #@d4
    move-object/from16 v0, v24

    #@d6
    move-object/from16 v1, p0

    #@d8
    if-ne v0, v1, :cond_ec

    #@da
    .line 353
    move-object/from16 v0, p0

    #@dc
    iget-object v0, v0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@de
    move-object/from16 v24, v0

    #@e0
    const/16 v25, 0x0

    #@e2
    move-object/from16 v0, p0

    #@e4
    move-object/from16 v1, v24

    #@e6
    move/from16 v2, v25

    #@e8
    invoke-virtual {v0, v1, v3, v4, v2}, Lcom/android/internal/widget/ActionBarContextView;->measureChildView(Landroid/view/View;III)I

    #@eb
    move-result v3

    #@ec
    .line 357
    :cond_ec
    move-object/from16 v0, p0

    #@ee
    iget-object v0, v0, Lcom/android/internal/widget/ActionBarContextView;->mTitleLayout:Landroid/widget/LinearLayout;

    #@f0
    move-object/from16 v24, v0

    #@f2
    if-eqz v24, :cond_13e

    #@f4
    move-object/from16 v0, p0

    #@f6
    iget-object v0, v0, Lcom/android/internal/widget/ActionBarContextView;->mCustomView:Landroid/view/View;

    #@f8
    move-object/from16 v24, v0

    #@fa
    if-nez v24, :cond_13e

    #@fc
    .line 358
    move-object/from16 v0, p0

    #@fe
    iget-boolean v0, v0, Lcom/android/internal/widget/ActionBarContextView;->mTitleOptional:Z

    #@100
    move/from16 v24, v0

    #@102
    if-eqz v24, :cond_1d3

    #@104
    .line 359
    const/16 v24, 0x0

    #@106
    const/16 v25, 0x0

    #@108
    invoke-static/range {v24 .. v25}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@10b
    move-result v20

    #@10c
    .line 360
    .local v20, titleWidthSpec:I
    move-object/from16 v0, p0

    #@10e
    iget-object v0, v0, Lcom/android/internal/widget/ActionBarContextView;->mTitleLayout:Landroid/widget/LinearLayout;

    #@110
    move-object/from16 v24, v0

    #@112
    move-object/from16 v0, v24

    #@114
    move/from16 v1, v20

    #@116
    invoke-virtual {v0, v1, v4}, Landroid/widget/LinearLayout;->measure(II)V

    #@119
    .line 361
    move-object/from16 v0, p0

    #@11b
    iget-object v0, v0, Lcom/android/internal/widget/ActionBarContextView;->mTitleLayout:Landroid/widget/LinearLayout;

    #@11d
    move-object/from16 v24, v0

    #@11f
    invoke-virtual/range {v24 .. v24}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    #@122
    move-result v19

    #@123
    .line 362
    .local v19, titleWidth:I
    move/from16 v0, v19

    #@125
    if-gt v0, v3, :cond_1cb

    #@127
    const/16 v18, 0x1

    #@129
    .line 363
    .local v18, titleFits:Z
    :goto_129
    if-eqz v18, :cond_12d

    #@12b
    .line 364
    sub-int v3, v3, v19

    #@12d
    .line 366
    :cond_12d
    move-object/from16 v0, p0

    #@12f
    iget-object v0, v0, Lcom/android/internal/widget/ActionBarContextView;->mTitleLayout:Landroid/widget/LinearLayout;

    #@131
    move-object/from16 v25, v0

    #@133
    if-eqz v18, :cond_1cf

    #@135
    const/16 v24, 0x0

    #@137
    :goto_137
    move-object/from16 v0, v25

    #@139
    move/from16 v1, v24

    #@13b
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    #@13e
    .line 372
    .end local v18           #titleFits:Z
    .end local v19           #titleWidth:I
    .end local v20           #titleWidthSpec:I
    :cond_13e
    :goto_13e
    move-object/from16 v0, p0

    #@140
    iget-object v0, v0, Lcom/android/internal/widget/ActionBarContextView;->mCustomView:Landroid/view/View;

    #@142
    move-object/from16 v24, v0

    #@144
    if-eqz v24, :cond_19d

    #@146
    .line 373
    move-object/from16 v0, p0

    #@148
    iget-object v0, v0, Lcom/android/internal/widget/ActionBarContextView;->mCustomView:Landroid/view/View;

    #@14a
    move-object/from16 v24, v0

    #@14c
    invoke-virtual/range {v24 .. v24}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@14f
    move-result-object v14

    #@150
    .line 374
    .local v14, lp:Landroid/view/ViewGroup$LayoutParams;
    iget v0, v14, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@152
    move/from16 v24, v0

    #@154
    const/16 v25, -0x2

    #@156
    move/from16 v0, v24

    #@158
    move/from16 v1, v25

    #@15a
    if-eq v0, v1, :cond_1e7

    #@15c
    const/high16 v10, 0x4000

    #@15e
    .line 376
    .local v10, customWidthMode:I
    :goto_15e
    iget v0, v14, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@160
    move/from16 v24, v0

    #@162
    if-ltz v24, :cond_1eb

    #@164
    iget v0, v14, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@166
    move/from16 v24, v0

    #@168
    move/from16 v0, v24

    #@16a
    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    #@16d
    move-result v9

    #@16e
    .line 378
    .local v9, customWidth:I
    :goto_16e
    iget v0, v14, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@170
    move/from16 v24, v0

    #@172
    const/16 v25, -0x2

    #@174
    move/from16 v0, v24

    #@176
    move/from16 v1, v25

    #@178
    if-eq v0, v1, :cond_1ed

    #@17a
    const/high16 v8, 0x4000

    #@17c
    .line 380
    .local v8, customHeightMode:I
    :goto_17c
    iget v0, v14, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@17e
    move/from16 v24, v0

    #@180
    if-ltz v24, :cond_1f0

    #@182
    iget v0, v14, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@184
    move/from16 v24, v0

    #@186
    move/from16 v0, v24

    #@188
    invoke-static {v0, v11}, Ljava/lang/Math;->min(II)I

    #@18b
    move-result v7

    #@18c
    .line 382
    .local v7, customHeight:I
    :goto_18c
    move-object/from16 v0, p0

    #@18e
    iget-object v0, v0, Lcom/android/internal/widget/ActionBarContextView;->mCustomView:Landroid/view/View;

    #@190
    move-object/from16 v24, v0

    #@192
    invoke-static {v9, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@195
    move-result v25

    #@196
    invoke-static {v7, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@199
    move-result v26

    #@19a
    invoke-virtual/range {v24 .. v26}, Landroid/view/View;->measure(II)V

    #@19d
    .line 386
    .end local v7           #customHeight:I
    .end local v8           #customHeightMode:I
    .end local v9           #customWidth:I
    .end local v10           #customWidthMode:I
    .end local v14           #lp:Landroid/view/ViewGroup$LayoutParams;
    :cond_19d
    move-object/from16 v0, p0

    #@19f
    iget v0, v0, Lcom/android/internal/widget/AbsActionBarView;->mContentHeight:I

    #@1a1
    move/from16 v24, v0

    #@1a3
    if-gtz v24, :cond_1fa

    #@1a5
    .line 387
    const/16 v16, 0x0

    #@1a7
    .line 388
    .local v16, measuredHeight:I
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/widget/ActionBarContextView;->getChildCount()I

    #@1aa
    move-result v6

    #@1ab
    .line 389
    .local v6, count:I
    const/4 v13, 0x0

    #@1ac
    .local v13, i:I
    :goto_1ac
    if-ge v13, v6, :cond_1f2

    #@1ae
    .line 390
    move-object/from16 v0, p0

    #@1b0
    invoke-virtual {v0, v13}, Lcom/android/internal/widget/ActionBarContextView;->getChildAt(I)Landroid/view/View;

    #@1b3
    move-result-object v21

    #@1b4
    .line 391
    .local v21, v:Landroid/view/View;
    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getMeasuredHeight()I

    #@1b7
    move-result v24

    #@1b8
    add-int v17, v24, v22

    #@1ba
    .line 392
    .local v17, paddedViewHeight:I
    move/from16 v0, v17

    #@1bc
    move/from16 v1, v16

    #@1be
    if-le v0, v1, :cond_1c2

    #@1c0
    .line 393
    move/from16 v16, v17

    #@1c2
    .line 389
    :cond_1c2
    add-int/lit8 v13, v13, 0x1

    #@1c4
    goto :goto_1ac

    #@1c5
    .line 338
    .end local v3           #availableWidth:I
    .end local v4           #childSpecHeight:I
    .end local v6           #count:I
    .end local v11           #height:I
    .end local v13           #i:I
    .end local v15           #maxHeight:I
    .end local v16           #measuredHeight:I
    .end local v17           #paddedViewHeight:I
    .end local v21           #v:Landroid/view/View;
    .end local v22           #verticalPadding:I
    :cond_1c5
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@1c8
    move-result v15

    #@1c9
    goto/16 :goto_70

    #@1cb
    .line 362
    .restart local v3       #availableWidth:I
    .restart local v4       #childSpecHeight:I
    .restart local v11       #height:I
    .restart local v15       #maxHeight:I
    .restart local v19       #titleWidth:I
    .restart local v20       #titleWidthSpec:I
    .restart local v22       #verticalPadding:I
    :cond_1cb
    const/16 v18, 0x0

    #@1cd
    goto/16 :goto_129

    #@1cf
    .line 366
    .restart local v18       #titleFits:Z
    :cond_1cf
    const/16 v24, 0x8

    #@1d1
    goto/16 :goto_137

    #@1d3
    .line 368
    .end local v18           #titleFits:Z
    .end local v19           #titleWidth:I
    .end local v20           #titleWidthSpec:I
    :cond_1d3
    move-object/from16 v0, p0

    #@1d5
    iget-object v0, v0, Lcom/android/internal/widget/ActionBarContextView;->mTitleLayout:Landroid/widget/LinearLayout;

    #@1d7
    move-object/from16 v24, v0

    #@1d9
    const/16 v25, 0x0

    #@1db
    move-object/from16 v0, p0

    #@1dd
    move-object/from16 v1, v24

    #@1df
    move/from16 v2, v25

    #@1e1
    invoke-virtual {v0, v1, v3, v4, v2}, Lcom/android/internal/widget/ActionBarContextView;->measureChildView(Landroid/view/View;III)I

    #@1e4
    move-result v3

    #@1e5
    goto/16 :goto_13e

    #@1e7
    .line 374
    .restart local v14       #lp:Landroid/view/ViewGroup$LayoutParams;
    :cond_1e7
    const/high16 v10, -0x8000

    #@1e9
    goto/16 :goto_15e

    #@1eb
    .restart local v10       #customWidthMode:I
    :cond_1eb
    move v9, v3

    #@1ec
    .line 376
    goto :goto_16e

    #@1ed
    .line 378
    .restart local v9       #customWidth:I
    :cond_1ed
    const/high16 v8, -0x8000

    #@1ef
    goto :goto_17c

    #@1f0
    .restart local v8       #customHeightMode:I
    :cond_1f0
    move v7, v11

    #@1f1
    .line 380
    goto :goto_18c

    #@1f2
    .line 396
    .end local v8           #customHeightMode:I
    .end local v9           #customWidth:I
    .end local v10           #customWidthMode:I
    .end local v14           #lp:Landroid/view/ViewGroup$LayoutParams;
    .restart local v6       #count:I
    .restart local v13       #i:I
    .restart local v16       #measuredHeight:I
    :cond_1f2
    move-object/from16 v0, p0

    #@1f4
    move/from16 v1, v16

    #@1f6
    invoke-virtual {v0, v5, v1}, Lcom/android/internal/widget/ActionBarContextView;->setMeasuredDimension(II)V

    #@1f9
    .line 400
    .end local v6           #count:I
    .end local v13           #i:I
    .end local v16           #measuredHeight:I
    :goto_1f9
    return-void

    #@1fa
    .line 398
    :cond_1fa
    move-object/from16 v0, p0

    #@1fc
    invoke-virtual {v0, v5, v15}, Lcom/android/internal/widget/ActionBarContextView;->setMeasuredDimension(II)V

    #@1ff
    goto :goto_1f9
.end method

.method public setContentHeight(I)V
    .registers 2
    .parameter "height"

    #@0
    .prologue
    .line 140
    iput p1, p0, Lcom/android/internal/widget/AbsActionBarView;->mContentHeight:I

    #@2
    .line 141
    return-void
.end method

.method public setCustomView(Landroid/view/View;)V
    .registers 3
    .parameter "view"

    #@0
    .prologue
    .line 144
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarContextView;->mCustomView:Landroid/view/View;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 145
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarContextView;->mCustomView:Landroid/view/View;

    #@6
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/ActionBarContextView;->removeView(Landroid/view/View;)V

    #@9
    .line 147
    :cond_9
    iput-object p1, p0, Lcom/android/internal/widget/ActionBarContextView;->mCustomView:Landroid/view/View;

    #@b
    .line 148
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarContextView;->mTitleLayout:Landroid/widget/LinearLayout;

    #@d
    if-eqz v0, :cond_17

    #@f
    .line 149
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarContextView;->mTitleLayout:Landroid/widget/LinearLayout;

    #@11
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/ActionBarContextView;->removeView(Landroid/view/View;)V

    #@14
    .line 150
    const/4 v0, 0x0

    #@15
    iput-object v0, p0, Lcom/android/internal/widget/ActionBarContextView;->mTitleLayout:Landroid/widget/LinearLayout;

    #@17
    .line 152
    :cond_17
    if-eqz p1, :cond_1c

    #@19
    .line 153
    invoke-virtual {p0, p1}, Lcom/android/internal/widget/ActionBarContextView;->addView(Landroid/view/View;)V

    #@1c
    .line 155
    :cond_1c
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarContextView;->requestLayout()V

    #@1f
    .line 156
    return-void
.end method

.method public setSplitActionBar(Z)V
    .registers 8
    .parameter "split"

    #@0
    .prologue
    const/4 v5, -0x1

    #@1
    .line 108
    iget-boolean v2, p0, Lcom/android/internal/widget/AbsActionBarView;->mSplitActionBar:Z

    #@3
    if-eq v2, p1, :cond_38

    #@5
    .line 109
    iget-object v2, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@7
    if-eqz v2, :cond_35

    #@9
    .line 111
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    #@b
    const/4 v2, -0x2

    #@c
    invoke-direct {v0, v2, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    #@f
    .line 113
    .local v0, layoutParams:Landroid/view/ViewGroup$LayoutParams;
    if-nez p1, :cond_39

    #@11
    .line 114
    iget-object v2, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@13
    invoke-virtual {v2, p0}, Lcom/android/internal/view/menu/ActionMenuPresenter;->getMenuView(Landroid/view/ViewGroup;)Lcom/android/internal/view/menu/MenuView;

    #@16
    move-result-object v2

    #@17
    check-cast v2, Lcom/android/internal/view/menu/ActionMenuView;

    #@19
    iput-object v2, p0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@1b
    .line 115
    iget-object v2, p0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@1d
    const/4 v3, 0x0

    #@1e
    invoke-virtual {v2, v3}, Lcom/android/internal/view/menu/ActionMenuView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    #@21
    .line 116
    iget-object v2, p0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@23
    invoke-virtual {v2}, Lcom/android/internal/view/menu/ActionMenuView;->getParent()Landroid/view/ViewParent;

    #@26
    move-result-object v1

    #@27
    check-cast v1, Landroid/view/ViewGroup;

    #@29
    .line 117
    .local v1, oldParent:Landroid/view/ViewGroup;
    if-eqz v1, :cond_30

    #@2b
    iget-object v2, p0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@2d
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    #@30
    .line 118
    :cond_30
    iget-object v2, p0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@32
    invoke-virtual {p0, v2, v0}, Lcom/android/internal/widget/ActionBarContextView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@35
    .line 135
    .end local v0           #layoutParams:Landroid/view/ViewGroup$LayoutParams;
    .end local v1           #oldParent:Landroid/view/ViewGroup;
    :cond_35
    :goto_35
    invoke-super {p0, p1}, Lcom/android/internal/widget/AbsActionBarView;->setSplitActionBar(Z)V

    #@38
    .line 137
    :cond_38
    return-void

    #@39
    .line 121
    .restart local v0       #layoutParams:Landroid/view/ViewGroup$LayoutParams;
    :cond_39
    iget-object v2, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@3b
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarContextView;->getContext()Landroid/content/Context;

    #@3e
    move-result-object v3

    #@3f
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@42
    move-result-object v3

    #@43
    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@46
    move-result-object v3

    #@47
    iget v3, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    #@49
    const/4 v4, 0x1

    #@4a
    invoke-virtual {v2, v3, v4}, Lcom/android/internal/view/menu/ActionMenuPresenter;->setWidthLimit(IZ)V

    #@4d
    .line 124
    iget-object v2, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@4f
    const v3, 0x7fffffff

    #@52
    invoke-virtual {v2, v3}, Lcom/android/internal/view/menu/ActionMenuPresenter;->setItemLimit(I)V

    #@55
    .line 126
    iput v5, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@57
    .line 127
    iget v2, p0, Lcom/android/internal/widget/AbsActionBarView;->mContentHeight:I

    #@59
    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@5b
    .line 128
    iget-object v2, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@5d
    invoke-virtual {v2, p0}, Lcom/android/internal/view/menu/ActionMenuPresenter;->getMenuView(Landroid/view/ViewGroup;)Lcom/android/internal/view/menu/MenuView;

    #@60
    move-result-object v2

    #@61
    check-cast v2, Lcom/android/internal/view/menu/ActionMenuView;

    #@63
    iput-object v2, p0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@65
    .line 129
    iget-object v2, p0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@67
    iget-object v3, p0, Lcom/android/internal/widget/ActionBarContextView;->mSplitBackground:Landroid/graphics/drawable/Drawable;

    #@69
    invoke-virtual {v2, v3}, Lcom/android/internal/view/menu/ActionMenuView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    #@6c
    .line 130
    iget-object v2, p0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@6e
    invoke-virtual {v2}, Lcom/android/internal/view/menu/ActionMenuView;->getParent()Landroid/view/ViewParent;

    #@71
    move-result-object v1

    #@72
    check-cast v1, Landroid/view/ViewGroup;

    #@74
    .line 131
    .restart local v1       #oldParent:Landroid/view/ViewGroup;
    if-eqz v1, :cond_7b

    #@76
    iget-object v2, p0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@78
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    #@7b
    .line 132
    :cond_7b
    iget-object v2, p0, Lcom/android/internal/widget/AbsActionBarView;->mSplitView:Lcom/android/internal/widget/ActionBarContainer;

    #@7d
    iget-object v3, p0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@7f
    invoke-virtual {v2, v3, v0}, Lcom/android/internal/widget/ActionBarContainer;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@82
    goto :goto_35
.end method

.method public setSubtitle(Ljava/lang/CharSequence;)V
    .registers 2
    .parameter "subtitle"

    #@0
    .prologue
    .line 164
    iput-object p1, p0, Lcom/android/internal/widget/ActionBarContextView;->mSubtitle:Ljava/lang/CharSequence;

    #@2
    .line 165
    invoke-direct {p0}, Lcom/android/internal/widget/ActionBarContextView;->initTitle()V

    #@5
    .line 166
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .registers 2
    .parameter "title"

    #@0
    .prologue
    .line 159
    iput-object p1, p0, Lcom/android/internal/widget/ActionBarContextView;->mTitle:Ljava/lang/CharSequence;

    #@2
    .line 160
    invoke-direct {p0}, Lcom/android/internal/widget/ActionBarContextView;->initTitle()V

    #@5
    .line 161
    return-void
.end method

.method public setTitleOptional(Z)V
    .registers 3
    .parameter "titleOptional"

    #@0
    .prologue
    .line 532
    iget-boolean v0, p0, Lcom/android/internal/widget/ActionBarContextView;->mTitleOptional:Z

    #@2
    if-eq p1, v0, :cond_7

    #@4
    .line 533
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarContextView;->requestLayout()V

    #@7
    .line 535
    :cond_7
    iput-boolean p1, p0, Lcom/android/internal/widget/ActionBarContextView;->mTitleOptional:Z

    #@9
    .line 536
    return-void
.end method

.method public shouldDelayChildPressedState()Z
    .registers 2

    #@0
    .prologue
    .line 515
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public showOverflowMenu()Z
    .registers 2

    #@0
    .prologue
    .line 288
    iget-object v0, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 289
    iget-object v0, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@6
    invoke-virtual {v0}, Lcom/android/internal/view/menu/ActionMenuPresenter;->showOverflowMenu()Z

    #@9
    move-result v0

    #@a
    .line 291
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method
