.class public Lcom/android/internal/widget/multiwaveview/MultiWaveView;
.super Landroid/view/View;
.source "MultiWaveView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;,
        Lcom/android/internal/widget/multiwaveview/MultiWaveView$OnTriggerListener;
    }
.end annotation


# static fields
.field private static final CHEVRON_ANIMATION_DURATION:I = 0x352

.field private static final CHEVRON_INCREMENTAL_DELAY:I = 0xa0

.field private static final DEBUG:Z = false

.field private static final HIDE_ANIMATION_DELAY:I = 0xc8

.field private static final HIDE_ANIMATION_DURATION:I = 0xc8

.field private static final INITIAL_SHOW_HANDLE_DURATION:I = 0xc8

.field private static final RETURN_TO_HOME_DELAY:I = 0x4b0

.field private static final RETURN_TO_HOME_DURATION:I = 0xc8

.field private static final RING_SCALE_COLLAPSED:F = 0.5f

.field private static final RING_SCALE_EXPANDED:F = 1.0f

.field private static final SHOW_ANIMATION_DELAY:I = 0x32

.field private static final SHOW_ANIMATION_DURATION:I = 0xc8

.field private static final SNAP_MARGIN_DEFAULT:F = 20.0f

.field private static final STATE_FINISH:I = 0x5

.field private static final STATE_FIRST_TOUCH:I = 0x2

.field private static final STATE_IDLE:I = 0x0

.field private static final STATE_SNAP:I = 0x4

.field private static final STATE_START:I = 0x1

.field private static final STATE_TRACKING:I = 0x3

.field private static final TAG:Ljava/lang/String; = "MultiWaveView"

.field private static final TAP_RADIUS_SCALE_ACCESSIBILITY_ENABLED:F = 1.3f

.field private static final TARGET_SCALE_COLLAPSED:F = 0.8f

.field private static final TARGET_SCALE_EXPANDED:F = 1.0f


# instance fields
.field private mActiveTarget:I

.field private mAlwaysTrackFinger:Z

.field private mAnimatingTargets:Z

.field private mBackgroundAnimator:Lcom/android/internal/widget/multiwaveview/Tweener;

.field private mChevronAnimationInterpolator:Landroid/animation/TimeInterpolator;

.field private mChevronAnimations:Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;

.field private mChevronDrawables:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/widget/multiwaveview/TargetDrawable;",
            ">;"
        }
    .end annotation
.end field

.field private mDirectionDescriptions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mDirectionDescriptionsResourceId:I

.field private mDragging:Z

.field private mFeedbackCount:I

.field private mGrabbedState:I

.field private mGravity:I

.field private mHandleAnimations:Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;

.field private mHandleDrawable:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

.field private mHorizontalInset:I

.field private mInitialLayout:Z

.field private mMaxTargetHeight:I

.field private mMaxTargetWidth:I

.field private mNewTargetResources:I

.field private mOnTriggerListener:Lcom/android/internal/widget/multiwaveview/MultiWaveView$OnTriggerListener;

.field private mOuterRadius:F

.field private mOuterRing:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

.field private mResetListener:Landroid/animation/Animator$AnimatorListener;

.field private mResetListenerWithPing:Landroid/animation/Animator$AnimatorListener;

.field private mSnapMargin:F

.field private mTapRadius:F

.field private mTargetAnimations:Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;

.field private mTargetDescriptions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mTargetDescriptionsResourceId:I

.field private mTargetDrawables:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/widget/multiwaveview/TargetDrawable;",
            ">;"
        }
    .end annotation
.end field

.field private mTargetResourceId:I

.field private mTargetUpdateListener:Landroid/animation/Animator$AnimatorListener;

.field private mUpdateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

.field private mVerticalInset:I

.field private mVibrationDuration:I

.field private mVibrator:Landroid/os/Vibrator;

.field private mWaveCenterX:F

.field private mWaveCenterY:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 207
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 208
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 13
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 211
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@3
    .line 98
    sget-object v8, Lcom/android/internal/widget/multiwaveview/Ease$Quad;->easeOut:Landroid/animation/TimeInterpolator;

    #@5
    iput-object v8, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mChevronAnimationInterpolator:Landroid/animation/TimeInterpolator;

    #@7
    .line 100
    new-instance v8, Ljava/util/ArrayList;

    #@9
    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    #@c
    iput-object v8, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    #@e
    .line 101
    new-instance v8, Ljava/util/ArrayList;

    #@10
    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    #@13
    iput-object v8, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mChevronDrawables:Ljava/util/ArrayList;

    #@15
    .line 102
    new-instance v8, Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;

    #@17
    const/4 v9, 0x0

    #@18
    invoke-direct {v8, p0, v9}, Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;-><init>(Lcom/android/internal/widget/multiwaveview/MultiWaveView;Lcom/android/internal/widget/multiwaveview/MultiWaveView$1;)V

    #@1b
    iput-object v8, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mChevronAnimations:Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;

    #@1d
    .line 103
    new-instance v8, Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;

    #@1f
    const/4 v9, 0x0

    #@20
    invoke-direct {v8, p0, v9}, Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;-><init>(Lcom/android/internal/widget/multiwaveview/MultiWaveView;Lcom/android/internal/widget/multiwaveview/MultiWaveView$1;)V

    #@23
    iput-object v8, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetAnimations:Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;

    #@25
    .line 104
    new-instance v8, Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;

    #@27
    const/4 v9, 0x0

    #@28
    invoke-direct {v8, p0, v9}, Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;-><init>(Lcom/android/internal/widget/multiwaveview/MultiWaveView;Lcom/android/internal/widget/multiwaveview/MultiWaveView$1;)V

    #@2b
    iput-object v8, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mHandleAnimations:Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;

    #@2d
    .line 112
    const/4 v8, 0x3

    #@2e
    iput v8, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mFeedbackCount:I

    #@30
    .line 113
    const/4 v8, 0x0

    #@31
    iput v8, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mVibrationDuration:I

    #@33
    .line 115
    const/4 v8, -0x1

    #@34
    iput v8, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mActiveTarget:I

    #@36
    .line 122
    const/4 v8, 0x0

    #@37
    iput v8, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mOuterRadius:F

    #@39
    .line 123
    const/4 v8, 0x0

    #@3a
    iput v8, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mSnapMargin:F

    #@3c
    .line 163
    new-instance v8, Lcom/android/internal/widget/multiwaveview/MultiWaveView$1;

    #@3e
    invoke-direct {v8, p0}, Lcom/android/internal/widget/multiwaveview/MultiWaveView$1;-><init>(Lcom/android/internal/widget/multiwaveview/MultiWaveView;)V

    #@41
    iput-object v8, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mResetListener:Landroid/animation/Animator$AnimatorListener;

    #@43
    .line 170
    new-instance v8, Lcom/android/internal/widget/multiwaveview/MultiWaveView$2;

    #@45
    invoke-direct {v8, p0}, Lcom/android/internal/widget/multiwaveview/MultiWaveView$2;-><init>(Lcom/android/internal/widget/multiwaveview/MultiWaveView;)V

    #@48
    iput-object v8, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mResetListenerWithPing:Landroid/animation/Animator$AnimatorListener;

    #@4a
    .line 178
    new-instance v8, Lcom/android/internal/widget/multiwaveview/MultiWaveView$3;

    #@4c
    invoke-direct {v8, p0}, Lcom/android/internal/widget/multiwaveview/MultiWaveView$3;-><init>(Lcom/android/internal/widget/multiwaveview/MultiWaveView;)V

    #@4f
    iput-object v8, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mUpdateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    #@51
    .line 186
    new-instance v8, Lcom/android/internal/widget/multiwaveview/MultiWaveView$4;

    #@53
    invoke-direct {v8, p0}, Lcom/android/internal/widget/multiwaveview/MultiWaveView$4;-><init>(Lcom/android/internal/widget/multiwaveview/MultiWaveView;)V

    #@56
    iput-object v8, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetUpdateListener:Landroid/animation/Animator$AnimatorListener;

    #@58
    .line 202
    const/16 v8, 0x30

    #@5a
    iput v8, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mGravity:I

    #@5c
    .line 203
    const/4 v8, 0x1

    #@5d
    iput-boolean v8, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mInitialLayout:Z

    #@5f
    .line 212
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@62
    move-result-object v6

    #@63
    .line 214
    .local v6, res:Landroid/content/res/Resources;
    sget-object v8, Lcom/android/internal/R$styleable;->MultiWaveView:[I

    #@65
    invoke-virtual {p1, p2, v8}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@68
    move-result-object v0

    #@69
    .line 215
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v8, 0x6

    #@6a
    iget v9, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mOuterRadius:F

    #@6c
    invoke-virtual {v0, v8, v9}, Landroid/content/res/TypedArray;->getDimension(IF)F

    #@6f
    move-result v8

    #@70
    iput v8, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mOuterRadius:F

    #@72
    .line 216
    const/16 v8, 0x8

    #@74
    iget v9, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mSnapMargin:F

    #@76
    invoke-virtual {v0, v8, v9}, Landroid/content/res/TypedArray;->getDimension(IF)F

    #@79
    move-result v8

    #@7a
    iput v8, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mSnapMargin:F

    #@7c
    .line 217
    const/4 v8, 0x7

    #@7d
    iget v9, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mVibrationDuration:I

    #@7f
    invoke-virtual {v0, v8, v9}, Landroid/content/res/TypedArray;->getInt(II)I

    #@82
    move-result v8

    #@83
    iput v8, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mVibrationDuration:I

    #@85
    .line 219
    const/16 v8, 0x9

    #@87
    iget v9, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mFeedbackCount:I

    #@89
    invoke-virtual {v0, v8, v9}, Landroid/content/res/TypedArray;->getInt(II)I

    #@8c
    move-result v8

    #@8d
    iput v8, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mFeedbackCount:I

    #@8f
    .line 221
    new-instance v8, Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@91
    const/4 v9, 0x3

    #@92
    invoke-virtual {v0, v9}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    #@95
    move-result-object v9

    #@96
    iget v9, v9, Landroid/util/TypedValue;->resourceId:I

    #@98
    invoke-direct {v8, v6, v9}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;-><init>(Landroid/content/res/Resources;I)V

    #@9b
    iput-object v8, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@9d
    .line 223
    iget-object v8, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@9f
    invoke-virtual {v8}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->getWidth()I

    #@a2
    move-result v8

    #@a3
    div-int/lit8 v8, v8, 0x2

    #@a5
    int-to-float v8, v8

    #@a6
    iput v8, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTapRadius:F

    #@a8
    .line 224
    new-instance v8, Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@aa
    const/4 v9, 0x5

    #@ab
    invoke-virtual {v0, v9}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    #@ae
    move-result-object v9

    #@af
    iget v9, v9, Landroid/util/TypedValue;->resourceId:I

    #@b1
    invoke-direct {v8, v6, v9}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;-><init>(Landroid/content/res/Resources;I)V

    #@b4
    iput-object v8, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mOuterRing:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@b6
    .line 226
    const/16 v8, 0xa

    #@b8
    const/4 v9, 0x0

    #@b9
    invoke-virtual {v0, v8, v9}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@bc
    move-result v8

    #@bd
    iput-boolean v8, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mAlwaysTrackFinger:Z

    #@bf
    .line 229
    new-instance v5, Landroid/util/TypedValue;

    #@c1
    invoke-direct {v5}, Landroid/util/TypedValue;-><init>()V

    #@c4
    .line 230
    .local v5, outValue:Landroid/util/TypedValue;
    const/4 v8, 0x4

    #@c5
    invoke-virtual {v0, v8, v5}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    #@c8
    move-result v8

    #@c9
    if-eqz v8, :cond_f7

    #@cb
    .line 231
    iget v8, v5, Landroid/util/TypedValue;->resourceId:I

    #@cd
    invoke-direct {p0, v8}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->loadDrawableArray(I)Ljava/util/ArrayList;

    #@d0
    move-result-object v2

    #@d1
    .line 232
    .local v2, chevrons:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/widget/multiwaveview/TargetDrawable;>;"
    const/4 v3, 0x0

    #@d2
    .local v3, i:I
    :goto_d2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@d5
    move-result v8

    #@d6
    if-ge v3, v8, :cond_f7

    #@d8
    .line 233
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@db
    move-result-object v1

    #@dc
    check-cast v1, Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@de
    .line 234
    .local v1, chevron:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    const/4 v4, 0x0

    #@df
    .local v4, k:I
    :goto_df
    iget v8, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mFeedbackCount:I

    #@e1
    if-ge v4, v8, :cond_f4

    #@e3
    .line 235
    iget-object v9, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mChevronDrawables:Ljava/util/ArrayList;

    #@e5
    if-nez v1, :cond_ee

    #@e7
    const/4 v8, 0x0

    #@e8
    :goto_e8
    invoke-virtual {v9, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@eb
    .line 234
    add-int/lit8 v4, v4, 0x1

    #@ed
    goto :goto_df

    #@ee
    .line 235
    :cond_ee
    new-instance v8, Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@f0
    invoke-direct {v8, v1}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;-><init>(Lcom/android/internal/widget/multiwaveview/TargetDrawable;)V

    #@f3
    goto :goto_e8

    #@f4
    .line 232
    :cond_f4
    add-int/lit8 v3, v3, 0x1

    #@f6
    goto :goto_d2

    #@f7
    .line 241
    .end local v1           #chevron:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    .end local v2           #chevrons:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/widget/multiwaveview/TargetDrawable;>;"
    .end local v3           #i:I
    .end local v4           #k:I
    :cond_f7
    const/4 v8, 0x2

    #@f8
    invoke-virtual {v0, v8, v5}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    #@fb
    move-result v8

    #@fc
    if-eqz v8, :cond_103

    #@fe
    .line 242
    iget v8, v5, Landroid/util/TypedValue;->resourceId:I

    #@100
    invoke-direct {p0, v8}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->internalSetTargetResources(I)V

    #@103
    .line 244
    :cond_103
    iget-object v8, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    #@105
    if-eqz v8, :cond_10f

    #@107
    iget-object v8, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    #@109
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    #@10c
    move-result v8

    #@10d
    if-nez v8, :cond_117

    #@10f
    .line 245
    :cond_10f
    new-instance v8, Ljava/lang/IllegalStateException;

    #@111
    const-string v9, "Must specify at least one target drawable"

    #@113
    invoke-direct {v8, v9}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@116
    throw v8

    #@117
    .line 249
    :cond_117
    const/4 v8, 0x0

    #@118
    invoke-virtual {v0, v8, v5}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    #@11b
    move-result v8

    #@11c
    if-eqz v8, :cond_12d

    #@11e
    .line 250
    iget v7, v5, Landroid/util/TypedValue;->resourceId:I

    #@120
    .line 251
    .local v7, resourceId:I
    if-nez v7, :cond_12a

    #@122
    .line 252
    new-instance v8, Ljava/lang/IllegalStateException;

    #@124
    const-string v9, "Must specify target descriptions"

    #@126
    invoke-direct {v8, v9}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@129
    throw v8

    #@12a
    .line 254
    :cond_12a
    invoke-virtual {p0, v7}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->setTargetDescriptionsResourceId(I)V

    #@12d
    .line 258
    .end local v7           #resourceId:I
    :cond_12d
    const/4 v8, 0x1

    #@12e
    invoke-virtual {v0, v8, v5}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    #@131
    move-result v8

    #@132
    if-eqz v8, :cond_143

    #@134
    .line 259
    iget v7, v5, Landroid/util/TypedValue;->resourceId:I

    #@136
    .line 260
    .restart local v7       #resourceId:I
    if-nez v7, :cond_140

    #@138
    .line 261
    new-instance v8, Ljava/lang/IllegalStateException;

    #@13a
    const-string v9, "Must specify direction descriptions"

    #@13c
    invoke-direct {v8, v9}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@13f
    throw v8

    #@140
    .line 263
    :cond_140
    invoke-virtual {p0, v7}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->setDirectionDescriptionsResourceId(I)V

    #@143
    .line 266
    .end local v7           #resourceId:I
    :cond_143
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@146
    .line 269
    sget-object v8, Landroid/R$styleable;->LinearLayout:[I

    #@148
    invoke-virtual {p1, p2, v8}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@14b
    move-result-object v0

    #@14c
    .line 270
    const/4 v8, 0x0

    #@14d
    const/16 v9, 0x30

    #@14f
    invoke-virtual {v0, v8, v9}, Landroid/content/res/TypedArray;->getInt(II)I

    #@152
    move-result v8

    #@153
    iput v8, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mGravity:I

    #@155
    .line 271
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@158
    .line 273
    iget v8, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mVibrationDuration:I

    #@15a
    if-lez v8, :cond_164

    #@15c
    const/4 v8, 0x1

    #@15d
    :goto_15d
    invoke-virtual {p0, v8}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->setVibrateEnabled(Z)V

    #@160
    .line 274
    invoke-direct {p0}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->assignDefaultsIfNeeded()V

    #@163
    .line 275
    return-void

    #@164
    .line 273
    :cond_164
    const/4 v8, 0x0

    #@165
    goto :goto_15d
.end method

.method static synthetic access$100(Lcom/android/internal/widget/multiwaveview/MultiWaveView;)F
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 56
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mWaveCenterX:F

    #@2
    return v0
.end method

.method static synthetic access$200(Lcom/android/internal/widget/multiwaveview/MultiWaveView;)F
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 56
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mWaveCenterY:F

    #@2
    return v0
.end method

.method static synthetic access$300(Lcom/android/internal/widget/multiwaveview/MultiWaveView;IFF)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 56
    invoke-direct {p0, p1, p2, p3}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->switchToState(IFF)V

    #@3
    return-void
.end method

.method static synthetic access$400(Lcom/android/internal/widget/multiwaveview/MultiWaveView;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 56
    invoke-direct {p0}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->dispatchOnFinishFinalAnimation()V

    #@3
    return-void
.end method

.method static synthetic access$500(Lcom/android/internal/widget/multiwaveview/MultiWaveView;)Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 56
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@2
    return-object v0
.end method

.method static synthetic access$600(Lcom/android/internal/widget/multiwaveview/MultiWaveView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 56
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mNewTargetResources:I

    #@2
    return v0
.end method

.method static synthetic access$602(Lcom/android/internal/widget/multiwaveview/MultiWaveView;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 56
    iput p1, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mNewTargetResources:I

    #@2
    return p1
.end method

.method static synthetic access$700(Lcom/android/internal/widget/multiwaveview/MultiWaveView;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->internalSetTargetResources(I)V

    #@3
    return-void
.end method

.method static synthetic access$800(Lcom/android/internal/widget/multiwaveview/MultiWaveView;ZZ)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->hideTargets(ZZ)V

    #@3
    return-void
.end method

.method static synthetic access$902(Lcom/android/internal/widget/multiwaveview/MultiWaveView;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 56
    iput-boolean p1, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mAnimatingTargets:Z

    #@2
    return p1
.end method

.method private activateHandle(IIFLandroid/animation/Animator$AnimatorListener;)V
    .registers 12
    .parameter "duration"
    .parameter "delay"
    .parameter "finalAlpha"
    .parameter "finishListener"

    #@0
    .prologue
    .line 383
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mHandleAnimations:Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;->cancel()V

    #@5
    .line 384
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mHandleAnimations:Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;

    #@7
    iget-object v1, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@9
    int-to-long v2, p1

    #@a
    const/16 v4, 0xa

    #@c
    new-array v4, v4, [Ljava/lang/Object;

    #@e
    const/4 v5, 0x0

    #@f
    const-string v6, "ease"

    #@11
    aput-object v6, v4, v5

    #@13
    const/4 v5, 0x1

    #@14
    sget-object v6, Lcom/android/internal/widget/multiwaveview/Ease$Cubic;->easeIn:Landroid/animation/TimeInterpolator;

    #@16
    aput-object v6, v4, v5

    #@18
    const/4 v5, 0x2

    #@19
    const-string v6, "delay"

    #@1b
    aput-object v6, v4, v5

    #@1d
    const/4 v5, 0x3

    #@1e
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@21
    move-result-object v6

    #@22
    aput-object v6, v4, v5

    #@24
    const/4 v5, 0x4

    #@25
    const-string v6, "alpha"

    #@27
    aput-object v6, v4, v5

    #@29
    const/4 v5, 0x5

    #@2a
    invoke-static {p3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@2d
    move-result-object v6

    #@2e
    aput-object v6, v4, v5

    #@30
    const/4 v5, 0x6

    #@31
    const-string v6, "onUpdate"

    #@33
    aput-object v6, v4, v5

    #@35
    const/4 v5, 0x7

    #@36
    iget-object v6, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mUpdateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    #@38
    aput-object v6, v4, v5

    #@3a
    const/16 v5, 0x8

    #@3c
    const-string v6, "onComplete"

    #@3e
    aput-object v6, v4, v5

    #@40
    const/16 v5, 0x9

    #@42
    aput-object p4, v4, v5

    #@44
    invoke-static {v1, v2, v3, v4}, Lcom/android/internal/widget/multiwaveview/Tweener;->to(Ljava/lang/Object;J[Ljava/lang/Object;)Lcom/android/internal/widget/multiwaveview/Tweener;

    #@47
    move-result-object v1

    #@48
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;->add(Ljava/lang/Object;)Z

    #@4b
    .line 390
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mHandleAnimations:Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;

    #@4d
    invoke-virtual {v0}, Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;->start()V

    #@50
    .line 391
    return-void
.end method

.method private announceTargets()V
    .registers 9

    #@0
    .prologue
    .line 1124
    new-instance v5, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 1125
    .local v5, utterance:Ljava/lang/StringBuilder;
    iget-object v6, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@a
    move-result v2

    #@b
    .line 1126
    .local v2, targetCount:I
    const/4 v1, 0x0

    #@c
    .local v1, i:I
    :goto_c
    if-ge v1, v2, :cond_3f

    #@e
    .line 1127
    invoke-direct {p0, v1}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->getTargetDescription(I)Ljava/lang/String;

    #@11
    move-result-object v3

    #@12
    .line 1128
    .local v3, targetDescription:Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->getDirectionDescription(I)Ljava/lang/String;

    #@15
    move-result-object v0

    #@16
    .line 1129
    .local v0, directionDescription:Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@19
    move-result v6

    #@1a
    if-nez v6, :cond_2f

    #@1c
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@1f
    move-result v6

    #@20
    if-nez v6, :cond_2f

    #@22
    .line 1131
    const/4 v6, 0x1

    #@23
    new-array v6, v6, [Ljava/lang/Object;

    #@25
    const/4 v7, 0x0

    #@26
    aput-object v3, v6, v7

    #@28
    invoke-static {v0, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@2b
    move-result-object v4

    #@2c
    .line 1132
    .local v4, text:Ljava/lang/String;
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    .line 1134
    .end local v4           #text:Ljava/lang/String;
    :cond_2f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    #@32
    move-result v6

    #@33
    if-lez v6, :cond_3c

    #@35
    .line 1135
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v6

    #@39
    invoke-direct {p0, v6}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->announceText(Ljava/lang/String;)V

    #@3c
    .line 1126
    :cond_3c
    add-int/lit8 v1, v1, 0x1

    #@3e
    goto :goto_c

    #@3f
    .line 1138
    .end local v0           #directionDescription:Ljava/lang/String;
    .end local v3           #targetDescription:Ljava/lang/String;
    :cond_3f
    return-void
.end method

.method private announceText(Ljava/lang/String;)V
    .registers 3
    .parameter "text"

    #@0
    .prologue
    .line 1141
    invoke-virtual {p0, p1}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->setContentDescription(Ljava/lang/CharSequence;)V

    #@3
    .line 1142
    const/16 v0, 0x8

    #@5
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->sendAccessibilityEvent(I)V

    #@8
    .line 1143
    const/4 v0, 0x0

    #@9
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->setContentDescription(Ljava/lang/CharSequence;)V

    #@c
    .line 1144
    return-void
.end method

.method private assignDefaultsIfNeeded()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 967
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mOuterRadius:F

    #@3
    cmpl-float v0, v0, v2

    #@5
    if-nez v0, :cond_1d

    #@7
    .line 968
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mOuterRing:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@9
    invoke-virtual {v0}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->getWidth()I

    #@c
    move-result v0

    #@d
    iget-object v1, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mOuterRing:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@f
    invoke-virtual {v1}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->getHeight()I

    #@12
    move-result v1

    #@13
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@16
    move-result v0

    #@17
    int-to-float v0, v0

    #@18
    const/high16 v1, 0x4000

    #@1a
    div-float/2addr v0, v1

    #@1b
    iput v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mOuterRadius:F

    #@1d
    .line 970
    :cond_1d
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mSnapMargin:F

    #@1f
    cmpl-float v0, v0, v2

    #@21
    if-nez v0, :cond_38

    #@23
    .line 971
    const/4 v0, 0x1

    #@24
    const/high16 v1, 0x41a0

    #@26
    invoke-virtual {p0}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->getContext()Landroid/content/Context;

    #@29
    move-result-object v2

    #@2a
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@2d
    move-result-object v2

    #@2e
    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@31
    move-result-object v2

    #@32
    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    #@35
    move-result v0

    #@36
    iput v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mSnapMargin:F

    #@38
    .line 974
    :cond_38
    return-void
.end method

.method private computeInsets(II)V
    .registers 7
    .parameter "dx"
    .parameter "dy"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 977
    invoke-virtual {p0}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->getLayoutDirection()I

    #@4
    move-result v1

    #@5
    .line 978
    .local v1, layoutDirection:I
    iget v2, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mGravity:I

    #@7
    invoke-static {v2, v1}, Landroid/view/Gravity;->getAbsoluteGravity(II)I

    #@a
    move-result v0

    #@b
    .line 980
    .local v0, absoluteGravity:I
    and-int/lit8 v2, v0, 0x7

    #@d
    packed-switch v2, :pswitch_data_2a

    #@10
    .line 989
    :pswitch_10
    div-int/lit8 v2, p1, 0x2

    #@12
    iput v2, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mHorizontalInset:I

    #@14
    .line 992
    :goto_14
    and-int/lit8 v2, v0, 0x70

    #@16
    sparse-switch v2, :sswitch_data_34

    #@19
    .line 1001
    div-int/lit8 v2, p2, 0x2

    #@1b
    iput v2, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mVerticalInset:I

    #@1d
    .line 1004
    :goto_1d
    return-void

    #@1e
    .line 982
    :pswitch_1e
    iput v3, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mHorizontalInset:I

    #@20
    goto :goto_14

    #@21
    .line 985
    :pswitch_21
    iput p1, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mHorizontalInset:I

    #@23
    goto :goto_14

    #@24
    .line 994
    :sswitch_24
    iput v3, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mVerticalInset:I

    #@26
    goto :goto_1d

    #@27
    .line 997
    :sswitch_27
    iput p2, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mVerticalInset:I

    #@29
    goto :goto_1d

    #@2a
    .line 980
    :pswitch_data_2a
    .packed-switch 0x3
        :pswitch_1e
        :pswitch_10
        :pswitch_21
    .end packed-switch

    #@34
    .line 992
    :sswitch_data_34
    .sparse-switch
        0x30 -> :sswitch_24
        0x50 -> :sswitch_27
    .end sparse-switch
.end method

.method private deactivateHandle(IIFLandroid/animation/Animator$AnimatorListener;)V
    .registers 13
    .parameter "duration"
    .parameter "delay"
    .parameter "finalAlpha"
    .parameter "finishListener"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 395
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mHandleAnimations:Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;

    #@3
    invoke-virtual {v0}, Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;->cancel()V

    #@6
    .line 396
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mHandleAnimations:Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;

    #@8
    iget-object v1, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@a
    int-to-long v2, p1

    #@b
    const/16 v4, 0xe

    #@d
    new-array v4, v4, [Ljava/lang/Object;

    #@f
    const-string v5, "ease"

    #@11
    aput-object v5, v4, v7

    #@13
    const/4 v5, 0x1

    #@14
    sget-object v6, Lcom/android/internal/widget/multiwaveview/Ease$Quart;->easeOut:Landroid/animation/TimeInterpolator;

    #@16
    aput-object v6, v4, v5

    #@18
    const/4 v5, 0x2

    #@19
    const-string v6, "delay"

    #@1b
    aput-object v6, v4, v5

    #@1d
    const/4 v5, 0x3

    #@1e
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@21
    move-result-object v6

    #@22
    aput-object v6, v4, v5

    #@24
    const/4 v5, 0x4

    #@25
    const-string v6, "alpha"

    #@27
    aput-object v6, v4, v5

    #@29
    const/4 v5, 0x5

    #@2a
    invoke-static {p3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@2d
    move-result-object v6

    #@2e
    aput-object v6, v4, v5

    #@30
    const/4 v5, 0x6

    #@31
    const-string v6, "x"

    #@33
    aput-object v6, v4, v5

    #@35
    const/4 v5, 0x7

    #@36
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@39
    move-result-object v6

    #@3a
    aput-object v6, v4, v5

    #@3c
    const/16 v5, 0x8

    #@3e
    const-string v6, "y"

    #@40
    aput-object v6, v4, v5

    #@42
    const/16 v5, 0x9

    #@44
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@47
    move-result-object v6

    #@48
    aput-object v6, v4, v5

    #@4a
    const/16 v5, 0xa

    #@4c
    const-string v6, "onUpdate"

    #@4e
    aput-object v6, v4, v5

    #@50
    const/16 v5, 0xb

    #@52
    iget-object v6, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mUpdateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    #@54
    aput-object v6, v4, v5

    #@56
    const/16 v5, 0xc

    #@58
    const-string v6, "onComplete"

    #@5a
    aput-object v6, v4, v5

    #@5c
    const/16 v5, 0xd

    #@5e
    aput-object p4, v4, v5

    #@60
    invoke-static {v1, v2, v3, v4}, Lcom/android/internal/widget/multiwaveview/Tweener;->to(Ljava/lang/Object;J[Ljava/lang/Object;)Lcom/android/internal/widget/multiwaveview/Tweener;

    #@63
    move-result-object v1

    #@64
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;->add(Ljava/lang/Object;)Z

    #@67
    .line 404
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mHandleAnimations:Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;

    #@69
    invoke-virtual {v0}, Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;->start()V

    #@6c
    .line 405
    return-void
.end method

.method private deactivateTargets()V
    .registers 5

    #@0
    .prologue
    .line 452
    iget-object v3, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v0

    #@6
    .line 453
    .local v0, count:I
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    if-ge v1, v0, :cond_19

    #@9
    .line 454
    iget-object v3, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v2

    #@f
    check-cast v2, Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@11
    .line 455
    .local v2, target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    sget-object v3, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->STATE_INACTIVE:[I

    #@13
    invoke-virtual {v2, v3}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->setState([I)V

    #@16
    .line 453
    add-int/lit8 v1, v1, 0x1

    #@18
    goto :goto_7

    #@19
    .line 457
    .end local v2           #target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    :cond_19
    const/4 v3, -0x1

    #@1a
    iput v3, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mActiveTarget:I

    #@1c
    .line 458
    return-void
.end method

.method private dispatchOnFinishFinalAnimation()V
    .registers 2

    #@0
    .prologue
    .line 488
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mOnTriggerListener:Lcom/android/internal/widget/multiwaveview/MultiWaveView$OnTriggerListener;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 489
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mOnTriggerListener:Lcom/android/internal/widget/multiwaveview/MultiWaveView$OnTriggerListener;

    #@6
    invoke-interface {v0}, Lcom/android/internal/widget/multiwaveview/MultiWaveView$OnTriggerListener;->onFinishFinalAnimation()V

    #@9
    .line 491
    :cond_9
    return-void
.end method

.method private dispatchTriggerEvent(I)V
    .registers 3
    .parameter "whichTarget"

    #@0
    .prologue
    .line 481
    invoke-direct {p0}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->vibrate()V

    #@3
    .line 482
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mOnTriggerListener:Lcom/android/internal/widget/multiwaveview/MultiWaveView$OnTriggerListener;

    #@5
    if-eqz v0, :cond_c

    #@7
    .line 483
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mOnTriggerListener:Lcom/android/internal/widget/multiwaveview/MultiWaveView$OnTriggerListener;

    #@9
    invoke-interface {v0, p0, p1}, Lcom/android/internal/widget/multiwaveview/MultiWaveView$OnTriggerListener;->onTrigger(Landroid/view/View;I)V

    #@c
    .line 485
    :cond_c
    return-void
.end method

.method private dist2(FF)F
    .registers 5
    .parameter "dx"
    .parameter "dy"

    #@0
    .prologue
    .line 1110
    mul-float v0, p1, p1

    #@2
    mul-float v1, p2, p2

    #@4
    add-float/2addr v0, v1

    #@5
    return v0
.end method

.method private doFinish()V
    .registers 8

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/16 v6, 0xc8

    #@3
    const/4 v3, 0x0

    #@4
    .line 494
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mActiveTarget:I

    #@6
    .line 495
    .local v0, activeTarget:I
    const/4 v4, -0x1

    #@7
    if-eq v0, v4, :cond_27

    #@9
    move v1, v2

    #@a
    .line 497
    .local v1, targetHit:Z
    :goto_a
    if-eqz v1, :cond_29

    #@c
    .line 500
    invoke-direct {p0, v0}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->highlightSelected(I)V

    #@f
    .line 503
    const/16 v2, 0x4b0

    #@11
    const/4 v4, 0x0

    #@12
    iget-object v5, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mResetListener:Landroid/animation/Animator$AnimatorListener;

    #@14
    invoke-direct {p0, v6, v2, v4, v5}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->deactivateHandle(IIFLandroid/animation/Animator$AnimatorListener;)V

    #@17
    .line 504
    invoke-direct {p0, v0}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->dispatchTriggerEvent(I)V

    #@1a
    .line 505
    iget-boolean v2, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mAlwaysTrackFinger:Z

    #@1c
    if-nez v2, :cond_23

    #@1e
    .line 507
    iget-object v2, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetAnimations:Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;

    #@20
    invoke-virtual {v2}, Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;->stop()V

    #@23
    .line 516
    :cond_23
    :goto_23
    invoke-direct {p0, v3}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->setGrabbedState(I)V

    #@26
    .line 517
    return-void

    #@27
    .end local v1           #targetHit:Z
    :cond_27
    move v1, v3

    #@28
    .line 495
    goto :goto_a

    #@29
    .line 511
    .restart local v1       #targetHit:Z
    :cond_29
    const/high16 v4, 0x3f80

    #@2b
    iget-object v5, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mResetListenerWithPing:Landroid/animation/Animator$AnimatorListener;

    #@2d
    invoke-direct {p0, v6, v6, v4, v5}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->deactivateHandle(IIFLandroid/animation/Animator$AnimatorListener;)V

    #@30
    .line 513
    invoke-direct {p0, v2, v3}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->hideTargets(ZZ)V

    #@33
    goto :goto_23
.end method

.method private dump()V
    .registers 4

    #@0
    .prologue
    .line 278
    const-string v0, "MultiWaveView"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "Outer Radius = "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget v2, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mOuterRadius:F

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 279
    const-string v0, "MultiWaveView"

    #@1c
    new-instance v1, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v2, "SnapMargin = "

    #@23
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v1

    #@27
    iget v2, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mSnapMargin:F

    #@29
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v1

    #@2d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v1

    #@31
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    .line 280
    const-string v0, "MultiWaveView"

    #@36
    new-instance v1, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v2, "FeedbackCount = "

    #@3d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v1

    #@41
    iget v2, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mFeedbackCount:I

    #@43
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@46
    move-result-object v1

    #@47
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v1

    #@4b
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    .line 281
    const-string v0, "MultiWaveView"

    #@50
    new-instance v1, Ljava/lang/StringBuilder;

    #@52
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@55
    const-string v2, "VibrationDuration = "

    #@57
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v1

    #@5b
    iget v2, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mVibrationDuration:I

    #@5d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@60
    move-result-object v1

    #@61
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v1

    #@65
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@68
    .line 282
    const-string v0, "MultiWaveView"

    #@6a
    new-instance v1, Ljava/lang/StringBuilder;

    #@6c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@6f
    const-string v2, "TapRadius = "

    #@71
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v1

    #@75
    iget v2, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTapRadius:F

    #@77
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v1

    #@7b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7e
    move-result-object v1

    #@7f
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@82
    .line 283
    const-string v0, "MultiWaveView"

    #@84
    new-instance v1, Ljava/lang/StringBuilder;

    #@86
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@89
    const-string v2, "WaveCenterX = "

    #@8b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v1

    #@8f
    iget v2, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mWaveCenterX:F

    #@91
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@94
    move-result-object v1

    #@95
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@98
    move-result-object v1

    #@99
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@9c
    .line 284
    const-string v0, "MultiWaveView"

    #@9e
    new-instance v1, Ljava/lang/StringBuilder;

    #@a0
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a3
    const-string v2, "WaveCenterY = "

    #@a5
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v1

    #@a9
    iget v2, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mWaveCenterY:F

    #@ab
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v1

    #@af
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b2
    move-result-object v1

    #@b3
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b6
    .line 285
    return-void
.end method

.method private getDirectionDescription(I)Ljava/lang/String;
    .registers 4
    .parameter "index"

    #@0
    .prologue
    .line 1159
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mDirectionDescriptions:Ljava/util/ArrayList;

    #@2
    if-eqz v0, :cond_c

    #@4
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mDirectionDescriptions:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_2b

    #@c
    .line 1160
    :cond_c
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mDirectionDescriptionsResourceId:I

    #@e
    invoke-direct {p0, v0}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->loadDescriptions(I)Ljava/util/ArrayList;

    #@11
    move-result-object v0

    #@12
    iput-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mDirectionDescriptions:Ljava/util/ArrayList;

    #@14
    .line 1161
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    #@16
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@19
    move-result v0

    #@1a
    iget-object v1, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mDirectionDescriptions:Ljava/util/ArrayList;

    #@1c
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@1f
    move-result v1

    #@20
    if-eq v0, v1, :cond_2b

    #@22
    .line 1162
    const-string v0, "MultiWaveView"

    #@24
    const-string v1, "The number of target drawables must be euqal to the number of direction descriptions."

    #@26
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 1164
    const/4 v0, 0x0

    #@2a
    .line 1167
    :goto_2a
    return-object v0

    #@2b
    :cond_2b
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mDirectionDescriptions:Ljava/util/ArrayList;

    #@2d
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@30
    move-result-object v0

    #@31
    check-cast v0, Ljava/lang/String;

    #@33
    goto :goto_2a
.end method

.method private getScaledTapRadiusSquared()F
    .registers 4

    #@0
    .prologue
    .line 1115
    iget-object v1, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mContext:Landroid/content/Context;

    #@2
    invoke-static {v1}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_18

    #@c
    .line 1116
    const v1, 0x3fa66666

    #@f
    iget v2, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTapRadius:F

    #@11
    mul-float v0, v1, v2

    #@13
    .line 1120
    .local v0, scaledTapRadius:F
    :goto_13
    invoke-direct {p0, v0}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->square(F)F

    #@16
    move-result v1

    #@17
    return v1

    #@18
    .line 1118
    .end local v0           #scaledTapRadius:F
    :cond_18
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTapRadius:F

    #@1a
    .restart local v0       #scaledTapRadius:F
    goto :goto_13
.end method

.method private getTargetDescription(I)Ljava/lang/String;
    .registers 4
    .parameter "index"

    #@0
    .prologue
    .line 1147
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetDescriptions:Ljava/util/ArrayList;

    #@2
    if-eqz v0, :cond_c

    #@4
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetDescriptions:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_2b

    #@c
    .line 1148
    :cond_c
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetDescriptionsResourceId:I

    #@e
    invoke-direct {p0, v0}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->loadDescriptions(I)Ljava/util/ArrayList;

    #@11
    move-result-object v0

    #@12
    iput-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetDescriptions:Ljava/util/ArrayList;

    #@14
    .line 1149
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    #@16
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@19
    move-result v0

    #@1a
    iget-object v1, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetDescriptions:Ljava/util/ArrayList;

    #@1c
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@1f
    move-result v1

    #@20
    if-eq v0, v1, :cond_2b

    #@22
    .line 1150
    const-string v0, "MultiWaveView"

    #@24
    const-string v1, "The number of target drawables must be euqal to the number of target descriptions."

    #@26
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 1152
    const/4 v0, 0x0

    #@2a
    .line 1155
    :goto_2a
    return-object v0

    #@2b
    :cond_2b
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetDescriptions:Ljava/util/ArrayList;

    #@2d
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@30
    move-result-object v0

    #@31
    check-cast v0, Ljava/lang/String;

    #@33
    goto :goto_2a
.end method

.method private handleCancel(Landroid/view/MotionEvent;)V
    .registers 5
    .parameter "event"

    #@0
    .prologue
    .line 819
    const/4 v0, 0x5

    #@1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@4
    move-result v1

    #@5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@8
    move-result v2

    #@9
    invoke-direct {p0, v0, v1, v2}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->switchToState(IFF)V

    #@c
    .line 820
    return-void
.end method

.method private handleDown(Landroid/view/MotionEvent;)V
    .registers 5
    .parameter "event"

    #@0
    .prologue
    .line 795
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@3
    move-result v0

    #@4
    .line 796
    .local v0, eventX:F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@7
    move-result v1

    #@8
    .line 797
    .local v1, eventY:F
    const/4 v2, 0x1

    #@9
    invoke-direct {p0, v2, v0, v1}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->switchToState(IFF)V

    #@c
    .line 798
    invoke-direct {p0, v0, v1}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->trySwitchToFirstTouchState(FF)Z

    #@f
    move-result v2

    #@10
    if-nez v2, :cond_18

    #@12
    .line 799
    const/4 v2, 0x0

    #@13
    iput-boolean v2, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mDragging:Z

    #@15
    .line 800
    invoke-virtual {p0}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->ping()V

    #@18
    .line 802
    :cond_18
    return-void
.end method

.method private handleMove(Landroid/view/MotionEvent;)V
    .registers 38
    .parameter "event"

    #@0
    .prologue
    .line 823
    const/4 v5, -0x1

    #@1
    .line 824
    .local v5, activeTarget:I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getHistorySize()I

    #@4
    move-result v11

    #@5
    .line 825
    .local v11, historySize:I
    move-object/from16 v0, p0

    #@7
    iget-object v0, v0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    #@9
    move-object/from16 v26, v0

    #@b
    .line 826
    .local v26, targets:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/widget/multiwaveview/TargetDrawable;>;"
    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->size()I

    #@e
    move-result v16

    #@f
    .line 827
    .local v16, ntargets:I
    const/16 v30, 0x0

    #@11
    .line 828
    .local v30, x:F
    const/16 v31, 0x0

    #@13
    .line 829
    .local v31, y:F
    const/4 v13, 0x0

    #@14
    .local v13, k:I
    :goto_14
    add-int/lit8 v32, v11, 0x1

    #@16
    move/from16 v0, v32

    #@18
    if-ge v13, v0, :cond_132

    #@1a
    .line 830
    if-ge v13, v11, :cond_118

    #@1c
    move-object/from16 v0, p1

    #@1e
    invoke-virtual {v0, v13}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    #@21
    move-result v9

    #@22
    .line 831
    .local v9, eventX:F
    :goto_22
    if-ge v13, v11, :cond_11e

    #@24
    move-object/from16 v0, p1

    #@26
    invoke-virtual {v0, v13}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    #@29
    move-result v10

    #@2a
    .line 833
    .local v10, eventY:F
    :goto_2a
    move-object/from16 v0, p0

    #@2c
    iget v0, v0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mWaveCenterX:F

    #@2e
    move/from16 v32, v0

    #@30
    sub-float v28, v9, v32

    #@32
    .line 834
    .local v28, tx:F
    move-object/from16 v0, p0

    #@34
    iget v0, v0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mWaveCenterY:F

    #@36
    move/from16 v32, v0

    #@38
    sub-float v29, v10, v32

    #@3a
    .line 835
    .local v29, ty:F
    move-object/from16 v0, p0

    #@3c
    move/from16 v1, v28

    #@3e
    move/from16 v2, v29

    #@40
    invoke-direct {v0, v1, v2}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->dist2(FF)F

    #@43
    move-result v32

    #@44
    move/from16 v0, v32

    #@46
    float-to-double v0, v0

    #@47
    move-wide/from16 v32, v0

    #@49
    invoke-static/range {v32 .. v33}, Ljava/lang/Math;->sqrt(D)D

    #@4c
    move-result-wide v32

    #@4d
    move-wide/from16 v0, v32

    #@4f
    double-to-float v0, v0

    #@50
    move/from16 v27, v0

    #@52
    .line 836
    .local v27, touchRadius:F
    move-object/from16 v0, p0

    #@54
    iget v0, v0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mOuterRadius:F

    #@56
    move/from16 v32, v0

    #@58
    cmpl-float v32, v27, v32

    #@5a
    if-lez v32, :cond_124

    #@5c
    move-object/from16 v0, p0

    #@5e
    iget v0, v0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mOuterRadius:F

    #@60
    move/from16 v32, v0

    #@62
    div-float v17, v32, v27

    #@64
    .line 837
    .local v17, scale:F
    :goto_64
    mul-float v14, v28, v17

    #@66
    .line 838
    .local v14, limitX:F
    mul-float v15, v29, v17

    #@68
    .line 839
    .local v15, limitY:F
    move/from16 v0, v29

    #@6a
    neg-float v0, v0

    #@6b
    move/from16 v32, v0

    #@6d
    move/from16 v0, v32

    #@6f
    float-to-double v0, v0

    #@70
    move-wide/from16 v32, v0

    #@72
    move/from16 v0, v28

    #@74
    float-to-double v0, v0

    #@75
    move-wide/from16 v34, v0

    #@77
    invoke-static/range {v32 .. v35}, Ljava/lang/Math;->atan2(DD)D

    #@7a
    move-result-wide v7

    #@7b
    .line 841
    .local v7, angleRad:D
    move-object/from16 v0, p0

    #@7d
    iget-boolean v0, v0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mDragging:Z

    #@7f
    move/from16 v32, v0

    #@81
    if-nez v32, :cond_88

    #@83
    .line 842
    move-object/from16 v0, p0

    #@85
    invoke-direct {v0, v9, v10}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->trySwitchToFirstTouchState(FF)Z

    #@88
    .line 845
    :cond_88
    move-object/from16 v0, p0

    #@8a
    iget-boolean v0, v0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mDragging:Z

    #@8c
    move/from16 v32, v0

    #@8e
    if-eqz v32, :cond_12a

    #@90
    .line 847
    move-object/from16 v0, p0

    #@92
    iget v0, v0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mOuterRadius:F

    #@94
    move/from16 v32, v0

    #@96
    move-object/from16 v0, p0

    #@98
    iget v0, v0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mSnapMargin:F

    #@9a
    move/from16 v33, v0

    #@9c
    sub-float v19, v32, v33

    #@9e
    .line 848
    .local v19, snapRadius:F
    mul-float v18, v19, v19

    #@a0
    .line 850
    .local v18, snapDistance2:F
    const/4 v12, 0x0

    #@a1
    .local v12, i:I
    :goto_a1
    move/from16 v0, v16

    #@a3
    if-ge v12, v0, :cond_12a

    #@a5
    .line 851
    move-object/from16 v0, v26

    #@a7
    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@aa
    move-result-object v20

    #@ab
    check-cast v20, Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@ad
    .line 853
    .local v20, target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    int-to-double v0, v12

    #@ae
    move-wide/from16 v32, v0

    #@b0
    const-wide/high16 v34, 0x3fe0

    #@b2
    sub-double v32, v32, v34

    #@b4
    const-wide/high16 v34, 0x4000

    #@b6
    mul-double v32, v32, v34

    #@b8
    const-wide v34, 0x400921fb54442d18L

    #@bd
    mul-double v32, v32, v34

    #@bf
    move/from16 v0, v16

    #@c1
    int-to-double v0, v0

    #@c2
    move-wide/from16 v34, v0

    #@c4
    div-double v24, v32, v34

    #@c6
    .line 854
    .local v24, targetMinRad:D
    int-to-double v0, v12

    #@c7
    move-wide/from16 v32, v0

    #@c9
    const-wide/high16 v34, 0x3fe0

    #@cb
    add-double v32, v32, v34

    #@cd
    const-wide/high16 v34, 0x4000

    #@cf
    mul-double v32, v32, v34

    #@d1
    const-wide v34, 0x400921fb54442d18L

    #@d6
    mul-double v32, v32, v34

    #@d8
    move/from16 v0, v16

    #@da
    int-to-double v0, v0

    #@db
    move-wide/from16 v34, v0

    #@dd
    div-double v22, v32, v34

    #@df
    .line 855
    .local v22, targetMaxRad:D
    invoke-virtual/range {v20 .. v20}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->isEnabled()Z

    #@e2
    move-result v32

    #@e3
    if-eqz v32, :cond_115

    #@e5
    .line 856
    cmpl-double v32, v7, v24

    #@e7
    if-lez v32, :cond_ed

    #@e9
    cmpg-double v32, v7, v22

    #@eb
    if-lez v32, :cond_103

    #@ed
    :cond_ed
    const-wide v32, 0x401921fb54442d18L

    #@f2
    add-double v32, v32, v7

    #@f4
    cmpl-double v32, v32, v24

    #@f6
    if-lez v32, :cond_128

    #@f8
    const-wide v32, 0x401921fb54442d18L

    #@fd
    add-double v32, v32, v7

    #@ff
    cmpg-double v32, v32, v22

    #@101
    if-gtz v32, :cond_128

    #@103
    :cond_103
    const/4 v6, 0x1

    #@104
    .line 860
    .local v6, angleMatches:Z
    :goto_104
    if-eqz v6, :cond_115

    #@106
    move-object/from16 v0, p0

    #@108
    move/from16 v1, v28

    #@10a
    move/from16 v2, v29

    #@10c
    invoke-direct {v0, v1, v2}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->dist2(FF)F

    #@10f
    move-result v32

    #@110
    cmpl-float v32, v32, v18

    #@112
    if-lez v32, :cond_115

    #@114
    .line 861
    move v5, v12

    #@115
    .line 850
    .end local v6           #angleMatches:Z
    :cond_115
    add-int/lit8 v12, v12, 0x1

    #@117
    goto :goto_a1

    #@118
    .line 830
    .end local v7           #angleRad:D
    .end local v9           #eventX:F
    .end local v10           #eventY:F
    .end local v12           #i:I
    .end local v14           #limitX:F
    .end local v15           #limitY:F
    .end local v17           #scale:F
    .end local v18           #snapDistance2:F
    .end local v19           #snapRadius:F
    .end local v20           #target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    .end local v22           #targetMaxRad:D
    .end local v24           #targetMinRad:D
    .end local v27           #touchRadius:F
    .end local v28           #tx:F
    .end local v29           #ty:F
    :cond_118
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    #@11b
    move-result v9

    #@11c
    goto/16 :goto_22

    #@11e
    .line 831
    .restart local v9       #eventX:F
    :cond_11e
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    #@121
    move-result v10

    #@122
    goto/16 :goto_2a

    #@124
    .line 836
    .restart local v10       #eventY:F
    .restart local v27       #touchRadius:F
    .restart local v28       #tx:F
    .restart local v29       #ty:F
    :cond_124
    const/high16 v17, 0x3f80

    #@126
    goto/16 :goto_64

    #@128
    .line 856
    .restart local v7       #angleRad:D
    .restart local v12       #i:I
    .restart local v14       #limitX:F
    .restart local v15       #limitY:F
    .restart local v17       #scale:F
    .restart local v18       #snapDistance2:F
    .restart local v19       #snapRadius:F
    .restart local v20       #target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    .restart local v22       #targetMaxRad:D
    .restart local v24       #targetMinRad:D
    :cond_128
    const/4 v6, 0x0

    #@129
    goto :goto_104

    #@12a
    .line 866
    .end local v12           #i:I
    .end local v18           #snapDistance2:F
    .end local v19           #snapRadius:F
    .end local v20           #target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    .end local v22           #targetMaxRad:D
    .end local v24           #targetMinRad:D
    :cond_12a
    move/from16 v30, v14

    #@12c
    .line 867
    move/from16 v31, v15

    #@12e
    .line 829
    add-int/lit8 v13, v13, 0x1

    #@130
    goto/16 :goto_14

    #@132
    .line 870
    .end local v7           #angleRad:D
    .end local v9           #eventX:F
    .end local v10           #eventY:F
    .end local v14           #limitX:F
    .end local v15           #limitY:F
    .end local v17           #scale:F
    .end local v27           #touchRadius:F
    .end local v28           #tx:F
    .end local v29           #ty:F
    :cond_132
    move-object/from16 v0, p0

    #@134
    iget-boolean v0, v0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mDragging:Z

    #@136
    move/from16 v32, v0

    #@138
    if-nez v32, :cond_13b

    #@13a
    .line 909
    :goto_13a
    return-void

    #@13b
    .line 874
    :cond_13b
    const/16 v32, -0x1

    #@13d
    move/from16 v0, v32

    #@13f
    if-eq v5, v0, :cond_200

    #@141
    .line 875
    const/16 v32, 0x4

    #@143
    move-object/from16 v0, p0

    #@145
    move/from16 v1, v32

    #@147
    move/from16 v2, v30

    #@149
    move/from16 v3, v31

    #@14b
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->switchToState(IFF)V

    #@14e
    .line 876
    const/16 v32, 0x0

    #@150
    move-object/from16 v0, p0

    #@152
    move/from16 v1, v30

    #@154
    move/from16 v2, v31

    #@156
    move/from16 v3, v32

    #@158
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->moveHandleTo(FFZ)V

    #@15b
    .line 883
    :goto_15b
    move-object/from16 v0, p0

    #@15d
    iget-object v0, v0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@15f
    move-object/from16 v32, v0

    #@161
    move-object/from16 v0, p0

    #@163
    move-object/from16 v1, v32

    #@165
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->invalidateGlobalRegion(Lcom/android/internal/widget/multiwaveview/TargetDrawable;)V

    #@168
    .line 885
    move-object/from16 v0, p0

    #@16a
    iget v0, v0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mActiveTarget:I

    #@16c
    move/from16 v32, v0

    #@16e
    move/from16 v0, v32

    #@170
    if-eq v0, v5, :cond_1fa

    #@172
    .line 887
    move-object/from16 v0, p0

    #@174
    iget v0, v0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mActiveTarget:I

    #@176
    move/from16 v32, v0

    #@178
    const/16 v33, -0x1

    #@17a
    move/from16 v0, v32

    #@17c
    move/from16 v1, v33

    #@17e
    if-eq v0, v1, :cond_1a5

    #@180
    .line 888
    move-object/from16 v0, p0

    #@182
    iget v0, v0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mActiveTarget:I

    #@184
    move/from16 v32, v0

    #@186
    move-object/from16 v0, v26

    #@188
    move/from16 v1, v32

    #@18a
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@18d
    move-result-object v20

    #@18e
    check-cast v20, Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@190
    .line 889
    .restart local v20       #target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    sget-object v32, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->STATE_FOCUSED:[I

    #@192
    move-object/from16 v0, v20

    #@194
    move-object/from16 v1, v32

    #@196
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->hasState([I)Z

    #@199
    move-result v32

    #@19a
    if-eqz v32, :cond_1a5

    #@19c
    .line 890
    sget-object v32, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->STATE_INACTIVE:[I

    #@19e
    move-object/from16 v0, v20

    #@1a0
    move-object/from16 v1, v32

    #@1a2
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->setState([I)V

    #@1a5
    .line 894
    .end local v20           #target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    :cond_1a5
    const/16 v32, -0x1

    #@1a7
    move/from16 v0, v32

    #@1a9
    if-eq v5, v0, :cond_21c

    #@1ab
    .line 895
    move-object/from16 v0, v26

    #@1ad
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1b0
    move-result-object v20

    #@1b1
    check-cast v20, Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@1b3
    .line 896
    .restart local v20       #target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    sget-object v32, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->STATE_FOCUSED:[I

    #@1b5
    move-object/from16 v0, v20

    #@1b7
    move-object/from16 v1, v32

    #@1b9
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->hasState([I)Z

    #@1bc
    move-result v32

    #@1bd
    if-eqz v32, :cond_1c8

    #@1bf
    .line 897
    sget-object v32, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->STATE_FOCUSED:[I

    #@1c1
    move-object/from16 v0, v20

    #@1c3
    move-object/from16 v1, v32

    #@1c5
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->setState([I)V

    #@1c8
    .line 899
    :cond_1c8
    move-object/from16 v0, p0

    #@1ca
    iget-object v0, v0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mContext:Landroid/content/Context;

    #@1cc
    move-object/from16 v32, v0

    #@1ce
    invoke-static/range {v32 .. v32}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    #@1d1
    move-result-object v32

    #@1d2
    invoke-virtual/range {v32 .. v32}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    #@1d5
    move-result v32

    #@1d6
    if-eqz v32, :cond_1e5

    #@1d8
    .line 900
    move-object/from16 v0, p0

    #@1da
    invoke-direct {v0, v5}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->getTargetDescription(I)Ljava/lang/String;

    #@1dd
    move-result-object v21

    #@1de
    .line 901
    .local v21, targetContentDescription:Ljava/lang/String;
    move-object/from16 v0, p0

    #@1e0
    move-object/from16 v1, v21

    #@1e2
    invoke-direct {v0, v1}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->announceText(Ljava/lang/String;)V

    #@1e5
    .line 903
    .end local v21           #targetContentDescription:Ljava/lang/String;
    :cond_1e5
    const/16 v32, 0x0

    #@1e7
    const/16 v33, 0x0

    #@1e9
    const/16 v34, 0x0

    #@1eb
    const/16 v35, 0x0

    #@1ed
    move-object/from16 v0, p0

    #@1ef
    move/from16 v1, v32

    #@1f1
    move/from16 v2, v33

    #@1f3
    move/from16 v3, v34

    #@1f5
    move-object/from16 v4, v35

    #@1f7
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->activateHandle(IIFLandroid/animation/Animator$AnimatorListener;)V

    #@1fa
    .line 908
    .end local v20           #target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    :cond_1fa
    :goto_1fa
    move-object/from16 v0, p0

    #@1fc
    iput v5, v0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mActiveTarget:I

    #@1fe
    goto/16 :goto_13a

    #@200
    .line 878
    :cond_200
    const/16 v32, 0x3

    #@202
    move-object/from16 v0, p0

    #@204
    move/from16 v1, v32

    #@206
    move/from16 v2, v30

    #@208
    move/from16 v3, v31

    #@20a
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->switchToState(IFF)V

    #@20d
    .line 879
    const/16 v32, 0x0

    #@20f
    move-object/from16 v0, p0

    #@211
    move/from16 v1, v30

    #@213
    move/from16 v2, v31

    #@215
    move/from16 v3, v32

    #@217
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->moveHandleTo(FFZ)V

    #@21a
    goto/16 :goto_15b

    #@21c
    .line 905
    :cond_21c
    const/16 v32, 0x0

    #@21e
    const/16 v33, 0x0

    #@220
    const/high16 v34, 0x3f80

    #@222
    const/16 v35, 0x0

    #@224
    move-object/from16 v0, p0

    #@226
    move/from16 v1, v32

    #@228
    move/from16 v2, v33

    #@22a
    move/from16 v3, v34

    #@22c
    move-object/from16 v4, v35

    #@22e
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->activateHandle(IIFLandroid/animation/Animator$AnimatorListener;)V

    #@231
    goto :goto_1fa
.end method

.method private handleUp(Landroid/view/MotionEvent;)V
    .registers 5
    .parameter "event"

    #@0
    .prologue
    .line 806
    const/4 v0, 0x5

    #@1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@4
    move-result v1

    #@5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@8
    move-result v2

    #@9
    invoke-direct {p0, v0, v1, v2}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->switchToState(IFF)V

    #@c
    .line 807
    return-void
.end method

.method private hideChevrons()V
    .registers 6

    #@0
    .prologue
    .line 1071
    iget-object v1, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mChevronDrawables:Ljava/util/ArrayList;

    #@2
    .line 1072
    .local v1, chevrons:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/widget/multiwaveview/TargetDrawable;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v3

    #@6
    .line 1073
    .local v3, size:I
    const/4 v2, 0x0

    #@7
    .local v2, i:I
    :goto_7
    if-ge v2, v3, :cond_18

    #@9
    .line 1074
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@c
    move-result-object v0

    #@d
    check-cast v0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@f
    .line 1075
    .local v0, chevron:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    if-eqz v0, :cond_15

    #@11
    .line 1076
    const/4 v4, 0x0

    #@12
    invoke-virtual {v0, v4}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->setAlpha(F)V

    #@15
    .line 1073
    :cond_15
    add-int/lit8 v2, v2, 0x1

    #@17
    goto :goto_7

    #@18
    .line 1079
    .end local v0           #chevron:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    :cond_18
    return-void
.end method

.method private hideTargets(ZZ)V
    .registers 17
    .parameter "animate"
    .parameter "expanded"

    #@0
    .prologue
    .line 534
    iget-object v7, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetAnimations:Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;

    #@2
    invoke-virtual {v7}, Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;->cancel()V

    #@5
    .line 537
    iput-boolean p1, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mAnimatingTargets:Z

    #@7
    .line 538
    if-eqz p1, :cond_83

    #@9
    const/16 v1, 0xc8

    #@b
    .line 539
    .local v1, duration:I
    :goto_b
    if-eqz p1, :cond_85

    #@d
    const/16 v0, 0xc8

    #@f
    .line 541
    .local v0, delay:I
    :goto_f
    if-eqz p2, :cond_87

    #@11
    const/high16 v6, 0x3f80

    #@13
    .line 542
    .local v6, targetScale:F
    :goto_13
    iget-object v7, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    #@15
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    #@18
    move-result v3

    #@19
    .line 543
    .local v3, length:I
    const/4 v2, 0x0

    #@1a
    .local v2, i:I
    :goto_1a
    if-ge v2, v3, :cond_8b

    #@1c
    .line 544
    iget-object v7, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    #@1e
    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@21
    move-result-object v5

    #@22
    check-cast v5, Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@24
    .line 545
    .local v5, target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    sget-object v7, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->STATE_INACTIVE:[I

    #@26
    invoke-virtual {v5, v7}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->setState([I)V

    #@29
    .line 546
    iget-object v7, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetAnimations:Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;

    #@2b
    int-to-long v8, v1

    #@2c
    const/16 v10, 0xc

    #@2e
    new-array v10, v10, [Ljava/lang/Object;

    #@30
    const/4 v11, 0x0

    #@31
    const-string v12, "ease"

    #@33
    aput-object v12, v10, v11

    #@35
    const/4 v11, 0x1

    #@36
    sget-object v12, Lcom/android/internal/widget/multiwaveview/Ease$Cubic;->easeOut:Landroid/animation/TimeInterpolator;

    #@38
    aput-object v12, v10, v11

    #@3a
    const/4 v11, 0x2

    #@3b
    const-string v12, "alpha"

    #@3d
    aput-object v12, v10, v11

    #@3f
    const/4 v11, 0x3

    #@40
    const/4 v12, 0x0

    #@41
    invoke-static {v12}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@44
    move-result-object v12

    #@45
    aput-object v12, v10, v11

    #@47
    const/4 v11, 0x4

    #@48
    const-string v12, "scaleX"

    #@4a
    aput-object v12, v10, v11

    #@4c
    const/4 v11, 0x5

    #@4d
    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@50
    move-result-object v12

    #@51
    aput-object v12, v10, v11

    #@53
    const/4 v11, 0x6

    #@54
    const-string v12, "scaleY"

    #@56
    aput-object v12, v10, v11

    #@58
    const/4 v11, 0x7

    #@59
    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@5c
    move-result-object v12

    #@5d
    aput-object v12, v10, v11

    #@5f
    const/16 v11, 0x8

    #@61
    const-string v12, "delay"

    #@63
    aput-object v12, v10, v11

    #@65
    const/16 v11, 0x9

    #@67
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@6a
    move-result-object v12

    #@6b
    aput-object v12, v10, v11

    #@6d
    const/16 v11, 0xa

    #@6f
    const-string v12, "onUpdate"

    #@71
    aput-object v12, v10, v11

    #@73
    const/16 v11, 0xb

    #@75
    iget-object v12, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mUpdateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    #@77
    aput-object v12, v10, v11

    #@79
    invoke-static {v5, v8, v9, v10}, Lcom/android/internal/widget/multiwaveview/Tweener;->to(Ljava/lang/Object;J[Ljava/lang/Object;)Lcom/android/internal/widget/multiwaveview/Tweener;

    #@7c
    move-result-object v8

    #@7d
    invoke-virtual {v7, v8}, Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;->add(Ljava/lang/Object;)Z

    #@80
    .line 543
    add-int/lit8 v2, v2, 0x1

    #@82
    goto :goto_1a

    #@83
    .line 538
    .end local v0           #delay:I
    .end local v1           #duration:I
    .end local v2           #i:I
    .end local v3           #length:I
    .end local v5           #target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    .end local v6           #targetScale:F
    :cond_83
    const/4 v1, 0x0

    #@84
    goto :goto_b

    #@85
    .line 539
    .restart local v1       #duration:I
    :cond_85
    const/4 v0, 0x0

    #@86
    goto :goto_f

    #@87
    .line 541
    .restart local v0       #delay:I
    :cond_87
    const v6, 0x3f4ccccd

    #@8a
    goto :goto_13

    #@8b
    .line 555
    .restart local v2       #i:I
    .restart local v3       #length:I
    .restart local v6       #targetScale:F
    :cond_8b
    if-eqz p2, :cond_fa

    #@8d
    const/high16 v4, 0x3f80

    #@8f
    .line 556
    .local v4, ringScaleTarget:F
    :goto_8f
    iget-object v7, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetAnimations:Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;

    #@91
    iget-object v8, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mOuterRing:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@93
    int-to-long v9, v1

    #@94
    const/16 v11, 0xe

    #@96
    new-array v11, v11, [Ljava/lang/Object;

    #@98
    const/4 v12, 0x0

    #@99
    const-string v13, "ease"

    #@9b
    aput-object v13, v11, v12

    #@9d
    const/4 v12, 0x1

    #@9e
    sget-object v13, Lcom/android/internal/widget/multiwaveview/Ease$Cubic;->easeOut:Landroid/animation/TimeInterpolator;

    #@a0
    aput-object v13, v11, v12

    #@a2
    const/4 v12, 0x2

    #@a3
    const-string v13, "alpha"

    #@a5
    aput-object v13, v11, v12

    #@a7
    const/4 v12, 0x3

    #@a8
    const/4 v13, 0x0

    #@a9
    invoke-static {v13}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@ac
    move-result-object v13

    #@ad
    aput-object v13, v11, v12

    #@af
    const/4 v12, 0x4

    #@b0
    const-string v13, "scaleX"

    #@b2
    aput-object v13, v11, v12

    #@b4
    const/4 v12, 0x5

    #@b5
    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@b8
    move-result-object v13

    #@b9
    aput-object v13, v11, v12

    #@bb
    const/4 v12, 0x6

    #@bc
    const-string v13, "scaleY"

    #@be
    aput-object v13, v11, v12

    #@c0
    const/4 v12, 0x7

    #@c1
    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@c4
    move-result-object v13

    #@c5
    aput-object v13, v11, v12

    #@c7
    const/16 v12, 0x8

    #@c9
    const-string v13, "delay"

    #@cb
    aput-object v13, v11, v12

    #@cd
    const/16 v12, 0x9

    #@cf
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@d2
    move-result-object v13

    #@d3
    aput-object v13, v11, v12

    #@d5
    const/16 v12, 0xa

    #@d7
    const-string v13, "onUpdate"

    #@d9
    aput-object v13, v11, v12

    #@db
    const/16 v12, 0xb

    #@dd
    iget-object v13, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mUpdateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    #@df
    aput-object v13, v11, v12

    #@e1
    const/16 v12, 0xc

    #@e3
    const-string v13, "onComplete"

    #@e5
    aput-object v13, v11, v12

    #@e7
    const/16 v12, 0xd

    #@e9
    iget-object v13, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetUpdateListener:Landroid/animation/Animator$AnimatorListener;

    #@eb
    aput-object v13, v11, v12

    #@ed
    invoke-static {v8, v9, v10, v11}, Lcom/android/internal/widget/multiwaveview/Tweener;->to(Ljava/lang/Object;J[Ljava/lang/Object;)Lcom/android/internal/widget/multiwaveview/Tweener;

    #@f0
    move-result-object v8

    #@f1
    invoke-virtual {v7, v8}, Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;->add(Ljava/lang/Object;)Z

    #@f4
    .line 565
    iget-object v7, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetAnimations:Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;

    #@f6
    invoke-virtual {v7}, Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;->start()V

    #@f9
    .line 566
    return-void

    #@fa
    .line 555
    .end local v4           #ringScaleTarget:F
    :cond_fa
    const/high16 v4, 0x3f00

    #@fc
    goto :goto_8f
.end method

.method private hideUnselected(I)V
    .registers 5
    .parameter "active"

    #@0
    .prologue
    .line 526
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    iget-object v1, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v1

    #@7
    if-ge v0, v1, :cond_1a

    #@9
    .line 527
    if-eq v0, p1, :cond_17

    #@b
    .line 528
    iget-object v1, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    #@d
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@10
    move-result-object v1

    #@11
    check-cast v1, Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@13
    const/4 v2, 0x0

    #@14
    invoke-virtual {v1, v2}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->setAlpha(F)V

    #@17
    .line 526
    :cond_17
    add-int/lit8 v0, v0, 0x1

    #@19
    goto :goto_1

    #@1a
    .line 531
    :cond_1a
    return-void
.end method

.method private highlightSelected(I)V
    .registers 4
    .parameter "activeTarget"

    #@0
    .prologue
    .line 521
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@8
    sget-object v1, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->STATE_ACTIVE:[I

    #@a
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->setState([I)V

    #@d
    .line 522
    invoke-direct {p0, p1}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->hideUnselected(I)V

    #@10
    .line 523
    return-void
.end method

.method private internalSetTargetResources(I)V
    .registers 9
    .parameter "resourceId"

    #@0
    .prologue
    .line 621
    invoke-direct {p0, p1}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->loadDrawableArray(I)Ljava/util/ArrayList;

    #@3
    move-result-object v5

    #@4
    iput-object v5, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    #@6
    .line 622
    iput p1, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetResourceId:I

    #@8
    .line 623
    iget-object v5, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    #@a
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@d
    move-result v0

    #@e
    .line 624
    .local v0, count:I
    iget-object v5, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@10
    invoke-virtual {v5}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->getWidth()I

    #@13
    move-result v3

    #@14
    .line 625
    .local v3, maxWidth:I
    iget-object v5, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@16
    invoke-virtual {v5}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->getHeight()I

    #@19
    move-result v2

    #@1a
    .line 626
    .local v2, maxHeight:I
    const/4 v1, 0x0

    #@1b
    .local v1, i:I
    :goto_1b
    if-ge v1, v0, :cond_38

    #@1d
    .line 627
    iget-object v5, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    #@1f
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@22
    move-result-object v4

    #@23
    check-cast v4, Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@25
    .line 628
    .local v4, target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    invoke-virtual {v4}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->getWidth()I

    #@28
    move-result v5

    #@29
    invoke-static {v3, v5}, Ljava/lang/Math;->max(II)I

    #@2c
    move-result v3

    #@2d
    .line 629
    invoke-virtual {v4}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->getHeight()I

    #@30
    move-result v5

    #@31
    invoke-static {v2, v5}, Ljava/lang/Math;->max(II)I

    #@34
    move-result v2

    #@35
    .line 626
    add-int/lit8 v1, v1, 0x1

    #@37
    goto :goto_1b

    #@38
    .line 631
    .end local v4           #target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    :cond_38
    iget v5, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mMaxTargetWidth:I

    #@3a
    if-ne v5, v3, :cond_40

    #@3c
    iget v5, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mMaxTargetHeight:I

    #@3e
    if-eq v5, v2, :cond_48

    #@40
    .line 632
    :cond_40
    iput v3, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mMaxTargetWidth:I

    #@42
    .line 633
    iput v2, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mMaxTargetHeight:I

    #@44
    .line 634
    invoke-virtual {p0}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->requestLayout()V

    #@47
    .line 639
    :goto_47
    return-void

    #@48
    .line 636
    :cond_48
    iget v5, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mWaveCenterX:F

    #@4a
    iget v6, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mWaveCenterY:F

    #@4c
    invoke-direct {p0, v5, v6}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->updateTargetPositions(FF)V

    #@4f
    .line 637
    iget v5, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mWaveCenterX:F

    #@51
    iget v6, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mWaveCenterY:F

    #@53
    invoke-direct {p0, v5, v6}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->updateChevronPositions(FF)V

    #@56
    goto :goto_47
.end method

.method private loadDescriptions(I)Ljava/util/ArrayList;
    .registers 8
    .parameter "resourceId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1171
    invoke-virtual {p0}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->getContext()Landroid/content/Context;

    #@3
    move-result-object v5

    #@4
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@7
    move-result-object v5

    #@8
    invoke-virtual {v5, p1}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    #@b
    move-result-object v0

    #@c
    .line 1172
    .local v0, array:Landroid/content/res/TypedArray;
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->length()I

    #@f
    move-result v2

    #@10
    .line 1173
    .local v2, count:I
    new-instance v4, Ljava/util/ArrayList;

    #@12
    invoke-direct {v4, v2}, Ljava/util/ArrayList;-><init>(I)V

    #@15
    .line 1174
    .local v4, targetContentDescriptions:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v3, 0x0

    #@16
    .local v3, i:I
    :goto_16
    if-ge v3, v2, :cond_22

    #@18
    .line 1175
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    .line 1176
    .local v1, contentDescription:Ljava/lang/String;
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1f
    .line 1174
    add-int/lit8 v3, v3, 0x1

    #@21
    goto :goto_16

    #@22
    .line 1178
    .end local v1           #contentDescription:Ljava/lang/String;
    :cond_22
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@25
    .line 1179
    return-object v4
.end method

.method private loadDrawableArray(I)Ljava/util/ArrayList;
    .registers 10
    .parameter "resourceId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/widget/multiwaveview/TargetDrawable;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 607
    invoke-virtual {p0}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->getContext()Landroid/content/Context;

    #@3
    move-result-object v7

    #@4
    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@7
    move-result-object v4

    #@8
    .line 608
    .local v4, res:Landroid/content/res/Resources;
    invoke-virtual {v4, p1}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    #@b
    move-result-object v0

    #@c
    .line 609
    .local v0, array:Landroid/content/res/TypedArray;
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->length()I

    #@f
    move-result v1

    #@10
    .line 610
    .local v1, count:I
    new-instance v2, Ljava/util/ArrayList;

    #@12
    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    #@15
    .line 611
    .local v2, drawables:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/widget/multiwaveview/TargetDrawable;>;"
    const/4 v3, 0x0

    #@16
    .local v3, i:I
    :goto_16
    if-ge v3, v1, :cond_2d

    #@18
    .line 612
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    #@1b
    move-result-object v6

    #@1c
    .line 613
    .local v6, value:Landroid/util/TypedValue;
    new-instance v5, Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@1e
    if-eqz v6, :cond_2b

    #@20
    iget v7, v6, Landroid/util/TypedValue;->resourceId:I

    #@22
    :goto_22
    invoke-direct {v5, v4, v7}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;-><init>(Landroid/content/res/Resources;I)V

    #@25
    .line 614
    .local v5, target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@28
    .line 611
    add-int/lit8 v3, v3, 0x1

    #@2a
    goto :goto_16

    #@2b
    .line 613
    .end local v5           #target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    :cond_2b
    const/4 v7, 0x0

    #@2c
    goto :goto_22

    #@2d
    .line 616
    .end local v6           #value:Landroid/util/TypedValue;
    :cond_2d
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@30
    .line 617
    return-object v2
.end method

.method private moveHandleTo(FFZ)V
    .registers 5
    .parameter "x"
    .parameter "y"
    .parameter "animate"

    #@0
    .prologue
    .line 790
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->setX(F)V

    #@5
    .line 791
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@7
    invoke-virtual {v0, p2}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->setY(F)V

    #@a
    .line 792
    return-void
.end method

.method private replaceTargetDrawables(Landroid/content/res/Resources;II)Z
    .registers 10
    .parameter "res"
    .parameter "existingResourceId"
    .parameter "newResourceId"

    #@0
    .prologue
    .line 1214
    if-eqz p2, :cond_4

    #@2
    if-nez p3, :cond_6

    #@4
    .line 1215
    :cond_4
    const/4 v2, 0x0

    #@5
    .line 1233
    :cond_5
    :goto_5
    return v2

    #@6
    .line 1218
    :cond_6
    const/4 v2, 0x0

    #@7
    .line 1219
    .local v2, result:Z
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    #@9
    .line 1220
    .local v0, drawables:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/widget/multiwaveview/TargetDrawable;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@c
    move-result v3

    #@d
    .line 1221
    .local v3, size:I
    const/4 v1, 0x0

    #@e
    .local v1, i:I
    :goto_e
    if-ge v1, v3, :cond_25

    #@10
    .line 1222
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@13
    move-result-object v4

    #@14
    check-cast v4, Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@16
    .line 1223
    .local v4, target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    if-eqz v4, :cond_22

    #@18
    invoke-virtual {v4}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->getResourceId()I

    #@1b
    move-result v5

    #@1c
    if-ne v5, p2, :cond_22

    #@1e
    .line 1224
    invoke-virtual {v4, p1, p3}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->setDrawable(Landroid/content/res/Resources;I)V

    #@21
    .line 1225
    const/4 v2, 0x1

    #@22
    .line 1221
    :cond_22
    add-int/lit8 v1, v1, 0x1

    #@24
    goto :goto_e

    #@25
    .line 1229
    .end local v4           #target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    :cond_25
    if-eqz v2, :cond_5

    #@27
    .line 1230
    invoke-virtual {p0}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->requestLayout()V

    #@2a
    goto :goto_5
.end method

.method private resolveMeasured(II)I
    .registers 6
    .parameter "measureSpec"
    .parameter "desired"

    #@0
    .prologue
    .line 318
    const/4 v0, 0x0

    #@1
    .line 319
    .local v0, result:I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@4
    move-result v1

    #@5
    .line 320
    .local v1, specSize:I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@8
    move-result v2

    #@9
    sparse-switch v2, :sswitch_data_16

    #@c
    .line 329
    move v0, v1

    #@d
    .line 331
    :goto_d
    return v0

    #@e
    .line 322
    :sswitch_e
    move v0, p2

    #@f
    .line 323
    goto :goto_d

    #@10
    .line 325
    :sswitch_10
    invoke-static {v1, p2}, Ljava/lang/Math;->min(II)I

    #@13
    move-result v0

    #@14
    .line 326
    goto :goto_d

    #@15
    .line 320
    nop

    #@16
    :sswitch_data_16
    .sparse-switch
        -0x80000000 -> :sswitch_10
        0x0 -> :sswitch_e
    .end sparse-switch
.end method

.method private setGrabbedState(I)V
    .registers 4
    .parameter "newState"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 937
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mGrabbedState:I

    #@3
    if-eq p1, v0, :cond_1c

    #@5
    .line 938
    if-eqz p1, :cond_a

    #@7
    .line 939
    invoke-direct {p0}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->vibrate()V

    #@a
    .line 941
    :cond_a
    iput p1, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mGrabbedState:I

    #@c
    .line 942
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mOnTriggerListener:Lcom/android/internal/widget/multiwaveview/MultiWaveView$OnTriggerListener;

    #@e
    if-eqz v0, :cond_1c

    #@10
    .line 943
    if-nez p1, :cond_1d

    #@12
    .line 944
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mOnTriggerListener:Lcom/android/internal/widget/multiwaveview/MultiWaveView$OnTriggerListener;

    #@14
    invoke-interface {v0, p0, v1}, Lcom/android/internal/widget/multiwaveview/MultiWaveView$OnTriggerListener;->onReleased(Landroid/view/View;I)V

    #@17
    .line 948
    :goto_17
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mOnTriggerListener:Lcom/android/internal/widget/multiwaveview/MultiWaveView$OnTriggerListener;

    #@19
    invoke-interface {v0, p0, p1}, Lcom/android/internal/widget/multiwaveview/MultiWaveView$OnTriggerListener;->onGrabbedStateChange(Landroid/view/View;I)V

    #@1c
    .line 951
    :cond_1c
    return-void

    #@1d
    .line 946
    :cond_1d
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mOnTriggerListener:Lcom/android/internal/widget/multiwaveview/MultiWaveView$OnTriggerListener;

    #@1f
    invoke-interface {v0, p0, v1}, Lcom/android/internal/widget/multiwaveview/MultiWaveView$OnTriggerListener;->onGrabbed(Landroid/view/View;I)V

    #@22
    goto :goto_17
.end method

.method private showTargets(Z)V
    .registers 14
    .parameter "animate"

    #@0
    .prologue
    .line 569
    iget-object v5, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetAnimations:Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;

    #@2
    invoke-virtual {v5}, Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;->stop()V

    #@5
    .line 570
    iput-boolean p1, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mAnimatingTargets:Z

    #@7
    .line 571
    if-eqz p1, :cond_84

    #@9
    const/16 v0, 0x32

    #@b
    .line 572
    .local v0, delay:I
    :goto_b
    if-eqz p1, :cond_86

    #@d
    const/16 v1, 0xc8

    #@f
    .line 573
    .local v1, duration:I
    :goto_f
    iget-object v5, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    #@11
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@14
    move-result v3

    #@15
    .line 574
    .local v3, length:I
    const/4 v2, 0x0

    #@16
    .local v2, i:I
    :goto_16
    if-ge v2, v3, :cond_88

    #@18
    .line 575
    iget-object v5, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    #@1a
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1d
    move-result-object v4

    #@1e
    check-cast v4, Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@20
    .line 576
    .local v4, target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    sget-object v5, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->STATE_INACTIVE:[I

    #@22
    invoke-virtual {v4, v5}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->setState([I)V

    #@25
    .line 577
    iget-object v5, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetAnimations:Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;

    #@27
    int-to-long v6, v1

    #@28
    const/16 v8, 0xc

    #@2a
    new-array v8, v8, [Ljava/lang/Object;

    #@2c
    const/4 v9, 0x0

    #@2d
    const-string v10, "ease"

    #@2f
    aput-object v10, v8, v9

    #@31
    const/4 v9, 0x1

    #@32
    sget-object v10, Lcom/android/internal/widget/multiwaveview/Ease$Cubic;->easeOut:Landroid/animation/TimeInterpolator;

    #@34
    aput-object v10, v8, v9

    #@36
    const/4 v9, 0x2

    #@37
    const-string v10, "alpha"

    #@39
    aput-object v10, v8, v9

    #@3b
    const/4 v9, 0x3

    #@3c
    const/high16 v10, 0x3f80

    #@3e
    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@41
    move-result-object v10

    #@42
    aput-object v10, v8, v9

    #@44
    const/4 v9, 0x4

    #@45
    const-string v10, "scaleX"

    #@47
    aput-object v10, v8, v9

    #@49
    const/4 v9, 0x5

    #@4a
    const/high16 v10, 0x3f80

    #@4c
    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@4f
    move-result-object v10

    #@50
    aput-object v10, v8, v9

    #@52
    const/4 v9, 0x6

    #@53
    const-string v10, "scaleY"

    #@55
    aput-object v10, v8, v9

    #@57
    const/4 v9, 0x7

    #@58
    const/high16 v10, 0x3f80

    #@5a
    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@5d
    move-result-object v10

    #@5e
    aput-object v10, v8, v9

    #@60
    const/16 v9, 0x8

    #@62
    const-string v10, "delay"

    #@64
    aput-object v10, v8, v9

    #@66
    const/16 v9, 0x9

    #@68
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@6b
    move-result-object v10

    #@6c
    aput-object v10, v8, v9

    #@6e
    const/16 v9, 0xa

    #@70
    const-string v10, "onUpdate"

    #@72
    aput-object v10, v8, v9

    #@74
    const/16 v9, 0xb

    #@76
    iget-object v10, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mUpdateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    #@78
    aput-object v10, v8, v9

    #@7a
    invoke-static {v4, v6, v7, v8}, Lcom/android/internal/widget/multiwaveview/Tweener;->to(Ljava/lang/Object;J[Ljava/lang/Object;)Lcom/android/internal/widget/multiwaveview/Tweener;

    #@7d
    move-result-object v6

    #@7e
    invoke-virtual {v5, v6}, Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;->add(Ljava/lang/Object;)Z

    #@81
    .line 574
    add-int/lit8 v2, v2, 0x1

    #@83
    goto :goto_16

    #@84
    .line 571
    .end local v0           #delay:I
    .end local v1           #duration:I
    .end local v2           #i:I
    .end local v3           #length:I
    .end local v4           #target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    :cond_84
    const/4 v0, 0x0

    #@85
    goto :goto_b

    #@86
    .line 572
    .restart local v0       #delay:I
    :cond_86
    const/4 v1, 0x0

    #@87
    goto :goto_f

    #@88
    .line 585
    .restart local v1       #duration:I
    .restart local v2       #i:I
    .restart local v3       #length:I
    :cond_88
    iget-object v5, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetAnimations:Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;

    #@8a
    iget-object v6, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mOuterRing:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@8c
    int-to-long v7, v1

    #@8d
    const/16 v9, 0xe

    #@8f
    new-array v9, v9, [Ljava/lang/Object;

    #@91
    const/4 v10, 0x0

    #@92
    const-string v11, "ease"

    #@94
    aput-object v11, v9, v10

    #@96
    const/4 v10, 0x1

    #@97
    sget-object v11, Lcom/android/internal/widget/multiwaveview/Ease$Cubic;->easeOut:Landroid/animation/TimeInterpolator;

    #@99
    aput-object v11, v9, v10

    #@9b
    const/4 v10, 0x2

    #@9c
    const-string v11, "alpha"

    #@9e
    aput-object v11, v9, v10

    #@a0
    const/4 v10, 0x3

    #@a1
    const/high16 v11, 0x3f80

    #@a3
    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@a6
    move-result-object v11

    #@a7
    aput-object v11, v9, v10

    #@a9
    const/4 v10, 0x4

    #@aa
    const-string v11, "scaleX"

    #@ac
    aput-object v11, v9, v10

    #@ae
    const/4 v10, 0x5

    #@af
    const/high16 v11, 0x3f80

    #@b1
    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@b4
    move-result-object v11

    #@b5
    aput-object v11, v9, v10

    #@b7
    const/4 v10, 0x6

    #@b8
    const-string v11, "scaleY"

    #@ba
    aput-object v11, v9, v10

    #@bc
    const/4 v10, 0x7

    #@bd
    const/high16 v11, 0x3f80

    #@bf
    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@c2
    move-result-object v11

    #@c3
    aput-object v11, v9, v10

    #@c5
    const/16 v10, 0x8

    #@c7
    const-string v11, "delay"

    #@c9
    aput-object v11, v9, v10

    #@cb
    const/16 v10, 0x9

    #@cd
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@d0
    move-result-object v11

    #@d1
    aput-object v11, v9, v10

    #@d3
    const/16 v10, 0xa

    #@d5
    const-string v11, "onUpdate"

    #@d7
    aput-object v11, v9, v10

    #@d9
    const/16 v10, 0xb

    #@db
    iget-object v11, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mUpdateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    #@dd
    aput-object v11, v9, v10

    #@df
    const/16 v10, 0xc

    #@e1
    const-string v11, "onComplete"

    #@e3
    aput-object v11, v9, v10

    #@e5
    const/16 v10, 0xd

    #@e7
    iget-object v11, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetUpdateListener:Landroid/animation/Animator$AnimatorListener;

    #@e9
    aput-object v11, v9, v10

    #@eb
    invoke-static {v6, v7, v8, v9}, Lcom/android/internal/widget/multiwaveview/Tweener;->to(Ljava/lang/Object;J[Ljava/lang/Object;)Lcom/android/internal/widget/multiwaveview/Tweener;

    #@ee
    move-result-object v6

    #@ef
    invoke-virtual {v5, v6}, Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;->add(Ljava/lang/Object;)Z

    #@f2
    .line 594
    iget-object v5, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetAnimations:Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;

    #@f4
    invoke-virtual {v5}, Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;->start()V

    #@f7
    .line 595
    return-void
.end method

.method private square(F)F
    .registers 3
    .parameter "d"

    #@0
    .prologue
    .line 1106
    mul-float v0, p1, p1

    #@2
    return v0
.end method

.method private startBackgroundAnimation(IF)V
    .registers 11
    .parameter "duration"
    .parameter "alpha"

    #@0
    .prologue
    const/4 v5, 0x2

    #@1
    const/4 v7, 0x1

    #@2
    const/4 v6, 0x0

    #@3
    .line 741
    invoke-virtual {p0}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->getBackground()Landroid/graphics/drawable/Drawable;

    #@6
    move-result-object v0

    #@7
    .line 742
    .local v0, background:Landroid/graphics/drawable/Drawable;
    iget-boolean v1, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mAlwaysTrackFinger:Z

    #@9
    if-eqz v1, :cond_50

    #@b
    if-eqz v0, :cond_50

    #@d
    .line 743
    iget-object v1, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mBackgroundAnimator:Lcom/android/internal/widget/multiwaveview/Tweener;

    #@f
    if-eqz v1, :cond_18

    #@11
    .line 744
    iget-object v1, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mBackgroundAnimator:Lcom/android/internal/widget/multiwaveview/Tweener;

    #@13
    iget-object v1, v1, Lcom/android/internal/widget/multiwaveview/Tweener;->animator:Landroid/animation/ObjectAnimator;

    #@15
    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->end()V

    #@18
    .line 746
    :cond_18
    int-to-long v1, p1

    #@19
    const/4 v3, 0x6

    #@1a
    new-array v3, v3, [Ljava/lang/Object;

    #@1c
    const-string v4, "ease"

    #@1e
    aput-object v4, v3, v6

    #@20
    sget-object v4, Lcom/android/internal/widget/multiwaveview/Ease$Cubic;->easeIn:Landroid/animation/TimeInterpolator;

    #@22
    aput-object v4, v3, v7

    #@24
    const-string v4, "alpha"

    #@26
    aput-object v4, v3, v5

    #@28
    const/4 v4, 0x3

    #@29
    new-array v5, v5, [I

    #@2b
    aput v6, v5, v6

    #@2d
    const/high16 v6, 0x437f

    #@2f
    mul-float/2addr v6, p2

    #@30
    float-to-int v6, v6

    #@31
    aput v6, v5, v7

    #@33
    aput-object v5, v3, v4

    #@35
    const/4 v4, 0x4

    #@36
    const-string v5, "delay"

    #@38
    aput-object v5, v3, v4

    #@3a
    const/4 v4, 0x5

    #@3b
    const/16 v5, 0x32

    #@3d
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@40
    move-result-object v5

    #@41
    aput-object v5, v3, v4

    #@43
    invoke-static {v0, v1, v2, v3}, Lcom/android/internal/widget/multiwaveview/Tweener;->to(Ljava/lang/Object;J[Ljava/lang/Object;)Lcom/android/internal/widget/multiwaveview/Tweener;

    #@46
    move-result-object v1

    #@47
    iput-object v1, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mBackgroundAnimator:Lcom/android/internal/widget/multiwaveview/Tweener;

    #@49
    .line 750
    iget-object v1, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mBackgroundAnimator:Lcom/android/internal/widget/multiwaveview/Tweener;

    #@4b
    iget-object v1, v1, Lcom/android/internal/widget/multiwaveview/Tweener;->animator:Landroid/animation/ObjectAnimator;

    #@4d
    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    #@50
    .line 752
    :cond_50
    return-void
.end method

.method private startChevronAnimation()V
    .registers 25

    #@0
    .prologue
    .line 413
    move-object/from16 v0, p0

    #@2
    iget-object v0, v0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@4
    move-object/from16 v18, v0

    #@6
    invoke-virtual/range {v18 .. v18}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->getWidth()I

    #@9
    move-result v18

    #@a
    move/from16 v0, v18

    #@c
    int-to-float v0, v0

    #@d
    move/from16 v18, v0

    #@f
    const v19, 0x3f4ccccd

    #@12
    mul-float v5, v18, v19

    #@14
    .line 414
    .local v5, chevronStartDistance:F
    move-object/from16 v0, p0

    #@16
    iget v0, v0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mOuterRadius:F

    #@18
    move/from16 v18, v0

    #@1a
    const v19, 0x3f666666

    #@1d
    mul-float v18, v18, v19

    #@1f
    const/high16 v19, 0x4000

    #@21
    div-float v6, v18, v19

    #@23
    .line 415
    .local v6, chevronStopDistance:F
    const/high16 v13, 0x3f00

    #@25
    .line 416
    .local v13, startScale:F
    const/high16 v11, 0x4000

    #@27
    .line 417
    .local v11, endScale:F
    move-object/from16 v0, p0

    #@29
    iget v0, v0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mFeedbackCount:I

    #@2b
    move/from16 v18, v0

    #@2d
    if-lez v18, :cond_c0

    #@2f
    move-object/from16 v0, p0

    #@31
    iget-object v0, v0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mChevronDrawables:Ljava/util/ArrayList;

    #@33
    move-object/from16 v18, v0

    #@35
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    #@38
    move-result v18

    #@39
    move-object/from16 v0, p0

    #@3b
    iget v0, v0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mFeedbackCount:I

    #@3d
    move/from16 v19, v0

    #@3f
    div-int v10, v18, v19

    #@41
    .line 419
    .local v10, directionCount:I
    :goto_41
    move-object/from16 v0, p0

    #@43
    iget-object v0, v0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mChevronAnimations:Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;

    #@45
    move-object/from16 v18, v0

    #@47
    invoke-virtual/range {v18 .. v18}, Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;->stop()V

    #@4a
    .line 423
    const/4 v9, 0x0

    #@4b
    .local v9, direction:I
    :goto_4b
    if-ge v9, v10, :cond_164

    #@4d
    .line 424
    const-wide v18, 0x401921fb54442d18L

    #@52
    int-to-double v0, v9

    #@53
    move-wide/from16 v20, v0

    #@55
    mul-double v18, v18, v20

    #@57
    int-to-double v0, v10

    #@58
    move-wide/from16 v20, v0

    #@5a
    div-double v3, v18, v20

    #@5c
    .line 425
    .local v3, angle:D
    invoke-static {v3, v4}, Ljava/lang/Math;->cos(D)D

    #@5f
    move-result-wide v18

    #@60
    move-wide/from16 v0, v18

    #@62
    double-to-float v14, v0

    #@63
    .line 426
    .local v14, sx:F
    const/16 v18, 0x0

    #@65
    invoke-static {v3, v4}, Ljava/lang/Math;->sin(D)D

    #@68
    move-result-wide v19

    #@69
    move-wide/from16 v0, v19

    #@6b
    double-to-float v0, v0

    #@6c
    move/from16 v19, v0

    #@6e
    sub-float v15, v18, v19

    #@70
    .line 427
    .local v15, sy:F
    const/16 v18, 0x2

    #@72
    move/from16 v0, v18

    #@74
    new-array v0, v0, [F

    #@76
    move-object/from16 v16, v0

    #@78
    const/16 v18, 0x0

    #@7a
    mul-float v19, v14, v5

    #@7c
    aput v19, v16, v18

    #@7e
    const/16 v18, 0x1

    #@80
    mul-float v19, v14, v6

    #@82
    aput v19, v16, v18

    #@84
    .line 429
    .local v16, xrange:[F
    const/16 v18, 0x2

    #@86
    move/from16 v0, v18

    #@88
    new-array v0, v0, [F

    #@8a
    move-object/from16 v17, v0

    #@8c
    const/16 v18, 0x0

    #@8e
    mul-float v19, v15, v5

    #@90
    aput v19, v17, v18

    #@92
    const/16 v18, 0x1

    #@94
    mul-float v19, v15, v6

    #@96
    aput v19, v17, v18

    #@98
    .line 431
    .local v17, yrange:[F
    const/4 v7, 0x0

    #@99
    .local v7, count:I
    :goto_99
    move-object/from16 v0, p0

    #@9b
    iget v0, v0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mFeedbackCount:I

    #@9d
    move/from16 v18, v0

    #@9f
    move/from16 v0, v18

    #@a1
    if-ge v7, v0, :cond_160

    #@a3
    .line 432
    mul-int/lit16 v8, v7, 0xa0

    #@a5
    .line 433
    .local v8, delay:I
    move-object/from16 v0, p0

    #@a7
    iget-object v0, v0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mChevronDrawables:Ljava/util/ArrayList;

    #@a9
    move-object/from16 v18, v0

    #@ab
    move-object/from16 v0, p0

    #@ad
    iget v0, v0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mFeedbackCount:I

    #@af
    move/from16 v19, v0

    #@b1
    mul-int v19, v19, v9

    #@b3
    add-int v19, v19, v7

    #@b5
    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@b8
    move-result-object v12

    #@b9
    check-cast v12, Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@bb
    .line 434
    .local v12, icon:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    if-nez v12, :cond_c2

    #@bd
    .line 431
    :goto_bd
    add-int/lit8 v7, v7, 0x1

    #@bf
    goto :goto_99

    #@c0
    .line 417
    .end local v3           #angle:D
    .end local v7           #count:I
    .end local v8           #delay:I
    .end local v9           #direction:I
    .end local v10           #directionCount:I
    .end local v12           #icon:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    .end local v14           #sx:F
    .end local v15           #sy:F
    .end local v16           #xrange:[F
    .end local v17           #yrange:[F
    :cond_c0
    const/4 v10, 0x0

    #@c1
    goto :goto_41

    #@c2
    .line 437
    .restart local v3       #angle:D
    .restart local v7       #count:I
    .restart local v8       #delay:I
    .restart local v9       #direction:I
    .restart local v10       #directionCount:I
    .restart local v12       #icon:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    .restart local v14       #sx:F
    .restart local v15       #sy:F
    .restart local v16       #xrange:[F
    .restart local v17       #yrange:[F
    :cond_c2
    move-object/from16 v0, p0

    #@c4
    iget-object v0, v0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mChevronAnimations:Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;

    #@c6
    move-object/from16 v18, v0

    #@c8
    const-wide/16 v19, 0x352

    #@ca
    const/16 v21, 0x10

    #@cc
    move/from16 v0, v21

    #@ce
    new-array v0, v0, [Ljava/lang/Object;

    #@d0
    move-object/from16 v21, v0

    #@d2
    const/16 v22, 0x0

    #@d4
    const-string v23, "ease"

    #@d6
    aput-object v23, v21, v22

    #@d8
    const/16 v22, 0x1

    #@da
    move-object/from16 v0, p0

    #@dc
    iget-object v0, v0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mChevronAnimationInterpolator:Landroid/animation/TimeInterpolator;

    #@de
    move-object/from16 v23, v0

    #@e0
    aput-object v23, v21, v22

    #@e2
    const/16 v22, 0x2

    #@e4
    const-string v23, "delay"

    #@e6
    aput-object v23, v21, v22

    #@e8
    const/16 v22, 0x3

    #@ea
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@ed
    move-result-object v23

    #@ee
    aput-object v23, v21, v22

    #@f0
    const/16 v22, 0x4

    #@f2
    const-string v23, "x"

    #@f4
    aput-object v23, v21, v22

    #@f6
    const/16 v22, 0x5

    #@f8
    aput-object v16, v21, v22

    #@fa
    const/16 v22, 0x6

    #@fc
    const-string v23, "y"

    #@fe
    aput-object v23, v21, v22

    #@100
    const/16 v22, 0x7

    #@102
    aput-object v17, v21, v22

    #@104
    const/16 v22, 0x8

    #@106
    const-string v23, "alpha"

    #@108
    aput-object v23, v21, v22

    #@10a
    const/16 v22, 0x9

    #@10c
    const/16 v23, 0x2

    #@10e
    move/from16 v0, v23

    #@110
    new-array v0, v0, [F

    #@112
    move-object/from16 v23, v0

    #@114
    fill-array-data v23, :array_16e

    #@117
    aput-object v23, v21, v22

    #@119
    const/16 v22, 0xa

    #@11b
    const-string v23, "scaleX"

    #@11d
    aput-object v23, v21, v22

    #@11f
    const/16 v22, 0xb

    #@121
    const/16 v23, 0x2

    #@123
    move/from16 v0, v23

    #@125
    new-array v0, v0, [F

    #@127
    move-object/from16 v23, v0

    #@129
    fill-array-data v23, :array_176

    #@12c
    aput-object v23, v21, v22

    #@12e
    const/16 v22, 0xc

    #@130
    const-string v23, "scaleY"

    #@132
    aput-object v23, v21, v22

    #@134
    const/16 v22, 0xd

    #@136
    const/16 v23, 0x2

    #@138
    move/from16 v0, v23

    #@13a
    new-array v0, v0, [F

    #@13c
    move-object/from16 v23, v0

    #@13e
    fill-array-data v23, :array_17e

    #@141
    aput-object v23, v21, v22

    #@143
    const/16 v22, 0xe

    #@145
    const-string v23, "onUpdate"

    #@147
    aput-object v23, v21, v22

    #@149
    const/16 v22, 0xf

    #@14b
    move-object/from16 v0, p0

    #@14d
    iget-object v0, v0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mUpdateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    #@14f
    move-object/from16 v23, v0

    #@151
    aput-object v23, v21, v22

    #@153
    move-wide/from16 v0, v19

    #@155
    move-object/from16 v2, v21

    #@157
    invoke-static {v12, v0, v1, v2}, Lcom/android/internal/widget/multiwaveview/Tweener;->to(Ljava/lang/Object;J[Ljava/lang/Object;)Lcom/android/internal/widget/multiwaveview/Tweener;

    #@15a
    move-result-object v19

    #@15b
    invoke-virtual/range {v18 .. v19}, Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;->add(Ljava/lang/Object;)Z

    #@15e
    goto/16 :goto_bd

    #@160
    .line 423
    .end local v8           #delay:I
    .end local v12           #icon:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    :cond_160
    add-int/lit8 v9, v9, 0x1

    #@162
    goto/16 :goto_4b

    #@164
    .line 448
    .end local v3           #angle:D
    .end local v7           #count:I
    .end local v14           #sx:F
    .end local v15           #sy:F
    .end local v16           #xrange:[F
    .end local v17           #yrange:[F
    :cond_164
    move-object/from16 v0, p0

    #@166
    iget-object v0, v0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mChevronAnimations:Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;

    #@168
    move-object/from16 v18, v0

    #@16a
    invoke-virtual/range {v18 .. v18}, Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;->start()V

    #@16d
    .line 449
    return-void

    #@16e
    .line 437
    :array_16e
    .array-data 0x4
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
    .end array-data

    #@176
    :array_176
    .array-data 0x4
        0x0t 0x0t 0x0t 0x3ft
        0x0t 0x0t 0x0t 0x40t
    .end array-data

    #@17e
    :array_17e
    .array-data 0x4
        0x0t 0x0t 0x0t 0x3ft
        0x0t 0x0t 0x0t 0x40t
    .end array-data
.end method

.method private switchToState(IFF)V
    .registers 9
    .parameter "state"
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    const/high16 v4, 0x3f80

    #@2
    const/4 v3, 0x0

    #@3
    const/4 v2, 0x1

    #@4
    const/4 v1, 0x0

    #@5
    .line 345
    packed-switch p1, :pswitch_data_4c

    #@8
    .line 379
    :cond_8
    :goto_8
    :pswitch_8
    return-void

    #@9
    .line 347
    :pswitch_9
    invoke-direct {p0}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->deactivateTargets()V

    #@c
    .line 348
    invoke-direct {p0, v2, v1}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->hideTargets(ZZ)V

    #@f
    .line 349
    invoke-direct {p0, v1, v3}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->startBackgroundAnimation(IF)V

    #@12
    .line 350
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@14
    sget-object v1, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->STATE_INACTIVE:[I

    #@16
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->setState([I)V

    #@19
    goto :goto_8

    #@1a
    .line 354
    :pswitch_1a
    const/4 v0, 0x0

    #@1b
    invoke-direct {p0, v1, v1, v4, v0}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->deactivateHandle(IIFLandroid/animation/Animator$AnimatorListener;)V

    #@1e
    .line 355
    invoke-direct {p0, v1, v3}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->startBackgroundAnimation(IF)V

    #@21
    goto :goto_8

    #@22
    .line 359
    :pswitch_22
    invoke-direct {p0}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->deactivateTargets()V

    #@25
    .line 360
    invoke-direct {p0, v2}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->showTargets(Z)V

    #@28
    .line 361
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@2a
    sget-object v1, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->STATE_ACTIVE:[I

    #@2c
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->setState([I)V

    #@2f
    .line 362
    const/16 v0, 0xc8

    #@31
    invoke-direct {p0, v0, v4}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->startBackgroundAnimation(IF)V

    #@34
    .line 363
    invoke-direct {p0, v2}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->setGrabbedState(I)V

    #@37
    .line 364
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mContext:Landroid/content/Context;

    #@39
    invoke-static {v0}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    #@3c
    move-result-object v0

    #@3d
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    #@40
    move-result v0

    #@41
    if-eqz v0, :cond_8

    #@43
    .line 365
    invoke-direct {p0}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->announceTargets()V

    #@46
    goto :goto_8

    #@47
    .line 376
    :pswitch_47
    invoke-direct {p0}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->doFinish()V

    #@4a
    goto :goto_8

    #@4b
    .line 345
    nop

    #@4c
    :pswitch_data_4c
    .packed-switch 0x0
        :pswitch_9
        :pswitch_1a
        :pswitch_22
        :pswitch_8
        :pswitch_8
        :pswitch_47
    .end packed-switch
.end method

.method private trySwitchToFirstTouchState(FF)Z
    .registers 9
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 954
    iget v4, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mWaveCenterX:F

    #@4
    sub-float v0, p1, v4

    #@6
    .line 955
    .local v0, tx:F
    iget v4, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mWaveCenterY:F

    #@8
    sub-float v1, p2, v4

    #@a
    .line 956
    .local v1, ty:F
    iget-boolean v4, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mAlwaysTrackFinger:Z

    #@c
    if-nez v4, :cond_1a

    #@e
    invoke-direct {p0, v0, v1}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->dist2(FF)F

    #@11
    move-result v4

    #@12
    invoke-direct {p0}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->getScaledTapRadiusSquared()F

    #@15
    move-result v5

    #@16
    cmpg-float v4, v4, v5

    #@18
    if-gtz v4, :cond_24

    #@1a
    .line 958
    :cond_1a
    const/4 v4, 0x2

    #@1b
    invoke-direct {p0, v4, p1, p2}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->switchToState(IFF)V

    #@1e
    .line 959
    invoke-direct {p0, v0, v1, v2}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->moveHandleTo(FFZ)V

    #@21
    .line 960
    iput-boolean v3, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mDragging:Z

    #@23
    move v2, v3

    #@24
    .line 963
    :cond_24
    return v2
.end method

.method private updateChevronPositions(FF)V
    .registers 7
    .parameter "centerX"
    .parameter "centerY"

    #@0
    .prologue
    .line 1059
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mChevronDrawables:Ljava/util/ArrayList;

    #@2
    .line 1060
    .local v0, chevrons:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/widget/multiwaveview/TargetDrawable;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v2

    #@6
    .line 1061
    .local v2, size:I
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    if-ge v1, v2, :cond_1a

    #@9
    .line 1062
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@c
    move-result-object v3

    #@d
    check-cast v3, Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@f
    .line 1063
    .local v3, target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    if-eqz v3, :cond_17

    #@11
    .line 1064
    invoke-virtual {v3, p1}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->setPositionX(F)V

    #@14
    .line 1065
    invoke-virtual {v3, p2}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->setPositionY(F)V

    #@17
    .line 1061
    :cond_17
    add-int/lit8 v1, v1, 0x1

    #@19
    goto :goto_7

    #@1a
    .line 1068
    .end local v3           #target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    :cond_1a
    return-void
.end method

.method private updateTargetPositions(FF)V
    .registers 13
    .parameter "centerX"
    .parameter "centerY"

    #@0
    .prologue
    .line 1045
    iget-object v5, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    #@2
    .line 1046
    .local v5, targets:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/widget/multiwaveview/TargetDrawable;>;"
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v3

    #@6
    .line 1047
    .local v3, size:I
    const-wide v6, -0x3fe6de04abbbd2e8L

    #@b
    int-to-double v8, v3

    #@c
    div-double/2addr v6, v8

    #@d
    double-to-float v0, v6

    #@e
    .line 1048
    .local v0, alpha:F
    const/4 v2, 0x0

    #@f
    .local v2, i:I
    :goto_f
    if-ge v2, v3, :cond_3b

    #@11
    .line 1049
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@14
    move-result-object v4

    #@15
    check-cast v4, Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@17
    .line 1050
    .local v4, targetIcon:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    int-to-float v6, v2

    #@18
    mul-float v1, v0, v6

    #@1a
    .line 1051
    .local v1, angle:F
    invoke-virtual {v4, p1}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->setPositionX(F)V

    #@1d
    .line 1052
    invoke-virtual {v4, p2}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->setPositionY(F)V

    #@20
    .line 1053
    iget v6, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mOuterRadius:F

    #@22
    float-to-double v7, v1

    #@23
    invoke-static {v7, v8}, Ljava/lang/Math;->cos(D)D

    #@26
    move-result-wide v7

    #@27
    double-to-float v7, v7

    #@28
    mul-float/2addr v6, v7

    #@29
    invoke-virtual {v4, v6}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->setX(F)V

    #@2c
    .line 1054
    iget v6, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mOuterRadius:F

    #@2e
    float-to-double v7, v1

    #@2f
    invoke-static {v7, v8}, Ljava/lang/Math;->sin(D)D

    #@32
    move-result-wide v7

    #@33
    double-to-float v7, v7

    #@34
    mul-float/2addr v6, v7

    #@35
    invoke-virtual {v4, v6}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->setY(F)V

    #@38
    .line 1048
    add-int/lit8 v2, v2, 0x1

    #@3a
    goto :goto_f

    #@3b
    .line 1056
    .end local v1           #angle:F
    .end local v4           #targetIcon:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    :cond_3b
    return-void
.end method

.method private vibrate()V
    .registers 5

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 598
    iget-object v1, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mContext:Landroid/content/Context;

    #@3
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@6
    move-result-object v1

    #@7
    const-string v2, "haptic_feedback_enabled"

    #@9
    const/4 v3, -0x2

    #@a
    invoke-static {v1, v2, v0, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_1f

    #@10
    .line 601
    .local v0, hapticEnabled:Z
    :goto_10
    iget-object v1, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mVibrator:Landroid/os/Vibrator;

    #@12
    if-eqz v1, :cond_1e

    #@14
    if-eqz v0, :cond_1e

    #@16
    .line 602
    iget-object v1, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mVibrator:Landroid/os/Vibrator;

    #@18
    iget v2, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mVibrationDuration:I

    #@1a
    int-to-long v2, v2

    #@1b
    invoke-virtual {v1, v2, v3}, Landroid/os/Vibrator;->vibrate(J)V

    #@1e
    .line 604
    :cond_1e
    return-void

    #@1f
    .line 598
    .end local v0           #hapticEnabled:Z
    :cond_1f
    const/4 v0, 0x0

    #@20
    goto :goto_10
.end method


# virtual methods
.method public getDirectionDescriptionsResourceId()I
    .registers 2

    #@0
    .prologue
    .line 698
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mDirectionDescriptionsResourceId:I

    #@2
    return v0
.end method

.method public getResourceIdForTarget(I)I
    .registers 4
    .parameter "index"

    #@0
    .prologue
    .line 1183
    iget-object v1, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@8
    .line 1184
    .local v0, drawable:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    if-nez v0, :cond_c

    #@a
    const/4 v1, 0x0

    #@b
    :goto_b
    return v1

    #@c
    :cond_c
    invoke-virtual {v0}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->getResourceId()I

    #@f
    move-result v1

    #@10
    goto :goto_b
.end method

.method protected getSuggestedMinimumHeight()I
    .registers 4

    #@0
    .prologue
    .line 313
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mOuterRing:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->getHeight()I

    #@5
    move-result v0

    #@6
    int-to-float v0, v0

    #@7
    const/high16 v1, 0x4000

    #@9
    iget v2, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mOuterRadius:F

    #@b
    mul-float/2addr v1, v2

    #@c
    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    #@f
    move-result v0

    #@10
    iget v1, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mMaxTargetHeight:I

    #@12
    int-to-float v1, v1

    #@13
    add-float/2addr v0, v1

    #@14
    float-to-int v0, v0

    #@15
    return v0
.end method

.method protected getSuggestedMinimumWidth()I
    .registers 4

    #@0
    .prologue
    .line 306
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mOuterRing:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->getWidth()I

    #@5
    move-result v0

    #@6
    int-to-float v0, v0

    #@7
    const/high16 v1, 0x4000

    #@9
    iget v2, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mOuterRadius:F

    #@b
    mul-float/2addr v1, v2

    #@c
    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    #@f
    move-result v0

    #@10
    iget v1, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mMaxTargetWidth:I

    #@12
    int-to-float v1, v1

    #@13
    add-float/2addr v0, v1

    #@14
    float-to-int v0, v0

    #@15
    return v0
.end method

.method public getTargetDescriptionsResourceId()I
    .registers 2

    #@0
    .prologue
    .line 677
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetDescriptionsResourceId:I

    #@2
    return v0
.end method

.method public getTargetPosition(I)I
    .registers 5
    .parameter "resourceId"

    #@0
    .prologue
    .line 1203
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    iget-object v2, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v2

    #@7
    if-ge v0, v2, :cond_1b

    #@9
    .line 1204
    iget-object v2, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v1

    #@f
    check-cast v1, Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@11
    .line 1205
    .local v1, target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    invoke-virtual {v1}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->getResourceId()I

    #@14
    move-result v2

    #@15
    if-ne v2, p1, :cond_18

    #@17
    .line 1209
    .end local v0           #i:I
    .end local v1           #target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    :goto_17
    return v0

    #@18
    .line 1203
    .restart local v0       #i:I
    .restart local v1       #target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    :cond_18
    add-int/lit8 v0, v0, 0x1

    #@1a
    goto :goto_1

    #@1b
    .line 1209
    .end local v1           #target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    :cond_1b
    const/4 v0, -0x1

    #@1c
    goto :goto_17
.end method

.method public getTargetResourceId()I
    .registers 2

    #@0
    .prologue
    .line 656
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetResourceId:I

    #@2
    return v0
.end method

.method invalidateGlobalRegion(Lcom/android/internal/widget/multiwaveview/TargetDrawable;)V
    .registers 11
    .parameter "drawable"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 461
    invoke-virtual {p1}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->getWidth()I

    #@4
    move-result v3

    #@5
    .line 462
    .local v3, width:I
    invoke-virtual {p1}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->getHeight()I

    #@8
    move-result v1

    #@9
    .line 463
    .local v1, height:I
    new-instance v0, Landroid/graphics/RectF;

    #@b
    int-to-float v4, v3

    #@c
    int-to-float v5, v1

    #@d
    invoke-direct {v0, v6, v6, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    #@10
    .line 464
    .local v0, childBounds:Landroid/graphics/RectF;
    invoke-virtual {p1}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->getX()F

    #@13
    move-result v4

    #@14
    div-int/lit8 v5, v3, 0x2

    #@16
    int-to-float v5, v5

    #@17
    sub-float/2addr v4, v5

    #@18
    invoke-virtual {p1}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->getY()F

    #@1b
    move-result v5

    #@1c
    div-int/lit8 v6, v1, 0x2

    #@1e
    int-to-float v6, v6

    #@1f
    sub-float/2addr v5, v6

    #@20
    invoke-virtual {v0, v4, v5}, Landroid/graphics/RectF;->offset(FF)V

    #@23
    .line 465
    move-object v2, p0

    #@24
    .line 466
    .local v2, view:Landroid/view/View;
    :goto_24
    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    #@27
    move-result-object v4

    #@28
    if-eqz v4, :cond_63

    #@2a
    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    #@2d
    move-result-object v4

    #@2e
    instance-of v4, v4, Landroid/view/View;

    #@30
    if-eqz v4, :cond_63

    #@32
    .line 467
    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    #@35
    move-result-object v2

    #@36
    .end local v2           #view:Landroid/view/View;
    check-cast v2, Landroid/view/View;

    #@38
    .line 468
    .restart local v2       #view:Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getMatrix()Landroid/graphics/Matrix;

    #@3b
    move-result-object v4

    #@3c
    invoke-virtual {v4, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    #@3f
    .line 469
    iget v4, v0, Landroid/graphics/RectF;->left:F

    #@41
    float-to-double v4, v4

    #@42
    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    #@45
    move-result-wide v4

    #@46
    double-to-int v4, v4

    #@47
    iget v5, v0, Landroid/graphics/RectF;->top:F

    #@49
    float-to-double v5, v5

    #@4a
    invoke-static {v5, v6}, Ljava/lang/Math;->floor(D)D

    #@4d
    move-result-wide v5

    #@4e
    double-to-int v5, v5

    #@4f
    iget v6, v0, Landroid/graphics/RectF;->right:F

    #@51
    float-to-double v6, v6

    #@52
    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    #@55
    move-result-wide v6

    #@56
    double-to-int v6, v6

    #@57
    iget v7, v0, Landroid/graphics/RectF;->bottom:F

    #@59
    float-to-double v7, v7

    #@5a
    invoke-static {v7, v8}, Ljava/lang/Math;->ceil(D)D

    #@5d
    move-result-wide v7

    #@5e
    double-to-int v7, v7

    #@5f
    invoke-virtual {v2, v4, v5, v6, v7}, Landroid/view/View;->invalidate(IIII)V

    #@62
    goto :goto_24

    #@63
    .line 474
    :cond_63
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 8
    .parameter "canvas"

    #@0
    .prologue
    .line 1083
    iget-object v5, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mOuterRing:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@2
    invoke-virtual {v5, p1}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->draw(Landroid/graphics/Canvas;)V

    #@5
    .line 1084
    iget-object v5, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@a
    move-result v3

    #@b
    .line 1085
    .local v3, ntargets:I
    const/4 v1, 0x0

    #@c
    .local v1, i:I
    :goto_c
    if-ge v1, v3, :cond_1e

    #@e
    .line 1086
    iget-object v5, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    #@10
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@13
    move-result-object v4

    #@14
    check-cast v4, Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@16
    .line 1087
    .local v4, target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    if-eqz v4, :cond_1b

    #@18
    .line 1088
    invoke-virtual {v4, p1}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->draw(Landroid/graphics/Canvas;)V

    #@1b
    .line 1085
    :cond_1b
    add-int/lit8 v1, v1, 0x1

    #@1d
    goto :goto_c

    #@1e
    .line 1091
    .end local v4           #target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    :cond_1e
    iget-object v5, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mChevronDrawables:Ljava/util/ArrayList;

    #@20
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@23
    move-result v2

    #@24
    .line 1092
    .local v2, nchevrons:I
    const/4 v1, 0x0

    #@25
    :goto_25
    if-ge v1, v2, :cond_37

    #@27
    .line 1093
    iget-object v5, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mChevronDrawables:Ljava/util/ArrayList;

    #@29
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@2c
    move-result-object v0

    #@2d
    check-cast v0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@2f
    .line 1094
    .local v0, chevron:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    if-eqz v0, :cond_34

    #@31
    .line 1095
    invoke-virtual {v0, p1}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->draw(Landroid/graphics/Canvas;)V

    #@34
    .line 1092
    :cond_34
    add-int/lit8 v1, v1, 0x1

    #@36
    goto :goto_25

    #@37
    .line 1098
    .end local v0           #chevron:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    :cond_37
    iget-object v5, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@39
    invoke-virtual {v5, p1}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->draw(Landroid/graphics/Canvas;)V

    #@3c
    .line 1099
    return-void
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .registers 4
    .parameter "event"

    #@0
    .prologue
    .line 913
    iget-object v1, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mContext:Landroid/content/Context;

    #@2
    invoke-static {v1}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_19

    #@c
    .line 914
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@f
    move-result v0

    #@10
    .line 915
    .local v0, action:I
    packed-switch v0, :pswitch_data_2e

    #@13
    .line 926
    :goto_13
    :pswitch_13
    invoke-virtual {p0, p1}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    #@16
    .line 927
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->setAction(I)V

    #@19
    .line 929
    .end local v0           #action:I
    :cond_19
    invoke-super {p0, p1}, Landroid/view/View;->onHoverEvent(Landroid/view/MotionEvent;)Z

    #@1c
    move-result v1

    #@1d
    return v1

    #@1e
    .line 917
    .restart local v0       #action:I
    :pswitch_1e
    const/4 v1, 0x0

    #@1f
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->setAction(I)V

    #@22
    goto :goto_13

    #@23
    .line 920
    :pswitch_23
    const/4 v1, 0x2

    #@24
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->setAction(I)V

    #@27
    goto :goto_13

    #@28
    .line 923
    :pswitch_28
    const/4 v1, 0x1

    #@29
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->setAction(I)V

    #@2c
    goto :goto_13

    #@2d
    .line 915
    nop

    #@2e
    :pswitch_data_2e
    .packed-switch 0x7
        :pswitch_23
        :pswitch_13
        :pswitch_1e
        :pswitch_28
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .registers 15
    .parameter "changed"
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    .line 1008
    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    #@3
    .line 1009
    sub-int v5, p4, p2

    #@5
    .line 1010
    .local v5, width:I
    sub-int v0, p5, p3

    #@7
    .line 1014
    .local v0, height:I
    iget-object v6, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mOuterRing:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@9
    invoke-virtual {v6}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->getWidth()I

    #@c
    move-result v6

    #@d
    int-to-float v6, v6

    #@e
    const/high16 v7, 0x4000

    #@10
    iget v8, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mOuterRadius:F

    #@12
    mul-float/2addr v7, v8

    #@13
    invoke-static {v6, v7}, Ljava/lang/Math;->max(FF)F

    #@16
    move-result v4

    #@17
    .line 1015
    .local v4, placementWidth:F
    iget-object v6, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mOuterRing:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@19
    invoke-virtual {v6}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->getHeight()I

    #@1c
    move-result v6

    #@1d
    int-to-float v6, v6

    #@1e
    const/high16 v7, 0x4000

    #@20
    iget v8, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mOuterRadius:F

    #@22
    mul-float/2addr v7, v8

    #@23
    invoke-static {v6, v7}, Ljava/lang/Math;->max(FF)F

    #@26
    move-result v3

    #@27
    .line 1016
    .local v3, placementHeight:F
    iget v6, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mHorizontalInset:I

    #@29
    int-to-float v6, v6

    #@2a
    int-to-float v7, v5

    #@2b
    iget v8, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mMaxTargetWidth:I

    #@2d
    int-to-float v8, v8

    #@2e
    add-float/2addr v8, v4

    #@2f
    invoke-static {v7, v8}, Ljava/lang/Math;->max(FF)F

    #@32
    move-result v7

    #@33
    const/high16 v8, 0x4000

    #@35
    div-float/2addr v7, v8

    #@36
    add-float v1, v6, v7

    #@38
    .line 1018
    .local v1, newWaveCenterX:F
    iget v6, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mVerticalInset:I

    #@3a
    int-to-float v6, v6

    #@3b
    int-to-float v7, v0

    #@3c
    iget v8, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mMaxTargetHeight:I

    #@3e
    int-to-float v8, v8

    #@3f
    add-float/2addr v8, v3

    #@40
    invoke-static {v7, v8}, Ljava/lang/Math;->max(FF)F

    #@43
    move-result v7

    #@44
    const/high16 v8, 0x4000

    #@46
    div-float/2addr v7, v8

    #@47
    add-float v2, v6, v7

    #@49
    .line 1021
    .local v2, newWaveCenterY:F
    iget-boolean v6, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mInitialLayout:Z

    #@4b
    if-eqz v6, :cond_5e

    #@4d
    .line 1022
    invoke-direct {p0}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->hideChevrons()V

    #@50
    .line 1023
    const/4 v6, 0x0

    #@51
    const/4 v7, 0x0

    #@52
    invoke-direct {p0, v6, v7}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->hideTargets(ZZ)V

    #@55
    .line 1024
    const/4 v6, 0x0

    #@56
    const/4 v7, 0x0

    #@57
    const/4 v8, 0x0

    #@58
    invoke-direct {p0, v6, v7, v8}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->moveHandleTo(FFZ)V

    #@5b
    .line 1025
    const/4 v6, 0x0

    #@5c
    iput-boolean v6, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mInitialLayout:Z

    #@5e
    .line 1028
    :cond_5e
    iget-object v6, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mOuterRing:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@60
    invoke-virtual {v6, v1}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->setPositionX(F)V

    #@63
    .line 1029
    iget-object v6, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mOuterRing:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@65
    invoke-virtual {v6, v2}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->setPositionY(F)V

    #@68
    .line 1031
    iget-object v6, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@6a
    invoke-virtual {v6, v1}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->setPositionX(F)V

    #@6d
    .line 1032
    iget-object v6, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@6f
    invoke-virtual {v6, v2}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->setPositionY(F)V

    #@72
    .line 1034
    invoke-direct {p0, v1, v2}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->updateTargetPositions(FF)V

    #@75
    .line 1035
    invoke-direct {p0, v1, v2}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->updateChevronPositions(FF)V

    #@78
    .line 1037
    iput v1, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mWaveCenterX:F

    #@7a
    .line 1038
    iput v2, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mWaveCenterY:F

    #@7c
    .line 1041
    return-void
.end method

.method protected onMeasure(II)V
    .registers 9
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    .line 336
    invoke-virtual {p0}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->getSuggestedMinimumWidth()I

    #@3
    move-result v3

    #@4
    .line 337
    .local v3, minimumWidth:I
    invoke-virtual {p0}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->getSuggestedMinimumHeight()I

    #@7
    move-result v2

    #@8
    .line 338
    .local v2, minimumHeight:I
    invoke-direct {p0, p1, v3}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->resolveMeasured(II)I

    #@b
    move-result v1

    #@c
    .line 339
    .local v1, computedWidth:I
    invoke-direct {p0, p2, v2}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->resolveMeasured(II)I

    #@f
    move-result v0

    #@10
    .line 340
    .local v0, computedHeight:I
    sub-int v4, v1, v3

    #@12
    sub-int v5, v0, v2

    #@14
    invoke-direct {p0, v4, v5}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->computeInsets(II)V

    #@17
    .line 341
    invoke-virtual {p0, v1, v0}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->setMeasuredDimension(II)V

    #@1a
    .line 342
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 5
    .parameter "event"

    #@0
    .prologue
    .line 756
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@3
    move-result v0

    #@4
    .line 757
    .local v0, action:I
    const/4 v1, 0x0

    #@5
    .line 758
    .local v1, handled:Z
    packed-switch v0, :pswitch_data_2e

    #@8
    .line 785
    :goto_8
    invoke-virtual {p0}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->invalidate()V

    #@b
    .line 786
    if-eqz v1, :cond_29

    #@d
    const/4 v2, 0x1

    #@e
    :goto_e
    return v2

    #@f
    .line 761
    :pswitch_f
    invoke-direct {p0, p1}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->handleDown(Landroid/view/MotionEvent;)V

    #@12
    .line 762
    const/4 v1, 0x1

    #@13
    .line 763
    goto :goto_8

    #@14
    .line 767
    :pswitch_14
    invoke-direct {p0, p1}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->handleMove(Landroid/view/MotionEvent;)V

    #@17
    .line 768
    const/4 v1, 0x1

    #@18
    .line 769
    goto :goto_8

    #@19
    .line 773
    :pswitch_19
    invoke-direct {p0, p1}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->handleMove(Landroid/view/MotionEvent;)V

    #@1c
    .line 774
    invoke-direct {p0, p1}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->handleUp(Landroid/view/MotionEvent;)V

    #@1f
    .line 775
    const/4 v1, 0x1

    #@20
    .line 776
    goto :goto_8

    #@21
    .line 780
    :pswitch_21
    invoke-direct {p0, p1}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->handleMove(Landroid/view/MotionEvent;)V

    #@24
    .line 781
    invoke-direct {p0, p1}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->handleCancel(Landroid/view/MotionEvent;)V

    #@27
    .line 782
    const/4 v1, 0x1

    #@28
    goto :goto_8

    #@29
    .line 786
    :cond_29
    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    #@2c
    move-result v2

    #@2d
    goto :goto_e

    #@2e
    .line 758
    :pswitch_data_2e
    .packed-switch 0x0
        :pswitch_f
        :pswitch_19
        :pswitch_14
        :pswitch_21
    .end packed-switch
.end method

.method public ping()V
    .registers 1

    #@0
    .prologue
    .line 720
    invoke-direct {p0}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->startChevronAnimation()V

    #@3
    .line 721
    return-void
.end method

.method public replaceTargetDrawablesIfPresent(Landroid/content/ComponentName;Ljava/lang/String;I)Z
    .registers 14
    .parameter "component"
    .parameter "name"
    .parameter "existingResId"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 1246
    if-nez p3, :cond_4

    #@3
    .line 1267
    :cond_3
    :goto_3
    return v6

    #@4
    .line 1249
    :cond_4
    :try_start_4
    iget-object v7, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mContext:Landroid/content/Context;

    #@6
    invoke-virtual {v7}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@9
    move-result-object v4

    #@a
    .line 1251
    .local v4, packageManager:Landroid/content/pm/PackageManager;
    const/16 v7, 0x80

    #@c
    invoke-virtual {v4, p1, v7}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    #@f
    move-result-object v7

    #@10
    iget-object v2, v7, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    #@12
    .line 1253
    .local v2, metaData:Landroid/os/Bundle;
    if-eqz v2, :cond_3

    #@14
    .line 1254
    invoke-virtual {v2, p2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@17
    move-result v1

    #@18
    .line 1255
    .local v1, iconResId:I
    if-eqz v1, :cond_3

    #@1a
    .line 1256
    invoke-virtual {v4, p1}, Landroid/content/pm/PackageManager;->getResourcesForActivity(Landroid/content/ComponentName;)Landroid/content/res/Resources;

    #@1d
    move-result-object v5

    #@1e
    .line 1257
    .local v5, res:Landroid/content/res/Resources;
    invoke-direct {p0, v5, p3, v1}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->replaceTargetDrawables(Landroid/content/res/Resources;II)Z
    :try_end_21
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_4 .. :try_end_21} :catch_23
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_4 .. :try_end_21} :catch_47

    #@21
    move-result v6

    #@22
    goto :goto_3

    #@23
    .line 1260
    .end local v1           #iconResId:I
    .end local v2           #metaData:Landroid/os/Bundle;
    .end local v4           #packageManager:Landroid/content/pm/PackageManager;
    .end local v5           #res:Landroid/content/res/Resources;
    :catch_23
    move-exception v0

    #@24
    .line 1261
    .local v0, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v7, "MultiWaveView"

    #@26
    new-instance v8, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    const-string v9, "Failed to swap drawable; "

    #@2d
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v8

    #@31
    invoke-virtual {p1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    #@34
    move-result-object v9

    #@35
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v8

    #@39
    const-string v9, " not found"

    #@3b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v8

    #@3f
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v8

    #@43
    invoke-static {v7, v8, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@46
    goto :goto_3

    #@47
    .line 1263
    .end local v0           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_47
    move-exception v3

    #@48
    .line 1264
    .local v3, nfe:Landroid/content/res/Resources$NotFoundException;
    const-string v7, "MultiWaveView"

    #@4a
    new-instance v8, Ljava/lang/StringBuilder;

    #@4c
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@4f
    const-string v9, "Failed to swap drawable from "

    #@51
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v8

    #@55
    invoke-virtual {p1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    #@58
    move-result-object v9

    #@59
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v8

    #@5d
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@60
    move-result-object v8

    #@61
    invoke-static {v7, v8, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@64
    goto :goto_3
.end method

.method public reset(Z)V
    .registers 5
    .parameter "animate"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 730
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mChevronAnimations:Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;

    #@3
    invoke-virtual {v0}, Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;->stop()V

    #@6
    .line 731
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mHandleAnimations:Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;

    #@8
    invoke-virtual {v0}, Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;->stop()V

    #@b
    .line 732
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetAnimations:Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;

    #@d
    invoke-virtual {v0}, Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;->stop()V

    #@10
    .line 733
    const/4 v0, 0x0

    #@11
    invoke-direct {p0, v2, v0}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->startBackgroundAnimation(IF)V

    #@14
    .line 734
    invoke-direct {p0}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->hideChevrons()V

    #@17
    .line 735
    invoke-direct {p0, p1, v2}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->hideTargets(ZZ)V

    #@1a
    .line 736
    const/high16 v0, 0x3f80

    #@1c
    const/4 v1, 0x0

    #@1d
    invoke-direct {p0, v2, v2, v0, v1}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->deactivateHandle(IIFLandroid/animation/Animator$AnimatorListener;)V

    #@20
    .line 737
    invoke-static {}, Lcom/android/internal/widget/multiwaveview/Tweener;->reset()V

    #@23
    .line 738
    return-void
.end method

.method public resumeAnimations()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 294
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mChevronAnimations:Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;

    #@3
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;->setSuspended(Z)V

    #@6
    .line 295
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetAnimations:Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;

    #@8
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;->setSuspended(Z)V

    #@b
    .line 296
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mHandleAnimations:Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;

    #@d
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;->setSuspended(Z)V

    #@10
    .line 297
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mChevronAnimations:Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;

    #@12
    invoke-virtual {v0}, Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;->start()V

    #@15
    .line 298
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetAnimations:Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;

    #@17
    invoke-virtual {v0}, Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;->start()V

    #@1a
    .line 299
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mHandleAnimations:Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;

    #@1c
    invoke-virtual {v0}, Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;->start()V

    #@1f
    .line 300
    return-void
.end method

.method public setDirectionDescriptionsResourceId(I)V
    .registers 3
    .parameter "resourceId"

    #@0
    .prologue
    .line 686
    iput p1, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mDirectionDescriptionsResourceId:I

    #@2
    .line 687
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mDirectionDescriptions:Ljava/util/ArrayList;

    #@4
    if-eqz v0, :cond_b

    #@6
    .line 688
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mDirectionDescriptions:Ljava/util/ArrayList;

    #@8
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    #@b
    .line 690
    :cond_b
    return-void
.end method

.method public setEnableTarget(IZ)V
    .registers 6
    .parameter "resourceId"
    .parameter "enabled"

    #@0
    .prologue
    .line 1188
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    iget-object v2, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v2

    #@7
    if-ge v0, v2, :cond_1a

    #@9
    .line 1189
    iget-object v2, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v1

    #@f
    check-cast v1, Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@11
    .line 1190
    .local v1, target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    invoke-virtual {v1}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->getResourceId()I

    #@14
    move-result v2

    #@15
    if-ne v2, p1, :cond_1b

    #@17
    .line 1191
    invoke-virtual {v1, p2}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->setEnabled(Z)V

    #@1a
    .line 1195
    .end local v1           #target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    :cond_1a
    return-void

    #@1b
    .line 1188
    .restart local v1       #target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    :cond_1b
    add-int/lit8 v0, v0, 0x1

    #@1d
    goto :goto_1
.end method

.method public setOnTriggerListener(Lcom/android/internal/widget/multiwaveview/MultiWaveView$OnTriggerListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 1102
    iput-object p1, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mOnTriggerListener:Lcom/android/internal/widget/multiwaveview/MultiWaveView$OnTriggerListener;

    #@2
    .line 1103
    return-void
.end method

.method public setTargetDescriptionsResourceId(I)V
    .registers 3
    .parameter "resourceId"

    #@0
    .prologue
    .line 665
    iput p1, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetDescriptionsResourceId:I

    #@2
    .line 666
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetDescriptions:Ljava/util/ArrayList;

    #@4
    if-eqz v0, :cond_b

    #@6
    .line 667
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetDescriptions:Ljava/util/ArrayList;

    #@8
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    #@b
    .line 669
    :cond_b
    return-void
.end method

.method public setTargetResources(I)V
    .registers 3
    .parameter "resourceId"

    #@0
    .prologue
    .line 647
    iget-boolean v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mAnimatingTargets:Z

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 649
    iput p1, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mNewTargetResources:I

    #@6
    .line 653
    :goto_6
    return-void

    #@7
    .line 651
    :cond_7
    invoke-direct {p0, p1}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->internalSetTargetResources(I)V

    #@a
    goto :goto_6
.end method

.method public setVibrateEnabled(Z)V
    .registers 4
    .parameter "enabled"

    #@0
    .prologue
    .line 707
    if-eqz p1, :cond_15

    #@2
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mVibrator:Landroid/os/Vibrator;

    #@4
    if-nez v0, :cond_15

    #@6
    .line 708
    invoke-virtual {p0}, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->getContext()Landroid/content/Context;

    #@9
    move-result-object v0

    #@a
    const-string v1, "vibrator"

    #@c
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Landroid/os/Vibrator;

    #@12
    iput-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mVibrator:Landroid/os/Vibrator;

    #@14
    .line 712
    :goto_14
    return-void

    #@15
    .line 710
    :cond_15
    const/4 v0, 0x0

    #@16
    iput-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mVibrator:Landroid/os/Vibrator;

    #@18
    goto :goto_14
.end method

.method public suspendAnimations()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 288
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mChevronAnimations:Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;

    #@3
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;->setSuspended(Z)V

    #@6
    .line 289
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mTargetAnimations:Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;

    #@8
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;->setSuspended(Z)V

    #@b
    .line 290
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/MultiWaveView;->mHandleAnimations:Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;

    #@d
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/multiwaveview/MultiWaveView$AnimationBundle;->setSuspended(Z)V

    #@10
    .line 291
    return-void
.end method
