.class public Lcom/android/internal/widget/RotarySelector;
.super Landroid/view/View;
.source "RotarySelector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/widget/RotarySelector$OnDialTriggerListener;
    }
.end annotation


# static fields
.field private static final ARROW_SCRUNCH_DIP:I = 0x6

.field private static final DBG:Z = false

.field private static final EDGE_PADDING_DIP:I = 0x9

.field private static final EDGE_TRIGGER_DIP:I = 0x64

.field public static final HORIZONTAL:I = 0x0

.field public static final LEFT_HANDLE_GRABBED:I = 0x1

.field private static final LOG_TAG:Ljava/lang/String; = "RotarySelector"

.field public static final NOTHING_GRABBED:I = 0x0

.field static final OUTER_ROTARY_RADIUS_DIP:I = 0x186

.field public static final RIGHT_HANDLE_GRABBED:I = 0x2

.field static final ROTARY_STROKE_WIDTH_DIP:I = 0x53

.field static final SNAP_BACK_ANIMATION_DURATION_MILLIS:I = 0x12c

.field static final SPIN_ANIMATION_DURATION_MILLIS:I = 0x320

.field public static final VERTICAL:I = 0x1

.field private static final VIBRATE_LONG:J = 0x14L

.field private static final VIBRATE_SHORT:J = 0x14L

.field private static final VISUAL_DEBUG:Z


# instance fields
.field private mAnimating:Z

.field private mAnimatingDeltaXEnd:I

.field private mAnimatingDeltaXStart:I

.field private mAnimationDuration:J

.field private mAnimationStartTime:J

.field private mArrowLongLeft:Landroid/graphics/Bitmap;

.field private mArrowLongRight:Landroid/graphics/Bitmap;

.field final mArrowMatrix:Landroid/graphics/Matrix;

.field private mArrowShortLeftAndRight:Landroid/graphics/Bitmap;

.field private mBackground:Landroid/graphics/Bitmap;

.field private mBackgroundHeight:I

.field private mBackgroundWidth:I

.field final mBgMatrix:Landroid/graphics/Matrix;

.field private mDensity:F

.field private mDimple:Landroid/graphics/Bitmap;

.field private mDimpleDim:Landroid/graphics/Bitmap;

.field private mDimpleSpacing:I

.field private mDimpleWidth:I

.field private mDimplesOfFling:I

.field private mEdgeTriggerThresh:I

.field private mGrabbedState:I

.field private final mInnerRadius:I

.field private mInterpolator:Landroid/view/animation/DecelerateInterpolator;

.field private mLeftHandleIcon:Landroid/graphics/Bitmap;

.field private mLeftHandleX:I

.field private mMaximumVelocity:I

.field private mMinimumVelocity:I

.field private mOnDialTriggerListener:Lcom/android/internal/widget/RotarySelector$OnDialTriggerListener;

.field private mOrientation:I

.field private final mOuterRadius:I

.field private mPaint:Landroid/graphics/Paint;

.field private mRightHandleIcon:Landroid/graphics/Bitmap;

.field private mRightHandleX:I

.field private mRotaryOffsetX:I

.field private mTriggered:Z

.field private mVelocityTracker:Landroid/view/VelocityTracker;

.field private mVibrator:Landroid/os/Vibrator;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 164
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Lcom/android/internal/widget/RotarySelector;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 165
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 8
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 171
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 77
    iput v4, p0, Lcom/android/internal/widget/RotarySelector;->mRotaryOffsetX:I

    #@6
    .line 81
    iput-boolean v4, p0, Lcom/android/internal/widget/RotarySelector;->mAnimating:Z

    #@8
    .line 89
    new-instance v3, Landroid/graphics/Paint;

    #@a
    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    #@d
    iput-object v3, p0, Lcom/android/internal/widget/RotarySelector;->mPaint:Landroid/graphics/Paint;

    #@f
    .line 92
    new-instance v3, Landroid/graphics/Matrix;

    #@11
    invoke-direct {v3}, Landroid/graphics/Matrix;-><init>()V

    #@14
    iput-object v3, p0, Lcom/android/internal/widget/RotarySelector;->mBgMatrix:Landroid/graphics/Matrix;

    #@16
    .line 93
    new-instance v3, Landroid/graphics/Matrix;

    #@18
    invoke-direct {v3}, Landroid/graphics/Matrix;-><init>()V

    #@1b
    iput-object v3, p0, Lcom/android/internal/widget/RotarySelector;->mArrowMatrix:Landroid/graphics/Matrix;

    #@1d
    .line 98
    iput v4, p0, Lcom/android/internal/widget/RotarySelector;->mGrabbedState:I

    #@1f
    .line 107
    iput-boolean v4, p0, Lcom/android/internal/widget/RotarySelector;->mTriggered:Z

    #@21
    .line 155
    iput v4, p0, Lcom/android/internal/widget/RotarySelector;->mDimplesOfFling:I

    #@23
    .line 173
    sget-object v3, Lcom/android/internal/R$styleable;->RotarySelector:[I

    #@25
    invoke-virtual {p1, p2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@28
    move-result-object v0

    #@29
    .line 175
    .local v0, a:Landroid/content/res/TypedArray;
    invoke-virtual {v0, v4, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    #@2c
    move-result v3

    #@2d
    iput v3, p0, Lcom/android/internal/widget/RotarySelector;->mOrientation:I

    #@2f
    .line 176
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@32
    .line 178
    invoke-virtual {p0}, Lcom/android/internal/widget/RotarySelector;->getResources()Landroid/content/res/Resources;

    #@35
    move-result-object v2

    #@36
    .line 179
    .local v2, r:Landroid/content/res/Resources;
    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@39
    move-result-object v3

    #@3a
    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    #@3c
    iput v3, p0, Lcom/android/internal/widget/RotarySelector;->mDensity:F

    #@3e
    .line 183
    const v3, 0x108038b

    #@41
    invoke-direct {p0, v3}, Lcom/android/internal/widget/RotarySelector;->getBitmapFor(I)Landroid/graphics/Bitmap;

    #@44
    move-result-object v3

    #@45
    iput-object v3, p0, Lcom/android/internal/widget/RotarySelector;->mBackground:Landroid/graphics/Bitmap;

    #@47
    .line 184
    const v3, 0x108038c

    #@4a
    invoke-direct {p0, v3}, Lcom/android/internal/widget/RotarySelector;->getBitmapFor(I)Landroid/graphics/Bitmap;

    #@4d
    move-result-object v3

    #@4e
    iput-object v3, p0, Lcom/android/internal/widget/RotarySelector;->mDimple:Landroid/graphics/Bitmap;

    #@50
    .line 185
    const v3, 0x108038d

    #@53
    invoke-direct {p0, v3}, Lcom/android/internal/widget/RotarySelector;->getBitmapFor(I)Landroid/graphics/Bitmap;

    #@56
    move-result-object v3

    #@57
    iput-object v3, p0, Lcom/android/internal/widget/RotarySelector;->mDimpleDim:Landroid/graphics/Bitmap;

    #@59
    .line 187
    const v3, 0x1080383

    #@5c
    invoke-direct {p0, v3}, Lcom/android/internal/widget/RotarySelector;->getBitmapFor(I)Landroid/graphics/Bitmap;

    #@5f
    move-result-object v3

    #@60
    iput-object v3, p0, Lcom/android/internal/widget/RotarySelector;->mArrowLongLeft:Landroid/graphics/Bitmap;

    #@62
    .line 188
    const v3, 0x1080386

    #@65
    invoke-direct {p0, v3}, Lcom/android/internal/widget/RotarySelector;->getBitmapFor(I)Landroid/graphics/Bitmap;

    #@68
    move-result-object v3

    #@69
    iput-object v3, p0, Lcom/android/internal/widget/RotarySelector;->mArrowLongRight:Landroid/graphics/Bitmap;

    #@6b
    .line 189
    const v3, 0x1080389

    #@6e
    invoke-direct {p0, v3}, Lcom/android/internal/widget/RotarySelector;->getBitmapFor(I)Landroid/graphics/Bitmap;

    #@71
    move-result-object v3

    #@72
    iput-object v3, p0, Lcom/android/internal/widget/RotarySelector;->mArrowShortLeftAndRight:Landroid/graphics/Bitmap;

    #@74
    .line 191
    new-instance v3, Landroid/view/animation/DecelerateInterpolator;

    #@76
    const/high16 v4, 0x3f80

    #@78
    invoke-direct {v3, v4}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    #@7b
    iput-object v3, p0, Lcom/android/internal/widget/RotarySelector;->mInterpolator:Landroid/view/animation/DecelerateInterpolator;

    #@7d
    .line 193
    iget v3, p0, Lcom/android/internal/widget/RotarySelector;->mDensity:F

    #@7f
    const/high16 v4, 0x42c8

    #@81
    mul-float/2addr v3, v4

    #@82
    float-to-int v3, v3

    #@83
    iput v3, p0, Lcom/android/internal/widget/RotarySelector;->mEdgeTriggerThresh:I

    #@85
    .line 195
    iget-object v3, p0, Lcom/android/internal/widget/RotarySelector;->mDimple:Landroid/graphics/Bitmap;

    #@87
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    #@8a
    move-result v3

    #@8b
    iput v3, p0, Lcom/android/internal/widget/RotarySelector;->mDimpleWidth:I

    #@8d
    .line 197
    iget-object v3, p0, Lcom/android/internal/widget/RotarySelector;->mBackground:Landroid/graphics/Bitmap;

    #@8f
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    #@92
    move-result v3

    #@93
    iput v3, p0, Lcom/android/internal/widget/RotarySelector;->mBackgroundWidth:I

    #@95
    .line 198
    iget-object v3, p0, Lcom/android/internal/widget/RotarySelector;->mBackground:Landroid/graphics/Bitmap;

    #@97
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    #@9a
    move-result v3

    #@9b
    iput v3, p0, Lcom/android/internal/widget/RotarySelector;->mBackgroundHeight:I

    #@9d
    .line 199
    iget v3, p0, Lcom/android/internal/widget/RotarySelector;->mDensity:F

    #@9f
    const/high16 v4, 0x43c3

    #@a1
    mul-float/2addr v3, v4

    #@a2
    float-to-int v3, v3

    #@a3
    iput v3, p0, Lcom/android/internal/widget/RotarySelector;->mOuterRadius:I

    #@a5
    .line 200
    const v3, 0x43998000

    #@a8
    iget v4, p0, Lcom/android/internal/widget/RotarySelector;->mDensity:F

    #@aa
    mul-float/2addr v3, v4

    #@ab
    float-to-int v3, v3

    #@ac
    iput v3, p0, Lcom/android/internal/widget/RotarySelector;->mInnerRadius:I

    #@ae
    .line 202
    iget-object v3, p0, Lcom/android/internal/widget/RotarySelector;->mContext:Landroid/content/Context;

    #@b0
    invoke-static {v3}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    #@b3
    move-result-object v1

    #@b4
    .line 203
    .local v1, configuration:Landroid/view/ViewConfiguration;
    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    #@b7
    move-result v3

    #@b8
    mul-int/lit8 v3, v3, 0x2

    #@ba
    iput v3, p0, Lcom/android/internal/widget/RotarySelector;->mMinimumVelocity:I

    #@bc
    .line 204
    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    #@bf
    move-result v3

    #@c0
    iput v3, p0, Lcom/android/internal/widget/RotarySelector;->mMaximumVelocity:I

    #@c2
    .line 205
    return-void
.end method

.method private dispatchTriggerEvent(I)V
    .registers 4
    .parameter "whichHandle"

    #@0
    .prologue
    .line 711
    const-wide/16 v0, 0x14

    #@2
    invoke-direct {p0, v0, v1}, Lcom/android/internal/widget/RotarySelector;->vibrate(J)V

    #@5
    .line 712
    iget-object v0, p0, Lcom/android/internal/widget/RotarySelector;->mOnDialTriggerListener:Lcom/android/internal/widget/RotarySelector$OnDialTriggerListener;

    #@7
    if-eqz v0, :cond_e

    #@9
    .line 713
    iget-object v0, p0, Lcom/android/internal/widget/RotarySelector;->mOnDialTriggerListener:Lcom/android/internal/widget/RotarySelector$OnDialTriggerListener;

    #@b
    invoke-interface {v0, p0, p1}, Lcom/android/internal/widget/RotarySelector$OnDialTriggerListener;->onDialTrigger(Landroid/view/View;I)V

    #@e
    .line 715
    :cond_e
    return-void
.end method

.method private drawCentered(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;II)V
    .registers 10
    .parameter "d"
    .parameter "c"
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 690
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    #@3
    move-result v1

    #@4
    .line 691
    .local v1, w:I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    #@7
    move-result v0

    #@8
    .line 693
    .local v0, h:I
    div-int/lit8 v2, v1, 0x2

    #@a
    sub-int v2, p3, v2

    #@c
    int-to-float v2, v2

    #@d
    div-int/lit8 v3, v0, 0x2

    #@f
    sub-int v3, p4, v3

    #@11
    int-to-float v3, v3

    #@12
    iget-object v4, p0, Lcom/android/internal/widget/RotarySelector;->mPaint:Landroid/graphics/Paint;

    #@14
    invoke-virtual {p2, p1, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    #@17
    .line 694
    return-void
.end method

.method private getBitmapFor(I)Landroid/graphics/Bitmap;
    .registers 3
    .parameter "resId"

    #@0
    .prologue
    .line 208
    invoke-virtual {p0}, Lcom/android/internal/widget/RotarySelector;->getContext()Landroid/content/Context;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@7
    move-result-object v0

    #@8
    invoke-static {v0, p1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    #@b
    move-result-object v0

    #@c
    return-object v0
.end method

.method private getYOnArc(IIII)I
    .registers 11
    .parameter "backgroundWidth"
    .parameter "innerRadius"
    .parameter "outerRadius"
    .parameter "x"

    #@0
    .prologue
    .line 468
    sub-int v4, p3, p2

    #@2
    div-int/lit8 v0, v4, 0x2

    #@4
    .line 469
    .local v0, halfWidth:I
    add-int v1, p2, v0

    #@6
    .line 472
    .local v1, middleRadius:I
    div-int/lit8 v4, p1, 0x2

    #@8
    sub-int v2, v4, p4

    #@a
    .line 475
    .local v2, triangleBottom:I
    mul-int v4, v1, v1

    #@c
    mul-int v5, v2, v2

    #@e
    sub-int/2addr v4, v5

    #@f
    int-to-double v4, v4

    #@10
    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    #@13
    move-result-wide v4

    #@14
    double-to-int v3, v4

    #@15
    .line 482
    .local v3, triangleY:I
    sub-int v4, v1, v3

    #@17
    add-int/2addr v4, v0

    #@18
    return v4
.end method

.method private isHoriz()Z
    .registers 2

    #@0
    .prologue
    .line 235
    iget v0, p0, Lcom/android/internal/widget/RotarySelector;->mOrientation:I

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method private log(Ljava/lang/String;)V
    .registers 3
    .parameter "msg"

    #@0
    .prologue
    .line 771
    const-string v0, "RotarySelector"

    #@2
    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 772
    return-void
.end method

.method private reset()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 661
    iput-boolean v0, p0, Lcom/android/internal/widget/RotarySelector;->mAnimating:Z

    #@3
    .line 662
    iput v0, p0, Lcom/android/internal/widget/RotarySelector;->mRotaryOffsetX:I

    #@5
    .line 663
    iput v0, p0, Lcom/android/internal/widget/RotarySelector;->mDimplesOfFling:I

    #@7
    .line 664
    invoke-direct {p0, v0}, Lcom/android/internal/widget/RotarySelector;->setGrabbedState(I)V

    #@a
    .line 665
    iput-boolean v0, p0, Lcom/android/internal/widget/RotarySelector;->mTriggered:Z

    #@c
    .line 666
    return-void
.end method

.method private setGrabbedState(I)V
    .registers 4
    .parameter "newState"

    #@0
    .prologue
    .line 722
    iget v0, p0, Lcom/android/internal/widget/RotarySelector;->mGrabbedState:I

    #@2
    if-eq p1, v0, :cond_11

    #@4
    .line 723
    iput p1, p0, Lcom/android/internal/widget/RotarySelector;->mGrabbedState:I

    #@6
    .line 724
    iget-object v0, p0, Lcom/android/internal/widget/RotarySelector;->mOnDialTriggerListener:Lcom/android/internal/widget/RotarySelector$OnDialTriggerListener;

    #@8
    if-eqz v0, :cond_11

    #@a
    .line 725
    iget-object v0, p0, Lcom/android/internal/widget/RotarySelector;->mOnDialTriggerListener:Lcom/android/internal/widget/RotarySelector$OnDialTriggerListener;

    #@c
    iget v1, p0, Lcom/android/internal/widget/RotarySelector;->mGrabbedState:I

    #@e
    invoke-interface {v0, p0, v1}, Lcom/android/internal/widget/RotarySelector$OnDialTriggerListener;->onGrabbedStateChange(Landroid/view/View;I)V

    #@11
    .line 728
    :cond_11
    return-void
.end method

.method private startAnimation(III)V
    .registers 7
    .parameter "startX"
    .parameter "endX"
    .parameter "duration"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 609
    const/4 v0, 0x1

    #@2
    iput-boolean v0, p0, Lcom/android/internal/widget/RotarySelector;->mAnimating:Z

    #@4
    .line 610
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    #@7
    move-result-wide v0

    #@8
    iput-wide v0, p0, Lcom/android/internal/widget/RotarySelector;->mAnimationStartTime:J

    #@a
    .line 611
    int-to-long v0, p3

    #@b
    iput-wide v0, p0, Lcom/android/internal/widget/RotarySelector;->mAnimationDuration:J

    #@d
    .line 612
    iput p1, p0, Lcom/android/internal/widget/RotarySelector;->mAnimatingDeltaXStart:I

    #@f
    .line 613
    iput p2, p0, Lcom/android/internal/widget/RotarySelector;->mAnimatingDeltaXEnd:I

    #@11
    .line 614
    invoke-direct {p0, v2}, Lcom/android/internal/widget/RotarySelector;->setGrabbedState(I)V

    #@14
    .line 615
    iput v2, p0, Lcom/android/internal/widget/RotarySelector;->mDimplesOfFling:I

    #@16
    .line 616
    invoke-virtual {p0}, Lcom/android/internal/widget/RotarySelector;->invalidate()V

    #@19
    .line 617
    return-void
.end method

.method private startAnimationWithVelocity(III)V
    .registers 6
    .parameter "startX"
    .parameter "endX"
    .parameter "pixelsPerSecond"

    #@0
    .prologue
    .line 620
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Lcom/android/internal/widget/RotarySelector;->mAnimating:Z

    #@3
    .line 621
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    #@6
    move-result-wide v0

    #@7
    iput-wide v0, p0, Lcom/android/internal/widget/RotarySelector;->mAnimationStartTime:J

    #@9
    .line 622
    sub-int v0, p2, p1

    #@b
    mul-int/lit16 v0, v0, 0x3e8

    #@d
    div-int/2addr v0, p3

    #@e
    int-to-long v0, v0

    #@f
    iput-wide v0, p0, Lcom/android/internal/widget/RotarySelector;->mAnimationDuration:J

    #@11
    .line 623
    iput p1, p0, Lcom/android/internal/widget/RotarySelector;->mAnimatingDeltaXStart:I

    #@13
    .line 624
    iput p2, p0, Lcom/android/internal/widget/RotarySelector;->mAnimatingDeltaXEnd:I

    #@15
    .line 625
    const/4 v0, 0x0

    #@16
    invoke-direct {p0, v0}, Lcom/android/internal/widget/RotarySelector;->setGrabbedState(I)V

    #@19
    .line 626
    invoke-virtual {p0}, Lcom/android/internal/widget/RotarySelector;->invalidate()V

    #@1c
    .line 627
    return-void
.end method

.method private updateAnimation()V
    .registers 13

    #@0
    .prologue
    .line 630
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    #@3
    move-result-wide v8

    #@4
    iget-wide v10, p0, Lcom/android/internal/widget/RotarySelector;->mAnimationStartTime:J

    #@6
    sub-long v5, v8, v10

    #@8
    .line 631
    .local v5, millisSoFar:J
    iget-wide v8, p0, Lcom/android/internal/widget/RotarySelector;->mAnimationDuration:J

    #@a
    sub-long v3, v8, v5

    #@c
    .line 632
    .local v3, millisLeft:J
    iget v8, p0, Lcom/android/internal/widget/RotarySelector;->mAnimatingDeltaXStart:I

    #@e
    iget v9, p0, Lcom/android/internal/widget/RotarySelector;->mAnimatingDeltaXEnd:I

    #@10
    sub-int v7, v8, v9

    #@12
    .line 633
    .local v7, totalDeltaX:I
    if-gez v7, :cond_1f

    #@14
    const/4 v1, 0x1

    #@15
    .line 635
    .local v1, goingRight:Z
    :goto_15
    const-wide/16 v8, 0x0

    #@17
    cmp-long v8, v3, v8

    #@19
    if-gtz v8, :cond_21

    #@1b
    .line 636
    invoke-direct {p0}, Lcom/android/internal/widget/RotarySelector;->reset()V

    #@1e
    .line 658
    :goto_1e
    return-void

    #@1f
    .line 633
    .end local v1           #goingRight:Z
    :cond_1f
    const/4 v1, 0x0

    #@20
    goto :goto_15

    #@21
    .line 640
    .restart local v1       #goingRight:Z
    :cond_21
    iget-object v8, p0, Lcom/android/internal/widget/RotarySelector;->mInterpolator:Landroid/view/animation/DecelerateInterpolator;

    #@23
    long-to-float v9, v5

    #@24
    iget-wide v10, p0, Lcom/android/internal/widget/RotarySelector;->mAnimationDuration:J

    #@26
    long-to-float v10, v10

    #@27
    div-float/2addr v9, v10

    #@28
    invoke-virtual {v8, v9}, Landroid/view/animation/DecelerateInterpolator;->getInterpolation(F)F

    #@2b
    move-result v2

    #@2c
    .line 642
    .local v2, interpolation:F
    int-to-float v8, v7

    #@2d
    const/high16 v9, 0x3f80

    #@2f
    sub-float/2addr v9, v2

    #@30
    mul-float/2addr v8, v9

    #@31
    float-to-int v0, v8

    #@32
    .line 643
    .local v0, dx:I
    iget v8, p0, Lcom/android/internal/widget/RotarySelector;->mAnimatingDeltaXEnd:I

    #@34
    add-int/2addr v8, v0

    #@35
    iput v8, p0, Lcom/android/internal/widget/RotarySelector;->mRotaryOffsetX:I

    #@37
    .line 648
    iget v8, p0, Lcom/android/internal/widget/RotarySelector;->mDimplesOfFling:I

    #@39
    if-lez v8, :cond_4f

    #@3b
    .line 649
    if-nez v1, :cond_53

    #@3d
    iget v8, p0, Lcom/android/internal/widget/RotarySelector;->mRotaryOffsetX:I

    #@3f
    iget v9, p0, Lcom/android/internal/widget/RotarySelector;->mDimpleSpacing:I

    #@41
    mul-int/lit8 v9, v9, -0x3

    #@43
    if-ge v8, v9, :cond_53

    #@45
    .line 651
    iget v8, p0, Lcom/android/internal/widget/RotarySelector;->mRotaryOffsetX:I

    #@47
    iget v9, p0, Lcom/android/internal/widget/RotarySelector;->mDimplesOfFling:I

    #@49
    iget v10, p0, Lcom/android/internal/widget/RotarySelector;->mDimpleSpacing:I

    #@4b
    mul-int/2addr v9, v10

    #@4c
    add-int/2addr v8, v9

    #@4d
    iput v8, p0, Lcom/android/internal/widget/RotarySelector;->mRotaryOffsetX:I

    #@4f
    .line 657
    :cond_4f
    :goto_4f
    invoke-virtual {p0}, Lcom/android/internal/widget/RotarySelector;->invalidate()V

    #@52
    goto :goto_1e

    #@53
    .line 652
    :cond_53
    if-eqz v1, :cond_4f

    #@55
    iget v8, p0, Lcom/android/internal/widget/RotarySelector;->mRotaryOffsetX:I

    #@57
    iget v9, p0, Lcom/android/internal/widget/RotarySelector;->mDimpleSpacing:I

    #@59
    mul-int/lit8 v9, v9, 0x3

    #@5b
    if-le v8, v9, :cond_4f

    #@5d
    .line 654
    iget v8, p0, Lcom/android/internal/widget/RotarySelector;->mRotaryOffsetX:I

    #@5f
    iget v9, p0, Lcom/android/internal/widget/RotarySelector;->mDimplesOfFling:I

    #@61
    iget v10, p0, Lcom/android/internal/widget/RotarySelector;->mDimpleSpacing:I

    #@63
    mul-int/2addr v9, v10

    #@64
    sub-int/2addr v8, v9

    #@65
    iput v8, p0, Lcom/android/internal/widget/RotarySelector;->mRotaryOffsetX:I

    #@67
    goto :goto_4f
.end method

.method private declared-synchronized vibrate(J)V
    .registers 8
    .parameter "duration"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 672
    monitor-enter p0

    #@2
    :try_start_2
    iget-object v1, p0, Lcom/android/internal/widget/RotarySelector;->mContext:Landroid/content/Context;

    #@4
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7
    move-result-object v1

    #@8
    const-string v2, "haptic_feedback_enabled"

    #@a
    const/4 v3, 0x1

    #@b
    const/4 v4, -0x2

    #@c
    invoke-static {v1, v2, v3, v4}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    #@f
    move-result v1

    #@10
    if-eqz v1, :cond_2d

    #@12
    .line 675
    .local v0, hapticEnabled:Z
    :goto_12
    if-eqz v0, :cond_2b

    #@14
    .line 676
    iget-object v1, p0, Lcom/android/internal/widget/RotarySelector;->mVibrator:Landroid/os/Vibrator;

    #@16
    if-nez v1, :cond_26

    #@18
    .line 677
    invoke-virtual {p0}, Lcom/android/internal/widget/RotarySelector;->getContext()Landroid/content/Context;

    #@1b
    move-result-object v1

    #@1c
    const-string v2, "vibrator"

    #@1e
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@21
    move-result-object v1

    #@22
    check-cast v1, Landroid/os/Vibrator;

    #@24
    iput-object v1, p0, Lcom/android/internal/widget/RotarySelector;->mVibrator:Landroid/os/Vibrator;

    #@26
    .line 680
    :cond_26
    iget-object v1, p0, Lcom/android/internal/widget/RotarySelector;->mVibrator:Landroid/os/Vibrator;

    #@28
    invoke-virtual {v1, p1, p2}, Landroid/os/Vibrator;->vibrate(J)V
    :try_end_2b
    .catchall {:try_start_2 .. :try_end_2b} :catchall_2f

    #@2b
    .line 682
    :cond_2b
    monitor-exit p0

    #@2c
    return-void

    #@2d
    .line 672
    .end local v0           #hapticEnabled:Z
    :cond_2d
    const/4 v0, 0x0

    #@2e
    goto :goto_12

    #@2f
    :catchall_2f
    move-exception v1

    #@30
    monitor-exit p0

    #@31
    throw v1
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 23
    .parameter "canvas"

    #@0
    .prologue
    .line 292
    invoke-super/range {p0 .. p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    #@3
    .line 294
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/widget/RotarySelector;->getWidth()I

    #@6
    move-result v13

    #@7
    .line 303
    .local v13, width:I
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/widget/RotarySelector;->getHeight()I

    #@a
    move-result v11

    #@b
    .line 306
    .local v11, height:I
    move-object/from16 v0, p0

    #@d
    iget-boolean v0, v0, Lcom/android/internal/widget/RotarySelector;->mAnimating:Z

    #@f
    move/from16 v17, v0

    #@11
    if-eqz v17, :cond_16

    #@13
    .line 307
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/widget/RotarySelector;->updateAnimation()V

    #@16
    .line 311
    :cond_16
    move-object/from16 v0, p0

    #@18
    iget-object v0, v0, Lcom/android/internal/widget/RotarySelector;->mBackground:Landroid/graphics/Bitmap;

    #@1a
    move-object/from16 v17, v0

    #@1c
    move-object/from16 v0, p0

    #@1e
    iget-object v0, v0, Lcom/android/internal/widget/RotarySelector;->mBgMatrix:Landroid/graphics/Matrix;

    #@20
    move-object/from16 v18, v0

    #@22
    move-object/from16 v0, p0

    #@24
    iget-object v0, v0, Lcom/android/internal/widget/RotarySelector;->mPaint:Landroid/graphics/Paint;

    #@26
    move-object/from16 v19, v0

    #@28
    move-object/from16 v0, p1

    #@2a
    move-object/from16 v1, v17

    #@2c
    move-object/from16 v2, v18

    #@2e
    move-object/from16 v3, v19

    #@30
    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    #@33
    .line 314
    move-object/from16 v0, p0

    #@35
    iget-object v0, v0, Lcom/android/internal/widget/RotarySelector;->mArrowMatrix:Landroid/graphics/Matrix;

    #@37
    move-object/from16 v17, v0

    #@39
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Matrix;->reset()V

    #@3c
    .line 315
    move-object/from16 v0, p0

    #@3e
    iget v0, v0, Lcom/android/internal/widget/RotarySelector;->mGrabbedState:I

    #@40
    move/from16 v17, v0

    #@42
    packed-switch v17, :pswitch_data_38c

    #@45
    .line 337
    new-instance v17, Ljava/lang/IllegalStateException;

    #@47
    new-instance v18, Ljava/lang/StringBuilder;

    #@49
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@4c
    const-string v19, "invalid mGrabbedState: "

    #@4e
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v18

    #@52
    move-object/from16 v0, p0

    #@54
    iget v0, v0, Lcom/android/internal/widget/RotarySelector;->mGrabbedState:I

    #@56
    move/from16 v19, v0

    #@58
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v18

    #@5c
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5f
    move-result-object v18

    #@60
    invoke-direct/range {v17 .. v18}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@63
    throw v17

    #@64
    .line 320
    :pswitch_64
    move-object/from16 v0, p0

    #@66
    iget-object v0, v0, Lcom/android/internal/widget/RotarySelector;->mArrowMatrix:Landroid/graphics/Matrix;

    #@68
    move-object/from16 v17, v0

    #@6a
    const/16 v18, 0x0

    #@6c
    const/16 v19, 0x0

    #@6e
    invoke-virtual/range {v17 .. v19}, Landroid/graphics/Matrix;->setTranslate(FF)V

    #@71
    .line 321
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/widget/RotarySelector;->isHoriz()Z

    #@74
    move-result v17

    #@75
    if-nez v17, :cond_94

    #@77
    .line 322
    move-object/from16 v0, p0

    #@79
    iget-object v0, v0, Lcom/android/internal/widget/RotarySelector;->mArrowMatrix:Landroid/graphics/Matrix;

    #@7b
    move-object/from16 v17, v0

    #@7d
    const/high16 v18, -0x3d4c

    #@7f
    const/16 v19, 0x0

    #@81
    const/16 v20, 0x0

    #@83
    invoke-virtual/range {v17 .. v20}, Landroid/graphics/Matrix;->preRotate(FFF)Z

    #@86
    .line 323
    move-object/from16 v0, p0

    #@88
    iget-object v0, v0, Lcom/android/internal/widget/RotarySelector;->mArrowMatrix:Landroid/graphics/Matrix;

    #@8a
    move-object/from16 v17, v0

    #@8c
    const/16 v18, 0x0

    #@8e
    int-to-float v0, v11

    #@8f
    move/from16 v19, v0

    #@91
    invoke-virtual/range {v17 .. v19}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    #@94
    .line 325
    :cond_94
    move-object/from16 v0, p0

    #@96
    iget-object v0, v0, Lcom/android/internal/widget/RotarySelector;->mArrowLongLeft:Landroid/graphics/Bitmap;

    #@98
    move-object/from16 v17, v0

    #@9a
    move-object/from16 v0, p0

    #@9c
    iget-object v0, v0, Lcom/android/internal/widget/RotarySelector;->mArrowMatrix:Landroid/graphics/Matrix;

    #@9e
    move-object/from16 v18, v0

    #@a0
    move-object/from16 v0, p0

    #@a2
    iget-object v0, v0, Lcom/android/internal/widget/RotarySelector;->mPaint:Landroid/graphics/Paint;

    #@a4
    move-object/from16 v19, v0

    #@a6
    move-object/from16 v0, p1

    #@a8
    move-object/from16 v1, v17

    #@aa
    move-object/from16 v2, v18

    #@ac
    move-object/from16 v3, v19

    #@ae
    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    #@b1
    .line 340
    :goto_b1
    :pswitch_b1
    move-object/from16 v0, p0

    #@b3
    iget v5, v0, Lcom/android/internal/widget/RotarySelector;->mBackgroundHeight:I

    #@b5
    .line 341
    .local v5, bgHeight:I
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/widget/RotarySelector;->isHoriz()Z

    #@b8
    move-result v17

    #@b9
    if-eqz v17, :cond_29a

    #@bb
    sub-int v6, v11, v5

    #@bd
    .line 359
    .local v6, bgTop:I
    :goto_bd
    move-object/from16 v0, p0

    #@bf
    iget v0, v0, Lcom/android/internal/widget/RotarySelector;->mLeftHandleX:I

    #@c1
    move/from16 v17, v0

    #@c3
    move-object/from16 v0, p0

    #@c5
    iget v0, v0, Lcom/android/internal/widget/RotarySelector;->mRotaryOffsetX:I

    #@c7
    move/from16 v18, v0

    #@c9
    add-int v15, v17, v18

    #@cb
    .line 360
    .local v15, xOffset:I
    move-object/from16 v0, p0

    #@cd
    iget v0, v0, Lcom/android/internal/widget/RotarySelector;->mBackgroundWidth:I

    #@cf
    move/from16 v17, v0

    #@d1
    move-object/from16 v0, p0

    #@d3
    iget v0, v0, Lcom/android/internal/widget/RotarySelector;->mInnerRadius:I

    #@d5
    move/from16 v18, v0

    #@d7
    move-object/from16 v0, p0

    #@d9
    iget v0, v0, Lcom/android/internal/widget/RotarySelector;->mOuterRadius:I

    #@db
    move/from16 v19, v0

    #@dd
    move-object/from16 v0, p0

    #@df
    move/from16 v1, v17

    #@e1
    move/from16 v2, v18

    #@e3
    move/from16 v3, v19

    #@e5
    invoke-direct {v0, v1, v2, v3, v15}, Lcom/android/internal/widget/RotarySelector;->getYOnArc(IIII)I

    #@e8
    move-result v9

    #@e9
    .line 365
    .local v9, drawableY:I
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/widget/RotarySelector;->isHoriz()Z

    #@ec
    move-result v17

    #@ed
    if-eqz v17, :cond_29e

    #@ef
    move v14, v15

    #@f0
    .line 366
    .local v14, x:I
    :goto_f0
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/widget/RotarySelector;->isHoriz()Z

    #@f3
    move-result v17

    #@f4
    if-eqz v17, :cond_2a2

    #@f6
    add-int v16, v9, v6

    #@f8
    .line 367
    .local v16, y:I
    :goto_f8
    move-object/from16 v0, p0

    #@fa
    iget v0, v0, Lcom/android/internal/widget/RotarySelector;->mGrabbedState:I

    #@fc
    move/from16 v17, v0

    #@fe
    const/16 v18, 0x2

    #@100
    move/from16 v0, v17

    #@102
    move/from16 v1, v18

    #@104
    if-eq v0, v1, :cond_2a6

    #@106
    .line 368
    move-object/from16 v0, p0

    #@108
    iget-object v0, v0, Lcom/android/internal/widget/RotarySelector;->mDimple:Landroid/graphics/Bitmap;

    #@10a
    move-object/from16 v17, v0

    #@10c
    move-object/from16 v0, p0

    #@10e
    move-object/from16 v1, v17

    #@110
    move-object/from16 v2, p1

    #@112
    move/from16 v3, v16

    #@114
    invoke-direct {v0, v1, v2, v14, v3}, Lcom/android/internal/widget/RotarySelector;->drawCentered(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;II)V

    #@117
    .line 369
    move-object/from16 v0, p0

    #@119
    iget-object v0, v0, Lcom/android/internal/widget/RotarySelector;->mLeftHandleIcon:Landroid/graphics/Bitmap;

    #@11b
    move-object/from16 v17, v0

    #@11d
    move-object/from16 v0, p0

    #@11f
    move-object/from16 v1, v17

    #@121
    move-object/from16 v2, p1

    #@123
    move/from16 v3, v16

    #@125
    invoke-direct {v0, v1, v2, v14, v3}, Lcom/android/internal/widget/RotarySelector;->drawCentered(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;II)V

    #@128
    .line 377
    :goto_128
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/widget/RotarySelector;->isHoriz()Z

    #@12b
    move-result v17

    #@12c
    if-eqz v17, :cond_2b9

    #@12e
    div-int/lit8 v17, v13, 0x2

    #@130
    move-object/from16 v0, p0

    #@132
    iget v0, v0, Lcom/android/internal/widget/RotarySelector;->mRotaryOffsetX:I

    #@134
    move/from16 v18, v0

    #@136
    add-int v15, v17, v18

    #@138
    .line 380
    :goto_138
    move-object/from16 v0, p0

    #@13a
    iget v0, v0, Lcom/android/internal/widget/RotarySelector;->mBackgroundWidth:I

    #@13c
    move/from16 v17, v0

    #@13e
    move-object/from16 v0, p0

    #@140
    iget v0, v0, Lcom/android/internal/widget/RotarySelector;->mInnerRadius:I

    #@142
    move/from16 v18, v0

    #@144
    move-object/from16 v0, p0

    #@146
    iget v0, v0, Lcom/android/internal/widget/RotarySelector;->mOuterRadius:I

    #@148
    move/from16 v19, v0

    #@14a
    move-object/from16 v0, p0

    #@14c
    move/from16 v1, v17

    #@14e
    move/from16 v2, v18

    #@150
    move/from16 v3, v19

    #@152
    invoke-direct {v0, v1, v2, v3, v15}, Lcom/android/internal/widget/RotarySelector;->getYOnArc(IIII)I

    #@155
    move-result v9

    #@156
    .line 386
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/widget/RotarySelector;->isHoriz()Z

    #@159
    move-result v17

    #@15a
    if-eqz v17, :cond_2c5

    #@15c
    .line 387
    move-object/from16 v0, p0

    #@15e
    iget-object v0, v0, Lcom/android/internal/widget/RotarySelector;->mDimpleDim:Landroid/graphics/Bitmap;

    #@160
    move-object/from16 v17, v0

    #@162
    add-int v18, v9, v6

    #@164
    move-object/from16 v0, p0

    #@166
    move-object/from16 v1, v17

    #@168
    move-object/from16 v2, p1

    #@16a
    move/from16 v3, v18

    #@16c
    invoke-direct {v0, v1, v2, v15, v3}, Lcom/android/internal/widget/RotarySelector;->drawCentered(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;II)V

    #@16f
    .line 396
    :goto_16f
    move-object/from16 v0, p0

    #@171
    iget v0, v0, Lcom/android/internal/widget/RotarySelector;->mRightHandleX:I

    #@173
    move/from16 v17, v0

    #@175
    move-object/from16 v0, p0

    #@177
    iget v0, v0, Lcom/android/internal/widget/RotarySelector;->mRotaryOffsetX:I

    #@179
    move/from16 v18, v0

    #@17b
    add-int v15, v17, v18

    #@17d
    .line 397
    move-object/from16 v0, p0

    #@17f
    iget v0, v0, Lcom/android/internal/widget/RotarySelector;->mBackgroundWidth:I

    #@181
    move/from16 v17, v0

    #@183
    move-object/from16 v0, p0

    #@185
    iget v0, v0, Lcom/android/internal/widget/RotarySelector;->mInnerRadius:I

    #@187
    move/from16 v18, v0

    #@189
    move-object/from16 v0, p0

    #@18b
    iget v0, v0, Lcom/android/internal/widget/RotarySelector;->mOuterRadius:I

    #@18d
    move/from16 v19, v0

    #@18f
    move-object/from16 v0, p0

    #@191
    move/from16 v1, v17

    #@193
    move/from16 v2, v18

    #@195
    move/from16 v3, v19

    #@197
    invoke-direct {v0, v1, v2, v3, v15}, Lcom/android/internal/widget/RotarySelector;->getYOnArc(IIII)I

    #@19a
    move-result v9

    #@19b
    .line 403
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/widget/RotarySelector;->isHoriz()Z

    #@19e
    move-result v17

    #@19f
    if-eqz v17, :cond_2de

    #@1a1
    move v14, v15

    #@1a2
    .line 404
    :goto_1a2
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/widget/RotarySelector;->isHoriz()Z

    #@1a5
    move-result v17

    #@1a6
    if-eqz v17, :cond_2e2

    #@1a8
    add-int v16, v9, v6

    #@1aa
    .line 405
    :goto_1aa
    move-object/from16 v0, p0

    #@1ac
    iget v0, v0, Lcom/android/internal/widget/RotarySelector;->mGrabbedState:I

    #@1ae
    move/from16 v17, v0

    #@1b0
    const/16 v18, 0x1

    #@1b2
    move/from16 v0, v17

    #@1b4
    move/from16 v1, v18

    #@1b6
    if-eq v0, v1, :cond_2e6

    #@1b8
    .line 406
    move-object/from16 v0, p0

    #@1ba
    iget-object v0, v0, Lcom/android/internal/widget/RotarySelector;->mDimple:Landroid/graphics/Bitmap;

    #@1bc
    move-object/from16 v17, v0

    #@1be
    move-object/from16 v0, p0

    #@1c0
    move-object/from16 v1, v17

    #@1c2
    move-object/from16 v2, p1

    #@1c4
    move/from16 v3, v16

    #@1c6
    invoke-direct {v0, v1, v2, v14, v3}, Lcom/android/internal/widget/RotarySelector;->drawCentered(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;II)V

    #@1c9
    .line 407
    move-object/from16 v0, p0

    #@1cb
    iget-object v0, v0, Lcom/android/internal/widget/RotarySelector;->mRightHandleIcon:Landroid/graphics/Bitmap;

    #@1cd
    move-object/from16 v17, v0

    #@1cf
    move-object/from16 v0, p0

    #@1d1
    move-object/from16 v1, v17

    #@1d3
    move-object/from16 v2, p1

    #@1d5
    move/from16 v3, v16

    #@1d7
    invoke-direct {v0, v1, v2, v14, v3}, Lcom/android/internal/widget/RotarySelector;->drawCentered(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;II)V

    #@1da
    .line 414
    :goto_1da
    move-object/from16 v0, p0

    #@1dc
    iget v0, v0, Lcom/android/internal/widget/RotarySelector;->mRotaryOffsetX:I

    #@1de
    move/from16 v17, v0

    #@1e0
    move-object/from16 v0, p0

    #@1e2
    iget v0, v0, Lcom/android/internal/widget/RotarySelector;->mLeftHandleX:I

    #@1e4
    move/from16 v18, v0

    #@1e6
    add-int v17, v17, v18

    #@1e8
    move-object/from16 v0, p0

    #@1ea
    iget v0, v0, Lcom/android/internal/widget/RotarySelector;->mDimpleSpacing:I

    #@1ec
    move/from16 v18, v0

    #@1ee
    sub-int v7, v17, v18

    #@1f0
    .line 415
    .local v7, dimpleLeft:I
    move-object/from16 v0, p0

    #@1f2
    iget v0, v0, Lcom/android/internal/widget/RotarySelector;->mDimpleWidth:I

    #@1f4
    move/from16 v17, v0

    #@1f6
    div-int/lit8 v10, v17, 0x2

    #@1f8
    .line 416
    .local v10, halfdimple:I
    :goto_1f8
    neg-int v0, v10

    #@1f9
    move/from16 v17, v0

    #@1fb
    move/from16 v0, v17

    #@1fd
    if-le v7, v0, :cond_312

    #@1ff
    .line 417
    move-object/from16 v0, p0

    #@201
    iget v0, v0, Lcom/android/internal/widget/RotarySelector;->mBackgroundWidth:I

    #@203
    move/from16 v17, v0

    #@205
    move-object/from16 v0, p0

    #@207
    iget v0, v0, Lcom/android/internal/widget/RotarySelector;->mInnerRadius:I

    #@209
    move/from16 v18, v0

    #@20b
    move-object/from16 v0, p0

    #@20d
    iget v0, v0, Lcom/android/internal/widget/RotarySelector;->mOuterRadius:I

    #@20f
    move/from16 v19, v0

    #@211
    move-object/from16 v0, p0

    #@213
    move/from16 v1, v17

    #@215
    move/from16 v2, v18

    #@217
    move/from16 v3, v19

    #@219
    invoke-direct {v0, v1, v2, v3, v7}, Lcom/android/internal/widget/RotarySelector;->getYOnArc(IIII)I

    #@21c
    move-result v9

    #@21d
    .line 423
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/widget/RotarySelector;->isHoriz()Z

    #@220
    move-result v17

    #@221
    if-eqz v17, :cond_2f9

    #@223
    .line 424
    move-object/from16 v0, p0

    #@225
    iget-object v0, v0, Lcom/android/internal/widget/RotarySelector;->mDimpleDim:Landroid/graphics/Bitmap;

    #@227
    move-object/from16 v17, v0

    #@229
    add-int v18, v9, v6

    #@22b
    move-object/from16 v0, p0

    #@22d
    move-object/from16 v1, v17

    #@22f
    move-object/from16 v2, p1

    #@231
    move/from16 v3, v18

    #@233
    invoke-direct {v0, v1, v2, v7, v3}, Lcom/android/internal/widget/RotarySelector;->drawCentered(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;II)V

    #@236
    .line 428
    :goto_236
    move-object/from16 v0, p0

    #@238
    iget v0, v0, Lcom/android/internal/widget/RotarySelector;->mDimpleSpacing:I

    #@23a
    move/from16 v17, v0

    #@23c
    sub-int v7, v7, v17

    #@23e
    .line 429
    goto :goto_1f8

    #@23f
    .line 328
    .end local v5           #bgHeight:I
    .end local v6           #bgTop:I
    .end local v7           #dimpleLeft:I
    .end local v9           #drawableY:I
    .end local v10           #halfdimple:I
    .end local v14           #x:I
    .end local v15           #xOffset:I
    .end local v16           #y:I
    :pswitch_23f
    move-object/from16 v0, p0

    #@241
    iget-object v0, v0, Lcom/android/internal/widget/RotarySelector;->mArrowMatrix:Landroid/graphics/Matrix;

    #@243
    move-object/from16 v17, v0

    #@245
    const/16 v18, 0x0

    #@247
    const/16 v19, 0x0

    #@249
    invoke-virtual/range {v17 .. v19}, Landroid/graphics/Matrix;->setTranslate(FF)V

    #@24c
    .line 329
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/widget/RotarySelector;->isHoriz()Z

    #@24f
    move-result v17

    #@250
    if-nez v17, :cond_27b

    #@252
    .line 330
    move-object/from16 v0, p0

    #@254
    iget-object v0, v0, Lcom/android/internal/widget/RotarySelector;->mArrowMatrix:Landroid/graphics/Matrix;

    #@256
    move-object/from16 v17, v0

    #@258
    const/high16 v18, -0x3d4c

    #@25a
    const/16 v19, 0x0

    #@25c
    const/16 v20, 0x0

    #@25e
    invoke-virtual/range {v17 .. v20}, Landroid/graphics/Matrix;->preRotate(FFF)Z

    #@261
    .line 332
    move-object/from16 v0, p0

    #@263
    iget-object v0, v0, Lcom/android/internal/widget/RotarySelector;->mArrowMatrix:Landroid/graphics/Matrix;

    #@265
    move-object/from16 v17, v0

    #@267
    const/16 v18, 0x0

    #@269
    move-object/from16 v0, p0

    #@26b
    iget v0, v0, Lcom/android/internal/widget/RotarySelector;->mBackgroundWidth:I

    #@26d
    move/from16 v19, v0

    #@26f
    sub-int v19, v19, v11

    #@271
    add-int v19, v19, v11

    #@273
    move/from16 v0, v19

    #@275
    int-to-float v0, v0

    #@276
    move/from16 v19, v0

    #@278
    invoke-virtual/range {v17 .. v19}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    #@27b
    .line 334
    :cond_27b
    move-object/from16 v0, p0

    #@27d
    iget-object v0, v0, Lcom/android/internal/widget/RotarySelector;->mArrowLongRight:Landroid/graphics/Bitmap;

    #@27f
    move-object/from16 v17, v0

    #@281
    move-object/from16 v0, p0

    #@283
    iget-object v0, v0, Lcom/android/internal/widget/RotarySelector;->mArrowMatrix:Landroid/graphics/Matrix;

    #@285
    move-object/from16 v18, v0

    #@287
    move-object/from16 v0, p0

    #@289
    iget-object v0, v0, Lcom/android/internal/widget/RotarySelector;->mPaint:Landroid/graphics/Paint;

    #@28b
    move-object/from16 v19, v0

    #@28d
    move-object/from16 v0, p1

    #@28f
    move-object/from16 v1, v17

    #@291
    move-object/from16 v2, v18

    #@293
    move-object/from16 v3, v19

    #@295
    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    #@298
    goto/16 :goto_b1

    #@29a
    .line 341
    .restart local v5       #bgHeight:I
    :cond_29a
    sub-int v6, v13, v5

    #@29c
    goto/16 :goto_bd

    #@29e
    .line 365
    .restart local v6       #bgTop:I
    .restart local v9       #drawableY:I
    .restart local v15       #xOffset:I
    :cond_29e
    add-int v14, v9, v6

    #@2a0
    goto/16 :goto_f0

    #@2a2
    .line 366
    .restart local v14       #x:I
    :cond_2a2
    sub-int v16, v11, v15

    #@2a4
    goto/16 :goto_f8

    #@2a6
    .line 371
    .restart local v16       #y:I
    :cond_2a6
    move-object/from16 v0, p0

    #@2a8
    iget-object v0, v0, Lcom/android/internal/widget/RotarySelector;->mDimpleDim:Landroid/graphics/Bitmap;

    #@2aa
    move-object/from16 v17, v0

    #@2ac
    move-object/from16 v0, p0

    #@2ae
    move-object/from16 v1, v17

    #@2b0
    move-object/from16 v2, p1

    #@2b2
    move/from16 v3, v16

    #@2b4
    invoke-direct {v0, v1, v2, v14, v3}, Lcom/android/internal/widget/RotarySelector;->drawCentered(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;II)V

    #@2b7
    goto/16 :goto_128

    #@2b9
    .line 377
    :cond_2b9
    div-int/lit8 v17, v11, 0x2

    #@2bb
    move-object/from16 v0, p0

    #@2bd
    iget v0, v0, Lcom/android/internal/widget/RotarySelector;->mRotaryOffsetX:I

    #@2bf
    move/from16 v18, v0

    #@2c1
    add-int v15, v17, v18

    #@2c3
    goto/16 :goto_138

    #@2c5
    .line 390
    :cond_2c5
    move-object/from16 v0, p0

    #@2c7
    iget-object v0, v0, Lcom/android/internal/widget/RotarySelector;->mDimpleDim:Landroid/graphics/Bitmap;

    #@2c9
    move-object/from16 v17, v0

    #@2cb
    add-int v18, v9, v6

    #@2cd
    sub-int v19, v11, v15

    #@2cf
    move-object/from16 v0, p0

    #@2d1
    move-object/from16 v1, v17

    #@2d3
    move-object/from16 v2, p1

    #@2d5
    move/from16 v3, v18

    #@2d7
    move/from16 v4, v19

    #@2d9
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/internal/widget/RotarySelector;->drawCentered(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;II)V

    #@2dc
    goto/16 :goto_16f

    #@2de
    .line 403
    :cond_2de
    add-int v14, v9, v6

    #@2e0
    goto/16 :goto_1a2

    #@2e2
    .line 404
    :cond_2e2
    sub-int v16, v11, v15

    #@2e4
    goto/16 :goto_1aa

    #@2e6
    .line 409
    :cond_2e6
    move-object/from16 v0, p0

    #@2e8
    iget-object v0, v0, Lcom/android/internal/widget/RotarySelector;->mDimpleDim:Landroid/graphics/Bitmap;

    #@2ea
    move-object/from16 v17, v0

    #@2ec
    move-object/from16 v0, p0

    #@2ee
    move-object/from16 v1, v17

    #@2f0
    move-object/from16 v2, p1

    #@2f2
    move/from16 v3, v16

    #@2f4
    invoke-direct {v0, v1, v2, v14, v3}, Lcom/android/internal/widget/RotarySelector;->drawCentered(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;II)V

    #@2f7
    goto/16 :goto_1da

    #@2f9
    .line 426
    .restart local v7       #dimpleLeft:I
    .restart local v10       #halfdimple:I
    :cond_2f9
    move-object/from16 v0, p0

    #@2fb
    iget-object v0, v0, Lcom/android/internal/widget/RotarySelector;->mDimpleDim:Landroid/graphics/Bitmap;

    #@2fd
    move-object/from16 v17, v0

    #@2ff
    add-int v18, v9, v6

    #@301
    sub-int v19, v11, v7

    #@303
    move-object/from16 v0, p0

    #@305
    move-object/from16 v1, v17

    #@307
    move-object/from16 v2, p1

    #@309
    move/from16 v3, v18

    #@30b
    move/from16 v4, v19

    #@30d
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/internal/widget/RotarySelector;->drawCentered(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;II)V

    #@310
    goto/16 :goto_236

    #@312
    .line 432
    :cond_312
    move-object/from16 v0, p0

    #@314
    iget v0, v0, Lcom/android/internal/widget/RotarySelector;->mRotaryOffsetX:I

    #@316
    move/from16 v17, v0

    #@318
    move-object/from16 v0, p0

    #@31a
    iget v0, v0, Lcom/android/internal/widget/RotarySelector;->mRightHandleX:I

    #@31c
    move/from16 v18, v0

    #@31e
    add-int v17, v17, v18

    #@320
    move-object/from16 v0, p0

    #@322
    iget v0, v0, Lcom/android/internal/widget/RotarySelector;->mDimpleSpacing:I

    #@324
    move/from16 v18, v0

    #@326
    add-int v8, v17, v18

    #@328
    .line 433
    .local v8, dimpleRight:I
    move-object/from16 v0, p0

    #@32a
    iget v0, v0, Lcom/android/internal/widget/RotarySelector;->mRight:I

    #@32c
    move/from16 v17, v0

    #@32e
    add-int v12, v17, v10

    #@330
    .line 434
    .local v12, rightThresh:I
    :goto_330
    if-ge v8, v12, :cond_38a

    #@332
    .line 435
    move-object/from16 v0, p0

    #@334
    iget v0, v0, Lcom/android/internal/widget/RotarySelector;->mBackgroundWidth:I

    #@336
    move/from16 v17, v0

    #@338
    move-object/from16 v0, p0

    #@33a
    iget v0, v0, Lcom/android/internal/widget/RotarySelector;->mInnerRadius:I

    #@33c
    move/from16 v18, v0

    #@33e
    move-object/from16 v0, p0

    #@340
    iget v0, v0, Lcom/android/internal/widget/RotarySelector;->mOuterRadius:I

    #@342
    move/from16 v19, v0

    #@344
    move-object/from16 v0, p0

    #@346
    move/from16 v1, v17

    #@348
    move/from16 v2, v18

    #@34a
    move/from16 v3, v19

    #@34c
    invoke-direct {v0, v1, v2, v3, v8}, Lcom/android/internal/widget/RotarySelector;->getYOnArc(IIII)I

    #@34f
    move-result v9

    #@350
    .line 441
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/widget/RotarySelector;->isHoriz()Z

    #@353
    move-result v17

    #@354
    if-eqz v17, :cond_372

    #@356
    .line 442
    move-object/from16 v0, p0

    #@358
    iget-object v0, v0, Lcom/android/internal/widget/RotarySelector;->mDimpleDim:Landroid/graphics/Bitmap;

    #@35a
    move-object/from16 v17, v0

    #@35c
    add-int v18, v9, v6

    #@35e
    move-object/from16 v0, p0

    #@360
    move-object/from16 v1, v17

    #@362
    move-object/from16 v2, p1

    #@364
    move/from16 v3, v18

    #@366
    invoke-direct {v0, v1, v2, v8, v3}, Lcom/android/internal/widget/RotarySelector;->drawCentered(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;II)V

    #@369
    .line 446
    :goto_369
    move-object/from16 v0, p0

    #@36b
    iget v0, v0, Lcom/android/internal/widget/RotarySelector;->mDimpleSpacing:I

    #@36d
    move/from16 v17, v0

    #@36f
    add-int v8, v8, v17

    #@371
    .line 447
    goto :goto_330

    #@372
    .line 444
    :cond_372
    move-object/from16 v0, p0

    #@374
    iget-object v0, v0, Lcom/android/internal/widget/RotarySelector;->mDimpleDim:Landroid/graphics/Bitmap;

    #@376
    move-object/from16 v17, v0

    #@378
    add-int v18, v9, v6

    #@37a
    sub-int v19, v11, v8

    #@37c
    move-object/from16 v0, p0

    #@37e
    move-object/from16 v1, v17

    #@380
    move-object/from16 v2, p1

    #@382
    move/from16 v3, v18

    #@384
    move/from16 v4, v19

    #@386
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/internal/widget/RotarySelector;->drawCentered(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;II)V

    #@389
    goto :goto_369

    #@38a
    .line 448
    :cond_38a
    return-void

    #@38b
    .line 315
    nop

    #@38c
    :pswitch_data_38c
    .packed-switch 0x0
        :pswitch_b1
        :pswitch_64
        :pswitch_23f
    .end packed-switch
.end method

.method protected onMeasure(II)V
    .registers 9
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    .line 271
    invoke-direct {p0}, Lcom/android/internal/widget/RotarySelector;->isHoriz()Z

    #@3
    move-result v4

    #@4
    if-eqz v4, :cond_25

    #@6
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@9
    move-result v3

    #@a
    .line 274
    .local v3, length:I
    :goto_a
    const/high16 v4, 0x40c0

    #@c
    iget v5, p0, Lcom/android/internal/widget/RotarySelector;->mDensity:F

    #@e
    mul-float/2addr v4, v5

    #@f
    float-to-int v1, v4

    #@10
    .line 275
    .local v1, arrowScrunch:I
    iget-object v4, p0, Lcom/android/internal/widget/RotarySelector;->mArrowShortLeftAndRight:Landroid/graphics/Bitmap;

    #@12
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    #@15
    move-result v0

    #@16
    .line 281
    .local v0, arrowH:I
    iget v4, p0, Lcom/android/internal/widget/RotarySelector;->mBackgroundHeight:I

    #@18
    add-int/2addr v4, v0

    #@19
    sub-int v2, v4, v1

    #@1b
    .line 283
    .local v2, height:I
    invoke-direct {p0}, Lcom/android/internal/widget/RotarySelector;->isHoriz()Z

    #@1e
    move-result v4

    #@1f
    if-eqz v4, :cond_2a

    #@21
    .line 284
    invoke-virtual {p0, v3, v2}, Lcom/android/internal/widget/RotarySelector;->setMeasuredDimension(II)V

    #@24
    .line 288
    :goto_24
    return-void

    #@25
    .line 271
    .end local v0           #arrowH:I
    .end local v1           #arrowScrunch:I
    .end local v2           #height:I
    .end local v3           #length:I
    :cond_25
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@28
    move-result v3

    #@29
    goto :goto_a

    #@2a
    .line 286
    .restart local v0       #arrowH:I
    .restart local v1       #arrowScrunch:I
    .restart local v2       #height:I
    .restart local v3       #length:I
    :cond_2a
    invoke-virtual {p0, v2, v3}, Lcom/android/internal/widget/RotarySelector;->setMeasuredDimension(II)V

    #@2d
    goto :goto_24
.end method

.method protected onSizeChanged(IIII)V
    .registers 11
    .parameter "w"
    .parameter "h"
    .parameter "oldw"
    .parameter "oldh"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 213
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    #@4
    .line 215
    const/high16 v3, 0x4110

    #@6
    iget v4, p0, Lcom/android/internal/widget/RotarySelector;->mDensity:F

    #@8
    mul-float/2addr v3, v4

    #@9
    float-to-int v0, v3

    #@a
    .line 216
    .local v0, edgePadding:I
    iget v3, p0, Lcom/android/internal/widget/RotarySelector;->mDimpleWidth:I

    #@c
    div-int/lit8 v3, v3, 0x2

    #@e
    add-int/2addr v3, v0

    #@f
    iput v3, p0, Lcom/android/internal/widget/RotarySelector;->mLeftHandleX:I

    #@11
    .line 217
    invoke-direct {p0}, Lcom/android/internal/widget/RotarySelector;->isHoriz()Z

    #@14
    move-result v3

    #@15
    if-eqz v3, :cond_46

    #@17
    move v2, p1

    #@18
    .line 218
    .local v2, length:I
    :goto_18
    sub-int v3, v2, v0

    #@1a
    iget v4, p0, Lcom/android/internal/widget/RotarySelector;->mDimpleWidth:I

    #@1c
    div-int/lit8 v4, v4, 0x2

    #@1e
    sub-int/2addr v3, v4

    #@1f
    iput v3, p0, Lcom/android/internal/widget/RotarySelector;->mRightHandleX:I

    #@21
    .line 219
    div-int/lit8 v3, v2, 0x2

    #@23
    iget v4, p0, Lcom/android/internal/widget/RotarySelector;->mLeftHandleX:I

    #@25
    sub-int/2addr v3, v4

    #@26
    iput v3, p0, Lcom/android/internal/widget/RotarySelector;->mDimpleSpacing:I

    #@28
    .line 222
    iget-object v3, p0, Lcom/android/internal/widget/RotarySelector;->mBgMatrix:Landroid/graphics/Matrix;

    #@2a
    invoke-virtual {v3, v5, v5}, Landroid/graphics/Matrix;->setTranslate(FF)V

    #@2d
    .line 223
    invoke-direct {p0}, Lcom/android/internal/widget/RotarySelector;->isHoriz()Z

    #@30
    move-result v3

    #@31
    if-nez v3, :cond_48

    #@33
    .line 225
    iget v3, p0, Lcom/android/internal/widget/RotarySelector;->mBackgroundHeight:I

    #@35
    sub-int v1, p1, v3

    #@37
    .line 226
    .local v1, left:I
    iget-object v3, p0, Lcom/android/internal/widget/RotarySelector;->mBgMatrix:Landroid/graphics/Matrix;

    #@39
    const/high16 v4, -0x3d4c

    #@3b
    invoke-virtual {v3, v4, v5, v5}, Landroid/graphics/Matrix;->preRotate(FFF)Z

    #@3e
    .line 227
    iget-object v3, p0, Lcom/android/internal/widget/RotarySelector;->mBgMatrix:Landroid/graphics/Matrix;

    #@40
    int-to-float v4, v1

    #@41
    int-to-float v5, p2

    #@42
    invoke-virtual {v3, v4, v5}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    #@45
    .line 232
    .end local v1           #left:I
    :goto_45
    return-void

    #@46
    .end local v2           #length:I
    :cond_46
    move v2, p2

    #@47
    .line 217
    goto :goto_18

    #@48
    .line 230
    .restart local v2       #length:I
    :cond_48
    iget-object v3, p0, Lcom/android/internal/widget/RotarySelector;->mBgMatrix:Landroid/graphics/Matrix;

    #@4a
    iget v4, p0, Lcom/android/internal/widget/RotarySelector;->mBackgroundHeight:I

    #@4c
    sub-int v4, p2, v4

    #@4e
    int-to-float v4, v4

    #@4f
    invoke-virtual {v3, v5, v4}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    #@52
    goto :goto_45
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 16
    .parameter "event"

    #@0
    .prologue
    const/16 v13, 0x8

    #@2
    const/4 v12, 0x5

    #@3
    const/4 v9, 0x2

    #@4
    const/4 v10, 0x0

    #@5
    const/4 v11, 0x1

    #@6
    .line 493
    iget-boolean v8, p0, Lcom/android/internal/widget/RotarySelector;->mAnimating:Z

    #@8
    if-eqz v8, :cond_b

    #@a
    .line 605
    :cond_a
    :goto_a
    return v11

    #@b
    .line 496
    :cond_b
    iget-object v8, p0, Lcom/android/internal/widget/RotarySelector;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@d
    if-nez v8, :cond_15

    #@f
    .line 497
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    #@12
    move-result-object v8

    #@13
    iput-object v8, p0, Lcom/android/internal/widget/RotarySelector;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@15
    .line 499
    :cond_15
    iget-object v8, p0, Lcom/android/internal/widget/RotarySelector;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@17
    invoke-virtual {v8, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    #@1a
    .line 501
    invoke-virtual {p0}, Lcom/android/internal/widget/RotarySelector;->getHeight()I

    #@1d
    move-result v2

    #@1e
    .line 503
    .local v2, height:I
    invoke-direct {p0}, Lcom/android/internal/widget/RotarySelector;->isHoriz()Z

    #@21
    move-result v8

    #@22
    if-eqz v8, :cond_56

    #@24
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@27
    move-result v8

    #@28
    float-to-int v1, v8

    #@29
    .line 506
    .local v1, eventX:I
    :goto_29
    iget v3, p0, Lcom/android/internal/widget/RotarySelector;->mDimpleWidth:I

    #@2b
    .line 508
    .local v3, hitWindow:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@2e
    move-result v0

    #@2f
    .line 509
    .local v0, action:I
    packed-switch v0, :pswitch_data_18e

    #@32
    goto :goto_a

    #@33
    .line 512
    :pswitch_33
    iput-boolean v10, p0, Lcom/android/internal/widget/RotarySelector;->mTriggered:Z

    #@35
    .line 513
    iget v8, p0, Lcom/android/internal/widget/RotarySelector;->mGrabbedState:I

    #@37
    if-eqz v8, :cond_3f

    #@39
    .line 514
    invoke-direct {p0}, Lcom/android/internal/widget/RotarySelector;->reset()V

    #@3c
    .line 515
    invoke-virtual {p0}, Lcom/android/internal/widget/RotarySelector;->invalidate()V

    #@3f
    .line 517
    :cond_3f
    iget v8, p0, Lcom/android/internal/widget/RotarySelector;->mLeftHandleX:I

    #@41
    add-int/2addr v8, v3

    #@42
    if-ge v1, v8, :cond_5e

    #@44
    .line 518
    iget v8, p0, Lcom/android/internal/widget/RotarySelector;->mLeftHandleX:I

    #@46
    sub-int v8, v1, v8

    #@48
    iput v8, p0, Lcom/android/internal/widget/RotarySelector;->mRotaryOffsetX:I

    #@4a
    .line 519
    invoke-direct {p0, v11}, Lcom/android/internal/widget/RotarySelector;->setGrabbedState(I)V

    #@4d
    .line 520
    invoke-virtual {p0}, Lcom/android/internal/widget/RotarySelector;->invalidate()V

    #@50
    .line 521
    const-wide/16 v8, 0x14

    #@52
    invoke-direct {p0, v8, v9}, Lcom/android/internal/widget/RotarySelector;->vibrate(J)V

    #@55
    goto :goto_a

    #@56
    .line 503
    .end local v0           #action:I
    .end local v1           #eventX:I
    .end local v3           #hitWindow:I
    :cond_56
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@59
    move-result v8

    #@5a
    float-to-int v8, v8

    #@5b
    sub-int v1, v2, v8

    #@5d
    goto :goto_29

    #@5e
    .line 522
    .restart local v0       #action:I
    .restart local v1       #eventX:I
    .restart local v3       #hitWindow:I
    :cond_5e
    iget v8, p0, Lcom/android/internal/widget/RotarySelector;->mRightHandleX:I

    #@60
    sub-int/2addr v8, v3

    #@61
    if-le v1, v8, :cond_a

    #@63
    .line 523
    iget v8, p0, Lcom/android/internal/widget/RotarySelector;->mRightHandleX:I

    #@65
    sub-int v8, v1, v8

    #@67
    iput v8, p0, Lcom/android/internal/widget/RotarySelector;->mRotaryOffsetX:I

    #@69
    .line 524
    invoke-direct {p0, v9}, Lcom/android/internal/widget/RotarySelector;->setGrabbedState(I)V

    #@6c
    .line 525
    invoke-virtual {p0}, Lcom/android/internal/widget/RotarySelector;->invalidate()V

    #@6f
    .line 526
    const-wide/16 v8, 0x14

    #@71
    invoke-direct {p0, v8, v9}, Lcom/android/internal/widget/RotarySelector;->vibrate(J)V

    #@74
    goto :goto_a

    #@75
    .line 532
    :pswitch_75
    iget v8, p0, Lcom/android/internal/widget/RotarySelector;->mGrabbedState:I

    #@77
    if-ne v8, v11, :cond_db

    #@79
    .line 533
    iget v8, p0, Lcom/android/internal/widget/RotarySelector;->mLeftHandleX:I

    #@7b
    sub-int v8, v1, v8

    #@7d
    iput v8, p0, Lcom/android/internal/widget/RotarySelector;->mRotaryOffsetX:I

    #@7f
    .line 534
    invoke-virtual {p0}, Lcom/android/internal/widget/RotarySelector;->invalidate()V

    #@82
    .line 535
    invoke-direct {p0}, Lcom/android/internal/widget/RotarySelector;->isHoriz()Z

    #@85
    move-result v8

    #@86
    if-eqz v8, :cond_d2

    #@88
    invoke-virtual {p0}, Lcom/android/internal/widget/RotarySelector;->getRight()I

    #@8b
    move-result v5

    #@8c
    .line 536
    .local v5, rightThresh:I
    :goto_8c
    iget v8, p0, Lcom/android/internal/widget/RotarySelector;->mEdgeTriggerThresh:I

    #@8e
    sub-int v8, v5, v8

    #@90
    if-lt v1, v8, :cond_a

    #@92
    iget-boolean v8, p0, Lcom/android/internal/widget/RotarySelector;->mTriggered:Z

    #@94
    if-nez v8, :cond_a

    #@96
    .line 537
    iput-boolean v11, p0, Lcom/android/internal/widget/RotarySelector;->mTriggered:Z

    #@98
    .line 538
    invoke-direct {p0, v11}, Lcom/android/internal/widget/RotarySelector;->dispatchTriggerEvent(I)V

    #@9b
    .line 539
    iget-object v7, p0, Lcom/android/internal/widget/RotarySelector;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@9d
    .line 540
    .local v7, velocityTracker:Landroid/view/VelocityTracker;
    const/16 v8, 0x3e8

    #@9f
    iget v9, p0, Lcom/android/internal/widget/RotarySelector;->mMaximumVelocity:I

    #@a1
    int-to-float v9, v9

    #@a2
    invoke-virtual {v7, v8, v9}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    #@a5
    .line 541
    invoke-direct {p0}, Lcom/android/internal/widget/RotarySelector;->isHoriz()Z

    #@a8
    move-result v8

    #@a9
    if-eqz v8, :cond_d4

    #@ab
    invoke-virtual {v7}, Landroid/view/VelocityTracker;->getXVelocity()F

    #@ae
    move-result v8

    #@af
    float-to-int v4, v8

    #@b0
    .line 544
    .local v4, rawVelocity:I
    :goto_b0
    iget v8, p0, Lcom/android/internal/widget/RotarySelector;->mMinimumVelocity:I

    #@b2
    invoke-static {v8, v4}, Ljava/lang/Math;->max(II)I

    #@b5
    move-result v6

    #@b6
    .line 545
    .local v6, velocity:I
    iget v8, p0, Lcom/android/internal/widget/RotarySelector;->mDimpleSpacing:I

    #@b8
    div-int v8, v6, v8

    #@ba
    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    #@bd
    move-result v8

    #@be
    invoke-static {v13, v8}, Ljava/lang/Math;->max(II)I

    #@c1
    move-result v8

    #@c2
    iput v8, p0, Lcom/android/internal/widget/RotarySelector;->mDimplesOfFling:I

    #@c4
    .line 548
    iget v8, p0, Lcom/android/internal/widget/RotarySelector;->mLeftHandleX:I

    #@c6
    sub-int v8, v1, v8

    #@c8
    iget v9, p0, Lcom/android/internal/widget/RotarySelector;->mDimplesOfFling:I

    #@ca
    iget v10, p0, Lcom/android/internal/widget/RotarySelector;->mDimpleSpacing:I

    #@cc
    mul-int/2addr v9, v10

    #@cd
    invoke-direct {p0, v8, v9, v6}, Lcom/android/internal/widget/RotarySelector;->startAnimationWithVelocity(III)V

    #@d0
    goto/16 :goto_a

    #@d2
    .end local v4           #rawVelocity:I
    .end local v5           #rightThresh:I
    .end local v6           #velocity:I
    .end local v7           #velocityTracker:Landroid/view/VelocityTracker;
    :cond_d2
    move v5, v2

    #@d3
    .line 535
    goto :goto_8c

    #@d4
    .line 541
    .restart local v5       #rightThresh:I
    .restart local v7       #velocityTracker:Landroid/view/VelocityTracker;
    :cond_d4
    invoke-virtual {v7}, Landroid/view/VelocityTracker;->getYVelocity()F

    #@d7
    move-result v8

    #@d8
    float-to-int v8, v8

    #@d9
    neg-int v4, v8

    #@da
    goto :goto_b0

    #@db
    .line 553
    .end local v5           #rightThresh:I
    .end local v7           #velocityTracker:Landroid/view/VelocityTracker;
    :cond_db
    iget v8, p0, Lcom/android/internal/widget/RotarySelector;->mGrabbedState:I

    #@dd
    if-ne v8, v9, :cond_a

    #@df
    .line 554
    iget v8, p0, Lcom/android/internal/widget/RotarySelector;->mRightHandleX:I

    #@e1
    sub-int v8, v1, v8

    #@e3
    iput v8, p0, Lcom/android/internal/widget/RotarySelector;->mRotaryOffsetX:I

    #@e5
    .line 555
    invoke-virtual {p0}, Lcom/android/internal/widget/RotarySelector;->invalidate()V

    #@e8
    .line 556
    iget v8, p0, Lcom/android/internal/widget/RotarySelector;->mEdgeTriggerThresh:I

    #@ea
    if-gt v1, v8, :cond_a

    #@ec
    iget-boolean v8, p0, Lcom/android/internal/widget/RotarySelector;->mTriggered:Z

    #@ee
    if-nez v8, :cond_a

    #@f0
    .line 557
    iput-boolean v11, p0, Lcom/android/internal/widget/RotarySelector;->mTriggered:Z

    #@f2
    .line 558
    invoke-direct {p0, v9}, Lcom/android/internal/widget/RotarySelector;->dispatchTriggerEvent(I)V

    #@f5
    .line 559
    iget-object v7, p0, Lcom/android/internal/widget/RotarySelector;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@f7
    .line 560
    .restart local v7       #velocityTracker:Landroid/view/VelocityTracker;
    const/16 v8, 0x3e8

    #@f9
    iget v9, p0, Lcom/android/internal/widget/RotarySelector;->mMaximumVelocity:I

    #@fb
    int-to-float v9, v9

    #@fc
    invoke-virtual {v7, v8, v9}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    #@ff
    .line 561
    invoke-direct {p0}, Lcom/android/internal/widget/RotarySelector;->isHoriz()Z

    #@102
    move-result v8

    #@103
    if-eqz v8, :cond_12e

    #@105
    invoke-virtual {v7}, Landroid/view/VelocityTracker;->getXVelocity()F

    #@108
    move-result v8

    #@109
    float-to-int v4, v8

    #@10a
    .line 564
    .restart local v4       #rawVelocity:I
    :goto_10a
    iget v8, p0, Lcom/android/internal/widget/RotarySelector;->mMinimumVelocity:I

    #@10c
    neg-int v8, v8

    #@10d
    invoke-static {v8, v4}, Ljava/lang/Math;->min(II)I

    #@110
    move-result v6

    #@111
    .line 565
    .restart local v6       #velocity:I
    iget v8, p0, Lcom/android/internal/widget/RotarySelector;->mDimpleSpacing:I

    #@113
    div-int v8, v6, v8

    #@115
    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    #@118
    move-result v8

    #@119
    invoke-static {v13, v8}, Ljava/lang/Math;->max(II)I

    #@11c
    move-result v8

    #@11d
    iput v8, p0, Lcom/android/internal/widget/RotarySelector;->mDimplesOfFling:I

    #@11f
    .line 568
    iget v8, p0, Lcom/android/internal/widget/RotarySelector;->mRightHandleX:I

    #@121
    sub-int v8, v1, v8

    #@123
    iget v9, p0, Lcom/android/internal/widget/RotarySelector;->mDimplesOfFling:I

    #@125
    iget v10, p0, Lcom/android/internal/widget/RotarySelector;->mDimpleSpacing:I

    #@127
    mul-int/2addr v9, v10

    #@128
    neg-int v9, v9

    #@129
    invoke-direct {p0, v8, v9, v6}, Lcom/android/internal/widget/RotarySelector;->startAnimationWithVelocity(III)V

    #@12c
    goto/16 :goto_a

    #@12e
    .line 561
    .end local v4           #rawVelocity:I
    .end local v6           #velocity:I
    :cond_12e
    invoke-virtual {v7}, Landroid/view/VelocityTracker;->getYVelocity()F

    #@131
    move-result v8

    #@132
    float-to-int v8, v8

    #@133
    neg-int v4, v8

    #@134
    goto :goto_10a

    #@135
    .line 578
    .end local v7           #velocityTracker:Landroid/view/VelocityTracker;
    :pswitch_135
    iget v8, p0, Lcom/android/internal/widget/RotarySelector;->mGrabbedState:I

    #@137
    if-ne v8, v11, :cond_162

    #@139
    iget v8, p0, Lcom/android/internal/widget/RotarySelector;->mLeftHandleX:I

    #@13b
    sub-int v8, v1, v8

    #@13d
    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    #@140
    move-result v8

    #@141
    if-le v8, v12, :cond_162

    #@143
    .line 581
    iget v8, p0, Lcom/android/internal/widget/RotarySelector;->mLeftHandleX:I

    #@145
    sub-int v8, v1, v8

    #@147
    const/16 v9, 0x12c

    #@149
    invoke-direct {p0, v8, v10, v9}, Lcom/android/internal/widget/RotarySelector;->startAnimation(III)V

    #@14c
    .line 587
    :cond_14c
    :goto_14c
    iput v10, p0, Lcom/android/internal/widget/RotarySelector;->mRotaryOffsetX:I

    #@14e
    .line 588
    invoke-direct {p0, v10}, Lcom/android/internal/widget/RotarySelector;->setGrabbedState(I)V

    #@151
    .line 589
    invoke-virtual {p0}, Lcom/android/internal/widget/RotarySelector;->invalidate()V

    #@154
    .line 590
    iget-object v8, p0, Lcom/android/internal/widget/RotarySelector;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@156
    if-eqz v8, :cond_a

    #@158
    .line 591
    iget-object v8, p0, Lcom/android/internal/widget/RotarySelector;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@15a
    invoke-virtual {v8}, Landroid/view/VelocityTracker;->recycle()V

    #@15d
    .line 592
    const/4 v8, 0x0

    #@15e
    iput-object v8, p0, Lcom/android/internal/widget/RotarySelector;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@160
    goto/16 :goto_a

    #@162
    .line 582
    :cond_162
    iget v8, p0, Lcom/android/internal/widget/RotarySelector;->mGrabbedState:I

    #@164
    if-ne v8, v9, :cond_14c

    #@166
    iget v8, p0, Lcom/android/internal/widget/RotarySelector;->mRightHandleX:I

    #@168
    sub-int v8, v1, v8

    #@16a
    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    #@16d
    move-result v8

    #@16e
    if-le v8, v12, :cond_14c

    #@170
    .line 585
    iget v8, p0, Lcom/android/internal/widget/RotarySelector;->mRightHandleX:I

    #@172
    sub-int v8, v1, v8

    #@174
    const/16 v9, 0x12c

    #@176
    invoke-direct {p0, v8, v10, v9}, Lcom/android/internal/widget/RotarySelector;->startAnimation(III)V

    #@179
    goto :goto_14c

    #@17a
    .line 597
    :pswitch_17a
    invoke-direct {p0}, Lcom/android/internal/widget/RotarySelector;->reset()V

    #@17d
    .line 598
    invoke-virtual {p0}, Lcom/android/internal/widget/RotarySelector;->invalidate()V

    #@180
    .line 599
    iget-object v8, p0, Lcom/android/internal/widget/RotarySelector;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@182
    if-eqz v8, :cond_a

    #@184
    .line 600
    iget-object v8, p0, Lcom/android/internal/widget/RotarySelector;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@186
    invoke-virtual {v8}, Landroid/view/VelocityTracker;->recycle()V

    #@189
    .line 601
    const/4 v8, 0x0

    #@18a
    iput-object v8, p0, Lcom/android/internal/widget/RotarySelector;->mVelocityTracker:Landroid/view/VelocityTracker;

    #@18c
    goto/16 :goto_a

    #@18e
    .line 509
    :pswitch_data_18e
    .packed-switch 0x0
        :pswitch_33
        :pswitch_135
        :pswitch_75
        :pswitch_17a
    .end packed-switch
.end method

.method public setLeftHandleResource(I)V
    .registers 3
    .parameter "resId"

    #@0
    .prologue
    .line 247
    if-eqz p1, :cond_8

    #@2
    .line 248
    invoke-direct {p0, p1}, Lcom/android/internal/widget/RotarySelector;->getBitmapFor(I)Landroid/graphics/Bitmap;

    #@5
    move-result-object v0

    #@6
    iput-object v0, p0, Lcom/android/internal/widget/RotarySelector;->mLeftHandleIcon:Landroid/graphics/Bitmap;

    #@8
    .line 250
    :cond_8
    invoke-virtual {p0}, Lcom/android/internal/widget/RotarySelector;->invalidate()V

    #@b
    .line 251
    return-void
.end method

.method public setOnDialTriggerListener(Lcom/android/internal/widget/RotarySelector$OnDialTriggerListener;)V
    .registers 2
    .parameter "l"

    #@0
    .prologue
    .line 704
    iput-object p1, p0, Lcom/android/internal/widget/RotarySelector;->mOnDialTriggerListener:Lcom/android/internal/widget/RotarySelector$OnDialTriggerListener;

    #@2
    .line 705
    return-void
.end method

.method public setRightHandleResource(I)V
    .registers 3
    .parameter "resId"

    #@0
    .prologue
    .line 262
    if-eqz p1, :cond_8

    #@2
    .line 263
    invoke-direct {p0, p1}, Lcom/android/internal/widget/RotarySelector;->getBitmapFor(I)Landroid/graphics/Bitmap;

    #@5
    move-result-object v0

    #@6
    iput-object v0, p0, Lcom/android/internal/widget/RotarySelector;->mRightHandleIcon:Landroid/graphics/Bitmap;

    #@8
    .line 265
    :cond_8
    invoke-virtual {p0}, Lcom/android/internal/widget/RotarySelector;->invalidate()V

    #@b
    .line 266
    return-void
.end method
