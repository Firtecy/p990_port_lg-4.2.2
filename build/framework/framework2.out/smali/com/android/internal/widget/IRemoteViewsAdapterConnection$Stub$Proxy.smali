.class Lcom/android/internal/widget/IRemoteViewsAdapterConnection$Stub$Proxy;
.super Ljava/lang/Object;
.source "IRemoteViewsAdapterConnection.java"

# interfaces
.implements Lcom/android/internal/widget/IRemoteViewsAdapterConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/widget/IRemoteViewsAdapterConnection$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 69
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 70
    iput-object p1, p0, Lcom/android/internal/widget/IRemoteViewsAdapterConnection$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 71
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 74
    iget-object v0, p0, Lcom/android/internal/widget/IRemoteViewsAdapterConnection$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 78
    const-string v0, "com.android.internal.widget.IRemoteViewsAdapterConnection"

    #@2
    return-object v0
.end method

.method public onServiceConnected(Landroid/os/IBinder;)V
    .registers 7
    .parameter "service"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 82
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 83
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 85
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.android.internal.widget.IRemoteViewsAdapterConnection"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 86
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@10
    .line 87
    iget-object v2, p0, Lcom/android/internal/widget/IRemoteViewsAdapterConnection$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v3, 0x1

    #@13
    const/4 v4, 0x0

    #@14
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@17
    .line 88
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1a
    .catchall {:try_start_8 .. :try_end_1a} :catchall_21

    #@1a
    .line 91
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1d
    .line 92
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@20
    .line 94
    return-void

    #@21
    .line 91
    :catchall_21
    move-exception v2

    #@22
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 92
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    throw v2
.end method

.method public onServiceDisconnected()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 97
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 98
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 100
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.android.internal.widget.IRemoteViewsAdapterConnection"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 101
    iget-object v2, p0, Lcom/android/internal/widget/IRemoteViewsAdapterConnection$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v3, 0x2

    #@10
    const/4 v4, 0x0

    #@11
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 102
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_17
    .catchall {:try_start_8 .. :try_end_17} :catchall_1e

    #@17
    .line 105
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1a
    .line 106
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1d
    .line 108
    return-void

    #@1e
    .line 105
    :catchall_1e
    move-exception v2

    #@1f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 106
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    throw v2
.end method
