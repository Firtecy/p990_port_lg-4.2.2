.class public Lcom/android/internal/widget/TransportControlView;
.super Landroid/widget/FrameLayout;
.source "TransportControlView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/android/internal/widget/LockScreenWidgetInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/widget/TransportControlView$SavedState;,
        Lcom/android/internal/widget/TransportControlView$Metadata;,
        Lcom/android/internal/widget/TransportControlView$IRemoteControlDisplayWeak;
    }
.end annotation


# static fields
.field protected static final DEBUG:Z = false

.field private static final DISPLAY_TIMEOUT_MS:I = 0x1388

.field private static final MAXDIM:I = 0x200

.field private static final MSG_SET_ARTWORK:I = 0x67

.field private static final MSG_SET_GENERATION_ID:I = 0x68

.field private static final MSG_SET_METADATA:I = 0x65

.field private static final MSG_SET_TRANSPORT_CONTROLS:I = 0x66

.field private static final MSG_UPDATE_STATE:I = 0x64

.field protected static final TAG:Ljava/lang/String; = "TransportControlView"


# instance fields
.field private mAlbumArt:Landroid/widget/ImageView;

.field private mAttached:Z

.field private mAudioManager:Landroid/media/AudioManager;

.field private mBtnNext:Landroid/widget/ImageView;

.field private mBtnPlay:Landroid/widget/ImageView;

.field private mBtnPrev:Landroid/widget/ImageView;

.field private mClientGeneration:I

.field private mClientIntent:Landroid/app/PendingIntent;

.field private mCurrentPlayState:I

.field private mHandler:Landroid/os/Handler;

.field private mIRCD:Lcom/android/internal/widget/TransportControlView$IRemoteControlDisplayWeak;

.field private mMetadata:Lcom/android/internal/widget/TransportControlView$Metadata;

.field private mPopulateMetadataWhenAttached:Landroid/os/Bundle;

.field private mTrackTitle:Landroid/widget/TextView;

.field private mTransportControlFlags:I

.field private mWidgetCallbacks:Lcom/android/internal/widget/LockScreenWidgetCallback;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 5
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 194
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@3
    .line 74
    new-instance v0, Lcom/android/internal/widget/TransportControlView$Metadata;

    #@5
    invoke-direct {v0, p0}, Lcom/android/internal/widget/TransportControlView$Metadata;-><init>(Lcom/android/internal/widget/TransportControlView;)V

    #@8
    iput-object v0, p0, Lcom/android/internal/widget/TransportControlView;->mMetadata:Lcom/android/internal/widget/TransportControlView$Metadata;

    #@a
    .line 86
    const/4 v0, 0x0

    #@b
    iput-object v0, p0, Lcom/android/internal/widget/TransportControlView;->mPopulateMetadataWhenAttached:Landroid/os/Bundle;

    #@d
    .line 90
    new-instance v0, Lcom/android/internal/widget/TransportControlView$1;

    #@f
    invoke-direct {v0, p0}, Lcom/android/internal/widget/TransportControlView$1;-><init>(Lcom/android/internal/widget/TransportControlView;)V

    #@12
    iput-object v0, p0, Lcom/android/internal/widget/TransportControlView;->mHandler:Landroid/os/Handler;

    #@14
    .line 196
    new-instance v0, Landroid/media/AudioManager;

    #@16
    iget-object v1, p0, Lcom/android/internal/widget/TransportControlView;->mContext:Landroid/content/Context;

    #@18
    invoke-direct {v0, v1}, Landroid/media/AudioManager;-><init>(Landroid/content/Context;)V

    #@1b
    iput-object v0, p0, Lcom/android/internal/widget/TransportControlView;->mAudioManager:Landroid/media/AudioManager;

    #@1d
    .line 197
    const/4 v0, 0x0

    #@1e
    iput v0, p0, Lcom/android/internal/widget/TransportControlView;->mCurrentPlayState:I

    #@20
    .line 198
    new-instance v0, Lcom/android/internal/widget/TransportControlView$IRemoteControlDisplayWeak;

    #@22
    iget-object v1, p0, Lcom/android/internal/widget/TransportControlView;->mHandler:Landroid/os/Handler;

    #@24
    invoke-direct {v0, v1}, Lcom/android/internal/widget/TransportControlView$IRemoteControlDisplayWeak;-><init>(Landroid/os/Handler;)V

    #@27
    iput-object v0, p0, Lcom/android/internal/widget/TransportControlView;->mIRCD:Lcom/android/internal/widget/TransportControlView$IRemoteControlDisplayWeak;

    #@29
    .line 199
    return-void
.end method

.method static synthetic access$000(Lcom/android/internal/widget/TransportControlView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 55
    iget v0, p0, Lcom/android/internal/widget/TransportControlView;->mClientGeneration:I

    #@2
    return v0
.end method

.method static synthetic access$002(Lcom/android/internal/widget/TransportControlView;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 55
    iput p1, p0, Lcom/android/internal/widget/TransportControlView;->mClientGeneration:I

    #@2
    return p1
.end method

.method static synthetic access$100(Lcom/android/internal/widget/TransportControlView;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/android/internal/widget/TransportControlView;->updatePlayPauseState(I)V

    #@3
    return-void
.end method

.method static synthetic access$200(Lcom/android/internal/widget/TransportControlView;Landroid/os/Bundle;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/android/internal/widget/TransportControlView;->updateMetadata(Landroid/os/Bundle;)V

    #@3
    return-void
.end method

.method static synthetic access$300(Lcom/android/internal/widget/TransportControlView;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/android/internal/widget/TransportControlView;->updateTransportControls(I)V

    #@3
    return-void
.end method

.method static synthetic access$400(Lcom/android/internal/widget/TransportControlView;)Lcom/android/internal/widget/TransportControlView$Metadata;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/internal/widget/TransportControlView;->mMetadata:Lcom/android/internal/widget/TransportControlView$Metadata;

    #@2
    return-object v0
.end method

.method static synthetic access$600(Lcom/android/internal/widget/TransportControlView;)Landroid/widget/ImageView;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/internal/widget/TransportControlView;->mAlbumArt:Landroid/widget/ImageView;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Lcom/android/internal/widget/TransportControlView;)Lcom/android/internal/widget/LockScreenWidgetCallback;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/internal/widget/TransportControlView;->mWidgetCallbacks:Lcom/android/internal/widget/LockScreenWidgetCallback;

    #@2
    return-object v0
.end method

.method static synthetic access$802(Lcom/android/internal/widget/TransportControlView;Landroid/app/PendingIntent;)Landroid/app/PendingIntent;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 55
    iput-object p1, p0, Lcom/android/internal/widget/TransportControlView;->mClientIntent:Landroid/app/PendingIntent;

    #@2
    return-object p1
.end method

.method private getMdString(Landroid/os/Bundle;I)Ljava/lang/String;
    .registers 4
    .parameter "data"
    .parameter "id"

    #@0
    .prologue
    .line 264
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method private populateMetadata()V
    .registers 9

    #@0
    .prologue
    const/16 v7, 0x21

    #@2
    .line 282
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    .line 283
    .local v1, sb:Ljava/lang/StringBuilder;
    const/4 v3, 0x0

    #@8
    .line 284
    .local v3, trackTitleLength:I
    iget-object v4, p0, Lcom/android/internal/widget/TransportControlView;->mMetadata:Lcom/android/internal/widget/TransportControlView$Metadata;

    #@a
    invoke-static {v4}, Lcom/android/internal/widget/TransportControlView$Metadata;->access$1000(Lcom/android/internal/widget/TransportControlView$Metadata;)Ljava/lang/String;

    #@d
    move-result-object v4

    #@e
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@11
    move-result v4

    #@12
    if-nez v4, :cond_27

    #@14
    .line 285
    iget-object v4, p0, Lcom/android/internal/widget/TransportControlView;->mMetadata:Lcom/android/internal/widget/TransportControlView$Metadata;

    #@16
    invoke-static {v4}, Lcom/android/internal/widget/TransportControlView$Metadata;->access$1000(Lcom/android/internal/widget/TransportControlView$Metadata;)Ljava/lang/String;

    #@19
    move-result-object v4

    #@1a
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    .line 286
    iget-object v4, p0, Lcom/android/internal/widget/TransportControlView;->mMetadata:Lcom/android/internal/widget/TransportControlView$Metadata;

    #@1f
    invoke-static {v4}, Lcom/android/internal/widget/TransportControlView$Metadata;->access$1000(Lcom/android/internal/widget/TransportControlView$Metadata;)Ljava/lang/String;

    #@22
    move-result-object v4

    #@23
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    #@26
    move-result v3

    #@27
    .line 288
    :cond_27
    iget-object v4, p0, Lcom/android/internal/widget/TransportControlView;->mMetadata:Lcom/android/internal/widget/TransportControlView$Metadata;

    #@29
    invoke-static {v4}, Lcom/android/internal/widget/TransportControlView$Metadata;->access$900(Lcom/android/internal/widget/TransportControlView$Metadata;)Ljava/lang/String;

    #@2c
    move-result-object v4

    #@2d
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@30
    move-result v4

    #@31
    if-nez v4, :cond_47

    #@33
    .line 289
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    #@36
    move-result v4

    #@37
    if-eqz v4, :cond_3e

    #@39
    .line 290
    const-string v4, " - "

    #@3b
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    .line 292
    :cond_3e
    iget-object v4, p0, Lcom/android/internal/widget/TransportControlView;->mMetadata:Lcom/android/internal/widget/TransportControlView$Metadata;

    #@40
    invoke-static {v4}, Lcom/android/internal/widget/TransportControlView$Metadata;->access$900(Lcom/android/internal/widget/TransportControlView$Metadata;)Ljava/lang/String;

    #@43
    move-result-object v4

    #@44
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    .line 294
    :cond_47
    iget-object v4, p0, Lcom/android/internal/widget/TransportControlView;->mMetadata:Lcom/android/internal/widget/TransportControlView$Metadata;

    #@49
    invoke-static {v4}, Lcom/android/internal/widget/TransportControlView$Metadata;->access$1100(Lcom/android/internal/widget/TransportControlView$Metadata;)Ljava/lang/String;

    #@4c
    move-result-object v4

    #@4d
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@50
    move-result v4

    #@51
    if-nez v4, :cond_67

    #@53
    .line 295
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    #@56
    move-result v4

    #@57
    if-eqz v4, :cond_5e

    #@59
    .line 296
    const-string v4, " - "

    #@5b
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    .line 298
    :cond_5e
    iget-object v4, p0, Lcom/android/internal/widget/TransportControlView;->mMetadata:Lcom/android/internal/widget/TransportControlView$Metadata;

    #@60
    invoke-static {v4}, Lcom/android/internal/widget/TransportControlView$Metadata;->access$1100(Lcom/android/internal/widget/TransportControlView$Metadata;)Ljava/lang/String;

    #@63
    move-result-object v4

    #@64
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    .line 300
    :cond_67
    iget-object v4, p0, Lcom/android/internal/widget/TransportControlView;->mTrackTitle:Landroid/widget/TextView;

    #@69
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6c
    move-result-object v5

    #@6d
    sget-object v6, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    #@6f
    invoke-virtual {v4, v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    #@72
    .line 301
    iget-object v4, p0, Lcom/android/internal/widget/TransportControlView;->mTrackTitle:Landroid/widget/TextView;

    #@74
    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@77
    move-result-object v2

    #@78
    check-cast v2, Landroid/text/Spannable;

    #@7a
    .line 302
    .local v2, str:Landroid/text/Spannable;
    if-eqz v3, :cond_88

    #@7c
    .line 303
    new-instance v4, Landroid/text/style/ForegroundColorSpan;

    #@7e
    const/4 v5, -0x1

    #@7f
    invoke-direct {v4, v5}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    #@82
    const/4 v5, 0x0

    #@83
    invoke-interface {v2, v4, v5, v3, v7}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    #@86
    .line 305
    add-int/lit8 v3, v3, 0x1

    #@88
    .line 307
    :cond_88
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    #@8b
    move-result v4

    #@8c
    if-le v4, v3, :cond_9d

    #@8e
    .line 308
    new-instance v4, Landroid/text/style/ForegroundColorSpan;

    #@90
    const v5, 0x7fffffff

    #@93
    invoke-direct {v4, v5}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    #@96
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    #@99
    move-result v5

    #@9a
    invoke-interface {v2, v4, v3, v5, v7}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    #@9d
    .line 312
    :cond_9d
    iget-object v4, p0, Lcom/android/internal/widget/TransportControlView;->mAlbumArt:Landroid/widget/ImageView;

    #@9f
    iget-object v5, p0, Lcom/android/internal/widget/TransportControlView;->mMetadata:Lcom/android/internal/widget/TransportControlView$Metadata;

    #@a1
    invoke-static {v5}, Lcom/android/internal/widget/TransportControlView$Metadata;->access$500(Lcom/android/internal/widget/TransportControlView$Metadata;)Landroid/graphics/Bitmap;

    #@a4
    move-result-object v5

    #@a5
    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    #@a8
    .line 313
    iget v0, p0, Lcom/android/internal/widget/TransportControlView;->mTransportControlFlags:I

    #@aa
    .line 314
    .local v0, flags:I
    iget-object v4, p0, Lcom/android/internal/widget/TransportControlView;->mBtnPrev:Landroid/widget/ImageView;

    #@ac
    const/4 v5, 0x1

    #@ad
    invoke-static {v4, v0, v5}, Lcom/android/internal/widget/TransportControlView;->setVisibilityBasedOnFlag(Landroid/view/View;II)V

    #@b0
    .line 315
    iget-object v4, p0, Lcom/android/internal/widget/TransportControlView;->mBtnNext:Landroid/widget/ImageView;

    #@b2
    const/16 v5, 0x80

    #@b4
    invoke-static {v4, v0, v5}, Lcom/android/internal/widget/TransportControlView;->setVisibilityBasedOnFlag(Landroid/view/View;II)V

    #@b7
    .line 316
    iget-object v4, p0, Lcom/android/internal/widget/TransportControlView;->mBtnPlay:Landroid/widget/ImageView;

    #@b9
    const/16 v5, 0x3c

    #@bb
    invoke-static {v4, v0, v5}, Lcom/android/internal/widget/TransportControlView;->setVisibilityBasedOnFlag(Landroid/view/View;II)V

    #@be
    .line 322
    iget v4, p0, Lcom/android/internal/widget/TransportControlView;->mCurrentPlayState:I

    #@c0
    invoke-direct {p0, v4}, Lcom/android/internal/widget/TransportControlView;->updatePlayPauseState(I)V

    #@c3
    .line 323
    return-void
.end method

.method private sendMediaButtonClick(I)V
    .registers 8
    .parameter "keyCode"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 449
    iget-object v3, p0, Lcom/android/internal/widget/TransportControlView;->mClientIntent:Landroid/app/PendingIntent;

    #@3
    if-nez v3, :cond_d

    #@5
    .line 451
    const-string v3, "TransportControlView"

    #@7
    const-string v4, "sendMediaButtonClick(): No client is currently registered"

    #@9
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 475
    :goto_c
    return-void

    #@d
    .line 456
    :cond_d
    new-instance v2, Landroid/view/KeyEvent;

    #@f
    invoke-direct {v2, v4, p1}, Landroid/view/KeyEvent;-><init>(II)V

    #@12
    .line 457
    .local v2, keyEvent:Landroid/view/KeyEvent;
    new-instance v1, Landroid/content/Intent;

    #@14
    const-string v3, "android.intent.action.MEDIA_BUTTON"

    #@16
    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@19
    .line 458
    .local v1, intent:Landroid/content/Intent;
    const-string v3, "android.intent.extra.KEY_EVENT"

    #@1b
    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@1e
    .line 460
    :try_start_1e
    iget-object v3, p0, Lcom/android/internal/widget/TransportControlView;->mClientIntent:Landroid/app/PendingIntent;

    #@20
    invoke-virtual {p0}, Lcom/android/internal/widget/TransportControlView;->getContext()Landroid/content/Context;

    #@23
    move-result-object v4

    #@24
    const/4 v5, 0x0

    #@25
    invoke-virtual {v3, v4, v5, v1}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;)V
    :try_end_28
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_1e .. :try_end_28} :catch_62

    #@28
    .line 466
    :goto_28
    new-instance v2, Landroid/view/KeyEvent;

    #@2a
    .end local v2           #keyEvent:Landroid/view/KeyEvent;
    const/4 v3, 0x1

    #@2b
    invoke-direct {v2, v3, p1}, Landroid/view/KeyEvent;-><init>(II)V

    #@2e
    .line 467
    .restart local v2       #keyEvent:Landroid/view/KeyEvent;
    new-instance v1, Landroid/content/Intent;

    #@30
    .end local v1           #intent:Landroid/content/Intent;
    const-string v3, "android.intent.action.MEDIA_BUTTON"

    #@32
    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@35
    .line 468
    .restart local v1       #intent:Landroid/content/Intent;
    const-string v3, "android.intent.extra.KEY_EVENT"

    #@37
    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@3a
    .line 470
    :try_start_3a
    iget-object v3, p0, Lcom/android/internal/widget/TransportControlView;->mClientIntent:Landroid/app/PendingIntent;

    #@3c
    invoke-virtual {p0}, Lcom/android/internal/widget/TransportControlView;->getContext()Landroid/content/Context;

    #@3f
    move-result-object v4

    #@40
    const/4 v5, 0x0

    #@41
    invoke-virtual {v3, v4, v5, v1}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;)V
    :try_end_44
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_3a .. :try_end_44} :catch_45

    #@44
    goto :goto_c

    #@45
    .line 471
    :catch_45
    move-exception v0

    #@46
    .line 472
    .local v0, e:Landroid/app/PendingIntent$CanceledException;
    const-string v3, "TransportControlView"

    #@48
    new-instance v4, Ljava/lang/StringBuilder;

    #@4a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@4d
    const-string v5, "Error sending intent for media button up: "

    #@4f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v4

    #@53
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v4

    #@57
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v4

    #@5b
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5e
    .line 473
    invoke-virtual {v0}, Landroid/app/PendingIntent$CanceledException;->printStackTrace()V

    #@61
    goto :goto_c

    #@62
    .line 461
    .end local v0           #e:Landroid/app/PendingIntent$CanceledException;
    :catch_62
    move-exception v0

    #@63
    .line 462
    .restart local v0       #e:Landroid/app/PendingIntent$CanceledException;
    const-string v3, "TransportControlView"

    #@65
    new-instance v4, Ljava/lang/StringBuilder;

    #@67
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@6a
    const-string v5, "Error sending intent for media button down: "

    #@6c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v4

    #@70
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v4

    #@74
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@77
    move-result-object v4

    #@78
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7b
    .line 463
    invoke-virtual {v0}, Landroid/app/PendingIntent$CanceledException;->printStackTrace()V

    #@7e
    goto :goto_28
.end method

.method private static setVisibilityBasedOnFlag(Landroid/view/View;II)V
    .registers 4
    .parameter "view"
    .parameter "flags"
    .parameter "flag"

    #@0
    .prologue
    .line 326
    and-int v0, p1, p2

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 327
    const/4 v0, 0x0

    #@5
    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    #@8
    .line 331
    :goto_8
    return-void

    #@9
    .line 329
    :cond_9
    const/16 v0, 0x8

    #@b
    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    #@e
    goto :goto_8
.end method

.method private updateMetadata(Landroid/os/Bundle;)V
    .registers 4
    .parameter "data"

    #@0
    .prologue
    .line 268
    iget-boolean v0, p0, Lcom/android/internal/widget/TransportControlView;->mAttached:Z

    #@2
    if-eqz v0, :cond_27

    #@4
    .line 269
    iget-object v0, p0, Lcom/android/internal/widget/TransportControlView;->mMetadata:Lcom/android/internal/widget/TransportControlView$Metadata;

    #@6
    const/16 v1, 0xd

    #@8
    invoke-direct {p0, p1, v1}, Lcom/android/internal/widget/TransportControlView;->getMdString(Landroid/os/Bundle;I)Ljava/lang/String;

    #@b
    move-result-object v1

    #@c
    invoke-static {v0, v1}, Lcom/android/internal/widget/TransportControlView$Metadata;->access$902(Lcom/android/internal/widget/TransportControlView$Metadata;Ljava/lang/String;)Ljava/lang/String;

    #@f
    .line 270
    iget-object v0, p0, Lcom/android/internal/widget/TransportControlView;->mMetadata:Lcom/android/internal/widget/TransportControlView$Metadata;

    #@11
    const/4 v1, 0x7

    #@12
    invoke-direct {p0, p1, v1}, Lcom/android/internal/widget/TransportControlView;->getMdString(Landroid/os/Bundle;I)Ljava/lang/String;

    #@15
    move-result-object v1

    #@16
    invoke-static {v0, v1}, Lcom/android/internal/widget/TransportControlView$Metadata;->access$1002(Lcom/android/internal/widget/TransportControlView$Metadata;Ljava/lang/String;)Ljava/lang/String;

    #@19
    .line 271
    iget-object v0, p0, Lcom/android/internal/widget/TransportControlView;->mMetadata:Lcom/android/internal/widget/TransportControlView$Metadata;

    #@1b
    const/4 v1, 0x1

    #@1c
    invoke-direct {p0, p1, v1}, Lcom/android/internal/widget/TransportControlView;->getMdString(Landroid/os/Bundle;I)Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    invoke-static {v0, v1}, Lcom/android/internal/widget/TransportControlView$Metadata;->access$1102(Lcom/android/internal/widget/TransportControlView$Metadata;Ljava/lang/String;)Ljava/lang/String;

    #@23
    .line 272
    invoke-direct {p0}, Lcom/android/internal/widget/TransportControlView;->populateMetadata()V

    #@26
    .line 276
    :goto_26
    return-void

    #@27
    .line 274
    :cond_27
    iput-object p1, p0, Lcom/android/internal/widget/TransportControlView;->mPopulateMetadataWhenAttached:Landroid/os/Bundle;

    #@29
    goto :goto_26
.end method

.method private updatePlayPauseState(I)V
    .registers 7
    .parameter "state"

    #@0
    .prologue
    .line 336
    iget v3, p0, Lcom/android/internal/widget/TransportControlView;->mCurrentPlayState:I

    #@2
    if-ne p1, v3, :cond_5

    #@4
    .line 375
    :goto_4
    return-void

    #@5
    .line 341
    :cond_5
    const/4 v2, 0x0

    #@6
    .line 342
    .local v2, showIfHidden:Z
    sparse-switch p1, :sswitch_data_50

    #@9
    .line 364
    const v1, 0x1080024

    #@c
    .line 365
    .local v1, imageResId:I
    const v0, 0x1040323

    #@f
    .line 366
    .local v0, imageDescId:I
    const/4 v2, 0x0

    #@10
    .line 369
    :goto_10
    iget-object v3, p0, Lcom/android/internal/widget/TransportControlView;->mBtnPlay:Landroid/widget/ImageView;

    #@12
    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    #@15
    .line 370
    iget-object v3, p0, Lcom/android/internal/widget/TransportControlView;->mBtnPlay:Landroid/widget/ImageView;

    #@17
    invoke-virtual {p0}, Lcom/android/internal/widget/TransportControlView;->getResources()Landroid/content/res/Resources;

    #@1a
    move-result-object v4

    #@1b
    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1e
    move-result-object v4

    #@1f
    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    #@22
    .line 371
    if-eqz v2, :cond_35

    #@24
    iget-object v3, p0, Lcom/android/internal/widget/TransportControlView;->mWidgetCallbacks:Lcom/android/internal/widget/LockScreenWidgetCallback;

    #@26
    if-eqz v3, :cond_35

    #@28
    iget-object v3, p0, Lcom/android/internal/widget/TransportControlView;->mWidgetCallbacks:Lcom/android/internal/widget/LockScreenWidgetCallback;

    #@2a
    invoke-interface {v3, p0}, Lcom/android/internal/widget/LockScreenWidgetCallback;->isVisible(Landroid/view/View;)Z

    #@2d
    move-result v3

    #@2e
    if-nez v3, :cond_35

    #@30
    .line 372
    iget-object v3, p0, Lcom/android/internal/widget/TransportControlView;->mWidgetCallbacks:Lcom/android/internal/widget/LockScreenWidgetCallback;

    #@32
    invoke-interface {v3, p0}, Lcom/android/internal/widget/LockScreenWidgetCallback;->requestShow(Landroid/view/View;)V

    #@35
    .line 374
    :cond_35
    iput p1, p0, Lcom/android/internal/widget/TransportControlView;->mCurrentPlayState:I

    #@37
    goto :goto_4

    #@38
    .line 344
    .end local v0           #imageDescId:I
    .end local v1           #imageResId:I
    :sswitch_38
    const v1, 0x108008a

    #@3b
    .line 347
    .restart local v1       #imageResId:I
    const v0, 0x1040323

    #@3e
    .line 348
    .restart local v0       #imageDescId:I
    goto :goto_10

    #@3f
    .line 351
    .end local v0           #imageDescId:I
    .end local v1           #imageResId:I
    :sswitch_3f
    const v1, 0x1080023

    #@42
    .line 352
    .restart local v1       #imageResId:I
    const v0, 0x1040322

    #@45
    .line 353
    .restart local v0       #imageDescId:I
    const/4 v2, 0x1

    #@46
    .line 354
    goto :goto_10

    #@47
    .line 357
    .end local v0           #imageDescId:I
    .end local v1           #imageResId:I
    :sswitch_47
    const v1, 0x1080321

    #@4a
    .line 358
    .restart local v1       #imageResId:I
    const v0, 0x1040324

    #@4d
    .line 359
    .restart local v0       #imageDescId:I
    const/4 v2, 0x1

    #@4e
    .line 360
    goto :goto_10

    #@4f
    .line 342
    nop

    #@50
    :sswitch_data_50
    .sparse-switch
        0x3 -> :sswitch_3f
        0x8 -> :sswitch_47
        0x9 -> :sswitch_38
    .end sparse-switch
.end method

.method private updateTransportControls(I)V
    .registers 2
    .parameter "transportControlFlags"

    #@0
    .prologue
    .line 202
    iput p1, p0, Lcom/android/internal/widget/TransportControlView;->mTransportControlFlags:I

    #@2
    .line 203
    return-void
.end method

.method private wasPlayingRecently(IJ)Z
    .registers 10
    .parameter "state"
    .parameter "stateChangeTimeMs"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 486
    packed-switch p1, :pswitch_data_34

    #@5
    .line 510
    const-string v0, "TransportControlView"

    #@7
    new-instance v2, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v3, "Unknown playback state "

    #@e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    const-string v3, " in wasPlayingRecently()"

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v2

    #@20
    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    move v0, v1

    #@24
    .line 511
    :cond_24
    :goto_24
    :pswitch_24
    return v0

    #@25
    :pswitch_25
    move v0, v1

    #@26
    .line 496
    goto :goto_24

    #@27
    .line 508
    :pswitch_27
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@2a
    move-result-wide v2

    #@2b
    sub-long/2addr v2, p2

    #@2c
    const-wide/16 v4, 0x1388

    #@2e
    cmp-long v2, v2, v4

    #@30
    if-ltz v2, :cond_24

    #@32
    move v0, v1

    #@33
    goto :goto_24

    #@34
    .line 486
    :pswitch_data_34
    .packed-switch 0x0
        :pswitch_25
        :pswitch_27
        :pswitch_27
        :pswitch_24
        :pswitch_24
        :pswitch_24
        :pswitch_24
        :pswitch_24
        :pswitch_24
        :pswitch_27
    .end packed-switch
.end method


# virtual methods
.method public onAttachedToWindow()V
    .registers 3

    #@0
    .prologue
    .line 222
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    #@3
    .line 223
    iget-object v0, p0, Lcom/android/internal/widget/TransportControlView;->mPopulateMetadataWhenAttached:Landroid/os/Bundle;

    #@5
    if-eqz v0, :cond_f

    #@7
    .line 224
    iget-object v0, p0, Lcom/android/internal/widget/TransportControlView;->mPopulateMetadataWhenAttached:Landroid/os/Bundle;

    #@9
    invoke-direct {p0, v0}, Lcom/android/internal/widget/TransportControlView;->updateMetadata(Landroid/os/Bundle;)V

    #@c
    .line 225
    const/4 v0, 0x0

    #@d
    iput-object v0, p0, Lcom/android/internal/widget/TransportControlView;->mPopulateMetadataWhenAttached:Landroid/os/Bundle;

    #@f
    .line 227
    :cond_f
    iget-boolean v0, p0, Lcom/android/internal/widget/TransportControlView;->mAttached:Z

    #@11
    if-nez v0, :cond_1a

    #@13
    .line 229
    iget-object v0, p0, Lcom/android/internal/widget/TransportControlView;->mAudioManager:Landroid/media/AudioManager;

    #@15
    iget-object v1, p0, Lcom/android/internal/widget/TransportControlView;->mIRCD:Lcom/android/internal/widget/TransportControlView$IRemoteControlDisplayWeak;

    #@17
    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->registerRemoteControlDisplay(Landroid/media/IRemoteControlDisplay;)V

    #@1a
    .line 231
    :cond_1a
    const/4 v0, 0x1

    #@1b
    iput-boolean v0, p0, Lcom/android/internal/widget/TransportControlView;->mAttached:Z

    #@1d
    .line 232
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 4
    .parameter "v"

    #@0
    .prologue
    .line 431
    const/4 v0, -0x1

    #@1
    .line 432
    .local v0, keyCode:I
    iget-object v1, p0, Lcom/android/internal/widget/TransportControlView;->mBtnPrev:Landroid/widget/ImageView;

    #@3
    if-ne p1, v1, :cond_17

    #@5
    .line 433
    const/16 v0, 0x58

    #@7
    .line 440
    :cond_7
    :goto_7
    const/4 v1, -0x1

    #@8
    if-eq v0, v1, :cond_16

    #@a
    .line 441
    invoke-direct {p0, v0}, Lcom/android/internal/widget/TransportControlView;->sendMediaButtonClick(I)V

    #@d
    .line 442
    iget-object v1, p0, Lcom/android/internal/widget/TransportControlView;->mWidgetCallbacks:Lcom/android/internal/widget/LockScreenWidgetCallback;

    #@f
    if-eqz v1, :cond_16

    #@11
    .line 443
    iget-object v1, p0, Lcom/android/internal/widget/TransportControlView;->mWidgetCallbacks:Lcom/android/internal/widget/LockScreenWidgetCallback;

    #@13
    invoke-interface {v1, p0}, Lcom/android/internal/widget/LockScreenWidgetCallback;->userActivity(Landroid/view/View;)V

    #@16
    .line 446
    :cond_16
    return-void

    #@17
    .line 434
    :cond_17
    iget-object v1, p0, Lcom/android/internal/widget/TransportControlView;->mBtnNext:Landroid/widget/ImageView;

    #@19
    if-ne p1, v1, :cond_1e

    #@1b
    .line 435
    const/16 v0, 0x57

    #@1d
    goto :goto_7

    #@1e
    .line 436
    :cond_1e
    iget-object v1, p0, Lcom/android/internal/widget/TransportControlView;->mBtnPlay:Landroid/widget/ImageView;

    #@20
    if-ne p1, v1, :cond_7

    #@22
    .line 437
    const/16 v0, 0x55

    #@24
    goto :goto_7
.end method

.method public onDetachedFromWindow()V
    .registers 3

    #@0
    .prologue
    .line 236
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    #@3
    .line 237
    iget-boolean v0, p0, Lcom/android/internal/widget/TransportControlView;->mAttached:Z

    #@5
    if-eqz v0, :cond_e

    #@7
    .line 239
    iget-object v0, p0, Lcom/android/internal/widget/TransportControlView;->mAudioManager:Landroid/media/AudioManager;

    #@9
    iget-object v1, p0, Lcom/android/internal/widget/TransportControlView;->mIRCD:Lcom/android/internal/widget/TransportControlView$IRemoteControlDisplayWeak;

    #@b
    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->unregisterRemoteControlDisplay(Landroid/media/IRemoteControlDisplay;)V

    #@e
    .line 241
    :cond_e
    const/4 v0, 0x0

    #@f
    iput-boolean v0, p0, Lcom/android/internal/widget/TransportControlView;->mAttached:Z

    #@11
    .line 242
    return-void
.end method

.method public onFinishInflate()V
    .registers 9

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    .line 207
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    #@4
    .line 208
    const v5, 0x1020016

    #@7
    invoke-virtual {p0, v5}, Lcom/android/internal/widget/TransportControlView;->findViewById(I)Landroid/view/View;

    #@a
    move-result-object v5

    #@b
    check-cast v5, Landroid/widget/TextView;

    #@d
    iput-object v5, p0, Lcom/android/internal/widget/TransportControlView;->mTrackTitle:Landroid/widget/TextView;

    #@f
    .line 209
    iget-object v5, p0, Lcom/android/internal/widget/TransportControlView;->mTrackTitle:Landroid/widget/TextView;

    #@11
    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setSelected(Z)V

    #@14
    .line 210
    const v5, 0x1020315

    #@17
    invoke-virtual {p0, v5}, Lcom/android/internal/widget/TransportControlView;->findViewById(I)Landroid/view/View;

    #@1a
    move-result-object v5

    #@1b
    check-cast v5, Landroid/widget/ImageView;

    #@1d
    iput-object v5, p0, Lcom/android/internal/widget/TransportControlView;->mAlbumArt:Landroid/widget/ImageView;

    #@1f
    .line 211
    const v5, 0x1020316

    #@22
    invoke-virtual {p0, v5}, Lcom/android/internal/widget/TransportControlView;->findViewById(I)Landroid/view/View;

    #@25
    move-result-object v5

    #@26
    check-cast v5, Landroid/widget/ImageView;

    #@28
    iput-object v5, p0, Lcom/android/internal/widget/TransportControlView;->mBtnPrev:Landroid/widget/ImageView;

    #@2a
    .line 212
    const v5, 0x1020317

    #@2d
    invoke-virtual {p0, v5}, Lcom/android/internal/widget/TransportControlView;->findViewById(I)Landroid/view/View;

    #@30
    move-result-object v5

    #@31
    check-cast v5, Landroid/widget/ImageView;

    #@33
    iput-object v5, p0, Lcom/android/internal/widget/TransportControlView;->mBtnPlay:Landroid/widget/ImageView;

    #@35
    .line 213
    const v5, 0x1020318

    #@38
    invoke-virtual {p0, v5}, Lcom/android/internal/widget/TransportControlView;->findViewById(I)Landroid/view/View;

    #@3b
    move-result-object v5

    #@3c
    check-cast v5, Landroid/widget/ImageView;

    #@3e
    iput-object v5, p0, Lcom/android/internal/widget/TransportControlView;->mBtnNext:Landroid/widget/ImageView;

    #@40
    .line 214
    const/4 v5, 0x3

    #@41
    new-array v1, v5, [Landroid/view/View;

    #@43
    const/4 v5, 0x0

    #@44
    iget-object v6, p0, Lcom/android/internal/widget/TransportControlView;->mBtnPrev:Landroid/widget/ImageView;

    #@46
    aput-object v6, v1, v5

    #@48
    iget-object v5, p0, Lcom/android/internal/widget/TransportControlView;->mBtnPlay:Landroid/widget/ImageView;

    #@4a
    aput-object v5, v1, v7

    #@4c
    const/4 v5, 0x2

    #@4d
    iget-object v6, p0, Lcom/android/internal/widget/TransportControlView;->mBtnNext:Landroid/widget/ImageView;

    #@4f
    aput-object v6, v1, v5

    #@51
    .line 215
    .local v1, buttons:[Landroid/view/View;
    move-object v0, v1

    #@52
    .local v0, arr$:[Landroid/view/View;
    array-length v3, v0

    #@53
    .local v3, len$:I
    const/4 v2, 0x0

    #@54
    .local v2, i$:I
    :goto_54
    if-ge v2, v3, :cond_5e

    #@56
    aget-object v4, v0, v2

    #@58
    .line 216
    .local v4, view:Landroid/view/View;
    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@5b
    .line 215
    add-int/lit8 v2, v2, 0x1

    #@5d
    goto :goto_54

    #@5e
    .line 218
    .end local v4           #view:Landroid/view/View;
    :cond_5e
    return-void
.end method

.method protected onMeasure(II)V
    .registers 7
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    .line 246
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    #@3
    .line 247
    const/16 v1, 0x200

    #@5
    invoke-virtual {p0}, Lcom/android/internal/widget/TransportControlView;->getWidth()I

    #@8
    move-result v2

    #@9
    invoke-virtual {p0}, Lcom/android/internal/widget/TransportControlView;->getHeight()I

    #@c
    move-result v3

    #@d
    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    #@10
    move-result v2

    #@11
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    #@14
    move-result v0

    #@15
    .line 250
    .local v0, dim:I
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .registers 4
    .parameter "state"

    #@0
    .prologue
    .line 419
    instance-of v1, p1, Lcom/android/internal/widget/TransportControlView$SavedState;

    #@2
    if-nez v1, :cond_8

    #@4
    .line 420
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    #@7
    .line 428
    :cond_7
    :goto_7
    return-void

    #@8
    :cond_8
    move-object v0, p1

    #@9
    .line 423
    check-cast v0, Lcom/android/internal/widget/TransportControlView$SavedState;

    #@b
    .line 424
    .local v0, ss:Lcom/android/internal/widget/TransportControlView$SavedState;
    invoke-virtual {v0}, Lcom/android/internal/widget/TransportControlView$SavedState;->getSuperState()Landroid/os/Parcelable;

    #@e
    move-result-object v1

    #@f
    invoke-super {p0, v1}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    #@12
    .line 425
    iget-boolean v1, v0, Lcom/android/internal/widget/TransportControlView$SavedState;->wasShowing:Z

    #@14
    if-eqz v1, :cond_7

    #@16
    iget-object v1, p0, Lcom/android/internal/widget/TransportControlView;->mWidgetCallbacks:Lcom/android/internal/widget/LockScreenWidgetCallback;

    #@18
    if-eqz v1, :cond_7

    #@1a
    .line 426
    iget-object v1, p0, Lcom/android/internal/widget/TransportControlView;->mWidgetCallbacks:Lcom/android/internal/widget/LockScreenWidgetCallback;

    #@1c
    invoke-interface {v1, p0}, Lcom/android/internal/widget/LockScreenWidgetCallback;->requestShow(Landroid/view/View;)V

    #@1f
    goto :goto_7
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .registers 4

    #@0
    .prologue
    .line 410
    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    #@3
    move-result-object v1

    #@4
    .line 411
    .local v1, superState:Landroid/os/Parcelable;
    new-instance v0, Lcom/android/internal/widget/TransportControlView$SavedState;

    #@6
    invoke-direct {v0, v1}, Lcom/android/internal/widget/TransportControlView$SavedState;-><init>(Landroid/os/Parcelable;)V

    #@9
    .line 412
    .local v0, ss:Lcom/android/internal/widget/TransportControlView$SavedState;
    iget-object v2, p0, Lcom/android/internal/widget/TransportControlView;->mWidgetCallbacks:Lcom/android/internal/widget/LockScreenWidgetCallback;

    #@b
    if-eqz v2, :cond_19

    #@d
    iget-object v2, p0, Lcom/android/internal/widget/TransportControlView;->mWidgetCallbacks:Lcom/android/internal/widget/LockScreenWidgetCallback;

    #@f
    invoke-interface {v2, p0}, Lcom/android/internal/widget/LockScreenWidgetCallback;->isVisible(Landroid/view/View;)Z

    #@12
    move-result v2

    #@13
    if-eqz v2, :cond_19

    #@15
    const/4 v2, 0x1

    #@16
    :goto_16
    iput-boolean v2, v0, Lcom/android/internal/widget/TransportControlView$SavedState;->wasShowing:Z

    #@18
    .line 413
    return-object v0

    #@19
    .line 412
    :cond_19
    const/4 v2, 0x0

    #@1a
    goto :goto_16
.end method

.method public providesClock()Z
    .registers 2

    #@0
    .prologue
    .line 482
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public setCallback(Lcom/android/internal/widget/LockScreenWidgetCallback;)V
    .registers 2
    .parameter "callback"

    #@0
    .prologue
    .line 478
    iput-object p1, p0, Lcom/android/internal/widget/TransportControlView;->mWidgetCallbacks:Lcom/android/internal/widget/LockScreenWidgetCallback;

    #@2
    .line 479
    return-void
.end method
