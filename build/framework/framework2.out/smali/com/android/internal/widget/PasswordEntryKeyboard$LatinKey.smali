.class Lcom/android/internal/widget/PasswordEntryKeyboard$LatinKey;
.super Landroid/inputmethodservice/Keyboard$Key;
.source "PasswordEntryKeyboard.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/widget/PasswordEntryKeyboard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "LatinKey"
.end annotation


# instance fields
.field private mEnabled:Z

.field private mShiftLockEnabled:Z


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Landroid/inputmethodservice/Keyboard$Row;IILandroid/content/res/XmlResourceParser;)V
    .registers 7
    .parameter "res"
    .parameter "parent"
    .parameter "x"
    .parameter "y"
    .parameter "parser"

    #@0
    .prologue
    .line 225
    invoke-direct/range {p0 .. p5}, Landroid/inputmethodservice/Keyboard$Key;-><init>(Landroid/content/res/Resources;Landroid/inputmethodservice/Keyboard$Row;IILandroid/content/res/XmlResourceParser;)V

    #@3
    .line 221
    const/4 v0, 0x1

    #@4
    iput-boolean v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboard$LatinKey;->mEnabled:Z

    #@6
    .line 226
    iget-object v0, p0, Landroid/inputmethodservice/Keyboard$Key;->popupCharacters:Ljava/lang/CharSequence;

    #@8
    if-eqz v0, :cond_15

    #@a
    iget-object v0, p0, Landroid/inputmethodservice/Keyboard$Key;->popupCharacters:Ljava/lang/CharSequence;

    #@c
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    #@f
    move-result v0

    #@10
    if-nez v0, :cond_15

    #@12
    .line 228
    const/4 v0, 0x0

    #@13
    iput v0, p0, Landroid/inputmethodservice/Keyboard$Key;->popupResId:I

    #@15
    .line 230
    :cond_15
    return-void
.end method


# virtual methods
.method enableShiftLock()V
    .registers 2

    #@0
    .prologue
    .line 237
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboard$LatinKey;->mShiftLockEnabled:Z

    #@3
    .line 238
    return-void
.end method

.method public isInside(II)Z
    .registers 8
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v4, -0x1

    #@2
    const/4 v3, -0x5

    #@3
    .line 254
    iget-boolean v2, p0, Lcom/android/internal/widget/PasswordEntryKeyboard$LatinKey;->mEnabled:Z

    #@5
    if-nez v2, :cond_8

    #@7
    .line 265
    :goto_7
    return v1

    #@8
    .line 257
    :cond_8
    iget-object v2, p0, Lcom/android/internal/widget/PasswordEntryKeyboard$LatinKey;->codes:[I

    #@a
    aget v0, v2, v1

    #@c
    .line 258
    .local v0, code:I
    if-eq v0, v4, :cond_10

    #@e
    if-ne v0, v3, :cond_28

    #@10
    .line 259
    :cond_10
    iget v1, p0, Lcom/android/internal/widget/PasswordEntryKeyboard$LatinKey;->height:I

    #@12
    div-int/lit8 v1, v1, 0xa

    #@14
    sub-int/2addr p2, v1

    #@15
    .line 260
    if-ne v0, v4, :cond_1c

    #@17
    iget v1, p0, Lcom/android/internal/widget/PasswordEntryKeyboard$LatinKey;->width:I

    #@19
    div-int/lit8 v1, v1, 0x6

    #@1b
    add-int/2addr p1, v1

    #@1c
    .line 261
    :cond_1c
    if-ne v0, v3, :cond_23

    #@1e
    iget v1, p0, Lcom/android/internal/widget/PasswordEntryKeyboard$LatinKey;->width:I

    #@20
    div-int/lit8 v1, v1, 0x6

    #@22
    sub-int/2addr p1, v1

    #@23
    .line 265
    :cond_23
    :goto_23
    invoke-super {p0, p1, p2}, Landroid/inputmethodservice/Keyboard$Key;->isInside(II)Z

    #@26
    move-result v1

    #@27
    goto :goto_7

    #@28
    .line 262
    :cond_28
    const/16 v1, 0x20

    #@2a
    if-ne v0, v1, :cond_23

    #@2c
    .line 263
    sget v1, Lcom/android/internal/widget/PasswordEntryKeyboard;->sSpacebarVerticalCorrection:I

    #@2e
    add-int/2addr p2, v1

    #@2f
    goto :goto_23
.end method

.method public onReleased(Z)V
    .registers 3
    .parameter "inside"

    #@0
    .prologue
    .line 242
    iget-boolean v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboard$LatinKey;->mShiftLockEnabled:Z

    #@2
    if-nez v0, :cond_8

    #@4
    .line 243
    invoke-super {p0, p1}, Landroid/inputmethodservice/Keyboard$Key;->onReleased(Z)V

    #@7
    .line 247
    :goto_7
    return-void

    #@8
    .line 245
    :cond_8
    iget-boolean v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboard$LatinKey;->pressed:Z

    #@a
    if-nez v0, :cond_10

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    iput-boolean v0, p0, Lcom/android/internal/widget/PasswordEntryKeyboard$LatinKey;->pressed:Z

    #@f
    goto :goto_7

    #@10
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_d
.end method

.method setEnabled(Z)V
    .registers 2
    .parameter "enabled"

    #@0
    .prologue
    .line 233
    iput-boolean p1, p0, Lcom/android/internal/widget/PasswordEntryKeyboard$LatinKey;->mEnabled:Z

    #@2
    .line 234
    return-void
.end method
