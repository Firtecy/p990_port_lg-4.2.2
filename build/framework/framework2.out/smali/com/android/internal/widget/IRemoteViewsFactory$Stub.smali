.class public abstract Lcom/android/internal/widget/IRemoteViewsFactory$Stub;
.super Landroid/os/Binder;
.source "IRemoteViewsFactory.java"

# interfaces
.implements Lcom/android/internal/widget/IRemoteViewsFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/widget/IRemoteViewsFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/widget/IRemoteViewsFactory$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.android.internal.widget.IRemoteViewsFactory"

.field static final TRANSACTION_getCount:I = 0x4

.field static final TRANSACTION_getItemId:I = 0x8

.field static final TRANSACTION_getLoadingView:I = 0x6

.field static final TRANSACTION_getViewAt:I = 0x5

.field static final TRANSACTION_getViewTypeCount:I = 0x7

.field static final TRANSACTION_hasStableIds:I = 0x9

.field static final TRANSACTION_isCreated:I = 0xa

.field static final TRANSACTION_onDataSetChanged:I = 0x1

.field static final TRANSACTION_onDataSetChangedAsync:I = 0x2

.field static final TRANSACTION_onDestroy:I = 0x3


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 15
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 16
    const-string v0, "com.android.internal.widget.IRemoteViewsFactory"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/android/internal/widget/IRemoteViewsFactory$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 17
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/android/internal/widget/IRemoteViewsFactory;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 24
    if-nez p0, :cond_4

    #@2
    .line 25
    const/4 v0, 0x0

    #@3
    .line 31
    :goto_3
    return-object v0

    #@4
    .line 27
    :cond_4
    const-string v1, "com.android.internal.widget.IRemoteViewsFactory"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 28
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/android/internal/widget/IRemoteViewsFactory;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 29
    check-cast v0, Lcom/android/internal/widget/IRemoteViewsFactory;

    #@12
    goto :goto_3

    #@13
    .line 31
    :cond_13
    new-instance v0, Lcom/android/internal/widget/IRemoteViewsFactory$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/android/internal/widget/IRemoteViewsFactory$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 35
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 11
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    .line 39
    sparse-switch p1, :sswitch_data_d2

    #@5
    .line 145
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v4

    #@9
    :goto_9
    return v4

    #@a
    .line 43
    :sswitch_a
    const-string v3, "com.android.internal.widget.IRemoteViewsFactory"

    #@c
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 48
    :sswitch_10
    const-string v3, "com.android.internal.widget.IRemoteViewsFactory"

    #@12
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 49
    invoke-virtual {p0}, Lcom/android/internal/widget/IRemoteViewsFactory$Stub;->onDataSetChanged()V

    #@18
    .line 50
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1b
    goto :goto_9

    #@1c
    .line 55
    :sswitch_1c
    const-string v3, "com.android.internal.widget.IRemoteViewsFactory"

    #@1e
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@21
    .line 56
    invoke-virtual {p0}, Lcom/android/internal/widget/IRemoteViewsFactory$Stub;->onDataSetChangedAsync()V

    #@24
    goto :goto_9

    #@25
    .line 61
    :sswitch_25
    const-string v3, "com.android.internal.widget.IRemoteViewsFactory"

    #@27
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2a
    .line 63
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2d
    move-result v3

    #@2e
    if-eqz v3, :cond_3c

    #@30
    .line 64
    sget-object v3, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@32
    invoke-interface {v3, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@35
    move-result-object v0

    #@36
    check-cast v0, Landroid/content/Intent;

    #@38
    .line 69
    .local v0, _arg0:Landroid/content/Intent;
    :goto_38
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/IRemoteViewsFactory$Stub;->onDestroy(Landroid/content/Intent;)V

    #@3b
    goto :goto_9

    #@3c
    .line 67
    .end local v0           #_arg0:Landroid/content/Intent;
    :cond_3c
    const/4 v0, 0x0

    #@3d
    .restart local v0       #_arg0:Landroid/content/Intent;
    goto :goto_38

    #@3e
    .line 74
    .end local v0           #_arg0:Landroid/content/Intent;
    :sswitch_3e
    const-string v3, "com.android.internal.widget.IRemoteViewsFactory"

    #@40
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@43
    .line 75
    invoke-virtual {p0}, Lcom/android/internal/widget/IRemoteViewsFactory$Stub;->getCount()I

    #@46
    move-result v1

    #@47
    .line 76
    .local v1, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4a
    .line 77
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@4d
    goto :goto_9

    #@4e
    .line 82
    .end local v1           #_result:I
    :sswitch_4e
    const-string v5, "com.android.internal.widget.IRemoteViewsFactory"

    #@50
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@53
    .line 84
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@56
    move-result v0

    #@57
    .line 85
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/IRemoteViewsFactory$Stub;->getViewAt(I)Landroid/widget/RemoteViews;

    #@5a
    move-result-object v1

    #@5b
    .line 86
    .local v1, _result:Landroid/widget/RemoteViews;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@5e
    .line 87
    if-eqz v1, :cond_67

    #@60
    .line 88
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@63
    .line 89
    invoke-virtual {v1, p3, v4}, Landroid/widget/RemoteViews;->writeToParcel(Landroid/os/Parcel;I)V

    #@66
    goto :goto_9

    #@67
    .line 92
    :cond_67
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@6a
    goto :goto_9

    #@6b
    .line 98
    .end local v0           #_arg0:I
    .end local v1           #_result:Landroid/widget/RemoteViews;
    :sswitch_6b
    const-string v5, "com.android.internal.widget.IRemoteViewsFactory"

    #@6d
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@70
    .line 99
    invoke-virtual {p0}, Lcom/android/internal/widget/IRemoteViewsFactory$Stub;->getLoadingView()Landroid/widget/RemoteViews;

    #@73
    move-result-object v1

    #@74
    .line 100
    .restart local v1       #_result:Landroid/widget/RemoteViews;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@77
    .line 101
    if-eqz v1, :cond_80

    #@79
    .line 102
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@7c
    .line 103
    invoke-virtual {v1, p3, v4}, Landroid/widget/RemoteViews;->writeToParcel(Landroid/os/Parcel;I)V

    #@7f
    goto :goto_9

    #@80
    .line 106
    :cond_80
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@83
    goto :goto_9

    #@84
    .line 112
    .end local v1           #_result:Landroid/widget/RemoteViews;
    :sswitch_84
    const-string v3, "com.android.internal.widget.IRemoteViewsFactory"

    #@86
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@89
    .line 113
    invoke-virtual {p0}, Lcom/android/internal/widget/IRemoteViewsFactory$Stub;->getViewTypeCount()I

    #@8c
    move-result v1

    #@8d
    .line 114
    .local v1, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@90
    .line 115
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@93
    goto/16 :goto_9

    #@95
    .line 120
    .end local v1           #_result:I
    :sswitch_95
    const-string v3, "com.android.internal.widget.IRemoteViewsFactory"

    #@97
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@9a
    .line 122
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@9d
    move-result v0

    #@9e
    .line 123
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/IRemoteViewsFactory$Stub;->getItemId(I)J

    #@a1
    move-result-wide v1

    #@a2
    .line 124
    .local v1, _result:J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@a5
    .line 125
    invoke-virtual {p3, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    #@a8
    goto/16 :goto_9

    #@aa
    .line 130
    .end local v0           #_arg0:I
    .end local v1           #_result:J
    :sswitch_aa
    const-string v5, "com.android.internal.widget.IRemoteViewsFactory"

    #@ac
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@af
    .line 131
    invoke-virtual {p0}, Lcom/android/internal/widget/IRemoteViewsFactory$Stub;->hasStableIds()Z

    #@b2
    move-result v1

    #@b3
    .line 132
    .local v1, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@b6
    .line 133
    if-eqz v1, :cond_b9

    #@b8
    move v3, v4

    #@b9
    :cond_b9
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@bc
    goto/16 :goto_9

    #@be
    .line 138
    .end local v1           #_result:Z
    :sswitch_be
    const-string v5, "com.android.internal.widget.IRemoteViewsFactory"

    #@c0
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@c3
    .line 139
    invoke-virtual {p0}, Lcom/android/internal/widget/IRemoteViewsFactory$Stub;->isCreated()Z

    #@c6
    move-result v1

    #@c7
    .line 140
    .restart local v1       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@ca
    .line 141
    if-eqz v1, :cond_cd

    #@cc
    move v3, v4

    #@cd
    :cond_cd
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@d0
    goto/16 :goto_9

    #@d2
    .line 39
    :sswitch_data_d2
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_1c
        0x3 -> :sswitch_25
        0x4 -> :sswitch_3e
        0x5 -> :sswitch_4e
        0x6 -> :sswitch_6b
        0x7 -> :sswitch_84
        0x8 -> :sswitch_95
        0x9 -> :sswitch_aa
        0xa -> :sswitch_be
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
