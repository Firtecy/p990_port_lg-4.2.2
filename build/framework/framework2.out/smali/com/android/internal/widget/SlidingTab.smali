.class public Lcom/android/internal/widget/SlidingTab;
.super Landroid/view/ViewGroup;
.source "SlidingTab.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/widget/SlidingTab$Slider;,
        Lcom/android/internal/widget/SlidingTab$OnTriggerListener;
    }
.end annotation


# static fields
.field private static final ANIM_DURATION:I = 0xfa

.field private static final ANIM_TARGET_TIME:I = 0x1f4

.field private static final DBG:Z = false

.field private static final HORIZONTAL:I = 0x0

.field private static final LOG_TAG:Ljava/lang/String; = "SlidingTab"

.field private static final THRESHOLD:F = 0.6666667f

.field private static final TRACKING_MARGIN:I = 0x32

.field private static final VERTICAL:I = 0x1

.field private static final VIBRATE_LONG:J = 0x28L

.field private static final VIBRATE_SHORT:J = 0x1eL


# instance fields
.field private mAnimating:Z

.field private final mAnimationDoneListener:Landroid/view/animation/Animation$AnimationListener;

.field private mCurrentSlider:Lcom/android/internal/widget/SlidingTab$Slider;

.field private final mDensity:F

.field private mGrabbedState:I

.field private mHoldLeftOnTransition:Z

.field private mHoldRightOnTransition:Z

.field private final mLeftSlider:Lcom/android/internal/widget/SlidingTab$Slider;

.field private mOnTriggerListener:Lcom/android/internal/widget/SlidingTab$OnTriggerListener;

.field private final mOrientation:I

.field private mOtherSlider:Lcom/android/internal/widget/SlidingTab$Slider;

.field private final mRightSlider:Lcom/android/internal/widget/SlidingTab$Slider;

.field private mThreshold:F

.field private final mTmpRect:Landroid/graphics/Rect;

.field private mTracking:Z

.field private mTriggered:Z

.field private mVibrator:Landroid/os/Vibrator;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 439
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Lcom/android/internal/widget/SlidingTab;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 440
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 9
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    const v5, 0x10803b4

    #@3
    const/4 v2, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 446
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@8
    .line 66
    iput-boolean v2, p0, Lcom/android/internal/widget/SlidingTab;->mHoldLeftOnTransition:Z

    #@a
    .line 67
    iput-boolean v2, p0, Lcom/android/internal/widget/SlidingTab;->mHoldRightOnTransition:Z

    #@c
    .line 70
    iput v3, p0, Lcom/android/internal/widget/SlidingTab;->mGrabbedState:I

    #@e
    .line 71
    iput-boolean v3, p0, Lcom/android/internal/widget/SlidingTab;->mTriggered:Z

    #@10
    .line 92
    new-instance v2, Lcom/android/internal/widget/SlidingTab$1;

    #@12
    invoke-direct {v2, p0}, Lcom/android/internal/widget/SlidingTab$1;-><init>(Lcom/android/internal/widget/SlidingTab;)V

    #@15
    iput-object v2, p0, Lcom/android/internal/widget/SlidingTab;->mAnimationDoneListener:Landroid/view/animation/Animation$AnimationListener;

    #@17
    .line 449
    new-instance v2, Landroid/graphics/Rect;

    #@19
    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    #@1c
    iput-object v2, p0, Lcom/android/internal/widget/SlidingTab;->mTmpRect:Landroid/graphics/Rect;

    #@1e
    .line 451
    sget-object v2, Lcom/android/internal/R$styleable;->SlidingTab:[I

    #@20
    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@23
    move-result-object v0

    #@24
    .line 452
    .local v0, a:Landroid/content/res/TypedArray;
    invoke-virtual {v0, v3, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    #@27
    move-result v2

    #@28
    iput v2, p0, Lcom/android/internal/widget/SlidingTab;->mOrientation:I

    #@2a
    .line 453
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@2d
    .line 455
    invoke-virtual {p0}, Lcom/android/internal/widget/SlidingTab;->getResources()Landroid/content/res/Resources;

    #@30
    move-result-object v1

    #@31
    .line 456
    .local v1, r:Landroid/content/res/Resources;
    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@34
    move-result-object v2

    #@35
    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    #@37
    iput v2, p0, Lcom/android/internal/widget/SlidingTab;->mDensity:F

    #@39
    .line 459
    new-instance v2, Lcom/android/internal/widget/SlidingTab$Slider;

    #@3b
    const v3, 0x10803a6

    #@3e
    const v4, 0x1080395

    #@41
    invoke-direct {v2, p0, v3, v4, v5}, Lcom/android/internal/widget/SlidingTab$Slider;-><init>(Landroid/view/ViewGroup;III)V

    #@44
    iput-object v2, p0, Lcom/android/internal/widget/SlidingTab;->mLeftSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@46
    .line 463
    new-instance v2, Lcom/android/internal/widget/SlidingTab$Slider;

    #@48
    const v3, 0x10803af

    #@4b
    const v4, 0x108039e

    #@4e
    invoke-direct {v2, p0, v3, v4, v5}, Lcom/android/internal/widget/SlidingTab$Slider;-><init>(Landroid/view/ViewGroup;III)V

    #@51
    iput-object v2, p0, Lcom/android/internal/widget/SlidingTab;->mRightSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@53
    .line 469
    return-void
.end method

.method static synthetic access$000(Lcom/android/internal/widget/SlidingTab;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 53
    invoke-direct {p0}, Lcom/android/internal/widget/SlidingTab;->onAnimationDone()V

    #@3
    return-void
.end method

.method static synthetic access$202(Lcom/android/internal/widget/SlidingTab;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/android/internal/widget/SlidingTab;->mAnimating:Z

    #@2
    return p1
.end method

.method static synthetic access$300(Lcom/android/internal/widget/SlidingTab;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 53
    invoke-direct {p0}, Lcom/android/internal/widget/SlidingTab;->resetView()V

    #@3
    return-void
.end method

.method static synthetic access$400(Lcom/android/internal/widget/SlidingTab;)Landroid/view/animation/Animation$AnimationListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 53
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab;->mAnimationDoneListener:Landroid/view/animation/Animation$AnimationListener;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Lcom/android/internal/widget/SlidingTab;)Lcom/android/internal/widget/SlidingTab$Slider;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 53
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab;->mLeftSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@2
    return-object v0
.end method

.method static synthetic access$600(Lcom/android/internal/widget/SlidingTab;)Lcom/android/internal/widget/SlidingTab$Slider;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 53
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab;->mRightSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@2
    return-object v0
.end method

.method private cancelGrab()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x0

    #@2
    .line 624
    iput-boolean v2, p0, Lcom/android/internal/widget/SlidingTab;->mTracking:Z

    #@4
    .line 625
    iput-boolean v2, p0, Lcom/android/internal/widget/SlidingTab;->mTriggered:Z

    #@6
    .line 626
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab;->mOtherSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@8
    const/4 v1, 0x1

    #@9
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/SlidingTab$Slider;->show(Z)V

    #@c
    .line 627
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab;->mCurrentSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@e
    invoke-virtual {v0, v2}, Lcom/android/internal/widget/SlidingTab$Slider;->reset(Z)V

    #@11
    .line 628
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab;->mCurrentSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@13
    invoke-virtual {v0}, Lcom/android/internal/widget/SlidingTab$Slider;->hideTarget()V

    #@16
    .line 629
    iput-object v3, p0, Lcom/android/internal/widget/SlidingTab;->mCurrentSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@18
    .line 630
    iput-object v3, p0, Lcom/android/internal/widget/SlidingTab;->mOtherSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@1a
    .line 631
    invoke-direct {p0, v2}, Lcom/android/internal/widget/SlidingTab;->setGrabbedState(I)V

    #@1d
    .line 632
    return-void
.end method

.method private dispatchTriggerEvent(I)V
    .registers 4
    .parameter "whichHandle"

    #@0
    .prologue
    .line 842
    const-wide/16 v0, 0x28

    #@2
    invoke-direct {p0, v0, v1}, Lcom/android/internal/widget/SlidingTab;->vibrate(J)V

    #@5
    .line 843
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab;->mOnTriggerListener:Lcom/android/internal/widget/SlidingTab$OnTriggerListener;

    #@7
    if-eqz v0, :cond_e

    #@9
    .line 844
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab;->mOnTriggerListener:Lcom/android/internal/widget/SlidingTab$OnTriggerListener;

    #@b
    invoke-interface {v0, p0, p1}, Lcom/android/internal/widget/SlidingTab$OnTriggerListener;->onTrigger(Landroid/view/View;I)V

    #@e
    .line 846
    :cond_e
    return-void
.end method

.method private isHorizontal()Z
    .registers 2

    #@0
    .prologue
    .line 714
    iget v0, p0, Lcom/android/internal/widget/SlidingTab;->mOrientation:I

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method private log(Ljava/lang/String;)V
    .registers 3
    .parameter "msg"

    #@0
    .prologue
    .line 873
    const-string v0, "SlidingTab"

    #@2
    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 874
    return-void
.end method

.method private moveHandle(FF)V
    .registers 9
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 733
    iget-object v4, p0, Lcom/android/internal/widget/SlidingTab;->mCurrentSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@2
    invoke-static {v4}, Lcom/android/internal/widget/SlidingTab$Slider;->access$100(Lcom/android/internal/widget/SlidingTab$Slider;)Landroid/widget/ImageView;

    #@5
    move-result-object v3

    #@6
    .line 734
    .local v3, handle:Landroid/view/View;
    iget-object v4, p0, Lcom/android/internal/widget/SlidingTab;->mCurrentSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@8
    invoke-static {v4}, Lcom/android/internal/widget/SlidingTab$Slider;->access$700(Lcom/android/internal/widget/SlidingTab$Slider;)Landroid/widget/TextView;

    #@b
    move-result-object v0

    #@c
    .line 735
    .local v0, content:Landroid/view/View;
    invoke-direct {p0}, Lcom/android/internal/widget/SlidingTab;->isHorizontal()Z

    #@f
    move-result v4

    #@10
    if-eqz v4, :cond_2a

    #@12
    .line 736
    float-to-int v4, p1

    #@13
    invoke-virtual {v3}, Landroid/widget/ImageView;->getLeft()I

    #@16
    move-result v5

    #@17
    sub-int/2addr v4, v5

    #@18
    invoke-virtual {v3}, Landroid/widget/ImageView;->getWidth()I

    #@1b
    move-result v5

    #@1c
    div-int/lit8 v5, v5, 0x2

    #@1e
    sub-int v1, v4, v5

    #@20
    .line 737
    .local v1, deltaX:I
    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->offsetLeftAndRight(I)V

    #@23
    .line 738
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->offsetLeftAndRight(I)V

    #@26
    .line 744
    .end local v1           #deltaX:I
    :goto_26
    invoke-virtual {p0}, Lcom/android/internal/widget/SlidingTab;->invalidate()V

    #@29
    .line 745
    return-void

    #@2a
    .line 740
    :cond_2a
    float-to-int v4, p2

    #@2b
    invoke-virtual {v3}, Landroid/widget/ImageView;->getTop()I

    #@2e
    move-result v5

    #@2f
    sub-int/2addr v4, v5

    #@30
    invoke-virtual {v3}, Landroid/widget/ImageView;->getHeight()I

    #@33
    move-result v5

    #@34
    div-int/lit8 v5, v5, 0x2

    #@36
    sub-int v2, v4, v5

    #@38
    .line 741
    .local v2, deltaY:I
    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->offsetTopAndBottom(I)V

    #@3b
    .line 742
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->offsetTopAndBottom(I)V

    #@3e
    goto :goto_26
.end method

.method private onAnimationDone()V
    .registers 2

    #@0
    .prologue
    .line 704
    invoke-direct {p0}, Lcom/android/internal/widget/SlidingTab;->resetView()V

    #@3
    .line 705
    const/4 v0, 0x0

    #@4
    iput-boolean v0, p0, Lcom/android/internal/widget/SlidingTab;->mAnimating:Z

    #@6
    .line 706
    return-void
.end method

.method private resetView()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 718
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab;->mLeftSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@3
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/SlidingTab$Slider;->reset(Z)V

    #@6
    .line 719
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab;->mRightSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@8
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/SlidingTab$Slider;->reset(Z)V

    #@b
    .line 721
    return-void
.end method

.method private setGrabbedState(I)V
    .registers 4
    .parameter "newState"

    #@0
    .prologue
    .line 864
    iget v0, p0, Lcom/android/internal/widget/SlidingTab;->mGrabbedState:I

    #@2
    if-eq p1, v0, :cond_11

    #@4
    .line 865
    iput p1, p0, Lcom/android/internal/widget/SlidingTab;->mGrabbedState:I

    #@6
    .line 866
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab;->mOnTriggerListener:Lcom/android/internal/widget/SlidingTab$OnTriggerListener;

    #@8
    if-eqz v0, :cond_11

    #@a
    .line 867
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab;->mOnTriggerListener:Lcom/android/internal/widget/SlidingTab$OnTriggerListener;

    #@c
    iget v1, p0, Lcom/android/internal/widget/SlidingTab;->mGrabbedState:I

    #@e
    invoke-interface {v0, p0, v1}, Lcom/android/internal/widget/SlidingTab$OnTriggerListener;->onGrabbedStateChange(Landroid/view/View;I)V

    #@11
    .line 870
    :cond_11
    return-void
.end method

.method private declared-synchronized vibrate(J)V
    .registers 8
    .parameter "duration"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 816
    monitor-enter p0

    #@2
    :try_start_2
    iget-object v1, p0, Lcom/android/internal/widget/SlidingTab;->mContext:Landroid/content/Context;

    #@4
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7
    move-result-object v1

    #@8
    const-string v2, "haptic_feedback_enabled"

    #@a
    const/4 v3, 0x1

    #@b
    const/4 v4, -0x2

    #@c
    invoke-static {v1, v2, v3, v4}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    #@f
    move-result v1

    #@10
    if-eqz v1, :cond_2d

    #@12
    .line 819
    .local v0, hapticEnabled:Z
    :goto_12
    if-eqz v0, :cond_2b

    #@14
    .line 820
    iget-object v1, p0, Lcom/android/internal/widget/SlidingTab;->mVibrator:Landroid/os/Vibrator;

    #@16
    if-nez v1, :cond_26

    #@18
    .line 821
    invoke-virtual {p0}, Lcom/android/internal/widget/SlidingTab;->getContext()Landroid/content/Context;

    #@1b
    move-result-object v1

    #@1c
    const-string v2, "vibrator"

    #@1e
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@21
    move-result-object v1

    #@22
    check-cast v1, Landroid/os/Vibrator;

    #@24
    iput-object v1, p0, Lcom/android/internal/widget/SlidingTab;->mVibrator:Landroid/os/Vibrator;

    #@26
    .line 824
    :cond_26
    iget-object v1, p0, Lcom/android/internal/widget/SlidingTab;->mVibrator:Landroid/os/Vibrator;

    #@28
    invoke-virtual {v1, p1, p2}, Landroid/os/Vibrator;->vibrate(J)V
    :try_end_2b
    .catchall {:try_start_2 .. :try_end_2b} :catchall_2f

    #@2b
    .line 826
    :cond_2b
    monitor-exit p0

    #@2c
    return-void

    #@2d
    .line 816
    .end local v0           #hapticEnabled:Z
    :cond_2d
    const/4 v0, 0x0

    #@2e
    goto :goto_12

    #@2f
    :catchall_2f
    move-exception v1

    #@30
    monitor-exit p0

    #@31
    throw v1
.end method

.method private withinView(FFLandroid/view/View;)Z
    .registers 6
    .parameter "x"
    .parameter "y"
    .parameter "view"

    #@0
    .prologue
    const/high16 v1, -0x3db8

    #@2
    .line 709
    invoke-direct {p0}, Lcom/android/internal/widget/SlidingTab;->isHorizontal()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_17

    #@8
    cmpl-float v0, p2, v1

    #@a
    if-lez v0, :cond_17

    #@c
    invoke-virtual {p3}, Landroid/view/View;->getHeight()I

    #@f
    move-result v0

    #@10
    add-int/lit8 v0, v0, 0x32

    #@12
    int-to-float v0, v0

    #@13
    cmpg-float v0, p2, v0

    #@15
    if-ltz v0, :cond_2c

    #@17
    :cond_17
    invoke-direct {p0}, Lcom/android/internal/widget/SlidingTab;->isHorizontal()Z

    #@1a
    move-result v0

    #@1b
    if-nez v0, :cond_2e

    #@1d
    cmpl-float v0, p1, v1

    #@1f
    if-lez v0, :cond_2e

    #@21
    invoke-virtual {p3}, Landroid/view/View;->getWidth()I

    #@24
    move-result v0

    #@25
    add-int/lit8 v0, v0, 0x32

    #@27
    int-to-float v0, v0

    #@28
    cmpg-float v0, p1, v0

    #@2a
    if-gez v0, :cond_2e

    #@2c
    :cond_2c
    const/4 v0, 0x1

    #@2d
    :goto_2d
    return v0

    #@2e
    :cond_2e
    const/4 v0, 0x0

    #@2f
    goto :goto_2d
.end method


# virtual methods
.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 16
    .parameter "event"

    #@0
    .prologue
    const v7, 0x3f2aaaab

    #@3
    const v8, 0x3eaaaaaa

    #@6
    const/4 v9, 0x0

    #@7
    const/4 v10, 0x1

    #@8
    .line 508
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@b
    move-result v0

    #@c
    .line 509
    .local v0, action:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@f
    move-result v5

    #@10
    .line 510
    .local v5, x:F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@13
    move-result v6

    #@14
    .line 512
    .local v6, y:F
    iget-boolean v11, p0, Lcom/android/internal/widget/SlidingTab;->mAnimating:Z

    #@16
    if-eqz v11, :cond_1a

    #@18
    move v7, v9

    #@19
    .line 551
    :goto_19
    return v7

    #@1a
    .line 516
    :cond_1a
    iget-object v11, p0, Lcom/android/internal/widget/SlidingTab;->mLeftSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@1c
    invoke-static {v11}, Lcom/android/internal/widget/SlidingTab$Slider;->access$100(Lcom/android/internal/widget/SlidingTab$Slider;)Landroid/widget/ImageView;

    #@1f
    move-result-object v1

    #@20
    .line 517
    .local v1, leftHandle:Landroid/view/View;
    iget-object v11, p0, Lcom/android/internal/widget/SlidingTab;->mTmpRect:Landroid/graphics/Rect;

    #@22
    invoke-virtual {v1, v11}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    #@25
    .line 518
    iget-object v11, p0, Lcom/android/internal/widget/SlidingTab;->mTmpRect:Landroid/graphics/Rect;

    #@27
    float-to-int v12, v5

    #@28
    float-to-int v13, v6

    #@29
    invoke-virtual {v11, v12, v13}, Landroid/graphics/Rect;->contains(II)Z

    #@2c
    move-result v2

    #@2d
    .line 520
    .local v2, leftHit:Z
    iget-object v11, p0, Lcom/android/internal/widget/SlidingTab;->mRightSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@2f
    invoke-static {v11}, Lcom/android/internal/widget/SlidingTab$Slider;->access$100(Lcom/android/internal/widget/SlidingTab$Slider;)Landroid/widget/ImageView;

    #@32
    move-result-object v3

    #@33
    .line 521
    .local v3, rightHandle:Landroid/view/View;
    iget-object v11, p0, Lcom/android/internal/widget/SlidingTab;->mTmpRect:Landroid/graphics/Rect;

    #@35
    invoke-virtual {v3, v11}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    #@38
    .line 522
    iget-object v11, p0, Lcom/android/internal/widget/SlidingTab;->mTmpRect:Landroid/graphics/Rect;

    #@3a
    float-to-int v12, v5

    #@3b
    float-to-int v13, v6

    #@3c
    invoke-virtual {v11, v12, v13}, Landroid/graphics/Rect;->contains(II)Z

    #@3f
    move-result v4

    #@40
    .line 524
    .local v4, rightHit:Z
    iget-boolean v11, p0, Lcom/android/internal/widget/SlidingTab;->mTracking:Z

    #@42
    if-nez v11, :cond_4a

    #@44
    if-nez v2, :cond_4a

    #@46
    if-nez v4, :cond_4a

    #@48
    move v7, v9

    #@49
    .line 525
    goto :goto_19

    #@4a
    .line 528
    :cond_4a
    packed-switch v0, :pswitch_data_96

    #@4d
    :goto_4d
    move v7, v10

    #@4e
    .line 551
    goto :goto_19

    #@4f
    .line 530
    :pswitch_4f
    iput-boolean v10, p0, Lcom/android/internal/widget/SlidingTab;->mTracking:Z

    #@51
    .line 531
    iput-boolean v9, p0, Lcom/android/internal/widget/SlidingTab;->mTriggered:Z

    #@53
    .line 532
    const-wide/16 v11, 0x1e

    #@55
    invoke-direct {p0, v11, v12}, Lcom/android/internal/widget/SlidingTab;->vibrate(J)V

    #@58
    .line 533
    if-eqz v2, :cond_7f

    #@5a
    .line 534
    iget-object v9, p0, Lcom/android/internal/widget/SlidingTab;->mLeftSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@5c
    iput-object v9, p0, Lcom/android/internal/widget/SlidingTab;->mCurrentSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@5e
    .line 535
    iget-object v9, p0, Lcom/android/internal/widget/SlidingTab;->mRightSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@60
    iput-object v9, p0, Lcom/android/internal/widget/SlidingTab;->mOtherSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@62
    .line 536
    invoke-direct {p0}, Lcom/android/internal/widget/SlidingTab;->isHorizontal()Z

    #@65
    move-result v9

    #@66
    if-eqz v9, :cond_7d

    #@68
    :goto_68
    iput v7, p0, Lcom/android/internal/widget/SlidingTab;->mThreshold:F

    #@6a
    .line 537
    invoke-direct {p0, v10}, Lcom/android/internal/widget/SlidingTab;->setGrabbedState(I)V

    #@6d
    .line 544
    :goto_6d
    iget-object v7, p0, Lcom/android/internal/widget/SlidingTab;->mCurrentSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@6f
    invoke-virtual {v7, v10}, Lcom/android/internal/widget/SlidingTab$Slider;->setState(I)V

    #@72
    .line 545
    iget-object v7, p0, Lcom/android/internal/widget/SlidingTab;->mCurrentSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@74
    invoke-virtual {v7}, Lcom/android/internal/widget/SlidingTab$Slider;->showTarget()V

    #@77
    .line 546
    iget-object v7, p0, Lcom/android/internal/widget/SlidingTab;->mOtherSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@79
    invoke-virtual {v7}, Lcom/android/internal/widget/SlidingTab$Slider;->hide()V

    #@7c
    goto :goto_4d

    #@7d
    :cond_7d
    move v7, v8

    #@7e
    .line 536
    goto :goto_68

    #@7f
    .line 539
    :cond_7f
    iget-object v9, p0, Lcom/android/internal/widget/SlidingTab;->mRightSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@81
    iput-object v9, p0, Lcom/android/internal/widget/SlidingTab;->mCurrentSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@83
    .line 540
    iget-object v9, p0, Lcom/android/internal/widget/SlidingTab;->mLeftSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@85
    iput-object v9, p0, Lcom/android/internal/widget/SlidingTab;->mOtherSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@87
    .line 541
    invoke-direct {p0}, Lcom/android/internal/widget/SlidingTab;->isHorizontal()Z

    #@8a
    move-result v9

    #@8b
    if-eqz v9, :cond_94

    #@8d
    :goto_8d
    iput v8, p0, Lcom/android/internal/widget/SlidingTab;->mThreshold:F

    #@8f
    .line 542
    const/4 v7, 0x2

    #@90
    invoke-direct {p0, v7}, Lcom/android/internal/widget/SlidingTab;->setGrabbedState(I)V

    #@93
    goto :goto_6d

    #@94
    :cond_94
    move v8, v7

    #@95
    .line 541
    goto :goto_8d

    #@96
    .line 528
    :pswitch_data_96
    .packed-switch 0x0
        :pswitch_4f
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .registers 12
    .parameter "changed"
    .parameter "l"
    .parameter "t"
    .parameter "r"
    .parameter "b"

    #@0
    .prologue
    .line 725
    if-nez p1, :cond_3

    #@2
    .line 730
    :goto_2
    return-void

    #@3
    .line 728
    :cond_3
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab;->mLeftSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@5
    invoke-direct {p0}, Lcom/android/internal/widget/SlidingTab;->isHorizontal()Z

    #@8
    move-result v1

    #@9
    if-eqz v1, :cond_24

    #@b
    const/4 v5, 0x0

    #@c
    :goto_c
    move v1, p2

    #@d
    move v2, p3

    #@e
    move v3, p4

    #@f
    move v4, p5

    #@10
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/widget/SlidingTab$Slider;->layout(IIIII)V

    #@13
    .line 729
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab;->mRightSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@15
    invoke-direct {p0}, Lcom/android/internal/widget/SlidingTab;->isHorizontal()Z

    #@18
    move-result v1

    #@19
    if-eqz v1, :cond_26

    #@1b
    const/4 v5, 0x1

    #@1c
    :goto_1c
    move v1, p2

    #@1d
    move v2, p3

    #@1e
    move v3, p4

    #@1f
    move v4, p5

    #@20
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/widget/SlidingTab$Slider;->layout(IIIII)V

    #@23
    goto :goto_2

    #@24
    .line 728
    :cond_24
    const/4 v5, 0x3

    #@25
    goto :goto_c

    #@26
    .line 729
    :cond_26
    const/4 v5, 0x2

    #@27
    goto :goto_1c
.end method

.method protected onMeasure(II)V
    .registers 14
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    .line 473
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@3
    move-result v8

    #@4
    .line 474
    .local v8, widthSpecMode:I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@7
    move-result v9

    #@8
    .line 476
    .local v9, widthSpecSize:I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@b
    move-result v1

    #@c
    .line 477
    .local v1, heightSpecMode:I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@f
    move-result v2

    #@10
    .line 488
    .local v2, heightSpecSize:I
    iget-object v10, p0, Lcom/android/internal/widget/SlidingTab;->mLeftSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@12
    invoke-virtual {v10}, Lcom/android/internal/widget/SlidingTab$Slider;->measure()V

    #@15
    .line 489
    iget-object v10, p0, Lcom/android/internal/widget/SlidingTab;->mRightSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@17
    invoke-virtual {v10}, Lcom/android/internal/widget/SlidingTab$Slider;->measure()V

    #@1a
    .line 490
    iget-object v10, p0, Lcom/android/internal/widget/SlidingTab;->mLeftSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@1c
    invoke-virtual {v10}, Lcom/android/internal/widget/SlidingTab$Slider;->getTabWidth()I

    #@1f
    move-result v4

    #@20
    .line 491
    .local v4, leftTabWidth:I
    iget-object v10, p0, Lcom/android/internal/widget/SlidingTab;->mRightSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@22
    invoke-virtual {v10}, Lcom/android/internal/widget/SlidingTab$Slider;->getTabWidth()I

    #@25
    move-result v6

    #@26
    .line 492
    .local v6, rightTabWidth:I
    iget-object v10, p0, Lcom/android/internal/widget/SlidingTab;->mLeftSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@28
    invoke-virtual {v10}, Lcom/android/internal/widget/SlidingTab$Slider;->getTabHeight()I

    #@2b
    move-result v3

    #@2c
    .line 493
    .local v3, leftTabHeight:I
    iget-object v10, p0, Lcom/android/internal/widget/SlidingTab;->mRightSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@2e
    invoke-virtual {v10}, Lcom/android/internal/widget/SlidingTab$Slider;->getTabHeight()I

    #@31
    move-result v5

    #@32
    .line 496
    .local v5, rightTabHeight:I
    invoke-direct {p0}, Lcom/android/internal/widget/SlidingTab;->isHorizontal()Z

    #@35
    move-result v10

    #@36
    if-eqz v10, :cond_46

    #@38
    .line 497
    add-int v10, v4, v6

    #@3a
    invoke-static {v9, v10}, Ljava/lang/Math;->max(II)I

    #@3d
    move-result v7

    #@3e
    .line 498
    .local v7, width:I
    invoke-static {v3, v5}, Ljava/lang/Math;->max(II)I

    #@41
    move-result v0

    #@42
    .line 503
    .local v0, height:I
    :goto_42
    invoke-virtual {p0, v7, v0}, Lcom/android/internal/widget/SlidingTab;->setMeasuredDimension(II)V

    #@45
    .line 504
    return-void

    #@46
    .line 500
    .end local v0           #height:I
    .end local v7           #width:I
    :cond_46
    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    #@49
    move-result v7

    #@4a
    .line 501
    .restart local v7       #width:I
    add-int v10, v3, v5

    #@4c
    invoke-static {v2, v10}, Ljava/lang/Math;->max(II)I

    #@4f
    move-result v0

    #@50
    .restart local v0       #height:I
    goto :goto_42
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 14
    .parameter "event"

    #@0
    .prologue
    const/4 v10, 0x2

    #@1
    const/4 v8, 0x1

    #@2
    const/4 v9, 0x0

    #@3
    .line 579
    iget-boolean v7, p0, Lcom/android/internal/widget/SlidingTab;->mTracking:Z

    #@5
    if-eqz v7, :cond_16

    #@7
    .line 580
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@a
    move-result v0

    #@b
    .line 581
    .local v0, action:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@e
    move-result v5

    #@f
    .line 582
    .local v5, x:F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@12
    move-result v6

    #@13
    .line 584
    .local v6, y:F
    packed-switch v0, :pswitch_data_ac

    #@16
    .line 620
    .end local v0           #action:I
    .end local v5           #x:F
    .end local v6           #y:F
    :cond_16
    :goto_16
    iget-boolean v7, p0, Lcom/android/internal/widget/SlidingTab;->mTracking:Z

    #@18
    if-nez v7, :cond_20

    #@1a
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onTouchEvent(Landroid/view/MotionEvent;)Z

    #@1d
    move-result v7

    #@1e
    if-eqz v7, :cond_21

    #@20
    :cond_20
    move v9, v8

    #@21
    :cond_21
    return v9

    #@22
    .line 586
    .restart local v0       #action:I
    .restart local v5       #x:F
    .restart local v6       #y:F
    :pswitch_22
    invoke-direct {p0, v5, v6, p0}, Lcom/android/internal/widget/SlidingTab;->withinView(FFLandroid/view/View;)Z

    #@25
    move-result v7

    #@26
    if-eqz v7, :cond_a7

    #@28
    .line 587
    invoke-direct {p0, v5, v6}, Lcom/android/internal/widget/SlidingTab;->moveHandle(FF)V

    #@2b
    .line 588
    invoke-direct {p0}, Lcom/android/internal/widget/SlidingTab;->isHorizontal()Z

    #@2e
    move-result v7

    #@2f
    if-eqz v7, :cond_79

    #@31
    move v2, v5

    #@32
    .line 589
    .local v2, position:F
    :goto_32
    iget v11, p0, Lcom/android/internal/widget/SlidingTab;->mThreshold:F

    #@34
    invoke-direct {p0}, Lcom/android/internal/widget/SlidingTab;->isHorizontal()Z

    #@37
    move-result v7

    #@38
    if-eqz v7, :cond_7b

    #@3a
    invoke-virtual {p0}, Lcom/android/internal/widget/SlidingTab;->getWidth()I

    #@3d
    move-result v7

    #@3e
    :goto_3e
    int-to-float v7, v7

    #@3f
    mul-float v3, v11, v7

    #@41
    .line 591
    .local v3, target:F
    invoke-direct {p0}, Lcom/android/internal/widget/SlidingTab;->isHorizontal()Z

    #@44
    move-result v7

    #@45
    if-eqz v7, :cond_8a

    #@47
    .line 592
    iget-object v7, p0, Lcom/android/internal/widget/SlidingTab;->mCurrentSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@49
    iget-object v11, p0, Lcom/android/internal/widget/SlidingTab;->mLeftSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@4b
    if-ne v7, v11, :cond_82

    #@4d
    cmpl-float v7, v2, v3

    #@4f
    if-lez v7, :cond_80

    #@51
    move v4, v8

    #@52
    .line 598
    .local v4, thresholdReached:Z
    :goto_52
    iget-boolean v7, p0, Lcom/android/internal/widget/SlidingTab;->mTriggered:Z

    #@54
    if-nez v7, :cond_16

    #@56
    if-eqz v4, :cond_16

    #@58
    .line 599
    iput-boolean v8, p0, Lcom/android/internal/widget/SlidingTab;->mTriggered:Z

    #@5a
    .line 600
    iput-boolean v9, p0, Lcom/android/internal/widget/SlidingTab;->mTracking:Z

    #@5c
    .line 601
    iget-object v7, p0, Lcom/android/internal/widget/SlidingTab;->mCurrentSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@5e
    invoke-virtual {v7, v10}, Lcom/android/internal/widget/SlidingTab$Slider;->setState(I)V

    #@61
    .line 602
    iget-object v7, p0, Lcom/android/internal/widget/SlidingTab;->mCurrentSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@63
    iget-object v11, p0, Lcom/android/internal/widget/SlidingTab;->mLeftSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@65
    if-ne v7, v11, :cond_a0

    #@67
    move v1, v8

    #@68
    .line 603
    .local v1, isLeft:Z
    :goto_68
    if-eqz v1, :cond_a2

    #@6a
    move v7, v8

    #@6b
    :goto_6b
    invoke-direct {p0, v7}, Lcom/android/internal/widget/SlidingTab;->dispatchTriggerEvent(I)V

    #@6e
    .line 606
    if-eqz v1, :cond_a4

    #@70
    iget-boolean v7, p0, Lcom/android/internal/widget/SlidingTab;->mHoldLeftOnTransition:Z

    #@72
    :goto_72
    invoke-virtual {p0, v7}, Lcom/android/internal/widget/SlidingTab;->startAnimating(Z)V

    #@75
    .line 607
    invoke-direct {p0, v9}, Lcom/android/internal/widget/SlidingTab;->setGrabbedState(I)V

    #@78
    goto :goto_16

    #@79
    .end local v1           #isLeft:Z
    .end local v2           #position:F
    .end local v3           #target:F
    .end local v4           #thresholdReached:Z
    :cond_79
    move v2, v6

    #@7a
    .line 588
    goto :goto_32

    #@7b
    .line 589
    .restart local v2       #position:F
    :cond_7b
    invoke-virtual {p0}, Lcom/android/internal/widget/SlidingTab;->getHeight()I

    #@7e
    move-result v7

    #@7f
    goto :goto_3e

    #@80
    .restart local v3       #target:F
    :cond_80
    move v4, v9

    #@81
    .line 592
    goto :goto_52

    #@82
    :cond_82
    cmpg-float v7, v2, v3

    #@84
    if-gez v7, :cond_88

    #@86
    move v4, v8

    #@87
    goto :goto_52

    #@88
    :cond_88
    move v4, v9

    #@89
    goto :goto_52

    #@8a
    .line 595
    :cond_8a
    iget-object v7, p0, Lcom/android/internal/widget/SlidingTab;->mCurrentSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@8c
    iget-object v11, p0, Lcom/android/internal/widget/SlidingTab;->mLeftSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@8e
    if-ne v7, v11, :cond_98

    #@90
    cmpg-float v7, v2, v3

    #@92
    if-gez v7, :cond_96

    #@94
    move v4, v8

    #@95
    .restart local v4       #thresholdReached:Z
    :goto_95
    goto :goto_52

    #@96
    .end local v4           #thresholdReached:Z
    :cond_96
    move v4, v9

    #@97
    goto :goto_95

    #@98
    :cond_98
    cmpl-float v7, v2, v3

    #@9a
    if-lez v7, :cond_9e

    #@9c
    move v4, v8

    #@9d
    goto :goto_95

    #@9e
    :cond_9e
    move v4, v9

    #@9f
    goto :goto_95

    #@a0
    .restart local v4       #thresholdReached:Z
    :cond_a0
    move v1, v9

    #@a1
    .line 602
    goto :goto_68

    #@a2
    .restart local v1       #isLeft:Z
    :cond_a2
    move v7, v10

    #@a3
    .line 603
    goto :goto_6b

    #@a4
    .line 606
    :cond_a4
    iget-boolean v7, p0, Lcom/android/internal/widget/SlidingTab;->mHoldRightOnTransition:Z

    #@a6
    goto :goto_72

    #@a7
    .line 615
    .end local v1           #isLeft:Z
    .end local v2           #position:F
    .end local v3           #target:F
    .end local v4           #thresholdReached:Z
    :cond_a7
    :pswitch_a7
    invoke-direct {p0}, Lcom/android/internal/widget/SlidingTab;->cancelGrab()V

    #@aa
    goto/16 :goto_16

    #@ac
    .line 584
    :pswitch_data_ac
    .packed-switch 0x1
        :pswitch_a7
        :pswitch_22
        :pswitch_a7
    .end packed-switch
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .registers 4
    .parameter "changedView"
    .parameter "visibility"

    #@0
    .prologue
    .line 850
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onVisibilityChanged(Landroid/view/View;I)V

    #@3
    .line 853
    if-ne p1, p0, :cond_e

    #@5
    if-eqz p2, :cond_e

    #@7
    iget v0, p0, Lcom/android/internal/widget/SlidingTab;->mGrabbedState:I

    #@9
    if-eqz v0, :cond_e

    #@b
    .line 855
    invoke-direct {p0}, Lcom/android/internal/widget/SlidingTab;->cancelGrab()V

    #@e
    .line 857
    :cond_e
    return-void
.end method

.method public reset(Z)V
    .registers 3
    .parameter "animate"

    #@0
    .prologue
    .line 561
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab;->mLeftSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/widget/SlidingTab$Slider;->reset(Z)V

    #@5
    .line 562
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab;->mRightSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@7
    invoke-virtual {v0, p1}, Lcom/android/internal/widget/SlidingTab$Slider;->reset(Z)V

    #@a
    .line 563
    if-nez p1, :cond_f

    #@c
    .line 564
    const/4 v0, 0x0

    #@d
    iput-boolean v0, p0, Lcom/android/internal/widget/SlidingTab;->mAnimating:Z

    #@f
    .line 566
    :cond_f
    return-void
.end method

.method public setHoldAfterTrigger(ZZ)V
    .registers 3
    .parameter "holdLeft"
    .parameter "holdRight"

    #@0
    .prologue
    .line 808
    iput-boolean p1, p0, Lcom/android/internal/widget/SlidingTab;->mHoldLeftOnTransition:Z

    #@2
    .line 809
    iput-boolean p2, p0, Lcom/android/internal/widget/SlidingTab;->mHoldRightOnTransition:Z

    #@4
    .line 810
    return-void
.end method

.method public setLeftHintText(I)V
    .registers 3
    .parameter "resId"

    #@0
    .prologue
    .line 772
    invoke-direct {p0}, Lcom/android/internal/widget/SlidingTab;->isHorizontal()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_b

    #@6
    .line 773
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab;->mLeftSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@8
    invoke-virtual {v0, p1}, Lcom/android/internal/widget/SlidingTab$Slider;->setHintText(I)V

    #@b
    .line 775
    :cond_b
    return-void
.end method

.method public setLeftTabResources(IIII)V
    .registers 6
    .parameter "iconId"
    .parameter "targetId"
    .parameter "barId"
    .parameter "tabId"

    #@0
    .prologue
    .line 759
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab;->mLeftSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/widget/SlidingTab$Slider;->setIcon(I)V

    #@5
    .line 760
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab;->mLeftSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@7
    invoke-virtual {v0, p2}, Lcom/android/internal/widget/SlidingTab$Slider;->setTarget(I)V

    #@a
    .line 761
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab;->mLeftSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@c
    invoke-virtual {v0, p3}, Lcom/android/internal/widget/SlidingTab$Slider;->setBarBackgroundResource(I)V

    #@f
    .line 762
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab;->mLeftSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@11
    invoke-virtual {v0, p4}, Lcom/android/internal/widget/SlidingTab$Slider;->setTabBackgroundResource(I)V

    #@14
    .line 763
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab;->mLeftSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@16
    invoke-virtual {v0}, Lcom/android/internal/widget/SlidingTab$Slider;->updateDrawableStates()V

    #@19
    .line 764
    return-void
.end method

.method public setOnTriggerListener(Lcom/android/internal/widget/SlidingTab$OnTriggerListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 834
    iput-object p1, p0, Lcom/android/internal/widget/SlidingTab;->mOnTriggerListener:Lcom/android/internal/widget/SlidingTab$OnTriggerListener;

    #@2
    .line 835
    return-void
.end method

.method public setRightHintText(I)V
    .registers 3
    .parameter "resId"

    #@0
    .prologue
    .line 802
    invoke-direct {p0}, Lcom/android/internal/widget/SlidingTab;->isHorizontal()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_b

    #@6
    .line 803
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab;->mRightSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@8
    invoke-virtual {v0, p1}, Lcom/android/internal/widget/SlidingTab$Slider;->setHintText(I)V

    #@b
    .line 805
    :cond_b
    return-void
.end method

.method public setRightTabResources(IIII)V
    .registers 6
    .parameter "iconId"
    .parameter "targetId"
    .parameter "barId"
    .parameter "tabId"

    #@0
    .prologue
    .line 789
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab;->mRightSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/widget/SlidingTab$Slider;->setIcon(I)V

    #@5
    .line 790
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab;->mRightSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@7
    invoke-virtual {v0, p2}, Lcom/android/internal/widget/SlidingTab$Slider;->setTarget(I)V

    #@a
    .line 791
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab;->mRightSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@c
    invoke-virtual {v0, p3}, Lcom/android/internal/widget/SlidingTab$Slider;->setBarBackgroundResource(I)V

    #@f
    .line 792
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab;->mRightSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@11
    invoke-virtual {v0, p4}, Lcom/android/internal/widget/SlidingTab$Slider;->setTabBackgroundResource(I)V

    #@14
    .line 793
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab;->mRightSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@16
    invoke-virtual {v0}, Lcom/android/internal/widget/SlidingTab$Slider;->updateDrawableStates()V

    #@19
    .line 794
    return-void
.end method

.method public setVisibility(I)V
    .registers 3
    .parameter "visibility"

    #@0
    .prologue
    .line 571
    invoke-virtual {p0}, Lcom/android/internal/widget/SlidingTab;->getVisibility()I

    #@3
    move-result v0

    #@4
    if-eq p1, v0, :cond_d

    #@6
    const/4 v0, 0x4

    #@7
    if-ne p1, v0, :cond_d

    #@9
    .line 572
    const/4 v0, 0x0

    #@a
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/SlidingTab;->reset(Z)V

    #@d
    .line 574
    :cond_d
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->setVisibility(I)V

    #@10
    .line 575
    return-void
.end method

.method startAnimating(Z)V
    .registers 25
    .parameter "holdAfter"

    #@0
    .prologue
    .line 635
    const/16 v19, 0x1

    #@2
    move/from16 v0, v19

    #@4
    move-object/from16 v1, p0

    #@6
    iput-boolean v0, v1, Lcom/android/internal/widget/SlidingTab;->mAnimating:Z

    #@8
    .line 638
    move-object/from16 v0, p0

    #@a
    iget-object v12, v0, Lcom/android/internal/widget/SlidingTab;->mCurrentSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@c
    .line 639
    .local v12, slider:Lcom/android/internal/widget/SlidingTab$Slider;
    move-object/from16 v0, p0

    #@e
    iget-object v10, v0, Lcom/android/internal/widget/SlidingTab;->mOtherSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@10
    .line 642
    .local v10, other:Lcom/android/internal/widget/SlidingTab$Slider;
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/widget/SlidingTab;->isHorizontal()Z

    #@13
    move-result v19

    #@14
    if-eqz v19, :cond_c7

    #@16
    .line 643
    invoke-static {v12}, Lcom/android/internal/widget/SlidingTab$Slider;->access$100(Lcom/android/internal/widget/SlidingTab$Slider;)Landroid/widget/ImageView;

    #@19
    move-result-object v19

    #@1a
    invoke-virtual/range {v19 .. v19}, Landroid/widget/ImageView;->getRight()I

    #@1d
    move-result v11

    #@1e
    .line 644
    .local v11, right:I
    invoke-static {v12}, Lcom/android/internal/widget/SlidingTab$Slider;->access$100(Lcom/android/internal/widget/SlidingTab$Slider;)Landroid/widget/ImageView;

    #@21
    move-result-object v19

    #@22
    invoke-virtual/range {v19 .. v19}, Landroid/widget/ImageView;->getWidth()I

    #@25
    move-result v18

    #@26
    .line 645
    .local v18, width:I
    invoke-static {v12}, Lcom/android/internal/widget/SlidingTab$Slider;->access$100(Lcom/android/internal/widget/SlidingTab$Slider;)Landroid/widget/ImageView;

    #@29
    move-result-object v19

    #@2a
    invoke-virtual/range {v19 .. v19}, Landroid/widget/ImageView;->getLeft()I

    #@2d
    move-result v9

    #@2e
    .line 646
    .local v9, left:I
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/widget/SlidingTab;->getWidth()I

    #@31
    move-result v17

    #@32
    .line 647
    .local v17, viewWidth:I
    if-eqz p1, :cond_bc

    #@34
    const/4 v8, 0x0

    #@35
    .line 648
    .local v8, holdOffset:I
    :goto_35
    move-object/from16 v0, p0

    #@37
    iget-object v0, v0, Lcom/android/internal/widget/SlidingTab;->mRightSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@39
    move-object/from16 v19, v0

    #@3b
    move-object/from16 v0, v19

    #@3d
    if-ne v12, v0, :cond_c0

    #@3f
    add-int v19, v11, v17

    #@41
    sub-int v19, v19, v8

    #@43
    move/from16 v0, v19

    #@45
    neg-int v5, v0

    #@46
    .line 650
    .local v5, dx:I
    :goto_46
    const/4 v6, 0x0

    #@47
    .line 661
    .end local v9           #left:I
    .end local v11           #right:I
    .end local v17           #viewWidth:I
    .end local v18           #width:I
    .local v6, dy:I
    :goto_47
    new-instance v14, Landroid/view/animation/TranslateAnimation;

    #@49
    const/16 v19, 0x0

    #@4b
    int-to-float v0, v5

    #@4c
    move/from16 v20, v0

    #@4e
    const/16 v21, 0x0

    #@50
    int-to-float v0, v6

    #@51
    move/from16 v22, v0

    #@53
    move/from16 v0, v19

    #@55
    move/from16 v1, v20

    #@57
    move/from16 v2, v21

    #@59
    move/from16 v3, v22

    #@5b
    invoke-direct {v14, v0, v1, v2, v3}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    #@5e
    .line 662
    .local v14, trans1:Landroid/view/animation/Animation;
    const-wide/16 v19, 0xfa

    #@60
    move-wide/from16 v0, v19

    #@62
    invoke-virtual {v14, v0, v1}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    #@65
    .line 663
    new-instance v19, Landroid/view/animation/LinearInterpolator;

    #@67
    invoke-direct/range {v19 .. v19}, Landroid/view/animation/LinearInterpolator;-><init>()V

    #@6a
    move-object/from16 v0, v19

    #@6c
    invoke-virtual {v14, v0}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    #@6f
    .line 664
    const/16 v19, 0x1

    #@71
    move/from16 v0, v19

    #@73
    invoke-virtual {v14, v0}, Landroid/view/animation/TranslateAnimation;->setFillAfter(Z)V

    #@76
    .line 665
    new-instance v15, Landroid/view/animation/TranslateAnimation;

    #@78
    const/16 v19, 0x0

    #@7a
    int-to-float v0, v5

    #@7b
    move/from16 v20, v0

    #@7d
    const/16 v21, 0x0

    #@7f
    int-to-float v0, v6

    #@80
    move/from16 v22, v0

    #@82
    move/from16 v0, v19

    #@84
    move/from16 v1, v20

    #@86
    move/from16 v2, v21

    #@88
    move/from16 v3, v22

    #@8a
    invoke-direct {v15, v0, v1, v2, v3}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    #@8d
    .line 666
    .local v15, trans2:Landroid/view/animation/Animation;
    const-wide/16 v19, 0xfa

    #@8f
    move-wide/from16 v0, v19

    #@91
    invoke-virtual {v15, v0, v1}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    #@94
    .line 667
    new-instance v19, Landroid/view/animation/LinearInterpolator;

    #@96
    invoke-direct/range {v19 .. v19}, Landroid/view/animation/LinearInterpolator;-><init>()V

    #@99
    move-object/from16 v0, v19

    #@9b
    invoke-virtual {v15, v0}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    #@9e
    .line 668
    const/16 v19, 0x1

    #@a0
    move/from16 v0, v19

    #@a2
    invoke-virtual {v15, v0}, Landroid/view/animation/TranslateAnimation;->setFillAfter(Z)V

    #@a5
    .line 670
    new-instance v19, Lcom/android/internal/widget/SlidingTab$2;

    #@a7
    move-object/from16 v0, v19

    #@a9
    move-object/from16 v1, p0

    #@ab
    move/from16 v2, p1

    #@ad
    invoke-direct {v0, v1, v2, v5, v6}, Lcom/android/internal/widget/SlidingTab$2;-><init>(Lcom/android/internal/widget/SlidingTab;ZII)V

    #@b0
    move-object/from16 v0, v19

    #@b2
    invoke-virtual {v14, v0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    #@b5
    .line 699
    invoke-virtual {v12}, Lcom/android/internal/widget/SlidingTab$Slider;->hideTarget()V

    #@b8
    .line 700
    invoke-virtual {v12, v14, v15}, Lcom/android/internal/widget/SlidingTab$Slider;->startAnimation(Landroid/view/animation/Animation;Landroid/view/animation/Animation;)V

    #@bb
    .line 701
    return-void

    #@bc
    .end local v5           #dx:I
    .end local v6           #dy:I
    .end local v8           #holdOffset:I
    .end local v14           #trans1:Landroid/view/animation/Animation;
    .end local v15           #trans2:Landroid/view/animation/Animation;
    .restart local v9       #left:I
    .restart local v11       #right:I
    .restart local v17       #viewWidth:I
    .restart local v18       #width:I
    :cond_bc
    move/from16 v8, v18

    #@be
    .line 647
    goto/16 :goto_35

    #@c0
    .line 648
    .restart local v8       #holdOffset:I
    :cond_c0
    sub-int v19, v17, v9

    #@c2
    add-int v19, v19, v17

    #@c4
    sub-int v5, v19, v8

    #@c6
    goto :goto_46

    #@c7
    .line 652
    .end local v8           #holdOffset:I
    .end local v9           #left:I
    .end local v11           #right:I
    .end local v17           #viewWidth:I
    .end local v18           #width:I
    :cond_c7
    invoke-static {v12}, Lcom/android/internal/widget/SlidingTab$Slider;->access$100(Lcom/android/internal/widget/SlidingTab$Slider;)Landroid/widget/ImageView;

    #@ca
    move-result-object v19

    #@cb
    invoke-virtual/range {v19 .. v19}, Landroid/widget/ImageView;->getTop()I

    #@ce
    move-result v13

    #@cf
    .line 653
    .local v13, top:I
    invoke-static {v12}, Lcom/android/internal/widget/SlidingTab$Slider;->access$100(Lcom/android/internal/widget/SlidingTab$Slider;)Landroid/widget/ImageView;

    #@d2
    move-result-object v19

    #@d3
    invoke-virtual/range {v19 .. v19}, Landroid/widget/ImageView;->getBottom()I

    #@d6
    move-result v4

    #@d7
    .line 654
    .local v4, bottom:I
    invoke-static {v12}, Lcom/android/internal/widget/SlidingTab$Slider;->access$100(Lcom/android/internal/widget/SlidingTab$Slider;)Landroid/widget/ImageView;

    #@da
    move-result-object v19

    #@db
    invoke-virtual/range {v19 .. v19}, Landroid/widget/ImageView;->getHeight()I

    #@de
    move-result v7

    #@df
    .line 655
    .local v7, height:I
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/widget/SlidingTab;->getHeight()I

    #@e2
    move-result v16

    #@e3
    .line 656
    .local v16, viewHeight:I
    if-eqz p1, :cond_f7

    #@e5
    const/4 v8, 0x0

    #@e6
    .line 657
    .restart local v8       #holdOffset:I
    :goto_e6
    const/4 v5, 0x0

    #@e7
    .line 658
    .restart local v5       #dx:I
    move-object/from16 v0, p0

    #@e9
    iget-object v0, v0, Lcom/android/internal/widget/SlidingTab;->mRightSlider:Lcom/android/internal/widget/SlidingTab$Slider;

    #@eb
    move-object/from16 v19, v0

    #@ed
    move-object/from16 v0, v19

    #@ef
    if-ne v12, v0, :cond_f9

    #@f1
    add-int v19, v13, v16

    #@f3
    sub-int v6, v19, v8

    #@f5
    .restart local v6       #dy:I
    :goto_f5
    goto/16 :goto_47

    #@f7
    .end local v5           #dx:I
    .end local v6           #dy:I
    .end local v8           #holdOffset:I
    :cond_f7
    move v8, v7

    #@f8
    .line 656
    goto :goto_e6

    #@f9
    .line 658
    .restart local v5       #dx:I
    .restart local v8       #holdOffset:I
    :cond_f9
    sub-int v19, v16, v4

    #@fb
    add-int v19, v19, v16

    #@fd
    sub-int v19, v19, v8

    #@ff
    move/from16 v0, v19

    #@101
    neg-int v6, v0

    #@102
    goto :goto_f5
.end method
