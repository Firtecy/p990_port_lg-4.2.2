.class final Lcom/android/internal/widget/multiwaveview/Ease$Quad$2;
.super Ljava/lang/Object;
.source "Ease.java"

# interfaces
.implements Landroid/animation/TimeInterpolator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/widget/multiwaveview/Ease$Quad;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 60
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .registers 4
    .parameter "input"

    #@0
    .prologue
    .line 62
    const/high16 v0, -0x4080

    #@2
    const/high16 v1, 0x3f80

    #@4
    div-float/2addr p1, v1

    #@5
    mul-float/2addr v0, p1

    #@6
    const/high16 v1, 0x4000

    #@8
    sub-float v1, p1, v1

    #@a
    mul-float/2addr v0, v1

    #@b
    const/4 v1, 0x0

    #@c
    add-float/2addr v0, v1

    #@d
    return v0
.end method
