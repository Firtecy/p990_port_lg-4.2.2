.class public Lcom/android/internal/widget/SizeAdaptiveLayout;
.super Landroid/view/ViewGroup;
.source "SizeAdaptiveLayout.java"


# annotations
.annotation runtime Landroid/widget/RemoteViews$RemoteView;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/widget/SizeAdaptiveLayout$BringToFrontOnEnd;,
        Lcom/android/internal/widget/SizeAdaptiveLayout$LayoutParams;
    }
.end annotation


# static fields
.field private static final CROSSFADE_TIME:J = 0xfaL

.field private static final DEBUG:Z = false

.field private static final MAX_VALID_HEIGHT:I = 0x0

.field private static final MIN_VALID_HEIGHT:I = 0x1

.field private static final REPORT_BAD_BOUNDS:Z = true

.field private static final TAG:Ljava/lang/String; = "SizeAdaptiveLayout"


# instance fields
.field private mActiveChild:Landroid/view/View;

.field private mAnimatorListener:Landroid/animation/Animator$AnimatorListener;

.field private mCanceledAnimationCount:I

.field private mEnteringView:Landroid/view/View;

.field private mFadePanel:Landroid/animation/ObjectAnimator;

.field private mFadeView:Landroid/animation/ObjectAnimator;

.field private mLastActive:Landroid/view/View;

.field private mLeavingView:Landroid/view/View;

.field private mModestyPanel:Landroid/view/View;

.field private mModestyPanelTop:I

.field private mTransitionAnimation:Landroid/animation/AnimatorSet;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 82
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    #@3
    .line 83
    invoke-direct {p0}, Lcom/android/internal/widget/SizeAdaptiveLayout;->initialize()V

    #@6
    .line 84
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 87
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@3
    .line 88
    invoke-direct {p0}, Lcom/android/internal/widget/SizeAdaptiveLayout;->initialize()V

    #@6
    .line 89
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    .line 92
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@3
    .line 93
    invoke-direct {p0}, Lcom/android/internal/widget/SizeAdaptiveLayout;->initialize()V

    #@6
    .line 94
    return-void
.end method

.method static synthetic access$000(Lcom/android/internal/widget/SizeAdaptiveLayout;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 52
    iget v0, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mCanceledAnimationCount:I

    #@2
    return v0
.end method

.method static synthetic access$008(Lcom/android/internal/widget/SizeAdaptiveLayout;)I
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 52
    iget v0, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mCanceledAnimationCount:I

    #@2
    add-int/lit8 v1, v0, 0x1

    #@4
    iput v1, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mCanceledAnimationCount:I

    #@6
    return v0
.end method

.method static synthetic access$010(Lcom/android/internal/widget/SizeAdaptiveLayout;)I
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 52
    iget v0, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mCanceledAnimationCount:I

    #@2
    add-int/lit8 v1, v0, -0x1

    #@4
    iput v1, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mCanceledAnimationCount:I

    #@6
    return v0
.end method

.method static synthetic access$100(Lcom/android/internal/widget/SizeAdaptiveLayout;)Landroid/view/View;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 52
    iget-object v0, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mLeavingView:Landroid/view/View;

    #@2
    return-object v0
.end method

.method static synthetic access$102(Lcom/android/internal/widget/SizeAdaptiveLayout;Landroid/view/View;)Landroid/view/View;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 52
    iput-object p1, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mLeavingView:Landroid/view/View;

    #@2
    return-object p1
.end method

.method static synthetic access$200(Lcom/android/internal/widget/SizeAdaptiveLayout;)Landroid/view/View;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 52
    iget-object v0, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mModestyPanel:Landroid/view/View;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/android/internal/widget/SizeAdaptiveLayout;)Landroid/view/View;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 52
    iget-object v0, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mEnteringView:Landroid/view/View;

    #@2
    return-object v0
.end method

.method static synthetic access$302(Lcom/android/internal/widget/SizeAdaptiveLayout;Landroid/view/View;)Landroid/view/View;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 52
    iput-object p1, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mEnteringView:Landroid/view/View;

    #@2
    return-object p1
.end method

.method private clampSizeToBounds(ILandroid/view/View;)I
    .registers 9
    .parameter "measuredHeight"
    .parameter "child"

    #@0
    .prologue
    .line 171
    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@3
    move-result-object v2

    #@4
    check-cast v2, Lcom/android/internal/widget/SizeAdaptiveLayout$LayoutParams;

    #@6
    .line 173
    .local v2, lp:Lcom/android/internal/widget/SizeAdaptiveLayout$LayoutParams;
    const v3, 0xffffff

    #@9
    and-int v1, v3, p1

    #@b
    .line 174
    .local v1, heightIn:I
    iget v3, v2, Lcom/android/internal/widget/SizeAdaptiveLayout$LayoutParams;->minHeight:I

    #@d
    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    #@10
    move-result v0

    #@11
    .line 175
    .local v0, height:I
    iget v3, v2, Lcom/android/internal/widget/SizeAdaptiveLayout$LayoutParams;->maxHeight:I

    #@13
    const/4 v4, -0x1

    #@14
    if-eq v3, v4, :cond_1c

    #@16
    .line 176
    iget v3, v2, Lcom/android/internal/widget/SizeAdaptiveLayout$LayoutParams;->maxHeight:I

    #@18
    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    #@1b
    move-result v0

    #@1c
    .line 179
    :cond_1c
    if-eq v1, v0, :cond_60

    #@1e
    .line 180
    const-string v3, "SizeAdaptiveLayout"

    #@20
    new-instance v4, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v4

    #@29
    const-string v5, "child view "

    #@2b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v4

    #@2f
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v4

    #@33
    const-string v5, " "

    #@35
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v4

    #@39
    const-string v5, "measured out of bounds at "

    #@3b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v4

    #@3f
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@42
    move-result-object v4

    #@43
    const-string v5, "px "

    #@45
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v4

    #@49
    const-string v5, "clamped to "

    #@4b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v4

    #@4f
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@52
    move-result-object v4

    #@53
    const-string v5, "px"

    #@55
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v4

    #@59
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5c
    move-result-object v4

    #@5d
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@60
    .line 185
    :cond_60
    return v0
.end method

.method private initialize()V
    .registers 10

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    const/4 v7, 0x0

    #@2
    const/4 v5, -0x1

    #@3
    const/4 v6, 0x0

    #@4
    .line 97
    new-instance v3, Landroid/view/View;

    #@6
    invoke-virtual {p0}, Lcom/android/internal/widget/SizeAdaptiveLayout;->getContext()Landroid/content/Context;

    #@9
    move-result-object v4

    #@a
    invoke-direct {v3, v4}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    #@d
    iput-object v3, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mModestyPanel:Landroid/view/View;

    #@f
    .line 99
    invoke-virtual {p0}, Lcom/android/internal/widget/SizeAdaptiveLayout;->getBackground()Landroid/graphics/drawable/Drawable;

    #@12
    move-result-object v0

    #@13
    .line 100
    .local v0, background:Landroid/graphics/drawable/Drawable;
    instance-of v3, v0, Landroid/graphics/drawable/StateListDrawable;

    #@15
    if-eqz v3, :cond_23

    #@17
    move-object v2, v0

    #@18
    .line 101
    check-cast v2, Landroid/graphics/drawable/StateListDrawable;

    #@1a
    .line 102
    .local v2, sld:Landroid/graphics/drawable/StateListDrawable;
    sget-object v3, Landroid/util/StateSet;->WILD_CARD:[I

    #@1c
    invoke-virtual {v2, v3}, Landroid/graphics/drawable/StateListDrawable;->setState([I)Z

    #@1f
    .line 103
    invoke-virtual {v2}, Landroid/graphics/drawable/StateListDrawable;->getCurrent()Landroid/graphics/drawable/Drawable;

    #@22
    move-result-object v0

    #@23
    .line 105
    .end local v2           #sld:Landroid/graphics/drawable/StateListDrawable;
    :cond_23
    instance-of v3, v0, Landroid/graphics/drawable/ColorDrawable;

    #@25
    if-eqz v3, :cond_80

    #@27
    .line 106
    iget-object v3, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mModestyPanel:Landroid/view/View;

    #@29
    invoke-virtual {v3, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    #@2c
    .line 110
    :goto_2c
    new-instance v1, Lcom/android/internal/widget/SizeAdaptiveLayout$LayoutParams;

    #@2e
    invoke-direct {v1, v5, v5}, Lcom/android/internal/widget/SizeAdaptiveLayout$LayoutParams;-><init>(II)V

    #@31
    .line 113
    .local v1, layout:Lcom/android/internal/widget/SizeAdaptiveLayout$LayoutParams;
    iget-object v3, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mModestyPanel:Landroid/view/View;

    #@33
    invoke-virtual {v3, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@36
    .line 114
    iget-object v3, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mModestyPanel:Landroid/view/View;

    #@38
    invoke-virtual {p0, v3}, Lcom/android/internal/widget/SizeAdaptiveLayout;->addView(Landroid/view/View;)V

    #@3b
    .line 115
    iget-object v3, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mModestyPanel:Landroid/view/View;

    #@3d
    const-string v4, "alpha"

    #@3f
    new-array v5, v8, [F

    #@41
    aput v6, v5, v7

    #@43
    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    #@46
    move-result-object v3

    #@47
    iput-object v3, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mFadePanel:Landroid/animation/ObjectAnimator;

    #@49
    .line 116
    const/4 v3, 0x0

    #@4a
    const-string v4, "alpha"

    #@4c
    new-array v5, v8, [F

    #@4e
    aput v6, v5, v7

    #@50
    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    #@53
    move-result-object v3

    #@54
    iput-object v3, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mFadeView:Landroid/animation/ObjectAnimator;

    #@56
    .line 117
    new-instance v3, Lcom/android/internal/widget/SizeAdaptiveLayout$BringToFrontOnEnd;

    #@58
    invoke-direct {v3, p0}, Lcom/android/internal/widget/SizeAdaptiveLayout$BringToFrontOnEnd;-><init>(Lcom/android/internal/widget/SizeAdaptiveLayout;)V

    #@5b
    iput-object v3, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mAnimatorListener:Landroid/animation/Animator$AnimatorListener;

    #@5d
    .line 118
    new-instance v3, Landroid/animation/AnimatorSet;

    #@5f
    invoke-direct {v3}, Landroid/animation/AnimatorSet;-><init>()V

    #@62
    iput-object v3, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mTransitionAnimation:Landroid/animation/AnimatorSet;

    #@64
    .line 119
    iget-object v3, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mTransitionAnimation:Landroid/animation/AnimatorSet;

    #@66
    iget-object v4, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mFadeView:Landroid/animation/ObjectAnimator;

    #@68
    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    #@6b
    move-result-object v3

    #@6c
    iget-object v4, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mFadePanel:Landroid/animation/ObjectAnimator;

    #@6e
    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    #@71
    .line 120
    iget-object v3, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mTransitionAnimation:Landroid/animation/AnimatorSet;

    #@73
    const-wide/16 v4, 0xfa

    #@75
    invoke-virtual {v3, v4, v5}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    #@78
    .line 121
    iget-object v3, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mTransitionAnimation:Landroid/animation/AnimatorSet;

    #@7a
    iget-object v4, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mAnimatorListener:Landroid/animation/Animator$AnimatorListener;

    #@7c
    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    #@7f
    .line 122
    return-void

    #@80
    .line 108
    .end local v1           #layout:Lcom/android/internal/widget/SizeAdaptiveLayout$LayoutParams;
    :cond_80
    iget-object v3, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mModestyPanel:Landroid/view/View;

    #@82
    const/high16 v4, -0x100

    #@84
    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundColor(I)V

    #@87
    goto :goto_2c
.end method

.method private selectActiveChild(I)Landroid/view/View;
    .registers 14
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    .line 190
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@3
    move-result v1

    #@4
    .line 191
    .local v1, heightMode:I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@7
    move-result v2

    #@8
    .line 193
    .local v2, heightSize:I
    const/4 v9, 0x0

    #@9
    .line 194
    .local v9, unboundedView:Landroid/view/View;
    const/4 v7, 0x0

    #@a
    .line 195
    .local v7, tallestView:Landroid/view/View;
    const/4 v8, 0x0

    #@b
    .line 196
    .local v8, tallestViewSize:I
    const/4 v5, 0x0

    #@c
    .line 197
    .local v5, smallestView:Landroid/view/View;
    const v6, 0x7fffffff

    #@f
    .line 198
    .local v6, smallestViewSize:I
    const/4 v3, 0x0

    #@10
    .local v3, i:I
    :goto_10
    invoke-virtual {p0}, Lcom/android/internal/widget/SizeAdaptiveLayout;->getChildCount()I

    #@13
    move-result v10

    #@14
    if-ge v3, v10, :cond_48

    #@16
    .line 199
    invoke-virtual {p0, v3}, Lcom/android/internal/widget/SizeAdaptiveLayout;->getChildAt(I)Landroid/view/View;

    #@19
    move-result-object v0

    #@1a
    .line 200
    .local v0, child:Landroid/view/View;
    iget-object v10, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mModestyPanel:Landroid/view/View;

    #@1c
    if-eq v0, v10, :cond_45

    #@1e
    .line 201
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@21
    move-result-object v4

    #@22
    check-cast v4, Lcom/android/internal/widget/SizeAdaptiveLayout$LayoutParams;

    #@24
    .line 206
    .local v4, lp:Lcom/android/internal/widget/SizeAdaptiveLayout$LayoutParams;
    iget v10, v4, Lcom/android/internal/widget/SizeAdaptiveLayout$LayoutParams;->maxHeight:I

    #@26
    const/4 v11, -0x1

    #@27
    if-ne v10, v11, :cond_2c

    #@29
    if-nez v9, :cond_2c

    #@2b
    .line 208
    move-object v9, v0

    #@2c
    .line 210
    :cond_2c
    iget v10, v4, Lcom/android/internal/widget/SizeAdaptiveLayout$LayoutParams;->maxHeight:I

    #@2e
    if-le v10, v8, :cond_33

    #@30
    .line 211
    iget v8, v4, Lcom/android/internal/widget/SizeAdaptiveLayout$LayoutParams;->maxHeight:I

    #@32
    .line 212
    move-object v7, v0

    #@33
    .line 214
    :cond_33
    iget v10, v4, Lcom/android/internal/widget/SizeAdaptiveLayout$LayoutParams;->minHeight:I

    #@35
    if-ge v10, v6, :cond_3a

    #@37
    .line 215
    iget v6, v4, Lcom/android/internal/widget/SizeAdaptiveLayout$LayoutParams;->minHeight:I

    #@39
    .line 216
    move-object v5, v0

    #@3a
    .line 218
    :cond_3a
    if-eqz v1, :cond_45

    #@3c
    iget v10, v4, Lcom/android/internal/widget/SizeAdaptiveLayout$LayoutParams;->minHeight:I

    #@3e
    if-lt v2, v10, :cond_45

    #@40
    iget v10, v4, Lcom/android/internal/widget/SizeAdaptiveLayout$LayoutParams;->maxHeight:I

    #@42
    if-gt v2, v10, :cond_45

    #@44
    .line 234
    .end local v0           #child:Landroid/view/View;
    .end local v4           #lp:Lcom/android/internal/widget/SizeAdaptiveLayout$LayoutParams;
    :goto_44
    return-object v0

    #@45
    .line 198
    .restart local v0       #child:Landroid/view/View;
    :cond_45
    add-int/lit8 v3, v3, 0x1

    #@47
    goto :goto_10

    #@48
    .line 225
    .end local v0           #child:Landroid/view/View;
    :cond_48
    if-eqz v9, :cond_4b

    #@4a
    .line 226
    move-object v7, v9

    #@4b
    .line 228
    :cond_4b
    if-nez v1, :cond_4f

    #@4d
    move-object v0, v7

    #@4e
    .line 229
    goto :goto_44

    #@4f
    .line 231
    :cond_4f
    if-le v2, v8, :cond_53

    #@51
    move-object v0, v7

    #@52
    .line 232
    goto :goto_44

    #@53
    :cond_53
    move-object v0, v5

    #@54
    .line 234
    goto :goto_44
.end method


# virtual methods
.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .registers 3
    .parameter "p"

    #@0
    .prologue
    .line 301
    instance-of v0, p1, Lcom/android/internal/widget/SizeAdaptiveLayout$LayoutParams;

    #@2
    return v0
.end method

.method protected bridge synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .registers 2

    #@0
    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/android/internal/widget/SizeAdaptiveLayout;->generateDefaultLayoutParams()Lcom/android/internal/widget/SizeAdaptiveLayout$LayoutParams;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method protected generateDefaultLayoutParams()Lcom/android/internal/widget/SizeAdaptiveLayout$LayoutParams;
    .registers 2

    #@0
    .prologue
    .line 296
    new-instance v0, Lcom/android/internal/widget/SizeAdaptiveLayout$LayoutParams;

    #@2
    invoke-direct {v0}, Lcom/android/internal/widget/SizeAdaptiveLayout$LayoutParams;-><init>()V

    #@5
    return-object v0
.end method

.method public bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 51
    invoke-virtual {p0, p1}, Lcom/android/internal/widget/SizeAdaptiveLayout;->generateLayoutParams(Landroid/util/AttributeSet;)Lcom/android/internal/widget/SizeAdaptiveLayout$LayoutParams;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method protected bridge synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 51
    invoke-virtual {p0, p1}, Lcom/android/internal/widget/SizeAdaptiveLayout;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Lcom/android/internal/widget/SizeAdaptiveLayout$LayoutParams;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Lcom/android/internal/widget/SizeAdaptiveLayout$LayoutParams;
    .registers 4
    .parameter "attrs"

    #@0
    .prologue
    .line 284
    new-instance v0, Lcom/android/internal/widget/SizeAdaptiveLayout$LayoutParams;

    #@2
    invoke-virtual {p0}, Lcom/android/internal/widget/SizeAdaptiveLayout;->getContext()Landroid/content/Context;

    #@5
    move-result-object v1

    #@6
    invoke-direct {v0, v1, p1}, Lcom/android/internal/widget/SizeAdaptiveLayout$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@9
    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Lcom/android/internal/widget/SizeAdaptiveLayout$LayoutParams;
    .registers 3
    .parameter "p"

    #@0
    .prologue
    .line 290
    new-instance v0, Lcom/android/internal/widget/SizeAdaptiveLayout$LayoutParams;

    #@2
    invoke-direct {v0, p1}, Lcom/android/internal/widget/SizeAdaptiveLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    #@5
    return-object v0
.end method

.method public getModestyPanel()Landroid/view/View;
    .registers 2

    #@0
    .prologue
    .line 137
    iget-object v0, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mModestyPanel:Landroid/view/View;

    #@2
    return-object v0
.end method

.method public getTransitionAnimation()Landroid/animation/Animator;
    .registers 2

    #@0
    .prologue
    .line 129
    iget-object v0, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mTransitionAnimation:Landroid/animation/AnimatorSet;

    #@2
    return-object v0
.end method

.method public onAttachedToWindow()V
    .registers 4

    #@0
    .prologue
    .line 142
    const/4 v1, 0x0

    #@1
    iput-object v1, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mLastActive:Landroid/view/View;

    #@3
    .line 144
    const/4 v0, 0x0

    #@4
    .local v0, i:I
    :goto_4
    invoke-virtual {p0}, Lcom/android/internal/widget/SizeAdaptiveLayout;->getChildCount()I

    #@7
    move-result v1

    #@8
    if-ge v0, v1, :cond_16

    #@a
    .line 145
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/SizeAdaptiveLayout;->getChildAt(I)Landroid/view/View;

    #@d
    move-result-object v1

    #@e
    const/16 v2, 0x8

    #@10
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    #@13
    .line 144
    add-int/lit8 v0, v0, 0x1

    #@15
    goto :goto_4

    #@16
    .line 147
    :cond_16
    return-void
.end method

.method protected onLayout(ZIIII)V
    .registers 15
    .parameter "changed"
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    const/high16 v6, 0x3f80

    #@3
    const/4 v5, 0x0

    #@4
    const/4 v7, 0x0

    #@5
    .line 240
    iget-object v3, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mActiveChild:Landroid/view/View;

    #@7
    iput-object v3, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mLastActive:Landroid/view/View;

    #@9
    .line 241
    sub-int v3, p5, p3

    #@b
    const/high16 v4, 0x4000

    #@d
    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@10
    move-result v2

    #@11
    .line 243
    .local v2, measureSpec:I
    invoke-direct {p0, v2}, Lcom/android/internal/widget/SizeAdaptiveLayout;->selectActiveChild(I)Landroid/view/View;

    #@14
    move-result-object v3

    #@15
    iput-object v3, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mActiveChild:Landroid/view/View;

    #@17
    .line 244
    iget-object v3, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mActiveChild:Landroid/view/View;

    #@19
    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    #@1c
    .line 246
    iget-object v3, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mLastActive:Landroid/view/View;

    #@1e
    iget-object v4, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mActiveChild:Landroid/view/View;

    #@20
    if-eq v3, v4, :cond_7f

    #@22
    iget-object v3, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mLastActive:Landroid/view/View;

    #@24
    if-eqz v3, :cond_7f

    #@26
    .line 250
    iget-object v3, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mActiveChild:Landroid/view/View;

    #@28
    iput-object v3, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mEnteringView:Landroid/view/View;

    #@2a
    .line 251
    iget-object v3, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mLastActive:Landroid/view/View;

    #@2c
    iput-object v3, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mLeavingView:Landroid/view/View;

    #@2e
    .line 253
    iget-object v3, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mEnteringView:Landroid/view/View;

    #@30
    invoke-virtual {v3, v6}, Landroid/view/View;->setAlpha(F)V

    #@33
    .line 255
    iget-object v3, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mModestyPanel:Landroid/view/View;

    #@35
    invoke-virtual {v3, v6}, Landroid/view/View;->setAlpha(F)V

    #@38
    .line 256
    iget-object v3, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mModestyPanel:Landroid/view/View;

    #@3a
    invoke-virtual {v3}, Landroid/view/View;->bringToFront()V

    #@3d
    .line 257
    iget-object v3, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mLeavingView:Landroid/view/View;

    #@3f
    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    #@42
    move-result v3

    #@43
    iput v3, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mModestyPanelTop:I

    #@45
    .line 258
    iget-object v3, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mModestyPanel:Landroid/view/View;

    #@47
    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    #@4a
    .line 261
    iget-object v3, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mLeavingView:Landroid/view/View;

    #@4c
    invoke-virtual {v3}, Landroid/view/View;->bringToFront()V

    #@4f
    .line 263
    iget-object v3, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mTransitionAnimation:Landroid/animation/AnimatorSet;

    #@51
    invoke-virtual {v3}, Landroid/animation/AnimatorSet;->isRunning()Z

    #@54
    move-result v3

    #@55
    if-eqz v3, :cond_5c

    #@57
    .line 264
    iget-object v3, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mTransitionAnimation:Landroid/animation/AnimatorSet;

    #@59
    invoke-virtual {v3}, Landroid/animation/AnimatorSet;->cancel()V

    #@5c
    .line 266
    :cond_5c
    iget-object v3, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mFadeView:Landroid/animation/ObjectAnimator;

    #@5e
    iget-object v4, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mLeavingView:Landroid/view/View;

    #@60
    invoke-virtual {v3, v4}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    #@63
    .line 267
    iget-object v3, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mFadeView:Landroid/animation/ObjectAnimator;

    #@65
    new-array v4, v8, [F

    #@67
    aput v5, v4, v7

    #@69
    invoke-virtual {v3, v4}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    #@6c
    .line 268
    iget-object v3, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mFadePanel:Landroid/animation/ObjectAnimator;

    #@6e
    new-array v4, v8, [F

    #@70
    aput v5, v4, v7

    #@72
    invoke-virtual {v3, v4}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    #@75
    .line 269
    iget-object v3, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mTransitionAnimation:Landroid/animation/AnimatorSet;

    #@77
    invoke-virtual {v3}, Landroid/animation/AnimatorSet;->setupStartValues()V

    #@7a
    .line 270
    iget-object v3, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mTransitionAnimation:Landroid/animation/AnimatorSet;

    #@7c
    invoke-virtual {v3}, Landroid/animation/AnimatorSet;->start()V

    #@7f
    .line 272
    :cond_7f
    iget-object v3, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mActiveChild:Landroid/view/View;

    #@81
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    #@84
    move-result v1

    #@85
    .line 273
    .local v1, childWidth:I
    iget-object v3, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mActiveChild:Landroid/view/View;

    #@87
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    #@8a
    move-result v0

    #@8b
    .line 275
    .local v0, childHeight:I
    iget-object v3, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mActiveChild:Landroid/view/View;

    #@8d
    add-int/lit8 v4, v1, 0x0

    #@8f
    add-int/lit8 v5, v0, 0x0

    #@91
    invoke-virtual {v3, v7, v7, v4, v5}, Landroid/view/View;->layout(IIII)V

    #@94
    .line 278
    iget-object v3, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mModestyPanel:Landroid/view/View;

    #@96
    iget v4, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mModestyPanelTop:I

    #@98
    add-int/lit8 v5, v1, 0x0

    #@9a
    iget v6, p0, Lcom/android/internal/widget/SizeAdaptiveLayout;->mModestyPanelTop:I

    #@9c
    add-int/2addr v6, v0

    #@9d
    invoke-virtual {v3, v7, v4, v5, v6}, Landroid/view/View;->layout(IIII)V

    #@a0
    .line 279
    return-void
.end method

.method protected onMeasure(II)V
    .registers 13
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    .line 153
    invoke-direct {p0, p2}, Lcom/android/internal/widget/SizeAdaptiveLayout;->selectActiveChild(I)Landroid/view/View;

    #@3
    move-result-object v5

    #@4
    .line 154
    .local v5, model:Landroid/view/View;
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@7
    move-result-object v4

    #@8
    check-cast v4, Lcom/android/internal/widget/SizeAdaptiveLayout$LayoutParams;

    #@a
    .line 157
    .local v4, lp:Lcom/android/internal/widget/SizeAdaptiveLayout$LayoutParams;
    invoke-virtual {p0, v5, p1, p2}, Lcom/android/internal/widget/SizeAdaptiveLayout;->measureChild(Landroid/view/View;II)V

    #@d
    .line 158
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    #@10
    move-result v1

    #@11
    .line 159
    .local v1, childHeight:I
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    #@14
    move-result v3

    #@15
    .line 160
    .local v3, childWidth:I
    const/4 v8, 0x0

    #@16
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredState()I

    #@19
    move-result v9

    #@1a
    invoke-static {v8, v9}, Lcom/android/internal/widget/SizeAdaptiveLayout;->combineMeasuredStates(II)I

    #@1d
    move-result v2

    #@1e
    .line 162
    .local v2, childState:I
    invoke-static {v3, p1, v2}, Lcom/android/internal/widget/SizeAdaptiveLayout;->resolveSizeAndState(III)I

    #@21
    move-result v7

    #@22
    .line 163
    .local v7, resolvedWidth:I
    invoke-static {v1, p2, v2}, Lcom/android/internal/widget/SizeAdaptiveLayout;->resolveSizeAndState(III)I

    #@25
    move-result v6

    #@26
    .line 165
    .local v6, resolvedHeight:I
    invoke-direct {p0, v6, v5}, Lcom/android/internal/widget/SizeAdaptiveLayout;->clampSizeToBounds(ILandroid/view/View;)I

    #@29
    move-result v0

    #@2a
    .line 167
    .local v0, boundedHeight:I
    invoke-virtual {p0, v7, v0}, Lcom/android/internal/widget/SizeAdaptiveLayout;->setMeasuredDimension(II)V

    #@2d
    .line 168
    return-void
.end method
