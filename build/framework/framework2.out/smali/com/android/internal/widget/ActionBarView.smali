.class public Lcom/android/internal/widget/ActionBarView;
.super Lcom/android/internal/widget/AbsActionBarView;
.source "ActionBarView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/widget/ActionBarView$ExpandedActionViewMenuPresenter;,
        Lcom/android/internal/widget/ActionBarView$HomeView;,
        Lcom/android/internal/widget/ActionBarView$SavedState;
    }
.end annotation


# static fields
.field private static final DEFAULT_CUSTOM_GRAVITY:I = 0x800013

.field public static final DISPLAY_DEFAULT:I = 0x0

.field private static final DISPLAY_RELAYOUT_MASK:I = 0x3f

.field private static final TAG:Ljava/lang/String; = "ActionBarView"


# instance fields
.field private mCallback:Landroid/app/ActionBar$OnNavigationListener;

.field private mContextView:Lcom/android/internal/widget/ActionBarContextView;

.field private mCustomNavView:Landroid/view/View;

.field private mDisplayOptions:I

.field mExpandedActionView:Landroid/view/View;

.field private final mExpandedActionViewUpListener:Landroid/view/View$OnClickListener;

.field private mExpandedHomeLayout:Lcom/android/internal/widget/ActionBarView$HomeView;

.field private mExpandedMenuPresenter:Lcom/android/internal/widget/ActionBarView$ExpandedActionViewMenuPresenter;

.field private mHomeLayout:Lcom/android/internal/widget/ActionBarView$HomeView;

.field private mIcon:Landroid/graphics/drawable/Drawable;

.field private mIncludeTabs:Z

.field private mIndeterminateProgressStyle:I

.field private mIndeterminateProgressView:Landroid/widget/ProgressBar;

.field private mIsCollapsable:Z

.field private mIsCollapsed:Z

.field private mItemPadding:I

.field private mListNavLayout:Landroid/widget/LinearLayout;

.field private mLogo:Landroid/graphics/drawable/Drawable;

.field private mLogoNavItem:Lcom/android/internal/view/menu/ActionMenuItem;

.field private final mNavItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private mNavigationMode:I

.field private mOptionsMenu:Lcom/android/internal/view/menu/MenuBuilder;

.field private mProgressBarPadding:I

.field private mProgressStyle:I

.field private mProgressView:Landroid/widget/ProgressBar;

.field private mSpinner:Landroid/widget/Spinner;

.field private mSpinnerAdapter:Landroid/widget/SpinnerAdapter;

.field private mSubtitle:Ljava/lang/CharSequence;

.field private mSubtitleStyleRes:I

.field private mSubtitleView:Landroid/widget/TextView;

.field private mTabScrollView:Lcom/android/internal/widget/ScrollingTabContainerView;

.field private mTabSelector:Ljava/lang/Runnable;

.field private mTitle:Ljava/lang/CharSequence;

.field private mTitleLayout:Landroid/widget/LinearLayout;

.field private mTitleStyleRes:I

.field private mTitleUpView:Landroid/view/View;

.field private mTitleView:Landroid/widget/TextView;

.field private final mUpClickListener:Landroid/view/View$OnClickListener;

.field private mUpGoerFive:Landroid/view/ViewGroup;

.field private mUserTitle:Z

.field private mWasHomeEnabled:Z

.field mWindowCallback:Landroid/view/Window$Callback;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 20
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 172
    invoke-direct/range {p0 .. p2}, Lcom/android/internal/widget/AbsActionBarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@3
    .line 92
    const/4 v2, -0x1

    #@4
    move-object/from16 v0, p0

    #@6
    iput v2, v0, Lcom/android/internal/widget/ActionBarView;->mDisplayOptions:I

    #@8
    .line 143
    new-instance v2, Lcom/android/internal/widget/ActionBarView$1;

    #@a
    move-object/from16 v0, p0

    #@c
    invoke-direct {v2, v0}, Lcom/android/internal/widget/ActionBarView$1;-><init>(Lcom/android/internal/widget/ActionBarView;)V

    #@f
    move-object/from16 v0, p0

    #@11
    iput-object v2, v0, Lcom/android/internal/widget/ActionBarView;->mNavItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    #@13
    .line 155
    new-instance v2, Lcom/android/internal/widget/ActionBarView$2;

    #@15
    move-object/from16 v0, p0

    #@17
    invoke-direct {v2, v0}, Lcom/android/internal/widget/ActionBarView$2;-><init>(Lcom/android/internal/widget/ActionBarView;)V

    #@1a
    move-object/from16 v0, p0

    #@1c
    iput-object v2, v0, Lcom/android/internal/widget/ActionBarView;->mExpandedActionViewUpListener:Landroid/view/View$OnClickListener;

    #@1e
    .line 165
    new-instance v2, Lcom/android/internal/widget/ActionBarView$3;

    #@20
    move-object/from16 v0, p0

    #@22
    invoke-direct {v2, v0}, Lcom/android/internal/widget/ActionBarView$3;-><init>(Lcom/android/internal/widget/ActionBarView;)V

    #@25
    move-object/from16 v0, p0

    #@27
    iput-object v2, v0, Lcom/android/internal/widget/ActionBarView;->mUpClickListener:Landroid/view/View$OnClickListener;

    #@29
    .line 175
    const/4 v2, 0x0

    #@2a
    move-object/from16 v0, p0

    #@2c
    invoke-virtual {v0, v2}, Lcom/android/internal/widget/ActionBarView;->setBackgroundResource(I)V

    #@2f
    .line 177
    sget-object v2, Lcom/android/internal/R$styleable;->ActionBar:[I

    #@31
    const v3, 0x10102ce

    #@34
    const/4 v4, 0x0

    #@35
    move-object/from16 v0, p1

    #@37
    move-object/from16 v1, p2

    #@39
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@3c
    move-result-object v9

    #@3d
    .line 180
    .local v9, a:Landroid/content/res/TypedArray;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    #@40
    move-result-object v10

    #@41
    .line 181
    .local v10, appInfo:Landroid/content/pm/ApplicationInfo;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@44
    move-result-object v15

    #@45
    .line 182
    .local v15, pm:Landroid/content/pm/PackageManager;
    const/4 v2, 0x7

    #@46
    const/4 v3, 0x0

    #@47
    invoke-virtual {v9, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    #@4a
    move-result v2

    #@4b
    move-object/from16 v0, p0

    #@4d
    iput v2, v0, Lcom/android/internal/widget/ActionBarView;->mNavigationMode:I

    #@4f
    .line 184
    const/4 v2, 0x5

    #@50
    invoke-virtual {v9, v2}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    #@53
    move-result-object v2

    #@54
    move-object/from16 v0, p0

    #@56
    iput-object v2, v0, Lcom/android/internal/widget/ActionBarView;->mTitle:Ljava/lang/CharSequence;

    #@58
    .line 185
    const/16 v2, 0x9

    #@5a
    invoke-virtual {v9, v2}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    #@5d
    move-result-object v2

    #@5e
    move-object/from16 v0, p0

    #@60
    iput-object v2, v0, Lcom/android/internal/widget/ActionBarView;->mSubtitle:Ljava/lang/CharSequence;

    #@62
    .line 187
    const/4 v2, 0x6

    #@63
    invoke-virtual {v9, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@66
    move-result-object v2

    #@67
    move-object/from16 v0, p0

    #@69
    iput-object v2, v0, Lcom/android/internal/widget/ActionBarView;->mLogo:Landroid/graphics/drawable/Drawable;

    #@6b
    .line 188
    move-object/from16 v0, p0

    #@6d
    iget-object v2, v0, Lcom/android/internal/widget/ActionBarView;->mLogo:Landroid/graphics/drawable/Drawable;

    #@6f
    if-nez v2, :cond_96

    #@71
    .line 189
    move-object/from16 v0, p1

    #@73
    instance-of v2, v0, Landroid/app/Activity;

    #@75
    if-eqz v2, :cond_88

    #@77
    .line 191
    :try_start_77
    move-object/from16 v0, p1

    #@79
    check-cast v0, Landroid/app/Activity;

    #@7b
    move-object v2, v0

    #@7c
    invoke-virtual {v2}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    #@7f
    move-result-object v2

    #@80
    invoke-virtual {v15, v2}, Landroid/content/pm/PackageManager;->getActivityLogo(Landroid/content/ComponentName;)Landroid/graphics/drawable/Drawable;

    #@83
    move-result-object v2

    #@84
    move-object/from16 v0, p0

    #@86
    iput-object v2, v0, Lcom/android/internal/widget/ActionBarView;->mLogo:Landroid/graphics/drawable/Drawable;
    :try_end_88
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_77 .. :try_end_88} :catch_20e

    #@88
    .line 196
    :cond_88
    :goto_88
    move-object/from16 v0, p0

    #@8a
    iget-object v2, v0, Lcom/android/internal/widget/ActionBarView;->mLogo:Landroid/graphics/drawable/Drawable;

    #@8c
    if-nez v2, :cond_96

    #@8e
    .line 197
    invoke-virtual {v10, v15}, Landroid/content/pm/ApplicationInfo;->loadLogo(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    #@91
    move-result-object v2

    #@92
    move-object/from16 v0, p0

    #@94
    iput-object v2, v0, Lcom/android/internal/widget/ActionBarView;->mLogo:Landroid/graphics/drawable/Drawable;

    #@96
    .line 201
    :cond_96
    const/4 v2, 0x0

    #@97
    invoke-virtual {v9, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@9a
    move-result-object v2

    #@9b
    move-object/from16 v0, p0

    #@9d
    iput-object v2, v0, Lcom/android/internal/widget/ActionBarView;->mIcon:Landroid/graphics/drawable/Drawable;

    #@9f
    .line 202
    move-object/from16 v0, p0

    #@a1
    iget-object v2, v0, Lcom/android/internal/widget/ActionBarView;->mIcon:Landroid/graphics/drawable/Drawable;

    #@a3
    if-nez v2, :cond_ca

    #@a5
    .line 203
    move-object/from16 v0, p1

    #@a7
    instance-of v2, v0, Landroid/app/Activity;

    #@a9
    if-eqz v2, :cond_bc

    #@ab
    .line 205
    :try_start_ab
    move-object/from16 v0, p1

    #@ad
    check-cast v0, Landroid/app/Activity;

    #@af
    move-object v2, v0

    #@b0
    invoke-virtual {v2}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    #@b3
    move-result-object v2

    #@b4
    invoke-virtual {v15, v2}, Landroid/content/pm/PackageManager;->getActivityIcon(Landroid/content/ComponentName;)Landroid/graphics/drawable/Drawable;

    #@b7
    move-result-object v2

    #@b8
    move-object/from16 v0, p0

    #@ba
    iput-object v2, v0, Lcom/android/internal/widget/ActionBarView;->mIcon:Landroid/graphics/drawable/Drawable;
    :try_end_bc
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_ab .. :try_end_bc} :catch_218

    #@bc
    .line 210
    :cond_bc
    :goto_bc
    move-object/from16 v0, p0

    #@be
    iget-object v2, v0, Lcom/android/internal/widget/ActionBarView;->mIcon:Landroid/graphics/drawable/Drawable;

    #@c0
    if-nez v2, :cond_ca

    #@c2
    .line 211
    invoke-virtual {v10, v15}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    #@c5
    move-result-object v2

    #@c6
    move-object/from16 v0, p0

    #@c8
    iput-object v2, v0, Lcom/android/internal/widget/ActionBarView;->mIcon:Landroid/graphics/drawable/Drawable;

    #@ca
    .line 215
    :cond_ca
    invoke-static/range {p1 .. p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    #@cd
    move-result-object v14

    #@ce
    .line 217
    .local v14, inflater:Landroid/view/LayoutInflater;
    const/16 v2, 0xf

    #@d0
    const v3, 0x1090018

    #@d3
    invoke-virtual {v9, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@d6
    move-result v13

    #@d7
    .line 221
    .local v13, homeResId:I
    const v2, 0x109001a

    #@da
    const/4 v3, 0x0

    #@db
    move-object/from16 v0, p0

    #@dd
    invoke-virtual {v14, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@e0
    move-result-object v2

    #@e1
    check-cast v2, Landroid/view/ViewGroup;

    #@e3
    move-object/from16 v0, p0

    #@e5
    iput-object v2, v0, Lcom/android/internal/widget/ActionBarView;->mUpGoerFive:Landroid/view/ViewGroup;

    #@e7
    .line 223
    move-object/from16 v0, p0

    #@e9
    iget-object v2, v0, Lcom/android/internal/widget/ActionBarView;->mUpGoerFive:Landroid/view/ViewGroup;

    #@eb
    const/4 v3, 0x0

    #@ec
    invoke-virtual {v14, v13, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@ef
    move-result-object v2

    #@f0
    check-cast v2, Lcom/android/internal/widget/ActionBarView$HomeView;

    #@f2
    move-object/from16 v0, p0

    #@f4
    iput-object v2, v0, Lcom/android/internal/widget/ActionBarView;->mHomeLayout:Lcom/android/internal/widget/ActionBarView$HomeView;

    #@f6
    .line 225
    move-object/from16 v0, p0

    #@f8
    iget-object v2, v0, Lcom/android/internal/widget/ActionBarView;->mUpGoerFive:Landroid/view/ViewGroup;

    #@fa
    const/4 v3, 0x0

    #@fb
    invoke-virtual {v14, v13, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@fe
    move-result-object v2

    #@ff
    check-cast v2, Lcom/android/internal/widget/ActionBarView$HomeView;

    #@101
    move-object/from16 v0, p0

    #@103
    iput-object v2, v0, Lcom/android/internal/widget/ActionBarView;->mExpandedHomeLayout:Lcom/android/internal/widget/ActionBarView$HomeView;

    #@105
    .line 226
    move-object/from16 v0, p0

    #@107
    iget-object v2, v0, Lcom/android/internal/widget/ActionBarView;->mExpandedHomeLayout:Lcom/android/internal/widget/ActionBarView$HomeView;

    #@109
    const/4 v3, 0x1

    #@10a
    invoke-virtual {v2, v3}, Lcom/android/internal/widget/ActionBarView$HomeView;->setUp(Z)V

    #@10d
    .line 227
    move-object/from16 v0, p0

    #@10f
    iget-object v2, v0, Lcom/android/internal/widget/ActionBarView;->mExpandedHomeLayout:Lcom/android/internal/widget/ActionBarView$HomeView;

    #@111
    move-object/from16 v0, p0

    #@113
    iget-object v3, v0, Lcom/android/internal/widget/ActionBarView;->mExpandedActionViewUpListener:Landroid/view/View$OnClickListener;

    #@115
    invoke-virtual {v2, v3}, Lcom/android/internal/widget/ActionBarView$HomeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@118
    .line 228
    move-object/from16 v0, p0

    #@11a
    iget-object v2, v0, Lcom/android/internal/widget/ActionBarView;->mExpandedHomeLayout:Lcom/android/internal/widget/ActionBarView$HomeView;

    #@11c
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/widget/ActionBarView;->getResources()Landroid/content/res/Resources;

    #@11f
    move-result-object v3

    #@120
    const v4, 0x104050b

    #@123
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@126
    move-result-object v3

    #@127
    invoke-virtual {v2, v3}, Lcom/android/internal/widget/ActionBarView$HomeView;->setContentDescription(Ljava/lang/CharSequence;)V

    #@12a
    .line 233
    move-object/from16 v0, p0

    #@12c
    iget-object v2, v0, Lcom/android/internal/widget/ActionBarView;->mUpGoerFive:Landroid/view/ViewGroup;

    #@12e
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getBackground()Landroid/graphics/drawable/Drawable;

    #@131
    move-result-object v16

    #@132
    .line 234
    .local v16, upBackground:Landroid/graphics/drawable/Drawable;
    if-eqz v16, :cond_143

    #@134
    .line 235
    move-object/from16 v0, p0

    #@136
    iget-object v2, v0, Lcom/android/internal/widget/ActionBarView;->mExpandedHomeLayout:Lcom/android/internal/widget/ActionBarView$HomeView;

    #@138
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    #@13b
    move-result-object v3

    #@13c
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    #@13f
    move-result-object v3

    #@140
    invoke-virtual {v2, v3}, Lcom/android/internal/widget/ActionBarView$HomeView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    #@143
    .line 237
    :cond_143
    move-object/from16 v0, p0

    #@145
    iget-object v2, v0, Lcom/android/internal/widget/ActionBarView;->mExpandedHomeLayout:Lcom/android/internal/widget/ActionBarView$HomeView;

    #@147
    const/4 v3, 0x1

    #@148
    invoke-virtual {v2, v3}, Lcom/android/internal/widget/ActionBarView$HomeView;->setEnabled(Z)V

    #@14b
    .line 238
    move-object/from16 v0, p0

    #@14d
    iget-object v2, v0, Lcom/android/internal/widget/ActionBarView;->mExpandedHomeLayout:Lcom/android/internal/widget/ActionBarView$HomeView;

    #@14f
    const/4 v3, 0x1

    #@150
    invoke-virtual {v2, v3}, Lcom/android/internal/widget/ActionBarView$HomeView;->setFocusable(Z)V

    #@153
    .line 240
    const/16 v2, 0xb

    #@155
    const/4 v3, 0x0

    #@156
    invoke-virtual {v9, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@159
    move-result v2

    #@15a
    move-object/from16 v0, p0

    #@15c
    iput v2, v0, Lcom/android/internal/widget/ActionBarView;->mTitleStyleRes:I

    #@15e
    .line 241
    const/16 v2, 0xc

    #@160
    const/4 v3, 0x0

    #@161
    invoke-virtual {v9, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@164
    move-result v2

    #@165
    move-object/from16 v0, p0

    #@167
    iput v2, v0, Lcom/android/internal/widget/ActionBarView;->mSubtitleStyleRes:I

    #@169
    .line 242
    const/4 v2, 0x1

    #@16a
    const/4 v3, 0x0

    #@16b
    invoke-virtual {v9, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@16e
    move-result v2

    #@16f
    move-object/from16 v0, p0

    #@171
    iput v2, v0, Lcom/android/internal/widget/ActionBarView;->mProgressStyle:I

    #@173
    .line 243
    const/16 v2, 0xd

    #@175
    const/4 v3, 0x0

    #@176
    invoke-virtual {v9, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@179
    move-result v2

    #@17a
    move-object/from16 v0, p0

    #@17c
    iput v2, v0, Lcom/android/internal/widget/ActionBarView;->mIndeterminateProgressStyle:I

    #@17e
    .line 246
    const/16 v2, 0xe

    #@180
    const/4 v3, 0x0

    #@181
    invoke-virtual {v9, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    #@184
    move-result v2

    #@185
    move-object/from16 v0, p0

    #@187
    iput v2, v0, Lcom/android/internal/widget/ActionBarView;->mProgressBarPadding:I

    #@189
    .line 247
    const/16 v2, 0x10

    #@18b
    const/4 v3, 0x0

    #@18c
    invoke-virtual {v9, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    #@18f
    move-result v2

    #@190
    move-object/from16 v0, p0

    #@192
    iput v2, v0, Lcom/android/internal/widget/ActionBarView;->mItemPadding:I

    #@194
    .line 249
    const/16 v2, 0x8

    #@196
    const/4 v3, 0x0

    #@197
    invoke-virtual {v9, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    #@19a
    move-result v2

    #@19b
    move-object/from16 v0, p0

    #@19d
    invoke-virtual {v0, v2}, Lcom/android/internal/widget/ActionBarView;->setDisplayOptions(I)V

    #@1a0
    .line 251
    const/16 v2, 0xa

    #@1a2
    const/4 v3, 0x0

    #@1a3
    invoke-virtual {v9, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@1a6
    move-result v11

    #@1a7
    .line 252
    .local v11, customNavId:I
    if-eqz v11, :cond_1c4

    #@1a9
    .line 253
    const/4 v2, 0x0

    #@1aa
    move-object/from16 v0, p0

    #@1ac
    invoke-virtual {v14, v11, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@1af
    move-result-object v2

    #@1b0
    move-object/from16 v0, p0

    #@1b2
    iput-object v2, v0, Lcom/android/internal/widget/ActionBarView;->mCustomNavView:Landroid/view/View;

    #@1b4
    .line 254
    const/4 v2, 0x0

    #@1b5
    move-object/from16 v0, p0

    #@1b7
    iput v2, v0, Lcom/android/internal/widget/ActionBarView;->mNavigationMode:I

    #@1b9
    .line 255
    move-object/from16 v0, p0

    #@1bb
    iget v2, v0, Lcom/android/internal/widget/ActionBarView;->mDisplayOptions:I

    #@1bd
    or-int/lit8 v2, v2, 0x10

    #@1bf
    move-object/from16 v0, p0

    #@1c1
    invoke-virtual {v0, v2}, Lcom/android/internal/widget/ActionBarView;->setDisplayOptions(I)V

    #@1c4
    .line 258
    :cond_1c4
    const/4 v2, 0x4

    #@1c5
    const/4 v3, 0x0

    #@1c6
    invoke-virtual {v9, v2, v3}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    #@1c9
    move-result v2

    #@1ca
    move-object/from16 v0, p0

    #@1cc
    iput v2, v0, Lcom/android/internal/widget/AbsActionBarView;->mContentHeight:I

    #@1ce
    .line 260
    invoke-virtual {v9}, Landroid/content/res/TypedArray;->recycle()V

    #@1d1
    .line 262
    new-instance v2, Lcom/android/internal/view/menu/ActionMenuItem;

    #@1d3
    const/4 v4, 0x0

    #@1d4
    const v5, 0x102002c

    #@1d7
    const/4 v6, 0x0

    #@1d8
    const/4 v7, 0x0

    #@1d9
    move-object/from16 v0, p0

    #@1db
    iget-object v8, v0, Lcom/android/internal/widget/ActionBarView;->mTitle:Ljava/lang/CharSequence;

    #@1dd
    move-object/from16 v3, p1

    #@1df
    invoke-direct/range {v2 .. v8}, Lcom/android/internal/view/menu/ActionMenuItem;-><init>(Landroid/content/Context;IIIILjava/lang/CharSequence;)V

    #@1e2
    move-object/from16 v0, p0

    #@1e4
    iput-object v2, v0, Lcom/android/internal/widget/ActionBarView;->mLogoNavItem:Lcom/android/internal/view/menu/ActionMenuItem;

    #@1e6
    .line 264
    move-object/from16 v0, p0

    #@1e8
    iget-object v2, v0, Lcom/android/internal/widget/ActionBarView;->mUpGoerFive:Landroid/view/ViewGroup;

    #@1ea
    move-object/from16 v0, p0

    #@1ec
    iget-object v3, v0, Lcom/android/internal/widget/ActionBarView;->mUpClickListener:Landroid/view/View$OnClickListener;

    #@1ee
    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@1f1
    .line 265
    move-object/from16 v0, p0

    #@1f3
    iget-object v2, v0, Lcom/android/internal/widget/ActionBarView;->mUpGoerFive:Landroid/view/ViewGroup;

    #@1f5
    const/4 v3, 0x1

    #@1f6
    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->setClickable(Z)V

    #@1f9
    .line 266
    move-object/from16 v0, p0

    #@1fb
    iget-object v2, v0, Lcom/android/internal/widget/ActionBarView;->mUpGoerFive:Landroid/view/ViewGroup;

    #@1fd
    const/4 v3, 0x1

    #@1fe
    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->setFocusable(Z)V

    #@201
    .line 268
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/widget/ActionBarView;->getImportantForAccessibility()I

    #@204
    move-result v2

    #@205
    if-nez v2, :cond_20d

    #@207
    .line 269
    const/4 v2, 0x1

    #@208
    move-object/from16 v0, p0

    #@20a
    invoke-virtual {v0, v2}, Lcom/android/internal/widget/ActionBarView;->setImportantForAccessibility(I)V

    #@20d
    .line 271
    :cond_20d
    return-void

    #@20e
    .line 192
    .end local v11           #customNavId:I
    .end local v13           #homeResId:I
    .end local v14           #inflater:Landroid/view/LayoutInflater;
    .end local v16           #upBackground:Landroid/graphics/drawable/Drawable;
    :catch_20e
    move-exception v12

    #@20f
    .line 193
    .local v12, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v2, "ActionBarView"

    #@211
    const-string v3, "Activity component name not found!"

    #@213
    invoke-static {v2, v3, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@216
    goto/16 :goto_88

    #@218
    .line 206
    .end local v12           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_218
    move-exception v12

    #@219
    .line 207
    .restart local v12       #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v2, "ActionBarView"

    #@21b
    const-string v3, "Activity component name not found!"

    #@21d
    invoke-static {v2, v3, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@220
    goto/16 :goto_bc
.end method

.method static synthetic access$000(Lcom/android/internal/widget/ActionBarView;)Landroid/app/ActionBar$OnNavigationListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 70
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarView;->mCallback:Landroid/app/ActionBar$OnNavigationListener;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/internal/widget/ActionBarView;)Lcom/android/internal/widget/ActionBarView$ExpandedActionViewMenuPresenter;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 70
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarView;->mExpandedMenuPresenter:Lcom/android/internal/widget/ActionBarView$ExpandedActionViewMenuPresenter;

    #@2
    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/internal/widget/ActionBarView;)Lcom/android/internal/widget/ScrollingTabContainerView;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 70
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarView;->mTabScrollView:Lcom/android/internal/widget/ScrollingTabContainerView;

    #@2
    return-object v0
.end method

.method static synthetic access$1100(Lcom/android/internal/widget/ActionBarView;)Landroid/widget/Spinner;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 70
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarView;->mSpinner:Landroid/widget/Spinner;

    #@2
    return-object v0
.end method

.method static synthetic access$1200(Lcom/android/internal/widget/ActionBarView;)Landroid/view/View;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 70
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarView;->mCustomNavView:Landroid/view/View;

    #@2
    return-object v0
.end method

.method static synthetic access$1300(Lcom/android/internal/widget/ActionBarView;ZZ)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 70
    invoke-direct {p0, p1, p2}, Lcom/android/internal/widget/ActionBarView;->setHomeButtonEnabled(ZZ)V

    #@3
    return-void
.end method

.method static synthetic access$1400(Lcom/android/internal/widget/ActionBarView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 70
    iget v0, p0, Lcom/android/internal/widget/ActionBarView;->mDisplayOptions:I

    #@2
    return v0
.end method

.method static synthetic access$1500(Lcom/android/internal/widget/ActionBarView;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 70
    invoke-direct {p0}, Lcom/android/internal/widget/ActionBarView;->initTitle()V

    #@3
    return-void
.end method

.method static synthetic access$1600(Lcom/android/internal/widget/ActionBarView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 70
    iget v0, p0, Lcom/android/internal/widget/ActionBarView;->mNavigationMode:I

    #@2
    return v0
.end method

.method static synthetic access$1700(Lcom/android/internal/widget/ActionBarView;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/android/internal/widget/ActionBarView;->mWasHomeEnabled:Z

    #@2
    return v0
.end method

.method static synthetic access$200(Lcom/android/internal/widget/ActionBarView;)Lcom/android/internal/view/menu/ActionMenuItem;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 70
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarView;->mLogoNavItem:Lcom/android/internal/view/menu/ActionMenuItem;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Lcom/android/internal/widget/ActionBarView;)Landroid/graphics/drawable/Drawable;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 70
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarView;->mIcon:Landroid/graphics/drawable/Drawable;

    #@2
    return-object v0
.end method

.method static synthetic access$600(Lcom/android/internal/widget/ActionBarView;)Lcom/android/internal/widget/ActionBarView$HomeView;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 70
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarView;->mExpandedHomeLayout:Lcom/android/internal/widget/ActionBarView$HomeView;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Lcom/android/internal/widget/ActionBarView;)Landroid/view/ViewGroup;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 70
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarView;->mUpGoerFive:Landroid/view/ViewGroup;

    #@2
    return-object v0
.end method

.method static synthetic access$800(Lcom/android/internal/widget/ActionBarView;)Lcom/android/internal/widget/ActionBarView$HomeView;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 70
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarView;->mHomeLayout:Lcom/android/internal/widget/ActionBarView$HomeView;

    #@2
    return-object v0
.end method

.method static synthetic access$900(Lcom/android/internal/widget/ActionBarView;)Landroid/widget/LinearLayout;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 70
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarView;->mTitleLayout:Landroid/widget/LinearLayout;

    #@2
    return-object v0
.end method

.method private configPresenters(Lcom/android/internal/view/menu/MenuBuilder;)V
    .registers 6
    .parameter "builder"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    .line 469
    if-eqz p1, :cond_f

    #@4
    .line 470
    iget-object v0, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@6
    invoke-virtual {p1, v0}, Lcom/android/internal/view/menu/MenuBuilder;->addMenuPresenter(Lcom/android/internal/view/menu/MenuPresenter;)V

    #@9
    .line 471
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarView;->mExpandedMenuPresenter:Lcom/android/internal/widget/ActionBarView$ExpandedActionViewMenuPresenter;

    #@b
    invoke-virtual {p1, v0}, Lcom/android/internal/view/menu/MenuBuilder;->addMenuPresenter(Lcom/android/internal/view/menu/MenuPresenter;)V

    #@e
    .line 478
    :goto_e
    return-void

    #@f
    .line 473
    :cond_f
    iget-object v0, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@11
    iget-object v1, p0, Lcom/android/internal/widget/ActionBarView;->mContext:Landroid/content/Context;

    #@13
    invoke-virtual {v0, v1, v3}, Lcom/android/internal/view/menu/ActionMenuPresenter;->initForMenu(Landroid/content/Context;Lcom/android/internal/view/menu/MenuBuilder;)V

    #@16
    .line 474
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarView;->mExpandedMenuPresenter:Lcom/android/internal/widget/ActionBarView$ExpandedActionViewMenuPresenter;

    #@18
    iget-object v1, p0, Lcom/android/internal/widget/ActionBarView;->mContext:Landroid/content/Context;

    #@1a
    invoke-virtual {v0, v1, v3}, Lcom/android/internal/widget/ActionBarView$ExpandedActionViewMenuPresenter;->initForMenu(Landroid/content/Context;Lcom/android/internal/view/menu/MenuBuilder;)V

    #@1d
    .line 475
    iget-object v0, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@1f
    invoke-virtual {v0, v2}, Lcom/android/internal/view/menu/ActionMenuPresenter;->updateMenuView(Z)V

    #@22
    .line 476
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarView;->mExpandedMenuPresenter:Lcom/android/internal/widget/ActionBarView$ExpandedActionViewMenuPresenter;

    #@24
    invoke-virtual {v0, v2}, Lcom/android/internal/widget/ActionBarView$ExpandedActionViewMenuPresenter;->updateMenuView(Z)V

    #@27
    goto :goto_e
.end method

.method private initTitle()V
    .registers 11

    #@0
    .prologue
    const/16 v7, 0x8

    #@2
    const/4 v5, 0x1

    #@3
    const/4 v6, 0x0

    #@4
    .line 803
    iget-object v4, p0, Lcom/android/internal/widget/ActionBarView;->mTitleLayout:Landroid/widget/LinearLayout;

    #@6
    if-nez v4, :cond_8f

    #@8
    .line 804
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarView;->getContext()Landroid/content/Context;

    #@b
    move-result-object v4

    #@c
    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    #@f
    move-result-object v1

    #@10
    .line 805
    .local v1, inflater:Landroid/view/LayoutInflater;
    const v4, 0x1090019

    #@13
    invoke-virtual {v1, v4, p0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@16
    move-result-object v4

    #@17
    check-cast v4, Landroid/widget/LinearLayout;

    #@19
    iput-object v4, p0, Lcom/android/internal/widget/ActionBarView;->mTitleLayout:Landroid/widget/LinearLayout;

    #@1b
    .line 807
    iget-object v4, p0, Lcom/android/internal/widget/ActionBarView;->mTitleLayout:Landroid/widget/LinearLayout;

    #@1d
    const v8, 0x102025c

    #@20
    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    #@23
    move-result-object v4

    #@24
    check-cast v4, Landroid/widget/TextView;

    #@26
    iput-object v4, p0, Lcom/android/internal/widget/ActionBarView;->mTitleView:Landroid/widget/TextView;

    #@28
    .line 808
    iget-object v4, p0, Lcom/android/internal/widget/ActionBarView;->mTitleLayout:Landroid/widget/LinearLayout;

    #@2a
    const v8, 0x102025d

    #@2d
    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    #@30
    move-result-object v4

    #@31
    check-cast v4, Landroid/widget/TextView;

    #@33
    iput-object v4, p0, Lcom/android/internal/widget/ActionBarView;->mSubtitleView:Landroid/widget/TextView;

    #@35
    .line 809
    iget-object v4, p0, Lcom/android/internal/widget/ActionBarView;->mTitleLayout:Landroid/widget/LinearLayout;

    #@37
    const v8, 0x1020254

    #@3a
    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    #@3d
    move-result-object v4

    #@3e
    iput-object v4, p0, Lcom/android/internal/widget/ActionBarView;->mTitleUpView:Landroid/view/View;

    #@40
    .line 811
    iget v4, p0, Lcom/android/internal/widget/ActionBarView;->mTitleStyleRes:I

    #@42
    if-eqz v4, :cond_4d

    #@44
    .line 812
    iget-object v4, p0, Lcom/android/internal/widget/ActionBarView;->mTitleView:Landroid/widget/TextView;

    #@46
    iget-object v8, p0, Lcom/android/internal/widget/ActionBarView;->mContext:Landroid/content/Context;

    #@48
    iget v9, p0, Lcom/android/internal/widget/ActionBarView;->mTitleStyleRes:I

    #@4a
    invoke-virtual {v4, v8, v9}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    #@4d
    .line 814
    :cond_4d
    iget-object v4, p0, Lcom/android/internal/widget/ActionBarView;->mTitle:Ljava/lang/CharSequence;

    #@4f
    if-eqz v4, :cond_58

    #@51
    .line 815
    iget-object v4, p0, Lcom/android/internal/widget/ActionBarView;->mTitleView:Landroid/widget/TextView;

    #@53
    iget-object v8, p0, Lcom/android/internal/widget/ActionBarView;->mTitle:Ljava/lang/CharSequence;

    #@55
    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@58
    .line 818
    :cond_58
    iget v4, p0, Lcom/android/internal/widget/ActionBarView;->mSubtitleStyleRes:I

    #@5a
    if-eqz v4, :cond_65

    #@5c
    .line 819
    iget-object v4, p0, Lcom/android/internal/widget/ActionBarView;->mSubtitleView:Landroid/widget/TextView;

    #@5e
    iget-object v8, p0, Lcom/android/internal/widget/ActionBarView;->mContext:Landroid/content/Context;

    #@60
    iget v9, p0, Lcom/android/internal/widget/ActionBarView;->mSubtitleStyleRes:I

    #@62
    invoke-virtual {v4, v8, v9}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    #@65
    .line 821
    :cond_65
    iget-object v4, p0, Lcom/android/internal/widget/ActionBarView;->mSubtitle:Ljava/lang/CharSequence;

    #@67
    if-eqz v4, :cond_75

    #@69
    .line 822
    iget-object v4, p0, Lcom/android/internal/widget/ActionBarView;->mSubtitleView:Landroid/widget/TextView;

    #@6b
    iget-object v8, p0, Lcom/android/internal/widget/ActionBarView;->mSubtitle:Ljava/lang/CharSequence;

    #@6d
    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@70
    .line 823
    iget-object v4, p0, Lcom/android/internal/widget/ActionBarView;->mSubtitleView:Landroid/widget/TextView;

    #@72
    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setVisibility(I)V

    #@75
    .line 826
    :cond_75
    iget v4, p0, Lcom/android/internal/widget/ActionBarView;->mDisplayOptions:I

    #@77
    and-int/lit8 v4, v4, 0x4

    #@79
    if-eqz v4, :cond_b0

    #@7b
    move v0, v5

    #@7c
    .line 827
    .local v0, homeAsUp:Z
    :goto_7c
    iget v4, p0, Lcom/android/internal/widget/ActionBarView;->mDisplayOptions:I

    #@7e
    and-int/lit8 v4, v4, 0x2

    #@80
    if-eqz v4, :cond_b2

    #@82
    move v2, v5

    #@83
    .line 828
    .local v2, showHome:Z
    :goto_83
    if-nez v2, :cond_b4

    #@85
    move v3, v5

    #@86
    .line 829
    .local v3, showTitleUp:Z
    :goto_86
    iget-object v4, p0, Lcom/android/internal/widget/ActionBarView;->mTitleUpView:Landroid/view/View;

    #@88
    if-eqz v3, :cond_b8

    #@8a
    if-eqz v0, :cond_b6

    #@8c
    :goto_8c
    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    #@8f
    .line 832
    .end local v0           #homeAsUp:Z
    .end local v1           #inflater:Landroid/view/LayoutInflater;
    .end local v2           #showHome:Z
    .end local v3           #showTitleUp:Z
    :cond_8f
    iget-object v4, p0, Lcom/android/internal/widget/ActionBarView;->mUpGoerFive:Landroid/view/ViewGroup;

    #@91
    iget-object v5, p0, Lcom/android/internal/widget/ActionBarView;->mTitleLayout:Landroid/widget/LinearLayout;

    #@93
    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    #@96
    .line 833
    iget-object v4, p0, Lcom/android/internal/widget/ActionBarView;->mExpandedActionView:Landroid/view/View;

    #@98
    if-nez v4, :cond_aa

    #@9a
    iget-object v4, p0, Lcom/android/internal/widget/ActionBarView;->mTitle:Ljava/lang/CharSequence;

    #@9c
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@9f
    move-result v4

    #@a0
    if-eqz v4, :cond_af

    #@a2
    iget-object v4, p0, Lcom/android/internal/widget/ActionBarView;->mSubtitle:Ljava/lang/CharSequence;

    #@a4
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@a7
    move-result v4

    #@a8
    if-eqz v4, :cond_af

    #@aa
    .line 836
    :cond_aa
    iget-object v4, p0, Lcom/android/internal/widget/ActionBarView;->mTitleLayout:Landroid/widget/LinearLayout;

    #@ac
    invoke-virtual {v4, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    #@af
    .line 838
    :cond_af
    return-void

    #@b0
    .restart local v1       #inflater:Landroid/view/LayoutInflater;
    :cond_b0
    move v0, v6

    #@b1
    .line 826
    goto :goto_7c

    #@b2
    .restart local v0       #homeAsUp:Z
    :cond_b2
    move v2, v6

    #@b3
    .line 827
    goto :goto_83

    #@b4
    .restart local v2       #showHome:Z
    :cond_b4
    move v3, v6

    #@b5
    .line 828
    goto :goto_86

    #@b6
    .line 829
    .restart local v3       #showTitleUp:Z
    :cond_b6
    const/4 v6, 0x4

    #@b7
    goto :goto_8c

    #@b8
    :cond_b8
    move v6, v7

    #@b9
    goto :goto_8c
.end method

.method private setHomeButtonEnabled(ZZ)V
    .registers 6
    .parameter "enable"
    .parameter "recordState"

    #@0
    .prologue
    .line 566
    if-eqz p2, :cond_4

    #@2
    .line 567
    iput-boolean p1, p0, Lcom/android/internal/widget/ActionBarView;->mWasHomeEnabled:Z

    #@4
    .line 570
    :cond_4
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarView;->mExpandedActionView:Landroid/view/View;

    #@6
    if-eqz v0, :cond_9

    #@8
    .line 593
    :goto_8
    return-void

    #@9
    .line 577
    :cond_9
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarView;->mUpGoerFive:Landroid/view/ViewGroup;

    #@b
    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->setEnabled(Z)V

    #@e
    .line 578
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarView;->mUpGoerFive:Landroid/view/ViewGroup;

    #@10
    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->setFocusable(Z)V

    #@13
    .line 580
    if-nez p1, :cond_22

    #@15
    .line 581
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarView;->mUpGoerFive:Landroid/view/ViewGroup;

    #@17
    const/4 v1, 0x0

    #@18
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setContentDescription(Ljava/lang/CharSequence;)V

    #@1b
    .line 582
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarView;->mUpGoerFive:Landroid/view/ViewGroup;

    #@1d
    const/4 v1, 0x2

    #@1e
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setImportantForAccessibility(I)V

    #@21
    goto :goto_8

    #@22
    .line 584
    :cond_22
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarView;->mUpGoerFive:Landroid/view/ViewGroup;

    #@24
    const/4 v1, 0x0

    #@25
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setImportantForAccessibility(I)V

    #@28
    .line 585
    iget v0, p0, Lcom/android/internal/widget/ActionBarView;->mDisplayOptions:I

    #@2a
    and-int/lit8 v0, v0, 0x4

    #@2c
    if-eqz v0, :cond_41

    #@2e
    .line 586
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarView;->mUpGoerFive:Landroid/view/ViewGroup;

    #@30
    iget-object v1, p0, Lcom/android/internal/widget/ActionBarView;->mContext:Landroid/content/Context;

    #@32
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@35
    move-result-object v1

    #@36
    const v2, 0x104050b

    #@39
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@3c
    move-result-object v1

    #@3d
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setContentDescription(Ljava/lang/CharSequence;)V

    #@40
    goto :goto_8

    #@41
    .line 589
    :cond_41
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarView;->mUpGoerFive:Landroid/view/ViewGroup;

    #@43
    iget-object v1, p0, Lcom/android/internal/widget/ActionBarView;->mContext:Landroid/content/Context;

    #@45
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@48
    move-result-object v1

    #@49
    const v2, 0x104050a

    #@4c
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@4f
    move-result-object v1

    #@50
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setContentDescription(Ljava/lang/CharSequence;)V

    #@53
    goto :goto_8
.end method

.method private setTitleImpl(Ljava/lang/CharSequence;)V
    .registers 5
    .parameter "title"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 532
    iput-object p1, p0, Lcom/android/internal/widget/ActionBarView;->mTitle:Ljava/lang/CharSequence;

    #@3
    .line 533
    iget-object v2, p0, Lcom/android/internal/widget/ActionBarView;->mTitleView:Landroid/widget/TextView;

    #@5
    if-eqz v2, :cond_2e

    #@7
    .line 534
    iget-object v2, p0, Lcom/android/internal/widget/ActionBarView;->mTitleView:Landroid/widget/TextView;

    #@9
    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@c
    .line 535
    iget-object v2, p0, Lcom/android/internal/widget/ActionBarView;->mExpandedActionView:Landroid/view/View;

    #@e
    if-nez v2, :cond_38

    #@10
    iget v2, p0, Lcom/android/internal/widget/ActionBarView;->mDisplayOptions:I

    #@12
    and-int/lit8 v2, v2, 0x8

    #@14
    if-eqz v2, :cond_38

    #@16
    iget-object v2, p0, Lcom/android/internal/widget/ActionBarView;->mTitle:Ljava/lang/CharSequence;

    #@18
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@1b
    move-result v2

    #@1c
    if-eqz v2, :cond_26

    #@1e
    iget-object v2, p0, Lcom/android/internal/widget/ActionBarView;->mSubtitle:Ljava/lang/CharSequence;

    #@20
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@23
    move-result v2

    #@24
    if-nez v2, :cond_38

    #@26
    :cond_26
    const/4 v0, 0x1

    #@27
    .line 538
    .local v0, visible:Z
    :goto_27
    iget-object v2, p0, Lcom/android/internal/widget/ActionBarView;->mTitleLayout:Landroid/widget/LinearLayout;

    #@29
    if-eqz v0, :cond_3a

    #@2b
    :goto_2b
    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    #@2e
    .line 540
    .end local v0           #visible:Z
    :cond_2e
    iget-object v1, p0, Lcom/android/internal/widget/ActionBarView;->mLogoNavItem:Lcom/android/internal/view/menu/ActionMenuItem;

    #@30
    if-eqz v1, :cond_37

    #@32
    .line 541
    iget-object v1, p0, Lcom/android/internal/widget/ActionBarView;->mLogoNavItem:Lcom/android/internal/view/menu/ActionMenuItem;

    #@34
    invoke-virtual {v1, p1}, Lcom/android/internal/view/menu/ActionMenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    #@37
    .line 543
    :cond_37
    return-void

    #@38
    :cond_38
    move v0, v1

    #@39
    .line 535
    goto :goto_27

    #@3a
    .line 538
    .restart local v0       #visible:Z
    :cond_3a
    const/16 v1, 0x8

    #@3c
    goto :goto_2b
.end method


# virtual methods
.method public collapseActionView()V
    .registers 3

    #@0
    .prologue
    .line 486
    iget-object v1, p0, Lcom/android/internal/widget/ActionBarView;->mExpandedMenuPresenter:Lcom/android/internal/widget/ActionBarView$ExpandedActionViewMenuPresenter;

    #@2
    if-nez v1, :cond_b

    #@4
    const/4 v0, 0x0

    #@5
    .line 488
    .local v0, item:Lcom/android/internal/view/menu/MenuItemImpl;
    :goto_5
    if-eqz v0, :cond_a

    #@7
    .line 489
    invoke-virtual {v0}, Lcom/android/internal/view/menu/MenuItemImpl;->collapseActionView()Z

    #@a
    .line 491
    :cond_a
    return-void

    #@b
    .line 486
    .end local v0           #item:Lcom/android/internal/view/menu/MenuItemImpl;
    :cond_b
    iget-object v1, p0, Lcom/android/internal/widget/ActionBarView;->mExpandedMenuPresenter:Lcom/android/internal/widget/ActionBarView$ExpandedActionViewMenuPresenter;

    #@d
    iget-object v0, v1, Lcom/android/internal/widget/ActionBarView$ExpandedActionViewMenuPresenter;->mCurrentExpandedItem:Lcom/android/internal/view/menu/MenuItemImpl;

    #@f
    goto :goto_5
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .registers 3

    #@0
    .prologue
    .line 781
    new-instance v0, Landroid/app/ActionBar$LayoutParams;

    #@2
    const v1, 0x800013

    #@5
    invoke-direct {v0, v1}, Landroid/app/ActionBar$LayoutParams;-><init>(I)V

    #@8
    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .registers 4
    .parameter "attrs"

    #@0
    .prologue
    .line 1254
    new-instance v0, Landroid/app/ActionBar$LayoutParams;

    #@2
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarView;->getContext()Landroid/content/Context;

    #@5
    move-result-object v1

    #@6
    invoke-direct {v0, v1, p1}, Landroid/app/ActionBar$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@9
    return-object v0
.end method

.method public generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .registers 2
    .parameter "lp"

    #@0
    .prologue
    .line 1259
    if-nez p1, :cond_6

    #@2
    .line 1260
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarView;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@5
    move-result-object p1

    #@6
    .line 1262
    :cond_6
    return-object p1
.end method

.method public getCustomNavigationView()Landroid/view/View;
    .registers 2

    #@0
    .prologue
    .line 766
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarView;->mCustomNavView:Landroid/view/View;

    #@2
    return-object v0
.end method

.method public getDisplayOptions()I
    .registers 2

    #@0
    .prologue
    .line 774
    iget v0, p0, Lcom/android/internal/widget/ActionBarView;->mDisplayOptions:I

    #@2
    return v0
.end method

.method public getDropdownAdapter()Landroid/widget/SpinnerAdapter;
    .registers 2

    #@0
    .prologue
    .line 754
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarView;->mSpinnerAdapter:Landroid/widget/SpinnerAdapter;

    #@2
    return-object v0
.end method

.method public getDropdownSelectedPosition()I
    .registers 2

    #@0
    .prologue
    .line 762
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarView;->mSpinner:Landroid/widget/Spinner;

    #@2
    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getNavigationMode()I
    .registers 2

    #@0
    .prologue
    .line 770
    iget v0, p0, Lcom/android/internal/widget/ActionBarView;->mNavigationMode:I

    #@2
    return v0
.end method

.method public getSubtitle()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 546
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarView;->mSubtitle:Ljava/lang/CharSequence;

    #@2
    return-object v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 505
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarView;->mTitle:Ljava/lang/CharSequence;

    #@2
    return-object v0
.end method

.method public hasEmbeddedTabs()Z
    .registers 2

    #@0
    .prologue
    .line 383
    iget-boolean v0, p0, Lcom/android/internal/widget/ActionBarView;->mIncludeTabs:Z

    #@2
    return v0
.end method

.method public hasExpandedActionView()Z
    .registers 2

    #@0
    .prologue
    .line 481
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarView;->mExpandedMenuPresenter:Lcom/android/internal/widget/ActionBarView$ExpandedActionViewMenuPresenter;

    #@2
    if-eqz v0, :cond_c

    #@4
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarView;->mExpandedMenuPresenter:Lcom/android/internal/widget/ActionBarView$ExpandedActionViewMenuPresenter;

    #@6
    iget-object v0, v0, Lcom/android/internal/widget/ActionBarView$ExpandedActionViewMenuPresenter;->mCurrentExpandedItem:Lcom/android/internal/view/menu/MenuItemImpl;

    #@8
    if-eqz v0, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method public initIndeterminateProgress()V
    .registers 6

    #@0
    .prologue
    .line 330
    new-instance v0, Landroid/widget/ProgressBar;

    #@2
    iget-object v1, p0, Lcom/android/internal/widget/ActionBarView;->mContext:Landroid/content/Context;

    #@4
    const/4 v2, 0x0

    #@5
    const/4 v3, 0x0

    #@6
    iget v4, p0, Lcom/android/internal/widget/ActionBarView;->mIndeterminateProgressStyle:I

    #@8
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    #@b
    iput-object v0, p0, Lcom/android/internal/widget/ActionBarView;->mIndeterminateProgressView:Landroid/widget/ProgressBar;

    #@d
    .line 332
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarView;->mIndeterminateProgressView:Landroid/widget/ProgressBar;

    #@f
    const v1, 0x102036a

    #@12
    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setId(I)V

    #@15
    .line 333
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarView;->mIndeterminateProgressView:Landroid/widget/ProgressBar;

    #@17
    const/16 v1, 0x8

    #@19
    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    #@1c
    .line 334
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarView;->mIndeterminateProgressView:Landroid/widget/ProgressBar;

    #@1e
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/ActionBarView;->addView(Landroid/view/View;)V

    #@21
    .line 335
    return-void
.end method

.method public initProgress()V
    .registers 6

    #@0
    .prologue
    .line 322
    new-instance v0, Landroid/widget/ProgressBar;

    #@2
    iget-object v1, p0, Lcom/android/internal/widget/ActionBarView;->mContext:Landroid/content/Context;

    #@4
    const/4 v2, 0x0

    #@5
    const/4 v3, 0x0

    #@6
    iget v4, p0, Lcom/android/internal/widget/ActionBarView;->mProgressStyle:I

    #@8
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    #@b
    iput-object v0, p0, Lcom/android/internal/widget/ActionBarView;->mProgressView:Landroid/widget/ProgressBar;

    #@d
    .line 323
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarView;->mProgressView:Landroid/widget/ProgressBar;

    #@f
    const v1, 0x102036b

    #@12
    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setId(I)V

    #@15
    .line 324
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarView;->mProgressView:Landroid/widget/ProgressBar;

    #@17
    const/16 v1, 0x2710

    #@19
    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    #@1c
    .line 325
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarView;->mProgressView:Landroid/widget/ProgressBar;

    #@1e
    const/16 v1, 0x8

    #@20
    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    #@23
    .line 326
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarView;->mProgressView:Landroid/widget/ProgressBar;

    #@25
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/ActionBarView;->addView(Landroid/view/View;)V

    #@28
    .line 327
    return-void
.end method

.method public isCollapsed()Z
    .registers 2

    #@0
    .prologue
    .line 849
    iget-boolean v0, p0, Lcom/android/internal/widget/ActionBarView;->mIsCollapsed:Z

    #@2
    return v0
.end method

.method public isLayoutRtl()Z
    .registers 3

    #@0
    .prologue
    .line 1635
    invoke-super {p0}, Lcom/android/internal/widget/AbsActionBarView;->isLayoutRtl()Z

    #@3
    move-result v0

    #@4
    .line 1636
    .local v0, isLayoutRtl:Z
    if-eqz v0, :cond_f

    #@6
    iget-object v1, p0, Lcom/android/internal/widget/ActionBarView;->mTabScrollView:Lcom/android/internal/widget/ScrollingTabContainerView;

    #@8
    if-eqz v1, :cond_f

    #@a
    iget-boolean v1, p0, Lcom/android/internal/widget/ActionBarView;->mIncludeTabs:Z

    #@c
    if-eqz v1, :cond_f

    #@e
    .line 1637
    const/4 v0, 0x0

    #@f
    .line 1640
    .end local v0           #isLayoutRtl:Z
    :cond_f
    return v0
.end method

.method public isSplitActionBar()Z
    .registers 2

    #@0
    .prologue
    .line 379
    iget-boolean v0, p0, Lcom/android/internal/widget/AbsActionBarView;->mSplitActionBar:Z

    #@2
    return v0
.end method

.method public isTitleTruncated()Z
    .registers 6

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 856
    iget-object v4, p0, Lcom/android/internal/widget/ActionBarView;->mTitleView:Landroid/widget/TextView;

    #@3
    if-nez v4, :cond_6

    #@5
    .line 871
    :cond_5
    :goto_5
    return v3

    #@6
    .line 860
    :cond_6
    iget-object v4, p0, Lcom/android/internal/widget/ActionBarView;->mTitleView:Landroid/widget/TextView;

    #@8
    invoke-virtual {v4}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    #@b
    move-result-object v2

    #@c
    .line 861
    .local v2, titleLayout:Landroid/text/Layout;
    if-eqz v2, :cond_5

    #@e
    .line 865
    invoke-virtual {v2}, Landroid/text/Layout;->getLineCount()I

    #@11
    move-result v1

    #@12
    .line 866
    .local v1, lineCount:I
    const/4 v0, 0x0

    #@13
    .local v0, i:I
    :goto_13
    if-ge v0, v1, :cond_5

    #@15
    .line 867
    invoke-virtual {v2, v0}, Landroid/text/Layout;->getEllipsisCount(I)I

    #@18
    move-result v4

    #@19
    if-lez v4, :cond_1d

    #@1b
    .line 868
    const/4 v3, 0x1

    #@1c
    goto :goto_5

    #@1d
    .line 866
    :cond_1d
    add-int/lit8 v0, v0, 0x1

    #@1f
    goto :goto_13
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 6
    .parameter "newConfig"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 275
    invoke-super {p0, p1}, Lcom/android/internal/widget/AbsActionBarView;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    #@4
    .line 277
    iput-object v3, p0, Lcom/android/internal/widget/ActionBarView;->mTitleView:Landroid/widget/TextView;

    #@6
    .line 278
    iput-object v3, p0, Lcom/android/internal/widget/ActionBarView;->mSubtitleView:Landroid/widget/TextView;

    #@8
    .line 279
    iput-object v3, p0, Lcom/android/internal/widget/ActionBarView;->mTitleUpView:Landroid/view/View;

    #@a
    .line 280
    iget-object v1, p0, Lcom/android/internal/widget/ActionBarView;->mTitleLayout:Landroid/widget/LinearLayout;

    #@c
    if-eqz v1, :cond_1f

    #@e
    iget-object v1, p0, Lcom/android/internal/widget/ActionBarView;->mTitleLayout:Landroid/widget/LinearLayout;

    #@10
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getParent()Landroid/view/ViewParent;

    #@13
    move-result-object v1

    #@14
    iget-object v2, p0, Lcom/android/internal/widget/ActionBarView;->mUpGoerFive:Landroid/view/ViewGroup;

    #@16
    if-ne v1, v2, :cond_1f

    #@18
    .line 281
    iget-object v1, p0, Lcom/android/internal/widget/ActionBarView;->mUpGoerFive:Landroid/view/ViewGroup;

    #@1a
    iget-object v2, p0, Lcom/android/internal/widget/ActionBarView;->mTitleLayout:Landroid/widget/LinearLayout;

    #@1c
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    #@1f
    .line 283
    :cond_1f
    iput-object v3, p0, Lcom/android/internal/widget/ActionBarView;->mTitleLayout:Landroid/widget/LinearLayout;

    #@21
    .line 284
    iget v1, p0, Lcom/android/internal/widget/ActionBarView;->mDisplayOptions:I

    #@23
    and-int/lit8 v1, v1, 0x8

    #@25
    if-eqz v1, :cond_2a

    #@27
    .line 285
    invoke-direct {p0}, Lcom/android/internal/widget/ActionBarView;->initTitle()V

    #@2a
    .line 288
    :cond_2a
    iget-object v1, p0, Lcom/android/internal/widget/ActionBarView;->mTabScrollView:Lcom/android/internal/widget/ScrollingTabContainerView;

    #@2c
    if-eqz v1, :cond_46

    #@2e
    iget-boolean v1, p0, Lcom/android/internal/widget/ActionBarView;->mIncludeTabs:Z

    #@30
    if-eqz v1, :cond_46

    #@32
    .line 289
    iget-object v1, p0, Lcom/android/internal/widget/ActionBarView;->mTabScrollView:Lcom/android/internal/widget/ScrollingTabContainerView;

    #@34
    invoke-virtual {v1}, Lcom/android/internal/widget/ScrollingTabContainerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@37
    move-result-object v0

    #@38
    .line 290
    .local v0, lp:Landroid/view/ViewGroup$LayoutParams;
    if-eqz v0, :cond_40

    #@3a
    .line 291
    const/4 v1, -0x2

    #@3b
    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@3d
    .line 292
    const/4 v1, -0x1

    #@3e
    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@40
    .line 294
    :cond_40
    iget-object v1, p0, Lcom/android/internal/widget/ActionBarView;->mTabScrollView:Lcom/android/internal/widget/ScrollingTabContainerView;

    #@42
    const/4 v2, 0x1

    #@43
    invoke-virtual {v1, v2}, Lcom/android/internal/widget/ScrollingTabContainerView;->setAllowCollapse(Z)V

    #@46
    .line 296
    .end local v0           #lp:Landroid/view/ViewGroup$LayoutParams;
    :cond_46
    return-void
.end method

.method public onDetachedFromWindow()V
    .registers 2

    #@0
    .prologue
    .line 308
    invoke-super {p0}, Lcom/android/internal/widget/AbsActionBarView;->onDetachedFromWindow()V

    #@3
    .line 309
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarView;->mTabSelector:Ljava/lang/Runnable;

    #@5
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/ActionBarView;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@8
    .line 310
    iget-object v0, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@a
    if-eqz v0, :cond_16

    #@c
    .line 311
    iget-object v0, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@e
    invoke-virtual {v0}, Lcom/android/internal/view/menu/ActionMenuPresenter;->hideOverflowMenu()Z

    #@11
    .line 312
    iget-object v0, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@13
    invoke-virtual {v0}, Lcom/android/internal/view/menu/ActionMenuPresenter;->hideSubMenus()Z

    #@16
    .line 314
    :cond_16
    return-void
.end method

.method protected onFinishInflate()V
    .registers 5

    #@0
    .prologue
    .line 786
    invoke-super {p0}, Lcom/android/internal/widget/AbsActionBarView;->onFinishInflate()V

    #@3
    .line 788
    iget-object v1, p0, Lcom/android/internal/widget/ActionBarView;->mUpGoerFive:Landroid/view/ViewGroup;

    #@5
    iget-object v2, p0, Lcom/android/internal/widget/ActionBarView;->mHomeLayout:Lcom/android/internal/widget/ActionBarView$HomeView;

    #@7
    const/4 v3, 0x0

    #@8
    invoke-virtual {v1, v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    #@b
    .line 789
    iget-object v1, p0, Lcom/android/internal/widget/ActionBarView;->mUpGoerFive:Landroid/view/ViewGroup;

    #@d
    invoke-virtual {p0, v1}, Lcom/android/internal/widget/ActionBarView;->addView(Landroid/view/View;)V

    #@10
    .line 791
    iget-object v1, p0, Lcom/android/internal/widget/ActionBarView;->mCustomNavView:Landroid/view/View;

    #@12
    if-eqz v1, :cond_32

    #@14
    iget v1, p0, Lcom/android/internal/widget/ActionBarView;->mDisplayOptions:I

    #@16
    and-int/lit8 v1, v1, 0x10

    #@18
    if-eqz v1, :cond_32

    #@1a
    .line 792
    iget-object v1, p0, Lcom/android/internal/widget/ActionBarView;->mCustomNavView:Landroid/view/View;

    #@1c
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    #@1f
    move-result-object v0

    #@20
    .line 793
    .local v0, parent:Landroid/view/ViewParent;
    if-eq v0, p0, :cond_32

    #@22
    .line 794
    instance-of v1, v0, Landroid/view/ViewGroup;

    #@24
    if-eqz v1, :cond_2d

    #@26
    .line 795
    check-cast v0, Landroid/view/ViewGroup;

    #@28
    .end local v0           #parent:Landroid/view/ViewParent;
    iget-object v1, p0, Lcom/android/internal/widget/ActionBarView;->mCustomNavView:Landroid/view/View;

    #@2a
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    #@2d
    .line 797
    :cond_2d
    iget-object v1, p0, Lcom/android/internal/widget/ActionBarView;->mCustomNavView:Landroid/view/View;

    #@2f
    invoke-virtual {p0, v1}, Lcom/android/internal/widget/ActionBarView;->addView(Landroid/view/View;)V

    #@32
    .line 800
    :cond_32
    return-void
.end method

.method protected onLayout(ZIIII)V
    .registers 45
    .parameter "changed"
    .parameter "l"
    .parameter "t"
    .parameter "r"
    .parameter "b"

    #@0
    .prologue
    .line 1094
    sub-int v3, p5, p3

    #@2
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/widget/ActionBarView;->getPaddingTop()I

    #@5
    move-result v4

    #@6
    sub-int/2addr v3, v4

    #@7
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/widget/ActionBarView;->getPaddingBottom()I

    #@a
    move-result v4

    #@b
    sub-int v7, v3, v4

    #@d
    .line 1096
    .local v7, contentHeight:I
    if-gtz v7, :cond_10

    #@f
    .line 1250
    :cond_f
    :goto_f
    return-void

    #@10
    .line 1101
    :cond_10
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/widget/ActionBarView;->isLayoutRtl()Z

    #@13
    move-result v8

    #@14
    .line 1102
    .local v8, isLayoutRtl:Z
    if-eqz v8, :cond_1bc

    #@16
    const/16 v22, 0x1

    #@18
    .line 1103
    .local v22, direction:I
    :goto_18
    if-eqz v8, :cond_1c0

    #@1a
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/widget/ActionBarView;->getPaddingLeft()I

    #@1d
    move-result v11

    #@1e
    .line 1106
    .local v11, menuStart:I
    :goto_1e
    if-eqz v8, :cond_1ca

    #@20
    sub-int v3, p4, p2

    #@22
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/widget/ActionBarView;->getPaddingRight()I

    #@25
    move-result v4

    #@26
    sub-int v36, v3, v4

    #@28
    .line 1107
    .local v36, x:I
    :goto_28
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/widget/ActionBarView;->getPaddingTop()I

    #@2b
    move-result v6

    #@2c
    .line 1109
    .local v6, y:I
    move-object/from16 v0, p0

    #@2e
    iget-object v3, v0, Lcom/android/internal/widget/ActionBarView;->mExpandedActionView:Landroid/view/View;

    #@30
    if-eqz v3, :cond_1d0

    #@32
    move-object/from16 v0, p0

    #@34
    iget-object v0, v0, Lcom/android/internal/widget/ActionBarView;->mExpandedHomeLayout:Lcom/android/internal/widget/ActionBarView$HomeView;

    #@36
    move-object/from16 v26, v0

    #@38
    .line 1110
    .local v26, homeLayout:Lcom/android/internal/widget/ActionBarView$HomeView;
    :goto_38
    invoke-virtual/range {v26 .. v26}, Lcom/android/internal/widget/ActionBarView$HomeView;->getVisibility()I

    #@3b
    move-result v3

    #@3c
    const/16 v4, 0x8

    #@3e
    if-eq v3, v4, :cond_1d8

    #@40
    invoke-virtual/range {v26 .. v26}, Lcom/android/internal/widget/ActionBarView$HomeView;->getParent()Landroid/view/ViewParent;

    #@43
    move-result-object v3

    #@44
    move-object/from16 v0, p0

    #@46
    iget-object v4, v0, Lcom/android/internal/widget/ActionBarView;->mUpGoerFive:Landroid/view/ViewGroup;

    #@48
    if-ne v3, v4, :cond_1d8

    #@4a
    invoke-virtual/range {v26 .. v26}, Lcom/android/internal/widget/ActionBarView$HomeView;->getStartOffset()I

    #@4d
    move-result v33

    #@4e
    .line 1114
    .local v33, startOffset:I
    :goto_4e
    move-object/from16 v0, p0

    #@50
    iget-object v4, v0, Lcom/android/internal/widget/ActionBarView;->mUpGoerFive:Landroid/view/ViewGroup;

    #@52
    move/from16 v0, v36

    #@54
    move/from16 v1, v33

    #@56
    invoke-static {v0, v1, v8}, Lcom/android/internal/widget/ActionBarView;->next(IIZ)I

    #@59
    move-result v5

    #@5a
    move-object/from16 v3, p0

    #@5c
    invoke-virtual/range {v3 .. v8}, Lcom/android/internal/widget/ActionBarView;->positionChild(Landroid/view/View;IIIZ)I

    #@5f
    move-result v3

    #@60
    add-int v5, v36, v3

    #@62
    .line 1116
    .end local v36           #x:I
    .local v5, x:I
    move/from16 v0, v33

    #@64
    invoke-static {v5, v0, v8}, Lcom/android/internal/widget/ActionBarView;->next(IIZ)I

    #@67
    move-result v5

    #@68
    .line 1118
    move-object/from16 v0, p0

    #@6a
    iget-object v3, v0, Lcom/android/internal/widget/ActionBarView;->mExpandedActionView:Landroid/view/View;

    #@6c
    if-nez v3, :cond_91

    #@6e
    .line 1119
    move-object/from16 v0, p0

    #@70
    iget-object v3, v0, Lcom/android/internal/widget/ActionBarView;->mTitleLayout:Landroid/widget/LinearLayout;

    #@72
    if-eqz v3, :cond_1dc

    #@74
    move-object/from16 v0, p0

    #@76
    iget-object v3, v0, Lcom/android/internal/widget/ActionBarView;->mTitleLayout:Landroid/widget/LinearLayout;

    #@78
    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getVisibility()I

    #@7b
    move-result v3

    #@7c
    const/16 v4, 0x8

    #@7e
    if-eq v3, v4, :cond_1dc

    #@80
    move-object/from16 v0, p0

    #@82
    iget v3, v0, Lcom/android/internal/widget/ActionBarView;->mDisplayOptions:I

    #@84
    and-int/lit8 v3, v3, 0x8

    #@86
    if-eqz v3, :cond_1dc

    #@88
    const/16 v32, 0x1

    #@8a
    .line 1122
    .local v32, showTitle:Z
    :goto_8a
    move-object/from16 v0, p0

    #@8c
    iget v3, v0, Lcom/android/internal/widget/ActionBarView;->mNavigationMode:I

    #@8e
    packed-switch v3, :pswitch_data_2ca

    #@91
    .line 1144
    .end local v32           #showTitle:Z
    :cond_91
    :goto_91
    :pswitch_91
    move-object/from16 v0, p0

    #@93
    iget-object v3, v0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@95
    if-eqz v3, :cond_bc

    #@97
    move-object/from16 v0, p0

    #@99
    iget-object v3, v0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@9b
    invoke-virtual {v3}, Lcom/android/internal/view/menu/ActionMenuView;->getParent()Landroid/view/ViewParent;

    #@9e
    move-result-object v3

    #@9f
    move-object/from16 v0, p0

    #@a1
    if-ne v3, v0, :cond_bc

    #@a3
    .line 1145
    move-object/from16 v0, p0

    #@a5
    iget-object v10, v0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@a7
    if-nez v8, :cond_22a

    #@a9
    const/4 v14, 0x1

    #@aa
    :goto_aa
    move-object/from16 v9, p0

    #@ac
    move v12, v6

    #@ad
    move v13, v7

    #@ae
    invoke-virtual/range {v9 .. v14}, Lcom/android/internal/widget/ActionBarView;->positionChild(Landroid/view/View;IIIZ)I

    #@b1
    .line 1146
    move-object/from16 v0, p0

    #@b3
    iget-object v3, v0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@b5
    invoke-virtual {v3}, Lcom/android/internal/view/menu/ActionMenuView;->getMeasuredWidth()I

    #@b8
    move-result v3

    #@b9
    mul-int v3, v3, v22

    #@bb
    add-int/2addr v11, v3

    #@bc
    .line 1149
    :cond_bc
    move-object/from16 v0, p0

    #@be
    iget-object v3, v0, Lcom/android/internal/widget/ActionBarView;->mIndeterminateProgressView:Landroid/widget/ProgressBar;

    #@c0
    if-eqz v3, :cond_e7

    #@c2
    move-object/from16 v0, p0

    #@c4
    iget-object v3, v0, Lcom/android/internal/widget/ActionBarView;->mIndeterminateProgressView:Landroid/widget/ProgressBar;

    #@c6
    invoke-virtual {v3}, Landroid/widget/ProgressBar;->getVisibility()I

    #@c9
    move-result v3

    #@ca
    const/16 v4, 0x8

    #@cc
    if-eq v3, v4, :cond_e7

    #@ce
    .line 1151
    move-object/from16 v0, p0

    #@d0
    iget-object v10, v0, Lcom/android/internal/widget/ActionBarView;->mIndeterminateProgressView:Landroid/widget/ProgressBar;

    #@d2
    if-nez v8, :cond_22d

    #@d4
    const/4 v14, 0x1

    #@d5
    :goto_d5
    move-object/from16 v9, p0

    #@d7
    move v12, v6

    #@d8
    move v13, v7

    #@d9
    invoke-virtual/range {v9 .. v14}, Lcom/android/internal/widget/ActionBarView;->positionChild(Landroid/view/View;IIIZ)I

    #@dc
    .line 1152
    move-object/from16 v0, p0

    #@de
    iget-object v3, v0, Lcom/android/internal/widget/ActionBarView;->mIndeterminateProgressView:Landroid/widget/ProgressBar;

    #@e0
    invoke-virtual {v3}, Landroid/widget/ProgressBar;->getMeasuredWidth()I

    #@e3
    move-result v3

    #@e4
    mul-int v3, v3, v22

    #@e6
    add-int/2addr v11, v3

    #@e7
    .line 1155
    :cond_e7
    const/16 v20, 0x0

    #@e9
    .line 1156
    .local v20, customView:Landroid/view/View;
    move-object/from16 v0, p0

    #@eb
    iget-object v3, v0, Lcom/android/internal/widget/ActionBarView;->mExpandedActionView:Landroid/view/View;

    #@ed
    if-eqz v3, :cond_230

    #@ef
    .line 1157
    move-object/from16 v0, p0

    #@f1
    iget-object v0, v0, Lcom/android/internal/widget/ActionBarView;->mExpandedActionView:Landroid/view/View;

    #@f3
    move-object/from16 v20, v0

    #@f5
    .line 1162
    :cond_f5
    :goto_f5
    if-eqz v20, :cond_186

    #@f7
    .line 1163
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/widget/ActionBarView;->getLayoutDirection()I

    #@fa
    move-result v27

    #@fb
    .line 1164
    .local v27, layoutDirection:I
    invoke-virtual/range {v20 .. v20}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@fe
    move-result-object v28

    #@ff
    .line 1165
    .local v28, lp:Landroid/view/ViewGroup$LayoutParams;
    move-object/from16 v0, v28

    #@101
    instance-of v3, v0, Landroid/app/ActionBar$LayoutParams;

    #@103
    if-eqz v3, :cond_246

    #@105
    check-cast v28, Landroid/app/ActionBar$LayoutParams;

    #@107
    .end local v28           #lp:Landroid/view/ViewGroup$LayoutParams;
    move-object/from16 v15, v28

    #@109
    .line 1167
    .local v15, ablp:Landroid/app/ActionBar$LayoutParams;
    :goto_109
    if-eqz v15, :cond_249

    #@10b
    iget v0, v15, Landroid/app/ActionBar$LayoutParams;->gravity:I

    #@10d
    move/from16 v23, v0

    #@10f
    .line 1168
    .local v23, gravity:I
    :goto_10f
    invoke-virtual/range {v20 .. v20}, Landroid/view/View;->getMeasuredWidth()I

    #@112
    move-result v29

    #@113
    .line 1170
    .local v29, navWidth:I
    const/16 v34, 0x0

    #@115
    .line 1171
    .local v34, topMargin:I
    const/16 v16, 0x0

    #@117
    .line 1172
    .local v16, bottomMargin:I
    if-eqz v15, :cond_130

    #@119
    .line 1173
    invoke-virtual {v15}, Landroid/app/ActionBar$LayoutParams;->getMarginStart()I

    #@11c
    move-result v3

    #@11d
    invoke-static {v5, v3, v8}, Lcom/android/internal/widget/ActionBarView;->next(IIZ)I

    #@120
    move-result v5

    #@121
    .line 1174
    invoke-virtual {v15}, Landroid/app/ActionBar$LayoutParams;->getMarginEnd()I

    #@124
    move-result v3

    #@125
    mul-int v3, v3, v22

    #@127
    add-int/2addr v11, v3

    #@128
    .line 1175
    iget v0, v15, Landroid/app/ActionBar$LayoutParams;->topMargin:I

    #@12a
    move/from16 v34, v0

    #@12c
    .line 1176
    iget v0, v15, Landroid/app/ActionBar$LayoutParams;->bottomMargin:I

    #@12e
    move/from16 v16, v0

    #@130
    .line 1179
    :cond_130
    const v3, 0x800007

    #@133
    and-int v25, v23, v3

    #@135
    .line 1181
    .local v25, hgravity:I
    const/4 v3, 0x1

    #@136
    move/from16 v0, v25

    #@138
    if-ne v0, v3, :cond_26a

    #@13a
    .line 1182
    move-object/from16 v0, p0

    #@13c
    iget v3, v0, Lcom/android/internal/widget/ActionBarView;->mRight:I

    #@13e
    move-object/from16 v0, p0

    #@140
    iget v4, v0, Lcom/android/internal/widget/ActionBarView;->mLeft:I

    #@142
    sub-int/2addr v3, v4

    #@143
    sub-int v3, v3, v29

    #@145
    div-int/lit8 v18, v3, 0x2

    #@147
    .line 1183
    .local v18, centeredLeft:I
    if-eqz v8, :cond_256

    #@149
    .line 1184
    add-int v19, v18, v29

    #@14b
    .line 1185
    .local v19, centeredStart:I
    move/from16 v17, v18

    #@14d
    .line 1186
    .local v17, centeredEnd:I
    move/from16 v0, v19

    #@14f
    if-le v0, v5, :cond_24e

    #@151
    .line 1187
    const/16 v25, 0x5

    #@153
    .line 1204
    .end local v17           #centeredEnd:I
    .end local v18           #centeredLeft:I
    .end local v19           #centeredStart:I
    :cond_153
    :goto_153
    const/16 v37, 0x0

    #@155
    .line 1205
    .local v37, xpos:I
    move/from16 v0, v25

    #@157
    move/from16 v1, v27

    #@159
    invoke-static {v0, v1}, Landroid/view/Gravity;->getAbsoluteGravity(II)I

    #@15c
    move-result v3

    #@15d
    packed-switch v3, :pswitch_data_2d4

    #@160
    .line 1217
    :goto_160
    :pswitch_160
    and-int/lit8 v35, v23, 0x70

    #@162
    .line 1219
    .local v35, vgravity:I
    if-nez v23, :cond_166

    #@164
    .line 1220
    const/16 v35, 0x10

    #@166
    .line 1223
    :cond_166
    const/16 v38, 0x0

    #@168
    .line 1224
    .local v38, ypos:I
    sparse-switch v35, :sswitch_data_2e2

    #@16b
    .line 1238
    :goto_16b
    invoke-virtual/range {v20 .. v20}, Landroid/view/View;->getMeasuredWidth()I

    #@16e
    move-result v21

    #@16f
    .line 1239
    .local v21, customWidth:I
    add-int v3, v37, v21

    #@171
    invoke-virtual/range {v20 .. v20}, Landroid/view/View;->getMeasuredHeight()I

    #@174
    move-result v4

    #@175
    add-int v4, v4, v38

    #@177
    move-object/from16 v0, v20

    #@179
    move/from16 v1, v37

    #@17b
    move/from16 v2, v38

    #@17d
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->layout(IIII)V

    #@180
    .line 1241
    move/from16 v0, v21

    #@182
    invoke-static {v5, v0, v8}, Lcom/android/internal/widget/ActionBarView;->next(IIZ)I

    #@185
    move-result v5

    #@186
    .line 1244
    .end local v15           #ablp:Landroid/app/ActionBar$LayoutParams;
    .end local v16           #bottomMargin:I
    .end local v21           #customWidth:I
    .end local v23           #gravity:I
    .end local v25           #hgravity:I
    .end local v27           #layoutDirection:I
    .end local v29           #navWidth:I
    .end local v34           #topMargin:I
    .end local v35           #vgravity:I
    .end local v37           #xpos:I
    .end local v38           #ypos:I
    :cond_186
    move-object/from16 v0, p0

    #@188
    iget-object v3, v0, Lcom/android/internal/widget/ActionBarView;->mProgressView:Landroid/widget/ProgressBar;

    #@18a
    if-eqz v3, :cond_f

    #@18c
    .line 1245
    move-object/from16 v0, p0

    #@18e
    iget-object v3, v0, Lcom/android/internal/widget/ActionBarView;->mProgressView:Landroid/widget/ProgressBar;

    #@190
    invoke-virtual {v3}, Landroid/widget/ProgressBar;->bringToFront()V

    #@193
    .line 1246
    move-object/from16 v0, p0

    #@195
    iget-object v3, v0, Lcom/android/internal/widget/ActionBarView;->mProgressView:Landroid/widget/ProgressBar;

    #@197
    invoke-virtual {v3}, Landroid/widget/ProgressBar;->getMeasuredHeight()I

    #@19a
    move-result v3

    #@19b
    div-int/lit8 v24, v3, 0x2

    #@19d
    .line 1247
    .local v24, halfProgressHeight:I
    move-object/from16 v0, p0

    #@19f
    iget-object v3, v0, Lcom/android/internal/widget/ActionBarView;->mProgressView:Landroid/widget/ProgressBar;

    #@1a1
    move-object/from16 v0, p0

    #@1a3
    iget v4, v0, Lcom/android/internal/widget/ActionBarView;->mProgressBarPadding:I

    #@1a5
    move/from16 v0, v24

    #@1a7
    neg-int v9, v0

    #@1a8
    move-object/from16 v0, p0

    #@1aa
    iget v10, v0, Lcom/android/internal/widget/ActionBarView;->mProgressBarPadding:I

    #@1ac
    move-object/from16 v0, p0

    #@1ae
    iget-object v12, v0, Lcom/android/internal/widget/ActionBarView;->mProgressView:Landroid/widget/ProgressBar;

    #@1b0
    invoke-virtual {v12}, Landroid/widget/ProgressBar;->getMeasuredWidth()I

    #@1b3
    move-result v12

    #@1b4
    add-int/2addr v10, v12

    #@1b5
    move/from16 v0, v24

    #@1b7
    invoke-virtual {v3, v4, v9, v10, v0}, Landroid/widget/ProgressBar;->layout(IIII)V

    #@1ba
    goto/16 :goto_f

    #@1bc
    .line 1102
    .end local v5           #x:I
    .end local v6           #y:I
    .end local v11           #menuStart:I
    .end local v20           #customView:Landroid/view/View;
    .end local v22           #direction:I
    .end local v24           #halfProgressHeight:I
    .end local v26           #homeLayout:Lcom/android/internal/widget/ActionBarView$HomeView;
    .end local v33           #startOffset:I
    :cond_1bc
    const/16 v22, -0x1

    #@1be
    goto/16 :goto_18

    #@1c0
    .line 1103
    .restart local v22       #direction:I
    :cond_1c0
    sub-int v3, p4, p2

    #@1c2
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/widget/ActionBarView;->getPaddingRight()I

    #@1c5
    move-result v4

    #@1c6
    sub-int v11, v3, v4

    #@1c8
    goto/16 :goto_1e

    #@1ca
    .line 1106
    .restart local v11       #menuStart:I
    :cond_1ca
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/widget/ActionBarView;->getPaddingLeft()I

    #@1cd
    move-result v36

    #@1ce
    goto/16 :goto_28

    #@1d0
    .line 1109
    .restart local v6       #y:I
    .restart local v36       #x:I
    :cond_1d0
    move-object/from16 v0, p0

    #@1d2
    iget-object v0, v0, Lcom/android/internal/widget/ActionBarView;->mHomeLayout:Lcom/android/internal/widget/ActionBarView$HomeView;

    #@1d4
    move-object/from16 v26, v0

    #@1d6
    goto/16 :goto_38

    #@1d8
    .line 1110
    .restart local v26       #homeLayout:Lcom/android/internal/widget/ActionBarView$HomeView;
    :cond_1d8
    const/16 v33, 0x0

    #@1da
    goto/16 :goto_4e

    #@1dc
    .line 1119
    .end local v36           #x:I
    .restart local v5       #x:I
    .restart local v33       #startOffset:I
    :cond_1dc
    const/16 v32, 0x0

    #@1de
    goto/16 :goto_8a

    #@1e0
    .line 1126
    .restart local v32       #showTitle:Z
    :pswitch_1e0
    move-object/from16 v0, p0

    #@1e2
    iget-object v3, v0, Lcom/android/internal/widget/ActionBarView;->mListNavLayout:Landroid/widget/LinearLayout;

    #@1e4
    if-eqz v3, :cond_91

    #@1e6
    .line 1127
    if-eqz v32, :cond_1f0

    #@1e8
    .line 1128
    move-object/from16 v0, p0

    #@1ea
    iget v3, v0, Lcom/android/internal/widget/ActionBarView;->mItemPadding:I

    #@1ec
    invoke-static {v5, v3, v8}, Lcom/android/internal/widget/ActionBarView;->next(IIZ)I

    #@1ef
    move-result v5

    #@1f0
    .line 1130
    :cond_1f0
    move-object/from16 v0, p0

    #@1f2
    iget-object v4, v0, Lcom/android/internal/widget/ActionBarView;->mListNavLayout:Landroid/widget/LinearLayout;

    #@1f4
    move-object/from16 v3, p0

    #@1f6
    invoke-virtual/range {v3 .. v8}, Lcom/android/internal/widget/ActionBarView;->positionChild(Landroid/view/View;IIIZ)I

    #@1f9
    move-result v3

    #@1fa
    add-int/2addr v5, v3

    #@1fb
    .line 1131
    move-object/from16 v0, p0

    #@1fd
    iget v3, v0, Lcom/android/internal/widget/ActionBarView;->mItemPadding:I

    #@1ff
    invoke-static {v5, v3, v8}, Lcom/android/internal/widget/ActionBarView;->next(IIZ)I

    #@202
    move-result v5

    #@203
    goto/16 :goto_91

    #@205
    .line 1135
    :pswitch_205
    move-object/from16 v0, p0

    #@207
    iget-object v3, v0, Lcom/android/internal/widget/ActionBarView;->mTabScrollView:Lcom/android/internal/widget/ScrollingTabContainerView;

    #@209
    if-eqz v3, :cond_91

    #@20b
    .line 1136
    if-eqz v32, :cond_215

    #@20d
    move-object/from16 v0, p0

    #@20f
    iget v3, v0, Lcom/android/internal/widget/ActionBarView;->mItemPadding:I

    #@211
    invoke-static {v5, v3, v8}, Lcom/android/internal/widget/ActionBarView;->next(IIZ)I

    #@214
    move-result v5

    #@215
    .line 1137
    :cond_215
    move-object/from16 v0, p0

    #@217
    iget-object v4, v0, Lcom/android/internal/widget/ActionBarView;->mTabScrollView:Lcom/android/internal/widget/ScrollingTabContainerView;

    #@219
    move-object/from16 v3, p0

    #@21b
    invoke-virtual/range {v3 .. v8}, Lcom/android/internal/widget/ActionBarView;->positionChild(Landroid/view/View;IIIZ)I

    #@21e
    move-result v3

    #@21f
    add-int/2addr v5, v3

    #@220
    .line 1138
    move-object/from16 v0, p0

    #@222
    iget v3, v0, Lcom/android/internal/widget/ActionBarView;->mItemPadding:I

    #@224
    invoke-static {v5, v3, v8}, Lcom/android/internal/widget/ActionBarView;->next(IIZ)I

    #@227
    move-result v5

    #@228
    goto/16 :goto_91

    #@22a
    .line 1145
    .end local v32           #showTitle:Z
    :cond_22a
    const/4 v14, 0x0

    #@22b
    goto/16 :goto_aa

    #@22d
    .line 1151
    :cond_22d
    const/4 v14, 0x0

    #@22e
    goto/16 :goto_d5

    #@230
    .line 1158
    .restart local v20       #customView:Landroid/view/View;
    :cond_230
    move-object/from16 v0, p0

    #@232
    iget v3, v0, Lcom/android/internal/widget/ActionBarView;->mDisplayOptions:I

    #@234
    and-int/lit8 v3, v3, 0x10

    #@236
    if-eqz v3, :cond_f5

    #@238
    move-object/from16 v0, p0

    #@23a
    iget-object v3, v0, Lcom/android/internal/widget/ActionBarView;->mCustomNavView:Landroid/view/View;

    #@23c
    if-eqz v3, :cond_f5

    #@23e
    .line 1160
    move-object/from16 v0, p0

    #@240
    iget-object v0, v0, Lcom/android/internal/widget/ActionBarView;->mCustomNavView:Landroid/view/View;

    #@242
    move-object/from16 v20, v0

    #@244
    goto/16 :goto_f5

    #@246
    .line 1165
    .restart local v27       #layoutDirection:I
    .restart local v28       #lp:Landroid/view/ViewGroup$LayoutParams;
    :cond_246
    const/4 v15, 0x0

    #@247
    goto/16 :goto_109

    #@249
    .line 1167
    .end local v28           #lp:Landroid/view/ViewGroup$LayoutParams;
    .restart local v15       #ablp:Landroid/app/ActionBar$LayoutParams;
    :cond_249
    const v23, 0x800013

    #@24c
    goto/16 :goto_10f

    #@24e
    .line 1188
    .restart local v16       #bottomMargin:I
    .restart local v17       #centeredEnd:I
    .restart local v18       #centeredLeft:I
    .restart local v19       #centeredStart:I
    .restart local v23       #gravity:I
    .restart local v25       #hgravity:I
    .restart local v29       #navWidth:I
    .restart local v34       #topMargin:I
    :cond_24e
    move/from16 v0, v17

    #@250
    if-ge v0, v11, :cond_153

    #@252
    .line 1189
    const/16 v25, 0x3

    #@254
    goto/16 :goto_153

    #@256
    .line 1192
    .end local v17           #centeredEnd:I
    .end local v19           #centeredStart:I
    :cond_256
    move/from16 v19, v18

    #@258
    .line 1193
    .restart local v19       #centeredStart:I
    add-int v17, v18, v29

    #@25a
    .line 1194
    .restart local v17       #centeredEnd:I
    move/from16 v0, v19

    #@25c
    if-ge v0, v5, :cond_262

    #@25e
    .line 1195
    const/16 v25, 0x3

    #@260
    goto/16 :goto_153

    #@262
    .line 1196
    :cond_262
    move/from16 v0, v17

    #@264
    if-le v0, v11, :cond_153

    #@266
    .line 1197
    const/16 v25, 0x5

    #@268
    goto/16 :goto_153

    #@26a
    .line 1200
    .end local v17           #centeredEnd:I
    .end local v18           #centeredLeft:I
    .end local v19           #centeredStart:I
    :cond_26a
    if-nez v23, :cond_153

    #@26c
    .line 1201
    const v25, 0x800003

    #@26f
    goto/16 :goto_153

    #@271
    .line 1207
    .restart local v37       #xpos:I
    :pswitch_271
    move-object/from16 v0, p0

    #@273
    iget v3, v0, Lcom/android/internal/widget/ActionBarView;->mRight:I

    #@275
    move-object/from16 v0, p0

    #@277
    iget v4, v0, Lcom/android/internal/widget/ActionBarView;->mLeft:I

    #@279
    sub-int/2addr v3, v4

    #@27a
    sub-int v3, v3, v29

    #@27c
    div-int/lit8 v37, v3, 0x2

    #@27e
    .line 1208
    goto/16 :goto_160

    #@280
    .line 1210
    :pswitch_280
    if-eqz v8, :cond_286

    #@282
    move/from16 v37, v11

    #@284
    .line 1211
    :goto_284
    goto/16 :goto_160

    #@286
    :cond_286
    move/from16 v37, v5

    #@288
    .line 1210
    goto :goto_284

    #@289
    .line 1213
    :pswitch_289
    if-eqz v8, :cond_28f

    #@28b
    sub-int v37, v5, v29

    #@28d
    :goto_28d
    goto/16 :goto_160

    #@28f
    :cond_28f
    sub-int v37, v11, v29

    #@291
    goto :goto_28d

    #@292
    .line 1226
    .restart local v35       #vgravity:I
    .restart local v38       #ypos:I
    :sswitch_292
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/widget/ActionBarView;->getPaddingTop()I

    #@295
    move-result v31

    #@296
    .line 1227
    .local v31, paddedTop:I
    move-object/from16 v0, p0

    #@298
    iget v3, v0, Lcom/android/internal/widget/ActionBarView;->mBottom:I

    #@29a
    move-object/from16 v0, p0

    #@29c
    iget v4, v0, Lcom/android/internal/widget/ActionBarView;->mTop:I

    #@29e
    sub-int/2addr v3, v4

    #@29f
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/widget/ActionBarView;->getPaddingBottom()I

    #@2a2
    move-result v4

    #@2a3
    sub-int v30, v3, v4

    #@2a5
    .line 1228
    .local v30, paddedBottom:I
    sub-int v3, v30, v31

    #@2a7
    invoke-virtual/range {v20 .. v20}, Landroid/view/View;->getMeasuredHeight()I

    #@2aa
    move-result v4

    #@2ab
    sub-int/2addr v3, v4

    #@2ac
    div-int/lit8 v38, v3, 0x2

    #@2ae
    .line 1229
    goto/16 :goto_16b

    #@2b0
    .line 1231
    .end local v30           #paddedBottom:I
    .end local v31           #paddedTop:I
    :sswitch_2b0
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/widget/ActionBarView;->getPaddingTop()I

    #@2b3
    move-result v3

    #@2b4
    add-int v38, v3, v34

    #@2b6
    .line 1232
    goto/16 :goto_16b

    #@2b8
    .line 1234
    :sswitch_2b8
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/widget/ActionBarView;->getHeight()I

    #@2bb
    move-result v3

    #@2bc
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/widget/ActionBarView;->getPaddingBottom()I

    #@2bf
    move-result v4

    #@2c0
    sub-int/2addr v3, v4

    #@2c1
    invoke-virtual/range {v20 .. v20}, Landroid/view/View;->getMeasuredHeight()I

    #@2c4
    move-result v4

    #@2c5
    sub-int/2addr v3, v4

    #@2c6
    sub-int v38, v3, v16

    #@2c8
    goto/16 :goto_16b

    #@2ca
    .line 1122
    :pswitch_data_2ca
    .packed-switch 0x0
        :pswitch_91
        :pswitch_1e0
        :pswitch_205
    .end packed-switch

    #@2d4
    .line 1205
    :pswitch_data_2d4
    .packed-switch 0x1
        :pswitch_271
        :pswitch_160
        :pswitch_280
        :pswitch_160
        :pswitch_289
    .end packed-switch

    #@2e2
    .line 1224
    :sswitch_data_2e2
    .sparse-switch
        0x10 -> :sswitch_292
        0x30 -> :sswitch_2b0
        0x50 -> :sswitch_2b8
    .end sparse-switch
.end method

.method protected onMeasure(II)V
    .registers 51
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    .line 876
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/widget/ActionBarView;->getChildCount()I

    #@3
    move-result v8

    #@4
    .line 877
    .local v8, childCount:I
    move-object/from16 v0, p0

    #@6
    iget-boolean v0, v0, Lcom/android/internal/widget/ActionBarView;->mIsCollapsable:Z

    #@8
    move/from16 v44, v0

    #@a
    if-eqz v44, :cond_96

    #@c
    .line 878
    const/16 v42, 0x0

    #@e
    .line 879
    .local v42, visibleChildren:I
    const/16 v25, 0x0

    #@10
    .local v25, i:I
    :goto_10
    move/from16 v0, v25

    #@12
    if-ge v0, v8, :cond_4d

    #@14
    .line 880
    move-object/from16 v0, p0

    #@16
    move/from16 v1, v25

    #@18
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/ActionBarView;->getChildAt(I)Landroid/view/View;

    #@1b
    move-result-object v7

    #@1c
    .line 881
    .local v7, child:Landroid/view/View;
    invoke-virtual {v7}, Landroid/view/View;->getVisibility()I

    #@1f
    move-result v44

    #@20
    const/16 v45, 0x8

    #@22
    move/from16 v0, v44

    #@24
    move/from16 v1, v45

    #@26
    if-eq v0, v1, :cond_4a

    #@28
    move-object/from16 v0, p0

    #@2a
    iget-object v0, v0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@2c
    move-object/from16 v44, v0

    #@2e
    move-object/from16 v0, v44

    #@30
    if-ne v7, v0, :cond_3e

    #@32
    move-object/from16 v0, p0

    #@34
    iget-object v0, v0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@36
    move-object/from16 v44, v0

    #@38
    invoke-virtual/range {v44 .. v44}, Lcom/android/internal/view/menu/ActionMenuView;->getChildCount()I

    #@3b
    move-result v44

    #@3c
    if-eqz v44, :cond_4a

    #@3e
    :cond_3e
    move-object/from16 v0, p0

    #@40
    iget-object v0, v0, Lcom/android/internal/widget/ActionBarView;->mUpGoerFive:Landroid/view/ViewGroup;

    #@42
    move-object/from16 v44, v0

    #@44
    move-object/from16 v0, v44

    #@46
    if-eq v7, v0, :cond_4a

    #@48
    .line 884
    add-int/lit8 v42, v42, 0x1

    #@4a
    .line 879
    :cond_4a
    add-int/lit8 v25, v25, 0x1

    #@4c
    goto :goto_10

    #@4d
    .line 888
    .end local v7           #child:Landroid/view/View;
    :cond_4d
    move-object/from16 v0, p0

    #@4f
    iget-object v0, v0, Lcom/android/internal/widget/ActionBarView;->mUpGoerFive:Landroid/view/ViewGroup;

    #@51
    move-object/from16 v44, v0

    #@53
    invoke-virtual/range {v44 .. v44}, Landroid/view/ViewGroup;->getChildCount()I

    #@56
    move-result v38

    #@57
    .line 889
    .local v38, upChildCount:I
    const/16 v25, 0x0

    #@59
    :goto_59
    move/from16 v0, v25

    #@5b
    move/from16 v1, v38

    #@5d
    if-ge v0, v1, :cond_7e

    #@5f
    .line 890
    move-object/from16 v0, p0

    #@61
    iget-object v0, v0, Lcom/android/internal/widget/ActionBarView;->mUpGoerFive:Landroid/view/ViewGroup;

    #@63
    move-object/from16 v44, v0

    #@65
    move-object/from16 v0, v44

    #@67
    move/from16 v1, v25

    #@69
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    #@6c
    move-result-object v7

    #@6d
    .line 891
    .restart local v7       #child:Landroid/view/View;
    invoke-virtual {v7}, Landroid/view/View;->getVisibility()I

    #@70
    move-result v44

    #@71
    const/16 v45, 0x8

    #@73
    move/from16 v0, v44

    #@75
    move/from16 v1, v45

    #@77
    if-eq v0, v1, :cond_7b

    #@79
    .line 892
    add-int/lit8 v42, v42, 0x1

    #@7b
    .line 889
    :cond_7b
    add-int/lit8 v25, v25, 0x1

    #@7d
    goto :goto_59

    #@7e
    .line 896
    .end local v7           #child:Landroid/view/View;
    :cond_7e
    if-nez v42, :cond_96

    #@80
    .line 898
    const/16 v44, 0x0

    #@82
    const/16 v45, 0x0

    #@84
    move-object/from16 v0, p0

    #@86
    move/from16 v1, v44

    #@88
    move/from16 v2, v45

    #@8a
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/widget/ActionBarView;->setMeasuredDimension(II)V

    #@8d
    .line 899
    const/16 v44, 0x1

    #@8f
    move/from16 v0, v44

    #@91
    move-object/from16 v1, p0

    #@93
    iput-boolean v0, v1, Lcom/android/internal/widget/ActionBarView;->mIsCollapsed:Z

    #@95
    .line 1090
    .end local v25           #i:I
    .end local v38           #upChildCount:I
    .end local v42           #visibleChildren:I
    :cond_95
    :goto_95
    return-void

    #@96
    .line 903
    :cond_96
    const/16 v44, 0x0

    #@98
    move/from16 v0, v44

    #@9a
    move-object/from16 v1, p0

    #@9c
    iput-boolean v0, v1, Lcom/android/internal/widget/ActionBarView;->mIsCollapsed:Z

    #@9e
    .line 905
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@a1
    move-result v43

    #@a2
    .line 906
    .local v43, widthMode:I
    const/high16 v44, 0x4000

    #@a4
    move/from16 v0, v43

    #@a6
    move/from16 v1, v44

    #@a8
    if-eq v0, v1, :cond_d1

    #@aa
    .line 907
    new-instance v44, Ljava/lang/IllegalStateException;

    #@ac
    new-instance v45, Ljava/lang/StringBuilder;

    #@ae
    invoke-direct/range {v45 .. v45}, Ljava/lang/StringBuilder;-><init>()V

    #@b1
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@b4
    move-result-object v46

    #@b5
    invoke-virtual/range {v46 .. v46}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@b8
    move-result-object v46

    #@b9
    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bc
    move-result-object v45

    #@bd
    const-string v46, " can only be used "

    #@bf
    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c2
    move-result-object v45

    #@c3
    const-string v46, "with android:layout_width=\"match_parent\" (or fill_parent)"

    #@c5
    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c8
    move-result-object v45

    #@c9
    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cc
    move-result-object v45

    #@cd
    invoke-direct/range {v44 .. v45}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@d0
    throw v44

    #@d1
    .line 911
    :cond_d1
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@d4
    move-result v18

    #@d5
    .line 912
    .local v18, heightMode:I
    const/high16 v44, -0x8000

    #@d7
    move/from16 v0, v18

    #@d9
    move/from16 v1, v44

    #@db
    if-eq v0, v1, :cond_104

    #@dd
    .line 913
    new-instance v44, Ljava/lang/IllegalStateException;

    #@df
    new-instance v45, Ljava/lang/StringBuilder;

    #@e1
    invoke-direct/range {v45 .. v45}, Ljava/lang/StringBuilder;-><init>()V

    #@e4
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@e7
    move-result-object v46

    #@e8
    invoke-virtual/range {v46 .. v46}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@eb
    move-result-object v46

    #@ec
    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ef
    move-result-object v45

    #@f0
    const-string v46, " can only be used "

    #@f2
    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f5
    move-result-object v45

    #@f6
    const-string v46, "with android:layout_height=\"wrap_content\""

    #@f8
    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fb
    move-result-object v45

    #@fc
    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ff
    move-result-object v45

    #@100
    invoke-direct/range {v44 .. v45}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@103
    throw v44

    #@104
    .line 917
    :cond_104
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@107
    move-result v10

    #@108
    .line 919
    .local v10, contentWidth:I
    move-object/from16 v0, p0

    #@10a
    iget v0, v0, Lcom/android/internal/widget/AbsActionBarView;->mContentHeight:I

    #@10c
    move/from16 v44, v0

    #@10e
    if-ltz v44, :cond_39d

    #@110
    move-object/from16 v0, p0

    #@112
    iget v0, v0, Lcom/android/internal/widget/AbsActionBarView;->mContentHeight:I

    #@114
    move/from16 v30, v0

    #@116
    .line 922
    .local v30, maxHeight:I
    :goto_116
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/widget/ActionBarView;->getPaddingTop()I

    #@119
    move-result v44

    #@11a
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/widget/ActionBarView;->getPaddingBottom()I

    #@11d
    move-result v45

    #@11e
    add-int v41, v44, v45

    #@120
    .line 923
    .local v41, verticalPadding:I
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/widget/ActionBarView;->getPaddingLeft()I

    #@123
    move-result v33

    #@124
    .line 924
    .local v33, paddingLeft:I
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/widget/ActionBarView;->getPaddingRight()I

    #@127
    move-result v34

    #@128
    .line 925
    .local v34, paddingRight:I
    sub-int v17, v30, v41

    #@12a
    .line 926
    .local v17, height:I
    const/high16 v44, -0x8000

    #@12c
    move/from16 v0, v17

    #@12e
    move/from16 v1, v44

    #@130
    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@133
    move-result v9

    #@134
    .line 927
    .local v9, childSpecHeight:I
    const/high16 v44, 0x4000

    #@136
    move/from16 v0, v17

    #@138
    move/from16 v1, v44

    #@13a
    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@13d
    move-result v16

    #@13e
    .line 929
    .local v16, exactHeightSpec:I
    sub-int v44, v10, v33

    #@140
    sub-int v6, v44, v34

    #@142
    .line 930
    .local v6, availableWidth:I
    div-int/lit8 v27, v6, 0x2

    #@144
    .line 931
    .local v27, leftOfCenter:I
    move/from16 v35, v27

    #@146
    .line 933
    .local v35, rightOfCenter:I
    move-object/from16 v0, p0

    #@148
    iget-object v0, v0, Lcom/android/internal/widget/ActionBarView;->mExpandedActionView:Landroid/view/View;

    #@14a
    move-object/from16 v44, v0

    #@14c
    if-eqz v44, :cond_3a3

    #@14e
    move-object/from16 v0, p0

    #@150
    iget-object v0, v0, Lcom/android/internal/widget/ActionBarView;->mExpandedHomeLayout:Lcom/android/internal/widget/ActionBarView$HomeView;

    #@152
    move-object/from16 v20, v0

    #@154
    .line 935
    .local v20, homeLayout:Lcom/android/internal/widget/ActionBarView$HomeView;
    :goto_154
    const/16 v22, 0x0

    #@156
    .line 936
    .local v22, homeWidth:I
    invoke-virtual/range {v20 .. v20}, Lcom/android/internal/widget/ActionBarView$HomeView;->getVisibility()I

    #@159
    move-result v44

    #@15a
    const/16 v45, 0x8

    #@15c
    move/from16 v0, v44

    #@15e
    move/from16 v1, v45

    #@160
    if-eq v0, v1, :cond_1a9

    #@162
    invoke-virtual/range {v20 .. v20}, Lcom/android/internal/widget/ActionBarView$HomeView;->getParent()Landroid/view/ViewParent;

    #@165
    move-result-object v44

    #@166
    move-object/from16 v0, p0

    #@168
    iget-object v0, v0, Lcom/android/internal/widget/ActionBarView;->mUpGoerFive:Landroid/view/ViewGroup;

    #@16a
    move-object/from16 v45, v0

    #@16c
    move-object/from16 v0, v44

    #@16e
    move-object/from16 v1, v45

    #@170
    if-ne v0, v1, :cond_1a9

    #@172
    .line 937
    invoke-virtual/range {v20 .. v20}, Lcom/android/internal/widget/ActionBarView$HomeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@175
    move-result-object v29

    #@176
    .line 939
    .local v29, lp:Landroid/view/ViewGroup$LayoutParams;
    move-object/from16 v0, v29

    #@178
    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@17a
    move/from16 v44, v0

    #@17c
    if-gez v44, :cond_3ab

    #@17e
    .line 940
    const/high16 v44, -0x8000

    #@180
    move/from16 v0, v44

    #@182
    invoke-static {v6, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@185
    move-result v23

    #@186
    .line 951
    .local v23, homeWidthSpec:I
    :goto_186
    move-object/from16 v0, v20

    #@188
    move/from16 v1, v23

    #@18a
    move/from16 v2, v16

    #@18c
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/widget/ActionBarView$HomeView;->measure(II)V

    #@18f
    .line 952
    invoke-virtual/range {v20 .. v20}, Lcom/android/internal/widget/ActionBarView$HomeView;->getMeasuredWidth()I

    #@192
    move-result v22

    #@193
    .line 953
    invoke-virtual/range {v20 .. v20}, Lcom/android/internal/widget/ActionBarView$HomeView;->getStartOffset()I

    #@196
    move-result v44

    #@197
    add-int v21, v22, v44

    #@199
    .line 954
    .local v21, homeOffsetWidth:I
    const/16 v44, 0x0

    #@19b
    sub-int v45, v6, v21

    #@19d
    invoke-static/range {v44 .. v45}, Ljava/lang/Math;->max(II)I

    #@1a0
    move-result v6

    #@1a1
    .line 955
    const/16 v44, 0x0

    #@1a3
    sub-int v45, v6, v21

    #@1a5
    invoke-static/range {v44 .. v45}, Ljava/lang/Math;->max(II)I

    #@1a8
    move-result v27

    #@1a9
    .line 958
    .end local v21           #homeOffsetWidth:I
    .end local v23           #homeWidthSpec:I
    .end local v29           #lp:Landroid/view/ViewGroup$LayoutParams;
    :cond_1a9
    move-object/from16 v0, p0

    #@1ab
    iget-object v0, v0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@1ad
    move-object/from16 v44, v0

    #@1af
    if-eqz v44, :cond_1e7

    #@1b1
    move-object/from16 v0, p0

    #@1b3
    iget-object v0, v0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@1b5
    move-object/from16 v44, v0

    #@1b7
    invoke-virtual/range {v44 .. v44}, Lcom/android/internal/view/menu/ActionMenuView;->getParent()Landroid/view/ViewParent;

    #@1ba
    move-result-object v44

    #@1bb
    move-object/from16 v0, v44

    #@1bd
    move-object/from16 v1, p0

    #@1bf
    if-ne v0, v1, :cond_1e7

    #@1c1
    .line 959
    move-object/from16 v0, p0

    #@1c3
    iget-object v0, v0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@1c5
    move-object/from16 v44, v0

    #@1c7
    const/16 v45, 0x0

    #@1c9
    move-object/from16 v0, p0

    #@1cb
    move-object/from16 v1, v44

    #@1cd
    move/from16 v2, v16

    #@1cf
    move/from16 v3, v45

    #@1d1
    invoke-virtual {v0, v1, v6, v2, v3}, Lcom/android/internal/widget/ActionBarView;->measureChildView(Landroid/view/View;III)I

    #@1d4
    move-result v6

    #@1d5
    .line 960
    const/16 v44, 0x0

    #@1d7
    move-object/from16 v0, p0

    #@1d9
    iget-object v0, v0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@1db
    move-object/from16 v45, v0

    #@1dd
    invoke-virtual/range {v45 .. v45}, Lcom/android/internal/view/menu/ActionMenuView;->getMeasuredWidth()I

    #@1e0
    move-result v45

    #@1e1
    sub-int v45, v35, v45

    #@1e3
    invoke-static/range {v44 .. v45}, Ljava/lang/Math;->max(II)I

    #@1e6
    move-result v35

    #@1e7
    .line 963
    :cond_1e7
    move-object/from16 v0, p0

    #@1e9
    iget-object v0, v0, Lcom/android/internal/widget/ActionBarView;->mIndeterminateProgressView:Landroid/widget/ProgressBar;

    #@1eb
    move-object/from16 v44, v0

    #@1ed
    if-eqz v44, :cond_225

    #@1ef
    move-object/from16 v0, p0

    #@1f1
    iget-object v0, v0, Lcom/android/internal/widget/ActionBarView;->mIndeterminateProgressView:Landroid/widget/ProgressBar;

    #@1f3
    move-object/from16 v44, v0

    #@1f5
    invoke-virtual/range {v44 .. v44}, Landroid/widget/ProgressBar;->getVisibility()I

    #@1f8
    move-result v44

    #@1f9
    const/16 v45, 0x8

    #@1fb
    move/from16 v0, v44

    #@1fd
    move/from16 v1, v45

    #@1ff
    if-eq v0, v1, :cond_225

    #@201
    .line 965
    move-object/from16 v0, p0

    #@203
    iget-object v0, v0, Lcom/android/internal/widget/ActionBarView;->mIndeterminateProgressView:Landroid/widget/ProgressBar;

    #@205
    move-object/from16 v44, v0

    #@207
    const/16 v45, 0x0

    #@209
    move-object/from16 v0, p0

    #@20b
    move-object/from16 v1, v44

    #@20d
    move/from16 v2, v45

    #@20f
    invoke-virtual {v0, v1, v6, v9, v2}, Lcom/android/internal/widget/ActionBarView;->measureChildView(Landroid/view/View;III)I

    #@212
    move-result v6

    #@213
    .line 967
    const/16 v44, 0x0

    #@215
    move-object/from16 v0, p0

    #@217
    iget-object v0, v0, Lcom/android/internal/widget/ActionBarView;->mIndeterminateProgressView:Landroid/widget/ProgressBar;

    #@219
    move-object/from16 v45, v0

    #@21b
    invoke-virtual/range {v45 .. v45}, Landroid/widget/ProgressBar;->getMeasuredWidth()I

    #@21e
    move-result v45

    #@21f
    sub-int v45, v35, v45

    #@221
    invoke-static/range {v44 .. v45}, Ljava/lang/Math;->max(II)I

    #@224
    move-result v35

    #@225
    .line 971
    :cond_225
    move-object/from16 v0, p0

    #@227
    iget-object v0, v0, Lcom/android/internal/widget/ActionBarView;->mTitleLayout:Landroid/widget/LinearLayout;

    #@229
    move-object/from16 v44, v0

    #@22b
    if-eqz v44, :cond_3b9

    #@22d
    move-object/from16 v0, p0

    #@22f
    iget-object v0, v0, Lcom/android/internal/widget/ActionBarView;->mTitleLayout:Landroid/widget/LinearLayout;

    #@231
    move-object/from16 v44, v0

    #@233
    invoke-virtual/range {v44 .. v44}, Landroid/widget/LinearLayout;->getVisibility()I

    #@236
    move-result v44

    #@237
    const/16 v45, 0x8

    #@239
    move/from16 v0, v44

    #@23b
    move/from16 v1, v45

    #@23d
    if-eq v0, v1, :cond_3b9

    #@23f
    move-object/from16 v0, p0

    #@241
    iget v0, v0, Lcom/android/internal/widget/ActionBarView;->mDisplayOptions:I

    #@243
    move/from16 v44, v0

    #@245
    and-int/lit8 v44, v44, 0x8

    #@247
    if-eqz v44, :cond_3b9

    #@249
    const/16 v36, 0x1

    #@24b
    .line 974
    .local v36, showTitle:Z
    :goto_24b
    move-object/from16 v0, p0

    #@24d
    iget-object v0, v0, Lcom/android/internal/widget/ActionBarView;->mExpandedActionView:Landroid/view/View;

    #@24f
    move-object/from16 v44, v0

    #@251
    if-nez v44, :cond_25c

    #@253
    .line 975
    move-object/from16 v0, p0

    #@255
    iget v0, v0, Lcom/android/internal/widget/ActionBarView;->mNavigationMode:I

    #@257
    move/from16 v44, v0

    #@259
    packed-switch v44, :pswitch_data_51e

    #@25c
    .line 1005
    :cond_25c
    :goto_25c
    const/4 v15, 0x0

    #@25d
    .line 1006
    .local v15, customView:Landroid/view/View;
    move-object/from16 v0, p0

    #@25f
    iget-object v0, v0, Lcom/android/internal/widget/ActionBarView;->mExpandedActionView:Landroid/view/View;

    #@261
    move-object/from16 v44, v0

    #@263
    if-eqz v44, :cond_47d

    #@265
    .line 1007
    move-object/from16 v0, p0

    #@267
    iget-object v15, v0, Lcom/android/internal/widget/ActionBarView;->mExpandedActionView:Landroid/view/View;

    #@269
    .line 1013
    :cond_269
    :goto_269
    if-eqz v15, :cond_336

    #@26b
    .line 1014
    invoke-virtual {v15}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@26e
    move-result-object v44

    #@26f
    move-object/from16 v0, p0

    #@271
    move-object/from16 v1, v44

    #@273
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/ActionBarView;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;

    #@276
    move-result-object v29

    #@277
    .line 1015
    .restart local v29       #lp:Landroid/view/ViewGroup$LayoutParams;
    move-object/from16 v0, v29

    #@279
    instance-of v0, v0, Landroid/app/ActionBar$LayoutParams;

    #@27b
    move/from16 v44, v0

    #@27d
    if-eqz v44, :cond_495

    #@27f
    move-object/from16 v44, v29

    #@281
    check-cast v44, Landroid/app/ActionBar$LayoutParams;

    #@283
    move-object/from16 v5, v44

    #@285
    .line 1018
    .local v5, ablp:Landroid/app/ActionBar$LayoutParams;
    :goto_285
    const/16 v24, 0x0

    #@287
    .line 1019
    .local v24, horizontalMargin:I
    const/16 v40, 0x0

    #@289
    .line 1020
    .local v40, verticalMargin:I
    if-eqz v5, :cond_29f

    #@28b
    .line 1021
    iget v0, v5, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@28d
    move/from16 v44, v0

    #@28f
    iget v0, v5, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@291
    move/from16 v45, v0

    #@293
    add-int v24, v44, v45

    #@295
    .line 1022
    iget v0, v5, Landroid/app/ActionBar$LayoutParams;->topMargin:I

    #@297
    move/from16 v44, v0

    #@299
    iget v0, v5, Landroid/app/ActionBar$LayoutParams;->bottomMargin:I

    #@29b
    move/from16 v45, v0

    #@29d
    add-int v40, v44, v45

    #@29f
    .line 1028
    :cond_29f
    move-object/from16 v0, p0

    #@2a1
    iget v0, v0, Lcom/android/internal/widget/AbsActionBarView;->mContentHeight:I

    #@2a3
    move/from16 v44, v0

    #@2a5
    if-gtz v44, :cond_498

    #@2a7
    .line 1029
    const/high16 v12, -0x8000

    #@2a9
    .line 1034
    .local v12, customNavHeightMode:I
    :goto_2a9
    const/16 v44, 0x0

    #@2ab
    move-object/from16 v0, v29

    #@2ad
    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@2af
    move/from16 v45, v0

    #@2b1
    if-ltz v45, :cond_2c1

    #@2b3
    move-object/from16 v0, v29

    #@2b5
    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@2b7
    move/from16 v45, v0

    #@2b9
    move/from16 v0, v45

    #@2bb
    move/from16 v1, v17

    #@2bd
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    #@2c0
    move-result v17

    #@2c1
    .end local v17           #height:I
    :cond_2c1
    sub-int v45, v17, v40

    #@2c3
    invoke-static/range {v44 .. v45}, Ljava/lang/Math;->max(II)I

    #@2c6
    move-result v11

    #@2c7
    .line 1037
    .local v11, customNavHeight:I
    move-object/from16 v0, v29

    #@2c9
    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@2cb
    move/from16 v44, v0

    #@2cd
    const/16 v45, -0x2

    #@2cf
    move/from16 v0, v44

    #@2d1
    move/from16 v1, v45

    #@2d3
    if-eq v0, v1, :cond_4ad

    #@2d5
    const/high16 v14, 0x4000

    #@2d7
    .line 1039
    .local v14, customNavWidthMode:I
    :goto_2d7
    const/16 v45, 0x0

    #@2d9
    move-object/from16 v0, v29

    #@2db
    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@2dd
    move/from16 v44, v0

    #@2df
    if-ltz v44, :cond_4b1

    #@2e1
    move-object/from16 v0, v29

    #@2e3
    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@2e5
    move/from16 v44, v0

    #@2e7
    move/from16 v0, v44

    #@2e9
    invoke-static {v0, v6}, Ljava/lang/Math;->min(II)I

    #@2ec
    move-result v44

    #@2ed
    :goto_2ed
    sub-int v44, v44, v24

    #@2ef
    move/from16 v0, v45

    #@2f1
    move/from16 v1, v44

    #@2f3
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@2f6
    move-result v13

    #@2f7
    .line 1042
    .local v13, customNavWidth:I
    if-eqz v5, :cond_4b5

    #@2f9
    iget v0, v5, Landroid/app/ActionBar$LayoutParams;->gravity:I

    #@2fb
    move/from16 v44, v0

    #@2fd
    :goto_2fd
    and-int/lit8 v19, v44, 0x7

    #@2ff
    .line 1047
    .local v19, hgrav:I
    const/16 v44, 0x1

    #@301
    move/from16 v0, v19

    #@303
    move/from16 v1, v44

    #@305
    if-ne v0, v1, :cond_31f

    #@307
    move-object/from16 v0, v29

    #@309
    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@30b
    move/from16 v44, v0

    #@30d
    const/16 v45, -0x1

    #@30f
    move/from16 v0, v44

    #@311
    move/from16 v1, v45

    #@313
    if-ne v0, v1, :cond_31f

    #@315
    .line 1048
    move/from16 v0, v27

    #@317
    move/from16 v1, v35

    #@319
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    #@31c
    move-result v44

    #@31d
    mul-int/lit8 v13, v44, 0x2

    #@31f
    .line 1051
    :cond_31f
    invoke-static {v13, v14}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@322
    move-result v44

    #@323
    invoke-static {v11, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@326
    move-result v45

    #@327
    move/from16 v0, v44

    #@329
    move/from16 v1, v45

    #@32b
    invoke-virtual {v15, v0, v1}, Landroid/view/View;->measure(II)V

    #@32e
    .line 1054
    invoke-virtual {v15}, Landroid/view/View;->getMeasuredWidth()I

    #@331
    move-result v44

    #@332
    add-int v44, v44, v24

    #@334
    sub-int v6, v6, v44

    #@336
    .line 1061
    .end local v5           #ablp:Landroid/app/ActionBar$LayoutParams;
    .end local v11           #customNavHeight:I
    .end local v12           #customNavHeightMode:I
    .end local v13           #customNavWidth:I
    .end local v14           #customNavWidthMode:I
    .end local v19           #hgrav:I
    .end local v24           #horizontalMargin:I
    .end local v29           #lp:Landroid/view/ViewGroup$LayoutParams;
    .end local v40           #verticalMargin:I
    :cond_336
    move-object/from16 v0, p0

    #@338
    iget-object v0, v0, Lcom/android/internal/widget/ActionBarView;->mUpGoerFive:Landroid/view/ViewGroup;

    #@33a
    move-object/from16 v44, v0

    #@33c
    add-int v45, v6, v22

    #@33e
    move-object/from16 v0, p0

    #@340
    iget v0, v0, Lcom/android/internal/widget/AbsActionBarView;->mContentHeight:I

    #@342
    move/from16 v46, v0

    #@344
    const/high16 v47, 0x4000

    #@346
    invoke-static/range {v46 .. v47}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@349
    move-result v46

    #@34a
    const/16 v47, 0x0

    #@34c
    move-object/from16 v0, p0

    #@34e
    move-object/from16 v1, v44

    #@350
    move/from16 v2, v45

    #@352
    move/from16 v3, v46

    #@354
    move/from16 v4, v47

    #@356
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/internal/widget/ActionBarView;->measureChildView(Landroid/view/View;III)I

    #@359
    move-result v6

    #@35a
    .line 1063
    move-object/from16 v0, p0

    #@35c
    iget-object v0, v0, Lcom/android/internal/widget/ActionBarView;->mTitleLayout:Landroid/widget/LinearLayout;

    #@35e
    move-object/from16 v44, v0

    #@360
    if-eqz v44, :cond_374

    #@362
    .line 1064
    const/16 v44, 0x0

    #@364
    move-object/from16 v0, p0

    #@366
    iget-object v0, v0, Lcom/android/internal/widget/ActionBarView;->mTitleLayout:Landroid/widget/LinearLayout;

    #@368
    move-object/from16 v45, v0

    #@36a
    invoke-virtual/range {v45 .. v45}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    #@36d
    move-result v45

    #@36e
    sub-int v45, v27, v45

    #@370
    invoke-static/range {v44 .. v45}, Ljava/lang/Math;->max(II)I

    #@373
    move-result v27

    #@374
    .line 1067
    :cond_374
    move-object/from16 v0, p0

    #@376
    iget v0, v0, Lcom/android/internal/widget/AbsActionBarView;->mContentHeight:I

    #@378
    move/from16 v44, v0

    #@37a
    if-gtz v44, :cond_515

    #@37c
    .line 1068
    const/16 v31, 0x0

    #@37e
    .line 1069
    .local v31, measuredHeight:I
    const/16 v25, 0x0

    #@380
    .restart local v25       #i:I
    :goto_380
    move/from16 v0, v25

    #@382
    if-ge v0, v8, :cond_4ba

    #@384
    .line 1070
    move-object/from16 v0, p0

    #@386
    move/from16 v1, v25

    #@388
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/ActionBarView;->getChildAt(I)Landroid/view/View;

    #@38b
    move-result-object v39

    #@38c
    .line 1071
    .local v39, v:Landroid/view/View;
    invoke-virtual/range {v39 .. v39}, Landroid/view/View;->getMeasuredHeight()I

    #@38f
    move-result v44

    #@390
    add-int v32, v44, v41

    #@392
    .line 1072
    .local v32, paddedViewHeight:I
    move/from16 v0, v32

    #@394
    move/from16 v1, v31

    #@396
    if-le v0, v1, :cond_39a

    #@398
    .line 1073
    move/from16 v31, v32

    #@39a
    .line 1069
    :cond_39a
    add-int/lit8 v25, v25, 0x1

    #@39c
    goto :goto_380

    #@39d
    .line 919
    .end local v6           #availableWidth:I
    .end local v9           #childSpecHeight:I
    .end local v15           #customView:Landroid/view/View;
    .end local v16           #exactHeightSpec:I
    .end local v20           #homeLayout:Lcom/android/internal/widget/ActionBarView$HomeView;
    .end local v22           #homeWidth:I
    .end local v25           #i:I
    .end local v27           #leftOfCenter:I
    .end local v30           #maxHeight:I
    .end local v31           #measuredHeight:I
    .end local v32           #paddedViewHeight:I
    .end local v33           #paddingLeft:I
    .end local v34           #paddingRight:I
    .end local v35           #rightOfCenter:I
    .end local v36           #showTitle:Z
    .end local v39           #v:Landroid/view/View;
    .end local v41           #verticalPadding:I
    :cond_39d
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@3a0
    move-result v30

    #@3a1
    goto/16 :goto_116

    #@3a3
    .line 933
    .restart local v6       #availableWidth:I
    .restart local v9       #childSpecHeight:I
    .restart local v16       #exactHeightSpec:I
    .restart local v17       #height:I
    .restart local v27       #leftOfCenter:I
    .restart local v30       #maxHeight:I
    .restart local v33       #paddingLeft:I
    .restart local v34       #paddingRight:I
    .restart local v35       #rightOfCenter:I
    .restart local v41       #verticalPadding:I
    :cond_3a3
    move-object/from16 v0, p0

    #@3a5
    iget-object v0, v0, Lcom/android/internal/widget/ActionBarView;->mHomeLayout:Lcom/android/internal/widget/ActionBarView$HomeView;

    #@3a7
    move-object/from16 v20, v0

    #@3a9
    goto/16 :goto_154

    #@3ab
    .line 942
    .restart local v20       #homeLayout:Lcom/android/internal/widget/ActionBarView$HomeView;
    .restart local v22       #homeWidth:I
    .restart local v29       #lp:Landroid/view/ViewGroup$LayoutParams;
    :cond_3ab
    move-object/from16 v0, v29

    #@3ad
    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@3af
    move/from16 v44, v0

    #@3b1
    const/high16 v45, 0x4000

    #@3b3
    invoke-static/range {v44 .. v45}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@3b6
    move-result v23

    #@3b7
    .restart local v23       #homeWidthSpec:I
    goto/16 :goto_186

    #@3b9
    .line 971
    .end local v23           #homeWidthSpec:I
    .end local v29           #lp:Landroid/view/ViewGroup$LayoutParams;
    :cond_3b9
    const/16 v36, 0x0

    #@3bb
    goto/16 :goto_24b

    #@3bd
    .line 977
    .restart local v36       #showTitle:Z
    :pswitch_3bd
    move-object/from16 v0, p0

    #@3bf
    iget-object v0, v0, Lcom/android/internal/widget/ActionBarView;->mListNavLayout:Landroid/widget/LinearLayout;

    #@3c1
    move-object/from16 v44, v0

    #@3c3
    if-eqz v44, :cond_25c

    #@3c5
    .line 978
    if-eqz v36, :cond_416

    #@3c7
    move-object/from16 v0, p0

    #@3c9
    iget v0, v0, Lcom/android/internal/widget/ActionBarView;->mItemPadding:I

    #@3cb
    move/from16 v44, v0

    #@3cd
    mul-int/lit8 v26, v44, 0x2

    #@3cf
    .line 979
    .local v26, itemPaddingSize:I
    :goto_3cf
    const/16 v44, 0x0

    #@3d1
    sub-int v45, v6, v26

    #@3d3
    invoke-static/range {v44 .. v45}, Ljava/lang/Math;->max(II)I

    #@3d6
    move-result v6

    #@3d7
    .line 980
    const/16 v44, 0x0

    #@3d9
    sub-int v45, v27, v26

    #@3db
    invoke-static/range {v44 .. v45}, Ljava/lang/Math;->max(II)I

    #@3de
    move-result v27

    #@3df
    .line 981
    move-object/from16 v0, p0

    #@3e1
    iget-object v0, v0, Lcom/android/internal/widget/ActionBarView;->mListNavLayout:Landroid/widget/LinearLayout;

    #@3e3
    move-object/from16 v44, v0

    #@3e5
    const/high16 v45, -0x8000

    #@3e7
    move/from16 v0, v45

    #@3e9
    invoke-static {v6, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@3ec
    move-result v45

    #@3ed
    const/high16 v46, 0x4000

    #@3ef
    move/from16 v0, v17

    #@3f1
    move/from16 v1, v46

    #@3f3
    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@3f6
    move-result v46

    #@3f7
    invoke-virtual/range {v44 .. v46}, Landroid/widget/LinearLayout;->measure(II)V

    #@3fa
    .line 984
    move-object/from16 v0, p0

    #@3fc
    iget-object v0, v0, Lcom/android/internal/widget/ActionBarView;->mListNavLayout:Landroid/widget/LinearLayout;

    #@3fe
    move-object/from16 v44, v0

    #@400
    invoke-virtual/range {v44 .. v44}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    #@403
    move-result v28

    #@404
    .line 985
    .local v28, listNavWidth:I
    const/16 v44, 0x0

    #@406
    sub-int v45, v6, v28

    #@408
    invoke-static/range {v44 .. v45}, Ljava/lang/Math;->max(II)I

    #@40b
    move-result v6

    #@40c
    .line 986
    const/16 v44, 0x0

    #@40e
    sub-int v45, v27, v28

    #@410
    invoke-static/range {v44 .. v45}, Ljava/lang/Math;->max(II)I

    #@413
    move-result v27

    #@414
    .line 987
    goto/16 :goto_25c

    #@416
    .line 978
    .end local v26           #itemPaddingSize:I
    .end local v28           #listNavWidth:I
    :cond_416
    move-object/from16 v0, p0

    #@418
    iget v0, v0, Lcom/android/internal/widget/ActionBarView;->mItemPadding:I

    #@41a
    move/from16 v26, v0

    #@41c
    goto :goto_3cf

    #@41d
    .line 990
    :pswitch_41d
    move-object/from16 v0, p0

    #@41f
    iget-object v0, v0, Lcom/android/internal/widget/ActionBarView;->mTabScrollView:Lcom/android/internal/widget/ScrollingTabContainerView;

    #@421
    move-object/from16 v44, v0

    #@423
    if-eqz v44, :cond_25c

    #@425
    .line 991
    if-eqz v36, :cond_476

    #@427
    move-object/from16 v0, p0

    #@429
    iget v0, v0, Lcom/android/internal/widget/ActionBarView;->mItemPadding:I

    #@42b
    move/from16 v44, v0

    #@42d
    mul-int/lit8 v26, v44, 0x2

    #@42f
    .line 992
    .restart local v26       #itemPaddingSize:I
    :goto_42f
    const/16 v44, 0x0

    #@431
    sub-int v45, v6, v26

    #@433
    invoke-static/range {v44 .. v45}, Ljava/lang/Math;->max(II)I

    #@436
    move-result v6

    #@437
    .line 993
    const/16 v44, 0x0

    #@439
    sub-int v45, v27, v26

    #@43b
    invoke-static/range {v44 .. v45}, Ljava/lang/Math;->max(II)I

    #@43e
    move-result v27

    #@43f
    .line 994
    move-object/from16 v0, p0

    #@441
    iget-object v0, v0, Lcom/android/internal/widget/ActionBarView;->mTabScrollView:Lcom/android/internal/widget/ScrollingTabContainerView;

    #@443
    move-object/from16 v44, v0

    #@445
    const/high16 v45, -0x8000

    #@447
    move/from16 v0, v45

    #@449
    invoke-static {v6, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@44c
    move-result v45

    #@44d
    const/high16 v46, 0x4000

    #@44f
    move/from16 v0, v17

    #@451
    move/from16 v1, v46

    #@453
    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@456
    move-result v46

    #@457
    invoke-virtual/range {v44 .. v46}, Lcom/android/internal/widget/ScrollingTabContainerView;->measure(II)V

    #@45a
    .line 997
    move-object/from16 v0, p0

    #@45c
    iget-object v0, v0, Lcom/android/internal/widget/ActionBarView;->mTabScrollView:Lcom/android/internal/widget/ScrollingTabContainerView;

    #@45e
    move-object/from16 v44, v0

    #@460
    invoke-virtual/range {v44 .. v44}, Lcom/android/internal/widget/ScrollingTabContainerView;->getMeasuredWidth()I

    #@463
    move-result v37

    #@464
    .line 998
    .local v37, tabWidth:I
    const/16 v44, 0x0

    #@466
    sub-int v45, v6, v37

    #@468
    invoke-static/range {v44 .. v45}, Ljava/lang/Math;->max(II)I

    #@46b
    move-result v6

    #@46c
    .line 999
    const/16 v44, 0x0

    #@46e
    sub-int v45, v27, v37

    #@470
    invoke-static/range {v44 .. v45}, Ljava/lang/Math;->max(II)I

    #@473
    move-result v27

    #@474
    goto/16 :goto_25c

    #@476
    .line 991
    .end local v26           #itemPaddingSize:I
    .end local v37           #tabWidth:I
    :cond_476
    move-object/from16 v0, p0

    #@478
    iget v0, v0, Lcom/android/internal/widget/ActionBarView;->mItemPadding:I

    #@47a
    move/from16 v26, v0

    #@47c
    goto :goto_42f

    #@47d
    .line 1008
    .restart local v15       #customView:Landroid/view/View;
    :cond_47d
    move-object/from16 v0, p0

    #@47f
    iget v0, v0, Lcom/android/internal/widget/ActionBarView;->mDisplayOptions:I

    #@481
    move/from16 v44, v0

    #@483
    and-int/lit8 v44, v44, 0x10

    #@485
    if-eqz v44, :cond_269

    #@487
    move-object/from16 v0, p0

    #@489
    iget-object v0, v0, Lcom/android/internal/widget/ActionBarView;->mCustomNavView:Landroid/view/View;

    #@48b
    move-object/from16 v44, v0

    #@48d
    if-eqz v44, :cond_269

    #@48f
    .line 1010
    move-object/from16 v0, p0

    #@491
    iget-object v15, v0, Lcom/android/internal/widget/ActionBarView;->mCustomNavView:Landroid/view/View;

    #@493
    goto/16 :goto_269

    #@495
    .line 1015
    .restart local v29       #lp:Landroid/view/ViewGroup$LayoutParams;
    :cond_495
    const/4 v5, 0x0

    #@496
    goto/16 :goto_285

    #@498
    .line 1031
    .restart local v5       #ablp:Landroid/app/ActionBar$LayoutParams;
    .restart local v24       #horizontalMargin:I
    .restart local v40       #verticalMargin:I
    :cond_498
    move-object/from16 v0, v29

    #@49a
    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@49c
    move/from16 v44, v0

    #@49e
    const/16 v45, -0x2

    #@4a0
    move/from16 v0, v44

    #@4a2
    move/from16 v1, v45

    #@4a4
    if-eq v0, v1, :cond_4aa

    #@4a6
    const/high16 v12, 0x4000

    #@4a8
    .restart local v12       #customNavHeightMode:I
    :goto_4a8
    goto/16 :goto_2a9

    #@4aa
    .end local v12           #customNavHeightMode:I
    :cond_4aa
    const/high16 v12, -0x8000

    #@4ac
    goto :goto_4a8

    #@4ad
    .line 1037
    .end local v17           #height:I
    .restart local v11       #customNavHeight:I
    .restart local v12       #customNavHeightMode:I
    :cond_4ad
    const/high16 v14, -0x8000

    #@4af
    goto/16 :goto_2d7

    #@4b1
    .restart local v14       #customNavWidthMode:I
    :cond_4b1
    move/from16 v44, v6

    #@4b3
    .line 1039
    goto/16 :goto_2ed

    #@4b5
    .line 1042
    .restart local v13       #customNavWidth:I
    :cond_4b5
    const v44, 0x800013

    #@4b8
    goto/16 :goto_2fd

    #@4ba
    .line 1076
    .end local v5           #ablp:Landroid/app/ActionBar$LayoutParams;
    .end local v11           #customNavHeight:I
    .end local v12           #customNavHeightMode:I
    .end local v13           #customNavWidth:I
    .end local v14           #customNavWidthMode:I
    .end local v24           #horizontalMargin:I
    .end local v29           #lp:Landroid/view/ViewGroup$LayoutParams;
    .end local v40           #verticalMargin:I
    .restart local v25       #i:I
    .restart local v31       #measuredHeight:I
    :cond_4ba
    move-object/from16 v0, p0

    #@4bc
    move/from16 v1, v31

    #@4be
    invoke-virtual {v0, v10, v1}, Lcom/android/internal/widget/ActionBarView;->setMeasuredDimension(II)V

    #@4c1
    .line 1081
    .end local v25           #i:I
    .end local v31           #measuredHeight:I
    :goto_4c1
    move-object/from16 v0, p0

    #@4c3
    iget-object v0, v0, Lcom/android/internal/widget/ActionBarView;->mContextView:Lcom/android/internal/widget/ActionBarContextView;

    #@4c5
    move-object/from16 v44, v0

    #@4c7
    if-eqz v44, :cond_4d6

    #@4c9
    .line 1082
    move-object/from16 v0, p0

    #@4cb
    iget-object v0, v0, Lcom/android/internal/widget/ActionBarView;->mContextView:Lcom/android/internal/widget/ActionBarContextView;

    #@4cd
    move-object/from16 v44, v0

    #@4cf
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/widget/ActionBarView;->getMeasuredHeight()I

    #@4d2
    move-result v45

    #@4d3
    invoke-virtual/range {v44 .. v45}, Lcom/android/internal/widget/ActionBarContextView;->setContentHeight(I)V

    #@4d6
    .line 1085
    :cond_4d6
    move-object/from16 v0, p0

    #@4d8
    iget-object v0, v0, Lcom/android/internal/widget/ActionBarView;->mProgressView:Landroid/widget/ProgressBar;

    #@4da
    move-object/from16 v44, v0

    #@4dc
    if-eqz v44, :cond_95

    #@4de
    move-object/from16 v0, p0

    #@4e0
    iget-object v0, v0, Lcom/android/internal/widget/ActionBarView;->mProgressView:Landroid/widget/ProgressBar;

    #@4e2
    move-object/from16 v44, v0

    #@4e4
    invoke-virtual/range {v44 .. v44}, Landroid/widget/ProgressBar;->getVisibility()I

    #@4e7
    move-result v44

    #@4e8
    const/16 v45, 0x8

    #@4ea
    move/from16 v0, v44

    #@4ec
    move/from16 v1, v45

    #@4ee
    if-eq v0, v1, :cond_95

    #@4f0
    .line 1086
    move-object/from16 v0, p0

    #@4f2
    iget-object v0, v0, Lcom/android/internal/widget/ActionBarView;->mProgressView:Landroid/widget/ProgressBar;

    #@4f4
    move-object/from16 v44, v0

    #@4f6
    move-object/from16 v0, p0

    #@4f8
    iget v0, v0, Lcom/android/internal/widget/ActionBarView;->mProgressBarPadding:I

    #@4fa
    move/from16 v45, v0

    #@4fc
    mul-int/lit8 v45, v45, 0x2

    #@4fe
    sub-int v45, v10, v45

    #@500
    const/high16 v46, 0x4000

    #@502
    invoke-static/range {v45 .. v46}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@505
    move-result v45

    #@506
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/widget/ActionBarView;->getMeasuredHeight()I

    #@509
    move-result v46

    #@50a
    const/high16 v47, -0x8000

    #@50c
    invoke-static/range {v46 .. v47}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@50f
    move-result v46

    #@510
    invoke-virtual/range {v44 .. v46}, Landroid/widget/ProgressBar;->measure(II)V

    #@513
    goto/16 :goto_95

    #@515
    .line 1078
    :cond_515
    move-object/from16 v0, p0

    #@517
    move/from16 v1, v30

    #@519
    invoke-virtual {v0, v10, v1}, Lcom/android/internal/widget/ActionBarView;->setMeasuredDimension(II)V

    #@51c
    goto :goto_4c1

    #@51d
    .line 975
    nop

    #@51e
    :pswitch_data_51e
    .packed-switch 0x1
        :pswitch_3bd
        :pswitch_41d
    .end packed-switch
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .registers 6
    .parameter "p"

    #@0
    .prologue
    .line 1281
    move-object v1, p1

    #@1
    check-cast v1, Lcom/android/internal/widget/ActionBarView$SavedState;

    #@3
    .line 1283
    .local v1, state:Lcom/android/internal/widget/ActionBarView$SavedState;
    invoke-virtual {v1}, Lcom/android/internal/widget/ActionBarView$SavedState;->getSuperState()Landroid/os/Parcelable;

    #@6
    move-result-object v2

    #@7
    invoke-super {p0, v2}, Lcom/android/internal/widget/AbsActionBarView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    #@a
    .line 1285
    iget v2, v1, Lcom/android/internal/widget/ActionBarView$SavedState;->expandedMenuItemId:I

    #@c
    if-eqz v2, :cond_23

    #@e
    iget-object v2, p0, Lcom/android/internal/widget/ActionBarView;->mExpandedMenuPresenter:Lcom/android/internal/widget/ActionBarView$ExpandedActionViewMenuPresenter;

    #@10
    if-eqz v2, :cond_23

    #@12
    iget-object v2, p0, Lcom/android/internal/widget/ActionBarView;->mOptionsMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@14
    if-eqz v2, :cond_23

    #@16
    .line 1287
    iget-object v2, p0, Lcom/android/internal/widget/ActionBarView;->mOptionsMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@18
    iget v3, v1, Lcom/android/internal/widget/ActionBarView$SavedState;->expandedMenuItemId:I

    #@1a
    invoke-virtual {v2, v3}, Lcom/android/internal/view/menu/MenuBuilder;->findItem(I)Landroid/view/MenuItem;

    #@1d
    move-result-object v0

    #@1e
    .line 1288
    .local v0, item:Landroid/view/MenuItem;
    if-eqz v0, :cond_23

    #@20
    .line 1289
    invoke-interface {v0}, Landroid/view/MenuItem;->expandActionView()Z

    #@23
    .line 1293
    .end local v0           #item:Landroid/view/MenuItem;
    :cond_23
    iget-boolean v2, v1, Lcom/android/internal/widget/ActionBarView$SavedState;->isOverflowOpen:Z

    #@25
    if-eqz v2, :cond_2a

    #@27
    .line 1294
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarView;->postShowOverflowMenu()V

    #@2a
    .line 1296
    :cond_2a
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .registers 4

    #@0
    .prologue
    .line 1267
    invoke-super {p0}, Lcom/android/internal/widget/AbsActionBarView;->onSaveInstanceState()Landroid/os/Parcelable;

    #@3
    move-result-object v1

    #@4
    .line 1268
    .local v1, superState:Landroid/os/Parcelable;
    new-instance v0, Lcom/android/internal/widget/ActionBarView$SavedState;

    #@6
    invoke-direct {v0, v1}, Lcom/android/internal/widget/ActionBarView$SavedState;-><init>(Landroid/os/Parcelable;)V

    #@9
    .line 1270
    .local v0, state:Lcom/android/internal/widget/ActionBarView$SavedState;
    iget-object v2, p0, Lcom/android/internal/widget/ActionBarView;->mExpandedMenuPresenter:Lcom/android/internal/widget/ActionBarView$ExpandedActionViewMenuPresenter;

    #@b
    if-eqz v2, :cond_1d

    #@d
    iget-object v2, p0, Lcom/android/internal/widget/ActionBarView;->mExpandedMenuPresenter:Lcom/android/internal/widget/ActionBarView$ExpandedActionViewMenuPresenter;

    #@f
    iget-object v2, v2, Lcom/android/internal/widget/ActionBarView$ExpandedActionViewMenuPresenter;->mCurrentExpandedItem:Lcom/android/internal/view/menu/MenuItemImpl;

    #@11
    if-eqz v2, :cond_1d

    #@13
    .line 1271
    iget-object v2, p0, Lcom/android/internal/widget/ActionBarView;->mExpandedMenuPresenter:Lcom/android/internal/widget/ActionBarView$ExpandedActionViewMenuPresenter;

    #@15
    iget-object v2, v2, Lcom/android/internal/widget/ActionBarView$ExpandedActionViewMenuPresenter;->mCurrentExpandedItem:Lcom/android/internal/view/menu/MenuItemImpl;

    #@17
    invoke-virtual {v2}, Lcom/android/internal/view/menu/MenuItemImpl;->getItemId()I

    #@1a
    move-result v2

    #@1b
    iput v2, v0, Lcom/android/internal/widget/ActionBarView$SavedState;->expandedMenuItemId:I

    #@1d
    .line 1274
    :cond_1d
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarView;->isOverflowMenuShowing()Z

    #@20
    move-result v2

    #@21
    iput-boolean v2, v0, Lcom/android/internal/widget/ActionBarView$SavedState;->isOverflowOpen:Z

    #@23
    .line 1276
    return-object v0
.end method

.method public setCallback(Landroid/app/ActionBar$OnNavigationListener;)V
    .registers 2
    .parameter "callback"

    #@0
    .prologue
    .line 402
    iput-object p1, p0, Lcom/android/internal/widget/ActionBarView;->mCallback:Landroid/app/ActionBar$OnNavigationListener;

    #@2
    .line 403
    return-void
.end method

.method public setCollapsable(Z)V
    .registers 2
    .parameter "collapsable"

    #@0
    .prologue
    .line 845
    iput-boolean p1, p0, Lcom/android/internal/widget/ActionBarView;->mIsCollapsable:Z

    #@2
    .line 846
    return-void
.end method

.method public setContextView(Lcom/android/internal/widget/ActionBarContextView;)V
    .registers 2
    .parameter "view"

    #@0
    .prologue
    .line 841
    iput-object p1, p0, Lcom/android/internal/widget/ActionBarView;->mContextView:Lcom/android/internal/widget/ActionBarContextView;

    #@2
    .line 842
    return-void
.end method

.method public setCustomNavigationView(Landroid/view/View;)V
    .registers 4
    .parameter "view"

    #@0
    .prologue
    .line 494
    iget v1, p0, Lcom/android/internal/widget/ActionBarView;->mDisplayOptions:I

    #@2
    and-int/lit8 v1, v1, 0x10

    #@4
    if-eqz v1, :cond_20

    #@6
    const/4 v0, 0x1

    #@7
    .line 495
    .local v0, showCustom:Z
    :goto_7
    iget-object v1, p0, Lcom/android/internal/widget/ActionBarView;->mCustomNavView:Landroid/view/View;

    #@9
    if-eqz v1, :cond_12

    #@b
    if-eqz v0, :cond_12

    #@d
    .line 496
    iget-object v1, p0, Lcom/android/internal/widget/ActionBarView;->mCustomNavView:Landroid/view/View;

    #@f
    invoke-virtual {p0, v1}, Lcom/android/internal/widget/ActionBarView;->removeView(Landroid/view/View;)V

    #@12
    .line 498
    :cond_12
    iput-object p1, p0, Lcom/android/internal/widget/ActionBarView;->mCustomNavView:Landroid/view/View;

    #@14
    .line 499
    iget-object v1, p0, Lcom/android/internal/widget/ActionBarView;->mCustomNavView:Landroid/view/View;

    #@16
    if-eqz v1, :cond_1f

    #@18
    if-eqz v0, :cond_1f

    #@1a
    .line 500
    iget-object v1, p0, Lcom/android/internal/widget/ActionBarView;->mCustomNavView:Landroid/view/View;

    #@1c
    invoke-virtual {p0, v1}, Lcom/android/internal/widget/ActionBarView;->addView(Landroid/view/View;)V

    #@1f
    .line 502
    :cond_1f
    return-void

    #@20
    .line 494
    .end local v0           #showCustom:Z
    :cond_20
    const/4 v0, 0x0

    #@21
    goto :goto_7
.end method

.method public setDisplayOptions(I)V
    .registers 14
    .parameter "options"

    #@0
    .prologue
    const/16 v7, 0x8

    #@2
    const/4 v11, 0x2

    #@3
    const/4 v0, -0x1

    #@4
    const/4 v8, 0x1

    #@5
    const/4 v6, 0x0

    #@6
    .line 596
    iget v9, p0, Lcom/android/internal/widget/ActionBarView;->mDisplayOptions:I

    #@8
    if-ne v9, v0, :cond_ac

    #@a
    .line 597
    .local v0, flagsChanged:I
    :goto_a
    iput p1, p0, Lcom/android/internal/widget/ActionBarView;->mDisplayOptions:I

    #@c
    .line 599
    and-int/lit8 v9, v0, 0x3f

    #@e
    if-eqz v9, :cond_dd

    #@10
    .line 600
    and-int/lit8 v9, p1, 0x2

    #@12
    if-eqz v9, :cond_b2

    #@14
    move v4, v8

    #@15
    .line 601
    .local v4, showHome:Z
    :goto_15
    if-eqz v4, :cond_b5

    #@17
    iget-object v9, p0, Lcom/android/internal/widget/ActionBarView;->mExpandedActionView:Landroid/view/View;

    #@19
    if-nez v9, :cond_b5

    #@1b
    move v5, v6

    #@1c
    .line 602
    .local v5, vis:I
    :goto_1c
    iget-object v9, p0, Lcom/android/internal/widget/ActionBarView;->mHomeLayout:Lcom/android/internal/widget/ActionBarView$HomeView;

    #@1e
    invoke-virtual {v9, v5}, Lcom/android/internal/widget/ActionBarView$HomeView;->setVisibility(I)V

    #@21
    .line 604
    and-int/lit8 v9, v0, 0x4

    #@23
    if-eqz v9, :cond_34

    #@25
    .line 605
    and-int/lit8 v9, p1, 0x4

    #@27
    if-eqz v9, :cond_b8

    #@29
    move v3, v8

    #@2a
    .line 606
    .local v3, setUp:Z
    :goto_2a
    iget-object v9, p0, Lcom/android/internal/widget/ActionBarView;->mHomeLayout:Lcom/android/internal/widget/ActionBarView$HomeView;

    #@2c
    invoke-virtual {v9, v3}, Lcom/android/internal/widget/ActionBarView$HomeView;->setUp(Z)V

    #@2f
    .line 612
    if-eqz v3, :cond_34

    #@31
    .line 613
    invoke-virtual {p0, v8}, Lcom/android/internal/widget/ActionBarView;->setHomeButtonEnabled(Z)V

    #@34
    .line 617
    .end local v3           #setUp:Z
    :cond_34
    and-int/lit8 v9, v0, 0x1

    #@36
    if-eqz v9, :cond_4a

    #@38
    .line 618
    iget-object v9, p0, Lcom/android/internal/widget/ActionBarView;->mLogo:Landroid/graphics/drawable/Drawable;

    #@3a
    if-eqz v9, :cond_bb

    #@3c
    and-int/lit8 v9, p1, 0x1

    #@3e
    if-eqz v9, :cond_bb

    #@40
    move v2, v8

    #@41
    .line 619
    .local v2, logoVis:Z
    :goto_41
    iget-object v10, p0, Lcom/android/internal/widget/ActionBarView;->mHomeLayout:Lcom/android/internal/widget/ActionBarView$HomeView;

    #@43
    if-eqz v2, :cond_bd

    #@45
    iget-object v9, p0, Lcom/android/internal/widget/ActionBarView;->mLogo:Landroid/graphics/drawable/Drawable;

    #@47
    :goto_47
    invoke-virtual {v10, v9}, Lcom/android/internal/widget/ActionBarView$HomeView;->setIcon(Landroid/graphics/drawable/Drawable;)V

    #@4a
    .line 622
    .end local v2           #logoVis:Z
    :cond_4a
    and-int/lit8 v9, v0, 0x8

    #@4c
    if-eqz v9, :cond_55

    #@4e
    .line 623
    and-int/lit8 v9, p1, 0x8

    #@50
    if-eqz v9, :cond_c0

    #@52
    .line 624
    invoke-direct {p0}, Lcom/android/internal/widget/ActionBarView;->initTitle()V

    #@55
    .line 630
    :cond_55
    :goto_55
    iget-object v9, p0, Lcom/android/internal/widget/ActionBarView;->mTitleLayout:Landroid/widget/LinearLayout;

    #@57
    if-eqz v9, :cond_6e

    #@59
    and-int/lit8 v9, v0, 0x6

    #@5b
    if-eqz v9, :cond_6e

    #@5d
    .line 632
    iget v9, p0, Lcom/android/internal/widget/ActionBarView;->mDisplayOptions:I

    #@5f
    and-int/lit8 v9, v9, 0x4

    #@61
    if-eqz v9, :cond_c8

    #@63
    move v1, v8

    #@64
    .line 633
    .local v1, homeAsUp:Z
    :goto_64
    iget-object v9, p0, Lcom/android/internal/widget/ActionBarView;->mTitleUpView:Landroid/view/View;

    #@66
    if-nez v4, :cond_6b

    #@68
    if-eqz v1, :cond_ca

    #@6a
    move v7, v6

    #@6b
    :cond_6b
    :goto_6b
    invoke-virtual {v9, v7}, Landroid/view/View;->setVisibility(I)V

    #@6e
    .line 636
    .end local v1           #homeAsUp:Z
    :cond_6e
    and-int/lit8 v7, v0, 0x10

    #@70
    if-eqz v7, :cond_7f

    #@72
    iget-object v7, p0, Lcom/android/internal/widget/ActionBarView;->mCustomNavView:Landroid/view/View;

    #@74
    if-eqz v7, :cond_7f

    #@76
    .line 637
    and-int/lit8 v7, p1, 0x10

    #@78
    if-eqz v7, :cond_cc

    #@7a
    .line 638
    iget-object v7, p0, Lcom/android/internal/widget/ActionBarView;->mCustomNavView:Landroid/view/View;

    #@7c
    invoke-virtual {p0, v7}, Lcom/android/internal/widget/ActionBarView;->addView(Landroid/view/View;)V

    #@7f
    .line 644
    :cond_7f
    :goto_7f
    iget-object v7, p0, Lcom/android/internal/widget/ActionBarView;->mTitleLayout:Landroid/widget/LinearLayout;

    #@81
    if-eqz v7, :cond_95

    #@83
    and-int/lit8 v7, v0, 0x20

    #@85
    if-eqz v7, :cond_95

    #@87
    .line 646
    and-int/lit8 v7, p1, 0x20

    #@89
    if-eqz v7, :cond_d2

    #@8b
    .line 647
    iget-object v7, p0, Lcom/android/internal/widget/ActionBarView;->mTitleView:Landroid/widget/TextView;

    #@8d
    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setSingleLine(Z)V

    #@90
    .line 648
    iget-object v7, p0, Lcom/android/internal/widget/ActionBarView;->mTitleView:Landroid/widget/TextView;

    #@92
    invoke-virtual {v7, v11}, Landroid/widget/TextView;->setMaxLines(I)V

    #@95
    .line 655
    :cond_95
    :goto_95
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarView;->requestLayout()V

    #@98
    .line 661
    .end local v4           #showHome:Z
    .end local v5           #vis:I
    :goto_98
    iget-object v7, p0, Lcom/android/internal/widget/ActionBarView;->mHomeLayout:Lcom/android/internal/widget/ActionBarView$HomeView;

    #@9a
    invoke-virtual {v7}, Lcom/android/internal/widget/ActionBarView$HomeView;->isEnabled()Z

    #@9d
    move-result v7

    #@9e
    if-nez v7, :cond_e1

    #@a0
    .line 662
    iget-object v6, p0, Lcom/android/internal/widget/ActionBarView;->mHomeLayout:Lcom/android/internal/widget/ActionBarView$HomeView;

    #@a2
    const/4 v7, 0x0

    #@a3
    invoke-virtual {v6, v7}, Lcom/android/internal/widget/ActionBarView$HomeView;->setContentDescription(Ljava/lang/CharSequence;)V

    #@a6
    .line 663
    iget-object v6, p0, Lcom/android/internal/widget/ActionBarView;->mHomeLayout:Lcom/android/internal/widget/ActionBarView$HomeView;

    #@a8
    invoke-virtual {v6, v11}, Lcom/android/internal/widget/ActionBarView$HomeView;->setImportantForAccessibility(I)V

    #@ab
    .line 674
    :goto_ab
    return-void

    #@ac
    .line 596
    .end local v0           #flagsChanged:I
    :cond_ac
    iget v9, p0, Lcom/android/internal/widget/ActionBarView;->mDisplayOptions:I

    #@ae
    xor-int v0, p1, v9

    #@b0
    goto/16 :goto_a

    #@b2
    .restart local v0       #flagsChanged:I
    :cond_b2
    move v4, v6

    #@b3
    .line 600
    goto/16 :goto_15

    #@b5
    .restart local v4       #showHome:Z
    :cond_b5
    move v5, v7

    #@b6
    .line 601
    goto/16 :goto_1c

    #@b8
    .restart local v5       #vis:I
    :cond_b8
    move v3, v6

    #@b9
    .line 605
    goto/16 :goto_2a

    #@bb
    :cond_bb
    move v2, v6

    #@bc
    .line 618
    goto :goto_41

    #@bd
    .line 619
    .restart local v2       #logoVis:Z
    :cond_bd
    iget-object v9, p0, Lcom/android/internal/widget/ActionBarView;->mIcon:Landroid/graphics/drawable/Drawable;

    #@bf
    goto :goto_47

    #@c0
    .line 626
    .end local v2           #logoVis:Z
    :cond_c0
    iget-object v9, p0, Lcom/android/internal/widget/ActionBarView;->mUpGoerFive:Landroid/view/ViewGroup;

    #@c2
    iget-object v10, p0, Lcom/android/internal/widget/ActionBarView;->mTitleLayout:Landroid/widget/LinearLayout;

    #@c4
    invoke-virtual {v9, v10}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    #@c7
    goto :goto_55

    #@c8
    :cond_c8
    move v1, v6

    #@c9
    .line 632
    goto :goto_64

    #@ca
    .line 633
    .restart local v1       #homeAsUp:Z
    :cond_ca
    const/4 v7, 0x4

    #@cb
    goto :goto_6b

    #@cc
    .line 640
    .end local v1           #homeAsUp:Z
    :cond_cc
    iget-object v7, p0, Lcom/android/internal/widget/ActionBarView;->mCustomNavView:Landroid/view/View;

    #@ce
    invoke-virtual {p0, v7}, Lcom/android/internal/widget/ActionBarView;->removeView(Landroid/view/View;)V

    #@d1
    goto :goto_7f

    #@d2
    .line 650
    :cond_d2
    iget-object v7, p0, Lcom/android/internal/widget/ActionBarView;->mTitleView:Landroid/widget/TextView;

    #@d4
    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setMaxLines(I)V

    #@d7
    .line 651
    iget-object v7, p0, Lcom/android/internal/widget/ActionBarView;->mTitleView:Landroid/widget/TextView;

    #@d9
    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setSingleLine(Z)V

    #@dc
    goto :goto_95

    #@dd
    .line 657
    .end local v4           #showHome:Z
    .end local v5           #vis:I
    :cond_dd
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarView;->invalidate()V

    #@e0
    goto :goto_98

    #@e1
    .line 665
    :cond_e1
    iget-object v7, p0, Lcom/android/internal/widget/ActionBarView;->mHomeLayout:Lcom/android/internal/widget/ActionBarView$HomeView;

    #@e3
    invoke-virtual {v7, v6}, Lcom/android/internal/widget/ActionBarView$HomeView;->setImportantForAccessibility(I)V

    #@e6
    .line 666
    and-int/lit8 v6, p1, 0x4

    #@e8
    if-eqz v6, :cond_fd

    #@ea
    .line 667
    iget-object v6, p0, Lcom/android/internal/widget/ActionBarView;->mHomeLayout:Lcom/android/internal/widget/ActionBarView$HomeView;

    #@ec
    iget-object v7, p0, Lcom/android/internal/widget/ActionBarView;->mContext:Landroid/content/Context;

    #@ee
    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@f1
    move-result-object v7

    #@f2
    const v8, 0x104050b

    #@f5
    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@f8
    move-result-object v7

    #@f9
    invoke-virtual {v6, v7}, Lcom/android/internal/widget/ActionBarView$HomeView;->setContentDescription(Ljava/lang/CharSequence;)V

    #@fc
    goto :goto_ab

    #@fd
    .line 670
    :cond_fd
    iget-object v6, p0, Lcom/android/internal/widget/ActionBarView;->mHomeLayout:Lcom/android/internal/widget/ActionBarView$HomeView;

    #@ff
    iget-object v7, p0, Lcom/android/internal/widget/ActionBarView;->mContext:Landroid/content/Context;

    #@101
    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@104
    move-result-object v7

    #@105
    const v8, 0x104050a

    #@108
    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@10b
    move-result-object v7

    #@10c
    invoke-virtual {v6, v7}, Lcom/android/internal/widget/ActionBarView$HomeView;->setContentDescription(Ljava/lang/CharSequence;)V

    #@10f
    goto :goto_ab
.end method

.method public setDropdownAdapter(Landroid/widget/SpinnerAdapter;)V
    .registers 3
    .parameter "adapter"

    #@0
    .prologue
    .line 747
    iput-object p1, p0, Lcom/android/internal/widget/ActionBarView;->mSpinnerAdapter:Landroid/widget/SpinnerAdapter;

    #@2
    .line 748
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarView;->mSpinner:Landroid/widget/Spinner;

    #@4
    if-eqz v0, :cond_b

    #@6
    .line 749
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarView;->mSpinner:Landroid/widget/Spinner;

    #@8
    invoke-virtual {v0, p1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    #@b
    .line 751
    :cond_b
    return-void
.end method

.method public setDropdownSelectedPosition(I)V
    .registers 3
    .parameter "position"

    #@0
    .prologue
    .line 758
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarView;->mSpinner:Landroid/widget/Spinner;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/Spinner;->setSelection(I)V

    #@5
    .line 759
    return-void
.end method

.method public setEmbeddedTabView(Lcom/android/internal/widget/ScrollingTabContainerView;)V
    .registers 6
    .parameter "tabs"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 387
    iget-object v1, p0, Lcom/android/internal/widget/ActionBarView;->mTabScrollView:Lcom/android/internal/widget/ScrollingTabContainerView;

    #@3
    if-eqz v1, :cond_a

    #@5
    .line 388
    iget-object v1, p0, Lcom/android/internal/widget/ActionBarView;->mTabScrollView:Lcom/android/internal/widget/ScrollingTabContainerView;

    #@7
    invoke-virtual {p0, v1}, Lcom/android/internal/widget/ActionBarView;->removeView(Landroid/view/View;)V

    #@a
    .line 390
    :cond_a
    iput-object p1, p0, Lcom/android/internal/widget/ActionBarView;->mTabScrollView:Lcom/android/internal/widget/ScrollingTabContainerView;

    #@c
    .line 391
    if-eqz p1, :cond_2f

    #@e
    move v1, v2

    #@f
    :goto_f
    iput-boolean v1, p0, Lcom/android/internal/widget/ActionBarView;->mIncludeTabs:Z

    #@11
    .line 392
    iget-boolean v1, p0, Lcom/android/internal/widget/ActionBarView;->mIncludeTabs:Z

    #@13
    if-eqz v1, :cond_2e

    #@15
    iget v1, p0, Lcom/android/internal/widget/ActionBarView;->mNavigationMode:I

    #@17
    const/4 v3, 0x2

    #@18
    if-ne v1, v3, :cond_2e

    #@1a
    .line 393
    iget-object v1, p0, Lcom/android/internal/widget/ActionBarView;->mTabScrollView:Lcom/android/internal/widget/ScrollingTabContainerView;

    #@1c
    invoke-virtual {p0, v1}, Lcom/android/internal/widget/ActionBarView;->addView(Landroid/view/View;)V

    #@1f
    .line 394
    iget-object v1, p0, Lcom/android/internal/widget/ActionBarView;->mTabScrollView:Lcom/android/internal/widget/ScrollingTabContainerView;

    #@21
    invoke-virtual {v1}, Lcom/android/internal/widget/ScrollingTabContainerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@24
    move-result-object v0

    #@25
    .line 395
    .local v0, lp:Landroid/view/ViewGroup$LayoutParams;
    const/4 v1, -0x2

    #@26
    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@28
    .line 396
    const/4 v1, -0x1

    #@29
    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@2b
    .line 397
    invoke-virtual {p1, v2}, Lcom/android/internal/widget/ScrollingTabContainerView;->setAllowCollapse(Z)V

    #@2e
    .line 399
    .end local v0           #lp:Landroid/view/ViewGroup$LayoutParams;
    :cond_2e
    return-void

    #@2f
    .line 391
    :cond_2f
    const/4 v1, 0x0

    #@30
    goto :goto_f
.end method

.method public setHomeButtonEnabled(Z)V
    .registers 3
    .parameter "enable"

    #@0
    .prologue
    .line 562
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, p1, v0}, Lcom/android/internal/widget/ActionBarView;->setHomeButtonEnabled(ZZ)V

    #@4
    .line 563
    return-void
.end method

.method public setIcon(I)V
    .registers 3
    .parameter "resId"

    #@0
    .prologue
    .line 688
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarView;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@9
    move-result-object v0

    #@a
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/ActionBarView;->setIcon(Landroid/graphics/drawable/Drawable;)V

    #@d
    .line 689
    return-void
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)V
    .registers 5
    .parameter "icon"

    #@0
    .prologue
    .line 677
    iput-object p1, p0, Lcom/android/internal/widget/ActionBarView;->mIcon:Landroid/graphics/drawable/Drawable;

    #@2
    .line 678
    if-eqz p1, :cond_13

    #@4
    iget v0, p0, Lcom/android/internal/widget/ActionBarView;->mDisplayOptions:I

    #@6
    and-int/lit8 v0, v0, 0x1

    #@8
    if-eqz v0, :cond_e

    #@a
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarView;->mLogo:Landroid/graphics/drawable/Drawable;

    #@c
    if-nez v0, :cond_13

    #@e
    .line 680
    :cond_e
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarView;->mHomeLayout:Lcom/android/internal/widget/ActionBarView$HomeView;

    #@10
    invoke-virtual {v0, p1}, Lcom/android/internal/widget/ActionBarView$HomeView;->setIcon(Landroid/graphics/drawable/Drawable;)V

    #@13
    .line 682
    :cond_13
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarView;->mExpandedActionView:Landroid/view/View;

    #@15
    if-eqz v0, :cond_2a

    #@17
    .line 683
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarView;->mExpandedHomeLayout:Lcom/android/internal/widget/ActionBarView$HomeView;

    #@19
    iget-object v1, p0, Lcom/android/internal/widget/ActionBarView;->mIcon:Landroid/graphics/drawable/Drawable;

    #@1b
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarView;->getResources()Landroid/content/res/Resources;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    #@26
    move-result-object v1

    #@27
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/ActionBarView$HomeView;->setIcon(Landroid/graphics/drawable/Drawable;)V

    #@2a
    .line 685
    :cond_2a
    return-void
.end method

.method public setLogo(I)V
    .registers 3
    .parameter "resId"

    #@0
    .prologue
    .line 699
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarView;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@9
    move-result-object v0

    #@a
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/ActionBarView;->setLogo(Landroid/graphics/drawable/Drawable;)V

    #@d
    .line 700
    return-void
.end method

.method public setLogo(Landroid/graphics/drawable/Drawable;)V
    .registers 3
    .parameter "logo"

    #@0
    .prologue
    .line 692
    iput-object p1, p0, Lcom/android/internal/widget/ActionBarView;->mLogo:Landroid/graphics/drawable/Drawable;

    #@2
    .line 693
    if-eqz p1, :cond_f

    #@4
    iget v0, p0, Lcom/android/internal/widget/ActionBarView;->mDisplayOptions:I

    #@6
    and-int/lit8 v0, v0, 0x1

    #@8
    if-eqz v0, :cond_f

    #@a
    .line 694
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarView;->mHomeLayout:Lcom/android/internal/widget/ActionBarView$HomeView;

    #@c
    invoke-virtual {v0, p1}, Lcom/android/internal/widget/ActionBarView$HomeView;->setIcon(Landroid/graphics/drawable/Drawable;)V

    #@f
    .line 696
    :cond_f
    return-void
.end method

.method public setMenu(Landroid/view/Menu;Lcom/android/internal/view/menu/MenuPresenter$Callback;)V
    .registers 11
    .parameter "menu"
    .parameter "cb"

    #@0
    .prologue
    const/4 v7, -0x1

    #@1
    .line 406
    iget-object v4, p0, Lcom/android/internal/widget/ActionBarView;->mOptionsMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@3
    if-ne p1, v4, :cond_6

    #@5
    .line 466
    :goto_5
    return-void

    #@6
    .line 408
    :cond_6
    iget-object v4, p0, Lcom/android/internal/widget/ActionBarView;->mOptionsMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@8
    if-eqz v4, :cond_18

    #@a
    .line 409
    iget-object v4, p0, Lcom/android/internal/widget/ActionBarView;->mOptionsMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@c
    iget-object v5, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@e
    invoke-virtual {v4, v5}, Lcom/android/internal/view/menu/MenuBuilder;->removeMenuPresenter(Lcom/android/internal/view/menu/MenuPresenter;)V

    #@11
    .line 410
    iget-object v4, p0, Lcom/android/internal/widget/ActionBarView;->mOptionsMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@13
    iget-object v5, p0, Lcom/android/internal/widget/ActionBarView;->mExpandedMenuPresenter:Lcom/android/internal/widget/ActionBarView$ExpandedActionViewMenuPresenter;

    #@15
    invoke-virtual {v4, v5}, Lcom/android/internal/view/menu/MenuBuilder;->removeMenuPresenter(Lcom/android/internal/view/menu/MenuPresenter;)V

    #@18
    :cond_18
    move-object v0, p1

    #@19
    .line 413
    check-cast v0, Lcom/android/internal/view/menu/MenuBuilder;

    #@1b
    .line 414
    .local v0, builder:Lcom/android/internal/view/menu/MenuBuilder;
    iput-object v0, p0, Lcom/android/internal/widget/ActionBarView;->mOptionsMenu:Lcom/android/internal/view/menu/MenuBuilder;

    #@1d
    .line 415
    iget-object v4, p0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@1f
    if-eqz v4, :cond_30

    #@21
    .line 416
    iget-object v4, p0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@23
    invoke-virtual {v4}, Lcom/android/internal/view/menu/ActionMenuView;->getParent()Landroid/view/ViewParent;

    #@26
    move-result-object v3

    #@27
    check-cast v3, Landroid/view/ViewGroup;

    #@29
    .line 417
    .local v3, oldParent:Landroid/view/ViewGroup;
    if-eqz v3, :cond_30

    #@2b
    .line 418
    iget-object v4, p0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@2d
    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    #@30
    .line 421
    .end local v3           #oldParent:Landroid/view/ViewGroup;
    :cond_30
    iget-object v4, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@32
    if-nez v4, :cond_52

    #@34
    .line 422
    new-instance v4, Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@36
    iget-object v5, p0, Lcom/android/internal/widget/ActionBarView;->mContext:Landroid/content/Context;

    #@38
    invoke-direct {v4, v5}, Lcom/android/internal/view/menu/ActionMenuPresenter;-><init>(Landroid/content/Context;)V

    #@3b
    iput-object v4, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@3d
    .line 423
    iget-object v4, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@3f
    invoke-virtual {v4, p2}, Lcom/android/internal/view/menu/ActionMenuPresenter;->setCallback(Lcom/android/internal/view/menu/MenuPresenter$Callback;)V

    #@42
    .line 424
    iget-object v4, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@44
    const v5, 0x1020258

    #@47
    invoke-virtual {v4, v5}, Lcom/android/internal/view/menu/ActionMenuPresenter;->setId(I)V

    #@4a
    .line 425
    new-instance v4, Lcom/android/internal/widget/ActionBarView$ExpandedActionViewMenuPresenter;

    #@4c
    const/4 v5, 0x0

    #@4d
    invoke-direct {v4, p0, v5}, Lcom/android/internal/widget/ActionBarView$ExpandedActionViewMenuPresenter;-><init>(Lcom/android/internal/widget/ActionBarView;Lcom/android/internal/widget/ActionBarView$1;)V

    #@50
    iput-object v4, p0, Lcom/android/internal/widget/ActionBarView;->mExpandedMenuPresenter:Lcom/android/internal/widget/ActionBarView$ExpandedActionViewMenuPresenter;

    #@52
    .line 429
    :cond_52
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    #@54
    const/4 v4, -0x2

    #@55
    invoke-direct {v1, v4, v7}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    #@58
    .line 431
    .local v1, layoutParams:Landroid/view/ViewGroup$LayoutParams;
    iget-boolean v4, p0, Lcom/android/internal/widget/AbsActionBarView;->mSplitActionBar:Z

    #@5a
    if-nez v4, :cond_8b

    #@5c
    .line 432
    iget-object v4, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@5e
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarView;->getResources()Landroid/content/res/Resources;

    #@61
    move-result-object v5

    #@62
    const v6, 0x1110009

    #@65
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@68
    move-result v5

    #@69
    invoke-virtual {v4, v5}, Lcom/android/internal/view/menu/ActionMenuPresenter;->setExpandedActionViewsExclusive(Z)V

    #@6c
    .line 435
    invoke-direct {p0, v0}, Lcom/android/internal/widget/ActionBarView;->configPresenters(Lcom/android/internal/view/menu/MenuBuilder;)V

    #@6f
    .line 436
    iget-object v4, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@71
    invoke-virtual {v4, p0}, Lcom/android/internal/view/menu/ActionMenuPresenter;->getMenuView(Landroid/view/ViewGroup;)Lcom/android/internal/view/menu/MenuView;

    #@74
    move-result-object v2

    #@75
    check-cast v2, Lcom/android/internal/view/menu/ActionMenuView;

    #@77
    .line 437
    .local v2, menuView:Lcom/android/internal/view/menu/ActionMenuView;
    invoke-virtual {v2}, Lcom/android/internal/view/menu/ActionMenuView;->getParent()Landroid/view/ViewParent;

    #@7a
    move-result-object v3

    #@7b
    check-cast v3, Landroid/view/ViewGroup;

    #@7d
    .line 438
    .restart local v3       #oldParent:Landroid/view/ViewGroup;
    if-eqz v3, :cond_84

    #@7f
    if-eq v3, p0, :cond_84

    #@81
    .line 439
    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    #@84
    .line 441
    :cond_84
    invoke-virtual {p0, v2, v1}, Lcom/android/internal/widget/ActionBarView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@87
    .line 465
    .end local v3           #oldParent:Landroid/view/ViewGroup;
    :goto_87
    iput-object v2, p0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@89
    goto/16 :goto_5

    #@8b
    .line 443
    .end local v2           #menuView:Lcom/android/internal/view/menu/ActionMenuView;
    :cond_8b
    iget-object v4, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@8d
    const/4 v5, 0x0

    #@8e
    invoke-virtual {v4, v5}, Lcom/android/internal/view/menu/ActionMenuPresenter;->setExpandedActionViewsExclusive(Z)V

    #@91
    .line 445
    iget-object v4, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@93
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarView;->getContext()Landroid/content/Context;

    #@96
    move-result-object v5

    #@97
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@9a
    move-result-object v5

    #@9b
    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@9e
    move-result-object v5

    #@9f
    iget v5, v5, Landroid/util/DisplayMetrics;->widthPixels:I

    #@a1
    const/4 v6, 0x1

    #@a2
    invoke-virtual {v4, v5, v6}, Lcom/android/internal/view/menu/ActionMenuPresenter;->setWidthLimit(IZ)V

    #@a5
    .line 448
    iget-object v4, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@a7
    const v5, 0x7fffffff

    #@aa
    invoke-virtual {v4, v5}, Lcom/android/internal/view/menu/ActionMenuPresenter;->setItemLimit(I)V

    #@ad
    .line 450
    iput v7, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@af
    .line 451
    invoke-direct {p0, v0}, Lcom/android/internal/widget/ActionBarView;->configPresenters(Lcom/android/internal/view/menu/MenuBuilder;)V

    #@b2
    .line 452
    iget-object v4, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@b4
    invoke-virtual {v4, p0}, Lcom/android/internal/view/menu/ActionMenuPresenter;->getMenuView(Landroid/view/ViewGroup;)Lcom/android/internal/view/menu/MenuView;

    #@b7
    move-result-object v2

    #@b8
    check-cast v2, Lcom/android/internal/view/menu/ActionMenuView;

    #@ba
    .line 453
    .restart local v2       #menuView:Lcom/android/internal/view/menu/ActionMenuView;
    iget-object v4, p0, Lcom/android/internal/widget/AbsActionBarView;->mSplitView:Lcom/android/internal/widget/ActionBarContainer;

    #@bc
    if-eqz v4, :cond_da

    #@be
    .line 454
    invoke-virtual {v2}, Lcom/android/internal/view/menu/ActionMenuView;->getParent()Landroid/view/ViewParent;

    #@c1
    move-result-object v3

    #@c2
    check-cast v3, Landroid/view/ViewGroup;

    #@c4
    .line 455
    .restart local v3       #oldParent:Landroid/view/ViewGroup;
    if-eqz v3, :cond_cd

    #@c6
    iget-object v4, p0, Lcom/android/internal/widget/AbsActionBarView;->mSplitView:Lcom/android/internal/widget/ActionBarContainer;

    #@c8
    if-eq v3, v4, :cond_cd

    #@ca
    .line 456
    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    #@cd
    .line 458
    :cond_cd
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarView;->getAnimatedVisibility()I

    #@d0
    move-result v4

    #@d1
    invoke-virtual {v2, v4}, Lcom/android/internal/view/menu/ActionMenuView;->setVisibility(I)V

    #@d4
    .line 459
    iget-object v4, p0, Lcom/android/internal/widget/AbsActionBarView;->mSplitView:Lcom/android/internal/widget/ActionBarContainer;

    #@d6
    invoke-virtual {v4, v2, v1}, Lcom/android/internal/widget/ActionBarContainer;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@d9
    goto :goto_87

    #@da
    .line 462
    .end local v3           #oldParent:Landroid/view/ViewGroup;
    :cond_da
    invoke-virtual {v2, v1}, Lcom/android/internal/view/menu/ActionMenuView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@dd
    goto :goto_87
.end method

.method public setNavigationMode(I)V
    .registers 8
    .parameter "mode"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 703
    iget v0, p0, Lcom/android/internal/widget/ActionBarView;->mNavigationMode:I

    #@3
    .line 704
    .local v0, oldMode:I
    if-eq p1, v0, :cond_10

    #@5
    .line 705
    packed-switch v0, :pswitch_data_84

    #@8
    .line 717
    :cond_8
    :goto_8
    packed-switch p1, :pswitch_data_8c

    #@b
    .line 741
    :cond_b
    :goto_b
    iput p1, p0, Lcom/android/internal/widget/ActionBarView;->mNavigationMode:I

    #@d
    .line 742
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarView;->requestLayout()V

    #@10
    .line 744
    :cond_10
    return-void

    #@11
    .line 707
    :pswitch_11
    iget-object v2, p0, Lcom/android/internal/widget/ActionBarView;->mListNavLayout:Landroid/widget/LinearLayout;

    #@13
    if-eqz v2, :cond_8

    #@15
    .line 708
    iget-object v2, p0, Lcom/android/internal/widget/ActionBarView;->mListNavLayout:Landroid/widget/LinearLayout;

    #@17
    invoke-virtual {p0, v2}, Lcom/android/internal/widget/ActionBarView;->removeView(Landroid/view/View;)V

    #@1a
    goto :goto_8

    #@1b
    .line 712
    :pswitch_1b
    iget-object v2, p0, Lcom/android/internal/widget/ActionBarView;->mTabScrollView:Lcom/android/internal/widget/ScrollingTabContainerView;

    #@1d
    if-eqz v2, :cond_8

    #@1f
    iget-boolean v2, p0, Lcom/android/internal/widget/ActionBarView;->mIncludeTabs:Z

    #@21
    if-eqz v2, :cond_8

    #@23
    .line 713
    iget-object v2, p0, Lcom/android/internal/widget/ActionBarView;->mTabScrollView:Lcom/android/internal/widget/ScrollingTabContainerView;

    #@25
    invoke-virtual {p0, v2}, Lcom/android/internal/widget/ActionBarView;->removeView(Landroid/view/View;)V

    #@28
    goto :goto_8

    #@29
    .line 719
    :pswitch_29
    iget-object v2, p0, Lcom/android/internal/widget/ActionBarView;->mSpinner:Landroid/widget/Spinner;

    #@2b
    if-nez v2, :cond_57

    #@2d
    .line 720
    new-instance v2, Landroid/widget/Spinner;

    #@2f
    iget-object v3, p0, Lcom/android/internal/widget/ActionBarView;->mContext:Landroid/content/Context;

    #@31
    const v4, 0x10102d7

    #@34
    invoke-direct {v2, v3, v5, v4}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@37
    iput-object v2, p0, Lcom/android/internal/widget/ActionBarView;->mSpinner:Landroid/widget/Spinner;

    #@39
    .line 722
    new-instance v2, Landroid/widget/LinearLayout;

    #@3b
    iget-object v3, p0, Lcom/android/internal/widget/ActionBarView;->mContext:Landroid/content/Context;

    #@3d
    const v4, 0x10102f4

    #@40
    invoke-direct {v2, v3, v5, v4}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@43
    iput-object v2, p0, Lcom/android/internal/widget/ActionBarView;->mListNavLayout:Landroid/widget/LinearLayout;

    #@45
    .line 724
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    #@47
    const/4 v2, -0x2

    #@48
    const/4 v3, -0x1

    #@49
    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    #@4c
    .line 726
    .local v1, params:Landroid/widget/LinearLayout$LayoutParams;
    const/16 v2, 0x11

    #@4e
    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    #@50
    .line 727
    iget-object v2, p0, Lcom/android/internal/widget/ActionBarView;->mListNavLayout:Landroid/widget/LinearLayout;

    #@52
    iget-object v3, p0, Lcom/android/internal/widget/ActionBarView;->mSpinner:Landroid/widget/Spinner;

    #@54
    invoke-virtual {v2, v3, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@57
    .line 729
    .end local v1           #params:Landroid/widget/LinearLayout$LayoutParams;
    :cond_57
    iget-object v2, p0, Lcom/android/internal/widget/ActionBarView;->mSpinner:Landroid/widget/Spinner;

    #@59
    invoke-virtual {v2}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    #@5c
    move-result-object v2

    #@5d
    iget-object v3, p0, Lcom/android/internal/widget/ActionBarView;->mSpinnerAdapter:Landroid/widget/SpinnerAdapter;

    #@5f
    if-eq v2, v3, :cond_68

    #@61
    .line 730
    iget-object v2, p0, Lcom/android/internal/widget/ActionBarView;->mSpinner:Landroid/widget/Spinner;

    #@63
    iget-object v3, p0, Lcom/android/internal/widget/ActionBarView;->mSpinnerAdapter:Landroid/widget/SpinnerAdapter;

    #@65
    invoke-virtual {v2, v3}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    #@68
    .line 732
    :cond_68
    iget-object v2, p0, Lcom/android/internal/widget/ActionBarView;->mSpinner:Landroid/widget/Spinner;

    #@6a
    iget-object v3, p0, Lcom/android/internal/widget/ActionBarView;->mNavItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    #@6c
    invoke-virtual {v2, v3}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    #@6f
    .line 733
    iget-object v2, p0, Lcom/android/internal/widget/ActionBarView;->mListNavLayout:Landroid/widget/LinearLayout;

    #@71
    invoke-virtual {p0, v2}, Lcom/android/internal/widget/ActionBarView;->addView(Landroid/view/View;)V

    #@74
    goto :goto_b

    #@75
    .line 736
    :pswitch_75
    iget-object v2, p0, Lcom/android/internal/widget/ActionBarView;->mTabScrollView:Lcom/android/internal/widget/ScrollingTabContainerView;

    #@77
    if-eqz v2, :cond_b

    #@79
    iget-boolean v2, p0, Lcom/android/internal/widget/ActionBarView;->mIncludeTabs:Z

    #@7b
    if-eqz v2, :cond_b

    #@7d
    .line 737
    iget-object v2, p0, Lcom/android/internal/widget/ActionBarView;->mTabScrollView:Lcom/android/internal/widget/ScrollingTabContainerView;

    #@7f
    invoke-virtual {p0, v2}, Lcom/android/internal/widget/ActionBarView;->addView(Landroid/view/View;)V

    #@82
    goto :goto_b

    #@83
    .line 705
    nop

    #@84
    :pswitch_data_84
    .packed-switch 0x1
        :pswitch_11
        :pswitch_1b
    .end packed-switch

    #@8c
    .line 717
    :pswitch_data_8c
    .packed-switch 0x1
        :pswitch_29
        :pswitch_75
    .end packed-switch
.end method

.method public setSplitActionBar(Z)V
    .registers 6
    .parameter "splitActionBar"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 339
    iget-boolean v1, p0, Lcom/android/internal/widget/AbsActionBarView;->mSplitActionBar:Z

    #@3
    if-eq v1, p1, :cond_58

    #@5
    .line 340
    iget-object v1, p0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@7
    if-eqz v1, :cond_33

    #@9
    .line 341
    iget-object v1, p0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@b
    invoke-virtual {v1}, Lcom/android/internal/view/menu/ActionMenuView;->getParent()Landroid/view/ViewParent;

    #@e
    move-result-object v0

    #@f
    check-cast v0, Landroid/view/ViewGroup;

    #@11
    .line 342
    .local v0, oldParent:Landroid/view/ViewGroup;
    if-eqz v0, :cond_18

    #@13
    .line 343
    iget-object v1, p0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@15
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    #@18
    .line 345
    :cond_18
    if-eqz p1, :cond_59

    #@1a
    .line 346
    iget-object v1, p0, Lcom/android/internal/widget/AbsActionBarView;->mSplitView:Lcom/android/internal/widget/ActionBarContainer;

    #@1c
    if-eqz v1, :cond_25

    #@1e
    .line 347
    iget-object v1, p0, Lcom/android/internal/widget/AbsActionBarView;->mSplitView:Lcom/android/internal/widget/ActionBarContainer;

    #@20
    iget-object v3, p0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@22
    invoke-virtual {v1, v3}, Lcom/android/internal/widget/ActionBarContainer;->addView(Landroid/view/View;)V

    #@25
    .line 349
    :cond_25
    iget-object v1, p0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@27
    invoke-virtual {v1}, Lcom/android/internal/view/menu/ActionMenuView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@2a
    move-result-object v1

    #@2b
    const/4 v3, -0x1

    #@2c
    iput v3, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@2e
    .line 354
    :goto_2e
    iget-object v1, p0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@30
    invoke-virtual {v1}, Lcom/android/internal/view/menu/ActionMenuView;->requestLayout()V

    #@33
    .line 356
    .end local v0           #oldParent:Landroid/view/ViewGroup;
    :cond_33
    iget-object v1, p0, Lcom/android/internal/widget/AbsActionBarView;->mSplitView:Lcom/android/internal/widget/ActionBarContainer;

    #@35
    if-eqz v1, :cond_3f

    #@37
    .line 357
    iget-object v3, p0, Lcom/android/internal/widget/AbsActionBarView;->mSplitView:Lcom/android/internal/widget/ActionBarContainer;

    #@39
    if-eqz p1, :cond_68

    #@3b
    move v1, v2

    #@3c
    :goto_3c
    invoke-virtual {v3, v1}, Lcom/android/internal/widget/ActionBarContainer;->setVisibility(I)V

    #@3f
    .line 360
    :cond_3f
    iget-object v1, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@41
    if-eqz v1, :cond_55

    #@43
    .line 361
    if-nez p1, :cond_6b

    #@45
    .line 362
    iget-object v1, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@47
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarView;->getResources()Landroid/content/res/Resources;

    #@4a
    move-result-object v2

    #@4b
    const v3, 0x1110009

    #@4e
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@51
    move-result v2

    #@52
    invoke-virtual {v1, v2}, Lcom/android/internal/view/menu/ActionMenuPresenter;->setExpandedActionViewsExclusive(Z)V

    #@55
    .line 374
    :cond_55
    :goto_55
    invoke-super {p0, p1}, Lcom/android/internal/widget/AbsActionBarView;->setSplitActionBar(Z)V

    #@58
    .line 376
    :cond_58
    return-void

    #@59
    .line 351
    .restart local v0       #oldParent:Landroid/view/ViewGroup;
    :cond_59
    iget-object v1, p0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@5b
    invoke-virtual {p0, v1}, Lcom/android/internal/widget/ActionBarView;->addView(Landroid/view/View;)V

    #@5e
    .line 352
    iget-object v1, p0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@60
    invoke-virtual {v1}, Lcom/android/internal/view/menu/ActionMenuView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@63
    move-result-object v1

    #@64
    const/4 v3, -0x2

    #@65
    iput v3, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@67
    goto :goto_2e

    #@68
    .line 357
    .end local v0           #oldParent:Landroid/view/ViewGroup;
    :cond_68
    const/16 v1, 0x8

    #@6a
    goto :goto_3c

    #@6b
    .line 366
    :cond_6b
    iget-object v1, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@6d
    invoke-virtual {v1, v2}, Lcom/android/internal/view/menu/ActionMenuPresenter;->setExpandedActionViewsExclusive(Z)V

    #@70
    .line 368
    iget-object v1, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@72
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarView;->getContext()Landroid/content/Context;

    #@75
    move-result-object v2

    #@76
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@79
    move-result-object v2

    #@7a
    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@7d
    move-result-object v2

    #@7e
    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    #@80
    const/4 v3, 0x1

    #@81
    invoke-virtual {v1, v2, v3}, Lcom/android/internal/view/menu/ActionMenuPresenter;->setWidthLimit(IZ)V

    #@84
    .line 371
    iget-object v1, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@86
    const v2, 0x7fffffff

    #@89
    invoke-virtual {v1, v2}, Lcom/android/internal/view/menu/ActionMenuPresenter;->setItemLimit(I)V

    #@8c
    goto :goto_55
.end method

.method public setSubtitle(Ljava/lang/CharSequence;)V
    .registers 7
    .parameter "subtitle"

    #@0
    .prologue
    const/16 v3, 0x8

    #@2
    const/4 v2, 0x0

    #@3
    .line 550
    iput-object p1, p0, Lcom/android/internal/widget/ActionBarView;->mSubtitle:Ljava/lang/CharSequence;

    #@5
    .line 551
    iget-object v1, p0, Lcom/android/internal/widget/ActionBarView;->mSubtitleView:Landroid/widget/TextView;

    #@7
    if-eqz v1, :cond_38

    #@9
    .line 552
    iget-object v1, p0, Lcom/android/internal/widget/ActionBarView;->mSubtitleView:Landroid/widget/TextView;

    #@b
    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@e
    .line 553
    iget-object v4, p0, Lcom/android/internal/widget/ActionBarView;->mSubtitleView:Landroid/widget/TextView;

    #@10
    if-eqz p1, :cond_39

    #@12
    move v1, v2

    #@13
    :goto_13
    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setVisibility(I)V

    #@16
    .line 554
    iget-object v1, p0, Lcom/android/internal/widget/ActionBarView;->mExpandedActionView:Landroid/view/View;

    #@18
    if-nez v1, :cond_3b

    #@1a
    iget v1, p0, Lcom/android/internal/widget/ActionBarView;->mDisplayOptions:I

    #@1c
    and-int/lit8 v1, v1, 0x8

    #@1e
    if-eqz v1, :cond_3b

    #@20
    iget-object v1, p0, Lcom/android/internal/widget/ActionBarView;->mTitle:Ljava/lang/CharSequence;

    #@22
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@25
    move-result v1

    #@26
    if-eqz v1, :cond_30

    #@28
    iget-object v1, p0, Lcom/android/internal/widget/ActionBarView;->mSubtitle:Ljava/lang/CharSequence;

    #@2a
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@2d
    move-result v1

    #@2e
    if-nez v1, :cond_3b

    #@30
    :cond_30
    const/4 v0, 0x1

    #@31
    .line 557
    .local v0, visible:Z
    :goto_31
    iget-object v1, p0, Lcom/android/internal/widget/ActionBarView;->mTitleLayout:Landroid/widget/LinearLayout;

    #@33
    if-eqz v0, :cond_3d

    #@35
    :goto_35
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    #@38
    .line 559
    .end local v0           #visible:Z
    :cond_38
    return-void

    #@39
    :cond_39
    move v1, v3

    #@3a
    .line 553
    goto :goto_13

    #@3b
    :cond_3b
    move v0, v2

    #@3c
    .line 554
    goto :goto_31

    #@3d
    .restart local v0       #visible:Z
    :cond_3d
    move v2, v3

    #@3e
    .line 557
    goto :goto_35
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .registers 3
    .parameter "title"

    #@0
    .prologue
    .line 515
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Lcom/android/internal/widget/ActionBarView;->mUserTitle:Z

    #@3
    .line 516
    invoke-direct {p0, p1}, Lcom/android/internal/widget/ActionBarView;->setTitleImpl(Ljava/lang/CharSequence;)V

    #@6
    .line 517
    return-void
.end method

.method public setWindowCallback(Landroid/view/Window$Callback;)V
    .registers 2
    .parameter "cb"

    #@0
    .prologue
    .line 303
    iput-object p1, p0, Lcom/android/internal/widget/ActionBarView;->mWindowCallback:Landroid/view/Window$Callback;

    #@2
    .line 304
    return-void
.end method

.method public setWindowTitle(Ljava/lang/CharSequence;)V
    .registers 3
    .parameter "title"

    #@0
    .prologue
    .line 526
    iget-boolean v0, p0, Lcom/android/internal/widget/ActionBarView;->mUserTitle:Z

    #@2
    if-nez v0, :cond_7

    #@4
    .line 527
    invoke-direct {p0, p1}, Lcom/android/internal/widget/ActionBarView;->setTitleImpl(Ljava/lang/CharSequence;)V

    #@7
    .line 529
    :cond_7
    return-void
.end method

.method public shouldDelayChildPressedState()Z
    .registers 2

    #@0
    .prologue
    .line 318
    const/4 v0, 0x0

    #@1
    return v0
.end method
