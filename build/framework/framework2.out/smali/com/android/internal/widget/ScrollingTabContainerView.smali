.class public Lcom/android/internal/widget/ScrollingTabContainerView;
.super Landroid/widget/HorizontalScrollView;
.source "ScrollingTabContainerView.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/widget/ScrollingTabContainerView$VisibilityAnimListener;,
        Lcom/android/internal/widget/ScrollingTabContainerView$TabClickListener;,
        Lcom/android/internal/widget/ScrollingTabContainerView$TabAdapter;,
        Lcom/android/internal/widget/ScrollingTabContainerView$TabView;
    }
.end annotation


# static fields
.field private static final FADE_DURATION:I = 0xc8

.field private static final TAG:Ljava/lang/String; = "ScrollingTabContainerView"

.field private static final sAlphaInterpolator:Landroid/animation/TimeInterpolator;


# instance fields
.field protected mAllowCollapse:Z

.field private mContentHeight:I

.field mMaxTabWidth:I

.field private mSelectedTabIndex:I

.field mStackedTabMaxWidth:I

.field private mTabClickListener:Lcom/android/internal/widget/ScrollingTabContainerView$TabClickListener;

.field protected mTabLayout:Landroid/widget/LinearLayout;

.field mTabSelector:Ljava/lang/Runnable;

.field protected mTabSpinner:Landroid/widget/Spinner;

.field protected final mVisAnimListener:Lcom/android/internal/widget/ScrollingTabContainerView$VisibilityAnimListener;

.field protected mVisibilityAnim:Landroid/animation/Animator;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 72
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    #@2
    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    #@5
    sput-object v0, Lcom/android/internal/widget/ScrollingTabContainerView;->sAlphaInterpolator:Landroid/animation/TimeInterpolator;

    #@7
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 7
    .parameter "context"

    #@0
    .prologue
    .line 77
    invoke-direct {p0, p1}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;)V

    #@3
    .line 70
    new-instance v1, Lcom/android/internal/widget/ScrollingTabContainerView$VisibilityAnimListener;

    #@5
    invoke-direct {v1, p0}, Lcom/android/internal/widget/ScrollingTabContainerView$VisibilityAnimListener;-><init>(Lcom/android/internal/widget/ScrollingTabContainerView;)V

    #@8
    iput-object v1, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mVisAnimListener:Lcom/android/internal/widget/ScrollingTabContainerView$VisibilityAnimListener;

    #@a
    .line 78
    const/4 v1, 0x0

    #@b
    invoke-virtual {p0, v1}, Lcom/android/internal/widget/ScrollingTabContainerView;->setHorizontalScrollBarEnabled(Z)V

    #@e
    .line 80
    invoke-static {p1}, Lcom/android/internal/view/ActionBarPolicy;->get(Landroid/content/Context;)Lcom/android/internal/view/ActionBarPolicy;

    #@11
    move-result-object v0

    #@12
    .line 81
    .local v0, abp:Lcom/android/internal/view/ActionBarPolicy;
    invoke-virtual {v0}, Lcom/android/internal/view/ActionBarPolicy;->getTabContainerHeight()I

    #@15
    move-result v1

    #@16
    invoke-virtual {p0, v1}, Lcom/android/internal/widget/ScrollingTabContainerView;->setContentHeight(I)V

    #@19
    .line 82
    invoke-virtual {v0}, Lcom/android/internal/view/ActionBarPolicy;->getStackedTabMaxWidth()I

    #@1c
    move-result v1

    #@1d
    iput v1, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mStackedTabMaxWidth:I

    #@1f
    .line 84
    invoke-direct {p0}, Lcom/android/internal/widget/ScrollingTabContainerView;->createTabLayout()Landroid/widget/LinearLayout;

    #@22
    move-result-object v1

    #@23
    iput-object v1, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabLayout:Landroid/widget/LinearLayout;

    #@25
    .line 85
    iget-object v1, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabLayout:Landroid/widget/LinearLayout;

    #@27
    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    #@29
    const/4 v3, -0x2

    #@2a
    const/4 v4, -0x1

    #@2b
    invoke-direct {v2, v3, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    #@2e
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/widget/ScrollingTabContainerView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@31
    .line 87
    return-void
.end method

.method static synthetic access$200(Lcom/android/internal/widget/ScrollingTabContainerView;Landroid/app/ActionBar$Tab;Z)Lcom/android/internal/widget/ScrollingTabContainerView$TabView;
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Lcom/android/internal/widget/ScrollingTabContainerView;->createTabView(Landroid/app/ActionBar$Tab;Z)Lcom/android/internal/widget/ScrollingTabContainerView$TabView;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method private createSpinner()Landroid/widget/Spinner;
    .registers 5

    #@0
    .prologue
    .line 214
    new-instance v0, Landroid/widget/Spinner;

    #@2
    invoke-virtual {p0}, Lcom/android/internal/widget/ScrollingTabContainerView;->getContext()Landroid/content/Context;

    #@5
    move-result-object v1

    #@6
    const/4 v2, 0x0

    #@7
    const v3, 0x10102d7

    #@a
    invoke-direct {v0, v1, v2, v3}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@d
    .line 216
    .local v0, spinner:Landroid/widget/Spinner;
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    #@f
    const/4 v2, -0x2

    #@10
    const/4 v3, -0x1

    #@11
    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    #@14
    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@17
    .line 218
    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemClickListenerInt(Landroid/widget/AdapterView$OnItemClickListener;)V

    #@1a
    .line 219
    return-object v0
.end method

.method private createTabLayout()Landroid/widget/LinearLayout;
    .registers 5

    #@0
    .prologue
    .line 204
    new-instance v0, Landroid/widget/LinearLayout;

    #@2
    invoke-virtual {p0}, Lcom/android/internal/widget/ScrollingTabContainerView;->getContext()Landroid/content/Context;

    #@5
    move-result-object v1

    #@6
    const/4 v2, 0x0

    #@7
    const v3, 0x10102f4

    #@a
    invoke-direct {v0, v1, v2, v3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@d
    .line 206
    .local v0, tabLayout:Landroid/widget/LinearLayout;
    const/4 v1, 0x1

    #@e
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setMeasureWithLargestChildEnabled(Z)V

    #@11
    .line 207
    const/16 v1, 0x11

    #@13
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setGravity(I)V

    #@16
    .line 208
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    #@18
    const/4 v2, -0x2

    #@19
    const/4 v3, -0x1

    #@1a
    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    #@1d
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@20
    .line 210
    return-object v0
.end method

.method private createTabView(Landroid/app/ActionBar$Tab;Z)Lcom/android/internal/widget/ScrollingTabContainerView$TabView;
    .registers 7
    .parameter "tab"
    .parameter "forAdapter"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 290
    new-instance v0, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;

    #@3
    invoke-virtual {p0}, Lcom/android/internal/widget/ScrollingTabContainerView;->getContext()Landroid/content/Context;

    #@6
    move-result-object v1

    #@7
    invoke-direct {v0, p0, v1, p1, p2}, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;-><init>(Lcom/android/internal/widget/ScrollingTabContainerView;Landroid/content/Context;Landroid/app/ActionBar$Tab;Z)V

    #@a
    .line 291
    .local v0, tabView:Lcom/android/internal/widget/ScrollingTabContainerView$TabView;
    if-eqz p2, :cond_1b

    #@c
    .line 292
    invoke-virtual {v0, v2}, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    #@f
    .line 293
    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    #@11
    const/4 v2, -0x1

    #@12
    iget v3, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mContentHeight:I

    #@14
    invoke-direct {v1, v2, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    #@17
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@1a
    .line 303
    :goto_1a
    return-object v0

    #@1b
    .line 296
    :cond_1b
    const/4 v1, 0x1

    #@1c
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->setFocusable(Z)V

    #@1f
    .line 298
    iget-object v1, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabClickListener:Lcom/android/internal/widget/ScrollingTabContainerView$TabClickListener;

    #@21
    if-nez v1, :cond_2a

    #@23
    .line 299
    new-instance v1, Lcom/android/internal/widget/ScrollingTabContainerView$TabClickListener;

    #@25
    invoke-direct {v1, p0, v2}, Lcom/android/internal/widget/ScrollingTabContainerView$TabClickListener;-><init>(Lcom/android/internal/widget/ScrollingTabContainerView;Lcom/android/internal/widget/ScrollingTabContainerView$1;)V

    #@28
    iput-object v1, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabClickListener:Lcom/android/internal/widget/ScrollingTabContainerView$TabClickListener;

    #@2a
    .line 301
    :cond_2a
    iget-object v1, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabClickListener:Lcom/android/internal/widget/ScrollingTabContainerView$TabClickListener;

    #@2c
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@2f
    goto :goto_1a
.end method


# virtual methods
.method public addTab(Landroid/app/ActionBar$Tab;IZ)V
    .registers 10
    .parameter "tab"
    .parameter "position"
    .parameter "setSelected"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 322
    invoke-direct {p0, p1, v5}, Lcom/android/internal/widget/ScrollingTabContainerView;->createTabView(Landroid/app/ActionBar$Tab;Z)Lcom/android/internal/widget/ScrollingTabContainerView$TabView;

    #@4
    move-result-object v0

    #@5
    .line 323
    .local v0, tabView:Lcom/android/internal/widget/ScrollingTabContainerView$TabView;
    iget-object v1, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabLayout:Landroid/widget/LinearLayout;

    #@7
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    #@9
    const/4 v3, -0x1

    #@a
    const/high16 v4, 0x3f80

    #@c
    invoke-direct {v2, v5, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    #@f
    invoke-virtual {v1, v0, p2, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    #@12
    .line 325
    iget-object v1, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabSpinner:Landroid/widget/Spinner;

    #@14
    if-eqz v1, :cond_21

    #@16
    .line 326
    iget-object v1, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabSpinner:Landroid/widget/Spinner;

    #@18
    invoke-virtual {v1}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    #@1b
    move-result-object v1

    #@1c
    check-cast v1, Lcom/android/internal/widget/ScrollingTabContainerView$TabAdapter;

    #@1e
    invoke-virtual {v1}, Lcom/android/internal/widget/ScrollingTabContainerView$TabAdapter;->notifyDataSetChanged()V

    #@21
    .line 328
    :cond_21
    if-eqz p3, :cond_27

    #@23
    .line 329
    const/4 v1, 0x1

    #@24
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->setSelected(Z)V

    #@27
    .line 331
    :cond_27
    iget-boolean v1, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mAllowCollapse:Z

    #@29
    if-eqz v1, :cond_2e

    #@2b
    .line 332
    invoke-virtual {p0}, Lcom/android/internal/widget/ScrollingTabContainerView;->requestLayout()V

    #@2e
    .line 334
    :cond_2e
    return-void
.end method

.method public addTab(Landroid/app/ActionBar$Tab;Z)V
    .registers 9
    .parameter "tab"
    .parameter "setSelected"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 307
    invoke-direct {p0, p1, v5}, Lcom/android/internal/widget/ScrollingTabContainerView;->createTabView(Landroid/app/ActionBar$Tab;Z)Lcom/android/internal/widget/ScrollingTabContainerView$TabView;

    #@4
    move-result-object v0

    #@5
    .line 308
    .local v0, tabView:Lcom/android/internal/widget/ScrollingTabContainerView$TabView;
    iget-object v1, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabLayout:Landroid/widget/LinearLayout;

    #@7
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    #@9
    const/4 v3, -0x1

    #@a
    const/high16 v4, 0x3f80

    #@c
    invoke-direct {v2, v5, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    #@f
    invoke-virtual {v1, v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@12
    .line 310
    iget-object v1, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabSpinner:Landroid/widget/Spinner;

    #@14
    if-eqz v1, :cond_21

    #@16
    .line 311
    iget-object v1, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabSpinner:Landroid/widget/Spinner;

    #@18
    invoke-virtual {v1}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    #@1b
    move-result-object v1

    #@1c
    check-cast v1, Lcom/android/internal/widget/ScrollingTabContainerView$TabAdapter;

    #@1e
    invoke-virtual {v1}, Lcom/android/internal/widget/ScrollingTabContainerView$TabAdapter;->notifyDataSetChanged()V

    #@21
    .line 313
    :cond_21
    if-eqz p2, :cond_27

    #@23
    .line 314
    const/4 v1, 0x1

    #@24
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->setSelected(Z)V

    #@27
    .line 316
    :cond_27
    iget-boolean v1, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mAllowCollapse:Z

    #@29
    if-eqz v1, :cond_2e

    #@2b
    .line 317
    invoke-virtual {p0}, Lcom/android/internal/widget/ScrollingTabContainerView;->requestLayout()V

    #@2e
    .line 319
    :cond_2e
    return-void
.end method

.method public animateToTab(I)V
    .registers 4
    .parameter "position"

    #@0
    .prologue
    .line 258
    iget-object v1, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabLayout:Landroid/widget/LinearLayout;

    #@2
    invoke-virtual {v1, p1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    #@5
    move-result-object v0

    #@6
    .line 259
    .local v0, tabView:Landroid/view/View;
    iget-object v1, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabSelector:Ljava/lang/Runnable;

    #@8
    if-eqz v1, :cond_f

    #@a
    .line 260
    iget-object v1, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabSelector:Ljava/lang/Runnable;

    #@c
    invoke-virtual {p0, v1}, Lcom/android/internal/widget/ScrollingTabContainerView;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@f
    .line 262
    :cond_f
    new-instance v1, Lcom/android/internal/widget/ScrollingTabContainerView$1;

    #@11
    invoke-direct {v1, p0, v0}, Lcom/android/internal/widget/ScrollingTabContainerView$1;-><init>(Lcom/android/internal/widget/ScrollingTabContainerView;Landroid/view/View;)V

    #@14
    iput-object v1, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabSelector:Ljava/lang/Runnable;

    #@16
    .line 269
    iget-object v1, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabSelector:Ljava/lang/Runnable;

    #@18
    invoke-virtual {p0, v1}, Lcom/android/internal/widget/ScrollingTabContainerView;->post(Ljava/lang/Runnable;)Z

    #@1b
    .line 270
    return-void
.end method

.method public animateToVisibility(I)V
    .registers 9
    .parameter "visibility"

    #@0
    .prologue
    const-wide/16 v5, 0xc8

    #@2
    const/4 v2, 0x1

    #@3
    const/4 v4, 0x0

    #@4
    const/4 v3, 0x0

    #@5
    .line 234
    iget-object v1, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mVisibilityAnim:Landroid/animation/Animator;

    #@7
    if-eqz v1, :cond_e

    #@9
    .line 235
    iget-object v1, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mVisibilityAnim:Landroid/animation/Animator;

    #@b
    invoke-virtual {v1}, Landroid/animation/Animator;->cancel()V

    #@e
    .line 237
    :cond_e
    if-nez p1, :cond_3a

    #@10
    .line 238
    invoke-virtual {p0}, Lcom/android/internal/widget/ScrollingTabContainerView;->getVisibility()I

    #@13
    move-result v1

    #@14
    if-eqz v1, :cond_19

    #@16
    .line 239
    invoke-virtual {p0, v3}, Lcom/android/internal/widget/ScrollingTabContainerView;->setAlpha(F)V

    #@19
    .line 241
    :cond_19
    const-string v1, "alpha"

    #@1b
    new-array v2, v2, [F

    #@1d
    const/high16 v3, 0x3f80

    #@1f
    aput v3, v2, v4

    #@21
    invoke-static {p0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    #@24
    move-result-object v0

    #@25
    .line 242
    .local v0, anim:Landroid/animation/ObjectAnimator;
    invoke-virtual {v0, v5, v6}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    #@28
    .line 243
    sget-object v1, Lcom/android/internal/widget/ScrollingTabContainerView;->sAlphaInterpolator:Landroid/animation/TimeInterpolator;

    #@2a
    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    #@2d
    .line 245
    iget-object v1, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mVisAnimListener:Lcom/android/internal/widget/ScrollingTabContainerView$VisibilityAnimListener;

    #@2f
    invoke-virtual {v1, p1}, Lcom/android/internal/widget/ScrollingTabContainerView$VisibilityAnimListener;->withFinalVisibility(I)Lcom/android/internal/widget/ScrollingTabContainerView$VisibilityAnimListener;

    #@32
    move-result-object v1

    #@33
    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    #@36
    .line 246
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    #@39
    .line 255
    :goto_39
    return-void

    #@3a
    .line 248
    .end local v0           #anim:Landroid/animation/ObjectAnimator;
    :cond_3a
    const-string v1, "alpha"

    #@3c
    new-array v2, v2, [F

    #@3e
    aput v3, v2, v4

    #@40
    invoke-static {p0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    #@43
    move-result-object v0

    #@44
    .line 249
    .restart local v0       #anim:Landroid/animation/ObjectAnimator;
    invoke-virtual {v0, v5, v6}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    #@47
    .line 250
    sget-object v1, Lcom/android/internal/widget/ScrollingTabContainerView;->sAlphaInterpolator:Landroid/animation/TimeInterpolator;

    #@49
    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    #@4c
    .line 252
    iget-object v1, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mVisAnimListener:Lcom/android/internal/widget/ScrollingTabContainerView$VisibilityAnimListener;

    #@4e
    invoke-virtual {v1, p1}, Lcom/android/internal/widget/ScrollingTabContainerView$VisibilityAnimListener;->withFinalVisibility(I)Lcom/android/internal/widget/ScrollingTabContainerView$VisibilityAnimListener;

    #@51
    move-result-object v1

    #@52
    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    #@55
    .line 253
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    #@58
    goto :goto_39
.end method

.method protected isCollapsed()Z
    .registers 2

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabSpinner:Landroid/widget/Spinner;

    #@2
    if-eqz v0, :cond_e

    #@4
    iget-object v0, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabSpinner:Landroid/widget/Spinner;

    #@6
    invoke-virtual {v0}, Landroid/widget/Spinner;->getParent()Landroid/view/ViewParent;

    #@9
    move-result-object v0

    #@a
    if-ne v0, p0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public onAttachedToWindow()V
    .registers 2

    #@0
    .prologue
    .line 274
    invoke-super {p0}, Landroid/widget/HorizontalScrollView;->onAttachedToWindow()V

    #@3
    .line 275
    iget-object v0, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabSelector:Ljava/lang/Runnable;

    #@5
    if-eqz v0, :cond_c

    #@7
    .line 277
    iget-object v0, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabSelector:Ljava/lang/Runnable;

    #@9
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/ScrollingTabContainerView;->post(Ljava/lang/Runnable;)Z

    #@c
    .line 279
    :cond_c
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 4
    .parameter "newConfig"

    #@0
    .prologue
    .line 224
    invoke-super {p0, p1}, Landroid/widget/HorizontalScrollView;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    #@3
    .line 226
    invoke-virtual {p0}, Lcom/android/internal/widget/ScrollingTabContainerView;->getContext()Landroid/content/Context;

    #@6
    move-result-object v1

    #@7
    invoke-static {v1}, Lcom/android/internal/view/ActionBarPolicy;->get(Landroid/content/Context;)Lcom/android/internal/view/ActionBarPolicy;

    #@a
    move-result-object v0

    #@b
    .line 229
    .local v0, abp:Lcom/android/internal/view/ActionBarPolicy;
    invoke-virtual {v0}, Lcom/android/internal/view/ActionBarPolicy;->getTabContainerHeight()I

    #@e
    move-result v1

    #@f
    invoke-virtual {p0, v1}, Lcom/android/internal/widget/ScrollingTabContainerView;->setContentHeight(I)V

    #@12
    .line 230
    invoke-virtual {v0}, Lcom/android/internal/view/ActionBarPolicy;->getStackedTabMaxWidth()I

    #@15
    move-result v1

    #@16
    iput v1, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mStackedTabMaxWidth:I

    #@18
    .line 231
    return-void
.end method

.method public onDetachedFromWindow()V
    .registers 2

    #@0
    .prologue
    .line 283
    invoke-super {p0}, Landroid/widget/HorizontalScrollView;->onDetachedFromWindow()V

    #@3
    .line 284
    iget-object v0, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabSelector:Ljava/lang/Runnable;

    #@5
    if-eqz v0, :cond_c

    #@7
    .line 285
    iget-object v0, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabSelector:Ljava/lang/Runnable;

    #@9
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/ScrollingTabContainerView;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@c
    .line 287
    :cond_c
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 8
    .parameter
    .parameter "view"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    #@0
    .prologue
    .line 368
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    move-object v0, p2

    #@1
    check-cast v0, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;

    #@3
    .line 369
    .local v0, tabView:Lcom/android/internal/widget/ScrollingTabContainerView$TabView;
    invoke-virtual {v0}, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->getTab()Landroid/app/ActionBar$Tab;

    #@6
    move-result-object v1

    #@7
    invoke-virtual {v1}, Landroid/app/ActionBar$Tab;->select()V

    #@a
    .line 370
    return-void
.end method

.method public onMeasure(II)V
    .registers 15
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    const/high16 v11, 0x4000

    #@2
    const/4 v7, 0x1

    #@3
    const/4 v8, 0x0

    #@4
    .line 91
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@7
    move-result v6

    #@8
    .line 92
    .local v6, widthMode:I
    if-ne v6, v11, :cond_7c

    #@a
    move v3, v7

    #@b
    .line 93
    .local v3, lockedExpanded:Z
    :goto_b
    invoke-virtual {p0, v3}, Lcom/android/internal/widget/ScrollingTabContainerView;->setFillViewport(Z)V

    #@e
    .line 96
    invoke-virtual {p0}, Lcom/android/internal/widget/ScrollingTabContainerView;->getContext()Landroid/content/Context;

    #@11
    move-result-object v9

    #@12
    invoke-static {v9}, Lcom/android/internal/view/ActionBarPolicy;->get(Landroid/content/Context;)Lcom/android/internal/view/ActionBarPolicy;

    #@15
    move-result-object v0

    #@16
    .line 97
    .local v0, abp:Lcom/android/internal/view/ActionBarPolicy;
    invoke-virtual {v0}, Lcom/android/internal/view/ActionBarPolicy;->getTabContainerHeight()I

    #@19
    move-result v9

    #@1a
    invoke-virtual {p0, v9}, Lcom/android/internal/widget/ScrollingTabContainerView;->setContentHeight(I)V

    #@1d
    .line 100
    iget-object v9, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabLayout:Landroid/widget/LinearLayout;

    #@1f
    invoke-virtual {v9}, Landroid/widget/LinearLayout;->getChildCount()I

    #@22
    move-result v2

    #@23
    .line 101
    .local v2, childCount:I
    if-le v2, v7, :cond_87

    #@25
    if-eq v6, v11, :cond_2b

    #@27
    const/high16 v9, -0x8000

    #@29
    if-ne v6, v9, :cond_87

    #@2b
    .line 103
    :cond_2b
    const/4 v9, 0x2

    #@2c
    if-le v2, v9, :cond_7e

    #@2e
    .line 104
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@31
    move-result v9

    #@32
    int-to-float v9, v9

    #@33
    const v10, 0x3ecccccd

    #@36
    mul-float/2addr v9, v10

    #@37
    float-to-int v9, v9

    #@38
    iput v9, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mMaxTabWidth:I

    #@3a
    .line 108
    :goto_3a
    iget v9, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mMaxTabWidth:I

    #@3c
    iget v10, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mStackedTabMaxWidth:I

    #@3e
    invoke-static {v9, v10}, Ljava/lang/Math;->min(II)I

    #@41
    move-result v9

    #@42
    iput v9, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mMaxTabWidth:I

    #@44
    .line 113
    :goto_44
    iget v9, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mContentHeight:I

    #@46
    invoke-static {v9, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@49
    move-result p2

    #@4a
    .line 115
    if-nez v3, :cond_8b

    #@4c
    iget-boolean v9, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mAllowCollapse:Z

    #@4e
    if-eqz v9, :cond_8b

    #@50
    move v1, v7

    #@51
    .line 117
    .local v1, canCollapse:Z
    :goto_51
    if-eqz v1, :cond_91

    #@53
    .line 119
    iget-object v7, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabLayout:Landroid/widget/LinearLayout;

    #@55
    invoke-virtual {v7, v8, p2}, Landroid/widget/LinearLayout;->measure(II)V

    #@58
    .line 120
    iget-object v7, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabLayout:Landroid/widget/LinearLayout;

    #@5a
    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    #@5d
    move-result v7

    #@5e
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@61
    move-result v8

    #@62
    if-le v7, v8, :cond_8d

    #@64
    .line 121
    invoke-virtual {p0}, Lcom/android/internal/widget/ScrollingTabContainerView;->performCollapse()V

    #@67
    .line 129
    :goto_67
    invoke-virtual {p0}, Lcom/android/internal/widget/ScrollingTabContainerView;->getMeasuredWidth()I

    #@6a
    move-result v5

    #@6b
    .line 130
    .local v5, oldWidth:I
    invoke-super {p0, p1, p2}, Landroid/widget/HorizontalScrollView;->onMeasure(II)V

    #@6e
    .line 131
    invoke-virtual {p0}, Lcom/android/internal/widget/ScrollingTabContainerView;->getMeasuredWidth()I

    #@71
    move-result v4

    #@72
    .line 133
    .local v4, newWidth:I
    if-eqz v3, :cond_7b

    #@74
    if-eq v5, v4, :cond_7b

    #@76
    .line 135
    iget v7, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mSelectedTabIndex:I

    #@78
    invoke-virtual {p0, v7}, Lcom/android/internal/widget/ScrollingTabContainerView;->setTabSelected(I)V

    #@7b
    .line 137
    :cond_7b
    return-void

    #@7c
    .end local v0           #abp:Lcom/android/internal/view/ActionBarPolicy;
    .end local v1           #canCollapse:Z
    .end local v2           #childCount:I
    .end local v3           #lockedExpanded:Z
    .end local v4           #newWidth:I
    .end local v5           #oldWidth:I
    :cond_7c
    move v3, v8

    #@7d
    .line 92
    goto :goto_b

    #@7e
    .line 106
    .restart local v0       #abp:Lcom/android/internal/view/ActionBarPolicy;
    .restart local v2       #childCount:I
    .restart local v3       #lockedExpanded:Z
    :cond_7e
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@81
    move-result v9

    #@82
    div-int/lit8 v9, v9, 0x2

    #@84
    iput v9, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mMaxTabWidth:I

    #@86
    goto :goto_3a

    #@87
    .line 110
    :cond_87
    const/4 v9, -0x1

    #@88
    iput v9, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mMaxTabWidth:I

    #@8a
    goto :goto_44

    #@8b
    :cond_8b
    move v1, v8

    #@8c
    .line 115
    goto :goto_51

    #@8d
    .line 123
    .restart local v1       #canCollapse:Z
    :cond_8d
    invoke-virtual {p0}, Lcom/android/internal/widget/ScrollingTabContainerView;->performExpand()Z

    #@90
    goto :goto_67

    #@91
    .line 126
    :cond_91
    invoke-virtual {p0}, Lcom/android/internal/widget/ScrollingTabContainerView;->performExpand()Z

    #@94
    goto :goto_67
.end method

.method protected performCollapse()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 155
    invoke-virtual {p0}, Lcom/android/internal/widget/ScrollingTabContainerView;->isCollapsed()Z

    #@4
    move-result v0

    #@5
    if-eqz v0, :cond_8

    #@7
    .line 171
    :goto_7
    return-void

    #@8
    .line 157
    :cond_8
    iget-object v0, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabSpinner:Landroid/widget/Spinner;

    #@a
    if-nez v0, :cond_12

    #@c
    .line 158
    invoke-direct {p0}, Lcom/android/internal/widget/ScrollingTabContainerView;->createSpinner()Landroid/widget/Spinner;

    #@f
    move-result-object v0

    #@10
    iput-object v0, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabSpinner:Landroid/widget/Spinner;

    #@12
    .line 160
    :cond_12
    iget-object v0, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabLayout:Landroid/widget/LinearLayout;

    #@14
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/ScrollingTabContainerView;->removeView(Landroid/view/View;)V

    #@17
    .line 161
    iget-object v0, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabSpinner:Landroid/widget/Spinner;

    #@19
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    #@1b
    const/4 v2, -0x2

    #@1c
    const/4 v3, -0x1

    #@1d
    invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    #@20
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/widget/ScrollingTabContainerView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@23
    .line 163
    iget-object v0, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabSpinner:Landroid/widget/Spinner;

    #@25
    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    #@28
    move-result-object v0

    #@29
    if-nez v0, :cond_35

    #@2b
    .line 164
    iget-object v0, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabSpinner:Landroid/widget/Spinner;

    #@2d
    new-instance v1, Lcom/android/internal/widget/ScrollingTabContainerView$TabAdapter;

    #@2f
    invoke-direct {v1, p0, v4}, Lcom/android/internal/widget/ScrollingTabContainerView$TabAdapter;-><init>(Lcom/android/internal/widget/ScrollingTabContainerView;Lcom/android/internal/widget/ScrollingTabContainerView$1;)V

    #@32
    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    #@35
    .line 166
    :cond_35
    iget-object v0, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabSelector:Ljava/lang/Runnable;

    #@37
    if-eqz v0, :cond_40

    #@39
    .line 167
    iget-object v0, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabSelector:Ljava/lang/Runnable;

    #@3b
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/ScrollingTabContainerView;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@3e
    .line 168
    iput-object v4, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabSelector:Ljava/lang/Runnable;

    #@40
    .line 170
    :cond_40
    iget-object v0, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabSpinner:Landroid/widget/Spinner;

    #@42
    iget v1, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mSelectedTabIndex:I

    #@44
    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    #@47
    goto :goto_7
.end method

.method protected performExpand()Z
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 175
    invoke-virtual {p0}, Lcom/android/internal/widget/ScrollingTabContainerView;->isCollapsed()Z

    #@4
    move-result v0

    #@5
    if-nez v0, :cond_8

    #@7
    .line 182
    :goto_7
    return v4

    #@8
    .line 177
    :cond_8
    iget-object v0, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabSpinner:Landroid/widget/Spinner;

    #@a
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/ScrollingTabContainerView;->removeView(Landroid/view/View;)V

    #@d
    .line 178
    iget-object v0, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabLayout:Landroid/widget/LinearLayout;

    #@f
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    #@11
    const/4 v2, -0x2

    #@12
    const/4 v3, -0x1

    #@13
    invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    #@16
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/widget/ScrollingTabContainerView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@19
    .line 180
    iget-object v0, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabSpinner:Landroid/widget/Spinner;

    #@1b
    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    #@1e
    move-result v0

    #@1f
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/ScrollingTabContainerView;->setTabSelected(I)V

    #@22
    .line 181
    iget-object v0, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabSpinner:Landroid/widget/Spinner;

    #@24
    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    #@27
    move-result v0

    #@28
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/ScrollingTabContainerView;->updateTab(I)V

    #@2b
    goto :goto_7
.end method

.method public removeAllTabs()V
    .registers 2

    #@0
    .prologue
    .line 357
    iget-object v0, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabLayout:Landroid/widget/LinearLayout;

    #@2
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    #@5
    .line 358
    iget-object v0, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabSpinner:Landroid/widget/Spinner;

    #@7
    if-eqz v0, :cond_14

    #@9
    .line 359
    iget-object v0, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabSpinner:Landroid/widget/Spinner;

    #@b
    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    #@e
    move-result-object v0

    #@f
    check-cast v0, Lcom/android/internal/widget/ScrollingTabContainerView$TabAdapter;

    #@11
    invoke-virtual {v0}, Lcom/android/internal/widget/ScrollingTabContainerView$TabAdapter;->notifyDataSetChanged()V

    #@14
    .line 361
    :cond_14
    iget-boolean v0, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mAllowCollapse:Z

    #@16
    if-eqz v0, :cond_1b

    #@18
    .line 362
    invoke-virtual {p0}, Lcom/android/internal/widget/ScrollingTabContainerView;->requestLayout()V

    #@1b
    .line 364
    :cond_1b
    return-void
.end method

.method public removeTabAt(I)V
    .registers 3
    .parameter "position"

    #@0
    .prologue
    .line 347
    iget-object v0, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabLayout:Landroid/widget/LinearLayout;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->removeViewAt(I)V

    #@5
    .line 348
    iget-object v0, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabSpinner:Landroid/widget/Spinner;

    #@7
    if-eqz v0, :cond_14

    #@9
    .line 349
    iget-object v0, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabSpinner:Landroid/widget/Spinner;

    #@b
    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    #@e
    move-result-object v0

    #@f
    check-cast v0, Lcom/android/internal/widget/ScrollingTabContainerView$TabAdapter;

    #@11
    invoke-virtual {v0}, Lcom/android/internal/widget/ScrollingTabContainerView$TabAdapter;->notifyDataSetChanged()V

    #@14
    .line 351
    :cond_14
    iget-boolean v0, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mAllowCollapse:Z

    #@16
    if-eqz v0, :cond_1b

    #@18
    .line 352
    invoke-virtual {p0}, Lcom/android/internal/widget/ScrollingTabContainerView;->requestLayout()V

    #@1b
    .line 354
    :cond_1b
    return-void
.end method

.method public setAllowCollapse(Z)V
    .registers 2
    .parameter "allowCollapse"

    #@0
    .prologue
    .line 150
    iput-boolean p1, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mAllowCollapse:Z

    #@2
    .line 151
    return-void
.end method

.method public setContentHeight(I)V
    .registers 2
    .parameter "contentHeight"

    #@0
    .prologue
    .line 199
    iput p1, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mContentHeight:I

    #@2
    .line 200
    invoke-virtual {p0}, Lcom/android/internal/widget/ScrollingTabContainerView;->requestLayout()V

    #@5
    .line 201
    return-void
.end method

.method public setTabSelected(I)V
    .registers 7
    .parameter "position"

    #@0
    .prologue
    .line 186
    iput p1, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mSelectedTabIndex:I

    #@2
    .line 187
    iget-object v4, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabLayout:Landroid/widget/LinearLayout;

    #@4
    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getChildCount()I

    #@7
    move-result v3

    #@8
    .line 188
    .local v3, tabCount:I
    const/4 v1, 0x0

    #@9
    .local v1, i:I
    :goto_9
    if-ge v1, v3, :cond_21

    #@b
    .line 189
    iget-object v4, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabLayout:Landroid/widget/LinearLayout;

    #@d
    invoke-virtual {v4, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    #@10
    move-result-object v0

    #@11
    .line 190
    .local v0, child:Landroid/view/View;
    if-ne v1, p1, :cond_1f

    #@13
    const/4 v2, 0x1

    #@14
    .line 191
    .local v2, isSelected:Z
    :goto_14
    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    #@17
    .line 192
    if-eqz v2, :cond_1c

    #@19
    .line 193
    invoke-virtual {p0, p1}, Lcom/android/internal/widget/ScrollingTabContainerView;->animateToTab(I)V

    #@1c
    .line 188
    :cond_1c
    add-int/lit8 v1, v1, 0x1

    #@1e
    goto :goto_9

    #@1f
    .line 190
    .end local v2           #isSelected:Z
    :cond_1f
    const/4 v2, 0x0

    #@20
    goto :goto_14

    #@21
    .line 196
    .end local v0           #child:Landroid/view/View;
    :cond_21
    return-void
.end method

.method public updateTab(I)V
    .registers 3
    .parameter "position"

    #@0
    .prologue
    .line 337
    iget-object v0, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabLayout:Landroid/widget/LinearLayout;

    #@2
    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;

    #@8
    invoke-virtual {v0}, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->update()V

    #@b
    .line 338
    iget-object v0, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabSpinner:Landroid/widget/Spinner;

    #@d
    if-eqz v0, :cond_1a

    #@f
    .line 339
    iget-object v0, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabSpinner:Landroid/widget/Spinner;

    #@11
    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    #@14
    move-result-object v0

    #@15
    check-cast v0, Lcom/android/internal/widget/ScrollingTabContainerView$TabAdapter;

    #@17
    invoke-virtual {v0}, Lcom/android/internal/widget/ScrollingTabContainerView$TabAdapter;->notifyDataSetChanged()V

    #@1a
    .line 341
    :cond_1a
    iget-boolean v0, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mAllowCollapse:Z

    #@1c
    if-eqz v0, :cond_21

    #@1e
    .line 342
    invoke-virtual {p0}, Lcom/android/internal/widget/ScrollingTabContainerView;->requestLayout()V

    #@21
    .line 344
    :cond_21
    return-void
.end method
