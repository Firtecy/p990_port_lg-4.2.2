.class public Lcom/android/internal/widget/LockPatternView;
.super Landroid/view/View;
.source "LockPatternView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/widget/LockPatternView$1;,
        Lcom/android/internal/widget/LockPatternView$SavedState;,
        Lcom/android/internal/widget/LockPatternView$OnPatternListener;,
        Lcom/android/internal/widget/LockPatternView$DisplayMode;,
        Lcom/android/internal/widget/LockPatternView$Cell;
    }
.end annotation


# static fields
.field private static final ASPECT_LOCK_HEIGHT:I = 0x2

.field private static final ASPECT_LOCK_WIDTH:I = 0x1

.field private static final ASPECT_SQUARE:I = 0x0

.field private static final MILLIS_PER_CIRCLE_ANIMATING:I = 0x2bc

.field private static final PROFILE_DRAWING:Z = false

.field static final STATUS_BAR_HEIGHT:I = 0x19

.field private static final TAG:Ljava/lang/String; = "LockPatternView"


# instance fields
.field private mAnimatingPeriodStart:J

.field private final mArrowMatrix:Landroid/graphics/Matrix;

.field private mAspect:I

.field private mBitmapArrowGreenUp:Landroid/graphics/Bitmap;

.field private mBitmapArrowRedUp:Landroid/graphics/Bitmap;

.field private mBitmapBtnDefault:Landroid/graphics/Bitmap;

.field private mBitmapBtnTouched:Landroid/graphics/Bitmap;

.field private mBitmapCircleDefault:Landroid/graphics/Bitmap;

.field private mBitmapCircleGreen:Landroid/graphics/Bitmap;

.field private mBitmapCircleRed:Landroid/graphics/Bitmap;

.field private mBitmapHeight:I

.field private mBitmapWidth:I

.field private final mCircleMatrix:Landroid/graphics/Matrix;

.field private final mCurrentPath:Landroid/graphics/Path;

.field private mDiameterFactor:F

.field private mDrawingProfilingStarted:Z

.field private mEnableHapticFeedback:Z

.field private mHitFactor:F

.field private mInProgressX:F

.field private mInProgressY:F

.field private mInStealthMode:Z

.field private mInputEnabled:Z

.field private final mInvalidate:Landroid/graphics/Rect;

.field private mOnPatternListener:Lcom/android/internal/widget/LockPatternView$OnPatternListener;

.field private mPaint:Landroid/graphics/Paint;

.field private mPathPaint:Landroid/graphics/Paint;

.field private mPattern:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/widget/LockPatternView$Cell;",
            ">;"
        }
    .end annotation
.end field

.field private mPatternDisplayMode:Lcom/android/internal/widget/LockPatternView$DisplayMode;

.field private mPatternDrawLookup:[[Z

.field private mPatternInProgress:Z

.field private mSquareHeight:F

.field private mSquareWidth:F

.field private final mStrokeAlpha:I

.field private final tag:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 240
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Lcom/android/internal/widget/LockPatternView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 241
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 13
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 244
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@3
    .line 60
    const/4 v8, 0x0

    #@4
    iput-boolean v8, p0, Lcom/android/internal/widget/LockPatternView;->mDrawingProfilingStarted:Z

    #@6
    .line 65
    new-instance v8, Landroid/graphics/Paint;

    #@8
    invoke-direct {v8}, Landroid/graphics/Paint;-><init>()V

    #@b
    iput-object v8, p0, Lcom/android/internal/widget/LockPatternView;->mPaint:Landroid/graphics/Paint;

    #@d
    .line 66
    new-instance v8, Landroid/graphics/Paint;

    #@f
    invoke-direct {v8}, Landroid/graphics/Paint;-><init>()V

    #@12
    iput-object v8, p0, Lcom/android/internal/widget/LockPatternView;->mPathPaint:Landroid/graphics/Paint;

    #@14
    .line 79
    new-instance v8, Ljava/util/ArrayList;

    #@16
    const/16 v9, 0x9

    #@18
    invoke-direct {v8, v9}, Ljava/util/ArrayList;-><init>(I)V

    #@1b
    iput-object v8, p0, Lcom/android/internal/widget/LockPatternView;->mPattern:Ljava/util/ArrayList;

    #@1d
    .line 87
    const/4 v8, 0x3

    #@1e
    const/4 v9, 0x3

    #@1f
    filled-new-array {v8, v9}, [I

    #@22
    move-result-object v8

    #@23
    sget-object v9, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    #@25
    invoke-static {v9, v8}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    #@28
    move-result-object v8

    #@29
    check-cast v8, [[Z

    #@2b
    iput-object v8, p0, Lcom/android/internal/widget/LockPatternView;->mPatternDrawLookup:[[Z

    #@2d
    .line 94
    const/high16 v8, -0x4080

    #@2f
    iput v8, p0, Lcom/android/internal/widget/LockPatternView;->mInProgressX:F

    #@31
    .line 95
    const/high16 v8, -0x4080

    #@33
    iput v8, p0, Lcom/android/internal/widget/LockPatternView;->mInProgressY:F

    #@35
    .line 99
    sget-object v8, Lcom/android/internal/widget/LockPatternView$DisplayMode;->Correct:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    #@37
    iput-object v8, p0, Lcom/android/internal/widget/LockPatternView;->mPatternDisplayMode:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    #@39
    .line 100
    const/4 v8, 0x1

    #@3a
    iput-boolean v8, p0, Lcom/android/internal/widget/LockPatternView;->mInputEnabled:Z

    #@3c
    .line 101
    const/4 v8, 0x0

    #@3d
    iput-boolean v8, p0, Lcom/android/internal/widget/LockPatternView;->mInStealthMode:Z

    #@3f
    .line 102
    const/4 v8, 0x1

    #@40
    iput-boolean v8, p0, Lcom/android/internal/widget/LockPatternView;->mEnableHapticFeedback:Z

    #@42
    .line 103
    const/4 v8, 0x0

    #@43
    iput-boolean v8, p0, Lcom/android/internal/widget/LockPatternView;->mPatternInProgress:Z

    #@45
    .line 105
    const v8, 0x3dcccccd

    #@48
    iput v8, p0, Lcom/android/internal/widget/LockPatternView;->mDiameterFactor:F

    #@4a
    .line 106
    const/16 v8, 0x80

    #@4c
    iput v8, p0, Lcom/android/internal/widget/LockPatternView;->mStrokeAlpha:I

    #@4e
    .line 107
    const v8, 0x3f19999a

    #@51
    iput v8, p0, Lcom/android/internal/widget/LockPatternView;->mHitFactor:F

    #@53
    .line 121
    new-instance v8, Landroid/graphics/Path;

    #@55
    invoke-direct {v8}, Landroid/graphics/Path;-><init>()V

    #@58
    iput-object v8, p0, Lcom/android/internal/widget/LockPatternView;->mCurrentPath:Landroid/graphics/Path;

    #@5a
    .line 122
    new-instance v8, Landroid/graphics/Rect;

    #@5c
    invoke-direct {v8}, Landroid/graphics/Rect;-><init>()V

    #@5f
    iput-object v8, p0, Lcom/android/internal/widget/LockPatternView;->mInvalidate:Landroid/graphics/Rect;

    #@61
    .line 128
    new-instance v8, Landroid/graphics/Matrix;

    #@63
    invoke-direct {v8}, Landroid/graphics/Matrix;-><init>()V

    #@66
    iput-object v8, p0, Lcom/android/internal/widget/LockPatternView;->mArrowMatrix:Landroid/graphics/Matrix;

    #@68
    .line 129
    new-instance v8, Landroid/graphics/Matrix;

    #@6a
    invoke-direct {v8}, Landroid/graphics/Matrix;-><init>()V

    #@6d
    iput-object v8, p0, Lcom/android/internal/widget/LockPatternView;->mCircleMatrix:Landroid/graphics/Matrix;

    #@6f
    .line 246
    sget-object v8, Lcom/android/internal/R$styleable;->LockPatternView:[I

    #@71
    invoke-virtual {p1, p2, v8}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@74
    move-result-object v0

    #@75
    .line 248
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v8, 0x0

    #@76
    invoke-virtual {v0, v8}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@79
    move-result-object v2

    #@7a
    .line 250
    .local v2, aspect:Ljava/lang/String;
    const-string v8, "square"

    #@7c
    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7f
    move-result v8

    #@80
    if-eqz v8, :cond_14c

    #@82
    .line 251
    const/4 v8, 0x0

    #@83
    iput v8, p0, Lcom/android/internal/widget/LockPatternView;->mAspect:I

    #@85
    .line 261
    :goto_85
    sget-object v8, Lcom/android/internal/R$styleable;->View:[I

    #@87
    invoke-virtual {p1, p2, v8}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@8a
    move-result-object v7

    #@8b
    .line 262
    .local v7, ta:Landroid/content/res/TypedArray;
    const/16 v8, 0x9

    #@8d
    invoke-virtual {v7, v8}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@90
    move-result-object v8

    #@91
    iput-object v8, p0, Lcom/android/internal/widget/LockPatternView;->tag:Ljava/lang/String;

    #@93
    .line 265
    const/4 v8, 0x1

    #@94
    invoke-virtual {p0, v8}, Lcom/android/internal/widget/LockPatternView;->setClickable(Z)V

    #@97
    .line 267
    iget-object v8, p0, Lcom/android/internal/widget/LockPatternView;->mPathPaint:Landroid/graphics/Paint;

    #@99
    const/4 v9, 0x1

    #@9a
    invoke-virtual {v8, v9}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    #@9d
    .line 268
    iget-object v8, p0, Lcom/android/internal/widget/LockPatternView;->mPathPaint:Landroid/graphics/Paint;

    #@9f
    const/4 v9, 0x1

    #@a0
    invoke-virtual {v8, v9}, Landroid/graphics/Paint;->setDither(Z)V

    #@a3
    .line 269
    iget-object v8, p0, Lcom/android/internal/widget/LockPatternView;->mPathPaint:Landroid/graphics/Paint;

    #@a5
    const/4 v9, -0x1

    #@a6
    invoke-virtual {v8, v9}, Landroid/graphics/Paint;->setColor(I)V

    #@a9
    .line 270
    iget-object v8, p0, Lcom/android/internal/widget/LockPatternView;->mPathPaint:Landroid/graphics/Paint;

    #@ab
    const/16 v9, 0x80

    #@ad
    invoke-virtual {v8, v9}, Landroid/graphics/Paint;->setAlpha(I)V

    #@b0
    .line 271
    iget-object v8, p0, Lcom/android/internal/widget/LockPatternView;->mPathPaint:Landroid/graphics/Paint;

    #@b2
    sget-object v9, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    #@b4
    invoke-virtual {v8, v9}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    #@b7
    .line 272
    iget-object v8, p0, Lcom/android/internal/widget/LockPatternView;->mPathPaint:Landroid/graphics/Paint;

    #@b9
    sget-object v9, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    #@bb
    invoke-virtual {v8, v9}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    #@be
    .line 273
    iget-object v8, p0, Lcom/android/internal/widget/LockPatternView;->mPathPaint:Landroid/graphics/Paint;

    #@c0
    sget-object v9, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    #@c2
    invoke-virtual {v8, v9}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    #@c5
    .line 277
    const-string v8, "hiddenDot"

    #@c7
    iget-object v9, p0, Lcom/android/internal/widget/LockPatternView;->tag:Ljava/lang/String;

    #@c9
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@cc
    move-result v8

    #@cd
    if-eqz v8, :cond_16b

    #@cf
    .line 278
    const v8, 0x2020073

    #@d2
    invoke-direct {p0, v8}, Lcom/android/internal/widget/LockPatternView;->getBitmapFor(I)Landroid/graphics/Bitmap;

    #@d5
    move-result-object v8

    #@d6
    iput-object v8, p0, Lcom/android/internal/widget/LockPatternView;->mBitmapBtnDefault:Landroid/graphics/Bitmap;

    #@d8
    .line 279
    const v8, 0x2020076

    #@db
    invoke-direct {p0, v8}, Lcom/android/internal/widget/LockPatternView;->getBitmapFor(I)Landroid/graphics/Bitmap;

    #@de
    move-result-object v8

    #@df
    iput-object v8, p0, Lcom/android/internal/widget/LockPatternView;->mBitmapBtnTouched:Landroid/graphics/Bitmap;

    #@e1
    .line 280
    const v8, 0x2020383

    #@e4
    invoke-direct {p0, v8}, Lcom/android/internal/widget/LockPatternView;->getBitmapFor(I)Landroid/graphics/Bitmap;

    #@e7
    move-result-object v8

    #@e8
    iput-object v8, p0, Lcom/android/internal/widget/LockPatternView;->mBitmapCircleDefault:Landroid/graphics/Bitmap;

    #@ea
    .line 281
    const v8, 0x2020386

    #@ed
    invoke-direct {p0, v8}, Lcom/android/internal/widget/LockPatternView;->getBitmapFor(I)Landroid/graphics/Bitmap;

    #@f0
    move-result-object v8

    #@f1
    iput-object v8, p0, Lcom/android/internal/widget/LockPatternView;->mBitmapCircleGreen:Landroid/graphics/Bitmap;

    #@f3
    .line 282
    const v8, 0x2020389

    #@f6
    invoke-direct {p0, v8}, Lcom/android/internal/widget/LockPatternView;->getBitmapFor(I)Landroid/graphics/Bitmap;

    #@f9
    move-result-object v8

    #@fa
    iput-object v8, p0, Lcom/android/internal/widget/LockPatternView;->mBitmapCircleRed:Landroid/graphics/Bitmap;

    #@fc
    .line 291
    :goto_fc
    const v8, 0x1080375

    #@ff
    invoke-direct {p0, v8}, Lcom/android/internal/widget/LockPatternView;->getBitmapFor(I)Landroid/graphics/Bitmap;

    #@102
    move-result-object v8

    #@103
    iput-object v8, p0, Lcom/android/internal/widget/LockPatternView;->mBitmapArrowGreenUp:Landroid/graphics/Bitmap;

    #@105
    .line 292
    const v8, 0x1080376

    #@108
    invoke-direct {p0, v8}, Lcom/android/internal/widget/LockPatternView;->getBitmapFor(I)Landroid/graphics/Bitmap;

    #@10b
    move-result-object v8

    #@10c
    iput-object v8, p0, Lcom/android/internal/widget/LockPatternView;->mBitmapArrowRedUp:Landroid/graphics/Bitmap;

    #@10e
    .line 295
    const/4 v8, 0x5

    #@10f
    new-array v4, v8, [Landroid/graphics/Bitmap;

    #@111
    const/4 v8, 0x0

    #@112
    iget-object v9, p0, Lcom/android/internal/widget/LockPatternView;->mBitmapBtnDefault:Landroid/graphics/Bitmap;

    #@114
    aput-object v9, v4, v8

    #@116
    const/4 v8, 0x1

    #@117
    iget-object v9, p0, Lcom/android/internal/widget/LockPatternView;->mBitmapBtnTouched:Landroid/graphics/Bitmap;

    #@119
    aput-object v9, v4, v8

    #@11b
    const/4 v8, 0x2

    #@11c
    iget-object v9, p0, Lcom/android/internal/widget/LockPatternView;->mBitmapCircleDefault:Landroid/graphics/Bitmap;

    #@11e
    aput-object v9, v4, v8

    #@120
    const/4 v8, 0x3

    #@121
    iget-object v9, p0, Lcom/android/internal/widget/LockPatternView;->mBitmapCircleGreen:Landroid/graphics/Bitmap;

    #@123
    aput-object v9, v4, v8

    #@125
    const/4 v8, 0x4

    #@126
    iget-object v9, p0, Lcom/android/internal/widget/LockPatternView;->mBitmapCircleRed:Landroid/graphics/Bitmap;

    #@128
    aput-object v9, v4, v8

    #@12a
    .line 298
    .local v4, bitmaps:[Landroid/graphics/Bitmap;
    move-object v1, v4

    #@12b
    .local v1, arr$:[Landroid/graphics/Bitmap;
    array-length v6, v1

    #@12c
    .local v6, len$:I
    const/4 v5, 0x0

    #@12d
    .local v5, i$:I
    :goto_12d
    if-ge v5, v6, :cond_19a

    #@12f
    aget-object v3, v1, v5

    #@131
    .line 299
    .local v3, bitmap:Landroid/graphics/Bitmap;
    iget v8, p0, Lcom/android/internal/widget/LockPatternView;->mBitmapWidth:I

    #@133
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    #@136
    move-result v9

    #@137
    invoke-static {v8, v9}, Ljava/lang/Math;->max(II)I

    #@13a
    move-result v8

    #@13b
    iput v8, p0, Lcom/android/internal/widget/LockPatternView;->mBitmapWidth:I

    #@13d
    .line 300
    iget v8, p0, Lcom/android/internal/widget/LockPatternView;->mBitmapHeight:I

    #@13f
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    #@142
    move-result v9

    #@143
    invoke-static {v8, v9}, Ljava/lang/Math;->max(II)I

    #@146
    move-result v8

    #@147
    iput v8, p0, Lcom/android/internal/widget/LockPatternView;->mBitmapHeight:I

    #@149
    .line 298
    add-int/lit8 v5, v5, 0x1

    #@14b
    goto :goto_12d

    #@14c
    .line 252
    .end local v1           #arr$:[Landroid/graphics/Bitmap;
    .end local v3           #bitmap:Landroid/graphics/Bitmap;
    .end local v4           #bitmaps:[Landroid/graphics/Bitmap;
    .end local v5           #i$:I
    .end local v6           #len$:I
    .end local v7           #ta:Landroid/content/res/TypedArray;
    :cond_14c
    const-string v8, "lock_width"

    #@14e
    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@151
    move-result v8

    #@152
    if-eqz v8, :cond_159

    #@154
    .line 253
    const/4 v8, 0x1

    #@155
    iput v8, p0, Lcom/android/internal/widget/LockPatternView;->mAspect:I

    #@157
    goto/16 :goto_85

    #@159
    .line 254
    :cond_159
    const-string v8, "lock_height"

    #@15b
    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@15e
    move-result v8

    #@15f
    if-eqz v8, :cond_166

    #@161
    .line 255
    const/4 v8, 0x2

    #@162
    iput v8, p0, Lcom/android/internal/widget/LockPatternView;->mAspect:I

    #@164
    goto/16 :goto_85

    #@166
    .line 257
    :cond_166
    const/4 v8, 0x0

    #@167
    iput v8, p0, Lcom/android/internal/widget/LockPatternView;->mAspect:I

    #@169
    goto/16 :goto_85

    #@16b
    .line 284
    .restart local v7       #ta:Landroid/content/res/TypedArray;
    :cond_16b
    const v8, 0x1080115

    #@16e
    invoke-direct {p0, v8}, Lcom/android/internal/widget/LockPatternView;->getBitmapFor(I)Landroid/graphics/Bitmap;

    #@171
    move-result-object v8

    #@172
    iput-object v8, p0, Lcom/android/internal/widget/LockPatternView;->mBitmapBtnDefault:Landroid/graphics/Bitmap;

    #@174
    .line 285
    const v8, 0x1080117

    #@177
    invoke-direct {p0, v8}, Lcom/android/internal/widget/LockPatternView;->getBitmapFor(I)Landroid/graphics/Bitmap;

    #@17a
    move-result-object v8

    #@17b
    iput-object v8, p0, Lcom/android/internal/widget/LockPatternView;->mBitmapBtnTouched:Landroid/graphics/Bitmap;

    #@17d
    .line 286
    const v8, 0x1080378

    #@180
    invoke-direct {p0, v8}, Lcom/android/internal/widget/LockPatternView;->getBitmapFor(I)Landroid/graphics/Bitmap;

    #@183
    move-result-object v8

    #@184
    iput-object v8, p0, Lcom/android/internal/widget/LockPatternView;->mBitmapCircleDefault:Landroid/graphics/Bitmap;

    #@186
    .line 287
    const v8, 0x108037a

    #@189
    invoke-direct {p0, v8}, Lcom/android/internal/widget/LockPatternView;->getBitmapFor(I)Landroid/graphics/Bitmap;

    #@18c
    move-result-object v8

    #@18d
    iput-object v8, p0, Lcom/android/internal/widget/LockPatternView;->mBitmapCircleGreen:Landroid/graphics/Bitmap;

    #@18f
    .line 288
    const v8, 0x108037c

    #@192
    invoke-direct {p0, v8}, Lcom/android/internal/widget/LockPatternView;->getBitmapFor(I)Landroid/graphics/Bitmap;

    #@195
    move-result-object v8

    #@196
    iput-object v8, p0, Lcom/android/internal/widget/LockPatternView;->mBitmapCircleRed:Landroid/graphics/Bitmap;

    #@198
    goto/16 :goto_fc

    #@19a
    .line 303
    .restart local v1       #arr$:[Landroid/graphics/Bitmap;
    .restart local v4       #bitmaps:[Landroid/graphics/Bitmap;
    .restart local v5       #i$:I
    .restart local v6       #len$:I
    :cond_19a
    return-void
.end method

.method private addCellToPattern(Lcom/android/internal/widget/LockPatternView$Cell;)V
    .registers 5
    .parameter "newCell"

    #@0
    .prologue
    .line 572
    iget-object v0, p0, Lcom/android/internal/widget/LockPatternView;->mPatternDrawLookup:[[Z

    #@2
    invoke-virtual {p1}, Lcom/android/internal/widget/LockPatternView$Cell;->getRow()I

    #@5
    move-result v1

    #@6
    aget-object v0, v0, v1

    #@8
    invoke-virtual {p1}, Lcom/android/internal/widget/LockPatternView$Cell;->getColumn()I

    #@b
    move-result v1

    #@c
    const/4 v2, 0x1

    #@d
    aput-boolean v2, v0, v1

    #@f
    .line 573
    iget-object v0, p0, Lcom/android/internal/widget/LockPatternView;->mPattern:Ljava/util/ArrayList;

    #@11
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@14
    .line 574
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternView;->notifyCellAdded()V

    #@17
    .line 575
    return-void
.end method

.method private checkForNewHit(FF)Lcom/android/internal/widget/LockPatternView$Cell;
    .registers 7
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 580
    invoke-direct {p0, p2}, Lcom/android/internal/widget/LockPatternView;->getRowHit(F)I

    #@4
    move-result v1

    #@5
    .line 581
    .local v1, rowHit:I
    if-gez v1, :cond_8

    #@7
    .line 592
    :cond_7
    :goto_7
    return-object v2

    #@8
    .line 584
    :cond_8
    invoke-direct {p0, p1}, Lcom/android/internal/widget/LockPatternView;->getColumnHit(F)I

    #@b
    move-result v0

    #@c
    .line 585
    .local v0, columnHit:I
    if-ltz v0, :cond_7

    #@e
    .line 589
    iget-object v3, p0, Lcom/android/internal/widget/LockPatternView;->mPatternDrawLookup:[[Z

    #@10
    aget-object v3, v3, v1

    #@12
    aget-boolean v3, v3, v0

    #@14
    if-nez v3, :cond_7

    #@16
    .line 592
    invoke-static {v1, v0}, Lcom/android/internal/widget/LockPatternView$Cell;->of(II)Lcom/android/internal/widget/LockPatternView$Cell;

    #@19
    move-result-object v2

    #@1a
    goto :goto_7
.end method

.method private clearPatternDrawLookup()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x3

    #@1
    .line 440
    const/4 v0, 0x0

    #@2
    .local v0, i:I
    :goto_2
    if-ge v0, v4, :cond_14

    #@4
    .line 441
    const/4 v1, 0x0

    #@5
    .local v1, j:I
    :goto_5
    if-ge v1, v4, :cond_11

    #@7
    .line 442
    iget-object v2, p0, Lcom/android/internal/widget/LockPatternView;->mPatternDrawLookup:[[Z

    #@9
    aget-object v2, v2, v0

    #@b
    const/4 v3, 0x0

    #@c
    aput-boolean v3, v2, v1

    #@e
    .line 441
    add-int/lit8 v1, v1, 0x1

    #@10
    goto :goto_5

    #@11
    .line 440
    :cond_11
    add-int/lit8 v0, v0, 0x1

    #@13
    goto :goto_2

    #@14
    .line 445
    .end local v1           #j:I
    :cond_14
    return-void
.end method

.method private detectAndAddHit(FF)Lcom/android/internal/widget/LockPatternView$Cell;
    .registers 16
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    const/4 v12, 0x2

    #@1
    const/4 v10, -0x1

    #@2
    const/4 v9, 0x1

    #@3
    .line 531
    invoke-direct {p0, p1, p2}, Lcom/android/internal/widget/LockPatternView;->checkForNewHit(FF)Lcom/android/internal/widget/LockPatternView$Cell;

    #@6
    move-result-object v0

    #@7
    .line 532
    .local v0, cell:Lcom/android/internal/widget/LockPatternView$Cell;
    if-eqz v0, :cond_77

    #@9
    .line 535
    const/4 v4, 0x0

    #@a
    .line 536
    .local v4, fillInGapCell:Lcom/android/internal/widget/LockPatternView$Cell;
    iget-object v7, p0, Lcom/android/internal/widget/LockPatternView;->mPattern:Ljava/util/ArrayList;

    #@c
    .line 537
    .local v7, pattern:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/widget/LockPatternView$Cell;>;"
    invoke-virtual {v7}, Ljava/util/ArrayList;->isEmpty()Z

    #@f
    move-result v8

    #@10
    if-nez v8, :cond_58

    #@12
    .line 538
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    #@15
    move-result v8

    #@16
    add-int/lit8 v8, v8, -0x1

    #@18
    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1b
    move-result-object v6

    #@1c
    check-cast v6, Lcom/android/internal/widget/LockPatternView$Cell;

    #@1e
    .line 539
    .local v6, lastCell:Lcom/android/internal/widget/LockPatternView$Cell;
    iget v8, v0, Lcom/android/internal/widget/LockPatternView$Cell;->row:I

    #@20
    iget v11, v6, Lcom/android/internal/widget/LockPatternView$Cell;->row:I

    #@22
    sub-int v2, v8, v11

    #@24
    .line 540
    .local v2, dRow:I
    iget v8, v0, Lcom/android/internal/widget/LockPatternView$Cell;->column:I

    #@26
    iget v11, v6, Lcom/android/internal/widget/LockPatternView$Cell;->column:I

    #@28
    sub-int v1, v8, v11

    #@2a
    .line 542
    .local v1, dColumn:I
    iget v5, v6, Lcom/android/internal/widget/LockPatternView$Cell;->row:I

    #@2c
    .line 543
    .local v5, fillInRow:I
    iget v3, v6, Lcom/android/internal/widget/LockPatternView$Cell;->column:I

    #@2e
    .line 545
    .local v3, fillInColumn:I
    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    #@31
    move-result v8

    #@32
    if-ne v8, v12, :cond_41

    #@34
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    #@37
    move-result v8

    #@38
    if-eq v8, v9, :cond_41

    #@3a
    .line 546
    iget v11, v6, Lcom/android/internal/widget/LockPatternView$Cell;->row:I

    #@3c
    if-lez v2, :cond_75

    #@3e
    move v8, v9

    #@3f
    :goto_3f
    add-int v5, v11, v8

    #@41
    .line 549
    :cond_41
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    #@44
    move-result v8

    #@45
    if-ne v8, v12, :cond_54

    #@47
    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    #@4a
    move-result v8

    #@4b
    if-eq v8, v9, :cond_54

    #@4d
    .line 550
    iget v8, v6, Lcom/android/internal/widget/LockPatternView$Cell;->column:I

    #@4f
    if-lez v1, :cond_52

    #@51
    move v10, v9

    #@52
    :cond_52
    add-int v3, v8, v10

    #@54
    .line 553
    :cond_54
    invoke-static {v5, v3}, Lcom/android/internal/widget/LockPatternView$Cell;->of(II)Lcom/android/internal/widget/LockPatternView$Cell;

    #@57
    move-result-object v4

    #@58
    .line 556
    .end local v1           #dColumn:I
    .end local v2           #dRow:I
    .end local v3           #fillInColumn:I
    .end local v5           #fillInRow:I
    .end local v6           #lastCell:Lcom/android/internal/widget/LockPatternView$Cell;
    :cond_58
    if-eqz v4, :cond_69

    #@5a
    iget-object v8, p0, Lcom/android/internal/widget/LockPatternView;->mPatternDrawLookup:[[Z

    #@5c
    iget v10, v4, Lcom/android/internal/widget/LockPatternView$Cell;->row:I

    #@5e
    aget-object v8, v8, v10

    #@60
    iget v10, v4, Lcom/android/internal/widget/LockPatternView$Cell;->column:I

    #@62
    aget-boolean v8, v8, v10

    #@64
    if-nez v8, :cond_69

    #@66
    .line 558
    invoke-direct {p0, v4}, Lcom/android/internal/widget/LockPatternView;->addCellToPattern(Lcom/android/internal/widget/LockPatternView$Cell;)V

    #@69
    .line 560
    :cond_69
    invoke-direct {p0, v0}, Lcom/android/internal/widget/LockPatternView;->addCellToPattern(Lcom/android/internal/widget/LockPatternView$Cell;)V

    #@6c
    .line 561
    iget-boolean v8, p0, Lcom/android/internal/widget/LockPatternView;->mEnableHapticFeedback:Z

    #@6e
    if-eqz v8, :cond_74

    #@70
    .line 562
    const/4 v8, 0x3

    #@71
    invoke-virtual {p0, v9, v8}, Lcom/android/internal/widget/LockPatternView;->performHapticFeedback(II)Z

    #@74
    .line 568
    .end local v0           #cell:Lcom/android/internal/widget/LockPatternView$Cell;
    .end local v4           #fillInGapCell:Lcom/android/internal/widget/LockPatternView$Cell;
    .end local v7           #pattern:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/widget/LockPatternView$Cell;>;"
    :cond_74
    :goto_74
    return-object v0

    #@75
    .restart local v0       #cell:Lcom/android/internal/widget/LockPatternView$Cell;
    .restart local v1       #dColumn:I
    .restart local v2       #dRow:I
    .restart local v3       #fillInColumn:I
    .restart local v4       #fillInGapCell:Lcom/android/internal/widget/LockPatternView$Cell;
    .restart local v5       #fillInRow:I
    .restart local v6       #lastCell:Lcom/android/internal/widget/LockPatternView$Cell;
    .restart local v7       #pattern:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/widget/LockPatternView$Cell;>;"
    :cond_75
    move v8, v10

    #@76
    .line 546
    goto :goto_3f

    #@77
    .line 568
    .end local v1           #dColumn:I
    .end local v2           #dRow:I
    .end local v3           #fillInColumn:I
    .end local v4           #fillInGapCell:Lcom/android/internal/widget/LockPatternView$Cell;
    .end local v5           #fillInRow:I
    .end local v6           #lastCell:Lcom/android/internal/widget/LockPatternView$Cell;
    .end local v7           #pattern:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/widget/LockPatternView$Cell;>;"
    :cond_77
    const/4 v0, 0x0

    #@78
    goto :goto_74
.end method

.method private drawArrow(Landroid/graphics/Canvas;FFLcom/android/internal/widget/LockPatternView$Cell;Lcom/android/internal/widget/LockPatternView$Cell;)V
    .registers 27
    .parameter "canvas"
    .parameter "leftX"
    .parameter "topY"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 1022
    move-object/from16 v0, p0

    #@2
    iget-object v0, v0, Lcom/android/internal/widget/LockPatternView;->mPatternDisplayMode:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    #@4
    move-object/from16 v17, v0

    #@6
    sget-object v18, Lcom/android/internal/widget/LockPatternView$DisplayMode;->Wrong:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    #@8
    move-object/from16 v0, v17

    #@a
    move-object/from16 v1, v18

    #@c
    if-eq v0, v1, :cond_16d

    #@e
    const/4 v9, 0x1

    #@f
    .line 1024
    .local v9, green:Z
    :goto_f
    move-object/from16 v0, p5

    #@11
    iget v8, v0, Lcom/android/internal/widget/LockPatternView$Cell;->row:I

    #@13
    .line 1025
    .local v8, endRow:I
    move-object/from16 v0, p4

    #@15
    iget v13, v0, Lcom/android/internal/widget/LockPatternView$Cell;->row:I

    #@17
    .line 1026
    .local v13, startRow:I
    move-object/from16 v0, p5

    #@19
    iget v7, v0, Lcom/android/internal/widget/LockPatternView$Cell;->column:I

    #@1b
    .line 1027
    .local v7, endColumn:I
    move-object/from16 v0, p4

    #@1d
    iget v12, v0, Lcom/android/internal/widget/LockPatternView$Cell;->column:I

    #@1f
    .line 1030
    .local v12, startColumn:I
    move-object/from16 v0, p0

    #@21
    iget v0, v0, Lcom/android/internal/widget/LockPatternView;->mSquareWidth:F

    #@23
    move/from16 v17, v0

    #@25
    move/from16 v0, v17

    #@27
    float-to-int v0, v0

    #@28
    move/from16 v17, v0

    #@2a
    move-object/from16 v0, p0

    #@2c
    iget v0, v0, Lcom/android/internal/widget/LockPatternView;->mBitmapWidth:I

    #@2e
    move/from16 v18, v0

    #@30
    sub-int v17, v17, v18

    #@32
    div-int/lit8 v10, v17, 0x2

    #@34
    .line 1031
    .local v10, offsetX:I
    move-object/from16 v0, p0

    #@36
    iget v0, v0, Lcom/android/internal/widget/LockPatternView;->mSquareHeight:F

    #@38
    move/from16 v17, v0

    #@3a
    move/from16 v0, v17

    #@3c
    float-to-int v0, v0

    #@3d
    move/from16 v17, v0

    #@3f
    move-object/from16 v0, p0

    #@41
    iget v0, v0, Lcom/android/internal/widget/LockPatternView;->mBitmapHeight:I

    #@43
    move/from16 v18, v0

    #@45
    sub-int v17, v17, v18

    #@47
    div-int/lit8 v11, v17, 0x2

    #@49
    .line 1036
    .local v11, offsetY:I
    if-eqz v9, :cond_170

    #@4b
    move-object/from16 v0, p0

    #@4d
    iget-object v4, v0, Lcom/android/internal/widget/LockPatternView;->mBitmapArrowGreenUp:Landroid/graphics/Bitmap;

    #@4f
    .line 1037
    .local v4, arrow:Landroid/graphics/Bitmap;
    :goto_4f
    move-object/from16 v0, p0

    #@51
    iget v6, v0, Lcom/android/internal/widget/LockPatternView;->mBitmapWidth:I

    #@53
    .line 1038
    .local v6, cellWidth:I
    move-object/from16 v0, p0

    #@55
    iget v5, v0, Lcom/android/internal/widget/LockPatternView;->mBitmapHeight:I

    #@57
    .line 1041
    .local v5, cellHeight:I
    sub-int v17, v8, v13

    #@59
    move/from16 v0, v17

    #@5b
    int-to-double v0, v0

    #@5c
    move-wide/from16 v17, v0

    #@5e
    sub-int v19, v7, v12

    #@60
    move/from16 v0, v19

    #@62
    int-to-double v0, v0

    #@63
    move-wide/from16 v19, v0

    #@65
    invoke-static/range {v17 .. v20}, Ljava/lang/Math;->atan2(DD)D

    #@68
    move-result-wide v17

    #@69
    move-wide/from16 v0, v17

    #@6b
    double-to-float v0, v0

    #@6c
    move/from16 v16, v0

    #@6e
    .line 1043
    .local v16, theta:F
    move/from16 v0, v16

    #@70
    float-to-double v0, v0

    #@71
    move-wide/from16 v17, v0

    #@73
    invoke-static/range {v17 .. v18}, Ljava/lang/Math;->toDegrees(D)D

    #@76
    move-result-wide v17

    #@77
    move-wide/from16 v0, v17

    #@79
    double-to-float v0, v0

    #@7a
    move/from16 v17, v0

    #@7c
    const/high16 v18, 0x42b4

    #@7e
    add-float v3, v17, v18

    #@80
    .line 1046
    .local v3, angle:F
    move-object/from16 v0, p0

    #@82
    iget v0, v0, Lcom/android/internal/widget/LockPatternView;->mSquareWidth:F

    #@84
    move/from16 v17, v0

    #@86
    move-object/from16 v0, p0

    #@88
    iget v0, v0, Lcom/android/internal/widget/LockPatternView;->mBitmapWidth:I

    #@8a
    move/from16 v18, v0

    #@8c
    move/from16 v0, v18

    #@8e
    int-to-float v0, v0

    #@8f
    move/from16 v18, v0

    #@91
    div-float v17, v17, v18

    #@93
    const/high16 v18, 0x3f80

    #@95
    invoke-static/range {v17 .. v18}, Ljava/lang/Math;->min(FF)F

    #@98
    move-result v14

    #@99
    .line 1047
    .local v14, sx:F
    move-object/from16 v0, p0

    #@9b
    iget v0, v0, Lcom/android/internal/widget/LockPatternView;->mSquareHeight:F

    #@9d
    move/from16 v17, v0

    #@9f
    move-object/from16 v0, p0

    #@a1
    iget v0, v0, Lcom/android/internal/widget/LockPatternView;->mBitmapHeight:I

    #@a3
    move/from16 v18, v0

    #@a5
    move/from16 v0, v18

    #@a7
    int-to-float v0, v0

    #@a8
    move/from16 v18, v0

    #@aa
    div-float v17, v17, v18

    #@ac
    const/high16 v18, 0x3f80

    #@ae
    invoke-static/range {v17 .. v18}, Ljava/lang/Math;->min(FF)F

    #@b1
    move-result v15

    #@b2
    .line 1048
    .local v15, sy:F
    move-object/from16 v0, p0

    #@b4
    iget-object v0, v0, Lcom/android/internal/widget/LockPatternView;->mArrowMatrix:Landroid/graphics/Matrix;

    #@b6
    move-object/from16 v17, v0

    #@b8
    int-to-float v0, v10

    #@b9
    move/from16 v18, v0

    #@bb
    add-float v18, v18, p2

    #@bd
    int-to-float v0, v11

    #@be
    move/from16 v19, v0

    #@c0
    add-float v19, v19, p3

    #@c2
    invoke-virtual/range {v17 .. v19}, Landroid/graphics/Matrix;->setTranslate(FF)V

    #@c5
    .line 1049
    move-object/from16 v0, p0

    #@c7
    iget-object v0, v0, Lcom/android/internal/widget/LockPatternView;->mArrowMatrix:Landroid/graphics/Matrix;

    #@c9
    move-object/from16 v17, v0

    #@cb
    move-object/from16 v0, p0

    #@cd
    iget v0, v0, Lcom/android/internal/widget/LockPatternView;->mBitmapWidth:I

    #@cf
    move/from16 v18, v0

    #@d1
    div-int/lit8 v18, v18, 0x2

    #@d3
    move/from16 v0, v18

    #@d5
    int-to-float v0, v0

    #@d6
    move/from16 v18, v0

    #@d8
    move-object/from16 v0, p0

    #@da
    iget v0, v0, Lcom/android/internal/widget/LockPatternView;->mBitmapHeight:I

    #@dc
    move/from16 v19, v0

    #@de
    div-int/lit8 v19, v19, 0x2

    #@e0
    move/from16 v0, v19

    #@e2
    int-to-float v0, v0

    #@e3
    move/from16 v19, v0

    #@e5
    invoke-virtual/range {v17 .. v19}, Landroid/graphics/Matrix;->preTranslate(FF)Z

    #@e8
    .line 1050
    move-object/from16 v0, p0

    #@ea
    iget-object v0, v0, Lcom/android/internal/widget/LockPatternView;->mArrowMatrix:Landroid/graphics/Matrix;

    #@ec
    move-object/from16 v17, v0

    #@ee
    move-object/from16 v0, v17

    #@f0
    invoke-virtual {v0, v14, v15}, Landroid/graphics/Matrix;->preScale(FF)Z

    #@f3
    .line 1051
    move-object/from16 v0, p0

    #@f5
    iget-object v0, v0, Lcom/android/internal/widget/LockPatternView;->mArrowMatrix:Landroid/graphics/Matrix;

    #@f7
    move-object/from16 v17, v0

    #@f9
    move-object/from16 v0, p0

    #@fb
    iget v0, v0, Lcom/android/internal/widget/LockPatternView;->mBitmapWidth:I

    #@fd
    move/from16 v18, v0

    #@ff
    move/from16 v0, v18

    #@101
    neg-int v0, v0

    #@102
    move/from16 v18, v0

    #@104
    div-int/lit8 v18, v18, 0x2

    #@106
    move/from16 v0, v18

    #@108
    int-to-float v0, v0

    #@109
    move/from16 v18, v0

    #@10b
    move-object/from16 v0, p0

    #@10d
    iget v0, v0, Lcom/android/internal/widget/LockPatternView;->mBitmapHeight:I

    #@10f
    move/from16 v19, v0

    #@111
    move/from16 v0, v19

    #@113
    neg-int v0, v0

    #@114
    move/from16 v19, v0

    #@116
    div-int/lit8 v19, v19, 0x2

    #@118
    move/from16 v0, v19

    #@11a
    int-to-float v0, v0

    #@11b
    move/from16 v19, v0

    #@11d
    invoke-virtual/range {v17 .. v19}, Landroid/graphics/Matrix;->preTranslate(FF)Z

    #@120
    .line 1052
    move-object/from16 v0, p0

    #@122
    iget-object v0, v0, Lcom/android/internal/widget/LockPatternView;->mArrowMatrix:Landroid/graphics/Matrix;

    #@124
    move-object/from16 v17, v0

    #@126
    int-to-float v0, v6

    #@127
    move/from16 v18, v0

    #@129
    const/high16 v19, 0x4000

    #@12b
    div-float v18, v18, v19

    #@12d
    int-to-float v0, v5

    #@12e
    move/from16 v19, v0

    #@130
    const/high16 v20, 0x4000

    #@132
    div-float v19, v19, v20

    #@134
    move-object/from16 v0, v17

    #@136
    move/from16 v1, v18

    #@138
    move/from16 v2, v19

    #@13a
    invoke-virtual {v0, v3, v1, v2}, Landroid/graphics/Matrix;->preRotate(FFF)Z

    #@13d
    .line 1053
    move-object/from16 v0, p0

    #@13f
    iget-object v0, v0, Lcom/android/internal/widget/LockPatternView;->mArrowMatrix:Landroid/graphics/Matrix;

    #@141
    move-object/from16 v17, v0

    #@143
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    #@146
    move-result v18

    #@147
    sub-int v18, v6, v18

    #@149
    move/from16 v0, v18

    #@14b
    int-to-float v0, v0

    #@14c
    move/from16 v18, v0

    #@14e
    const/high16 v19, 0x4000

    #@150
    div-float v18, v18, v19

    #@152
    const/16 v19, 0x0

    #@154
    invoke-virtual/range {v17 .. v19}, Landroid/graphics/Matrix;->preTranslate(FF)Z

    #@157
    .line 1054
    move-object/from16 v0, p0

    #@159
    iget-object v0, v0, Lcom/android/internal/widget/LockPatternView;->mArrowMatrix:Landroid/graphics/Matrix;

    #@15b
    move-object/from16 v17, v0

    #@15d
    move-object/from16 v0, p0

    #@15f
    iget-object v0, v0, Lcom/android/internal/widget/LockPatternView;->mPaint:Landroid/graphics/Paint;

    #@161
    move-object/from16 v18, v0

    #@163
    move-object/from16 v0, p1

    #@165
    move-object/from16 v1, v17

    #@167
    move-object/from16 v2, v18

    #@169
    invoke-virtual {v0, v4, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    #@16c
    .line 1055
    return-void

    #@16d
    .line 1022
    .end local v3           #angle:F
    .end local v4           #arrow:Landroid/graphics/Bitmap;
    .end local v5           #cellHeight:I
    .end local v6           #cellWidth:I
    .end local v7           #endColumn:I
    .end local v8           #endRow:I
    .end local v9           #green:Z
    .end local v10           #offsetX:I
    .end local v11           #offsetY:I
    .end local v12           #startColumn:I
    .end local v13           #startRow:I
    .end local v14           #sx:F
    .end local v15           #sy:F
    .end local v16           #theta:F
    :cond_16d
    const/4 v9, 0x0

    #@16e
    goto/16 :goto_f

    #@170
    .line 1036
    .restart local v7       #endColumn:I
    .restart local v8       #endRow:I
    .restart local v9       #green:Z
    .restart local v10       #offsetX:I
    .restart local v11       #offsetY:I
    .restart local v12       #startColumn:I
    .restart local v13       #startRow:I
    :cond_170
    move-object/from16 v0, p0

    #@172
    iget-object v4, v0, Lcom/android/internal/widget/LockPatternView;->mBitmapArrowRedUp:Landroid/graphics/Bitmap;

    #@174
    goto/16 :goto_4f
.end method

.method private drawCircle(Landroid/graphics/Canvas;IIZ)V
    .registers 18
    .parameter "canvas"
    .parameter "leftX"
    .parameter "topY"
    .parameter "partOfPattern"

    #@0
    .prologue
    .line 1067
    if-eqz p4, :cond_c

    #@2
    iget-boolean v10, p0, Lcom/android/internal/widget/LockPatternView;->mInStealthMode:Z

    #@4
    if-eqz v10, :cond_7d

    #@6
    iget-object v10, p0, Lcom/android/internal/widget/LockPatternView;->mPatternDisplayMode:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    #@8
    sget-object v11, Lcom/android/internal/widget/LockPatternView$DisplayMode;->Wrong:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    #@a
    if-eq v10, v11, :cond_7d

    #@c
    .line 1069
    :cond_c
    iget-object v4, p0, Lcom/android/internal/widget/LockPatternView;->mBitmapCircleDefault:Landroid/graphics/Bitmap;

    #@e
    .line 1070
    .local v4, outerCircle:Landroid/graphics/Bitmap;
    iget-object v1, p0, Lcom/android/internal/widget/LockPatternView;->mBitmapBtnDefault:Landroid/graphics/Bitmap;

    #@10
    .line 1088
    .local v1, innerCircle:Landroid/graphics/Bitmap;
    :goto_10
    iget v9, p0, Lcom/android/internal/widget/LockPatternView;->mBitmapWidth:I

    #@12
    .line 1089
    .local v9, width:I
    iget v0, p0, Lcom/android/internal/widget/LockPatternView;->mBitmapHeight:I

    #@14
    .line 1091
    .local v0, height:I
    iget v6, p0, Lcom/android/internal/widget/LockPatternView;->mSquareWidth:F

    #@16
    .line 1092
    .local v6, squareWidth:F
    iget v5, p0, Lcom/android/internal/widget/LockPatternView;->mSquareHeight:F

    #@18
    .line 1094
    .local v5, squareHeight:F
    int-to-float v10, v9

    #@19
    sub-float v10, v6, v10

    #@1b
    const/high16 v11, 0x4000

    #@1d
    div-float/2addr v10, v11

    #@1e
    float-to-int v2, v10

    #@1f
    .line 1095
    .local v2, offsetX:I
    int-to-float v10, v0

    #@20
    sub-float v10, v5, v10

    #@22
    const/high16 v11, 0x4000

    #@24
    div-float/2addr v10, v11

    #@25
    float-to-int v3, v10

    #@26
    .line 1098
    .local v3, offsetY:I
    iget v10, p0, Lcom/android/internal/widget/LockPatternView;->mSquareWidth:F

    #@28
    iget v11, p0, Lcom/android/internal/widget/LockPatternView;->mBitmapWidth:I

    #@2a
    int-to-float v11, v11

    #@2b
    div-float/2addr v10, v11

    #@2c
    const/high16 v11, 0x3f80

    #@2e
    invoke-static {v10, v11}, Ljava/lang/Math;->min(FF)F

    #@31
    move-result v7

    #@32
    .line 1099
    .local v7, sx:F
    iget v10, p0, Lcom/android/internal/widget/LockPatternView;->mSquareHeight:F

    #@34
    iget v11, p0, Lcom/android/internal/widget/LockPatternView;->mBitmapHeight:I

    #@36
    int-to-float v11, v11

    #@37
    div-float/2addr v10, v11

    #@38
    const/high16 v11, 0x3f80

    #@3a
    invoke-static {v10, v11}, Ljava/lang/Math;->min(FF)F

    #@3d
    move-result v8

    #@3e
    .line 1101
    .local v8, sy:F
    iget-object v10, p0, Lcom/android/internal/widget/LockPatternView;->mCircleMatrix:Landroid/graphics/Matrix;

    #@40
    add-int v11, p2, v2

    #@42
    int-to-float v11, v11

    #@43
    add-int v12, p3, v3

    #@45
    int-to-float v12, v12

    #@46
    invoke-virtual {v10, v11, v12}, Landroid/graphics/Matrix;->setTranslate(FF)V

    #@49
    .line 1102
    iget-object v10, p0, Lcom/android/internal/widget/LockPatternView;->mCircleMatrix:Landroid/graphics/Matrix;

    #@4b
    iget v11, p0, Lcom/android/internal/widget/LockPatternView;->mBitmapWidth:I

    #@4d
    div-int/lit8 v11, v11, 0x2

    #@4f
    int-to-float v11, v11

    #@50
    iget v12, p0, Lcom/android/internal/widget/LockPatternView;->mBitmapHeight:I

    #@52
    div-int/lit8 v12, v12, 0x2

    #@54
    int-to-float v12, v12

    #@55
    invoke-virtual {v10, v11, v12}, Landroid/graphics/Matrix;->preTranslate(FF)Z

    #@58
    .line 1103
    iget-object v10, p0, Lcom/android/internal/widget/LockPatternView;->mCircleMatrix:Landroid/graphics/Matrix;

    #@5a
    invoke-virtual {v10, v7, v8}, Landroid/graphics/Matrix;->preScale(FF)Z

    #@5d
    .line 1104
    iget-object v10, p0, Lcom/android/internal/widget/LockPatternView;->mCircleMatrix:Landroid/graphics/Matrix;

    #@5f
    iget v11, p0, Lcom/android/internal/widget/LockPatternView;->mBitmapWidth:I

    #@61
    neg-int v11, v11

    #@62
    div-int/lit8 v11, v11, 0x2

    #@64
    int-to-float v11, v11

    #@65
    iget v12, p0, Lcom/android/internal/widget/LockPatternView;->mBitmapHeight:I

    #@67
    neg-int v12, v12

    #@68
    div-int/lit8 v12, v12, 0x2

    #@6a
    int-to-float v12, v12

    #@6b
    invoke-virtual {v10, v11, v12}, Landroid/graphics/Matrix;->preTranslate(FF)Z

    #@6e
    .line 1106
    iget-object v10, p0, Lcom/android/internal/widget/LockPatternView;->mCircleMatrix:Landroid/graphics/Matrix;

    #@70
    iget-object v11, p0, Lcom/android/internal/widget/LockPatternView;->mPaint:Landroid/graphics/Paint;

    #@72
    invoke-virtual {p1, v4, v10, v11}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    #@75
    .line 1107
    iget-object v10, p0, Lcom/android/internal/widget/LockPatternView;->mCircleMatrix:Landroid/graphics/Matrix;

    #@77
    iget-object v11, p0, Lcom/android/internal/widget/LockPatternView;->mPaint:Landroid/graphics/Paint;

    #@79
    invoke-virtual {p1, v1, v10, v11}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    #@7c
    .line 1108
    return-void

    #@7d
    .line 1071
    .end local v0           #height:I
    .end local v1           #innerCircle:Landroid/graphics/Bitmap;
    .end local v2           #offsetX:I
    .end local v3           #offsetY:I
    .end local v4           #outerCircle:Landroid/graphics/Bitmap;
    .end local v5           #squareHeight:F
    .end local v6           #squareWidth:F
    .end local v7           #sx:F
    .end local v8           #sy:F
    .end local v9           #width:I
    :cond_7d
    iget-boolean v10, p0, Lcom/android/internal/widget/LockPatternView;->mPatternInProgress:Z

    #@7f
    if-eqz v10, :cond_86

    #@81
    .line 1073
    iget-object v4, p0, Lcom/android/internal/widget/LockPatternView;->mBitmapCircleGreen:Landroid/graphics/Bitmap;

    #@83
    .line 1074
    .restart local v4       #outerCircle:Landroid/graphics/Bitmap;
    iget-object v1, p0, Lcom/android/internal/widget/LockPatternView;->mBitmapBtnTouched:Landroid/graphics/Bitmap;

    #@85
    .restart local v1       #innerCircle:Landroid/graphics/Bitmap;
    goto :goto_10

    #@86
    .line 1075
    .end local v1           #innerCircle:Landroid/graphics/Bitmap;
    .end local v4           #outerCircle:Landroid/graphics/Bitmap;
    :cond_86
    iget-object v10, p0, Lcom/android/internal/widget/LockPatternView;->mPatternDisplayMode:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    #@88
    sget-object v11, Lcom/android/internal/widget/LockPatternView$DisplayMode;->Wrong:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    #@8a
    if-ne v10, v11, :cond_91

    #@8c
    .line 1077
    iget-object v4, p0, Lcom/android/internal/widget/LockPatternView;->mBitmapCircleRed:Landroid/graphics/Bitmap;

    #@8e
    .line 1078
    .restart local v4       #outerCircle:Landroid/graphics/Bitmap;
    iget-object v1, p0, Lcom/android/internal/widget/LockPatternView;->mBitmapBtnDefault:Landroid/graphics/Bitmap;

    #@90
    .restart local v1       #innerCircle:Landroid/graphics/Bitmap;
    goto :goto_10

    #@91
    .line 1079
    .end local v1           #innerCircle:Landroid/graphics/Bitmap;
    .end local v4           #outerCircle:Landroid/graphics/Bitmap;
    :cond_91
    iget-object v10, p0, Lcom/android/internal/widget/LockPatternView;->mPatternDisplayMode:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    #@93
    sget-object v11, Lcom/android/internal/widget/LockPatternView$DisplayMode;->Correct:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    #@95
    if-eq v10, v11, :cond_9d

    #@97
    iget-object v10, p0, Lcom/android/internal/widget/LockPatternView;->mPatternDisplayMode:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    #@99
    sget-object v11, Lcom/android/internal/widget/LockPatternView$DisplayMode;->Animate:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    #@9b
    if-ne v10, v11, :cond_a3

    #@9d
    .line 1082
    :cond_9d
    iget-object v4, p0, Lcom/android/internal/widget/LockPatternView;->mBitmapCircleGreen:Landroid/graphics/Bitmap;

    #@9f
    .line 1083
    .restart local v4       #outerCircle:Landroid/graphics/Bitmap;
    iget-object v1, p0, Lcom/android/internal/widget/LockPatternView;->mBitmapBtnDefault:Landroid/graphics/Bitmap;

    #@a1
    .restart local v1       #innerCircle:Landroid/graphics/Bitmap;
    goto/16 :goto_10

    #@a3
    .line 1085
    .end local v1           #innerCircle:Landroid/graphics/Bitmap;
    .end local v4           #outerCircle:Landroid/graphics/Bitmap;
    :cond_a3
    new-instance v10, Ljava/lang/IllegalStateException;

    #@a5
    new-instance v11, Ljava/lang/StringBuilder;

    #@a7
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@aa
    const-string v12, "unknown display mode "

    #@ac
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@af
    move-result-object v11

    #@b0
    iget-object v12, p0, Lcom/android/internal/widget/LockPatternView;->mPatternDisplayMode:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    #@b2
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v11

    #@b6
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b9
    move-result-object v11

    #@ba
    invoke-direct {v10, v11}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@bd
    throw v10
.end method

.method private getBitmapFor(I)Landroid/graphics/Bitmap;
    .registers 3
    .parameter "resId"

    #@0
    .prologue
    .line 306
    invoke-virtual {p0}, Lcom/android/internal/widget/LockPatternView;->getContext()Landroid/content/Context;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@7
    move-result-object v0

    #@8
    invoke-static {v0, p1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    #@b
    move-result-object v0

    #@c
    return-object v0
.end method

.method private getCenterXForColumn(I)F
    .registers 5
    .parameter "column"

    #@0
    .prologue
    .line 882
    iget v0, p0, Lcom/android/internal/widget/LockPatternView;->mPaddingLeft:I

    #@2
    int-to-float v0, v0

    #@3
    int-to-float v1, p1

    #@4
    iget v2, p0, Lcom/android/internal/widget/LockPatternView;->mSquareWidth:F

    #@6
    mul-float/2addr v1, v2

    #@7
    add-float/2addr v0, v1

    #@8
    iget v1, p0, Lcom/android/internal/widget/LockPatternView;->mSquareWidth:F

    #@a
    const/high16 v2, 0x4000

    #@c
    div-float/2addr v1, v2

    #@d
    add-float/2addr v0, v1

    #@e
    return v0
.end method

.method private getCenterYForRow(I)F
    .registers 5
    .parameter "row"

    #@0
    .prologue
    .line 886
    iget v0, p0, Lcom/android/internal/widget/LockPatternView;->mPaddingTop:I

    #@2
    int-to-float v0, v0

    #@3
    int-to-float v1, p1

    #@4
    iget v2, p0, Lcom/android/internal/widget/LockPatternView;->mSquareHeight:F

    #@6
    mul-float/2addr v1, v2

    #@7
    add-float/2addr v0, v1

    #@8
    iget v1, p0, Lcom/android/internal/widget/LockPatternView;->mSquareHeight:F

    #@a
    const/high16 v2, 0x4000

    #@c
    div-float/2addr v1, v2

    #@d
    add-float/2addr v0, v1

    #@e
    return v0
.end method

.method private getColumnHit(F)I
    .registers 10
    .parameter "x"

    #@0
    .prologue
    .line 622
    iget v4, p0, Lcom/android/internal/widget/LockPatternView;->mSquareWidth:F

    #@2
    .line 623
    .local v4, squareWidth:F
    iget v5, p0, Lcom/android/internal/widget/LockPatternView;->mHitFactor:F

    #@4
    mul-float v1, v4, v5

    #@6
    .line 625
    .local v1, hitSize:F
    iget v5, p0, Lcom/android/internal/widget/LockPatternView;->mPaddingLeft:I

    #@8
    int-to-float v5, v5

    #@9
    sub-float v6, v4, v1

    #@b
    const/high16 v7, 0x4000

    #@d
    div-float/2addr v6, v7

    #@e
    add-float v3, v5, v6

    #@10
    .line 626
    .local v3, offset:F
    const/4 v2, 0x0

    #@11
    .local v2, i:I
    :goto_11
    const/4 v5, 0x3

    #@12
    if-ge v2, v5, :cond_26

    #@14
    .line 628
    int-to-float v5, v2

    #@15
    mul-float/2addr v5, v4

    #@16
    add-float v0, v3, v5

    #@18
    .line 629
    .local v0, hitLeft:F
    cmpl-float v5, p1, v0

    #@1a
    if-ltz v5, :cond_23

    #@1c
    add-float v5, v0, v1

    #@1e
    cmpg-float v5, p1, v5

    #@20
    if-gtz v5, :cond_23

    #@22
    .line 633
    .end local v0           #hitLeft:F
    .end local v2           #i:I
    :goto_22
    return v2

    #@23
    .line 626
    .restart local v0       #hitLeft:F
    .restart local v2       #i:I
    :cond_23
    add-int/lit8 v2, v2, 0x1

    #@25
    goto :goto_11

    #@26
    .line 633
    .end local v0           #hitLeft:F
    :cond_26
    const/4 v2, -0x1

    #@27
    goto :goto_22
.end method

.method private getRowHit(F)I
    .registers 10
    .parameter "y"

    #@0
    .prologue
    .line 602
    iget v4, p0, Lcom/android/internal/widget/LockPatternView;->mSquareHeight:F

    #@2
    .line 603
    .local v4, squareHeight:F
    iget v5, p0, Lcom/android/internal/widget/LockPatternView;->mHitFactor:F

    #@4
    mul-float v0, v4, v5

    #@6
    .line 605
    .local v0, hitSize:F
    iget v5, p0, Lcom/android/internal/widget/LockPatternView;->mPaddingTop:I

    #@8
    int-to-float v5, v5

    #@9
    sub-float v6, v4, v0

    #@b
    const/high16 v7, 0x4000

    #@d
    div-float/2addr v6, v7

    #@e
    add-float v3, v5, v6

    #@10
    .line 606
    .local v3, offset:F
    const/4 v2, 0x0

    #@11
    .local v2, i:I
    :goto_11
    const/4 v5, 0x3

    #@12
    if-ge v2, v5, :cond_26

    #@14
    .line 608
    int-to-float v5, v2

    #@15
    mul-float/2addr v5, v4

    #@16
    add-float v1, v3, v5

    #@18
    .line 609
    .local v1, hitTop:F
    cmpl-float v5, p1, v1

    #@1a
    if-ltz v5, :cond_23

    #@1c
    add-float v5, v1, v0

    #@1e
    cmpg-float v5, p1, v5

    #@20
    if-gtz v5, :cond_23

    #@22
    .line 613
    .end local v1           #hitTop:F
    .end local v2           #i:I
    :goto_22
    return v2

    #@23
    .line 606
    .restart local v1       #hitTop:F
    .restart local v2       #i:I
    :cond_23
    add-int/lit8 v2, v2, 0x1

    #@25
    goto :goto_11

    #@26
    .line 613
    .end local v1           #hitTop:F
    :cond_26
    const/4 v2, -0x1

    #@27
    goto :goto_22
.end method

.method private handleActionDown(Landroid/view/MotionEvent;)V
    .registers 13
    .parameter "event"

    #@0
    .prologue
    const/high16 v8, 0x4000

    #@2
    .line 837
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternView;->resetPattern()V

    #@5
    .line 838
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    #@8
    move-result v5

    #@9
    .line 839
    .local v5, x:F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    #@c
    move-result v6

    #@d
    .line 840
    .local v6, y:F
    invoke-direct {p0, v5, v6}, Lcom/android/internal/widget/LockPatternView;->detectAndAddHit(FF)Lcom/android/internal/widget/LockPatternView$Cell;

    #@10
    move-result-object v1

    #@11
    .line 841
    .local v1, hitCell:Lcom/android/internal/widget/LockPatternView$Cell;
    if-eqz v1, :cond_47

    #@13
    .line 842
    const/4 v7, 0x1

    #@14
    iput-boolean v7, p0, Lcom/android/internal/widget/LockPatternView;->mPatternInProgress:Z

    #@16
    .line 843
    sget-object v7, Lcom/android/internal/widget/LockPatternView$DisplayMode;->Correct:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    #@18
    iput-object v7, p0, Lcom/android/internal/widget/LockPatternView;->mPatternDisplayMode:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    #@1a
    .line 844
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternView;->notifyPatternStarted()V

    #@1d
    .line 849
    :cond_1d
    :goto_1d
    if-eqz v1, :cond_42

    #@1f
    .line 850
    iget v7, v1, Lcom/android/internal/widget/LockPatternView$Cell;->column:I

    #@21
    invoke-direct {p0, v7}, Lcom/android/internal/widget/LockPatternView;->getCenterXForColumn(I)F

    #@24
    move-result v2

    #@25
    .line 851
    .local v2, startX:F
    iget v7, v1, Lcom/android/internal/widget/LockPatternView$Cell;->row:I

    #@27
    invoke-direct {p0, v7}, Lcom/android/internal/widget/LockPatternView;->getCenterYForRow(I)F

    #@2a
    move-result v3

    #@2b
    .line 853
    .local v3, startY:F
    iget v7, p0, Lcom/android/internal/widget/LockPatternView;->mSquareWidth:F

    #@2d
    div-float v4, v7, v8

    #@2f
    .line 854
    .local v4, widthOffset:F
    iget v7, p0, Lcom/android/internal/widget/LockPatternView;->mSquareHeight:F

    #@31
    div-float v0, v7, v8

    #@33
    .line 856
    .local v0, heightOffset:F
    sub-float v7, v2, v4

    #@35
    float-to-int v7, v7

    #@36
    sub-float v8, v3, v0

    #@38
    float-to-int v8, v8

    #@39
    add-float v9, v2, v4

    #@3b
    float-to-int v9, v9

    #@3c
    add-float v10, v3, v0

    #@3e
    float-to-int v10, v10

    #@3f
    invoke-virtual {p0, v7, v8, v9, v10}, Lcom/android/internal/widget/LockPatternView;->invalidate(IIII)V

    #@42
    .line 859
    .end local v0           #heightOffset:F
    .end local v2           #startX:F
    .end local v3           #startY:F
    .end local v4           #widthOffset:F
    :cond_42
    iput v5, p0, Lcom/android/internal/widget/LockPatternView;->mInProgressX:F

    #@44
    .line 860
    iput v6, p0, Lcom/android/internal/widget/LockPatternView;->mInProgressY:F

    #@46
    .line 867
    return-void

    #@47
    .line 845
    :cond_47
    iget-boolean v7, p0, Lcom/android/internal/widget/LockPatternView;->mPatternInProgress:Z

    #@49
    if-eqz v7, :cond_1d

    #@4b
    .line 846
    const/4 v7, 0x0

    #@4c
    iput-boolean v7, p0, Lcom/android/internal/widget/LockPatternView;->mPatternInProgress:Z

    #@4e
    .line 847
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternView;->notifyPatternCleared()V

    #@51
    goto :goto_1d
.end method

.method private handleActionMove(Landroid/view/MotionEvent;)V
    .registers 33
    .parameter "event"

    #@0
    .prologue
    .line 693
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getHistorySize()I

    #@3
    move-result v8

    #@4
    .line 694
    .local v8, historySize:I
    const/4 v10, 0x0

    #@5
    .local v10, i:I
    :goto_5
    add-int/lit8 v27, v8, 0x1

    #@7
    move/from16 v0, v27

    #@9
    if-ge v10, v0, :cond_218

    #@b
    .line 695
    if-ge v10, v8, :cond_1df

    #@d
    move-object/from16 v0, p1

    #@f
    invoke-virtual {v0, v10}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    #@12
    move-result v25

    #@13
    .line 696
    .local v25, x:F
    :goto_13
    if-ge v10, v8, :cond_1e5

    #@15
    move-object/from16 v0, p1

    #@17
    invoke-virtual {v0, v10}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    #@1a
    move-result v26

    #@1b
    .line 697
    .local v26, y:F
    :goto_1b
    move-object/from16 v0, p0

    #@1d
    iget-object v0, v0, Lcom/android/internal/widget/LockPatternView;->mPattern:Ljava/util/ArrayList;

    #@1f
    move-object/from16 v27, v0

    #@21
    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->size()I

    #@24
    move-result v18

    #@25
    .line 698
    .local v18, patternSizePreHitDetect:I
    move-object/from16 v0, p0

    #@27
    move/from16 v1, v25

    #@29
    move/from16 v2, v26

    #@2b
    invoke-direct {v0, v1, v2}, Lcom/android/internal/widget/LockPatternView;->detectAndAddHit(FF)Lcom/android/internal/widget/LockPatternView$Cell;

    #@2e
    move-result-object v9

    #@2f
    .line 699
    .local v9, hitCell:Lcom/android/internal/widget/LockPatternView$Cell;
    move-object/from16 v0, p0

    #@31
    iget-object v0, v0, Lcom/android/internal/widget/LockPatternView;->mPattern:Ljava/util/ArrayList;

    #@33
    move-object/from16 v27, v0

    #@35
    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->size()I

    #@38
    move-result v17

    #@39
    .line 700
    .local v17, patternSize:I
    if-eqz v9, :cond_4e

    #@3b
    const/16 v27, 0x1

    #@3d
    move/from16 v0, v17

    #@3f
    move/from16 v1, v27

    #@41
    if-ne v0, v1, :cond_4e

    #@43
    .line 701
    const/16 v27, 0x1

    #@45
    move/from16 v0, v27

    #@47
    move-object/from16 v1, p0

    #@49
    iput-boolean v0, v1, Lcom/android/internal/widget/LockPatternView;->mPatternInProgress:Z

    #@4b
    .line 702
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/widget/LockPatternView;->notifyPatternStarted()V

    #@4e
    .line 705
    :cond_4e
    move-object/from16 v0, p0

    #@50
    iget v0, v0, Lcom/android/internal/widget/LockPatternView;->mInProgressX:F

    #@52
    move/from16 v27, v0

    #@54
    sub-float v27, v25, v27

    #@56
    invoke-static/range {v27 .. v27}, Ljava/lang/Math;->abs(F)F

    #@59
    move-result v5

    #@5a
    .line 706
    .local v5, dx:F
    move-object/from16 v0, p0

    #@5c
    iget v0, v0, Lcom/android/internal/widget/LockPatternView;->mInProgressY:F

    #@5e
    move/from16 v27, v0

    #@60
    sub-float v27, v26, v27

    #@62
    invoke-static/range {v27 .. v27}, Ljava/lang/Math;->abs(F)F

    #@65
    move-result v6

    #@66
    .line 707
    .local v6, dy:F
    add-float v27, v5, v6

    #@68
    move-object/from16 v0, p0

    #@6a
    iget v0, v0, Lcom/android/internal/widget/LockPatternView;->mSquareWidth:F

    #@6c
    move/from16 v28, v0

    #@6e
    const v29, 0x3c23d70a

    #@71
    mul-float v28, v28, v29

    #@73
    cmpl-float v27, v27, v28

    #@75
    if-lez v27, :cond_1db

    #@77
    .line 708
    move-object/from16 v0, p0

    #@79
    iget v14, v0, Lcom/android/internal/widget/LockPatternView;->mInProgressX:F

    #@7b
    .line 709
    .local v14, oldX:F
    move-object/from16 v0, p0

    #@7d
    iget v15, v0, Lcom/android/internal/widget/LockPatternView;->mInProgressY:F

    #@7f
    .line 711
    .local v15, oldY:F
    move/from16 v0, v25

    #@81
    move-object/from16 v1, p0

    #@83
    iput v0, v1, Lcom/android/internal/widget/LockPatternView;->mInProgressX:F

    #@85
    .line 712
    move/from16 v0, v26

    #@87
    move-object/from16 v1, p0

    #@89
    iput v0, v1, Lcom/android/internal/widget/LockPatternView;->mInProgressY:F

    #@8b
    .line 714
    move-object/from16 v0, p0

    #@8d
    iget-boolean v0, v0, Lcom/android/internal/widget/LockPatternView;->mPatternInProgress:Z

    #@8f
    move/from16 v27, v0

    #@91
    if-eqz v27, :cond_214

    #@93
    if-lez v17, :cond_214

    #@95
    .line 715
    move-object/from16 v0, p0

    #@97
    iget-object v0, v0, Lcom/android/internal/widget/LockPatternView;->mPattern:Ljava/util/ArrayList;

    #@99
    move-object/from16 v16, v0

    #@9b
    .line 716
    .local v16, pattern:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/widget/LockPatternView$Cell;>;"
    move-object/from16 v0, p0

    #@9d
    iget v0, v0, Lcom/android/internal/widget/LockPatternView;->mSquareWidth:F

    #@9f
    move/from16 v27, v0

    #@a1
    move-object/from16 v0, p0

    #@a3
    iget v0, v0, Lcom/android/internal/widget/LockPatternView;->mDiameterFactor:F

    #@a5
    move/from16 v28, v0

    #@a7
    mul-float v27, v27, v28

    #@a9
    const/high16 v28, 0x3f00

    #@ab
    mul-float v19, v27, v28

    #@ad
    .line 718
    .local v19, radius:F
    add-int/lit8 v27, v17, -0x1

    #@af
    move-object/from16 v0, v16

    #@b1
    move/from16 v1, v27

    #@b3
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@b6
    move-result-object v12

    #@b7
    check-cast v12, Lcom/android/internal/widget/LockPatternView$Cell;

    #@b9
    .line 720
    .local v12, lastCell:Lcom/android/internal/widget/LockPatternView$Cell;
    iget v0, v12, Lcom/android/internal/widget/LockPatternView$Cell;->column:I

    #@bb
    move/from16 v27, v0

    #@bd
    move-object/from16 v0, p0

    #@bf
    move/from16 v1, v27

    #@c1
    invoke-direct {v0, v1}, Lcom/android/internal/widget/LockPatternView;->getCenterXForColumn(I)F

    #@c4
    move-result v21

    #@c5
    .line 721
    .local v21, startX:F
    iget v0, v12, Lcom/android/internal/widget/LockPatternView$Cell;->row:I

    #@c7
    move/from16 v27, v0

    #@c9
    move-object/from16 v0, p0

    #@cb
    move/from16 v1, v27

    #@cd
    invoke-direct {v0, v1}, Lcom/android/internal/widget/LockPatternView;->getCenterYForRow(I)F

    #@d0
    move-result v22

    #@d1
    .line 728
    .local v22, startY:F
    move-object/from16 v0, p0

    #@d3
    iget-object v11, v0, Lcom/android/internal/widget/LockPatternView;->mInvalidate:Landroid/graphics/Rect;

    #@d5
    .line 730
    .local v11, invalidateRect:Landroid/graphics/Rect;
    cmpg-float v27, v21, v25

    #@d7
    if-gez v27, :cond_1eb

    #@d9
    .line 731
    move/from16 v13, v21

    #@db
    .line 732
    .local v13, left:F
    move/from16 v20, v25

    #@dd
    .line 738
    .local v20, right:F
    :goto_dd
    cmpg-float v27, v22, v26

    #@df
    if-gez v27, :cond_1f1

    #@e1
    .line 739
    move/from16 v23, v22

    #@e3
    .line 740
    .local v23, top:F
    move/from16 v4, v26

    #@e5
    .line 747
    .local v4, bottom:F
    :goto_e5
    sub-float v27, v13, v19

    #@e7
    move/from16 v0, v27

    #@e9
    float-to-int v0, v0

    #@ea
    move/from16 v27, v0

    #@ec
    sub-float v28, v23, v19

    #@ee
    move/from16 v0, v28

    #@f0
    float-to-int v0, v0

    #@f1
    move/from16 v28, v0

    #@f3
    add-float v29, v20, v19

    #@f5
    move/from16 v0, v29

    #@f7
    float-to-int v0, v0

    #@f8
    move/from16 v29, v0

    #@fa
    add-float v30, v4, v19

    #@fc
    move/from16 v0, v30

    #@fe
    float-to-int v0, v0

    #@ff
    move/from16 v30, v0

    #@101
    move/from16 v0, v27

    #@103
    move/from16 v1, v28

    #@105
    move/from16 v2, v29

    #@107
    move/from16 v3, v30

    #@109
    invoke-virtual {v11, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    #@10c
    .line 750
    cmpg-float v27, v21, v14

    #@10e
    if-gez v27, :cond_1f7

    #@110
    .line 751
    move/from16 v13, v21

    #@112
    .line 752
    move/from16 v20, v14

    #@114
    .line 758
    :goto_114
    cmpg-float v27, v22, v15

    #@116
    if-gez v27, :cond_1fc

    #@118
    .line 759
    move/from16 v23, v22

    #@11a
    .line 760
    move v4, v15

    #@11b
    .line 767
    :goto_11b
    sub-float v27, v13, v19

    #@11d
    move/from16 v0, v27

    #@11f
    float-to-int v0, v0

    #@120
    move/from16 v27, v0

    #@122
    sub-float v28, v23, v19

    #@124
    move/from16 v0, v28

    #@126
    float-to-int v0, v0

    #@127
    move/from16 v28, v0

    #@129
    add-float v29, v20, v19

    #@12b
    move/from16 v0, v29

    #@12d
    float-to-int v0, v0

    #@12e
    move/from16 v29, v0

    #@130
    add-float v30, v4, v19

    #@132
    move/from16 v0, v30

    #@134
    float-to-int v0, v0

    #@135
    move/from16 v30, v0

    #@137
    move/from16 v0, v27

    #@139
    move/from16 v1, v28

    #@13b
    move/from16 v2, v29

    #@13d
    move/from16 v3, v30

    #@13f
    invoke-virtual {v11, v0, v1, v2, v3}, Landroid/graphics/Rect;->union(IIII)V

    #@142
    .line 771
    if-eqz v9, :cond_1d6

    #@144
    .line 772
    iget v0, v9, Lcom/android/internal/widget/LockPatternView$Cell;->column:I

    #@146
    move/from16 v27, v0

    #@148
    move-object/from16 v0, p0

    #@14a
    move/from16 v1, v27

    #@14c
    invoke-direct {v0, v1}, Lcom/android/internal/widget/LockPatternView;->getCenterXForColumn(I)F

    #@14f
    move-result v21

    #@150
    .line 773
    iget v0, v9, Lcom/android/internal/widget/LockPatternView$Cell;->row:I

    #@152
    move/from16 v27, v0

    #@154
    move-object/from16 v0, p0

    #@156
    move/from16 v1, v27

    #@158
    invoke-direct {v0, v1}, Lcom/android/internal/widget/LockPatternView;->getCenterYForRow(I)F

    #@15b
    move-result v22

    #@15c
    .line 775
    const/16 v27, 0x2

    #@15e
    move/from16 v0, v17

    #@160
    move/from16 v1, v27

    #@162
    if-lt v0, v1, :cond_20b

    #@164
    .line 777
    add-int/lit8 v27, v17, -0x1

    #@166
    sub-int v28, v17, v18

    #@168
    sub-int v27, v27, v28

    #@16a
    move-object/from16 v0, v16

    #@16c
    move/from16 v1, v27

    #@16e
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@171
    move-result-object v9

    #@172
    .end local v9           #hitCell:Lcom/android/internal/widget/LockPatternView$Cell;
    check-cast v9, Lcom/android/internal/widget/LockPatternView$Cell;

    #@174
    .line 778
    .restart local v9       #hitCell:Lcom/android/internal/widget/LockPatternView$Cell;
    iget v0, v9, Lcom/android/internal/widget/LockPatternView$Cell;->column:I

    #@176
    move/from16 v27, v0

    #@178
    move-object/from16 v0, p0

    #@17a
    move/from16 v1, v27

    #@17c
    invoke-direct {v0, v1}, Lcom/android/internal/widget/LockPatternView;->getCenterXForColumn(I)F

    #@17f
    move-result v14

    #@180
    .line 779
    iget v0, v9, Lcom/android/internal/widget/LockPatternView$Cell;->row:I

    #@182
    move/from16 v27, v0

    #@184
    move-object/from16 v0, p0

    #@186
    move/from16 v1, v27

    #@188
    invoke-direct {v0, v1}, Lcom/android/internal/widget/LockPatternView;->getCenterYForRow(I)F

    #@18b
    move-result v15

    #@18c
    .line 781
    cmpg-float v27, v21, v14

    #@18e
    if-gez v27, :cond_202

    #@190
    .line 782
    move/from16 v13, v21

    #@192
    .line 783
    move/from16 v20, v14

    #@194
    .line 789
    :goto_194
    cmpg-float v27, v22, v15

    #@196
    if-gez v27, :cond_206

    #@198
    .line 790
    move/from16 v23, v22

    #@19a
    .line 791
    move v4, v15

    #@19b
    .line 801
    :goto_19b
    move-object/from16 v0, p0

    #@19d
    iget v0, v0, Lcom/android/internal/widget/LockPatternView;->mSquareWidth:F

    #@19f
    move/from16 v27, v0

    #@1a1
    const/high16 v28, 0x4000

    #@1a3
    div-float v24, v27, v28

    #@1a5
    .line 802
    .local v24, widthOffset:F
    move-object/from16 v0, p0

    #@1a7
    iget v0, v0, Lcom/android/internal/widget/LockPatternView;->mSquareHeight:F

    #@1a9
    move/from16 v27, v0

    #@1ab
    const/high16 v28, 0x4000

    #@1ad
    div-float v7, v27, v28

    #@1af
    .line 804
    .local v7, heightOffset:F
    sub-float v27, v13, v24

    #@1b1
    move/from16 v0, v27

    #@1b3
    float-to-int v0, v0

    #@1b4
    move/from16 v27, v0

    #@1b6
    sub-float v28, v23, v7

    #@1b8
    move/from16 v0, v28

    #@1ba
    float-to-int v0, v0

    #@1bb
    move/from16 v28, v0

    #@1bd
    add-float v29, v20, v24

    #@1bf
    move/from16 v0, v29

    #@1c1
    float-to-int v0, v0

    #@1c2
    move/from16 v29, v0

    #@1c4
    add-float v30, v4, v7

    #@1c6
    move/from16 v0, v30

    #@1c8
    float-to-int v0, v0

    #@1c9
    move/from16 v30, v0

    #@1cb
    move/from16 v0, v27

    #@1cd
    move/from16 v1, v28

    #@1cf
    move/from16 v2, v29

    #@1d1
    move/from16 v3, v30

    #@1d3
    invoke-virtual {v11, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    #@1d6
    .line 809
    .end local v7           #heightOffset:F
    .end local v24           #widthOffset:F
    :cond_1d6
    move-object/from16 v0, p0

    #@1d8
    invoke-virtual {v0, v11}, Lcom/android/internal/widget/LockPatternView;->invalidate(Landroid/graphics/Rect;)V

    #@1db
    .line 694
    .end local v4           #bottom:F
    .end local v11           #invalidateRect:Landroid/graphics/Rect;
    .end local v12           #lastCell:Lcom/android/internal/widget/LockPatternView$Cell;
    .end local v13           #left:F
    .end local v14           #oldX:F
    .end local v15           #oldY:F
    .end local v16           #pattern:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/widget/LockPatternView$Cell;>;"
    .end local v19           #radius:F
    .end local v20           #right:F
    .end local v21           #startX:F
    .end local v22           #startY:F
    .end local v23           #top:F
    :cond_1db
    :goto_1db
    add-int/lit8 v10, v10, 0x1

    #@1dd
    goto/16 :goto_5

    #@1df
    .line 695
    .end local v5           #dx:F
    .end local v6           #dy:F
    .end local v9           #hitCell:Lcom/android/internal/widget/LockPatternView$Cell;
    .end local v17           #patternSize:I
    .end local v18           #patternSizePreHitDetect:I
    .end local v25           #x:F
    .end local v26           #y:F
    :cond_1df
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    #@1e2
    move-result v25

    #@1e3
    goto/16 :goto_13

    #@1e5
    .line 696
    .restart local v25       #x:F
    :cond_1e5
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    #@1e8
    move-result v26

    #@1e9
    goto/16 :goto_1b

    #@1eb
    .line 734
    .restart local v5       #dx:F
    .restart local v6       #dy:F
    .restart local v9       #hitCell:Lcom/android/internal/widget/LockPatternView$Cell;
    .restart local v11       #invalidateRect:Landroid/graphics/Rect;
    .restart local v12       #lastCell:Lcom/android/internal/widget/LockPatternView$Cell;
    .restart local v14       #oldX:F
    .restart local v15       #oldY:F
    .restart local v16       #pattern:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/widget/LockPatternView$Cell;>;"
    .restart local v17       #patternSize:I
    .restart local v18       #patternSizePreHitDetect:I
    .restart local v19       #radius:F
    .restart local v21       #startX:F
    .restart local v22       #startY:F
    .restart local v26       #y:F
    :cond_1eb
    move/from16 v13, v25

    #@1ed
    .line 735
    .restart local v13       #left:F
    move/from16 v20, v21

    #@1ef
    .restart local v20       #right:F
    goto/16 :goto_dd

    #@1f1
    .line 742
    :cond_1f1
    move/from16 v23, v26

    #@1f3
    .line 743
    .restart local v23       #top:F
    move/from16 v4, v22

    #@1f5
    .restart local v4       #bottom:F
    goto/16 :goto_e5

    #@1f7
    .line 754
    :cond_1f7
    move v13, v14

    #@1f8
    .line 755
    move/from16 v20, v21

    #@1fa
    goto/16 :goto_114

    #@1fc
    .line 762
    :cond_1fc
    move/from16 v23, v15

    #@1fe
    .line 763
    move/from16 v4, v22

    #@200
    goto/16 :goto_11b

    #@202
    .line 785
    :cond_202
    move v13, v14

    #@203
    .line 786
    move/from16 v20, v21

    #@205
    goto :goto_194

    #@206
    .line 793
    :cond_206
    move/from16 v23, v15

    #@208
    .line 794
    move/from16 v4, v22

    #@20a
    goto :goto_19b

    #@20b
    .line 797
    :cond_20b
    move/from16 v20, v21

    #@20d
    move/from16 v13, v21

    #@20f
    .line 798
    move/from16 v4, v22

    #@211
    move/from16 v23, v22

    #@213
    goto :goto_19b

    #@214
    .line 811
    .end local v4           #bottom:F
    .end local v11           #invalidateRect:Landroid/graphics/Rect;
    .end local v12           #lastCell:Lcom/android/internal/widget/LockPatternView$Cell;
    .end local v13           #left:F
    .end local v16           #pattern:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/widget/LockPatternView$Cell;>;"
    .end local v19           #radius:F
    .end local v20           #right:F
    .end local v21           #startX:F
    .end local v22           #startY:F
    .end local v23           #top:F
    :cond_214
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/widget/LockPatternView;->invalidate()V

    #@217
    goto :goto_1db

    #@218
    .line 815
    .end local v5           #dx:F
    .end local v6           #dy:F
    .end local v9           #hitCell:Lcom/android/internal/widget/LockPatternView$Cell;
    .end local v14           #oldX:F
    .end local v15           #oldY:F
    .end local v17           #patternSize:I
    .end local v18           #patternSizePreHitDetect:I
    .end local v25           #x:F
    .end local v26           #y:F
    :cond_218
    return-void
.end method

.method private handleActionUp(Landroid/view/MotionEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 823
    iget-object v0, p0, Lcom/android/internal/widget/LockPatternView;->mPattern:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_11

    #@8
    .line 824
    const/4 v0, 0x0

    #@9
    iput-boolean v0, p0, Lcom/android/internal/widget/LockPatternView;->mPatternInProgress:Z

    #@b
    .line 825
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternView;->notifyPatternDetected()V

    #@e
    .line 826
    invoke-virtual {p0}, Lcom/android/internal/widget/LockPatternView;->invalidate()V

    #@11
    .line 834
    :cond_11
    return-void
.end method

.method private notifyCellAdded()V
    .registers 3

    #@0
    .prologue
    .line 392
    const v0, 0x1040344

    #@3
    invoke-direct {p0, v0}, Lcom/android/internal/widget/LockPatternView;->sendAccessEvent(I)V

    #@6
    .line 393
    iget-object v0, p0, Lcom/android/internal/widget/LockPatternView;->mOnPatternListener:Lcom/android/internal/widget/LockPatternView$OnPatternListener;

    #@8
    if-eqz v0, :cond_11

    #@a
    .line 394
    iget-object v0, p0, Lcom/android/internal/widget/LockPatternView;->mOnPatternListener:Lcom/android/internal/widget/LockPatternView$OnPatternListener;

    #@c
    iget-object v1, p0, Lcom/android/internal/widget/LockPatternView;->mPattern:Ljava/util/ArrayList;

    #@e
    invoke-interface {v0, v1}, Lcom/android/internal/widget/LockPatternView$OnPatternListener;->onPatternCellAdded(Ljava/util/List;)V

    #@11
    .line 396
    :cond_11
    return-void
.end method

.method private notifyPatternCleared()V
    .registers 2

    #@0
    .prologue
    .line 413
    const v0, 0x1040343

    #@3
    invoke-direct {p0, v0}, Lcom/android/internal/widget/LockPatternView;->sendAccessEvent(I)V

    #@6
    .line 414
    iget-object v0, p0, Lcom/android/internal/widget/LockPatternView;->mOnPatternListener:Lcom/android/internal/widget/LockPatternView$OnPatternListener;

    #@8
    if-eqz v0, :cond_f

    #@a
    .line 415
    iget-object v0, p0, Lcom/android/internal/widget/LockPatternView;->mOnPatternListener:Lcom/android/internal/widget/LockPatternView$OnPatternListener;

    #@c
    invoke-interface {v0}, Lcom/android/internal/widget/LockPatternView$OnPatternListener;->onPatternCleared()V

    #@f
    .line 417
    :cond_f
    return-void
.end method

.method private notifyPatternDetected()V
    .registers 3

    #@0
    .prologue
    .line 406
    const v0, 0x1040345

    #@3
    invoke-direct {p0, v0}, Lcom/android/internal/widget/LockPatternView;->sendAccessEvent(I)V

    #@6
    .line 407
    iget-object v0, p0, Lcom/android/internal/widget/LockPatternView;->mOnPatternListener:Lcom/android/internal/widget/LockPatternView$OnPatternListener;

    #@8
    if-eqz v0, :cond_11

    #@a
    .line 408
    iget-object v0, p0, Lcom/android/internal/widget/LockPatternView;->mOnPatternListener:Lcom/android/internal/widget/LockPatternView$OnPatternListener;

    #@c
    iget-object v1, p0, Lcom/android/internal/widget/LockPatternView;->mPattern:Ljava/util/ArrayList;

    #@e
    invoke-interface {v0, v1}, Lcom/android/internal/widget/LockPatternView$OnPatternListener;->onPatternDetected(Ljava/util/List;)V

    #@11
    .line 410
    :cond_11
    return-void
.end method

.method private notifyPatternStarted()V
    .registers 2

    #@0
    .prologue
    .line 399
    const v0, 0x1040342

    #@3
    invoke-direct {p0, v0}, Lcom/android/internal/widget/LockPatternView;->sendAccessEvent(I)V

    #@6
    .line 400
    iget-object v0, p0, Lcom/android/internal/widget/LockPatternView;->mOnPatternListener:Lcom/android/internal/widget/LockPatternView$OnPatternListener;

    #@8
    if-eqz v0, :cond_f

    #@a
    .line 401
    iget-object v0, p0, Lcom/android/internal/widget/LockPatternView;->mOnPatternListener:Lcom/android/internal/widget/LockPatternView$OnPatternListener;

    #@c
    invoke-interface {v0}, Lcom/android/internal/widget/LockPatternView$OnPatternListener;->onPatternStart()V

    #@f
    .line 403
    :cond_f
    return-void
.end method

.method private resetPattern()V
    .registers 2

    #@0
    .prologue
    .line 430
    iget-object v0, p0, Lcom/android/internal/widget/LockPatternView;->mPattern:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    #@5
    .line 431
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternView;->clearPatternDrawLookup()V

    #@8
    .line 432
    sget-object v0, Lcom/android/internal/widget/LockPatternView$DisplayMode;->Correct:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    #@a
    iput-object v0, p0, Lcom/android/internal/widget/LockPatternView;->mPatternDisplayMode:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    #@c
    .line 433
    invoke-virtual {p0}, Lcom/android/internal/widget/LockPatternView;->invalidate()V

    #@f
    .line 434
    return-void
.end method

.method private resolveMeasured(II)I
    .registers 6
    .parameter "measureSpec"
    .parameter "desired"

    #@0
    .prologue
    .line 473
    const/4 v0, 0x0

    #@1
    .line 474
    .local v0, result:I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@4
    move-result v1

    #@5
    .line 475
    .local v1, specSize:I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@8
    move-result v2

    #@9
    sparse-switch v2, :sswitch_data_16

    #@c
    .line 484
    move v0, v1

    #@d
    .line 486
    :goto_d
    return v0

    #@e
    .line 477
    :sswitch_e
    move v0, p2

    #@f
    .line 478
    goto :goto_d

    #@10
    .line 480
    :sswitch_10
    invoke-static {v1, p2}, Ljava/lang/Math;->max(II)I

    #@13
    move-result v0

    #@14
    .line 481
    goto :goto_d

    #@15
    .line 475
    nop

    #@16
    :sswitch_data_16
    .sparse-switch
        -0x80000000 -> :sswitch_10
        0x0 -> :sswitch_e
    .end sparse-switch
.end method

.method private sendAccessEvent(I)V
    .registers 3
    .parameter "resId"

    #@0
    .prologue
    .line 818
    iget-object v0, p0, Lcom/android/internal/widget/LockPatternView;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/LockPatternView;->announceForAccessibility(Ljava/lang/CharSequence;)V

    #@9
    .line 819
    return-void
.end method


# virtual methods
.method public clearPattern()V
    .registers 1

    #@0
    .prologue
    .line 423
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternView;->resetPattern()V

    #@3
    .line 424
    return-void
.end method

.method public disableInput()V
    .registers 2

    #@0
    .prologue
    .line 452
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Lcom/android/internal/widget/LockPatternView;->mInputEnabled:Z

    #@3
    .line 453
    return-void
.end method

.method public enableInput()V
    .registers 2

    #@0
    .prologue
    .line 459
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Lcom/android/internal/widget/LockPatternView;->mInputEnabled:Z

    #@3
    .line 460
    return-void
.end method

.method public getCenterXForLastColumn()F
    .registers 5

    #@0
    .prologue
    .line 871
    iget-object v1, p0, Lcom/android/internal/widget/LockPatternView;->mPattern:Ljava/util/ArrayList;

    #@2
    iget-object v2, p0, Lcom/android/internal/widget/LockPatternView;->mPattern:Ljava/util/ArrayList;

    #@4
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@7
    move-result v2

    #@8
    add-int/lit8 v2, v2, -0x1

    #@a
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@d
    move-result-object v0

    #@e
    check-cast v0, Lcom/android/internal/widget/LockPatternView$Cell;

    #@10
    .line 872
    .local v0, lastCell:Lcom/android/internal/widget/LockPatternView$Cell;
    iget v1, p0, Lcom/android/internal/widget/LockPatternView;->mPaddingLeft:I

    #@12
    int-to-float v1, v1

    #@13
    iget v2, v0, Lcom/android/internal/widget/LockPatternView$Cell;->column:I

    #@15
    int-to-float v2, v2

    #@16
    iget v3, p0, Lcom/android/internal/widget/LockPatternView;->mSquareWidth:F

    #@18
    mul-float/2addr v2, v3

    #@19
    add-float/2addr v1, v2

    #@1a
    iget v2, p0, Lcom/android/internal/widget/LockPatternView;->mSquareWidth:F

    #@1c
    const/high16 v3, 0x4000

    #@1e
    div-float/2addr v2, v3

    #@1f
    add-float/2addr v1, v2

    #@20
    return v1
.end method

.method public getCenterYForLastRow()F
    .registers 5

    #@0
    .prologue
    .line 876
    iget-object v1, p0, Lcom/android/internal/widget/LockPatternView;->mPattern:Ljava/util/ArrayList;

    #@2
    iget-object v2, p0, Lcom/android/internal/widget/LockPatternView;->mPattern:Ljava/util/ArrayList;

    #@4
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@7
    move-result v2

    #@8
    add-int/lit8 v2, v2, -0x1

    #@a
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@d
    move-result-object v0

    #@e
    check-cast v0, Lcom/android/internal/widget/LockPatternView$Cell;

    #@10
    .line 877
    .local v0, lastCell:Lcom/android/internal/widget/LockPatternView$Cell;
    iget v1, p0, Lcom/android/internal/widget/LockPatternView;->mPaddingTop:I

    #@12
    int-to-float v1, v1

    #@13
    iget v2, v0, Lcom/android/internal/widget/LockPatternView$Cell;->row:I

    #@15
    int-to-float v2, v2

    #@16
    iget v3, p0, Lcom/android/internal/widget/LockPatternView;->mSquareHeight:F

    #@18
    mul-float/2addr v2, v3

    #@19
    add-float/2addr v1, v2

    #@1a
    iget v2, p0, Lcom/android/internal/widget/LockPatternView;->mSquareHeight:F

    #@1c
    const/high16 v3, 0x4000

    #@1e
    div-float/2addr v2, v3

    #@1f
    add-float/2addr v1, v2

    #@20
    return v1
.end method

.method protected getSuggestedMinimumHeight()I
    .registers 2

    #@0
    .prologue
    .line 498
    iget v0, p0, Lcom/android/internal/widget/LockPatternView;->mBitmapWidth:I

    #@2
    mul-int/lit8 v0, v0, 0x3

    #@4
    return v0
.end method

.method protected getSuggestedMinimumWidth()I
    .registers 2

    #@0
    .prologue
    .line 492
    iget v0, p0, Lcom/android/internal/widget/LockPatternView;->mBitmapWidth:I

    #@2
    mul-int/lit8 v0, v0, 0x3

    #@4
    return v0
.end method

.method public isInStealthMode()Z
    .registers 2

    #@0
    .prologue
    .line 313
    iget-boolean v0, p0, Lcom/android/internal/widget/LockPatternView;->mInStealthMode:Z

    #@2
    return v0
.end method

.method public isTactileFeedbackEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 320
    iget-boolean v0, p0, Lcom/android/internal/widget/LockPatternView;->mEnableHapticFeedback:Z

    #@2
    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 38
    .parameter "canvas"

    #@0
    .prologue
    .line 891
    move-object/from16 v0, p0

    #@2
    iget-object v0, v0, Lcom/android/internal/widget/LockPatternView;->mPattern:Ljava/util/ArrayList;

    #@4
    move-object/from16 v28, v0

    #@6
    .line 892
    .local v28, pattern:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/widget/LockPatternView$Cell;>;"
    invoke-virtual/range {v28 .. v28}, Ljava/util/ArrayList;->size()I

    #@9
    move-result v12

    #@a
    .line 893
    .local v12, count:I
    move-object/from16 v0, p0

    #@c
    iget-object v15, v0, Lcom/android/internal/widget/LockPatternView;->mPatternDrawLookup:[[Z

    #@e
    .line 895
    .local v15, drawLookup:[[Z
    move-object/from16 v0, p0

    #@10
    iget-object v3, v0, Lcom/android/internal/widget/LockPatternView;->mPatternDisplayMode:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    #@12
    sget-object v4, Lcom/android/internal/widget/LockPatternView$DisplayMode;->Animate:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    #@14
    if-ne v3, v4, :cond_b7

    #@16
    .line 900
    add-int/lit8 v3, v12, 0x1

    #@18
    mul-int/lit16 v0, v3, 0x2bc

    #@1a
    move/from16 v25, v0

    #@1c
    .line 901
    .local v25, oneCycle:I
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@1f
    move-result-wide v3

    #@20
    move-object/from16 v0, p0

    #@22
    iget-wide v0, v0, Lcom/android/internal/widget/LockPatternView;->mAnimatingPeriodStart:J

    #@24
    move-wide/from16 v34, v0

    #@26
    sub-long v3, v3, v34

    #@28
    long-to-int v3, v3

    #@29
    rem-int v31, v3, v25

    #@2b
    .line 903
    .local v31, spotInCycle:I
    move/from16 v0, v31

    #@2d
    div-int/lit16 v0, v0, 0x2bc

    #@2f
    move/from16 v23, v0

    #@31
    .line 905
    .local v23, numCircles:I
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/widget/LockPatternView;->clearPatternDrawLookup()V

    #@34
    .line 906
    const/16 v19, 0x0

    #@36
    .local v19, i:I
    :goto_36
    move/from16 v0, v19

    #@38
    move/from16 v1, v23

    #@3a
    if-ge v0, v1, :cond_57

    #@3c
    .line 907
    move-object/from16 v0, v28

    #@3e
    move/from16 v1, v19

    #@40
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@43
    move-result-object v7

    #@44
    check-cast v7, Lcom/android/internal/widget/LockPatternView$Cell;

    #@46
    .line 908
    .local v7, cell:Lcom/android/internal/widget/LockPatternView$Cell;
    invoke-virtual {v7}, Lcom/android/internal/widget/LockPatternView$Cell;->getRow()I

    #@49
    move-result v3

    #@4a
    aget-object v3, v15, v3

    #@4c
    invoke-virtual {v7}, Lcom/android/internal/widget/LockPatternView$Cell;->getColumn()I

    #@4f
    move-result v4

    #@50
    const/16 v34, 0x1

    #@52
    aput-boolean v34, v3, v4

    #@54
    .line 906
    add-int/lit8 v19, v19, 0x1

    #@56
    goto :goto_36

    #@57
    .line 913
    .end local v7           #cell:Lcom/android/internal/widget/LockPatternView$Cell;
    :cond_57
    if-lez v23, :cond_11d

    #@59
    move/from16 v0, v23

    #@5b
    if-ge v0, v12, :cond_11d

    #@5d
    const/16 v21, 0x1

    #@5f
    .line 916
    .local v21, needToUpdateInProgressPoint:Z
    :goto_5f
    if-eqz v21, :cond_b4

    #@61
    .line 917
    move/from16 v0, v31

    #@63
    rem-int/lit16 v3, v0, 0x2bc

    #@65
    int-to-float v3, v3

    #@66
    const/high16 v4, 0x442f

    #@68
    div-float v29, v3, v4

    #@6a
    .line 921
    .local v29, percentageOfNextCircle:F
    add-int/lit8 v3, v23, -0x1

    #@6c
    move-object/from16 v0, v28

    #@6e
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@71
    move-result-object v13

    #@72
    check-cast v13, Lcom/android/internal/widget/LockPatternView$Cell;

    #@74
    .line 922
    .local v13, currentCell:Lcom/android/internal/widget/LockPatternView$Cell;
    iget v3, v13, Lcom/android/internal/widget/LockPatternView$Cell;->column:I

    #@76
    move-object/from16 v0, p0

    #@78
    invoke-direct {v0, v3}, Lcom/android/internal/widget/LockPatternView;->getCenterXForColumn(I)F

    #@7b
    move-result v10

    #@7c
    .line 923
    .local v10, centerX:F
    iget v3, v13, Lcom/android/internal/widget/LockPatternView$Cell;->row:I

    #@7e
    move-object/from16 v0, p0

    #@80
    invoke-direct {v0, v3}, Lcom/android/internal/widget/LockPatternView;->getCenterYForRow(I)F

    #@83
    move-result v11

    #@84
    .line 925
    .local v11, centerY:F
    move-object/from16 v0, v28

    #@86
    move/from16 v1, v23

    #@88
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@8b
    move-result-object v22

    #@8c
    check-cast v22, Lcom/android/internal/widget/LockPatternView$Cell;

    #@8e
    .line 926
    .local v22, nextCell:Lcom/android/internal/widget/LockPatternView$Cell;
    move-object/from16 v0, v22

    #@90
    iget v3, v0, Lcom/android/internal/widget/LockPatternView$Cell;->column:I

    #@92
    move-object/from16 v0, p0

    #@94
    invoke-direct {v0, v3}, Lcom/android/internal/widget/LockPatternView;->getCenterXForColumn(I)F

    #@97
    move-result v3

    #@98
    sub-float/2addr v3, v10

    #@99
    mul-float v17, v29, v3

    #@9b
    .line 928
    .local v17, dx:F
    move-object/from16 v0, v22

    #@9d
    iget v3, v0, Lcom/android/internal/widget/LockPatternView$Cell;->row:I

    #@9f
    move-object/from16 v0, p0

    #@a1
    invoke-direct {v0, v3}, Lcom/android/internal/widget/LockPatternView;->getCenterYForRow(I)F

    #@a4
    move-result v3

    #@a5
    sub-float/2addr v3, v11

    #@a6
    mul-float v18, v29, v3

    #@a8
    .line 930
    .local v18, dy:F
    add-float v3, v10, v17

    #@aa
    move-object/from16 v0, p0

    #@ac
    iput v3, v0, Lcom/android/internal/widget/LockPatternView;->mInProgressX:F

    #@ae
    .line 931
    add-float v3, v11, v18

    #@b0
    move-object/from16 v0, p0

    #@b2
    iput v3, v0, Lcom/android/internal/widget/LockPatternView;->mInProgressY:F

    #@b4
    .line 934
    .end local v10           #centerX:F
    .end local v11           #centerY:F
    .end local v13           #currentCell:Lcom/android/internal/widget/LockPatternView$Cell;
    .end local v17           #dx:F
    .end local v18           #dy:F
    .end local v22           #nextCell:Lcom/android/internal/widget/LockPatternView$Cell;
    .end local v29           #percentageOfNextCircle:F
    :cond_b4
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/widget/LockPatternView;->invalidate()V

    #@b7
    .line 937
    .end local v19           #i:I
    .end local v21           #needToUpdateInProgressPoint:Z
    .end local v23           #numCircles:I
    .end local v25           #oneCycle:I
    .end local v31           #spotInCycle:I
    :cond_b7
    move-object/from16 v0, p0

    #@b9
    iget v0, v0, Lcom/android/internal/widget/LockPatternView;->mSquareWidth:F

    #@bb
    move/from16 v33, v0

    #@bd
    .line 938
    .local v33, squareWidth:F
    move-object/from16 v0, p0

    #@bf
    iget v0, v0, Lcom/android/internal/widget/LockPatternView;->mSquareHeight:F

    #@c1
    move/from16 v32, v0

    #@c3
    .line 940
    .local v32, squareHeight:F
    move-object/from16 v0, p0

    #@c5
    iget v3, v0, Lcom/android/internal/widget/LockPatternView;->mDiameterFactor:F

    #@c7
    mul-float v3, v3, v33

    #@c9
    const/high16 v4, 0x3f00

    #@cb
    mul-float v30, v3, v4

    #@cd
    .line 941
    .local v30, radius:F
    move-object/from16 v0, p0

    #@cf
    iget-object v3, v0, Lcom/android/internal/widget/LockPatternView;->mPathPaint:Landroid/graphics/Paint;

    #@d1
    move/from16 v0, v30

    #@d3
    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    #@d6
    .line 943
    move-object/from16 v0, p0

    #@d8
    iget-object v14, v0, Lcom/android/internal/widget/LockPatternView;->mCurrentPath:Landroid/graphics/Path;

    #@da
    .line 944
    .local v14, currentPath:Landroid/graphics/Path;
    invoke-virtual {v14}, Landroid/graphics/Path;->rewind()V

    #@dd
    .line 947
    move-object/from16 v0, p0

    #@df
    iget v0, v0, Lcom/android/internal/widget/LockPatternView;->mPaddingTop:I

    #@e1
    move/from16 v27, v0

    #@e3
    .line 948
    .local v27, paddingTop:I
    move-object/from16 v0, p0

    #@e5
    iget v0, v0, Lcom/android/internal/widget/LockPatternView;->mPaddingLeft:I

    #@e7
    move/from16 v26, v0

    #@e9
    .line 950
    .local v26, paddingLeft:I
    const/16 v19, 0x0

    #@eb
    .restart local v19       #i:I
    :goto_eb
    const/4 v3, 0x3

    #@ec
    move/from16 v0, v19

    #@ee
    if-ge v0, v3, :cond_124

    #@f0
    .line 951
    move/from16 v0, v27

    #@f2
    int-to-float v3, v0

    #@f3
    move/from16 v0, v19

    #@f5
    int-to-float v4, v0

    #@f6
    mul-float v4, v4, v32

    #@f8
    add-float v6, v3, v4

    #@fa
    .line 953
    .local v6, topY:F
    const/16 v20, 0x0

    #@fc
    .local v20, j:I
    :goto_fc
    const/4 v3, 0x3

    #@fd
    move/from16 v0, v20

    #@ff
    if-ge v0, v3, :cond_121

    #@101
    .line 954
    move/from16 v0, v26

    #@103
    int-to-float v3, v0

    #@104
    move/from16 v0, v20

    #@106
    int-to-float v4, v0

    #@107
    mul-float v4, v4, v33

    #@109
    add-float v5, v3, v4

    #@10b
    .line 955
    .local v5, leftX:F
    float-to-int v3, v5

    #@10c
    float-to-int v4, v6

    #@10d
    aget-object v34, v15, v19

    #@10f
    aget-boolean v34, v34, v20

    #@111
    move-object/from16 v0, p0

    #@113
    move-object/from16 v1, p1

    #@115
    move/from16 v2, v34

    #@117
    invoke-direct {v0, v1, v3, v4, v2}, Lcom/android/internal/widget/LockPatternView;->drawCircle(Landroid/graphics/Canvas;IIZ)V

    #@11a
    .line 953
    add-int/lit8 v20, v20, 0x1

    #@11c
    goto :goto_fc

    #@11d
    .line 913
    .end local v5           #leftX:F
    .end local v6           #topY:F
    .end local v14           #currentPath:Landroid/graphics/Path;
    .end local v20           #j:I
    .end local v26           #paddingLeft:I
    .end local v27           #paddingTop:I
    .end local v30           #radius:F
    .end local v32           #squareHeight:F
    .end local v33           #squareWidth:F
    .restart local v23       #numCircles:I
    .restart local v25       #oneCycle:I
    .restart local v31       #spotInCycle:I
    :cond_11d
    const/16 v21, 0x0

    #@11f
    goto/16 :goto_5f

    #@121
    .line 950
    .end local v23           #numCircles:I
    .end local v25           #oneCycle:I
    .end local v31           #spotInCycle:I
    .restart local v6       #topY:F
    .restart local v14       #currentPath:Landroid/graphics/Path;
    .restart local v20       #j:I
    .restart local v26       #paddingLeft:I
    .restart local v27       #paddingTop:I
    .restart local v30       #radius:F
    .restart local v32       #squareHeight:F
    .restart local v33       #squareWidth:F
    :cond_121
    add-int/lit8 v19, v19, 0x1

    #@123
    goto :goto_eb

    #@124
    .line 963
    .end local v6           #topY:F
    .end local v20           #j:I
    :cond_124
    move-object/from16 v0, p0

    #@126
    iget-boolean v3, v0, Lcom/android/internal/widget/LockPatternView;->mInStealthMode:Z

    #@128
    if-eqz v3, :cond_132

    #@12a
    move-object/from16 v0, p0

    #@12c
    iget-object v3, v0, Lcom/android/internal/widget/LockPatternView;->mPatternDisplayMode:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    #@12e
    sget-object v4, Lcom/android/internal/widget/LockPatternView$DisplayMode;->Wrong:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    #@130
    if-ne v3, v4, :cond_1bd

    #@132
    :cond_132
    const/16 v16, 0x1

    #@134
    .line 967
    .local v16, drawPath:Z
    :goto_134
    move-object/from16 v0, p0

    #@136
    iget-object v3, v0, Lcom/android/internal/widget/LockPatternView;->mPaint:Landroid/graphics/Paint;

    #@138
    invoke-virtual {v3}, Landroid/graphics/Paint;->getFlags()I

    #@13b
    move-result v3

    #@13c
    and-int/lit8 v3, v3, 0x2

    #@13e
    if-eqz v3, :cond_1c1

    #@140
    const/16 v24, 0x1

    #@142
    .line 968
    .local v24, oldFlag:Z
    :goto_142
    move-object/from16 v0, p0

    #@144
    iget-object v3, v0, Lcom/android/internal/widget/LockPatternView;->mPaint:Landroid/graphics/Paint;

    #@146
    const/4 v4, 0x1

    #@147
    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    #@14a
    .line 969
    if-eqz v16, :cond_172

    #@14c
    .line 970
    const/16 v19, 0x0

    #@14e
    :goto_14e
    add-int/lit8 v3, v12, -0x1

    #@150
    move/from16 v0, v19

    #@152
    if-ge v0, v3, :cond_172

    #@154
    .line 971
    move-object/from16 v0, v28

    #@156
    move/from16 v1, v19

    #@158
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@15b
    move-result-object v7

    #@15c
    check-cast v7, Lcom/android/internal/widget/LockPatternView$Cell;

    #@15e
    .line 972
    .restart local v7       #cell:Lcom/android/internal/widget/LockPatternView$Cell;
    add-int/lit8 v3, v19, 0x1

    #@160
    move-object/from16 v0, v28

    #@162
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@165
    move-result-object v8

    #@166
    check-cast v8, Lcom/android/internal/widget/LockPatternView$Cell;

    #@168
    .line 977
    .local v8, next:Lcom/android/internal/widget/LockPatternView$Cell;
    iget v3, v8, Lcom/android/internal/widget/LockPatternView$Cell;->row:I

    #@16a
    aget-object v3, v15, v3

    #@16c
    iget v4, v8, Lcom/android/internal/widget/LockPatternView$Cell;->column:I

    #@16e
    aget-boolean v3, v3, v4

    #@170
    if-nez v3, :cond_1c5

    #@172
    .line 988
    .end local v7           #cell:Lcom/android/internal/widget/LockPatternView$Cell;
    .end local v8           #next:Lcom/android/internal/widget/LockPatternView$Cell;
    :cond_172
    if-eqz v16, :cond_1b3

    #@174
    .line 989
    const/4 v9, 0x0

    #@175
    .line 990
    .local v9, anyCircles:Z
    const/16 v19, 0x0

    #@177
    :goto_177
    move/from16 v0, v19

    #@179
    if-ge v0, v12, :cond_18f

    #@17b
    .line 991
    move-object/from16 v0, v28

    #@17d
    move/from16 v1, v19

    #@17f
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@182
    move-result-object v7

    #@183
    check-cast v7, Lcom/android/internal/widget/LockPatternView$Cell;

    #@185
    .line 996
    .restart local v7       #cell:Lcom/android/internal/widget/LockPatternView$Cell;
    iget v3, v7, Lcom/android/internal/widget/LockPatternView$Cell;->row:I

    #@187
    aget-object v3, v15, v3

    #@189
    iget v4, v7, Lcom/android/internal/widget/LockPatternView$Cell;->column:I

    #@18b
    aget-boolean v3, v3, v4

    #@18d
    if-nez v3, :cond_1e4

    #@18f
    .line 1011
    .end local v7           #cell:Lcom/android/internal/widget/LockPatternView$Cell;
    :cond_18f
    move-object/from16 v0, p0

    #@191
    iget-boolean v3, v0, Lcom/android/internal/widget/LockPatternView;->mPatternInProgress:Z

    #@193
    if-nez v3, :cond_19d

    #@195
    move-object/from16 v0, p0

    #@197
    iget-object v3, v0, Lcom/android/internal/widget/LockPatternView;->mPatternDisplayMode:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    #@199
    sget-object v4, Lcom/android/internal/widget/LockPatternView$DisplayMode;->Animate:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    #@19b
    if-ne v3, v4, :cond_1aa

    #@19d
    :cond_19d
    if-eqz v9, :cond_1aa

    #@19f
    .line 1013
    move-object/from16 v0, p0

    #@1a1
    iget v3, v0, Lcom/android/internal/widget/LockPatternView;->mInProgressX:F

    #@1a3
    move-object/from16 v0, p0

    #@1a5
    iget v4, v0, Lcom/android/internal/widget/LockPatternView;->mInProgressY:F

    #@1a7
    invoke-virtual {v14, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    #@1aa
    .line 1015
    :cond_1aa
    move-object/from16 v0, p0

    #@1ac
    iget-object v3, v0, Lcom/android/internal/widget/LockPatternView;->mPathPaint:Landroid/graphics/Paint;

    #@1ae
    move-object/from16 v0, p1

    #@1b0
    invoke-virtual {v0, v14, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    #@1b3
    .line 1018
    .end local v9           #anyCircles:Z
    :cond_1b3
    move-object/from16 v0, p0

    #@1b5
    iget-object v3, v0, Lcom/android/internal/widget/LockPatternView;->mPaint:Landroid/graphics/Paint;

    #@1b7
    move/from16 v0, v24

    #@1b9
    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    #@1bc
    .line 1019
    return-void

    #@1bd
    .line 963
    .end local v16           #drawPath:Z
    .end local v24           #oldFlag:Z
    :cond_1bd
    const/16 v16, 0x0

    #@1bf
    goto/16 :goto_134

    #@1c1
    .line 967
    .restart local v16       #drawPath:Z
    :cond_1c1
    const/16 v24, 0x0

    #@1c3
    goto/16 :goto_142

    #@1c5
    .line 981
    .restart local v7       #cell:Lcom/android/internal/widget/LockPatternView$Cell;
    .restart local v8       #next:Lcom/android/internal/widget/LockPatternView$Cell;
    .restart local v24       #oldFlag:Z
    :cond_1c5
    move/from16 v0, v26

    #@1c7
    int-to-float v3, v0

    #@1c8
    iget v4, v7, Lcom/android/internal/widget/LockPatternView$Cell;->column:I

    #@1ca
    int-to-float v4, v4

    #@1cb
    mul-float v4, v4, v33

    #@1cd
    add-float v5, v3, v4

    #@1cf
    .line 982
    .restart local v5       #leftX:F
    move/from16 v0, v27

    #@1d1
    int-to-float v3, v0

    #@1d2
    iget v4, v7, Lcom/android/internal/widget/LockPatternView$Cell;->row:I

    #@1d4
    int-to-float v4, v4

    #@1d5
    mul-float v4, v4, v32

    #@1d7
    add-float v6, v3, v4

    #@1d9
    .restart local v6       #topY:F
    move-object/from16 v3, p0

    #@1db
    move-object/from16 v4, p1

    #@1dd
    .line 984
    invoke-direct/range {v3 .. v8}, Lcom/android/internal/widget/LockPatternView;->drawArrow(Landroid/graphics/Canvas;FFLcom/android/internal/widget/LockPatternView$Cell;Lcom/android/internal/widget/LockPatternView$Cell;)V

    #@1e0
    .line 970
    add-int/lit8 v19, v19, 0x1

    #@1e2
    goto/16 :goto_14e

    #@1e4
    .line 999
    .end local v5           #leftX:F
    .end local v6           #topY:F
    .end local v8           #next:Lcom/android/internal/widget/LockPatternView$Cell;
    .restart local v9       #anyCircles:Z
    :cond_1e4
    const/4 v9, 0x1

    #@1e5
    .line 1001
    iget v3, v7, Lcom/android/internal/widget/LockPatternView$Cell;->column:I

    #@1e7
    move-object/from16 v0, p0

    #@1e9
    invoke-direct {v0, v3}, Lcom/android/internal/widget/LockPatternView;->getCenterXForColumn(I)F

    #@1ec
    move-result v10

    #@1ed
    .line 1002
    .restart local v10       #centerX:F
    iget v3, v7, Lcom/android/internal/widget/LockPatternView$Cell;->row:I

    #@1ef
    move-object/from16 v0, p0

    #@1f1
    invoke-direct {v0, v3}, Lcom/android/internal/widget/LockPatternView;->getCenterYForRow(I)F

    #@1f4
    move-result v11

    #@1f5
    .line 1003
    .restart local v11       #centerY:F
    if-nez v19, :cond_1fe

    #@1f7
    .line 1004
    invoke-virtual {v14, v10, v11}, Landroid/graphics/Path;->moveTo(FF)V

    #@1fa
    .line 990
    :goto_1fa
    add-int/lit8 v19, v19, 0x1

    #@1fc
    goto/16 :goto_177

    #@1fe
    .line 1006
    :cond_1fe
    invoke-virtual {v14, v10, v11}, Landroid/graphics/Path;->lineTo(FF)V

    #@201
    goto :goto_1fa
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .registers 4
    .parameter "event"

    #@0
    .prologue
    .line 638
    iget-object v1, p0, Lcom/android/internal/widget/LockPatternView;->mContext:Landroid/content/Context;

    #@2
    invoke-static {v1}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_19

    #@c
    .line 639
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@f
    move-result v0

    #@10
    .line 640
    .local v0, action:I
    packed-switch v0, :pswitch_data_2e

    #@13
    .line 651
    :goto_13
    :pswitch_13
    invoke-virtual {p0, p1}, Lcom/android/internal/widget/LockPatternView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    #@16
    .line 652
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->setAction(I)V

    #@19
    .line 654
    .end local v0           #action:I
    :cond_19
    invoke-super {p0, p1}, Landroid/view/View;->onHoverEvent(Landroid/view/MotionEvent;)Z

    #@1c
    move-result v1

    #@1d
    return v1

    #@1e
    .line 642
    .restart local v0       #action:I
    :pswitch_1e
    const/4 v1, 0x0

    #@1f
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->setAction(I)V

    #@22
    goto :goto_13

    #@23
    .line 645
    :pswitch_23
    const/4 v1, 0x2

    #@24
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->setAction(I)V

    #@27
    goto :goto_13

    #@28
    .line 648
    :pswitch_28
    const/4 v1, 0x1

    #@29
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->setAction(I)V

    #@2c
    goto :goto_13

    #@2d
    .line 640
    nop

    #@2e
    :pswitch_data_2e
    .packed-switch 0x7
        :pswitch_23
        :pswitch_13
        :pswitch_1e
        :pswitch_28
    .end packed-switch
.end method

.method protected onMeasure(II)V
    .registers 8
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    .line 503
    invoke-virtual {p0}, Lcom/android/internal/widget/LockPatternView;->getSuggestedMinimumWidth()I

    #@3
    move-result v1

    #@4
    .line 504
    .local v1, minimumWidth:I
    invoke-virtual {p0}, Lcom/android/internal/widget/LockPatternView;->getSuggestedMinimumHeight()I

    #@7
    move-result v0

    #@8
    .line 505
    .local v0, minimumHeight:I
    invoke-direct {p0, p1, v1}, Lcom/android/internal/widget/LockPatternView;->resolveMeasured(II)I

    #@b
    move-result v3

    #@c
    .line 506
    .local v3, viewWidth:I
    invoke-direct {p0, p2, v0}, Lcom/android/internal/widget/LockPatternView;->resolveMeasured(II)I

    #@f
    move-result v2

    #@10
    .line 508
    .local v2, viewHeight:I
    iget v4, p0, Lcom/android/internal/widget/LockPatternView;->mAspect:I

    #@12
    packed-switch v4, :pswitch_data_2a

    #@15
    .line 520
    :goto_15
    invoke-virtual {p0, v3, v2}, Lcom/android/internal/widget/LockPatternView;->setMeasuredDimension(II)V

    #@18
    .line 521
    return-void

    #@19
    .line 510
    :pswitch_19
    invoke-static {v3, v2}, Ljava/lang/Math;->min(II)I

    #@1c
    move-result v2

    #@1d
    move v3, v2

    #@1e
    .line 511
    goto :goto_15

    #@1f
    .line 513
    :pswitch_1f
    invoke-static {v3, v2}, Ljava/lang/Math;->min(II)I

    #@22
    move-result v2

    #@23
    .line 514
    goto :goto_15

    #@24
    .line 516
    :pswitch_24
    invoke-static {v3, v2}, Ljava/lang/Math;->min(II)I

    #@27
    move-result v3

    #@28
    goto :goto_15

    #@29
    .line 508
    nop

    #@2a
    :pswitch_data_2a
    .packed-switch 0x0
        :pswitch_19
        :pswitch_1f
        :pswitch_24
    .end packed-switch
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .registers 5
    .parameter "state"

    #@0
    .prologue
    .line 1121
    move-object v0, p1

    #@1
    check-cast v0, Lcom/android/internal/widget/LockPatternView$SavedState;

    #@3
    .line 1122
    .local v0, ss:Lcom/android/internal/widget/LockPatternView$SavedState;
    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternView$SavedState;->getSuperState()Landroid/os/Parcelable;

    #@6
    move-result-object v1

    #@7
    invoke-super {p0, v1}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    #@a
    .line 1123
    sget-object v1, Lcom/android/internal/widget/LockPatternView$DisplayMode;->Correct:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    #@c
    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternView$SavedState;->getSerializedPattern()Ljava/lang/String;

    #@f
    move-result-object v2

    #@10
    invoke-static {v2}, Lcom/android/internal/widget/LockPatternUtils;->stringToPattern(Ljava/lang/String;)Ljava/util/List;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/widget/LockPatternView;->setPattern(Lcom/android/internal/widget/LockPatternView$DisplayMode;Ljava/util/List;)V

    #@17
    .line 1126
    invoke-static {}, Lcom/android/internal/widget/LockPatternView$DisplayMode;->values()[Lcom/android/internal/widget/LockPatternView$DisplayMode;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternView$SavedState;->getDisplayMode()I

    #@1e
    move-result v2

    #@1f
    aget-object v1, v1, v2

    #@21
    iput-object v1, p0, Lcom/android/internal/widget/LockPatternView;->mPatternDisplayMode:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    #@23
    .line 1127
    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternView$SavedState;->isInputEnabled()Z

    #@26
    move-result v1

    #@27
    iput-boolean v1, p0, Lcom/android/internal/widget/LockPatternView;->mInputEnabled:Z

    #@29
    .line 1128
    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternView$SavedState;->isInStealthMode()Z

    #@2c
    move-result v1

    #@2d
    iput-boolean v1, p0, Lcom/android/internal/widget/LockPatternView;->mInStealthMode:Z

    #@2f
    .line 1129
    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternView$SavedState;->isTactileFeedbackEnabled()Z

    #@32
    move-result v1

    #@33
    iput-boolean v1, p0, Lcom/android/internal/widget/LockPatternView;->mEnableHapticFeedback:Z

    #@35
    .line 1130
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .registers 9

    #@0
    .prologue
    .line 1112
    invoke-super {p0}, Landroid/view/View;->onSaveInstanceState()Landroid/os/Parcelable;

    #@3
    move-result-object v1

    #@4
    .line 1113
    .local v1, superState:Landroid/os/Parcelable;
    new-instance v0, Lcom/android/internal/widget/LockPatternView$SavedState;

    #@6
    iget-object v2, p0, Lcom/android/internal/widget/LockPatternView;->mPattern:Ljava/util/ArrayList;

    #@8
    invoke-static {v2}, Lcom/android/internal/widget/LockPatternUtils;->patternToString(Ljava/util/List;)Ljava/lang/String;

    #@b
    move-result-object v2

    #@c
    iget-object v3, p0, Lcom/android/internal/widget/LockPatternView;->mPatternDisplayMode:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    #@e
    invoke-virtual {v3}, Lcom/android/internal/widget/LockPatternView$DisplayMode;->ordinal()I

    #@11
    move-result v3

    #@12
    iget-boolean v4, p0, Lcom/android/internal/widget/LockPatternView;->mInputEnabled:Z

    #@14
    iget-boolean v5, p0, Lcom/android/internal/widget/LockPatternView;->mInStealthMode:Z

    #@16
    iget-boolean v6, p0, Lcom/android/internal/widget/LockPatternView;->mEnableHapticFeedback:Z

    #@18
    const/4 v7, 0x0

    #@19
    invoke-direct/range {v0 .. v7}, Lcom/android/internal/widget/LockPatternView$SavedState;-><init>(Landroid/os/Parcelable;Ljava/lang/String;IZZZLcom/android/internal/widget/LockPatternView$1;)V

    #@1c
    return-object v0
.end method

.method protected onSizeChanged(IIII)V
    .registers 10
    .parameter "w"
    .parameter "h"
    .parameter "oldw"
    .parameter "oldh"

    #@0
    .prologue
    const/high16 v4, 0x4040

    #@2
    .line 464
    iget v2, p0, Lcom/android/internal/widget/LockPatternView;->mPaddingLeft:I

    #@4
    sub-int v2, p1, v2

    #@6
    iget v3, p0, Lcom/android/internal/widget/LockPatternView;->mPaddingRight:I

    #@8
    sub-int v1, v2, v3

    #@a
    .line 465
    .local v1, width:I
    int-to-float v2, v1

    #@b
    div-float/2addr v2, v4

    #@c
    iput v2, p0, Lcom/android/internal/widget/LockPatternView;->mSquareWidth:F

    #@e
    .line 467
    iget v2, p0, Lcom/android/internal/widget/LockPatternView;->mPaddingTop:I

    #@10
    sub-int v2, p2, v2

    #@12
    iget v3, p0, Lcom/android/internal/widget/LockPatternView;->mPaddingBottom:I

    #@14
    sub-int v0, v2, v3

    #@16
    .line 468
    .local v0, height:I
    int-to-float v2, v0

    #@17
    div-float/2addr v2, v4

    #@18
    iput v2, p0, Lcom/android/internal/widget/LockPatternView;->mSquareHeight:F

    #@1a
    .line 469
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 5
    .parameter "event"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x1

    #@2
    .line 659
    iget-boolean v2, p0, Lcom/android/internal/widget/LockPatternView;->mInputEnabled:Z

    #@4
    if-eqz v2, :cond_c

    #@6
    invoke-virtual {p0}, Lcom/android/internal/widget/LockPatternView;->isEnabled()Z

    #@9
    move-result v2

    #@a
    if-nez v2, :cond_e

    #@c
    :cond_c
    move v0, v1

    #@d
    .line 687
    :cond_d
    :goto_d
    return v0

    #@e
    .line 663
    :cond_e
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@11
    move-result v2

    #@12
    packed-switch v2, :pswitch_data_30

    #@15
    move v0, v1

    #@16
    .line 687
    goto :goto_d

    #@17
    .line 665
    :pswitch_17
    invoke-direct {p0, p1}, Lcom/android/internal/widget/LockPatternView;->handleActionDown(Landroid/view/MotionEvent;)V

    #@1a
    goto :goto_d

    #@1b
    .line 668
    :pswitch_1b
    invoke-direct {p0, p1}, Lcom/android/internal/widget/LockPatternView;->handleActionUp(Landroid/view/MotionEvent;)V

    #@1e
    goto :goto_d

    #@1f
    .line 671
    :pswitch_1f
    invoke-direct {p0, p1}, Lcom/android/internal/widget/LockPatternView;->handleActionMove(Landroid/view/MotionEvent;)V

    #@22
    goto :goto_d

    #@23
    .line 674
    :pswitch_23
    iget-boolean v2, p0, Lcom/android/internal/widget/LockPatternView;->mPatternInProgress:Z

    #@25
    if-eqz v2, :cond_d

    #@27
    .line 675
    iput-boolean v1, p0, Lcom/android/internal/widget/LockPatternView;->mPatternInProgress:Z

    #@29
    .line 676
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternView;->resetPattern()V

    #@2c
    .line 677
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternView;->notifyPatternCleared()V

    #@2f
    goto :goto_d

    #@30
    .line 663
    :pswitch_data_30
    .packed-switch 0x0
        :pswitch_17
        :pswitch_1b
        :pswitch_1f
        :pswitch_23
    .end packed-switch
.end method

.method public setDisplayMode(Lcom/android/internal/widget/LockPatternView$DisplayMode;)V
    .registers 5
    .parameter "displayMode"

    #@0
    .prologue
    .line 376
    iput-object p1, p0, Lcom/android/internal/widget/LockPatternView;->mPatternDisplayMode:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    #@2
    .line 377
    sget-object v1, Lcom/android/internal/widget/LockPatternView$DisplayMode;->Animate:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    #@4
    if-ne p1, v1, :cond_3c

    #@6
    .line 378
    iget-object v1, p0, Lcom/android/internal/widget/LockPatternView;->mPattern:Ljava/util/ArrayList;

    #@8
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@b
    move-result v1

    #@c
    if-nez v1, :cond_16

    #@e
    .line 379
    new-instance v1, Ljava/lang/IllegalStateException;

    #@10
    const-string v2, "you must have a pattern to animate if you want to set the display mode to animate"

    #@12
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@15
    throw v1

    #@16
    .line 382
    :cond_16
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@19
    move-result-wide v1

    #@1a
    iput-wide v1, p0, Lcom/android/internal/widget/LockPatternView;->mAnimatingPeriodStart:J

    #@1c
    .line 383
    iget-object v1, p0, Lcom/android/internal/widget/LockPatternView;->mPattern:Ljava/util/ArrayList;

    #@1e
    const/4 v2, 0x0

    #@1f
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@22
    move-result-object v0

    #@23
    check-cast v0, Lcom/android/internal/widget/LockPatternView$Cell;

    #@25
    .line 384
    .local v0, first:Lcom/android/internal/widget/LockPatternView$Cell;
    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternView$Cell;->getColumn()I

    #@28
    move-result v1

    #@29
    invoke-direct {p0, v1}, Lcom/android/internal/widget/LockPatternView;->getCenterXForColumn(I)F

    #@2c
    move-result v1

    #@2d
    iput v1, p0, Lcom/android/internal/widget/LockPatternView;->mInProgressX:F

    #@2f
    .line 385
    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternView$Cell;->getRow()I

    #@32
    move-result v1

    #@33
    invoke-direct {p0, v1}, Lcom/android/internal/widget/LockPatternView;->getCenterYForRow(I)F

    #@36
    move-result v1

    #@37
    iput v1, p0, Lcom/android/internal/widget/LockPatternView;->mInProgressY:F

    #@39
    .line 386
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternView;->clearPatternDrawLookup()V

    #@3c
    .line 388
    .end local v0           #first:Lcom/android/internal/widget/LockPatternView$Cell;
    :cond_3c
    invoke-virtual {p0}, Lcom/android/internal/widget/LockPatternView;->invalidate()V

    #@3f
    .line 389
    return-void
.end method

.method public setInStealthMode(Z)V
    .registers 2
    .parameter "inStealthMode"

    #@0
    .prologue
    .line 330
    iput-boolean p1, p0, Lcom/android/internal/widget/LockPatternView;->mInStealthMode:Z

    #@2
    .line 331
    return-void
.end method

.method public setOnPatternListener(Lcom/android/internal/widget/LockPatternView$OnPatternListener;)V
    .registers 2
    .parameter "onPatternListener"

    #@0
    .prologue
    .line 349
    iput-object p1, p0, Lcom/android/internal/widget/LockPatternView;->mOnPatternListener:Lcom/android/internal/widget/LockPatternView$OnPatternListener;

    #@2
    .line 350
    return-void
.end method

.method public setPattern(Lcom/android/internal/widget/LockPatternView$DisplayMode;Ljava/util/List;)V
    .registers 8
    .parameter "displayMode"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/internal/widget/LockPatternView$DisplayMode;",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/widget/LockPatternView$Cell;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 359
    .local p2, pattern:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/widget/LockPatternView$Cell;>;"
    iget-object v2, p0, Lcom/android/internal/widget/LockPatternView;->mPattern:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    #@5
    .line 360
    iget-object v2, p0, Lcom/android/internal/widget/LockPatternView;->mPattern:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    #@a
    .line 361
    invoke-direct {p0}, Lcom/android/internal/widget/LockPatternView;->clearPatternDrawLookup()V

    #@d
    .line 362
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@10
    move-result-object v1

    #@11
    .local v1, i$:Ljava/util/Iterator;
    :goto_11
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@14
    move-result v2

    #@15
    if-eqz v2, :cond_2d

    #@17
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1a
    move-result-object v0

    #@1b
    check-cast v0, Lcom/android/internal/widget/LockPatternView$Cell;

    #@1d
    .line 363
    .local v0, cell:Lcom/android/internal/widget/LockPatternView$Cell;
    iget-object v2, p0, Lcom/android/internal/widget/LockPatternView;->mPatternDrawLookup:[[Z

    #@1f
    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternView$Cell;->getRow()I

    #@22
    move-result v3

    #@23
    aget-object v2, v2, v3

    #@25
    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternView$Cell;->getColumn()I

    #@28
    move-result v3

    #@29
    const/4 v4, 0x1

    #@2a
    aput-boolean v4, v2, v3

    #@2c
    goto :goto_11

    #@2d
    .line 366
    .end local v0           #cell:Lcom/android/internal/widget/LockPatternView$Cell;
    :cond_2d
    invoke-virtual {p0, p1}, Lcom/android/internal/widget/LockPatternView;->setDisplayMode(Lcom/android/internal/widget/LockPatternView$DisplayMode;)V

    #@30
    .line 367
    return-void
.end method

.method public setTactileFeedbackEnabled(Z)V
    .registers 2
    .parameter "tactileFeedbackEnabled"

    #@0
    .prologue
    .line 340
    iput-boolean p1, p0, Lcom/android/internal/widget/LockPatternView;->mEnableHapticFeedback:Z

    #@2
    .line 341
    return-void
.end method
