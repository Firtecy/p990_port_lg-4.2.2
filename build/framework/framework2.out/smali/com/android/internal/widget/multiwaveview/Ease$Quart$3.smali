.class final Lcom/android/internal/widget/multiwaveview/Ease$Quart$3;
.super Ljava/lang/Object;
.source "Ease.java"

# interfaces
.implements Landroid/animation/TimeInterpolator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/widget/multiwaveview/Ease$Quart;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 85
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .registers 6
    .parameter "input"

    #@0
    .prologue
    const/high16 v3, 0x4000

    #@2
    const/high16 v1, 0x3f00

    #@4
    const/4 v2, 0x0

    #@5
    .line 87
    div-float/2addr p1, v1

    #@6
    const/high16 v0, 0x3f80

    #@8
    cmpg-float v0, p1, v0

    #@a
    if-gez v0, :cond_13

    #@c
    mul-float v0, v1, p1

    #@e
    mul-float/2addr v0, p1

    #@f
    mul-float/2addr v0, p1

    #@10
    mul-float/2addr v0, p1

    #@11
    add-float/2addr v0, v2

    #@12
    :goto_12
    return v0

    #@13
    :cond_13
    const/high16 v0, -0x4100

    #@15
    sub-float/2addr p1, v3

    #@16
    mul-float v1, p1, p1

    #@18
    mul-float/2addr v1, p1

    #@19
    mul-float/2addr v1, p1

    #@1a
    sub-float/2addr v1, v3

    #@1b
    mul-float/2addr v0, v1

    #@1c
    add-float/2addr v0, v2

    #@1d
    goto :goto_12
.end method
