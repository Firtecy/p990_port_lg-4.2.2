.class public abstract Lcom/android/internal/widget/AbsActionBarView;
.super Landroid/view/ViewGroup;
.source "AbsActionBarView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/widget/AbsActionBarView$VisibilityAnimListener;
    }
.end annotation


# static fields
.field private static final FADE_DURATION:I = 0xc8

.field private static final sAlphaInterpolator:Landroid/animation/TimeInterpolator;


# instance fields
.field protected mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

.field protected mContentHeight:I

.field protected mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

.field protected mSplitActionBar:Z

.field protected mSplitView:Lcom/android/internal/widget/ActionBarContainer;

.field protected mSplitWhenNarrow:Z

.field protected final mVisAnimListener:Lcom/android/internal/widget/AbsActionBarView$VisibilityAnimListener;

.field protected mVisibilityAnim:Landroid/animation/Animator;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 45
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    #@2
    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    #@5
    sput-object v0, Lcom/android/internal/widget/AbsActionBarView;->sAlphaInterpolator:Landroid/animation/TimeInterpolator;

    #@7
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 50
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    #@3
    .line 43
    new-instance v0, Lcom/android/internal/widget/AbsActionBarView$VisibilityAnimListener;

    #@5
    invoke-direct {v0, p0}, Lcom/android/internal/widget/AbsActionBarView$VisibilityAnimListener;-><init>(Lcom/android/internal/widget/AbsActionBarView;)V

    #@8
    iput-object v0, p0, Lcom/android/internal/widget/AbsActionBarView;->mVisAnimListener:Lcom/android/internal/widget/AbsActionBarView$VisibilityAnimListener;

    #@a
    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@3
    .line 43
    new-instance v0, Lcom/android/internal/widget/AbsActionBarView$VisibilityAnimListener;

    #@5
    invoke-direct {v0, p0}, Lcom/android/internal/widget/AbsActionBarView$VisibilityAnimListener;-><init>(Lcom/android/internal/widget/AbsActionBarView;)V

    #@8
    iput-object v0, p0, Lcom/android/internal/widget/AbsActionBarView;->mVisAnimListener:Lcom/android/internal/widget/AbsActionBarView$VisibilityAnimListener;

    #@a
    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    .line 58
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@3
    .line 43
    new-instance v0, Lcom/android/internal/widget/AbsActionBarView$VisibilityAnimListener;

    #@5
    invoke-direct {v0, p0}, Lcom/android/internal/widget/AbsActionBarView$VisibilityAnimListener;-><init>(Lcom/android/internal/widget/AbsActionBarView;)V

    #@8
    iput-object v0, p0, Lcom/android/internal/widget/AbsActionBarView;->mVisAnimListener:Lcom/android/internal/widget/AbsActionBarView$VisibilityAnimListener;

    #@a
    .line 59
    return-void
.end method

.method protected static next(IIZ)I
    .registers 4
    .parameter "x"
    .parameter "val"
    .parameter "isRtl"

    #@0
    .prologue
    .line 226
    if-eqz p2, :cond_5

    #@2
    sub-int v0, p0, p1

    #@4
    :goto_4
    return v0

    #@5
    :cond_5
    add-int v0, p0, p1

    #@7
    goto :goto_4
.end method


# virtual methods
.method public animateToVisibility(I)V
    .registers 13
    .parameter "visibility"

    #@0
    .prologue
    const/high16 v10, 0x3f80

    #@2
    const-wide/16 v8, 0xc8

    #@4
    const/4 v5, 0x1

    #@5
    const/4 v7, 0x0

    #@6
    const/4 v6, 0x0

    #@7
    .line 123
    iget-object v3, p0, Lcom/android/internal/widget/AbsActionBarView;->mVisibilityAnim:Landroid/animation/Animator;

    #@9
    if-eqz v3, :cond_10

    #@b
    .line 124
    iget-object v3, p0, Lcom/android/internal/widget/AbsActionBarView;->mVisibilityAnim:Landroid/animation/Animator;

    #@d
    invoke-virtual {v3}, Landroid/animation/Animator;->cancel()V

    #@10
    .line 126
    :cond_10
    if-nez p1, :cond_77

    #@12
    .line 127
    invoke-virtual {p0}, Lcom/android/internal/widget/AbsActionBarView;->getVisibility()I

    #@15
    move-result v3

    #@16
    if-eqz v3, :cond_28

    #@18
    .line 128
    invoke-virtual {p0, v6}, Lcom/android/internal/widget/AbsActionBarView;->setAlpha(F)V

    #@1b
    .line 129
    iget-object v3, p0, Lcom/android/internal/widget/AbsActionBarView;->mSplitView:Lcom/android/internal/widget/ActionBarContainer;

    #@1d
    if-eqz v3, :cond_28

    #@1f
    iget-object v3, p0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@21
    if-eqz v3, :cond_28

    #@23
    .line 130
    iget-object v3, p0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@25
    invoke-virtual {v3, v6}, Lcom/android/internal/view/menu/ActionMenuView;->setAlpha(F)V

    #@28
    .line 133
    :cond_28
    const-string v3, "alpha"

    #@2a
    new-array v4, v5, [F

    #@2c
    aput v10, v4, v7

    #@2e
    invoke-static {p0, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    #@31
    move-result-object v0

    #@32
    .line 134
    .local v0, anim:Landroid/animation/ObjectAnimator;
    invoke-virtual {v0, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    #@35
    .line 135
    sget-object v3, Lcom/android/internal/widget/AbsActionBarView;->sAlphaInterpolator:Landroid/animation/TimeInterpolator;

    #@37
    invoke-virtual {v0, v3}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    #@3a
    .line 136
    iget-object v3, p0, Lcom/android/internal/widget/AbsActionBarView;->mSplitView:Lcom/android/internal/widget/ActionBarContainer;

    #@3c
    if-eqz v3, :cond_6a

    #@3e
    iget-object v3, p0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@40
    if-eqz v3, :cond_6a

    #@42
    .line 137
    new-instance v1, Landroid/animation/AnimatorSet;

    #@44
    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    #@47
    .line 138
    .local v1, set:Landroid/animation/AnimatorSet;
    iget-object v3, p0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@49
    const-string v4, "alpha"

    #@4b
    new-array v5, v5, [F

    #@4d
    aput v10, v5, v7

    #@4f
    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    #@52
    move-result-object v2

    #@53
    .line 139
    .local v2, splitAnim:Landroid/animation/ObjectAnimator;
    invoke-virtual {v2, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    #@56
    .line 140
    iget-object v3, p0, Lcom/android/internal/widget/AbsActionBarView;->mVisAnimListener:Lcom/android/internal/widget/AbsActionBarView$VisibilityAnimListener;

    #@58
    invoke-virtual {v3, p1}, Lcom/android/internal/widget/AbsActionBarView$VisibilityAnimListener;->withFinalVisibility(I)Lcom/android/internal/widget/AbsActionBarView$VisibilityAnimListener;

    #@5b
    move-result-object v3

    #@5c
    invoke-virtual {v1, v3}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    #@5f
    .line 141
    invoke-virtual {v1, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    #@62
    move-result-object v3

    #@63
    invoke-virtual {v3, v2}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    #@66
    .line 142
    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->start()V

    #@69
    .line 163
    .end local v1           #set:Landroid/animation/AnimatorSet;
    .end local v2           #splitAnim:Landroid/animation/ObjectAnimator;
    :goto_69
    return-void

    #@6a
    .line 144
    :cond_6a
    iget-object v3, p0, Lcom/android/internal/widget/AbsActionBarView;->mVisAnimListener:Lcom/android/internal/widget/AbsActionBarView$VisibilityAnimListener;

    #@6c
    invoke-virtual {v3, p1}, Lcom/android/internal/widget/AbsActionBarView$VisibilityAnimListener;->withFinalVisibility(I)Lcom/android/internal/widget/AbsActionBarView$VisibilityAnimListener;

    #@6f
    move-result-object v3

    #@70
    invoke-virtual {v0, v3}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    #@73
    .line 145
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    #@76
    goto :goto_69

    #@77
    .line 148
    .end local v0           #anim:Landroid/animation/ObjectAnimator;
    :cond_77
    const-string v3, "alpha"

    #@79
    new-array v4, v5, [F

    #@7b
    aput v6, v4, v7

    #@7d
    invoke-static {p0, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    #@80
    move-result-object v0

    #@81
    .line 149
    .restart local v0       #anim:Landroid/animation/ObjectAnimator;
    invoke-virtual {v0, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    #@84
    .line 150
    sget-object v3, Lcom/android/internal/widget/AbsActionBarView;->sAlphaInterpolator:Landroid/animation/TimeInterpolator;

    #@86
    invoke-virtual {v0, v3}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    #@89
    .line 151
    iget-object v3, p0, Lcom/android/internal/widget/AbsActionBarView;->mSplitView:Lcom/android/internal/widget/ActionBarContainer;

    #@8b
    if-eqz v3, :cond_b9

    #@8d
    iget-object v3, p0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@8f
    if-eqz v3, :cond_b9

    #@91
    .line 152
    new-instance v1, Landroid/animation/AnimatorSet;

    #@93
    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    #@96
    .line 153
    .restart local v1       #set:Landroid/animation/AnimatorSet;
    iget-object v3, p0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@98
    const-string v4, "alpha"

    #@9a
    new-array v5, v5, [F

    #@9c
    aput v6, v5, v7

    #@9e
    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    #@a1
    move-result-object v2

    #@a2
    .line 154
    .restart local v2       #splitAnim:Landroid/animation/ObjectAnimator;
    invoke-virtual {v2, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    #@a5
    .line 155
    iget-object v3, p0, Lcom/android/internal/widget/AbsActionBarView;->mVisAnimListener:Lcom/android/internal/widget/AbsActionBarView$VisibilityAnimListener;

    #@a7
    invoke-virtual {v3, p1}, Lcom/android/internal/widget/AbsActionBarView$VisibilityAnimListener;->withFinalVisibility(I)Lcom/android/internal/widget/AbsActionBarView$VisibilityAnimListener;

    #@aa
    move-result-object v3

    #@ab
    invoke-virtual {v1, v3}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    #@ae
    .line 156
    invoke-virtual {v1, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    #@b1
    move-result-object v3

    #@b2
    invoke-virtual {v3, v2}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    #@b5
    .line 157
    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->start()V

    #@b8
    goto :goto_69

    #@b9
    .line 159
    .end local v1           #set:Landroid/animation/AnimatorSet;
    .end local v2           #splitAnim:Landroid/animation/ObjectAnimator;
    :cond_b9
    iget-object v3, p0, Lcom/android/internal/widget/AbsActionBarView;->mVisAnimListener:Lcom/android/internal/widget/AbsActionBarView$VisibilityAnimListener;

    #@bb
    invoke-virtual {v3, p1}, Lcom/android/internal/widget/AbsActionBarView$VisibilityAnimListener;->withFinalVisibility(I)Lcom/android/internal/widget/AbsActionBarView$VisibilityAnimListener;

    #@be
    move-result-object v3

    #@bf
    invoke-virtual {v0, v3}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    #@c2
    .line 160
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    #@c5
    goto :goto_69
.end method

.method public dismissPopupMenus()V
    .registers 2

    #@0
    .prologue
    .line 209
    iget-object v0, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 210
    iget-object v0, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@6
    invoke-virtual {v0}, Lcom/android/internal/view/menu/ActionMenuPresenter;->dismissPopupMenus()Z

    #@9
    .line 212
    :cond_9
    return-void
.end method

.method public getAnimatedVisibility()I
    .registers 2

    #@0
    .prologue
    .line 116
    iget-object v0, p0, Lcom/android/internal/widget/AbsActionBarView;->mVisibilityAnim:Landroid/animation/Animator;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 117
    iget-object v0, p0, Lcom/android/internal/widget/AbsActionBarView;->mVisAnimListener:Lcom/android/internal/widget/AbsActionBarView$VisibilityAnimListener;

    #@6
    iget v0, v0, Lcom/android/internal/widget/AbsActionBarView$VisibilityAnimListener;->mFinalVisibility:I

    #@8
    .line 119
    :goto_8
    return v0

    #@9
    :cond_9
    invoke-virtual {p0}, Lcom/android/internal/widget/AbsActionBarView;->getVisibility()I

    #@c
    move-result v0

    #@d
    goto :goto_8
.end method

.method public getContentHeight()I
    .registers 2

    #@0
    .prologue
    .line 105
    iget v0, p0, Lcom/android/internal/widget/AbsActionBarView;->mContentHeight:I

    #@2
    return v0
.end method

.method public hideOverflowMenu()Z
    .registers 2

    #@0
    .prologue
    .line 191
    iget-object v0, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 192
    iget-object v0, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@6
    invoke-virtual {v0}, Lcom/android/internal/view/menu/ActionMenuPresenter;->hideOverflowMenu()Z

    #@9
    move-result v0

    #@a
    .line 194
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public isOverflowMenuShowing()Z
    .registers 2

    #@0
    .prologue
    .line 198
    iget-object v0, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 199
    iget-object v0, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@6
    invoke-virtual {v0}, Lcom/android/internal/view/menu/ActionMenuPresenter;->isOverflowMenuShowing()Z

    #@9
    move-result v0

    #@a
    .line 201
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public isOverflowReserved()Z
    .registers 2

    #@0
    .prologue
    .line 205
    iget-object v0, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@2
    if-eqz v0, :cond_e

    #@4
    iget-object v0, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@6
    invoke-virtual {v0}, Lcom/android/internal/view/menu/ActionMenuPresenter;->isOverflowReserved()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method protected measureChildView(Landroid/view/View;III)I
    .registers 6
    .parameter "child"
    .parameter "availableWidth"
    .parameter "childSpecHeight"
    .parameter "spacing"

    #@0
    .prologue
    .line 216
    const/high16 v0, -0x8000

    #@2
    invoke-static {p2, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@5
    move-result v0

    #@6
    invoke-virtual {p1, v0, p3}, Landroid/view/View;->measure(II)V

    #@9
    .line 219
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    #@c
    move-result v0

    #@d
    sub-int/2addr p2, v0

    #@e
    .line 220
    sub-int/2addr p2, p4

    #@f
    .line 222
    const/4 v0, 0x0

    #@10
    invoke-static {v0, p2}, Ljava/lang/Math;->max(II)I

    #@13
    move-result v0

    #@14
    return v0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 8
    .parameter "newConfig"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 63
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    #@4
    .line 67
    invoke-virtual {p0}, Lcom/android/internal/widget/AbsActionBarView;->getContext()Landroid/content/Context;

    #@7
    move-result-object v1

    #@8
    const/4 v2, 0x0

    #@9
    sget-object v3, Lcom/android/internal/R$styleable;->ActionBar:[I

    #@b
    const v4, 0x10102ce

    #@e
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@11
    move-result-object v0

    #@12
    .line 69
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v1, 0x4

    #@13
    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    #@16
    move-result v1

    #@17
    invoke-virtual {p0, v1}, Lcom/android/internal/widget/AbsActionBarView;->setContentHeight(I)V

    #@1a
    .line 70
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@1d
    .line 71
    iget-boolean v1, p0, Lcom/android/internal/widget/AbsActionBarView;->mSplitWhenNarrow:Z

    #@1f
    if-eqz v1, :cond_33

    #@21
    .line 72
    invoke-virtual {p0}, Lcom/android/internal/widget/AbsActionBarView;->getContext()Landroid/content/Context;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@28
    move-result-object v1

    #@29
    const v2, 0x1110006

    #@2c
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@2f
    move-result v1

    #@30
    invoke-virtual {p0, v1}, Lcom/android/internal/widget/AbsActionBarView;->setSplitActionBar(Z)V

    #@33
    .line 75
    :cond_33
    iget-object v1, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@35
    if-eqz v1, :cond_3c

    #@37
    .line 76
    iget-object v1, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@39
    invoke-virtual {v1, p1}, Lcom/android/internal/view/menu/ActionMenuPresenter;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    #@3c
    .line 78
    :cond_3c
    return-void
.end method

.method protected positionChild(Landroid/view/View;IIIZ)I
    .registers 11
    .parameter "child"
    .parameter "x"
    .parameter "y"
    .parameter "contentHeight"
    .parameter "reverse"

    #@0
    .prologue
    .line 230
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    #@3
    move-result v2

    #@4
    .line 231
    .local v2, childWidth:I
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    #@7
    move-result v0

    #@8
    .line 232
    .local v0, childHeight:I
    sub-int v3, p4, v0

    #@a
    div-int/lit8 v3, v3, 0x2

    #@c
    add-int v1, p3, v3

    #@e
    .line 234
    .local v1, childTop:I
    if-eqz p5, :cond_1b

    #@10
    .line 235
    sub-int v3, p2, v2

    #@12
    add-int v4, v1, v0

    #@14
    invoke-virtual {p1, v3, v1, p2, v4}, Landroid/view/View;->layout(IIII)V

    #@17
    .line 240
    :goto_17
    if-eqz p5, :cond_1a

    #@19
    neg-int v2, v2

    #@1a
    .end local v2           #childWidth:I
    :cond_1a
    return v2

    #@1b
    .line 237
    .restart local v2       #childWidth:I
    :cond_1b
    add-int v3, p2, v2

    #@1d
    add-int v4, v1, v0

    #@1f
    invoke-virtual {p1, p2, v1, v3, v4}, Landroid/view/View;->layout(IIII)V

    #@22
    goto :goto_17
.end method

.method public postShowOverflowMenu()V
    .registers 2

    #@0
    .prologue
    .line 183
    new-instance v0, Lcom/android/internal/widget/AbsActionBarView$1;

    #@2
    invoke-direct {v0, p0}, Lcom/android/internal/widget/AbsActionBarView$1;-><init>(Lcom/android/internal/widget/AbsActionBarView;)V

    #@5
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/AbsActionBarView;->post(Ljava/lang/Runnable;)Z

    #@8
    .line 188
    return-void
.end method

.method public setContentHeight(I)V
    .registers 4
    .parameter "height"

    #@0
    .prologue
    .line 97
    iput p1, p0, Lcom/android/internal/widget/AbsActionBarView;->mContentHeight:I

    #@2
    .line 98
    iget-object v0, p0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@4
    if-eqz v0, :cond_d

    #@6
    .line 99
    iget-object v0, p0, Lcom/android/internal/widget/AbsActionBarView;->mMenuView:Lcom/android/internal/view/menu/ActionMenuView;

    #@8
    iget v1, p0, Lcom/android/internal/widget/AbsActionBarView;->mContentHeight:I

    #@a
    invoke-virtual {v0, v1}, Lcom/android/internal/view/menu/ActionMenuView;->setMaxItemHeight(I)V

    #@d
    .line 101
    :cond_d
    invoke-virtual {p0}, Lcom/android/internal/widget/AbsActionBarView;->requestLayout()V

    #@10
    .line 102
    return-void
.end method

.method public setSplitActionBar(Z)V
    .registers 2
    .parameter "split"

    #@0
    .prologue
    .line 85
    iput-boolean p1, p0, Lcom/android/internal/widget/AbsActionBarView;->mSplitActionBar:Z

    #@2
    .line 86
    return-void
.end method

.method public setSplitView(Lcom/android/internal/widget/ActionBarContainer;)V
    .registers 2
    .parameter "splitView"

    #@0
    .prologue
    .line 109
    iput-object p1, p0, Lcom/android/internal/widget/AbsActionBarView;->mSplitView:Lcom/android/internal/widget/ActionBarContainer;

    #@2
    .line 110
    return-void
.end method

.method public setSplitWhenNarrow(Z)V
    .registers 2
    .parameter "splitWhenNarrow"

    #@0
    .prologue
    .line 93
    iput-boolean p1, p0, Lcom/android/internal/widget/AbsActionBarView;->mSplitWhenNarrow:Z

    #@2
    .line 94
    return-void
.end method

.method public setVisibility(I)V
    .registers 3
    .parameter "visibility"

    #@0
    .prologue
    .line 167
    invoke-virtual {p0}, Lcom/android/internal/widget/AbsActionBarView;->getVisibility()I

    #@3
    move-result v0

    #@4
    if-eq p1, v0, :cond_12

    #@6
    .line 168
    iget-object v0, p0, Lcom/android/internal/widget/AbsActionBarView;->mVisibilityAnim:Landroid/animation/Animator;

    #@8
    if-eqz v0, :cond_f

    #@a
    .line 169
    iget-object v0, p0, Lcom/android/internal/widget/AbsActionBarView;->mVisibilityAnim:Landroid/animation/Animator;

    #@c
    invoke-virtual {v0}, Landroid/animation/Animator;->end()V

    #@f
    .line 171
    :cond_f
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->setVisibility(I)V

    #@12
    .line 173
    :cond_12
    return-void
.end method

.method public showOverflowMenu()Z
    .registers 2

    #@0
    .prologue
    .line 176
    iget-object v0, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 177
    iget-object v0, p0, Lcom/android/internal/widget/AbsActionBarView;->mActionMenuPresenter:Lcom/android/internal/view/menu/ActionMenuPresenter;

    #@6
    invoke-virtual {v0}, Lcom/android/internal/view/menu/ActionMenuPresenter;->showOverflowMenu()Z

    #@9
    move-result v0

    #@a
    .line 179
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method
