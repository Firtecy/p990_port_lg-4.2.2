.class public Lcom/android/internal/widget/LGScrollingTabContainerView;
.super Lcom/android/internal/widget/ScrollingTabContainerView;
.source "LGScrollingTabContainerView.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "LGScrollingTabContainerView"


# instance fields
.field private ANI_DURATION:I

.field private mAnimationLocked:Z

.field private mArrowLocated:Z

.field private mCurTabIndex:I

.field private mDestination:I

.field private mOnConfigured:Z

.field private mSelectedTabArrow:Landroid/widget/ImageView;

.field private mSource:I

.field private mTabAnim:Landroid/view/animation/Animation;

.field private mTabParentLayout:Landroid/widget/RelativeLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 10
    .parameter "context"

    #@0
    .prologue
    const/4 v7, -0x1

    #@1
    const/4 v6, -0x2

    #@2
    const/4 v4, 0x0

    #@3
    .line 58
    invoke-direct {p0, p1}, Lcom/android/internal/widget/ScrollingTabContainerView;-><init>(Landroid/content/Context;)V

    #@6
    .line 42
    const/16 v3, 0xc8

    #@8
    iput v3, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->ANI_DURATION:I

    #@a
    .line 48
    iput-boolean v4, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mArrowLocated:Z

    #@c
    .line 49
    iput-boolean v4, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mOnConfigured:Z

    #@e
    .line 50
    iput-boolean v4, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mAnimationLocked:Z

    #@10
    .line 52
    iput v4, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mSource:I

    #@12
    .line 53
    iput v4, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mDestination:I

    #@14
    .line 61
    new-instance v2, Landroid/widget/RelativeLayout;

    #@16
    invoke-virtual {p0}, Lcom/android/internal/widget/LGScrollingTabContainerView;->getContext()Landroid/content/Context;

    #@19
    move-result-object v3

    #@1a
    invoke-direct {v2, v3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    #@1d
    .line 62
    .local v2, tapParentLayout:Landroid/widget/RelativeLayout;
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    #@1f
    invoke-direct {v3, v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    #@22
    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@25
    .line 64
    iput-object v2, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mTabParentLayout:Landroid/widget/RelativeLayout;

    #@27
    .line 67
    new-instance v0, Landroid/widget/ImageView;

    #@29
    invoke-virtual {p0}, Lcom/android/internal/widget/LGScrollingTabContainerView;->getContext()Landroid/content/Context;

    #@2c
    move-result-object v3

    #@2d
    invoke-direct {v0, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    #@30
    .line 68
    .local v0, iv:Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/android/internal/widget/LGScrollingTabContainerView;->getResources()Landroid/content/res/Resources;

    #@33
    move-result-object v3

    #@34
    const v4, 0x2020674

    #@37
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@3a
    move-result-object v3

    #@3b
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    #@3e
    .line 69
    sget-object v3, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    #@40
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    #@43
    .line 70
    const/4 v3, 0x1

    #@44
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setAdjustViewBounds(Z)V

    #@47
    .line 71
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    #@49
    invoke-direct {v3, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    #@4c
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@4f
    .line 73
    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@52
    move-result-object v1

    #@53
    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    #@55
    .line 74
    .local v1, selectorLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v3, 0xc

    #@57
    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    #@5a
    .line 75
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@5d
    .line 76
    iput-object v0, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mSelectedTabArrow:Landroid/widget/ImageView;

    #@5f
    .line 77
    iget-object v3, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mSelectedTabArrow:Landroid/widget/ImageView;

    #@61
    const/4 v4, 0x4

    #@62
    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    #@65
    .line 80
    iget-object v3, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabLayout:Landroid/widget/LinearLayout;

    #@67
    invoke-virtual {p0, v3}, Lcom/android/internal/widget/LGScrollingTabContainerView;->removeView(Landroid/view/View;)V

    #@6a
    .line 81
    iget-object v3, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mTabParentLayout:Landroid/widget/RelativeLayout;

    #@6c
    iget-object v4, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabLayout:Landroid/widget/LinearLayout;

    #@6e
    new-instance v5, Landroid/view/ViewGroup$LayoutParams;

    #@70
    invoke-direct {v5, v7, v7}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    #@73
    invoke-virtual {v3, v4, v5}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@76
    .line 83
    iget-object v3, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mTabParentLayout:Landroid/widget/RelativeLayout;

    #@78
    iget-object v4, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mSelectedTabArrow:Landroid/widget/ImageView;

    #@7a
    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    #@7d
    .line 84
    iget-object v3, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mTabParentLayout:Landroid/widget/RelativeLayout;

    #@7f
    new-instance v4, Landroid/view/ViewGroup$LayoutParams;

    #@81
    invoke-direct {v4, v6, v7}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    #@84
    invoke-virtual {p0, v3, v4}, Lcom/android/internal/widget/LGScrollingTabContainerView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@87
    .line 86
    return-void
.end method

.method static synthetic access$000(Lcom/android/internal/widget/LGScrollingTabContainerView;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mAnimationLocked:Z

    #@2
    return v0
.end method

.method static synthetic access$002(Lcom/android/internal/widget/LGScrollingTabContainerView;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 38
    iput-boolean p1, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mAnimationLocked:Z

    #@2
    return p1
.end method

.method static synthetic access$100(Lcom/android/internal/widget/LGScrollingTabContainerView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 38
    iget v0, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mDestination:I

    #@2
    return v0
.end method

.method static synthetic access$200(Lcom/android/internal/widget/LGScrollingTabContainerView;)Landroid/widget/ImageView;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 38
    iget-object v0, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mSelectedTabArrow:Landroid/widget/ImageView;

    #@2
    return-object v0
.end method


# virtual methods
.method public addTab(Landroid/app/ActionBar$Tab;IZ)V
    .registers 6
    .parameter "tab"
    .parameter "position"
    .parameter "setSelected"

    #@0
    .prologue
    .line 207
    iget-object v0, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mSelectedTabArrow:Landroid/widget/ImageView;

    #@2
    const/4 v1, 0x4

    #@3
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    #@6
    .line 208
    const/4 v0, 0x0

    #@7
    iput-boolean v0, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mArrowLocated:Z

    #@9
    .line 209
    invoke-super {p0, p1, p2, p3}, Lcom/android/internal/widget/ScrollingTabContainerView;->addTab(Landroid/app/ActionBar$Tab;IZ)V

    #@c
    .line 210
    return-void
.end method

.method public addTab(Landroid/app/ActionBar$Tab;Z)V
    .registers 5
    .parameter "tab"
    .parameter "setSelected"

    #@0
    .prologue
    .line 200
    iget-object v0, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mSelectedTabArrow:Landroid/widget/ImageView;

    #@2
    const/4 v1, 0x4

    #@3
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    #@6
    .line 201
    const/4 v0, 0x0

    #@7
    iput-boolean v0, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mArrowLocated:Z

    #@9
    .line 202
    invoke-super {p0, p1, p2}, Lcom/android/internal/widget/ScrollingTabContainerView;->addTab(Landroid/app/ActionBar$Tab;Z)V

    #@c
    .line 203
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 4
    .parameter "newConfig"

    #@0
    .prologue
    .line 191
    iget-object v0, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mSelectedTabArrow:Landroid/widget/ImageView;

    #@2
    const/4 v1, 0x4

    #@3
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    #@6
    .line 192
    const/4 v0, 0x0

    #@7
    iput-boolean v0, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mArrowLocated:Z

    #@9
    .line 193
    const/4 v0, 0x1

    #@a
    iput-boolean v0, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mOnConfigured:Z

    #@c
    .line 195
    invoke-super {p0, p1}, Lcom/android/internal/widget/ScrollingTabContainerView;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    #@f
    .line 196
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 6
    .parameter "canvas"

    #@0
    .prologue
    .line 95
    iget-object v2, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabLayout:Landroid/widget/LinearLayout;

    #@2
    iget v3, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mCurTabIndex:I

    #@4
    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    #@7
    move-result-object v1

    #@8
    check-cast v1, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;

    #@a
    .line 98
    .local v1, tabView:Lcom/android/internal/widget/ScrollingTabContainerView$TabView;
    const/4 v0, 0x0

    #@b
    .line 99
    .local v0, pos:I
    iget-boolean v2, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mArrowLocated:Z

    #@d
    if-nez v2, :cond_3f

    #@f
    if-eqz v1, :cond_3f

    #@11
    invoke-virtual {v1}, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->getWidth()I

    #@14
    move-result v2

    #@15
    if-eqz v2, :cond_3f

    #@17
    .line 100
    invoke-virtual {v1}, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->getLeft()I

    #@1a
    move-result v2

    #@1b
    invoke-virtual {v1}, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->getWidth()I

    #@1e
    move-result v3

    #@1f
    div-int/lit8 v3, v3, 0x2

    #@21
    add-int/2addr v2, v3

    #@22
    iget-object v3, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mSelectedTabArrow:Landroid/widget/ImageView;

    #@24
    invoke-virtual {v3}, Landroid/widget/ImageView;->getWidth()I

    #@27
    move-result v3

    #@28
    div-int/lit8 v3, v3, 0x2

    #@2a
    sub-int v0, v2, v3

    #@2c
    .line 101
    if-lez v0, :cond_3f

    #@2e
    .line 102
    iget-object v2, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mSelectedTabArrow:Landroid/widget/ImageView;

    #@30
    int-to-float v3, v0

    #@31
    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setTranslationX(F)V

    #@34
    .line 103
    iget-object v2, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mSelectedTabArrow:Landroid/widget/ImageView;

    #@36
    const/4 v3, 0x0

    #@37
    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    #@3a
    .line 104
    const/4 v2, 0x1

    #@3b
    iput-boolean v2, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mArrowLocated:Z

    #@3d
    .line 105
    iput v0, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mDestination:I

    #@3f
    .line 108
    :cond_3f
    return-void
.end method

.method public onMeasure(II)V
    .registers 3
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    .line 90
    invoke-super {p0, p1, p2}, Lcom/android/internal/widget/ScrollingTabContainerView;->onMeasure(II)V

    #@3
    .line 91
    return-void
.end method

.method protected performCollapse()V
    .registers 5

    #@0
    .prologue
    .line 112
    invoke-virtual {p0}, Lcom/android/internal/widget/LGScrollingTabContainerView;->isCollapsed()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_24

    #@6
    .line 113
    iget-object v0, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mSelectedTabArrow:Landroid/widget/ImageView;

    #@8
    const/4 v1, 0x4

    #@9
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    #@c
    .line 114
    iget-object v0, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mTabParentLayout:Landroid/widget/RelativeLayout;

    #@e
    iget-object v1, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabLayout:Landroid/widget/LinearLayout;

    #@10
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    #@13
    .line 115
    iget-object v0, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mTabParentLayout:Landroid/widget/RelativeLayout;

    #@15
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/LGScrollingTabContainerView;->removeView(Landroid/view/View;)V

    #@18
    .line 116
    iget-object v0, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabLayout:Landroid/widget/LinearLayout;

    #@1a
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    #@1c
    const/4 v2, -0x2

    #@1d
    const/4 v3, -0x1

    #@1e
    invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    #@21
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/widget/LGScrollingTabContainerView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@24
    .line 119
    :cond_24
    invoke-super {p0}, Lcom/android/internal/widget/ScrollingTabContainerView;->performCollapse()V

    #@27
    .line 120
    return-void
.end method

.method protected performExpand()Z
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v3, -0x1

    #@2
    .line 124
    invoke-super {p0}, Lcom/android/internal/widget/ScrollingTabContainerView;->performExpand()Z

    #@5
    .line 126
    invoke-virtual {p0}, Lcom/android/internal/widget/LGScrollingTabContainerView;->isCollapsed()Z

    #@8
    move-result v0

    #@9
    if-eqz v0, :cond_1e

    #@b
    .line 127
    iget-object v0, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabLayout:Landroid/widget/LinearLayout;

    #@d
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/LGScrollingTabContainerView;->removeView(Landroid/view/View;)V

    #@10
    .line 128
    iget-object v0, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mTabParentLayout:Landroid/widget/RelativeLayout;

    #@12
    iget-object v1, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabLayout:Landroid/widget/LinearLayout;

    #@14
    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    #@16
    invoke-direct {v2, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    #@19
    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@1c
    .line 129
    iput-boolean v4, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mArrowLocated:Z

    #@1e
    .line 132
    :cond_1e
    return v4
.end method

.method public removeAllTabs()V
    .registers 3

    #@0
    .prologue
    .line 221
    iget-object v0, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mSelectedTabArrow:Landroid/widget/ImageView;

    #@2
    const/4 v1, 0x4

    #@3
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    #@6
    .line 222
    const/4 v0, 0x0

    #@7
    iput-boolean v0, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mArrowLocated:Z

    #@9
    .line 223
    invoke-super {p0}, Lcom/android/internal/widget/ScrollingTabContainerView;->removeAllTabs()V

    #@c
    .line 224
    return-void
.end method

.method public removeTabAt(I)V
    .registers 4
    .parameter "position"

    #@0
    .prologue
    .line 214
    iget-object v0, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mSelectedTabArrow:Landroid/widget/ImageView;

    #@2
    const/4 v1, 0x4

    #@3
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    #@6
    .line 215
    const/4 v0, 0x0

    #@7
    iput-boolean v0, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mArrowLocated:Z

    #@9
    .line 216
    invoke-super {p0, p1}, Lcom/android/internal/widget/ScrollingTabContainerView;->removeTabAt(I)V

    #@c
    .line 217
    return-void
.end method

.method public setTabSelected(I)V
    .registers 8
    .parameter "position"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 137
    invoke-super {p0, p1}, Lcom/android/internal/widget/ScrollingTabContainerView;->setTabSelected(I)V

    #@5
    .line 140
    iput-boolean v5, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mAnimationLocked:Z

    #@7
    .line 141
    iget v1, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mDestination:I

    #@9
    iput v1, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mSource:I

    #@b
    .line 143
    iget-boolean v1, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mArrowLocated:Z

    #@d
    if-nez v1, :cond_12

    #@f
    .line 144
    iput p1, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mCurTabIndex:I

    #@11
    .line 187
    :goto_11
    return-void

    #@12
    .line 148
    :cond_12
    iget-object v1, p0, Lcom/android/internal/widget/ScrollingTabContainerView;->mTabLayout:Landroid/widget/LinearLayout;

    #@14
    invoke-virtual {v1, p1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    #@17
    move-result-object v1

    #@18
    check-cast v1, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;

    #@1a
    move-object v0, v1

    #@1b
    check-cast v0, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;

    #@1d
    .line 149
    .local v0, tabView:Lcom/android/internal/widget/ScrollingTabContainerView$TabView;
    if-eqz v0, :cond_3f

    #@1f
    .line 151
    iget v1, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mCurTabIndex:I

    #@21
    if-ne v1, p1, :cond_29

    #@23
    .line 152
    const/4 v1, 0x0

    #@24
    iput-boolean v1, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mArrowLocated:Z

    #@26
    .line 153
    iput p1, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mCurTabIndex:I

    #@28
    goto :goto_11

    #@29
    .line 156
    :cond_29
    invoke-virtual {v0}, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->getLeft()I

    #@2c
    move-result v1

    #@2d
    invoke-virtual {v0}, Lcom/android/internal/widget/ScrollingTabContainerView$TabView;->getWidth()I

    #@30
    move-result v2

    #@31
    div-int/lit8 v2, v2, 0x2

    #@33
    add-int/2addr v1, v2

    #@34
    iget-object v2, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mSelectedTabArrow:Landroid/widget/ImageView;

    #@36
    invoke-virtual {v2}, Landroid/widget/ImageView;->getWidth()I

    #@39
    move-result v2

    #@3a
    div-int/lit8 v2, v2, 0x2

    #@3c
    sub-int/2addr v1, v2

    #@3d
    iput v1, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mDestination:I

    #@3f
    .line 159
    :cond_3f
    iput p1, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mCurTabIndex:I

    #@41
    .line 162
    iget-object v1, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mSelectedTabArrow:Landroid/widget/ImageView;

    #@43
    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setTranslationX(F)V

    #@46
    .line 164
    new-instance v1, Landroid/view/animation/TranslateAnimation;

    #@48
    iget v2, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mSource:I

    #@4a
    int-to-float v2, v2

    #@4b
    iget v3, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mDestination:I

    #@4d
    int-to-float v3, v3

    #@4e
    invoke-direct {v1, v2, v3, v4, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    #@51
    iput-object v1, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mTabAnim:Landroid/view/animation/Animation;

    #@53
    .line 165
    iget-object v1, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mTabAnim:Landroid/view/animation/Animation;

    #@55
    const-wide/16 v2, 0xc8

    #@57
    invoke-virtual {v1, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    #@5a
    .line 166
    iget-object v1, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mTabAnim:Landroid/view/animation/Animation;

    #@5c
    invoke-virtual {v1, v5}, Landroid/view/animation/Animation;->setFillEnabled(Z)V

    #@5f
    .line 167
    iget-object v1, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mTabAnim:Landroid/view/animation/Animation;

    #@61
    new-instance v2, Landroid/view/animation/DecelerateInterpolator;

    #@63
    invoke-direct {v2}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    #@66
    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    #@69
    .line 168
    iget-object v1, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mSelectedTabArrow:Landroid/widget/ImageView;

    #@6b
    iget-object v2, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mTabAnim:Landroid/view/animation/Animation;

    #@6d
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    #@70
    .line 170
    iget-object v1, p0, Lcom/android/internal/widget/LGScrollingTabContainerView;->mTabAnim:Landroid/view/animation/Animation;

    #@72
    new-instance v2, Lcom/android/internal/widget/LGScrollingTabContainerView$1;

    #@74
    invoke-direct {v2, p0}, Lcom/android/internal/widget/LGScrollingTabContainerView$1;-><init>(Lcom/android/internal/widget/LGScrollingTabContainerView;)V

    #@77
    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    #@7a
    goto :goto_11
.end method
