.class public Lcom/android/internal/widget/DialogTitle;
.super Landroid/widget/TextView;
.source "DialogTitle.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 42
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    #@3
    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@3
    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@3
    .line 35
    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .registers 14
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    .line 47
    invoke-super {p0, p1, p2}, Landroid/widget/TextView;->onMeasure(II)V

    #@4
    .line 49
    invoke-virtual {p0}, Lcom/android/internal/widget/DialogTitle;->getLayout()Landroid/text/Layout;

    #@7
    move-result-object v2

    #@8
    .line 50
    .local v2, layout:Landroid/text/Layout;
    if-eqz v2, :cond_3e

    #@a
    .line 51
    invoke-virtual {v2}, Landroid/text/Layout;->getLineCount()I

    #@d
    move-result v3

    #@e
    .line 52
    .local v3, lineCount:I
    if-lez v3, :cond_3e

    #@10
    .line 53
    add-int/lit8 v5, v3, -0x1

    #@12
    invoke-virtual {v2, v5}, Landroid/text/Layout;->getEllipsisCount(I)I

    #@15
    move-result v1

    #@16
    .line 54
    .local v1, ellipsisCount:I
    if-lez v1, :cond_3e

    #@18
    .line 55
    invoke-virtual {p0, v10}, Lcom/android/internal/widget/DialogTitle;->setSingleLine(Z)V

    #@1b
    .line 56
    const/4 v5, 0x2

    #@1c
    invoke-virtual {p0, v5}, Lcom/android/internal/widget/DialogTitle;->setMaxLines(I)V

    #@1f
    .line 58
    iget-object v5, p0, Lcom/android/internal/widget/DialogTitle;->mContext:Landroid/content/Context;

    #@21
    const/4 v6, 0x0

    #@22
    sget-object v7, Landroid/R$styleable;->TextAppearance:[I

    #@24
    const v8, 0x1010041

    #@27
    const v9, 0x1030044

    #@2a
    invoke-virtual {v5, v6, v7, v8, v9}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@2d
    move-result-object v0

    #@2e
    .line 61
    .local v0, a:Landroid/content/res/TypedArray;
    invoke-virtual {v0, v10, v10}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@31
    move-result v4

    #@32
    .line 63
    .local v4, textSize:I
    if-eqz v4, :cond_38

    #@34
    .line 65
    int-to-float v5, v4

    #@35
    invoke-virtual {p0, v10, v5}, Lcom/android/internal/widget/DialogTitle;->setTextSize(IF)V

    #@38
    .line 67
    :cond_38
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@3b
    .line 69
    invoke-super {p0, p1, p2}, Landroid/widget/TextView;->onMeasure(II)V

    #@3e
    .line 73
    .end local v0           #a:Landroid/content/res/TypedArray;
    .end local v1           #ellipsisCount:I
    .end local v3           #lineCount:I
    .end local v4           #textSize:I
    :cond_3e
    return-void
.end method
