.class public Lcom/android/internal/widget/ActionBarOverlayLayout;
.super Landroid/widget/FrameLayout;
.source "ActionBarOverlayLayout.java"


# static fields
.field static final mActionBarSizeAttr:[I


# instance fields
.field private mActionBar:Lcom/android/internal/app/ActionBarImpl;

.field private mActionBarBottom:Landroid/view/View;

.field private mActionBarHeight:I

.field private mActionBarTop:Landroid/view/View;

.field private mActionView:Lcom/android/internal/widget/ActionBarView;

.field private mContainerView:Lcom/android/internal/widget/ActionBarContainer;

.field private mContent:Landroid/view/View;

.field private mLastSystemUiVisibility:I

.field private mWindowVisibility:I

.field private final mZeroRect:Landroid/graphics/Rect;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 46
    const/4 v0, 0x1

    #@1
    new-array v0, v0, [I

    #@3
    const/4 v1, 0x0

    #@4
    const v2, 0x10102eb

    #@7
    aput v2, v0, v1

    #@9
    sput-object v0, Lcom/android/internal/widget/ActionBarOverlayLayout;->mActionBarSizeAttr:[I

    #@b
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 51
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    #@4
    .line 37
    iput v1, p0, Lcom/android/internal/widget/ActionBarOverlayLayout;->mWindowVisibility:I

    #@6
    .line 44
    new-instance v0, Landroid/graphics/Rect;

    #@8
    invoke-direct {v0, v1, v1, v1, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    #@b
    iput-object v0, p0, Lcom/android/internal/widget/ActionBarOverlayLayout;->mZeroRect:Landroid/graphics/Rect;

    #@d
    .line 52
    invoke-direct {p0, p1}, Lcom/android/internal/widget/ActionBarOverlayLayout;->init(Landroid/content/Context;)V

    #@10
    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 5
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 56
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 37
    iput v1, p0, Lcom/android/internal/widget/ActionBarOverlayLayout;->mWindowVisibility:I

    #@6
    .line 44
    new-instance v0, Landroid/graphics/Rect;

    #@8
    invoke-direct {v0, v1, v1, v1, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    #@b
    iput-object v0, p0, Lcom/android/internal/widget/ActionBarOverlayLayout;->mZeroRect:Landroid/graphics/Rect;

    #@d
    .line 57
    invoke-direct {p0, p1}, Lcom/android/internal/widget/ActionBarOverlayLayout;->init(Landroid/content/Context;)V

    #@10
    .line 58
    return-void
.end method

.method private applyInsets(Landroid/view/View;Landroid/graphics/Rect;ZZZZ)Z
    .registers 11
    .parameter "view"
    .parameter "insets"
    .parameter "left"
    .parameter "top"
    .parameter "bottom"
    .parameter "right"

    #@0
    .prologue
    .line 130
    const/4 v0, 0x0

    #@1
    .line 131
    .local v0, changed:Z
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@4
    move-result-object v1

    #@5
    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    #@7
    .line 132
    .local v1, lp:Landroid/widget/FrameLayout$LayoutParams;
    if-eqz p3, :cond_14

    #@9
    iget v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@b
    iget v3, p2, Landroid/graphics/Rect;->left:I

    #@d
    if-eq v2, v3, :cond_14

    #@f
    .line 133
    const/4 v0, 0x1

    #@10
    .line 134
    iget v2, p2, Landroid/graphics/Rect;->left:I

    #@12
    iput v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    #@14
    .line 136
    :cond_14
    if-eqz p4, :cond_21

    #@16
    iget v2, v1, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    #@18
    iget v3, p2, Landroid/graphics/Rect;->top:I

    #@1a
    if-eq v2, v3, :cond_21

    #@1c
    .line 137
    const/4 v0, 0x1

    #@1d
    .line 138
    iget v2, p2, Landroid/graphics/Rect;->top:I

    #@1f
    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    #@21
    .line 140
    :cond_21
    if-eqz p6, :cond_2e

    #@23
    iget v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@25
    iget v3, p2, Landroid/graphics/Rect;->right:I

    #@27
    if-eq v2, v3, :cond_2e

    #@29
    .line 141
    const/4 v0, 0x1

    #@2a
    .line 142
    iget v2, p2, Landroid/graphics/Rect;->right:I

    #@2c
    iput v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    #@2e
    .line 144
    :cond_2e
    if-eqz p5, :cond_3b

    #@30
    iget v2, v1, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    #@32
    iget v3, p2, Landroid/graphics/Rect;->bottom:I

    #@34
    if-eq v2, v3, :cond_3b

    #@36
    .line 145
    const/4 v0, 0x1

    #@37
    .line 146
    iget v2, p2, Landroid/graphics/Rect;->bottom:I

    #@39
    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    #@3b
    .line 148
    :cond_3b
    return v0
.end method

.method private init(Landroid/content/Context;)V
    .registers 6
    .parameter "context"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 61
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarOverlayLayout;->getContext()Landroid/content/Context;

    #@4
    move-result-object v1

    #@5
    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    #@8
    move-result-object v1

    #@9
    sget-object v2, Lcom/android/internal/widget/ActionBarOverlayLayout;->mActionBarSizeAttr:[I

    #@b
    invoke-virtual {v1, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    #@e
    move-result-object v0

    #@f
    .line 62
    .local v0, ta:Landroid/content/res/TypedArray;
    invoke-virtual {v0, v3, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@12
    move-result v1

    #@13
    iput v1, p0, Lcom/android/internal/widget/ActionBarOverlayLayout;->mActionBarHeight:I

    #@15
    .line 63
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@18
    .line 64
    return-void
.end method


# virtual methods
.method protected fitSystemWindows(Landroid/graphics/Rect;)Z
    .registers 13
    .parameter "insets"

    #@0
    .prologue
    .line 153
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarOverlayLayout;->pullChildren()V

    #@3
    .line 155
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarOverlayLayout;->getWindowSystemUiVisibility()I

    #@6
    move-result v10

    #@7
    .line 156
    .local v10, vis:I
    and-int/lit16 v0, v10, 0x100

    #@9
    if-eqz v0, :cond_9c

    #@b
    const/4 v8, 0x1

    #@c
    .line 159
    .local v8, stable:Z
    :goto_c
    iget-object v1, p0, Lcom/android/internal/widget/ActionBarOverlayLayout;->mActionBarTop:Landroid/view/View;

    #@e
    const/4 v3, 0x1

    #@f
    const/4 v4, 0x1

    #@10
    const/4 v5, 0x0

    #@11
    const/4 v6, 0x1

    #@12
    move-object v0, p0

    #@13
    move-object v2, p1

    #@14
    invoke-direct/range {v0 .. v6}, Lcom/android/internal/widget/ActionBarOverlayLayout;->applyInsets(Landroid/view/View;Landroid/graphics/Rect;ZZZZ)Z

    #@17
    move-result v7

    #@18
    .line 160
    .local v7, changed:Z
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarOverlayLayout;->mActionBarBottom:Landroid/view/View;

    #@1a
    if-eqz v0, :cond_29

    #@1c
    .line 161
    iget-object v1, p0, Lcom/android/internal/widget/ActionBarOverlayLayout;->mActionBarBottom:Landroid/view/View;

    #@1e
    const/4 v3, 0x1

    #@1f
    const/4 v4, 0x0

    #@20
    const/4 v5, 0x1

    #@21
    const/4 v6, 0x1

    #@22
    move-object v0, p0

    #@23
    move-object v2, p1

    #@24
    invoke-direct/range {v0 .. v6}, Lcom/android/internal/widget/ActionBarOverlayLayout;->applyInsets(Landroid/view/View;Landroid/graphics/Rect;ZZZZ)Z

    #@27
    move-result v0

    #@28
    or-int/2addr v7, v0

    #@29
    .line 168
    :cond_29
    and-int/lit16 v0, v10, 0x600

    #@2b
    if-nez v0, :cond_9f

    #@2d
    .line 169
    iget-object v1, p0, Lcom/android/internal/widget/ActionBarOverlayLayout;->mContent:Landroid/view/View;

    #@2f
    const/4 v3, 0x1

    #@30
    const/4 v4, 0x1

    #@31
    const/4 v5, 0x1

    #@32
    const/4 v6, 0x1

    #@33
    move-object v0, p0

    #@34
    move-object v2, p1

    #@35
    invoke-direct/range {v0 .. v6}, Lcom/android/internal/widget/ActionBarOverlayLayout;->applyInsets(Landroid/view/View;Landroid/graphics/Rect;ZZZZ)Z

    #@38
    move-result v0

    #@39
    or-int/2addr v7, v0

    #@3a
    .line 171
    const/4 v0, 0x0

    #@3b
    const/4 v1, 0x0

    #@3c
    const/4 v2, 0x0

    #@3d
    const/4 v3, 0x0

    #@3e
    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    #@41
    .line 177
    :goto_41
    if-nez v8, :cond_4b

    #@43
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarOverlayLayout;->mActionBarTop:Landroid/view/View;

    #@45
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    #@48
    move-result v0

    #@49
    if-nez v0, :cond_52

    #@4b
    .line 179
    :cond_4b
    iget v0, p1, Landroid/graphics/Rect;->top:I

    #@4d
    iget v1, p0, Lcom/android/internal/widget/ActionBarOverlayLayout;->mActionBarHeight:I

    #@4f
    add-int/2addr v0, v1

    #@50
    iput v0, p1, Landroid/graphics/Rect;->top:I

    #@52
    .line 182
    :cond_52
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarOverlayLayout;->mActionBar:Lcom/android/internal/app/ActionBarImpl;

    #@54
    if-eqz v0, :cond_75

    #@56
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarOverlayLayout;->mActionBar:Lcom/android/internal/app/ActionBarImpl;

    #@58
    invoke-virtual {v0}, Lcom/android/internal/app/ActionBarImpl;->hasNonEmbeddedTabs()Z

    #@5b
    move-result v0

    #@5c
    if-eqz v0, :cond_75

    #@5e
    .line 183
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarOverlayLayout;->mContainerView:Lcom/android/internal/widget/ActionBarContainer;

    #@60
    invoke-virtual {v0}, Lcom/android/internal/widget/ActionBarContainer;->getTabContainer()Landroid/view/View;

    #@63
    move-result-object v9

    #@64
    .line 184
    .local v9, tabs:Landroid/view/View;
    if-nez v8, :cond_6e

    #@66
    if-eqz v9, :cond_75

    #@68
    invoke-virtual {v9}, Landroid/view/View;->getVisibility()I

    #@6b
    move-result v0

    #@6c
    if-nez v0, :cond_75

    #@6e
    .line 186
    :cond_6e
    iget v0, p1, Landroid/graphics/Rect;->top:I

    #@70
    iget v1, p0, Lcom/android/internal/widget/ActionBarOverlayLayout;->mActionBarHeight:I

    #@72
    add-int/2addr v0, v1

    #@73
    iput v0, p1, Landroid/graphics/Rect;->top:I

    #@75
    .line 190
    .end local v9           #tabs:Landroid/view/View;
    :cond_75
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarOverlayLayout;->mActionView:Lcom/android/internal/widget/ActionBarView;

    #@77
    invoke-virtual {v0}, Lcom/android/internal/widget/ActionBarView;->isSplitActionBar()Z

    #@7a
    move-result v0

    #@7b
    if-eqz v0, :cond_92

    #@7d
    .line 191
    if-nez v8, :cond_8b

    #@7f
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarOverlayLayout;->mActionBarBottom:Landroid/view/View;

    #@81
    if-eqz v0, :cond_92

    #@83
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarOverlayLayout;->mActionBarBottom:Landroid/view/View;

    #@85
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    #@88
    move-result v0

    #@89
    if-nez v0, :cond_92

    #@8b
    .line 194
    :cond_8b
    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    #@8d
    iget v1, p0, Lcom/android/internal/widget/ActionBarOverlayLayout;->mActionBarHeight:I

    #@8f
    add-int/2addr v0, v1

    #@90
    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    #@92
    .line 198
    :cond_92
    if-eqz v7, :cond_97

    #@94
    .line 199
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarOverlayLayout;->requestLayout()V

    #@97
    .line 202
    :cond_97
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->fitSystemWindows(Landroid/graphics/Rect;)Z

    #@9a
    move-result v0

    #@9b
    return v0

    #@9c
    .line 156
    .end local v7           #changed:Z
    .end local v8           #stable:Z
    :cond_9c
    const/4 v8, 0x0

    #@9d
    goto/16 :goto_c

    #@9f
    .line 173
    .restart local v7       #changed:Z
    .restart local v8       #stable:Z
    :cond_9f
    iget-object v1, p0, Lcom/android/internal/widget/ActionBarOverlayLayout;->mContent:Landroid/view/View;

    #@a1
    iget-object v2, p0, Lcom/android/internal/widget/ActionBarOverlayLayout;->mZeroRect:Landroid/graphics/Rect;

    #@a3
    const/4 v3, 0x1

    #@a4
    const/4 v4, 0x1

    #@a5
    const/4 v5, 0x1

    #@a6
    const/4 v6, 0x1

    #@a7
    move-object v0, p0

    #@a8
    invoke-direct/range {v0 .. v6}, Lcom/android/internal/widget/ActionBarOverlayLayout;->applyInsets(Landroid/view/View;Landroid/graphics/Rect;ZZZZ)Z

    #@ab
    move-result v0

    #@ac
    or-int/2addr v7, v0

    #@ad
    goto :goto_41
.end method

.method public onWindowSystemUiVisibilityChanged(I)V
    .registers 7
    .parameter "visible"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 102
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onWindowSystemUiVisibilityChanged(I)V

    #@4
    .line 103
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarOverlayLayout;->pullChildren()V

    #@7
    .line 104
    iget v4, p0, Lcom/android/internal/widget/ActionBarOverlayLayout;->mLastSystemUiVisibility:I

    #@9
    xor-int v1, v4, p1

    #@b
    .line 105
    .local v1, diff:I
    iput p1, p0, Lcom/android/internal/widget/ActionBarOverlayLayout;->mLastSystemUiVisibility:I

    #@d
    .line 106
    and-int/lit8 v4, p1, 0x4

    #@f
    if-nez v4, :cond_33

    #@11
    move v0, v3

    #@12
    .line 107
    .local v0, barVisible:Z
    :goto_12
    iget-object v4, p0, Lcom/android/internal/widget/ActionBarOverlayLayout;->mActionBar:Lcom/android/internal/app/ActionBarImpl;

    #@14
    if-eqz v4, :cond_35

    #@16
    iget-object v3, p0, Lcom/android/internal/widget/ActionBarOverlayLayout;->mActionBar:Lcom/android/internal/app/ActionBarImpl;

    #@18
    invoke-virtual {v3}, Lcom/android/internal/app/ActionBarImpl;->isSystemShowing()Z

    #@1b
    move-result v2

    #@1c
    .line 108
    .local v2, wasVisible:Z
    :goto_1c
    iget-object v3, p0, Lcom/android/internal/widget/ActionBarOverlayLayout;->mActionBar:Lcom/android/internal/app/ActionBarImpl;

    #@1e
    if-eqz v3, :cond_27

    #@20
    .line 109
    if-eqz v0, :cond_37

    #@22
    iget-object v3, p0, Lcom/android/internal/widget/ActionBarOverlayLayout;->mActionBar:Lcom/android/internal/app/ActionBarImpl;

    #@24
    invoke-virtual {v3}, Lcom/android/internal/app/ActionBarImpl;->showForSystem()V

    #@27
    .line 112
    :cond_27
    :goto_27
    and-int/lit16 v3, v1, 0x100

    #@29
    if-eqz v3, :cond_32

    #@2b
    .line 113
    iget-object v3, p0, Lcom/android/internal/widget/ActionBarOverlayLayout;->mActionBar:Lcom/android/internal/app/ActionBarImpl;

    #@2d
    if-eqz v3, :cond_32

    #@2f
    .line 114
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarOverlayLayout;->requestFitSystemWindows()V

    #@32
    .line 117
    :cond_32
    return-void

    #@33
    .line 106
    .end local v0           #barVisible:Z
    .end local v2           #wasVisible:Z
    :cond_33
    const/4 v0, 0x0

    #@34
    goto :goto_12

    #@35
    .restart local v0       #barVisible:Z
    :cond_35
    move v2, v3

    #@36
    .line 107
    goto :goto_1c

    #@37
    .line 110
    .restart local v2       #wasVisible:Z
    :cond_37
    iget-object v3, p0, Lcom/android/internal/widget/ActionBarOverlayLayout;->mActionBar:Lcom/android/internal/app/ActionBarImpl;

    #@39
    invoke-virtual {v3}, Lcom/android/internal/app/ActionBarImpl;->hideForSystem()V

    #@3c
    goto :goto_27
.end method

.method protected onWindowVisibilityChanged(I)V
    .registers 3
    .parameter "visibility"

    #@0
    .prologue
    .line 121
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onWindowVisibilityChanged(I)V

    #@3
    .line 122
    iput p1, p0, Lcom/android/internal/widget/ActionBarOverlayLayout;->mWindowVisibility:I

    #@5
    .line 123
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarOverlayLayout;->mActionBar:Lcom/android/internal/app/ActionBarImpl;

    #@7
    if-eqz v0, :cond_e

    #@9
    .line 124
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarOverlayLayout;->mActionBar:Lcom/android/internal/app/ActionBarImpl;

    #@b
    invoke-virtual {v0, p1}, Lcom/android/internal/app/ActionBarImpl;->setWindowVisibility(I)V

    #@e
    .line 126
    :cond_e
    return-void
.end method

.method pullChildren()V
    .registers 2

    #@0
    .prologue
    .line 206
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarOverlayLayout;->mContent:Landroid/view/View;

    #@2
    if-nez v0, :cond_35

    #@4
    .line 207
    const v0, 0x1020002

    #@7
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/ActionBarOverlayLayout;->findViewById(I)Landroid/view/View;

    #@a
    move-result-object v0

    #@b
    iput-object v0, p0, Lcom/android/internal/widget/ActionBarOverlayLayout;->mContent:Landroid/view/View;

    #@d
    .line 208
    const v0, 0x1020371

    #@10
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/ActionBarOverlayLayout;->findViewById(I)Landroid/view/View;

    #@13
    move-result-object v0

    #@14
    iput-object v0, p0, Lcom/android/internal/widget/ActionBarOverlayLayout;->mActionBarTop:Landroid/view/View;

    #@16
    .line 209
    const v0, 0x102036c

    #@19
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/ActionBarOverlayLayout;->findViewById(I)Landroid/view/View;

    #@1c
    move-result-object v0

    #@1d
    check-cast v0, Lcom/android/internal/widget/ActionBarContainer;

    #@1f
    iput-object v0, p0, Lcom/android/internal/widget/ActionBarOverlayLayout;->mContainerView:Lcom/android/internal/widget/ActionBarContainer;

    #@21
    .line 211
    const v0, 0x102036d

    #@24
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/ActionBarOverlayLayout;->findViewById(I)Landroid/view/View;

    #@27
    move-result-object v0

    #@28
    check-cast v0, Lcom/android/internal/widget/ActionBarView;

    #@2a
    iput-object v0, p0, Lcom/android/internal/widget/ActionBarOverlayLayout;->mActionView:Lcom/android/internal/widget/ActionBarView;

    #@2c
    .line 212
    const v0, 0x102036f

    #@2f
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/ActionBarOverlayLayout;->findViewById(I)Landroid/view/View;

    #@32
    move-result-object v0

    #@33
    iput-object v0, p0, Lcom/android/internal/widget/ActionBarOverlayLayout;->mActionBarBottom:Landroid/view/View;

    #@35
    .line 214
    :cond_35
    return-void
.end method

.method public setActionBar(Lcom/android/internal/app/ActionBarImpl;)V
    .registers 5
    .parameter "impl"

    #@0
    .prologue
    .line 67
    iput-object p1, p0, Lcom/android/internal/widget/ActionBarOverlayLayout;->mActionBar:Lcom/android/internal/app/ActionBarImpl;

    #@2
    .line 68
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarOverlayLayout;->getWindowToken()Landroid/os/IBinder;

    #@5
    move-result-object v1

    #@6
    if-eqz v1, :cond_1b

    #@8
    .line 71
    iget-object v1, p0, Lcom/android/internal/widget/ActionBarOverlayLayout;->mActionBar:Lcom/android/internal/app/ActionBarImpl;

    #@a
    iget v2, p0, Lcom/android/internal/widget/ActionBarOverlayLayout;->mWindowVisibility:I

    #@c
    invoke-virtual {v1, v2}, Lcom/android/internal/app/ActionBarImpl;->setWindowVisibility(I)V

    #@f
    .line 72
    iget v1, p0, Lcom/android/internal/widget/ActionBarOverlayLayout;->mLastSystemUiVisibility:I

    #@11
    if-eqz v1, :cond_1b

    #@13
    .line 73
    iget v0, p0, Lcom/android/internal/widget/ActionBarOverlayLayout;->mLastSystemUiVisibility:I

    #@15
    .line 74
    .local v0, newVis:I
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/ActionBarOverlayLayout;->onWindowSystemUiVisibilityChanged(I)V

    #@18
    .line 75
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarOverlayLayout;->requestFitSystemWindows()V

    #@1b
    .line 78
    .end local v0           #newVis:I
    :cond_1b
    return-void
.end method

.method public setShowingForActionMode(Z)V
    .registers 4
    .parameter "showing"

    #@0
    .prologue
    .line 81
    if-eqz p1, :cond_11

    #@2
    .line 90
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarOverlayLayout;->getWindowSystemUiVisibility()I

    #@5
    move-result v0

    #@6
    and-int/lit16 v0, v0, 0x500

    #@8
    const/16 v1, 0x500

    #@a
    if-ne v0, v1, :cond_10

    #@c
    .line 93
    const/4 v0, 0x4

    #@d
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/ActionBarOverlayLayout;->setDisabledSystemUiVisibility(I)V

    #@10
    .line 98
    :cond_10
    :goto_10
    return-void

    #@11
    .line 96
    :cond_11
    const/4 v0, 0x0

    #@12
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/ActionBarOverlayLayout;->setDisabledSystemUiVisibility(I)V

    #@15
    goto :goto_10
.end method
