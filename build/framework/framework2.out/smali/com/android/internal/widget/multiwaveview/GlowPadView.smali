.class public Lcom/android/internal/widget/multiwaveview/GlowPadView;
.super Landroid/view/View;
.source "GlowPadView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;,
        Lcom/android/internal/widget/multiwaveview/GlowPadView$OnTriggerListener;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final HIDE_ANIMATION_DELAY:I = 0xc8

.field private static final HIDE_ANIMATION_DURATION:I = 0xc8

.field private static final INITIAL_SHOW_HANDLE_DURATION:I = 0xc8

.field private static final RETURN_TO_HOME_DELAY:I = 0x4b0

.field private static final RETURN_TO_HOME_DURATION:I = 0xc8

.field private static final REVEAL_GLOW_DELAY:I = 0x0

.field private static final REVEAL_GLOW_DURATION:I = 0x0

.field private static final RING_SCALE_COLLAPSED:F = 0.5f

.field private static final RING_SCALE_EXPANDED:F = 1.0f

.field private static final SHOW_ANIMATION_DELAY:I = 0x32

.field private static final SHOW_ANIMATION_DURATION:I = 0xc8

.field private static final SNAP_MARGIN_DEFAULT:F = 20.0f

.field private static final STATE_FINISH:I = 0x5

.field private static final STATE_FIRST_TOUCH:I = 0x2

.field private static final STATE_IDLE:I = 0x0

.field private static final STATE_SNAP:I = 0x4

.field private static final STATE_START:I = 0x1

.field private static final STATE_TRACKING:I = 0x3

.field private static final TAG:Ljava/lang/String; = "GlowPadView"

.field private static final TAP_RADIUS_SCALE_ACCESSIBILITY_ENABLED:F = 1.3f

.field private static final TARGET_SCALE_COLLAPSED:F = 0.8f

.field private static final TARGET_SCALE_EXPANDED:F = 1.0f

.field private static final WAVE_ANIMATION_DURATION:I = 0x546


# instance fields
.field private mActiveTarget:I

.field private mAllowScaling:Z

.field private mAlwaysTrackFinger:Z

.field private mAnimatingTargets:Z

.field private mBackgroundAnimator:Lcom/android/internal/widget/multiwaveview/Tweener;

.field private mDirectionDescriptions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mDirectionDescriptionsResourceId:I

.field private mDragging:Z

.field private mFeedbackCount:I

.field private mFirstItemOffset:F

.field private mGlowAnimations:Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;

.field private mGlowRadius:F

.field private mGrabbedState:I

.field private mGravity:I

.field private mHandleDrawable:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

.field private mHorizontalInset:I

.field private mInitialLayout:Z

.field private mInnerRadius:F

.field private mMagneticTargets:Z

.field private mMaxTargetHeight:I

.field private mMaxTargetWidth:I

.field private mNewTargetResources:I

.field private mOnTriggerListener:Lcom/android/internal/widget/multiwaveview/GlowPadView$OnTriggerListener;

.field private mOuterRadius:F

.field private mOuterRing:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

.field private mPointCloud:Lcom/android/internal/widget/multiwaveview/PointCloud;

.field private mPointerId:I

.field private mResetListener:Landroid/animation/Animator$AnimatorListener;

.field private mResetListenerWithPing:Landroid/animation/Animator$AnimatorListener;

.field private mRingScaleFactor:F

.field private mShowSelectedTargetName:Z

.field private mSnapMargin:F

.field private mTargetAnimations:Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;

.field private mTargetDescriptions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mTargetDescriptionsResourceId:I

.field private mTargetDrawables:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/widget/multiwaveview/TargetDrawable;",
            ">;"
        }
    .end annotation
.end field

.field private mTargetResourceId:I

.field private mTargetUpdateListener:Landroid/animation/Animator$AnimatorListener;

.field private mUpdateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

.field private mVerticalInset:I

.field private mVibrationDuration:I

.field private mVibrator:Landroid/os/Vibrator;

.field private mWaveAnimations:Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;

.field private mWaveCenterX:F

.field private mWaveCenterY:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 212
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Lcom/android/internal/widget/multiwaveview/GlowPadView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 213
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 13
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 216
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@3
    .line 97
    new-instance v7, Ljava/util/ArrayList;

    #@5
    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    #@8
    iput-object v7, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetDrawables:Ljava/util/ArrayList;

    #@a
    .line 98
    new-instance v7, Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;

    #@c
    const/4 v8, 0x0

    #@d
    invoke-direct {v7, p0, v8}, Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;-><init>(Lcom/android/internal/widget/multiwaveview/GlowPadView;Lcom/android/internal/widget/multiwaveview/GlowPadView$1;)V

    #@10
    iput-object v7, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mWaveAnimations:Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;

    #@12
    .line 99
    new-instance v7, Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;

    #@14
    const/4 v8, 0x0

    #@15
    invoke-direct {v7, p0, v8}, Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;-><init>(Lcom/android/internal/widget/multiwaveview/GlowPadView;Lcom/android/internal/widget/multiwaveview/GlowPadView$1;)V

    #@18
    iput-object v7, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetAnimations:Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;

    #@1a
    .line 100
    new-instance v7, Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;

    #@1c
    const/4 v8, 0x0

    #@1d
    invoke-direct {v7, p0, v8}, Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;-><init>(Lcom/android/internal/widget/multiwaveview/GlowPadView;Lcom/android/internal/widget/multiwaveview/GlowPadView$1;)V

    #@20
    iput-object v7, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mGlowAnimations:Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;

    #@22
    .line 108
    const/4 v7, 0x3

    #@23
    iput v7, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mFeedbackCount:I

    #@25
    .line 109
    const/4 v7, 0x0

    #@26
    iput v7, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mVibrationDuration:I

    #@28
    .line 111
    const/4 v7, -0x1

    #@29
    iput v7, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mActiveTarget:I

    #@2b
    .line 117
    const/high16 v7, 0x3f80

    #@2d
    iput v7, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mRingScaleFactor:F

    #@2f
    .line 120
    const/4 v7, 0x0

    #@30
    iput v7, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mOuterRadius:F

    #@32
    .line 121
    const/4 v7, 0x0

    #@33
    iput v7, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mSnapMargin:F

    #@35
    .line 122
    const/4 v7, 0x0

    #@36
    iput v7, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mFirstItemOffset:F

    #@38
    .line 123
    const/4 v7, 0x0

    #@39
    iput-boolean v7, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mMagneticTargets:Z

    #@3b
    .line 163
    new-instance v7, Lcom/android/internal/widget/multiwaveview/GlowPadView$1;

    #@3d
    invoke-direct {v7, p0}, Lcom/android/internal/widget/multiwaveview/GlowPadView$1;-><init>(Lcom/android/internal/widget/multiwaveview/GlowPadView;)V

    #@40
    iput-object v7, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mResetListener:Landroid/animation/Animator$AnimatorListener;

    #@42
    .line 170
    new-instance v7, Lcom/android/internal/widget/multiwaveview/GlowPadView$2;

    #@44
    invoke-direct {v7, p0}, Lcom/android/internal/widget/multiwaveview/GlowPadView$2;-><init>(Lcom/android/internal/widget/multiwaveview/GlowPadView;)V

    #@47
    iput-object v7, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mResetListenerWithPing:Landroid/animation/Animator$AnimatorListener;

    #@49
    .line 178
    new-instance v7, Lcom/android/internal/widget/multiwaveview/GlowPadView$3;

    #@4b
    invoke-direct {v7, p0}, Lcom/android/internal/widget/multiwaveview/GlowPadView$3;-><init>(Lcom/android/internal/widget/multiwaveview/GlowPadView;)V

    #@4e
    iput-object v7, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mUpdateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    #@50
    .line 185
    new-instance v7, Lcom/android/internal/widget/multiwaveview/GlowPadView$4;

    #@52
    invoke-direct {v7, p0}, Lcom/android/internal/widget/multiwaveview/GlowPadView$4;-><init>(Lcom/android/internal/widget/multiwaveview/GlowPadView;)V

    #@55
    iput-object v7, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetUpdateListener:Landroid/animation/Animator$AnimatorListener;

    #@57
    .line 202
    const/16 v7, 0x30

    #@59
    iput v7, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mGravity:I

    #@5b
    .line 203
    const/4 v7, 0x1

    #@5c
    iput-boolean v7, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mInitialLayout:Z

    #@5e
    .line 209
    const/4 v7, 0x0

    #@5f
    iput-boolean v7, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mShowSelectedTargetName:Z

    #@61
    .line 217
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@64
    move-result-object v5

    #@65
    .line 219
    .local v5, res:Landroid/content/res/Resources;
    sget-object v7, Lcom/android/internal/R$styleable;->GlowPadView:[I

    #@67
    invoke-virtual {p1, p2, v7}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@6a
    move-result-object v0

    #@6b
    .line 220
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v7, 0x1

    #@6c
    iget v8, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mInnerRadius:F

    #@6e
    invoke-virtual {v0, v7, v8}, Landroid/content/res/TypedArray;->getDimension(IF)F

    #@71
    move-result v7

    #@72
    iput v7, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mInnerRadius:F

    #@74
    .line 221
    const/16 v7, 0xc

    #@76
    iget v8, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mOuterRadius:F

    #@78
    invoke-virtual {v0, v7, v8}, Landroid/content/res/TypedArray;->getDimension(IF)F

    #@7b
    move-result v7

    #@7c
    iput v7, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mOuterRadius:F

    #@7e
    .line 222
    const/16 v7, 0xe

    #@80
    iget v8, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mSnapMargin:F

    #@82
    invoke-virtual {v0, v7, v8}, Landroid/content/res/TypedArray;->getDimension(IF)F

    #@85
    move-result v7

    #@86
    iput v7, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mSnapMargin:F

    #@88
    .line 223
    const/4 v7, 0x7

    #@89
    iget v8, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mFirstItemOffset:F

    #@8b
    float-to-double v8, v8

    #@8c
    invoke-static {v8, v9}, Ljava/lang/Math;->toDegrees(D)D

    #@8f
    move-result-wide v8

    #@90
    double-to-float v8, v8

    #@91
    invoke-virtual {v0, v7, v8}, Landroid/content/res/TypedArray;->getFloat(IF)F

    #@94
    move-result v7

    #@95
    float-to-double v7, v7

    #@96
    invoke-static {v7, v8}, Ljava/lang/Math;->toRadians(D)D

    #@99
    move-result-wide v7

    #@9a
    double-to-float v7, v7

    #@9b
    iput v7, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mFirstItemOffset:F

    #@9d
    .line 226
    const/16 v7, 0xd

    #@9f
    iget v8, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mVibrationDuration:I

    #@a1
    invoke-virtual {v0, v7, v8}, Landroid/content/res/TypedArray;->getInt(II)I

    #@a4
    move-result v7

    #@a5
    iput v7, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mVibrationDuration:I

    #@a7
    .line 228
    const/16 v7, 0xf

    #@a9
    iget v8, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mFeedbackCount:I

    #@ab
    invoke-virtual {v0, v7, v8}, Landroid/content/res/TypedArray;->getInt(II)I

    #@ae
    move-result v7

    #@af
    iput v7, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mFeedbackCount:I

    #@b1
    .line 230
    const/16 v7, 0x9

    #@b3
    const/4 v8, 0x0

    #@b4
    invoke-virtual {v0, v7, v8}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@b7
    move-result v7

    #@b8
    iput-boolean v7, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mAllowScaling:Z

    #@ba
    .line 231
    const/16 v7, 0xb

    #@bc
    invoke-virtual {v0, v7}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    #@bf
    move-result-object v1

    #@c0
    .line 232
    .local v1, handle:Landroid/util/TypedValue;
    new-instance v8, Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@c2
    if-eqz v1, :cond_12a

    #@c4
    iget v7, v1, Landroid/util/TypedValue;->resourceId:I

    #@c6
    :goto_c6
    invoke-direct {v8, v5, v7}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;-><init>(Landroid/content/res/Resources;I)V

    #@c9
    iput-object v8, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mHandleDrawable:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@cb
    .line 233
    iget-object v7, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mHandleDrawable:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@cd
    sget-object v8, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->STATE_INACTIVE:[I

    #@cf
    invoke-virtual {v7, v8}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->setState([I)V

    #@d2
    .line 234
    new-instance v7, Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@d4
    const/4 v8, 0x4

    #@d5
    invoke-direct {p0, v0, v8}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->getResourceId(Landroid/content/res/TypedArray;I)I

    #@d8
    move-result v8

    #@d9
    invoke-direct {v7, v5, v8}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;-><init>(Landroid/content/res/Resources;I)V

    #@dc
    iput-object v7, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mOuterRing:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@de
    .line 237
    const/16 v7, 0x10

    #@e0
    const/4 v8, 0x0

    #@e1
    invoke-virtual {v0, v7, v8}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@e4
    move-result v7

    #@e5
    iput-boolean v7, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mAlwaysTrackFinger:Z

    #@e7
    .line 238
    const/16 v7, 0x8

    #@e9
    iget-boolean v8, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mMagneticTargets:Z

    #@eb
    invoke-virtual {v0, v7, v8}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@ee
    move-result v7

    #@ef
    iput-boolean v7, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mMagneticTargets:Z

    #@f1
    .line 240
    const/4 v7, 0x5

    #@f2
    invoke-direct {p0, v0, v7}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->getResourceId(Landroid/content/res/TypedArray;I)I

    #@f5
    move-result v4

    #@f6
    .line 241
    .local v4, pointId:I
    if-eqz v4, :cond_12c

    #@f8
    invoke-virtual {v5, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@fb
    move-result-object v3

    #@fc
    .line 242
    .local v3, pointDrawable:Landroid/graphics/drawable/Drawable;
    :goto_fc
    const/4 v7, 0x6

    #@fd
    const/4 v8, 0x0

    #@fe
    invoke-virtual {v0, v7, v8}, Landroid/content/res/TypedArray;->getDimension(IF)F

    #@101
    move-result v7

    #@102
    iput v7, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mGlowRadius:F

    #@104
    .line 244
    new-instance v2, Landroid/util/TypedValue;

    #@106
    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    #@109
    .line 247
    .local v2, outValue:Landroid/util/TypedValue;
    const/16 v7, 0xa

    #@10b
    invoke-virtual {v0, v7, v2}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    #@10e
    move-result v7

    #@10f
    if-eqz v7, :cond_116

    #@111
    .line 248
    iget v7, v2, Landroid/util/TypedValue;->resourceId:I

    #@113
    invoke-direct {p0, v7}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->internalSetTargetResources(I)V

    #@116
    .line 250
    :cond_116
    iget-object v7, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetDrawables:Ljava/util/ArrayList;

    #@118
    if-eqz v7, :cond_122

    #@11a
    iget-object v7, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetDrawables:Ljava/util/ArrayList;

    #@11c
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    #@11f
    move-result v7

    #@120
    if-nez v7, :cond_12e

    #@122
    .line 251
    :cond_122
    new-instance v7, Ljava/lang/IllegalStateException;

    #@124
    const-string v8, "Must specify at least one target drawable"

    #@126
    invoke-direct {v7, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@129
    throw v7

    #@12a
    .line 232
    .end local v2           #outValue:Landroid/util/TypedValue;
    .end local v3           #pointDrawable:Landroid/graphics/drawable/Drawable;
    .end local v4           #pointId:I
    :cond_12a
    const/4 v7, 0x0

    #@12b
    goto :goto_c6

    #@12c
    .line 241
    .restart local v4       #pointId:I
    :cond_12c
    const/4 v3, 0x0

    #@12d
    goto :goto_fc

    #@12e
    .line 255
    .restart local v2       #outValue:Landroid/util/TypedValue;
    .restart local v3       #pointDrawable:Landroid/graphics/drawable/Drawable;
    :cond_12e
    const/4 v7, 0x2

    #@12f
    invoke-virtual {v0, v7, v2}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    #@132
    move-result v7

    #@133
    if-eqz v7, :cond_144

    #@135
    .line 256
    iget v6, v2, Landroid/util/TypedValue;->resourceId:I

    #@137
    .line 257
    .local v6, resourceId:I
    if-nez v6, :cond_141

    #@139
    .line 258
    new-instance v7, Ljava/lang/IllegalStateException;

    #@13b
    const-string v8, "Must specify target descriptions"

    #@13d
    invoke-direct {v7, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@140
    throw v7

    #@141
    .line 260
    :cond_141
    invoke-virtual {p0, v6}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->setTargetDescriptionsResourceId(I)V

    #@144
    .line 264
    .end local v6           #resourceId:I
    :cond_144
    const/4 v7, 0x3

    #@145
    invoke-virtual {v0, v7, v2}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    #@148
    move-result v7

    #@149
    if-eqz v7, :cond_15a

    #@14b
    .line 265
    iget v6, v2, Landroid/util/TypedValue;->resourceId:I

    #@14d
    .line 266
    .restart local v6       #resourceId:I
    if-nez v6, :cond_157

    #@14f
    .line 267
    new-instance v7, Ljava/lang/IllegalStateException;

    #@151
    const-string v8, "Must specify direction descriptions"

    #@153
    invoke-direct {v7, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@156
    throw v7

    #@157
    .line 269
    :cond_157
    invoke-virtual {p0, v6}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->setDirectionDescriptionsResourceId(I)V

    #@15a
    .line 272
    .end local v6           #resourceId:I
    :cond_15a
    const/4 v7, 0x0

    #@15b
    const/16 v8, 0x30

    #@15d
    invoke-virtual {v0, v7, v8}, Landroid/content/res/TypedArray;->getInt(II)I

    #@160
    move-result v7

    #@161
    iput v7, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mGravity:I

    #@163
    .line 274
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@166
    .line 276
    iget v7, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mVibrationDuration:I

    #@168
    if-lez v7, :cond_18b

    #@16a
    const/4 v7, 0x1

    #@16b
    :goto_16b
    invoke-virtual {p0, v7}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->setVibrateEnabled(Z)V

    #@16e
    .line 278
    invoke-direct {p0}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->assignDefaultsIfNeeded()V

    #@171
    .line 280
    new-instance v7, Lcom/android/internal/widget/multiwaveview/PointCloud;

    #@173
    invoke-direct {v7, v3}, Lcom/android/internal/widget/multiwaveview/PointCloud;-><init>(Landroid/graphics/drawable/Drawable;)V

    #@176
    iput-object v7, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mPointCloud:Lcom/android/internal/widget/multiwaveview/PointCloud;

    #@178
    .line 281
    iget-object v7, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mPointCloud:Lcom/android/internal/widget/multiwaveview/PointCloud;

    #@17a
    iget v8, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mInnerRadius:F

    #@17c
    iget v9, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mOuterRadius:F

    #@17e
    invoke-virtual {v7, v8, v9}, Lcom/android/internal/widget/multiwaveview/PointCloud;->makePointCloud(FF)V

    #@181
    .line 282
    iget-object v7, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mPointCloud:Lcom/android/internal/widget/multiwaveview/PointCloud;

    #@183
    iget-object v7, v7, Lcom/android/internal/widget/multiwaveview/PointCloud;->glowManager:Lcom/android/internal/widget/multiwaveview/PointCloud$GlowManager;

    #@185
    iget v8, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mGlowRadius:F

    #@187
    invoke-virtual {v7, v8}, Lcom/android/internal/widget/multiwaveview/PointCloud$GlowManager;->setRadius(F)V

    #@18a
    .line 283
    return-void

    #@18b
    .line 276
    :cond_18b
    const/4 v7, 0x0

    #@18c
    goto :goto_16b
.end method

.method static synthetic access$100(Lcom/android/internal/widget/multiwaveview/GlowPadView;)F
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 54
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mWaveCenterX:F

    #@2
    return v0
.end method

.method static synthetic access$200(Lcom/android/internal/widget/multiwaveview/GlowPadView;)F
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 54
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mWaveCenterY:F

    #@2
    return v0
.end method

.method static synthetic access$300(Lcom/android/internal/widget/multiwaveview/GlowPadView;IFF)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 54
    invoke-direct {p0, p1, p2, p3}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->switchToState(IFF)V

    #@3
    return-void
.end method

.method static synthetic access$400(Lcom/android/internal/widget/multiwaveview/GlowPadView;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 54
    invoke-direct {p0}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->dispatchOnFinishFinalAnimation()V

    #@3
    return-void
.end method

.method static synthetic access$500(Lcom/android/internal/widget/multiwaveview/GlowPadView;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 54
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mNewTargetResources:I

    #@2
    return v0
.end method

.method static synthetic access$502(Lcom/android/internal/widget/multiwaveview/GlowPadView;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 54
    iput p1, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mNewTargetResources:I

    #@2
    return p1
.end method

.method static synthetic access$600(Lcom/android/internal/widget/multiwaveview/GlowPadView;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->internalSetTargetResources(I)V

    #@3
    return-void
.end method

.method static synthetic access$700(Lcom/android/internal/widget/multiwaveview/GlowPadView;ZZ)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->hideTargets(ZZ)V

    #@3
    return-void
.end method

.method static synthetic access$802(Lcom/android/internal/widget/multiwaveview/GlowPadView;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mAnimatingTargets:Z

    #@2
    return p1
.end method

.method static synthetic access$900(Lcom/android/internal/widget/multiwaveview/GlowPadView;)Lcom/android/internal/widget/multiwaveview/PointCloud;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 54
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mPointCloud:Lcom/android/internal/widget/multiwaveview/PointCloud;

    #@2
    return-object v0
.end method

.method private announceTargets()V
    .registers 9

    #@0
    .prologue
    .line 1239
    new-instance v5, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 1240
    .local v5, utterance:Ljava/lang/StringBuilder;
    iget-object v6, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetDrawables:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@a
    move-result v2

    #@b
    .line 1241
    .local v2, targetCount:I
    const/4 v1, 0x0

    #@c
    .local v1, i:I
    :goto_c
    if-ge v1, v2, :cond_32

    #@e
    .line 1242
    invoke-direct {p0, v1}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->getTargetDescription(I)Ljava/lang/String;

    #@11
    move-result-object v3

    #@12
    .line 1243
    .local v3, targetDescription:Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->getDirectionDescription(I)Ljava/lang/String;

    #@15
    move-result-object v0

    #@16
    .line 1244
    .local v0, directionDescription:Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@19
    move-result v6

    #@1a
    if-nez v6, :cond_2f

    #@1c
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@1f
    move-result v6

    #@20
    if-nez v6, :cond_2f

    #@22
    .line 1246
    const/4 v6, 0x1

    #@23
    new-array v6, v6, [Ljava/lang/Object;

    #@25
    const/4 v7, 0x0

    #@26
    aput-object v3, v6, v7

    #@28
    invoke-static {v0, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@2b
    move-result-object v4

    #@2c
    .line 1247
    .local v4, text:Ljava/lang/String;
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    .line 1241
    .end local v4           #text:Ljava/lang/String;
    :cond_2f
    add-int/lit8 v1, v1, 0x1

    #@31
    goto :goto_c

    #@32
    .line 1250
    .end local v0           #directionDescription:Ljava/lang/String;
    .end local v3           #targetDescription:Ljava/lang/String;
    :cond_32
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    #@35
    move-result v6

    #@36
    if-lez v6, :cond_3f

    #@38
    .line 1251
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v6

    #@3c
    invoke-virtual {p0, v6}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->announceForAccessibility(Ljava/lang/CharSequence;)V

    #@3f
    .line 1253
    :cond_3f
    return-void
.end method

.method private assignDefaultsIfNeeded()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1004
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mOuterRadius:F

    #@3
    cmpl-float v0, v0, v3

    #@5
    if-nez v0, :cond_1d

    #@7
    .line 1005
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mOuterRing:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@9
    invoke-virtual {v0}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->getWidth()I

    #@c
    move-result v0

    #@d
    iget-object v1, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mOuterRing:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@f
    invoke-virtual {v1}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->getHeight()I

    #@12
    move-result v1

    #@13
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@16
    move-result v0

    #@17
    int-to-float v0, v0

    #@18
    const/high16 v1, 0x4000

    #@1a
    div-float/2addr v0, v1

    #@1b
    iput v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mOuterRadius:F

    #@1d
    .line 1007
    :cond_1d
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mSnapMargin:F

    #@1f
    cmpl-float v0, v0, v3

    #@21
    if-nez v0, :cond_38

    #@23
    .line 1008
    const/4 v0, 0x1

    #@24
    const/high16 v1, 0x41a0

    #@26
    invoke-virtual {p0}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->getContext()Landroid/content/Context;

    #@29
    move-result-object v2

    #@2a
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@2d
    move-result-object v2

    #@2e
    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@31
    move-result-object v2

    #@32
    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    #@35
    move-result v0

    #@36
    iput v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mSnapMargin:F

    #@38
    .line 1011
    :cond_38
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mInnerRadius:F

    #@3a
    cmpl-float v0, v0, v3

    #@3c
    if-nez v0, :cond_4a

    #@3e
    .line 1012
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mHandleDrawable:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@40
    invoke-virtual {v0}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->getWidth()I

    #@43
    move-result v0

    #@44
    int-to-float v0, v0

    #@45
    const/high16 v1, 0x4120

    #@47
    div-float/2addr v0, v1

    #@48
    iput v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mInnerRadius:F

    #@4a
    .line 1014
    :cond_4a
    return-void
.end method

.method private computeInsets(II)V
    .registers 7
    .parameter "dx"
    .parameter "dy"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1017
    invoke-virtual {p0}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->getLayoutDirection()I

    #@4
    move-result v1

    #@5
    .line 1018
    .local v1, layoutDirection:I
    iget v2, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mGravity:I

    #@7
    invoke-static {v2, v1}, Landroid/view/Gravity;->getAbsoluteGravity(II)I

    #@a
    move-result v0

    #@b
    .line 1020
    .local v0, absoluteGravity:I
    and-int/lit8 v2, v0, 0x7

    #@d
    packed-switch v2, :pswitch_data_2a

    #@10
    .line 1029
    :pswitch_10
    div-int/lit8 v2, p1, 0x2

    #@12
    iput v2, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mHorizontalInset:I

    #@14
    .line 1032
    :goto_14
    and-int/lit8 v2, v0, 0x70

    #@16
    sparse-switch v2, :sswitch_data_34

    #@19
    .line 1041
    div-int/lit8 v2, p2, 0x2

    #@1b
    iput v2, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mVerticalInset:I

    #@1d
    .line 1044
    :goto_1d
    return-void

    #@1e
    .line 1022
    :pswitch_1e
    iput v3, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mHorizontalInset:I

    #@20
    goto :goto_14

    #@21
    .line 1025
    :pswitch_21
    iput p1, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mHorizontalInset:I

    #@23
    goto :goto_14

    #@24
    .line 1034
    :sswitch_24
    iput v3, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mVerticalInset:I

    #@26
    goto :goto_1d

    #@27
    .line 1037
    :sswitch_27
    iput p2, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mVerticalInset:I

    #@29
    goto :goto_1d

    #@2a
    .line 1020
    :pswitch_data_2a
    .packed-switch 0x3
        :pswitch_1e
        :pswitch_10
        :pswitch_21
    .end packed-switch

    #@34
    .line 1032
    :sswitch_data_34
    .sparse-switch
        0x30 -> :sswitch_24
        0x50 -> :sswitch_27
    .end sparse-switch
.end method

.method private computeScaleFactor(IIII)F
    .registers 12
    .parameter "desiredWidth"
    .parameter "desiredHeight"
    .parameter "actualWidth"
    .parameter "actualHeight"

    #@0
    .prologue
    const/high16 v4, 0x3f80

    #@2
    .line 1054
    iget-boolean v5, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mAllowScaling:Z

    #@4
    if-nez v5, :cond_7

    #@6
    .line 1090
    :goto_6
    return v4

    #@7
    .line 1056
    :cond_7
    invoke-virtual {p0}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->getLayoutDirection()I

    #@a
    move-result v1

    #@b
    .line 1057
    .local v1, layoutDirection:I
    iget v5, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mGravity:I

    #@d
    invoke-static {v5, v1}, Landroid/view/Gravity;->getAbsoluteGravity(II)I

    #@10
    move-result v0

    #@11
    .line 1059
    .local v0, absoluteGravity:I
    const/high16 v2, 0x3f80

    #@13
    .line 1060
    .local v2, scaleX:F
    const/high16 v3, 0x3f80

    #@15
    .line 1066
    .local v3, scaleY:F
    and-int/lit8 v5, v0, 0x7

    #@17
    packed-switch v5, :pswitch_data_42

    #@1a
    .line 1072
    :pswitch_1a
    if-le p1, p3, :cond_29

    #@1c
    .line 1073
    int-to-float v5, p3

    #@1d
    mul-float/2addr v5, v4

    #@1e
    iget v6, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mMaxTargetWidth:I

    #@20
    int-to-float v6, v6

    #@21
    sub-float/2addr v5, v6

    #@22
    iget v6, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mMaxTargetWidth:I

    #@24
    sub-int v6, p1, v6

    #@26
    int-to-float v6, v6

    #@27
    div-float v2, v5, v6

    #@29
    .line 1078
    :cond_29
    :pswitch_29
    and-int/lit8 v5, v0, 0x70

    #@2b
    sparse-switch v5, :sswitch_data_4c

    #@2e
    .line 1084
    if-le p2, p4, :cond_3d

    #@30
    .line 1085
    int-to-float v5, p4

    #@31
    mul-float/2addr v4, v5

    #@32
    iget v5, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mMaxTargetHeight:I

    #@34
    int-to-float v5, v5

    #@35
    sub-float/2addr v4, v5

    #@36
    iget v5, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mMaxTargetHeight:I

    #@38
    sub-int v5, p2, v5

    #@3a
    int-to-float v5, v5

    #@3b
    div-float v3, v4, v5

    #@3d
    .line 1090
    :cond_3d
    :sswitch_3d
    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    #@40
    move-result v4

    #@41
    goto :goto_6

    #@42
    .line 1066
    :pswitch_data_42
    .packed-switch 0x3
        :pswitch_29
        :pswitch_1a
        :pswitch_29
    .end packed-switch

    #@4c
    .line 1078
    :sswitch_data_4c
    .sparse-switch
        0x30 -> :sswitch_3d
        0x50 -> :sswitch_3d
    .end sparse-switch
.end method

.method private deactivateTargets()V
    .registers 5

    #@0
    .prologue
    .line 432
    iget-object v3, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetDrawables:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v0

    #@6
    .line 433
    .local v0, count:I
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    if-ge v1, v0, :cond_19

    #@9
    .line 434
    iget-object v3, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetDrawables:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v2

    #@f
    check-cast v2, Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@11
    .line 435
    .local v2, target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    sget-object v3, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->STATE_INACTIVE:[I

    #@13
    invoke-virtual {v2, v3}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->setState([I)V

    #@16
    .line 433
    add-int/lit8 v1, v1, 0x1

    #@18
    goto :goto_7

    #@19
    .line 437
    .end local v2           #target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    :cond_19
    const/4 v3, -0x1

    #@1a
    iput v3, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mActiveTarget:I

    #@1c
    .line 438
    return-void
.end method

.method private dispatchOnFinishFinalAnimation()V
    .registers 2

    #@0
    .prologue
    .line 452
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mOnTriggerListener:Lcom/android/internal/widget/multiwaveview/GlowPadView$OnTriggerListener;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 453
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mOnTriggerListener:Lcom/android/internal/widget/multiwaveview/GlowPadView$OnTriggerListener;

    #@6
    invoke-interface {v0}, Lcom/android/internal/widget/multiwaveview/GlowPadView$OnTriggerListener;->onFinishFinalAnimation()V

    #@9
    .line 455
    :cond_9
    return-void
.end method

.method private dispatchTriggerEvent(I)V
    .registers 3
    .parameter "whichTarget"

    #@0
    .prologue
    .line 445
    invoke-direct {p0}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->vibrate()V

    #@3
    .line 446
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mOnTriggerListener:Lcom/android/internal/widget/multiwaveview/GlowPadView$OnTriggerListener;

    #@5
    if-eqz v0, :cond_c

    #@7
    .line 447
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mOnTriggerListener:Lcom/android/internal/widget/multiwaveview/GlowPadView$OnTriggerListener;

    #@9
    invoke-interface {v0, p0, p1}, Lcom/android/internal/widget/multiwaveview/GlowPadView$OnTriggerListener;->onTrigger(Landroid/view/View;I)V

    #@c
    .line 449
    :cond_c
    return-void
.end method

.method private dist2(FF)F
    .registers 5
    .parameter "dx"
    .parameter "dy"

    #@0
    .prologue
    .line 1225
    mul-float v0, p1, p1

    #@2
    mul-float v1, p2, p2

    #@4
    add-float/2addr v0, v1

    #@5
    return v0
.end method

.method private doFinish()V
    .registers 8

    #@0
    .prologue
    const/16 v6, 0xc8

    #@2
    const/4 v2, 0x1

    #@3
    const/4 v5, 0x0

    #@4
    const/4 v3, 0x0

    #@5
    .line 458
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mActiveTarget:I

    #@7
    .line 459
    .local v0, activeTarget:I
    const/4 v4, -0x1

    #@8
    if-eq v0, v4, :cond_27

    #@a
    move v1, v2

    #@b
    .line 461
    .local v1, targetHit:Z
    :goto_b
    if-eqz v1, :cond_29

    #@d
    .line 464
    invoke-direct {p0, v0}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->highlightSelected(I)V

    #@10
    .line 467
    const/16 v2, 0x4b0

    #@12
    iget-object v4, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mResetListener:Landroid/animation/Animator$AnimatorListener;

    #@14
    invoke-direct {p0, v6, v2, v5, v4}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->hideGlow(IIFLandroid/animation/Animator$AnimatorListener;)V

    #@17
    .line 468
    invoke-direct {p0, v0}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->dispatchTriggerEvent(I)V

    #@1a
    .line 469
    iget-boolean v2, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mAlwaysTrackFinger:Z

    #@1c
    if-nez v2, :cond_23

    #@1e
    .line 471
    iget-object v2, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetAnimations:Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;

    #@20
    invoke-virtual {v2}, Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;->stop()V

    #@23
    .line 479
    :cond_23
    :goto_23
    invoke-direct {p0, v3}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->setGrabbedState(I)V

    #@26
    .line 480
    return-void

    #@27
    .end local v1           #targetHit:Z
    :cond_27
    move v1, v3

    #@28
    .line 459
    goto :goto_b

    #@29
    .line 475
    .restart local v1       #targetHit:Z
    :cond_29
    iget-object v4, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mResetListenerWithPing:Landroid/animation/Animator$AnimatorListener;

    #@2b
    invoke-direct {p0, v6, v3, v5, v4}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->hideGlow(IIFLandroid/animation/Animator$AnimatorListener;)V

    #@2e
    .line 476
    invoke-direct {p0, v2, v3}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->hideTargets(ZZ)V

    #@31
    goto :goto_23
.end method

.method private dump()V
    .registers 4

    #@0
    .prologue
    .line 291
    const-string v0, "GlowPadView"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "Outer Radius = "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget v2, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mOuterRadius:F

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 292
    const-string v0, "GlowPadView"

    #@1c
    new-instance v1, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v2, "SnapMargin = "

    #@23
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v1

    #@27
    iget v2, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mSnapMargin:F

    #@29
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v1

    #@2d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v1

    #@31
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    .line 293
    const-string v0, "GlowPadView"

    #@36
    new-instance v1, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v2, "FeedbackCount = "

    #@3d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v1

    #@41
    iget v2, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mFeedbackCount:I

    #@43
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@46
    move-result-object v1

    #@47
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v1

    #@4b
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    .line 294
    const-string v0, "GlowPadView"

    #@50
    new-instance v1, Ljava/lang/StringBuilder;

    #@52
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@55
    const-string v2, "VibrationDuration = "

    #@57
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v1

    #@5b
    iget v2, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mVibrationDuration:I

    #@5d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@60
    move-result-object v1

    #@61
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v1

    #@65
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@68
    .line 295
    const-string v0, "GlowPadView"

    #@6a
    new-instance v1, Ljava/lang/StringBuilder;

    #@6c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@6f
    const-string v2, "GlowRadius = "

    #@71
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v1

    #@75
    iget v2, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mGlowRadius:F

    #@77
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v1

    #@7b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7e
    move-result-object v1

    #@7f
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@82
    .line 296
    const-string v0, "GlowPadView"

    #@84
    new-instance v1, Ljava/lang/StringBuilder;

    #@86
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@89
    const-string v2, "WaveCenterX = "

    #@8b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v1

    #@8f
    iget v2, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mWaveCenterX:F

    #@91
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@94
    move-result-object v1

    #@95
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@98
    move-result-object v1

    #@99
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@9c
    .line 297
    const-string v0, "GlowPadView"

    #@9e
    new-instance v1, Ljava/lang/StringBuilder;

    #@a0
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a3
    const-string v2, "WaveCenterY = "

    #@a5
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v1

    #@a9
    iget v2, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mWaveCenterY:F

    #@ab
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v1

    #@af
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b2
    move-result-object v1

    #@b3
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b6
    .line 298
    return-void
.end method

.method private getAngle(FI)F
    .registers 5
    .parameter "alpha"
    .parameter "i"

    #@0
    .prologue
    .line 1191
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mFirstItemOffset:F

    #@2
    int-to-float v1, p2

    #@3
    mul-float/2addr v1, p1

    #@4
    add-float/2addr v0, v1

    #@5
    return v0
.end method

.method private getDirectionDescription(I)Ljava/lang/String;
    .registers 6
    .parameter "index"

    #@0
    .prologue
    .line 1278
    iget-object v2, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mDirectionDescriptions:Ljava/util/ArrayList;

    #@2
    if-eqz v2, :cond_c

    #@4
    iget-object v2, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mDirectionDescriptions:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_2b

    #@c
    .line 1279
    :cond_c
    iget v2, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mDirectionDescriptionsResourceId:I

    #@e
    invoke-direct {p0, v2}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->loadDescriptions(I)Ljava/util/ArrayList;

    #@11
    move-result-object v2

    #@12
    iput-object v2, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mDirectionDescriptions:Ljava/util/ArrayList;

    #@14
    .line 1280
    iget-object v2, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetDrawables:Ljava/util/ArrayList;

    #@16
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@19
    move-result v2

    #@1a
    iget-object v3, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mDirectionDescriptions:Ljava/util/ArrayList;

    #@1c
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@1f
    move-result v3

    #@20
    if-eq v2, v3, :cond_2b

    #@22
    .line 1281
    const-string v2, "GlowPadView"

    #@24
    const-string v3, "The number of target drawables must be equal to the number of direction descriptions."

    #@26
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 1283
    const/4 v0, 0x0

    #@2a
    .line 1293
    :goto_2a
    return-object v0

    #@2b
    .line 1289
    :cond_2b
    :try_start_2b
    iget-object v2, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mDirectionDescriptions:Ljava/util/ArrayList;

    #@2d
    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@30
    move-result-object v0

    #@31
    check-cast v0, Ljava/lang/String;
    :try_end_33
    .catch Ljava/lang/Exception; {:try_start_2b .. :try_end_33} :catch_34

    #@33
    .line 1291
    .local v0, direction2:Ljava/lang/String;
    goto :goto_2a

    #@34
    .line 1292
    .end local v0           #direction2:Ljava/lang/String;
    :catch_34
    move-exception v1

    #@35
    .line 1293
    .local v1, e:Ljava/lang/Exception;
    const-string v0, ""

    #@37
    goto :goto_2a
.end method

.method private getResourceId(Landroid/content/res/TypedArray;I)I
    .registers 5
    .parameter "a"
    .parameter "id"

    #@0
    .prologue
    .line 286
    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    #@3
    move-result-object v0

    #@4
    .line 287
    .local v0, tv:Landroid/util/TypedValue;
    if-nez v0, :cond_8

    #@6
    const/4 v1, 0x0

    #@7
    :goto_7
    return v1

    #@8
    :cond_8
    iget v1, v0, Landroid/util/TypedValue;->resourceId:I

    #@a
    goto :goto_7
.end method

.method private getRingHeight()F
    .registers 5

    #@0
    .prologue
    .line 1115
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mRingScaleFactor:F

    #@2
    iget-object v1, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mOuterRing:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@4
    invoke-virtual {v1}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->getHeight()I

    #@7
    move-result v1

    #@8
    int-to-float v1, v1

    #@9
    const/high16 v2, 0x4000

    #@b
    iget v3, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mOuterRadius:F

    #@d
    mul-float/2addr v2, v3

    #@e
    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    #@11
    move-result v1

    #@12
    mul-float/2addr v0, v1

    #@13
    return v0
.end method

.method private getRingWidth()F
    .registers 5

    #@0
    .prologue
    .line 1111
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mRingScaleFactor:F

    #@2
    iget-object v1, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mOuterRing:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@4
    invoke-virtual {v1}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->getWidth()I

    #@7
    move-result v1

    #@8
    int-to-float v1, v1

    #@9
    const/high16 v2, 0x4000

    #@b
    iget v3, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mOuterRadius:F

    #@d
    mul-float/2addr v2, v3

    #@e
    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    #@11
    move-result v1

    #@12
    mul-float/2addr v0, v1

    #@13
    return v0
.end method

.method private getScaledGlowRadiusSquared()F
    .registers 4

    #@0
    .prologue
    .line 1230
    iget-object v1, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mContext:Landroid/content/Context;

    #@2
    invoke-static {v1}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_18

    #@c
    .line 1231
    const v1, 0x3fa66666

    #@f
    iget v2, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mGlowRadius:F

    #@11
    mul-float v0, v1, v2

    #@13
    .line 1235
    .local v0, scaledTapRadius:F
    :goto_13
    invoke-direct {p0, v0}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->square(F)F

    #@16
    move-result v1

    #@17
    return v1

    #@18
    .line 1233
    .end local v0           #scaledTapRadius:F
    :cond_18
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mGlowRadius:F

    #@1a
    .restart local v0       #scaledTapRadius:F
    goto :goto_13
.end method

.method private getSliceAngle()F
    .registers 5

    #@0
    .prologue
    .line 1195
    const-wide v0, -0x3fe6de04abbbd2e8L

    #@5
    iget-object v2, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetDrawables:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@a
    move-result v2

    #@b
    int-to-double v2, v2

    #@c
    div-double/2addr v0, v2

    #@d
    double-to-float v0, v0

    #@e
    return v0
.end method

.method private getTargetDescription(I)Ljava/lang/String;
    .registers 7
    .parameter "index"

    #@0
    .prologue
    .line 1256
    iget-object v3, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetDescriptions:Ljava/util/ArrayList;

    #@2
    if-eqz v3, :cond_c

    #@4
    iget-object v3, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetDescriptions:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    #@9
    move-result v3

    #@a
    if-eqz v3, :cond_2b

    #@c
    .line 1257
    :cond_c
    iget v3, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetDescriptionsResourceId:I

    #@e
    invoke-direct {p0, v3}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->loadDescriptions(I)Ljava/util/ArrayList;

    #@11
    move-result-object v3

    #@12
    iput-object v3, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetDescriptions:Ljava/util/ArrayList;

    #@14
    .line 1258
    iget-object v3, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetDrawables:Ljava/util/ArrayList;

    #@16
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@19
    move-result v3

    #@1a
    iget-object v4, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetDescriptions:Ljava/util/ArrayList;

    #@1c
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@1f
    move-result v4

    #@20
    if-eq v3, v4, :cond_2b

    #@22
    .line 1259
    const-string v3, "GlowPadView"

    #@24
    const-string v4, "The number of target drawables must be equal to the number of target descriptions."

    #@26
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 1261
    const/4 v2, 0x0

    #@2a
    .line 1273
    :goto_2a
    return-object v2

    #@2b
    .line 1266
    :cond_2b
    const/4 v2, 0x0

    #@2c
    .line 1269
    .local v2, target2:Ljava/lang/String;
    :try_start_2c
    iget-object v3, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetDescriptions:Ljava/util/ArrayList;

    #@2e
    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@31
    move-result-object v3

    #@32
    move-object v0, v3

    #@33
    check-cast v0, Ljava/lang/String;

    #@35
    move-object v2, v0
    :try_end_36
    .catch Ljava/lang/Exception; {:try_start_2c .. :try_end_36} :catch_37

    #@36
    .line 1271
    goto :goto_2a

    #@37
    .line 1272
    :catch_37
    move-exception v1

    #@38
    .line 1273
    .local v1, e:Ljava/lang/Exception;
    goto :goto_2a
.end method

.method private handleCancel(Landroid/view/MotionEvent;)V
    .registers 6
    .parameter "event"

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    .line 835
    iput v2, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mActiveTarget:I

    #@3
    .line 837
    iget v1, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mPointerId:I

    #@5
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    #@8
    move-result v0

    #@9
    .line 838
    .local v0, actionIndex:I
    if-ne v0, v2, :cond_c

    #@b
    const/4 v0, 0x0

    #@c
    .line 839
    :cond_c
    const/4 v1, 0x5

    #@d
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    #@10
    move-result v2

    #@11
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    #@14
    move-result v3

    #@15
    invoke-direct {p0, v1, v2, v3}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->switchToState(IFF)V

    #@18
    .line 840
    return-void
.end method

.method private handleDown(Landroid/view/MotionEvent;)V
    .registers 6
    .parameter "event"

    #@0
    .prologue
    .line 811
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    #@3
    move-result v0

    #@4
    .line 812
    .local v0, actionIndex:I
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    #@7
    move-result v1

    #@8
    .line 813
    .local v1, eventX:F
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    #@b
    move-result v2

    #@c
    .line 814
    .local v2, eventY:F
    const/4 v3, 0x1

    #@d
    invoke-direct {p0, v3, v1, v2}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->switchToState(IFF)V

    #@10
    .line 815
    invoke-direct {p0, v1, v2}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->trySwitchToFirstTouchState(FF)Z

    #@13
    move-result v3

    #@14
    if-nez v3, :cond_1a

    #@16
    .line 816
    const/4 v3, 0x0

    #@17
    iput-boolean v3, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mDragging:Z

    #@19
    .line 821
    :goto_19
    return-void

    #@1a
    .line 818
    :cond_1a
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@1d
    move-result v3

    #@1e
    iput v3, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mPointerId:I

    #@20
    .line 819
    invoke-direct {p0, v1, v2}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->updateGlowPosition(FF)V

    #@23
    goto :goto_19
.end method

.method private handleMove(Landroid/view/MotionEvent;)V
    .registers 41
    .parameter "event"

    #@0
    .prologue
    .line 843
    const/4 v6, -0x1

    #@1
    .line 844
    .local v6, activeTarget:I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getHistorySize()I

    #@4
    move-result v12

    #@5
    .line 845
    .local v12, historySize:I
    move-object/from16 v0, p0

    #@7
    iget-object v0, v0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetDrawables:Ljava/util/ArrayList;

    #@9
    move-object/from16 v27, v0

    #@b
    .line 846
    .local v27, targets:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/widget/multiwaveview/TargetDrawable;>;"
    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->size()I

    #@e
    move-result v17

    #@f
    .line 847
    .local v17, ntargets:I
    const/16 v31, 0x0

    #@11
    .line 848
    .local v31, x:F
    const/16 v32, 0x0

    #@13
    .line 849
    .local v32, y:F
    const/4 v5, 0x0

    #@14
    .line 850
    .local v5, activeAngle:F
    move-object/from16 v0, p0

    #@16
    iget v0, v0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mPointerId:I

    #@18
    move/from16 v33, v0

    #@1a
    move-object/from16 v0, p1

    #@1c
    move/from16 v1, v33

    #@1e
    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    #@21
    move-result v4

    #@22
    .line 852
    .local v4, actionIndex:I
    const/16 v33, -0x1

    #@24
    move/from16 v0, v33

    #@26
    if-ne v4, v0, :cond_29

    #@28
    .line 945
    :cond_28
    :goto_28
    return-void

    #@29
    .line 856
    :cond_29
    const/4 v14, 0x0

    #@2a
    .local v14, k:I
    :goto_2a
    add-int/lit8 v33, v12, 0x1

    #@2c
    move/from16 v0, v33

    #@2e
    if-ge v14, v0, :cond_18b

    #@30
    .line 857
    if-ge v14, v12, :cond_16d

    #@32
    move-object/from16 v0, p1

    #@34
    invoke-virtual {v0, v4, v14}, Landroid/view/MotionEvent;->getHistoricalX(II)F

    #@37
    move-result v10

    #@38
    .line 859
    .local v10, eventX:F
    :goto_38
    if-ge v14, v12, :cond_175

    #@3a
    move-object/from16 v0, p1

    #@3c
    invoke-virtual {v0, v4, v14}, Landroid/view/MotionEvent;->getHistoricalY(II)F

    #@3f
    move-result v11

    #@40
    .line 862
    .local v11, eventY:F
    :goto_40
    move-object/from16 v0, p0

    #@42
    iget v0, v0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mWaveCenterX:F

    #@44
    move/from16 v33, v0

    #@46
    sub-float v29, v10, v33

    #@48
    .line 863
    .local v29, tx:F
    move-object/from16 v0, p0

    #@4a
    iget v0, v0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mWaveCenterY:F

    #@4c
    move/from16 v33, v0

    #@4e
    sub-float v30, v11, v33

    #@50
    .line 864
    .local v30, ty:F
    move-object/from16 v0, p0

    #@52
    move/from16 v1, v29

    #@54
    move/from16 v2, v30

    #@56
    invoke-direct {v0, v1, v2}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->dist2(FF)F

    #@59
    move-result v33

    #@5a
    move/from16 v0, v33

    #@5c
    float-to-double v0, v0

    #@5d
    move-wide/from16 v33, v0

    #@5f
    invoke-static/range {v33 .. v34}, Ljava/lang/Math;->sqrt(D)D

    #@62
    move-result-wide v33

    #@63
    move-wide/from16 v0, v33

    #@65
    double-to-float v0, v0

    #@66
    move/from16 v28, v0

    #@68
    .line 865
    .local v28, touchRadius:F
    move-object/from16 v0, p0

    #@6a
    iget v0, v0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mOuterRadius:F

    #@6c
    move/from16 v33, v0

    #@6e
    cmpl-float v33, v28, v33

    #@70
    if-lez v33, :cond_17d

    #@72
    move-object/from16 v0, p0

    #@74
    iget v0, v0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mOuterRadius:F

    #@76
    move/from16 v33, v0

    #@78
    div-float v18, v33, v28

    #@7a
    .line 866
    .local v18, scale:F
    :goto_7a
    mul-float v15, v29, v18

    #@7c
    .line 867
    .local v15, limitX:F
    mul-float v16, v30, v18

    #@7e
    .line 868
    .local v16, limitY:F
    move/from16 v0, v30

    #@80
    neg-float v0, v0

    #@81
    move/from16 v33, v0

    #@83
    move/from16 v0, v33

    #@85
    float-to-double v0, v0

    #@86
    move-wide/from16 v33, v0

    #@88
    move/from16 v0, v29

    #@8a
    float-to-double v0, v0

    #@8b
    move-wide/from16 v35, v0

    #@8d
    invoke-static/range {v33 .. v36}, Ljava/lang/Math;->atan2(DD)D

    #@90
    move-result-wide v8

    #@91
    .line 870
    .local v8, angleRad:D
    move-object/from16 v0, p0

    #@93
    iget-boolean v0, v0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mDragging:Z

    #@95
    move/from16 v33, v0

    #@97
    if-nez v33, :cond_9e

    #@99
    .line 871
    move-object/from16 v0, p0

    #@9b
    invoke-direct {v0, v10, v11}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->trySwitchToFirstTouchState(FF)Z

    #@9e
    .line 874
    :cond_9e
    move-object/from16 v0, p0

    #@a0
    iget-boolean v0, v0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mDragging:Z

    #@a2
    move/from16 v33, v0

    #@a4
    if-eqz v33, :cond_183

    #@a6
    .line 876
    move-object/from16 v0, p0

    #@a8
    iget v0, v0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mRingScaleFactor:F

    #@aa
    move/from16 v33, v0

    #@ac
    move-object/from16 v0, p0

    #@ae
    iget v0, v0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mOuterRadius:F

    #@b0
    move/from16 v34, v0

    #@b2
    mul-float v33, v33, v34

    #@b4
    move-object/from16 v0, p0

    #@b6
    iget v0, v0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mSnapMargin:F

    #@b8
    move/from16 v34, v0

    #@ba
    sub-float v20, v33, v34

    #@bc
    .line 877
    .local v20, snapRadius:F
    mul-float v19, v20, v20

    #@be
    .line 879
    .local v19, snapDistance2:F
    const/4 v13, 0x0

    #@bf
    .local v13, i:I
    :goto_bf
    move/from16 v0, v17

    #@c1
    if-ge v13, v0, :cond_183

    #@c3
    .line 880
    move-object/from16 v0, v27

    #@c5
    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@c8
    move-result-object v21

    #@c9
    check-cast v21, Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@cb
    .line 882
    .local v21, target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    move-object/from16 v0, p0

    #@cd
    iget v0, v0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mFirstItemOffset:F

    #@cf
    move/from16 v33, v0

    #@d1
    move/from16 v0, v33

    #@d3
    float-to-double v0, v0

    #@d4
    move-wide/from16 v33, v0

    #@d6
    int-to-double v0, v13

    #@d7
    move-wide/from16 v35, v0

    #@d9
    const-wide/high16 v37, 0x3fe0

    #@db
    sub-double v35, v35, v37

    #@dd
    const-wide/high16 v37, 0x4000

    #@df
    mul-double v35, v35, v37

    #@e1
    const-wide v37, 0x400921fb54442d18L

    #@e6
    mul-double v35, v35, v37

    #@e8
    move/from16 v0, v17

    #@ea
    int-to-double v0, v0

    #@eb
    move-wide/from16 v37, v0

    #@ed
    div-double v35, v35, v37

    #@ef
    add-double v25, v33, v35

    #@f1
    .line 883
    .local v25, targetMinRad:D
    move-object/from16 v0, p0

    #@f3
    iget v0, v0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mFirstItemOffset:F

    #@f5
    move/from16 v33, v0

    #@f7
    move/from16 v0, v33

    #@f9
    float-to-double v0, v0

    #@fa
    move-wide/from16 v33, v0

    #@fc
    int-to-double v0, v13

    #@fd
    move-wide/from16 v35, v0

    #@ff
    const-wide/high16 v37, 0x3fe0

    #@101
    add-double v35, v35, v37

    #@103
    const-wide/high16 v37, 0x4000

    #@105
    mul-double v35, v35, v37

    #@107
    const-wide v37, 0x400921fb54442d18L

    #@10c
    mul-double v35, v35, v37

    #@10e
    move/from16 v0, v17

    #@110
    int-to-double v0, v0

    #@111
    move-wide/from16 v37, v0

    #@113
    div-double v35, v35, v37

    #@115
    add-double v23, v33, v35

    #@117
    .line 884
    .local v23, targetMaxRad:D
    invoke-virtual/range {v21 .. v21}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->isEnabled()Z

    #@11a
    move-result v33

    #@11b
    if-eqz v33, :cond_169

    #@11d
    .line 885
    cmpl-double v33, v8, v25

    #@11f
    if-lez v33, :cond_125

    #@121
    cmpg-double v33, v8, v23

    #@123
    if-lez v33, :cond_151

    #@125
    :cond_125
    const-wide v33, 0x401921fb54442d18L

    #@12a
    add-double v33, v33, v8

    #@12c
    cmpl-double v33, v33, v25

    #@12e
    if-lez v33, :cond_13b

    #@130
    const-wide v33, 0x401921fb54442d18L

    #@135
    add-double v33, v33, v8

    #@137
    cmpg-double v33, v33, v23

    #@139
    if-lez v33, :cond_151

    #@13b
    :cond_13b
    const-wide v33, 0x401921fb54442d18L

    #@140
    sub-double v33, v8, v33

    #@142
    cmpl-double v33, v33, v25

    #@144
    if-lez v33, :cond_181

    #@146
    const-wide v33, 0x401921fb54442d18L

    #@14b
    sub-double v33, v8, v33

    #@14d
    cmpg-double v33, v33, v23

    #@14f
    if-gtz v33, :cond_181

    #@151
    :cond_151
    const/4 v7, 0x1

    #@152
    .line 891
    .local v7, angleMatches:Z
    :goto_152
    if-eqz v7, :cond_169

    #@154
    move-object/from16 v0, p0

    #@156
    move/from16 v1, v29

    #@158
    move/from16 v2, v30

    #@15a
    invoke-direct {v0, v1, v2}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->dist2(FF)F

    #@15d
    move-result v33

    #@15e
    cmpl-float v33, v33, v19

    #@160
    if-lez v33, :cond_169

    #@162
    .line 892
    move v6, v13

    #@163
    .line 893
    neg-double v0, v8

    #@164
    move-wide/from16 v33, v0

    #@166
    move-wide/from16 v0, v33

    #@168
    double-to-float v5, v0

    #@169
    .line 879
    .end local v7           #angleMatches:Z
    :cond_169
    add-int/lit8 v13, v13, 0x1

    #@16b
    goto/16 :goto_bf

    #@16d
    .line 857
    .end local v8           #angleRad:D
    .end local v10           #eventX:F
    .end local v11           #eventY:F
    .end local v13           #i:I
    .end local v15           #limitX:F
    .end local v16           #limitY:F
    .end local v18           #scale:F
    .end local v19           #snapDistance2:F
    .end local v20           #snapRadius:F
    .end local v21           #target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    .end local v23           #targetMaxRad:D
    .end local v25           #targetMinRad:D
    .end local v28           #touchRadius:F
    .end local v29           #tx:F
    .end local v30           #ty:F
    :cond_16d
    move-object/from16 v0, p1

    #@16f
    invoke-virtual {v0, v4}, Landroid/view/MotionEvent;->getX(I)F

    #@172
    move-result v10

    #@173
    goto/16 :goto_38

    #@175
    .line 859
    .restart local v10       #eventX:F
    :cond_175
    move-object/from16 v0, p1

    #@177
    invoke-virtual {v0, v4}, Landroid/view/MotionEvent;->getY(I)F

    #@17a
    move-result v11

    #@17b
    goto/16 :goto_40

    #@17d
    .line 865
    .restart local v11       #eventY:F
    .restart local v28       #touchRadius:F
    .restart local v29       #tx:F
    .restart local v30       #ty:F
    :cond_17d
    const/high16 v18, 0x3f80

    #@17f
    goto/16 :goto_7a

    #@181
    .line 885
    .restart local v8       #angleRad:D
    .restart local v13       #i:I
    .restart local v15       #limitX:F
    .restart local v16       #limitY:F
    .restart local v18       #scale:F
    .restart local v19       #snapDistance2:F
    .restart local v20       #snapRadius:F
    .restart local v21       #target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    .restart local v23       #targetMaxRad:D
    .restart local v25       #targetMinRad:D
    :cond_181
    const/4 v7, 0x0

    #@182
    goto :goto_152

    #@183
    .line 898
    .end local v13           #i:I
    .end local v19           #snapDistance2:F
    .end local v20           #snapRadius:F
    .end local v21           #target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    .end local v23           #targetMaxRad:D
    .end local v25           #targetMinRad:D
    :cond_183
    move/from16 v31, v15

    #@185
    .line 899
    move/from16 v32, v16

    #@187
    .line 856
    add-int/lit8 v14, v14, 0x1

    #@189
    goto/16 :goto_2a

    #@18b
    .line 902
    .end local v8           #angleRad:D
    .end local v10           #eventX:F
    .end local v11           #eventY:F
    .end local v15           #limitX:F
    .end local v16           #limitY:F
    .end local v18           #scale:F
    .end local v28           #touchRadius:F
    .end local v29           #tx:F
    .end local v30           #ty:F
    :cond_18b
    move-object/from16 v0, p0

    #@18d
    iget-boolean v0, v0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mDragging:Z

    #@18f
    move/from16 v33, v0

    #@191
    if-eqz v33, :cond_28

    #@193
    .line 906
    const/16 v33, -0x1

    #@195
    move/from16 v0, v33

    #@197
    if-eq v6, v0, :cond_27e

    #@199
    .line 907
    const/16 v33, 0x4

    #@19b
    move-object/from16 v0, p0

    #@19d
    move/from16 v1, v33

    #@19f
    move/from16 v2, v31

    #@1a1
    move/from16 v3, v32

    #@1a3
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->switchToState(IFF)V

    #@1a6
    .line 908
    move-object/from16 v0, p0

    #@1a8
    move/from16 v1, v31

    #@1aa
    move/from16 v2, v32

    #@1ac
    invoke-direct {v0, v1, v2}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->updateGlowPosition(FF)V

    #@1af
    .line 914
    :goto_1af
    move-object/from16 v0, p0

    #@1b1
    iget v0, v0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mActiveTarget:I

    #@1b3
    move/from16 v33, v0

    #@1b5
    move/from16 v0, v33

    #@1b7
    if-eq v0, v6, :cond_278

    #@1b9
    .line 916
    move-object/from16 v0, p0

    #@1bb
    iget v0, v0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mActiveTarget:I

    #@1bd
    move/from16 v33, v0

    #@1bf
    const/16 v34, -0x1

    #@1c1
    move/from16 v0, v33

    #@1c3
    move/from16 v1, v34

    #@1c5
    if-eq v0, v1, :cond_216

    #@1c7
    .line 917
    move-object/from16 v0, p0

    #@1c9
    iget v0, v0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mActiveTarget:I

    #@1cb
    move/from16 v33, v0

    #@1cd
    move-object/from16 v0, v27

    #@1cf
    move/from16 v1, v33

    #@1d1
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1d4
    move-result-object v21

    #@1d5
    check-cast v21, Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@1d7
    .line 918
    .restart local v21       #target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    sget-object v33, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->STATE_FOCUSED:[I

    #@1d9
    move-object/from16 v0, v21

    #@1db
    move-object/from16 v1, v33

    #@1dd
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->hasState([I)Z

    #@1e0
    move-result v33

    #@1e1
    if-eqz v33, :cond_1ec

    #@1e3
    .line 919
    sget-object v33, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->STATE_INACTIVE:[I

    #@1e5
    move-object/from16 v0, v21

    #@1e7
    move-object/from16 v1, v33

    #@1e9
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->setState([I)V

    #@1ec
    .line 921
    :cond_1ec
    move-object/from16 v0, p0

    #@1ee
    iget-boolean v0, v0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mMagneticTargets:Z

    #@1f0
    move/from16 v33, v0

    #@1f2
    if-eqz v33, :cond_211

    #@1f4
    .line 922
    move-object/from16 v0, p0

    #@1f6
    iget v0, v0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mActiveTarget:I

    #@1f8
    move/from16 v33, v0

    #@1fa
    move-object/from16 v0, p0

    #@1fc
    iget v0, v0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mWaveCenterX:F

    #@1fe
    move/from16 v34, v0

    #@200
    move-object/from16 v0, p0

    #@202
    iget v0, v0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mWaveCenterY:F

    #@204
    move/from16 v35, v0

    #@206
    move-object/from16 v0, p0

    #@208
    move/from16 v1, v33

    #@20a
    move/from16 v2, v34

    #@20c
    move/from16 v3, v35

    #@20e
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->updateTargetPosition(IFF)V

    #@211
    .line 925
    :cond_211
    move-object/from16 v0, p0

    #@213
    invoke-virtual {v0, v6}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->showSelectedTargetName(I)V

    #@216
    .line 928
    .end local v21           #target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    :cond_216
    const/16 v33, -0x1

    #@218
    move/from16 v0, v33

    #@21a
    if-eq v6, v0, :cond_278

    #@21c
    .line 929
    move-object/from16 v0, v27

    #@21e
    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@221
    move-result-object v21

    #@222
    check-cast v21, Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@224
    .line 930
    .restart local v21       #target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    sget-object v33, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->STATE_FOCUSED:[I

    #@226
    move-object/from16 v0, v21

    #@228
    move-object/from16 v1, v33

    #@22a
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->hasState([I)Z

    #@22d
    move-result v33

    #@22e
    if-eqz v33, :cond_239

    #@230
    .line 931
    sget-object v33, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->STATE_FOCUSED:[I

    #@232
    move-object/from16 v0, v21

    #@234
    move-object/from16 v1, v33

    #@236
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->setState([I)V

    #@239
    .line 933
    :cond_239
    move-object/from16 v0, p0

    #@23b
    iget-boolean v0, v0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mMagneticTargets:Z

    #@23d
    move/from16 v33, v0

    #@23f
    if-eqz v33, :cond_256

    #@241
    .line 934
    move-object/from16 v0, p0

    #@243
    iget v0, v0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mWaveCenterX:F

    #@245
    move/from16 v33, v0

    #@247
    move-object/from16 v0, p0

    #@249
    iget v0, v0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mWaveCenterY:F

    #@24b
    move/from16 v34, v0

    #@24d
    move-object/from16 v0, p0

    #@24f
    move/from16 v1, v33

    #@251
    move/from16 v2, v34

    #@253
    invoke-direct {v0, v6, v1, v2, v5}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->updateTargetPosition(IFFF)V

    #@256
    .line 936
    :cond_256
    move-object/from16 v0, p0

    #@258
    iget-object v0, v0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mContext:Landroid/content/Context;

    #@25a
    move-object/from16 v33, v0

    #@25c
    invoke-static/range {v33 .. v33}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    #@25f
    move-result-object v33

    #@260
    invoke-virtual/range {v33 .. v33}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    #@263
    move-result v33

    #@264
    if-eqz v33, :cond_273

    #@266
    .line 937
    move-object/from16 v0, p0

    #@268
    invoke-direct {v0, v6}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->getTargetDescription(I)Ljava/lang/String;

    #@26b
    move-result-object v22

    #@26c
    .line 938
    .local v22, targetContentDescription:Ljava/lang/String;
    move-object/from16 v0, p0

    #@26e
    move-object/from16 v1, v22

    #@270
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->announceForAccessibility(Ljava/lang/CharSequence;)V

    #@273
    .line 941
    .end local v22           #targetContentDescription:Ljava/lang/String;
    :cond_273
    move-object/from16 v0, p0

    #@275
    invoke-virtual {v0, v6}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->showSelectedTargetName(I)V

    #@278
    .line 944
    .end local v21           #target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    :cond_278
    move-object/from16 v0, p0

    #@27a
    iput v6, v0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mActiveTarget:I

    #@27c
    goto/16 :goto_28

    #@27e
    .line 910
    :cond_27e
    const/16 v33, 0x3

    #@280
    move-object/from16 v0, p0

    #@282
    move/from16 v1, v33

    #@284
    move/from16 v2, v31

    #@286
    move/from16 v3, v32

    #@288
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->switchToState(IFF)V

    #@28b
    .line 911
    move-object/from16 v0, p0

    #@28d
    move/from16 v1, v31

    #@28f
    move/from16 v2, v32

    #@291
    invoke-direct {v0, v1, v2}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->updateGlowPosition(FF)V

    #@294
    goto/16 :goto_1af
.end method

.method private handleUp(Landroid/view/MotionEvent;)V
    .registers 6
    .parameter "event"

    #@0
    .prologue
    .line 825
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    #@3
    move-result v0

    #@4
    .line 826
    .local v0, actionIndex:I
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    #@7
    move-result v1

    #@8
    iget v2, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mPointerId:I

    #@a
    if-ne v1, v2, :cond_18

    #@c
    .line 827
    const/4 v1, 0x5

    #@d
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    #@10
    move-result v2

    #@11
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    #@14
    move-result v3

    #@15
    invoke-direct {p0, v1, v2, v3}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->switchToState(IFF)V

    #@18
    .line 829
    :cond_18
    return-void
.end method

.method private hideGlow(IIFLandroid/animation/Animator$AnimatorListener;)V
    .registers 13
    .parameter "duration"
    .parameter "delay"
    .parameter "finalAlpha"
    .parameter "finishListener"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 419
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mGlowAnimations:Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;

    #@3
    invoke-virtual {v0}, Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;->cancel()V

    #@6
    .line 420
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mGlowAnimations:Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;

    #@8
    iget-object v1, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mPointCloud:Lcom/android/internal/widget/multiwaveview/PointCloud;

    #@a
    iget-object v1, v1, Lcom/android/internal/widget/multiwaveview/PointCloud;->glowManager:Lcom/android/internal/widget/multiwaveview/PointCloud$GlowManager;

    #@c
    int-to-long v2, p1

    #@d
    const/16 v4, 0xe

    #@f
    new-array v4, v4, [Ljava/lang/Object;

    #@11
    const/4 v5, 0x0

    #@12
    const-string v6, "ease"

    #@14
    aput-object v6, v4, v5

    #@16
    const/4 v5, 0x1

    #@17
    sget-object v6, Lcom/android/internal/widget/multiwaveview/Ease$Quart;->easeOut:Landroid/animation/TimeInterpolator;

    #@19
    aput-object v6, v4, v5

    #@1b
    const/4 v5, 0x2

    #@1c
    const-string v6, "delay"

    #@1e
    aput-object v6, v4, v5

    #@20
    const/4 v5, 0x3

    #@21
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@24
    move-result-object v6

    #@25
    aput-object v6, v4, v5

    #@27
    const/4 v5, 0x4

    #@28
    const-string v6, "alpha"

    #@2a
    aput-object v6, v4, v5

    #@2c
    const/4 v5, 0x5

    #@2d
    invoke-static {p3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@30
    move-result-object v6

    #@31
    aput-object v6, v4, v5

    #@33
    const/4 v5, 0x6

    #@34
    const-string v6, "x"

    #@36
    aput-object v6, v4, v5

    #@38
    const/4 v5, 0x7

    #@39
    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@3c
    move-result-object v6

    #@3d
    aput-object v6, v4, v5

    #@3f
    const/16 v5, 0x8

    #@41
    const-string v6, "y"

    #@43
    aput-object v6, v4, v5

    #@45
    const/16 v5, 0x9

    #@47
    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@4a
    move-result-object v6

    #@4b
    aput-object v6, v4, v5

    #@4d
    const/16 v5, 0xa

    #@4f
    const-string v6, "onUpdate"

    #@51
    aput-object v6, v4, v5

    #@53
    const/16 v5, 0xb

    #@55
    iget-object v6, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mUpdateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    #@57
    aput-object v6, v4, v5

    #@59
    const/16 v5, 0xc

    #@5b
    const-string v6, "onComplete"

    #@5d
    aput-object v6, v4, v5

    #@5f
    const/16 v5, 0xd

    #@61
    aput-object p4, v4, v5

    #@63
    invoke-static {v1, v2, v3, v4}, Lcom/android/internal/widget/multiwaveview/Tweener;->to(Ljava/lang/Object;J[Ljava/lang/Object;)Lcom/android/internal/widget/multiwaveview/Tweener;

    #@66
    move-result-object v1

    #@67
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;->add(Ljava/lang/Object;)Z

    #@6a
    .line 428
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mGlowAnimations:Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;

    #@6c
    invoke-virtual {v0}, Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;->start()V

    #@6f
    .line 429
    return-void
.end method

.method private hideTargets(ZZ)V
    .registers 20
    .parameter "animate"
    .parameter "expanded"

    #@0
    .prologue
    .line 497
    move-object/from16 v0, p0

    #@2
    iget-object v10, v0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetAnimations:Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;

    #@4
    invoke-virtual {v10}, Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;->cancel()V

    #@7
    .line 500
    move/from16 v0, p1

    #@9
    move-object/from16 v1, p0

    #@b
    iput-boolean v0, v1, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mAnimatingTargets:Z

    #@d
    .line 501
    if-eqz p1, :cond_91

    #@f
    const/16 v3, 0xc8

    #@11
    .line 502
    .local v3, duration:I
    :goto_11
    if-eqz p1, :cond_94

    #@13
    const/16 v2, 0xc8

    #@15
    .line 504
    .local v2, delay:I
    :goto_15
    if-eqz p2, :cond_96

    #@17
    const/high16 v9, 0x3f80

    #@19
    .line 506
    .local v9, targetScale:F
    :goto_19
    move-object/from16 v0, p0

    #@1b
    iget-object v10, v0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetDrawables:Ljava/util/ArrayList;

    #@1d
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    #@20
    move-result v6

    #@21
    .line 507
    .local v6, length:I
    sget-object v5, Lcom/android/internal/widget/multiwaveview/Ease$Cubic;->easeOut:Landroid/animation/TimeInterpolator;

    #@23
    .line 508
    .local v5, interpolator:Landroid/animation/TimeInterpolator;
    const/4 v4, 0x0

    #@24
    .local v4, i:I
    :goto_24
    if-ge v4, v6, :cond_9a

    #@26
    .line 509
    move-object/from16 v0, p0

    #@28
    iget-object v10, v0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetDrawables:Ljava/util/ArrayList;

    #@2a
    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@2d
    move-result-object v8

    #@2e
    check-cast v8, Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@30
    .line 510
    .local v8, target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    sget-object v10, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->STATE_INACTIVE:[I

    #@32
    invoke-virtual {v8, v10}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->setState([I)V

    #@35
    .line 511
    move-object/from16 v0, p0

    #@37
    iget-object v10, v0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetAnimations:Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;

    #@39
    int-to-long v11, v3

    #@3a
    const/16 v13, 0xc

    #@3c
    new-array v13, v13, [Ljava/lang/Object;

    #@3e
    const/4 v14, 0x0

    #@3f
    const-string v15, "ease"

    #@41
    aput-object v15, v13, v14

    #@43
    const/4 v14, 0x1

    #@44
    aput-object v5, v13, v14

    #@46
    const/4 v14, 0x2

    #@47
    const-string v15, "alpha"

    #@49
    aput-object v15, v13, v14

    #@4b
    const/4 v14, 0x3

    #@4c
    const/4 v15, 0x0

    #@4d
    invoke-static {v15}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@50
    move-result-object v15

    #@51
    aput-object v15, v13, v14

    #@53
    const/4 v14, 0x4

    #@54
    const-string v15, "scaleX"

    #@56
    aput-object v15, v13, v14

    #@58
    const/4 v14, 0x5

    #@59
    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@5c
    move-result-object v15

    #@5d
    aput-object v15, v13, v14

    #@5f
    const/4 v14, 0x6

    #@60
    const-string v15, "scaleY"

    #@62
    aput-object v15, v13, v14

    #@64
    const/4 v14, 0x7

    #@65
    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@68
    move-result-object v15

    #@69
    aput-object v15, v13, v14

    #@6b
    const/16 v14, 0x8

    #@6d
    const-string v15, "delay"

    #@6f
    aput-object v15, v13, v14

    #@71
    const/16 v14, 0x9

    #@73
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@76
    move-result-object v15

    #@77
    aput-object v15, v13, v14

    #@79
    const/16 v14, 0xa

    #@7b
    const-string v15, "onUpdate"

    #@7d
    aput-object v15, v13, v14

    #@7f
    const/16 v14, 0xb

    #@81
    move-object/from16 v0, p0

    #@83
    iget-object v15, v0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mUpdateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    #@85
    aput-object v15, v13, v14

    #@87
    invoke-static {v8, v11, v12, v13}, Lcom/android/internal/widget/multiwaveview/Tweener;->to(Ljava/lang/Object;J[Ljava/lang/Object;)Lcom/android/internal/widget/multiwaveview/Tweener;

    #@8a
    move-result-object v11

    #@8b
    invoke-virtual {v10, v11}, Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;->add(Ljava/lang/Object;)Z

    #@8e
    .line 508
    add-int/lit8 v4, v4, 0x1

    #@90
    goto :goto_24

    #@91
    .line 501
    .end local v2           #delay:I
    .end local v3           #duration:I
    .end local v4           #i:I
    .end local v5           #interpolator:Landroid/animation/TimeInterpolator;
    .end local v6           #length:I
    .end local v8           #target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    .end local v9           #targetScale:F
    :cond_91
    const/4 v3, 0x0

    #@92
    goto/16 :goto_11

    #@94
    .line 502
    .restart local v3       #duration:I
    :cond_94
    const/4 v2, 0x0

    #@95
    goto :goto_15

    #@96
    .line 504
    .restart local v2       #delay:I
    :cond_96
    const v9, 0x3f4ccccd

    #@99
    goto :goto_19

    #@9a
    .line 520
    .restart local v4       #i:I
    .restart local v5       #interpolator:Landroid/animation/TimeInterpolator;
    .restart local v6       #length:I
    .restart local v9       #targetScale:F
    :cond_9a
    if-eqz p2, :cond_11b

    #@9c
    const/high16 v7, 0x3f80

    #@9e
    .line 522
    .local v7, ringScaleTarget:F
    :goto_9e
    move-object/from16 v0, p0

    #@a0
    iget v10, v0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mRingScaleFactor:F

    #@a2
    mul-float/2addr v7, v10

    #@a3
    .line 523
    move-object/from16 v0, p0

    #@a5
    iget-object v10, v0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetAnimations:Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;

    #@a7
    move-object/from16 v0, p0

    #@a9
    iget-object v11, v0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mOuterRing:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@ab
    int-to-long v12, v3

    #@ac
    const/16 v14, 0xe

    #@ae
    new-array v14, v14, [Ljava/lang/Object;

    #@b0
    const/4 v15, 0x0

    #@b1
    const-string v16, "ease"

    #@b3
    aput-object v16, v14, v15

    #@b5
    const/4 v15, 0x1

    #@b6
    aput-object v5, v14, v15

    #@b8
    const/4 v15, 0x2

    #@b9
    const-string v16, "alpha"

    #@bb
    aput-object v16, v14, v15

    #@bd
    const/4 v15, 0x3

    #@be
    const/16 v16, 0x0

    #@c0
    invoke-static/range {v16 .. v16}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@c3
    move-result-object v16

    #@c4
    aput-object v16, v14, v15

    #@c6
    const/4 v15, 0x4

    #@c7
    const-string v16, "scaleX"

    #@c9
    aput-object v16, v14, v15

    #@cb
    const/4 v15, 0x5

    #@cc
    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@cf
    move-result-object v16

    #@d0
    aput-object v16, v14, v15

    #@d2
    const/4 v15, 0x6

    #@d3
    const-string v16, "scaleY"

    #@d5
    aput-object v16, v14, v15

    #@d7
    const/4 v15, 0x7

    #@d8
    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@db
    move-result-object v16

    #@dc
    aput-object v16, v14, v15

    #@de
    const/16 v15, 0x8

    #@e0
    const-string v16, "delay"

    #@e2
    aput-object v16, v14, v15

    #@e4
    const/16 v15, 0x9

    #@e6
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@e9
    move-result-object v16

    #@ea
    aput-object v16, v14, v15

    #@ec
    const/16 v15, 0xa

    #@ee
    const-string v16, "onUpdate"

    #@f0
    aput-object v16, v14, v15

    #@f2
    const/16 v15, 0xb

    #@f4
    move-object/from16 v0, p0

    #@f6
    iget-object v0, v0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mUpdateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    #@f8
    move-object/from16 v16, v0

    #@fa
    aput-object v16, v14, v15

    #@fc
    const/16 v15, 0xc

    #@fe
    const-string v16, "onComplete"

    #@100
    aput-object v16, v14, v15

    #@102
    const/16 v15, 0xd

    #@104
    move-object/from16 v0, p0

    #@106
    iget-object v0, v0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetUpdateListener:Landroid/animation/Animator$AnimatorListener;

    #@108
    move-object/from16 v16, v0

    #@10a
    aput-object v16, v14, v15

    #@10c
    invoke-static {v11, v12, v13, v14}, Lcom/android/internal/widget/multiwaveview/Tweener;->to(Ljava/lang/Object;J[Ljava/lang/Object;)Lcom/android/internal/widget/multiwaveview/Tweener;

    #@10f
    move-result-object v11

    #@110
    invoke-virtual {v10, v11}, Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;->add(Ljava/lang/Object;)Z

    #@113
    .line 532
    move-object/from16 v0, p0

    #@115
    iget-object v10, v0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetAnimations:Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;

    #@117
    invoke-virtual {v10}, Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;->start()V

    #@11a
    .line 533
    return-void

    #@11b
    .line 520
    .end local v7           #ringScaleTarget:F
    :cond_11b
    const/high16 v7, 0x3f00

    #@11d
    goto :goto_9e
.end method

.method private hideUnselected(I)V
    .registers 5
    .parameter "active"

    #@0
    .prologue
    .line 489
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    iget-object v1, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetDrawables:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v1

    #@7
    if-ge v0, v1, :cond_1a

    #@9
    .line 490
    if-eq v0, p1, :cond_17

    #@b
    .line 491
    iget-object v1, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetDrawables:Ljava/util/ArrayList;

    #@d
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@10
    move-result-object v1

    #@11
    check-cast v1, Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@13
    const/4 v2, 0x0

    #@14
    invoke-virtual {v1, v2}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->setAlpha(F)V

    #@17
    .line 489
    :cond_17
    add-int/lit8 v0, v0, 0x1

    #@19
    goto :goto_1

    #@1a
    .line 494
    :cond_1a
    return-void
.end method

.method private highlightSelected(I)V
    .registers 4
    .parameter "activeTarget"

    #@0
    .prologue
    .line 484
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetDrawables:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@8
    sget-object v1, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->STATE_ACTIVE:[I

    #@a
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->setState([I)V

    #@d
    .line 485
    invoke-direct {p0, p1}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->hideUnselected(I)V

    #@10
    .line 486
    return-void
.end method

.method private internalSetTargetResources(I)V
    .registers 10
    .parameter "resourceId"

    #@0
    .prologue
    .line 590
    invoke-direct {p0, p1}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->loadDrawableArray(I)Ljava/util/ArrayList;

    #@3
    move-result-object v5

    #@4
    .line 591
    .local v5, targets:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/widget/multiwaveview/TargetDrawable;>;"
    iput-object v5, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetDrawables:Ljava/util/ArrayList;

    #@6
    .line 592
    iput p1, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetResourceId:I

    #@8
    .line 594
    iget-object v6, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mHandleDrawable:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@a
    invoke-virtual {v6}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->getWidth()I

    #@d
    move-result v3

    #@e
    .line 595
    .local v3, maxWidth:I
    iget-object v6, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mHandleDrawable:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@10
    invoke-virtual {v6}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->getHeight()I

    #@13
    move-result v2

    #@14
    .line 596
    .local v2, maxHeight:I
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@17
    move-result v0

    #@18
    .line 597
    .local v0, count:I
    const/4 v1, 0x0

    #@19
    .local v1, i:I
    :goto_19
    if-ge v1, v0, :cond_34

    #@1b
    .line 598
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1e
    move-result-object v4

    #@1f
    check-cast v4, Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@21
    .line 599
    .local v4, target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    invoke-virtual {v4}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->getWidth()I

    #@24
    move-result v6

    #@25
    invoke-static {v3, v6}, Ljava/lang/Math;->max(II)I

    #@28
    move-result v3

    #@29
    .line 600
    invoke-virtual {v4}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->getHeight()I

    #@2c
    move-result v6

    #@2d
    invoke-static {v2, v6}, Ljava/lang/Math;->max(II)I

    #@30
    move-result v2

    #@31
    .line 597
    add-int/lit8 v1, v1, 0x1

    #@33
    goto :goto_19

    #@34
    .line 602
    .end local v4           #target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    :cond_34
    iget v6, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mMaxTargetWidth:I

    #@36
    if-ne v6, v3, :cond_3c

    #@38
    iget v6, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mMaxTargetHeight:I

    #@3a
    if-eq v6, v2, :cond_44

    #@3c
    .line 603
    :cond_3c
    iput v3, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mMaxTargetWidth:I

    #@3e
    .line 604
    iput v2, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mMaxTargetHeight:I

    #@40
    .line 605
    invoke-virtual {p0}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->requestLayout()V

    #@43
    .line 610
    :goto_43
    return-void

    #@44
    .line 607
    :cond_44
    iget v6, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mWaveCenterX:F

    #@46
    iget v7, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mWaveCenterY:F

    #@48
    invoke-direct {p0, v6, v7}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->updateTargetPositions(FF)V

    #@4b
    .line 608
    iget v6, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mWaveCenterX:F

    #@4d
    iget v7, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mWaveCenterY:F

    #@4f
    invoke-direct {p0, v6, v7}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->updatePointCloudPosition(FF)V

    #@52
    goto :goto_43
.end method

.method private loadDescriptions(I)Ljava/util/ArrayList;
    .registers 8
    .parameter "resourceId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1298
    invoke-virtual {p0}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->getContext()Landroid/content/Context;

    #@3
    move-result-object v5

    #@4
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@7
    move-result-object v5

    #@8
    invoke-virtual {v5, p1}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    #@b
    move-result-object v0

    #@c
    .line 1299
    .local v0, array:Landroid/content/res/TypedArray;
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->length()I

    #@f
    move-result v2

    #@10
    .line 1300
    .local v2, count:I
    new-instance v4, Ljava/util/ArrayList;

    #@12
    invoke-direct {v4, v2}, Ljava/util/ArrayList;-><init>(I)V

    #@15
    .line 1301
    .local v4, targetContentDescriptions:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v3, 0x0

    #@16
    .local v3, i:I
    :goto_16
    if-ge v3, v2, :cond_22

    #@18
    .line 1302
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    .line 1303
    .local v1, contentDescription:Ljava/lang/String;
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1f
    .line 1301
    add-int/lit8 v3, v3, 0x1

    #@21
    goto :goto_16

    #@22
    .line 1305
    .end local v1           #contentDescription:Ljava/lang/String;
    :cond_22
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@25
    .line 1306
    return-object v4
.end method

.method private loadDrawableArray(I)Ljava/util/ArrayList;
    .registers 10
    .parameter "resourceId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/widget/multiwaveview/TargetDrawable;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 576
    invoke-virtual {p0}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->getContext()Landroid/content/Context;

    #@3
    move-result-object v7

    #@4
    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@7
    move-result-object v4

    #@8
    .line 577
    .local v4, res:Landroid/content/res/Resources;
    invoke-virtual {v4, p1}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    #@b
    move-result-object v0

    #@c
    .line 578
    .local v0, array:Landroid/content/res/TypedArray;
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->length()I

    #@f
    move-result v1

    #@10
    .line 579
    .local v1, count:I
    new-instance v2, Ljava/util/ArrayList;

    #@12
    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    #@15
    .line 580
    .local v2, drawables:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/widget/multiwaveview/TargetDrawable;>;"
    const/4 v3, 0x0

    #@16
    .local v3, i:I
    :goto_16
    if-ge v3, v1, :cond_2d

    #@18
    .line 581
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    #@1b
    move-result-object v6

    #@1c
    .line 582
    .local v6, value:Landroid/util/TypedValue;
    new-instance v5, Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@1e
    if-eqz v6, :cond_2b

    #@20
    iget v7, v6, Landroid/util/TypedValue;->resourceId:I

    #@22
    :goto_22
    invoke-direct {v5, v4, v7}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;-><init>(Landroid/content/res/Resources;I)V

    #@25
    .line 583
    .local v5, target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@28
    .line 580
    add-int/lit8 v3, v3, 0x1

    #@2a
    goto :goto_16

    #@2b
    .line 582
    .end local v5           #target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    :cond_2b
    const/4 v7, 0x0

    #@2c
    goto :goto_22

    #@2d
    .line 585
    .end local v6           #value:Landroid/util/TypedValue;
    :cond_2d
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@30
    .line 586
    return-object v2
.end method

.method private replaceTargetDrawables(Landroid/content/res/Resources;II)Z
    .registers 10
    .parameter "res"
    .parameter "existingResourceId"
    .parameter "newResourceId"

    #@0
    .prologue
    .line 1341
    if-eqz p2, :cond_4

    #@2
    if-nez p3, :cond_6

    #@4
    .line 1342
    :cond_4
    const/4 v2, 0x0

    #@5
    .line 1360
    :cond_5
    :goto_5
    return v2

    #@6
    .line 1345
    :cond_6
    const/4 v2, 0x0

    #@7
    .line 1346
    .local v2, result:Z
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetDrawables:Ljava/util/ArrayList;

    #@9
    .line 1347
    .local v0, drawables:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/widget/multiwaveview/TargetDrawable;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@c
    move-result v3

    #@d
    .line 1348
    .local v3, size:I
    const/4 v1, 0x0

    #@e
    .local v1, i:I
    :goto_e
    if-ge v1, v3, :cond_25

    #@10
    .line 1349
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@13
    move-result-object v4

    #@14
    check-cast v4, Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@16
    .line 1350
    .local v4, target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    if-eqz v4, :cond_22

    #@18
    invoke-virtual {v4}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->getResourceId()I

    #@1b
    move-result v5

    #@1c
    if-ne v5, p2, :cond_22

    #@1e
    .line 1351
    invoke-virtual {v4, p1, p3}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->setDrawable(Landroid/content/res/Resources;I)V

    #@21
    .line 1352
    const/4 v2, 0x1

    #@22
    .line 1348
    :cond_22
    add-int/lit8 v1, v1, 0x1

    #@24
    goto :goto_e

    #@25
    .line 1356
    .end local v4           #target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    :cond_25
    if-eqz v2, :cond_5

    #@27
    .line 1357
    invoke-virtual {p0}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->requestLayout()V

    #@2a
    goto :goto_5
.end method

.method private resolveMeasured(II)I
    .registers 6
    .parameter "measureSpec"
    .parameter "desired"

    #@0
    .prologue
    .line 347
    const/4 v0, 0x0

    #@1
    .line 348
    .local v0, result:I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@4
    move-result v1

    #@5
    .line 349
    .local v1, specSize:I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@8
    move-result v2

    #@9
    sparse-switch v2, :sswitch_data_16

    #@c
    .line 358
    move v0, v1

    #@d
    .line 360
    :goto_d
    return v0

    #@e
    .line 351
    :sswitch_e
    move v0, p2

    #@f
    .line 352
    goto :goto_d

    #@10
    .line 354
    :sswitch_10
    invoke-static {v1, p2}, Ljava/lang/Math;->min(II)I

    #@13
    move-result v0

    #@14
    .line 355
    goto :goto_d

    #@15
    .line 349
    nop

    #@16
    :sswitch_data_16
    .sparse-switch
        -0x80000000 -> :sswitch_10
        0x0 -> :sswitch_e
    .end sparse-switch
.end method

.method private setGrabbedState(I)V
    .registers 4
    .parameter "newState"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 974
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mGrabbedState:I

    #@3
    if-eq p1, v0, :cond_1c

    #@5
    .line 975
    if-eqz p1, :cond_a

    #@7
    .line 976
    invoke-direct {p0}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->vibrate()V

    #@a
    .line 978
    :cond_a
    iput p1, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mGrabbedState:I

    #@c
    .line 979
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mOnTriggerListener:Lcom/android/internal/widget/multiwaveview/GlowPadView$OnTriggerListener;

    #@e
    if-eqz v0, :cond_1c

    #@10
    .line 980
    if-nez p1, :cond_1d

    #@12
    .line 981
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mOnTriggerListener:Lcom/android/internal/widget/multiwaveview/GlowPadView$OnTriggerListener;

    #@14
    invoke-interface {v0, p0, v1}, Lcom/android/internal/widget/multiwaveview/GlowPadView$OnTriggerListener;->onReleased(Landroid/view/View;I)V

    #@17
    .line 985
    :goto_17
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mOnTriggerListener:Lcom/android/internal/widget/multiwaveview/GlowPadView$OnTriggerListener;

    #@19
    invoke-interface {v0, p0, p1}, Lcom/android/internal/widget/multiwaveview/GlowPadView$OnTriggerListener;->onGrabbedStateChange(Landroid/view/View;I)V

    #@1c
    .line 988
    :cond_1c
    return-void

    #@1d
    .line 983
    :cond_1d
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mOnTriggerListener:Lcom/android/internal/widget/multiwaveview/GlowPadView$OnTriggerListener;

    #@1f
    invoke-interface {v0, p0, v1}, Lcom/android/internal/widget/multiwaveview/GlowPadView$OnTriggerListener;->onGrabbed(Landroid/view/View;I)V

    #@22
    goto :goto_17
.end method

.method private showGlow(IIFLandroid/animation/Animator$AnimatorListener;)V
    .registers 12
    .parameter "duration"
    .parameter "delay"
    .parameter "finalAlpha"
    .parameter "finishListener"

    #@0
    .prologue
    .line 407
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mGlowAnimations:Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;->cancel()V

    #@5
    .line 408
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mGlowAnimations:Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;

    #@7
    iget-object v1, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mPointCloud:Lcom/android/internal/widget/multiwaveview/PointCloud;

    #@9
    iget-object v1, v1, Lcom/android/internal/widget/multiwaveview/PointCloud;->glowManager:Lcom/android/internal/widget/multiwaveview/PointCloud$GlowManager;

    #@b
    int-to-long v2, p1

    #@c
    const/16 v4, 0xa

    #@e
    new-array v4, v4, [Ljava/lang/Object;

    #@10
    const/4 v5, 0x0

    #@11
    const-string v6, "ease"

    #@13
    aput-object v6, v4, v5

    #@15
    const/4 v5, 0x1

    #@16
    sget-object v6, Lcom/android/internal/widget/multiwaveview/Ease$Cubic;->easeIn:Landroid/animation/TimeInterpolator;

    #@18
    aput-object v6, v4, v5

    #@1a
    const/4 v5, 0x2

    #@1b
    const-string v6, "delay"

    #@1d
    aput-object v6, v4, v5

    #@1f
    const/4 v5, 0x3

    #@20
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@23
    move-result-object v6

    #@24
    aput-object v6, v4, v5

    #@26
    const/4 v5, 0x4

    #@27
    const-string v6, "alpha"

    #@29
    aput-object v6, v4, v5

    #@2b
    const/4 v5, 0x5

    #@2c
    invoke-static {p3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@2f
    move-result-object v6

    #@30
    aput-object v6, v4, v5

    #@32
    const/4 v5, 0x6

    #@33
    const-string v6, "onUpdate"

    #@35
    aput-object v6, v4, v5

    #@37
    const/4 v5, 0x7

    #@38
    iget-object v6, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mUpdateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    #@3a
    aput-object v6, v4, v5

    #@3c
    const/16 v5, 0x8

    #@3e
    const-string v6, "onComplete"

    #@40
    aput-object v6, v4, v5

    #@42
    const/16 v5, 0x9

    #@44
    aput-object p4, v4, v5

    #@46
    invoke-static {v1, v2, v3, v4}, Lcom/android/internal/widget/multiwaveview/Tweener;->to(Ljava/lang/Object;J[Ljava/lang/Object;)Lcom/android/internal/widget/multiwaveview/Tweener;

    #@49
    move-result-object v1

    #@4a
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;->add(Ljava/lang/Object;)Z

    #@4d
    .line 414
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mGlowAnimations:Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;

    #@4f
    invoke-virtual {v0}, Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;->start()V

    #@52
    .line 415
    return-void
.end method

.method private showTargets(Z)V
    .registers 15
    .parameter "animate"

    #@0
    .prologue
    .line 536
    iget-object v6, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetAnimations:Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;

    #@2
    invoke-virtual {v6}, Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;->stop()V

    #@5
    .line 537
    iput-boolean p1, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mAnimatingTargets:Z

    #@7
    .line 538
    if-eqz p1, :cond_84

    #@9
    const/16 v0, 0x32

    #@b
    .line 539
    .local v0, delay:I
    :goto_b
    if-eqz p1, :cond_86

    #@d
    const/16 v1, 0xc8

    #@f
    .line 540
    .local v1, duration:I
    :goto_f
    iget-object v6, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetDrawables:Ljava/util/ArrayList;

    #@11
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@14
    move-result v3

    #@15
    .line 541
    .local v3, length:I
    const/4 v2, 0x0

    #@16
    .local v2, i:I
    :goto_16
    if-ge v2, v3, :cond_88

    #@18
    .line 542
    iget-object v6, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetDrawables:Ljava/util/ArrayList;

    #@1a
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1d
    move-result-object v5

    #@1e
    check-cast v5, Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@20
    .line 543
    .local v5, target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    sget-object v6, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->STATE_INACTIVE:[I

    #@22
    invoke-virtual {v5, v6}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->setState([I)V

    #@25
    .line 544
    iget-object v6, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetAnimations:Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;

    #@27
    int-to-long v7, v1

    #@28
    const/16 v9, 0xc

    #@2a
    new-array v9, v9, [Ljava/lang/Object;

    #@2c
    const/4 v10, 0x0

    #@2d
    const-string v11, "ease"

    #@2f
    aput-object v11, v9, v10

    #@31
    const/4 v10, 0x1

    #@32
    sget-object v11, Lcom/android/internal/widget/multiwaveview/Ease$Cubic;->easeOut:Landroid/animation/TimeInterpolator;

    #@34
    aput-object v11, v9, v10

    #@36
    const/4 v10, 0x2

    #@37
    const-string v11, "alpha"

    #@39
    aput-object v11, v9, v10

    #@3b
    const/4 v10, 0x3

    #@3c
    const/high16 v11, 0x3f80

    #@3e
    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@41
    move-result-object v11

    #@42
    aput-object v11, v9, v10

    #@44
    const/4 v10, 0x4

    #@45
    const-string v11, "scaleX"

    #@47
    aput-object v11, v9, v10

    #@49
    const/4 v10, 0x5

    #@4a
    const/high16 v11, 0x3f80

    #@4c
    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@4f
    move-result-object v11

    #@50
    aput-object v11, v9, v10

    #@52
    const/4 v10, 0x6

    #@53
    const-string v11, "scaleY"

    #@55
    aput-object v11, v9, v10

    #@57
    const/4 v10, 0x7

    #@58
    const/high16 v11, 0x3f80

    #@5a
    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@5d
    move-result-object v11

    #@5e
    aput-object v11, v9, v10

    #@60
    const/16 v10, 0x8

    #@62
    const-string v11, "delay"

    #@64
    aput-object v11, v9, v10

    #@66
    const/16 v10, 0x9

    #@68
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@6b
    move-result-object v11

    #@6c
    aput-object v11, v9, v10

    #@6e
    const/16 v10, 0xa

    #@70
    const-string v11, "onUpdate"

    #@72
    aput-object v11, v9, v10

    #@74
    const/16 v10, 0xb

    #@76
    iget-object v11, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mUpdateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    #@78
    aput-object v11, v9, v10

    #@7a
    invoke-static {v5, v7, v8, v9}, Lcom/android/internal/widget/multiwaveview/Tweener;->to(Ljava/lang/Object;J[Ljava/lang/Object;)Lcom/android/internal/widget/multiwaveview/Tweener;

    #@7d
    move-result-object v7

    #@7e
    invoke-virtual {v6, v7}, Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;->add(Ljava/lang/Object;)Z

    #@81
    .line 541
    add-int/lit8 v2, v2, 0x1

    #@83
    goto :goto_16

    #@84
    .line 538
    .end local v0           #delay:I
    .end local v1           #duration:I
    .end local v2           #i:I
    .end local v3           #length:I
    .end local v5           #target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    :cond_84
    const/4 v0, 0x0

    #@85
    goto :goto_b

    #@86
    .line 539
    .restart local v0       #delay:I
    :cond_86
    const/4 v1, 0x0

    #@87
    goto :goto_f

    #@88
    .line 553
    .restart local v1       #duration:I
    .restart local v2       #i:I
    .restart local v3       #length:I
    :cond_88
    iget v6, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mRingScaleFactor:F

    #@8a
    const/high16 v7, 0x3f80

    #@8c
    mul-float v4, v6, v7

    #@8e
    .line 554
    .local v4, ringScale:F
    iget-object v6, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetAnimations:Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;

    #@90
    iget-object v7, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mOuterRing:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@92
    int-to-long v8, v1

    #@93
    const/16 v10, 0xe

    #@95
    new-array v10, v10, [Ljava/lang/Object;

    #@97
    const/4 v11, 0x0

    #@98
    const-string v12, "ease"

    #@9a
    aput-object v12, v10, v11

    #@9c
    const/4 v11, 0x1

    #@9d
    sget-object v12, Lcom/android/internal/widget/multiwaveview/Ease$Cubic;->easeOut:Landroid/animation/TimeInterpolator;

    #@9f
    aput-object v12, v10, v11

    #@a1
    const/4 v11, 0x2

    #@a2
    const-string v12, "alpha"

    #@a4
    aput-object v12, v10, v11

    #@a6
    const/4 v11, 0x3

    #@a7
    const/high16 v12, 0x3f80

    #@a9
    invoke-static {v12}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@ac
    move-result-object v12

    #@ad
    aput-object v12, v10, v11

    #@af
    const/4 v11, 0x4

    #@b0
    const-string v12, "scaleX"

    #@b2
    aput-object v12, v10, v11

    #@b4
    const/4 v11, 0x5

    #@b5
    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@b8
    move-result-object v12

    #@b9
    aput-object v12, v10, v11

    #@bb
    const/4 v11, 0x6

    #@bc
    const-string v12, "scaleY"

    #@be
    aput-object v12, v10, v11

    #@c0
    const/4 v11, 0x7

    #@c1
    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@c4
    move-result-object v12

    #@c5
    aput-object v12, v10, v11

    #@c7
    const/16 v11, 0x8

    #@c9
    const-string v12, "delay"

    #@cb
    aput-object v12, v10, v11

    #@cd
    const/16 v11, 0x9

    #@cf
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@d2
    move-result-object v12

    #@d3
    aput-object v12, v10, v11

    #@d5
    const/16 v11, 0xa

    #@d7
    const-string v12, "onUpdate"

    #@d9
    aput-object v12, v10, v11

    #@db
    const/16 v11, 0xb

    #@dd
    iget-object v12, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mUpdateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    #@df
    aput-object v12, v10, v11

    #@e1
    const/16 v11, 0xc

    #@e3
    const-string v12, "onComplete"

    #@e5
    aput-object v12, v10, v11

    #@e7
    const/16 v11, 0xd

    #@e9
    iget-object v12, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetUpdateListener:Landroid/animation/Animator$AnimatorListener;

    #@eb
    aput-object v12, v10, v11

    #@ed
    invoke-static {v7, v8, v9, v10}, Lcom/android/internal/widget/multiwaveview/Tweener;->to(Ljava/lang/Object;J[Ljava/lang/Object;)Lcom/android/internal/widget/multiwaveview/Tweener;

    #@f0
    move-result-object v7

    #@f1
    invoke-virtual {v6, v7}, Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;->add(Ljava/lang/Object;)Z

    #@f4
    .line 563
    iget-object v6, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetAnimations:Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;

    #@f6
    invoke-virtual {v6}, Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;->start()V

    #@f9
    .line 564
    return-void
.end method

.method private square(F)F
    .registers 3
    .parameter "d"

    #@0
    .prologue
    .line 1221
    mul-float v0, p1, p1

    #@2
    return v0
.end method

.method private startBackgroundAnimation(IF)V
    .registers 9
    .parameter "duration"
    .parameter "alpha"

    #@0
    .prologue
    .line 749
    invoke-virtual {p0}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->getBackground()Landroid/graphics/drawable/Drawable;

    #@3
    move-result-object v0

    #@4
    .line 750
    .local v0, background:Landroid/graphics/drawable/Drawable;
    iget-boolean v1, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mAlwaysTrackFinger:Z

    #@6
    if-eqz v1, :cond_4e

    #@8
    if-eqz v0, :cond_4e

    #@a
    .line 751
    iget-object v1, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mBackgroundAnimator:Lcom/android/internal/widget/multiwaveview/Tweener;

    #@c
    if-eqz v1, :cond_15

    #@e
    .line 752
    iget-object v1, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mBackgroundAnimator:Lcom/android/internal/widget/multiwaveview/Tweener;

    #@10
    iget-object v1, v1, Lcom/android/internal/widget/multiwaveview/Tweener;->animator:Landroid/animation/ObjectAnimator;

    #@12
    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->cancel()V

    #@15
    .line 754
    :cond_15
    int-to-long v1, p1

    #@16
    const/4 v3, 0x6

    #@17
    new-array v3, v3, [Ljava/lang/Object;

    #@19
    const/4 v4, 0x0

    #@1a
    const-string v5, "ease"

    #@1c
    aput-object v5, v3, v4

    #@1e
    const/4 v4, 0x1

    #@1f
    sget-object v5, Lcom/android/internal/widget/multiwaveview/Ease$Cubic;->easeIn:Landroid/animation/TimeInterpolator;

    #@21
    aput-object v5, v3, v4

    #@23
    const/4 v4, 0x2

    #@24
    const-string v5, "alpha"

    #@26
    aput-object v5, v3, v4

    #@28
    const/4 v4, 0x3

    #@29
    const/high16 v5, 0x437f

    #@2b
    mul-float/2addr v5, p2

    #@2c
    float-to-int v5, v5

    #@2d
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@30
    move-result-object v5

    #@31
    aput-object v5, v3, v4

    #@33
    const/4 v4, 0x4

    #@34
    const-string v5, "delay"

    #@36
    aput-object v5, v3, v4

    #@38
    const/4 v4, 0x5

    #@39
    const/16 v5, 0x32

    #@3b
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3e
    move-result-object v5

    #@3f
    aput-object v5, v3, v4

    #@41
    invoke-static {v0, v1, v2, v3}, Lcom/android/internal/widget/multiwaveview/Tweener;->to(Ljava/lang/Object;J[Ljava/lang/Object;)Lcom/android/internal/widget/multiwaveview/Tweener;

    #@44
    move-result-object v1

    #@45
    iput-object v1, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mBackgroundAnimator:Lcom/android/internal/widget/multiwaveview/Tweener;

    #@47
    .line 758
    iget-object v1, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mBackgroundAnimator:Lcom/android/internal/widget/multiwaveview/Tweener;

    #@49
    iget-object v1, v1, Lcom/android/internal/widget/multiwaveview/Tweener;->animator:Landroid/animation/ObjectAnimator;

    #@4b
    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    #@4e
    .line 760
    :cond_4e
    return-void
.end method

.method private startWaveAnimation()V
    .registers 10

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    const/high16 v7, 0x4000

    #@3
    .line 714
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mWaveAnimations:Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;

    #@5
    invoke-virtual {v0}, Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;->cancel()V

    #@8
    .line 715
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mPointCloud:Lcom/android/internal/widget/multiwaveview/PointCloud;

    #@a
    iget-object v0, v0, Lcom/android/internal/widget/multiwaveview/PointCloud;->waveManager:Lcom/android/internal/widget/multiwaveview/PointCloud$WaveManager;

    #@c
    const/high16 v1, 0x3f80

    #@e
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/multiwaveview/PointCloud$WaveManager;->setAlpha(F)V

    #@11
    .line 716
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mPointCloud:Lcom/android/internal/widget/multiwaveview/PointCloud;

    #@13
    iget-object v0, v0, Lcom/android/internal/widget/multiwaveview/PointCloud;->waveManager:Lcom/android/internal/widget/multiwaveview/PointCloud$WaveManager;

    #@15
    iget-object v1, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mHandleDrawable:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@17
    invoke-virtual {v1}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->getWidth()I

    #@1a
    move-result v1

    #@1b
    int-to-float v1, v1

    #@1c
    div-float/2addr v1, v7

    #@1d
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/multiwaveview/PointCloud$WaveManager;->setRadius(F)V

    #@20
    .line 717
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mWaveAnimations:Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;

    #@22
    iget-object v1, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mPointCloud:Lcom/android/internal/widget/multiwaveview/PointCloud;

    #@24
    iget-object v1, v1, Lcom/android/internal/widget/multiwaveview/PointCloud;->waveManager:Lcom/android/internal/widget/multiwaveview/PointCloud$WaveManager;

    #@26
    const-wide/16 v2, 0x546

    #@28
    const/16 v4, 0xa

    #@2a
    new-array v4, v4, [Ljava/lang/Object;

    #@2c
    const-string v5, "ease"

    #@2e
    aput-object v5, v4, v8

    #@30
    const/4 v5, 0x1

    #@31
    sget-object v6, Lcom/android/internal/widget/multiwaveview/Ease$Quad;->easeOut:Landroid/animation/TimeInterpolator;

    #@33
    aput-object v6, v4, v5

    #@35
    const/4 v5, 0x2

    #@36
    const-string v6, "delay"

    #@38
    aput-object v6, v4, v5

    #@3a
    const/4 v5, 0x3

    #@3b
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3e
    move-result-object v6

    #@3f
    aput-object v6, v4, v5

    #@41
    const/4 v5, 0x4

    #@42
    const-string v6, "radius"

    #@44
    aput-object v6, v4, v5

    #@46
    const/4 v5, 0x5

    #@47
    iget v6, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mOuterRadius:F

    #@49
    mul-float/2addr v6, v7

    #@4a
    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@4d
    move-result-object v6

    #@4e
    aput-object v6, v4, v5

    #@50
    const/4 v5, 0x6

    #@51
    const-string v6, "onUpdate"

    #@53
    aput-object v6, v4, v5

    #@55
    const/4 v5, 0x7

    #@56
    iget-object v6, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mUpdateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    #@58
    aput-object v6, v4, v5

    #@5a
    const/16 v5, 0x8

    #@5c
    const-string v6, "onComplete"

    #@5e
    aput-object v6, v4, v5

    #@60
    const/16 v5, 0x9

    #@62
    new-instance v6, Lcom/android/internal/widget/multiwaveview/GlowPadView$5;

    #@64
    invoke-direct {v6, p0}, Lcom/android/internal/widget/multiwaveview/GlowPadView$5;-><init>(Lcom/android/internal/widget/multiwaveview/GlowPadView;)V

    #@67
    aput-object v6, v4, v5

    #@69
    invoke-static {v1, v2, v3, v4}, Lcom/android/internal/widget/multiwaveview/Tweener;->to(Ljava/lang/Object;J[Ljava/lang/Object;)Lcom/android/internal/widget/multiwaveview/Tweener;

    #@6c
    move-result-object v1

    #@6d
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;->add(Ljava/lang/Object;)Z

    #@70
    .line 729
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mWaveAnimations:Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;

    #@72
    invoke-virtual {v0}, Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;->start()V

    #@75
    .line 730
    return-void
.end method

.method private stopAndHideWaveAnimation()V
    .registers 3

    #@0
    .prologue
    .line 709
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mWaveAnimations:Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;->cancel()V

    #@5
    .line 710
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mPointCloud:Lcom/android/internal/widget/multiwaveview/PointCloud;

    #@7
    iget-object v0, v0, Lcom/android/internal/widget/multiwaveview/PointCloud;->waveManager:Lcom/android/internal/widget/multiwaveview/PointCloud$WaveManager;

    #@9
    const/4 v1, 0x0

    #@a
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/multiwaveview/PointCloud$WaveManager;->setAlpha(F)V

    #@d
    .line 711
    return-void
.end method

.method private switchToState(IFF)V
    .registers 10
    .parameter "state"
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    const/high16 v3, 0x3f80

    #@4
    const/4 v2, 0x0

    #@5
    const/4 v1, 0x0

    #@6
    .line 364
    packed-switch p1, :pswitch_data_5e

    #@9
    .line 403
    :cond_9
    :goto_9
    return-void

    #@a
    .line 366
    :pswitch_a
    invoke-direct {p0}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->deactivateTargets()V

    #@d
    .line 367
    invoke-direct {p0, v1, v1, v2, v4}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->hideGlow(IIFLandroid/animation/Animator$AnimatorListener;)V

    #@10
    .line 368
    invoke-direct {p0, v1, v2}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->startBackgroundAnimation(IF)V

    #@13
    .line 369
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mHandleDrawable:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@15
    sget-object v1, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->STATE_INACTIVE:[I

    #@17
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->setState([I)V

    #@1a
    .line 370
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mHandleDrawable:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@1c
    invoke-virtual {v0, v3}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->setAlpha(F)V

    #@1f
    goto :goto_9

    #@20
    .line 374
    :pswitch_20
    invoke-direct {p0, v1, v2}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->startBackgroundAnimation(IF)V

    #@23
    goto :goto_9

    #@24
    .line 378
    :pswitch_24
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mHandleDrawable:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@26
    invoke-virtual {v0, v2}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->setAlpha(F)V

    #@29
    .line 379
    invoke-direct {p0}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->deactivateTargets()V

    #@2c
    .line 380
    invoke-direct {p0, v5}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->showTargets(Z)V

    #@2f
    .line 381
    const/16 v0, 0xc8

    #@31
    invoke-direct {p0, v0, v3}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->startBackgroundAnimation(IF)V

    #@34
    .line 382
    invoke-direct {p0, v5}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->setGrabbedState(I)V

    #@37
    .line 383
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mContext:Landroid/content/Context;

    #@39
    invoke-static {v0}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    #@3c
    move-result-object v0

    #@3d
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    #@40
    move-result v0

    #@41
    if-eqz v0, :cond_9

    #@43
    .line 384
    invoke-direct {p0}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->announceTargets()V

    #@46
    goto :goto_9

    #@47
    .line 389
    :pswitch_47
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mHandleDrawable:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@49
    invoke-virtual {v0, v2}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->setAlpha(F)V

    #@4c
    .line 390
    invoke-direct {p0, v1, v1, v3, v4}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->showGlow(IIFLandroid/animation/Animator$AnimatorListener;)V

    #@4f
    goto :goto_9

    #@50
    .line 395
    :pswitch_50
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mHandleDrawable:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@52
    invoke-virtual {v0, v2}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->setAlpha(F)V

    #@55
    .line 396
    invoke-direct {p0, v1, v1, v2, v4}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->showGlow(IIFLandroid/animation/Animator$AnimatorListener;)V

    #@58
    goto :goto_9

    #@59
    .line 400
    :pswitch_59
    invoke-direct {p0}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->doFinish()V

    #@5c
    goto :goto_9

    #@5d
    .line 364
    nop

    #@5e
    :pswitch_data_5e
    .packed-switch 0x0
        :pswitch_a
        :pswitch_20
        :pswitch_24
        :pswitch_47
        :pswitch_50
        :pswitch_59
    .end packed-switch
.end method

.method private trySwitchToFirstTouchState(FF)Z
    .registers 8
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 991
    iget v3, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mWaveCenterX:F

    #@3
    sub-float v0, p1, v3

    #@5
    .line 992
    .local v0, tx:F
    iget v3, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mWaveCenterY:F

    #@7
    sub-float v1, p2, v3

    #@9
    .line 993
    .local v1, ty:F
    iget-boolean v3, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mAlwaysTrackFinger:Z

    #@b
    if-nez v3, :cond_19

    #@d
    invoke-direct {p0, v0, v1}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->dist2(FF)F

    #@10
    move-result v3

    #@11
    invoke-direct {p0}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->getScaledGlowRadiusSquared()F

    #@14
    move-result v4

    #@15
    cmpg-float v3, v3, v4

    #@17
    if-gtz v3, :cond_23

    #@19
    .line 995
    :cond_19
    const/4 v3, 0x2

    #@1a
    invoke-direct {p0, v3, p1, p2}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->switchToState(IFF)V

    #@1d
    .line 996
    invoke-direct {p0, v0, v1}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->updateGlowPosition(FF)V

    #@20
    .line 997
    iput-boolean v2, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mDragging:Z

    #@22
    .line 1000
    :goto_22
    return v2

    #@23
    :cond_23
    const/4 v2, 0x0

    #@24
    goto :goto_22
.end method

.method private updateGlowPosition(FF)V
    .registers 7
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    const/high16 v3, 0x3f80

    #@2
    .line 802
    iget-object v2, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mOuterRing:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@4
    invoke-virtual {v2}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->getX()F

    #@7
    move-result v2

    #@8
    sub-float v0, p1, v2

    #@a
    .line 803
    .local v0, dx:F
    iget-object v2, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mOuterRing:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@c
    invoke-virtual {v2}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->getY()F

    #@f
    move-result v2

    #@10
    sub-float v1, p2, v2

    #@12
    .line 804
    .local v1, dy:F
    iget v2, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mRingScaleFactor:F

    #@14
    div-float v2, v3, v2

    #@16
    mul-float/2addr v0, v2

    #@17
    .line 805
    iget v2, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mRingScaleFactor:F

    #@19
    div-float v2, v3, v2

    #@1b
    mul-float/2addr v1, v2

    #@1c
    .line 806
    iget-object v2, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mPointCloud:Lcom/android/internal/widget/multiwaveview/PointCloud;

    #@1e
    iget-object v2, v2, Lcom/android/internal/widget/multiwaveview/PointCloud;->glowManager:Lcom/android/internal/widget/multiwaveview/PointCloud$GlowManager;

    #@20
    iget-object v3, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mOuterRing:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@22
    invoke-virtual {v3}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->getX()F

    #@25
    move-result v3

    #@26
    add-float/2addr v3, v0

    #@27
    invoke-virtual {v2, v3}, Lcom/android/internal/widget/multiwaveview/PointCloud$GlowManager;->setX(F)V

    #@2a
    .line 807
    iget-object v2, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mPointCloud:Lcom/android/internal/widget/multiwaveview/PointCloud;

    #@2c
    iget-object v2, v2, Lcom/android/internal/widget/multiwaveview/PointCloud;->glowManager:Lcom/android/internal/widget/multiwaveview/PointCloud$GlowManager;

    #@2e
    iget-object v3, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mOuterRing:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@30
    invoke-virtual {v3}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->getY()F

    #@33
    move-result v3

    #@34
    add-float/2addr v3, v1

    #@35
    invoke-virtual {v2, v3}, Lcom/android/internal/widget/multiwaveview/PointCloud$GlowManager;->setY(F)V

    #@38
    .line 808
    return-void
.end method

.method private updatePointCloudPosition(FF)V
    .registers 4
    .parameter "centerX"
    .parameter "centerY"

    #@0
    .prologue
    .line 1199
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mPointCloud:Lcom/android/internal/widget/multiwaveview/PointCloud;

    #@2
    invoke-virtual {v0, p1, p2}, Lcom/android/internal/widget/multiwaveview/PointCloud;->setCenter(FF)V

    #@5
    .line 1200
    return-void
.end method

.method private updateTargetPosition(IFF)V
    .registers 6
    .parameter "i"
    .parameter "centerX"
    .parameter "centerY"

    #@0
    .prologue
    .line 1158
    invoke-direct {p0}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->getSliceAngle()F

    #@3
    move-result v1

    #@4
    invoke-direct {p0, v1, p1}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->getAngle(FI)F

    #@7
    move-result v0

    #@8
    .line 1159
    .local v0, angle:F
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->updateTargetPosition(IFFF)V

    #@b
    .line 1160
    return-void
.end method

.method private updateTargetPosition(IFFF)V
    .registers 11
    .parameter "i"
    .parameter "centerX"
    .parameter "centerY"
    .parameter "angle"

    #@0
    .prologue
    const/high16 v5, 0x4000

    #@2
    .line 1163
    invoke-direct {p0}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->getRingWidth()F

    #@5
    move-result v4

    #@6
    div-float v0, v4, v5

    #@8
    .line 1164
    .local v0, placementRadiusX:F
    invoke-direct {p0}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->getRingHeight()F

    #@b
    move-result v4

    #@c
    div-float v1, v4, v5

    #@e
    .line 1165
    .local v1, placementRadiusY:F
    if-ltz p1, :cond_32

    #@10
    .line 1166
    iget-object v3, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetDrawables:Ljava/util/ArrayList;

    #@12
    .line 1167
    .local v3, targets:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/widget/multiwaveview/TargetDrawable;>;"
    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@15
    move-result-object v2

    #@16
    check-cast v2, Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@18
    .line 1168
    .local v2, targetIcon:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    invoke-virtual {v2, p2}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->setPositionX(F)V

    #@1b
    .line 1169
    invoke-virtual {v2, p3}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->setPositionY(F)V

    #@1e
    .line 1170
    float-to-double v4, p4

    #@1f
    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    #@22
    move-result-wide v4

    #@23
    double-to-float v4, v4

    #@24
    mul-float/2addr v4, v0

    #@25
    invoke-virtual {v2, v4}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->setX(F)V

    #@28
    .line 1171
    float-to-double v4, p4

    #@29
    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    #@2c
    move-result-wide v4

    #@2d
    double-to-float v4, v4

    #@2e
    mul-float/2addr v4, v1

    #@2f
    invoke-virtual {v2, v4}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->setY(F)V

    #@32
    .line 1173
    .end local v2           #targetIcon:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    .end local v3           #targets:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/widget/multiwaveview/TargetDrawable;>;"
    :cond_32
    return-void
.end method

.method private updateTargetPositions(FF)V
    .registers 4
    .parameter "centerX"
    .parameter "centerY"

    #@0
    .prologue
    .line 1176
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->updateTargetPositions(FFZ)V

    #@4
    .line 1177
    return-void
.end method

.method private updateTargetPositions(FFZ)V
    .registers 8
    .parameter "centerX"
    .parameter "centerY"
    .parameter "skipActive"

    #@0
    .prologue
    .line 1180
    iget-object v3, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetDrawables:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v2

    #@6
    .line 1181
    .local v2, size:I
    invoke-direct {p0}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->getSliceAngle()F

    #@9
    move-result v0

    #@a
    .line 1183
    .local v0, alpha:F
    const/4 v1, 0x0

    #@b
    .local v1, i:I
    :goto_b
    if-ge v1, v2, :cond_1d

    #@d
    .line 1184
    if-eqz p3, :cond_13

    #@f
    iget v3, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mActiveTarget:I

    #@11
    if-eq v1, v3, :cond_1a

    #@13
    .line 1185
    :cond_13
    invoke-direct {p0, v0, v1}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->getAngle(FI)F

    #@16
    move-result v3

    #@17
    invoke-direct {p0, v1, p1, p2, v3}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->updateTargetPosition(IFFF)V

    #@1a
    .line 1183
    :cond_1a
    add-int/lit8 v1, v1, 0x1

    #@1c
    goto :goto_b

    #@1d
    .line 1188
    :cond_1d
    return-void
.end method

.method private vibrate()V
    .registers 5

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 567
    iget-object v1, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mContext:Landroid/content/Context;

    #@3
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@6
    move-result-object v1

    #@7
    const-string v2, "haptic_feedback_enabled"

    #@9
    const/4 v3, -0x2

    #@a
    invoke-static {v1, v2, v0, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_1f

    #@10
    .line 570
    .local v0, hapticEnabled:Z
    :goto_10
    iget-object v1, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mVibrator:Landroid/os/Vibrator;

    #@12
    if-eqz v1, :cond_1e

    #@14
    if-eqz v0, :cond_1e

    #@16
    .line 571
    iget-object v1, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mVibrator:Landroid/os/Vibrator;

    #@18
    iget v2, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mVibrationDuration:I

    #@1a
    int-to-long v2, v2

    #@1b
    invoke-virtual {v1, v2, v3}, Landroid/os/Vibrator;->vibrate(J)V

    #@1e
    .line 573
    :cond_1e
    return-void

    #@1f
    .line 567
    .end local v0           #hapticEnabled:Z
    :cond_1f
    const/4 v0, 0x0

    #@20
    goto :goto_10
.end method


# virtual methods
.method protected changeTargets(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/widget/multiwaveview/TargetDrawable;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .local p1, drawables:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/widget/multiwaveview/TargetDrawable;>;"
    .local p2, targetDescriptions:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local p3, directionDescriptions:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v2, -0x1

    #@1
    .line 1429
    if-eqz p1, :cond_57

    #@3
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v0

    #@7
    if-lez v0, :cond_57

    #@9
    if-eqz p2, :cond_57

    #@b
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    #@e
    move-result v0

    #@f
    if-lez v0, :cond_57

    #@11
    if-eqz p3, :cond_57

    #@13
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    #@16
    move-result v0

    #@17
    if-lez v0, :cond_57

    #@19
    .line 1432
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    #@1c
    move-result v0

    #@1d
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    #@20
    move-result v1

    #@21
    if-ne v0, v1, :cond_57

    #@23
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    #@26
    move-result v0

    #@27
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    #@2a
    move-result v1

    #@2b
    if-ne v0, v1, :cond_57

    #@2d
    .line 1433
    iput-object p1, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetDrawables:Ljava/util/ArrayList;

    #@2f
    .line 1434
    iput-object p2, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetDescriptions:Ljava/util/ArrayList;

    #@31
    .line 1435
    iput-object p3, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mDirectionDescriptions:Ljava/util/ArrayList;

    #@33
    .line 1437
    iput v2, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetResourceId:I

    #@35
    .line 1438
    iput v2, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetDescriptionsResourceId:I

    #@37
    .line 1439
    iput v2, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mDirectionDescriptionsResourceId:I

    #@39
    .line 1441
    const-string v0, "GlowPadView"

    #@3b
    new-instance v1, Ljava/lang/StringBuilder;

    #@3d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@40
    const-string v2, "changeTargets() succeeded. size: "

    #@42
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v1

    #@46
    iget-object v2, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetDrawables:Ljava/util/ArrayList;

    #@48
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@4b
    move-result v2

    #@4c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v1

    #@50
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@53
    move-result-object v1

    #@54
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@57
    .line 1444
    :cond_57
    return-void
.end method

.method protected finish()V
    .registers 2

    #@0
    .prologue
    .line 1405
    const/4 v0, -0x1

    #@1
    iput v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mActiveTarget:I

    #@3
    .line 1406
    invoke-direct {p0}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->doFinish()V

    #@6
    .line 1407
    return-void
.end method

.method public getDirectionDescriptionsResourceId()I
    .registers 2

    #@0
    .prologue
    .line 669
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mDirectionDescriptionsResourceId:I

    #@2
    return v0
.end method

.method public getResourceIdForTarget(I)I
    .registers 4
    .parameter "index"

    #@0
    .prologue
    .line 1310
    iget-object v1, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetDrawables:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@8
    .line 1311
    .local v0, drawable:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    if-nez v0, :cond_c

    #@a
    const/4 v1, 0x0

    #@b
    :goto_b
    return v1

    #@c
    :cond_c
    invoke-virtual {v0}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->getResourceId()I

    #@f
    move-result v1

    #@10
    goto :goto_b
.end method

.method protected getScaledSuggestedMinimumHeight()I
    .registers 5

    #@0
    .prologue
    .line 341
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mRingScaleFactor:F

    #@2
    iget-object v1, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mOuterRing:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@4
    invoke-virtual {v1}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->getHeight()I

    #@7
    move-result v1

    #@8
    int-to-float v1, v1

    #@9
    const/high16 v2, 0x4000

    #@b
    iget v3, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mOuterRadius:F

    #@d
    mul-float/2addr v2, v3

    #@e
    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    #@11
    move-result v1

    #@12
    mul-float/2addr v0, v1

    #@13
    iget v1, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mMaxTargetHeight:I

    #@15
    int-to-float v1, v1

    #@16
    add-float/2addr v0, v1

    #@17
    float-to-int v0, v0

    #@18
    return v0
.end method

.method protected getScaledSuggestedMinimumWidth()I
    .registers 5

    #@0
    .prologue
    .line 333
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mRingScaleFactor:F

    #@2
    iget-object v1, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mOuterRing:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@4
    invoke-virtual {v1}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->getWidth()I

    #@7
    move-result v1

    #@8
    int-to-float v1, v1

    #@9
    const/high16 v2, 0x4000

    #@b
    iget v3, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mOuterRadius:F

    #@d
    mul-float/2addr v2, v3

    #@e
    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    #@11
    move-result v1

    #@12
    mul-float/2addr v0, v1

    #@13
    iget v1, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mMaxTargetWidth:I

    #@15
    int-to-float v1, v1

    #@16
    add-float/2addr v0, v1

    #@17
    float-to-int v0, v0

    #@18
    return v0
.end method

.method protected getSuggestedMinimumHeight()I
    .registers 4

    #@0
    .prologue
    .line 326
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mOuterRing:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->getHeight()I

    #@5
    move-result v0

    #@6
    int-to-float v0, v0

    #@7
    const/high16 v1, 0x4000

    #@9
    iget v2, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mOuterRadius:F

    #@b
    mul-float/2addr v1, v2

    #@c
    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    #@f
    move-result v0

    #@10
    iget v1, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mMaxTargetHeight:I

    #@12
    int-to-float v1, v1

    #@13
    add-float/2addr v0, v1

    #@14
    float-to-int v0, v0

    #@15
    return v0
.end method

.method protected getSuggestedMinimumWidth()I
    .registers 4

    #@0
    .prologue
    .line 319
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mOuterRing:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->getWidth()I

    #@5
    move-result v0

    #@6
    int-to-float v0, v0

    #@7
    const/high16 v1, 0x4000

    #@9
    iget v2, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mOuterRadius:F

    #@b
    mul-float/2addr v1, v2

    #@c
    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    #@f
    move-result v0

    #@10
    iget v1, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mMaxTargetWidth:I

    #@12
    int-to-float v1, v1

    #@13
    add-float/2addr v0, v1

    #@14
    float-to-int v0, v0

    #@15
    return v0
.end method

.method public getTargetDescriptionsResourceId()I
    .registers 2

    #@0
    .prologue
    .line 648
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetDescriptionsResourceId:I

    #@2
    return v0
.end method

.method public getTargetPosition(I)I
    .registers 5
    .parameter "resourceId"

    #@0
    .prologue
    .line 1330
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    iget-object v2, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetDrawables:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v2

    #@7
    if-ge v0, v2, :cond_1b

    #@9
    .line 1331
    iget-object v2, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetDrawables:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v1

    #@f
    check-cast v1, Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@11
    .line 1332
    .local v1, target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    invoke-virtual {v1}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->getResourceId()I

    #@14
    move-result v2

    #@15
    if-ne v2, p1, :cond_18

    #@17
    .line 1336
    .end local v0           #i:I
    .end local v1           #target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    :goto_17
    return v0

    #@18
    .line 1330
    .restart local v0       #i:I
    .restart local v1       #target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    :cond_18
    add-int/lit8 v0, v0, 0x1

    #@1a
    goto :goto_1

    #@1b
    .line 1336
    .end local v1           #target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    :cond_1b
    const/4 v0, -0x1

    #@1c
    goto :goto_17
.end method

.method public getTargetResourceId()I
    .registers 2

    #@0
    .prologue
    .line 627
    iget v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetResourceId:I

    #@2
    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 6
    .parameter "canvas"

    #@0
    .prologue
    .line 1204
    iget-object v3, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mPointCloud:Lcom/android/internal/widget/multiwaveview/PointCloud;

    #@2
    invoke-virtual {v3, p1}, Lcom/android/internal/widget/multiwaveview/PointCloud;->draw(Landroid/graphics/Canvas;)V

    #@5
    .line 1205
    iget-object v3, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mOuterRing:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@7
    invoke-virtual {v3, p1}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->draw(Landroid/graphics/Canvas;)V

    #@a
    .line 1206
    iget-object v3, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetDrawables:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@f
    move-result v1

    #@10
    .line 1207
    .local v1, ntargets:I
    const/4 v0, 0x0

    #@11
    .local v0, i:I
    :goto_11
    if-ge v0, v1, :cond_23

    #@13
    .line 1208
    iget-object v3, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetDrawables:Ljava/util/ArrayList;

    #@15
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@18
    move-result-object v2

    #@19
    check-cast v2, Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@1b
    .line 1209
    .local v2, target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    if-eqz v2, :cond_20

    #@1d
    .line 1210
    invoke-virtual {v2, p1}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->draw(Landroid/graphics/Canvas;)V

    #@20
    .line 1207
    :cond_20
    add-int/lit8 v0, v0, 0x1

    #@22
    goto :goto_11

    #@23
    .line 1213
    .end local v2           #target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    :cond_23
    iget-object v3, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mHandleDrawable:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@25
    invoke-virtual {v3, p1}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->draw(Landroid/graphics/Canvas;)V

    #@28
    .line 1214
    return-void
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .registers 5
    .parameter "event"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 949
    iget-object v1, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mContext:Landroid/content/Context;

    #@3
    invoke-static {v1}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    #@6
    move-result-object v1

    #@7
    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    #@a
    move-result v1

    #@b
    if-eqz v1, :cond_1a

    #@d
    .line 950
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@10
    move-result v0

    #@11
    .line 951
    .local v0, action:I
    packed-switch v0, :pswitch_data_2c

    #@14
    .line 962
    :goto_14
    :pswitch_14
    invoke-virtual {p0, p1}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    #@17
    .line 963
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->setAction(I)V

    #@1a
    .line 965
    .end local v0           #action:I
    :cond_1a
    invoke-super {p0, p1}, Landroid/view/View;->onHoverEvent(Landroid/view/MotionEvent;)Z

    #@1d
    .line 966
    return v2

    #@1e
    .line 953
    .restart local v0       #action:I
    :pswitch_1e
    const/4 v1, 0x0

    #@1f
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->setAction(I)V

    #@22
    goto :goto_14

    #@23
    .line 956
    :pswitch_23
    const/4 v1, 0x2

    #@24
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->setAction(I)V

    #@27
    goto :goto_14

    #@28
    .line 959
    :pswitch_28
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->setAction(I)V

    #@2b
    goto :goto_14

    #@2c
    .line 951
    :pswitch_data_2c
    .packed-switch 0x7
        :pswitch_23
        :pswitch_14
        :pswitch_1e
        :pswitch_28
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .registers 15
    .parameter "changed"
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    .line 1120
    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    #@3
    .line 1121
    sub-int v5, p4, p2

    #@5
    .line 1122
    .local v5, width:I
    sub-int v0, p5, p3

    #@7
    .line 1126
    .local v0, height:I
    invoke-direct {p0}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->getRingWidth()F

    #@a
    move-result v4

    #@b
    .line 1127
    .local v4, placementWidth:F
    invoke-direct {p0}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->getRingHeight()F

    #@e
    move-result v3

    #@f
    .line 1128
    .local v3, placementHeight:F
    iget v6, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mHorizontalInset:I

    #@11
    int-to-float v6, v6

    #@12
    int-to-float v7, v5

    #@13
    iget v8, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mMaxTargetWidth:I

    #@15
    int-to-float v8, v8

    #@16
    add-float/2addr v8, v4

    #@17
    invoke-static {v7, v8}, Ljava/lang/Math;->max(FF)F

    #@1a
    move-result v7

    #@1b
    const/high16 v8, 0x4000

    #@1d
    div-float/2addr v7, v8

    #@1e
    add-float v1, v6, v7

    #@20
    .line 1130
    .local v1, newWaveCenterX:F
    iget v6, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mVerticalInset:I

    #@22
    int-to-float v6, v6

    #@23
    int-to-float v7, v0

    #@24
    iget v8, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mMaxTargetHeight:I

    #@26
    int-to-float v8, v8

    #@27
    add-float/2addr v8, v3

    #@28
    invoke-static {v7, v8}, Ljava/lang/Math;->max(FF)F

    #@2b
    move-result v7

    #@2c
    const/high16 v8, 0x4000

    #@2e
    div-float/2addr v7, v8

    #@2f
    add-float v2, v6, v7

    #@31
    .line 1133
    .local v2, newWaveCenterY:F
    iget-boolean v6, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mInitialLayout:Z

    #@33
    if-eqz v6, :cond_40

    #@35
    .line 1134
    invoke-direct {p0}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->stopAndHideWaveAnimation()V

    #@38
    .line 1135
    const/4 v6, 0x0

    #@39
    const/4 v7, 0x0

    #@3a
    invoke-direct {p0, v6, v7}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->hideTargets(ZZ)V

    #@3d
    .line 1136
    const/4 v6, 0x0

    #@3e
    iput-boolean v6, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mInitialLayout:Z

    #@40
    .line 1139
    :cond_40
    iget-object v6, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mOuterRing:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@42
    invoke-virtual {v6, v1}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->setPositionX(F)V

    #@45
    .line 1140
    iget-object v6, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mOuterRing:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@47
    invoke-virtual {v6, v2}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->setPositionY(F)V

    #@4a
    .line 1142
    iget-object v6, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mPointCloud:Lcom/android/internal/widget/multiwaveview/PointCloud;

    #@4c
    iget v7, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mRingScaleFactor:F

    #@4e
    invoke-virtual {v6, v7}, Lcom/android/internal/widget/multiwaveview/PointCloud;->setScale(F)V

    #@51
    .line 1144
    iget-object v6, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mHandleDrawable:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@53
    invoke-virtual {v6, v1}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->setPositionX(F)V

    #@56
    .line 1145
    iget-object v6, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mHandleDrawable:Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@58
    invoke-virtual {v6, v2}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->setPositionY(F)V

    #@5b
    .line 1147
    invoke-direct {p0, v1, v2}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->updateTargetPositions(FF)V

    #@5e
    .line 1148
    invoke-direct {p0, v1, v2}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->updatePointCloudPosition(FF)V

    #@61
    .line 1149
    invoke-direct {p0, v1, v2}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->updateGlowPosition(FF)V

    #@64
    .line 1151
    iput v1, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mWaveCenterX:F

    #@66
    .line 1152
    iput v2, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mWaveCenterY:F

    #@68
    .line 1155
    return-void
.end method

.method protected onMeasure(II)V
    .registers 11
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    .line 1095
    invoke-virtual {p0}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->getSuggestedMinimumWidth()I

    #@3
    move-result v3

    #@4
    .line 1096
    .local v3, minimumWidth:I
    invoke-virtual {p0}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->getSuggestedMinimumHeight()I

    #@7
    move-result v2

    #@8
    .line 1097
    .local v2, minimumHeight:I
    invoke-direct {p0, p1, v3}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->resolveMeasured(II)I

    #@b
    move-result v1

    #@c
    .line 1098
    .local v1, computedWidth:I
    invoke-direct {p0, p2, v2}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->resolveMeasured(II)I

    #@f
    move-result v0

    #@10
    .line 1100
    .local v0, computedHeight:I
    invoke-direct {p0, v3, v2, v1, v0}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->computeScaleFactor(IIII)F

    #@13
    move-result v6

    #@14
    iput v6, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mRingScaleFactor:F

    #@16
    .line 1103
    invoke-virtual {p0}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->getScaledSuggestedMinimumWidth()I

    #@19
    move-result v5

    #@1a
    .line 1104
    .local v5, scaledWidth:I
    invoke-virtual {p0}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->getScaledSuggestedMinimumHeight()I

    #@1d
    move-result v4

    #@1e
    .line 1106
    .local v4, scaledHeight:I
    sub-int v6, v1, v5

    #@20
    sub-int v7, v0, v4

    #@22
    invoke-direct {p0, v6, v7}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->computeInsets(II)V

    #@25
    .line 1107
    invoke-virtual {p0, v1, v0}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->setMeasuredDimension(II)V

    #@28
    .line 1108
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 5
    .parameter "event"

    #@0
    .prologue
    .line 764
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    #@3
    move-result v0

    #@4
    .line 765
    .local v0, action:I
    const/4 v1, 0x0

    #@5
    .line 766
    .local v1, handled:Z
    packed-switch v0, :pswitch_data_32

    #@8
    .line 797
    :goto_8
    :pswitch_8
    invoke-virtual {p0}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->invalidate()V

    #@b
    .line 798
    if-eqz v1, :cond_2c

    #@d
    const/4 v2, 0x1

    #@e
    :goto_e
    return v2

    #@f
    .line 770
    :pswitch_f
    invoke-direct {p0, p1}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->handleDown(Landroid/view/MotionEvent;)V

    #@12
    .line 771
    invoke-direct {p0, p1}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->handleMove(Landroid/view/MotionEvent;)V

    #@15
    .line 772
    const/4 v1, 0x1

    #@16
    .line 773
    goto :goto_8

    #@17
    .line 777
    :pswitch_17
    invoke-direct {p0, p1}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->handleMove(Landroid/view/MotionEvent;)V

    #@1a
    .line 778
    const/4 v1, 0x1

    #@1b
    .line 779
    goto :goto_8

    #@1c
    .line 784
    :pswitch_1c
    invoke-direct {p0, p1}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->handleMove(Landroid/view/MotionEvent;)V

    #@1f
    .line 785
    invoke-direct {p0, p1}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->handleUp(Landroid/view/MotionEvent;)V

    #@22
    .line 786
    const/4 v1, 0x1

    #@23
    .line 787
    goto :goto_8

    #@24
    .line 791
    :pswitch_24
    invoke-direct {p0, p1}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->handleMove(Landroid/view/MotionEvent;)V

    #@27
    .line 792
    invoke-direct {p0, p1}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->handleCancel(Landroid/view/MotionEvent;)V

    #@2a
    .line 793
    const/4 v1, 0x1

    #@2b
    goto :goto_8

    #@2c
    .line 798
    :cond_2c
    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    #@2f
    move-result v2

    #@30
    goto :goto_e

    #@31
    .line 766
    nop

    #@32
    :pswitch_data_32
    .packed-switch 0x0
        :pswitch_f
        :pswitch_1c
        :pswitch_17
        :pswitch_24
        :pswitch_8
        :pswitch_f
        :pswitch_1c
    .end packed-switch
.end method

.method public ping()V
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 690
    iget v4, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mFeedbackCount:I

    #@3
    if-lez v4, :cond_34

    #@5
    .line 691
    const/4 v0, 0x1

    #@6
    .line 692
    .local v0, doWaveAnimation:Z
    iget-object v3, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mWaveAnimations:Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;

    #@8
    .line 695
    .local v3, waveAnimations:Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;
    invoke-virtual {v3}, Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;->size()I

    #@b
    move-result v4

    #@c
    if-lez v4, :cond_2f

    #@e
    invoke-virtual {v3, v5}, Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;->get(I)Ljava/lang/Object;

    #@11
    move-result-object v4

    #@12
    check-cast v4, Lcom/android/internal/widget/multiwaveview/Tweener;

    #@14
    iget-object v4, v4, Lcom/android/internal/widget/multiwaveview/Tweener;->animator:Landroid/animation/ObjectAnimator;

    #@16
    invoke-virtual {v4}, Landroid/animation/ObjectAnimator;->isRunning()Z

    #@19
    move-result v4

    #@1a
    if-eqz v4, :cond_2f

    #@1c
    .line 696
    invoke-virtual {v3, v5}, Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;->get(I)Ljava/lang/Object;

    #@1f
    move-result-object v4

    #@20
    check-cast v4, Lcom/android/internal/widget/multiwaveview/Tweener;

    #@22
    iget-object v4, v4, Lcom/android/internal/widget/multiwaveview/Tweener;->animator:Landroid/animation/ObjectAnimator;

    #@24
    invoke-virtual {v4}, Landroid/animation/ObjectAnimator;->getCurrentPlayTime()J

    #@27
    move-result-wide v1

    #@28
    .line 697
    .local v1, t:J
    const-wide/16 v4, 0x2a3

    #@2a
    cmp-long v4, v1, v4

    #@2c
    if-gez v4, :cond_2f

    #@2e
    .line 698
    const/4 v0, 0x0

    #@2f
    .line 702
    .end local v1           #t:J
    :cond_2f
    if-eqz v0, :cond_34

    #@31
    .line 703
    invoke-direct {p0}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->startWaveAnimation()V

    #@34
    .line 706
    .end local v0           #doWaveAnimation:Z
    .end local v3           #waveAnimations:Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;
    :cond_34
    return-void
.end method

.method public replaceTargetDrawablesIfPresent(Landroid/content/ComponentName;Ljava/lang/String;I)Z
    .registers 14
    .parameter "component"
    .parameter "name"
    .parameter "existingResId"

    #@0
    .prologue
    .line 1373
    if-nez p3, :cond_4

    #@2
    const/4 v5, 0x0

    #@3
    .line 1401
    :cond_3
    :goto_3
    return v5

    #@4
    .line 1375
    :cond_4
    const/4 v5, 0x0

    #@5
    .line 1376
    .local v5, replaced:Z
    if-eqz p1, :cond_25

    #@7
    .line 1378
    :try_start_7
    iget-object v7, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mContext:Landroid/content/Context;

    #@9
    invoke-virtual {v7}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@c
    move-result-object v4

    #@d
    .line 1380
    .local v4, packageManager:Landroid/content/pm/PackageManager;
    const/16 v7, 0x80

    #@f
    invoke-virtual {v4, p1, v7}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    #@12
    move-result-object v7

    #@13
    iget-object v2, v7, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    #@15
    .line 1382
    .local v2, metaData:Landroid/os/Bundle;
    if-eqz v2, :cond_25

    #@17
    .line 1383
    invoke-virtual {v2, p2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@1a
    move-result v1

    #@1b
    .line 1384
    .local v1, iconResId:I
    if-eqz v1, :cond_25

    #@1d
    .line 1385
    invoke-virtual {v4, p1}, Landroid/content/pm/PackageManager;->getResourcesForActivity(Landroid/content/ComponentName;)Landroid/content/res/Resources;

    #@20
    move-result-object v6

    #@21
    .line 1386
    .local v6, res:Landroid/content/res/Resources;
    invoke-direct {p0, v6, p3, v1}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->replaceTargetDrawables(Landroid/content/res/Resources;II)Z
    :try_end_24
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_7 .. :try_end_24} :catch_31
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_7 .. :try_end_24} :catch_55

    #@24
    move-result v5

    #@25
    .line 1397
    .end local v1           #iconResId:I
    .end local v2           #metaData:Landroid/os/Bundle;
    .end local v4           #packageManager:Landroid/content/pm/PackageManager;
    .end local v6           #res:Landroid/content/res/Resources;
    :cond_25
    :goto_25
    if-nez v5, :cond_3

    #@27
    .line 1399
    iget-object v7, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mContext:Landroid/content/Context;

    #@29
    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@2c
    move-result-object v7

    #@2d
    invoke-direct {p0, v7, p3, p3}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->replaceTargetDrawables(Landroid/content/res/Resources;II)Z

    #@30
    goto :goto_3

    #@31
    .line 1389
    :catch_31
    move-exception v0

    #@32
    .line 1390
    .local v0, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v7, "GlowPadView"

    #@34
    new-instance v8, Ljava/lang/StringBuilder;

    #@36
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    const-string v9, "Failed to swap drawable; "

    #@3b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v8

    #@3f
    invoke-virtual {p1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    #@42
    move-result-object v9

    #@43
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v8

    #@47
    const-string v9, " not found"

    #@49
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v8

    #@4d
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50
    move-result-object v8

    #@51
    invoke-static {v7, v8, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@54
    goto :goto_25

    #@55
    .line 1392
    .end local v0           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_55
    move-exception v3

    #@56
    .line 1393
    .local v3, nfe:Landroid/content/res/Resources$NotFoundException;
    const-string v7, "GlowPadView"

    #@58
    new-instance v8, Ljava/lang/StringBuilder;

    #@5a
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@5d
    const-string v9, "Failed to swap drawable from "

    #@5f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v8

    #@63
    invoke-virtual {p1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    #@66
    move-result-object v9

    #@67
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v8

    #@6b
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6e
    move-result-object v8

    #@6f
    invoke-static {v7, v8, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@72
    goto :goto_25
.end method

.method public reset(Z)V
    .registers 5
    .parameter "animate"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 739
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mGlowAnimations:Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;

    #@4
    invoke-virtual {v0}, Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;->stop()V

    #@7
    .line 740
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetAnimations:Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;

    #@9
    invoke-virtual {v0}, Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;->stop()V

    #@c
    .line 741
    invoke-direct {p0, v1, v2}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->startBackgroundAnimation(IF)V

    #@f
    .line 742
    invoke-direct {p0}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->stopAndHideWaveAnimation()V

    #@12
    .line 743
    invoke-direct {p0, p1, v1}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->hideTargets(ZZ)V

    #@15
    .line 744
    const/4 v0, 0x0

    #@16
    invoke-direct {p0, v1, v1, v2, v0}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->hideGlow(IIFLandroid/animation/Animator$AnimatorListener;)V

    #@19
    .line 745
    invoke-static {}, Lcom/android/internal/widget/multiwaveview/Tweener;->reset()V

    #@1c
    .line 746
    return-void
.end method

.method public resumeAnimations()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 307
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mWaveAnimations:Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;

    #@3
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;->setSuspended(Z)V

    #@6
    .line 308
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetAnimations:Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;

    #@8
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;->setSuspended(Z)V

    #@b
    .line 309
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mGlowAnimations:Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;

    #@d
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;->setSuspended(Z)V

    #@10
    .line 310
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mWaveAnimations:Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;

    #@12
    invoke-virtual {v0}, Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;->start()V

    #@15
    .line 311
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetAnimations:Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;

    #@17
    invoke-virtual {v0}, Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;->start()V

    #@1a
    .line 312
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mGlowAnimations:Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;

    #@1c
    invoke-virtual {v0}, Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;->start()V

    #@1f
    .line 313
    return-void
.end method

.method public setDirectionDescriptionsResourceId(I)V
    .registers 3
    .parameter "resourceId"

    #@0
    .prologue
    .line 657
    iput p1, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mDirectionDescriptionsResourceId:I

    #@2
    .line 658
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mDirectionDescriptions:Ljava/util/ArrayList;

    #@4
    if-eqz v0, :cond_b

    #@6
    .line 659
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mDirectionDescriptions:Ljava/util/ArrayList;

    #@8
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    #@b
    .line 661
    :cond_b
    return-void
.end method

.method public setEnableTarget(IZ)V
    .registers 6
    .parameter "resourceId"
    .parameter "enabled"

    #@0
    .prologue
    .line 1315
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    iget-object v2, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetDrawables:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v2

    #@7
    if-ge v0, v2, :cond_1a

    #@9
    .line 1316
    iget-object v2, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetDrawables:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v1

    #@f
    check-cast v1, Lcom/android/internal/widget/multiwaveview/TargetDrawable;

    #@11
    .line 1317
    .local v1, target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    invoke-virtual {v1}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->getResourceId()I

    #@14
    move-result v2

    #@15
    if-ne v2, p1, :cond_1b

    #@17
    .line 1318
    invoke-virtual {v1, p2}, Lcom/android/internal/widget/multiwaveview/TargetDrawable;->setEnabled(Z)V

    #@1a
    .line 1322
    .end local v1           #target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    :cond_1a
    return-void

    #@1b
    .line 1315
    .restart local v1       #target:Lcom/android/internal/widget/multiwaveview/TargetDrawable;
    :cond_1b
    add-int/lit8 v0, v0, 0x1

    #@1d
    goto :goto_1
.end method

.method public setOnTriggerListener(Lcom/android/internal/widget/multiwaveview/GlowPadView$OnTriggerListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 1217
    iput-object p1, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mOnTriggerListener:Lcom/android/internal/widget/multiwaveview/GlowPadView$OnTriggerListener;

    #@2
    .line 1218
    return-void
.end method

.method protected setShowSelectedTargetName(Z)Z
    .registers 2
    .parameter "show"

    #@0
    .prologue
    .line 1411
    iput-boolean p1, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mShowSelectedTargetName:Z

    #@2
    return p1
.end method

.method public setTargetDescriptionsResourceId(I)V
    .registers 3
    .parameter "resourceId"

    #@0
    .prologue
    .line 636
    iput p1, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetDescriptionsResourceId:I

    #@2
    .line 637
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetDescriptions:Ljava/util/ArrayList;

    #@4
    if-eqz v0, :cond_b

    #@6
    .line 638
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetDescriptions:Ljava/util/ArrayList;

    #@8
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    #@b
    .line 640
    :cond_b
    return-void
.end method

.method public setTargetResources(I)V
    .registers 3
    .parameter "resourceId"

    #@0
    .prologue
    .line 618
    iget-boolean v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mAnimatingTargets:Z

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 620
    iput p1, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mNewTargetResources:I

    #@6
    .line 624
    :goto_6
    return-void

    #@7
    .line 622
    :cond_7
    invoke-direct {p0, p1}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->internalSetTargetResources(I)V

    #@a
    goto :goto_6
.end method

.method public setVibrateEnabled(Z)V
    .registers 4
    .parameter "enabled"

    #@0
    .prologue
    .line 678
    if-eqz p1, :cond_15

    #@2
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mVibrator:Landroid/os/Vibrator;

    #@4
    if-nez v0, :cond_15

    #@6
    .line 679
    invoke-virtual {p0}, Lcom/android/internal/widget/multiwaveview/GlowPadView;->getContext()Landroid/content/Context;

    #@9
    move-result-object v0

    #@a
    const-string v1, "vibrator"

    #@c
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Landroid/os/Vibrator;

    #@12
    iput-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mVibrator:Landroid/os/Vibrator;

    #@14
    .line 683
    :goto_14
    return-void

    #@15
    .line 681
    :cond_15
    const/4 v0, 0x0

    #@16
    iput-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mVibrator:Landroid/os/Vibrator;

    #@18
    goto :goto_14
.end method

.method protected showSelectedTargetName(I)V
    .registers 4
    .parameter "activeTarget"

    #@0
    .prologue
    .line 1415
    iget-boolean v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mShowSelectedTargetName:Z

    #@2
    if-nez v0, :cond_5

    #@4
    .line 1426
    :cond_4
    :goto_4
    return-void

    #@5
    .line 1419
    :cond_5
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mOnTriggerListener:Lcom/android/internal/widget/multiwaveview/GlowPadView$OnTriggerListener;

    #@7
    if-eqz v0, :cond_4

    #@9
    .line 1420
    const/4 v0, -0x1

    #@a
    if-ne p1, v0, :cond_14

    #@c
    .line 1421
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mOnTriggerListener:Lcom/android/internal/widget/multiwaveview/GlowPadView$OnTriggerListener;

    #@e
    const/high16 v1, -0x8000

    #@10
    invoke-interface {v0, p0, v1}, Lcom/android/internal/widget/multiwaveview/GlowPadView$OnTriggerListener;->onTrigger(Landroid/view/View;I)V

    #@13
    goto :goto_4

    #@14
    .line 1423
    :cond_14
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mOnTriggerListener:Lcom/android/internal/widget/multiwaveview/GlowPadView$OnTriggerListener;

    #@16
    mul-int/lit8 v1, p1, -0x1

    #@18
    invoke-interface {v0, p0, v1}, Lcom/android/internal/widget/multiwaveview/GlowPadView$OnTriggerListener;->onTrigger(Landroid/view/View;I)V

    #@1b
    goto :goto_4
.end method

.method public suspendAnimations()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 301
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mWaveAnimations:Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;

    #@3
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;->setSuspended(Z)V

    #@6
    .line 302
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mTargetAnimations:Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;

    #@8
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;->setSuspended(Z)V

    #@b
    .line 303
    iget-object v0, p0, Lcom/android/internal/widget/multiwaveview/GlowPadView;->mGlowAnimations:Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;

    #@d
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/multiwaveview/GlowPadView$AnimationBundle;->setSuspended(Z)V

    #@10
    .line 304
    return-void
.end method
