.class final Lcom/android/internal/widget/multiwaveview/Ease$Sine$2;
.super Ljava/lang/Object;
.source "Ease.java"

# interfaces
.implements Landroid/animation/TimeInterpolator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/widget/multiwaveview/Ease$Sine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 120
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .registers 7
    .parameter "input"

    #@0
    .prologue
    const/high16 v4, 0x3f80

    #@2
    .line 122
    div-float v0, p1, v4

    #@4
    float-to-double v0, v0

    #@5
    const-wide v2, 0x3ff921fb54442d18L

    #@a
    mul-double/2addr v0, v2

    #@b
    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    #@e
    move-result-wide v0

    #@f
    double-to-float v0, v0

    #@10
    mul-float/2addr v0, v4

    #@11
    const/4 v1, 0x0

    #@12
    add-float/2addr v0, v1

    #@13
    return v0
.end method
