.class Lcom/android/internal/widget/multiwaveview/Tweener;
.super Ljava/lang/Object;
.source "Tweener.java"


# static fields
.field private static final DEBUG:Z = false

.field private static final TAG:Ljava/lang/String; = "Tweener"

.field private static mCleanupListener:Landroid/animation/Animator$AnimatorListener;

.field private static sTweens:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Object;",
            "Lcom/android/internal/widget/multiwaveview/Tweener;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field animator:Landroid/animation/ObjectAnimator;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 38
    new-instance v0, Ljava/util/HashMap;

    #@2
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@5
    sput-object v0, Lcom/android/internal/widget/multiwaveview/Tweener;->sTweens:Ljava/util/HashMap;

    #@7
    .line 140
    new-instance v0, Lcom/android/internal/widget/multiwaveview/Tweener$1;

    #@9
    invoke-direct {v0}, Lcom/android/internal/widget/multiwaveview/Tweener$1;-><init>()V

    #@c
    sput-object v0, Lcom/android/internal/widget/multiwaveview/Tweener;->mCleanupListener:Landroid/animation/Animator$AnimatorListener;

    #@e
    return-void
.end method

.method public constructor <init>(Landroid/animation/ObjectAnimator;)V
    .registers 2
    .parameter "anim"

    #@0
    .prologue
    .line 40
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 41
    iput-object p1, p0, Lcom/android/internal/widget/multiwaveview/Tweener;->animator:Landroid/animation/ObjectAnimator;

    #@5
    .line 42
    return-void
.end method

.method static synthetic access$000(Landroid/animation/Animator;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 33
    invoke-static {p0}, Lcom/android/internal/widget/multiwaveview/Tweener;->remove(Landroid/animation/Animator;)V

    #@3
    return-void
.end method

.method private static remove(Landroid/animation/Animator;)V
    .registers 4
    .parameter "animator"

    #@0
    .prologue
    .line 45
    sget-object v2, Lcom/android/internal/widget/multiwaveview/Tweener;->sTweens:Ljava/util/HashMap;

    #@2
    invoke-virtual {v2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@5
    move-result-object v2

    #@6
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@9
    move-result-object v1

    #@a
    .line 46
    .local v1, iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Object;Lcom/android/internal/widget/multiwaveview/Tweener;>;>;"
    :cond_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@d
    move-result v2

    #@e
    if-eqz v2, :cond_23

    #@10
    .line 47
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@13
    move-result-object v0

    #@14
    check-cast v0, Ljava/util/Map$Entry;

    #@16
    .line 48
    .local v0, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/Object;Lcom/android/internal/widget/multiwaveview/Tweener;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@19
    move-result-object v2

    #@1a
    check-cast v2, Lcom/android/internal/widget/multiwaveview/Tweener;

    #@1c
    iget-object v2, v2, Lcom/android/internal/widget/multiwaveview/Tweener;->animator:Landroid/animation/ObjectAnimator;

    #@1e
    if-ne v2, p0, :cond_a

    #@20
    .line 51
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    #@23
    .line 55
    .end local v0           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/Object;Lcom/android/internal/widget/multiwaveview/Tweener;>;"
    :cond_23
    return-void
.end method

.method private static varargs replace(Ljava/util/ArrayList;[Ljava/lang/Object;)V
    .registers 9
    .parameter
    .parameter "args"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/animation/PropertyValuesHolder;",
            ">;[",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    #@0
    .prologue
    .line 164
    .local p0, props:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/PropertyValuesHolder;>;"
    move-object v0, p1

    #@1
    .local v0, arr$:[Ljava/lang/Object;
    array-length v3, v0

    #@2
    .local v3, len$:I
    const/4 v1, 0x0

    #@3
    .local v1, i$:I
    :goto_3
    if-ge v1, v3, :cond_32

    #@5
    aget-object v2, v0, v1

    #@7
    .line 165
    .local v2, killobject:Ljava/lang/Object;
    sget-object v5, Lcom/android/internal/widget/multiwaveview/Tweener;->sTweens:Ljava/util/HashMap;

    #@9
    invoke-virtual {v5, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@c
    move-result-object v4

    #@d
    check-cast v4, Lcom/android/internal/widget/multiwaveview/Tweener;

    #@f
    .line 166
    .local v4, tween:Lcom/android/internal/widget/multiwaveview/Tweener;
    if-eqz v4, :cond_29

    #@11
    .line 167
    iget-object v5, v4, Lcom/android/internal/widget/multiwaveview/Tweener;->animator:Landroid/animation/ObjectAnimator;

    #@13
    invoke-virtual {v5}, Landroid/animation/ObjectAnimator;->cancel()V

    #@16
    .line 168
    if-eqz p0, :cond_2c

    #@18
    .line 169
    iget-object v6, v4, Lcom/android/internal/widget/multiwaveview/Tweener;->animator:Landroid/animation/ObjectAnimator;

    #@1a
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    #@1d
    move-result v5

    #@1e
    new-array v5, v5, [Landroid/animation/PropertyValuesHolder;

    #@20
    invoke-virtual {p0, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@23
    move-result-object v5

    #@24
    check-cast v5, [Landroid/animation/PropertyValuesHolder;

    #@26
    invoke-virtual {v6, v5}, Landroid/animation/ObjectAnimator;->setValues([Landroid/animation/PropertyValuesHolder;)V

    #@29
    .line 164
    :cond_29
    :goto_29
    add-int/lit8 v1, v1, 0x1

    #@2b
    goto :goto_3

    #@2c
    .line 172
    :cond_2c
    sget-object v5, Lcom/android/internal/widget/multiwaveview/Tweener;->sTweens:Ljava/util/HashMap;

    #@2e
    invoke-virtual {v5, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@31
    goto :goto_29

    #@32
    .line 176
    .end local v2           #killobject:Ljava/lang/Object;
    .end local v4           #tween:Lcom/android/internal/widget/multiwaveview/Tweener;
    :cond_32
    return-void
.end method

.method public static reset()V
    .registers 1

    #@0
    .prologue
    .line 160
    sget-object v0, Lcom/android/internal/widget/multiwaveview/Tweener;->sTweens:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    #@5
    .line 161
    return-void
.end method

.method public static varargs to(Ljava/lang/Object;J[Ljava/lang/Object;)Lcom/android/internal/widget/multiwaveview/Tweener;
    .registers 22
    .parameter "object"
    .parameter "duration"
    .parameter "vars"

    #@0
    .prologue
    .line 58
    const-wide/16 v3, 0x0

    #@2
    .line 59
    .local v3, delay:J
    const/4 v12, 0x0

    #@3
    .line 60
    .local v12, updateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;
    const/4 v9, 0x0

    #@4
    .line 61
    .local v9, listener:Landroid/animation/Animator$AnimatorListener;
    const/4 v7, 0x0

    #@5
    .line 64
    .local v7, interpolator:Landroid/animation/TimeInterpolator;
    new-instance v10, Ljava/util/ArrayList;

    #@7
    move-object/from16 v0, p3

    #@9
    array-length v14, v0

    #@a
    div-int/lit8 v14, v14, 0x2

    #@c
    invoke-direct {v10, v14}, Ljava/util/ArrayList;-><init>(I)V

    #@f
    .line 65
    .local v10, props:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/animation/PropertyValuesHolder;>;"
    const/4 v6, 0x0

    #@10
    .local v6, i:I
    :goto_10
    move-object/from16 v0, p3

    #@12
    array-length v14, v0

    #@13
    if-ge v6, v14, :cond_123

    #@15
    .line 66
    aget-object v14, p3, v6

    #@17
    instance-of v14, v14, Ljava/lang/String;

    #@19
    if-nez v14, :cond_36

    #@1b
    .line 67
    new-instance v14, Ljava/lang/IllegalArgumentException;

    #@1d
    new-instance v15, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    const-string v16, "Key must be a string: "

    #@24
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v15

    #@28
    aget-object v16, p3, v6

    #@2a
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v15

    #@2e
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v15

    #@32
    invoke-direct {v14, v15}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@35
    throw v14

    #@36
    .line 69
    :cond_36
    aget-object v8, p3, v6

    #@38
    check-cast v8, Ljava/lang/String;

    #@3a
    .line 70
    .local v8, key:Ljava/lang/String;
    add-int/lit8 v14, v6, 0x1

    #@3c
    aget-object v13, p3, v14

    #@3e
    .line 71
    .local v13, value:Ljava/lang/Object;
    const-string v14, "simultaneousTween"

    #@40
    invoke-virtual {v14, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@43
    move-result v14

    #@44
    if-eqz v14, :cond_49

    #@46
    .line 65
    .end local v13           #value:Ljava/lang/Object;
    :cond_46
    :goto_46
    add-int/lit8 v6, v6, 0x2

    #@48
    goto :goto_10

    #@49
    .line 73
    .restart local v13       #value:Ljava/lang/Object;
    :cond_49
    const-string v14, "ease"

    #@4b
    invoke-virtual {v14, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4e
    move-result v14

    #@4f
    if-eqz v14, :cond_55

    #@51
    move-object v7, v13

    #@52
    .line 74
    check-cast v7, Landroid/animation/TimeInterpolator;

    #@54
    goto :goto_46

    #@55
    .line 75
    :cond_55
    const-string v14, "onUpdate"

    #@57
    invoke-virtual {v14, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5a
    move-result v14

    #@5b
    if-nez v14, :cond_65

    #@5d
    const-string v14, "onUpdateListener"

    #@5f
    invoke-virtual {v14, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@62
    move-result v14

    #@63
    if-eqz v14, :cond_69

    #@65
    :cond_65
    move-object v12, v13

    #@66
    .line 76
    check-cast v12, Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    #@68
    goto :goto_46

    #@69
    .line 77
    :cond_69
    const-string v14, "onComplete"

    #@6b
    invoke-virtual {v14, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6e
    move-result v14

    #@6f
    if-nez v14, :cond_79

    #@71
    const-string v14, "onCompleteListener"

    #@73
    invoke-virtual {v14, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@76
    move-result v14

    #@77
    if-eqz v14, :cond_7d

    #@79
    :cond_79
    move-object v9, v13

    #@7a
    .line 78
    check-cast v9, Landroid/animation/Animator$AnimatorListener;

    #@7c
    goto :goto_46

    #@7d
    .line 79
    :cond_7d
    const-string v14, "delay"

    #@7f
    invoke-virtual {v14, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@82
    move-result v14

    #@83
    if-eqz v14, :cond_8c

    #@85
    .line 80
    check-cast v13, Ljava/lang/Number;

    #@87
    .end local v13           #value:Ljava/lang/Object;
    invoke-virtual {v13}, Ljava/lang/Number;->longValue()J

    #@8a
    move-result-wide v3

    #@8b
    goto :goto_46

    #@8c
    .line 81
    .restart local v13       #value:Ljava/lang/Object;
    :cond_8c
    const-string v14, "syncWith"

    #@8e
    invoke-virtual {v14, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@91
    move-result v14

    #@92
    if-nez v14, :cond_46

    #@94
    .line 83
    instance-of v14, v13, [F

    #@96
    if-eqz v14, :cond_bb

    #@98
    .line 84
    const/4 v14, 0x2

    #@99
    new-array v15, v14, [F

    #@9b
    const/16 v16, 0x0

    #@9d
    move-object v14, v13

    #@9e
    check-cast v14, [F

    #@a0
    check-cast v14, [F

    #@a2
    const/16 v17, 0x0

    #@a4
    aget v14, v14, v17

    #@a6
    aput v14, v15, v16

    #@a8
    const/4 v14, 0x1

    #@a9
    check-cast v13, [F

    #@ab
    .end local v13           #value:Ljava/lang/Object;
    check-cast v13, [F

    #@ad
    const/16 v16, 0x1

    #@af
    aget v16, v13, v16

    #@b1
    aput v16, v15, v14

    #@b3
    invoke-static {v8, v15}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    #@b6
    move-result-object v14

    #@b7
    invoke-virtual {v10, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@ba
    goto :goto_46

    #@bb
    .line 86
    .restart local v13       #value:Ljava/lang/Object;
    :cond_bb
    instance-of v14, v13, [I

    #@bd
    if-eqz v14, :cond_e3

    #@bf
    .line 87
    const/4 v14, 0x2

    #@c0
    new-array v15, v14, [I

    #@c2
    const/16 v16, 0x0

    #@c4
    move-object v14, v13

    #@c5
    check-cast v14, [I

    #@c7
    check-cast v14, [I

    #@c9
    const/16 v17, 0x0

    #@cb
    aget v14, v14, v17

    #@cd
    aput v14, v15, v16

    #@cf
    const/4 v14, 0x1

    #@d0
    check-cast v13, [I

    #@d2
    .end local v13           #value:Ljava/lang/Object;
    check-cast v13, [I

    #@d4
    const/16 v16, 0x1

    #@d6
    aget v16, v13, v16

    #@d8
    aput v16, v15, v14

    #@da
    invoke-static {v8, v15}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    #@dd
    move-result-object v14

    #@de
    invoke-virtual {v10, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@e1
    goto/16 :goto_46

    #@e3
    .line 89
    .restart local v13       #value:Ljava/lang/Object;
    :cond_e3
    instance-of v14, v13, Ljava/lang/Number;

    #@e5
    if-eqz v14, :cond_fc

    #@e7
    .line 90
    check-cast v13, Ljava/lang/Number;

    #@e9
    .end local v13           #value:Ljava/lang/Object;
    invoke-virtual {v13}, Ljava/lang/Number;->floatValue()F

    #@ec
    move-result v5

    #@ed
    .line 91
    .local v5, floatValue:F
    const/4 v14, 0x1

    #@ee
    new-array v14, v14, [F

    #@f0
    const/4 v15, 0x0

    #@f1
    aput v5, v14, v15

    #@f3
    invoke-static {v8, v14}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    #@f6
    move-result-object v14

    #@f7
    invoke-virtual {v10, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@fa
    goto/16 :goto_46

    #@fc
    .line 93
    .end local v5           #floatValue:F
    .restart local v13       #value:Ljava/lang/Object;
    :cond_fc
    new-instance v14, Ljava/lang/IllegalArgumentException;

    #@fe
    new-instance v15, Ljava/lang/StringBuilder;

    #@100
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@103
    const-string v16, "Bad argument for key \""

    #@105
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@108
    move-result-object v15

    #@109
    invoke-virtual {v15, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10c
    move-result-object v15

    #@10d
    const-string v16, "\" with value "

    #@10f
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@112
    move-result-object v15

    #@113
    invoke-virtual {v13}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@116
    move-result-object v16

    #@117
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@11a
    move-result-object v15

    #@11b
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11e
    move-result-object v15

    #@11f
    invoke-direct {v14, v15}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@122
    throw v14

    #@123
    .line 99
    .end local v8           #key:Ljava/lang/String;
    .end local v13           #value:Ljava/lang/Object;
    :cond_123
    sget-object v14, Lcom/android/internal/widget/multiwaveview/Tweener;->sTweens:Ljava/util/HashMap;

    #@125
    move-object/from16 v0, p0

    #@127
    invoke-virtual {v14, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@12a
    move-result-object v11

    #@12b
    check-cast v11, Lcom/android/internal/widget/multiwaveview/Tweener;

    #@12d
    .line 100
    .local v11, tween:Lcom/android/internal/widget/multiwaveview/Tweener;
    const/4 v2, 0x0

    #@12e
    .line 101
    .local v2, anim:Landroid/animation/ObjectAnimator;
    if-nez v11, :cond_171

    #@130
    .line 102
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    #@133
    move-result v14

    #@134
    new-array v14, v14, [Landroid/animation/PropertyValuesHolder;

    #@136
    invoke-virtual {v10, v14}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@139
    move-result-object v14

    #@13a
    check-cast v14, [Landroid/animation/PropertyValuesHolder;

    #@13c
    move-object/from16 v0, p0

    #@13e
    invoke-static {v0, v14}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    #@141
    move-result-object v2

    #@142
    .line 104
    new-instance v11, Lcom/android/internal/widget/multiwaveview/Tweener;

    #@144
    .end local v11           #tween:Lcom/android/internal/widget/multiwaveview/Tweener;
    invoke-direct {v11, v2}, Lcom/android/internal/widget/multiwaveview/Tweener;-><init>(Landroid/animation/ObjectAnimator;)V

    #@147
    .line 105
    .restart local v11       #tween:Lcom/android/internal/widget/multiwaveview/Tweener;
    sget-object v14, Lcom/android/internal/widget/multiwaveview/Tweener;->sTweens:Ljava/util/HashMap;

    #@149
    move-object/from16 v0, p0

    #@14b
    invoke-virtual {v14, v0, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@14e
    .line 112
    :goto_14e
    if-eqz v7, :cond_153

    #@150
    .line 113
    invoke-virtual {v2, v7}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    #@153
    .line 117
    :cond_153
    invoke-virtual {v2, v3, v4}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    #@156
    .line 118
    move-wide/from16 v0, p1

    #@158
    invoke-virtual {v2, v0, v1}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    #@15b
    .line 119
    if-eqz v12, :cond_163

    #@15d
    .line 120
    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->removeAllUpdateListeners()V

    #@160
    .line 121
    invoke-virtual {v2, v12}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    #@163
    .line 123
    :cond_163
    if-eqz v9, :cond_16b

    #@165
    .line 124
    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->removeAllListeners()V

    #@168
    .line 125
    invoke-virtual {v2, v9}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    #@16b
    .line 127
    :cond_16b
    sget-object v14, Lcom/android/internal/widget/multiwaveview/Tweener;->mCleanupListener:Landroid/animation/Animator$AnimatorListener;

    #@16d
    invoke-virtual {v2, v14}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    #@170
    .line 129
    return-object v11

    #@171
    .line 108
    :cond_171
    sget-object v14, Lcom/android/internal/widget/multiwaveview/Tweener;->sTweens:Ljava/util/HashMap;

    #@173
    move-object/from16 v0, p0

    #@175
    invoke-virtual {v14, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@178
    move-result-object v14

    #@179
    check-cast v14, Lcom/android/internal/widget/multiwaveview/Tweener;

    #@17b
    iget-object v2, v14, Lcom/android/internal/widget/multiwaveview/Tweener;->animator:Landroid/animation/ObjectAnimator;

    #@17d
    .line 109
    const/4 v14, 0x1

    #@17e
    new-array v14, v14, [Ljava/lang/Object;

    #@180
    const/4 v15, 0x0

    #@181
    aput-object p0, v14, v15

    #@183
    invoke-static {v10, v14}, Lcom/android/internal/widget/multiwaveview/Tweener;->replace(Ljava/util/ArrayList;[Ljava/lang/Object;)V

    #@186
    goto :goto_14e
.end method


# virtual methods
.method varargs from(Ljava/lang/Object;J[Ljava/lang/Object;)Lcom/android/internal/widget/multiwaveview/Tweener;
    .registers 6
    .parameter "object"
    .parameter "duration"
    .parameter "vars"

    #@0
    .prologue
    .line 136
    invoke-static {p1, p2, p3, p4}, Lcom/android/internal/widget/multiwaveview/Tweener;->to(Ljava/lang/Object;J[Ljava/lang/Object;)Lcom/android/internal/widget/multiwaveview/Tweener;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method
