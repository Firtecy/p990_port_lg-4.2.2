.class public Lcom/android/internal/widget/ActionBarContainer;
.super Landroid/widget/FrameLayout;
.source "ActionBarContainer.java"


# instance fields
.field private mActionBarView:Lcom/android/internal/widget/ActionBarView;

.field private mBackground:Landroid/graphics/drawable/Drawable;

.field private mIsSplit:Z

.field private mIsStacked:Z

.field private mIsTransitioning:Z

.field private mSplitBackground:Landroid/graphics/drawable/Drawable;

.field private mStackedBackground:Landroid/graphics/drawable/Drawable;

.field private mTabContainer:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 48
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Lcom/android/internal/widget/ActionBarContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 8
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    .line 52
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@5
    .line 54
    const/4 v3, 0x0

    #@6
    invoke-virtual {p0, v3}, Lcom/android/internal/widget/ActionBarContainer;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    #@9
    .line 56
    sget-object v3, Lcom/android/internal/R$styleable;->ActionBar:[I

    #@b
    invoke-virtual {p1, p2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@e
    move-result-object v0

    #@f
    .line 58
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v3, 0x2

    #@10
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@13
    move-result-object v3

    #@14
    iput-object v3, p0, Lcom/android/internal/widget/ActionBarContainer;->mBackground:Landroid/graphics/drawable/Drawable;

    #@16
    .line 59
    const/16 v3, 0x11

    #@18
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@1b
    move-result-object v3

    #@1c
    iput-object v3, p0, Lcom/android/internal/widget/ActionBarContainer;->mStackedBackground:Landroid/graphics/drawable/Drawable;

    #@1e
    .line 62
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarContainer;->getId()I

    #@21
    move-result v3

    #@22
    const v4, 0x102036f

    #@25
    if-ne v3, v4, :cond_31

    #@27
    .line 63
    iput-boolean v1, p0, Lcom/android/internal/widget/ActionBarContainer;->mIsSplit:Z

    #@29
    .line 64
    const/16 v3, 0x12

    #@2b
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@2e
    move-result-object v3

    #@2f
    iput-object v3, p0, Lcom/android/internal/widget/ActionBarContainer;->mSplitBackground:Landroid/graphics/drawable/Drawable;

    #@31
    .line 67
    :cond_31
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@34
    .line 69
    iget-boolean v3, p0, Lcom/android/internal/widget/ActionBarContainer;->mIsSplit:Z

    #@36
    if-eqz v3, :cond_42

    #@38
    iget-object v3, p0, Lcom/android/internal/widget/ActionBarContainer;->mSplitBackground:Landroid/graphics/drawable/Drawable;

    #@3a
    if-nez v3, :cond_40

    #@3c
    :cond_3c
    :goto_3c
    invoke-virtual {p0, v1}, Lcom/android/internal/widget/ActionBarContainer;->setWillNotDraw(Z)V

    #@3f
    .line 71
    return-void

    #@40
    :cond_40
    move v1, v2

    #@41
    .line 69
    goto :goto_3c

    #@42
    :cond_42
    iget-object v3, p0, Lcom/android/internal/widget/ActionBarContainer;->mBackground:Landroid/graphics/drawable/Drawable;

    #@44
    if-nez v3, :cond_4a

    #@46
    iget-object v3, p0, Lcom/android/internal/widget/ActionBarContainer;->mStackedBackground:Landroid/graphics/drawable/Drawable;

    #@48
    if-eqz v3, :cond_3c

    #@4a
    :cond_4a
    move v1, v2

    #@4b
    goto :goto_3c
.end method


# virtual methods
.method protected drawableStateChanged()V
    .registers 3

    #@0
    .prologue
    .line 138
    invoke-super {p0}, Landroid/widget/FrameLayout;->drawableStateChanged()V

    #@3
    .line 139
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarContainer;->mBackground:Landroid/graphics/drawable/Drawable;

    #@5
    if-eqz v0, :cond_18

    #@7
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarContainer;->mBackground:Landroid/graphics/drawable/Drawable;

    #@9
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    #@c
    move-result v0

    #@d
    if-eqz v0, :cond_18

    #@f
    .line 140
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarContainer;->mBackground:Landroid/graphics/drawable/Drawable;

    #@11
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarContainer;->getDrawableState()[I

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@18
    .line 142
    :cond_18
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarContainer;->mStackedBackground:Landroid/graphics/drawable/Drawable;

    #@1a
    if-eqz v0, :cond_2d

    #@1c
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarContainer;->mStackedBackground:Landroid/graphics/drawable/Drawable;

    #@1e
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    #@21
    move-result v0

    #@22
    if-eqz v0, :cond_2d

    #@24
    .line 143
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarContainer;->mStackedBackground:Landroid/graphics/drawable/Drawable;

    #@26
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarContainer;->getDrawableState()[I

    #@29
    move-result-object v1

    #@2a
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@2d
    .line 145
    :cond_2d
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarContainer;->mSplitBackground:Landroid/graphics/drawable/Drawable;

    #@2f
    if-eqz v0, :cond_42

    #@31
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarContainer;->mSplitBackground:Landroid/graphics/drawable/Drawable;

    #@33
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    #@36
    move-result v0

    #@37
    if-eqz v0, :cond_42

    #@39
    .line 146
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarContainer;->mSplitBackground:Landroid/graphics/drawable/Drawable;

    #@3b
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarContainer;->getDrawableState()[I

    #@3e
    move-result-object v1

    #@3f
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@42
    .line 148
    :cond_42
    return-void
.end method

.method public getTabContainer()Landroid/view/View;
    .registers 2

    #@0
    .prologue
    .line 231
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarContainer;->mTabContainer:Landroid/view/View;

    #@2
    return-object v0
.end method

.method public jumpDrawablesToCurrentState()V
    .registers 2

    #@0
    .prologue
    .line 152
    invoke-super {p0}, Landroid/widget/FrameLayout;->jumpDrawablesToCurrentState()V

    #@3
    .line 153
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarContainer;->mBackground:Landroid/graphics/drawable/Drawable;

    #@5
    if-eqz v0, :cond_c

    #@7
    .line 154
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarContainer;->mBackground:Landroid/graphics/drawable/Drawable;

    #@9
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    #@c
    .line 156
    :cond_c
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarContainer;->mStackedBackground:Landroid/graphics/drawable/Drawable;

    #@e
    if-eqz v0, :cond_15

    #@10
    .line 157
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarContainer;->mStackedBackground:Landroid/graphics/drawable/Drawable;

    #@12
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    #@15
    .line 159
    :cond_15
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarContainer;->mSplitBackground:Landroid/graphics/drawable/Drawable;

    #@17
    if-eqz v0, :cond_1e

    #@19
    .line 160
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarContainer;->mSplitBackground:Landroid/graphics/drawable/Drawable;

    #@1b
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    #@1e
    .line 162
    :cond_1e
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .registers 3
    .parameter "canvas"

    #@0
    .prologue
    .line 236
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarContainer;->getWidth()I

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_c

    #@6
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarContainer;->getHeight()I

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_d

    #@c
    .line 250
    :cond_c
    :goto_c
    return-void

    #@d
    .line 240
    :cond_d
    iget-boolean v0, p0, Lcom/android/internal/widget/ActionBarContainer;->mIsSplit:Z

    #@f
    if-eqz v0, :cond_1b

    #@11
    .line 241
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarContainer;->mSplitBackground:Landroid/graphics/drawable/Drawable;

    #@13
    if-eqz v0, :cond_c

    #@15
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarContainer;->mSplitBackground:Landroid/graphics/drawable/Drawable;

    #@17
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@1a
    goto :goto_c

    #@1b
    .line 243
    :cond_1b
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarContainer;->mBackground:Landroid/graphics/drawable/Drawable;

    #@1d
    if-eqz v0, :cond_24

    #@1f
    .line 244
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarContainer;->mBackground:Landroid/graphics/drawable/Drawable;

    #@21
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@24
    .line 246
    :cond_24
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarContainer;->mStackedBackground:Landroid/graphics/drawable/Drawable;

    #@26
    if-eqz v0, :cond_c

    #@28
    iget-boolean v0, p0, Lcom/android/internal/widget/ActionBarContainer;->mIsStacked:Z

    #@2a
    if-eqz v0, :cond_c

    #@2c
    .line 247
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarContainer;->mStackedBackground:Landroid/graphics/drawable/Drawable;

    #@2e
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@31
    goto :goto_c
.end method

.method public onFinishInflate()V
    .registers 2

    #@0
    .prologue
    .line 75
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    #@3
    .line 76
    const v0, 0x102036d

    #@6
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/ActionBarContainer;->findViewById(I)Landroid/view/View;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Lcom/android/internal/widget/ActionBarView;

    #@c
    iput-object v0, p0, Lcom/android/internal/widget/ActionBarContainer;->mActionBarView:Lcom/android/internal/widget/ActionBarView;

    #@e
    .line 77
    return-void
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "ev"

    #@0
    .prologue
    .line 210
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onHoverEvent(Landroid/view/MotionEvent;)Z

    #@3
    .line 213
    const/4 v0, 0x1

    #@4
    return v0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "ev"

    #@0
    .prologue
    .line 197
    iget-boolean v0, p0, Lcom/android/internal/widget/ActionBarContainer;->mIsTransitioning:Z

    #@2
    if-nez v0, :cond_a

    #@4
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_c

    #@a
    :cond_a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method public onLayout(ZIIII)V
    .registers 19
    .parameter "changed"
    .parameter "l"
    .parameter "t"
    .parameter "r"
    .parameter "b"

    #@0
    .prologue
    .line 281
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    #@3
    .line 283
    iget-object v8, p0, Lcom/android/internal/widget/ActionBarContainer;->mTabContainer:Landroid/view/View;

    #@5
    if-eqz v8, :cond_46

    #@7
    iget-object v8, p0, Lcom/android/internal/widget/ActionBarContainer;->mTabContainer:Landroid/view/View;

    #@9
    invoke-virtual {v8}, Landroid/view/View;->getVisibility()I

    #@c
    move-result v8

    #@d
    const/16 v9, 0x8

    #@f
    if-eq v8, v9, :cond_46

    #@11
    const/4 v4, 0x1

    #@12
    .line 285
    .local v4, hasTabs:Z
    :goto_12
    iget-object v8, p0, Lcom/android/internal/widget/ActionBarContainer;->mTabContainer:Landroid/view/View;

    #@14
    if-eqz v8, :cond_5c

    #@16
    iget-object v8, p0, Lcom/android/internal/widget/ActionBarContainer;->mTabContainer:Landroid/view/View;

    #@18
    invoke-virtual {v8}, Landroid/view/View;->getVisibility()I

    #@1b
    move-result v8

    #@1c
    const/16 v9, 0x8

    #@1e
    if-eq v8, v9, :cond_5c

    #@20
    .line 286
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarContainer;->getMeasuredHeight()I

    #@23
    move-result v2

    #@24
    .line 287
    .local v2, containerHeight:I
    iget-object v8, p0, Lcom/android/internal/widget/ActionBarContainer;->mTabContainer:Landroid/view/View;

    #@26
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredHeight()I

    #@29
    move-result v7

    #@2a
    .line 289
    .local v7, tabHeight:I
    iget-object v8, p0, Lcom/android/internal/widget/ActionBarContainer;->mActionBarView:Lcom/android/internal/widget/ActionBarView;

    #@2c
    invoke-virtual {v8}, Lcom/android/internal/widget/ActionBarView;->getDisplayOptions()I

    #@2f
    move-result v8

    #@30
    and-int/lit8 v8, v8, 0x2

    #@32
    if-nez v8, :cond_7b

    #@34
    .line 291
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarContainer;->getChildCount()I

    #@37
    move-result v3

    #@38
    .line 292
    .local v3, count:I
    const/4 v5, 0x0

    #@39
    .local v5, i:I
    :goto_39
    if-ge v5, v3, :cond_54

    #@3b
    .line 293
    invoke-virtual {p0, v5}, Lcom/android/internal/widget/ActionBarContainer;->getChildAt(I)Landroid/view/View;

    #@3e
    move-result-object v1

    #@3f
    .line 295
    .local v1, child:Landroid/view/View;
    iget-object v8, p0, Lcom/android/internal/widget/ActionBarContainer;->mTabContainer:Landroid/view/View;

    #@41
    if-ne v1, v8, :cond_48

    #@43
    .line 292
    :cond_43
    :goto_43
    add-int/lit8 v5, v5, 0x1

    #@45
    goto :goto_39

    #@46
    .line 283
    .end local v1           #child:Landroid/view/View;
    .end local v2           #containerHeight:I
    .end local v3           #count:I
    .end local v4           #hasTabs:Z
    .end local v5           #i:I
    .end local v7           #tabHeight:I
    :cond_46
    const/4 v4, 0x0

    #@47
    goto :goto_12

    #@48
    .line 297
    .restart local v1       #child:Landroid/view/View;
    .restart local v2       #containerHeight:I
    .restart local v3       #count:I
    .restart local v4       #hasTabs:Z
    .restart local v5       #i:I
    .restart local v7       #tabHeight:I
    :cond_48
    iget-object v8, p0, Lcom/android/internal/widget/ActionBarContainer;->mActionBarView:Lcom/android/internal/widget/ActionBarView;

    #@4a
    invoke-virtual {v8}, Lcom/android/internal/widget/ActionBarView;->isCollapsed()Z

    #@4d
    move-result v8

    #@4e
    if-nez v8, :cond_43

    #@50
    .line 298
    invoke-virtual {v1, v7}, Landroid/view/View;->offsetTopAndBottom(I)V

    #@53
    goto :goto_43

    #@54
    .line 301
    .end local v1           #child:Landroid/view/View;
    :cond_54
    iget-object v8, p0, Lcom/android/internal/widget/ActionBarContainer;->mTabContainer:Landroid/view/View;

    #@56
    const/4 v9, 0x0

    #@57
    move/from16 v0, p4

    #@59
    invoke-virtual {v8, p2, v9, v0, v7}, Landroid/view/View;->layout(IIII)V

    #@5c
    .line 307
    .end local v2           #containerHeight:I
    .end local v3           #count:I
    .end local v5           #i:I
    .end local v7           #tabHeight:I
    :cond_5c
    :goto_5c
    const/4 v6, 0x0

    #@5d
    .line 308
    .local v6, needsInvalidate:Z
    iget-boolean v8, p0, Lcom/android/internal/widget/ActionBarContainer;->mIsSplit:Z

    #@5f
    if-eqz v8, :cond_85

    #@61
    .line 309
    iget-object v8, p0, Lcom/android/internal/widget/ActionBarContainer;->mSplitBackground:Landroid/graphics/drawable/Drawable;

    #@63
    if-eqz v8, :cond_75

    #@65
    .line 310
    iget-object v8, p0, Lcom/android/internal/widget/ActionBarContainer;->mSplitBackground:Landroid/graphics/drawable/Drawable;

    #@67
    const/4 v9, 0x0

    #@68
    const/4 v10, 0x0

    #@69
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarContainer;->getMeasuredWidth()I

    #@6c
    move-result v11

    #@6d
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarContainer;->getMeasuredHeight()I

    #@70
    move-result v12

    #@71
    invoke-virtual {v8, v9, v10, v11, v12}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@74
    .line 311
    const/4 v6, 0x1

    #@75
    .line 326
    :cond_75
    :goto_75
    if-eqz v6, :cond_7a

    #@77
    .line 327
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarContainer;->invalidate()V

    #@7a
    .line 329
    :cond_7a
    return-void

    #@7b
    .line 303
    .end local v6           #needsInvalidate:Z
    .restart local v2       #containerHeight:I
    .restart local v7       #tabHeight:I
    :cond_7b
    iget-object v8, p0, Lcom/android/internal/widget/ActionBarContainer;->mTabContainer:Landroid/view/View;

    #@7d
    sub-int v9, v2, v7

    #@7f
    move/from16 v0, p4

    #@81
    invoke-virtual {v8, p2, v9, v0, v2}, Landroid/view/View;->layout(IIII)V

    #@84
    goto :goto_5c

    #@85
    .line 314
    .end local v2           #containerHeight:I
    .end local v7           #tabHeight:I
    .restart local v6       #needsInvalidate:Z
    :cond_85
    iget-object v8, p0, Lcom/android/internal/widget/ActionBarContainer;->mBackground:Landroid/graphics/drawable/Drawable;

    #@87
    if-eqz v8, :cond_a7

    #@89
    .line 315
    iget-object v8, p0, Lcom/android/internal/widget/ActionBarContainer;->mBackground:Landroid/graphics/drawable/Drawable;

    #@8b
    iget-object v9, p0, Lcom/android/internal/widget/ActionBarContainer;->mActionBarView:Lcom/android/internal/widget/ActionBarView;

    #@8d
    invoke-virtual {v9}, Lcom/android/internal/widget/ActionBarView;->getLeft()I

    #@90
    move-result v9

    #@91
    iget-object v10, p0, Lcom/android/internal/widget/ActionBarContainer;->mActionBarView:Lcom/android/internal/widget/ActionBarView;

    #@93
    invoke-virtual {v10}, Lcom/android/internal/widget/ActionBarView;->getTop()I

    #@96
    move-result v10

    #@97
    iget-object v11, p0, Lcom/android/internal/widget/ActionBarContainer;->mActionBarView:Lcom/android/internal/widget/ActionBarView;

    #@99
    invoke-virtual {v11}, Lcom/android/internal/widget/ActionBarView;->getRight()I

    #@9c
    move-result v11

    #@9d
    iget-object v12, p0, Lcom/android/internal/widget/ActionBarContainer;->mActionBarView:Lcom/android/internal/widget/ActionBarView;

    #@9f
    invoke-virtual {v12}, Lcom/android/internal/widget/ActionBarView;->getBottom()I

    #@a2
    move-result v12

    #@a3
    invoke-virtual {v8, v9, v10, v11, v12}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@a6
    .line 317
    const/4 v6, 0x1

    #@a7
    .line 319
    :cond_a7
    if-eqz v4, :cond_d1

    #@a9
    iget-object v8, p0, Lcom/android/internal/widget/ActionBarContainer;->mStackedBackground:Landroid/graphics/drawable/Drawable;

    #@ab
    if-eqz v8, :cond_d1

    #@ad
    const/4 v8, 0x1

    #@ae
    :goto_ae
    iput-boolean v8, p0, Lcom/android/internal/widget/ActionBarContainer;->mIsStacked:Z

    #@b0
    if-eqz v8, :cond_75

    #@b2
    .line 320
    iget-object v8, p0, Lcom/android/internal/widget/ActionBarContainer;->mStackedBackground:Landroid/graphics/drawable/Drawable;

    #@b4
    iget-object v9, p0, Lcom/android/internal/widget/ActionBarContainer;->mTabContainer:Landroid/view/View;

    #@b6
    invoke-virtual {v9}, Landroid/view/View;->getLeft()I

    #@b9
    move-result v9

    #@ba
    iget-object v10, p0, Lcom/android/internal/widget/ActionBarContainer;->mTabContainer:Landroid/view/View;

    #@bc
    invoke-virtual {v10}, Landroid/view/View;->getTop()I

    #@bf
    move-result v10

    #@c0
    iget-object v11, p0, Lcom/android/internal/widget/ActionBarContainer;->mTabContainer:Landroid/view/View;

    #@c2
    invoke-virtual {v11}, Landroid/view/View;->getRight()I

    #@c5
    move-result v11

    #@c6
    iget-object v12, p0, Lcom/android/internal/widget/ActionBarContainer;->mTabContainer:Landroid/view/View;

    #@c8
    invoke-virtual {v12}, Landroid/view/View;->getBottom()I

    #@cb
    move-result v12

    #@cc
    invoke-virtual {v8, v9, v10, v11, v12}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@cf
    .line 322
    const/4 v6, 0x1

    #@d0
    goto :goto_75

    #@d1
    .line 319
    :cond_d1
    const/4 v8, 0x0

    #@d2
    goto :goto_ae
.end method

.method public onMeasure(II)V
    .registers 9
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    .line 260
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    #@3
    .line 262
    iget-object v4, p0, Lcom/android/internal/widget/ActionBarContainer;->mActionBarView:Lcom/android/internal/widget/ActionBarView;

    #@5
    if-nez v4, :cond_8

    #@7
    .line 277
    :cond_7
    :goto_7
    return-void

    #@8
    .line 264
    :cond_8
    iget-object v4, p0, Lcom/android/internal/widget/ActionBarContainer;->mActionBarView:Lcom/android/internal/widget/ActionBarView;

    #@a
    invoke-virtual {v4}, Lcom/android/internal/widget/ActionBarView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@d
    move-result-object v1

    #@e
    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    #@10
    .line 265
    .local v1, lp:Landroid/widget/FrameLayout$LayoutParams;
    iget-object v4, p0, Lcom/android/internal/widget/ActionBarContainer;->mActionBarView:Lcom/android/internal/widget/ActionBarView;

    #@12
    invoke-virtual {v4}, Lcom/android/internal/widget/ActionBarView;->isCollapsed()Z

    #@15
    move-result v4

    #@16
    if-eqz v4, :cond_46

    #@18
    const/4 v0, 0x0

    #@19
    .line 268
    .local v0, actionBarViewHeight:I
    :goto_19
    iget-object v4, p0, Lcom/android/internal/widget/ActionBarContainer;->mTabContainer:Landroid/view/View;

    #@1b
    if-eqz v4, :cond_7

    #@1d
    iget-object v4, p0, Lcom/android/internal/widget/ActionBarContainer;->mTabContainer:Landroid/view/View;

    #@1f
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    #@22
    move-result v4

    #@23
    const/16 v5, 0x8

    #@25
    if-eq v4, v5, :cond_7

    #@27
    .line 269
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@2a
    move-result v3

    #@2b
    .line 270
    .local v3, mode:I
    const/high16 v4, -0x8000

    #@2d
    if-ne v3, v4, :cond_7

    #@2f
    .line 271
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@32
    move-result v2

    #@33
    .line 272
    .local v2, maxHeight:I
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarContainer;->getMeasuredWidth()I

    #@36
    move-result v4

    #@37
    iget-object v5, p0, Lcom/android/internal/widget/ActionBarContainer;->mTabContainer:Landroid/view/View;

    #@39
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    #@3c
    move-result v5

    #@3d
    add-int/2addr v5, v0

    #@3e
    invoke-static {v5, v2}, Ljava/lang/Math;->min(II)I

    #@41
    move-result v5

    #@42
    invoke-virtual {p0, v4, v5}, Lcom/android/internal/widget/ActionBarContainer;->setMeasuredDimension(II)V

    #@45
    goto :goto_7

    #@46
    .line 265
    .end local v0           #actionBarViewHeight:I
    .end local v2           #maxHeight:I
    .end local v3           #mode:I
    :cond_46
    iget-object v4, p0, Lcom/android/internal/widget/ActionBarContainer;->mActionBarView:Lcom/android/internal/widget/ActionBarView;

    #@48
    invoke-virtual {v4}, Lcom/android/internal/widget/ActionBarView;->getMeasuredHeight()I

    #@4b
    move-result v4

    #@4c
    iget v5, v1, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    #@4e
    add-int/2addr v4, v5

    #@4f
    iget v5, v1, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    #@51
    add-int v0, v4, v5

    #@53
    goto :goto_19
.end method

.method public onResolveDrawables(I)V
    .registers 3
    .parameter "layoutDirection"

    #@0
    .prologue
    .line 169
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onResolveDrawables(I)V

    #@3
    .line 170
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarContainer;->mBackground:Landroid/graphics/drawable/Drawable;

    #@5
    if-eqz v0, :cond_c

    #@7
    .line 171
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarContainer;->mBackground:Landroid/graphics/drawable/Drawable;

    #@9
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setLayoutDirection(I)V

    #@c
    .line 173
    :cond_c
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarContainer;->mStackedBackground:Landroid/graphics/drawable/Drawable;

    #@e
    if-eqz v0, :cond_15

    #@10
    .line 174
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarContainer;->mStackedBackground:Landroid/graphics/drawable/Drawable;

    #@12
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setLayoutDirection(I)V

    #@15
    .line 176
    :cond_15
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarContainer;->mSplitBackground:Landroid/graphics/drawable/Drawable;

    #@17
    if-eqz v0, :cond_1e

    #@19
    .line 177
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarContainer;->mSplitBackground:Landroid/graphics/drawable/Drawable;

    #@1b
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setLayoutDirection(I)V

    #@1e
    .line 179
    :cond_1e
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "ev"

    #@0
    .prologue
    .line 202
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    #@3
    .line 205
    const/4 v0, 0x1

    #@4
    return v0
.end method

.method public setPrimaryBackground(Landroid/graphics/drawable/Drawable;)V
    .registers 6
    .parameter "bg"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 80
    iget-object v2, p0, Lcom/android/internal/widget/ActionBarContainer;->mBackground:Landroid/graphics/drawable/Drawable;

    #@4
    if-eqz v2, :cond_11

    #@6
    .line 81
    iget-object v2, p0, Lcom/android/internal/widget/ActionBarContainer;->mBackground:Landroid/graphics/drawable/Drawable;

    #@8
    const/4 v3, 0x0

    #@9
    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@c
    .line 82
    iget-object v2, p0, Lcom/android/internal/widget/ActionBarContainer;->mBackground:Landroid/graphics/drawable/Drawable;

    #@e
    invoke-virtual {p0, v2}, Lcom/android/internal/widget/ActionBarContainer;->unscheduleDrawable(Landroid/graphics/drawable/Drawable;)V

    #@11
    .line 84
    :cond_11
    iput-object p1, p0, Lcom/android/internal/widget/ActionBarContainer;->mBackground:Landroid/graphics/drawable/Drawable;

    #@13
    .line 85
    if-eqz p1, :cond_18

    #@15
    .line 86
    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@18
    .line 88
    :cond_18
    iget-boolean v2, p0, Lcom/android/internal/widget/ActionBarContainer;->mIsSplit:Z

    #@1a
    if-eqz v2, :cond_29

    #@1c
    iget-object v2, p0, Lcom/android/internal/widget/ActionBarContainer;->mSplitBackground:Landroid/graphics/drawable/Drawable;

    #@1e
    if-nez v2, :cond_27

    #@20
    :cond_20
    :goto_20
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/ActionBarContainer;->setWillNotDraw(Z)V

    #@23
    .line 90
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarContainer;->invalidate()V

    #@26
    .line 91
    return-void

    #@27
    :cond_27
    move v0, v1

    #@28
    .line 88
    goto :goto_20

    #@29
    :cond_29
    iget-object v2, p0, Lcom/android/internal/widget/ActionBarContainer;->mBackground:Landroid/graphics/drawable/Drawable;

    #@2b
    if-nez v2, :cond_31

    #@2d
    iget-object v2, p0, Lcom/android/internal/widget/ActionBarContainer;->mStackedBackground:Landroid/graphics/drawable/Drawable;

    #@2f
    if-eqz v2, :cond_20

    #@31
    :cond_31
    move v0, v1

    #@32
    goto :goto_20
.end method

.method public setSplitBackground(Landroid/graphics/drawable/Drawable;)V
    .registers 6
    .parameter "bg"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 108
    iget-object v2, p0, Lcom/android/internal/widget/ActionBarContainer;->mSplitBackground:Landroid/graphics/drawable/Drawable;

    #@4
    if-eqz v2, :cond_11

    #@6
    .line 109
    iget-object v2, p0, Lcom/android/internal/widget/ActionBarContainer;->mSplitBackground:Landroid/graphics/drawable/Drawable;

    #@8
    const/4 v3, 0x0

    #@9
    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@c
    .line 110
    iget-object v2, p0, Lcom/android/internal/widget/ActionBarContainer;->mSplitBackground:Landroid/graphics/drawable/Drawable;

    #@e
    invoke-virtual {p0, v2}, Lcom/android/internal/widget/ActionBarContainer;->unscheduleDrawable(Landroid/graphics/drawable/Drawable;)V

    #@11
    .line 112
    :cond_11
    iput-object p1, p0, Lcom/android/internal/widget/ActionBarContainer;->mSplitBackground:Landroid/graphics/drawable/Drawable;

    #@13
    .line 113
    if-eqz p1, :cond_18

    #@15
    .line 114
    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@18
    .line 116
    :cond_18
    iget-boolean v2, p0, Lcom/android/internal/widget/ActionBarContainer;->mIsSplit:Z

    #@1a
    if-eqz v2, :cond_29

    #@1c
    iget-object v2, p0, Lcom/android/internal/widget/ActionBarContainer;->mSplitBackground:Landroid/graphics/drawable/Drawable;

    #@1e
    if-nez v2, :cond_27

    #@20
    :cond_20
    :goto_20
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/ActionBarContainer;->setWillNotDraw(Z)V

    #@23
    .line 118
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarContainer;->invalidate()V

    #@26
    .line 119
    return-void

    #@27
    :cond_27
    move v0, v1

    #@28
    .line 116
    goto :goto_20

    #@29
    :cond_29
    iget-object v2, p0, Lcom/android/internal/widget/ActionBarContainer;->mBackground:Landroid/graphics/drawable/Drawable;

    #@2b
    if-nez v2, :cond_31

    #@2d
    iget-object v2, p0, Lcom/android/internal/widget/ActionBarContainer;->mStackedBackground:Landroid/graphics/drawable/Drawable;

    #@2f
    if-eqz v2, :cond_20

    #@31
    :cond_31
    move v0, v1

    #@32
    goto :goto_20
.end method

.method public setStackedBackground(Landroid/graphics/drawable/Drawable;)V
    .registers 6
    .parameter "bg"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 94
    iget-object v2, p0, Lcom/android/internal/widget/ActionBarContainer;->mStackedBackground:Landroid/graphics/drawable/Drawable;

    #@4
    if-eqz v2, :cond_11

    #@6
    .line 95
    iget-object v2, p0, Lcom/android/internal/widget/ActionBarContainer;->mStackedBackground:Landroid/graphics/drawable/Drawable;

    #@8
    const/4 v3, 0x0

    #@9
    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@c
    .line 96
    iget-object v2, p0, Lcom/android/internal/widget/ActionBarContainer;->mStackedBackground:Landroid/graphics/drawable/Drawable;

    #@e
    invoke-virtual {p0, v2}, Lcom/android/internal/widget/ActionBarContainer;->unscheduleDrawable(Landroid/graphics/drawable/Drawable;)V

    #@11
    .line 98
    :cond_11
    iput-object p1, p0, Lcom/android/internal/widget/ActionBarContainer;->mStackedBackground:Landroid/graphics/drawable/Drawable;

    #@13
    .line 99
    if-eqz p1, :cond_18

    #@15
    .line 100
    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@18
    .line 102
    :cond_18
    iget-boolean v2, p0, Lcom/android/internal/widget/ActionBarContainer;->mIsSplit:Z

    #@1a
    if-eqz v2, :cond_29

    #@1c
    iget-object v2, p0, Lcom/android/internal/widget/ActionBarContainer;->mSplitBackground:Landroid/graphics/drawable/Drawable;

    #@1e
    if-nez v2, :cond_27

    #@20
    :cond_20
    :goto_20
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/ActionBarContainer;->setWillNotDraw(Z)V

    #@23
    .line 104
    invoke-virtual {p0}, Lcom/android/internal/widget/ActionBarContainer;->invalidate()V

    #@26
    .line 105
    return-void

    #@27
    :cond_27
    move v0, v1

    #@28
    .line 102
    goto :goto_20

    #@29
    :cond_29
    iget-object v2, p0, Lcom/android/internal/widget/ActionBarContainer;->mBackground:Landroid/graphics/drawable/Drawable;

    #@2b
    if-nez v2, :cond_31

    #@2d
    iget-object v2, p0, Lcom/android/internal/widget/ActionBarContainer;->mStackedBackground:Landroid/graphics/drawable/Drawable;

    #@2f
    if-eqz v2, :cond_20

    #@31
    :cond_31
    move v0, v1

    #@32
    goto :goto_20
.end method

.method public setTabContainer(Lcom/android/internal/widget/ScrollingTabContainerView;)V
    .registers 4
    .parameter "tabView"

    #@0
    .prologue
    .line 217
    iget-object v1, p0, Lcom/android/internal/widget/ActionBarContainer;->mTabContainer:Landroid/view/View;

    #@2
    if-eqz v1, :cond_9

    #@4
    .line 218
    iget-object v1, p0, Lcom/android/internal/widget/ActionBarContainer;->mTabContainer:Landroid/view/View;

    #@6
    invoke-virtual {p0, v1}, Lcom/android/internal/widget/ActionBarContainer;->removeView(Landroid/view/View;)V

    #@9
    .line 220
    :cond_9
    iput-object p1, p0, Lcom/android/internal/widget/ActionBarContainer;->mTabContainer:Landroid/view/View;

    #@b
    .line 221
    if-eqz p1, :cond_1e

    #@d
    .line 222
    invoke-virtual {p0, p1}, Lcom/android/internal/widget/ActionBarContainer;->addView(Landroid/view/View;)V

    #@10
    .line 223
    invoke-virtual {p1}, Lcom/android/internal/widget/ScrollingTabContainerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@13
    move-result-object v0

    #@14
    .line 224
    .local v0, lp:Landroid/view/ViewGroup$LayoutParams;
    const/4 v1, -0x1

    #@15
    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@17
    .line 225
    const/4 v1, -0x2

    #@18
    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@1a
    .line 226
    const/4 v1, 0x0

    #@1b
    invoke-virtual {p1, v1}, Lcom/android/internal/widget/ScrollingTabContainerView;->setAllowCollapse(Z)V

    #@1e
    .line 228
    .end local v0           #lp:Landroid/view/ViewGroup$LayoutParams;
    :cond_1e
    return-void
.end method

.method public setTransitioning(Z)V
    .registers 3
    .parameter "isTransitioning"

    #@0
    .prologue
    .line 190
    iput-boolean p1, p0, Lcom/android/internal/widget/ActionBarContainer;->mIsTransitioning:Z

    #@2
    .line 191
    if-eqz p1, :cond_a

    #@4
    const/high16 v0, 0x6

    #@6
    :goto_6
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/ActionBarContainer;->setDescendantFocusability(I)V

    #@9
    .line 193
    return-void

    #@a
    .line 191
    :cond_a
    const/high16 v0, 0x4

    #@c
    goto :goto_6
.end method

.method public setVisibility(I)V
    .registers 5
    .parameter "visibility"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 123
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    #@4
    .line 124
    if-nez p1, :cond_23

    #@6
    const/4 v0, 0x1

    #@7
    .line 125
    .local v0, isVisible:Z
    :goto_7
    iget-object v2, p0, Lcom/android/internal/widget/ActionBarContainer;->mBackground:Landroid/graphics/drawable/Drawable;

    #@9
    if-eqz v2, :cond_10

    #@b
    iget-object v2, p0, Lcom/android/internal/widget/ActionBarContainer;->mBackground:Landroid/graphics/drawable/Drawable;

    #@d
    invoke-virtual {v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    #@10
    .line 126
    :cond_10
    iget-object v2, p0, Lcom/android/internal/widget/ActionBarContainer;->mStackedBackground:Landroid/graphics/drawable/Drawable;

    #@12
    if-eqz v2, :cond_19

    #@14
    iget-object v2, p0, Lcom/android/internal/widget/ActionBarContainer;->mStackedBackground:Landroid/graphics/drawable/Drawable;

    #@16
    invoke-virtual {v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    #@19
    .line 127
    :cond_19
    iget-object v2, p0, Lcom/android/internal/widget/ActionBarContainer;->mSplitBackground:Landroid/graphics/drawable/Drawable;

    #@1b
    if-eqz v2, :cond_22

    #@1d
    iget-object v2, p0, Lcom/android/internal/widget/ActionBarContainer;->mSplitBackground:Landroid/graphics/drawable/Drawable;

    #@1f
    invoke-virtual {v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    #@22
    .line 128
    :cond_22
    return-void

    #@23
    .end local v0           #isVisible:Z
    :cond_23
    move v0, v1

    #@24
    .line 124
    goto :goto_7
.end method

.method public startActionModeForChild(Landroid/view/View;Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;
    .registers 4
    .parameter "child"
    .parameter "callback"

    #@0
    .prologue
    .line 255
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .registers 3
    .parameter "who"

    #@0
    .prologue
    .line 132
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarContainer;->mBackground:Landroid/graphics/drawable/Drawable;

    #@2
    if-ne p1, v0, :cond_8

    #@4
    iget-boolean v0, p0, Lcom/android/internal/widget/ActionBarContainer;->mIsSplit:Z

    #@6
    if-eqz v0, :cond_1e

    #@8
    :cond_8
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarContainer;->mStackedBackground:Landroid/graphics/drawable/Drawable;

    #@a
    if-ne p1, v0, :cond_10

    #@c
    iget-boolean v0, p0, Lcom/android/internal/widget/ActionBarContainer;->mIsStacked:Z

    #@e
    if-nez v0, :cond_1e

    #@10
    :cond_10
    iget-object v0, p0, Lcom/android/internal/widget/ActionBarContainer;->mSplitBackground:Landroid/graphics/drawable/Drawable;

    #@12
    if-ne p1, v0, :cond_18

    #@14
    iget-boolean v0, p0, Lcom/android/internal/widget/ActionBarContainer;->mIsSplit:Z

    #@16
    if-nez v0, :cond_1e

    #@18
    :cond_18
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    #@1b
    move-result v0

    #@1c
    if-eqz v0, :cond_20

    #@1e
    :cond_1e
    const/4 v0, 0x1

    #@1f
    :goto_1f
    return v0

    #@20
    :cond_20
    const/4 v0, 0x0

    #@21
    goto :goto_1f
.end method
