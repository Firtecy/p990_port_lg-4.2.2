.class final Lcom/android/internal/widget/multiwaveview/Ease$Quint$3;
.super Ljava/lang/Object;
.source "Ease.java"

# interfaces
.implements Landroid/animation/TimeInterpolator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/widget/multiwaveview/Ease$Quint;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 105
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .registers 6
    .parameter "input"

    #@0
    .prologue
    const/high16 v3, 0x4000

    #@2
    const/4 v2, 0x0

    #@3
    const/high16 v1, 0x3f00

    #@5
    .line 107
    div-float/2addr p1, v1

    #@6
    const/high16 v0, 0x3f80

    #@8
    cmpg-float v0, p1, v0

    #@a
    if-gez v0, :cond_14

    #@c
    mul-float v0, v1, p1

    #@e
    mul-float/2addr v0, p1

    #@f
    mul-float/2addr v0, p1

    #@10
    mul-float/2addr v0, p1

    #@11
    mul-float/2addr v0, p1

    #@12
    add-float/2addr v0, v2

    #@13
    :goto_13
    return v0

    #@14
    :cond_14
    sub-float/2addr p1, v3

    #@15
    mul-float v0, p1, p1

    #@17
    mul-float/2addr v0, p1

    #@18
    mul-float/2addr v0, p1

    #@19
    mul-float/2addr v0, p1

    #@1a
    add-float/2addr v0, v3

    #@1b
    mul-float/2addr v0, v1

    #@1c
    add-float/2addr v0, v2

    #@1d
    goto :goto_13
.end method
