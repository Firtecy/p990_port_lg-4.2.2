.class public Lcom/android/internal/widget/FaceUnlockView;
.super Landroid/widget/RelativeLayout;
.source "FaceUnlockView.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "FaceUnlockView"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 29
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Lcom/android/internal/widget/FaceUnlockView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@3
    .line 34
    return-void
.end method

.method private resolveMeasured(II)I
    .registers 6
    .parameter "measureSpec"
    .parameter "desired"

    #@0
    .prologue
    .line 38
    const/4 v0, 0x0

    #@1
    .line 39
    .local v0, result:I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    #@4
    move-result v1

    #@5
    .line 40
    .local v1, specSize:I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    #@8
    move-result v2

    #@9
    sparse-switch v2, :sswitch_data_16

    #@c
    .line 49
    move v0, v1

    #@d
    .line 51
    :goto_d
    return v0

    #@e
    .line 42
    :sswitch_e
    move v0, p2

    #@f
    .line 43
    goto :goto_d

    #@10
    .line 45
    :sswitch_10
    invoke-static {v1, p2}, Ljava/lang/Math;->max(II)I

    #@13
    move-result v0

    #@14
    .line 46
    goto :goto_d

    #@15
    .line 40
    nop

    #@16
    :sswitch_data_16
    .sparse-switch
        -0x80000000 -> :sswitch_10
        0x0 -> :sswitch_e
    .end sparse-switch
.end method


# virtual methods
.method protected onMeasure(II)V
    .registers 11
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    #@0
    .prologue
    const/high16 v7, -0x8000

    #@2
    .line 56
    invoke-virtual {p0}, Lcom/android/internal/widget/FaceUnlockView;->getSuggestedMinimumWidth()I

    #@5
    move-result v2

    #@6
    .line 57
    .local v2, minimumWidth:I
    invoke-virtual {p0}, Lcom/android/internal/widget/FaceUnlockView;->getSuggestedMinimumHeight()I

    #@9
    move-result v1

    #@a
    .line 58
    .local v1, minimumHeight:I
    invoke-direct {p0, p1, v2}, Lcom/android/internal/widget/FaceUnlockView;->resolveMeasured(II)I

    #@d
    move-result v6

    #@e
    .line 59
    .local v6, viewWidth:I
    invoke-direct {p0, p2, v1}, Lcom/android/internal/widget/FaceUnlockView;->resolveMeasured(II)I

    #@11
    move-result v5

    #@12
    .line 61
    .local v5, viewHeight:I
    invoke-static {v6, v5}, Ljava/lang/Math;->min(II)I

    #@15
    move-result v0

    #@16
    .line 62
    .local v0, chosenSize:I
    invoke-static {v0, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@19
    move-result v4

    #@1a
    .line 64
    .local v4, newWidthMeasureSpec:I
    invoke-static {v0, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    #@1d
    move-result v3

    #@1e
    .line 67
    .local v3, newHeightMeasureSpec:I
    invoke-super {p0, v4, v3}, Landroid/widget/RelativeLayout;->onMeasure(II)V

    #@21
    .line 68
    return-void
.end method
