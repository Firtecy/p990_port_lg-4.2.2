.class final Lcom/android/internal/widget/multiwaveview/Ease$Sine$1;
.super Ljava/lang/Object;
.source "Ease.java"

# interfaces
.implements Landroid/animation/TimeInterpolator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/widget/multiwaveview/Ease$Sine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 115
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .registers 8
    .parameter "input"

    #@0
    .prologue
    const/high16 v5, 0x3f80

    #@2
    .line 117
    const/high16 v0, -0x4080

    #@4
    div-float v1, p1, v5

    #@6
    float-to-double v1, v1

    #@7
    const-wide v3, 0x3ff921fb54442d18L

    #@c
    mul-double/2addr v1, v3

    #@d
    invoke-static {v1, v2}, Ljava/lang/Math;->cos(D)D

    #@10
    move-result-wide v1

    #@11
    double-to-float v1, v1

    #@12
    mul-float/2addr v0, v1

    #@13
    add-float/2addr v0, v5

    #@14
    const/4 v1, 0x0

    #@15
    add-float/2addr v0, v1

    #@16
    return v0
.end method
