.class public abstract Lcom/lge/uicc/ISmartCardInfo$Stub;
.super Landroid/os/Binder;
.source "ISmartCardInfo.java"

# interfaces
.implements Lcom/lge/uicc/ISmartCardInfo;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/uicc/ISmartCardInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/uicc/ISmartCardInfo$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.lge.uicc.ISmartCardInfo"

.field static final TRANSACTION_smartCardGetATR:I = 0x2

.field static final TRANSACTION_smartCardTransmit:I = 0x1


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 15
    const-string v0, "com.lge.uicc.ISmartCardInfo"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/lge/uicc/ISmartCardInfo$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/lge/uicc/ISmartCardInfo;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 23
    if-nez p0, :cond_4

    #@2
    .line 24
    const/4 v0, 0x0

    #@3
    .line 30
    :goto_3
    return-object v0

    #@4
    .line 26
    :cond_4
    const-string v1, "com.lge.uicc.ISmartCardInfo"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 27
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/lge/uicc/ISmartCardInfo;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 28
    check-cast v0, Lcom/lge/uicc/ISmartCardInfo;

    #@12
    goto :goto_3

    #@13
    .line 30
    :cond_13
    new-instance v0, Lcom/lge/uicc/ISmartCardInfo$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/lge/uicc/ISmartCardInfo$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 10
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    .line 38
    sparse-switch p1, :sswitch_data_46

    #@5
    .line 76
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v2

    #@9
    :goto_9
    return v2

    #@a
    .line 42
    :sswitch_a
    const-string v3, "com.lge.uicc.ISmartCardInfo"

    #@c
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 47
    :sswitch_10
    const-string v3, "com.lge.uicc.ISmartCardInfo"

    #@12
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@18
    move-result-object v0

    #@19
    .line 50
    .local v0, _arg0:[B
    invoke-virtual {p0, v0}, Lcom/lge/uicc/ISmartCardInfo$Stub;->smartCardTransmit([B)Lcom/android/internal/telephony/uicc/SmartCardResult;

    #@1c
    move-result-object v1

    #@1d
    .line 51
    .local v1, _result:Lcom/android/internal/telephony/uicc/SmartCardResult;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@20
    .line 52
    if-eqz v1, :cond_29

    #@22
    .line 53
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@25
    .line 54
    invoke-virtual {v1, p3, v2}, Lcom/android/internal/telephony/uicc/SmartCardResult;->writeToParcel(Landroid/os/Parcel;I)V

    #@28
    goto :goto_9

    #@29
    .line 57
    :cond_29
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@2c
    goto :goto_9

    #@2d
    .line 63
    .end local v0           #_arg0:[B
    .end local v1           #_result:Lcom/android/internal/telephony/uicc/SmartCardResult;
    :sswitch_2d
    const-string v3, "com.lge.uicc.ISmartCardInfo"

    #@2f
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@32
    .line 64
    invoke-virtual {p0}, Lcom/lge/uicc/ISmartCardInfo$Stub;->smartCardGetATR()Lcom/android/internal/telephony/uicc/SmartCardResult;

    #@35
    move-result-object v1

    #@36
    .line 65
    .restart local v1       #_result:Lcom/android/internal/telephony/uicc/SmartCardResult;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@39
    .line 66
    if-eqz v1, :cond_42

    #@3b
    .line 67
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@3e
    .line 68
    invoke-virtual {v1, p3, v2}, Lcom/android/internal/telephony/uicc/SmartCardResult;->writeToParcel(Landroid/os/Parcel;I)V

    #@41
    goto :goto_9

    #@42
    .line 71
    :cond_42
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@45
    goto :goto_9

    #@46
    .line 38
    :sswitch_data_46
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_2d
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
