.class public final Lcom/lge/uicc/SmartCardWrapper;
.super Ljava/lang/Object;
.source "SmartCardWrapper.java"


# static fields
.field private static final CHANNEL_BYTES:I = 0x3

.field public static final SMARTCARD_IO_ALREADY_CONNECTED:I = -0x2

.field public static final SMARTCARD_IO_CARD_NOT_EXIST:I = -0x7

.field public static final SMARTCARD_IO_ERROR_ATR_BUFFER:I = -0x6

.field public static final SMARTCARD_IO_ERROR_NOT_CONNECT:I = -0x3

.field public static final SMARTCARD_IO_ERROR_OPEN_CHANNEL:I = -0x1

.field public static final SMARTCARD_IO_ERROR_RESPONSE_BUFFER:I = -0x5

.field public static final SMARTCARD_IO_ERROR_TRANSMIT_BUFFER:I = -0x4

.field public static final SMARTCARD_IO_ERROR_UNKNOWN:I = -0x8

.field public static final SMARTCARD_IO_SUCCESS:I = 0x0

.field private static final TAG:Ljava/lang/String; = "LGE_USIM"

.field private static sInstance:Lcom/lge/uicc/SmartCardWrapper;


# instance fields
.field private channel:I

.field private mPhone:Lcom/android/internal/telephony/gsm/GSMPhone;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 72
    new-instance v0, Lcom/lge/uicc/SmartCardWrapper;

    #@2
    invoke-direct {v0}, Lcom/lge/uicc/SmartCardWrapper;-><init>()V

    #@5
    sput-object v0, Lcom/lge/uicc/SmartCardWrapper;->sInstance:Lcom/lge/uicc/SmartCardWrapper;

    #@7
    return-void
.end method

.method private constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 70
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 51
    const/4 v0, -0x1

    #@4
    iput v0, p0, Lcom/lge/uicc/SmartCardWrapper;->channel:I

    #@6
    .line 70
    return-void
.end method

.method public static getInstance()Lcom/lge/uicc/SmartCardWrapper;
    .registers 1

    #@0
    .prologue
    .line 76
    sget-object v0, Lcom/lge/uicc/SmartCardWrapper;->sInstance:Lcom/lge/uicc/SmartCardWrapper;

    #@2
    return-object v0
.end method


# virtual methods
.method public connect()I
    .registers 9

    #@0
    .prologue
    const/4 v7, 0x5

    #@1
    const/4 v4, -0x1

    #@2
    .line 81
    const-string v5, "LGE_USIM"

    #@4
    const-string v6, "Smartcard connect()"

    #@6
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 82
    new-array v0, v7, [B

    #@b
    fill-array-data v0, :array_7e

    #@e
    .line 83
    .local v0, command:[B
    const/4 v5, 0x3

    #@f
    new-array v1, v5, [B

    #@11
    .line 85
    .local v1, response:[B
    const/4 v3, 0x0

    #@12
    .line 88
    .local v3, searchSW:Ljava/lang/String;
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@15
    move-result-object v5

    #@16
    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->getSimState()I

    #@19
    move-result v5

    #@1a
    if-eq v5, v7, :cond_25

    #@1c
    .line 90
    const-string v4, "LGE_USIM"

    #@1e
    const-string v5, "[connect] Usim Card Not Inserted!!"

    #@20
    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 91
    const/4 v4, -0x7

    #@24
    .line 113
    :cond_24
    :goto_24
    return v4

    #@25
    .line 95
    :cond_25
    iget v5, p0, Lcom/lge/uicc/SmartCardWrapper;->channel:I

    #@27
    if-eq v5, v4, :cond_32

    #@29
    .line 97
    const-string v4, "LGE_USIM"

    #@2b
    const-string v5, "[connect] Logical Channel is already open!!"

    #@2d
    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    .line 98
    const/4 v4, -0x2

    #@31
    goto :goto_24

    #@32
    .line 101
    :cond_32
    invoke-virtual {p0, v0, v1}, Lcom/lge/uicc/SmartCardWrapper;->transmit([B[B)I

    #@35
    move-result v2

    #@36
    .line 102
    .local v2, response_length:I
    invoke-static {v1}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@39
    move-result-object v3

    #@3a
    .line 103
    const-string v5, "LGE_USIM"

    #@3c
    new-instance v6, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v7, "[connect] response : "

    #@43
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v6

    #@47
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v6

    #@4b
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v6

    #@4f
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@52
    .line 105
    const-string v5, "9000"

    #@54
    invoke-virtual {v3, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@57
    move-result v5

    #@58
    if-eqz v5, :cond_24

    #@5a
    .line 110
    const/4 v4, 0x0

    #@5b
    aget-byte v4, v1, v4

    #@5d
    and-int/lit16 v4, v4, 0xff

    #@5f
    iput v4, p0, Lcom/lge/uicc/SmartCardWrapper;->channel:I

    #@61
    .line 111
    const-string v4, "LGE_USIM"

    #@63
    new-instance v5, Ljava/lang/StringBuilder;

    #@65
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@68
    const-string v6, "open channel : "

    #@6a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v5

    #@6e
    iget v6, p0, Lcom/lge/uicc/SmartCardWrapper;->channel:I

    #@70
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@73
    move-result-object v5

    #@74
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@77
    move-result-object v5

    #@78
    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7b
    .line 113
    iget v4, p0, Lcom/lge/uicc/SmartCardWrapper;->channel:I

    #@7d
    goto :goto_24

    #@7e
    .line 82
    :array_7e
    .array-data 0x1
        0x0t
        0x70t
        0x0t
        0x0t
        0x1t
    .end array-data
.end method

.method public disconnect()I
    .registers 10

    #@0
    .prologue
    const/4 v7, 0x2

    #@1
    const/4 v8, -0x1

    #@2
    const/4 v4, 0x0

    #@3
    .line 120
    const-string v5, "LGE_USIM"

    #@5
    const-string v6, "Smartcard disconnect()"

    #@7
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@a
    .line 121
    const/4 v5, 0x4

    #@b
    new-array v0, v5, [B

    #@d
    aput-byte v4, v0, v4

    #@f
    const/4 v5, 0x1

    #@10
    const/16 v6, 0x70

    #@12
    aput-byte v6, v0, v5

    #@14
    const/16 v5, -0x80

    #@16
    aput-byte v5, v0, v7

    #@18
    const/4 v5, 0x3

    #@19
    iget v6, p0, Lcom/lge/uicc/SmartCardWrapper;->channel:I

    #@1b
    and-int/lit16 v6, v6, 0xff

    #@1d
    int-to-byte v6, v6

    #@1e
    aput-byte v6, v0, v5

    #@20
    .line 122
    .local v0, command:[B
    new-array v1, v7, [B

    #@22
    .line 124
    .local v1, response:[B
    const/4 v3, 0x0

    #@23
    .line 127
    .local v3, searchSW:Ljava/lang/String;
    iget v5, p0, Lcom/lge/uicc/SmartCardWrapper;->channel:I

    #@25
    if-ne v5, v8, :cond_29

    #@27
    .line 129
    const/4 v4, -0x3

    #@28
    .line 142
    :goto_28
    return v4

    #@29
    .line 132
    :cond_29
    invoke-virtual {p0, v0, v1}, Lcom/lge/uicc/SmartCardWrapper;->transmit([B[B)I

    #@2c
    move-result v2

    #@2d
    .line 133
    .local v2, response_length:I
    invoke-static {v1}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@30
    move-result-object v3

    #@31
    .line 134
    const-string v5, "LGE_USIM"

    #@33
    new-instance v6, Ljava/lang/StringBuilder;

    #@35
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    const-string v7, "[disconnect] response : "

    #@3a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v6

    #@3e
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v6

    #@42
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v6

    #@46
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@49
    .line 136
    const-string v5, "9000"

    #@4b
    invoke-virtual {v3, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@4e
    move-result v5

    #@4f
    if-nez v5, :cond_53

    #@51
    .line 138
    const/4 v4, -0x8

    #@52
    goto :goto_28

    #@53
    .line 141
    :cond_53
    iput v8, p0, Lcom/lge/uicc/SmartCardWrapper;->channel:I

    #@55
    goto :goto_28
.end method

.method public getATR([B)I
    .registers 11
    .parameter "atr"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    const/4 v4, -0x8

    #@2
    .line 203
    const-string v5, "LGE_USIM"

    #@4
    const-string v6, "Smartcard getATR()"

    #@6
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 204
    const/4 v3, 0x0

    #@a
    .line 207
    .local v3, tmpdata:Lcom/android/internal/telephony/uicc/SmartCardResult;
    const-string v5, "ISmartCardInfo"

    #@c
    invoke-static {v5}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@f
    move-result-object v0

    #@10
    .line 210
    .local v0, b:Landroid/os/IBinder;
    if-nez v0, :cond_1a

    #@12
    .line 211
    new-instance v4, Ljava/lang/RuntimeException;

    #@14
    const-string v5, "ISmartCardInfo binder not available!"

    #@16
    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@19
    throw v4

    #@1a
    .line 213
    :cond_1a
    invoke-static {v0}, Lcom/lge/uicc/ISmartCardInfo$Stub;->asInterface(Landroid/os/IBinder;)Lcom/lge/uicc/ISmartCardInfo;

    #@1d
    move-result-object v2

    #@1e
    .line 217
    .local v2, mSCInfo:Lcom/lge/uicc/ISmartCardInfo;
    :try_start_1e
    invoke-interface {v2}, Lcom/lge/uicc/ISmartCardInfo;->smartCardGetATR()Lcom/android/internal/telephony/uicc/SmartCardResult;

    #@21
    move-result-object v3

    #@22
    .line 218
    const-string v5, "LGE_USIM"

    #@24
    new-instance v6, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v7, "[getATR] tmpdata.ret : "

    #@2b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v6

    #@2f
    iget v7, v3, Lcom/android/internal/telephony/uicc/SmartCardResult;->ret:I

    #@31
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@34
    move-result-object v6

    #@35
    const-string v7, " tmpdata.data : "

    #@37
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v6

    #@3b
    iget-object v7, v3, Lcom/android/internal/telephony/uicc/SmartCardResult;->data:[B

    #@3d
    invoke-static {v7}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@40
    move-result-object v7

    #@41
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v6

    #@45
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v6

    #@49
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4c
    .catch Landroid/os/RemoteException; {:try_start_1e .. :try_end_4c} :catch_4f

    #@4c
    .line 225
    :goto_4c
    if-nez v3, :cond_58

    #@4e
    .line 243
    :cond_4e
    :goto_4e
    return v4

    #@4f
    .line 220
    :catch_4f
    move-exception v1

    #@50
    .line 222
    .local v1, e:Landroid/os/RemoteException;
    const-string v5, "LGE_USIM"

    #@52
    const-string v6, "Call getATR Failed"

    #@54
    invoke-static {v5, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@57
    goto :goto_4c

    #@58
    .line 230
    .end local v1           #e:Landroid/os/RemoteException;
    :cond_58
    iget v5, v3, Lcom/android/internal/telephony/uicc/SmartCardResult;->ret:I

    #@5a
    if-nez v5, :cond_4e

    #@5c
    .line 232
    iget v4, v3, Lcom/android/internal/telephony/uicc/SmartCardResult;->data_length:I

    #@5e
    array-length v5, p1

    #@5f
    if-le v4, v5, :cond_6a

    #@61
    .line 234
    const-string v4, "LGE_USIM"

    #@63
    const-string v5, "[getATR] SMARTCARD_IO_ERROR_ATR_BUFFER!!"

    #@65
    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@68
    .line 235
    const/4 v4, -0x6

    #@69
    goto :goto_4e

    #@6a
    .line 237
    :cond_6a
    iget-object v4, v3, Lcom/android/internal/telephony/uicc/SmartCardResult;->data:[B

    #@6c
    iget v5, v3, Lcom/android/internal/telephony/uicc/SmartCardResult;->data_length:I

    #@6e
    invoke-static {v4, v8, p1, v8, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@71
    .line 238
    const-string v4, "LGE_USIM"

    #@73
    new-instance v5, Ljava/lang/StringBuilder;

    #@75
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@78
    const-string v6, "[getATR] atr : "

    #@7a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v5

    #@7e
    invoke-static {p1}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@81
    move-result-object v6

    #@82
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v5

    #@86
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@89
    move-result-object v5

    #@8a
    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@8d
    .line 239
    iget v4, v3, Lcom/android/internal/telephony/uicc/SmartCardResult;->data_length:I

    #@8f
    goto :goto_4e
.end method

.method public getChannel()I
    .registers 3

    #@0
    .prologue
    .line 250
    const-string v0, "LGE_USIM"

    #@2
    const-string v1, "Smartcard getChannel()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 252
    iget v0, p0, Lcom/lge/uicc/SmartCardWrapper;->channel:I

    #@9
    if-gez v0, :cond_d

    #@b
    .line 254
    const/4 v0, -0x3

    #@c
    .line 256
    :goto_c
    return v0

    #@d
    :cond_d
    iget v0, p0, Lcom/lge/uicc/SmartCardWrapper;->channel:I

    #@f
    goto :goto_c
.end method

.method public transmit([B[B)I
    .registers 12
    .parameter "command"
    .parameter "response"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    const/4 v4, -0x8

    #@2
    .line 148
    const-string v5, "LGE_USIM"

    #@4
    const-string v6, "Smartcard transmit()"

    #@6
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 150
    const-string v5, "ISmartCardInfo"

    #@b
    invoke-static {v5}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@e
    move-result-object v0

    #@f
    .line 153
    .local v0, b:Landroid/os/IBinder;
    if-nez v0, :cond_19

    #@11
    .line 154
    new-instance v4, Ljava/lang/RuntimeException;

    #@13
    const-string v5, "ISmartCardInfo binder not available!"

    #@15
    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@18
    throw v4

    #@19
    .line 156
    :cond_19
    invoke-static {v0}, Lcom/lge/uicc/ISmartCardInfo$Stub;->asInterface(Landroid/os/IBinder;)Lcom/lge/uicc/ISmartCardInfo;

    #@1c
    move-result-object v2

    #@1d
    .line 158
    .local v2, mSCInfo:Lcom/lge/uicc/ISmartCardInfo;
    const/4 v3, 0x0

    #@1e
    .line 160
    .local v3, tmpdata:Lcom/android/internal/telephony/uicc/SmartCardResult;
    if-nez p1, :cond_22

    #@20
    .line 162
    const/4 v4, -0x4

    #@21
    .line 196
    :cond_21
    :goto_21
    return v4

    #@22
    .line 165
    :cond_22
    const-string v5, "LGE_USIM"

    #@24
    new-instance v6, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v7, "[transmit] command : "

    #@2b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v6

    #@2f
    invoke-static {p1}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@32
    move-result-object v7

    #@33
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v6

    #@37
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v6

    #@3b
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    .line 169
    :try_start_3e
    invoke-interface {v2, p1}, Lcom/lge/uicc/ISmartCardInfo;->smartCardTransmit([B)Lcom/android/internal/telephony/uicc/SmartCardResult;

    #@41
    move-result-object v3

    #@42
    .line 170
    const-string v5, "LGE_USIM"

    #@44
    new-instance v6, Ljava/lang/StringBuilder;

    #@46
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@49
    const-string v7, "[transmit] tmpdata.ret : "

    #@4b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v6

    #@4f
    iget v7, v3, Lcom/android/internal/telephony/uicc/SmartCardResult;->ret:I

    #@51
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@54
    move-result-object v6

    #@55
    const-string v7, " tmpdata.data : "

    #@57
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v6

    #@5b
    iget-object v7, v3, Lcom/android/internal/telephony/uicc/SmartCardResult;->data:[B

    #@5d
    invoke-static {v7}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@60
    move-result-object v7

    #@61
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v6

    #@65
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@68
    move-result-object v6

    #@69
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6c
    .catch Landroid/os/RemoteException; {:try_start_3e .. :try_end_6c} :catch_80

    #@6c
    .line 177
    :goto_6c
    if-eqz v3, :cond_21

    #@6e
    .line 182
    iget v5, v3, Lcom/android/internal/telephony/uicc/SmartCardResult;->ret:I

    #@70
    if-nez v5, :cond_b0

    #@72
    .line 184
    iget v4, v3, Lcom/android/internal/telephony/uicc/SmartCardResult;->data_length:I

    #@74
    array-length v5, p2

    #@75
    if-le v4, v5, :cond_89

    #@77
    .line 186
    const-string v4, "LGE_USIM"

    #@79
    const-string v5, "[transmit] SMARTCARD_IO_ERROR_RESPONSE_BUFFER!!"

    #@7b
    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7e
    .line 187
    const/4 v4, -0x5

    #@7f
    goto :goto_21

    #@80
    .line 172
    :catch_80
    move-exception v1

    #@81
    .line 174
    .local v1, e:Landroid/os/RemoteException;
    const-string v5, "LGE_USIM"

    #@83
    const-string v6, "Call smartCardTransmit Failed"

    #@85
    invoke-static {v5, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@88
    goto :goto_6c

    #@89
    .line 189
    .end local v1           #e:Landroid/os/RemoteException;
    :cond_89
    iget-object v4, v3, Lcom/android/internal/telephony/uicc/SmartCardResult;->data:[B

    #@8b
    iget v5, v3, Lcom/android/internal/telephony/uicc/SmartCardResult;->data_length:I

    #@8d
    invoke-static {v4, v8, p2, v8, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@90
    .line 190
    const-string v4, "LGE_USIM"

    #@92
    new-instance v5, Ljava/lang/StringBuilder;

    #@94
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@97
    const-string v6, "[transmit] response : "

    #@99
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v5

    #@9d
    invoke-static {p2}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@a0
    move-result-object v6

    #@a1
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v5

    #@a5
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a8
    move-result-object v5

    #@a9
    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@ac
    .line 191
    iget v4, v3, Lcom/android/internal/telephony/uicc/SmartCardResult;->data_length:I

    #@ae
    goto/16 :goto_21

    #@b0
    .line 195
    :cond_b0
    const-string v5, "LGE_USIM"

    #@b2
    const-string v6, "[transmit] transmit error"

    #@b4
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@b7
    goto/16 :goto_21
.end method
