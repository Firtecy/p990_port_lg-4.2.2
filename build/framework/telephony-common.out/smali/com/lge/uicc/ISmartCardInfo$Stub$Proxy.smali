.class Lcom/lge/uicc/ISmartCardInfo$Stub$Proxy;
.super Ljava/lang/Object;
.source "ISmartCardInfo.java"

# interfaces
.implements Lcom/lge/uicc/ISmartCardInfo;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/uicc/ISmartCardInfo$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 82
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 83
    iput-object p1, p0, Lcom/lge/uicc/ISmartCardInfo$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 84
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 87
    iget-object v0, p0, Lcom/lge/uicc/ISmartCardInfo$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 91
    const-string v0, "com.lge.uicc.ISmartCardInfo"

    #@2
    return-object v0
.end method

.method public smartCardGetATR()Lcom/android/internal/telephony/uicc/SmartCardResult;
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 118
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 119
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 122
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.lge.uicc.ISmartCardInfo"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 123
    iget-object v3, p0, Lcom/lge/uicc/ISmartCardInfo$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v4, 0x2

    #@10
    const/4 v5, 0x0

    #@11
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 124
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@17
    .line 125
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@1a
    move-result v3

    #@1b
    if-eqz v3, :cond_2c

    #@1d
    .line 126
    sget-object v3, Lcom/android/internal/telephony/uicc/SmartCardResult;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1f
    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@22
    move-result-object v2

    #@23
    check-cast v2, Lcom/android/internal/telephony/uicc/SmartCardResult;
    :try_end_25
    .catchall {:try_start_8 .. :try_end_25} :catchall_2e

    #@25
    .line 133
    .local v2, _result:Lcom/android/internal/telephony/uicc/SmartCardResult;
    :goto_25
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 134
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 136
    return-object v2

    #@2c
    .line 129
    .end local v2           #_result:Lcom/android/internal/telephony/uicc/SmartCardResult;
    :cond_2c
    const/4 v2, 0x0

    #@2d
    .restart local v2       #_result:Lcom/android/internal/telephony/uicc/SmartCardResult;
    goto :goto_25

    #@2e
    .line 133
    .end local v2           #_result:Lcom/android/internal/telephony/uicc/SmartCardResult;
    :catchall_2e
    move-exception v3

    #@2f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@32
    .line 134
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@35
    .line 133
    throw v3
.end method

.method public smartCardTransmit([B)Lcom/android/internal/telephony/uicc/SmartCardResult;
    .registers 8
    .parameter "command"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 95
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 96
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 99
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.lge.uicc.ISmartCardInfo"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 100
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeByteArray([B)V

    #@10
    .line 101
    iget-object v3, p0, Lcom/lge/uicc/ISmartCardInfo$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v4, 0x1

    #@13
    const/4 v5, 0x0

    #@14
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@17
    .line 102
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1a
    .line 103
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@1d
    move-result v3

    #@1e
    if-eqz v3, :cond_2f

    #@20
    .line 104
    sget-object v3, Lcom/android/internal/telephony/uicc/SmartCardResult;->CREATOR:Landroid/os/Parcelable$Creator;

    #@22
    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@25
    move-result-object v2

    #@26
    check-cast v2, Lcom/android/internal/telephony/uicc/SmartCardResult;
    :try_end_28
    .catchall {:try_start_8 .. :try_end_28} :catchall_31

    #@28
    .line 111
    .local v2, _result:Lcom/android/internal/telephony/uicc/SmartCardResult;
    :goto_28
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 112
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 114
    return-object v2

    #@2f
    .line 107
    .end local v2           #_result:Lcom/android/internal/telephony/uicc/SmartCardResult;
    :cond_2f
    const/4 v2, 0x0

    #@30
    .restart local v2       #_result:Lcom/android/internal/telephony/uicc/SmartCardResult;
    goto :goto_28

    #@31
    .line 111
    .end local v2           #_result:Lcom/android/internal/telephony/uicc/SmartCardResult;
    :catchall_31
    move-exception v3

    #@32
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@35
    .line 112
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@38
    .line 111
    throw v3
.end method
