.class public Lcom/lge/ciq/VoiceCallCIQ;
.super Ljava/lang/Object;
.source "VoiceCallCIQ.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ciq/VoiceCallCIQ$CSCallHolder;
    }
.end annotation


# static fields
.field private static final ACTION_DDM_CSC:Ljava/lang/String; = "diagandroid.ddm.csc"

.field private static final DEBUG:Z = false

.field private static final EXTRA_DDM_CSC_ATTR:Ljava/lang/String; = "DdmCscAttr"

.field private static final EXTRA_DDM_CSC_CID:Ljava/lang/String; = "DdmCscCID"

.field private static final EXTRA_DDM_CSC_ERROR:Ljava/lang/String; = "DdmCscError"

.field private static final EXTRA_DDM_CSC_MID:Ljava/lang/String; = "DdmCscMID"

.field private static final EXTRA_DDM_CSC_NUMBER:Ljava/lang/String; = "DdmCscNumber"

.field private static final EXTRA_DDM_CSC_STATE:Ljava/lang/String; = "DdmCscState"

.field private static final EXTRA_DDM_CSC_TERM:Ljava/lang/String; = "DdmCscTerm"

.field private static final LOGTAG:Ljava/lang/String; = "LGDDM-CSC"


# direct methods
.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 59
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 60
    return-void
.end method

.method public static getGS01Metric()Lcom/carrieriq/iqagent/client/metrics/gs/GS01;
    .registers 1

    #@0
    .prologue
    .line 75
    invoke-static {}, Lcom/lge/ciq/VoiceCallCIQ$CSCallHolder;->access$000()Lcom/carrieriq/iqagent/client/metrics/gs/GS01;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public static getGS02Metric()Lcom/carrieriq/iqagent/client/metrics/gs/GS02;
    .registers 1

    #@0
    .prologue
    .line 82
    invoke-static {}, Lcom/lge/ciq/VoiceCallCIQ$CSCallHolder;->access$100()Lcom/carrieriq/iqagent/client/metrics/gs/GS02;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public static getGS03Metric()Lcom/carrieriq/iqagent/client/metrics/gs/GS03;
    .registers 1

    #@0
    .prologue
    .line 89
    invoke-static {}, Lcom/lge/ciq/VoiceCallCIQ$CSCallHolder;->access$200()Lcom/carrieriq/iqagent/client/metrics/gs/GS03;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public static submitCSCall(Landroid/content/Context;Lcom/carrieriq/iqagent/client/Metric;)V
    .registers 5
    .parameter "context"
    .parameter "metric"

    #@0
    .prologue
    .line 96
    if-nez p0, :cond_a

    #@2
    .line 97
    const-string v1, "LGDDM-CSC"

    #@4
    const-string v2, "context is null!!!"

    #@6
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 128
    :cond_9
    :goto_9
    return-void

    #@a
    .line 101
    :cond_a
    if-nez p1, :cond_14

    #@c
    .line 102
    const-string v1, "LGDDM-CSC"

    #@e
    const-string v2, "metric instance is null"

    #@10
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@13
    goto :goto_9

    #@14
    .line 106
    :cond_14
    new-instance v0, Landroid/content/Intent;

    #@16
    const-string v1, "diagandroid.ddm.csc"

    #@18
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@1b
    .line 107
    .local v0, intent:Landroid/content/Intent;
    iget v1, p1, Lcom/carrieriq/iqagent/client/Metric;->metricID:I

    #@1d
    invoke-static {}, Lcom/lge/ciq/VoiceCallCIQ;->getGS01Metric()Lcom/carrieriq/iqagent/client/metrics/gs/GS01;

    #@20
    sget v2, Lcom/carrieriq/iqagent/client/metrics/gs/GS01;->ID:I

    #@22
    if-ne v1, v2, :cond_57

    #@24
    .line 109
    const-string v2, "DdmCscCID"

    #@26
    move-object v1, p1

    #@27
    check-cast v1, Lcom/carrieriq/iqagent/client/metrics/gs/GS01;

    #@29
    iget v1, v1, Lcom/carrieriq/iqagent/client/metrics/gs/GS01;->dwCallId:I

    #@2b
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@2e
    .line 110
    const-string v2, "DdmCscAttr"

    #@30
    move-object v1, p1

    #@31
    check-cast v1, Lcom/carrieriq/iqagent/client/metrics/gs/GS01;

    #@33
    iget-byte v1, v1, Lcom/carrieriq/iqagent/client/metrics/gs/GS01;->ucCallAttr:B

    #@35
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;B)Landroid/content/Intent;

    #@38
    .line 111
    const-string v2, "DdmCscState"

    #@3a
    move-object v1, p1

    #@3b
    check-cast v1, Lcom/carrieriq/iqagent/client/metrics/gs/GS01;

    #@3d
    iget-byte v1, v1, Lcom/carrieriq/iqagent/client/metrics/gs/GS01;->ucCallState:B

    #@3f
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;B)Landroid/content/Intent;

    #@42
    .line 112
    const-string v2, "DdmCscNumber"

    #@44
    move-object v1, p1

    #@45
    check-cast v1, Lcom/carrieriq/iqagent/client/metrics/gs/GS01;

    #@47
    iget-object v1, v1, Lcom/carrieriq/iqagent/client/metrics/gs/GS01;->szNumber:Ljava/lang/String;

    #@49
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@4c
    .line 126
    :goto_4c
    const-string v1, "DdmCscMID"

    #@4e
    iget v2, p1, Lcom/carrieriq/iqagent/client/Metric;->metricID:I

    #@50
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@53
    .line 127
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@56
    goto :goto_9

    #@57
    .line 113
    :cond_57
    iget v1, p1, Lcom/carrieriq/iqagent/client/Metric;->metricID:I

    #@59
    invoke-static {}, Lcom/lge/ciq/VoiceCallCIQ;->getGS02Metric()Lcom/carrieriq/iqagent/client/metrics/gs/GS02;

    #@5c
    sget v2, Lcom/carrieriq/iqagent/client/metrics/gs/GS02;->ID:I

    #@5e
    if-ne v1, v2, :cond_75

    #@60
    .line 115
    const-string v2, "DdmCscCID"

    #@62
    move-object v1, p1

    #@63
    check-cast v1, Lcom/carrieriq/iqagent/client/metrics/gs/GS02;

    #@65
    iget v1, v1, Lcom/carrieriq/iqagent/client/metrics/gs/GS02;->dwCallId:I

    #@67
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@6a
    .line 116
    const-string v2, "DdmCscState"

    #@6c
    move-object v1, p1

    #@6d
    check-cast v1, Lcom/carrieriq/iqagent/client/metrics/gs/GS02;

    #@6f
    iget-byte v1, v1, Lcom/carrieriq/iqagent/client/metrics/gs/GS02;->ucCallState:B

    #@71
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;B)Landroid/content/Intent;

    #@74
    goto :goto_4c

    #@75
    .line 117
    :cond_75
    iget v1, p1, Lcom/carrieriq/iqagent/client/Metric;->metricID:I

    #@77
    invoke-static {}, Lcom/lge/ciq/VoiceCallCIQ;->getGS03Metric()Lcom/carrieriq/iqagent/client/metrics/gs/GS03;

    #@7a
    sget v2, Lcom/carrieriq/iqagent/client/metrics/gs/GS03;->ID:I

    #@7c
    if-ne v1, v2, :cond_9

    #@7e
    .line 119
    const-string v2, "DdmCscCID"

    #@80
    move-object v1, p1

    #@81
    check-cast v1, Lcom/carrieriq/iqagent/client/metrics/gs/GS03;

    #@83
    iget v1, v1, Lcom/carrieriq/iqagent/client/metrics/gs/GS03;->dwCallId:I

    #@85
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@88
    .line 120
    const-string v2, "DdmCscError"

    #@8a
    move-object v1, p1

    #@8b
    check-cast v1, Lcom/carrieriq/iqagent/client/metrics/gs/GS03;

    #@8d
    iget v1, v1, Lcom/carrieriq/iqagent/client/metrics/gs/GS03;->dwErrCode:I

    #@8f
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@92
    .line 121
    const-string v2, "DdmCscTerm"

    #@94
    move-object v1, p1

    #@95
    check-cast v1, Lcom/carrieriq/iqagent/client/metrics/gs/GS03;

    #@97
    iget-short v1, v1, Lcom/carrieriq/iqagent/client/metrics/gs/GS03;->wTermCode:S

    #@99
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;S)Landroid/content/Intent;

    #@9c
    goto :goto_4c
.end method
