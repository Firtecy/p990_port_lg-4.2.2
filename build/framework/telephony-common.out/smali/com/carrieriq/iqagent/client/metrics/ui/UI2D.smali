.class public Lcom/carrieriq/iqagent/client/metrics/ui/UI2D;
.super Lcom/carrieriq/iqagent/client/Metric;
.source "UI2D.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/carrieriq/iqagent/client/metrics/ui/UI2D$AppInfo;
    }
.end annotation


# static fields
.field public static final ID:I


# instance fields
.field appinfo:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/carrieriq/iqagent/client/metrics/ui/UI2D$AppInfo;",
            ">;"
        }
    .end annotation
.end field

.field public ucNumApp:B


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 55
    const-string v0, "UI2D"

    #@2
    invoke-static {v0}, Lcom/carrieriq/iqagent/client/metrics/ui/UI2D;->idFromString(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    sput v0, Lcom/carrieriq/iqagent/client/metrics/ui/UI2D;->ID:I

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 72
    sget v0, Lcom/carrieriq/iqagent/client/metrics/ui/UI2D;->ID:I

    #@2
    invoke-direct {p0, v0}, Lcom/carrieriq/iqagent/client/Metric;-><init>(I)V

    #@5
    .line 73
    new-instance v0, Ljava/util/ArrayList;

    #@7
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@a
    iput-object v0, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI2D;->appinfo:Ljava/util/ArrayList;

    #@c
    .line 74
    return-void
.end method


# virtual methods
.method public addApp(IIIIJBLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 13
    .parameter "id"
    .parameter "code"
    .parameter "data"
    .parameter "cache"
    .parameter "installTime"
    .parameter "appType"
    .parameter "name"
    .parameter "version"
    .parameter "source"

    #@0
    .prologue
    .line 79
    new-instance v0, Lcom/carrieriq/iqagent/client/metrics/ui/UI2D$AppInfo;

    #@2
    invoke-direct {v0, p0}, Lcom/carrieriq/iqagent/client/metrics/ui/UI2D$AppInfo;-><init>(Lcom/carrieriq/iqagent/client/metrics/ui/UI2D;)V

    #@5
    .line 80
    .local v0, i:Lcom/carrieriq/iqagent/client/metrics/ui/UI2D$AppInfo;
    iput p1, v0, Lcom/carrieriq/iqagent/client/metrics/ui/UI2D$AppInfo;->dwInstAppId:I

    #@7
    .line 81
    iput p2, v0, Lcom/carrieriq/iqagent/client/metrics/ui/UI2D$AppInfo;->dwSizeCode:I

    #@9
    .line 82
    iput p3, v0, Lcom/carrieriq/iqagent/client/metrics/ui/UI2D$AppInfo;->dwSizeData:I

    #@b
    .line 83
    iput p4, v0, Lcom/carrieriq/iqagent/client/metrics/ui/UI2D$AppInfo;->dwSizeCache:I

    #@d
    .line 84
    iput-wide p5, v0, Lcom/carrieriq/iqagent/client/metrics/ui/UI2D$AppInfo;->qwInstallTime:J

    #@f
    .line 85
    iput-byte p7, v0, Lcom/carrieriq/iqagent/client/metrics/ui/UI2D$AppInfo;->ucAppType:B

    #@11
    .line 86
    iput-object p8, v0, Lcom/carrieriq/iqagent/client/metrics/ui/UI2D$AppInfo;->szAppName:Ljava/lang/String;

    #@13
    .line 87
    iput-object p9, v0, Lcom/carrieriq/iqagent/client/metrics/ui/UI2D$AppInfo;->szAppVersion:Ljava/lang/String;

    #@15
    .line 88
    iput-object p10, v0, Lcom/carrieriq/iqagent/client/metrics/ui/UI2D$AppInfo;->szInstallSource:Ljava/lang/String;

    #@17
    .line 89
    iget-object v1, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI2D;->appinfo:Ljava/util/ArrayList;

    #@19
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1c
    .line 90
    return-void
.end method

.method public clearAppList()V
    .registers 2

    #@0
    .prologue
    .line 93
    iget-object v0, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI2D;->appinfo:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    #@5
    .line 94
    return-void
.end method

.method public serialize(Ljava/nio/ByteBuffer;)I
    .registers 6
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/BufferOverflowException;
        }
    .end annotation

    #@0
    .prologue
    .line 97
    iget-object v2, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI2D;->appinfo:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v2

    #@6
    int-to-byte v2, v2

    #@7
    iput-byte v2, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI2D;->ucNumApp:B

    #@9
    .line 98
    iget-byte v2, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI2D;->ucNumApp:B

    #@b
    invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@e
    .line 99
    iget-object v2, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI2D;->appinfo:Ljava/util/ArrayList;

    #@10
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@13
    move-result-object v1

    #@14
    .line 100
    .local v1, itr:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/carrieriq/iqagent/client/metrics/ui/UI2D$AppInfo;>;"
    :goto_14
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@17
    move-result v2

    #@18
    if-eqz v2, :cond_4e

    #@1a
    .line 101
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1d
    move-result-object v0

    #@1e
    check-cast v0, Lcom/carrieriq/iqagent/client/metrics/ui/UI2D$AppInfo;

    #@20
    .line 102
    .local v0, iqit:Lcom/carrieriq/iqagent/client/metrics/ui/UI2D$AppInfo;
    iget v2, v0, Lcom/carrieriq/iqagent/client/metrics/ui/UI2D$AppInfo;->dwInstAppId:I

    #@22
    invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@25
    .line 103
    iget v2, v0, Lcom/carrieriq/iqagent/client/metrics/ui/UI2D$AppInfo;->dwSizeCode:I

    #@27
    invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@2a
    .line 104
    iget v2, v0, Lcom/carrieriq/iqagent/client/metrics/ui/UI2D$AppInfo;->dwSizeData:I

    #@2c
    invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@2f
    .line 105
    iget v2, v0, Lcom/carrieriq/iqagent/client/metrics/ui/UI2D$AppInfo;->dwSizeCache:I

    #@31
    invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@34
    .line 106
    iget-wide v2, v0, Lcom/carrieriq/iqagent/client/metrics/ui/UI2D$AppInfo;->qwInstallTime:J

    #@36
    invoke-virtual {p1, v2, v3}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    #@39
    .line 107
    iget-byte v2, v0, Lcom/carrieriq/iqagent/client/metrics/ui/UI2D$AppInfo;->ucAppType:B

    #@3b
    invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@3e
    .line 108
    iget-object v2, v0, Lcom/carrieriq/iqagent/client/metrics/ui/UI2D$AppInfo;->szAppName:Ljava/lang/String;

    #@40
    invoke-virtual {p0, p1, v2}, Lcom/carrieriq/iqagent/client/metrics/ui/UI2D;->szStringOut(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    #@43
    .line 109
    iget-object v2, v0, Lcom/carrieriq/iqagent/client/metrics/ui/UI2D$AppInfo;->szAppVersion:Ljava/lang/String;

    #@45
    invoke-virtual {p0, p1, v2}, Lcom/carrieriq/iqagent/client/metrics/ui/UI2D;->szStringOut(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    #@48
    .line 110
    iget-object v2, v0, Lcom/carrieriq/iqagent/client/metrics/ui/UI2D$AppInfo;->szInstallSource:Ljava/lang/String;

    #@4a
    invoke-virtual {p0, p1, v2}, Lcom/carrieriq/iqagent/client/metrics/ui/UI2D;->szStringOut(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    #@4d
    goto :goto_14

    #@4e
    .line 112
    .end local v0           #iqit:Lcom/carrieriq/iqagent/client/metrics/ui/UI2D$AppInfo;
    :cond_4e
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    #@51
    move-result v2

    #@52
    return v2
.end method
