.class public Lcom/carrieriq/iqagent/client/Metric;
.super Ljava/lang/Object;
.source "Metric.java"


# static fields
.field public static final CURRENT_TIME:J = -0x1L


# instance fields
.field public metricID:I

.field public timestamp:J


# direct methods
.method public constructor <init>(I)V
    .registers 4
    .parameter "_metricID"

    #@0
    .prologue
    .line 80
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 81
    iput p1, p0, Lcom/carrieriq/iqagent/client/Metric;->metricID:I

    #@5
    .line 82
    const-wide/16 v0, -0x1

    #@7
    iput-wide v0, p0, Lcom/carrieriq/iqagent/client/Metric;->timestamp:J

    #@9
    .line 83
    return-void
.end method

.method public constructor <init>(IJ)V
    .registers 4
    .parameter "_metricID"
    .parameter "_ts"

    #@0
    .prologue
    .line 85
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 86
    iput p1, p0, Lcom/carrieriq/iqagent/client/Metric;->metricID:I

    #@5
    .line 87
    iput-wide p2, p0, Lcom/carrieriq/iqagent/client/Metric;->timestamp:J

    #@7
    .line 88
    return-void
.end method

.method public static idFromString(Ljava/lang/String;)I
    .registers 4
    .parameter "_id"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 59
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@4
    move-result v1

    #@5
    const/4 v2, 0x4

    #@6
    if-eq v1, v2, :cond_9

    #@8
    .line 64
    :goto_8
    return v0

    #@9
    .line 60
    :cond_9
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    #@c
    move-result v1

    #@d
    and-int/lit16 v1, v1, 0xff

    #@f
    shl-int/lit8 v0, v1, 0x18

    #@11
    .line 61
    .local v0, id:I
    const/4 v1, 0x1

    #@12
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    #@15
    move-result v1

    #@16
    and-int/lit16 v1, v1, 0xff

    #@18
    shl-int/lit8 v1, v1, 0x10

    #@1a
    or-int/2addr v0, v1

    #@1b
    .line 62
    const/4 v1, 0x2

    #@1c
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    #@1f
    move-result v1

    #@20
    and-int/lit16 v1, v1, 0xff

    #@22
    shl-int/lit8 v1, v1, 0x8

    #@24
    or-int/2addr v0, v1

    #@25
    .line 63
    const/4 v1, 0x3

    #@26
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    #@29
    move-result v1

    #@2a
    and-int/lit16 v1, v1, 0xff

    #@2c
    or-int/2addr v0, v1

    #@2d
    .line 64
    goto :goto_8
.end method

.method public static idToBytes(I[BI)V
    .registers 5
    .parameter "_id"
    .parameter "array"
    .parameter "offset"

    #@0
    .prologue
    .line 68
    add-int/lit8 v0, p2, 0x0

    #@2
    shr-int/lit8 v1, p0, 0x18

    #@4
    and-int/lit16 v1, v1, 0xff

    #@6
    int-to-byte v1, v1

    #@7
    aput-byte v1, p1, v0

    #@9
    .line 69
    add-int/lit8 v0, p2, 0x1

    #@b
    shr-int/lit8 v1, p0, 0x10

    #@d
    and-int/lit16 v1, v1, 0xff

    #@f
    int-to-byte v1, v1

    #@10
    aput-byte v1, p1, v0

    #@12
    .line 70
    add-int/lit8 v0, p2, 0x2

    #@14
    shr-int/lit8 v1, p0, 0x8

    #@16
    and-int/lit16 v1, v1, 0xff

    #@18
    int-to-byte v1, v1

    #@19
    aput-byte v1, p1, v0

    #@1b
    .line 71
    add-int/lit8 v0, p2, 0x3

    #@1d
    and-int/lit16 v1, p0, 0xff

    #@1f
    int-to-byte v1, v1

    #@20
    aput-byte v1, p1, v0

    #@22
    .line 72
    return-void
.end method

.method public static idToString(I)Ljava/lang/String;
    .registers 5
    .parameter "_id"

    #@0
    .prologue
    const/4 v3, 0x4

    #@1
    const/4 v2, 0x0

    #@2
    .line 75
    new-array v0, v3, [B

    #@4
    .line 76
    .local v0, bytes:[B
    invoke-static {p0, v0, v2}, Lcom/carrieriq/iqagent/client/Metric;->idToBytes(I[BI)V

    #@7
    .line 77
    new-instance v1, Ljava/lang/String;

    #@9
    invoke-direct {v1, v0, v2, v3}, Ljava/lang/String;-><init>([BII)V

    #@c
    return-object v1
.end method


# virtual methods
.method public serialize(Ljava/nio/ByteBuffer;)I
    .registers 3
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/BufferOverflowException;
        }
    .end annotation

    #@0
    .prologue
    .line 118
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public setTimestamp(J)V
    .registers 3
    .parameter "_ts"

    #@0
    .prologue
    .line 91
    iput-wide p1, p0, Lcom/carrieriq/iqagent/client/Metric;->timestamp:J

    #@2
    .line 92
    return-void
.end method

.method public szStringOut(Ljava/nio/ByteBuffer;Ljava/lang/String;)V
    .registers 6
    .parameter "out"
    .parameter "iString"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/BufferOverflowException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 95
    if-eqz p2, :cond_10

    #@3
    .line 97
    const/16 v1, 0x5f

    #@5
    invoke-virtual {p2, v2, v1}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    .line 98
    .local v0, aString:Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    #@c
    move-result-object v1

    #@d
    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    #@10
    .line 100
    .end local v0           #aString:Ljava/lang/String;
    :cond_10
    invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@13
    .line 101
    return-void
.end method

.method public szStringOutPadToWord(Ljava/nio/ByteBuffer;Ljava/lang/String;)V
    .registers 6
    .parameter "out"
    .parameter "aString"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/BufferOverflowException;
        }
    .end annotation

    #@0
    .prologue
    .line 104
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    #@3
    move-result v1

    #@4
    .line 105
    .local v1, startPos:I
    invoke-virtual {p0, p1, p2}, Lcom/carrieriq/iqagent/client/Metric;->szStringOut(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    #@7
    .line 106
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    #@a
    move-result v2

    #@b
    sub-int/2addr v2, v1

    #@c
    rem-int/lit8 v0, v2, 0x4

    #@e
    .line 107
    .local v0, padding:I
    if-eqz v0, :cond_12

    #@10
    .line 108
    rsub-int/lit8 v0, v0, 0x4

    #@12
    .line 110
    :cond_12
    :goto_12
    if-lez v0, :cond_1b

    #@14
    .line 111
    const/4 v2, 0x0

    #@15
    invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@18
    .line 112
    add-int/lit8 v0, v0, -0x1

    #@1a
    goto :goto_12

    #@1b
    .line 114
    :cond_1b
    return-void
.end method
