.class public Lcom/carrieriq/iqagent/client/metrics/gs/GS47;
.super Lcom/carrieriq/iqagent/client/Metric;
.source "GS47.java"


# static fields
.field public static final ID:I


# instance fields
.field public ucNetworkSvcState:B

.field public ucRadioSvcState:B


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 54
    const-string v0, "GS47"

    #@2
    invoke-static {v0}, Lcom/carrieriq/iqagent/client/metrics/gs/GS47;->idFromString(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    sput v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS47;->ID:I

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 60
    sget v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS47;->ID:I

    #@2
    invoke-direct {p0, v0}, Lcom/carrieriq/iqagent/client/Metric;-><init>(I)V

    #@5
    .line 61
    return-void
.end method

.method public constructor <init>(BB)V
    .registers 4
    .parameter "radioState"
    .parameter "networkState"

    #@0
    .prologue
    .line 64
    sget v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS47;->ID:I

    #@2
    invoke-direct {p0, v0}, Lcom/carrieriq/iqagent/client/Metric;-><init>(I)V

    #@5
    .line 65
    iput-byte p1, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS47;->ucRadioSvcState:B

    #@7
    .line 66
    iput-byte p2, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS47;->ucNetworkSvcState:B

    #@9
    .line 67
    return-void
.end method


# virtual methods
.method public serialize(Ljava/nio/ByteBuffer;)I
    .registers 3
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/BufferOverflowException;
        }
    .end annotation

    #@0
    .prologue
    .line 70
    iget-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS47;->ucRadioSvcState:B

    #@2
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@5
    .line 71
    iget-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS47;->ucNetworkSvcState:B

    #@7
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@a
    .line 72
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    #@d
    move-result v0

    #@e
    return v0
.end method
