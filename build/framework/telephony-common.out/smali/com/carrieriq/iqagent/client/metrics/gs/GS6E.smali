.class public Lcom/carrieriq/iqagent/client/metrics/gs/GS6E;
.super Lcom/carrieriq/iqagent/client/Metric;
.source "GS6E.java"


# static fields
.field public static final ID:I


# instance fields
.field public ucRadioMode:B


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 54
    const-string v0, "GS6E"

    #@2
    invoke-static {v0}, Lcom/carrieriq/iqagent/client/metrics/gs/GS6E;->idFromString(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    sput v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS6E;->ID:I

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 59
    sget v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS6E;->ID:I

    #@2
    invoke-direct {p0, v0}, Lcom/carrieriq/iqagent/client/Metric;-><init>(I)V

    #@5
    .line 60
    return-void
.end method

.method public constructor <init>(B)V
    .registers 3
    .parameter "mode"

    #@0
    .prologue
    .line 63
    sget v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS6E;->ID:I

    #@2
    invoke-direct {p0, v0}, Lcom/carrieriq/iqagent/client/Metric;-><init>(I)V

    #@5
    .line 64
    iput-byte p1, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS6E;->ucRadioMode:B

    #@7
    .line 65
    return-void
.end method


# virtual methods
.method public serialize(Ljava/nio/ByteBuffer;)I
    .registers 3
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/BufferOverflowException;
        }
    .end annotation

    #@0
    .prologue
    .line 68
    iget-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS6E;->ucRadioMode:B

    #@2
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@5
    .line 69
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    #@8
    move-result v0

    #@9
    return v0
.end method
