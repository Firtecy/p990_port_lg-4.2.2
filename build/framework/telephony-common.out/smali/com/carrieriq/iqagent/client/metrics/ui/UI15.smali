.class public Lcom/carrieriq/iqagent/client/metrics/ui/UI15;
.super Lcom/carrieriq/iqagent/client/Metric;
.source "UI15.java"


# static fields
.field public static final ID:I


# instance fields
.field public dwAppID:I

.field public shStatus:S


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 54
    const-string v0, "UI15"

    #@2
    invoke-static {v0}, Lcom/carrieriq/iqagent/client/metrics/ui/UI15;->idFromString(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    sput v0, Lcom/carrieriq/iqagent/client/metrics/ui/UI15;->ID:I

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 60
    sget v0, Lcom/carrieriq/iqagent/client/metrics/ui/UI15;->ID:I

    #@2
    invoke-direct {p0, v0}, Lcom/carrieriq/iqagent/client/Metric;-><init>(I)V

    #@5
    .line 61
    return-void
.end method

.method public constructor <init>(IS)V
    .registers 4
    .parameter "appInstanceId"
    .parameter "status"

    #@0
    .prologue
    .line 64
    sget v0, Lcom/carrieriq/iqagent/client/metrics/ui/UI15;->ID:I

    #@2
    invoke-direct {p0, v0}, Lcom/carrieriq/iqagent/client/Metric;-><init>(I)V

    #@5
    .line 65
    iput p1, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI15;->dwAppID:I

    #@7
    .line 66
    iput-short p2, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI15;->shStatus:S

    #@9
    .line 67
    return-void
.end method


# virtual methods
.method public serialize(Ljava/nio/ByteBuffer;)I
    .registers 3
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/BufferOverflowException;
        }
    .end annotation

    #@0
    .prologue
    .line 70
    iget v0, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI15;->dwAppID:I

    #@2
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@5
    .line 71
    iget-short v0, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI15;->shStatus:S

    #@7
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    #@a
    .line 72
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    #@d
    move-result v0

    #@e
    return v0
.end method
