.class public Lcom/carrieriq/iqagent/client/metrics/ui/UI11;
.super Lcom/carrieriq/iqagent/client/Metric;
.source "UI11.java"


# static fields
.field public static final ID:I


# instance fields
.field public dwErrorCode:I

.field public dwUserInputEventId:I

.field public dwVendorDataLength:I

.field public ucErrorLevel:B

.field public ucPenEvent:B

.field public ucVendorData:[B

.field public wMaxScreenLocX:S

.field public wMaxScreenLocY:S

.field public wScreenLocX:S

.field public wScreenLocY:S


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 54
    const-string v0, "UI11"

    #@2
    invoke-static {v0}, Lcom/carrieriq/iqagent/client/metrics/ui/UI11;->idFromString(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    sput v0, Lcom/carrieriq/iqagent/client/metrics/ui/UI11;->ID:I

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 68
    sget v0, Lcom/carrieriq/iqagent/client/metrics/ui/UI11;->ID:I

    #@2
    invoke-direct {p0, v0}, Lcom/carrieriq/iqagent/client/Metric;-><init>(I)V

    #@5
    .line 69
    return-void
.end method


# virtual methods
.method public serialize(Ljava/nio/ByteBuffer;)I
    .registers 3
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/BufferOverflowException;
        }
    .end annotation

    #@0
    .prologue
    .line 72
    iget-short v0, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI11;->wMaxScreenLocX:S

    #@2
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    #@5
    .line 73
    iget-short v0, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI11;->wMaxScreenLocY:S

    #@7
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    #@a
    .line 74
    iget-short v0, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI11;->wScreenLocX:S

    #@c
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    #@f
    .line 75
    iget-short v0, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI11;->wScreenLocY:S

    #@11
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    #@14
    .line 76
    iget v0, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI11;->dwUserInputEventId:I

    #@16
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@19
    .line 77
    iget v0, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI11;->dwErrorCode:I

    #@1b
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@1e
    .line 78
    iget v0, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI11;->dwVendorDataLength:I

    #@20
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@23
    .line 79
    iget-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI11;->ucErrorLevel:B

    #@25
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@28
    .line 80
    iget-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI11;->ucPenEvent:B

    #@2a
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@2d
    .line 81
    iget v0, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI11;->dwVendorDataLength:I

    #@2f
    if-eqz v0, :cond_36

    #@31
    .line 82
    iget-object v0, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI11;->ucVendorData:[B

    #@33
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    #@36
    .line 84
    :cond_36
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    #@39
    move-result v0

    #@3a
    return v0
.end method
