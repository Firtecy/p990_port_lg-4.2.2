.class public Lcom/carrieriq/iqagent/client/metrics/ui/UI05;
.super Lcom/carrieriq/iqagent/client/Metric;
.source "UI05.java"


# static fields
.field public static final ID:I


# instance fields
.field public sStatus:S

.field public strAppURI:Ljava/lang/String;

.field public ucIsNormal:B

.field public ucIsReferring:B


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 54
    const-string v0, "UI05"

    #@2
    invoke-static {v0}, Lcom/carrieriq/iqagent/client/metrics/ui/UI05;->idFromString(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    sput v0, Lcom/carrieriq/iqagent/client/metrics/ui/UI05;->ID:I

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 62
    sget v0, Lcom/carrieriq/iqagent/client/metrics/ui/UI05;->ID:I

    #@2
    invoke-direct {p0, v0}, Lcom/carrieriq/iqagent/client/Metric;-><init>(I)V

    #@5
    .line 63
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;SBB)V
    .registers 6
    .parameter "_strAppURI"
    .parameter "_sStatus"
    .parameter "_ucIsNormal"
    .parameter "_ucIsReferring"

    #@0
    .prologue
    .line 66
    sget v0, Lcom/carrieriq/iqagent/client/metrics/ui/UI05;->ID:I

    #@2
    invoke-direct {p0, v0}, Lcom/carrieriq/iqagent/client/Metric;-><init>(I)V

    #@5
    .line 67
    iput-object p1, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI05;->strAppURI:Ljava/lang/String;

    #@7
    .line 68
    iput-short p2, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI05;->sStatus:S

    #@9
    .line 69
    iput-byte p3, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI05;->ucIsNormal:B

    #@b
    .line 70
    iput-byte p4, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI05;->ucIsReferring:B

    #@d
    .line 71
    return-void
.end method


# virtual methods
.method public serialize(Ljava/nio/ByteBuffer;)I
    .registers 3
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/BufferOverflowException;
        }
    .end annotation

    #@0
    .prologue
    .line 74
    iget-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI05;->ucIsNormal:B

    #@2
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@5
    .line 75
    iget-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI05;->ucIsReferring:B

    #@7
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@a
    .line 76
    iget-short v0, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI05;->sStatus:S

    #@c
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    #@f
    .line 77
    iget-object v0, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI05;->strAppURI:Ljava/lang/String;

    #@11
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    #@14
    move-result-object v0

    #@15
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    #@18
    .line 78
    const/4 v0, 0x0

    #@19
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@1c
    .line 79
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    #@1f
    move-result v0

    #@20
    return v0
.end method
