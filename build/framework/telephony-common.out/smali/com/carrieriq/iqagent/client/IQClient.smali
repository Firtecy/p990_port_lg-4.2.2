.class public Lcom/carrieriq/iqagent/client/IQClient;
.super Ljava/lang/Object;
.source "IQClient.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 54
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 56
    return-void
.end method

.method private LoadClientLibrary()V
    .registers 1

    #@0
    .prologue
    .line 65
    return-void
.end method

.method private reregisterNativeEvents()V
    .registers 1

    #@0
    .prologue
    .line 105
    return-void
.end method

.method private unregisterForEventType(Ljava/util/TreeMap;ILcom/carrieriq/iqagent/client/IEventCallback;)V
    .registers 4
    .parameter
    .parameter "eventParam"
    .parameter "callback"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/carrieriq/iqagent/client/IEventCallback;",
            ">;>;I",
            "Lcom/carrieriq/iqagent/client/IEventCallback;",
            ")V"
        }
    .end annotation

    #@0
    .prologue
    .line 102
    .local p1, byParam:Ljava/util/TreeMap;,"Ljava/util/TreeMap<Ljava/lang/Integer;Ljava/util/ArrayList<Lcom/carrieriq/iqagent/client/IEventCallback;>;>;"
    return-void
.end method

.method private unregisterRef(Ljava/util/ArrayList;Lcom/carrieriq/iqagent/client/IEventCallback;)V
    .registers 3
    .parameter
    .parameter "callback"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/carrieriq/iqagent/client/IEventCallback;",
            ">;",
            "Lcom/carrieriq/iqagent/client/IEventCallback;",
            ")V"
        }
    .end annotation

    #@0
    .prologue
    .line 99
    .local p1, refList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/carrieriq/iqagent/client/IEventCallback;>;"
    return-void
.end method


# virtual methods
.method public checkSMS(Ljava/lang/String;)Z
    .registers 3
    .parameter "payload"

    #@0
    .prologue
    .line 128
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public checkSMS([B)Z
    .registers 3
    .parameter "payload"

    #@0
    .prologue
    .line 132
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public checkWAPPush([B)Z
    .registers 3
    .parameter "payload"

    #@0
    .prologue
    .line 136
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public compareAgentVersion([I)I
    .registers 3
    .parameter "compareToVersion"

    #@0
    .prologue
    .line 76
    const/4 v0, -0x1

    #@1
    return v0
.end method

.method public disconnect()V
    .registers 1

    #@0
    .prologue
    .line 144
    return-void
.end method

.method public finalize()V
    .registers 1

    #@0
    .prologue
    .line 147
    return-void
.end method

.method public getAgentVersion()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 68
    const-string v0, "UNKNOWN"

    #@2
    return-object v0
.end method

.method public getAgentVersionComponents()[I
    .registers 3

    #@0
    .prologue
    .line 72
    const/4 v1, 0x3

    #@1
    new-array v0, v1, [I

    #@3
    .line 73
    .local v0, components:[I
    return-object v0
.end method

.method public getTimestamp()J
    .registers 3

    #@0
    .prologue
    .line 79
    const-wide/16 v0, -0x1

    #@2
    return-wide v0
.end method

.method public registerForEvent(IILcom/carrieriq/iqagent/client/IEventCallback;)I
    .registers 5
    .parameter "eventType"
    .parameter "eventParam"
    .parameter "callback"

    #@0
    .prologue
    .line 95
    const/4 v0, -0x1

    #@1
    return v0
.end method

.method public registerQueriableMetric(ILcom/carrieriq/iqagent/client/MetricQueryCallback;)I
    .registers 4
    .parameter "metricID"
    .parameter "callback"

    #@0
    .prologue
    .line 116
    const/4 v0, -0x1

    #@1
    return v0
.end method

.method public reportKeyCode([B)Z
    .registers 3
    .parameter "keys"

    #@0
    .prologue
    .line 140
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public shouldSubmitMetric(I)Z
    .registers 3
    .parameter "metricID"

    #@0
    .prologue
    .line 83
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public submitMetric(IJ[BII)I
    .registers 8
    .parameter "metricID"
    .parameter "timestamp"
    .parameter "payloadBytes"
    .parameter "payloadOffs"
    .parameter "payloadLen"

    #@0
    .prologue
    .line 91
    const/4 v0, -0x1

    #@1
    return v0
.end method

.method public submitMetric(Lcom/carrieriq/iqagent/client/Metric;)I
    .registers 3
    .parameter "metric"

    #@0
    .prologue
    .line 87
    const/4 v0, -0x1

    #@1
    return v0
.end method

.method public unregisterAllQueriableMetrics(Lcom/carrieriq/iqagent/client/MetricQueryCallback;)I
    .registers 3
    .parameter "callback"

    #@0
    .prologue
    .line 124
    const/4 v0, -0x1

    #@1
    return v0
.end method

.method public unregisterForAllEvents(Lcom/carrieriq/iqagent/client/IEventCallback;)I
    .registers 3
    .parameter "callback"

    #@0
    .prologue
    .line 112
    const/4 v0, -0x1

    #@1
    return v0
.end method

.method public unregisterForEvent(IILcom/carrieriq/iqagent/client/IEventCallback;)I
    .registers 5
    .parameter "eventType"
    .parameter "eventParam"
    .parameter "callback"

    #@0
    .prologue
    .line 108
    const/4 v0, -0x1

    #@1
    return v0
.end method

.method public unregisterQueriableMetric(ILcom/carrieriq/iqagent/client/MetricQueryCallback;)I
    .registers 4
    .parameter "metricID"
    .parameter "callback"

    #@0
    .prologue
    .line 120
    const/4 v0, -0x1

    #@1
    return v0
.end method
