.class public Lcom/carrieriq/iqagent/client/metrics/ui/UI37;
.super Lcom/carrieriq/iqagent/client/Metric;
.source "UI37.java"


# static fields
.field public static final ID:I


# instance fields
.field public dwRunAppID:I

.field public dwTotalRxBytes:I

.field public dwTotalTxBytes:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 54
    const-string v0, "UI37"

    #@2
    invoke-static {v0}, Lcom/carrieriq/iqagent/client/metrics/ui/UI37;->idFromString(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    sput v0, Lcom/carrieriq/iqagent/client/metrics/ui/UI37;->ID:I

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 61
    sget v0, Lcom/carrieriq/iqagent/client/metrics/ui/UI37;->ID:I

    #@2
    invoke-direct {p0, v0}, Lcom/carrieriq/iqagent/client/Metric;-><init>(I)V

    #@5
    .line 62
    return-void
.end method


# virtual methods
.method public serialize(Ljava/nio/ByteBuffer;)I
    .registers 3
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/BufferOverflowException;
        }
    .end annotation

    #@0
    .prologue
    .line 65
    iget v0, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI37;->dwRunAppID:I

    #@2
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@5
    .line 66
    iget v0, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI37;->dwTotalTxBytes:I

    #@7
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@a
    .line 67
    iget v0, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI37;->dwTotalRxBytes:I

    #@c
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@f
    .line 68
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    #@12
    move-result v0

    #@13
    return v0
.end method
