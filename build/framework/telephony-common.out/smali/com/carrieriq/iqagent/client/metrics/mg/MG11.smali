.class public Lcom/carrieriq/iqagent/client/metrics/mg/MG11;
.super Lcom/carrieriq/iqagent/client/Metric;
.source "MG11.java"


# static fields
.field public static final ID:I


# instance fields
.field public dwContentType:I

.field public dwSize:I

.field public shMmsVersion:S

.field public szMmsTransId:Ljava/lang/String;

.field public szRecipient:Ljava/lang/String;

.field public szRelayURL:Ljava/lang/String;

.field public szSender:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 54
    const-string v0, "MG11"

    #@2
    invoke-static {v0}, Lcom/carrieriq/iqagent/client/metrics/mg/MG11;->idFromString(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    sput v0, Lcom/carrieriq/iqagent/client/metrics/mg/MG11;->ID:I

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 66
    sget v0, Lcom/carrieriq/iqagent/client/metrics/mg/MG11;->ID:I

    #@2
    invoke-direct {p0, v0}, Lcom/carrieriq/iqagent/client/Metric;-><init>(I)V

    #@5
    .line 67
    return-void
.end method


# virtual methods
.method public serialize(Ljava/nio/ByteBuffer;)I
    .registers 3
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/BufferOverflowException;
        }
    .end annotation

    #@0
    .prologue
    .line 70
    iget v0, p0, Lcom/carrieriq/iqagent/client/metrics/mg/MG11;->dwSize:I

    #@2
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@5
    .line 71
    iget v0, p0, Lcom/carrieriq/iqagent/client/metrics/mg/MG11;->dwContentType:I

    #@7
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@a
    .line 72
    iget-short v0, p0, Lcom/carrieriq/iqagent/client/metrics/mg/MG11;->shMmsVersion:S

    #@c
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    #@f
    .line 73
    iget-object v0, p0, Lcom/carrieriq/iqagent/client/metrics/mg/MG11;->szMmsTransId:Ljava/lang/String;

    #@11
    invoke-virtual {p0, p1, v0}, Lcom/carrieriq/iqagent/client/metrics/mg/MG11;->szStringOut(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    #@14
    .line 74
    iget-object v0, p0, Lcom/carrieriq/iqagent/client/metrics/mg/MG11;->szSender:Ljava/lang/String;

    #@16
    invoke-virtual {p0, p1, v0}, Lcom/carrieriq/iqagent/client/metrics/mg/MG11;->szStringOut(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    #@19
    .line 75
    iget-object v0, p0, Lcom/carrieriq/iqagent/client/metrics/mg/MG11;->szRecipient:Ljava/lang/String;

    #@1b
    invoke-virtual {p0, p1, v0}, Lcom/carrieriq/iqagent/client/metrics/mg/MG11;->szStringOut(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    #@1e
    .line 76
    iget-object v0, p0, Lcom/carrieriq/iqagent/client/metrics/mg/MG11;->szRelayURL:Ljava/lang/String;

    #@20
    invoke-virtual {p0, p1, v0}, Lcom/carrieriq/iqagent/client/metrics/mg/MG11;->szStringOut(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    #@23
    .line 77
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    #@26
    move-result v0

    #@27
    return v0
.end method
