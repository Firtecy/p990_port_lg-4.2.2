.class public Lcom/carrieriq/iqagent/client/metrics/gs/GS18;
.super Lcom/carrieriq/iqagent/client/Metric;
.source "GS18.java"


# static fields
.field public static final ID:I

.field public static IQ_ANDROID_REASON_APN_CHANGED:S

.field public static IQ_ANDROID_REASON_APN_FAILED:S

.field public static IQ_ANDROID_REASON_APN_SWITCHED:S

.field public static IQ_ANDROID_REASON_APN_TYPE_DISABLED:S

.field public static IQ_ANDROID_REASON_APN_TYPE_ENABLED:S

.field public static IQ_ANDROID_REASON_CDMA_DATA_ATTACHED:S

.field public static IQ_ANDROID_REASON_CDMA_DATA_DETACHED:S

.field public static IQ_ANDROID_REASON_CDMA_OTA_PROVISIONING:S

.field public static IQ_ANDROID_REASON_CDMA_SUBSCRIPTION_SOURCE_CHANGED:S

.field public static IQ_ANDROID_REASON_DATA_CONNECTION_PROPERTY_CHANGED:S

.field public static IQ_ANDROID_REASON_DATA_DISABLED:S

.field public static IQ_ANDROID_REASON_DATA_ENABLED:S

.field public static IQ_ANDROID_REASON_DATA_NETWORK_ATTACHED:S

.field public static IQ_ANDROID_REASON_DATA_NETWORK_DETACHED:S

.field public static IQ_ANDROID_REASON_DATA_PROFILE_DB_CHANGED:S

.field public static IQ_ANDROID_REASON_DEFAULT_DATA_DISABLED:S

.field public static IQ_ANDROID_REASON_DEFAULT_DATA_ENABLED:S

.field public static IQ_ANDROID_REASON_GPRS_ATTACHED:S

.field public static IQ_ANDROID_REASON_GPRS_DETACHED:S

.field public static IQ_ANDROID_REASON_ICC_RECORDS_LOADED:S

.field public static IQ_ANDROID_REASON_MASTER_DATA_DISABLED:S

.field public static IQ_ANDROID_REASON_MASTER_DATA_ENABLED:S

.field public static IQ_ANDROID_REASON_NETWORK_OR_MODEM_DISCONNECT:S

.field public static IQ_ANDROID_REASON_PDP_RESET:S

.field public static IQ_ANDROID_REASON_PS_RESTRICT_DISABLED:S

.field public static IQ_ANDROID_REASON_PS_RESTRICT_ENABLED:S

.field public static IQ_ANDROID_REASON_RADIO_OFF:S

.field public static IQ_ANDROID_REASON_RADIO_ON:S

.field public static IQ_ANDROID_REASON_RADIO_TECH_CHANGED:S

.field public static IQ_ANDROID_REASON_RADIO_TURNED_OFF:S

.field public static IQ_ANDROID_REASON_RESTORE_DEFAULT_APN:S

.field public static IQ_ANDROID_REASON_ROAMING_OFF:S

.field public static IQ_ANDROID_REASON_ROAMING_ON:S

.field public static IQ_ANDROID_REASON_SIM_LOADED:S

.field public static IQ_ANDROID_REASON_TETHERED_MODE_CHANGED:S

.field public static IQ_ANDROID_REASON_UNKNOWN:S

.field public static IQ_ANDROID_REASON_UNKNOWN_DATA_ERROR:S

.field public static IQ_ANDROID_REASON_UNKNOWN_PDP_DISCONNECT:S

.field public static IQ_ANDROID_REASON_VOICE_CALL_ENDED:S

.field public static IQ_ANDROID_REASON_VOICE_CALL_STARTED:S


# instance fields
.field public dwContextId:I

.field public dwErrCode:I

.field public ucInitiator:B

.field public wTermCode:S


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 54
    const-string v0, "GS18"

    #@2
    invoke-static {v0}, Lcom/carrieriq/iqagent/client/metrics/gs/GS18;->idFromString(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    sput v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS18;->ID:I

    #@8
    .line 61
    const/16 v0, 0x12d

    #@a
    sput-short v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS18;->IQ_ANDROID_REASON_UNKNOWN:S

    #@c
    .line 62
    const/16 v0, 0x12e

    #@e
    sput-short v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS18;->IQ_ANDROID_REASON_RADIO_TURNED_OFF:S

    #@10
    .line 63
    const/16 v0, 0x12f

    #@12
    sput-short v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS18;->IQ_ANDROID_REASON_UNKNOWN_PDP_DISCONNECT:S

    #@14
    .line 64
    const/16 v0, 0x130

    #@16
    sput-short v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS18;->IQ_ANDROID_REASON_UNKNOWN_DATA_ERROR:S

    #@18
    .line 65
    const/16 v0, 0x131

    #@1a
    sput-short v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS18;->IQ_ANDROID_REASON_ROAMING_ON:S

    #@1c
    .line 66
    const/16 v0, 0x132

    #@1e
    sput-short v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS18;->IQ_ANDROID_REASON_ROAMING_OFF:S

    #@20
    .line 67
    const/16 v0, 0x133

    #@22
    sput-short v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS18;->IQ_ANDROID_REASON_DATA_DISABLED:S

    #@24
    .line 68
    const/16 v0, 0x134

    #@26
    sput-short v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS18;->IQ_ANDROID_REASON_DATA_ENABLED:S

    #@28
    .line 69
    const/16 v0, 0x135

    #@2a
    sput-short v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS18;->IQ_ANDROID_REASON_GPRS_ATTACHED:S

    #@2c
    .line 70
    const/16 v0, 0x136

    #@2e
    sput-short v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS18;->IQ_ANDROID_REASON_GPRS_DETACHED:S

    #@30
    .line 71
    const/16 v0, 0x137

    #@32
    sput-short v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS18;->IQ_ANDROID_REASON_CDMA_DATA_ATTACHED:S

    #@34
    .line 72
    const/16 v0, 0x138

    #@36
    sput-short v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS18;->IQ_ANDROID_REASON_CDMA_DATA_DETACHED:S

    #@38
    .line 73
    const/16 v0, 0x139

    #@3a
    sput-short v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS18;->IQ_ANDROID_REASON_APN_CHANGED:S

    #@3c
    .line 74
    const/16 v0, 0x13a

    #@3e
    sput-short v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS18;->IQ_ANDROID_REASON_APN_SWITCHED:S

    #@40
    .line 75
    const/16 v0, 0x13b

    #@42
    sput-short v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS18;->IQ_ANDROID_REASON_APN_FAILED:S

    #@44
    .line 76
    const/16 v0, 0x13c

    #@46
    sput-short v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS18;->IQ_ANDROID_REASON_RESTORE_DEFAULT_APN:S

    #@48
    .line 77
    const/16 v0, 0x13d

    #@4a
    sput-short v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS18;->IQ_ANDROID_REASON_PDP_RESET:S

    #@4c
    .line 78
    const/16 v0, 0x13e

    #@4e
    sput-short v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS18;->IQ_ANDROID_REASON_VOICE_CALL_ENDED:S

    #@50
    .line 79
    const/16 v0, 0x13f

    #@52
    sput-short v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS18;->IQ_ANDROID_REASON_VOICE_CALL_STARTED:S

    #@54
    .line 80
    const/16 v0, 0x140

    #@56
    sput-short v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS18;->IQ_ANDROID_REASON_PS_RESTRICT_ENABLED:S

    #@58
    .line 81
    const/16 v0, 0x141

    #@5a
    sput-short v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS18;->IQ_ANDROID_REASON_PS_RESTRICT_DISABLED:S

    #@5c
    .line 82
    const/16 v0, 0x142

    #@5e
    sput-short v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS18;->IQ_ANDROID_REASON_SIM_LOADED:S

    #@60
    .line 83
    const/16 v0, 0x143

    #@62
    sput-short v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS18;->IQ_ANDROID_REASON_APN_TYPE_DISABLED:S

    #@64
    .line 84
    const/16 v0, 0x144

    #@66
    sput-short v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS18;->IQ_ANDROID_REASON_APN_TYPE_ENABLED:S

    #@68
    .line 85
    const/16 v0, 0x145

    #@6a
    sput-short v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS18;->IQ_ANDROID_REASON_MASTER_DATA_DISABLED:S

    #@6c
    .line 86
    const/16 v0, 0x146

    #@6e
    sput-short v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS18;->IQ_ANDROID_REASON_MASTER_DATA_ENABLED:S

    #@70
    .line 87
    const/16 v0, 0x147

    #@72
    sput-short v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS18;->IQ_ANDROID_REASON_ICC_RECORDS_LOADED:S

    #@74
    .line 88
    const/16 v0, 0x148

    #@76
    sput-short v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS18;->IQ_ANDROID_REASON_CDMA_OTA_PROVISIONING:S

    #@78
    .line 89
    const/16 v0, 0x149

    #@7a
    sput-short v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS18;->IQ_ANDROID_REASON_DEFAULT_DATA_DISABLED:S

    #@7c
    .line 90
    const/16 v0, 0x14a

    #@7e
    sput-short v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS18;->IQ_ANDROID_REASON_DEFAULT_DATA_ENABLED:S

    #@80
    .line 91
    const/16 v0, 0x14b

    #@82
    sput-short v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS18;->IQ_ANDROID_REASON_RADIO_ON:S

    #@84
    .line 92
    const/16 v0, 0x14c

    #@86
    sput-short v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS18;->IQ_ANDROID_REASON_RADIO_OFF:S

    #@88
    .line 93
    const/16 v0, 0x14d

    #@8a
    sput-short v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS18;->IQ_ANDROID_REASON_RADIO_TECH_CHANGED:S

    #@8c
    .line 94
    const/16 v0, 0x14e

    #@8e
    sput-short v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS18;->IQ_ANDROID_REASON_NETWORK_OR_MODEM_DISCONNECT:S

    #@90
    .line 95
    const/16 v0, 0x14f

    #@92
    sput-short v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS18;->IQ_ANDROID_REASON_DATA_NETWORK_ATTACHED:S

    #@94
    .line 96
    const/16 v0, 0x150

    #@96
    sput-short v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS18;->IQ_ANDROID_REASON_DATA_NETWORK_DETACHED:S

    #@98
    .line 97
    const/16 v0, 0x151

    #@9a
    sput-short v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS18;->IQ_ANDROID_REASON_DATA_PROFILE_DB_CHANGED:S

    #@9c
    .line 98
    const/16 v0, 0x152

    #@9e
    sput-short v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS18;->IQ_ANDROID_REASON_CDMA_SUBSCRIPTION_SOURCE_CHANGED:S

    #@a0
    .line 99
    const/16 v0, 0x153

    #@a2
    sput-short v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS18;->IQ_ANDROID_REASON_TETHERED_MODE_CHANGED:S

    #@a4
    .line 100
    const/16 v0, 0x157

    #@a6
    sput-short v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS18;->IQ_ANDROID_REASON_DATA_CONNECTION_PROPERTY_CHANGED:S

    #@a8
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 104
    sget v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS18;->ID:I

    #@2
    invoke-direct {p0, v0}, Lcom/carrieriq/iqagent/client/Metric;-><init>(I)V

    #@5
    .line 105
    return-void
.end method

.method public constructor <init>(IISB)V
    .registers 6
    .parameter "context"
    .parameter "errCode"
    .parameter "termCode"
    .parameter "initiator"

    #@0
    .prologue
    .line 108
    sget v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS18;->ID:I

    #@2
    invoke-direct {p0, v0}, Lcom/carrieriq/iqagent/client/Metric;-><init>(I)V

    #@5
    .line 109
    iput p1, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS18;->dwContextId:I

    #@7
    .line 110
    iput p2, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS18;->dwErrCode:I

    #@9
    .line 111
    iput-short p3, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS18;->wTermCode:S

    #@b
    .line 112
    iput-byte p4, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS18;->ucInitiator:B

    #@d
    .line 113
    return-void
.end method


# virtual methods
.method public serialize(Ljava/nio/ByteBuffer;)I
    .registers 3
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/BufferOverflowException;
        }
    .end annotation

    #@0
    .prologue
    .line 117
    iget v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS18;->dwContextId:I

    #@2
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@5
    .line 119
    iget-short v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS18;->wTermCode:S

    #@7
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    #@a
    .line 120
    iget-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS18;->ucInitiator:B

    #@c
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@f
    .line 121
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    #@12
    move-result v0

    #@13
    return v0
.end method
