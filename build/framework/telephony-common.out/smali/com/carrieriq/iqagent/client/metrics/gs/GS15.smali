.class public Lcom/carrieriq/iqagent/client/metrics/gs/GS15;
.super Lcom/carrieriq/iqagent/client/Metric;
.source "GS15.java"


# static fields
.field public static final ID:I


# instance fields
.field public dwContextId:I

.field public dwDns1:I

.field public dwDns2:I

.field public dwIpAddress:I

.field public dwIpv6QuadInts:[S


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 54
    const-string v0, "GS15"

    #@2
    invoke-static {v0}, Lcom/carrieriq/iqagent/client/metrics/gs/GS15;->idFromString(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    sput v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS15;->ID:I

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 63
    sget v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS15;->ID:I

    #@2
    invoke-direct {p0, v0}, Lcom/carrieriq/iqagent/client/Metric;-><init>(I)V

    #@5
    .line 60
    const/4 v0, 0x4

    #@6
    new-array v0, v0, [S

    #@8
    fill-array-data v0, :array_e

    #@b
    iput-object v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS15;->dwIpv6QuadInts:[S

    #@d
    .line 64
    return-void

    #@e
    .line 60
    :array_e
    .array-data 0x2
        0x0t 0x0t
        0x0t 0x0t
        0x0t 0x0t
        0x0t 0x0t
    .end array-data
.end method

.method public constructor <init>(IIII)V
    .registers 6
    .parameter "context"
    .parameter "ip"
    .parameter "dns1"
    .parameter "dns2"

    #@0
    .prologue
    .line 67
    sget v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS15;->ID:I

    #@2
    invoke-direct {p0, v0}, Lcom/carrieriq/iqagent/client/Metric;-><init>(I)V

    #@5
    .line 60
    const/4 v0, 0x4

    #@6
    new-array v0, v0, [S

    #@8
    fill-array-data v0, :array_16

    #@b
    iput-object v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS15;->dwIpv6QuadInts:[S

    #@d
    .line 68
    iput p1, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS15;->dwContextId:I

    #@f
    .line 69
    iput p2, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS15;->dwIpAddress:I

    #@11
    .line 70
    iput p3, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS15;->dwDns1:I

    #@13
    .line 71
    iput p4, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS15;->dwDns2:I

    #@15
    .line 72
    return-void

    #@16
    .line 60
    :array_16
    .array-data 0x2
        0x0t 0x0t
        0x0t 0x0t
        0x0t 0x0t
        0x0t 0x0t
    .end array-data
.end method


# virtual methods
.method public serialize(Ljava/nio/ByteBuffer;)I
    .registers 3
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/BufferOverflowException;
        }
    .end annotation

    #@0
    .prologue
    .line 76
    iget v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS15;->dwContextId:I

    #@2
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@5
    .line 77
    iget v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS15;->dwIpAddress:I

    #@7
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@a
    .line 78
    iget v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS15;->dwDns1:I

    #@c
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@f
    .line 79
    iget v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS15;->dwDns2:I

    #@11
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@14
    .line 83
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    #@17
    move-result v0

    #@18
    return v0
.end method
