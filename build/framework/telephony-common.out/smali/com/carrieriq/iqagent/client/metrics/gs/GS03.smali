.class public Lcom/carrieriq/iqagent/client/metrics/gs/GS03;
.super Lcom/carrieriq/iqagent/client/Metric;
.source "GS03.java"


# static fields
.field public static final ID:I


# instance fields
.field public dwCallId:I

.field public dwErrCode:I

.field public wTermCode:S


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 54
    const-string v0, "GS03"

    #@2
    invoke-static {v0}, Lcom/carrieriq/iqagent/client/metrics/gs/GS03;->idFromString(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    sput v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS03;->ID:I

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 61
    sget v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS03;->ID:I

    #@2
    invoke-direct {p0, v0}, Lcom/carrieriq/iqagent/client/Metric;-><init>(I)V

    #@5
    .line 62
    return-void
.end method

.method public constructor <init>(IIS)V
    .registers 5
    .parameter "callId"
    .parameter "errCode"
    .parameter "termCode"

    #@0
    .prologue
    .line 66
    sget v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS03;->ID:I

    #@2
    invoke-direct {p0, v0}, Lcom/carrieriq/iqagent/client/Metric;-><init>(I)V

    #@5
    .line 67
    iput p1, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS03;->dwCallId:I

    #@7
    .line 68
    iput p2, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS03;->dwErrCode:I

    #@9
    .line 69
    iput-short p3, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS03;->wTermCode:S

    #@b
    .line 70
    return-void
.end method


# virtual methods
.method public serialize(Ljava/nio/ByteBuffer;)I
    .registers 3
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/BufferOverflowException;
        }
    .end annotation

    #@0
    .prologue
    .line 74
    iget v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS03;->dwCallId:I

    #@2
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@5
    .line 75
    iget v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS03;->dwErrCode:I

    #@7
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@a
    .line 76
    iget-short v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS03;->wTermCode:S

    #@c
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    #@f
    .line 77
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    #@12
    move-result v0

    #@13
    return v0
.end method
