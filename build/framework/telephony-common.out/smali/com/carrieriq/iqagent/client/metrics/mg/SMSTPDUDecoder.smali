.class public Lcom/carrieriq/iqagent/client/metrics/mg/SMSTPDUDecoder;
.super Ljava/lang/Object;
.source "SMSTPDUDecoder.java"


# instance fields
.field public address:Ljava/lang/String;

.field public dataLen:S

.field public messageRef:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 14
    .parameter "data"

    #@0
    .prologue
    const/4 v11, 0x6

    #@1
    const/4 v9, 0x4

    #@2
    const/16 v10, 0x10

    #@4
    .line 61
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@7
    .line 62
    const/4 v8, 0x2

    #@8
    invoke-virtual {p1, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@b
    move-result-object v8

    #@c
    invoke-static {v8, v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    #@f
    move-result v5

    #@10
    .line 63
    .local v5, smsSubmitCode:I
    const/4 v7, 0x0

    #@11
    .line 67
    .local v7, timevalidoffset:I
    and-int/lit8 v8, v5, 0xc

    #@13
    sparse-switch v8, :sswitch_data_76

    #@16
    .line 73
    :goto_16
    invoke-virtual {p1, v9, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@19
    move-result-object v8

    #@1a
    invoke-static {v8, v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    #@1d
    move-result v8

    #@1e
    iput v8, p0, Lcom/carrieriq/iqagent/client/metrics/mg/SMSTPDUDecoder;->messageRef:I

    #@20
    .line 76
    const/16 v8, 0x8

    #@22
    invoke-virtual {p1, v11, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@25
    move-result-object v8

    #@26
    invoke-static {v8, v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    #@29
    move-result v1

    #@2a
    .line 77
    .local v1, addressLen:I
    move v2, v1

    #@2b
    .line 78
    .local v2, addressSpace:I
    rem-int/lit8 v8, v1, 0x2

    #@2d
    if-eqz v8, :cond_31

    #@2f
    .line 79
    add-int/lit8 v2, v2, 0x1

    #@31
    .line 81
    :cond_31
    const/16 v8, 0xa

    #@33
    add-int/lit8 v9, v2, 0xb

    #@35
    invoke-virtual {p1, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@38
    move-result-object v8

    #@39
    iput-object v8, p0, Lcom/carrieriq/iqagent/client/metrics/mg/SMSTPDUDecoder;->address:Ljava/lang/String;

    #@3b
    .line 83
    iget-object v8, p0, Lcom/carrieriq/iqagent/client/metrics/mg/SMSTPDUDecoder;->address:Ljava/lang/String;

    #@3d
    invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B

    #@40
    move-result-object v0

    #@41
    .line 84
    .local v0, addressData:[B
    const/4 v4, 0x0

    #@42
    .local v4, k:I
    :goto_42
    if-ge v4, v2, :cond_5d

    #@44
    .line 85
    aget-byte v6, v0, v4

    #@46
    .line 86
    .local v6, temp:B
    add-int/lit8 v8, v4, 0x1

    #@48
    aget-byte v8, v0, v8

    #@4a
    aput-byte v8, v0, v4

    #@4c
    .line 87
    add-int/lit8 v8, v4, 0x1

    #@4e
    aput-byte v6, v0, v8

    #@50
    .line 84
    add-int/lit8 v4, v4, 0x2

    #@52
    goto :goto_42

    #@53
    .line 68
    .end local v0           #addressData:[B
    .end local v1           #addressLen:I
    .end local v2           #addressSpace:I
    .end local v4           #k:I
    .end local v6           #temp:B
    :sswitch_53
    const/4 v7, 0x0

    #@54
    goto :goto_16

    #@55
    .line 69
    :sswitch_55
    const/4 v7, 0x2

    #@56
    goto :goto_16

    #@57
    .line 70
    :sswitch_57
    const/16 v7, 0xe

    #@59
    goto :goto_16

    #@5a
    .line 71
    :sswitch_5a
    const/16 v7, 0xe

    #@5c
    goto :goto_16

    #@5d
    .line 89
    .restart local v0       #addressData:[B
    .restart local v1       #addressLen:I
    .restart local v2       #addressSpace:I
    .restart local v4       #k:I
    :cond_5d
    new-instance v8, Ljava/lang/String;

    #@5f
    const/4 v9, 0x0

    #@60
    invoke-direct {v8, v0, v9, v1}, Ljava/lang/String;-><init>([BII)V

    #@63
    iput-object v8, p0, Lcom/carrieriq/iqagent/client/metrics/mg/SMSTPDUDecoder;->address:Ljava/lang/String;

    #@65
    .line 90
    add-int v8, v7, v2

    #@67
    add-int/lit8 v3, v8, 0xe

    #@69
    .line 91
    .local v3, dataLenStart:I
    add-int/lit8 v8, v3, 0x2

    #@6b
    invoke-virtual {p1, v3, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@6e
    move-result-object v8

    #@6f
    invoke-static {v8, v10}, Ljava/lang/Short;->parseShort(Ljava/lang/String;I)S

    #@72
    move-result v8

    #@73
    iput-short v8, p0, Lcom/carrieriq/iqagent/client/metrics/mg/SMSTPDUDecoder;->dataLen:S

    #@75
    .line 93
    return-void

    #@76
    .line 67
    :sswitch_data_76
    .sparse-switch
        0x0 -> :sswitch_53
        0x4 -> :sswitch_57
        0x8 -> :sswitch_55
        0xc -> :sswitch_5a
    .end sparse-switch
.end method
