.class public Lcom/carrieriq/iqagent/client/metrics/mg/MG15;
.super Lcom/carrieriq/iqagent/client/Metric;
.source "MG15.java"


# static fields
.field public static final ID:I


# instance fields
.field public dwContentType:I

.field public dwErrorCode:I

.field public szLocationUrl:Ljava/lang/String;

.field public szMmsMsgId:Ljava/lang/String;

.field public szMmsTransId:Ljava/lang/String;

.field public wMmsVersion:S

.field public wResultCode:S


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 54
    const-string v0, "MG15"

    #@2
    invoke-static {v0}, Lcom/carrieriq/iqagent/client/metrics/mg/MG15;->idFromString(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    sput v0, Lcom/carrieriq/iqagent/client/metrics/mg/MG15;->ID:I

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 66
    sget v0, Lcom/carrieriq/iqagent/client/metrics/mg/MG15;->ID:I

    #@2
    invoke-direct {p0, v0}, Lcom/carrieriq/iqagent/client/Metric;-><init>(I)V

    #@5
    .line 67
    return-void
.end method


# virtual methods
.method public serialize(Ljava/nio/ByteBuffer;)I
    .registers 3
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/BufferOverflowException;
        }
    .end annotation

    #@0
    .prologue
    .line 70
    iget v0, p0, Lcom/carrieriq/iqagent/client/metrics/mg/MG15;->dwContentType:I

    #@2
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@5
    .line 71
    iget v0, p0, Lcom/carrieriq/iqagent/client/metrics/mg/MG15;->dwErrorCode:I

    #@7
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@a
    .line 72
    iget-short v0, p0, Lcom/carrieriq/iqagent/client/metrics/mg/MG15;->wResultCode:S

    #@c
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    #@f
    .line 73
    iget-short v0, p0, Lcom/carrieriq/iqagent/client/metrics/mg/MG15;->wMmsVersion:S

    #@11
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    #@14
    .line 74
    iget-object v0, p0, Lcom/carrieriq/iqagent/client/metrics/mg/MG15;->szMmsTransId:Ljava/lang/String;

    #@16
    invoke-virtual {p0, p1, v0}, Lcom/carrieriq/iqagent/client/metrics/mg/MG15;->szStringOut(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    #@19
    .line 75
    iget-object v0, p0, Lcom/carrieriq/iqagent/client/metrics/mg/MG15;->szMmsMsgId:Ljava/lang/String;

    #@1b
    invoke-virtual {p0, p1, v0}, Lcom/carrieriq/iqagent/client/metrics/mg/MG15;->szStringOut(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    #@1e
    .line 76
    iget-object v0, p0, Lcom/carrieriq/iqagent/client/metrics/mg/MG15;->szLocationUrl:Ljava/lang/String;

    #@20
    invoke-virtual {p0, p1, v0}, Lcom/carrieriq/iqagent/client/metrics/mg/MG15;->szStringOut(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    #@23
    .line 77
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    #@26
    move-result v0

    #@27
    return v0
.end method
