.class public Lcom/carrieriq/iqagent/client/metrics/gs/GS46;
.super Lcom/carrieriq/iqagent/client/Metric;
.source "GS46.java"


# static fields
.field public static final ID:I


# instance fields
.field public ucAccessTech:B

.field public ucFieldValidity:B

.field public ucFreqBand:B

.field public ucRAC:B

.field public wCellId:S

.field public wLAC:S

.field public wMCC:S

.field public wMNC:S

.field public wRNCId:S


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 53
    const-string v0, "GS46"

    #@2
    invoke-static {v0}, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->idFromString(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    sput v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ID:I

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 66
    sget v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ID:I

    #@2
    invoke-direct {p0, v0}, Lcom/carrieriq/iqagent/client/Metric;-><init>(I)V

    #@5
    .line 67
    const/4 v0, 0x0

    #@6
    iput-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ucFieldValidity:B

    #@8
    .line 68
    return-void
.end method

.method public constructor <init>(BB)V
    .registers 4
    .parameter "tech"
    .parameter "band"

    #@0
    .prologue
    .line 71
    sget v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ID:I

    #@2
    invoke-direct {p0, v0}, Lcom/carrieriq/iqagent/client/Metric;-><init>(I)V

    #@5
    .line 72
    const/4 v0, 0x0

    #@6
    iput-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ucFieldValidity:B

    #@8
    .line 73
    iput-byte p1, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ucAccessTech:B

    #@a
    .line 74
    iput-byte p2, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ucFreqBand:B

    #@c
    .line 75
    return-void
.end method


# virtual methods
.method public clearData()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 148
    iput-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ucFieldValidity:B

    #@3
    .line 149
    iput-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ucRAC:B

    #@5
    .line 150
    iput-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ucAccessTech:B

    #@7
    .line 151
    iput-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ucFreqBand:B

    #@9
    .line 152
    iput-short v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->wMCC:S

    #@b
    .line 153
    iput-short v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->wMNC:S

    #@d
    .line 154
    iput-short v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->wCellId:S

    #@f
    .line 155
    iput-short v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->wLAC:S

    #@11
    .line 156
    iput-short v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->wRNCId:S

    #@13
    .line 157
    return-void
.end method

.method public compareTo(Lcom/carrieriq/iqagent/client/metrics/gs/GS46;)Z
    .registers 5
    .parameter "otherGs46"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 90
    if-nez p1, :cond_4

    #@3
    .line 106
    :cond_3
    :goto_3
    return v0

    #@4
    .line 91
    :cond_4
    iget-byte v1, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ucFieldValidity:B

    #@6
    iget-byte v2, p1, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ucFieldValidity:B

    #@8
    if-ne v1, v2, :cond_3

    #@a
    .line 92
    iget-byte v1, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ucAccessTech:B

    #@c
    iget-byte v2, p1, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ucAccessTech:B

    #@e
    if-ne v1, v2, :cond_3

    #@10
    .line 93
    iget-byte v1, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ucFreqBand:B

    #@12
    iget-byte v2, p1, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ucFreqBand:B

    #@14
    if-ne v1, v2, :cond_3

    #@16
    .line 94
    iget-byte v1, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ucFieldValidity:B

    #@18
    and-int/lit8 v1, v1, 0x10

    #@1a
    if-eqz v1, :cond_22

    #@1c
    iget-byte v1, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ucRAC:B

    #@1e
    iget-byte v2, p1, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ucRAC:B

    #@20
    if-ne v1, v2, :cond_3

    #@22
    .line 96
    :cond_22
    iget-byte v1, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ucFieldValidity:B

    #@24
    and-int/lit8 v1, v1, 0x20

    #@26
    if-eqz v1, :cond_2e

    #@28
    iget-short v1, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->wRNCId:S

    #@2a
    iget-short v2, p1, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->wRNCId:S

    #@2c
    if-ne v1, v2, :cond_3

    #@2e
    .line 98
    :cond_2e
    iget-byte v1, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ucFieldValidity:B

    #@30
    and-int/lit8 v1, v1, 0x1

    #@32
    if-eqz v1, :cond_3a

    #@34
    iget-short v1, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->wMCC:S

    #@36
    iget-short v2, p1, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->wMCC:S

    #@38
    if-ne v1, v2, :cond_3

    #@3a
    .line 100
    :cond_3a
    iget-byte v1, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ucFieldValidity:B

    #@3c
    and-int/lit8 v1, v1, 0x2

    #@3e
    if-eqz v1, :cond_46

    #@40
    iget-short v1, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->wMNC:S

    #@42
    iget-short v2, p1, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->wMNC:S

    #@44
    if-ne v1, v2, :cond_3

    #@46
    .line 102
    :cond_46
    iget-byte v1, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ucFieldValidity:B

    #@48
    and-int/lit8 v1, v1, 0x4

    #@4a
    if-eqz v1, :cond_52

    #@4c
    iget-short v1, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->wLAC:S

    #@4e
    iget-short v2, p1, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->wLAC:S

    #@50
    if-ne v1, v2, :cond_3

    #@52
    .line 104
    :cond_52
    iget-byte v1, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ucFieldValidity:B

    #@54
    and-int/lit8 v1, v1, 0x8

    #@56
    if-eqz v1, :cond_5e

    #@58
    iget-short v1, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->wCellId:S

    #@5a
    iget-short v2, p1, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->wCellId:S

    #@5c
    if-ne v1, v2, :cond_3

    #@5e
    .line 106
    :cond_5e
    const/4 v0, 0x1

    #@5f
    goto :goto_3
.end method

.method public dupe(Lcom/carrieriq/iqagent/client/metrics/gs/GS46;)V
    .registers 3
    .parameter "in"

    #@0
    .prologue
    .line 78
    iget-byte v0, p1, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ucFieldValidity:B

    #@2
    iput-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ucFieldValidity:B

    #@4
    .line 79
    iget-byte v0, p1, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ucRAC:B

    #@6
    iput-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ucRAC:B

    #@8
    .line 80
    iget-byte v0, p1, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ucAccessTech:B

    #@a
    iput-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ucAccessTech:B

    #@c
    .line 81
    iget-byte v0, p1, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ucFreqBand:B

    #@e
    iput-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ucFreqBand:B

    #@10
    .line 82
    iget-short v0, p1, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->wMCC:S

    #@12
    iput-short v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->wMCC:S

    #@14
    .line 83
    iget-short v0, p1, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->wMNC:S

    #@16
    iput-short v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->wMNC:S

    #@18
    .line 84
    iget-short v0, p1, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->wLAC:S

    #@1a
    iput-short v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->wLAC:S

    #@1c
    .line 85
    iget-short v0, p1, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->wCellId:S

    #@1e
    iput-short v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->wCellId:S

    #@20
    .line 86
    iget-short v0, p1, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->wRNCId:S

    #@22
    iput-short v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->wRNCId:S

    #@24
    .line 87
    return-void
.end method

.method public serialize(Ljava/nio/ByteBuffer;)I
    .registers 3
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/BufferOverflowException;
        }
    .end annotation

    #@0
    .prologue
    .line 160
    iget-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ucFieldValidity:B

    #@2
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@5
    .line 161
    iget-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ucRAC:B

    #@7
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@a
    .line 162
    iget-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ucAccessTech:B

    #@c
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@f
    .line 163
    iget-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ucFreqBand:B

    #@11
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@14
    .line 164
    iget-short v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->wMCC:S

    #@16
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    #@19
    .line 165
    iget-short v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->wMNC:S

    #@1b
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    #@1e
    .line 166
    iget-short v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->wLAC:S

    #@20
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    #@23
    .line 167
    iget-short v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->wCellId:S

    #@25
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    #@28
    .line 168
    iget-short v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->wRNCId:S

    #@2a
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    #@2d
    .line 169
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    #@30
    move-result v0

    #@31
    return v0
.end method

.method public setCellId(S)V
    .registers 3
    .parameter "cellId"

    #@0
    .prologue
    .line 135
    iget-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ucFieldValidity:B

    #@2
    or-int/lit8 v0, v0, 0x8

    #@4
    int-to-byte v0, v0

    #@5
    iput-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ucFieldValidity:B

    #@7
    .line 136
    iput-short p1, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->wCellId:S

    #@9
    .line 137
    return-void
.end method

.method public setLAC(S)V
    .registers 3
    .parameter "lac"

    #@0
    .prologue
    .line 130
    iget-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ucFieldValidity:B

    #@2
    or-int/lit8 v0, v0, 0x4

    #@4
    int-to-byte v0, v0

    #@5
    iput-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ucFieldValidity:B

    #@7
    .line 131
    iput-short p1, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->wLAC:S

    #@9
    .line 132
    return-void
.end method

.method public setMCC(S)V
    .registers 3
    .parameter "mcc"

    #@0
    .prologue
    .line 120
    iget-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ucFieldValidity:B

    #@2
    or-int/lit8 v0, v0, 0x1

    #@4
    int-to-byte v0, v0

    #@5
    iput-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ucFieldValidity:B

    #@7
    .line 121
    iput-short p1, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->wMCC:S

    #@9
    .line 122
    return-void
.end method

.method public setMNC(S)V
    .registers 3
    .parameter "mnc"

    #@0
    .prologue
    .line 125
    iget-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ucFieldValidity:B

    #@2
    or-int/lit8 v0, v0, 0x2

    #@4
    int-to-byte v0, v0

    #@5
    iput-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ucFieldValidity:B

    #@7
    .line 126
    iput-short p1, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->wMNC:S

    #@9
    .line 127
    return-void
.end method

.method public setRAC(B)V
    .registers 3
    .parameter "rac"

    #@0
    .prologue
    .line 110
    iget-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ucFieldValidity:B

    #@2
    or-int/lit8 v0, v0, 0x10

    #@4
    int-to-byte v0, v0

    #@5
    iput-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ucFieldValidity:B

    #@7
    .line 111
    iput-byte p1, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ucRAC:B

    #@9
    .line 112
    return-void
.end method

.method public setRNCId(B)V
    .registers 3
    .parameter "rncId"

    #@0
    .prologue
    .line 115
    iget-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ucFieldValidity:B

    #@2
    or-int/lit8 v0, v0, 0x20

    #@4
    int-to-byte v0, v0

    #@5
    iput-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ucFieldValidity:B

    #@7
    .line 116
    int-to-short v0, p1

    #@8
    iput-short v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->wRNCId:S

    #@a
    .line 117
    return-void
.end method

.method public setRoaming(B)V
    .registers 3
    .parameter "roaming"

    #@0
    .prologue
    .line 140
    if-nez p1, :cond_a

    #@2
    .line 141
    iget-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ucFieldValidity:B

    #@4
    and-int/lit16 v0, v0, -0x81

    #@6
    int-to-byte v0, v0

    #@7
    iput-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ucFieldValidity:B

    #@9
    .line 145
    :goto_9
    return-void

    #@a
    .line 143
    :cond_a
    iget-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ucFieldValidity:B

    #@c
    or-int/lit16 v0, v0, 0x80

    #@e
    int-to-byte v0, v0

    #@f
    iput-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS46;->ucFieldValidity:B

    #@11
    goto :goto_9
.end method
