.class public Lcom/carrieriq/iqagent/client/metrics/ui/UI2A;
.super Lcom/carrieriq/iqagent/client/Metric;
.source "UI2A.java"


# static fields
.field public static final ID:I


# instance fields
.field public dwAttemptId:I

.field public dwSize:I

.field public szAppName:Ljava/lang/String;

.field public szAppVersion:Ljava/lang/String;

.field public szInstallSource:Ljava/lang/String;

.field public ucAppType:B


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 54
    const-string v0, "UI2A"

    #@2
    invoke-static {v0}, Lcom/carrieriq/iqagent/client/metrics/ui/UI2A;->idFromString(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    sput v0, Lcom/carrieriq/iqagent/client/metrics/ui/UI2A;->ID:I

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 64
    sget v0, Lcom/carrieriq/iqagent/client/metrics/ui/UI2A;->ID:I

    #@2
    invoke-direct {p0, v0}, Lcom/carrieriq/iqagent/client/Metric;-><init>(I)V

    #@5
    .line 65
    return-void
.end method


# virtual methods
.method public serialize(Ljava/nio/ByteBuffer;)I
    .registers 3
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/BufferOverflowException;
        }
    .end annotation

    #@0
    .prologue
    .line 69
    iget v0, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI2A;->dwAttemptId:I

    #@2
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@5
    .line 70
    iget v0, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI2A;->dwSize:I

    #@7
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@a
    .line 71
    iget-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI2A;->ucAppType:B

    #@c
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@f
    .line 72
    iget-object v0, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI2A;->szAppName:Ljava/lang/String;

    #@11
    invoke-virtual {p0, p1, v0}, Lcom/carrieriq/iqagent/client/metrics/ui/UI2A;->szStringOut(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    #@14
    .line 73
    iget-object v0, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI2A;->szAppVersion:Ljava/lang/String;

    #@16
    invoke-virtual {p0, p1, v0}, Lcom/carrieriq/iqagent/client/metrics/ui/UI2A;->szStringOut(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    #@19
    .line 74
    iget-object v0, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI2A;->szInstallSource:Ljava/lang/String;

    #@1b
    invoke-virtual {p0, p1, v0}, Lcom/carrieriq/iqagent/client/metrics/ui/UI2A;->szStringOut(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    #@1e
    .line 75
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    #@21
    move-result v0

    #@22
    return v0
.end method
