.class public Lcom/carrieriq/iqagent/client/metrics/ui/UI13;
.super Lcom/carrieriq/iqagent/client/Metric;
.source "UI13.java"


# static fields
.field public static final ID:I


# instance fields
.field public dwAppID:I

.field public szAppName:Ljava/lang/String;

.field public ucAppType:B


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 54
    const-string v0, "UI13"

    #@2
    invoke-static {v0}, Lcom/carrieriq/iqagent/client/metrics/ui/UI13;->idFromString(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    sput v0, Lcom/carrieriq/iqagent/client/metrics/ui/UI13;->ID:I

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 61
    sget v0, Lcom/carrieriq/iqagent/client/metrics/ui/UI13;->ID:I

    #@2
    invoke-direct {p0, v0}, Lcom/carrieriq/iqagent/client/Metric;-><init>(I)V

    #@5
    .line 62
    return-void
.end method

.method public constructor <init>(IBLjava/lang/String;)V
    .registers 5
    .parameter "appInstanceId"
    .parameter "appType"
    .parameter "appName"

    #@0
    .prologue
    .line 65
    sget v0, Lcom/carrieriq/iqagent/client/metrics/ui/UI13;->ID:I

    #@2
    invoke-direct {p0, v0}, Lcom/carrieriq/iqagent/client/Metric;-><init>(I)V

    #@5
    .line 66
    iput p1, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI13;->dwAppID:I

    #@7
    .line 67
    iput-byte p2, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI13;->ucAppType:B

    #@9
    .line 68
    iput-object p3, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI13;->szAppName:Ljava/lang/String;

    #@b
    .line 69
    return-void
.end method


# virtual methods
.method public serialize(Ljava/nio/ByteBuffer;)I
    .registers 3
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/BufferOverflowException;
        }
    .end annotation

    #@0
    .prologue
    .line 72
    iget v0, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI13;->dwAppID:I

    #@2
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@5
    .line 73
    iget-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI13;->ucAppType:B

    #@7
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@a
    .line 74
    iget-object v0, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI13;->szAppName:Ljava/lang/String;

    #@c
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    #@f
    move-result-object v0

    #@10
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    #@13
    .line 75
    const/4 v0, 0x0

    #@14
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@17
    .line 76
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    #@1a
    move-result v0

    #@1b
    return v0
.end method
