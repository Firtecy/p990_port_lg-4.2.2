.class public Lcom/carrieriq/iqagent/client/metrics/gs/GS01;
.super Lcom/carrieriq/iqagent/client/Metric;
.source "GS01.java"


# static fields
.field public static final ID:I


# instance fields
.field public dwCallId:I

.field public szNumber:Ljava/lang/String;

.field public ucCallAttr:B

.field public ucCallState:B


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 54
    const-string v0, "GS01"

    #@2
    invoke-static {v0}, Lcom/carrieriq/iqagent/client/metrics/gs/GS01;->idFromString(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    sput v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS01;->ID:I

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 62
    sget v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS01;->ID:I

    #@2
    invoke-direct {p0, v0}, Lcom/carrieriq/iqagent/client/Metric;-><init>(I)V

    #@5
    .line 63
    return-void
.end method

.method public constructor <init>(IBLjava/lang/String;)V
    .registers 5
    .parameter "callId"
    .parameter "callState"
    .parameter "number"

    #@0
    .prologue
    .line 82
    sget v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS01;->ID:I

    #@2
    invoke-direct {p0, v0}, Lcom/carrieriq/iqagent/client/Metric;-><init>(I)V

    #@5
    .line 83
    iput p1, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS01;->dwCallId:I

    #@7
    .line 84
    const/4 v0, 0x0

    #@8
    iput-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS01;->ucCallAttr:B

    #@a
    .line 85
    iput-byte p2, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS01;->ucCallState:B

    #@c
    .line 86
    iput-object p3, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS01;->szNumber:Ljava/lang/String;

    #@e
    .line 87
    return-void
.end method


# virtual methods
.method public serialize(Ljava/nio/ByteBuffer;)I
    .registers 3
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/BufferOverflowException;
        }
    .end annotation

    #@0
    .prologue
    .line 91
    iget v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS01;->dwCallId:I

    #@2
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@5
    .line 92
    iget-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS01;->ucCallAttr:B

    #@7
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@a
    .line 93
    iget-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS01;->ucCallState:B

    #@c
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@f
    .line 94
    iget-object v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS01;->szNumber:Ljava/lang/String;

    #@11
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    #@14
    move-result-object v0

    #@15
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    #@18
    .line 95
    const/4 v0, 0x0

    #@19
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@1c
    .line 96
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    #@1f
    move-result v0

    #@20
    return v0
.end method

.method public setOriginated()V
    .registers 2

    #@0
    .prologue
    .line 70
    iget-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS01;->ucCallAttr:B

    #@2
    and-int/lit16 v0, v0, 0xfe

    #@4
    int-to-byte v0, v0

    #@5
    iput-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS01;->ucCallAttr:B

    #@7
    .line 71
    return-void
.end method

.method public setTerminated()V
    .registers 2

    #@0
    .prologue
    .line 66
    iget-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS01;->ucCallAttr:B

    #@2
    or-int/lit8 v0, v0, 0x1

    #@4
    int-to-byte v0, v0

    #@5
    iput-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS01;->ucCallAttr:B

    #@7
    .line 67
    return-void
.end method

.method public setVideo()V
    .registers 2

    #@0
    .prologue
    .line 78
    iget-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS01;->ucCallAttr:B

    #@2
    or-int/lit8 v0, v0, 0x2

    #@4
    int-to-byte v0, v0

    #@5
    iput-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS01;->ucCallAttr:B

    #@7
    .line 79
    return-void
.end method

.method public setVoice()V
    .registers 2

    #@0
    .prologue
    .line 74
    iget-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS01;->ucCallAttr:B

    #@2
    and-int/lit16 v0, v0, 0xfd

    #@4
    int-to-byte v0, v0

    #@5
    iput-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS01;->ucCallAttr:B

    #@7
    .line 75
    return-void
.end method
