.class public Lcom/carrieriq/iqagent/client/metrics/mg/MG14;
.super Lcom/carrieriq/iqagent/client/Metric;
.source "MG14.java"


# static fields
.field public static final ID:I


# instance fields
.field public dwErrorCode:I

.field public szLocationUrl:Ljava/lang/String;

.field public szMmsTransId:Ljava/lang/String;

.field public ucRetryCount:B

.field public ucState:B

.field public wResultCode:S


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 54
    const-string v0, "MG14"

    #@2
    invoke-static {v0}, Lcom/carrieriq/iqagent/client/metrics/mg/MG14;->idFromString(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    sput v0, Lcom/carrieriq/iqagent/client/metrics/mg/MG14;->ID:I

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 65
    sget v0, Lcom/carrieriq/iqagent/client/metrics/mg/MG14;->ID:I

    #@2
    invoke-direct {p0, v0}, Lcom/carrieriq/iqagent/client/Metric;-><init>(I)V

    #@5
    .line 66
    return-void
.end method


# virtual methods
.method public serialize(Ljava/nio/ByteBuffer;)I
    .registers 3
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/BufferOverflowException;
        }
    .end annotation

    #@0
    .prologue
    .line 69
    iget-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/mg/MG14;->ucState:B

    #@2
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@5
    .line 70
    iget-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/mg/MG14;->ucRetryCount:B

    #@7
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@a
    .line 71
    iget-short v0, p0, Lcom/carrieriq/iqagent/client/metrics/mg/MG14;->wResultCode:S

    #@c
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    #@f
    .line 72
    iget v0, p0, Lcom/carrieriq/iqagent/client/metrics/mg/MG14;->dwErrorCode:I

    #@11
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@14
    .line 73
    iget-object v0, p0, Lcom/carrieriq/iqagent/client/metrics/mg/MG14;->szMmsTransId:Ljava/lang/String;

    #@16
    invoke-virtual {p0, p1, v0}, Lcom/carrieriq/iqagent/client/metrics/mg/MG14;->szStringOut(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    #@19
    .line 74
    iget-object v0, p0, Lcom/carrieriq/iqagent/client/metrics/mg/MG14;->szLocationUrl:Ljava/lang/String;

    #@1b
    invoke-virtual {p0, p1, v0}, Lcom/carrieriq/iqagent/client/metrics/mg/MG14;->szStringOut(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    #@1e
    .line 75
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    #@21
    move-result v0

    #@22
    return v0
.end method
