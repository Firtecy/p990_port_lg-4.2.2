.class public Lcom/carrieriq/iqagent/client/metrics/ui/UI36;
.super Lcom/carrieriq/iqagent/client/Metric;
.source "UI36.java"


# static fields
.field public static final ID:I


# instance fields
.field public dwElapsedCpu:I

.field public dwMaxMemoryUsed:I

.field public dwRunAppID:I

.field public wRunProcesses:S


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 54
    const-string v0, "UI36"

    #@2
    invoke-static {v0}, Lcom/carrieriq/iqagent/client/metrics/ui/UI36;->idFromString(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    sput v0, Lcom/carrieriq/iqagent/client/metrics/ui/UI36;->ID:I

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 62
    sget v0, Lcom/carrieriq/iqagent/client/metrics/ui/UI36;->ID:I

    #@2
    invoke-direct {p0, v0}, Lcom/carrieriq/iqagent/client/Metric;-><init>(I)V

    #@5
    .line 63
    return-void
.end method


# virtual methods
.method public serialize(Ljava/nio/ByteBuffer;)I
    .registers 3
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/BufferOverflowException;
        }
    .end annotation

    #@0
    .prologue
    .line 66
    iget v0, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI36;->dwRunAppID:I

    #@2
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@5
    .line 67
    iget v0, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI36;->dwElapsedCpu:I

    #@7
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@a
    .line 68
    iget v0, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI36;->dwMaxMemoryUsed:I

    #@c
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@f
    .line 69
    iget-short v0, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI36;->wRunProcesses:S

    #@11
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    #@14
    .line 70
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    #@17
    move-result v0

    #@18
    return v0
.end method
