.class public Lcom/carrieriq/iqagent/client/metrics/mg/MG12;
.super Lcom/carrieriq/iqagent/client/Metric;
.source "MG12.java"


# static fields
.field public static final ID:I


# instance fields
.field public dwErrorCode:I

.field public szMmsMsgId:Ljava/lang/String;

.field public szMmsTransId:Ljava/lang/String;

.field public wMmsVersion:S

.field public wResultCode:S


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 54
    const-string v0, "MG12"

    #@2
    invoke-static {v0}, Lcom/carrieriq/iqagent/client/metrics/mg/MG12;->idFromString(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    sput v0, Lcom/carrieriq/iqagent/client/metrics/mg/MG12;->ID:I

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 64
    sget v0, Lcom/carrieriq/iqagent/client/metrics/mg/MG12;->ID:I

    #@2
    invoke-direct {p0, v0}, Lcom/carrieriq/iqagent/client/Metric;-><init>(I)V

    #@5
    .line 65
    return-void
.end method


# virtual methods
.method public serialize(Ljava/nio/ByteBuffer;)I
    .registers 3
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/BufferOverflowException;
        }
    .end annotation

    #@0
    .prologue
    .line 68
    iget v0, p0, Lcom/carrieriq/iqagent/client/metrics/mg/MG12;->dwErrorCode:I

    #@2
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@5
    .line 69
    iget-short v0, p0, Lcom/carrieriq/iqagent/client/metrics/mg/MG12;->wResultCode:S

    #@7
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    #@a
    .line 70
    iget-short v0, p0, Lcom/carrieriq/iqagent/client/metrics/mg/MG12;->wMmsVersion:S

    #@c
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    #@f
    .line 71
    iget-object v0, p0, Lcom/carrieriq/iqagent/client/metrics/mg/MG12;->szMmsTransId:Ljava/lang/String;

    #@11
    invoke-virtual {p0, p1, v0}, Lcom/carrieriq/iqagent/client/metrics/mg/MG12;->szStringOut(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    #@14
    .line 72
    iget-object v0, p0, Lcom/carrieriq/iqagent/client/metrics/mg/MG12;->szMmsMsgId:Ljava/lang/String;

    #@16
    invoke-virtual {p0, p1, v0}, Lcom/carrieriq/iqagent/client/metrics/mg/MG12;->szStringOut(Ljava/nio/ByteBuffer;Ljava/lang/String;)V

    #@19
    .line 73
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    #@1c
    move-result v0

    #@1d
    return v0
.end method
