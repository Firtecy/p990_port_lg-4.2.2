.class public Lcom/carrieriq/iqagent/client/metrics/ui/UI1F;
.super Lcom/carrieriq/iqagent/client/Metric;
.source "UI1F.java"


# static fields
.field public static final ID:I


# instance fields
.field public dwDuration:I

.field public dwRunAppID:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 54
    const-string v0, "UI1F"

    #@2
    invoke-static {v0}, Lcom/carrieriq/iqagent/client/metrics/ui/UI1F;->idFromString(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    sput v0, Lcom/carrieriq/iqagent/client/metrics/ui/UI1F;->ID:I

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 60
    sget v0, Lcom/carrieriq/iqagent/client/metrics/ui/UI1F;->ID:I

    #@2
    invoke-direct {p0, v0}, Lcom/carrieriq/iqagent/client/Metric;-><init>(I)V

    #@5
    .line 61
    return-void
.end method

.method public constructor <init>(II)V
    .registers 4
    .parameter "appInstanceId"
    .parameter "duration"

    #@0
    .prologue
    .line 64
    sget v0, Lcom/carrieriq/iqagent/client/metrics/ui/UI1F;->ID:I

    #@2
    invoke-direct {p0, v0}, Lcom/carrieriq/iqagent/client/Metric;-><init>(I)V

    #@5
    .line 65
    iput p1, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI1F;->dwRunAppID:I

    #@7
    .line 66
    iput p2, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI1F;->dwDuration:I

    #@9
    .line 67
    return-void
.end method


# virtual methods
.method public serialize(Ljava/nio/ByteBuffer;)I
    .registers 3
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/BufferOverflowException;
        }
    .end annotation

    #@0
    .prologue
    .line 70
    iget v0, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI1F;->dwRunAppID:I

    #@2
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@5
    .line 71
    iget v0, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI1F;->dwDuration:I

    #@7
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@a
    .line 72
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    #@d
    move-result v0

    #@e
    return v0
.end method
