.class public Lcom/carrieriq/iqagent/client/metrics/ui/UI04;
.super Lcom/carrieriq/iqagent/client/Metric;
.source "UI04.java"


# static fields
.field public static final ID:I


# instance fields
.field public strAppURI:Ljava/lang/String;

.field public ucAppType:B


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 54
    const-string v0, "UI04"

    #@2
    invoke-static {v0}, Lcom/carrieriq/iqagent/client/metrics/ui/UI04;->idFromString(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    sput v0, Lcom/carrieriq/iqagent/client/metrics/ui/UI04;->ID:I

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 60
    sget v0, Lcom/carrieriq/iqagent/client/metrics/ui/UI04;->ID:I

    #@2
    invoke-direct {p0, v0}, Lcom/carrieriq/iqagent/client/Metric;-><init>(I)V

    #@5
    .line 61
    return-void
.end method

.method public constructor <init>(BLjava/lang/String;)V
    .registers 4
    .parameter "_ucAppType"
    .parameter "_strAppURI"

    #@0
    .prologue
    .line 64
    sget v0, Lcom/carrieriq/iqagent/client/metrics/ui/UI04;->ID:I

    #@2
    invoke-direct {p0, v0}, Lcom/carrieriq/iqagent/client/Metric;-><init>(I)V

    #@5
    .line 65
    iput-byte p1, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI04;->ucAppType:B

    #@7
    .line 66
    iput-object p2, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI04;->strAppURI:Ljava/lang/String;

    #@9
    .line 67
    return-void
.end method


# virtual methods
.method public serialize(Ljava/nio/ByteBuffer;)I
    .registers 3
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/BufferOverflowException;
        }
    .end annotation

    #@0
    .prologue
    .line 70
    iget-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI04;->ucAppType:B

    #@2
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@5
    .line 71
    iget-object v0, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI04;->strAppURI:Ljava/lang/String;

    #@7
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    #@a
    move-result-object v0

    #@b
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    #@e
    .line 72
    const/4 v0, 0x0

    #@f
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@12
    .line 73
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    #@15
    move-result v0

    #@16
    return v0
.end method
