.class public Lcom/carrieriq/iqagent/client/metrics/gs/GS19;
.super Lcom/carrieriq/iqagent/client/Metric;
.source "GS19.java"


# static fields
.field public static final ID:I


# instance fields
.field public dwAssocContextId:I

.field public dwContextId:I

.field public szAPN:Ljava/lang/String;

.field public ucInitiator:B

.field public ucNSAPI:B

.field public ucOrdinal:B

.field public ucSAPI:B


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 54
    const-string v0, "GS19"

    #@2
    invoke-static {v0}, Lcom/carrieriq/iqagent/client/metrics/gs/GS19;->idFromString(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    sput v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS19;->ID:I

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 65
    sget v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS19;->ID:I

    #@2
    invoke-direct {p0, v0}, Lcom/carrieriq/iqagent/client/Metric;-><init>(I)V

    #@5
    .line 62
    const-string v0, ""

    #@7
    iput-object v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS19;->szAPN:Ljava/lang/String;

    #@9
    .line 66
    return-void
.end method

.method public constructor <init>(IIBBBLjava/lang/String;)V
    .registers 8
    .parameter "context"
    .parameter "assocContext"
    .parameter "ordinal"
    .parameter "nsapi"
    .parameter "sapi"
    .parameter "apn"

    #@0
    .prologue
    .line 69
    sget v0, Lcom/carrieriq/iqagent/client/metrics/gs/GS19;->ID:I

    #@2
    invoke-direct {p0, v0}, Lcom/carrieriq/iqagent/client/Metric;-><init>(I)V

    #@5
    .line 62
    const-string v0, ""

    #@7
    iput-object v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS19;->szAPN:Ljava/lang/String;

    #@9
    .line 70
    iput p1, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS19;->dwContextId:I

    #@b
    .line 71
    iput p2, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS19;->dwAssocContextId:I

    #@d
    .line 72
    iput-byte p4, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS19;->ucNSAPI:B

    #@f
    .line 73
    iput-byte p5, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS19;->ucSAPI:B

    #@11
    .line 74
    iput-object p6, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS19;->szAPN:Ljava/lang/String;

    #@13
    .line 75
    return-void
.end method


# virtual methods
.method public serialize(Ljava/nio/ByteBuffer;)I
    .registers 3
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/BufferOverflowException;
        }
    .end annotation

    #@0
    .prologue
    .line 79
    iget v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS19;->dwContextId:I

    #@2
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@5
    .line 80
    iget v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS19;->dwAssocContextId:I

    #@7
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@a
    .line 81
    iget-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS19;->ucOrdinal:B

    #@c
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@f
    .line 82
    iget-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS19;->ucInitiator:B

    #@11
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@14
    .line 83
    iget-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS19;->ucNSAPI:B

    #@16
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@19
    .line 84
    iget-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS19;->ucSAPI:B

    #@1b
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@1e
    .line 85
    iget-object v0, p0, Lcom/carrieriq/iqagent/client/metrics/gs/GS19;->szAPN:Ljava/lang/String;

    #@20
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    #@23
    move-result-object v0

    #@24
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    #@27
    .line 86
    const/4 v0, 0x0

    #@28
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@2b
    .line 87
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    #@2e
    move-result v0

    #@2f
    return v0
.end method
