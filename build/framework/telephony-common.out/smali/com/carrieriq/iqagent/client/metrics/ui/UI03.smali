.class public Lcom/carrieriq/iqagent/client/metrics/ui/UI03;
.super Lcom/carrieriq/iqagent/client/Metric;
.source "UI03.java"


# static fields
.field public static final ID:I


# instance fields
.field public dwErrorCode:I

.field public szErrorMsg:Ljava/lang/String;

.field public ucAppType:B

.field public ucLevel:B


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 54
    const-string v0, "UI03"

    #@2
    invoke-static {v0}, Lcom/carrieriq/iqagent/client/metrics/ui/UI03;->idFromString(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    sput v0, Lcom/carrieriq/iqagent/client/metrics/ui/UI03;->ID:I

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 62
    sget v0, Lcom/carrieriq/iqagent/client/metrics/ui/UI03;->ID:I

    #@2
    invoke-direct {p0, v0}, Lcom/carrieriq/iqagent/client/Metric;-><init>(I)V

    #@5
    .line 63
    return-void
.end method


# virtual methods
.method public serialize(Ljava/nio/ByteBuffer;)I
    .registers 3
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/BufferOverflowException;
        }
    .end annotation

    #@0
    .prologue
    .line 66
    iget v0, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI03;->dwErrorCode:I

    #@2
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@5
    .line 67
    iget-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI03;->ucAppType:B

    #@7
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@a
    .line 68
    iget-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI03;->ucLevel:B

    #@c
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@f
    .line 69
    iget-object v0, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI03;->szErrorMsg:Ljava/lang/String;

    #@11
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    #@14
    move-result-object v0

    #@15
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    #@18
    .line 70
    const/4 v0, 0x0

    #@19
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@1c
    .line 71
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    #@1f
    move-result v0

    #@20
    return v0
.end method
