.class public Lcom/carrieriq/iqagent/client/metrics/ui/UI09;
.super Lcom/carrieriq/iqagent/client/Metric;
.source "UI09.java"


# static fields
#the value of this static final field might be set in the static constructor
.field public static final ID:I = 0x0

.field public static final IQ_BATTERY_UNKNOWN:B = -0x1t


# instance fields
.field public ucBatteryPercentage:B


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 54
    const-string v0, "UI09"

    #@2
    invoke-static {v0}, Lcom/carrieriq/iqagent/client/metrics/ui/UI09;->idFromString(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    sput v0, Lcom/carrieriq/iqagent/client/metrics/ui/UI09;->ID:I

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 61
    sget v0, Lcom/carrieriq/iqagent/client/metrics/ui/UI09;->ID:I

    #@2
    invoke-direct {p0, v0}, Lcom/carrieriq/iqagent/client/Metric;-><init>(I)V

    #@5
    .line 62
    const/4 v0, -0x1

    #@6
    iput-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI09;->ucBatteryPercentage:B

    #@8
    .line 63
    return-void
.end method

.method public constructor <init>(B)V
    .registers 3
    .parameter "_ucBatteryPercentage"

    #@0
    .prologue
    .line 66
    sget v0, Lcom/carrieriq/iqagent/client/metrics/ui/UI09;->ID:I

    #@2
    invoke-direct {p0, v0}, Lcom/carrieriq/iqagent/client/Metric;-><init>(I)V

    #@5
    .line 67
    iput-byte p1, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI09;->ucBatteryPercentage:B

    #@7
    .line 68
    return-void
.end method

.method public constructor <init>(BB)V
    .registers 4
    .parameter "_ucBatteryDisplay"
    .parameter "_ucBatteryPercentage"

    #@0
    .prologue
    .line 71
    sget v0, Lcom/carrieriq/iqagent/client/metrics/ui/UI09;->ID:I

    #@2
    invoke-direct {p0, v0}, Lcom/carrieriq/iqagent/client/Metric;-><init>(I)V

    #@5
    .line 72
    iput-byte p2, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI09;->ucBatteryPercentage:B

    #@7
    .line 73
    return-void
.end method


# virtual methods
.method public serialize(Ljava/nio/ByteBuffer;)I
    .registers 3
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/BufferOverflowException;
        }
    .end annotation

    #@0
    .prologue
    .line 76
    const/4 v0, -0x1

    #@1
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@4
    .line 77
    iget-byte v0, p0, Lcom/carrieriq/iqagent/client/metrics/ui/UI09;->ucBatteryPercentage:B

    #@6
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@9
    .line 78
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    #@c
    move-result v0

    #@d
    return v0
.end method
