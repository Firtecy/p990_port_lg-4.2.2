.class public final enum Lcom/movial/ipphone/IPUtils$EmergencyState;
.super Ljava/lang/Enum;
.source "IPUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/movial/ipphone/IPUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EmergencyState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/movial/ipphone/IPUtils$EmergencyState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/movial/ipphone/IPUtils$EmergencyState;

.field public static final enum CS_CALL_CONNECTED:Lcom/movial/ipphone/IPUtils$EmergencyState;

.field public static final enum CS_CALL_DIALING:Lcom/movial/ipphone/IPUtils$EmergencyState;

.field public static final enum CS_CALL_DISCONNECTED:Lcom/movial/ipphone/IPUtils$EmergencyState;

.field public static final enum CS_CALL_FAILED:Lcom/movial/ipphone/IPUtils$EmergencyState;

.field public static final enum CS_TURNING_ON_RADIO:Lcom/movial/ipphone/IPUtils$EmergencyState;

.field public static final enum IDLE:Lcom/movial/ipphone/IPUtils$EmergencyState;

.field public static final enum IMS_CALL_CONNECTED:Lcom/movial/ipphone/IPUtils$EmergencyState;

.field public static final enum IMS_CALL_DIALING:Lcom/movial/ipphone/IPUtils$EmergencyState;

.field public static final enum IMS_CALL_DISCONNECTED:Lcom/movial/ipphone/IPUtils$EmergencyState;

.field public static final enum IMS_CALL_FAILED:Lcom/movial/ipphone/IPUtils$EmergencyState;

.field public static final enum NOT_INITIALIZED:Lcom/movial/ipphone/IPUtils$EmergencyState;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 129
    new-instance v0, Lcom/movial/ipphone/IPUtils$EmergencyState;

    #@7
    const-string v1, "NOT_INITIALIZED"

    #@9
    invoke-direct {v0, v1, v3}, Lcom/movial/ipphone/IPUtils$EmergencyState;-><init>(Ljava/lang/String;I)V

    #@c
    sput-object v0, Lcom/movial/ipphone/IPUtils$EmergencyState;->NOT_INITIALIZED:Lcom/movial/ipphone/IPUtils$EmergencyState;

    #@e
    .line 130
    new-instance v0, Lcom/movial/ipphone/IPUtils$EmergencyState;

    #@10
    const-string v1, "IDLE"

    #@12
    invoke-direct {v0, v1, v4}, Lcom/movial/ipphone/IPUtils$EmergencyState;-><init>(Ljava/lang/String;I)V

    #@15
    sput-object v0, Lcom/movial/ipphone/IPUtils$EmergencyState;->IDLE:Lcom/movial/ipphone/IPUtils$EmergencyState;

    #@17
    .line 131
    new-instance v0, Lcom/movial/ipphone/IPUtils$EmergencyState;

    #@19
    const-string v1, "CS_TURNING_ON_RADIO"

    #@1b
    invoke-direct {v0, v1, v5}, Lcom/movial/ipphone/IPUtils$EmergencyState;-><init>(Ljava/lang/String;I)V

    #@1e
    sput-object v0, Lcom/movial/ipphone/IPUtils$EmergencyState;->CS_TURNING_ON_RADIO:Lcom/movial/ipphone/IPUtils$EmergencyState;

    #@20
    .line 132
    new-instance v0, Lcom/movial/ipphone/IPUtils$EmergencyState;

    #@22
    const-string v1, "CS_CALL_DIALING"

    #@24
    invoke-direct {v0, v1, v6}, Lcom/movial/ipphone/IPUtils$EmergencyState;-><init>(Ljava/lang/String;I)V

    #@27
    sput-object v0, Lcom/movial/ipphone/IPUtils$EmergencyState;->CS_CALL_DIALING:Lcom/movial/ipphone/IPUtils$EmergencyState;

    #@29
    .line 133
    new-instance v0, Lcom/movial/ipphone/IPUtils$EmergencyState;

    #@2b
    const-string v1, "CS_CALL_CONNECTED"

    #@2d
    invoke-direct {v0, v1, v7}, Lcom/movial/ipphone/IPUtils$EmergencyState;-><init>(Ljava/lang/String;I)V

    #@30
    sput-object v0, Lcom/movial/ipphone/IPUtils$EmergencyState;->CS_CALL_CONNECTED:Lcom/movial/ipphone/IPUtils$EmergencyState;

    #@32
    .line 134
    new-instance v0, Lcom/movial/ipphone/IPUtils$EmergencyState;

    #@34
    const-string v1, "CS_CALL_DISCONNECTED"

    #@36
    const/4 v2, 0x5

    #@37
    invoke-direct {v0, v1, v2}, Lcom/movial/ipphone/IPUtils$EmergencyState;-><init>(Ljava/lang/String;I)V

    #@3a
    sput-object v0, Lcom/movial/ipphone/IPUtils$EmergencyState;->CS_CALL_DISCONNECTED:Lcom/movial/ipphone/IPUtils$EmergencyState;

    #@3c
    .line 135
    new-instance v0, Lcom/movial/ipphone/IPUtils$EmergencyState;

    #@3e
    const-string v1, "CS_CALL_FAILED"

    #@40
    const/4 v2, 0x6

    #@41
    invoke-direct {v0, v1, v2}, Lcom/movial/ipphone/IPUtils$EmergencyState;-><init>(Ljava/lang/String;I)V

    #@44
    sput-object v0, Lcom/movial/ipphone/IPUtils$EmergencyState;->CS_CALL_FAILED:Lcom/movial/ipphone/IPUtils$EmergencyState;

    #@46
    .line 136
    new-instance v0, Lcom/movial/ipphone/IPUtils$EmergencyState;

    #@48
    const-string v1, "IMS_CALL_DIALING"

    #@4a
    const/4 v2, 0x7

    #@4b
    invoke-direct {v0, v1, v2}, Lcom/movial/ipphone/IPUtils$EmergencyState;-><init>(Ljava/lang/String;I)V

    #@4e
    sput-object v0, Lcom/movial/ipphone/IPUtils$EmergencyState;->IMS_CALL_DIALING:Lcom/movial/ipphone/IPUtils$EmergencyState;

    #@50
    .line 137
    new-instance v0, Lcom/movial/ipphone/IPUtils$EmergencyState;

    #@52
    const-string v1, "IMS_CALL_CONNECTED"

    #@54
    const/16 v2, 0x8

    #@56
    invoke-direct {v0, v1, v2}, Lcom/movial/ipphone/IPUtils$EmergencyState;-><init>(Ljava/lang/String;I)V

    #@59
    sput-object v0, Lcom/movial/ipphone/IPUtils$EmergencyState;->IMS_CALL_CONNECTED:Lcom/movial/ipphone/IPUtils$EmergencyState;

    #@5b
    .line 138
    new-instance v0, Lcom/movial/ipphone/IPUtils$EmergencyState;

    #@5d
    const-string v1, "IMS_CALL_DISCONNECTED"

    #@5f
    const/16 v2, 0x9

    #@61
    invoke-direct {v0, v1, v2}, Lcom/movial/ipphone/IPUtils$EmergencyState;-><init>(Ljava/lang/String;I)V

    #@64
    sput-object v0, Lcom/movial/ipphone/IPUtils$EmergencyState;->IMS_CALL_DISCONNECTED:Lcom/movial/ipphone/IPUtils$EmergencyState;

    #@66
    .line 139
    new-instance v0, Lcom/movial/ipphone/IPUtils$EmergencyState;

    #@68
    const-string v1, "IMS_CALL_FAILED"

    #@6a
    const/16 v2, 0xa

    #@6c
    invoke-direct {v0, v1, v2}, Lcom/movial/ipphone/IPUtils$EmergencyState;-><init>(Ljava/lang/String;I)V

    #@6f
    sput-object v0, Lcom/movial/ipphone/IPUtils$EmergencyState;->IMS_CALL_FAILED:Lcom/movial/ipphone/IPUtils$EmergencyState;

    #@71
    .line 128
    const/16 v0, 0xb

    #@73
    new-array v0, v0, [Lcom/movial/ipphone/IPUtils$EmergencyState;

    #@75
    sget-object v1, Lcom/movial/ipphone/IPUtils$EmergencyState;->NOT_INITIALIZED:Lcom/movial/ipphone/IPUtils$EmergencyState;

    #@77
    aput-object v1, v0, v3

    #@79
    sget-object v1, Lcom/movial/ipphone/IPUtils$EmergencyState;->IDLE:Lcom/movial/ipphone/IPUtils$EmergencyState;

    #@7b
    aput-object v1, v0, v4

    #@7d
    sget-object v1, Lcom/movial/ipphone/IPUtils$EmergencyState;->CS_TURNING_ON_RADIO:Lcom/movial/ipphone/IPUtils$EmergencyState;

    #@7f
    aput-object v1, v0, v5

    #@81
    sget-object v1, Lcom/movial/ipphone/IPUtils$EmergencyState;->CS_CALL_DIALING:Lcom/movial/ipphone/IPUtils$EmergencyState;

    #@83
    aput-object v1, v0, v6

    #@85
    sget-object v1, Lcom/movial/ipphone/IPUtils$EmergencyState;->CS_CALL_CONNECTED:Lcom/movial/ipphone/IPUtils$EmergencyState;

    #@87
    aput-object v1, v0, v7

    #@89
    const/4 v1, 0x5

    #@8a
    sget-object v2, Lcom/movial/ipphone/IPUtils$EmergencyState;->CS_CALL_DISCONNECTED:Lcom/movial/ipphone/IPUtils$EmergencyState;

    #@8c
    aput-object v2, v0, v1

    #@8e
    const/4 v1, 0x6

    #@8f
    sget-object v2, Lcom/movial/ipphone/IPUtils$EmergencyState;->CS_CALL_FAILED:Lcom/movial/ipphone/IPUtils$EmergencyState;

    #@91
    aput-object v2, v0, v1

    #@93
    const/4 v1, 0x7

    #@94
    sget-object v2, Lcom/movial/ipphone/IPUtils$EmergencyState;->IMS_CALL_DIALING:Lcom/movial/ipphone/IPUtils$EmergencyState;

    #@96
    aput-object v2, v0, v1

    #@98
    const/16 v1, 0x8

    #@9a
    sget-object v2, Lcom/movial/ipphone/IPUtils$EmergencyState;->IMS_CALL_CONNECTED:Lcom/movial/ipphone/IPUtils$EmergencyState;

    #@9c
    aput-object v2, v0, v1

    #@9e
    const/16 v1, 0x9

    #@a0
    sget-object v2, Lcom/movial/ipphone/IPUtils$EmergencyState;->IMS_CALL_DISCONNECTED:Lcom/movial/ipphone/IPUtils$EmergencyState;

    #@a2
    aput-object v2, v0, v1

    #@a4
    const/16 v1, 0xa

    #@a6
    sget-object v2, Lcom/movial/ipphone/IPUtils$EmergencyState;->IMS_CALL_FAILED:Lcom/movial/ipphone/IPUtils$EmergencyState;

    #@a8
    aput-object v2, v0, v1

    #@aa
    sput-object v0, Lcom/movial/ipphone/IPUtils$EmergencyState;->$VALUES:[Lcom/movial/ipphone/IPUtils$EmergencyState;

    #@ac
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 128
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/movial/ipphone/IPUtils$EmergencyState;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 128
    const-class v0, Lcom/movial/ipphone/IPUtils$EmergencyState;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/movial/ipphone/IPUtils$EmergencyState;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/movial/ipphone/IPUtils$EmergencyState;
    .registers 1

    #@0
    .prologue
    .line 128
    sget-object v0, Lcom/movial/ipphone/IPUtils$EmergencyState;->$VALUES:[Lcom/movial/ipphone/IPUtils$EmergencyState;

    #@2
    invoke-virtual {v0}, [Lcom/movial/ipphone/IPUtils$EmergencyState;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/movial/ipphone/IPUtils$EmergencyState;

    #@8
    return-object v0
.end method
