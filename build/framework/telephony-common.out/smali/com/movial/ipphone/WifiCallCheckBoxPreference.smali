.class public Lcom/movial/ipphone/WifiCallCheckBoxPreference;
.super Landroid/preference/CheckBoxPreference;
.source "WifiCallCheckBoxPreference.java"


# static fields
.field private static final EVENT_IMS_WIFI_STATUS:I = 0x1

.field private static final TAG:Ljava/lang/String; = "WifiCallCheckBoxPreference"


# instance fields
.field private mCellOnly:Z

.field private final mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mPreference:Landroid/preference/Preference;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 79
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Lcom/movial/ipphone/WifiCallCheckBoxPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 80
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 83
    const v0, 0x101008f

    #@3
    invoke-direct {p0, p1, p2, v0}, Lcom/movial/ipphone/WifiCallCheckBoxPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@6
    .line 84
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    .line 87
    invoke-direct {p0, p1, p2, p3}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@3
    .line 65
    new-instance v0, Lcom/movial/ipphone/WifiCallCheckBoxPreference$1;

    #@5
    invoke-direct {v0, p0}, Lcom/movial/ipphone/WifiCallCheckBoxPreference$1;-><init>(Lcom/movial/ipphone/WifiCallCheckBoxPreference;)V

    #@8
    iput-object v0, p0, Lcom/movial/ipphone/WifiCallCheckBoxPreference;->mHandler:Landroid/os/Handler;

    #@a
    .line 88
    iput-object p1, p0, Lcom/movial/ipphone/WifiCallCheckBoxPreference;->mContext:Landroid/content/Context;

    #@c
    .line 90
    return-void
.end method

.method static synthetic access$000(Lcom/movial/ipphone/WifiCallCheckBoxPreference;)Landroid/preference/Preference;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 54
    iget-object v0, p0, Lcom/movial/ipphone/WifiCallCheckBoxPreference;->mPreference:Landroid/preference/Preference;

    #@2
    return-object v0
.end method

.method private registerToIPRegistry(Z)V
    .registers 2
    .parameter "register"

    #@0
    .prologue
    .line 116
    return-void
.end method


# virtual methods
.method protected onClick()V
    .registers 4

    #@0
    .prologue
    .line 94
    invoke-super {p0}, Landroid/preference/CheckBoxPreference;->onClick()V

    #@3
    .line 95
    const-string v0, "WifiCallCheckBoxPreference"

    #@5
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v2, "onClick. "

    #@c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {p0}, Lcom/movial/ipphone/WifiCallCheckBoxPreference;->isChecked()Z

    #@13
    move-result v2

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 97
    invoke-virtual {p0}, Lcom/movial/ipphone/WifiCallCheckBoxPreference;->isChecked()Z

    #@22
    move-result v0

    #@23
    if-nez v0, :cond_29

    #@25
    const/4 v0, 0x1

    #@26
    :goto_26
    iput-boolean v0, p0, Lcom/movial/ipphone/WifiCallCheckBoxPreference;->mCellOnly:Z

    #@28
    .line 100
    return-void

    #@29
    .line 97
    :cond_29
    const/4 v0, 0x0

    #@2a
    goto :goto_26
.end method

.method public pause()V
    .registers 2

    #@0
    .prologue
    .line 111
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, v0}, Lcom/movial/ipphone/WifiCallCheckBoxPreference;->registerToIPRegistry(Z)V

    #@4
    .line 112
    return-void
.end method

.method public resume()V
    .registers 1

    #@0
    .prologue
    .line 108
    return-void
.end method

.method public setValues(Landroid/preference/Preference;)V
    .registers 2
    .parameter "preference"

    #@0
    .prologue
    .line 103
    iput-object p1, p0, Lcom/movial/ipphone/WifiCallCheckBoxPreference;->mPreference:Landroid/preference/Preference;

    #@2
    .line 104
    return-void
.end method
