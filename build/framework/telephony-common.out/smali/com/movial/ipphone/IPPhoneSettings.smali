.class public Lcom/movial/ipphone/IPPhoneSettings;
.super Ljava/lang/Object;
.source "IPPhoneSettings.java"


# static fields
.field public static final CELL_ONLY:Ljava/lang/String; = "CELL_ONLY"

.field public static final CLIP:Ljava/lang/String; = "CLIP"

.field public static final CONTENT_URI:Landroid/net/Uri; = null

.field public static final ECM:Ljava/lang/String; = "ECM"

.field public static final FIRST_WIFI_CALL:Ljava/lang/String; = "FIRST_WIFI_CALL"

.field public static final GBA_INIT:Ljava/lang/String; = "GBA_INIT"

.field public static final PREFERRED_OPTION:Ljava/lang/String; = "PREFERRED_OPTION"

.field public static final ROVE_IN:Ljava/lang/String; = "ROVE_IN"

.field public static final ROVE_OUT:Ljava/lang/String; = "ROVE_OUT"

.field public static final ROVE_THRESHOLD:Ljava/lang/String; = "ROVE_THRESHOLD"

.field private static TAG:Ljava/lang/String; = null

.field public static final WIFI_FIRST_CONNECTED:Ljava/lang/String; = "WIFI_FIRST_CONNECTED"

.field public static final WIFI_FIRST_TURNON:Ljava/lang/String; = "WIFI_FIRST_TURNON"

.field public static final WIFI_SETTINGS_FIRST_LAUNCHED:Ljava/lang/String; = "WIFI_SETTINGS_FIRST_LAUNCHED"


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 51
    const-string v0, "IPPhoneSettings"

    #@2
    sput-object v0, Lcom/movial/ipphone/IPPhoneSettings;->TAG:Ljava/lang/String;

    #@4
    .line 53
    const-string v0, "content://ipprovider/ipphonesettings"

    #@6
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@9
    move-result-object v0

    #@a
    sput-object v0, Lcom/movial/ipphone/IPPhoneSettings;->CONTENT_URI:Landroid/net/Uri;

    #@c
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 49
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z
    .registers 6
    .parameter "resolver"
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, -0x1

    #@2
    .line 113
    invoke-static {p0, p1, v2}, Lcom/movial/ipphone/IPPhoneSettings;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@5
    move-result v0

    #@6
    .line 114
    .local v0, i:I
    if-ne v0, v2, :cond_9

    #@8
    .line 115
    .end local p2
    :goto_8
    return p2

    #@9
    .restart local p2
    :cond_9
    if-ne v0, v1, :cond_d

    #@b
    :goto_b
    move p2, v1

    #@c
    goto :goto_8

    #@d
    :cond_d
    const/4 v1, 0x0

    #@e
    goto :goto_b
.end method

.method public static getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I
    .registers 5
    .parameter "resolver"
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 101
    const/4 v1, 0x0

    #@1
    invoke-static {p0, p1, v1}, Lcom/movial/ipphone/IPPhoneSettings;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@4
    move-result-object v0

    #@5
    .line 102
    .local v0, s:Ljava/lang/String;
    if-nez v0, :cond_8

    #@7
    .line 105
    .end local p2
    :goto_7
    return p2

    #@8
    .restart local p2
    :cond_8
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@b
    move-result p2

    #@c
    goto :goto_7
.end method

.method public static getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 11
    .parameter "resolver"
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 83
    const/4 v6, 0x0

    #@1
    .line 85
    .local v6, c:Landroid/database/Cursor;
    :try_start_1
    sget-object v1, Lcom/movial/ipphone/IPPhoneSettings;->CONTENT_URI:Landroid/net/Uri;

    #@3
    const/4 v0, 0x1

    #@4
    new-array v2, v0, [Ljava/lang/String;

    #@6
    const/4 v0, 0x0

    #@7
    const-string v3, "value"

    #@9
    aput-object v3, v2, v0

    #@b
    const-string v3, "name = ?"

    #@d
    const/4 v0, 0x1

    #@e
    new-array v4, v0, [Ljava/lang/String;

    #@10
    const/4 v0, 0x0

    #@11
    aput-object p1, v4, v0

    #@13
    const/4 v5, 0x0

    #@14
    move-object v0, p0

    #@15
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@18
    move-result-object v6

    #@19
    .line 86
    if-eqz v6, :cond_26

    #@1b
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    #@1e
    move-result v0

    #@1f
    if-eqz v0, :cond_26

    #@21
    .line 87
    const/4 v0, 0x0

    #@22
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_25
    .catchall {:try_start_1 .. :try_end_25} :catchall_39
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_25} :catch_2c

    #@25
    move-result-object p2

    #@26
    .line 91
    :cond_26
    if-eqz v6, :cond_2b

    #@28
    :goto_28
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@2b
    .line 93
    :cond_2b
    return-object p2

    #@2c
    .line 88
    :catch_2c
    move-exception v7

    #@2d
    .line 89
    .local v7, e:Ljava/lang/Exception;
    :try_start_2d
    sget-object v0, Lcom/movial/ipphone/IPPhoneSettings;->TAG:Ljava/lang/String;

    #@2f
    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@32
    move-result-object v1

    #@33
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_36
    .catchall {:try_start_2d .. :try_end_36} :catchall_39

    #@36
    .line 91
    if-eqz v6, :cond_2b

    #@38
    goto :goto_28

    #@39
    .end local v7           #e:Ljava/lang/Exception;
    :catchall_39
    move-exception v0

    #@3a
    if-eqz v6, :cond_3f

    #@3c
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@3f
    :cond_3f
    throw v0
.end method

.method public static putBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z
    .registers 4
    .parameter "resolver"
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 109
    if-eqz p2, :cond_8

    #@2
    const/4 v0, 0x1

    #@3
    :goto_3
    invoke-static {p0, p1, v0}, Lcom/movial/ipphone/IPPhoneSettings;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@6
    move-result v0

    #@7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_3
.end method

.method public static putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    .registers 4
    .parameter "resolver"
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 97
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-static {p0, p1, v0}, Lcom/movial/ipphone/IPPhoneSettings;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public static putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z
    .registers 7
    .parameter "resolver"
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 71
    :try_start_0
    new-instance v1, Landroid/content/ContentValues;

    #@2
    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    #@5
    .line 72
    .local v1, values:Landroid/content/ContentValues;
    const-string v2, "name"

    #@7
    invoke-virtual {v1, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 73
    const-string v2, "value"

    #@c
    invoke-virtual {v1, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@f
    .line 74
    sget-object v2, Lcom/movial/ipphone/IPPhoneSettings;->CONTENT_URI:Landroid/net/Uri;

    #@11
    invoke-virtual {p0, v2, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_14} :catch_16

    #@14
    .line 75
    const/4 v2, 0x1

    #@15
    .line 78
    .end local v1           #values:Landroid/content/ContentValues;
    :goto_15
    return v2

    #@16
    .line 76
    :catch_16
    move-exception v0

    #@17
    .line 77
    .local v0, e:Ljava/lang/Exception;
    sget-object v2, Lcom/movial/ipphone/IPPhoneSettings;->TAG:Ljava/lang/String;

    #@19
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@1c
    move-result-object v3

    #@1d
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 78
    const/4 v2, 0x0

    #@21
    goto :goto_15
.end method
