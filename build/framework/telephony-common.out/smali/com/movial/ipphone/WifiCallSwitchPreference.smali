.class public Lcom/movial/ipphone/WifiCallSwitchPreference;
.super Landroid/preference/SwitchPreference;
.source "WifiCallSwitchPreference.java"


# static fields
.field private static final EVENT_IMS_WIFI_STATUS:I = 0x1

.field private static final TAG:Ljava/lang/String; = "WifiCallSwitchPreference"


# instance fields
.field private mCellOnly:Z

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 72
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Lcom/movial/ipphone/WifiCallSwitchPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 73
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 76
    const v0, 0x101036d

    #@3
    invoke-direct {p0, p1, p2, v0}, Lcom/movial/ipphone/WifiCallSwitchPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@6
    .line 77
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    #@0
    .prologue
    .line 80
    invoke-direct {p0, p1, p2, p3}, Landroid/preference/SwitchPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    #@3
    .line 81
    iput-object p1, p0, Lcom/movial/ipphone/WifiCallSwitchPreference;->mContext:Landroid/content/Context;

    #@5
    .line 84
    const-string v0, "On"

    #@7
    invoke-virtual {p0, v0}, Lcom/movial/ipphone/WifiCallSwitchPreference;->setSwitchTextOn(Ljava/lang/CharSequence;)V

    #@a
    .line 85
    const-string v0, "Off"

    #@c
    invoke-virtual {p0, v0}, Lcom/movial/ipphone/WifiCallSwitchPreference;->setSwitchTextOff(Ljava/lang/CharSequence;)V

    #@f
    .line 86
    const-string v0, "Wi-Fi Calling"

    #@11
    invoke-virtual {p0, v0}, Lcom/movial/ipphone/WifiCallSwitchPreference;->setTitle(Ljava/lang/CharSequence;)V

    #@14
    .line 87
    return-void
.end method

.method private registerToIPRegistry(Z)V
    .registers 2
    .parameter "register"

    #@0
    .prologue
    .line 115
    return-void
.end method


# virtual methods
.method protected onBindView(Landroid/view/View;)V
    .registers 2
    .parameter "view"

    #@0
    .prologue
    .line 91
    invoke-super {p0, p1}, Landroid/preference/SwitchPreference;->onBindView(Landroid/view/View;)V

    #@3
    .line 93
    return-void
.end method

.method protected onClick()V
    .registers 1

    #@0
    .prologue
    .line 97
    invoke-super {p0}, Landroid/preference/SwitchPreference;->onClick()V

    #@3
    .line 99
    return-void
.end method

.method public onSwitchClicked()V
    .registers 1

    #@0
    .prologue
    .line 103
    return-void
.end method

.method public pause()V
    .registers 1

    #@0
    .prologue
    .line 111
    return-void
.end method

.method public resume()V
    .registers 1

    #@0
    .prologue
    .line 107
    return-void
.end method
