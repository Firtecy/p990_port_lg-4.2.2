.class public Lcom/movial/ipphone/IPPhoneProxy;
.super Lcom/android/internal/telephony/PhoneProxy;
.source "IPPhoneProxy.java"


# instance fields
.field private mActivePhone:Lcom/android/internal/telephony/Phone;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;Lcom/android/internal/telephony/PhoneNotifier;)V
    .registers 4
    .parameter "context"
    .parameter "ci"
    .parameter "notifier"

    #@0
    .prologue
    .line 87
    invoke-direct {p0}, Lcom/android/internal/telephony/PhoneProxy;-><init>()V

    #@3
    .line 88
    return-void
.end method

.method public constructor <init>(Lcom/android/internal/telephony/Phone;)V
    .registers 2
    .parameter "phone"

    #@0
    .prologue
    .line 83
    invoke-direct {p0}, Lcom/android/internal/telephony/PhoneProxy;-><init>()V

    #@3
    .line 84
    return-void
.end method

.method public static getIPPhoneProfile()I
    .registers 1

    #@0
    .prologue
    .line 134
    const/4 v0, -0x1

    #@1
    return v0
.end method

.method public static getIPPhoneServiceState()Z
    .registers 1

    #@0
    .prologue
    .line 113
    const/4 v0, 0x0

    #@1
    return v0
.end method


# virtual methods
.method public getEmergencyState()Lcom/movial/ipphone/IPUtils$EmergencyState;
    .registers 2

    #@0
    .prologue
    .line 107
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public getImsBackgroundCall()Lcom/android/internal/telephony/Call;
    .registers 2

    #@0
    .prologue
    .line 99
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public getImsForegroundCall()Lcom/android/internal/telephony/Call;
    .registers 2

    #@0
    .prologue
    .line 95
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public getImsRingingCall()Lcom/android/internal/telephony/Call;
    .registers 2

    #@0
    .prologue
    .line 103
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public hangupFakeCall(Z)V
    .registers 2
    .parameter "dialed"

    #@0
    .prologue
    .line 124
    return-void
.end method

.method public registerForOn(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 4
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 118
    return-void
.end method

.method public setRadioPower(Z)V
    .registers 2
    .parameter "power"

    #@0
    .prologue
    .line 92
    return-void
.end method

.method public startImsEmergencyCall()V
    .registers 1

    #@0
    .prologue
    .line 129
    return-void
.end method

.method public unregisterForOn(Landroid/os/Handler;)V
    .registers 2
    .parameter "h"

    #@0
    .prologue
    .line 120
    return-void
.end method
