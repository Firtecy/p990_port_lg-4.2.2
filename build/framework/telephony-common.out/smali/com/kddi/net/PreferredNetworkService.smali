.class public Lcom/kddi/net/PreferredNetworkService;
.super Landroid/app/Service;
.source "PreferredNetworkService.java"


# static fields
.field private static final ACTION_PREFERRED_NETWORK_GET:Ljava/lang/String; = "GetNetworkMode_KDDI_LTE"

.field private static final ACTION_PREFERRED_NETWORK_SET:Ljava/lang/String; = "SetNetworkMode_KDDI_LTE"

.field private static final EXTRA_NATWORK_RESPONSE:Ljava/lang/String; = "response"

.field private static final EXTRA_NETWORK_TYPE:Ljava/lang/String; = "NetworkType"

.field protected static final LOG_TAG:Ljava/lang/String; = "RIL_PreferredNetworkService"

.field public static final NT_MODE_DEFAULT:I = 0x0

.field public static final NT_MODE_LTEOFF:I = 0x1

.field public static final PREFERRED_OTHER_ERROR:I = -0x2

.field public static final PREFERRED_REQUEST_SUCCESS:I = 0x0

.field public static final PREFERRED_SETTING_TYPE_ERROR:I = -0x1

.field public static preNetworkType:I


# instance fields
.field public lteOffNetworkType:[I

.field public mAction:Ljava/lang/Boolean;

.field private final mBinder:Lcom/kddi/net/IPreferredNetworkService$Stub;

.field public mContext:Landroid/content/Context;

.field public mResponse:Landroid/os/Message;

.field private myBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private final permName:Ljava/lang/String;

.field preferredNetworkMode:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 47
    const/4 v0, 0x0

    #@1
    sput v0, Lcom/kddi/net/PreferredNetworkService;->preNetworkType:I

    #@3
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 25
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    #@3
    .line 28
    const-string v0, "com.kddi.android.permission.PREFERRED_NETWORK_SETTINGS"

    #@5
    iput-object v0, p0, Lcom/kddi/net/PreferredNetworkService;->permName:Ljava/lang/String;

    #@7
    .line 33
    const/16 v0, 0xa

    #@9
    iput v0, p0, Lcom/kddi/net/PreferredNetworkService;->preferredNetworkMode:I

    #@b
    .line 48
    const/4 v0, 0x7

    #@c
    new-array v0, v0, [I

    #@e
    fill-array-data v0, :array_2a

    #@11
    iput-object v0, p0, Lcom/kddi/net/PreferredNetworkService;->lteOffNetworkType:[I

    #@13
    .line 53
    const/4 v0, 0x1

    #@14
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@17
    move-result-object v0

    #@18
    iput-object v0, p0, Lcom/kddi/net/PreferredNetworkService;->mAction:Ljava/lang/Boolean;

    #@1a
    .line 73
    new-instance v0, Lcom/kddi/net/PreferredNetworkService$1;

    #@1c
    invoke-direct {v0, p0}, Lcom/kddi/net/PreferredNetworkService$1;-><init>(Lcom/kddi/net/PreferredNetworkService;)V

    #@1f
    iput-object v0, p0, Lcom/kddi/net/PreferredNetworkService;->myBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@21
    .line 117
    new-instance v0, Lcom/kddi/net/PreferredNetworkService$2;

    #@23
    invoke-direct {v0, p0}, Lcom/kddi/net/PreferredNetworkService$2;-><init>(Lcom/kddi/net/PreferredNetworkService;)V

    #@26
    iput-object v0, p0, Lcom/kddi/net/PreferredNetworkService;->mBinder:Lcom/kddi/net/IPreferredNetworkService$Stub;

    #@28
    return-void

    #@29
    .line 48
    nop

    #@2a
    :array_2a
    .array-data 0x4
        0x1t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
        0x6t 0x0t 0x0t 0x0t
        0x7t 0x0t 0x0t 0x0t
    .end array-data
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .registers 4
    .parameter "intent"

    #@0
    .prologue
    .line 68
    const-string v0, "RIL_PreferredNetworkService"

    #@2
    const-string v1, "binding service implementation"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 69
    iget-object v0, p0, Lcom/kddi/net/PreferredNetworkService;->mBinder:Lcom/kddi/net/IPreferredNetworkService$Stub;

    #@9
    return-object v0
.end method

.method public onCreate()V
    .registers 4

    #@0
    .prologue
    .line 57
    new-instance v0, Landroid/content/IntentFilter;

    #@2
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@5
    .line 58
    .local v0, intentFilter:Landroid/content/IntentFilter;
    const-string v1, "SetNetWorkMode"

    #@7
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@a
    .line 59
    const-string v1, "GetNetWorkMode"

    #@c
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@f
    .line 60
    invoke-virtual {p0}, Lcom/kddi/net/PreferredNetworkService;->getApplicationContext()Landroid/content/Context;

    #@12
    move-result-object v1

    #@13
    iget-object v2, p0, Lcom/kddi/net/PreferredNetworkService;->myBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@15
    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@18
    .line 61
    return-void
.end method
