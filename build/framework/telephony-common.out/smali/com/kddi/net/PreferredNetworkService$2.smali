.class Lcom/kddi/net/PreferredNetworkService$2;
.super Lcom/kddi/net/IPreferredNetworkService$Stub;
.source "PreferredNetworkService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kddi/net/PreferredNetworkService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/kddi/net/PreferredNetworkService;


# direct methods
.method constructor <init>(Lcom/kddi/net/PreferredNetworkService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 117
    iput-object p1, p0, Lcom/kddi/net/PreferredNetworkService$2;->this$0:Lcom/kddi/net/PreferredNetworkService;

    #@2
    invoke-direct {p0}, Lcom/kddi/net/IPreferredNetworkService$Stub;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public getPreferredNetworkType(Landroid/os/Message;)I
    .registers 7
    .parameter "response"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 194
    const-string v2, "RIL_PreferredNetworkService"

    #@3
    const-string v3, "getPreferredNetworkType()"

    #@5
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 196
    iget-object v2, p0, Lcom/kddi/net/PreferredNetworkService$2;->this$0:Lcom/kddi/net/PreferredNetworkService;

    #@a
    iput-object p1, v2, Lcom/kddi/net/PreferredNetworkService;->mResponse:Landroid/os/Message;

    #@c
    .line 197
    iget-object v2, p0, Lcom/kddi/net/PreferredNetworkService$2;->this$0:Lcom/kddi/net/PreferredNetworkService;

    #@e
    invoke-virtual {v2}, Lcom/kddi/net/PreferredNetworkService;->getApplicationContext()Landroid/content/Context;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@15
    move-result-object v2

    #@16
    const-string v3, "com.kddi.android.permission.PREFERRED_NETWORK_SETTINGS"

    #@18
    iget-object v4, p0, Lcom/kddi/net/PreferredNetworkService$2;->this$0:Lcom/kddi/net/PreferredNetworkService;

    #@1a
    invoke-virtual {v4}, Lcom/kddi/net/PreferredNetworkService;->getApplicationContext()Landroid/content/Context;

    #@1d
    move-result-object v4

    #@1e
    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@21
    move-result-object v4

    #@22
    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    move-result v2

    #@26
    const/4 v3, -0x1

    #@27
    if-ne v2, v3, :cond_2b

    #@29
    .line 198
    const/4 v1, -0x2

    #@2a
    .line 205
    :cond_2a
    :goto_2a
    return v1

    #@2b
    .line 199
    :cond_2b
    iget-object v2, p0, Lcom/kddi/net/PreferredNetworkService$2;->this$0:Lcom/kddi/net/PreferredNetworkService;

    #@2d
    iget-object v2, v2, Lcom/kddi/net/PreferredNetworkService;->mAction:Ljava/lang/Boolean;

    #@2f
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    #@32
    move-result v2

    #@33
    if-eqz v2, :cond_2a

    #@35
    .line 200
    new-instance v0, Landroid/content/Intent;

    #@37
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    #@3a
    .line 201
    .local v0, intent:Landroid/content/Intent;
    const-string v2, "GetNetworkMode_KDDI_LTE"

    #@3c
    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@3f
    .line 202
    iget-object v2, p0, Lcom/kddi/net/PreferredNetworkService$2;->this$0:Lcom/kddi/net/PreferredNetworkService;

    #@41
    invoke-virtual {v2}, Lcom/kddi/net/PreferredNetworkService;->getApplicationContext()Landroid/content/Context;

    #@44
    move-result-object v2

    #@45
    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@48
    .line 203
    iget-object v2, p0, Lcom/kddi/net/PreferredNetworkService$2;->this$0:Lcom/kddi/net/PreferredNetworkService;

    #@4a
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@4d
    move-result-object v3

    #@4e
    iput-object v3, v2, Lcom/kddi/net/PreferredNetworkService;->mAction:Ljava/lang/Boolean;

    #@50
    goto :goto_2a
.end method

.method public setPreferredNetworkType(ILandroid/os/Message;)I
    .registers 16
    .parameter "networkType"
    .parameter "response"

    #@0
    .prologue
    const/4 v9, -0x1

    #@1
    const/4 v7, -0x2

    #@2
    const/4 v8, 0x0

    #@3
    .line 123
    const-string v10, "RIL_PreferredNetworkService"

    #@5
    new-instance v11, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v12, "setPreferredNetworkType()  networkType: "

    #@c
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v11

    #@10
    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v11

    #@14
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v11

    #@18
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 125
    const/16 v5, 0xa

    #@1d
    .line 126
    .local v5, setNetworkType:I
    const/16 v6, 0xa

    #@1f
    .line 127
    .local v6, settingsNetworkMode:I
    iget-object v10, p0, Lcom/kddi/net/PreferredNetworkService$2;->this$0:Lcom/kddi/net/PreferredNetworkService;

    #@21
    iput-object p2, v10, Lcom/kddi/net/PreferredNetworkService;->mResponse:Landroid/os/Message;

    #@23
    .line 129
    iget-object v10, p0, Lcom/kddi/net/PreferredNetworkService$2;->this$0:Lcom/kddi/net/PreferredNetworkService;

    #@25
    invoke-virtual {v10}, Lcom/kddi/net/PreferredNetworkService;->getApplicationContext()Landroid/content/Context;

    #@28
    move-result-object v10

    #@29
    invoke-virtual {v10}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@2c
    move-result-object v10

    #@2d
    const-string v11, "com.kddi.android.permission.PREFERRED_NETWORK_SETTINGS"

    #@2f
    iget-object v12, p0, Lcom/kddi/net/PreferredNetworkService$2;->this$0:Lcom/kddi/net/PreferredNetworkService;

    #@31
    invoke-virtual {v12}, Lcom/kddi/net/PreferredNetworkService;->getApplicationContext()Landroid/content/Context;

    #@34
    move-result-object v12

    #@35
    invoke-virtual {v12}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@38
    move-result-object v12

    #@39
    invoke-virtual {v10, v11, v12}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    #@3c
    move-result v10

    #@3d
    if-ne v10, v9, :cond_40

    #@3f
    .line 187
    :cond_3f
    :goto_3f
    return v7

    #@40
    .line 131
    :cond_40
    iget-object v10, p0, Lcom/kddi/net/PreferredNetworkService$2;->this$0:Lcom/kddi/net/PreferredNetworkService;

    #@42
    iget-object v10, v10, Lcom/kddi/net/PreferredNetworkService;->mAction:Ljava/lang/Boolean;

    #@44
    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    #@47
    move-result v10

    #@48
    if-eqz v10, :cond_c4

    #@4a
    .line 132
    packed-switch p1, :pswitch_data_138

    #@4d
    move v7, v9

    #@4e
    .line 183
    goto :goto_3f

    #@4f
    .line 134
    :pswitch_4f
    iget-object v7, p0, Lcom/kddi/net/PreferredNetworkService$2;->this$0:Lcom/kddi/net/PreferredNetworkService;

    #@51
    invoke-virtual {v7}, Lcom/kddi/net/PreferredNetworkService;->getApplicationContext()Landroid/content/Context;

    #@54
    move-result-object v7

    #@55
    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@58
    move-result-object v7

    #@59
    const-string v9, "preferred_network_mode"

    #@5b
    iget-object v10, p0, Lcom/kddi/net/PreferredNetworkService$2;->this$0:Lcom/kddi/net/PreferredNetworkService;

    #@5d
    iget v10, v10, Lcom/kddi/net/PreferredNetworkService;->preferredNetworkMode:I

    #@5f
    invoke-static {v7, v9, v10}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@62
    move-result v6

    #@63
    .line 139
    const-string v7, "RIL_PreferredNetworkService"

    #@65
    new-instance v9, Ljava/lang/StringBuilder;

    #@67
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@6a
    const-string v10, "NT_MODE_DEFAULT settingsNetworkMode: "

    #@6c
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v9

    #@70
    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@73
    move-result-object v9

    #@74
    const-string v10, "preNetworkType:"

    #@76
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v9

    #@7a
    sget v10, Lcom/kddi/net/PreferredNetworkService;->preNetworkType:I

    #@7c
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v9

    #@80
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@83
    move-result-object v9

    #@84
    invoke-static {v7, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@87
    .line 145
    sget v7, Lcom/kddi/net/PreferredNetworkService;->preNetworkType:I

    #@89
    const/16 v9, 0xa

    #@8b
    if-ne v7, v9, :cond_90

    #@8d
    const/4 v7, 0x7

    #@8e
    if-eq v6, v7, :cond_a4

    #@90
    :cond_90
    sget v7, Lcom/kddi/net/PreferredNetworkService;->preNetworkType:I

    #@92
    const/16 v9, 0x9

    #@94
    if-ne v7, v9, :cond_99

    #@96
    const/4 v7, 0x3

    #@97
    if-eq v6, v7, :cond_a4

    #@99
    :cond_99
    sget v7, Lcom/kddi/net/PreferredNetworkService;->preNetworkType:I

    #@9b
    const/16 v9, 0x8

    #@9d
    if-ne v7, v9, :cond_a2

    #@9f
    const/4 v7, 0x4

    #@a0
    if-eq v6, v7, :cond_a4

    #@a2
    :cond_a2
    move v7, v8

    #@a3
    .line 148
    goto :goto_3f

    #@a4
    .line 151
    :cond_a4
    new-instance v2, Landroid/content/Intent;

    #@a6
    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    #@a9
    .line 152
    .local v2, intent:Landroid/content/Intent;
    const-string v7, "SetNetworkMode_KDDI_LTE"

    #@ab
    invoke-virtual {v2, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@ae
    .line 153
    const-string v7, "NetworkType"

    #@b0
    invoke-virtual {v2, v7, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@b3
    .line 154
    iget-object v7, p0, Lcom/kddi/net/PreferredNetworkService$2;->this$0:Lcom/kddi/net/PreferredNetworkService;

    #@b5
    invoke-virtual {v7}, Lcom/kddi/net/PreferredNetworkService;->getApplicationContext()Landroid/content/Context;

    #@b8
    move-result-object v7

    #@b9
    invoke-virtual {v7, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@bc
    .line 155
    iget-object v7, p0, Lcom/kddi/net/PreferredNetworkService$2;->this$0:Lcom/kddi/net/PreferredNetworkService;

    #@be
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@c1
    move-result-object v9

    #@c2
    iput-object v9, v7, Lcom/kddi/net/PreferredNetworkService;->mAction:Ljava/lang/Boolean;

    #@c4
    .end local v2           #intent:Landroid/content/Intent;
    :cond_c4
    :goto_c4
    move v7, v8

    #@c5
    .line 187
    goto/16 :goto_3f

    #@c7
    .line 160
    :pswitch_c7
    iget-object v9, p0, Lcom/kddi/net/PreferredNetworkService$2;->this$0:Lcom/kddi/net/PreferredNetworkService;

    #@c9
    invoke-virtual {v9}, Lcom/kddi/net/PreferredNetworkService;->getApplicationContext()Landroid/content/Context;

    #@cc
    move-result-object v9

    #@cd
    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@d0
    move-result-object v9

    #@d1
    const-string v10, "preferred_network_mode"

    #@d3
    iget-object v11, p0, Lcom/kddi/net/PreferredNetworkService$2;->this$0:Lcom/kddi/net/PreferredNetworkService;

    #@d5
    iget v11, v11, Lcom/kddi/net/PreferredNetworkService;->preferredNetworkMode:I

    #@d7
    invoke-static {v9, v10, v11}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@da
    move-result v6

    #@db
    .line 165
    iget-object v9, p0, Lcom/kddi/net/PreferredNetworkService$2;->this$0:Lcom/kddi/net/PreferredNetworkService;

    #@dd
    iget-object v0, v9, Lcom/kddi/net/PreferredNetworkService;->lteOffNetworkType:[I

    #@df
    .local v0, arr$:[I
    array-length v3, v0

    #@e0
    .local v3, len$:I
    const/4 v1, 0x0

    #@e1
    .local v1, i$:I
    :goto_e1
    if-ge v1, v3, :cond_ed

    #@e3
    aget v4, v0, v1

    #@e5
    .line 166
    .local v4, mNetworkType:I
    if-ne v4, v6, :cond_ea

    #@e7
    move v7, v8

    #@e8
    .line 167
    goto/16 :goto_3f

    #@ea
    .line 165
    :cond_ea
    add-int/lit8 v1, v1, 0x1

    #@ec
    goto :goto_e1

    #@ed
    .line 170
    .end local v4           #mNetworkType:I
    :cond_ed
    const/16 v9, 0xb

    #@ef
    if-eq v6, v9, :cond_3f

    #@f1
    .line 173
    sput v6, Lcom/kddi/net/PreferredNetworkService;->preNetworkType:I

    #@f3
    .line 174
    const-string v7, "RIL_PreferredNetworkService"

    #@f5
    new-instance v9, Ljava/lang/StringBuilder;

    #@f7
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@fa
    const-string v10, "NT_MODE_LTEOFF settingsNetworkMode: "

    #@fc
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ff
    move-result-object v9

    #@100
    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@103
    move-result-object v9

    #@104
    const-string v10, "preNetworkType:"

    #@106
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@109
    move-result-object v9

    #@10a
    sget v10, Lcom/kddi/net/PreferredNetworkService;->preNetworkType:I

    #@10c
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10f
    move-result-object v9

    #@110
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@113
    move-result-object v9

    #@114
    invoke-static {v7, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@117
    .line 176
    new-instance v2, Landroid/content/Intent;

    #@119
    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    #@11c
    .line 177
    .restart local v2       #intent:Landroid/content/Intent;
    const-string v7, "SetNetworkMode_KDDI_LTE"

    #@11e
    invoke-virtual {v2, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@121
    .line 178
    const-string v7, "NetworkType"

    #@123
    invoke-virtual {v2, v7, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@126
    .line 179
    iget-object v7, p0, Lcom/kddi/net/PreferredNetworkService$2;->this$0:Lcom/kddi/net/PreferredNetworkService;

    #@128
    invoke-virtual {v7}, Lcom/kddi/net/PreferredNetworkService;->getApplicationContext()Landroid/content/Context;

    #@12b
    move-result-object v7

    #@12c
    invoke-virtual {v7, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@12f
    .line 180
    iget-object v7, p0, Lcom/kddi/net/PreferredNetworkService$2;->this$0:Lcom/kddi/net/PreferredNetworkService;

    #@131
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@134
    move-result-object v9

    #@135
    iput-object v9, v7, Lcom/kddi/net/PreferredNetworkService;->mAction:Ljava/lang/Boolean;

    #@137
    goto :goto_c4

    #@138
    .line 132
    :pswitch_data_138
    .packed-switch 0x0
        :pswitch_4f
        :pswitch_c7
    .end packed-switch
.end method
