.class Lcom/kddi/net/PreferredNetworkService$1;
.super Landroid/content/BroadcastReceiver;
.source "PreferredNetworkService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kddi/net/PreferredNetworkService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/kddi/net/PreferredNetworkService;


# direct methods
.method constructor <init>(Lcom/kddi/net/PreferredNetworkService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 73
    iput-object p1, p0, Lcom/kddi/net/PreferredNetworkService$1;->this$0:Lcom/kddi/net/PreferredNetworkService;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 12
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/16 v8, 0xa

    #@2
    const/4 v7, 0x1

    #@3
    const/4 v6, 0x0

    #@4
    const/4 v5, 0x0

    #@5
    .line 75
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    .line 76
    .local v0, action:Ljava/lang/String;
    const-string v2, "SetNetWorkMode"

    #@b
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e
    move-result v2

    #@f
    if-eqz v2, :cond_45

    #@11
    .line 77
    const-string v2, "RIL_PreferredNetworkService"

    #@13
    const-string v3, "myBroadcastReceiver SetNetWorkMode()"

    #@15
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 78
    iget-object v2, p0, Lcom/kddi/net/PreferredNetworkService$1;->this$0:Lcom/kddi/net/PreferredNetworkService;

    #@1a
    iget-object v2, v2, Lcom/kddi/net/PreferredNetworkService;->mResponse:Landroid/os/Message;

    #@1c
    if-eqz v2, :cond_44

    #@1e
    .line 79
    iget-object v2, p0, Lcom/kddi/net/PreferredNetworkService$1;->this$0:Lcom/kddi/net/PreferredNetworkService;

    #@20
    iget-object v2, v2, Lcom/kddi/net/PreferredNetworkService;->mResponse:Landroid/os/Message;

    #@22
    const-string v3, "exception"

    #@24
    invoke-virtual {p2, v3, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@27
    move-result v3

    #@28
    iput v3, v2, Landroid/os/Message;->arg1:I

    #@2a
    .line 80
    iget-object v2, p0, Lcom/kddi/net/PreferredNetworkService$1;->this$0:Lcom/kddi/net/PreferredNetworkService;

    #@2c
    iget-object v2, v2, Lcom/kddi/net/PreferredNetworkService;->mResponse:Landroid/os/Message;

    #@2e
    invoke-static {v2, v5, v5}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)Landroid/os/AsyncResult;

    #@31
    .line 82
    iget-object v2, p0, Lcom/kddi/net/PreferredNetworkService$1;->this$0:Lcom/kddi/net/PreferredNetworkService;

    #@33
    iget-object v2, v2, Lcom/kddi/net/PreferredNetworkService;->mResponse:Landroid/os/Message;

    #@35
    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    #@38
    .line 84
    iget-object v2, p0, Lcom/kddi/net/PreferredNetworkService$1;->this$0:Lcom/kddi/net/PreferredNetworkService;

    #@3a
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@3d
    move-result-object v3

    #@3e
    iput-object v3, v2, Lcom/kddi/net/PreferredNetworkService;->mAction:Ljava/lang/Boolean;

    #@40
    .line 85
    iget-object v2, p0, Lcom/kddi/net/PreferredNetworkService$1;->this$0:Lcom/kddi/net/PreferredNetworkService;

    #@42
    iput-object v5, v2, Lcom/kddi/net/PreferredNetworkService;->mResponse:Landroid/os/Message;

    #@44
    .line 111
    :cond_44
    :goto_44
    return-void

    #@45
    .line 87
    :cond_45
    const-string v2, "GetNetWorkMode"

    #@47
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4a
    move-result v2

    #@4b
    if-eqz v2, :cond_44

    #@4d
    .line 88
    const-string v2, "RIL_PreferredNetworkService"

    #@4f
    const-string v3, "#########################################################"

    #@51
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@54
    .line 89
    iget-object v2, p0, Lcom/kddi/net/PreferredNetworkService$1;->this$0:Lcom/kddi/net/PreferredNetworkService;

    #@56
    iget-object v2, v2, Lcom/kddi/net/PreferredNetworkService;->mResponse:Landroid/os/Message;

    #@58
    if-eqz v2, :cond_44

    #@5a
    .line 90
    const-string v2, "networkmode"

    #@5c
    invoke-virtual {p2, v2, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@5f
    move-result v1

    #@60
    .line 91
    .local v1, networkType:I
    const-string v2, "RIL_PreferredNetworkService"

    #@62
    new-instance v3, Ljava/lang/StringBuilder;

    #@64
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@67
    const-string v4, "mResponse.what"

    #@69
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v3

    #@6d
    iget-object v4, p0, Lcom/kddi/net/PreferredNetworkService$1;->this$0:Lcom/kddi/net/PreferredNetworkService;

    #@6f
    iget-object v4, v4, Lcom/kddi/net/PreferredNetworkService;->mResponse:Landroid/os/Message;

    #@71
    iget v4, v4, Landroid/os/Message;->what:I

    #@73
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@76
    move-result-object v3

    #@77
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7a
    move-result-object v3

    #@7b
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7e
    .line 92
    iget-object v2, p0, Lcom/kddi/net/PreferredNetworkService$1;->this$0:Lcom/kddi/net/PreferredNetworkService;

    #@80
    iget-object v2, v2, Lcom/kddi/net/PreferredNetworkService;->mResponse:Landroid/os/Message;

    #@82
    const-string v3, "exception"

    #@84
    invoke-virtual {p2, v3, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@87
    move-result v3

    #@88
    iput v3, v2, Landroid/os/Message;->arg1:I

    #@8a
    .line 93
    const-string v2, "RIL_PreferredNetworkService"

    #@8c
    new-instance v3, Ljava/lang/StringBuilder;

    #@8e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@91
    const-string v4, "myBroadcastReceiver GetNetWorkMode() networkType:"

    #@93
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v3

    #@97
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v3

    #@9b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9e
    move-result-object v3

    #@9f
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a2
    .line 96
    sget v2, Lcom/kddi/net/PreferredNetworkService;->preNetworkType:I

    #@a4
    if-ne v2, v8, :cond_a9

    #@a6
    const/4 v2, 0x7

    #@a7
    if-eq v1, v2, :cond_dd

    #@a9
    :cond_a9
    sget v2, Lcom/kddi/net/PreferredNetworkService;->preNetworkType:I

    #@ab
    const/16 v3, 0x9

    #@ad
    if-ne v2, v3, :cond_b2

    #@af
    const/4 v2, 0x3

    #@b0
    if-eq v1, v2, :cond_dd

    #@b2
    :cond_b2
    sget v2, Lcom/kddi/net/PreferredNetworkService;->preNetworkType:I

    #@b4
    const/16 v3, 0x8

    #@b6
    if-ne v2, v3, :cond_bb

    #@b8
    const/4 v2, 0x4

    #@b9
    if-eq v1, v2, :cond_dd

    #@bb
    .line 99
    :cond_bb
    iget-object v2, p0, Lcom/kddi/net/PreferredNetworkService$1;->this$0:Lcom/kddi/net/PreferredNetworkService;

    #@bd
    iget-object v2, v2, Lcom/kddi/net/PreferredNetworkService;->mResponse:Landroid/os/Message;

    #@bf
    iput v6, v2, Landroid/os/Message;->arg2:I

    #@c1
    .line 104
    :goto_c1
    iget-object v2, p0, Lcom/kddi/net/PreferredNetworkService$1;->this$0:Lcom/kddi/net/PreferredNetworkService;

    #@c3
    iget-object v2, v2, Lcom/kddi/net/PreferredNetworkService;->mResponse:Landroid/os/Message;

    #@c5
    invoke-static {v2, v5, v5}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)Landroid/os/AsyncResult;

    #@c8
    .line 106
    iget-object v2, p0, Lcom/kddi/net/PreferredNetworkService$1;->this$0:Lcom/kddi/net/PreferredNetworkService;

    #@ca
    iget-object v2, v2, Lcom/kddi/net/PreferredNetworkService;->mResponse:Landroid/os/Message;

    #@cc
    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    #@cf
    .line 107
    iget-object v2, p0, Lcom/kddi/net/PreferredNetworkService$1;->this$0:Lcom/kddi/net/PreferredNetworkService;

    #@d1
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@d4
    move-result-object v3

    #@d5
    iput-object v3, v2, Lcom/kddi/net/PreferredNetworkService;->mAction:Ljava/lang/Boolean;

    #@d7
    .line 108
    iget-object v2, p0, Lcom/kddi/net/PreferredNetworkService$1;->this$0:Lcom/kddi/net/PreferredNetworkService;

    #@d9
    iput-object v5, v2, Lcom/kddi/net/PreferredNetworkService;->mResponse:Landroid/os/Message;

    #@db
    goto/16 :goto_44

    #@dd
    .line 102
    :cond_dd
    iget-object v2, p0, Lcom/kddi/net/PreferredNetworkService$1;->this$0:Lcom/kddi/net/PreferredNetworkService;

    #@df
    iget-object v2, v2, Lcom/kddi/net/PreferredNetworkService;->mResponse:Landroid/os/Message;

    #@e1
    iput v7, v2, Landroid/os/Message;->arg2:I

    #@e3
    goto :goto_c1
.end method
