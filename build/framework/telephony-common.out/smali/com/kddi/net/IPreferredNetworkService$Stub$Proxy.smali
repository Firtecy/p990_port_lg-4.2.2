.class Lcom/kddi/net/IPreferredNetworkService$Stub$Proxy;
.super Ljava/lang/Object;
.source "IPreferredNetworkService.java"

# interfaces
.implements Lcom/kddi/net/IPreferredNetworkService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kddi/net/IPreferredNetworkService$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 85
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 86
    iput-object p1, p0, Lcom/kddi/net/IPreferredNetworkService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 87
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 90
    iget-object v0, p0, Lcom/kddi/net/IPreferredNetworkService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 94
    const-string v0, "com.kddi.net.IPreferredNetworkService"

    #@2
    return-object v0
.end method

.method public getPreferredNetworkType(Landroid/os/Message;)I
    .registers 8
    .parameter "response"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 125
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 126
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 129
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.kddi.net.IPreferredNetworkService"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 130
    if-eqz p1, :cond_2c

    #@f
    .line 131
    const/4 v3, 0x1

    #@10
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 132
    const/4 v3, 0x0

    #@14
    invoke-virtual {p1, v0, v3}, Landroid/os/Message;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 137
    :goto_17
    iget-object v3, p0, Lcom/kddi/net/IPreferredNetworkService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/4 v4, 0x2

    #@1a
    const/4 v5, 0x0

    #@1b
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 138
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@21
    .line 139
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_24
    .catchall {:try_start_8 .. :try_end_24} :catchall_31

    #@24
    move-result v2

    #@25
    .line 142
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 143
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 145
    return v2

    #@2c
    .line 135
    .end local v2           #_result:I
    :cond_2c
    const/4 v3, 0x0

    #@2d
    :try_start_2d
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_30
    .catchall {:try_start_2d .. :try_end_30} :catchall_31

    #@30
    goto :goto_17

    #@31
    .line 142
    :catchall_31
    move-exception v3

    #@32
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@35
    .line 143
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@38
    .line 142
    throw v3
.end method

.method public setPreferredNetworkType(ILandroid/os/Message;)I
    .registers 9
    .parameter "networkType"
    .parameter "response"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 100
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 101
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 104
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.kddi.net.IPreferredNetworkService"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 105
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 106
    if-eqz p2, :cond_2f

    #@12
    .line 107
    const/4 v3, 0x1

    #@13
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 108
    const/4 v3, 0x0

    #@17
    invoke-virtual {p2, v0, v3}, Landroid/os/Message;->writeToParcel(Landroid/os/Parcel;I)V

    #@1a
    .line 113
    :goto_1a
    iget-object v3, p0, Lcom/kddi/net/IPreferredNetworkService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1c
    const/4 v4, 0x1

    #@1d
    const/4 v5, 0x0

    #@1e
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@21
    .line 114
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@24
    .line 115
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_27
    .catchall {:try_start_8 .. :try_end_27} :catchall_34

    #@27
    move-result v2

    #@28
    .line 118
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 119
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 121
    return v2

    #@2f
    .line 111
    .end local v2           #_result:I
    :cond_2f
    const/4 v3, 0x0

    #@30
    :try_start_30
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_33
    .catchall {:try_start_30 .. :try_end_33} :catchall_34

    #@33
    goto :goto_1a

    #@34
    .line 118
    :catchall_34
    move-exception v3

    #@35
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@38
    .line 119
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3b
    .line 118
    throw v3
.end method
