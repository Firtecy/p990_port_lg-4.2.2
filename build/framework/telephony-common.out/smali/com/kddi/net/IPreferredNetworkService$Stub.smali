.class public abstract Lcom/kddi/net/IPreferredNetworkService$Stub;
.super Landroid/os/Binder;
.source "IPreferredNetworkService.java"

# interfaces
.implements Lcom/kddi/net/IPreferredNetworkService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kddi/net/IPreferredNetworkService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/kddi/net/IPreferredNetworkService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.kddi.net.IPreferredNetworkService"

.field static final TRANSACTION_getPreferredNetworkType:I = 0x2

.field static final TRANSACTION_setPreferredNetworkType:I = 0x1


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 15
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 16
    const-string v0, "com.kddi.net.IPreferredNetworkService"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/kddi/net/IPreferredNetworkService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 17
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/kddi/net/IPreferredNetworkService;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 24
    if-nez p0, :cond_4

    #@2
    .line 25
    const/4 v0, 0x0

    #@3
    .line 31
    :goto_3
    return-object v0

    #@4
    .line 27
    :cond_4
    const-string v1, "com.kddi.net.IPreferredNetworkService"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 28
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/kddi/net/IPreferredNetworkService;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 29
    check-cast v0, Lcom/kddi/net/IPreferredNetworkService;

    #@12
    goto :goto_3

    #@13
    .line 31
    :cond_13
    new-instance v0, Lcom/kddi/net/IPreferredNetworkService$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/kddi/net/IPreferredNetworkService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 35
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 10
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 39
    sparse-switch p1, :sswitch_data_54

    #@4
    .line 79
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v3

    #@8
    :goto_8
    return v3

    #@9
    .line 43
    :sswitch_9
    const-string v4, "com.kddi.net.IPreferredNetworkService"

    #@b
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    goto :goto_8

    #@f
    .line 48
    :sswitch_f
    const-string v4, "com.kddi.net.IPreferredNetworkService"

    #@11
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 50
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@17
    move-result v0

    #@18
    .line 52
    .local v0, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1b
    move-result v4

    #@1c
    if-eqz v4, :cond_31

    #@1e
    .line 53
    sget-object v4, Landroid/os/Message;->CREATOR:Landroid/os/Parcelable$Creator;

    #@20
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@23
    move-result-object v1

    #@24
    check-cast v1, Landroid/os/Message;

    #@26
    .line 58
    .local v1, _arg1:Landroid/os/Message;
    :goto_26
    invoke-virtual {p0, v0, v1}, Lcom/kddi/net/IPreferredNetworkService$Stub;->setPreferredNetworkType(ILandroid/os/Message;)I

    #@29
    move-result v2

    #@2a
    .line 59
    .local v2, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2d
    .line 60
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@30
    goto :goto_8

    #@31
    .line 56
    .end local v1           #_arg1:Landroid/os/Message;
    .end local v2           #_result:I
    :cond_31
    const/4 v1, 0x0

    #@32
    .restart local v1       #_arg1:Landroid/os/Message;
    goto :goto_26

    #@33
    .line 65
    .end local v0           #_arg0:I
    .end local v1           #_arg1:Landroid/os/Message;
    :sswitch_33
    const-string v4, "com.kddi.net.IPreferredNetworkService"

    #@35
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@38
    .line 67
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@3b
    move-result v4

    #@3c
    if-eqz v4, :cond_51

    #@3e
    .line 68
    sget-object v4, Landroid/os/Message;->CREATOR:Landroid/os/Parcelable$Creator;

    #@40
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@43
    move-result-object v0

    #@44
    check-cast v0, Landroid/os/Message;

    #@46
    .line 73
    .local v0, _arg0:Landroid/os/Message;
    :goto_46
    invoke-virtual {p0, v0}, Lcom/kddi/net/IPreferredNetworkService$Stub;->getPreferredNetworkType(Landroid/os/Message;)I

    #@49
    move-result v2

    #@4a
    .line 74
    .restart local v2       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4d
    .line 75
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@50
    goto :goto_8

    #@51
    .line 71
    .end local v0           #_arg0:Landroid/os/Message;
    .end local v2           #_result:I
    :cond_51
    const/4 v0, 0x0

    #@52
    .restart local v0       #_arg0:Landroid/os/Message;
    goto :goto_46

    #@53
    .line 39
    nop

    #@54
    :sswitch_data_54
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_33
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
