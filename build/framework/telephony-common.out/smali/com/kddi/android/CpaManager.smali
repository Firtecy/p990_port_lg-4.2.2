.class public Lcom/kddi/android/CpaManager;
.super Ljava/lang/Object;
.source "CpaManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/kddi/android/CpaManager$ConnInfo;,
        Lcom/kddi/android/CpaManager$Settings;
    }
.end annotation


# static fields
.field public static final AUTHENTICATION_ERROR:I = -0x3

.field public static final CONNECTED:I = 0x2

.field public static final CONNECTING:I = 0x1

.field public static final CONNECTIVITY_ACTION:Ljava/lang/String; = "com.kddi.android.cpa.CONNECTIVITY_CHANGE"

.field public static final DISCONNECTED:I = 0x4

.field public static final DISCONNECTING:I = 0x3

.field public static final EXTRA_CONNECTIVITY_STATUS:Ljava/lang/String; = "connStatus"

.field public static final EXTRA_ERRNO:Ljava/lang/String; = "errno"

.field public static final MODE_DEFAULT:Ljava/lang/String; = "DEFAULT"

.field public static final MODE_NAVI:Ljava/lang/String; = "NAVI"

.field public static final PARAMETER_ERROR:I = -0x1

.field public static final RADIO_NOT_AVAILABLE:I = -0x2

.field public static final SUCCESS:I = 0x0

.field private static final TAG:Ljava/lang/String; = "RIL_Cpa"

.field public static final UNKNOWN_ERROR:I = -0x4


# instance fields
.field private mCPAStatus:I

.field private mConnInfo:Lcom/kddi/android/CpaManager$ConnInfo;

.field private mContext:Landroid/content/Context;

.field private mErrno:I

.field private mPackageName:Ljava/lang/String;

.field mReceiver:Landroid/content/BroadcastReceiver;

.field private mSettings:Lcom/kddi/android/CpaManager$Settings;

.field private final permName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 6
    .parameter "context"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 164
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 28
    iput-object v1, p0, Lcom/kddi/android/CpaManager;->mSettings:Lcom/kddi/android/CpaManager$Settings;

    #@6
    .line 29
    iput-object v1, p0, Lcom/kddi/android/CpaManager;->mConnInfo:Lcom/kddi/android/CpaManager$ConnInfo;

    #@8
    .line 30
    const/4 v1, 0x4

    #@9
    iput v1, p0, Lcom/kddi/android/CpaManager;->mCPAStatus:I

    #@b
    .line 31
    const/4 v1, -0x4

    #@c
    iput v1, p0, Lcom/kddi/android/CpaManager;->mErrno:I

    #@e
    .line 58
    const-string v1, "com.kddi.android.permission.MANAGE_CPA"

    #@10
    iput-object v1, p0, Lcom/kddi/android/CpaManager;->permName:Ljava/lang/String;

    #@12
    .line 60
    new-instance v1, Lcom/kddi/android/CpaManager$1;

    #@14
    invoke-direct {v1, p0}, Lcom/kddi/android/CpaManager$1;-><init>(Lcom/kddi/android/CpaManager;)V

    #@17
    iput-object v1, p0, Lcom/kddi/android/CpaManager;->mReceiver:Landroid/content/BroadcastReceiver;

    #@19
    .line 165
    iput-object p1, p0, Lcom/kddi/android/CpaManager;->mContext:Landroid/content/Context;

    #@1b
    .line 167
    iget-object v1, p0, Lcom/kddi/android/CpaManager;->mContext:Landroid/content/Context;

    #@1d
    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@20
    move-result-object v1

    #@21
    const-string v2, "com.kddi.android.permission.MANAGE_CPA"

    #@23
    iget-object v3, p0, Lcom/kddi/android/CpaManager;->mContext:Landroid/content/Context;

    #@25
    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@28
    move-result-object v3

    #@29
    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    move-result v1

    #@2d
    const/4 v2, -0x1

    #@2e
    if-ne v1, v2, :cond_38

    #@30
    .line 168
    new-instance v1, Landroid/os/RemoteException;

    #@32
    const-string v2, "PERMISSION_DENIED"

    #@34
    invoke-direct {v1, v2}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    #@37
    throw v1

    #@38
    .line 170
    :cond_38
    iget-object v1, p0, Lcom/kddi/android/CpaManager;->mContext:Landroid/content/Context;

    #@3a
    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@3d
    move-result-object v1

    #@3e
    iput-object v1, p0, Lcom/kddi/android/CpaManager;->mPackageName:Ljava/lang/String;

    #@40
    .line 171
    new-instance v1, Lcom/kddi/android/CpaManager$ConnInfo;

    #@42
    invoke-direct {v1}, Lcom/kddi/android/CpaManager$ConnInfo;-><init>()V

    #@45
    iput-object v1, p0, Lcom/kddi/android/CpaManager;->mConnInfo:Lcom/kddi/android/CpaManager$ConnInfo;

    #@47
    .line 173
    new-instance v0, Landroid/content/IntentFilter;

    #@49
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@4c
    .line 174
    .local v0, intentFilter:Landroid/content/IntentFilter;
    const-string v1, "cpa_onSetupConnectionCompleted"

    #@4e
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@51
    .line 175
    iget-object v1, p0, Lcom/kddi/android/CpaManager;->mContext:Landroid/content/Context;

    #@53
    iget-object v2, p0, Lcom/kddi/android/CpaManager;->mReceiver:Landroid/content/BroadcastReceiver;

    #@55
    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@58
    .line 177
    const-string v1, "RIL_Cpa"

    #@5a
    new-instance v2, Ljava/lang/StringBuilder;

    #@5c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5f
    const-string v3, "CpaManager mPackageName:"

    #@61
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v2

    #@65
    iget-object v3, p0, Lcom/kddi/android/CpaManager;->mPackageName:Ljava/lang/String;

    #@67
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v2

    #@6b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6e
    move-result-object v2

    #@6f
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@72
    .line 178
    return-void
.end method

.method static synthetic access$000(Lcom/kddi/android/CpaManager;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 23
    iget v0, p0, Lcom/kddi/android/CpaManager;->mCPAStatus:I

    #@2
    return v0
.end method

.method static synthetic access$002(Lcom/kddi/android/CpaManager;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 23
    iput p1, p0, Lcom/kddi/android/CpaManager;->mCPAStatus:I

    #@2
    return p1
.end method

.method static synthetic access$100(Lcom/kddi/android/CpaManager;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 23
    iget v0, p0, Lcom/kddi/android/CpaManager;->mErrno:I

    #@2
    return v0
.end method

.method static synthetic access$102(Lcom/kddi/android/CpaManager;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 23
    iput p1, p0, Lcom/kddi/android/CpaManager;->mErrno:I

    #@2
    return p1
.end method

.method static synthetic access$200(Lcom/kddi/android/CpaManager;)Lcom/kddi/android/CpaManager$ConnInfo;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 23
    iget-object v0, p0, Lcom/kddi/android/CpaManager;->mConnInfo:Lcom/kddi/android/CpaManager$ConnInfo;

    #@2
    return-object v0
.end method

.method static synthetic access$202(Lcom/kddi/android/CpaManager;Lcom/kddi/android/CpaManager$ConnInfo;)Lcom/kddi/android/CpaManager$ConnInfo;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 23
    iput-object p1, p0, Lcom/kddi/android/CpaManager;->mConnInfo:Lcom/kddi/android/CpaManager$ConnInfo;

    #@2
    return-object p1
.end method

.method static synthetic access$300(Lcom/kddi/android/CpaManager;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 23
    iget-object v0, p0, Lcom/kddi/android/CpaManager;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method


# virtual methods
.method public changeMode(Ljava/lang/String;Lcom/kddi/android/CpaManager$Settings;)I
    .registers 13
    .parameter "mode"
    .parameter "settings"

    #@0
    .prologue
    const/4 v9, 0x4

    #@1
    const/4 v8, 0x3

    #@2
    const/4 v7, 0x1

    #@3
    const/4 v6, 0x0

    #@4
    .line 181
    const/4 v4, -0x1

    #@5
    iput v4, p0, Lcom/kddi/android/CpaManager;->mErrno:I

    #@7
    .line 184
    iget-object v4, p0, Lcom/kddi/android/CpaManager;->mReceiver:Landroid/content/BroadcastReceiver;

    #@9
    if-nez v4, :cond_23

    #@b
    .line 185
    new-instance v1, Landroid/content/IntentFilter;

    #@d
    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    #@10
    .line 186
    .local v1, intentFilter:Landroid/content/IntentFilter;
    const-string v4, "cpa_onSetupConnectionCompleted"

    #@12
    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@15
    .line 187
    iget-object v4, p0, Lcom/kddi/android/CpaManager;->mContext:Landroid/content/Context;

    #@17
    iget-object v5, p0, Lcom/kddi/android/CpaManager;->mReceiver:Landroid/content/BroadcastReceiver;

    #@19
    invoke-virtual {v4, v5, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@1c
    .line 188
    const-string v4, "RIL_Cpa"

    #@1e
    const-string v5, "changeMode() recreate registerReceiver"

    #@20
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 192
    .end local v1           #intentFilter:Landroid/content/IntentFilter;
    :cond_23
    const-string v4, "NAVI"

    #@25
    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@28
    move-result v4

    #@29
    if-eqz v4, :cond_172

    #@2b
    .line 194
    iget-object v4, p2, Lcom/kddi/android/CpaManager$Settings;->apn:Ljava/lang/String;

    #@2d
    if-nez v4, :cond_37

    #@2f
    .line 195
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@31
    const-string v5, "apn name null"

    #@33
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@36
    throw v4

    #@37
    .line 198
    :cond_37
    iget v4, p0, Lcom/kddi/android/CpaManager;->mCPAStatus:I

    #@39
    if-eq v4, v7, :cond_40

    #@3b
    iget v4, p0, Lcom/kddi/android/CpaManager;->mCPAStatus:I

    #@3d
    const/4 v5, 0x2

    #@3e
    if-ne v4, v5, :cond_85

    #@40
    .line 200
    :cond_40
    iput v6, p0, Lcom/kddi/android/CpaManager;->mErrno:I

    #@42
    .line 203
    new-instance v3, Landroid/content/Intent;

    #@44
    const-string v4, "com.kddi.android.cpa.CONNECTIVITY_CHANGE"

    #@46
    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@49
    .line 204
    .local v3, sintent:Landroid/content/Intent;
    const-string v4, "connStatus"

    #@4b
    iget v5, p0, Lcom/kddi/android/CpaManager;->mCPAStatus:I

    #@4d
    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@50
    .line 205
    const-string v4, "errno"

    #@52
    iget v5, p0, Lcom/kddi/android/CpaManager;->mErrno:I

    #@54
    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@57
    .line 206
    iget-object v4, p0, Lcom/kddi/android/CpaManager;->mContext:Landroid/content/Context;

    #@59
    invoke-virtual {v4, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@5c
    .line 208
    const-string v4, "RIL_Cpa"

    #@5e
    new-instance v5, Ljava/lang/StringBuilder;

    #@60
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@63
    const-string v6, "=============1)MODE_NAVI changeMode() send intent CONNECTIVITY_ACTION mCPAStatus="

    #@65
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v5

    #@69
    iget v6, p0, Lcom/kddi/android/CpaManager;->mCPAStatus:I

    #@6b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v5

    #@6f
    const-string v6, " mErrno="

    #@71
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v5

    #@75
    iget v6, p0, Lcom/kddi/android/CpaManager;->mErrno:I

    #@77
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v5

    #@7b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7e
    move-result-object v5

    #@7f
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@82
    .line 210
    iget v4, p0, Lcom/kddi/android/CpaManager;->mErrno:I

    #@84
    .line 298
    :goto_84
    return v4

    #@85
    .line 222
    .end local v3           #sintent:Landroid/content/Intent;
    :cond_85
    new-instance v0, Landroid/content/Intent;

    #@87
    const-string v4, "com.kddi.android.cpa_CHANGED"

    #@89
    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@8c
    .line 223
    .local v0, intent:Landroid/content/Intent;
    const-string v4, "cpa_enable"

    #@8e
    invoke-virtual {v0, v4, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@91
    .line 224
    const-string v4, "cpa_apn"

    #@93
    iget-object v5, p2, Lcom/kddi/android/CpaManager$Settings;->apn:Ljava/lang/String;

    #@95
    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@98
    .line 225
    const-string v4, "cpa_user"

    #@9a
    iget-object v5, p2, Lcom/kddi/android/CpaManager$Settings;->userId:Ljava/lang/String;

    #@9c
    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@9f
    .line 226
    const-string v4, "cpa_password"

    #@a1
    iget-object v5, p2, Lcom/kddi/android/CpaManager$Settings;->password:Ljava/lang/String;

    #@a3
    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@a6
    .line 227
    const-string v4, "cpa_authType"

    #@a8
    iget v5, p2, Lcom/kddi/android/CpaManager$Settings;->authType:I

    #@aa
    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@ad
    .line 228
    const-string v4, "cpa_dns1"

    #@af
    iget-object v5, p2, Lcom/kddi/android/CpaManager$Settings;->dns1:Ljava/lang/String;

    #@b1
    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@b4
    .line 229
    const-string v4, "cpa_dns2"

    #@b6
    iget-object v5, p2, Lcom/kddi/android/CpaManager$Settings;->dns2:Ljava/lang/String;

    #@b8
    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@bb
    .line 230
    const-string v4, "cpa_PackageName"

    #@bd
    iget-object v5, p0, Lcom/kddi/android/CpaManager;->mPackageName:Ljava/lang/String;

    #@bf
    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@c2
    .line 231
    iget-object v4, p0, Lcom/kddi/android/CpaManager;->mContext:Landroid/content/Context;

    #@c4
    invoke-virtual {v4, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@c7
    .line 233
    iput-object p2, p0, Lcom/kddi/android/CpaManager;->mSettings:Lcom/kddi/android/CpaManager$Settings;

    #@c9
    .line 234
    iput v7, p0, Lcom/kddi/android/CpaManager;->mCPAStatus:I

    #@cb
    .line 235
    iput v6, p0, Lcom/kddi/android/CpaManager;->mErrno:I

    #@cd
    .line 238
    new-instance v3, Landroid/content/Intent;

    #@cf
    const-string v4, "com.kddi.android.cpa.CONNECTIVITY_CHANGE"

    #@d1
    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@d4
    .line 239
    .restart local v3       #sintent:Landroid/content/Intent;
    const-string v4, "connStatus"

    #@d6
    iget v5, p0, Lcom/kddi/android/CpaManager;->mCPAStatus:I

    #@d8
    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@db
    .line 240
    const-string v4, "errno"

    #@dd
    iget v5, p0, Lcom/kddi/android/CpaManager;->mErrno:I

    #@df
    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@e2
    .line 241
    iget-object v4, p0, Lcom/kddi/android/CpaManager;->mContext:Landroid/content/Context;

    #@e4
    invoke-virtual {v4, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@e7
    .line 243
    const-string v4, "RIL_Cpa"

    #@e9
    new-instance v5, Ljava/lang/StringBuilder;

    #@eb
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@ee
    const-string v6, "=============2)MODE_NAVI changeMode() send intent CONNECTIVITY_ACTION mCPAStatus="

    #@f0
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f3
    move-result-object v5

    #@f4
    iget v6, p0, Lcom/kddi/android/CpaManager;->mCPAStatus:I

    #@f6
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f9
    move-result-object v5

    #@fa
    const-string v6, " mErrno="

    #@fc
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ff
    move-result-object v5

    #@100
    iget v6, p0, Lcom/kddi/android/CpaManager;->mErrno:I

    #@102
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@105
    move-result-object v5

    #@106
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@109
    move-result-object v5

    #@10a
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@10d
    .line 246
    const-string v4, "RIL_Cpa"

    #@10f
    new-instance v5, Ljava/lang/StringBuilder;

    #@111
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@114
    const-string v6, "changeMode() MODE_NAVI mode apn("

    #@116
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@119
    move-result-object v5

    #@11a
    iget-object v6, p2, Lcom/kddi/android/CpaManager$Settings;->apn:Ljava/lang/String;

    #@11c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11f
    move-result-object v5

    #@120
    const-string v6, ") userId("

    #@122
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@125
    move-result-object v5

    #@126
    iget-object v6, p2, Lcom/kddi/android/CpaManager$Settings;->userId:Ljava/lang/String;

    #@128
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12b
    move-result-object v5

    #@12c
    const-string v6, ") password("

    #@12e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@131
    move-result-object v5

    #@132
    iget-object v6, p2, Lcom/kddi/android/CpaManager$Settings;->password:Ljava/lang/String;

    #@134
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@137
    move-result-object v5

    #@138
    const-string v6, ") authType("

    #@13a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13d
    move-result-object v5

    #@13e
    iget v6, p2, Lcom/kddi/android/CpaManager$Settings;->authType:I

    #@140
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@143
    move-result-object v5

    #@144
    const-string v6, ") dns1 ("

    #@146
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@149
    move-result-object v5

    #@14a
    iget-object v6, p2, Lcom/kddi/android/CpaManager$Settings;->dns1:Ljava/lang/String;

    #@14c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14f
    move-result-object v5

    #@150
    const-string v6, ") dns2 ("

    #@152
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@155
    move-result-object v5

    #@156
    iget-object v6, p2, Lcom/kddi/android/CpaManager$Settings;->dns2:Ljava/lang/String;

    #@158
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15b
    move-result-object v5

    #@15c
    const-string v6, ")"

    #@15e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@161
    move-result-object v5

    #@162
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@165
    move-result-object v5

    #@166
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@169
    .line 297
    :goto_169
    const-wide/16 v4, 0x64

    #@16b
    invoke-static {v4, v5}, Landroid/os/SystemClock;->sleep(J)V

    #@16e
    .line 298
    iget v4, p0, Lcom/kddi/android/CpaManager;->mErrno:I

    #@170
    goto/16 :goto_84

    #@172
    .line 250
    .end local v0           #intent:Landroid/content/Intent;
    .end local v3           #sintent:Landroid/content/Intent;
    :cond_172
    const-string v4, "DEFAULT"

    #@174
    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@177
    move-result v4

    #@178
    if-eqz v4, :cond_23c

    #@17a
    .line 253
    iget v4, p0, Lcom/kddi/android/CpaManager;->mCPAStatus:I

    #@17c
    if-eq v4, v8, :cond_182

    #@17e
    iget v4, p0, Lcom/kddi/android/CpaManager;->mCPAStatus:I

    #@180
    if-ne v4, v9, :cond_1de

    #@182
    .line 255
    :cond_182
    iput v6, p0, Lcom/kddi/android/CpaManager;->mErrno:I

    #@184
    .line 258
    const-string v4, "ril.btdun.send"

    #@186
    const-string v5, "false"

    #@188
    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@18b
    move-result-object v2

    #@18c
    .line 259
    .local v2, mSendIntent:Ljava/lang/String;
    iget v4, p0, Lcom/kddi/android/CpaManager;->mCPAStatus:I

    #@18e
    if-ne v4, v8, :cond_19a

    #@190
    const-string v4, "true"

    #@192
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@195
    move-result v4

    #@196
    if-eqz v4, :cond_19a

    #@198
    .line 260
    iput v9, p0, Lcom/kddi/android/CpaManager;->mCPAStatus:I

    #@19a
    .line 263
    :cond_19a
    new-instance v3, Landroid/content/Intent;

    #@19c
    const-string v4, "com.kddi.android.cpa.CONNECTIVITY_CHANGE"

    #@19e
    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@1a1
    .line 264
    .restart local v3       #sintent:Landroid/content/Intent;
    const-string v4, "connStatus"

    #@1a3
    iget v5, p0, Lcom/kddi/android/CpaManager;->mCPAStatus:I

    #@1a5
    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@1a8
    .line 265
    const-string v4, "errno"

    #@1aa
    iget v5, p0, Lcom/kddi/android/CpaManager;->mErrno:I

    #@1ac
    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@1af
    .line 266
    iget-object v4, p0, Lcom/kddi/android/CpaManager;->mContext:Landroid/content/Context;

    #@1b1
    invoke-virtual {v4, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@1b4
    .line 268
    const-string v4, "RIL_Cpa"

    #@1b6
    new-instance v5, Ljava/lang/StringBuilder;

    #@1b8
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@1bb
    const-string v6, "============= 3)MODE_DEFAULT changeMode() send intent CONNECTIVITY_ACTION mCPAStatus="

    #@1bd
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c0
    move-result-object v5

    #@1c1
    iget v6, p0, Lcom/kddi/android/CpaManager;->mCPAStatus:I

    #@1c3
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c6
    move-result-object v5

    #@1c7
    const-string v6, " mErrno="

    #@1c9
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1cc
    move-result-object v5

    #@1cd
    iget v6, p0, Lcom/kddi/android/CpaManager;->mErrno:I

    #@1cf
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d2
    move-result-object v5

    #@1d3
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d6
    move-result-object v5

    #@1d7
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1da
    .line 270
    iget v4, p0, Lcom/kddi/android/CpaManager;->mErrno:I

    #@1dc
    goto/16 :goto_84

    #@1de
    .line 274
    .end local v2           #mSendIntent:Ljava/lang/String;
    .end local v3           #sintent:Landroid/content/Intent;
    :cond_1de
    new-instance v0, Landroid/content/Intent;

    #@1e0
    const-string v4, "com.kddi.android.cpa_CHANGED"

    #@1e2
    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@1e5
    .line 275
    .restart local v0       #intent:Landroid/content/Intent;
    const-string v4, "cpa_enable"

    #@1e7
    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@1ea
    .line 276
    iget-object v4, p0, Lcom/kddi/android/CpaManager;->mContext:Landroid/content/Context;

    #@1ec
    invoke-virtual {v4, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@1ef
    .line 278
    iput v8, p0, Lcom/kddi/android/CpaManager;->mCPAStatus:I

    #@1f1
    .line 279
    iput v6, p0, Lcom/kddi/android/CpaManager;->mErrno:I

    #@1f3
    .line 282
    new-instance v3, Landroid/content/Intent;

    #@1f5
    const-string v4, "com.kddi.android.cpa.CONNECTIVITY_CHANGE"

    #@1f7
    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@1fa
    .line 283
    .restart local v3       #sintent:Landroid/content/Intent;
    const-string v4, "connStatus"

    #@1fc
    iget v5, p0, Lcom/kddi/android/CpaManager;->mCPAStatus:I

    #@1fe
    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@201
    .line 284
    const-string v4, "errno"

    #@203
    iget v5, p0, Lcom/kddi/android/CpaManager;->mErrno:I

    #@205
    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@208
    .line 285
    iget-object v4, p0, Lcom/kddi/android/CpaManager;->mContext:Landroid/content/Context;

    #@20a
    invoke-virtual {v4, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@20d
    .line 287
    const-string v4, "RIL_Cpa"

    #@20f
    new-instance v5, Ljava/lang/StringBuilder;

    #@211
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@214
    const-string v6, "============= 4) MODE_DEFAULT changeMode() send intent CONNECTIVITY_ACTION mCPAStatus="

    #@216
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@219
    move-result-object v5

    #@21a
    iget v6, p0, Lcom/kddi/android/CpaManager;->mCPAStatus:I

    #@21c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@21f
    move-result-object v5

    #@220
    const-string v6, " mErrno="

    #@222
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@225
    move-result-object v5

    #@226
    iget v6, p0, Lcom/kddi/android/CpaManager;->mErrno:I

    #@228
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22b
    move-result-object v5

    #@22c
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22f
    move-result-object v5

    #@230
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@233
    .line 290
    const-string v4, "RIL_Cpa"

    #@235
    const-string v5, "changeMode() MODE_DEFAULT mode"

    #@237
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@23a
    goto/16 :goto_169

    #@23c
    .line 294
    .end local v0           #intent:Landroid/content/Intent;
    .end local v3           #sintent:Landroid/content/Intent;
    :cond_23c
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@23e
    new-instance v5, Ljava/lang/StringBuilder;

    #@240
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@243
    const-string v6, "wrong mode:"

    #@245
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@248
    move-result-object v5

    #@249
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24c
    move-result-object v5

    #@24d
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@250
    move-result-object v5

    #@251
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@254
    throw v4
.end method

.method public getConnInfo()Lcom/kddi/android/CpaManager$ConnInfo;
    .registers 5

    #@0
    .prologue
    .line 307
    const-string v0, "RIL_Cpa"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "getConnInfo() mCPAStatus:"

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget v2, p0, Lcom/kddi/android/CpaManager;->mCPAStatus:I

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    const-string v2, " mErrno:"

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    iget v2, p0, Lcom/kddi/android/CpaManager;->mErrno:I

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 308
    iget v0, p0, Lcom/kddi/android/CpaManager;->mCPAStatus:I

    #@28
    const/4 v1, 0x2

    #@29
    if-ne v0, v1, :cond_2f

    #@2b
    iget v0, p0, Lcom/kddi/android/CpaManager;->mErrno:I

    #@2d
    if-eqz v0, :cond_31

    #@2f
    .line 309
    :cond_2f
    const/4 v0, 0x0

    #@30
    .line 320
    :goto_30
    return-object v0

    #@31
    .line 311
    :cond_31
    iget-object v0, p0, Lcom/kddi/android/CpaManager;->mConnInfo:Lcom/kddi/android/CpaManager$ConnInfo;

    #@33
    if-nez v0, :cond_3f

    #@35
    .line 312
    const-string v0, "RIL_Cpa"

    #@37
    const-string v1, "getConnInfo() mConnInfo == null"

    #@39
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3c
    .line 320
    :goto_3c
    iget-object v0, p0, Lcom/kddi/android/CpaManager;->mConnInfo:Lcom/kddi/android/CpaManager$ConnInfo;

    #@3e
    goto :goto_30

    #@3f
    .line 315
    :cond_3f
    const-string v0, "RIL_Cpa"

    #@41
    new-instance v1, Ljava/lang/StringBuilder;

    #@43
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@46
    const-string v2, "getConnInfo() mConnInfo.localAddress:"

    #@48
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v1

    #@4c
    iget-object v2, p0, Lcom/kddi/android/CpaManager;->mConnInfo:Lcom/kddi/android/CpaManager$ConnInfo;

    #@4e
    iget-object v2, v2, Lcom/kddi/android/CpaManager$ConnInfo;->localAddress:Ljava/net/InetAddress;

    #@50
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v1

    #@54
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@57
    move-result-object v1

    #@58
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5b
    .line 316
    const-string v0, "RIL_Cpa"

    #@5d
    new-instance v1, Ljava/lang/StringBuilder;

    #@5f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@62
    const-string v2, "getConnInfo() mConnInfo.dnsAddress[0]:"

    #@64
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v1

    #@68
    iget-object v2, p0, Lcom/kddi/android/CpaManager;->mConnInfo:Lcom/kddi/android/CpaManager$ConnInfo;

    #@6a
    iget-object v2, v2, Lcom/kddi/android/CpaManager$ConnInfo;->dnsAddress:[Ljava/net/InetAddress;

    #@6c
    const/4 v3, 0x0

    #@6d
    aget-object v2, v2, v3

    #@6f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v1

    #@73
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@76
    move-result-object v1

    #@77
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7a
    .line 317
    const-string v0, "RIL_Cpa"

    #@7c
    new-instance v1, Ljava/lang/StringBuilder;

    #@7e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@81
    const-string v2, "getConnInfo() mConnInfo.dnsAddress[1]:"

    #@83
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v1

    #@87
    iget-object v2, p0, Lcom/kddi/android/CpaManager;->mConnInfo:Lcom/kddi/android/CpaManager$ConnInfo;

    #@89
    iget-object v2, v2, Lcom/kddi/android/CpaManager$ConnInfo;->dnsAddress:[Ljava/net/InetAddress;

    #@8b
    const/4 v3, 0x1

    #@8c
    aget-object v2, v2, v3

    #@8e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@91
    move-result-object v1

    #@92
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@95
    move-result-object v1

    #@96
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@99
    goto :goto_3c
.end method

.method public getConnStatus()I
    .registers 4

    #@0
    .prologue
    .line 302
    const-string v0, "RIL_Cpa"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "getConnStatus() mCPAStatus:"

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget v2, p0, Lcom/kddi/android/CpaManager;->mCPAStatus:I

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 303
    iget v0, p0, Lcom/kddi/android/CpaManager;->mCPAStatus:I

    #@1c
    return v0
.end method
