.class Lcom/kddi/android/CpaManager$1;
.super Landroid/content/BroadcastReceiver;
.source "CpaManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kddi/android/CpaManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/kddi/android/CpaManager;


# direct methods
.method constructor <init>(Lcom/kddi/android/CpaManager;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 60
    iput-object p1, p0, Lcom/kddi/android/CpaManager$1;->this$0:Lcom/kddi/android/CpaManager;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 13
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 63
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 65
    .local v0, action:Ljava/lang/String;
    const-string v7, "cpa_onSetupConnectionCompleted"

    #@6
    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v7

    #@a
    if-eqz v7, :cond_184

    #@c
    .line 66
    const-string v7, "RIL_Cpa"

    #@e
    new-instance v8, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v9, "============= onReceive() Action cpa_onSetupConnectionCompleted - mCPAStatus="

    #@15
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v8

    #@19
    iget-object v9, p0, Lcom/kddi/android/CpaManager$1;->this$0:Lcom/kddi/android/CpaManager;

    #@1b
    invoke-static {v9}, Lcom/kddi/android/CpaManager;->access$000(Lcom/kddi/android/CpaManager;)I

    #@1e
    move-result v9

    #@1f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v8

    #@23
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v8

    #@27
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 69
    const-string v7, "ril.btdun.send"

    #@2c
    const-string v8, "ture"

    #@2e
    invoke-static {v7, v8}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@31
    .line 72
    const-string v7, "result"

    #@33
    const/4 v8, 0x0

    #@34
    invoke-virtual {p2, v7, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@37
    move-result v4

    #@38
    .line 73
    .local v4, resultKey:Z
    iget-object v7, p0, Lcom/kddi/android/CpaManager$1;->this$0:Lcom/kddi/android/CpaManager;

    #@3a
    const-string v8, "mFailCause"

    #@3c
    const/4 v9, -0x4

    #@3d
    invoke-virtual {p2, v8, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@40
    move-result v8

    #@41
    invoke-static {v7, v8}, Lcom/kddi/android/CpaManager;->access$102(Lcom/kddi/android/CpaManager;I)I

    #@44
    .line 74
    const/4 v2, 0x0

    #@45
    .line 75
    .local v2, bsendIntent:Z
    const-string v7, "status"

    #@47
    invoke-virtual {p2, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@4a
    move-result-object v6

    #@4b
    .line 84
    .local v6, stat:Ljava/lang/String;
    if-eqz v4, :cond_1f5

    #@4d
    .line 86
    iget-object v7, p0, Lcom/kddi/android/CpaManager$1;->this$0:Lcom/kddi/android/CpaManager;

    #@4f
    invoke-static {v7}, Lcom/kddi/android/CpaManager;->access$000(Lcom/kddi/android/CpaManager;)I

    #@52
    move-result v7

    #@53
    const/4 v8, 0x1

    #@54
    if-ne v7, v8, :cond_1d5

    #@56
    .line 88
    iget-object v7, p0, Lcom/kddi/android/CpaManager$1;->this$0:Lcom/kddi/android/CpaManager;

    #@58
    const/4 v8, 0x2

    #@59
    invoke-static {v7, v8}, Lcom/kddi/android/CpaManager;->access$002(Lcom/kddi/android/CpaManager;I)I

    #@5c
    .line 89
    iget-object v7, p0, Lcom/kddi/android/CpaManager$1;->this$0:Lcom/kddi/android/CpaManager;

    #@5e
    const/4 v8, 0x0

    #@5f
    invoke-static {v7, v8}, Lcom/kddi/android/CpaManager;->access$102(Lcom/kddi/android/CpaManager;I)I

    #@62
    .line 91
    const-string v7, "addresses"

    #@64
    invoke-virtual {p2, v7}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    #@67
    move-result-object v1

    #@68
    .line 92
    .local v1, addresses:[Ljava/lang/String;
    const-string v7, "dnses"

    #@6a
    invoke-virtual {p2, v7}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    #@6d
    move-result-object v3

    #@6e
    .line 94
    .local v3, dnses:[Ljava/lang/String;
    iget-object v7, p0, Lcom/kddi/android/CpaManager$1;->this$0:Lcom/kddi/android/CpaManager;

    #@70
    invoke-static {v7}, Lcom/kddi/android/CpaManager;->access$200(Lcom/kddi/android/CpaManager;)Lcom/kddi/android/CpaManager$ConnInfo;

    #@73
    move-result-object v7

    #@74
    if-nez v7, :cond_80

    #@76
    .line 95
    iget-object v7, p0, Lcom/kddi/android/CpaManager$1;->this$0:Lcom/kddi/android/CpaManager;

    #@78
    new-instance v8, Lcom/kddi/android/CpaManager$ConnInfo;

    #@7a
    invoke-direct {v8}, Lcom/kddi/android/CpaManager$ConnInfo;-><init>()V

    #@7d
    invoke-static {v7, v8}, Lcom/kddi/android/CpaManager;->access$202(Lcom/kddi/android/CpaManager;Lcom/kddi/android/CpaManager$ConnInfo;)Lcom/kddi/android/CpaManager$ConnInfo;

    #@80
    .line 96
    :cond_80
    if-eqz v1, :cond_185

    #@82
    array-length v7, v1

    #@83
    if-eqz v7, :cond_185

    #@85
    .line 98
    const-string v7, "RIL_Cpa"

    #@87
    new-instance v8, Ljava/lang/StringBuilder;

    #@89
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@8c
    const-string v9, "addresses.length="

    #@8e
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    move-result-object v8

    #@92
    array-length v9, v1

    #@93
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@96
    move-result-object v8

    #@97
    const-string v9, " addresses[0]="

    #@99
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v8

    #@9d
    const/4 v9, 0x0

    #@9e
    aget-object v9, v1, v9

    #@a0
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v8

    #@a4
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a7
    move-result-object v8

    #@a8
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ab
    .line 99
    const/4 v7, 0x0

    #@ac
    const/4 v8, 0x0

    #@ad
    aget-object v8, v1, v8

    #@af
    const-string v9, "/"

    #@b1
    invoke-virtual {v8, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@b4
    move-result-object v8

    #@b5
    const/4 v9, 0x0

    #@b6
    aget-object v8, v8, v9

    #@b8
    aput-object v8, v1, v7

    #@ba
    .line 100
    iget-object v7, p0, Lcom/kddi/android/CpaManager$1;->this$0:Lcom/kddi/android/CpaManager;

    #@bc
    invoke-static {v7}, Lcom/kddi/android/CpaManager;->access$200(Lcom/kddi/android/CpaManager;)Lcom/kddi/android/CpaManager$ConnInfo;

    #@bf
    move-result-object v7

    #@c0
    const/4 v8, 0x0

    #@c1
    aget-object v8, v1, v8

    #@c3
    invoke-static {v8}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@c6
    move-result-object v8

    #@c7
    iput-object v8, v7, Lcom/kddi/android/CpaManager$ConnInfo;->localAddress:Ljava/net/InetAddress;

    #@c9
    .line 104
    :goto_c9
    if-eqz v3, :cond_1bb

    #@cb
    array-length v7, v3

    #@cc
    if-eqz v7, :cond_1bb

    #@ce
    .line 106
    array-length v7, v3

    #@cf
    const/4 v8, 0x1

    #@d0
    if-le v7, v8, :cond_190

    #@d2
    .line 107
    const-string v7, "RIL_Cpa"

    #@d4
    new-instance v8, Ljava/lang/StringBuilder;

    #@d6
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@d9
    const-string v9, "dnses.length="

    #@db
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@de
    move-result-object v8

    #@df
    array-length v9, v3

    #@e0
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e3
    move-result-object v8

    #@e4
    const-string v9, " dnses[0]="

    #@e6
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e9
    move-result-object v8

    #@ea
    const/4 v9, 0x0

    #@eb
    aget-object v9, v3, v9

    #@ed
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f0
    move-result-object v8

    #@f1
    const-string v9, " dnses[1]="

    #@f3
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f6
    move-result-object v8

    #@f7
    const/4 v9, 0x1

    #@f8
    aget-object v9, v3, v9

    #@fa
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fd
    move-result-object v8

    #@fe
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@101
    move-result-object v8

    #@102
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@105
    .line 110
    :goto_105
    iget-object v7, p0, Lcom/kddi/android/CpaManager$1;->this$0:Lcom/kddi/android/CpaManager;

    #@107
    invoke-static {v7}, Lcom/kddi/android/CpaManager;->access$200(Lcom/kddi/android/CpaManager;)Lcom/kddi/android/CpaManager$ConnInfo;

    #@10a
    move-result-object v7

    #@10b
    iget-object v7, v7, Lcom/kddi/android/CpaManager$ConnInfo;->dnsAddress:[Ljava/net/InetAddress;

    #@10d
    const/4 v8, 0x0

    #@10e
    const/4 v9, 0x0

    #@10f
    aget-object v9, v3, v9

    #@111
    invoke-static {v9}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@114
    move-result-object v9

    #@115
    aput-object v9, v7, v8

    #@117
    .line 111
    array-length v7, v3

    #@118
    const/4 v8, 0x1

    #@119
    if-le v7, v8, :cond_1ad

    #@11b
    .line 112
    iget-object v7, p0, Lcom/kddi/android/CpaManager$1;->this$0:Lcom/kddi/android/CpaManager;

    #@11d
    invoke-static {v7}, Lcom/kddi/android/CpaManager;->access$200(Lcom/kddi/android/CpaManager;)Lcom/kddi/android/CpaManager$ConnInfo;

    #@120
    move-result-object v7

    #@121
    iget-object v7, v7, Lcom/kddi/android/CpaManager$ConnInfo;->dnsAddress:[Ljava/net/InetAddress;

    #@123
    const/4 v8, 0x1

    #@124
    const/4 v9, 0x1

    #@125
    aget-object v9, v3, v9

    #@127
    invoke-static {v9}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@12a
    move-result-object v9

    #@12b
    aput-object v9, v7, v8

    #@12d
    .line 122
    :goto_12d
    const/4 v2, 0x1

    #@12e
    .line 149
    .end local v1           #addresses:[Ljava/lang/String;
    .end local v3           #dnses:[Ljava/lang/String;
    :cond_12e
    :goto_12e
    if-eqz v2, :cond_184

    #@130
    .line 151
    new-instance v5, Landroid/content/Intent;

    #@132
    const-string v7, "com.kddi.android.cpa.CONNECTIVITY_CHANGE"

    #@134
    invoke-direct {v5, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@137
    .line 152
    .local v5, sintent:Landroid/content/Intent;
    const-string v7, "connStatus"

    #@139
    iget-object v8, p0, Lcom/kddi/android/CpaManager$1;->this$0:Lcom/kddi/android/CpaManager;

    #@13b
    invoke-static {v8}, Lcom/kddi/android/CpaManager;->access$000(Lcom/kddi/android/CpaManager;)I

    #@13e
    move-result v8

    #@13f
    invoke-virtual {v5, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@142
    .line 153
    const-string v7, "errno"

    #@144
    iget-object v8, p0, Lcom/kddi/android/CpaManager$1;->this$0:Lcom/kddi/android/CpaManager;

    #@146
    invoke-static {v8}, Lcom/kddi/android/CpaManager;->access$100(Lcom/kddi/android/CpaManager;)I

    #@149
    move-result v8

    #@14a
    invoke-virtual {v5, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@14d
    .line 154
    iget-object v7, p0, Lcom/kddi/android/CpaManager$1;->this$0:Lcom/kddi/android/CpaManager;

    #@14f
    invoke-static {v7}, Lcom/kddi/android/CpaManager;->access$300(Lcom/kddi/android/CpaManager;)Landroid/content/Context;

    #@152
    move-result-object v7

    #@153
    invoke-virtual {v7, v5}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@156
    .line 156
    const-string v7, "RIL_Cpa"

    #@158
    new-instance v8, Ljava/lang/StringBuilder;

    #@15a
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@15d
    const-string v9, "============= onReceive() send intent CONNECTIVITY_ACTION mCPAStatus="

    #@15f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@162
    move-result-object v8

    #@163
    iget-object v9, p0, Lcom/kddi/android/CpaManager$1;->this$0:Lcom/kddi/android/CpaManager;

    #@165
    invoke-static {v9}, Lcom/kddi/android/CpaManager;->access$000(Lcom/kddi/android/CpaManager;)I

    #@168
    move-result v9

    #@169
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16c
    move-result-object v8

    #@16d
    const-string v9, " mErrno="

    #@16f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@172
    move-result-object v8

    #@173
    iget-object v9, p0, Lcom/kddi/android/CpaManager$1;->this$0:Lcom/kddi/android/CpaManager;

    #@175
    invoke-static {v9}, Lcom/kddi/android/CpaManager;->access$100(Lcom/kddi/android/CpaManager;)I

    #@178
    move-result v9

    #@179
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17c
    move-result-object v8

    #@17d
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@180
    move-result-object v8

    #@181
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@184
    .line 161
    .end local v2           #bsendIntent:Z
    .end local v4           #resultKey:Z
    .end local v5           #sintent:Landroid/content/Intent;
    .end local v6           #stat:Ljava/lang/String;
    :cond_184
    return-void

    #@185
    .line 103
    .restart local v1       #addresses:[Ljava/lang/String;
    .restart local v2       #bsendIntent:Z
    .restart local v3       #dnses:[Ljava/lang/String;
    .restart local v4       #resultKey:Z
    .restart local v6       #stat:Ljava/lang/String;
    :cond_185
    iget-object v7, p0, Lcom/kddi/android/CpaManager$1;->this$0:Lcom/kddi/android/CpaManager;

    #@187
    invoke-static {v7}, Lcom/kddi/android/CpaManager;->access$200(Lcom/kddi/android/CpaManager;)Lcom/kddi/android/CpaManager$ConnInfo;

    #@18a
    move-result-object v7

    #@18b
    const/4 v8, 0x0

    #@18c
    iput-object v8, v7, Lcom/kddi/android/CpaManager$ConnInfo;->localAddress:Ljava/net/InetAddress;

    #@18e
    goto/16 :goto_c9

    #@190
    .line 109
    :cond_190
    const-string v7, "RIL_Cpa"

    #@192
    new-instance v8, Ljava/lang/StringBuilder;

    #@194
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@197
    const-string v9, "dnses.length=1 dnses[0]="

    #@199
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19c
    move-result-object v8

    #@19d
    const/4 v9, 0x0

    #@19e
    aget-object v9, v3, v9

    #@1a0
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a3
    move-result-object v8

    #@1a4
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a7
    move-result-object v8

    #@1a8
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1ab
    goto/16 :goto_105

    #@1ad
    .line 114
    :cond_1ad
    iget-object v7, p0, Lcom/kddi/android/CpaManager$1;->this$0:Lcom/kddi/android/CpaManager;

    #@1af
    invoke-static {v7}, Lcom/kddi/android/CpaManager;->access$200(Lcom/kddi/android/CpaManager;)Lcom/kddi/android/CpaManager$ConnInfo;

    #@1b2
    move-result-object v7

    #@1b3
    iget-object v7, v7, Lcom/kddi/android/CpaManager$ConnInfo;->dnsAddress:[Ljava/net/InetAddress;

    #@1b5
    const/4 v8, 0x1

    #@1b6
    const/4 v9, 0x0

    #@1b7
    aput-object v9, v7, v8

    #@1b9
    goto/16 :goto_12d

    #@1bb
    .line 118
    :cond_1bb
    iget-object v7, p0, Lcom/kddi/android/CpaManager$1;->this$0:Lcom/kddi/android/CpaManager;

    #@1bd
    invoke-static {v7}, Lcom/kddi/android/CpaManager;->access$200(Lcom/kddi/android/CpaManager;)Lcom/kddi/android/CpaManager$ConnInfo;

    #@1c0
    move-result-object v7

    #@1c1
    iget-object v7, v7, Lcom/kddi/android/CpaManager$ConnInfo;->dnsAddress:[Ljava/net/InetAddress;

    #@1c3
    const/4 v8, 0x0

    #@1c4
    const/4 v9, 0x0

    #@1c5
    aput-object v9, v7, v8

    #@1c7
    .line 119
    iget-object v7, p0, Lcom/kddi/android/CpaManager$1;->this$0:Lcom/kddi/android/CpaManager;

    #@1c9
    invoke-static {v7}, Lcom/kddi/android/CpaManager;->access$200(Lcom/kddi/android/CpaManager;)Lcom/kddi/android/CpaManager$ConnInfo;

    #@1cc
    move-result-object v7

    #@1cd
    iget-object v7, v7, Lcom/kddi/android/CpaManager$ConnInfo;->dnsAddress:[Ljava/net/InetAddress;

    #@1cf
    const/4 v8, 0x1

    #@1d0
    const/4 v9, 0x0

    #@1d1
    aput-object v9, v7, v8

    #@1d3
    goto/16 :goto_12d

    #@1d5
    .line 125
    .end local v1           #addresses:[Ljava/lang/String;
    .end local v3           #dnses:[Ljava/lang/String;
    :cond_1d5
    iget-object v7, p0, Lcom/kddi/android/CpaManager$1;->this$0:Lcom/kddi/android/CpaManager;

    #@1d7
    invoke-static {v7}, Lcom/kddi/android/CpaManager;->access$000(Lcom/kddi/android/CpaManager;)I

    #@1da
    move-result v7

    #@1db
    const/4 v8, 0x3

    #@1dc
    if-eq v7, v8, :cond_1e6

    #@1de
    const-string v7, "DISCONNECTED"

    #@1e0
    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1e3
    move-result v7

    #@1e4
    if-eqz v7, :cond_12e

    #@1e6
    .line 127
    :cond_1e6
    iget-object v7, p0, Lcom/kddi/android/CpaManager$1;->this$0:Lcom/kddi/android/CpaManager;

    #@1e8
    const/4 v8, 0x4

    #@1e9
    invoke-static {v7, v8}, Lcom/kddi/android/CpaManager;->access$002(Lcom/kddi/android/CpaManager;I)I

    #@1ec
    .line 128
    iget-object v7, p0, Lcom/kddi/android/CpaManager$1;->this$0:Lcom/kddi/android/CpaManager;

    #@1ee
    const/4 v8, 0x0

    #@1ef
    invoke-static {v7, v8}, Lcom/kddi/android/CpaManager;->access$102(Lcom/kddi/android/CpaManager;I)I

    #@1f2
    .line 131
    const/4 v2, 0x1

    #@1f3
    goto/16 :goto_12e

    #@1f5
    .line 136
    :cond_1f5
    iget-object v7, p0, Lcom/kddi/android/CpaManager$1;->this$0:Lcom/kddi/android/CpaManager;

    #@1f7
    invoke-static {v7}, Lcom/kddi/android/CpaManager;->access$100(Lcom/kddi/android/CpaManager;)I

    #@1fa
    move-result v7

    #@1fb
    if-gez v7, :cond_20d

    #@1fd
    .line 138
    const-string v7, "RIL_Cpa"

    #@1ff
    const-string v8, "============= onReceive() mErrno is valid, so bsendIntent set true"

    #@201
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@204
    .line 139
    iget-object v7, p0, Lcom/kddi/android/CpaManager$1;->this$0:Lcom/kddi/android/CpaManager;

    #@206
    const/4 v8, 0x4

    #@207
    invoke-static {v7, v8}, Lcom/kddi/android/CpaManager;->access$002(Lcom/kddi/android/CpaManager;I)I

    #@20a
    .line 140
    const/4 v2, 0x1

    #@20b
    goto/16 :goto_12e

    #@20d
    .line 145
    :cond_20d
    iget-object v7, p0, Lcom/kddi/android/CpaManager$1;->this$0:Lcom/kddi/android/CpaManager;

    #@20f
    const/4 v8, 0x4

    #@210
    invoke-static {v7, v8}, Lcom/kddi/android/CpaManager;->access$002(Lcom/kddi/android/CpaManager;I)I

    #@213
    .line 146
    iget-object v7, p0, Lcom/kddi/android/CpaManager$1;->this$0:Lcom/kddi/android/CpaManager;

    #@215
    const-string v8, "mFailCause"

    #@217
    const/4 v9, -0x4

    #@218
    invoke-virtual {p2, v8, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@21b
    move-result v8

    #@21c
    invoke-static {v7, v8}, Lcom/kddi/android/CpaManager;->access$102(Lcom/kddi/android/CpaManager;I)I

    #@21f
    goto/16 :goto_12e
.end method
