.class Lcom/android/internal/telephony/cdma/CdmaCallTracker$KddiCdmaInfoRec;
.super Ljava/lang/Thread;
.source "CdmaCallTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/cdma/CdmaCallTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "KddiCdmaInfoRec"
.end annotation


# static fields
.field private static final CLASS_NAME:Ljava/lang/String; = "KddiCdmaInfoRec"


# instance fields
.field private r:Landroid/os/AsyncResult;

.field final synthetic this$0:Lcom/android/internal/telephony/cdma/CdmaCallTracker;


# direct methods
.method constructor <init>(Lcom/android/internal/telephony/cdma/CdmaCallTracker;Landroid/os/AsyncResult;)V
    .registers 3
    .parameter
    .parameter "rusult"

    #@0
    .prologue
    .line 1602
    iput-object p1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker$KddiCdmaInfoRec;->this$0:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@2
    .line 1603
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    #@5
    .line 1604
    iput-object p2, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker$KddiCdmaInfoRec;->r:Landroid/os/AsyncResult;

    #@7
    .line 1605
    return-void
.end method

.method private handleCdmaInfoRecForOffhook(Ljava/util/ArrayList;)V
    .registers 9
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/telephony/cdma/CdmaInformationRecords;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .local p1, listInfoRecs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/cdma/CdmaInformationRecords;>;"
    const/4 v6, 0x1

    #@1
    .line 1725
    if-nez p1, :cond_4

    #@3
    .line 1753
    :cond_3
    return-void

    #@4
    .line 1729
    :cond_4
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@7
    move-result-object v1

    #@8
    .local v1, i$:Ljava/util/Iterator;
    :cond_8
    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@b
    move-result v3

    #@c
    if-eqz v3, :cond_3

    #@e
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@11
    move-result-object v2

    #@12
    check-cast v2, Lcom/android/internal/telephony/cdma/CdmaInformationRecords;

    #@14
    .line 1730
    .local v2, infoRec:Lcom/android/internal/telephony/cdma/CdmaInformationRecords;
    iget-object v3, v2, Lcom/android/internal/telephony/cdma/CdmaInformationRecords;->record:Ljava/lang/Object;

    #@16
    instance-of v3, v3, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaT53ClirInfoRec;

    #@18
    if-eqz v3, :cond_8

    #@1a
    .line 1731
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker$KddiCdmaInfoRec;->this$0:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@1c
    new-instance v4, Lcom/android/internal/telephony/cdma/CdmaCallWaitingNotification;

    #@1e
    invoke-direct {v4}, Lcom/android/internal/telephony/cdma/CdmaCallWaitingNotification;-><init>()V

    #@21
    invoke-static {v3, v4}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->access$300(Lcom/android/internal/telephony/cdma/CdmaCallTracker;Lcom/android/internal/telephony/cdma/CdmaCallWaitingNotification;)V

    #@24
    .line 1733
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker$KddiCdmaInfoRec;->this$0:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@26
    iget-object v3, v3, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->ringingCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@28
    invoke-virtual {v3}, Lcom/android/internal/telephony/cdma/CdmaCall;->getLatestConnection()Lcom/android/internal/telephony/Connection;

    #@2b
    move-result-object v0

    #@2c
    check-cast v0, Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@2e
    .line 1735
    .local v0, conn:Lcom/android/internal/telephony/cdma/CdmaConnection;
    if-eqz v0, :cond_3

    #@30
    .line 1737
    iget-boolean v3, v0, Lcom/android/internal/telephony/cdma/CdmaConnection;->isCdmaInfoRecReceivedKddi:Z

    #@32
    if-nez v3, :cond_3

    #@34
    iget-boolean v3, v0, Lcom/android/internal/telephony/cdma/CdmaConnection;->isNotifyRingingConnectionKddi:Z

    #@36
    if-nez v3, :cond_3

    #@38
    .line 1742
    iget-object v3, v2, Lcom/android/internal/telephony/cdma/CdmaInformationRecords;->record:Ljava/lang/Object;

    #@3a
    check-cast v3, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaT53ClirInfoRec;

    #@3c
    invoke-direct {p0, v3, v0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker$KddiCdmaInfoRec;->handleClirInfoRecKddi(Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaT53ClirInfoRec;Lcom/android/internal/telephony/cdma/CdmaConnection;)Z

    #@3f
    move-result v3

    #@40
    if-eqz v3, :cond_5e

    #@42
    .line 1743
    iput-boolean v6, v0, Lcom/android/internal/telephony/cdma/CdmaConnection;->isCdmaInfoRecReceivedKddi:Z

    #@44
    .line 1744
    const-string v3, "CDMA"

    #@46
    new-instance v4, Ljava/lang/StringBuilder;

    #@48
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@4b
    const-string v5, "handleClirInfoRecKddi() end isCdmaInfoRecReceivedKddi:"

    #@4d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v4

    #@51
    iget-boolean v5, v0, Lcom/android/internal/telephony/cdma/CdmaConnection;->isCdmaInfoRecReceivedKddi:Z

    #@53
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@56
    move-result-object v4

    #@57
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v4

    #@5b
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5e
    .line 1747
    :cond_5e
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker$KddiCdmaInfoRec;->this$0:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@60
    invoke-static {v3}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->access$100(Lcom/android/internal/telephony/cdma/CdmaCallTracker;)V

    #@63
    .line 1749
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker$KddiCdmaInfoRec;->this$0:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@65
    iget-object v4, v0, Lcom/android/internal/telephony/cdma/CdmaConnection;->cwKddi:Lcom/android/internal/telephony/cdma/CdmaCallWaitingNotification;

    #@67
    invoke-static {v3, v4}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->access$200(Lcom/android/internal/telephony/cdma/CdmaCallTracker;Lcom/android/internal/telephony/cdma/CdmaCallWaitingNotification;)V

    #@6a
    .line 1750
    iput-boolean v6, v0, Lcom/android/internal/telephony/cdma/CdmaConnection;->isNotifyRingingConnectionKddi:Z

    #@6c
    goto :goto_8
.end method

.method private handleCdmaInfoRecForRinging(Ljava/util/ArrayList;)V
    .registers 9
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/telephony/cdma/CdmaInformationRecords;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .local p1, listInfoRecs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/cdma/CdmaInformationRecords;>;"
    const/4 v6, 0x1

    #@1
    .line 1672
    if-nez p1, :cond_4

    #@3
    .line 1717
    :cond_3
    :goto_3
    return-void

    #@4
    .line 1676
    :cond_4
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker$KddiCdmaInfoRec;->this$0:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@6
    iget-object v3, v3, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->ringingCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@8
    invoke-virtual {v3}, Lcom/android/internal/telephony/cdma/CdmaCall;->getLatestConnection()Lcom/android/internal/telephony/Connection;

    #@b
    move-result-object v0

    #@c
    check-cast v0, Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@e
    .line 1678
    .local v0, conn:Lcom/android/internal/telephony/cdma/CdmaConnection;
    if-eqz v0, :cond_3

    #@10
    .line 1681
    iget-boolean v3, v0, Lcom/android/internal/telephony/cdma/CdmaConnection;->isCdmaInfoRecReceivedKddi:Z

    #@12
    if-nez v3, :cond_3

    #@14
    iget-boolean v3, v0, Lcom/android/internal/telephony/cdma/CdmaConnection;->isNotifyRingingConnectionKddi:Z

    #@16
    if-nez v3, :cond_3

    #@18
    .line 1688
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@1b
    move-result-object v1

    #@1c
    .local v1, i$:Ljava/util/Iterator;
    :cond_1c
    :goto_1c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@1f
    move-result v3

    #@20
    if-eqz v3, :cond_9d

    #@22
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@25
    move-result-object v2

    #@26
    check-cast v2, Lcom/android/internal/telephony/cdma/CdmaInformationRecords;

    #@28
    .line 1689
    .local v2, infoRec:Lcom/android/internal/telephony/cdma/CdmaInformationRecords;
    iget-object v3, v2, Lcom/android/internal/telephony/cdma/CdmaInformationRecords;->record:Ljava/lang/Object;

    #@2a
    instance-of v3, v3, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaNumberInfoRec;

    #@2c
    if-eqz v3, :cond_55

    #@2e
    .line 1690
    iget-object v3, v2, Lcom/android/internal/telephony/cdma/CdmaInformationRecords;->record:Ljava/lang/Object;

    #@30
    check-cast v3, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaNumberInfoRec;

    #@32
    invoke-direct {p0, v3, v0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker$KddiCdmaInfoRec;->handleNumberInfoRec(Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaNumberInfoRec;Lcom/android/internal/telephony/cdma/CdmaConnection;)Z

    #@35
    move-result v3

    #@36
    if-eqz v3, :cond_1c

    #@38
    .line 1691
    iput-boolean v6, v0, Lcom/android/internal/telephony/cdma/CdmaConnection;->isCdmaInfoRecReceivedKddi:Z

    #@3a
    .line 1692
    const-string v3, "CDMA"

    #@3c
    new-instance v4, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v5, "handleNumberInfoRec() end isCdmaInfoRecReceivedKddi:"

    #@43
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v4

    #@47
    iget-boolean v5, v0, Lcom/android/internal/telephony/cdma/CdmaConnection;->isCdmaInfoRecReceivedKddi:Z

    #@49
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v4

    #@4d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50
    move-result-object v4

    #@51
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@54
    goto :goto_1c

    #@55
    .line 1694
    :cond_55
    iget-object v3, v2, Lcom/android/internal/telephony/cdma/CdmaInformationRecords;->record:Ljava/lang/Object;

    #@57
    instance-of v3, v3, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaT53ClirInfoRec;

    #@59
    if-eqz v3, :cond_82

    #@5b
    .line 1695
    iget-object v3, v2, Lcom/android/internal/telephony/cdma/CdmaInformationRecords;->record:Ljava/lang/Object;

    #@5d
    check-cast v3, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaT53ClirInfoRec;

    #@5f
    invoke-direct {p0, v3, v0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker$KddiCdmaInfoRec;->handleClirInfoRecKddi(Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaT53ClirInfoRec;Lcom/android/internal/telephony/cdma/CdmaConnection;)Z

    #@62
    move-result v3

    #@63
    if-eqz v3, :cond_1c

    #@65
    .line 1696
    iput-boolean v6, v0, Lcom/android/internal/telephony/cdma/CdmaConnection;->isCdmaInfoRecReceivedKddi:Z

    #@67
    .line 1697
    const-string v3, "CDMA"

    #@69
    new-instance v4, Ljava/lang/StringBuilder;

    #@6b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@6e
    const-string v5, "handleClirInfoRecKddi() end isCdmaInfoRecReceivedKddi:"

    #@70
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v4

    #@74
    iget-boolean v5, v0, Lcom/android/internal/telephony/cdma/CdmaConnection;->isCdmaInfoRecReceivedKddi:Z

    #@76
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@79
    move-result-object v4

    #@7a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7d
    move-result-object v4

    #@7e
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@81
    goto :goto_1c

    #@82
    .line 1702
    :cond_82
    const-string v3, "CDMA"

    #@84
    new-instance v4, Ljava/lang/StringBuilder;

    #@86
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@89
    const-string v5, "!!!!!!!!!!! error infoRec.record= "

    #@8b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v4

    #@8f
    iget-object v5, v2, Lcom/android/internal/telephony/cdma/CdmaInformationRecords;->record:Ljava/lang/Object;

    #@91
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v4

    #@95
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@98
    move-result-object v4

    #@99
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9c
    goto :goto_1c

    #@9d
    .line 1706
    .end local v2           #infoRec:Lcom/android/internal/telephony/cdma/CdmaInformationRecords;
    :cond_9d
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker$KddiCdmaInfoRec;->this$0:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@9f
    iget-object v3, v3, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->ringingCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@a1
    invoke-virtual {v3}, Lcom/android/internal/telephony/cdma/CdmaCall;->getState()Lcom/android/internal/telephony/Call$State;

    #@a4
    move-result-object v3

    #@a5
    sget-object v4, Lcom/android/internal/telephony/Call$State;->INCOMING:Lcom/android/internal/telephony/Call$State;

    #@a7
    if-ne v3, v4, :cond_b0

    #@a9
    .line 1707
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker$KddiCdmaInfoRec;->this$0:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@ab
    invoke-static {v3}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->access$000(Lcom/android/internal/telephony/cdma/CdmaCallTracker;)V

    #@ae
    goto/16 :goto_3

    #@b0
    .line 1708
    :cond_b0
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker$KddiCdmaInfoRec;->this$0:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@b2
    iget-object v3, v3, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->ringingCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@b4
    invoke-virtual {v3}, Lcom/android/internal/telephony/cdma/CdmaCall;->getState()Lcom/android/internal/telephony/Call$State;

    #@b7
    move-result-object v3

    #@b8
    sget-object v4, Lcom/android/internal/telephony/Call$State;->WAITING:Lcom/android/internal/telephony/Call$State;

    #@ba
    if-ne v3, v4, :cond_3

    #@bc
    .line 1709
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker$KddiCdmaInfoRec;->this$0:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@be
    invoke-static {v3}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->access$100(Lcom/android/internal/telephony/cdma/CdmaCallTracker;)V

    #@c1
    .line 1711
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker$KddiCdmaInfoRec;->this$0:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@c3
    iget-object v4, v0, Lcom/android/internal/telephony/cdma/CdmaConnection;->cwKddi:Lcom/android/internal/telephony/cdma/CdmaCallWaitingNotification;

    #@c5
    invoke-static {v3, v4}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->access$200(Lcom/android/internal/telephony/cdma/CdmaCallTracker;Lcom/android/internal/telephony/cdma/CdmaCallWaitingNotification;)V

    #@c8
    .line 1712
    iput-boolean v6, v0, Lcom/android/internal/telephony/cdma/CdmaConnection;->isNotifyRingingConnectionKddi:Z

    #@ca
    goto/16 :goto_3
.end method

.method private handleClirInfoRecKddi(Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaT53ClirInfoRec;Lcom/android/internal/telephony/cdma/CdmaConnection;)Z
    .registers 6
    .parameter "clirInfoRec"
    .parameter "connection"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v0, 0x0

    #@2
    .line 1788
    if-eqz p1, :cond_6

    #@4
    if-nez p2, :cond_7

    #@6
    .line 1818
    :cond_6
    :goto_6
    return v0

    #@7
    .line 1792
    :cond_7
    iget-boolean v2, p2, Lcom/android/internal/telephony/cdma/CdmaConnection;->isNumberInfoReceivedKddi:Z

    #@9
    if-eqz v2, :cond_f

    #@b
    iget v2, p2, Lcom/android/internal/telephony/cdma/CdmaConnection;->presentationIndicatorsKddi:I

    #@d
    if-ne v2, v1, :cond_19

    #@f
    .line 1795
    :cond_f
    iget-byte v2, p1, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaT53ClirInfoRec;->cause:B

    #@11
    packed-switch v2, :pswitch_data_2a

    #@14
    goto :goto_6

    #@15
    .line 1797
    :pswitch_15
    sget v0, Lcom/android/internal/telephony/PhoneConstants;->PRESENTATION_ALLOWED:I

    #@17
    iput v0, p2, Lcom/android/internal/telephony/cdma/CdmaConnection;->numberPresentation:I

    #@19
    :cond_19
    :goto_19
    move v0, v1

    #@1a
    .line 1818
    goto :goto_6

    #@1b
    .line 1801
    :pswitch_1b
    sget v0, Lcom/android/internal/telephony/PhoneConstants;->PRESENTATION_RESTRICTED:I

    #@1d
    iput v0, p2, Lcom/android/internal/telephony/cdma/CdmaConnection;->numberPresentation:I

    #@1f
    goto :goto_19

    #@20
    .line 1806
    :pswitch_20
    sget v0, Lcom/android/internal/telephony/PhoneConstants;->PRESENTATION_UNKNOWN:I

    #@22
    iput v0, p2, Lcom/android/internal/telephony/cdma/CdmaConnection;->numberPresentation:I

    #@24
    goto :goto_19

    #@25
    .line 1810
    :pswitch_25
    sget v0, Lcom/android/internal/telephony/PhoneConstants;->PRESENTATION_PAYPHONE:I

    #@27
    iput v0, p2, Lcom/android/internal/telephony/cdma/CdmaConnection;->numberPresentation:I

    #@29
    goto :goto_19

    #@2a
    .line 1795
    :pswitch_data_2a
    .packed-switch 0x0
        :pswitch_15
        :pswitch_1b
        :pswitch_20
        :pswitch_25
        :pswitch_20
    .end packed-switch
.end method

.method private handleNumberInfoRec(Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaNumberInfoRec;Lcom/android/internal/telephony/cdma/CdmaConnection;)Z
    .registers 7
    .parameter "numberInfoRec"
    .parameter "connection"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 1761
    if-eqz p1, :cond_5

    #@3
    if-nez p2, :cond_29

    #@5
    .line 1762
    :cond_5
    const-string v1, "CDMA"

    #@7
    new-instance v2, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v3, "handleNumberInfoRec return numberInfoRec:"

    #@e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    const-string v3, " connection:"

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 1763
    const/4 v1, 0x0

    #@28
    .line 1780
    :goto_28
    return v1

    #@29
    .line 1766
    :cond_29
    iget-byte v2, p1, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaNumberInfoRec;->pi:B

    #@2b
    iput v2, p2, Lcom/android/internal/telephony/cdma/CdmaConnection;->presentationIndicatorsKddi:I

    #@2d
    .line 1767
    iget-byte v2, p1, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaNumberInfoRec;->pi:B

    #@2f
    if-nez v2, :cond_39

    #@31
    .line 1768
    iget-object v0, p1, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaNumberInfoRec;->number:Ljava/lang/String;

    #@33
    .line 1770
    .local v0, dirtyPhoneNumberKddi:Ljava/lang/String;
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->extractNetworkPortion(Ljava/lang/String;)Ljava/lang/String;

    #@36
    move-result-object v2

    #@37
    iput-object v2, p2, Lcom/android/internal/telephony/cdma/CdmaConnection;->address:Ljava/lang/String;

    #@39
    .line 1773
    .end local v0           #dirtyPhoneNumberKddi:Ljava/lang/String;
    :cond_39
    sget v2, Lcom/android/internal/telephony/PhoneConstants;->PRESENTATION_ALLOWED:I

    #@3b
    iput v2, p2, Lcom/android/internal/telephony/cdma/CdmaConnection;->numberPresentation:I

    #@3d
    .line 1775
    iput-boolean v1, p2, Lcom/android/internal/telephony/cdma/CdmaConnection;->isNumberInfoReceivedKddi:Z

    #@3f
    goto :goto_28
.end method


# virtual methods
.method public run()V
    .registers 10

    #@0
    .prologue
    .line 1610
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker$KddiCdmaInfoRec;->r:Landroid/os/AsyncResult;

    #@2
    if-nez v6, :cond_5

    #@4
    .line 1664
    :cond_4
    :goto_4
    return-void

    #@5
    .line 1616
    :cond_5
    :try_start_5
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker$KddiCdmaInfoRec;->r:Landroid/os/AsyncResult;

    #@7
    iget-object v6, v6, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@9
    check-cast v6, Ljava/util/ArrayList;

    #@b
    move-object v0, v6

    #@c
    check-cast v0, Ljava/util/ArrayList;

    #@e
    move-object v4, v0
    :try_end_f
    .catch Ljava/lang/ClassCastException; {:try_start_5 .. :try_end_f} :catch_66

    #@f
    .line 1622
    .local v4, listInfoRecs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/cdma/CdmaInformationRecords;>;"
    const/4 v5, 0x0

    #@10
    .line 1624
    .local v5, sleep_ms:I
    :cond_10
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker$KddiCdmaInfoRec;->this$0:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@12
    iget-object v6, v6, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->state:Lcom/android/internal/telephony/PhoneConstants$State;

    #@14
    sget-object v7, Lcom/android/internal/telephony/PhoneConstants$State;->IDLE:Lcom/android/internal/telephony/PhoneConstants$State;

    #@16
    if-ne v6, v7, :cond_22

    #@18
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker$KddiCdmaInfoRec;->this$0:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@1a
    iget-object v6, v6, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->ringingCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@1c
    invoke-virtual {v6}, Lcom/android/internal/telephony/cdma/CdmaCall;->isIdle()Z

    #@1f
    move-result v6

    #@20
    if-nez v6, :cond_3e

    #@22
    :cond_22
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker$KddiCdmaInfoRec;->this$0:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@24
    iget-object v6, v6, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->state:Lcom/android/internal/telephony/PhoneConstants$State;

    #@26
    sget-object v7, Lcom/android/internal/telephony/PhoneConstants$State;->OFFHOOK:Lcom/android/internal/telephony/PhoneConstants$State;

    #@28
    if-ne v6, v7, :cond_82

    #@2a
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker$KddiCdmaInfoRec;->this$0:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@2c
    iget-object v6, v6, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->foregroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@2e
    invoke-virtual {v6}, Lcom/android/internal/telephony/cdma/CdmaCall;->isDialingOrAlerting()Z

    #@31
    move-result v6

    #@32
    if-eqz v6, :cond_82

    #@34
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker$KddiCdmaInfoRec;->this$0:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@36
    iget-object v6, v6, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->ringingCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@38
    invoke-virtual {v6}, Lcom/android/internal/telephony/cdma/CdmaCall;->isIdle()Z

    #@3b
    move-result v6

    #@3c
    if-eqz v6, :cond_82

    #@3e
    .line 1627
    :cond_3e
    const-wide/16 v6, 0xa

    #@40
    :try_start_40
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_43
    .catch Ljava/lang/InterruptedException; {:try_start_40 .. :try_end_43} :catch_68

    #@43
    .line 1628
    add-int/lit8 v5, v5, 0xa

    #@45
    .line 1632
    :goto_45
    const/16 v6, 0x2710

    #@47
    if-lt v5, v6, :cond_10

    #@49
    .line 1633
    const-string v6, "CDMA"

    #@4b
    new-instance v7, Ljava/lang/StringBuilder;

    #@4d
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@50
    const-string v8, "while loop abnormal exit : "

    #@52
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v7

    #@56
    iget-object v8, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker$KddiCdmaInfoRec;->this$0:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@58
    iget-object v8, v8, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->state:Lcom/android/internal/telephony/PhoneConstants$State;

    #@5a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v7

    #@5e
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@61
    move-result-object v7

    #@62
    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@65
    goto :goto_4

    #@66
    .line 1617
    .end local v4           #listInfoRecs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/cdma/CdmaInformationRecords;>;"
    .end local v5           #sleep_ms:I
    :catch_66
    move-exception v2

    #@67
    .line 1618
    .local v2, e:Ljava/lang/ClassCastException;
    goto :goto_4

    #@68
    .line 1629
    .end local v2           #e:Ljava/lang/ClassCastException;
    .restart local v4       #listInfoRecs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/cdma/CdmaInformationRecords;>;"
    .restart local v5       #sleep_ms:I
    :catch_68
    move-exception v2

    #@69
    .line 1630
    .local v2, e:Ljava/lang/InterruptedException;
    const-string v6, "CDMA"

    #@6b
    new-instance v7, Ljava/lang/StringBuilder;

    #@6d
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@70
    const-string v8, "KddiCdmaInfoRec run stopped: "

    #@72
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v7

    #@76
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v7

    #@7a
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7d
    move-result-object v7

    #@7e
    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@81
    goto :goto_45

    #@82
    .line 1639
    .end local v2           #e:Ljava/lang/InterruptedException;
    :cond_82
    const/4 v3, 0x0

    #@83
    .local v3, iKddi:I
    :goto_83
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker$KddiCdmaInfoRec;->this$0:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@85
    iget-object v6, v6, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->foregroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@87
    invoke-virtual {v6}, Lcom/android/internal/telephony/cdma/CdmaCall;->getConnections()Ljava/util/List;

    #@8a
    move-result-object v6

    #@8b
    invoke-interface {v6}, Ljava/util/List;->size()I

    #@8e
    move-result v6

    #@8f
    if-ge v3, v6, :cond_dc

    #@91
    .line 1640
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker$KddiCdmaInfoRec;->this$0:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@93
    iget-object v6, v6, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->foregroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@95
    iget-object v6, v6, Lcom/android/internal/telephony/cdma/CdmaCall;->connections:Ljava/util/ArrayList;

    #@97
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@9a
    move-result-object v6

    #@9b
    check-cast v6, Lcom/android/internal/telephony/Connection;

    #@9d
    invoke-virtual {v6}, Lcom/android/internal/telephony/Connection;->getState()Lcom/android/internal/telephony/Call$State;

    #@a0
    move-result-object v6

    #@a1
    invoke-virtual {v6}, Lcom/android/internal/telephony/Call$State;->isDialing()Z

    #@a4
    move-result v6

    #@a5
    if-eqz v6, :cond_d2

    #@a7
    .line 1642
    :try_start_a7
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker$KddiCdmaInfoRec;->this$0:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@a9
    iget-object v6, v6, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->foregroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@ab
    iget-object v6, v6, Lcom/android/internal/telephony/cdma/CdmaCall;->connections:Ljava/util/ArrayList;

    #@ad
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@b0
    move-result-object v1

    #@b1
    check-cast v1, Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@b3
    .line 1643
    .local v1, cnKddi:Lcom/android/internal/telephony/cdma/CdmaConnection;
    :cond_b3
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker$KddiCdmaInfoRec;->this$0:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@b5
    iget-object v6, v6, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pendingMO:Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@b7
    if-nez v6, :cond_b3

    #@b9
    .line 1646
    iget v6, v1, Lcom/android/internal/telephony/cdma/CdmaConnection;->index:I

    #@bb
    if-ltz v6, :cond_d5

    #@bd
    .line 1647
    iget-object v7, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker$KddiCdmaInfoRec;->this$0:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@bf
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker$KddiCdmaInfoRec;->this$0:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@c1
    iget-object v6, v6, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->foregroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@c3
    invoke-virtual {v6}, Lcom/android/internal/telephony/cdma/CdmaCall;->getConnections()Ljava/util/List;

    #@c6
    move-result-object v6

    #@c7
    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@ca
    move-result-object v6

    #@cb
    check-cast v6, Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@cd
    check-cast v6, Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@cf
    invoke-virtual {v7, v6}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->hangup(Lcom/android/internal/telephony/cdma/CdmaConnection;)V

    #@d2
    .line 1639
    .end local v1           #cnKddi:Lcom/android/internal/telephony/cdma/CdmaConnection;
    :cond_d2
    :goto_d2
    add-int/lit8 v3, v3, 0x1

    #@d4
    goto :goto_83

    #@d5
    .line 1648
    .restart local v1       #cnKddi:Lcom/android/internal/telephony/cdma/CdmaConnection;
    :cond_d5
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker$KddiCdmaInfoRec;->this$0:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@d7
    iget-object v6, v6, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pendingMO:Lcom/android/internal/telephony/cdma/CdmaConnection;
    :try_end_d9
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_a7 .. :try_end_d9} :catch_f8

    #@d9
    if-nez v6, :cond_d2

    #@db
    goto :goto_d2

    #@dc
    .line 1656
    .end local v1           #cnKddi:Lcom/android/internal/telephony/cdma/CdmaConnection;
    :cond_dc
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker$KddiCdmaInfoRec;->this$0:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@de
    iget-object v6, v6, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->ringingCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@e0
    invoke-virtual {v6}, Lcom/android/internal/telephony/cdma/CdmaCall;->isRinging()Z

    #@e3
    move-result v6

    #@e4
    if-eqz v6, :cond_eb

    #@e6
    .line 1657
    invoke-direct {p0, v4}, Lcom/android/internal/telephony/cdma/CdmaCallTracker$KddiCdmaInfoRec;->handleCdmaInfoRecForRinging(Ljava/util/ArrayList;)V

    #@e9
    goto/16 :goto_4

    #@eb
    .line 1658
    :cond_eb
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker$KddiCdmaInfoRec;->this$0:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@ed
    iget-object v6, v6, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->state:Lcom/android/internal/telephony/PhoneConstants$State;

    #@ef
    sget-object v7, Lcom/android/internal/telephony/PhoneConstants$State;->OFFHOOK:Lcom/android/internal/telephony/PhoneConstants$State;

    #@f1
    if-ne v6, v7, :cond_4

    #@f3
    .line 1659
    invoke-direct {p0, v4}, Lcom/android/internal/telephony/cdma/CdmaCallTracker$KddiCdmaInfoRec;->handleCdmaInfoRecForOffhook(Ljava/util/ArrayList;)V

    #@f6
    goto/16 :goto_4

    #@f8
    .line 1650
    :catch_f8
    move-exception v6

    #@f9
    goto :goto_d2
.end method
