.class Lcom/android/internal/telephony/DataConnection$DcActiveState;
.super Lcom/android/internal/util/State;
.source "DataConnection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/DataConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DcActiveState"
.end annotation


# instance fields
.field private mConnectionParams:Lcom/android/internal/telephony/DataConnection$ConnectionParams;

.field private mFailCause:Lcom/android/internal/telephony/DataConnection$FailCause;

.field final synthetic this$0:Lcom/android/internal/telephony/DataConnection;


# direct methods
.method private constructor <init>(Lcom/android/internal/telephony/DataConnection;)V
    .registers 3
    .parameter

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 1596
    iput-object p1, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@3
    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    #@6
    .line 1597
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->mConnectionParams:Lcom/android/internal/telephony/DataConnection$ConnectionParams;

    #@8
    .line 1598
    iput-object v0, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->mFailCause:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@a
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/internal/telephony/DataConnection;Lcom/android/internal/telephony/DataConnection$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1596
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/DataConnection$DcActiveState;-><init>(Lcom/android/internal/telephony/DataConnection;)V

    #@3
    return-void
.end method

.method private notifyQoSInfo(Landroid/net/LinkCapabilities$Flow;)V
    .registers 11
    .parameter "flow"

    #@0
    .prologue
    .line 1906
    new-instance v4, Landroid/content/Intent;

    #@2
    const-string v6, "com.lge.internal.telephony.qos-changed"

    #@4
    invoke-direct {v4, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 1909
    .local v4, intent_qos:Landroid/content/Intent;
    new-instance v5, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    .line 1911
    .local v5, sb:Ljava/lang/StringBuilder;
    iget-object v6, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->mConnectionParams:Lcom/android/internal/telephony/DataConnection$ConnectionParams;

    #@e
    if-eqz v6, :cond_1e

    #@10
    iget-object v6, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->mConnectionParams:Lcom/android/internal/telephony/DataConnection$ConnectionParams;

    #@12
    iget-object v6, v6, Lcom/android/internal/telephony/DataConnection$ConnectionParams;->apn:Lcom/android/internal/telephony/DataProfile;

    #@14
    if-eqz v6, :cond_1e

    #@16
    iget-object v6, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->mConnectionParams:Lcom/android/internal/telephony/DataConnection$ConnectionParams;

    #@18
    iget-object v6, v6, Lcom/android/internal/telephony/DataConnection$ConnectionParams;->apn:Lcom/android/internal/telephony/DataProfile;

    #@1a
    iget-object v6, v6, Lcom/android/internal/telephony/DataProfile;->types:[Ljava/lang/String;

    #@1c
    if-nez v6, :cond_167

    #@1e
    .line 1913
    :cond_1e
    iget-object v6, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@20
    const-string v7, "notifyQoSInfo :  mConnectionParams, mConnectionParams.apn or mConnectionParams.apn.types is null"

    #@22
    invoke-virtual {v6, v7}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@25
    .line 1925
    :cond_25
    const-string v6, "type"

    #@27
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v7

    #@2b
    invoke-virtual {v4, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@2e
    .line 1926
    const-string v6, "id"

    #@30
    invoke-virtual {p1}, Landroid/net/LinkCapabilities$Flow;->getID()I

    #@33
    move-result v7

    #@34
    invoke-virtual {v4, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@37
    .line 1927
    const-string v6, "status"

    #@39
    invoke-virtual {p1}, Landroid/net/LinkCapabilities$Flow;->getState()Landroid/net/LinkCapabilities$FlowState;

    #@3c
    move-result-object v7

    #@3d
    invoke-virtual {v7}, Landroid/net/LinkCapabilities$FlowState;->toString()Ljava/lang/String;

    #@40
    move-result-object v7

    #@41
    invoke-virtual {v4, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@44
    .line 1928
    const-string v6, "tx-desc"

    #@46
    const-string v7, "tx"

    #@48
    invoke-virtual {p1, v7}, Landroid/net/LinkCapabilities$Flow;->getFlowDescriptions(Ljava/lang/String;)Ljava/lang/String;

    #@4b
    move-result-object v7

    #@4c
    invoke-virtual {v4, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@4f
    .line 1929
    const-string v6, "rx-desc"

    #@51
    const-string v7, "rx"

    #@53
    invoke-virtual {p1, v7}, Landroid/net/LinkCapabilities$Flow;->getFlowDescriptions(Ljava/lang/String;)Ljava/lang/String;

    #@56
    move-result-object v7

    #@57
    invoke-virtual {v4, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@5a
    .line 1931
    iget-object v6, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@5c
    const-string v7, "notifyQoSInfo : Broadcast QoS Information"

    #@5e
    invoke-virtual {v6, v7}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@61
    .line 1932
    iget-object v6, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@63
    new-instance v7, Ljava/lang/StringBuilder;

    #@65
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@68
    const-string v8, "    type : "

    #@6a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v7

    #@6e
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@71
    move-result-object v8

    #@72
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v7

    #@76
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@79
    move-result-object v7

    #@7a
    invoke-virtual {v6, v7}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@7d
    .line 1933
    iget-object v6, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@7f
    new-instance v7, Ljava/lang/StringBuilder;

    #@81
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@84
    const-string v8, "    id : "

    #@86
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89
    move-result-object v7

    #@8a
    invoke-virtual {p1}, Landroid/net/LinkCapabilities$Flow;->getID()I

    #@8d
    move-result v8

    #@8e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@91
    move-result-object v7

    #@92
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@95
    move-result-object v7

    #@96
    invoke-virtual {v6, v7}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@99
    .line 1934
    iget-object v6, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@9b
    new-instance v7, Ljava/lang/StringBuilder;

    #@9d
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@a0
    const-string v8, "    status : "

    #@a2
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v7

    #@a6
    invoke-virtual {p1}, Landroid/net/LinkCapabilities$Flow;->getState()Landroid/net/LinkCapabilities$FlowState;

    #@a9
    move-result-object v8

    #@aa
    invoke-virtual {v8}, Landroid/net/LinkCapabilities$FlowState;->toString()Ljava/lang/String;

    #@ad
    move-result-object v8

    #@ae
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v7

    #@b2
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b5
    move-result-object v7

    #@b6
    invoke-virtual {v6, v7}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@b9
    .line 1935
    iget-object v6, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@bb
    new-instance v7, Ljava/lang/StringBuilder;

    #@bd
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@c0
    const-string v8, "    tx-desc : "

    #@c2
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c5
    move-result-object v7

    #@c6
    const-string v8, "tx"

    #@c8
    invoke-virtual {p1, v8}, Landroid/net/LinkCapabilities$Flow;->getFlowDescriptions(Ljava/lang/String;)Ljava/lang/String;

    #@cb
    move-result-object v8

    #@cc
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cf
    move-result-object v7

    #@d0
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d3
    move-result-object v7

    #@d4
    invoke-virtual {v6, v7}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@d7
    .line 1936
    iget-object v6, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@d9
    new-instance v7, Ljava/lang/StringBuilder;

    #@db
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@de
    const-string v8, "    rx-desc : "

    #@e0
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e3
    move-result-object v7

    #@e4
    const-string v8, "rx"

    #@e6
    invoke-virtual {p1, v8}, Landroid/net/LinkCapabilities$Flow;->getFlowDescriptions(Ljava/lang/String;)Ljava/lang/String;

    #@e9
    move-result-object v8

    #@ea
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ed
    move-result-object v7

    #@ee
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f1
    move-result-object v7

    #@f2
    invoke-virtual {v6, v7}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@f5
    .line 1938
    const-string v6, "tx"

    #@f7
    invoke-virtual {p1, v6}, Landroid/net/LinkCapabilities$Flow;->getFlowFilterCount(Ljava/lang/String;)I

    #@fa
    move-result v1

    #@fb
    .line 1939
    .local v1, TXcount:I
    const-string v6, "rx"

    #@fd
    invoke-virtual {p1, v6}, Landroid/net/LinkCapabilities$Flow;->getFlowFilterCount(Ljava/lang/String;)I

    #@100
    move-result v0

    #@101
    .line 1941
    .local v0, RXcount:I
    const-string v6, "TX-filterCount"

    #@103
    invoke-virtual {v4, v6, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@106
    .line 1942
    const-string v6, "RX-filterCount"

    #@108
    invoke-virtual {v4, v6, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@10b
    .line 1944
    iget-object v6, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@10d
    new-instance v7, Ljava/lang/StringBuilder;

    #@10f
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@112
    const-string v8, "    TXcount : "

    #@114
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@117
    move-result-object v7

    #@118
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11b
    move-result-object v7

    #@11c
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11f
    move-result-object v7

    #@120
    invoke-virtual {v6, v7}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@123
    .line 1945
    iget-object v6, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@125
    new-instance v7, Ljava/lang/StringBuilder;

    #@127
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@12a
    const-string v8, "    RXcount : "

    #@12c
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12f
    move-result-object v7

    #@130
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@133
    move-result-object v7

    #@134
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@137
    move-result-object v7

    #@138
    invoke-virtual {v6, v7}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@13b
    .line 1947
    if-nez v1, :cond_18f

    #@13d
    .line 1948
    const-string v6, "tx-filter"

    #@13f
    const-string v7, ""

    #@141
    invoke-virtual {v4, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@144
    .line 1949
    iget-object v6, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@146
    const-string v7, "    tx-filter : "

    #@148
    invoke-virtual {v6, v7}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@14b
    .line 1958
    :cond_14b
    if-nez v0, :cond_1d9

    #@14d
    .line 1959
    const-string v6, "rx-filter"

    #@14f
    const-string v7, ""

    #@151
    invoke-virtual {v4, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@154
    .line 1960
    iget-object v6, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@156
    const-string v7, "    rx-filter : "

    #@158
    invoke-virtual {v6, v7}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@15b
    .line 1968
    :cond_15b
    iget-object v6, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@15d
    iget-object v6, v6, Lcom/android/internal/telephony/DataConnection;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@15f
    invoke-virtual {v6}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@162
    move-result-object v6

    #@163
    invoke-virtual {v6, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@166
    .line 1969
    return-void

    #@167
    .line 1917
    .end local v0           #RXcount:I
    .end local v1           #TXcount:I
    :cond_167
    const/4 v3, 0x0

    #@168
    .local v3, i:I
    :goto_168
    iget-object v6, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->mConnectionParams:Lcom/android/internal/telephony/DataConnection$ConnectionParams;

    #@16a
    iget-object v6, v6, Lcom/android/internal/telephony/DataConnection$ConnectionParams;->apn:Lcom/android/internal/telephony/DataProfile;

    #@16c
    iget-object v6, v6, Lcom/android/internal/telephony/DataProfile;->types:[Ljava/lang/String;

    #@16e
    array-length v6, v6

    #@16f
    if-ge v3, v6, :cond_25

    #@171
    .line 1918
    iget-object v6, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->mConnectionParams:Lcom/android/internal/telephony/DataConnection$ConnectionParams;

    #@173
    iget-object v6, v6, Lcom/android/internal/telephony/DataConnection$ConnectionParams;->apn:Lcom/android/internal/telephony/DataProfile;

    #@175
    iget-object v6, v6, Lcom/android/internal/telephony/DataProfile;->types:[Ljava/lang/String;

    #@177
    aget-object v6, v6, v3

    #@179
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17c
    .line 1919
    iget-object v6, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->mConnectionParams:Lcom/android/internal/telephony/DataConnection$ConnectionParams;

    #@17e
    iget-object v6, v6, Lcom/android/internal/telephony/DataConnection$ConnectionParams;->apn:Lcom/android/internal/telephony/DataProfile;

    #@180
    iget-object v6, v6, Lcom/android/internal/telephony/DataProfile;->types:[Ljava/lang/String;

    #@182
    array-length v6, v6

    #@183
    add-int/lit8 v6, v6, -0x1

    #@185
    if-ge v3, v6, :cond_18c

    #@187
    .line 1920
    const-string v6, ","

    #@189
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18c
    .line 1917
    :cond_18c
    add-int/lit8 v3, v3, 0x1

    #@18e
    goto :goto_168

    #@18f
    .line 1951
    .end local v3           #i:I
    .restart local v0       #RXcount:I
    .restart local v1       #TXcount:I
    :cond_18f
    const/4 v3, 0x0

    #@190
    .restart local v3       #i:I
    :goto_190
    if-ge v3, v1, :cond_14b

    #@192
    .line 1952
    const-string v6, "tx"

    #@194
    invoke-virtual {p1, v3, v6}, Landroid/net/LinkCapabilities$Flow;->getFlowFilter(ILjava/lang/String;)Ljava/lang/String;

    #@197
    move-result-object v2

    #@198
    .line 1953
    .local v2, filter:Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    #@19a
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@19d
    const-string v7, "tx-filter["

    #@19f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a2
    move-result-object v6

    #@1a3
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a6
    move-result-object v6

    #@1a7
    const-string v7, "]"

    #@1a9
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ac
    move-result-object v6

    #@1ad
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b0
    move-result-object v6

    #@1b1
    invoke-virtual {v4, v6, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@1b4
    .line 1954
    iget-object v6, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@1b6
    new-instance v7, Ljava/lang/StringBuilder;

    #@1b8
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@1bb
    const-string v8, "    tx-filter["

    #@1bd
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c0
    move-result-object v7

    #@1c1
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c4
    move-result-object v7

    #@1c5
    const-string v8, "] : "

    #@1c7
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ca
    move-result-object v7

    #@1cb
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ce
    move-result-object v7

    #@1cf
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d2
    move-result-object v7

    #@1d3
    invoke-virtual {v6, v7}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@1d6
    .line 1951
    add-int/lit8 v3, v3, 0x1

    #@1d8
    goto :goto_190

    #@1d9
    .line 1962
    .end local v2           #filter:Ljava/lang/String;
    .end local v3           #i:I
    :cond_1d9
    const/4 v3, 0x0

    #@1da
    .restart local v3       #i:I
    :goto_1da
    if-ge v3, v0, :cond_15b

    #@1dc
    .line 1963
    const-string v6, "rx"

    #@1de
    invoke-virtual {p1, v3, v6}, Landroid/net/LinkCapabilities$Flow;->getFlowFilter(ILjava/lang/String;)Ljava/lang/String;

    #@1e1
    move-result-object v2

    #@1e2
    .line 1964
    .restart local v2       #filter:Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    #@1e4
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@1e7
    const-string v7, "rx-filter["

    #@1e9
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ec
    move-result-object v6

    #@1ed
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f0
    move-result-object v6

    #@1f1
    const-string v7, "]"

    #@1f3
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f6
    move-result-object v6

    #@1f7
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1fa
    move-result-object v6

    #@1fb
    invoke-virtual {v4, v6, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@1fe
    .line 1965
    iget-object v6, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@200
    new-instance v7, Ljava/lang/StringBuilder;

    #@202
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@205
    const-string v8, "    rx-filter["

    #@207
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20a
    move-result-object v7

    #@20b
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20e
    move-result-object v7

    #@20f
    const-string v8, "] : "

    #@211
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@214
    move-result-object v7

    #@215
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@218
    move-result-object v7

    #@219
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21c
    move-result-object v7

    #@21d
    invoke-virtual {v6, v7}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@220
    .line 1962
    add-int/lit8 v3, v3, 0x1

    #@222
    goto :goto_1da
.end method

.method private onQoSChanged(Landroid/os/AsyncResult;)V
    .registers 12
    .parameter "ar"

    #@0
    .prologue
    .line 1973
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@2
    const-string v8, "onGetQoSChanged : ENTRY"

    #@4
    invoke-virtual {v7, v8}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@7
    .line 1975
    const/4 v6, 0x0

    #@8
    .line 1976
    .local v6, result:Ljava/lang/String;
    const/4 v0, 0x0

    #@9
    .line 1982
    .local v0, QoSInfo:[Ljava/lang/String;
    iget-object v7, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@b
    instance-of v7, v7, Ljava/lang/String;

    #@d
    if-eqz v7, :cond_3b

    #@f
    .line 1984
    iget-object v6, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@11
    .end local v6           #result:Ljava/lang/String;
    check-cast v6, Ljava/lang/String;

    #@13
    .line 1985
    .restart local v6       #result:Ljava/lang/String;
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@15
    new-instance v8, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v9, " GET QoS Info: "

    #@1c
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v8

    #@20
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v8

    #@24
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v8

    #@28
    invoke-virtual {v7, v8}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@2b
    .line 1987
    const-string v7, ";"

    #@2d
    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@30
    move-result-object v0

    #@31
    .line 1995
    if-nez v0, :cond_43

    #@33
    .line 1997
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@35
    const-string v8, "onQoSChanged : EXIT with Error, QoSInfo is null"

    #@37
    invoke-virtual {v7, v8}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@3a
    .line 2081
    :goto_3a
    return-void

    #@3b
    .line 1991
    :cond_3b
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@3d
    const-string v8, "onQoSChanged : EXIT with Error, result is not String object"

    #@3f
    invoke-virtual {v7, v8}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@42
    goto :goto_3a

    #@43
    .line 2001
    :cond_43
    array-length v7, v0

    #@44
    const/4 v8, 0x3

    #@45
    if-ge v7, v8, :cond_61

    #@47
    .line 2003
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@49
    new-instance v8, Ljava/lang/StringBuilder;

    #@4b
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@4e
    const-string v9, "onQoSChanged : EXIT with Error, there is no full information, # of qos entities: "

    #@50
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v8

    #@54
    array-length v9, v0

    #@55
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@58
    move-result-object v8

    #@59
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5c
    move-result-object v8

    #@5d
    invoke-virtual {v7, v8}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@60
    goto :goto_3a

    #@61
    .line 2009
    :cond_61
    const/4 v7, 0x0

    #@62
    :try_start_62
    aget-object v7, v0, v7

    #@64
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@67
    move-result v3

    #@68
    .line 2010
    .local v3, qos_cid:I
    const/4 v7, 0x1

    #@69
    aget-object v7, v0, v7

    #@6b
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@6e
    move-result v4

    #@6f
    .line 2011
    .local v4, qos_qid:I
    const/4 v7, 0x2

    #@70
    aget-object v7, v0, v7

    #@72
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_75
    .catch Ljava/lang/Exception; {:try_start_62 .. :try_end_75} :catch_a3

    #@75
    move-result v5

    #@76
    .line 2019
    .local v5, qos_status:I
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@78
    iget v7, v7, Lcom/android/internal/telephony/DataConnection;->cid:I

    #@7a
    if-eq v7, v3, :cond_be

    #@7c
    .line 2021
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@7e
    new-instance v8, Ljava/lang/StringBuilder;

    #@80
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@83
    const-string v9, "onGetQoSChanged : EXIT, QoS Event is not for this data connection, cid ("

    #@85
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@88
    move-result-object v8

    #@89
    iget-object v9, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@8b
    iget v9, v9, Lcom/android/internal/telephony/DataConnection;->cid:I

    #@8d
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@90
    move-result-object v8

    #@91
    const-string v9, "), qos_cid: "

    #@93
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v8

    #@97
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v8

    #@9b
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9e
    move-result-object v8

    #@9f
    invoke-virtual {v7, v8}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@a2
    goto :goto_3a

    #@a3
    .line 2013
    .end local v3           #qos_cid:I
    .end local v4           #qos_qid:I
    .end local v5           #qos_status:I
    :catch_a3
    move-exception v1

    #@a4
    .line 2015
    .local v1, e:Ljava/lang/Exception;
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@a6
    new-instance v8, Ljava/lang/StringBuilder;

    #@a8
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@ab
    const-string v9, "onQoSChanged : Catch Exception: "

    #@ad
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v8

    #@b1
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v8

    #@b5
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b8
    move-result-object v8

    #@b9
    invoke-virtual {v7, v8}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@bc
    goto/16 :goto_3a

    #@be
    .line 2025
    .end local v1           #e:Ljava/lang/Exception;
    .restart local v3       #qos_cid:I
    .restart local v4       #qos_qid:I
    .restart local v5       #qos_status:I
    :cond_be
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@c0
    iget-object v8, v7, Lcom/android/internal/telephony/DataConnection;->mCapabilities:Landroid/net/LinkCapabilities;

    #@c2
    const/4 v7, 0x1

    #@c3
    if-ne v5, v7, :cond_e6

    #@c5
    const/4 v7, 0x1

    #@c6
    :goto_c6
    invoke-virtual {v8, v4, v7}, Landroid/net/LinkCapabilities;->getFlow(IZ)Landroid/net/LinkCapabilities$Flow;

    #@c9
    move-result-object v2

    #@ca
    .line 2026
    .local v2, flow:Landroid/net/LinkCapabilities$Flow;
    if-nez v2, :cond_e8

    #@cc
    .line 2028
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@ce
    new-instance v8, Ljava/lang/StringBuilder;

    #@d0
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@d3
    const-string v9, "onQoSChanged : Failed to create/find Flow for "

    #@d5
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d8
    move-result-object v8

    #@d9
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@dc
    move-result-object v8

    #@dd
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e0
    move-result-object v8

    #@e1
    invoke-virtual {v7, v8}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@e4
    goto/16 :goto_3a

    #@e6
    .line 2025
    .end local v2           #flow:Landroid/net/LinkCapabilities$Flow;
    :cond_e6
    const/4 v7, 0x0

    #@e7
    goto :goto_c6

    #@e8
    .line 2032
    .restart local v2       #flow:Landroid/net/LinkCapabilities$Flow;
    :cond_e8
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@ea
    new-instance v8, Ljava/lang/StringBuilder;

    #@ec
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@ef
    const-string v9, "onQoSChanged : Handing for QoS Status : "

    #@f1
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f4
    move-result-object v8

    #@f5
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f8
    move-result-object v8

    #@f9
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fc
    move-result-object v8

    #@fd
    invoke-virtual {v7, v8}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@100
    .line 2034
    packed-switch v5, :pswitch_data_1ca

    #@103
    .line 2080
    :cond_103
    :goto_103
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@105
    const-string v8, "onQoSChanged : EXIT with Success"

    #@107
    invoke-virtual {v7, v8}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@10a
    goto/16 :goto_3a

    #@10c
    .line 2038
    :pswitch_10c
    invoke-virtual {v2}, Landroid/net/LinkCapabilities$Flow;->clearFlow()V

    #@10f
    .line 2041
    :pswitch_10f
    array-length v7, v0

    #@110
    const/4 v8, 0x3

    #@111
    if-le v7, v8, :cond_129

    #@113
    const/4 v7, 0x3

    #@114
    aget-object v7, v0, v7

    #@116
    if-eqz v7, :cond_129

    #@118
    const/4 v7, 0x3

    #@119
    aget-object v7, v0, v7

    #@11b
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    #@11e
    move-result v7

    #@11f
    if-lez v7, :cond_129

    #@121
    .line 2043
    const-string v7, "tx"

    #@123
    const/4 v8, 0x3

    #@124
    aget-object v8, v0, v8

    #@126
    invoke-virtual {v2, v7, v8}, Landroid/net/LinkCapabilities$Flow;->putFlowDescriptions(Ljava/lang/String;Ljava/lang/String;)V

    #@129
    .line 2045
    :cond_129
    array-length v7, v0

    #@12a
    const/4 v8, 0x4

    #@12b
    if-le v7, v8, :cond_143

    #@12d
    const/4 v7, 0x4

    #@12e
    aget-object v7, v0, v7

    #@130
    if-eqz v7, :cond_143

    #@132
    const/4 v7, 0x4

    #@133
    aget-object v7, v0, v7

    #@135
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    #@138
    move-result v7

    #@139
    if-lez v7, :cond_143

    #@13b
    .line 2047
    const-string v7, "rx"

    #@13d
    const/4 v8, 0x4

    #@13e
    aget-object v8, v0, v8

    #@140
    invoke-virtual {v2, v7, v8}, Landroid/net/LinkCapabilities$Flow;->putFlowDescriptions(Ljava/lang/String;Ljava/lang/String;)V

    #@143
    .line 2049
    :cond_143
    array-length v7, v0

    #@144
    const/4 v8, 0x5

    #@145
    if-le v7, v8, :cond_15d

    #@147
    const/4 v7, 0x5

    #@148
    aget-object v7, v0, v7

    #@14a
    if-eqz v7, :cond_15d

    #@14c
    const/4 v7, 0x5

    #@14d
    aget-object v7, v0, v7

    #@14f
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    #@152
    move-result v7

    #@153
    if-lez v7, :cond_15d

    #@155
    .line 2051
    const-string v7, "tx"

    #@157
    const/4 v8, 0x5

    #@158
    aget-object v8, v0, v8

    #@15a
    invoke-virtual {v2, v7, v8}, Landroid/net/LinkCapabilities$Flow;->putFlowFilters(Ljava/lang/String;Ljava/lang/String;)V

    #@15d
    .line 2053
    :cond_15d
    array-length v7, v0

    #@15e
    const/4 v8, 0x6

    #@15f
    if-le v7, v8, :cond_177

    #@161
    const/4 v7, 0x6

    #@162
    aget-object v7, v0, v7

    #@164
    if-eqz v7, :cond_177

    #@166
    const/4 v7, 0x6

    #@167
    aget-object v7, v0, v7

    #@169
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    #@16c
    move-result v7

    #@16d
    if-lez v7, :cond_177

    #@16f
    .line 2055
    const-string v7, "rx"

    #@171
    const/4 v8, 0x6

    #@172
    aget-object v8, v0, v8

    #@174
    invoke-virtual {v2, v7, v8}, Landroid/net/LinkCapabilities$Flow;->putFlowFilters(Ljava/lang/String;Ljava/lang/String;)V

    #@177
    .line 2058
    :cond_177
    invoke-direct {p0, v2, v5}, Lcom/android/internal/telephony/DataConnection$DcActiveState;->setFlowState(Landroid/net/LinkCapabilities$Flow;I)Z

    #@17a
    move-result v7

    #@17b
    if-eqz v7, :cond_103

    #@17d
    .line 2060
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@17f
    new-instance v8, Ljava/lang/StringBuilder;

    #@181
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@184
    const-string v9, "onQoSChanged : Flow Updated : "

    #@186
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@189
    move-result-object v8

    #@18a
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18d
    move-result-object v8

    #@18e
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@191
    move-result-object v8

    #@192
    invoke-virtual {v7, v8}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@195
    .line 2061
    const/4 v7, 0x1

    #@196
    if-eq v5, v7, :cond_103

    #@198
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/DataConnection$DcActiveState;->notifyQoSInfo(Landroid/net/LinkCapabilities$Flow;)V

    #@19b
    goto/16 :goto_103

    #@19d
    .line 2066
    :pswitch_19d
    invoke-virtual {v2}, Landroid/net/LinkCapabilities$Flow;->clearFlow()V

    #@1a0
    .line 2067
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@1a2
    iget-object v7, v7, Lcom/android/internal/telephony/DataConnection;->mCapabilities:Landroid/net/LinkCapabilities;

    #@1a4
    invoke-virtual {v7, v4}, Landroid/net/LinkCapabilities;->removeFlow(I)Z

    #@1a7
    .line 2071
    :pswitch_1a7
    invoke-direct {p0, v2, v5}, Lcom/android/internal/telephony/DataConnection$DcActiveState;->setFlowState(Landroid/net/LinkCapabilities$Flow;I)Z

    #@1aa
    move-result v7

    #@1ab
    if-eqz v7, :cond_103

    #@1ad
    .line 2073
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@1af
    new-instance v8, Ljava/lang/StringBuilder;

    #@1b1
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@1b4
    const-string v9, "onQoSChanged : Flow Updated : "

    #@1b6
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b9
    move-result-object v8

    #@1ba
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1bd
    move-result-object v8

    #@1be
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c1
    move-result-object v8

    #@1c2
    invoke-virtual {v7, v8}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@1c5
    .line 2074
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/DataConnection$DcActiveState;->notifyQoSInfo(Landroid/net/LinkCapabilities$Flow;)V

    #@1c8
    goto/16 :goto_103

    #@1ca
    .line 2034
    :pswitch_data_1ca
    .packed-switch 0x1
        :pswitch_10c
        :pswitch_10c
        :pswitch_19d
        :pswitch_1a7
        :pswitch_10f
        :pswitch_1a7
    .end packed-switch
.end method

.method private setFlowState(Landroid/net/LinkCapabilities$Flow;I)Z
    .registers 8
    .parameter "flow"
    .parameter "qos_status"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1833
    invoke-virtual {p1}, Landroid/net/LinkCapabilities$Flow;->getState()Landroid/net/LinkCapabilities$FlowState;

    #@4
    move-result-object v0

    #@5
    .line 1834
    .local v0, flowState:Landroid/net/LinkCapabilities$FlowState;
    packed-switch p2, :pswitch_data_192

    #@8
    .line 1902
    :goto_8
    return v1

    #@9
    .line 1837
    :pswitch_9
    sget-object v2, Landroid/net/LinkCapabilities$FlowState;->INACTIVE:Landroid/net/LinkCapabilities$FlowState;

    #@b
    if-eq v0, v2, :cond_46

    #@d
    .line 1839
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@f
    new-instance v3, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v4, "Abnomal Flow State Change: QoS ID ("

    #@16
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {p1}, Landroid/net/LinkCapabilities$Flow;->getID()I

    #@1d
    move-result v4

    #@1e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@21
    move-result-object v3

    #@22
    const-string v4, "), Current State ("

    #@24
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v3

    #@28
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v3

    #@2c
    const-string v4, "), New State ("

    #@2e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v3

    #@32
    sget-object v4, Landroid/net/LinkCapabilities$FlowState;->ACTIVATED:Landroid/net/LinkCapabilities$FlowState;

    #@34
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v3

    #@38
    const-string v4, ")"

    #@3a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v3

    #@3e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v3

    #@42
    invoke-virtual {v2, v3}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@45
    goto :goto_8

    #@46
    .line 1844
    :cond_46
    sget-object v1, Landroid/net/LinkCapabilities$FlowState;->ACTIVATED:Landroid/net/LinkCapabilities$FlowState;

    #@48
    invoke-virtual {p1, v1}, Landroid/net/LinkCapabilities$Flow;->setState(Landroid/net/LinkCapabilities$FlowState;)V

    #@4b
    .line 1902
    :cond_4b
    :goto_4b
    const/4 v1, 0x1

    #@4c
    goto :goto_8

    #@4d
    .line 1848
    :pswitch_4d
    sget-object v2, Landroid/net/LinkCapabilities$FlowState;->INACTIVE:Landroid/net/LinkCapabilities$FlowState;

    #@4f
    if-ne v0, v2, :cond_8b

    #@51
    .line 1850
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@53
    new-instance v3, Ljava/lang/StringBuilder;

    #@55
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@58
    const-string v4, "Abnomal Flow State Change: QoS ID ("

    #@5a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v3

    #@5e
    invoke-virtual {p1}, Landroid/net/LinkCapabilities$Flow;->getID()I

    #@61
    move-result v4

    #@62
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@65
    move-result-object v3

    #@66
    const-string v4, "), Current State ("

    #@68
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v3

    #@6c
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v3

    #@70
    const-string v4, "), New State ("

    #@72
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v3

    #@76
    sget-object v4, Landroid/net/LinkCapabilities$FlowState;->ENABLED:Landroid/net/LinkCapabilities$FlowState;

    #@78
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v3

    #@7c
    const-string v4, ")"

    #@7e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v3

    #@82
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@85
    move-result-object v3

    #@86
    invoke-virtual {v2, v3}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@89
    goto/16 :goto_8

    #@8b
    .line 1855
    :cond_8b
    sget-object v1, Landroid/net/LinkCapabilities$FlowState;->ENABLED:Landroid/net/LinkCapabilities$FlowState;

    #@8d
    invoke-virtual {p1, v1}, Landroid/net/LinkCapabilities$Flow;->setState(Landroid/net/LinkCapabilities$FlowState;)V

    #@90
    goto :goto_4b

    #@91
    .line 1859
    :pswitch_91
    sget-object v2, Landroid/net/LinkCapabilities$FlowState;->INACTIVE:Landroid/net/LinkCapabilities$FlowState;

    #@93
    if-ne v0, v2, :cond_cf

    #@95
    .line 1861
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@97
    new-instance v3, Ljava/lang/StringBuilder;

    #@99
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@9c
    const-string v4, "Abnomal Flow State Change: QoS ID ("

    #@9e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v3

    #@a2
    invoke-virtual {p1}, Landroid/net/LinkCapabilities$Flow;->getID()I

    #@a5
    move-result v4

    #@a6
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v3

    #@aa
    const-string v4, "), Current State ("

    #@ac
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@af
    move-result-object v3

    #@b0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@b3
    move-result-object v3

    #@b4
    const-string v4, "), New State ("

    #@b6
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b9
    move-result-object v3

    #@ba
    sget-object v4, Landroid/net/LinkCapabilities$FlowState;->INACTIVE:Landroid/net/LinkCapabilities$FlowState;

    #@bc
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v3

    #@c0
    const-string v4, ")"

    #@c2
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c5
    move-result-object v3

    #@c6
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c9
    move-result-object v3

    #@ca
    invoke-virtual {v2, v3}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@cd
    goto/16 :goto_8

    #@cf
    .line 1866
    :cond_cf
    sget-object v1, Landroid/net/LinkCapabilities$FlowState;->INACTIVE:Landroid/net/LinkCapabilities$FlowState;

    #@d1
    invoke-virtual {p1, v1}, Landroid/net/LinkCapabilities$Flow;->setState(Landroid/net/LinkCapabilities$FlowState;)V

    #@d4
    goto/16 :goto_4b

    #@d6
    .line 1870
    :pswitch_d6
    sget-object v2, Landroid/net/LinkCapabilities$FlowState;->INACTIVE:Landroid/net/LinkCapabilities$FlowState;

    #@d8
    if-ne v0, v2, :cond_114

    #@da
    .line 1872
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@dc
    new-instance v3, Ljava/lang/StringBuilder;

    #@de
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@e1
    const-string v4, "Abnomal Flow State Change: QoS ID ("

    #@e3
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e6
    move-result-object v3

    #@e7
    invoke-virtual {p1}, Landroid/net/LinkCapabilities$Flow;->getID()I

    #@ea
    move-result v4

    #@eb
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ee
    move-result-object v3

    #@ef
    const-string v4, "), Current State ("

    #@f1
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f4
    move-result-object v3

    #@f5
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@f8
    move-result-object v3

    #@f9
    const-string v4, "), New State ("

    #@fb
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fe
    move-result-object v3

    #@ff
    sget-object v4, Landroid/net/LinkCapabilities$FlowState;->DISABLED:Landroid/net/LinkCapabilities$FlowState;

    #@101
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@104
    move-result-object v3

    #@105
    const-string v4, ")"

    #@107
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10a
    move-result-object v3

    #@10b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10e
    move-result-object v3

    #@10f
    invoke-virtual {v2, v3}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@112
    goto/16 :goto_8

    #@114
    .line 1877
    :cond_114
    sget-object v1, Landroid/net/LinkCapabilities$FlowState;->DISABLED:Landroid/net/LinkCapabilities$FlowState;

    #@116
    invoke-virtual {p1, v1}, Landroid/net/LinkCapabilities$Flow;->setState(Landroid/net/LinkCapabilities$FlowState;)V

    #@119
    goto/16 :goto_4b

    #@11b
    .line 1881
    :pswitch_11b
    sget-object v2, Landroid/net/LinkCapabilities$FlowState;->INACTIVE:Landroid/net/LinkCapabilities$FlowState;

    #@11d
    if-ne v0, v2, :cond_159

    #@11f
    .line 1883
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@121
    new-instance v3, Ljava/lang/StringBuilder;

    #@123
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@126
    const-string v4, "Abnomal Flow State Change: QoS ID ("

    #@128
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12b
    move-result-object v3

    #@12c
    invoke-virtual {p1}, Landroid/net/LinkCapabilities$Flow;->getID()I

    #@12f
    move-result v4

    #@130
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@133
    move-result-object v3

    #@134
    const-string v4, "), Current State ("

    #@136
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@139
    move-result-object v3

    #@13a
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@13d
    move-result-object v3

    #@13e
    const-string v4, "), New State ("

    #@140
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@143
    move-result-object v3

    #@144
    sget-object v4, Landroid/net/LinkCapabilities$FlowState;->SUSPENDED:Landroid/net/LinkCapabilities$FlowState;

    #@146
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@149
    move-result-object v3

    #@14a
    const-string v4, ")"

    #@14c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14f
    move-result-object v3

    #@150
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@153
    move-result-object v3

    #@154
    invoke-virtual {v2, v3}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@157
    goto/16 :goto_8

    #@159
    .line 1888
    :cond_159
    sget-object v1, Landroid/net/LinkCapabilities$FlowState;->SUSPENDED:Landroid/net/LinkCapabilities$FlowState;

    #@15b
    invoke-virtual {p1, v1}, Landroid/net/LinkCapabilities$Flow;->setState(Landroid/net/LinkCapabilities$FlowState;)V

    #@15e
    goto/16 :goto_4b

    #@160
    .line 1892
    :pswitch_160
    sget-object v2, Landroid/net/LinkCapabilities$FlowState;->INACTIVE:Landroid/net/LinkCapabilities$FlowState;

    #@162
    if-ne v0, v2, :cond_4b

    #@164
    .line 1894
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@166
    new-instance v3, Ljava/lang/StringBuilder;

    #@168
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@16b
    const-string v4, "Abnomal Flow State Change: QoS ID ("

    #@16d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@170
    move-result-object v3

    #@171
    invoke-virtual {p1}, Landroid/net/LinkCapabilities$Flow;->getID()I

    #@174
    move-result v4

    #@175
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@178
    move-result-object v3

    #@179
    const-string v4, "), Modified Event Received at State ("

    #@17b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17e
    move-result-object v3

    #@17f
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@182
    move-result-object v3

    #@183
    const-string v4, ")"

    #@185
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@188
    move-result-object v3

    #@189
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18c
    move-result-object v3

    #@18d
    invoke-virtual {v2, v3}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@190
    goto/16 :goto_8

    #@192
    .line 1834
    :pswitch_data_192
    .packed-switch 0x1
        :pswitch_9
        :pswitch_160
        :pswitch_91
        :pswitch_11b
        :pswitch_4d
        :pswitch_d6
    .end packed-switch
.end method


# virtual methods
.method public enter()V
    .registers 5

    #@0
    .prologue
    .line 1613
    const/4 v0, 0x0

    #@1
    sput v0, Lcom/android/internal/telephony/DataConnection;->rilFailCount:I

    #@3
    .line 1628
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->mConnectionParams:Lcom/android/internal/telephony/DataConnection$ConnectionParams;

    #@5
    if-eqz v0, :cond_2d

    #@7
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->mFailCause:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@9
    if-eqz v0, :cond_2d

    #@b
    .line 1630
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@d
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->mConnectionParams:Lcom/android/internal/telephony/DataConnection$ConnectionParams;

    #@f
    iget-object v2, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->mFailCause:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@11
    invoke-static {v0, v1, v2}, Lcom/android/internal/telephony/DataConnection;->access$800(Lcom/android/internal/telephony/DataConnection;Lcom/android/internal/telephony/DataConnection$ConnectionParams;Lcom/android/internal/telephony/DataConnection$FailCause;)V

    #@14
    .line 1633
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->mConnectionParams:Lcom/android/internal/telephony/DataConnection$ConnectionParams;

    #@16
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnection$ConnectionParams;->apn:Lcom/android/internal/telephony/DataProfile;

    #@18
    if-eqz v0, :cond_2d

    #@1a
    invoke-static {}, Lcom/android/internal/telephony/DataConnection;->access$1200()Z

    #@1d
    move-result v0

    #@1e
    if-eqz v0, :cond_2d

    #@20
    .line 1635
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->mConnectionParams:Lcom/android/internal/telephony/DataConnection$ConnectionParams;

    #@22
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnection$ConnectionParams;->apn:Lcom/android/internal/telephony/DataProfile;

    #@24
    iget v0, v0, Lcom/android/internal/telephony/DataProfile;->id:I

    #@26
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@28
    iget-object v1, v1, Lcom/android/internal/telephony/DataConnection;->mLinkProperties:Landroid/net/LinkProperties;

    #@2a
    invoke-static {v0, v1}, Lcom/android/internal/telephony/PDPContextStateBroadcaster;->sendConnected(ILandroid/net/LinkProperties;)V

    #@2d
    .line 1642
    :cond_2d
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@2f
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnection;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@31
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@33
    invoke-interface {v0}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@36
    move-result-object v0

    #@37
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_QOS_NOTIFY:Z

    #@39
    if-eqz v0, :cond_6a

    #@3b
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->mConnectionParams:Lcom/android/internal/telephony/DataConnection$ConnectionParams;

    #@3d
    if-eqz v0, :cond_6a

    #@3f
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->mConnectionParams:Lcom/android/internal/telephony/DataConnection$ConnectionParams;

    #@41
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnection$ConnectionParams;->apn:Lcom/android/internal/telephony/DataProfile;

    #@43
    const-string v1, "ims"

    #@45
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/DataProfile;->canHandleType(Ljava/lang/String;)Z

    #@48
    move-result v0

    #@49
    if-nez v0, :cond_57

    #@4b
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->mConnectionParams:Lcom/android/internal/telephony/DataConnection$ConnectionParams;

    #@4d
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnection$ConnectionParams;->apn:Lcom/android/internal/telephony/DataProfile;

    #@4f
    const-string v1, "emergency"

    #@51
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/DataProfile;->canHandleType(Ljava/lang/String;)Z

    #@54
    move-result v0

    #@55
    if-eqz v0, :cond_6a

    #@57
    .line 1647
    :cond_57
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@59
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnection;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@5b
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@5d
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@5f
    invoke-virtual {v1}, Lcom/android/internal/telephony/DataConnection;->getHandler()Landroid/os/Handler;

    #@62
    move-result-object v1

    #@63
    const v2, 0x40009

    #@66
    const/4 v3, 0x0

    #@67
    invoke-interface {v0, v1, v2, v3}, Lcom/android/internal/telephony/CommandsInterface;->registerForDataQosChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@6a
    .line 1651
    :cond_6a
    return-void
.end method

.method public exit()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1656
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@3
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnection;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@5
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@7
    invoke-interface {v0}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@a
    move-result-object v0

    #@b
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_QOS_NOTIFY:Z

    #@d
    if-eqz v0, :cond_41

    #@f
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->mConnectionParams:Lcom/android/internal/telephony/DataConnection$ConnectionParams;

    #@11
    if-eqz v0, :cond_41

    #@13
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->mConnectionParams:Lcom/android/internal/telephony/DataConnection$ConnectionParams;

    #@15
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnection$ConnectionParams;->apn:Lcom/android/internal/telephony/DataProfile;

    #@17
    const-string v1, "ims"

    #@19
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/DataProfile;->canHandleType(Ljava/lang/String;)Z

    #@1c
    move-result v0

    #@1d
    if-nez v0, :cond_2b

    #@1f
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->mConnectionParams:Lcom/android/internal/telephony/DataConnection$ConnectionParams;

    #@21
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnection$ConnectionParams;->apn:Lcom/android/internal/telephony/DataProfile;

    #@23
    const-string v1, "emergency"

    #@25
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/DataProfile;->canHandleType(Ljava/lang/String;)Z

    #@28
    move-result v0

    #@29
    if-eqz v0, :cond_41

    #@2b
    .line 1661
    :cond_2b
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@2d
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnection;->mCapabilities:Landroid/net/LinkCapabilities;

    #@2f
    invoke-virtual {v0}, Landroid/net/LinkCapabilities;->removeAllFlow()V

    #@32
    .line 1662
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@34
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnection;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@36
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@38
    iget-object v1, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@3a
    invoke-virtual {v1}, Lcom/android/internal/telephony/DataConnection;->getHandler()Landroid/os/Handler;

    #@3d
    move-result-object v1

    #@3e
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForDataQosChanged(Landroid/os/Handler;)V

    #@41
    .line 1667
    :cond_41
    iput-object v2, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->mConnectionParams:Lcom/android/internal/telephony/DataConnection$ConnectionParams;

    #@43
    .line 1668
    iput-object v2, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->mFailCause:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@45
    .line 1669
    return-void
.end method

.method public processMessage(Landroid/os/Message;)Z
    .registers 13
    .parameter "msg"

    #@0
    .prologue
    const v10, 0x40008

    #@3
    const/4 v7, 0x0

    #@4
    const v9, 0x40007

    #@7
    .line 1680
    iget v4, p1, Landroid/os/Message;->what:I

    #@9
    sparse-switch v4, :sswitch_data_200

    #@c
    .line 1825
    const/4 v3, 0x0

    #@d
    .line 1828
    .local v3, retVal:Z
    :goto_d
    return v3

    #@e
    .line 1682
    .end local v3           #retVal:Z
    :sswitch_e
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@10
    iget v5, v4, Lcom/android/internal/telephony/DataConnection;->mRefCount:I

    #@12
    add-int/lit8 v5, v5, 0x1

    #@14
    iput v5, v4, Lcom/android/internal/telephony/DataConnection;->mRefCount:I

    #@16
    .line 1683
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@18
    new-instance v5, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v6, "DcActiveState msg.what=EVENT_CONNECT RefCount="

    #@1f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v5

    #@23
    iget-object v6, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@25
    iget v6, v6, Lcom/android/internal/telephony/DataConnection;->mRefCount:I

    #@27
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v5

    #@2b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v5

    #@2f
    invoke-virtual {v4, v5}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@32
    .line 1684
    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@34
    if-eqz v4, :cond_41

    #@36
    .line 1685
    iget-object v5, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@38
    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@3a
    check-cast v4, Lcom/android/internal/telephony/DataConnection$ConnectionParams;

    #@3c
    sget-object v6, Lcom/android/internal/telephony/DataConnection$FailCause;->NONE:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@3e
    invoke-static {v5, v4, v6}, Lcom/android/internal/telephony/DataConnection;->access$800(Lcom/android/internal/telephony/DataConnection;Lcom/android/internal/telephony/DataConnection$ConnectionParams;Lcom/android/internal/telephony/DataConnection$FailCause;)V

    #@41
    .line 1687
    :cond_41
    const/4 v3, 0x1

    #@42
    .line 1688
    .restart local v3       #retVal:Z
    goto :goto_d

    #@43
    .line 1690
    .end local v3           #retVal:Z
    :sswitch_43
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@45
    iget v5, v4, Lcom/android/internal/telephony/DataConnection;->mRefCount:I

    #@47
    add-int/lit8 v5, v5, -0x1

    #@49
    iput v5, v4, Lcom/android/internal/telephony/DataConnection;->mRefCount:I

    #@4b
    .line 1691
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@4d
    new-instance v5, Ljava/lang/StringBuilder;

    #@4f
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@52
    const-string v6, "DcActiveState msg.what=EVENT_DISCONNECT RefCount="

    #@54
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v5

    #@58
    iget-object v6, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@5a
    iget v6, v6, Lcom/android/internal/telephony/DataConnection;->mRefCount:I

    #@5c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v5

    #@60
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@63
    move-result-object v5

    #@64
    invoke-virtual {v4, v5}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@67
    .line 1692
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@69
    iget v4, v4, Lcom/android/internal/telephony/DataConnection;->mRefCount:I

    #@6b
    if-nez v4, :cond_a1

    #@6d
    .line 1694
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@6f
    check-cast v2, Lcom/android/internal/telephony/DataConnection$DisconnectParams;

    #@71
    .line 1695
    .local v2, dp:Lcom/android/internal/telephony/DataConnection$DisconnectParams;
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@73
    iget v4, v4, Lcom/android/internal/telephony/DataConnection;->mTag:I

    #@75
    iput v4, v2, Lcom/android/internal/telephony/DataConnection$DisconnectParams;->tag:I

    #@77
    .line 1698
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->mConnectionParams:Lcom/android/internal/telephony/DataConnection$ConnectionParams;

    #@79
    iget-object v4, v4, Lcom/android/internal/telephony/DataConnection$ConnectionParams;->apn:Lcom/android/internal/telephony/DataProfile;

    #@7b
    if-eqz v4, :cond_8e

    #@7d
    invoke-static {}, Lcom/android/internal/telephony/DataConnection;->access$1200()Z

    #@80
    move-result v4

    #@81
    if-eqz v4, :cond_8e

    #@83
    .line 1700
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->mConnectionParams:Lcom/android/internal/telephony/DataConnection$ConnectionParams;

    #@85
    iget-object v4, v4, Lcom/android/internal/telephony/DataConnection$ConnectionParams;->apn:Lcom/android/internal/telephony/DataProfile;

    #@87
    iget v4, v4, Lcom/android/internal/telephony/DataProfile;->id:I

    #@89
    iget-object v5, v2, Lcom/android/internal/telephony/DataConnection$DisconnectParams;->reason:Ljava/lang/String;

    #@8b
    invoke-static {v4, v5}, Lcom/android/internal/telephony/PDPContextStateBroadcaster;->sendDisconnected(ILjava/lang/String;)V

    #@8e
    .line 1704
    :cond_8e
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@90
    invoke-static {v4, v2}, Lcom/android/internal/telephony/DataConnection;->access$2300(Lcom/android/internal/telephony/DataConnection;Ljava/lang/Object;)V

    #@93
    .line 1705
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@95
    iget-object v5, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@97
    invoke-static {v5}, Lcom/android/internal/telephony/DataConnection;->access$3400(Lcom/android/internal/telephony/DataConnection;)Lcom/android/internal/telephony/DataConnection$DcDisconnectingState;

    #@9a
    move-result-object v5

    #@9b
    invoke-static {v4, v5}, Lcom/android/internal/telephony/DataConnection;->access$3500(Lcom/android/internal/telephony/DataConnection;Lcom/android/internal/util/IState;)V

    #@9e
    .line 1711
    .end local v2           #dp:Lcom/android/internal/telephony/DataConnection$DisconnectParams;
    :cond_9e
    :goto_9e
    const/4 v3, 0x1

    #@9f
    .line 1712
    .restart local v3       #retVal:Z
    goto/16 :goto_d

    #@a1
    .line 1707
    .end local v3           #retVal:Z
    :cond_a1
    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@a3
    if-eqz v4, :cond_9e

    #@a5
    .line 1708
    iget-object v5, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@a7
    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@a9
    check-cast v4, Lcom/android/internal/telephony/DataConnection$DisconnectParams;

    #@ab
    invoke-static {v5, v4, v7}, Lcom/android/internal/telephony/DataConnection;->access$1300(Lcom/android/internal/telephony/DataConnection;Lcom/android/internal/telephony/DataConnection$DisconnectParams;Z)V

    #@ae
    goto :goto_9e

    #@af
    .line 1716
    :sswitch_af
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@b1
    new-instance v5, Ljava/lang/StringBuilder;

    #@b3
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@b6
    const-string v6, "DcActiveState msg.what=EVENT_DISCONNECT_ALL RefCount="

    #@b8
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bb
    move-result-object v5

    #@bc
    iget-object v6, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@be
    iget v6, v6, Lcom/android/internal/telephony/DataConnection;->mRefCount:I

    #@c0
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v5

    #@c4
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c7
    move-result-object v5

    #@c8
    invoke-virtual {v4, v5}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@cb
    .line 1718
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@cd
    iput v7, v4, Lcom/android/internal/telephony/DataConnection;->mRefCount:I

    #@cf
    .line 1719
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@d1
    check-cast v2, Lcom/android/internal/telephony/DataConnection$DisconnectParams;

    #@d3
    .line 1720
    .restart local v2       #dp:Lcom/android/internal/telephony/DataConnection$DisconnectParams;
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@d5
    iget v4, v4, Lcom/android/internal/telephony/DataConnection;->mTag:I

    #@d7
    iput v4, v2, Lcom/android/internal/telephony/DataConnection$DisconnectParams;->tag:I

    #@d9
    .line 1723
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->mConnectionParams:Lcom/android/internal/telephony/DataConnection$ConnectionParams;

    #@db
    iget-object v4, v4, Lcom/android/internal/telephony/DataConnection$ConnectionParams;->apn:Lcom/android/internal/telephony/DataProfile;

    #@dd
    if-eqz v4, :cond_f0

    #@df
    invoke-static {}, Lcom/android/internal/telephony/DataConnection;->access$1200()Z

    #@e2
    move-result v4

    #@e3
    if-eqz v4, :cond_f0

    #@e5
    .line 1725
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->mConnectionParams:Lcom/android/internal/telephony/DataConnection$ConnectionParams;

    #@e7
    iget-object v4, v4, Lcom/android/internal/telephony/DataConnection$ConnectionParams;->apn:Lcom/android/internal/telephony/DataProfile;

    #@e9
    iget v4, v4, Lcom/android/internal/telephony/DataProfile;->id:I

    #@eb
    iget-object v5, v2, Lcom/android/internal/telephony/DataConnection$DisconnectParams;->reason:Ljava/lang/String;

    #@ed
    invoke-static {v4, v5}, Lcom/android/internal/telephony/PDPContextStateBroadcaster;->sendDisconnected(ILjava/lang/String;)V

    #@f0
    .line 1729
    :cond_f0
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@f2
    invoke-static {v4, v2}, Lcom/android/internal/telephony/DataConnection;->access$2300(Lcom/android/internal/telephony/DataConnection;Ljava/lang/Object;)V

    #@f5
    .line 1730
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@f7
    iget-object v5, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@f9
    invoke-static {v5}, Lcom/android/internal/telephony/DataConnection;->access$3400(Lcom/android/internal/telephony/DataConnection;)Lcom/android/internal/telephony/DataConnection$DcDisconnectingState;

    #@fc
    move-result-object v5

    #@fd
    invoke-static {v4, v5}, Lcom/android/internal/telephony/DataConnection;->access$3600(Lcom/android/internal/telephony/DataConnection;Lcom/android/internal/util/IState;)V

    #@100
    .line 1731
    const/4 v3, 0x1

    #@101
    .line 1732
    .restart local v3       #retVal:Z
    goto/16 :goto_d

    #@103
    .line 1737
    .end local v2           #dp:Lcom/android/internal/telephony/DataConnection$DisconnectParams;
    .end local v3           #retVal:Z
    :sswitch_103
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@105
    const-string v5, "DcActiveState: msg.what=REQ_UPDATE_PCSCF_ADDRESS"

    #@107
    invoke-virtual {v4, v5}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@10a
    .line 1739
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@10c
    iget-object v4, v4, Lcom/android/internal/telephony/DataConnection;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@10e
    iget-object v4, v4, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@110
    invoke-interface {v4}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@113
    move-result-object v4

    #@114
    iget-boolean v4, v4, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_PCSCF_INTERFACE:Z

    #@116
    if-eqz v4, :cond_157

    #@118
    .line 1743
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@11a
    iget-object v4, v4, Lcom/android/internal/telephony/DataConnection;->mLinkProperties:Landroid/net/LinkProperties;

    #@11c
    invoke-virtual {v4}, Landroid/net/LinkProperties;->isIpv4Connected()Z

    #@11f
    move-result v4

    #@120
    if-eqz v4, :cond_15a

    #@122
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@124
    iget-object v4, v4, Lcom/android/internal/telephony/DataConnection;->mLinkProperties:Landroid/net/LinkProperties;

    #@126
    invoke-virtual {v4}, Landroid/net/LinkProperties;->isIpv6Connected()Z

    #@129
    move-result v4

    #@12a
    if-eqz v4, :cond_15a

    #@12c
    .line 1745
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@12e
    const-string v5, "DcActiveState Request IPv4v6 PCSCF addresses"

    #@130
    invoke-virtual {v4, v5}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@133
    .line 1746
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@135
    iget-object v4, v4, Lcom/android/internal/telephony/DataConnection;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@137
    iget-object v4, v4, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@139
    iget-object v5, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@13b
    iget v5, v5, Lcom/android/internal/telephony/DataConnection;->cid:I

    #@13d
    const-string v6, "IPV4V6"

    #@13f
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@141
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->mConnectionParams:Lcom/android/internal/telephony/DataConnection$ConnectionParams;

    #@143
    invoke-virtual {v7, v9, v8}, Lcom/android/internal/telephony/DataConnection;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@146
    move-result-object v7

    #@147
    invoke-interface {v4, v5, v6, v7}, Lcom/android/internal/telephony/CommandsInterface;->getPcscfAddress(ILjava/lang/String;Landroid/os/Message;)V

    #@14a
    .line 1767
    :goto_14a
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@14c
    iget-object v5, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@14e
    invoke-virtual {v5, v10}, Lcom/android/internal/telephony/DataConnection;->obtainMessage(I)Landroid/os/Message;

    #@151
    move-result-object v5

    #@152
    const-wide/16 v6, 0xbb8

    #@154
    invoke-virtual {v4, v5, v6, v7}, Lcom/android/internal/telephony/DataConnection;->sendMessageDelayed(Landroid/os/Message;J)V

    #@157
    .line 1769
    :cond_157
    const/4 v3, 0x1

    #@158
    .line 1770
    .restart local v3       #retVal:Z
    goto/16 :goto_d

    #@15a
    .line 1748
    .end local v3           #retVal:Z
    :cond_15a
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@15c
    iget-object v4, v4, Lcom/android/internal/telephony/DataConnection;->mLinkProperties:Landroid/net/LinkProperties;

    #@15e
    invoke-virtual {v4}, Landroid/net/LinkProperties;->isIpv4Connected()Z

    #@161
    move-result v4

    #@162
    if-eqz v4, :cond_183

    #@164
    .line 1750
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@166
    const-string v5, "DcActiveState Request IPv4 PCSCF addresses"

    #@168
    invoke-virtual {v4, v5}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@16b
    .line 1751
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@16d
    iget-object v4, v4, Lcom/android/internal/telephony/DataConnection;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@16f
    iget-object v4, v4, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@171
    iget-object v5, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@173
    iget v5, v5, Lcom/android/internal/telephony/DataConnection;->cid:I

    #@175
    const-string v6, "IP"

    #@177
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@179
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->mConnectionParams:Lcom/android/internal/telephony/DataConnection$ConnectionParams;

    #@17b
    invoke-virtual {v7, v9, v8}, Lcom/android/internal/telephony/DataConnection;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@17e
    move-result-object v7

    #@17f
    invoke-interface {v4, v5, v6, v7}, Lcom/android/internal/telephony/CommandsInterface;->getPcscfAddress(ILjava/lang/String;Landroid/os/Message;)V

    #@182
    goto :goto_14a

    #@183
    .line 1753
    :cond_183
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@185
    iget-object v4, v4, Lcom/android/internal/telephony/DataConnection;->mLinkProperties:Landroid/net/LinkProperties;

    #@187
    invoke-virtual {v4}, Landroid/net/LinkProperties;->isIpv6Connected()Z

    #@18a
    move-result v4

    #@18b
    if-eqz v4, :cond_1ac

    #@18d
    .line 1755
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@18f
    const-string v5, "DcActiveState Request IPv6 PCSCF addresses"

    #@191
    invoke-virtual {v4, v5}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@194
    .line 1756
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@196
    iget-object v4, v4, Lcom/android/internal/telephony/DataConnection;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@198
    iget-object v4, v4, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@19a
    iget-object v5, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@19c
    iget v5, v5, Lcom/android/internal/telephony/DataConnection;->cid:I

    #@19e
    const-string v6, "IPV6"

    #@1a0
    iget-object v7, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@1a2
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->mConnectionParams:Lcom/android/internal/telephony/DataConnection$ConnectionParams;

    #@1a4
    invoke-virtual {v7, v9, v8}, Lcom/android/internal/telephony/DataConnection;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@1a7
    move-result-object v7

    #@1a8
    invoke-interface {v4, v5, v6, v7}, Lcom/android/internal/telephony/CommandsInterface;->getPcscfAddress(ILjava/lang/String;Landroid/os/Message;)V

    #@1ab
    goto :goto_14a

    #@1ac
    .line 1760
    :cond_1ac
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@1ae
    const-string v5, "DcActiveState SHOULD NOT BE CALLED"

    #@1b0
    invoke-virtual {v4, v5}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@1b3
    goto :goto_14a

    #@1b4
    .line 1773
    :sswitch_1b4
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@1b6
    const-string v5, "DcActiveState msg.what=EVENT_GET_PCSCF_ADDRESS_DONE"

    #@1b8
    invoke-virtual {v4, v5}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@1bb
    .line 1775
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1bd
    check-cast v0, Landroid/os/AsyncResult;

    #@1bf
    .line 1776
    .local v0, ar:Landroid/os/AsyncResult;
    iget-object v1, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@1c1
    check-cast v1, Lcom/android/internal/telephony/DataConnection$ConnectionParams;

    #@1c3
    .line 1778
    .local v1, cp:Lcom/android/internal/telephony/DataConnection$ConnectionParams;
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@1c5
    invoke-static {v4, v10}, Lcom/android/internal/telephony/DataConnection;->access$3700(Lcom/android/internal/telephony/DataConnection;I)V

    #@1c8
    .line 1779
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@1ca
    invoke-static {v4, v0}, Lcom/android/internal/telephony/DataConnection;->access$3000(Lcom/android/internal/telephony/DataConnection;Landroid/os/AsyncResult;)V

    #@1cd
    .line 1789
    const/4 v3, 0x1

    #@1ce
    .line 1790
    .restart local v3       #retVal:Z
    goto/16 :goto_d

    #@1d0
    .line 1793
    .end local v0           #ar:Landroid/os/AsyncResult;
    .end local v1           #cp:Lcom/android/internal/telephony/DataConnection$ConnectionParams;
    .end local v3           #retVal:Z
    :sswitch_1d0
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@1d2
    const-string v5, "DcActiveState msg.what=EVENT_GET_PCSCF_ADDRESS_FAIL"

    #@1d4
    invoke-virtual {v4, v5}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@1d7
    .line 1803
    const/4 v3, 0x1

    #@1d8
    .line 1804
    .restart local v3       #retVal:Z
    goto/16 :goto_d

    #@1da
    .line 1809
    .end local v3           #retVal:Z
    :sswitch_1da
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@1dc
    const-string v5, "DcActiveState msg.what=EVENT_QOS_CHANGED"

    #@1de
    invoke-virtual {v4, v5}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@1e1
    .line 1811
    iget-object v4, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@1e3
    iget-object v4, v4, Lcom/android/internal/telephony/DataConnection;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@1e5
    iget-object v4, v4, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@1e7
    invoke-interface {v4}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@1ea
    move-result-object v4

    #@1eb
    iget-boolean v4, v4, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_QOS_NOTIFY:Z

    #@1ed
    if-eqz v4, :cond_1fc

    #@1ef
    .line 1813
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1f1
    check-cast v0, Landroid/os/AsyncResult;

    #@1f3
    .line 1814
    .restart local v0       #ar:Landroid/os/AsyncResult;
    if-eqz v0, :cond_1fc

    #@1f5
    iget-object v4, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@1f7
    if-eqz v4, :cond_1fc

    #@1f9
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/DataConnection$DcActiveState;->onQoSChanged(Landroid/os/AsyncResult;)V

    #@1fc
    .line 1816
    .end local v0           #ar:Landroid/os/AsyncResult;
    :cond_1fc
    const/4 v3, 0x1

    #@1fd
    .line 1817
    .restart local v3       #retVal:Z
    goto/16 :goto_d

    #@1ff
    .line 1680
    nop

    #@200
    :sswitch_data_200
    .sparse-switch
        0x40000 -> :sswitch_e
        0x40004 -> :sswitch_43
        0x40006 -> :sswitch_af
        0x40007 -> :sswitch_1b4
        0x40008 -> :sswitch_1d0
        0x40009 -> :sswitch_1da
        0x4101c -> :sswitch_103
    .end sparse-switch
.end method

.method public setEnterNotificationParams(Lcom/android/internal/telephony/DataConnection$ConnectionParams;Lcom/android/internal/telephony/DataConnection$FailCause;)V
    .registers 3
    .parameter "cp"
    .parameter "cause"

    #@0
    .prologue
    .line 1607
    iput-object p1, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->mConnectionParams:Lcom/android/internal/telephony/DataConnection$ConnectionParams;

    #@2
    .line 1608
    iput-object p2, p0, Lcom/android/internal/telephony/DataConnection$DcActiveState;->mFailCause:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@4
    .line 1609
    return-void
.end method
