.class Lcom/android/internal/telephony/DataConnectionTracker$2;
.super Landroid/content/BroadcastReceiver;
.source "DataConnectionTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/DataConnectionTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/DataConnectionTracker;


# direct methods
.method constructor <init>(Lcom/android/internal/telephony/DataConnectionTracker;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 576
    iput-object p1, p0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 23
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 580
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v4

    #@4
    .line 581
    .local v4, action:Ljava/lang/String;
    move-object/from16 v0, p0

    #@6
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@8
    move-object/from16 v17, v0

    #@a
    new-instance v18, Ljava/lang/StringBuilder;

    #@c
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v19, "onReceive: action="

    #@11
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v18

    #@15
    move-object/from16 v0, v18

    #@17
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v18

    #@1b
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v18

    #@1f
    invoke-virtual/range {v17 .. v18}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@22
    .line 582
    const-string v17, "android.intent.action.SCREEN_ON"

    #@24
    move-object/from16 v0, v17

    #@26
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@29
    move-result v17

    #@2a
    if-eqz v17, :cond_e9

    #@2c
    .line 583
    move-object/from16 v0, p0

    #@2e
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@30
    move-object/from16 v17, v0

    #@32
    const/16 v18, 0x1

    #@34
    move/from16 v0, v18

    #@36
    move-object/from16 v1, v17

    #@38
    iput-boolean v0, v1, Lcom/android/internal/telephony/DataConnectionTracker;->mIsScreenOn:Z

    #@3a
    .line 584
    move-object/from16 v0, p0

    #@3c
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@3e
    move-object/from16 v17, v0

    #@40
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/telephony/DataConnectionTracker;->stopNetStatPoll()V

    #@43
    .line 585
    move-object/from16 v0, p0

    #@45
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@47
    move-object/from16 v17, v0

    #@49
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/telephony/DataConnectionTracker;->startNetStatPoll()V

    #@4c
    .line 586
    move-object/from16 v0, p0

    #@4e
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@50
    move-object/from16 v17, v0

    #@52
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/telephony/DataConnectionTracker;->restartDataStallAlarm()V

    #@55
    .line 588
    move-object/from16 v0, p0

    #@57
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@59
    move-object/from16 v17, v0

    #@5b
    move-object/from16 v0, v17

    #@5d
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@5f
    move-object/from16 v17, v0

    #@61
    move-object/from16 v0, v17

    #@63
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@65
    move-object/from16 v17, v0

    #@67
    invoke-interface/range {v17 .. v17}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@6a
    move-result-object v17

    #@6b
    move-object/from16 v0, v17

    #@6d
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_PSRETRY_ON_SCREENON:Z

    #@6f
    move/from16 v17, v0

    #@71
    if-eqz v17, :cond_e8

    #@73
    .line 589
    move-object/from16 v0, p0

    #@75
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@77
    move-object/from16 v17, v0

    #@79
    move-object/from16 v0, v17

    #@7b
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@7d
    move-object/from16 v17, v0

    #@7f
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@82
    move-result-object v17

    #@83
    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@86
    move-result-object v17

    #@87
    const-string v18, "airplane_mode_on"

    #@89
    const/16 v19, 0x0

    #@8b
    invoke-static/range {v17 .. v19}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@8e
    move-result v5

    #@8f
    .line 590
    .local v5, airplaneMode:I
    const/16 v17, 0x1

    #@91
    move/from16 v0, v17

    #@93
    if-eq v5, v0, :cond_e8

    #@95
    move-object/from16 v0, p0

    #@97
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@99
    move-object/from16 v17, v0

    #@9b
    move-object/from16 v0, v17

    #@9d
    iget-boolean v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mIsWifiConnected:Z

    #@9f
    move/from16 v17, v0

    #@a1
    if-nez v17, :cond_e8

    #@a3
    move-object/from16 v0, p0

    #@a5
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@a7
    move-object/from16 v17, v0

    #@a9
    move-object/from16 v0, v17

    #@ab
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@ad
    move-object/from16 v17, v0

    #@af
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@b2
    move-result-object v17

    #@b3
    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@b6
    move-result-object v17

    #@b7
    const-string v18, "mobile_data"

    #@b9
    const/16 v19, 0x1

    #@bb
    invoke-static/range {v17 .. v19}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@be
    move-result v17

    #@bf
    const/16 v18, 0x1

    #@c1
    move/from16 v0, v17

    #@c3
    move/from16 v1, v18

    #@c5
    if-ne v0, v1, :cond_e8

    #@c7
    .line 591
    move-object/from16 v0, p0

    #@c9
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@cb
    move-object/from16 v17, v0

    #@cd
    const-string v18, "Send Message : EVENT_PS_RETRY_RESET"

    #@cf
    invoke-virtual/range {v17 .. v18}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@d2
    .line 592
    move-object/from16 v0, p0

    #@d4
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@d6
    move-object/from16 v17, v0

    #@d8
    move-object/from16 v0, p0

    #@da
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@dc
    move-object/from16 v18, v0

    #@de
    const v19, 0x42026

    #@e1
    invoke-virtual/range {v18 .. v19}, Lcom/android/internal/telephony/DataConnectionTracker;->obtainMessage(I)Landroid/os/Message;

    #@e4
    move-result-object v18

    #@e5
    invoke-virtual/range {v17 .. v18}, Lcom/android/internal/telephony/DataConnectionTracker;->sendMessage(Landroid/os/Message;)Z

    #@e8
    .line 889
    .end local v5           #airplaneMode:I
    :cond_e8
    :goto_e8
    return-void

    #@e9
    .line 597
    :cond_e9
    const-string v17, "android.intent.action.SCREEN_OFF"

    #@eb
    move-object/from16 v0, v17

    #@ed
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f0
    move-result v17

    #@f1
    if-eqz v17, :cond_11d

    #@f3
    .line 598
    move-object/from16 v0, p0

    #@f5
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@f7
    move-object/from16 v17, v0

    #@f9
    const/16 v18, 0x0

    #@fb
    move/from16 v0, v18

    #@fd
    move-object/from16 v1, v17

    #@ff
    iput-boolean v0, v1, Lcom/android/internal/telephony/DataConnectionTracker;->mIsScreenOn:Z

    #@101
    .line 599
    move-object/from16 v0, p0

    #@103
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@105
    move-object/from16 v17, v0

    #@107
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/telephony/DataConnectionTracker;->stopNetStatPoll()V

    #@10a
    .line 600
    move-object/from16 v0, p0

    #@10c
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@10e
    move-object/from16 v17, v0

    #@110
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/telephony/DataConnectionTracker;->startNetStatPoll()V

    #@113
    .line 601
    move-object/from16 v0, p0

    #@115
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@117
    move-object/from16 v17, v0

    #@119
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/telephony/DataConnectionTracker;->restartDataStallAlarm()V

    #@11c
    goto :goto_e8

    #@11d
    .line 602
    :cond_11d
    move-object/from16 v0, p0

    #@11f
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@121
    move-object/from16 v17, v0

    #@123
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/telephony/DataConnectionTracker;->getActionIntentReconnectAlarm()Ljava/lang/String;

    #@126
    move-result-object v17

    #@127
    move-object/from16 v0, v17

    #@129
    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@12c
    move-result v17

    #@12d
    if-eqz v17, :cond_165

    #@12f
    .line 603
    move-object/from16 v0, p0

    #@131
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@133
    move-object/from16 v17, v0

    #@135
    new-instance v18, Ljava/lang/StringBuilder;

    #@137
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@13a
    const-string v19, "Reconnect alarm. Previous state was "

    #@13c
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13f
    move-result-object v18

    #@140
    move-object/from16 v0, p0

    #@142
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@144
    move-object/from16 v19, v0

    #@146
    move-object/from16 v0, v19

    #@148
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mState:Lcom/android/internal/telephony/DctConstants$State;

    #@14a
    move-object/from16 v19, v0

    #@14c
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@14f
    move-result-object v18

    #@150
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@153
    move-result-object v18

    #@154
    invoke-virtual/range {v17 .. v18}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@157
    .line 604
    move-object/from16 v0, p0

    #@159
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@15b
    move-object/from16 v17, v0

    #@15d
    move-object/from16 v0, v17

    #@15f
    move-object/from16 v1, p2

    #@161
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->onActionIntentReconnectAlarm(Landroid/content/Intent;)V

    #@164
    goto :goto_e8

    #@165
    .line 605
    :cond_165
    move-object/from16 v0, p0

    #@167
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@169
    move-object/from16 v17, v0

    #@16b
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/telephony/DataConnectionTracker;->getActionIntentDataStallAlarm()Ljava/lang/String;

    #@16e
    move-result-object v17

    #@16f
    move-object/from16 v0, v17

    #@171
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@174
    move-result v17

    #@175
    if-eqz v17, :cond_186

    #@177
    .line 606
    move-object/from16 v0, p0

    #@179
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@17b
    move-object/from16 v17, v0

    #@17d
    move-object/from16 v0, v17

    #@17f
    move-object/from16 v1, p2

    #@181
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->onActionIntentDataStallAlarm(Landroid/content/Intent;)V

    #@184
    goto/16 :goto_e8

    #@186
    .line 607
    :cond_186
    const-string v17, "android.net.wifi.STATE_CHANGE"

    #@188
    move-object/from16 v0, v17

    #@18a
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@18d
    move-result v17

    #@18e
    if-eqz v17, :cond_217

    #@190
    .line 608
    const-string v17, "networkInfo"

    #@192
    move-object/from16 v0, p2

    #@194
    move-object/from16 v1, v17

    #@196
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@199
    move-result-object v14

    #@19a
    check-cast v14, Landroid/net/NetworkInfo;

    #@19c
    .line 610
    .local v14, networkInfo:Landroid/net/NetworkInfo;
    move-object/from16 v0, p0

    #@19e
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@1a0
    move-object/from16 v18, v0

    #@1a2
    if-eqz v14, :cond_214

    #@1a4
    invoke-virtual {v14}, Landroid/net/NetworkInfo;->isConnected()Z

    #@1a7
    move-result v17

    #@1a8
    if-eqz v17, :cond_214

    #@1aa
    const/16 v17, 0x1

    #@1ac
    :goto_1ac
    move/from16 v0, v17

    #@1ae
    move-object/from16 v1, v18

    #@1b0
    iput-boolean v0, v1, Lcom/android/internal/telephony/DataConnectionTracker;->mIsWifiConnected:Z

    #@1b2
    .line 613
    move-object/from16 v0, p0

    #@1b4
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@1b6
    move-object/from16 v17, v0

    #@1b8
    move-object/from16 v0, v17

    #@1ba
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@1bc
    move-object/from16 v17, v0

    #@1be
    move-object/from16 v0, v17

    #@1c0
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@1c2
    move-object/from16 v17, v0

    #@1c4
    invoke-interface/range {v17 .. v17}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@1c7
    move-result-object v17

    #@1c8
    move-object/from16 v0, v17

    #@1ca
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DISPLAY_DATAERROR_ICON_SPRINT:Z

    #@1cc
    move/from16 v17, v0

    #@1ce
    if-eqz v17, :cond_e8

    #@1d0
    .line 615
    move-object/from16 v0, p0

    #@1d2
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@1d4
    move-object/from16 v17, v0

    #@1d6
    move-object/from16 v0, v17

    #@1d8
    iget-boolean v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mIsWifiConnected:Z

    #@1da
    move/from16 v17, v0

    #@1dc
    if-eqz v17, :cond_e8

    #@1de
    .line 617
    move-object/from16 v0, p0

    #@1e0
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@1e2
    move-object/from16 v17, v0

    #@1e4
    const-string v18, "com.lge.android.data.DisplayDataErrorIcon: No Display(wifi connected)"

    #@1e6
    invoke-virtual/range {v17 .. v18}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@1e9
    .line 618
    new-instance v3, Landroid/content/Intent;

    #@1eb
    const-string v17, "com.lge.android.data.DisplayDataErrorIcon"

    #@1ed
    move-object/from16 v0, v17

    #@1ef
    invoke-direct {v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@1f2
    .line 619
    .local v3, DisplayDataErrorIcon:Landroid/content/Intent;
    const-string v17, "Display"

    #@1f4
    const/16 v18, 0x0

    #@1f6
    move-object/from16 v0, v17

    #@1f8
    move/from16 v1, v18

    #@1fa
    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@1fd
    .line 620
    move-object/from16 v0, p0

    #@1ff
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@201
    move-object/from16 v17, v0

    #@203
    move-object/from16 v0, v17

    #@205
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@207
    move-object/from16 v17, v0

    #@209
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@20c
    move-result-object v17

    #@20d
    move-object/from16 v0, v17

    #@20f
    invoke-virtual {v0, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@212
    goto/16 :goto_e8

    #@214
    .line 610
    .end local v3           #DisplayDataErrorIcon:Landroid/content/Intent;
    :cond_214
    const/16 v17, 0x0

    #@216
    goto :goto_1ac

    #@217
    .line 626
    .end local v14           #networkInfo:Landroid/net/NetworkInfo;
    :cond_217
    const-string v17, "android.net.wifi.WIFI_STATE_CHANGED"

    #@219
    move-object/from16 v0, v17

    #@21b
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@21e
    move-result v17

    #@21f
    if-eqz v17, :cond_24c

    #@221
    .line 627
    const-string v17, "wifi_state"

    #@223
    const/16 v18, 0x4

    #@225
    move-object/from16 v0, p2

    #@227
    move-object/from16 v1, v17

    #@229
    move/from16 v2, v18

    #@22b
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@22e
    move-result v17

    #@22f
    const/16 v18, 0x3

    #@231
    move/from16 v0, v17

    #@233
    move/from16 v1, v18

    #@235
    if-ne v0, v1, :cond_24a

    #@237
    const/4 v8, 0x1

    #@238
    .line 630
    .local v8, enabled:Z
    :goto_238
    if-nez v8, :cond_e8

    #@23a
    .line 633
    move-object/from16 v0, p0

    #@23c
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@23e
    move-object/from16 v17, v0

    #@240
    const/16 v18, 0x0

    #@242
    move/from16 v0, v18

    #@244
    move-object/from16 v1, v17

    #@246
    iput-boolean v0, v1, Lcom/android/internal/telephony/DataConnectionTracker;->mIsWifiConnected:Z

    #@248
    goto/16 :goto_e8

    #@24a
    .line 627
    .end local v8           #enabled:Z
    :cond_24a
    const/4 v8, 0x0

    #@24b
    goto :goto_238

    #@24c
    .line 635
    :cond_24c
    const-string v17, "com.android.internal.telephony.dataconnectiontracker.intent_set_fail_data_setup_counter"

    #@24e
    move-object/from16 v0, v17

    #@250
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@253
    move-result v17

    #@254
    if-eqz v17, :cond_2d2

    #@256
    .line 636
    move-object/from16 v0, p0

    #@258
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@25a
    move-object/from16 v17, v0

    #@25c
    const-string v18, "fail_data_setup_counter"

    #@25e
    const/16 v19, 0x1

    #@260
    move-object/from16 v0, p2

    #@262
    move-object/from16 v1, v18

    #@264
    move/from16 v2, v19

    #@266
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@269
    move-result v18

    #@26a
    move/from16 v0, v18

    #@26c
    move-object/from16 v1, v17

    #@26e
    iput v0, v1, Lcom/android/internal/telephony/DataConnectionTracker;->mFailDataSetupCounter:I

    #@270
    .line 637
    move-object/from16 v0, p0

    #@272
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@274
    move-object/from16 v17, v0

    #@276
    const-string v18, "fail_data_setup_fail_cause"

    #@278
    sget-object v19, Lcom/android/internal/telephony/DataConnection$FailCause;->ERROR_UNSPECIFIED:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@27a
    invoke-virtual/range {v19 .. v19}, Lcom/android/internal/telephony/DataConnection$FailCause;->getErrorCode()I

    #@27d
    move-result v19

    #@27e
    move-object/from16 v0, p2

    #@280
    move-object/from16 v1, v18

    #@282
    move/from16 v2, v19

    #@284
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@287
    move-result v18

    #@288
    invoke-static/range {v18 .. v18}, Lcom/android/internal/telephony/DataConnection$FailCause;->fromInt(I)Lcom/android/internal/telephony/DataConnection$FailCause;

    #@28b
    move-result-object v18

    #@28c
    move-object/from16 v0, v18

    #@28e
    move-object/from16 v1, v17

    #@290
    iput-object v0, v1, Lcom/android/internal/telephony/DataConnectionTracker;->mFailDataSetupFailCause:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@292
    .line 640
    move-object/from16 v0, p0

    #@294
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@296
    move-object/from16 v17, v0

    #@298
    new-instance v18, Ljava/lang/StringBuilder;

    #@29a
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@29d
    const-string v19, "set mFailDataSetupCounter="

    #@29f
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a2
    move-result-object v18

    #@2a3
    move-object/from16 v0, p0

    #@2a5
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@2a7
    move-object/from16 v19, v0

    #@2a9
    move-object/from16 v0, v19

    #@2ab
    iget v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mFailDataSetupCounter:I

    #@2ad
    move/from16 v19, v0

    #@2af
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2b2
    move-result-object v18

    #@2b3
    const-string v19, " mFailDataSetupFailCause="

    #@2b5
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b8
    move-result-object v18

    #@2b9
    move-object/from16 v0, p0

    #@2bb
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@2bd
    move-object/from16 v19, v0

    #@2bf
    move-object/from16 v0, v19

    #@2c1
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mFailDataSetupFailCause:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@2c3
    move-object/from16 v19, v0

    #@2c5
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2c8
    move-result-object v18

    #@2c9
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2cc
    move-result-object v18

    #@2cd
    invoke-virtual/range {v17 .. v18}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@2d0
    goto/16 :goto_e8

    #@2d2
    .line 644
    :cond_2d2
    const-string v17, "android.intent.action.SIM_TYPE_CHANGED"

    #@2d4
    move-object/from16 v0, v17

    #@2d6
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2d9
    move-result v17

    #@2da
    if-eqz v17, :cond_2f2

    #@2dc
    .line 645
    move-object/from16 v0, p0

    #@2de
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@2e0
    move-object/from16 v17, v0

    #@2e2
    const-string v18, "android.intent.action.SIM_TYPE_CHANGED Intent received"

    #@2e4
    invoke-virtual/range {v17 .. v18}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@2e7
    .line 646
    move-object/from16 v0, p0

    #@2e9
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@2eb
    move-object/from16 v17, v0

    #@2ed
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/telephony/DataConnectionTracker;->loadPreferAPN()V

    #@2f0
    goto/16 :goto_e8

    #@2f2
    .line 650
    :cond_2f2
    const-string v17, "android.intent.action.ACTION_DELAY_MODE_CHANGE_FOR_IMS"

    #@2f4
    move-object/from16 v0, v17

    #@2f6
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2f9
    move-result v17

    #@2fa
    if-eqz v17, :cond_367

    #@2fc
    .line 651
    move-object/from16 v0, p0

    #@2fe
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@300
    move-object/from16 v17, v0

    #@302
    const-string v18, " !!!!!!!! ACTION_DELAY_MODE_CHANGE_FOR_IMS !!!!!!!!!"

    #@304
    invoke-virtual/range {v17 .. v18}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@307
    .line 652
    move-object/from16 v0, p0

    #@309
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@30b
    move-object/from16 v17, v0

    #@30d
    const/16 v18, 0x0

    #@30f
    move/from16 v0, v18

    #@311
    move-object/from16 v1, v17

    #@313
    iput-boolean v0, v1, Lcom/android/internal/telephony/DataConnectionTracker;->modeChangeAlarmState:Z

    #@315
    .line 654
    move-object/from16 v0, p0

    #@317
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@319
    move-object/from16 v17, v0

    #@31b
    move-object/from16 v0, v17

    #@31d
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mDelayModeChangeforIms:Landroid/app/PendingIntent;

    #@31f
    move-object/from16 v17, v0

    #@321
    if-eqz v17, :cond_35a

    #@323
    .line 655
    move-object/from16 v0, p0

    #@325
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@327
    move-object/from16 v17, v0

    #@329
    move-object/from16 v0, v17

    #@32b
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@32d
    move-object/from16 v17, v0

    #@32f
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@332
    move-result-object v17

    #@333
    const-string v18, "alarm"

    #@335
    invoke-virtual/range {v17 .. v18}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@338
    move-result-object v6

    #@339
    check-cast v6, Landroid/app/AlarmManager;

    #@33b
    .line 656
    .local v6, am:Landroid/app/AlarmManager;
    move-object/from16 v0, p0

    #@33d
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@33f
    move-object/from16 v17, v0

    #@341
    move-object/from16 v0, v17

    #@343
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mDelayModeChangeforIms:Landroid/app/PendingIntent;

    #@345
    move-object/from16 v17, v0

    #@347
    move-object/from16 v0, v17

    #@349
    invoke-virtual {v6, v0}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    #@34c
    .line 657
    move-object/from16 v0, p0

    #@34e
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@350
    move-object/from16 v17, v0

    #@352
    const/16 v18, 0x0

    #@354
    move-object/from16 v0, v18

    #@356
    move-object/from16 v1, v17

    #@358
    iput-object v0, v1, Lcom/android/internal/telephony/DataConnectionTracker;->mDelayModeChangeforIms:Landroid/app/PendingIntent;

    #@35a
    .line 659
    .end local v6           #am:Landroid/app/AlarmManager;
    :cond_35a
    move-object/from16 v0, p0

    #@35c
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@35e
    move-object/from16 v17, v0

    #@360
    const/16 v18, 0x0

    #@362
    invoke-virtual/range {v17 .. v18}, Lcom/android/internal/telephony/DataConnectionTracker;->changePreferrredNetworkMode(Z)V

    #@365
    goto/16 :goto_e8

    #@367
    .line 664
    :cond_367
    const-string v17, "com.lge.callingsetmobile"

    #@369
    move-object/from16 v0, v17

    #@36b
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@36e
    move-result v17

    #@36f
    if-eqz v17, :cond_3b9

    #@371
    .line 665
    move-object/from16 v0, p0

    #@373
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@375
    move-object/from16 v17, v0

    #@377
    const-string v18, "CallingSetMobileDataEnabled intent receiver"

    #@379
    invoke-virtual/range {v17 .. v18}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@37c
    .line 666
    const-string v17, "CallingPackagesName"

    #@37e
    move-object/from16 v0, p2

    #@380
    move-object/from16 v1, v17

    #@382
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@385
    move-result-object v15

    #@386
    .line 667
    .local v15, packname:Ljava/lang/String;
    const-string v17, "enabled"

    #@388
    const/16 v18, 0x0

    #@38a
    move-object/from16 v0, p2

    #@38c
    move-object/from16 v1, v17

    #@38e
    move/from16 v2, v18

    #@390
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@393
    move-result v17

    #@394
    invoke-static/range {v17 .. v17}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@397
    move-result-object v8

    #@398
    .line 668
    .local v8, enabled:Ljava/lang/Boolean;
    const/4 v12, 0x0

    #@399
    .line 669
    .local v12, mobile_enable:I
    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    #@39c
    move-result v17

    #@39d
    if-eqz v17, :cond_3b7

    #@39f
    .line 670
    const/4 v12, 0x1

    #@3a0
    .line 674
    :goto_3a0
    move-object/from16 v0, p0

    #@3a2
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@3a4
    move-object/from16 v17, v0

    #@3a6
    move-object/from16 v0, v17

    #@3a8
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->dataMgr:Lcom/android/internal/telephony/DataConnectionManager;

    #@3aa
    move-object/from16 v17, v0

    #@3ac
    sget-object v18, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->callingSetMobileDataEnabled:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@3ae
    move-object/from16 v0, v17

    #@3b0
    move-object/from16 v1, v18

    #@3b2
    invoke-virtual {v0, v1, v15, v12}, Lcom/android/internal/telephony/DataConnectionManager;->IntegrationAPI(Lcom/android/internal/telephony/DataConnectionManager$FunctionName;Ljava/lang/String;I)I

    #@3b5
    goto/16 :goto_e8

    #@3b7
    .line 672
    :cond_3b7
    const/4 v12, 0x0

    #@3b8
    goto :goto_3a0

    #@3b9
    .line 678
    .end local v8           #enabled:Ljava/lang/Boolean;
    .end local v12           #mobile_enable:I
    .end local v15           #packname:Ljava/lang/String;
    :cond_3b9
    const-string v17, "android.intent.action.IPV6_STATUS"

    #@3bb
    move-object/from16 v0, v17

    #@3bd
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3c0
    move-result v17

    #@3c1
    if-eqz v17, :cond_525

    #@3c3
    .line 679
    move-object/from16 v0, p0

    #@3c5
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@3c7
    move-object/from16 v17, v0

    #@3c9
    const-string v18, "[EHRPD_IPV6] !!!!!!!IPV6_STATUS CHANGE!!!!!!!!!"

    #@3cb
    invoke-virtual/range {v17 .. v18}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@3ce
    .line 681
    new-instance v11, Landroid/content/Intent;

    #@3d0
    const-string v17, "android.intent.action.IPV6_STATUS_RESULT"

    #@3d2
    move-object/from16 v0, v17

    #@3d4
    invoke-direct {v11, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@3d7
    .line 683
    .local v11, mIntent:Landroid/content/Intent;
    move-object/from16 v0, p0

    #@3d9
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@3db
    move-object/from16 v17, v0

    #@3dd
    move-object/from16 v0, v17

    #@3df
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@3e1
    move-object/from16 v17, v0

    #@3e3
    move-object/from16 v0, v17

    #@3e5
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@3e7
    move-object/from16 v17, v0

    #@3e9
    invoke-interface/range {v17 .. v17}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@3ec
    move-result-object v17

    #@3ed
    move-object/from16 v0, v17

    #@3ef
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_TCPIP_IP_V6_BLOCK_CONFIG_ON_EHRPD_VZW:Z

    #@3f1
    move/from16 v17, v0

    #@3f3
    const/16 v18, 0x1

    #@3f5
    move/from16 v0, v17

    #@3f7
    move/from16 v1, v18

    #@3f9
    if-ne v0, v1, :cond_4f8

    #@3fb
    move-object/from16 v0, p0

    #@3fd
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@3ff
    move-object/from16 v17, v0

    #@401
    move-object/from16 v0, v17

    #@403
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mAllApns:Ljava/util/ArrayList;

    #@405
    move-object/from16 v17, v0

    #@407
    if-eqz v17, :cond_4f8

    #@409
    .line 685
    const-string v17, "ipv6_status_result"

    #@40b
    const/16 v18, 0x1

    #@40d
    move-object/from16 v0, v17

    #@40f
    move/from16 v1, v18

    #@411
    invoke-virtual {v11, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@414
    .line 687
    const-string v17, "ipv6_status_enable"

    #@416
    const/16 v18, 0x0

    #@418
    move-object/from16 v0, p2

    #@41a
    move-object/from16 v1, v17

    #@41c
    move/from16 v2, v18

    #@41e
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@421
    move-result v9

    #@422
    .line 688
    .local v9, isEnable:Z
    move-object/from16 v0, p0

    #@424
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@426
    move-object/from16 v17, v0

    #@428
    new-instance v18, Ljava/lang/StringBuilder;

    #@42a
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@42d
    const-string v19, "[EHRPD_IPV6] !!!!!!!IPV6_STATUS CHANGE ==> "

    #@42f
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@432
    move-result-object v18

    #@433
    move-object/from16 v0, v18

    #@435
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@438
    move-result-object v18

    #@439
    const-string v19, "!!!!!!!!!"

    #@43b
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43e
    move-result-object v18

    #@43f
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@442
    move-result-object v18

    #@443
    invoke-virtual/range {v17 .. v18}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@446
    .line 689
    if-eqz v9, :cond_4a0

    #@448
    .line 691
    move-object/from16 v0, p0

    #@44a
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@44c
    move-object/from16 v17, v0

    #@44e
    move-object/from16 v0, v17

    #@450
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@452
    move-object/from16 v17, v0

    #@454
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@457
    move-result-object v17

    #@458
    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@45b
    move-result-object v17

    #@45c
    const-string v18, "data_ehrpd_internet_ipv6_enabled"

    #@45e
    const/16 v19, 0x1

    #@460
    invoke-static/range {v17 .. v19}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@463
    .line 692
    const-string v17, "ril.current.ehrpdipv6enable"

    #@465
    const-string v18, "1"

    #@467
    invoke-static/range {v17 .. v18}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@46a
    .line 693
    move-object/from16 v0, p0

    #@46c
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@46e
    move-object/from16 v17, v0

    #@470
    const/16 v18, 0x1

    #@472
    invoke-virtual/range {v17 .. v18}, Lcom/android/internal/telephony/DataConnectionTracker;->send_ehrpd_ipv6_setting(I)V

    #@475
    .line 694
    move-object/from16 v0, p0

    #@477
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@479
    move-object/from16 v17, v0

    #@47b
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/telephony/DataConnectionTracker;->enable_ehrpd_internet_ipv6()V

    #@47e
    .line 696
    const-string v17, "ipv6_status_enable"

    #@480
    const/16 v18, 0x1

    #@482
    move-object/from16 v0, v17

    #@484
    move/from16 v1, v18

    #@486
    invoke-virtual {v11, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@489
    .line 697
    move-object/from16 v0, p0

    #@48b
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@48d
    move-object/from16 v17, v0

    #@48f
    move-object/from16 v0, v17

    #@491
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@493
    move-object/from16 v17, v0

    #@495
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@498
    move-result-object v17

    #@499
    move-object/from16 v0, v17

    #@49b
    invoke-virtual {v0, v11}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@49e
    goto/16 :goto_e8

    #@4a0
    .line 701
    :cond_4a0
    move-object/from16 v0, p0

    #@4a2
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@4a4
    move-object/from16 v17, v0

    #@4a6
    move-object/from16 v0, v17

    #@4a8
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@4aa
    move-object/from16 v17, v0

    #@4ac
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@4af
    move-result-object v17

    #@4b0
    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@4b3
    move-result-object v17

    #@4b4
    const-string v18, "data_ehrpd_internet_ipv6_enabled"

    #@4b6
    const/16 v19, 0x0

    #@4b8
    invoke-static/range {v17 .. v19}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@4bb
    .line 702
    const-string v17, "ril.current.ehrpdipv6enable"

    #@4bd
    const-string v18, "0"

    #@4bf
    invoke-static/range {v17 .. v18}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@4c2
    .line 703
    move-object/from16 v0, p0

    #@4c4
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@4c6
    move-object/from16 v17, v0

    #@4c8
    const/16 v18, 0x0

    #@4ca
    invoke-virtual/range {v17 .. v18}, Lcom/android/internal/telephony/DataConnectionTracker;->send_ehrpd_ipv6_setting(I)V

    #@4cd
    .line 704
    move-object/from16 v0, p0

    #@4cf
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@4d1
    move-object/from16 v17, v0

    #@4d3
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/telephony/DataConnectionTracker;->disable_ehrpd_internet_ipv6()V

    #@4d6
    .line 706
    const-string v17, "ipv6_status_enable"

    #@4d8
    const/16 v18, 0x0

    #@4da
    move-object/from16 v0, v17

    #@4dc
    move/from16 v1, v18

    #@4de
    invoke-virtual {v11, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@4e1
    .line 707
    move-object/from16 v0, p0

    #@4e3
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@4e5
    move-object/from16 v17, v0

    #@4e7
    move-object/from16 v0, v17

    #@4e9
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@4eb
    move-object/from16 v17, v0

    #@4ed
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@4f0
    move-result-object v17

    #@4f1
    move-object/from16 v0, v17

    #@4f3
    invoke-virtual {v0, v11}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@4f6
    goto/16 :goto_e8

    #@4f8
    .line 711
    .end local v9           #isEnable:Z
    :cond_4f8
    const-string v17, "ipv6_status_result"

    #@4fa
    const/16 v18, 0x0

    #@4fc
    move-object/from16 v0, v17

    #@4fe
    move/from16 v1, v18

    #@500
    invoke-virtual {v11, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@503
    .line 712
    move-object/from16 v0, p0

    #@505
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@507
    move-object/from16 v17, v0

    #@509
    move-object/from16 v0, v17

    #@50b
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@50d
    move-object/from16 v17, v0

    #@50f
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@512
    move-result-object v17

    #@513
    move-object/from16 v0, v17

    #@515
    invoke-virtual {v0, v11}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@518
    .line 714
    move-object/from16 v0, p0

    #@51a
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@51c
    move-object/from16 v17, v0

    #@51e
    const-string v18, "[EHRPD_IPV6] mAllApns is Null"

    #@520
    invoke-virtual/range {v17 .. v18}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@523
    goto/16 :goto_e8

    #@525
    .line 720
    .end local v11           #mIntent:Landroid/content/Intent;
    :cond_525
    const-string v17, "lge.intent.action.LTE_NETWORK_SUPPORTED_INFO"

    #@527
    move-object/from16 v0, v17

    #@529
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@52c
    move-result v17

    #@52d
    if-eqz v17, :cond_5ab

    #@52f
    .line 721
    move-object/from16 v0, p0

    #@531
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@533
    move-object/from16 v17, v0

    #@535
    const-string v18, "VoPS_Support"

    #@537
    const/16 v19, 0x0

    #@539
    move-object/from16 v0, p2

    #@53b
    move-object/from16 v1, v18

    #@53d
    move/from16 v2, v19

    #@53f
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@542
    move-result v18

    #@543
    invoke-static/range {v18 .. v18}, Lcom/android/internal/telephony/PhoneConstants$VolteAndEPDNSupport;->fromInt(I)Lcom/android/internal/telephony/PhoneConstants$VolteAndEPDNSupport;

    #@546
    move-result-object v18

    #@547
    move-object/from16 v0, v18

    #@549
    move-object/from16 v1, v17

    #@54b
    iput-object v0, v1, Lcom/android/internal/telephony/DataConnectionTracker;->mVolteSupport:Lcom/android/internal/telephony/PhoneConstants$VolteAndEPDNSupport;

    #@54d
    .line 722
    move-object/from16 v0, p0

    #@54f
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@551
    move-object/from16 v17, v0

    #@553
    const-string v18, "EPDN_Support"

    #@555
    const/16 v19, 0x0

    #@557
    move-object/from16 v0, p2

    #@559
    move-object/from16 v1, v18

    #@55b
    move/from16 v2, v19

    #@55d
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@560
    move-result v18

    #@561
    invoke-static/range {v18 .. v18}, Lcom/android/internal/telephony/PhoneConstants$VolteAndEPDNSupport;->fromInt(I)Lcom/android/internal/telephony/PhoneConstants$VolteAndEPDNSupport;

    #@564
    move-result-object v18

    #@565
    move-object/from16 v0, v18

    #@567
    move-object/from16 v1, v17

    #@569
    iput-object v0, v1, Lcom/android/internal/telephony/DataConnectionTracker;->mEPDNSupport:Lcom/android/internal/telephony/PhoneConstants$VolteAndEPDNSupport;

    #@56b
    .line 724
    move-object/from16 v0, p0

    #@56d
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@56f
    move-object/from16 v17, v0

    #@571
    new-instance v18, Ljava/lang/StringBuilder;

    #@573
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@576
    const-string v19, "[EPDN] ACTION_VOLTE_EPS_NETWORK_SUPPORT: mVolteSupport="

    #@578
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57b
    move-result-object v18

    #@57c
    move-object/from16 v0, p0

    #@57e
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@580
    move-object/from16 v19, v0

    #@582
    move-object/from16 v0, v19

    #@584
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mVolteSupport:Lcom/android/internal/telephony/PhoneConstants$VolteAndEPDNSupport;

    #@586
    move-object/from16 v19, v0

    #@588
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@58b
    move-result-object v18

    #@58c
    const-string v19, ", mEPDNSupport="

    #@58e
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@591
    move-result-object v18

    #@592
    move-object/from16 v0, p0

    #@594
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@596
    move-object/from16 v19, v0

    #@598
    move-object/from16 v0, v19

    #@59a
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mEPDNSupport:Lcom/android/internal/telephony/PhoneConstants$VolteAndEPDNSupport;

    #@59c
    move-object/from16 v19, v0

    #@59e
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5a1
    move-result-object v18

    #@5a2
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a5
    move-result-object v18

    #@5a6
    invoke-virtual/range {v17 .. v18}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@5a9
    goto/16 :goto_e8

    #@5ab
    .line 726
    :cond_5ab
    const-string v17, "lge.intent.action.LTE_NETWORK_SIB_INFO"

    #@5ad
    move-object/from16 v0, v17

    #@5af
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5b2
    move-result v17

    #@5b3
    if-eqz v17, :cond_699

    #@5b5
    .line 727
    move-object/from16 v0, p0

    #@5b7
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@5b9
    move-object/from16 v17, v0

    #@5bb
    const-string v18, "Emer_Attach_Support"

    #@5bd
    const/16 v19, 0x0

    #@5bf
    move-object/from16 v0, p2

    #@5c1
    move-object/from16 v1, v18

    #@5c3
    move/from16 v2, v19

    #@5c5
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@5c8
    move-result v18

    #@5c9
    invoke-static/range {v18 .. v18}, Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;->fromInt(I)Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

    #@5cc
    move-result-object v18

    #@5cd
    move-object/from16 v0, v18

    #@5cf
    move-object/from16 v1, v17

    #@5d1
    iput-object v0, v1, Lcom/android/internal/telephony/DataConnectionTracker;->mEmerAttachSupport:Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

    #@5d3
    .line 728
    move-object/from16 v0, p0

    #@5d5
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@5d7
    move-object/from16 v17, v0

    #@5d9
    const-string v18, "EPDN_Barring"

    #@5db
    const/16 v19, 0x0

    #@5dd
    move-object/from16 v0, p2

    #@5df
    move-object/from16 v1, v18

    #@5e1
    move/from16 v2, v19

    #@5e3
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@5e6
    move-result v18

    #@5e7
    invoke-static/range {v18 .. v18}, Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;->fromInt(I)Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

    #@5ea
    move-result-object v18

    #@5eb
    move-object/from16 v0, v18

    #@5ed
    move-object/from16 v1, v17

    #@5ef
    iput-object v0, v1, Lcom/android/internal/telephony/DataConnectionTracker;->mEPDNBarring:Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

    #@5f1
    .line 730
    move-object/from16 v0, p0

    #@5f3
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@5f5
    move-object/from16 v17, v0

    #@5f7
    const-string v18, "Emer_Camped_CID"

    #@5f9
    const/16 v19, 0x0

    #@5fb
    move-object/from16 v0, p2

    #@5fd
    move-object/from16 v1, v18

    #@5ff
    move/from16 v2, v19

    #@601
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@604
    move-result v18

    #@605
    invoke-static/range {v18 .. v18}, Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;->fromInt(I)Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

    #@608
    move-result-object v18

    #@609
    move-object/from16 v0, v18

    #@60b
    move-object/from16 v1, v17

    #@60d
    iput-object v0, v1, Lcom/android/internal/telephony/DataConnectionTracker;->mEmerCampedCID:Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

    #@60f
    .line 731
    move-object/from16 v0, p0

    #@611
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@613
    move-object/from16 v17, v0

    #@615
    const-string v18, "Emer_Camped_TAC"

    #@617
    const/16 v19, 0x0

    #@619
    move-object/from16 v0, p2

    #@61b
    move-object/from16 v1, v18

    #@61d
    move/from16 v2, v19

    #@61f
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@622
    move-result v18

    #@623
    invoke-static/range {v18 .. v18}, Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;->fromInt(I)Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

    #@626
    move-result-object v18

    #@627
    move-object/from16 v0, v18

    #@629
    move-object/from16 v1, v17

    #@62b
    iput-object v0, v1, Lcom/android/internal/telephony/DataConnectionTracker;->mEmerCampedTAC:Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

    #@62d
    .line 733
    move-object/from16 v0, p0

    #@62f
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@631
    move-object/from16 v17, v0

    #@633
    new-instance v18, Ljava/lang/StringBuilder;

    #@635
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@638
    const-string v19, "[EPDN] ACTION_VOLTE_NETWORK_SIB_INFO: mEmerAttachSupport="

    #@63a
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63d
    move-result-object v18

    #@63e
    move-object/from16 v0, p0

    #@640
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@642
    move-object/from16 v19, v0

    #@644
    move-object/from16 v0, v19

    #@646
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mEmerAttachSupport:Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

    #@648
    move-object/from16 v19, v0

    #@64a
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@64d
    move-result-object v18

    #@64e
    const-string v19, ", mEPDNBarring="

    #@650
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@653
    move-result-object v18

    #@654
    move-object/from16 v0, p0

    #@656
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@658
    move-object/from16 v19, v0

    #@65a
    move-object/from16 v0, v19

    #@65c
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mEPDNBarring:Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

    #@65e
    move-object/from16 v19, v0

    #@660
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@663
    move-result-object v18

    #@664
    const-string v19, ", mEmerCampedCID"

    #@666
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@669
    move-result-object v18

    #@66a
    move-object/from16 v0, p0

    #@66c
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@66e
    move-object/from16 v19, v0

    #@670
    move-object/from16 v0, v19

    #@672
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mEmerCampedCID:Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

    #@674
    move-object/from16 v19, v0

    #@676
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@679
    move-result-object v18

    #@67a
    const-string v19, ", mEmerCampedTAC"

    #@67c
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67f
    move-result-object v18

    #@680
    move-object/from16 v0, p0

    #@682
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@684
    move-object/from16 v19, v0

    #@686
    move-object/from16 v0, v19

    #@688
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mEmerCampedTAC:Lcom/android/internal/telephony/PhoneConstants$SIBInfoForEPDN;

    #@68a
    move-object/from16 v19, v0

    #@68c
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@68f
    move-result-object v18

    #@690
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@693
    move-result-object v18

    #@694
    invoke-virtual/range {v17 .. v18}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@697
    goto/16 :goto_e8

    #@699
    .line 736
    :cond_699
    const-string v17, "lge.intent.action.DATA_EMERGENCY_FAILED"

    #@69b
    move-object/from16 v0, v17

    #@69d
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6a0
    move-result v17

    #@6a1
    if-eqz v17, :cond_6eb

    #@6a3
    .line 737
    move-object/from16 v0, p0

    #@6a5
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@6a7
    move-object/from16 v17, v0

    #@6a9
    const-string v18, "EMC_FailCause"

    #@6ab
    const/16 v19, 0x0

    #@6ad
    move-object/from16 v0, p2

    #@6af
    move-object/from16 v1, v18

    #@6b1
    move/from16 v2, v19

    #@6b3
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@6b6
    move-result v18

    #@6b7
    invoke-static/range {v18 .. v18}, Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;->fromInt(I)Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;

    #@6ba
    move-result-object v18

    #@6bb
    move-object/from16 v0, v18

    #@6bd
    move-object/from16 v1, v17

    #@6bf
    iput-object v0, v1, Lcom/android/internal/telephony/DataConnectionTracker;->mEmcFailCause:Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;

    #@6c1
    .line 739
    move-object/from16 v0, p0

    #@6c3
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@6c5
    move-object/from16 v17, v0

    #@6c7
    new-instance v18, Ljava/lang/StringBuilder;

    #@6c9
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@6cc
    const-string v19, "[EPDN] ACTION_VOLTE_EMERGENCY_CALL_FAIL_CAUSE: mEmcFailCause="

    #@6ce
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d1
    move-result-object v18

    #@6d2
    move-object/from16 v0, p0

    #@6d4
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@6d6
    move-object/from16 v19, v0

    #@6d8
    move-object/from16 v0, v19

    #@6da
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mEmcFailCause:Lcom/android/internal/telephony/PhoneConstants$EmcFailCause;

    #@6dc
    move-object/from16 v19, v0

    #@6de
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6e1
    move-result-object v18

    #@6e2
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6e5
    move-result-object v18

    #@6e6
    invoke-virtual/range {v17 .. v18}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@6e9
    goto/16 :goto_e8

    #@6eb
    .line 741
    :cond_6eb
    const-string v17, "lge.intent.action.LTE_STATE_INFO"

    #@6ed
    move-object/from16 v0, v17

    #@6ef
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6f2
    move-result v17

    #@6f3
    if-eqz v17, :cond_73d

    #@6f5
    .line 742
    move-object/from16 v0, p0

    #@6f7
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@6f9
    move-object/from16 v17, v0

    #@6fb
    const-string v18, "LteStateInfo"

    #@6fd
    const/16 v19, 0x0

    #@6ff
    move-object/from16 v0, p2

    #@701
    move-object/from16 v1, v18

    #@703
    move/from16 v2, v19

    #@705
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@708
    move-result v18

    #@709
    invoke-static/range {v18 .. v18}, Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;->fromInt(I)Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;

    #@70c
    move-result-object v18

    #@70d
    move-object/from16 v0, v18

    #@70f
    move-object/from16 v1, v17

    #@711
    iput-object v0, v1, Lcom/android/internal/telephony/DataConnectionTracker;->mLteStateInfo:Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;

    #@713
    .line 743
    move-object/from16 v0, p0

    #@715
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@717
    move-object/from16 v17, v0

    #@719
    new-instance v18, Ljava/lang/StringBuilder;

    #@71b
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@71e
    const-string v19, "[EPDN] ACTION_VOLTE_LTE_STATE_INFO: mLteStateInfo="

    #@720
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@723
    move-result-object v18

    #@724
    move-object/from16 v0, p0

    #@726
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@728
    move-object/from16 v19, v0

    #@72a
    move-object/from16 v0, v19

    #@72c
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mLteStateInfo:Lcom/android/internal/telephony/PhoneConstants$LteStateInfo;

    #@72e
    move-object/from16 v19, v0

    #@730
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@733
    move-result-object v18

    #@734
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@737
    move-result-object v18

    #@738
    invoke-virtual/range {v17 .. v18}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@73b
    goto/16 :goto_e8

    #@73d
    .line 748
    :cond_73d
    move-object/from16 v0, p0

    #@73f
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@741
    move-object/from16 v17, v0

    #@743
    move-object/from16 v0, v17

    #@745
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->ACTION_MOBILE_DATA_ROAMING_STATE_CHANGE_REQUEST:Ljava/lang/String;

    #@747
    move-object/from16 v17, v0

    #@749
    move-object/from16 v0, v17

    #@74b
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@74e
    move-result v17

    #@74f
    if-eqz v17, :cond_7aa

    #@751
    .line 750
    move-object/from16 v0, p0

    #@753
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@755
    move-object/from16 v17, v0

    #@757
    move-object/from16 v0, v17

    #@759
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->REQUEST_STATE:Ljava/lang/String;

    #@75b
    move-object/from16 v17, v0

    #@75d
    const/16 v18, 0x0

    #@75f
    move-object/from16 v0, p2

    #@761
    move-object/from16 v1, v17

    #@763
    move/from16 v2, v18

    #@765
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@768
    move-result v17

    #@769
    const/16 v18, 0x1

    #@76b
    move/from16 v0, v17

    #@76d
    move/from16 v1, v18

    #@76f
    if-ne v0, v1, :cond_79c

    #@771
    const/4 v10, 0x1

    #@772
    .line 752
    .local v10, isOK:Z
    :goto_772
    const/16 v17, 0x1

    #@774
    move/from16 v0, v17

    #@776
    if-ne v10, v0, :cond_79e

    #@778
    .line 753
    move-object/from16 v0, p0

    #@77a
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@77c
    move-object/from16 v17, v0

    #@77e
    const/16 v18, 0x1

    #@780
    invoke-virtual/range {v17 .. v18}, Lcom/android/internal/telephony/DataConnectionTracker;->setDataOnRoamingEnabled(Z)V

    #@783
    .line 758
    :goto_783
    move-object/from16 v0, p0

    #@785
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@787
    move-object/from16 v17, v0

    #@789
    const/16 v18, 0x1

    #@78b
    move/from16 v0, v18

    #@78d
    move-object/from16 v1, v17

    #@78f
    iput-boolean v0, v1, Lcom/android/internal/telephony/DataConnectionTracker;->roamingOnforResponse:Z

    #@791
    .line 759
    move-object/from16 v0, p0

    #@793
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@795
    move-object/from16 v17, v0

    #@797
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/telephony/DataConnectionTracker;->onRoamingOn()V

    #@79a
    goto/16 :goto_e8

    #@79c
    .line 750
    .end local v10           #isOK:Z
    :cond_79c
    const/4 v10, 0x0

    #@79d
    goto :goto_772

    #@79e
    .line 756
    .restart local v10       #isOK:Z
    :cond_79e
    move-object/from16 v0, p0

    #@7a0
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@7a2
    move-object/from16 v17, v0

    #@7a4
    const/16 v18, 0x0

    #@7a6
    invoke-virtual/range {v17 .. v18}, Lcom/android/internal/telephony/DataConnectionTracker;->setDataOnRoamingEnabled(Z)V

    #@7a9
    goto :goto_783

    #@7aa
    .line 761
    .end local v10           #isOK:Z
    :cond_7aa
    move-object/from16 v0, p0

    #@7ac
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@7ae
    move-object/from16 v17, v0

    #@7b0
    move-object/from16 v0, v17

    #@7b2
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->ACTION_ENABLE_DATA_IN_HPLMN_RESPONSE:Ljava/lang/String;

    #@7b4
    move-object/from16 v17, v0

    #@7b6
    move-object/from16 v0, v17

    #@7b8
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7bb
    move-result v17

    #@7bc
    if-eqz v17, :cond_7ce

    #@7be
    .line 762
    move-object/from16 v0, p0

    #@7c0
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@7c2
    move-object/from16 v17, v0

    #@7c4
    const/16 v18, 0x0

    #@7c6
    move/from16 v0, v18

    #@7c8
    move-object/from16 v1, v17

    #@7ca
    iput-boolean v0, v1, Lcom/android/internal/telephony/DataConnectionTracker;->isDataonPopupShown:Z

    #@7cc
    goto/16 :goto_e8

    #@7ce
    .line 767
    :cond_7ce
    move-object/from16 v0, p0

    #@7d0
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@7d2
    move-object/from16 v17, v0

    #@7d4
    move-object/from16 v0, v17

    #@7d6
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@7d8
    move-object/from16 v17, v0

    #@7da
    move-object/from16 v0, v17

    #@7dc
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@7de
    move-object/from16 v17, v0

    #@7e0
    invoke-interface/range {v17 .. v17}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@7e3
    sget-boolean v17, Lcom/android/internal/telephony/LGfeature;->omadmDataBlock:Z

    #@7e5
    const/16 v18, 0x1

    #@7e7
    move/from16 v0, v17

    #@7e9
    move/from16 v1, v18

    #@7eb
    if-ne v0, v1, :cond_e8

    #@7ed
    .line 768
    const-string v17, "android.intent.action.OMADM_DEVICE_LOCK_MSG"

    #@7ef
    move-object/from16 v0, v17

    #@7f1
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7f4
    move-result v17

    #@7f5
    if-eqz v17, :cond_853

    #@7f7
    .line 770
    move-object/from16 v0, p0

    #@7f9
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@7fb
    move-object/from16 v17, v0

    #@7fd
    new-instance v18, Ljava/lang/StringBuilder;

    #@7ff
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@802
    const-string v19, "[LG_DATA_SPRINT_OMADM_DATA_BLOCK] : before OMADM_DEVICE_LOCK_MSG, mIsOmaDmLock= "

    #@804
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@807
    move-result-object v18

    #@808
    move-object/from16 v0, p0

    #@80a
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@80c
    move-object/from16 v19, v0

    #@80e
    move-object/from16 v0, v19

    #@810
    iget v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mIsOmaDmLock:I

    #@812
    move/from16 v19, v0

    #@814
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@817
    move-result-object v18

    #@818
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@81b
    move-result-object v18

    #@81c
    invoke-virtual/range {v17 .. v18}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@81f
    .line 773
    move-object/from16 v0, p0

    #@821
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@823
    move-object/from16 v17, v0

    #@825
    const/16 v18, 0x1

    #@827
    move/from16 v0, v18

    #@829
    move-object/from16 v1, v17

    #@82b
    iput v0, v1, Lcom/android/internal/telephony/DataConnectionTracker;->mIsOmaDmLock:I

    #@82d
    .line 774
    move-object/from16 v0, p0

    #@82f
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@831
    move-object/from16 v17, v0

    #@833
    const/16 v18, 0x0

    #@835
    move/from16 v0, v18

    #@837
    move-object/from16 v1, v17

    #@839
    iput v0, v1, Lcom/android/internal/telephony/DataConnectionTracker;->mIsOmaDmSession:I

    #@83b
    .line 777
    move-object/from16 v0, p0

    #@83d
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@83f
    move-object/from16 v17, v0

    #@841
    const-string v18, "[LG_DATA_SPRINT_OMADM_DATA_BLOCK] :call setDataConnection(false);"

    #@843
    invoke-virtual/range {v17 .. v18}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@846
    .line 780
    move-object/from16 v0, p0

    #@848
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@84a
    move-object/from16 v17, v0

    #@84c
    const/16 v18, 0x0

    #@84e
    invoke-virtual/range {v17 .. v18}, Lcom/android/internal/telephony/DataConnectionTracker;->setDataConnection(Z)V

    #@851
    goto/16 :goto_e8

    #@853
    .line 781
    :cond_853
    const-string v17, "android.intent.action.DEVICE_UNLOCKED_MSG"

    #@855
    move-object/from16 v0, v17

    #@857
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@85a
    move-result v17

    #@85b
    if-eqz v17, :cond_8b9

    #@85d
    .line 783
    move-object/from16 v0, p0

    #@85f
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@861
    move-object/from16 v17, v0

    #@863
    move-object/from16 v0, v17

    #@865
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@867
    move-object/from16 v17, v0

    #@869
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@86c
    move-result-object v17

    #@86d
    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@870
    move-result-object v17

    #@871
    const-string v18, "lg_omadm_lwmo_lock_state"

    #@873
    const/16 v19, 0x0

    #@875
    invoke-static/range {v17 .. v19}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@878
    .line 784
    move-object/from16 v0, p0

    #@87a
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@87c
    move-object/from16 v17, v0

    #@87e
    move-object/from16 v0, v17

    #@880
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@882
    move-object/from16 v17, v0

    #@884
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@887
    move-result-object v17

    #@888
    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@88b
    move-result-object v17

    #@88c
    const-string v18, "lg_omadm_lwmo_lock_code"

    #@88e
    const-string v19, "0"

    #@890
    invoke-static/range {v17 .. v19}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    #@893
    .line 785
    move-object/from16 v0, p0

    #@895
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@897
    move-object/from16 v17, v0

    #@899
    const/16 v18, 0x0

    #@89b
    move/from16 v0, v18

    #@89d
    move-object/from16 v1, v17

    #@89f
    iput v0, v1, Lcom/android/internal/telephony/DataConnectionTracker;->mIsOmaDmLock:I

    #@8a1
    .line 787
    move-object/from16 v0, p0

    #@8a3
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@8a5
    move-object/from16 v17, v0

    #@8a7
    const-string v18, "[LG_DATA_SPRINT_OMADM_DATA_BLOCK] :call setDataConnection(true);"

    #@8a9
    invoke-virtual/range {v17 .. v18}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@8ac
    .line 788
    move-object/from16 v0, p0

    #@8ae
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@8b0
    move-object/from16 v17, v0

    #@8b2
    const/16 v18, 0x1

    #@8b4
    invoke-virtual/range {v17 .. v18}, Lcom/android/internal/telephony/DataConnectionTracker;->setDataConnection(Z)V

    #@8b7
    goto/16 :goto_e8

    #@8b9
    .line 791
    :cond_8b9
    const-string v17, "android.intent.action.REQUEST_START_OMADM_SESSION_MSG"

    #@8bb
    move-object/from16 v0, v17

    #@8bd
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8c0
    move-result v17

    #@8c1
    if-eqz v17, :cond_975

    #@8c3
    .line 792
    move-object/from16 v0, p0

    #@8c5
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@8c7
    move-object/from16 v17, v0

    #@8c9
    move-object/from16 v0, v17

    #@8cb
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@8cd
    move-object/from16 v17, v0

    #@8cf
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@8d2
    move-result-object v17

    #@8d3
    invoke-virtual/range {v17 .. v17}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@8d6
    move-result v16

    #@8d7
    .line 794
    .local v16, roaming:Z
    const/4 v7, 0x0

    #@8d8
    .line 796
    .local v7, dmcLockStatus:I
    :try_start_8d8
    move-object/from16 v0, p0

    #@8da
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@8dc
    move-object/from16 v17, v0

    #@8de
    move-object/from16 v0, v17

    #@8e0
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@8e2
    move-object/from16 v17, v0

    #@8e4
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@8e7
    move-result-object v17

    #@8e8
    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@8eb
    move-result-object v17

    #@8ec
    const-string v18, "lg_omadm_lwmo_lock_state"

    #@8ee
    const/16 v19, 0x0

    #@8f0
    invoke-static/range {v17 .. v19}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I
    :try_end_8f3
    .catch Ljava/lang/Exception; {:try_start_8d8 .. :try_end_8f3} :catch_c8f

    #@8f3
    move-result v7

    #@8f4
    .line 804
    :goto_8f4
    if-eqz v16, :cond_902

    #@8f6
    move-object/from16 v0, p0

    #@8f8
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@8fa
    move-object/from16 v17, v0

    #@8fc
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/telephony/DataConnectionTracker;->getDataOnRoamingEnabled()Z

    #@8ff
    move-result v17

    #@900
    if-eqz v17, :cond_908

    #@902
    :cond_902
    const/16 v17, 0x1

    #@904
    move/from16 v0, v17

    #@906
    if-ne v7, v0, :cond_94b

    #@908
    .line 805
    :cond_908
    move-object/from16 v0, p0

    #@90a
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@90c
    move-object/from16 v17, v0

    #@90e
    const/16 v18, 0x1

    #@910
    move/from16 v0, v18

    #@912
    move-object/from16 v1, v17

    #@914
    iput v0, v1, Lcom/android/internal/telephony/DataConnectionTracker;->mIsOmaDmSession:I

    #@916
    .line 806
    move-object/from16 v0, p0

    #@918
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@91a
    move-object/from16 v17, v0

    #@91c
    const/16 v18, 0x1

    #@91e
    invoke-virtual/range {v17 .. v18}, Lcom/android/internal/telephony/DataConnectionTracker;->setDataConnection(Z)V

    #@921
    .line 807
    move-object/from16 v0, p0

    #@923
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@925
    move-object/from16 v17, v0

    #@927
    new-instance v18, Ljava/lang/StringBuilder;

    #@929
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@92c
    const-string v19, "[LG_DATA_SPRINT_OMADM_START] : Receive Message for OMADM Start , roaming or lock status. mIsOmaDmSession : "

    #@92e
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@931
    move-result-object v18

    #@932
    move-object/from16 v0, p0

    #@934
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@936
    move-object/from16 v19, v0

    #@938
    move-object/from16 v0, v19

    #@93a
    iget v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mIsOmaDmSession:I

    #@93c
    move/from16 v19, v0

    #@93e
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@941
    move-result-object v18

    #@942
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@945
    move-result-object v18

    #@946
    invoke-virtual/range {v17 .. v18}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@949
    goto/16 :goto_e8

    #@94b
    .line 809
    :cond_94b
    move-object/from16 v0, p0

    #@94d
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@94f
    move-object/from16 v17, v0

    #@951
    new-instance v18, Ljava/lang/StringBuilder;

    #@953
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@956
    const-string v19, "[LG_DATA_SPRINT_OMADM_START] : Receive Message for OMADM Start , not roaming and not lock. mIsOmaDmSession :"

    #@958
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95b
    move-result-object v18

    #@95c
    move-object/from16 v0, p0

    #@95e
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@960
    move-object/from16 v19, v0

    #@962
    move-object/from16 v0, v19

    #@964
    iget v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mIsOmaDmSession:I

    #@966
    move/from16 v19, v0

    #@968
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@96b
    move-result-object v18

    #@96c
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@96f
    move-result-object v18

    #@970
    invoke-virtual/range {v17 .. v18}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@973
    goto/16 :goto_e8

    #@975
    .line 811
    .end local v7           #dmcLockStatus:I
    .end local v16           #roaming:Z
    :cond_975
    const-string v17, "android.intent.action.REQUEST_END_OMADM_SESSION_MSG"

    #@977
    move-object/from16 v0, v17

    #@979
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@97c
    move-result v17

    #@97d
    if-eqz v17, :cond_a31

    #@97f
    .line 812
    move-object/from16 v0, p0

    #@981
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@983
    move-object/from16 v17, v0

    #@985
    move-object/from16 v0, v17

    #@987
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@989
    move-object/from16 v17, v0

    #@98b
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@98e
    move-result-object v17

    #@98f
    invoke-virtual/range {v17 .. v17}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@992
    move-result v16

    #@993
    .line 814
    .restart local v16       #roaming:Z
    const/4 v7, 0x0

    #@994
    .line 817
    .restart local v7       #dmcLockStatus:I
    :try_start_994
    move-object/from16 v0, p0

    #@996
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@998
    move-object/from16 v17, v0

    #@99a
    move-object/from16 v0, v17

    #@99c
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@99e
    move-object/from16 v17, v0

    #@9a0
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@9a3
    move-result-object v17

    #@9a4
    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@9a7
    move-result-object v17

    #@9a8
    const-string v18, "lg_omadm_lwmo_lock_state"

    #@9aa
    const/16 v19, 0x0

    #@9ac
    invoke-static/range {v17 .. v19}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I
    :try_end_9af
    .catch Ljava/lang/Exception; {:try_start_994 .. :try_end_9af} :catch_c8c

    #@9af
    move-result v7

    #@9b0
    .line 824
    :goto_9b0
    move-object/from16 v0, p0

    #@9b2
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@9b4
    move-object/from16 v17, v0

    #@9b6
    const/16 v18, 0x0

    #@9b8
    move/from16 v0, v18

    #@9ba
    move-object/from16 v1, v17

    #@9bc
    iput v0, v1, Lcom/android/internal/telephony/DataConnectionTracker;->mIsOmaDmSession:I

    #@9be
    .line 825
    if-eqz v16, :cond_9cc

    #@9c0
    move-object/from16 v0, p0

    #@9c2
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@9c4
    move-object/from16 v17, v0

    #@9c6
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/telephony/DataConnectionTracker;->getDataOnRoamingEnabled()Z

    #@9c9
    move-result v17

    #@9ca
    if-eqz v17, :cond_9d2

    #@9cc
    :cond_9cc
    const/16 v17, 0x1

    #@9ce
    move/from16 v0, v17

    #@9d0
    if-ne v7, v0, :cond_a07

    #@9d2
    .line 826
    :cond_9d2
    move-object/from16 v0, p0

    #@9d4
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@9d6
    move-object/from16 v17, v0

    #@9d8
    const/16 v18, 0x0

    #@9da
    invoke-virtual/range {v17 .. v18}, Lcom/android/internal/telephony/DataConnectionTracker;->setDataConnection(Z)V

    #@9dd
    .line 827
    move-object/from16 v0, p0

    #@9df
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@9e1
    move-object/from16 v17, v0

    #@9e3
    new-instance v18, Ljava/lang/StringBuilder;

    #@9e5
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@9e8
    const-string v19, "[LG_DATA_SPRINT_OMADM_END] : Receive Message for OMADM Start , roaming or lock status. mIsOmaDmSession : "

    #@9ea
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9ed
    move-result-object v18

    #@9ee
    move-object/from16 v0, p0

    #@9f0
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@9f2
    move-object/from16 v19, v0

    #@9f4
    move-object/from16 v0, v19

    #@9f6
    iget v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mIsOmaDmSession:I

    #@9f8
    move/from16 v19, v0

    #@9fa
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@9fd
    move-result-object v18

    #@9fe
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a01
    move-result-object v18

    #@a02
    invoke-virtual/range {v17 .. v18}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@a05
    goto/16 :goto_e8

    #@a07
    .line 829
    :cond_a07
    move-object/from16 v0, p0

    #@a09
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@a0b
    move-object/from16 v17, v0

    #@a0d
    new-instance v18, Ljava/lang/StringBuilder;

    #@a0f
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@a12
    const-string v19, "[LG_DATA_SPRINT_OMADM_END] : Receive Message for OMADM Start , not roaming and not lock. mIsOmaDmSession : "

    #@a14
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a17
    move-result-object v18

    #@a18
    move-object/from16 v0, p0

    #@a1a
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@a1c
    move-object/from16 v19, v0

    #@a1e
    move-object/from16 v0, v19

    #@a20
    iget v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mIsOmaDmSession:I

    #@a22
    move/from16 v19, v0

    #@a24
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a27
    move-result-object v18

    #@a28
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a2b
    move-result-object v18

    #@a2c
    invoke-virtual/range {v17 .. v18}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@a2f
    goto/16 :goto_e8

    #@a31
    .line 831
    .end local v7           #dmcLockStatus:I
    .end local v16           #roaming:Z
    :cond_a31
    const-string v17, "android.intent.action.REQUEST_FOR_OMADM_DATA_SETUP"

    #@a33
    move-object/from16 v0, v17

    #@a35
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a38
    move-result v17

    #@a39
    if-eqz v17, :cond_a53

    #@a3b
    .line 832
    move-object/from16 v0, p0

    #@a3d
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@a3f
    move-object/from16 v17, v0

    #@a41
    const/16 v18, 0x0

    #@a43
    invoke-virtual/range {v17 .. v18}, Lcom/android/internal/telephony/DataConnectionTracker;->setDataConnection(Z)V

    #@a46
    .line 833
    move-object/from16 v0, p0

    #@a48
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@a4a
    move-object/from16 v17, v0

    #@a4c
    const/16 v18, 0x1

    #@a4e
    invoke-virtual/range {v17 .. v18}, Lcom/android/internal/telephony/DataConnectionTracker;->setDataConnection(Z)V

    #@a51
    goto/16 :goto_e8

    #@a53
    .line 834
    :cond_a53
    const-string v17, "android.intent.action.REQUEST_FOR_OMADM_DATA_DISCONNECT"

    #@a55
    move-object/from16 v0, v17

    #@a57
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a5a
    move-result v17

    #@a5b
    if-eqz v17, :cond_a6a

    #@a5d
    .line 835
    move-object/from16 v0, p0

    #@a5f
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@a61
    move-object/from16 v17, v0

    #@a63
    const/16 v18, 0x0

    #@a65
    invoke-virtual/range {v17 .. v18}, Lcom/android/internal/telephony/DataConnectionTracker;->setDataConnection(Z)V

    #@a68
    goto/16 :goto_e8

    #@a6a
    .line 836
    :cond_a6a
    const-string v17, "android.intent.action.REQUEST_FOR_OMADM_DATA_CONNECT"

    #@a6c
    move-object/from16 v0, v17

    #@a6e
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a71
    move-result v17

    #@a72
    if-eqz v17, :cond_a81

    #@a74
    .line 837
    move-object/from16 v0, p0

    #@a76
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@a78
    move-object/from16 v17, v0

    #@a7a
    const/16 v18, 0x1

    #@a7c
    invoke-virtual/range {v17 .. v18}, Lcom/android/internal/telephony/DataConnectionTracker;->setDataConnection(Z)V

    #@a7f
    goto/16 :goto_e8

    #@a81
    .line 840
    :cond_a81
    const-string v17, "com.kddi.android.cpa_CHANGED"

    #@a83
    move-object/from16 v0, v17

    #@a85
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a88
    move-result v17

    #@a89
    if-eqz v17, :cond_c53

    #@a8b
    .line 841
    move-object/from16 v0, p0

    #@a8d
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@a8f
    move-object/from16 v17, v0

    #@a91
    const-string v18, "cpa_enable"

    #@a93
    const/16 v19, 0x0

    #@a95
    move-object/from16 v0, p2

    #@a97
    move-object/from16 v1, v18

    #@a99
    move/from16 v2, v19

    #@a9b
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@a9e
    move-result v18

    #@a9f
    move/from16 v0, v18

    #@aa1
    move-object/from16 v1, v17

    #@aa3
    iput-boolean v0, v1, Lcom/android/internal/telephony/DataConnectionTracker;->cpa_enable:Z

    #@aa5
    .line 842
    move-object/from16 v0, p0

    #@aa7
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@aa9
    move-object/from16 v17, v0

    #@aab
    move-object/from16 v0, v17

    #@aad
    iget-boolean v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->cpa_enable:Z

    #@aaf
    move/from16 v17, v0

    #@ab1
    if-eqz v17, :cond_bf0

    #@ab3
    .line 844
    move-object/from16 v0, p0

    #@ab5
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@ab7
    move-object/from16 v17, v0

    #@ab9
    const/16 v18, 0x1

    #@abb
    move/from16 v0, v18

    #@abd
    move-object/from16 v1, v17

    #@abf
    iput-boolean v0, v1, Lcom/android/internal/telephony/DataConnectionTracker;->cpa_send_result:Z

    #@ac1
    .line 845
    move-object/from16 v0, p0

    #@ac3
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@ac5
    move-object/from16 v17, v0

    #@ac7
    const-string v18, "cpa_apn"

    #@ac9
    move-object/from16 v0, p2

    #@acb
    move-object/from16 v1, v18

    #@acd
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@ad0
    move-result-object v18

    #@ad1
    move-object/from16 v0, v18

    #@ad3
    move-object/from16 v1, v17

    #@ad5
    iput-object v0, v1, Lcom/android/internal/telephony/DataConnectionTracker;->cpa_apn:Ljava/lang/String;

    #@ad7
    .line 846
    move-object/from16 v0, p0

    #@ad9
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@adb
    move-object/from16 v17, v0

    #@add
    new-instance v18, Ljava/lang/StringBuilder;

    #@adf
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@ae2
    const-string v19, "LGCPA_"

    #@ae4
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ae7
    move-result-object v18

    #@ae8
    const-string v19, "cpa_user"

    #@aea
    move-object/from16 v0, p2

    #@aec
    move-object/from16 v1, v19

    #@aee
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@af1
    move-result-object v19

    #@af2
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@af5
    move-result-object v18

    #@af6
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@af9
    move-result-object v18

    #@afa
    move-object/from16 v0, v18

    #@afc
    move-object/from16 v1, v17

    #@afe
    iput-object v0, v1, Lcom/android/internal/telephony/DataConnectionTracker;->cpa_user:Ljava/lang/String;

    #@b00
    .line 847
    move-object/from16 v0, p0

    #@b02
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@b04
    move-object/from16 v17, v0

    #@b06
    new-instance v18, Ljava/lang/StringBuilder;

    #@b08
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@b0b
    const-string v19, "LGCPA_"

    #@b0d
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b10
    move-result-object v18

    #@b11
    const-string v19, "cpa_password"

    #@b13
    move-object/from16 v0, p2

    #@b15
    move-object/from16 v1, v19

    #@b17
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@b1a
    move-result-object v19

    #@b1b
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b1e
    move-result-object v18

    #@b1f
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b22
    move-result-object v18

    #@b23
    move-object/from16 v0, v18

    #@b25
    move-object/from16 v1, v17

    #@b27
    iput-object v0, v1, Lcom/android/internal/telephony/DataConnectionTracker;->cpa_password:Ljava/lang/String;

    #@b29
    .line 848
    move-object/from16 v0, p0

    #@b2b
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@b2d
    move-object/from16 v17, v0

    #@b2f
    const-string v18, "cpa_authType"

    #@b31
    const/16 v19, 0x0

    #@b33
    move-object/from16 v0, p2

    #@b35
    move-object/from16 v1, v18

    #@b37
    move/from16 v2, v19

    #@b39
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@b3c
    move-result v18

    #@b3d
    move/from16 v0, v18

    #@b3f
    move-object/from16 v1, v17

    #@b41
    iput v0, v1, Lcom/android/internal/telephony/DataConnectionTracker;->cpa_authType:I

    #@b43
    .line 850
    move-object/from16 v0, p0

    #@b45
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@b47
    move-object/from16 v17, v0

    #@b49
    const-string v18, "cpa_dns1"

    #@b4b
    move-object/from16 v0, p2

    #@b4d
    move-object/from16 v1, v18

    #@b4f
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@b52
    move-result-object v18

    #@b53
    move-object/from16 v0, v18

    #@b55
    move-object/from16 v1, v17

    #@b57
    iput-object v0, v1, Lcom/android/internal/telephony/DataConnectionTracker;->cpa_dns1:Ljava/lang/String;

    #@b59
    .line 851
    move-object/from16 v0, p0

    #@b5b
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@b5d
    move-object/from16 v17, v0

    #@b5f
    const-string v18, "cpa_dns2"

    #@b61
    move-object/from16 v0, p2

    #@b63
    move-object/from16 v1, v18

    #@b65
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@b68
    move-result-object v18

    #@b69
    move-object/from16 v0, v18

    #@b6b
    move-object/from16 v1, v17

    #@b6d
    iput-object v0, v1, Lcom/android/internal/telephony/DataConnectionTracker;->cpa_dns2:Ljava/lang/String;

    #@b6f
    .line 853
    const-string v17, "ril.btdun.dns1"

    #@b71
    move-object/from16 v0, p0

    #@b73
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@b75
    move-object/from16 v18, v0

    #@b77
    move-object/from16 v0, v18

    #@b79
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->cpa_dns1:Ljava/lang/String;

    #@b7b
    move-object/from16 v18, v0

    #@b7d
    invoke-static/range {v17 .. v18}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@b80
    .line 854
    const-string v17, "ril.btdun.dns2"

    #@b82
    move-object/from16 v0, p0

    #@b84
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@b86
    move-object/from16 v18, v0

    #@b88
    move-object/from16 v0, v18

    #@b8a
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->cpa_dns2:Ljava/lang/String;

    #@b8c
    move-object/from16 v18, v0

    #@b8e
    invoke-static/range {v17 .. v18}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@b91
    .line 856
    move-object/from16 v0, p0

    #@b93
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@b95
    move-object/from16 v17, v0

    #@b97
    new-instance v18, Ljava/lang/StringBuilder;

    #@b99
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@b9c
    const-string v19, "ril.btdun.dns1"

    #@b9e
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ba1
    move-result-object v18

    #@ba2
    move-object/from16 v0, p0

    #@ba4
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@ba6
    move-object/from16 v19, v0

    #@ba8
    move-object/from16 v0, v19

    #@baa
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->cpa_dns1:Ljava/lang/String;

    #@bac
    move-object/from16 v19, v0

    #@bae
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bb1
    move-result-object v18

    #@bb2
    const-string v19, " ril.btdun.dns2="

    #@bb4
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bb7
    move-result-object v18

    #@bb8
    move-object/from16 v0, p0

    #@bba
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@bbc
    move-object/from16 v19, v0

    #@bbe
    move-object/from16 v0, v19

    #@bc0
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->cpa_dns2:Ljava/lang/String;

    #@bc2
    move-object/from16 v19, v0

    #@bc4
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bc7
    move-result-object v18

    #@bc8
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bcb
    move-result-object v18

    #@bcc
    invoke-virtual/range {v17 .. v18}, Lcom/android/internal/telephony/DataConnectionTracker;->log(Ljava/lang/String;)V

    #@bcf
    .line 860
    move-object/from16 v0, p0

    #@bd1
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@bd3
    move-object/from16 v17, v0

    #@bd5
    const-string v18, "cpa_PackageName"

    #@bd7
    move-object/from16 v0, p2

    #@bd9
    move-object/from16 v1, v18

    #@bdb
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@bde
    move-result-object v18

    #@bdf
    move-object/from16 v0, v18

    #@be1
    move-object/from16 v1, v17

    #@be3
    iput-object v0, v1, Lcom/android/internal/telephony/DataConnectionTracker;->cpa_PackageName:Ljava/lang/String;

    #@be5
    .line 873
    :goto_be5
    move-object/from16 v0, p0

    #@be7
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@be9
    move-object/from16 v17, v0

    #@beb
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/telephony/DataConnectionTracker;->CPAChanged()V

    #@bee
    goto/16 :goto_e8

    #@bf0
    .line 864
    :cond_bf0
    move-object/from16 v0, p0

    #@bf2
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@bf4
    move-object/from16 v17, v0

    #@bf6
    const/16 v18, 0x0

    #@bf8
    move/from16 v0, v18

    #@bfa
    move-object/from16 v1, v17

    #@bfc
    iput-boolean v0, v1, Lcom/android/internal/telephony/DataConnectionTracker;->cpa_send_result:Z

    #@bfe
    .line 865
    const-string v17, "ril.btdun.dns1"

    #@c00
    const/16 v18, 0x0

    #@c02
    invoke-static/range {v17 .. v18}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@c05
    .line 866
    const-string v17, "ril.btdun.dns2"

    #@c07
    const/16 v18, 0x0

    #@c09
    invoke-static/range {v17 .. v18}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@c0c
    .line 867
    move-object/from16 v0, p0

    #@c0e
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@c10
    move-object/from16 v17, v0

    #@c12
    const-string v18, ""

    #@c14
    move-object/from16 v0, v18

    #@c16
    move-object/from16 v1, v17

    #@c18
    iput-object v0, v1, Lcom/android/internal/telephony/DataConnectionTracker;->cpa_apn:Ljava/lang/String;

    #@c1a
    .line 868
    move-object/from16 v0, p0

    #@c1c
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@c1e
    move-object/from16 v17, v0

    #@c20
    const-string v18, ""

    #@c22
    move-object/from16 v0, v18

    #@c24
    move-object/from16 v1, v17

    #@c26
    iput-object v0, v1, Lcom/android/internal/telephony/DataConnectionTracker;->cpa_user:Ljava/lang/String;

    #@c28
    .line 869
    move-object/from16 v0, p0

    #@c2a
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@c2c
    move-object/from16 v17, v0

    #@c2e
    const-string v18, ""

    #@c30
    move-object/from16 v0, v18

    #@c32
    move-object/from16 v1, v17

    #@c34
    iput-object v0, v1, Lcom/android/internal/telephony/DataConnectionTracker;->cpa_password:Ljava/lang/String;

    #@c36
    .line 870
    move-object/from16 v0, p0

    #@c38
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@c3a
    move-object/from16 v17, v0

    #@c3c
    const/16 v18, 0x0

    #@c3e
    move/from16 v0, v18

    #@c40
    move-object/from16 v1, v17

    #@c42
    iput v0, v1, Lcom/android/internal/telephony/DataConnectionTracker;->cpa_authType:I

    #@c44
    .line 871
    move-object/from16 v0, p0

    #@c46
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@c48
    move-object/from16 v17, v0

    #@c4a
    const-string v18, ""

    #@c4c
    move-object/from16 v0, v18

    #@c4e
    move-object/from16 v1, v17

    #@c50
    iput-object v0, v1, Lcom/android/internal/telephony/DataConnectionTracker;->cpa_PackageName:Ljava/lang/String;

    #@c52
    goto :goto_be5

    #@c53
    .line 875
    :cond_c53
    const-string v17, "cpa_onSetupConnectionCompleted"

    #@c55
    move-object/from16 v0, v17

    #@c57
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c5a
    move-result v17

    #@c5b
    if-eqz v17, :cond_e8

    #@c5d
    .line 876
    move-object/from16 v0, p0

    #@c5f
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@c61
    move-object/from16 v17, v0

    #@c63
    move-object/from16 v0, v17

    #@c65
    iget-boolean v0, v0, Lcom/android/internal/telephony/DataConnectionTracker;->cpa_enable:Z

    #@c67
    move/from16 v17, v0

    #@c69
    if-eqz v17, :cond_e8

    #@c6b
    .line 879
    move-object/from16 v0, p0

    #@c6d
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@c6f
    move-object/from16 v17, v0

    #@c71
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/telephony/DataConnectionTracker;->obtainMessage()Landroid/os/Message;

    #@c74
    move-result-object v13

    #@c75
    .line 880
    .local v13, msg:Landroid/os/Message;
    const/16 v17, 0x400

    #@c77
    move/from16 v0, v17

    #@c79
    iput v0, v13, Landroid/os/Message;->what:I

    #@c7b
    .line 881
    move-object/from16 v0, p0

    #@c7d
    iget-object v0, v0, Lcom/android/internal/telephony/DataConnectionTracker$2;->this$0:Lcom/android/internal/telephony/DataConnectionTracker;

    #@c7f
    move-object/from16 v17, v0

    #@c81
    const-wide/16 v18, 0xbb8

    #@c83
    move-object/from16 v0, v17

    #@c85
    move-wide/from16 v1, v18

    #@c87
    invoke-virtual {v0, v13, v1, v2}, Lcom/android/internal/telephony/DataConnectionTracker;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@c8a
    goto/16 :goto_e8

    #@c8c
    .line 818
    .end local v13           #msg:Landroid/os/Message;
    .restart local v7       #dmcLockStatus:I
    .restart local v16       #roaming:Z
    :catch_c8c
    move-exception v17

    #@c8d
    goto/16 :goto_9b0

    #@c8f
    .line 797
    :catch_c8f
    move-exception v17

    #@c90
    goto/16 :goto_8f4
.end method
