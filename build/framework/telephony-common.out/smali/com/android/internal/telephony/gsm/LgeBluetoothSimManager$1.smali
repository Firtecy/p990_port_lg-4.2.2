.class Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager$1;
.super Landroid/content/BroadcastReceiver;
.source "LgeBluetoothSimManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;


# direct methods
.method constructor <init>(Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 104
    iput-object p1, p0, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager$1;->this$0:Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 16
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/16 v12, 0xc8

    #@2
    const/4 v11, 0x1

    #@3
    const/4 v10, 0x0

    #@4
    const/4 v8, 0x0

    #@5
    const/16 v9, 0xc9

    #@7
    .line 107
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    .line 108
    .local v0, action:Ljava/lang/String;
    const-string v6, "com.lge.bluetooth.sap.ACTION_SAP_REQUEST"

    #@d
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10
    move-result v6

    #@11
    if-eqz v6, :cond_4b

    #@13
    .line 112
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager$1;->this$0:Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;

    #@15
    const-string v7, "req_type"

    #@17
    invoke-virtual {p2, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@1a
    move-result v7

    #@1b
    invoke-static {v6, v7}, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->access$002(Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;I)I

    #@1e
    .line 113
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager$1;->this$0:Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;

    #@20
    invoke-static {v6}, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->access$000(Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;)I

    #@23
    move-result v6

    #@24
    packed-switch v6, :pswitch_data_188

    #@27
    .line 192
    :pswitch_27
    const-string v6, "LgeBluetoothSimManager"

    #@29
    new-instance v7, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v8, "[BTUI] SAP_REQUEST : type ("

    #@30
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v7

    #@34
    iget-object v8, p0, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager$1;->this$0:Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;

    #@36
    invoke-static {v8}, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->access$000(Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;)I

    #@39
    move-result v8

    #@3a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v7

    #@3e
    const-string v8, ")"

    #@40
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v7

    #@44
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v7

    #@48
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4b
    .line 196
    :cond_4b
    :goto_4b
    return-void

    #@4c
    .line 115
    :pswitch_4c
    const-string v6, "LgeBluetoothSimManager"

    #@4e
    const-string v7, "[BTUI] SAP_ENABLE_EVT"

    #@50
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@53
    goto :goto_4b

    #@54
    .line 119
    :pswitch_54
    const-string v6, "LgeBluetoothSimManager"

    #@56
    const-string v7, "[BTUI] SAP_DISABLE_EVT"

    #@58
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5b
    goto :goto_4b

    #@5c
    .line 123
    :pswitch_5c
    const-string v6, "LgeBluetoothSimManager"

    #@5e
    const-string v7, "[BTUI] SIM_OPEN_EVT"

    #@60
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@63
    .line 124
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager$1;->this$0:Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;

    #@65
    invoke-static {v6}, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->access$100(Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;)Landroid/os/Handler;

    #@68
    move-result-object v6

    #@69
    invoke-virtual {v6, v12}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@6c
    move-result-object v2

    #@6d
    .line 125
    .local v2, msg:Landroid/os/Message;
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager$1;->this$0:Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;

    #@6f
    invoke-static {v6}, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->access$200(Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;)Lcom/android/internal/telephony/CommandsInterface;

    #@72
    move-result-object v6

    #@73
    if-eqz v6, :cond_4b

    #@75
    if-eqz v2, :cond_4b

    #@77
    .line 126
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager$1;->this$0:Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;

    #@79
    invoke-static {v6}, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->access$200(Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;)Lcom/android/internal/telephony/CommandsInterface;

    #@7c
    move-result-object v6

    #@7d
    invoke-interface {v6, v11, v2}, Lcom/android/internal/telephony/CommandsInterface;->SAPConncetionrequest(ILandroid/os/Message;)V

    #@80
    goto :goto_4b

    #@81
    .line 131
    .end local v2           #msg:Landroid/os/Message;
    :pswitch_81
    const-string v6, "LgeBluetoothSimManager"

    #@83
    const-string v7, "[BTUI] SIM_CLOSE_EVT"

    #@85
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@88
    .line 132
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager$1;->this$0:Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;

    #@8a
    invoke-static {v6}, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->access$100(Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;)Landroid/os/Handler;

    #@8d
    move-result-object v6

    #@8e
    invoke-virtual {v6, v12}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@91
    move-result-object v2

    #@92
    .line 133
    .restart local v2       #msg:Landroid/os/Message;
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager$1;->this$0:Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;

    #@94
    invoke-static {v6}, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->access$200(Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;)Lcom/android/internal/telephony/CommandsInterface;

    #@97
    move-result-object v6

    #@98
    if-eqz v6, :cond_4b

    #@9a
    if-eqz v2, :cond_4b

    #@9c
    .line 134
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager$1;->this$0:Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;

    #@9e
    invoke-static {v6}, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->access$200(Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;)Lcom/android/internal/telephony/CommandsInterface;

    #@a1
    move-result-object v6

    #@a2
    invoke-interface {v6, v8, v2}, Lcom/android/internal/telephony/CommandsInterface;->SAPConncetionrequest(ILandroid/os/Message;)V

    #@a5
    goto :goto_4b

    #@a6
    .line 139
    .end local v2           #msg:Landroid/os/Message;
    :pswitch_a6
    const-string v6, "LgeBluetoothSimManager"

    #@a8
    const-string v7, "[BTUI] SIM_RESET_EVT"

    #@aa
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ad
    .line 140
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager$1;->this$0:Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;

    #@af
    invoke-static {v6}, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->access$100(Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;)Landroid/os/Handler;

    #@b2
    move-result-object v6

    #@b3
    invoke-virtual {v6, v9}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@b6
    move-result-object v2

    #@b7
    .line 141
    .restart local v2       #msg:Landroid/os/Message;
    if-eqz v2, :cond_4b

    #@b9
    .line 142
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager$1;->this$0:Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;

    #@bb
    const/4 v7, 0x4

    #@bc
    invoke-static {v6, v10, v7, v2}, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->access$300(Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;[BILandroid/os/Message;)V

    #@bf
    goto :goto_4b

    #@c0
    .line 147
    .end local v2           #msg:Landroid/os/Message;
    :pswitch_c0
    const-string v6, "LgeBluetoothSimManager"

    #@c2
    const-string v7, "[BTUI] SIM_POWER_ON_EVT"

    #@c4
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c7
    .line 148
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager$1;->this$0:Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;

    #@c9
    invoke-static {v6}, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->access$100(Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;)Landroid/os/Handler;

    #@cc
    move-result-object v6

    #@cd
    invoke-virtual {v6, v9}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@d0
    move-result-object v2

    #@d1
    .line 149
    .restart local v2       #msg:Landroid/os/Message;
    if-eqz v2, :cond_4b

    #@d3
    .line 150
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager$1;->this$0:Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;

    #@d5
    const/4 v7, 0x3

    #@d6
    invoke-static {v6, v10, v7, v2}, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->access$300(Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;[BILandroid/os/Message;)V

    #@d9
    goto/16 :goto_4b

    #@db
    .line 155
    .end local v2           #msg:Landroid/os/Message;
    :pswitch_db
    const-string v6, "LgeBluetoothSimManager"

    #@dd
    const-string v7, "[BTUI] SIM_POWER_OFF_EVT"

    #@df
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e2
    .line 156
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager$1;->this$0:Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;

    #@e4
    invoke-static {v6}, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->access$100(Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;)Landroid/os/Handler;

    #@e7
    move-result-object v6

    #@e8
    invoke-virtual {v6, v9}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@eb
    move-result-object v2

    #@ec
    .line 157
    .restart local v2       #msg:Landroid/os/Message;
    if-eqz v2, :cond_4b

    #@ee
    .line 158
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager$1;->this$0:Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;

    #@f0
    const/4 v7, 0x2

    #@f1
    invoke-static {v6, v10, v7, v2}, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->access$300(Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;[BILandroid/os/Message;)V

    #@f4
    goto/16 :goto_4b

    #@f6
    .line 163
    .end local v2           #msg:Landroid/os/Message;
    :pswitch_f6
    const-string v6, "LgeBluetoothSimManager"

    #@f8
    const-string v7, "[BTUI] SIM_TRANSFER_ATR_EVT"

    #@fa
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@fd
    .line 164
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager$1;->this$0:Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;

    #@ff
    invoke-static {v6}, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->access$100(Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;)Landroid/os/Handler;

    #@102
    move-result-object v6

    #@103
    invoke-virtual {v6, v9}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@106
    move-result-object v2

    #@107
    .line 165
    .restart local v2       #msg:Landroid/os/Message;
    if-eqz v2, :cond_4b

    #@109
    .line 166
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager$1;->this$0:Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;

    #@10b
    invoke-static {v6, v10, v8, v2}, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->access$300(Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;[BILandroid/os/Message;)V

    #@10e
    goto/16 :goto_4b

    #@110
    .line 171
    .end local v2           #msg:Landroid/os/Message;
    :pswitch_110
    const-string v6, "LgeBluetoothSimManager"

    #@112
    const-string v7, "[BTUI] SIM_TRANSFER_APDU_EVT"

    #@114
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@117
    .line 172
    const-string v6, "is_apdu_7816"

    #@119
    invoke-virtual {p2, v6, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@11c
    move-result v1

    #@11d
    .line 173
    .local v1, is_apdu_7816:I
    const-string v6, "req_len"

    #@11f
    invoke-virtual {p2, v6, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@122
    move-result v4

    #@123
    .line 174
    .local v4, req_len:I
    const-string v6, "req_data"

    #@125
    invoke-virtual {p2, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@128
    move-result-object v5

    #@129
    .line 175
    .local v5, req_str:Ljava/lang/String;
    const-string v6, "req_data_byte"

    #@12b
    invoke-virtual {p2, v6}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    #@12e
    move-result-object v3

    #@12f
    check-cast v3, [B

    #@131
    .line 176
    .local v3, req_byte:[B
    const-string v6, "LgeBluetoothSimManager"

    #@133
    new-instance v7, Ljava/lang/StringBuilder;

    #@135
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@138
    const-string v8, "[BTUI] input str = "

    #@13a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13d
    move-result-object v7

    #@13e
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@141
    move-result-object v7

    #@142
    const-string v8, " ("

    #@144
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@147
    move-result-object v7

    #@148
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14b
    move-result-object v7

    #@14c
    const-string v8, ")"

    #@14e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@151
    move-result-object v7

    #@152
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@155
    move-result-object v7

    #@156
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@159
    .line 177
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager$1;->this$0:Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;

    #@15b
    invoke-static {v6}, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->access$100(Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;)Landroid/os/Handler;

    #@15e
    move-result-object v6

    #@15f
    invoke-virtual {v6, v9}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@162
    move-result-object v2

    #@163
    .line 178
    .restart local v2       #msg:Landroid/os/Message;
    if-eqz v2, :cond_4b

    #@165
    .line 179
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager$1;->this$0:Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;

    #@167
    invoke-static {v6, v3, v11, v2}, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->access$300(Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;[BILandroid/os/Message;)V

    #@16a
    goto/16 :goto_4b

    #@16c
    .line 184
    .end local v1           #is_apdu_7816:I
    .end local v2           #msg:Landroid/os/Message;
    .end local v3           #req_byte:[B
    .end local v4           #req_len:I
    .end local v5           #req_str:Ljava/lang/String;
    :pswitch_16c
    const-string v6, "LgeBluetoothSimManager"

    #@16e
    const-string v7, "[BTUI] SIM_CARD_READER_STATUS_EVT"

    #@170
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@173
    .line 185
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager$1;->this$0:Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;

    #@175
    invoke-static {v6}, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->access$100(Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;)Landroid/os/Handler;

    #@178
    move-result-object v6

    #@179
    invoke-virtual {v6, v9}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@17c
    move-result-object v2

    #@17d
    .line 186
    .restart local v2       #msg:Landroid/os/Message;
    if-eqz v2, :cond_4b

    #@17f
    .line 187
    iget-object v6, p0, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager$1;->this$0:Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;

    #@181
    const/4 v7, 0x5

    #@182
    invoke-static {v6, v10, v7, v2}, Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;->access$300(Lcom/android/internal/telephony/gsm/LgeBluetoothSimManager;[BILandroid/os/Message;)V

    #@185
    goto/16 :goto_4b

    #@187
    .line 113
    nop

    #@188
    :pswitch_data_188
    .packed-switch 0x0
        :pswitch_5c
        :pswitch_81
        :pswitch_a6
        :pswitch_c0
        :pswitch_db
        :pswitch_f6
        :pswitch_110
        :pswitch_16c
        :pswitch_27
        :pswitch_27
        :pswitch_4c
        :pswitch_54
    .end packed-switch
.end method
