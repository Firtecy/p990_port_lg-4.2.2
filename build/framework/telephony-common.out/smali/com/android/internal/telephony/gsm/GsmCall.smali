.class Lcom/android/internal/telephony/gsm/GsmCall;
.super Lcom/android/internal/telephony/Call;
.source "GsmCall.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/gsm/GsmCall$1;
    }
.end annotation


# static fields
.field private static mIsUsingIPPhone:Z


# instance fields
.field connections:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/telephony/Connection;",
            ">;"
        }
    .end annotation
.end field

.field private mIsFake:Z

.field owner:Lcom/android/internal/telephony/gsm/GsmCallTracker;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 280
    const/4 v0, 0x0

    #@1
    sput-boolean v0, Lcom/android/internal/telephony/gsm/GsmCall;->mIsUsingIPPhone:Z

    #@3
    .line 283
    invoke-static {}, Lcom/android/internal/telephony/TelephonyUtils;->isIPPhoneSupported()Z

    #@6
    move-result v0

    #@7
    sput-boolean v0, Lcom/android/internal/telephony/gsm/GsmCall;->mIsUsingIPPhone:Z

    #@9
    .line 284
    return-void
.end method

.method constructor <init>(Lcom/android/internal/telephony/gsm/GsmCallTracker;)V
    .registers 3
    .parameter "owner"

    #@0
    .prologue
    .line 56
    invoke-direct {p0}, Lcom/android/internal/telephony/Call;-><init>()V

    #@3
    .line 34
    new-instance v0, Ljava/util/ArrayList;

    #@5
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCall;->connections:Ljava/util/ArrayList;

    #@a
    .line 285
    const/4 v0, 0x0

    #@b
    iput-boolean v0, p0, Lcom/android/internal/telephony/gsm/GsmCall;->mIsFake:Z

    #@d
    .line 57
    iput-object p1, p0, Lcom/android/internal/telephony/gsm/GsmCall;->owner:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@f
    .line 58
    return-void
.end method

.method static stateFromDCState(Lcom/android/internal/telephony/DriverCall$State;)Lcom/android/internal/telephony/Call$State;
    .registers 4
    .parameter "dcState"

    #@0
    .prologue
    .line 42
    sget-object v0, Lcom/android/internal/telephony/gsm/GsmCall$1;->$SwitchMap$com$android$internal$telephony$DriverCall$State:[I

    #@2
    invoke-virtual {p0}, Lcom/android/internal/telephony/DriverCall$State;->ordinal()I

    #@5
    move-result v1

    #@6
    aget v0, v0, v1

    #@8
    packed-switch v0, :pswitch_data_36

    #@b
    .line 49
    new-instance v0, Ljava/lang/RuntimeException;

    #@d
    new-instance v1, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v2, "illegal call state:"

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@23
    throw v0

    #@24
    .line 43
    :pswitch_24
    sget-object v0, Lcom/android/internal/telephony/Call$State;->ACTIVE:Lcom/android/internal/telephony/Call$State;

    #@26
    .line 48
    :goto_26
    return-object v0

    #@27
    .line 44
    :pswitch_27
    sget-object v0, Lcom/android/internal/telephony/Call$State;->HOLDING:Lcom/android/internal/telephony/Call$State;

    #@29
    goto :goto_26

    #@2a
    .line 45
    :pswitch_2a
    sget-object v0, Lcom/android/internal/telephony/Call$State;->DIALING:Lcom/android/internal/telephony/Call$State;

    #@2c
    goto :goto_26

    #@2d
    .line 46
    :pswitch_2d
    sget-object v0, Lcom/android/internal/telephony/Call$State;->ALERTING:Lcom/android/internal/telephony/Call$State;

    #@2f
    goto :goto_26

    #@30
    .line 47
    :pswitch_30
    sget-object v0, Lcom/android/internal/telephony/Call$State;->INCOMING:Lcom/android/internal/telephony/Call$State;

    #@32
    goto :goto_26

    #@33
    .line 48
    :pswitch_33
    sget-object v0, Lcom/android/internal/telephony/Call$State;->WAITING:Lcom/android/internal/telephony/Call$State;

    #@35
    goto :goto_26

    #@36
    .line 42
    :pswitch_data_36
    .packed-switch 0x1
        :pswitch_24
        :pswitch_27
        :pswitch_2a
        :pswitch_2d
        :pswitch_30
        :pswitch_33
    .end packed-switch
.end method


# virtual methods
.method attach(Lcom/android/internal/telephony/Connection;Lcom/android/internal/telephony/DriverCall;)V
    .registers 4
    .parameter "conn"
    .parameter "dc"

    #@0
    .prologue
    .line 148
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCall;->connections:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@5
    .line 150
    iget-object v0, p2, Lcom/android/internal/telephony/DriverCall;->state:Lcom/android/internal/telephony/DriverCall$State;

    #@7
    invoke-static {v0}, Lcom/android/internal/telephony/gsm/GsmCall;->stateFromDCState(Lcom/android/internal/telephony/DriverCall$State;)Lcom/android/internal/telephony/Call$State;

    #@a
    move-result-object v0

    #@b
    iput-object v0, p0, Lcom/android/internal/telephony/Call;->state:Lcom/android/internal/telephony/Call$State;

    #@d
    .line 151
    return-void
.end method

.method attachFake(Lcom/android/internal/telephony/Connection;Lcom/android/internal/telephony/Call$State;)V
    .registers 4
    .parameter "conn"
    .parameter "state"

    #@0
    .prologue
    .line 155
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCall;->connections:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@5
    .line 157
    iput-object p2, p0, Lcom/android/internal/telephony/Call;->state:Lcom/android/internal/telephony/Call$State;

    #@7
    .line 158
    return-void
.end method

.method clearDisconnected()V
    .registers 5

    #@0
    .prologue
    .line 254
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmCall;->connections:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v2

    #@6
    add-int/lit8 v1, v2, -0x1

    #@8
    .local v1, i:I
    :goto_8
    if-ltz v1, :cond_22

    #@a
    .line 255
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmCall;->connections:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Lcom/android/internal/telephony/gsm/GsmConnection;

    #@12
    .line 257
    .local v0, cn:Lcom/android/internal/telephony/gsm/GsmConnection;
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmConnection;->getState()Lcom/android/internal/telephony/Call$State;

    #@15
    move-result-object v2

    #@16
    sget-object v3, Lcom/android/internal/telephony/Call$State;->DISCONNECTED:Lcom/android/internal/telephony/Call$State;

    #@18
    if-ne v2, v3, :cond_1f

    #@1a
    .line 258
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmCall;->connections:Ljava/util/ArrayList;

    #@1c
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@1f
    .line 254
    :cond_1f
    add-int/lit8 v1, v1, -0x1

    #@21
    goto :goto_8

    #@22
    .line 262
    .end local v0           #cn:Lcom/android/internal/telephony/gsm/GsmConnection;
    :cond_22
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GsmCall;->connections:Ljava/util/ArrayList;

    #@24
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@27
    move-result v2

    #@28
    if-nez v2, :cond_2e

    #@2a
    .line 263
    sget-object v2, Lcom/android/internal/telephony/Call$State;->IDLE:Lcom/android/internal/telephony/Call$State;

    #@2c
    iput-object v2, p0, Lcom/android/internal/telephony/Call;->state:Lcom/android/internal/telephony/Call$State;

    #@2e
    .line 265
    :cond_2e
    return-void
.end method

.method connectionDisconnected(Lcom/android/internal/telephony/gsm/GsmConnection;)V
    .registers 7
    .parameter "conn"

    #@0
    .prologue
    .line 166
    sget-boolean v3, Lcom/android/internal/telephony/gsm/GsmCall;->mIsUsingIPPhone:Z

    #@2
    if-eqz v3, :cond_1f

    #@4
    .line 167
    iget-object v3, p0, Lcom/android/internal/telephony/Call;->state:Lcom/android/internal/telephony/Call$State;

    #@6
    sget-object v4, Lcom/android/internal/telephony/Call$State;->DIALING:Lcom/android/internal/telephony/Call$State;

    #@8
    if-ne v3, v4, :cond_1f

    #@a
    invoke-static {}, Lcom/android/internal/telephony/CallManager;->isNormalPowerOnReq()Z

    #@d
    move-result v3

    #@e
    if-nez v3, :cond_1f

    #@10
    invoke-virtual {p1}, Lcom/android/internal/telephony/gsm/GsmConnection;->getDisconnectCause()Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@13
    move-result-object v3

    #@14
    sget-object v4, Lcom/android/internal/telephony/Connection$DisconnectCause;->LOCAL:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@16
    if-eq v3, v4, :cond_1f

    #@18
    .line 170
    invoke-static {}, Lcom/android/internal/telephony/CallManager;->setKeepingFakeCall()V

    #@1b
    .line 171
    const/4 v3, 0x1

    #@1c
    invoke-static {v3}, Lcom/android/internal/telephony/CallManager;->setSkipRegistration(Z)V

    #@1f
    .line 175
    :cond_1f
    iget-object v3, p0, Lcom/android/internal/telephony/Call;->state:Lcom/android/internal/telephony/Call$State;

    #@21
    sget-object v4, Lcom/android/internal/telephony/Call$State;->DISCONNECTED:Lcom/android/internal/telephony/Call$State;

    #@23
    if-eq v3, v4, :cond_46

    #@25
    .line 178
    const/4 v0, 0x1

    #@26
    .line 180
    .local v0, hasOnlyDisconnectedConnections:Z
    const/4 v1, 0x0

    #@27
    .local v1, i:I
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GsmCall;->connections:Ljava/util/ArrayList;

    #@29
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@2c
    move-result v2

    #@2d
    .local v2, s:I
    :goto_2d
    if-ge v1, v2, :cond_40

    #@2f
    .line 181
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GsmCall;->connections:Ljava/util/ArrayList;

    #@31
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@34
    move-result-object v3

    #@35
    check-cast v3, Lcom/android/internal/telephony/Connection;

    #@37
    invoke-virtual {v3}, Lcom/android/internal/telephony/Connection;->getState()Lcom/android/internal/telephony/Call$State;

    #@3a
    move-result-object v3

    #@3b
    sget-object v4, Lcom/android/internal/telephony/Call$State;->DISCONNECTED:Lcom/android/internal/telephony/Call$State;

    #@3d
    if-eq v3, v4, :cond_47

    #@3f
    .line 184
    const/4 v0, 0x0

    #@40
    .line 189
    :cond_40
    if-eqz v0, :cond_46

    #@42
    .line 190
    sget-object v3, Lcom/android/internal/telephony/Call$State;->DISCONNECTED:Lcom/android/internal/telephony/Call$State;

    #@44
    iput-object v3, p0, Lcom/android/internal/telephony/Call;->state:Lcom/android/internal/telephony/Call$State;

    #@46
    .line 193
    .end local v0           #hasOnlyDisconnectedConnections:Z
    .end local v1           #i:I
    .end local v2           #s:I
    :cond_46
    return-void

    #@47
    .line 180
    .restart local v0       #hasOnlyDisconnectedConnections:Z
    .restart local v1       #i:I
    .restart local v2       #s:I
    :cond_47
    add-int/lit8 v1, v1, 0x1

    #@49
    goto :goto_2d
.end method

.method detach(Lcom/android/internal/telephony/gsm/GsmConnection;)V
    .registers 3
    .parameter "conn"

    #@0
    .prologue
    .line 198
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCall;->connections:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@5
    .line 200
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCall;->connections:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@a
    move-result v0

    #@b
    if-nez v0, :cond_11

    #@d
    .line 201
    sget-object v0, Lcom/android/internal/telephony/Call$State;->IDLE:Lcom/android/internal/telephony/Call$State;

    #@f
    iput-object v0, p0, Lcom/android/internal/telephony/Call;->state:Lcom/android/internal/telephony/Call$State;

    #@11
    .line 203
    :cond_11
    return-void
.end method

.method public dispose()V
    .registers 1

    #@0
    .prologue
    .line 61
    return-void
.end method

.method public getConnections()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/Connection;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 68
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCall;->connections:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method public getPhone()Lcom/android/internal/telephony/Phone;
    .registers 2

    #@0
    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCall;->owner:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@2
    iget-object v0, v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->phone:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@4
    return-object v0
.end method

.method public hangup()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 88
    const/4 v0, 0x0

    #@1
    sput-boolean v0, Lcom/android/internal/telephony/CallManager;->mIgnoreOutOfServiceArea:Z

    #@3
    .line 89
    sget-boolean v0, Lcom/android/internal/telephony/gsm/GsmCall;->mIsUsingIPPhone:Z

    #@5
    if-eqz v0, :cond_f

    #@7
    .line 90
    iget-boolean v0, p0, Lcom/android/internal/telephony/gsm/GsmCall;->mIsFake:Z

    #@9
    if-eqz v0, :cond_f

    #@b
    .line 91
    invoke-static {}, Lcom/android/internal/telephony/CallManager;->hangupFakeCall()V

    #@e
    .line 97
    :goto_e
    return-void

    #@f
    .line 96
    :cond_f
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCall;->owner:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@11
    invoke-virtual {v0, p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->hangup(Lcom/android/internal/telephony/gsm/GsmCall;)V

    #@14
    goto :goto_e
.end method

.method public hangup(Z)V
    .registers 3
    .parameter "answerWaiting"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 135
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCall;->owner:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@2
    invoke-virtual {v0, p0, p1}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->hangup(Lcom/android/internal/telephony/gsm/GsmCall;Z)V

    #@5
    .line 136
    return-void
.end method

.method public hangupAllCalls()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 123
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCall;->owner:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->hangupAllCalls()V

    #@5
    .line 124
    return-void
.end method

.method public hangupForVoIP()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 110
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCall;->owner:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@2
    invoke-virtual {v0, p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->hangupForVoIP(Lcom/android/internal/telephony/gsm/GsmCall;)V

    #@5
    .line 111
    return-void
.end method

.method public hangupNotResume()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 116
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCall;->owner:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@2
    invoke-virtual {v0, p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->hangupNotResume(Lcom/android/internal/telephony/gsm/GsmCall;)V

    #@5
    .line 117
    return-void
.end method

.method public hangup_FakeCall()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 101
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCall;->owner:Lcom/android/internal/telephony/gsm/GsmCallTracker;

    #@2
    invoke-virtual {v0, p0}, Lcom/android/internal/telephony/gsm/GsmCallTracker;->hangup(Lcom/android/internal/telephony/gsm/GsmCall;)V

    #@5
    .line 102
    return-void
.end method

.method isFull()Z
    .registers 3

    #@0
    .prologue
    .line 226
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/GsmCall;->connections:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v0

    #@6
    const/4 v1, 0x5

    #@7
    if-ne v0, v1, :cond_b

    #@9
    const/4 v0, 0x1

    #@a
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public isMultiparty()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 78
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/GsmCall;->connections:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v1

    #@7
    if-le v1, v0, :cond_a

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method onHangupLocal()V
    .registers 5

    #@0
    .prologue
    .line 239
    const/4 v1, 0x0

    #@1
    .local v1, i:I
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GsmCall;->connections:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v2

    #@7
    .line 240
    .local v2, s:I
    :goto_7
    if-ge v1, v2, :cond_17

    #@9
    .line 242
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GsmCall;->connections:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v0

    #@f
    check-cast v0, Lcom/android/internal/telephony/gsm/GsmConnection;

    #@11
    .line 244
    .local v0, cn:Lcom/android/internal/telephony/gsm/GsmConnection;
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmConnection;->onHangupLocal()V

    #@14
    .line 240
    add-int/lit8 v1, v1, 0x1

    #@16
    goto :goto_7

    #@17
    .line 246
    .end local v0           #cn:Lcom/android/internal/telephony/gsm/GsmConnection;
    :cond_17
    sget-object v3, Lcom/android/internal/telephony/Call$State;->DISCONNECTING:Lcom/android/internal/telephony/Call$State;

    #@19
    iput-object v3, p0, Lcom/android/internal/telephony/Call;->state:Lcom/android/internal/telephony/Call$State;

    #@1b
    .line 247
    return-void
.end method

.method onHangupLocalMissed()V
    .registers 5

    #@0
    .prologue
    .line 270
    const/4 v1, 0x0

    #@1
    .local v1, i:I
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GsmCall;->connections:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v2

    #@7
    .line 271
    .local v2, s:I
    :goto_7
    if-ge v1, v2, :cond_17

    #@9
    .line 273
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GsmCall;->connections:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v0

    #@f
    check-cast v0, Lcom/android/internal/telephony/gsm/GsmConnection;

    #@11
    .line 275
    .local v0, cn:Lcom/android/internal/telephony/gsm/GsmConnection;
    invoke-virtual {v0}, Lcom/android/internal/telephony/gsm/GsmConnection;->onHangupLocalMissed()V

    #@14
    .line 271
    add-int/lit8 v1, v1, 0x1

    #@16
    goto :goto_7

    #@17
    .line 277
    .end local v0           #cn:Lcom/android/internal/telephony/gsm/GsmConnection;
    :cond_17
    return-void
.end method

.method public setAsFake(Z)V
    .registers 3
    .parameter "fake"

    #@0
    .prologue
    .line 287
    iput-boolean p1, p0, Lcom/android/internal/telephony/gsm/GsmCall;->mIsFake:Z

    #@2
    .line 288
    sget-object v0, Lcom/android/internal/telephony/Call$State;->DIALING:Lcom/android/internal/telephony/Call$State;

    #@4
    iput-object v0, p0, Lcom/android/internal/telephony/Call;->state:Lcom/android/internal/telephony/Call$State;

    #@6
    .line 289
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 141
    iget-object v0, p0, Lcom/android/internal/telephony/Call;->state:Lcom/android/internal/telephony/Call$State;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/Call$State;->toString()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method update(Lcom/android/internal/telephony/gsm/GsmConnection;Lcom/android/internal/telephony/DriverCall;)Z
    .registers 6
    .parameter "conn"
    .parameter "dc"

    #@0
    .prologue
    .line 208
    const/4 v0, 0x0

    #@1
    .line 210
    .local v0, changed:Z
    iget-object v2, p2, Lcom/android/internal/telephony/DriverCall;->state:Lcom/android/internal/telephony/DriverCall$State;

    #@3
    invoke-static {v2}, Lcom/android/internal/telephony/gsm/GsmCall;->stateFromDCState(Lcom/android/internal/telephony/DriverCall$State;)Lcom/android/internal/telephony/Call$State;

    #@6
    move-result-object v1

    #@7
    .line 212
    .local v1, newState:Lcom/android/internal/telephony/Call$State;
    iget-object v2, p0, Lcom/android/internal/telephony/Call;->state:Lcom/android/internal/telephony/Call$State;

    #@9
    if-eq v1, v2, :cond_e

    #@b
    .line 213
    iput-object v1, p0, Lcom/android/internal/telephony/Call;->state:Lcom/android/internal/telephony/Call$State;

    #@d
    .line 214
    const/4 v0, 0x1

    #@e
    .line 217
    :cond_e
    return v0
.end method
