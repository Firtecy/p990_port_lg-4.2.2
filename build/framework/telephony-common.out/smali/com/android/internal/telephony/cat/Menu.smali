.class public Lcom/android/internal/telephony/cat/Menu;
.super Ljava/lang/Object;
.source "Menu.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/android/internal/telephony/cat/Menu;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public defaultItem:I

.field public helpAvailable:Z

.field public items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/cat/Item;",
            ">;"
        }
    .end annotation
.end field

.field public itemsIconSelfExplanatory:Z

.field public presentationType:Lcom/android/internal/telephony/cat/PresentationType;

.field public softKeyPreferred:Z

.field public title:Ljava/lang/String;

.field public titleAttrs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/cat/TextAttribute;",
            ">;"
        }
    .end annotation
.end field

.field public titleIcon:Landroid/graphics/Bitmap;

.field public titleIconSelfExplanatory:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 106
    new-instance v0, Lcom/android/internal/telephony/cat/Menu$1;

    #@2
    invoke-direct {v0}, Lcom/android/internal/telephony/cat/Menu$1;-><init>()V

    #@5
    sput-object v0, Lcom/android/internal/telephony/cat/Menu;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x0

    #@2
    .line 43
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 45
    new-instance v0, Ljava/util/ArrayList;

    #@7
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@a
    iput-object v0, p0, Lcom/android/internal/telephony/cat/Menu;->items:Ljava/util/List;

    #@c
    .line 46
    iput-object v3, p0, Lcom/android/internal/telephony/cat/Menu;->title:Ljava/lang/String;

    #@e
    .line 47
    iput-object v3, p0, Lcom/android/internal/telephony/cat/Menu;->titleAttrs:Ljava/util/List;

    #@10
    .line 49
    const-string v0, "VDF"

    #@12
    const-string v1, "ro.build.target_operator"

    #@14
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1b
    move-result v0

    #@1c
    if-eqz v0, :cond_30

    #@1e
    .line 50
    const/4 v0, -0x1

    #@1f
    iput v0, p0, Lcom/android/internal/telephony/cat/Menu;->defaultItem:I

    #@21
    .line 58
    :goto_21
    iput-boolean v2, p0, Lcom/android/internal/telephony/cat/Menu;->softKeyPreferred:Z

    #@23
    .line 59
    iput-boolean v2, p0, Lcom/android/internal/telephony/cat/Menu;->helpAvailable:Z

    #@25
    .line 60
    iput-boolean v2, p0, Lcom/android/internal/telephony/cat/Menu;->titleIconSelfExplanatory:Z

    #@27
    .line 61
    iput-boolean v2, p0, Lcom/android/internal/telephony/cat/Menu;->itemsIconSelfExplanatory:Z

    #@29
    .line 62
    iput-object v3, p0, Lcom/android/internal/telephony/cat/Menu;->titleIcon:Landroid/graphics/Bitmap;

    #@2b
    .line 64
    sget-object v0, Lcom/android/internal/telephony/cat/PresentationType;->NAVIGATION_OPTIONS:Lcom/android/internal/telephony/cat/PresentationType;

    #@2d
    iput-object v0, p0, Lcom/android/internal/telephony/cat/Menu;->presentationType:Lcom/android/internal/telephony/cat/PresentationType;

    #@2f
    .line 65
    return-void

    #@30
    .line 55
    :cond_30
    iput v2, p0, Lcom/android/internal/telephony/cat/Menu;->defaultItem:I

    #@32
    goto :goto_21
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 9
    .parameter "in"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v5, 0x0

    #@2
    const/4 v4, 0x1

    #@3
    .line 67
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 68
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@9
    move-result-object v3

    #@a
    iput-object v3, p0, Lcom/android/internal/telephony/cat/Menu;->title:Ljava/lang/String;

    #@c
    .line 69
    invoke-virtual {p1, v6}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    #@f
    move-result-object v3

    #@10
    check-cast v3, Landroid/graphics/Bitmap;

    #@12
    iput-object v3, p0, Lcom/android/internal/telephony/cat/Menu;->titleIcon:Landroid/graphics/Bitmap;

    #@14
    .line 71
    new-instance v3, Ljava/util/ArrayList;

    #@16
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@19
    iput-object v3, p0, Lcom/android/internal/telephony/cat/Menu;->items:Ljava/util/List;

    #@1b
    .line 72
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@1e
    move-result v2

    #@1f
    .line 73
    .local v2, size:I
    const/4 v0, 0x0

    #@20
    .local v0, i:I
    :goto_20
    if-ge v0, v2, :cond_30

    #@22
    .line 74
    invoke-virtual {p1, v6}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    #@25
    move-result-object v1

    #@26
    check-cast v1, Lcom/android/internal/telephony/cat/Item;

    #@28
    .line 75
    .local v1, item:Lcom/android/internal/telephony/cat/Item;
    iget-object v3, p0, Lcom/android/internal/telephony/cat/Menu;->items:Ljava/util/List;

    #@2a
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@2d
    .line 73
    add-int/lit8 v0, v0, 0x1

    #@2f
    goto :goto_20

    #@30
    .line 77
    .end local v1           #item:Lcom/android/internal/telephony/cat/Item;
    :cond_30
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@33
    move-result v3

    #@34
    iput v3, p0, Lcom/android/internal/telephony/cat/Menu;->defaultItem:I

    #@36
    .line 78
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@39
    move-result v3

    #@3a
    if-ne v3, v4, :cond_66

    #@3c
    move v3, v4

    #@3d
    :goto_3d
    iput-boolean v3, p0, Lcom/android/internal/telephony/cat/Menu;->softKeyPreferred:Z

    #@3f
    .line 79
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@42
    move-result v3

    #@43
    if-ne v3, v4, :cond_68

    #@45
    move v3, v4

    #@46
    :goto_46
    iput-boolean v3, p0, Lcom/android/internal/telephony/cat/Menu;->helpAvailable:Z

    #@48
    .line 80
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@4b
    move-result v3

    #@4c
    if-ne v3, v4, :cond_6a

    #@4e
    move v3, v4

    #@4f
    :goto_4f
    iput-boolean v3, p0, Lcom/android/internal/telephony/cat/Menu;->titleIconSelfExplanatory:Z

    #@51
    .line 81
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@54
    move-result v3

    #@55
    if-ne v3, v4, :cond_6c

    #@57
    :goto_57
    iput-boolean v4, p0, Lcom/android/internal/telephony/cat/Menu;->itemsIconSelfExplanatory:Z

    #@59
    .line 82
    invoke-static {}, Lcom/android/internal/telephony/cat/PresentationType;->values()[Lcom/android/internal/telephony/cat/PresentationType;

    #@5c
    move-result-object v3

    #@5d
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@60
    move-result v4

    #@61
    aget-object v3, v3, v4

    #@63
    iput-object v3, p0, Lcom/android/internal/telephony/cat/Menu;->presentationType:Lcom/android/internal/telephony/cat/PresentationType;

    #@65
    .line 83
    return-void

    #@66
    :cond_66
    move v3, v5

    #@67
    .line 78
    goto :goto_3d

    #@68
    :cond_68
    move v3, v5

    #@69
    .line 79
    goto :goto_46

    #@6a
    :cond_6a
    move v3, v5

    #@6b
    .line 80
    goto :goto_4f

    #@6c
    :cond_6c
    move v4, v5

    #@6d
    .line 81
    goto :goto_57
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/android/internal/telephony/cat/Menu$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/cat/Menu;-><init>(Landroid/os/Parcel;)V

    #@3
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 86
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 8
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 90
    iget-object v2, p0, Lcom/android/internal/telephony/cat/Menu;->title:Ljava/lang/String;

    #@4
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@7
    .line 91
    iget-object v2, p0, Lcom/android/internal/telephony/cat/Menu;->titleIcon:Landroid/graphics/Bitmap;

    #@9
    invoke-virtual {p1, v2, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    #@c
    .line 93
    iget-object v2, p0, Lcom/android/internal/telephony/cat/Menu;->items:Ljava/util/List;

    #@e
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@11
    move-result v1

    #@12
    .line 94
    .local v1, size:I
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 95
    const/4 v0, 0x0

    #@16
    .local v0, i:I
    :goto_16
    if-ge v0, v1, :cond_26

    #@18
    .line 96
    iget-object v2, p0, Lcom/android/internal/telephony/cat/Menu;->items:Ljava/util/List;

    #@1a
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@1d
    move-result-object v2

    #@1e
    check-cast v2, Landroid/os/Parcelable;

    #@20
    invoke-virtual {p1, v2, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    #@23
    .line 95
    add-int/lit8 v0, v0, 0x1

    #@25
    goto :goto_16

    #@26
    .line 98
    :cond_26
    iget v2, p0, Lcom/android/internal/telephony/cat/Menu;->defaultItem:I

    #@28
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@2b
    .line 99
    iget-boolean v2, p0, Lcom/android/internal/telephony/cat/Menu;->softKeyPreferred:Z

    #@2d
    if-eqz v2, :cond_54

    #@2f
    move v2, v3

    #@30
    :goto_30
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@33
    .line 100
    iget-boolean v2, p0, Lcom/android/internal/telephony/cat/Menu;->helpAvailable:Z

    #@35
    if-eqz v2, :cond_56

    #@37
    move v2, v3

    #@38
    :goto_38
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@3b
    .line 101
    iget-boolean v2, p0, Lcom/android/internal/telephony/cat/Menu;->titleIconSelfExplanatory:Z

    #@3d
    if-eqz v2, :cond_58

    #@3f
    move v2, v3

    #@40
    :goto_40
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@43
    .line 102
    iget-boolean v2, p0, Lcom/android/internal/telephony/cat/Menu;->itemsIconSelfExplanatory:Z

    #@45
    if-eqz v2, :cond_5a

    #@47
    :goto_47
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@4a
    .line 103
    iget-object v2, p0, Lcom/android/internal/telephony/cat/Menu;->presentationType:Lcom/android/internal/telephony/cat/PresentationType;

    #@4c
    invoke-virtual {v2}, Lcom/android/internal/telephony/cat/PresentationType;->ordinal()I

    #@4f
    move-result v2

    #@50
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@53
    .line 104
    return-void

    #@54
    :cond_54
    move v2, v4

    #@55
    .line 99
    goto :goto_30

    #@56
    :cond_56
    move v2, v4

    #@57
    .line 100
    goto :goto_38

    #@58
    :cond_58
    move v2, v4

    #@59
    .line 101
    goto :goto_40

    #@5a
    :cond_5a
    move v3, v4

    #@5b
    .line 102
    goto :goto_47
.end method
