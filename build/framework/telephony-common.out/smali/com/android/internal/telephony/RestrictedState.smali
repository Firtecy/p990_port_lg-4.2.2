.class public Lcom/android/internal/telephony/RestrictedState;
.super Ljava/lang/Object;
.source "RestrictedState.java"


# instance fields
.field private mCsEmergencyRestricted:Z

.field private mCsNormalRestricted:Z

.field private mPsRestricted:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 36
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 37
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/RestrictedState;->setPsRestricted(Z)V

    #@7
    .line 38
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/RestrictedState;->setCsNormalRestricted(Z)V

    #@a
    .line 39
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/RestrictedState;->setCsEmergencyRestricted(Z)V

    #@d
    .line 40
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 8
    .parameter "o"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 93
    :try_start_1
    move-object v0, p1

    #@2
    check-cast v0, Lcom/android/internal/telephony/RestrictedState;

    #@4
    move-object v2, v0
    :try_end_5
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_5} :catch_8

    #@5
    .line 98
    .local v2, s:Lcom/android/internal/telephony/RestrictedState;
    if-nez p1, :cond_a

    #@7
    .line 102
    .end local v2           #s:Lcom/android/internal/telephony/RestrictedState;
    :cond_7
    :goto_7
    return v3

    #@8
    .line 94
    :catch_8
    move-exception v1

    #@9
    .line 95
    .local v1, ex:Ljava/lang/ClassCastException;
    goto :goto_7

    #@a
    .line 102
    .end local v1           #ex:Ljava/lang/ClassCastException;
    .restart local v2       #s:Lcom/android/internal/telephony/RestrictedState;
    :cond_a
    iget-boolean v4, p0, Lcom/android/internal/telephony/RestrictedState;->mPsRestricted:Z

    #@c
    iget-boolean v5, v2, Lcom/android/internal/telephony/RestrictedState;->mPsRestricted:Z

    #@e
    if-ne v4, v5, :cond_7

    #@10
    iget-boolean v4, p0, Lcom/android/internal/telephony/RestrictedState;->mCsNormalRestricted:Z

    #@12
    iget-boolean v5, v2, Lcom/android/internal/telephony/RestrictedState;->mCsNormalRestricted:Z

    #@14
    if-ne v4, v5, :cond_7

    #@16
    iget-boolean v4, p0, Lcom/android/internal/telephony/RestrictedState;->mCsEmergencyRestricted:Z

    #@18
    iget-boolean v5, v2, Lcom/android/internal/telephony/RestrictedState;->mCsEmergencyRestricted:Z

    #@1a
    if-ne v4, v5, :cond_7

    #@1c
    const/4 v3, 0x1

    #@1d
    goto :goto_7
.end method

.method public isCsEmergencyRestricted()Z
    .registers 2

    #@0
    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/android/internal/telephony/RestrictedState;->mCsEmergencyRestricted:Z

    #@2
    return v0
.end method

.method public isCsNormalRestricted()Z
    .registers 2

    #@0
    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/android/internal/telephony/RestrictedState;->mCsNormalRestricted:Z

    #@2
    return v0
.end method

.method public isCsRestricted()Z
    .registers 2

    #@0
    .prologue
    .line 85
    iget-boolean v0, p0, Lcom/android/internal/telephony/RestrictedState;->mCsNormalRestricted:Z

    #@2
    if-eqz v0, :cond_a

    #@4
    iget-boolean v0, p0, Lcom/android/internal/telephony/RestrictedState;->mCsEmergencyRestricted:Z

    #@6
    if-eqz v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public isPsRestricted()Z
    .registers 2

    #@0
    .prologue
    .line 81
    iget-boolean v0, p0, Lcom/android/internal/telephony/RestrictedState;->mPsRestricted:Z

    #@2
    return v0
.end method

.method public setCsEmergencyRestricted(Z)V
    .registers 2
    .parameter "csEmergencyRestricted"

    #@0
    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/android/internal/telephony/RestrictedState;->mCsEmergencyRestricted:Z

    #@2
    .line 47
    return-void
.end method

.method public setCsNormalRestricted(Z)V
    .registers 2
    .parameter "csNormalRestricted"

    #@0
    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/android/internal/telephony/RestrictedState;->mCsNormalRestricted:Z

    #@2
    .line 61
    return-void
.end method

.method public setPsRestricted(Z)V
    .registers 2
    .parameter "psRestricted"

    #@0
    .prologue
    .line 74
    iput-boolean p1, p0, Lcom/android/internal/telephony/RestrictedState;->mPsRestricted:Z

    #@2
    .line 75
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 109
    const-string v0, "none"

    #@2
    .line 111
    .local v0, csString:Ljava/lang/String;
    iget-boolean v1, p0, Lcom/android/internal/telephony/RestrictedState;->mCsEmergencyRestricted:Z

    #@4
    if-eqz v1, :cond_2c

    #@6
    iget-boolean v1, p0, Lcom/android/internal/telephony/RestrictedState;->mCsNormalRestricted:Z

    #@8
    if-eqz v1, :cond_2c

    #@a
    .line 112
    const-string v0, "all"

    #@c
    .line 119
    :cond_c
    :goto_c
    new-instance v1, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v2, "Restricted State CS: "

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    const-string v2, " PS:"

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    iget-boolean v2, p0, Lcom/android/internal/telephony/RestrictedState;->mPsRestricted:Z

    #@23
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@26
    move-result-object v1

    #@27
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v1

    #@2b
    return-object v1

    #@2c
    .line 113
    :cond_2c
    iget-boolean v1, p0, Lcom/android/internal/telephony/RestrictedState;->mCsEmergencyRestricted:Z

    #@2e
    if-eqz v1, :cond_37

    #@30
    iget-boolean v1, p0, Lcom/android/internal/telephony/RestrictedState;->mCsNormalRestricted:Z

    #@32
    if-nez v1, :cond_37

    #@34
    .line 114
    const-string v0, "emergency"

    #@36
    goto :goto_c

    #@37
    .line 115
    :cond_37
    iget-boolean v1, p0, Lcom/android/internal/telephony/RestrictedState;->mCsEmergencyRestricted:Z

    #@39
    if-nez v1, :cond_c

    #@3b
    iget-boolean v1, p0, Lcom/android/internal/telephony/RestrictedState;->mCsNormalRestricted:Z

    #@3d
    if-eqz v1, :cond_c

    #@3f
    .line 116
    const-string v0, "normal call"

    #@41
    goto :goto_c
.end method
