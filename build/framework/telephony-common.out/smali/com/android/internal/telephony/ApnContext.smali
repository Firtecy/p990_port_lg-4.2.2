.class public Lcom/android/internal/telephony/ApnContext;
.super Ljava/lang/Object;
.source "ApnContext.java"


# static fields
.field protected static final DBG:Z = true


# instance fields
.field public final LOG_TAG:Ljava/lang/String;

.field private final mApnType:Ljava/lang/String;

.field mDataConnection:Lcom/android/internal/telephony/DataConnection;

.field mDataConnectionAc:Lcom/android/internal/telephony/DataConnectionAc;

.field mDataEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private mDataProfile:Lcom/android/internal/telephony/DataProfile;

.field mDependencyMet:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final mPriority:I

.field mReason:Ljava/lang/String;

.field mRetryCount:I

.field private mState:Lcom/android/internal/telephony/DctConstants$State;

.field private mWaitingApns:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/telephony/DataProfile;",
            ">;"
        }
    .end annotation
.end field

.field private mWaitingApnsPermanentFailureCountDown:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "apnType"
    .parameter "logTag"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 70
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 45
    const/4 v0, 0x0

    #@5
    iput-object v0, p0, Lcom/android/internal/telephony/ApnContext;->mWaitingApns:Ljava/util/ArrayList;

    #@7
    .line 71
    iput-object p1, p0, Lcom/android/internal/telephony/ApnContext;->mApnType:Ljava/lang/String;

    #@9
    .line 72
    sget-object v0, Lcom/android/internal/telephony/DctConstants$State;->IDLE:Lcom/android/internal/telephony/DctConstants$State;

    #@b
    iput-object v0, p0, Lcom/android/internal/telephony/ApnContext;->mState:Lcom/android/internal/telephony/DctConstants$State;

    #@d
    .line 74
    sget-object v0, Lcom/android/internal/telephony/DataConnectionTracker;->mApnPriorities:Ljava/util/LinkedHashMap;

    #@f
    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@12
    move-result-object v0

    #@13
    if-nez v0, :cond_38

    #@15
    .line 75
    iput v2, p0, Lcom/android/internal/telephony/ApnContext;->mPriority:I

    #@17
    .line 80
    :goto_17
    const-string v0, "dataEnabled"

    #@19
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/ApnContext;->setReason(Ljava/lang/String;)V

    #@1c
    .line 81
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/ApnContext;->setRetryCount(I)V

    #@1f
    .line 82
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    #@21
    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    #@24
    iput-object v0, p0, Lcom/android/internal/telephony/ApnContext;->mDataEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@26
    .line 83
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    #@28
    const/4 v1, 0x1

    #@29
    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    #@2c
    iput-object v0, p0, Lcom/android/internal/telephony/ApnContext;->mDependencyMet:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@2e
    .line 84
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    #@30
    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    #@33
    iput-object v0, p0, Lcom/android/internal/telephony/ApnContext;->mWaitingApnsPermanentFailureCountDown:Ljava/util/concurrent/atomic/AtomicInteger;

    #@35
    .line 85
    iput-object p2, p0, Lcom/android/internal/telephony/ApnContext;->LOG_TAG:Ljava/lang/String;

    #@37
    .line 86
    return-void

    #@38
    .line 77
    :cond_38
    sget-object v0, Lcom/android/internal/telephony/DataConnectionTracker;->mApnPriorities:Ljava/util/LinkedHashMap;

    #@3a
    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@3d
    move-result-object v0

    #@3e
    check-cast v0, Ljava/lang/Integer;

    #@40
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@43
    move-result v0

    #@44
    iput v0, p0, Lcom/android/internal/telephony/ApnContext;->mPriority:I

    #@46
    goto :goto_17
.end method


# virtual methods
.method public decWaitingApnsPermFailCount()V
    .registers 2

    #@0
    .prologue
    .line 140
    iget-object v0, p0, Lcom/android/internal/telephony/ApnContext;->mWaitingApnsPermanentFailureCountDown:Ljava/util/concurrent/atomic/AtomicInteger;

    #@2
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    #@5
    .line 141
    return-void
.end method

.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 6
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 274
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "ApnContext: "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {p0}, Lcom/android/internal/telephony/ApnContext;->toString()Ljava/lang/String;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v0

    #@17
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1a
    .line 275
    return-void
.end method

.method public declared-synchronized getApnSetting()Lcom/android/internal/telephony/DataProfile;
    .registers 2

    #@0
    .prologue
    .line 123
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/internal/telephony/ApnContext;->mDataProfile:Lcom/android/internal/telephony/DataProfile;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return-object v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public getApnType()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 89
    iget-object v0, p0, Lcom/android/internal/telephony/ApnContext;->mApnType:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public declared-synchronized getDataConnection()Lcom/android/internal/telephony/DataConnection;
    .registers 2

    #@0
    .prologue
    .line 93
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/internal/telephony/ApnContext;->mDataConnection:Lcom/android/internal/telephony/DataConnection;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return-object v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized getDataConnectionAc()Lcom/android/internal/telephony/DataConnectionAc;
    .registers 2

    #@0
    .prologue
    .line 105
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/internal/telephony/ApnContext;->mDataConnectionAc:Lcom/android/internal/telephony/DataConnectionAc;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return-object v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public getDependencyMet()Z
    .registers 2

    #@0
    .prologue
    .line 256
    iget-object v0, p0, Lcom/android/internal/telephony/ApnContext;->mDependencyMet:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@2
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public declared-synchronized getNextWaitingApn()Lcom/android/internal/telephony/DataProfile;
    .registers 4

    #@0
    .prologue
    .line 144
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v1, p0, Lcom/android/internal/telephony/ApnContext;->mWaitingApns:Ljava/util/ArrayList;

    #@3
    .line 145
    .local v1, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/DataProfile;>;"
    const/4 v0, 0x0

    #@4
    .line 147
    .local v0, apn:Lcom/android/internal/telephony/DataProfile;
    if-eqz v1, :cond_13

    #@6
    .line 148
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    #@9
    move-result v2

    #@a
    if-nez v2, :cond_13

    #@c
    .line 149
    const/4 v2, 0x0

    #@d
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@10
    move-result-object v0

    #@11
    .end local v0           #apn:Lcom/android/internal/telephony/DataProfile;
    check-cast v0, Lcom/android/internal/telephony/DataProfile;
    :try_end_13
    .catchall {:try_start_1 .. :try_end_13} :catchall_15

    #@13
    .line 152
    .restart local v0       #apn:Lcom/android/internal/telephony/DataProfile;
    :cond_13
    monitor-exit p0

    #@14
    return-object v0

    #@15
    .line 144
    .end local v0           #apn:Lcom/android/internal/telephony/DataProfile;
    .end local v1           #list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/DataProfile;>;"
    :catchall_15
    move-exception v2

    #@16
    monitor-exit p0

    #@17
    throw v2
.end method

.method public declared-synchronized getPriority()I
    .registers 2

    #@0
    .prologue
    .line 167
    monitor-enter p0

    #@1
    :try_start_1
    iget v0, p0, Lcom/android/internal/telephony/ApnContext;->mPriority:I
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized getReason()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 215
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/internal/telephony/ApnContext;->mReason:Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return-object v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized getRetryCount()I
    .registers 2

    #@0
    .prologue
    .line 230
    monitor-enter p0

    #@1
    :try_start_1
    iget v0, p0, Lcom/android/internal/telephony/ApnContext;->mRetryCount:I
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized getState()Lcom/android/internal/telephony/DctConstants$State;
    .registers 2

    #@0
    .prologue
    .line 198
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/internal/telephony/ApnContext;->mState:Lcom/android/internal/telephony/DctConstants$State;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return-object v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public getTetheredCallOn()Z
    .registers 2

    #@0
    .prologue
    .line 282
    iget-object v0, p0, Lcom/android/internal/telephony/ApnContext;->mDataProfile:Lcom/android/internal/telephony/DataProfile;

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x0

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    iget-object v0, p0, Lcom/android/internal/telephony/ApnContext;->mDataProfile:Lcom/android/internal/telephony/DataProfile;

    #@8
    iget-boolean v0, v0, Lcom/android/internal/telephony/DataProfile;->mTetheredCallOn:Z

    #@a
    goto :goto_5
.end method

.method public declared-synchronized getWaitingApns()Ljava/util/ArrayList;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/telephony/DataProfile;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 162
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/internal/telephony/ApnContext;->mWaitingApns:Ljava/util/ArrayList;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return-object v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public getWaitingApnsPermFailCount()I
    .registers 2

    #@0
    .prologue
    .line 136
    iget-object v0, p0, Lcom/android/internal/telephony/ApnContext;->mWaitingApnsPermanentFailureCountDown:Ljava/util/concurrent/atomic/AtomicInteger;

    #@2
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isDisconnected()Z
    .registers 3

    #@0
    .prologue
    .line 202
    invoke-virtual {p0}, Lcom/android/internal/telephony/ApnContext;->getState()Lcom/android/internal/telephony/DctConstants$State;

    #@3
    move-result-object v0

    #@4
    .line 203
    .local v0, currentState:Lcom/android/internal/telephony/DctConstants$State;
    sget-object v1, Lcom/android/internal/telephony/DctConstants$State;->IDLE:Lcom/android/internal/telephony/DctConstants$State;

    #@6
    if-eq v0, v1, :cond_c

    #@8
    sget-object v1, Lcom/android/internal/telephony/DctConstants$State;->FAILED:Lcom/android/internal/telephony/DctConstants$State;

    #@a
    if-ne v0, v1, :cond_e

    #@c
    :cond_c
    const/4 v1, 0x1

    #@d
    :goto_d
    return v1

    #@e
    :cond_e
    const/4 v1, 0x0

    #@f
    goto :goto_d
.end method

.method public isEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 245
    iget-object v0, p0, Lcom/android/internal/telephony/ApnContext;->mDataEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@2
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public declared-synchronized isEqualPriority(Lcom/android/internal/telephony/ApnContext;)Z
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 179
    monitor-enter p0

    #@1
    :try_start_1
    iget v0, p0, Lcom/android/internal/telephony/ApnContext;->mPriority:I

    #@3
    invoke-virtual {p1}, Lcom/android/internal/telephony/ApnContext;->getPriority()I
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_e

    #@6
    move-result v1

    #@7
    if-ne v0, v1, :cond_c

    #@9
    const/4 v0, 0x1

    #@a
    :goto_a
    monitor-exit p0

    #@b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_a

    #@e
    :catchall_e
    move-exception v0

    #@f
    monitor-exit p0

    #@10
    throw v0
.end method

.method public declared-synchronized isHigherPriority(Lcom/android/internal/telephony/ApnContext;)Z
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 171
    monitor-enter p0

    #@1
    :try_start_1
    iget v0, p0, Lcom/android/internal/telephony/ApnContext;->mPriority:I

    #@3
    invoke-virtual {p1}, Lcom/android/internal/telephony/ApnContext;->getPriority()I
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_e

    #@6
    move-result v1

    #@7
    if-le v0, v1, :cond_c

    #@9
    const/4 v0, 0x1

    #@a
    :goto_a
    monitor-exit p0

    #@b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_a

    #@e
    :catchall_e
    move-exception v0

    #@f
    monitor-exit p0

    #@10
    throw v0
.end method

.method public declared-synchronized isLowerPriority(Lcom/android/internal/telephony/ApnContext;)Z
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 175
    monitor-enter p0

    #@1
    :try_start_1
    iget v0, p0, Lcom/android/internal/telephony/ApnContext;->mPriority:I

    #@3
    invoke-virtual {p1}, Lcom/android/internal/telephony/ApnContext;->getPriority()I
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_e

    #@6
    move-result v1

    #@7
    if-ge v0, v1, :cond_c

    #@9
    const/4 v0, 0x1

    #@a
    :goto_a
    monitor-exit p0

    #@b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_a

    #@e
    :catchall_e
    move-exception v0

    #@f
    monitor-exit p0

    #@10
    throw v0
.end method

.method public isReady()Z
    .registers 2

    #@0
    .prologue
    .line 234
    iget-object v0, p0, Lcom/android/internal/telephony/ApnContext;->mDataEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@2
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_18

    #@8
    iget-object v0, p0, Lcom/android/internal/telephony/ApnContext;->mDependencyMet:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@a
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_18

    #@10
    invoke-virtual {p0}, Lcom/android/internal/telephony/ApnContext;->getTetheredCallOn()Z

    #@13
    move-result v0

    #@14
    if-nez v0, :cond_18

    #@16
    const/4 v0, 0x1

    #@17
    :goto_17
    return v0

    #@18
    :cond_18
    const/4 v0, 0x0

    #@19
    goto :goto_17
.end method

.method protected log(Ljava/lang/String;)V
    .registers 5
    .parameter "s"

    #@0
    .prologue
    .line 270
    iget-object v0, p0, Lcom/android/internal/telephony/ApnContext;->LOG_TAG:Ljava/lang/String;

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[ApnContext:"

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget-object v2, p0, Lcom/android/internal/telephony/ApnContext;->mApnType:Ljava/lang/String;

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    const-string v2, "] "

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 271
    return-void
.end method

.method public declared-synchronized removeWaitingApn(Lcom/android/internal/telephony/DataProfile;)V
    .registers 3
    .parameter "apn"

    #@0
    .prologue
    .line 156
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/internal/telephony/ApnContext;->mWaitingApns:Ljava/util/ArrayList;

    #@3
    if-eqz v0, :cond_a

    #@5
    .line 157
    iget-object v0, p0, Lcom/android/internal/telephony/ApnContext;->mWaitingApns:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    #@a
    .line 159
    :cond_a
    monitor-exit p0

    #@b
    return-void

    #@c
    .line 156
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0

    #@e
    throw v0
.end method

.method public declared-synchronized setApnSetting(Lcom/android/internal/telephony/DataProfile;)V
    .registers 3
    .parameter "apnSetting"

    #@0
    .prologue
    .line 127
    monitor-enter p0

    #@1
    :try_start_1
    iput-object p1, p0, Lcom/android/internal/telephony/ApnContext;->mDataProfile:Lcom/android/internal/telephony/DataProfile;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    .line 128
    monitor-exit p0

    #@4
    return-void

    #@5
    .line 127
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized setDataConnection(Lcom/android/internal/telephony/DataConnection;)V
    .registers 4
    .parameter "dc"

    #@0
    .prologue
    .line 98
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v1, "setDataConnection: old dc="

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v0

    #@c
    iget-object v1, p0, Lcom/android/internal/telephony/ApnContext;->mDataConnection:Lcom/android/internal/telephony/DataConnection;

    #@e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v0

    #@12
    const-string v1, " new dc="

    #@14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v0

    #@18
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v0

    #@1c
    const-string v1, " this="

    #@1e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v0

    #@22
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v0

    #@26
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v0

    #@2a
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/ApnContext;->log(Ljava/lang/String;)V

    #@2d
    .line 100
    iput-object p1, p0, Lcom/android/internal/telephony/ApnContext;->mDataConnection:Lcom/android/internal/telephony/DataConnection;
    :try_end_2f
    .catchall {:try_start_1 .. :try_end_2f} :catchall_31

    #@2f
    .line 101
    monitor-exit p0

    #@30
    return-void

    #@31
    .line 98
    :catchall_31
    move-exception v0

    #@32
    monitor-exit p0

    #@33
    throw v0
.end method

.method public declared-synchronized setDataConnectionAc(Lcom/android/internal/telephony/DataConnectionAc;)V
    .registers 4
    .parameter "dcac"

    #@0
    .prologue
    .line 110
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v1, "setDataConnectionAc: old dcac="

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v0

    #@c
    iget-object v1, p0, Lcom/android/internal/telephony/ApnContext;->mDataConnectionAc:Lcom/android/internal/telephony/DataConnectionAc;

    #@e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v0

    #@12
    const-string v1, " new dcac="

    #@14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v0

    #@18
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v0

    #@1c
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v0

    #@20
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/ApnContext;->log(Ljava/lang/String;)V

    #@23
    .line 112
    if-eqz p1, :cond_2c

    #@25
    .line 113
    invoke-virtual {p1, p0}, Lcom/android/internal/telephony/DataConnectionAc;->addApnContextSync(Lcom/android/internal/telephony/ApnContext;)V

    #@28
    .line 119
    :cond_28
    :goto_28
    iput-object p1, p0, Lcom/android/internal/telephony/ApnContext;->mDataConnectionAc:Lcom/android/internal/telephony/DataConnectionAc;
    :try_end_2a
    .catchall {:try_start_1 .. :try_end_2a} :catchall_36

    #@2a
    .line 120
    monitor-exit p0

    #@2b
    return-void

    #@2c
    .line 115
    :cond_2c
    :try_start_2c
    iget-object v0, p0, Lcom/android/internal/telephony/ApnContext;->mDataConnectionAc:Lcom/android/internal/telephony/DataConnectionAc;

    #@2e
    if-eqz v0, :cond_28

    #@30
    .line 116
    iget-object v0, p0, Lcom/android/internal/telephony/ApnContext;->mDataConnectionAc:Lcom/android/internal/telephony/DataConnectionAc;

    #@32
    invoke-virtual {v0, p0}, Lcom/android/internal/telephony/DataConnectionAc;->removeApnContextSync(Lcom/android/internal/telephony/ApnContext;)V
    :try_end_35
    .catchall {:try_start_2c .. :try_end_35} :catchall_36

    #@35
    goto :goto_28

    #@36
    .line 110
    :catchall_36
    move-exception v0

    #@37
    monitor-exit p0

    #@38
    throw v0
.end method

.method public setDependencyMet(Z)V
    .registers 4
    .parameter "met"

    #@0
    .prologue
    .line 250
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "set mDependencyMet as "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    const-string v1, " current state is "

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    iget-object v1, p0, Lcom/android/internal/telephony/ApnContext;->mDependencyMet:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@17
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    #@1a
    move-result v1

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v0

    #@1f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v0

    #@23
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/ApnContext;->log(Ljava/lang/String;)V

    #@26
    .line 252
    iget-object v0, p0, Lcom/android/internal/telephony/ApnContext;->mDependencyMet:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@28
    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    #@2b
    .line 253
    return-void
.end method

.method public setEnabled(Z)V
    .registers 4
    .parameter "enabled"

    #@0
    .prologue
    .line 239
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "set enabled as "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    const-string v1, ", current state is "

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    iget-object v1, p0, Lcom/android/internal/telephony/ApnContext;->mDataEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@17
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    #@1a
    move-result v1

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v0

    #@1f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v0

    #@23
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/ApnContext;->log(Ljava/lang/String;)V

    #@26
    .line 241
    iget-object v0, p0, Lcom/android/internal/telephony/ApnContext;->mDataEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@28
    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    #@2b
    .line 242
    return-void
.end method

.method public declared-synchronized setReason(Ljava/lang/String;)V
    .registers 4
    .parameter "reason"

    #@0
    .prologue
    .line 209
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v1, "set reason as "

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v0

    #@c
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v0

    #@10
    const-string v1, ",current state "

    #@12
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v0

    #@16
    iget-object v1, p0, Lcom/android/internal/telephony/ApnContext;->mState:Lcom/android/internal/telephony/DctConstants$State;

    #@18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v0

    #@1c
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v0

    #@20
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/ApnContext;->log(Ljava/lang/String;)V

    #@23
    .line 211
    iput-object p1, p0, Lcom/android/internal/telephony/ApnContext;->mReason:Ljava/lang/String;
    :try_end_25
    .catchall {:try_start_1 .. :try_end_25} :catchall_27

    #@25
    .line 212
    monitor-exit p0

    #@26
    return-void

    #@27
    .line 209
    :catchall_27
    move-exception v0

    #@28
    monitor-exit p0

    #@29
    throw v0
.end method

.method public declared-synchronized setRetryCount(I)V
    .registers 5
    .parameter "retryCount"

    #@0
    .prologue
    .line 220
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v1, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v2, "setRetryCount: "

    #@8
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v1

    #@c
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/ApnContext;->log(Ljava/lang/String;)V

    #@17
    .line 222
    iput p1, p0, Lcom/android/internal/telephony/ApnContext;->mRetryCount:I

    #@19
    .line 223
    iget-object v0, p0, Lcom/android/internal/telephony/ApnContext;->mDataConnection:Lcom/android/internal/telephony/DataConnection;

    #@1b
    .line 224
    .local v0, dc:Lcom/android/internal/telephony/DataConnection;
    if-eqz v0, :cond_20

    #@1d
    .line 225
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/DataConnection;->setRetryCount(I)V
    :try_end_20
    .catchall {:try_start_1 .. :try_end_20} :catchall_22

    #@20
    .line 227
    :cond_20
    monitor-exit p0

    #@21
    return-void

    #@22
    .line 220
    .end local v0           #dc:Lcom/android/internal/telephony/DataConnection;
    :catchall_22
    move-exception v1

    #@23
    monitor-exit p0

    #@24
    throw v1
.end method

.method public declared-synchronized setState(Lcom/android/internal/telephony/DctConstants$State;)V
    .registers 4
    .parameter "s"

    #@0
    .prologue
    .line 185
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v1, "setState: "

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v0

    #@c
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v0

    #@10
    const-string v1, ", previous state:"

    #@12
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v0

    #@16
    iget-object v1, p0, Lcom/android/internal/telephony/ApnContext;->mState:Lcom/android/internal/telephony/DctConstants$State;

    #@18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v0

    #@1c
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v0

    #@20
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/ApnContext;->log(Ljava/lang/String;)V

    #@23
    .line 188
    iput-object p1, p0, Lcom/android/internal/telephony/ApnContext;->mState:Lcom/android/internal/telephony/DctConstants$State;

    #@25
    .line 190
    iget-object v0, p0, Lcom/android/internal/telephony/ApnContext;->mState:Lcom/android/internal/telephony/DctConstants$State;

    #@27
    sget-object v1, Lcom/android/internal/telephony/DctConstants$State;->FAILED:Lcom/android/internal/telephony/DctConstants$State;

    #@29
    if-ne v0, v1, :cond_34

    #@2b
    .line 191
    iget-object v0, p0, Lcom/android/internal/telephony/ApnContext;->mWaitingApns:Ljava/util/ArrayList;

    #@2d
    if-eqz v0, :cond_34

    #@2f
    .line 192
    iget-object v0, p0, Lcom/android/internal/telephony/ApnContext;->mWaitingApns:Ljava/util/ArrayList;

    #@31
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
    :try_end_34
    .catchall {:try_start_1 .. :try_end_34} :catchall_36

    #@34
    .line 195
    :cond_34
    monitor-exit p0

    #@35
    return-void

    #@36
    .line 185
    :catchall_36
    move-exception v0

    #@37
    monitor-exit p0

    #@38
    throw v0
.end method

.method public setTetheredCallOn(Z)V
    .registers 3
    .parameter "tetheredCallOn"

    #@0
    .prologue
    .line 278
    iget-object v0, p0, Lcom/android/internal/telephony/ApnContext;->mDataProfile:Lcom/android/internal/telephony/DataProfile;

    #@2
    if-eqz v0, :cond_8

    #@4
    iget-object v0, p0, Lcom/android/internal/telephony/ApnContext;->mDataProfile:Lcom/android/internal/telephony/DataProfile;

    #@6
    iput-boolean p1, v0, Lcom/android/internal/telephony/DataProfile;->mTetheredCallOn:Z

    #@8
    .line 279
    :cond_8
    return-void
.end method

.method public declared-synchronized setWaitingApns(Ljava/util/ArrayList;)V
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/telephony/DataProfile;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 131
    .local p1, waitingApns:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/DataProfile;>;"
    monitor-enter p0

    #@1
    :try_start_1
    iput-object p1, p0, Lcom/android/internal/telephony/ApnContext;->mWaitingApns:Ljava/util/ArrayList;

    #@3
    .line 132
    iget-object v0, p0, Lcom/android/internal/telephony/ApnContext;->mWaitingApnsPermanentFailureCountDown:Ljava/util/concurrent/atomic/AtomicInteger;

    #@5
    iget-object v1, p0, Lcom/android/internal/telephony/ApnContext;->mWaitingApns:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@a
    move-result v1

    #@b
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V
    :try_end_e
    .catchall {:try_start_1 .. :try_end_e} :catchall_10

    #@e
    .line 133
    monitor-exit p0

    #@f
    return-void

    #@10
    .line 131
    :catchall_10
    move-exception v0

    #@11
    monitor-exit p0

    #@12
    throw v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 262
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "{mApnType="

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget-object v1, p0, Lcom/android/internal/telephony/ApnContext;->mApnType:Ljava/lang/String;

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, " mState="

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    invoke-virtual {p0}, Lcom/android/internal/telephony/ApnContext;->getState()Lcom/android/internal/telephony/DctConstants$State;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v0

    #@1f
    const-string v1, " mWaitingApns="

    #@21
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v0

    #@25
    iget-object v1, p0, Lcom/android/internal/telephony/ApnContext;->mWaitingApns:Ljava/util/ArrayList;

    #@27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v0

    #@2b
    const-string v1, " mWaitingApnsPermanentFailureCountDown="

    #@2d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v0

    #@31
    iget-object v1, p0, Lcom/android/internal/telephony/ApnContext;->mWaitingApnsPermanentFailureCountDown:Ljava/util/concurrent/atomic/AtomicInteger;

    #@33
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v0

    #@37
    const-string v1, " mDataProfile ="

    #@39
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v0

    #@3d
    iget-object v1, p0, Lcom/android/internal/telephony/ApnContext;->mDataProfile:Lcom/android/internal/telephony/DataProfile;

    #@3f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v0

    #@43
    const-string v1, " mDataConnectionAc="

    #@45
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v0

    #@49
    iget-object v1, p0, Lcom/android/internal/telephony/ApnContext;->mDataConnectionAc:Lcom/android/internal/telephony/DataConnectionAc;

    #@4b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v0

    #@4f
    const-string v1, " mReason="

    #@51
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v0

    #@55
    iget-object v1, p0, Lcom/android/internal/telephony/ApnContext;->mReason:Ljava/lang/String;

    #@57
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v0

    #@5b
    const-string v1, " mRetryCount="

    #@5d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v0

    #@61
    iget v1, p0, Lcom/android/internal/telephony/ApnContext;->mRetryCount:I

    #@63
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@66
    move-result-object v0

    #@67
    const-string v1, " mDataEnabled="

    #@69
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v0

    #@6d
    iget-object v1, p0, Lcom/android/internal/telephony/ApnContext;->mDataEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@6f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v0

    #@73
    const-string v1, " mDependencyMet="

    #@75
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v0

    #@79
    iget-object v1, p0, Lcom/android/internal/telephony/ApnContext;->mDependencyMet:Ljava/util/concurrent/atomic/AtomicBoolean;

    #@7b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v0

    #@7f
    const-string v1, "}"

    #@81
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v0

    #@85
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@88
    move-result-object v0

    #@89
    return-object v0
.end method
