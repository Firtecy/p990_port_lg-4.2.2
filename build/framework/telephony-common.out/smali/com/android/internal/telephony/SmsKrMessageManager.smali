.class public Lcom/android/internal/telephony/SmsKrMessageManager;
.super Ljava/lang/Object;
.source "SmsKrMessageManager.java"


# static fields
.field public static final SPAM_RESULT:Ljava/lang/String; = "spam_result"

.field private static sLoader:Ljava/lang/ClassLoader;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 19
    new-instance v0, Ldalvik/system/PathClassLoader;

    #@2
    const-string v1, "/system/framework/com.lge.krmessage.jar"

    #@4
    const-class v2, Lcom/android/internal/telephony/SmsKrMessageManager;

    #@6
    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    #@9
    move-result-object v2

    #@a
    invoke-direct {v0, v1, v2}, Ldalvik/system/PathClassLoader;-><init>(Ljava/lang/String;Ljava/lang/ClassLoader;)V

    #@d
    sput-object v0, Lcom/android/internal/telephony/SmsKrMessageManager;->sLoader:Ljava/lang/ClassLoader;

    #@f
    .line 21
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 13
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static getOperatorMessage(Ljava/lang/String;Lcom/android/internal/telephony/SMSDispatcher;Landroid/content/Context;Lcom/android/internal/telephony/SmsMessageBase;[[B)Lcom/android/internal/telephony/SmsOperatorBasicMessage;
    .registers 13
    .parameter "className"
    .parameter "dispatcher"
    .parameter "context"
    .parameter "sms"
    .parameter "pdus"

    #@0
    .prologue
    .line 26
    :try_start_0
    const-string v5, "Gsm"

    #@2
    invoke-virtual {p0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@5
    move-result v5

    #@6
    if-eqz v5, :cond_53

    #@8
    .line 27
    new-instance v5, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v6, "com.android.internal.telephony.gsm."

    #@f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v5

    #@13
    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v5

    #@17
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v5

    #@1b
    const/4 v6, 0x1

    #@1c
    sget-object v7, Lcom/android/internal/telephony/SmsKrMessageManager;->sLoader:Ljava/lang/ClassLoader;

    #@1e
    invoke-static {v5, v6, v7}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    #@21
    move-result-object v1

    #@22
    .line 31
    .local v1, c:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    :goto_22
    const/4 v5, 0x4

    #@23
    new-array v4, v5, [Ljava/lang/Class;

    #@25
    const/4 v5, 0x0

    #@26
    const-class v6, Lcom/android/internal/telephony/SMSDispatcher;

    #@28
    aput-object v6, v4, v5

    #@2a
    const/4 v5, 0x1

    #@2b
    const-class v6, Landroid/content/Context;

    #@2d
    aput-object v6, v4, v5

    #@2f
    const/4 v5, 0x2

    #@30
    const-class v6, Lcom/android/internal/telephony/SmsMessageBase;

    #@32
    aput-object v6, v4, v5

    #@34
    const/4 v5, 0x3

    #@35
    const-class v6, [[B

    #@37
    aput-object v6, v4, v5

    #@39
    .line 32
    .local v4, paramTypes:[Ljava/lang/Class;,"[Ljava/lang/Class<*>;"
    invoke-virtual {v1, v4}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    #@3c
    move-result-object v2

    #@3d
    .line 33
    .local v2, constructor:Ljava/lang/reflect/Constructor;,"Ljava/lang/reflect/Constructor<*>;"
    const/4 v5, 0x4

    #@3e
    new-array v0, v5, [Ljava/lang/Object;

    #@40
    const/4 v5, 0x0

    #@41
    aput-object p1, v0, v5

    #@43
    const/4 v5, 0x1

    #@44
    aput-object p2, v0, v5

    #@46
    const/4 v5, 0x2

    #@47
    aput-object p3, v0, v5

    #@49
    const/4 v5, 0x3

    #@4a
    aput-object p4, v0, v5

    #@4c
    .line 34
    .local v0, args:[Ljava/lang/Object;
    invoke-virtual {v2, v0}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    #@4f
    move-result-object v5

    #@50
    check-cast v5, Lcom/android/internal/telephony/SmsOperatorBasicMessage;

    #@52
    .line 48
    .end local v0           #args:[Ljava/lang/Object;
    .end local v1           #c:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    .end local v2           #constructor:Ljava/lang/reflect/Constructor;,"Ljava/lang/reflect/Constructor<*>;"
    .end local v4           #paramTypes:[Ljava/lang/Class;,"[Ljava/lang/Class<*>;"
    :goto_52
    return-object v5

    #@53
    .line 29
    :cond_53
    new-instance v5, Ljava/lang/StringBuilder;

    #@55
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@58
    const-string v6, "com.android.internal.telephony.cdma."

    #@5a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v5

    #@5e
    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v5

    #@62
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@65
    move-result-object v5

    #@66
    const/4 v6, 0x1

    #@67
    sget-object v7, Lcom/android/internal/telephony/SmsKrMessageManager;->sLoader:Ljava/lang/ClassLoader;

    #@69
    invoke-static {v5, v6, v7}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;
    :try_end_6c
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_6c} :catch_6e
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_6c} :catch_74
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_6c} :catch_79
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_6c} :catch_7e
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_6c} :catch_83
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_6c} :catch_88

    #@6c
    move-result-object v1

    #@6d
    .restart local v1       #c:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    goto :goto_22

    #@6e
    .line 35
    .end local v1           #c:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    :catch_6e
    move-exception v3

    #@6f
    .line 36
    .local v3, e:Ljava/lang/ClassNotFoundException;
    invoke-virtual {v3}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    #@72
    .line 48
    .end local v3           #e:Ljava/lang/ClassNotFoundException;
    :goto_72
    const/4 v5, 0x0

    #@73
    goto :goto_52

    #@74
    .line 37
    :catch_74
    move-exception v3

    #@75
    .line 38
    .local v3, e:Ljava/lang/NoSuchMethodException;
    invoke-virtual {v3}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    #@78
    goto :goto_72

    #@79
    .line 39
    .end local v3           #e:Ljava/lang/NoSuchMethodException;
    :catch_79
    move-exception v3

    #@7a
    .line 40
    .local v3, e:Ljava/lang/IllegalArgumentException;
    invoke-virtual {v3}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    #@7d
    goto :goto_72

    #@7e
    .line 41
    .end local v3           #e:Ljava/lang/IllegalArgumentException;
    :catch_7e
    move-exception v3

    #@7f
    .line 42
    .local v3, e:Ljava/lang/InstantiationException;
    invoke-virtual {v3}, Ljava/lang/InstantiationException;->printStackTrace()V

    #@82
    goto :goto_72

    #@83
    .line 43
    .end local v3           #e:Ljava/lang/InstantiationException;
    :catch_83
    move-exception v3

    #@84
    .line 44
    .local v3, e:Ljava/lang/IllegalAccessException;
    invoke-virtual {v3}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    #@87
    goto :goto_72

    #@88
    .line 45
    .end local v3           #e:Ljava/lang/IllegalAccessException;
    :catch_88
    move-exception v3

    #@89
    .line 46
    .local v3, e:Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v3}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    #@8c
    goto :goto_72
.end method

.method public static getOperatorMessage(Ljava/lang/String;Lcom/android/internal/telephony/SMSDispatcher;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Lcom/android/internal/telephony/SmsOperatorBasicMessage;
    .registers 13
    .parameter "className"
    .parameter "dispatcher"
    .parameter "content"
    .parameter "recipient"
    .parameter "context"

    #@0
    .prologue
    .line 54
    :try_start_0
    const-string v5, "Gsm"

    #@2
    invoke-virtual {p0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@5
    move-result v5

    #@6
    if-eqz v5, :cond_53

    #@8
    .line 55
    new-instance v5, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v6, "com.android.internal.telephony.gsm."

    #@f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v5

    #@13
    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v5

    #@17
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v5

    #@1b
    const/4 v6, 0x1

    #@1c
    sget-object v7, Lcom/android/internal/telephony/SmsKrMessageManager;->sLoader:Ljava/lang/ClassLoader;

    #@1e
    invoke-static {v5, v6, v7}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    #@21
    move-result-object v1

    #@22
    .line 59
    .local v1, c:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    :goto_22
    const/4 v5, 0x4

    #@23
    new-array v4, v5, [Ljava/lang/Class;

    #@25
    const/4 v5, 0x0

    #@26
    const-class v6, Lcom/android/internal/telephony/SMSDispatcher;

    #@28
    aput-object v6, v4, v5

    #@2a
    const/4 v5, 0x1

    #@2b
    const-class v6, Ljava/lang/String;

    #@2d
    aput-object v6, v4, v5

    #@2f
    const/4 v5, 0x2

    #@30
    const-class v6, Ljava/lang/String;

    #@32
    aput-object v6, v4, v5

    #@34
    const/4 v5, 0x3

    #@35
    const-class v6, Landroid/content/Context;

    #@37
    aput-object v6, v4, v5

    #@39
    .line 60
    .local v4, paramTypes:[Ljava/lang/Class;,"[Ljava/lang/Class<*>;"
    invoke-virtual {v1, v4}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    #@3c
    move-result-object v2

    #@3d
    .line 61
    .local v2, constructor:Ljava/lang/reflect/Constructor;,"Ljava/lang/reflect/Constructor<*>;"
    const/4 v5, 0x4

    #@3e
    new-array v0, v5, [Ljava/lang/Object;

    #@40
    const/4 v5, 0x0

    #@41
    aput-object p1, v0, v5

    #@43
    const/4 v5, 0x1

    #@44
    aput-object p2, v0, v5

    #@46
    const/4 v5, 0x2

    #@47
    aput-object p3, v0, v5

    #@49
    const/4 v5, 0x3

    #@4a
    aput-object p4, v0, v5

    #@4c
    .line 62
    .local v0, args:[Ljava/lang/Object;
    invoke-virtual {v2, v0}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    #@4f
    move-result-object v5

    #@50
    check-cast v5, Lcom/android/internal/telephony/SmsOperatorBasicMessage;

    #@52
    .line 76
    .end local v0           #args:[Ljava/lang/Object;
    .end local v1           #c:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    .end local v2           #constructor:Ljava/lang/reflect/Constructor;,"Ljava/lang/reflect/Constructor<*>;"
    .end local v4           #paramTypes:[Ljava/lang/Class;,"[Ljava/lang/Class<*>;"
    :goto_52
    return-object v5

    #@53
    .line 57
    :cond_53
    new-instance v5, Ljava/lang/StringBuilder;

    #@55
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@58
    const-string v6, "com.android.internal.telephony.cdma."

    #@5a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v5

    #@5e
    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v5

    #@62
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@65
    move-result-object v5

    #@66
    const/4 v6, 0x1

    #@67
    sget-object v7, Lcom/android/internal/telephony/SmsKrMessageManager;->sLoader:Ljava/lang/ClassLoader;

    #@69
    invoke-static {v5, v6, v7}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;
    :try_end_6c
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_6c} :catch_6e
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_6c} :catch_74
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_6c} :catch_79
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_6c} :catch_7e
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_6c} :catch_83
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_6c} :catch_88

    #@6c
    move-result-object v1

    #@6d
    .restart local v1       #c:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    goto :goto_22

    #@6e
    .line 63
    .end local v1           #c:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    :catch_6e
    move-exception v3

    #@6f
    .line 64
    .local v3, e:Ljava/lang/ClassNotFoundException;
    invoke-virtual {v3}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    #@72
    .line 76
    .end local v3           #e:Ljava/lang/ClassNotFoundException;
    :goto_72
    const/4 v5, 0x0

    #@73
    goto :goto_52

    #@74
    .line 65
    :catch_74
    move-exception v3

    #@75
    .line 66
    .local v3, e:Ljava/lang/NoSuchMethodException;
    invoke-virtual {v3}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    #@78
    goto :goto_72

    #@79
    .line 67
    .end local v3           #e:Ljava/lang/NoSuchMethodException;
    :catch_79
    move-exception v3

    #@7a
    .line 68
    .local v3, e:Ljava/lang/IllegalArgumentException;
    invoke-virtual {v3}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    #@7d
    goto :goto_72

    #@7e
    .line 69
    .end local v3           #e:Ljava/lang/IllegalArgumentException;
    :catch_7e
    move-exception v3

    #@7f
    .line 70
    .local v3, e:Ljava/lang/InstantiationException;
    invoke-virtual {v3}, Ljava/lang/InstantiationException;->printStackTrace()V

    #@82
    goto :goto_72

    #@83
    .line 71
    .end local v3           #e:Ljava/lang/InstantiationException;
    :catch_83
    move-exception v3

    #@84
    .line 72
    .local v3, e:Ljava/lang/IllegalAccessException;
    invoke-virtual {v3}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    #@87
    goto :goto_72

    #@88
    .line 73
    .end local v3           #e:Ljava/lang/IllegalAccessException;
    :catch_88
    move-exception v3

    #@89
    .line 74
    .local v3, e:Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v3}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    #@8c
    goto :goto_72
.end method

.method public static getOperatorMessage(Ljava/lang/String;[BLandroid/content/Context;)Lcom/android/internal/telephony/SmsOperatorBasicMessage;
    .registers 11
    .parameter "className"
    .parameter "pdus"
    .parameter "context"

    #@0
    .prologue
    .line 86
    :try_start_0
    const-string v5, "Gsm"

    #@2
    invoke-virtual {p0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@5
    move-result v5

    #@6
    if-eqz v5, :cond_43

    #@8
    .line 87
    new-instance v5, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v6, "com.android.internal.telephony.gsm."

    #@f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v5

    #@13
    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v5

    #@17
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v5

    #@1b
    const/4 v6, 0x1

    #@1c
    sget-object v7, Lcom/android/internal/telephony/SmsKrMessageManager;->sLoader:Ljava/lang/ClassLoader;

    #@1e
    invoke-static {v5, v6, v7}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    #@21
    move-result-object v1

    #@22
    .line 91
    .local v1, c:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    :goto_22
    const/4 v5, 0x2

    #@23
    new-array v4, v5, [Ljava/lang/Class;

    #@25
    const/4 v5, 0x0

    #@26
    const-class v6, [B

    #@28
    aput-object v6, v4, v5

    #@2a
    const/4 v5, 0x1

    #@2b
    const-class v6, Landroid/content/Context;

    #@2d
    aput-object v6, v4, v5

    #@2f
    .line 92
    .local v4, paramTypes:[Ljava/lang/Class;,"[Ljava/lang/Class<*>;"
    invoke-virtual {v1, v4}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    #@32
    move-result-object v2

    #@33
    .line 93
    .local v2, constructor:Ljava/lang/reflect/Constructor;,"Ljava/lang/reflect/Constructor<*>;"
    const/4 v5, 0x2

    #@34
    new-array v0, v5, [Ljava/lang/Object;

    #@36
    const/4 v5, 0x0

    #@37
    aput-object p1, v0, v5

    #@39
    const/4 v5, 0x1

    #@3a
    aput-object p2, v0, v5

    #@3c
    .line 94
    .local v0, args:[Ljava/lang/Object;
    invoke-virtual {v2, v0}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    #@3f
    move-result-object v5

    #@40
    check-cast v5, Lcom/android/internal/telephony/SmsOperatorBasicMessage;

    #@42
    .line 108
    .end local v0           #args:[Ljava/lang/Object;
    .end local v1           #c:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    .end local v2           #constructor:Ljava/lang/reflect/Constructor;,"Ljava/lang/reflect/Constructor<*>;"
    .end local v4           #paramTypes:[Ljava/lang/Class;,"[Ljava/lang/Class<*>;"
    :goto_42
    return-object v5

    #@43
    .line 89
    :cond_43
    new-instance v5, Ljava/lang/StringBuilder;

    #@45
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@48
    const-string v6, "com.android.internal.telephony.cdma."

    #@4a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v5

    #@4e
    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v5

    #@52
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@55
    move-result-object v5

    #@56
    const/4 v6, 0x1

    #@57
    sget-object v7, Lcom/android/internal/telephony/SmsKrMessageManager;->sLoader:Ljava/lang/ClassLoader;

    #@59
    invoke-static {v5, v6, v7}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;
    :try_end_5c
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_5c} :catch_5e
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_5c} :catch_64
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_5c} :catch_69
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_5c} :catch_6e
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_5c} :catch_73
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_5c} :catch_78

    #@5c
    move-result-object v1

    #@5d
    .restart local v1       #c:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    goto :goto_22

    #@5e
    .line 95
    .end local v1           #c:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    :catch_5e
    move-exception v3

    #@5f
    .line 96
    .local v3, e:Ljava/lang/ClassNotFoundException;
    invoke-virtual {v3}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    #@62
    .line 108
    .end local v3           #e:Ljava/lang/ClassNotFoundException;
    :goto_62
    const/4 v5, 0x0

    #@63
    goto :goto_42

    #@64
    .line 97
    :catch_64
    move-exception v3

    #@65
    .line 98
    .local v3, e:Ljava/lang/NoSuchMethodException;
    invoke-virtual {v3}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    #@68
    goto :goto_62

    #@69
    .line 99
    .end local v3           #e:Ljava/lang/NoSuchMethodException;
    :catch_69
    move-exception v3

    #@6a
    .line 100
    .local v3, e:Ljava/lang/IllegalArgumentException;
    invoke-virtual {v3}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    #@6d
    goto :goto_62

    #@6e
    .line 101
    .end local v3           #e:Ljava/lang/IllegalArgumentException;
    :catch_6e
    move-exception v3

    #@6f
    .line 102
    .local v3, e:Ljava/lang/InstantiationException;
    invoke-virtual {v3}, Ljava/lang/InstantiationException;->printStackTrace()V

    #@72
    goto :goto_62

    #@73
    .line 103
    .end local v3           #e:Ljava/lang/InstantiationException;
    :catch_73
    move-exception v3

    #@74
    .line 104
    .local v3, e:Ljava/lang/IllegalAccessException;
    invoke-virtual {v3}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    #@77
    goto :goto_62

    #@78
    .line 105
    .end local v3           #e:Ljava/lang/IllegalAccessException;
    :catch_78
    move-exception v3

    #@79
    .line 106
    .local v3, e:Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v3}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    #@7c
    goto :goto_62
.end method

.method public static getOperatorSpamMessage(Lcom/android/internal/telephony/SMSDispatcher;Landroid/content/Context;Lcom/android/internal/telephony/SmsMessageBase;[[B)Lcom/android/internal/telephony/SmsOperatorBasicMessage;
    .registers 7
    .parameter "dispatcher"
    .parameter "context"
    .parameter "sms"
    .parameter "pdus"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    .line 113
    const-string v0, "SKTspam"

    #@4
    invoke-static {v2, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@7
    move-result v0

    #@8
    if-ne v0, v1, :cond_11

    #@a
    .line 114
    const-string v0, "GsmSmsSKTSpamMessage"

    #@c
    invoke-static {v0, p0, p1, p2, p3}, Lcom/android/internal/telephony/SmsKrMessageManager;->getOperatorMessage(Ljava/lang/String;Lcom/android/internal/telephony/SMSDispatcher;Landroid/content/Context;Lcom/android/internal/telephony/SmsMessageBase;[[B)Lcom/android/internal/telephony/SmsOperatorBasicMessage;

    #@f
    move-result-object v0

    #@10
    .line 122
    :goto_10
    return-object v0

    #@11
    .line 115
    :cond_11
    const-string v0, "LGUspam"

    #@13
    invoke-static {v2, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@16
    move-result v0

    #@17
    if-ne v0, v1, :cond_20

    #@19
    .line 116
    const-string v0, "CdmaSmsLgtSpamMessage"

    #@1b
    invoke-static {v0, p0, p1, p2, p3}, Lcom/android/internal/telephony/SmsKrMessageManager;->getOperatorMessage(Ljava/lang/String;Lcom/android/internal/telephony/SMSDispatcher;Landroid/content/Context;Lcom/android/internal/telephony/SmsMessageBase;[[B)Lcom/android/internal/telephony/SmsOperatorBasicMessage;

    #@1e
    move-result-object v0

    #@1f
    goto :goto_10

    #@20
    .line 117
    :cond_20
    const-string v0, "KTspam"

    #@22
    invoke-static {v2, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@25
    move-result v0

    #@26
    if-ne v0, v1, :cond_2f

    #@28
    .line 118
    const-string v0, "GsmSmsKTSpamMessage"

    #@2a
    invoke-static {v0, p0, p1, p2, p3}, Lcom/android/internal/telephony/SmsKrMessageManager;->getOperatorMessage(Ljava/lang/String;Lcom/android/internal/telephony/SMSDispatcher;Landroid/content/Context;Lcom/android/internal/telephony/SmsMessageBase;[[B)Lcom/android/internal/telephony/SmsOperatorBasicMessage;

    #@2d
    move-result-object v0

    #@2e
    goto :goto_10

    #@2f
    .line 119
    :cond_2f
    const-string v0, "GLOBALspam"

    #@31
    invoke-static {v2, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@34
    move-result v0

    #@35
    if-ne v0, v1, :cond_3e

    #@37
    .line 120
    const-string v0, "GsmSmsGlobalSpamMessage"

    #@39
    invoke-static {v0, p0, p1, p2, p3}, Lcom/android/internal/telephony/SmsKrMessageManager;->getOperatorMessage(Ljava/lang/String;Lcom/android/internal/telephony/SMSDispatcher;Landroid/content/Context;Lcom/android/internal/telephony/SmsMessageBase;[[B)Lcom/android/internal/telephony/SmsOperatorBasicMessage;

    #@3c
    move-result-object v0

    #@3d
    goto :goto_10

    #@3e
    .line 122
    :cond_3e
    const-string v0, "GsmSmsKTSpamMessage"

    #@40
    invoke-static {v0, p0, p1, p2, p3}, Lcom/android/internal/telephony/SmsKrMessageManager;->getOperatorMessage(Ljava/lang/String;Lcom/android/internal/telephony/SMSDispatcher;Landroid/content/Context;Lcom/android/internal/telephony/SmsMessageBase;[[B)Lcom/android/internal/telephony/SmsOperatorBasicMessage;

    #@43
    move-result-object v0

    #@44
    goto :goto_10
.end method

.method public static getOperatorSpamMessage(Lcom/android/internal/telephony/SMSDispatcher;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Lcom/android/internal/telephony/SmsOperatorBasicMessage;
    .registers 7
    .parameter "dispatcher"
    .parameter "content"
    .parameter "recipient"
    .parameter "context"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    .line 127
    const-string v0, "SKTspam"

    #@4
    invoke-static {v2, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@7
    move-result v0

    #@8
    if-ne v0, v1, :cond_11

    #@a
    .line 128
    const-string v0, "GsmSmsSKTSpamMessage"

    #@c
    invoke-static {v0, p0, p1, p2, p3}, Lcom/android/internal/telephony/SmsKrMessageManager;->getOperatorMessage(Ljava/lang/String;Lcom/android/internal/telephony/SMSDispatcher;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Lcom/android/internal/telephony/SmsOperatorBasicMessage;

    #@f
    move-result-object v0

    #@10
    .line 136
    :goto_10
    return-object v0

    #@11
    .line 129
    :cond_11
    const-string v0, "LGUspam"

    #@13
    invoke-static {v2, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@16
    move-result v0

    #@17
    if-ne v0, v1, :cond_20

    #@19
    .line 130
    const-string v0, "CdmaSmsLgtSpamMessage"

    #@1b
    invoke-static {v0, p0, p1, p2, p3}, Lcom/android/internal/telephony/SmsKrMessageManager;->getOperatorMessage(Ljava/lang/String;Lcom/android/internal/telephony/SMSDispatcher;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Lcom/android/internal/telephony/SmsOperatorBasicMessage;

    #@1e
    move-result-object v0

    #@1f
    goto :goto_10

    #@20
    .line 131
    :cond_20
    const-string v0, "KTspam"

    #@22
    invoke-static {v2, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@25
    move-result v0

    #@26
    if-ne v0, v1, :cond_2f

    #@28
    .line 132
    const-string v0, "GsmSmsKTSpamMessage"

    #@2a
    invoke-static {v0, p0, p1, p2, p3}, Lcom/android/internal/telephony/SmsKrMessageManager;->getOperatorMessage(Ljava/lang/String;Lcom/android/internal/telephony/SMSDispatcher;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Lcom/android/internal/telephony/SmsOperatorBasicMessage;

    #@2d
    move-result-object v0

    #@2e
    goto :goto_10

    #@2f
    .line 133
    :cond_2f
    const-string v0, "GLOBALspam"

    #@31
    invoke-static {v2, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@34
    move-result v0

    #@35
    if-ne v0, v1, :cond_3e

    #@37
    .line 134
    const-string v0, "GsmSmsGlobalSpamMessage"

    #@39
    invoke-static {v0, p0, p1, p2, p3}, Lcom/android/internal/telephony/SmsKrMessageManager;->getOperatorMessage(Ljava/lang/String;Lcom/android/internal/telephony/SMSDispatcher;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Lcom/android/internal/telephony/SmsOperatorBasicMessage;

    #@3c
    move-result-object v0

    #@3d
    goto :goto_10

    #@3e
    .line 136
    :cond_3e
    const-string v0, "GsmSmsKTSpamMessage"

    #@40
    invoke-static {v0, p0, p1, p2, p3}, Lcom/android/internal/telephony/SmsKrMessageManager;->getOperatorMessage(Ljava/lang/String;Lcom/android/internal/telephony/SMSDispatcher;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Lcom/android/internal/telephony/SmsOperatorBasicMessage;

    #@43
    move-result-object v0

    #@44
    goto :goto_10
.end method

.method public static getOperatorSpamMessage([BLandroid/content/Context;)Lcom/android/internal/telephony/SmsOperatorBasicMessage;
    .registers 5
    .parameter "pdus"
    .parameter "context"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    .line 150
    const-string v0, "SKTspam"

    #@4
    invoke-static {v2, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@7
    move-result v0

    #@8
    if-ne v0, v1, :cond_11

    #@a
    .line 151
    const-string v0, "GsmSmsSKTSpamMessage"

    #@c
    invoke-static {v0, p0, p1}, Lcom/android/internal/telephony/SmsKrMessageManager;->getOperatorMessage(Ljava/lang/String;[BLandroid/content/Context;)Lcom/android/internal/telephony/SmsOperatorBasicMessage;

    #@f
    move-result-object v0

    #@10
    .line 159
    :goto_10
    return-object v0

    #@11
    .line 152
    :cond_11
    const-string v0, "LGUspam"

    #@13
    invoke-static {v2, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@16
    move-result v0

    #@17
    if-ne v0, v1, :cond_20

    #@19
    .line 153
    const-string v0, "CdmaSmsLgtSpamMessage"

    #@1b
    invoke-static {v0, p0, p1}, Lcom/android/internal/telephony/SmsKrMessageManager;->getOperatorMessage(Ljava/lang/String;[BLandroid/content/Context;)Lcom/android/internal/telephony/SmsOperatorBasicMessage;

    #@1e
    move-result-object v0

    #@1f
    goto :goto_10

    #@20
    .line 154
    :cond_20
    const-string v0, "KTspam"

    #@22
    invoke-static {v2, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@25
    move-result v0

    #@26
    if-ne v0, v1, :cond_2f

    #@28
    .line 155
    const-string v0, "GsmSmsKTSpamMessage"

    #@2a
    invoke-static {v0, p0, p1}, Lcom/android/internal/telephony/SmsKrMessageManager;->getOperatorMessage(Ljava/lang/String;[BLandroid/content/Context;)Lcom/android/internal/telephony/SmsOperatorBasicMessage;

    #@2d
    move-result-object v0

    #@2e
    goto :goto_10

    #@2f
    .line 156
    :cond_2f
    const-string v0, "GLOBALspam"

    #@31
    invoke-static {v2, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@34
    move-result v0

    #@35
    if-ne v0, v1, :cond_3e

    #@37
    .line 157
    const-string v0, "GsmSmsGlobalSpamMessage"

    #@39
    invoke-static {v0, p0, p1}, Lcom/android/internal/telephony/SmsKrMessageManager;->getOperatorMessage(Ljava/lang/String;[BLandroid/content/Context;)Lcom/android/internal/telephony/SmsOperatorBasicMessage;

    #@3c
    move-result-object v0

    #@3d
    goto :goto_10

    #@3e
    .line 159
    :cond_3e
    const-string v0, "GsmSmsKTSpamMessage"

    #@40
    invoke-static {v0, p0, p1}, Lcom/android/internal/telephony/SmsKrMessageManager;->getOperatorMessage(Ljava/lang/String;[BLandroid/content/Context;)Lcom/android/internal/telephony/SmsOperatorBasicMessage;

    #@43
    move-result-object v0

    #@44
    goto :goto_10
.end method
