.class public final enum Lcom/android/internal/telephony/LGDataRecovery$LinkState;
.super Ljava/lang/Enum;
.source "LGDataRecovery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/LGDataRecovery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "LinkState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/internal/telephony/LGDataRecovery$LinkState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/internal/telephony/LGDataRecovery$LinkState;

.field public static final enum LINKDOWN:Lcom/android/internal/telephony/LGDataRecovery$LinkState;

.field public static final enum LINKUP:Lcom/android/internal/telephony/LGDataRecovery$LinkState;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 163
    new-instance v0, Lcom/android/internal/telephony/LGDataRecovery$LinkState;

    #@4
    const-string v1, "LINKDOWN"

    #@6
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/LGDataRecovery$LinkState;-><init>(Ljava/lang/String;I)V

    #@9
    sput-object v0, Lcom/android/internal/telephony/LGDataRecovery$LinkState;->LINKDOWN:Lcom/android/internal/telephony/LGDataRecovery$LinkState;

    #@b
    .line 164
    new-instance v0, Lcom/android/internal/telephony/LGDataRecovery$LinkState;

    #@d
    const-string v1, "LINKUP"

    #@f
    invoke-direct {v0, v1, v3}, Lcom/android/internal/telephony/LGDataRecovery$LinkState;-><init>(Ljava/lang/String;I)V

    #@12
    sput-object v0, Lcom/android/internal/telephony/LGDataRecovery$LinkState;->LINKUP:Lcom/android/internal/telephony/LGDataRecovery$LinkState;

    #@14
    .line 162
    const/4 v0, 0x2

    #@15
    new-array v0, v0, [Lcom/android/internal/telephony/LGDataRecovery$LinkState;

    #@17
    sget-object v1, Lcom/android/internal/telephony/LGDataRecovery$LinkState;->LINKDOWN:Lcom/android/internal/telephony/LGDataRecovery$LinkState;

    #@19
    aput-object v1, v0, v2

    #@1b
    sget-object v1, Lcom/android/internal/telephony/LGDataRecovery$LinkState;->LINKUP:Lcom/android/internal/telephony/LGDataRecovery$LinkState;

    #@1d
    aput-object v1, v0, v3

    #@1f
    sput-object v0, Lcom/android/internal/telephony/LGDataRecovery$LinkState;->$VALUES:[Lcom/android/internal/telephony/LGDataRecovery$LinkState;

    #@21
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 162
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/internal/telephony/LGDataRecovery$LinkState;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 162
    const-class v0, Lcom/android/internal/telephony/LGDataRecovery$LinkState;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/telephony/LGDataRecovery$LinkState;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/android/internal/telephony/LGDataRecovery$LinkState;
    .registers 1

    #@0
    .prologue
    .line 162
    sget-object v0, Lcom/android/internal/telephony/LGDataRecovery$LinkState;->$VALUES:[Lcom/android/internal/telephony/LGDataRecovery$LinkState;

    #@2
    invoke-virtual {v0}, [Lcom/android/internal/telephony/LGDataRecovery$LinkState;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/android/internal/telephony/LGDataRecovery$LinkState;

    #@8
    return-object v0
.end method
