.class public interface abstract Lcom/android/internal/telephony/IccPhonebookProvider$Contract$InfoGlobal$Column;
.super Ljava/lang/Object;
.source "IccPhonebookProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/IccPhonebookProvider$Contract$InfoGlobal;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Column"
.end annotation


# static fields
.field public static final ADN_MAX_LENGTH:Ljava/lang/String;

.field public static final ADN_TAG_ENCODING:Ljava/lang/String;

.field public static final ADN_TAG_MAX_BYTE_LENGTH:Ljava/lang/String;

.field public static final ANR_MAX_LENGTH:Ljava/lang/String;

.field public static final EMAIL_ENCODING:Ljava/lang/String;

.field public static final EMAIL_MAX_BYTE_LENGTH:Ljava/lang/String;

.field public static final EXT1_FREE_COUNT:Ljava/lang/String;

.field public static final EXT1_MAX_LENGTH:Ljava/lang/String;

.field public static final FEATURES:Ljava/lang/String;

.field public static final GROUP_CAPACITY:Ljava/lang/String;

.field public static final GROUP_NAME_ENCODING:Ljava/lang/String;

.field public static final GROUP_NAME_MAX_BYTE_LENGTH:Ljava/lang/String;

.field public static final MAX_ASSIGNABLE_ANR_COUNT:Ljava/lang/String;

.field public static final MAX_ASSIGNABLE_EMAIL_COUNT:Ljava/lang/String;

.field public static final MAX_ASSIGNABLE_GROUP_COUNT:Ljava/lang/String;

.field public static final SLICE_COUNT:Ljava/lang/String;

.field public static final SLOT_COUNT:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    const/16 v2, 0xf

    #@2
    .line 85
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->INFO_PROJECTION:[Ljava/lang/String;

    #@4
    const/4 v1, 0x0

    #@5
    aget-object v0, v0, v1

    #@7
    sput-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract$InfoGlobal$Column;->SLICE_COUNT:Ljava/lang/String;

    #@9
    .line 86
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->INFO_PROJECTION:[Ljava/lang/String;

    #@b
    const/4 v1, 0x1

    #@c
    aget-object v0, v0, v1

    #@e
    sput-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract$InfoGlobal$Column;->ADN_TAG_ENCODING:Ljava/lang/String;

    #@10
    .line 87
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->INFO_PROJECTION:[Ljava/lang/String;

    #@12
    const/4 v1, 0x2

    #@13
    aget-object v0, v0, v1

    #@15
    sput-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract$InfoGlobal$Column;->ADN_TAG_MAX_BYTE_LENGTH:Ljava/lang/String;

    #@17
    .line 88
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->INFO_PROJECTION:[Ljava/lang/String;

    #@19
    const/4 v1, 0x3

    #@1a
    aget-object v0, v0, v1

    #@1c
    sput-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract$InfoGlobal$Column;->ADN_MAX_LENGTH:Ljava/lang/String;

    #@1e
    .line 89
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->INFO_PROJECTION:[Ljava/lang/String;

    #@20
    const/4 v1, 0x4

    #@21
    aget-object v0, v0, v1

    #@23
    sput-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract$InfoGlobal$Column;->ANR_MAX_LENGTH:Ljava/lang/String;

    #@25
    .line 90
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->INFO_PROJECTION:[Ljava/lang/String;

    #@27
    const/4 v1, 0x5

    #@28
    aget-object v0, v0, v1

    #@2a
    sput-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract$InfoGlobal$Column;->EMAIL_ENCODING:Ljava/lang/String;

    #@2c
    .line 91
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->INFO_PROJECTION:[Ljava/lang/String;

    #@2e
    const/4 v1, 0x6

    #@2f
    aget-object v0, v0, v1

    #@31
    sput-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract$InfoGlobal$Column;->EMAIL_MAX_BYTE_LENGTH:Ljava/lang/String;

    #@33
    .line 92
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->INFO_PROJECTION:[Ljava/lang/String;

    #@35
    const/4 v1, 0x7

    #@36
    aget-object v0, v0, v1

    #@38
    sput-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract$InfoGlobal$Column;->EXT1_FREE_COUNT:Ljava/lang/String;

    #@3a
    .line 93
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->INFO_PROJECTION:[Ljava/lang/String;

    #@3c
    const/16 v1, 0x8

    #@3e
    aget-object v0, v0, v1

    #@40
    sput-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract$InfoGlobal$Column;->EXT1_MAX_LENGTH:Ljava/lang/String;

    #@42
    .line 94
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->INFO_PROJECTION:[Ljava/lang/String;

    #@44
    const/16 v1, 0x9

    #@46
    aget-object v0, v0, v1

    #@48
    sput-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract$InfoGlobal$Column;->GROUP_CAPACITY:Ljava/lang/String;

    #@4a
    .line 95
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->INFO_PROJECTION:[Ljava/lang/String;

    #@4c
    const/16 v1, 0xa

    #@4e
    aget-object v0, v0, v1

    #@50
    sput-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract$InfoGlobal$Column;->GROUP_NAME_ENCODING:Ljava/lang/String;

    #@52
    .line 96
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->INFO_PROJECTION:[Ljava/lang/String;

    #@54
    const/16 v1, 0xb

    #@56
    aget-object v0, v0, v1

    #@58
    sput-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract$InfoGlobal$Column;->GROUP_NAME_MAX_BYTE_LENGTH:Ljava/lang/String;

    #@5a
    .line 97
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->INFO_PROJECTION:[Ljava/lang/String;

    #@5c
    const/16 v1, 0xc

    #@5e
    aget-object v0, v0, v1

    #@60
    sput-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract$InfoGlobal$Column;->MAX_ASSIGNABLE_ANR_COUNT:Ljava/lang/String;

    #@62
    .line 98
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->INFO_PROJECTION:[Ljava/lang/String;

    #@64
    const/16 v1, 0xd

    #@66
    aget-object v0, v0, v1

    #@68
    sput-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract$InfoGlobal$Column;->MAX_ASSIGNABLE_EMAIL_COUNT:Ljava/lang/String;

    #@6a
    .line 99
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->INFO_PROJECTION:[Ljava/lang/String;

    #@6c
    const/16 v1, 0xe

    #@6e
    aget-object v0, v0, v1

    #@70
    sput-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract$InfoGlobal$Column;->MAX_ASSIGNABLE_GROUP_COUNT:Ljava/lang/String;

    #@72
    .line 100
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->INFO_PROJECTION:[Ljava/lang/String;

    #@74
    aget-object v0, v0, v2

    #@76
    sput-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract$InfoGlobal$Column;->FEATURES:Ljava/lang/String;

    #@78
    .line 101
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->INFO_PROJECTION:[Ljava/lang/String;

    #@7a
    aget-object v0, v0, v2

    #@7c
    sput-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract$InfoGlobal$Column;->SLOT_COUNT:Ljava/lang/String;

    #@7e
    return-void
.end method
