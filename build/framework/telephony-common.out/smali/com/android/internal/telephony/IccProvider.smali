.class public Lcom/android/internal/telephony/IccProvider;
.super Landroid/content/ContentProvider;
.source "IccProvider.java"


# static fields
.field protected static final ADDRESS_BOOK_COLUMN_NAMES:[Ljava/lang/String; = null

.field private static final ADN:I = 0x1

.field private static final DBG:Z = false

.field private static ENABLE_PRIVACY_LOG:Z = false

.field private static final FDN:I = 0x2

.field private static final SDN:I = 0x3

.field protected static final STR_EMAILS:Ljava/lang/String; = "emails"

.field protected static final STR_NUMBER:Ljava/lang/String; = "number"

.field protected static final STR_PIN2:Ljava/lang/String; = "pin2"

.field protected static final STR_TAG:Ljava/lang/String; = "tag"

.field private static final TAG:Ljava/lang/String; = "IccProvider"

.field private static final URL_MATCHER:Landroid/content/UriMatcher;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    #@0
    .prologue
    const/4 v6, 0x3

    #@1
    const/4 v5, 0x2

    #@2
    const/4 v1, 0x0

    #@3
    const/4 v2, 0x1

    #@4
    .line 46
    const-string v3, "persist.service.privacy.enable"

    #@6
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    const-string v4, "ATT"

    #@c
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f
    move-result v0

    #@10
    if-nez v0, :cond_1e

    #@12
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@15
    move-result-object v0

    #@16
    const-string v4, "TMO"

    #@18
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1b
    move-result v0

    #@1c
    if-eqz v0, :cond_6a

    #@1e
    :cond_1e
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getCountry()Ljava/lang/String;

    #@21
    move-result-object v0

    #@22
    const-string v4, "US"

    #@24
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@27
    move-result v0

    #@28
    if-eqz v0, :cond_6a

    #@2a
    move v0, v1

    #@2b
    :goto_2b
    invoke-static {v3, v0}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@2e
    move-result v0

    #@2f
    sput-boolean v0, Lcom/android/internal/telephony/IccProvider;->ENABLE_PRIVACY_LOG:Z

    #@31
    .line 48
    const/4 v0, 0x4

    #@32
    new-array v0, v0, [Ljava/lang/String;

    #@34
    const-string v3, "name"

    #@36
    aput-object v3, v0, v1

    #@38
    const-string v1, "number"

    #@3a
    aput-object v1, v0, v2

    #@3c
    const-string v1, "emails"

    #@3e
    aput-object v1, v0, v5

    #@40
    const-string v1, "_id"

    #@42
    aput-object v1, v0, v6

    #@44
    sput-object v0, Lcom/android/internal/telephony/IccProvider;->ADDRESS_BOOK_COLUMN_NAMES:[Ljava/lang/String;

    #@46
    .line 64
    new-instance v0, Landroid/content/UriMatcher;

    #@48
    const/4 v1, -0x1

    #@49
    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    #@4c
    sput-object v0, Lcom/android/internal/telephony/IccProvider;->URL_MATCHER:Landroid/content/UriMatcher;

    #@4e
    .line 68
    sget-object v0, Lcom/android/internal/telephony/IccProvider;->URL_MATCHER:Landroid/content/UriMatcher;

    #@50
    const-string v1, "icc"

    #@52
    const-string v3, "adn"

    #@54
    invoke-virtual {v0, v1, v3, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    #@57
    .line 69
    sget-object v0, Lcom/android/internal/telephony/IccProvider;->URL_MATCHER:Landroid/content/UriMatcher;

    #@59
    const-string v1, "icc"

    #@5b
    const-string v2, "fdn"

    #@5d
    invoke-virtual {v0, v1, v2, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    #@60
    .line 70
    sget-object v0, Lcom/android/internal/telephony/IccProvider;->URL_MATCHER:Landroid/content/UriMatcher;

    #@62
    const-string v1, "icc"

    #@64
    const-string v2, "sdn"

    #@66
    invoke-virtual {v0, v1, v2, v6}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    #@69
    .line 71
    return-void

    #@6a
    :cond_6a
    move v0, v2

    #@6b
    .line 46
    goto :goto_2b
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 42
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    #@3
    return-void
.end method

.method private addIccRecordToEf(ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Z
    .registers 14
    .parameter "efType"
    .parameter "name"
    .parameter "number"
    .parameter "emails"
    .parameter "pin2"

    #@0
    .prologue
    .line 335
    const/4 v7, 0x0

    #@1
    .line 343
    .local v7, success:Z
    :try_start_1
    const-string v1, "simphonebook"

    #@3
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@6
    move-result-object v1

    #@7
    invoke-static {v1}, Lcom/android/internal/telephony/IIccPhoneBook$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/IIccPhoneBook;

    #@a
    move-result-object v0

    #@b
    .line 345
    .local v0, iccIpb:Lcom/android/internal/telephony/IIccPhoneBook;
    if-eqz v0, :cond_19

    #@d
    .line 346
    const-string v2, ""

    #@f
    const-string v3, ""

    #@11
    move v1, p1

    #@12
    move-object v4, p2

    #@13
    move-object v5, p3

    #@14
    move-object v6, p5

    #@15
    invoke-interface/range {v0 .. v6}, Lcom/android/internal/telephony/IIccPhoneBook;->updateAdnRecordsInEfBySearch(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_18
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_18} :catch_1c
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_18} :catch_1a

    #@18
    move-result v7

    #@19
    .line 355
    .end local v0           #iccIpb:Lcom/android/internal/telephony/IIccPhoneBook;
    :cond_19
    :goto_19
    return v7

    #@1a
    .line 351
    :catch_1a
    move-exception v1

    #@1b
    goto :goto_19

    #@1c
    .line 349
    :catch_1c
    move-exception v1

    #@1d
    goto :goto_19
.end method

.method private deleteIccRecordFromEf(ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Z
    .registers 14
    .parameter "efType"
    .parameter "name"
    .parameter "number"
    .parameter "emails"
    .parameter "pin2"

    #@0
    .prologue
    .line 388
    const/4 v7, 0x0

    #@1
    .line 391
    .local v7, success:Z
    :try_start_1
    const-string v1, "simphonebook"

    #@3
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@6
    move-result-object v1

    #@7
    invoke-static {v1}, Lcom/android/internal/telephony/IIccPhoneBook$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/IIccPhoneBook;

    #@a
    move-result-object v0

    #@b
    .line 393
    .local v0, iccIpb:Lcom/android/internal/telephony/IIccPhoneBook;
    if-eqz v0, :cond_19

    #@d
    .line 394
    const-string v4, ""

    #@f
    const-string v5, ""

    #@11
    move v1, p1

    #@12
    move-object v2, p2

    #@13
    move-object v3, p3

    #@14
    move-object v6, p5

    #@15
    invoke-interface/range {v0 .. v6}, Lcom/android/internal/telephony/IIccPhoneBook;->updateAdnRecordsInEfBySearch(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_18
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_18} :catch_1c
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_18} :catch_1a

    #@18
    move-result v7

    #@19
    .line 403
    .end local v0           #iccIpb:Lcom/android/internal/telephony/IIccPhoneBook;
    :cond_19
    :goto_19
    return v7

    #@1a
    .line 399
    :catch_1a
    move-exception v1

    #@1b
    goto :goto_19

    #@1c
    .line 397
    :catch_1c
    move-exception v1

    #@1d
    goto :goto_19
.end method

.method private loadFromEf(I)Landroid/database/MatrixCursor;
    .registers 9
    .parameter "efType"

    #@0
    .prologue
    .line 301
    const/4 v1, 0x0

    #@1
    .line 303
    .local v1, adnRecords:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/telephony/uicc/AdnRecord;>;"
    :try_start_1
    const-string v5, "simphonebook"

    #@3
    invoke-static {v5}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@6
    move-result-object v5

    #@7
    invoke-static {v5}, Lcom/android/internal/telephony/IIccPhoneBook$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/IIccPhoneBook;

    #@a
    move-result-object v4

    #@b
    .line 305
    .local v4, iccIpb:Lcom/android/internal/telephony/IIccPhoneBook;
    if-eqz v4, :cond_11

    #@d
    .line 306
    invoke-interface {v4, p1}, Lcom/android/internal/telephony/IIccPhoneBook;->getAdnRecordsInEf(I)Ljava/util/List;
    :try_end_10
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_10} :catch_3e
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_10} :catch_3c

    #@10
    move-result-object v1

    #@11
    .line 314
    .end local v4           #iccIpb:Lcom/android/internal/telephony/IIccPhoneBook;
    :cond_11
    :goto_11
    if-eqz v1, :cond_2d

    #@13
    .line 316
    invoke-interface {v1}, Ljava/util/List;->size()I

    #@16
    move-result v0

    #@17
    .line 317
    .local v0, N:I
    new-instance v2, Landroid/database/MatrixCursor;

    #@19
    sget-object v5, Lcom/android/internal/telephony/IccProvider;->ADDRESS_BOOK_COLUMN_NAMES:[Ljava/lang/String;

    #@1b
    invoke-direct {v2, v5, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;I)V

    #@1e
    .line 319
    .local v2, cursor:Landroid/database/MatrixCursor;
    const/4 v3, 0x0

    #@1f
    .local v3, i:I
    :goto_1f
    if-ge v3, v0, :cond_3b

    #@21
    .line 320
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@24
    move-result-object v5

    #@25
    check-cast v5, Lcom/android/internal/telephony/uicc/AdnRecord;

    #@27
    invoke-virtual {p0, v5, v2, v3}, Lcom/android/internal/telephony/IccProvider;->loadRecord(Lcom/android/internal/telephony/uicc/AdnRecord;Landroid/database/MatrixCursor;I)V

    #@2a
    .line 319
    add-int/lit8 v3, v3, 0x1

    #@2c
    goto :goto_1f

    #@2d
    .line 325
    .end local v0           #N:I
    .end local v2           #cursor:Landroid/database/MatrixCursor;
    .end local v3           #i:I
    :cond_2d
    const-string v5, "IccProvider"

    #@2f
    const-string v6, "Cannot load ADN records"

    #@31
    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    .line 326
    new-instance v2, Landroid/database/MatrixCursor;

    #@36
    sget-object v5, Lcom/android/internal/telephony/IccProvider;->ADDRESS_BOOK_COLUMN_NAMES:[Ljava/lang/String;

    #@38
    invoke-direct {v2, v5}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    #@3b
    :cond_3b
    return-object v2

    #@3c
    .line 310
    :catch_3c
    move-exception v5

    #@3d
    goto :goto_11

    #@3e
    .line 308
    :catch_3e
    move-exception v5

    #@3f
    goto :goto_11
.end method

.method private updateIccRecordInEf(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .registers 15
    .parameter "efType"
    .parameter "oldName"
    .parameter "oldNumber"
    .parameter "newName"
    .parameter "newNumber"
    .parameter "pin2"

    #@0
    .prologue
    .line 364
    const/4 v7, 0x0

    #@1
    .line 367
    .local v7, success:Z
    :try_start_1
    const-string v1, "simphonebook"

    #@3
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@6
    move-result-object v1

    #@7
    invoke-static {v1}, Lcom/android/internal/telephony/IIccPhoneBook$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/IIccPhoneBook;

    #@a
    move-result-object v0

    #@b
    .line 369
    .local v0, iccIpb:Lcom/android/internal/telephony/IIccPhoneBook;
    if-eqz v0, :cond_17

    #@d
    move v1, p1

    #@e
    move-object v2, p2

    #@f
    move-object v3, p3

    #@10
    move-object v4, p4

    #@11
    move-object v5, p5

    #@12
    move-object v6, p6

    #@13
    .line 370
    invoke-interface/range {v0 .. v6}, Lcom/android/internal/telephony/IIccPhoneBook;->updateAdnRecordsInEfBySearch(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_16
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_16} :catch_1a
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_16} :catch_18

    #@16
    move-result v7

    #@17
    .line 379
    .end local v0           #iccIpb:Lcom/android/internal/telephony/IIccPhoneBook;
    :cond_17
    :goto_17
    return v7

    #@18
    .line 375
    :catch_18
    move-exception v1

    #@19
    goto :goto_17

    #@1a
    .line 373
    :catch_1a
    move-exception v1

    #@1b
    goto :goto_17
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 21
    .parameter "url"
    .parameter "where"
    .parameter "whereArgs"

    #@0
    .prologue
    .line 190
    sget-object v1, Lcom/android/internal/telephony/IccProvider;->URL_MATCHER:Landroid/content/UriMatcher;

    #@2
    move-object/from16 v0, p1

    #@4
    invoke-virtual {v1, v0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@7
    move-result v8

    #@8
    .line 191
    .local v8, match:I
    packed-switch v8, :pswitch_data_d4

    #@b
    .line 201
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    #@d
    new-instance v15, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v16, "Cannot insert into URL: "

    #@14
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v15

    #@18
    move-object/from16 v0, p1

    #@1a
    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v15

    #@1e
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v15

    #@22
    invoke-direct {v1, v15}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@25
    throw v1

    #@26
    .line 193
    :pswitch_26
    const/16 v2, 0x6f3a

    #@28
    .line 206
    .local v2, efType:I
    :goto_28
    const/4 v3, 0x0

    #@29
    .line 207
    .local v3, tag:Ljava/lang/String;
    const/4 v4, 0x0

    #@2a
    .line 208
    .local v4, number:Ljava/lang/String;
    const/4 v5, 0x0

    #@2b
    .line 209
    .local v5, emails:[Ljava/lang/String;
    const/4 v6, 0x0

    #@2c
    .line 211
    .local v6, pin2:Ljava/lang/String;
    const-string v1, "AND"

    #@2e
    move-object/from16 v0, p2

    #@30
    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@33
    move-result-object v13

    #@34
    .line 212
    .local v13, tokens:[Ljava/lang/String;
    array-length v9, v13

    #@35
    .line 214
    .local v9, n:I
    :cond_35
    :goto_35
    add-int/lit8 v9, v9, -0x1

    #@37
    if-ltz v9, :cond_a6

    #@39
    .line 215
    aget-object v11, v13, v9

    #@3b
    .line 218
    .local v11, param:Ljava/lang/String;
    const-string v1, "="

    #@3d
    invoke-virtual {v11, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@40
    move-result-object v10

    #@41
    .line 220
    .local v10, pair:[Ljava/lang/String;
    array-length v1, v10

    #@42
    const/4 v15, 0x2

    #@43
    if-eq v1, v15, :cond_61

    #@45
    .line 221
    const-string v1, "IccProvider"

    #@47
    new-instance v15, Ljava/lang/StringBuilder;

    #@49
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@4c
    const-string v16, "resolve: bad whereClause parameter: "

    #@4e
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v15

    #@52
    invoke-virtual {v15, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v15

    #@56
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v15

    #@5a
    invoke-static {v1, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5d
    goto :goto_35

    #@5e
    .line 197
    .end local v2           #efType:I
    .end local v3           #tag:Ljava/lang/String;
    .end local v4           #number:Ljava/lang/String;
    .end local v5           #emails:[Ljava/lang/String;
    .end local v6           #pin2:Ljava/lang/String;
    .end local v9           #n:I
    .end local v10           #pair:[Ljava/lang/String;
    .end local v11           #param:Ljava/lang/String;
    .end local v13           #tokens:[Ljava/lang/String;
    :pswitch_5e
    const/16 v2, 0x6f3b

    #@60
    .line 198
    .restart local v2       #efType:I
    goto :goto_28

    #@61
    .line 225
    .restart local v3       #tag:Ljava/lang/String;
    .restart local v4       #number:Ljava/lang/String;
    .restart local v5       #emails:[Ljava/lang/String;
    .restart local v6       #pin2:Ljava/lang/String;
    .restart local v9       #n:I
    .restart local v10       #pair:[Ljava/lang/String;
    .restart local v11       #param:Ljava/lang/String;
    .restart local v13       #tokens:[Ljava/lang/String;
    :cond_61
    const/4 v1, 0x0

    #@62
    aget-object v1, v10, v1

    #@64
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@67
    move-result-object v7

    #@68
    .line 226
    .local v7, key:Ljava/lang/String;
    const/4 v1, 0x1

    #@69
    aget-object v1, v10, v1

    #@6b
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@6e
    move-result-object v14

    #@6f
    .line 228
    .local v14, val:Ljava/lang/String;
    const-string v1, "tag"

    #@71
    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@74
    move-result v1

    #@75
    if-eqz v1, :cond_7e

    #@77
    .line 229
    move-object/from16 v0, p0

    #@79
    invoke-virtual {v0, v14}, Lcom/android/internal/telephony/IccProvider;->normalizeValue(Ljava/lang/String;)Ljava/lang/String;

    #@7c
    move-result-object v3

    #@7d
    goto :goto_35

    #@7e
    .line 230
    :cond_7e
    const-string v1, "number"

    #@80
    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@83
    move-result v1

    #@84
    if-eqz v1, :cond_8d

    #@86
    .line 231
    move-object/from16 v0, p0

    #@88
    invoke-virtual {v0, v14}, Lcom/android/internal/telephony/IccProvider;->normalizeValue(Ljava/lang/String;)Ljava/lang/String;

    #@8b
    move-result-object v4

    #@8c
    goto :goto_35

    #@8d
    .line 232
    :cond_8d
    const-string v1, "emails"

    #@8f
    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@92
    move-result v1

    #@93
    if-eqz v1, :cond_97

    #@95
    .line 234
    const/4 v5, 0x0

    #@96
    goto :goto_35

    #@97
    .line 235
    :cond_97
    const-string v1, "pin2"

    #@99
    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9c
    move-result v1

    #@9d
    if-eqz v1, :cond_35

    #@9f
    .line 236
    move-object/from16 v0, p0

    #@a1
    invoke-virtual {v0, v14}, Lcom/android/internal/telephony/IccProvider;->normalizeValue(Ljava/lang/String;)Ljava/lang/String;

    #@a4
    move-result-object v6

    #@a5
    goto :goto_35

    #@a6
    .line 240
    .end local v7           #key:Ljava/lang/String;
    .end local v10           #pair:[Ljava/lang/String;
    .end local v11           #param:Ljava/lang/String;
    .end local v14           #val:Ljava/lang/String;
    :cond_a6
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@a9
    move-result v1

    #@aa
    if-eqz v1, :cond_ae

    #@ac
    .line 241
    const/4 v1, 0x0

    #@ad
    .line 254
    :goto_ad
    return v1

    #@ae
    .line 244
    :cond_ae
    const/16 v1, 0x6f3b

    #@b0
    if-ne v2, v1, :cond_ba

    #@b2
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@b5
    move-result v1

    #@b6
    if-eqz v1, :cond_ba

    #@b8
    .line 245
    const/4 v1, 0x0

    #@b9
    goto :goto_ad

    #@ba
    :cond_ba
    move-object/from16 v1, p0

    #@bc
    .line 248
    invoke-direct/range {v1 .. v6}, Lcom/android/internal/telephony/IccProvider;->deleteIccRecordFromEf(ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Z

    #@bf
    move-result v12

    #@c0
    .line 249
    .local v12, success:Z
    if-nez v12, :cond_c4

    #@c2
    .line 250
    const/4 v1, 0x0

    #@c3
    goto :goto_ad

    #@c4
    .line 253
    :cond_c4
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/IccProvider;->getContext()Landroid/content/Context;

    #@c7
    move-result-object v1

    #@c8
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@cb
    move-result-object v1

    #@cc
    const/4 v15, 0x0

    #@cd
    move-object/from16 v0, p1

    #@cf
    invoke-virtual {v1, v0, v15}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    #@d2
    .line 254
    const/4 v1, 0x1

    #@d3
    goto :goto_ad

    #@d4
    .line 191
    :pswitch_data_d4
    .packed-switch 0x1
        :pswitch_26
        :pswitch_5e
    .end packed-switch
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .registers 5
    .parameter "url"

    #@0
    .prologue
    .line 99
    sget-object v0, Lcom/android/internal/telephony/IccProvider;->URL_MATCHER:Landroid/content/UriMatcher;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@5
    move-result v0

    #@6
    packed-switch v0, :pswitch_data_26

    #@9
    .line 106
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@b
    new-instance v1, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v2, "Unknown URL "

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@21
    throw v0

    #@22
    .line 103
    :pswitch_22
    const-string v0, "vnd.android.cursor.dir/sim-contact"

    #@24
    return-object v0

    #@25
    .line 99
    nop

    #@26
    :pswitch_data_26
    .packed-switch 0x1
        :pswitch_22
        :pswitch_22
        :pswitch_22
    .end packed-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .registers 14
    .parameter "url"
    .parameter "initialValues"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 114
    const/4 v5, 0x0

    #@2
    .line 118
    .local v5, pin2:Ljava/lang/String;
    sget-object v0, Lcom/android/internal/telephony/IccProvider;->URL_MATCHER:Landroid/content/UriMatcher;

    #@4
    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@7
    move-result v7

    #@8
    .line 119
    .local v7, match:I
    packed-switch v7, :pswitch_data_72

    #@b
    .line 130
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@d
    new-instance v4, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v10, "Cannot insert into URL: "

    #@14
    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v4

    #@18
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v4

    #@1c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v4

    #@20
    invoke-direct {v0, v4}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@23
    throw v0

    #@24
    .line 121
    :pswitch_24
    const/16 v1, 0x6f3a

    #@26
    .line 134
    .local v1, efType:I
    :goto_26
    const-string v0, "tag"

    #@28
    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    #@2b
    move-result-object v2

    #@2c
    .line 135
    .local v2, tag:Ljava/lang/String;
    const-string v0, "number"

    #@2e
    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    #@31
    move-result-object v3

    #@32
    .local v3, number:Ljava/lang/String;
    move-object v0, p0

    #@33
    .line 137
    invoke-direct/range {v0 .. v5}, Lcom/android/internal/telephony/IccProvider;->addIccRecordToEf(ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Z

    #@36
    move-result v9

    #@37
    .line 139
    .local v9, success:Z
    if-nez v9, :cond_43

    #@39
    .line 166
    :goto_39
    return-object v4

    #@3a
    .line 125
    .end local v1           #efType:I
    .end local v2           #tag:Ljava/lang/String;
    .end local v3           #number:Ljava/lang/String;
    .end local v9           #success:Z
    :pswitch_3a
    const/16 v1, 0x6f3b

    #@3c
    .line 126
    .restart local v1       #efType:I
    const-string v0, "pin2"

    #@3e
    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    #@41
    move-result-object v5

    #@42
    .line 127
    goto :goto_26

    #@43
    .line 143
    .restart local v2       #tag:Ljava/lang/String;
    .restart local v3       #number:Ljava/lang/String;
    .restart local v9       #success:Z
    :cond_43
    new-instance v6, Ljava/lang/StringBuilder;

    #@45
    const-string v0, "content://icc/"

    #@47
    invoke-direct {v6, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@4a
    .line 144
    .local v6, buf:Ljava/lang/StringBuilder;
    packed-switch v7, :pswitch_data_7a

    #@4d
    .line 155
    :goto_4d
    const/4 v0, 0x0

    #@4e
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@51
    .line 157
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v0

    #@55
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@58
    move-result-object v8

    #@59
    .line 159
    .local v8, resultUri:Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/android/internal/telephony/IccProvider;->getContext()Landroid/content/Context;

    #@5c
    move-result-object v0

    #@5d
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@60
    move-result-object v0

    #@61
    invoke-virtual {v0, p1, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    #@64
    move-object v4, v8

    #@65
    .line 166
    goto :goto_39

    #@66
    .line 146
    .end local v8           #resultUri:Landroid/net/Uri;
    :pswitch_66
    const-string v0, "adn/"

    #@68
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    goto :goto_4d

    #@6c
    .line 150
    :pswitch_6c
    const-string v0, "fdn/"

    #@6e
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    goto :goto_4d

    #@72
    .line 119
    :pswitch_data_72
    .packed-switch 0x1
        :pswitch_24
        :pswitch_3a
    .end packed-switch

    #@7a
    .line 144
    :pswitch_data_7a
    .packed-switch 0x1
        :pswitch_66
        :pswitch_6c
    .end packed-switch
.end method

.method protected loadRecord(Lcom/android/internal/telephony/uicc/AdnRecord;Landroid/database/MatrixCursor;I)V
    .registers 15
    .parameter "record"
    .parameter "cursor"
    .parameter "id"

    #@0
    .prologue
    .line 413
    invoke-virtual {p1}, Lcom/android/internal/telephony/uicc/AdnRecord;->isEmpty()Z

    #@3
    move-result v9

    #@4
    if-nez v9, :cond_45

    #@6
    .line 414
    const/4 v9, 0x4

    #@7
    new-array v2, v9, [Ljava/lang/Object;

    #@9
    .line 415
    .local v2, contact:[Ljava/lang/Object;
    invoke-virtual {p1}, Lcom/android/internal/telephony/uicc/AdnRecord;->getAlphaTag()Ljava/lang/String;

    #@c
    move-result-object v0

    #@d
    .line 416
    .local v0, alphaTag:Ljava/lang/String;
    invoke-virtual {p1}, Lcom/android/internal/telephony/uicc/AdnRecord;->getNumber()Ljava/lang/String;

    #@10
    move-result-object v8

    #@11
    .line 419
    .local v8, number:Ljava/lang/String;
    const/4 v9, 0x0

    #@12
    aput-object v0, v2, v9

    #@14
    .line 420
    const/4 v9, 0x1

    #@15
    aput-object v8, v2, v9

    #@17
    .line 422
    invoke-virtual {p1}, Lcom/android/internal/telephony/uicc/AdnRecord;->getEmails()[Ljava/lang/String;

    #@1a
    move-result-object v5

    #@1b
    .line 423
    .local v5, emails:[Ljava/lang/String;
    if-eqz v5, :cond_3b

    #@1d
    .line 424
    new-instance v4, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    .line 425
    .local v4, emailString:Ljava/lang/StringBuilder;
    move-object v1, v5

    #@23
    .local v1, arr$:[Ljava/lang/String;
    array-length v7, v1

    #@24
    .local v7, len$:I
    const/4 v6, 0x0

    #@25
    .local v6, i$:I
    :goto_25
    if-ge v6, v7, :cond_34

    #@27
    aget-object v3, v1, v6

    #@29
    .line 427
    .local v3, email:Ljava/lang/String;
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    .line 428
    const-string v9, ","

    #@2e
    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    .line 425
    add-int/lit8 v6, v6, 0x1

    #@33
    goto :goto_25

    #@34
    .line 430
    .end local v3           #email:Ljava/lang/String;
    :cond_34
    const/4 v9, 0x2

    #@35
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v10

    #@39
    aput-object v10, v2, v9

    #@3b
    .line 432
    .end local v1           #arr$:[Ljava/lang/String;
    .end local v4           #emailString:Ljava/lang/StringBuilder;
    .end local v6           #i$:I
    .end local v7           #len$:I
    :cond_3b
    const/4 v9, 0x3

    #@3c
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3f
    move-result-object v10

    #@40
    aput-object v10, v2, v9

    #@42
    .line 433
    invoke-virtual {p2, v2}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    #@45
    .line 435
    .end local v0           #alphaTag:Ljava/lang/String;
    .end local v2           #contact:[Ljava/lang/Object;
    .end local v5           #emails:[Ljava/lang/String;
    .end local v8           #number:Ljava/lang/String;
    :cond_45
    return-void
.end method

.method protected log(Ljava/lang/String;)V
    .registers 5
    .parameter "msg"

    #@0
    .prologue
    .line 438
    const-string v0, "IccProvider"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[IccProvider] "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 439
    return-void
.end method

.method protected normalizeValue(Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .parameter "inVal"

    #@0
    .prologue
    const/16 v3, 0x27

    #@2
    .line 170
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@5
    move-result v0

    #@6
    .line 172
    .local v0, len:I
    if-nez v0, :cond_9

    #@8
    .line 181
    .end local p1
    :goto_8
    return-object p1

    #@9
    .line 175
    .restart local p1
    :cond_9
    move-object v1, p1

    #@a
    .line 177
    .local v1, retVal:Ljava/lang/String;
    const/4 v2, 0x0

    #@b
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    #@e
    move-result v2

    #@f
    if-ne v2, v3, :cond_20

    #@11
    add-int/lit8 v2, v0, -0x1

    #@13
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    #@16
    move-result v2

    #@17
    if-ne v2, v3, :cond_20

    #@19
    .line 178
    const/4 v2, 0x1

    #@1a
    add-int/lit8 v3, v0, -0x1

    #@1c
    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    :cond_20
    move-object p1, v1

    #@21
    .line 181
    goto :goto_8
.end method

.method public onCreate()Z
    .registers 2

    #@0
    .prologue
    .line 76
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .registers 9
    .parameter "url"
    .parameter "projection"
    .parameter "selection"
    .parameter "selectionArgs"
    .parameter "sort"

    #@0
    .prologue
    .line 82
    sget-object v0, Lcom/android/internal/telephony/IccProvider;->URL_MATCHER:Landroid/content/UriMatcher;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@5
    move-result v0

    #@6
    packed-switch v0, :pswitch_data_38

    #@9
    .line 93
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@b
    new-instance v1, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v2, "Unknown URL "

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@21
    throw v0

    #@22
    .line 84
    :pswitch_22
    const/16 v0, 0x6f3a

    #@24
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/IccProvider;->loadFromEf(I)Landroid/database/MatrixCursor;

    #@27
    move-result-object v0

    #@28
    .line 90
    :goto_28
    return-object v0

    #@29
    .line 87
    :pswitch_29
    const/16 v0, 0x6f3b

    #@2b
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/IccProvider;->loadFromEf(I)Landroid/database/MatrixCursor;

    #@2e
    move-result-object v0

    #@2f
    goto :goto_28

    #@30
    .line 90
    :pswitch_30
    const/16 v0, 0x6f49

    #@32
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/IccProvider;->loadFromEf(I)Landroid/database/MatrixCursor;

    #@35
    move-result-object v0

    #@36
    goto :goto_28

    #@37
    .line 82
    nop

    #@38
    :pswitch_data_38
    .packed-switch 0x1
        :pswitch_22
        :pswitch_29
        :pswitch_30
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 18
    .parameter "url"
    .parameter "values"
    .parameter "where"
    .parameter "whereArgs"

    #@0
    .prologue
    .line 260
    const/4 v6, 0x0

    #@1
    .line 264
    .local v6, pin2:Ljava/lang/String;
    sget-object v0, Lcom/android/internal/telephony/IccProvider;->URL_MATCHER:Landroid/content/UriMatcher;

    #@3
    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@6
    move-result v8

    #@7
    .line 265
    .local v8, match:I
    packed-switch v8, :pswitch_data_60

    #@a
    .line 276
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@c
    new-instance v11, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v12, "Cannot insert into URL: "

    #@13
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v11

    #@17
    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v11

    #@1b
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v11

    #@1f
    invoke-direct {v0, v11}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@22
    throw v0

    #@23
    .line 267
    :pswitch_23
    const/16 v1, 0x6f3a

    #@25
    .line 280
    .local v1, efType:I
    :goto_25
    const-string v0, "tag"

    #@27
    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    #@2a
    move-result-object v2

    #@2b
    .line 281
    .local v2, tag:Ljava/lang/String;
    const-string v0, "number"

    #@2d
    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    #@30
    move-result-object v3

    #@31
    .line 282
    .local v3, number:Ljava/lang/String;
    const/4 v7, 0x0

    #@32
    .line 283
    .local v7, emails:[Ljava/lang/String;
    const-string v0, "newTag"

    #@34
    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    #@37
    move-result-object v4

    #@38
    .line 284
    .local v4, newTag:Ljava/lang/String;
    const-string v0, "newNumber"

    #@3a
    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    #@3d
    move-result-object v5

    #@3e
    .line 285
    .local v5, newNumber:Ljava/lang/String;
    const/4 v9, 0x0

    #@3f
    .local v9, newEmails:[Ljava/lang/String;
    move-object v0, p0

    #@40
    .line 287
    invoke-direct/range {v0 .. v6}, Lcom/android/internal/telephony/IccProvider;->updateIccRecordInEf(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    #@43
    move-result v10

    #@44
    .line 290
    .local v10, success:Z
    if-nez v10, :cond_51

    #@46
    .line 291
    const/4 v0, 0x0

    #@47
    .line 295
    :goto_47
    return v0

    #@48
    .line 271
    .end local v1           #efType:I
    .end local v2           #tag:Ljava/lang/String;
    .end local v3           #number:Ljava/lang/String;
    .end local v4           #newTag:Ljava/lang/String;
    .end local v5           #newNumber:Ljava/lang/String;
    .end local v7           #emails:[Ljava/lang/String;
    .end local v9           #newEmails:[Ljava/lang/String;
    .end local v10           #success:Z
    :pswitch_48
    const/16 v1, 0x6f3b

    #@4a
    .line 272
    .restart local v1       #efType:I
    const-string v0, "pin2"

    #@4c
    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    #@4f
    move-result-object v6

    #@50
    .line 273
    goto :goto_25

    #@51
    .line 294
    .restart local v2       #tag:Ljava/lang/String;
    .restart local v3       #number:Ljava/lang/String;
    .restart local v4       #newTag:Ljava/lang/String;
    .restart local v5       #newNumber:Ljava/lang/String;
    .restart local v7       #emails:[Ljava/lang/String;
    .restart local v9       #newEmails:[Ljava/lang/String;
    .restart local v10       #success:Z
    :cond_51
    invoke-virtual {p0}, Lcom/android/internal/telephony/IccProvider;->getContext()Landroid/content/Context;

    #@54
    move-result-object v0

    #@55
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@58
    move-result-object v0

    #@59
    const/4 v11, 0x0

    #@5a
    invoke-virtual {v0, p1, v11}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    #@5d
    .line 295
    const/4 v0, 0x1

    #@5e
    goto :goto_47

    #@5f
    .line 265
    nop

    #@60
    :pswitch_data_60
    .packed-switch 0x1
        :pswitch_23
        :pswitch_48
    .end packed-switch
.end method
