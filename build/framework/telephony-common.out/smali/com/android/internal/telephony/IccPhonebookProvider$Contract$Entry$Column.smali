.class public interface abstract Lcom/android/internal/telephony/IccPhonebookProvider$Contract$Entry$Column;
.super Ljava/lang/Object;
.source "IccPhonebookProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/IccPhonebookProvider$Contract$Entry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Column"
.end annotation


# static fields
.field public static final ALPHA_TAG:Ljava/lang/String;

.field public static final ANRS:Ljava/lang/String;

.field public static final EMAILS:Ljava/lang/String;

.field public static final GROUP_INDEX:Ljava/lang/String;

.field public static final INDEX:Ljava/lang/String;

.field public static final NUMBER:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 168
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->ENTRY_PROJECTION:[Ljava/lang/String;

    #@2
    const/4 v1, 0x0

    #@3
    aget-object v0, v0, v1

    #@5
    sput-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract$Entry$Column;->INDEX:Ljava/lang/String;

    #@7
    .line 169
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->ENTRY_PROJECTION:[Ljava/lang/String;

    #@9
    const/4 v1, 0x1

    #@a
    aget-object v0, v0, v1

    #@c
    sput-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract$Entry$Column;->ALPHA_TAG:Ljava/lang/String;

    #@e
    .line 170
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->ENTRY_PROJECTION:[Ljava/lang/String;

    #@10
    const/4 v1, 0x2

    #@11
    aget-object v0, v0, v1

    #@13
    sput-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract$Entry$Column;->NUMBER:Ljava/lang/String;

    #@15
    .line 171
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->ENTRY_PROJECTION:[Ljava/lang/String;

    #@17
    const/4 v1, 0x3

    #@18
    aget-object v0, v0, v1

    #@1a
    sput-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract$Entry$Column;->ANRS:Ljava/lang/String;

    #@1c
    .line 172
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->ENTRY_PROJECTION:[Ljava/lang/String;

    #@1e
    const/4 v1, 0x4

    #@1f
    aget-object v0, v0, v1

    #@21
    sput-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract$Entry$Column;->EMAILS:Ljava/lang/String;

    #@23
    .line 173
    sget-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract;->ENTRY_PROJECTION:[Ljava/lang/String;

    #@25
    const/4 v1, 0x5

    #@26
    aget-object v0, v0, v1

    #@28
    sput-object v0, Lcom/android/internal/telephony/IccPhonebookProvider$Contract$Entry$Column;->GROUP_INDEX:Ljava/lang/String;

    #@2a
    return-void
.end method
