.class public Lcom/android/internal/telephony/PhoneProxy$MyHandler;
.super Landroid/os/Handler;
.source "PhoneProxy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/PhoneProxy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MyHandler"
.end annotation


# static fields
.field private static final MESSAGE_GET_PREFERRED_NETWORK_TYPE:I = 0x0

.field private static final MESSAGE_SET_PREFERRED_NETWORK_TYPE:I = 0x1


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/PhoneProxy;


# direct methods
.method public constructor <init>(Lcom/android/internal/telephony/PhoneProxy;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1754
    iput-object p1, p0, Lcom/android/internal/telephony/PhoneProxy$MyHandler;->this$0:Lcom/android/internal/telephony/PhoneProxy;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 12
    .parameter "msg"

    #@0
    .prologue
    const/16 v8, 0xa

    #@2
    const/4 v9, 0x1

    #@3
    const/4 v6, -0x1

    #@4
    const/4 v5, 0x0

    #@5
    .line 1759
    iget v4, p1, Landroid/os/Message;->what:I

    #@7
    packed-switch v4, :pswitch_data_fa

    #@a
    .line 1817
    :goto_a
    return-void

    #@b
    .line 1761
    :pswitch_b
    const-string v4, "MyHandler  MESSAGE_SET_PREFERRED_NETWORK_TYPE "

    #@d
    invoke-static {v4}, Lcom/android/internal/telephony/PhoneProxy;->access$000(Ljava/lang/String;)V

    #@10
    .line 1763
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@12
    check-cast v0, Landroid/os/AsyncResult;

    #@14
    .line 1764
    .local v0, ar:Landroid/os/AsyncResult;
    iget-object v4, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@16
    if-nez v4, :cond_6d

    #@18
    .line 1765
    iget-object v4, p0, Lcom/android/internal/telephony/PhoneProxy$MyHandler;->this$0:Lcom/android/internal/telephony/PhoneProxy;

    #@1a
    iget-object v4, v4, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@1c
    invoke-interface {v4}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@1f
    move-result-object v4

    #@20
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@23
    move-result-object v4

    #@24
    const-string v7, "preferred_network_mode"

    #@26
    iget-object v8, p0, Lcom/android/internal/telephony/PhoneProxy$MyHandler;->this$0:Lcom/android/internal/telephony/PhoneProxy;

    #@28
    iget v8, v8, Lcom/android/internal/telephony/PhoneProxy;->setNetworkType:I

    #@2a
    invoke-static {v4, v7, v8}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@2d
    .line 1769
    new-instance v4, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    const-string v7, "MyHandler MESSAGE_SET_PREFERRED_NETWORK_TYPE setNetworkType "

    #@34
    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v4

    #@38
    iget-object v7, p0, Lcom/android/internal/telephony/PhoneProxy$MyHandler;->this$0:Lcom/android/internal/telephony/PhoneProxy;

    #@3a
    invoke-virtual {v7}, Lcom/android/internal/telephony/PhoneProxy;->getPreferredNetworkMode()I

    #@3d
    move-result v7

    #@3e
    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@41
    move-result-object v4

    #@42
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v4

    #@46
    invoke-static {v4}, Lcom/android/internal/telephony/PhoneProxy;->access$000(Ljava/lang/String;)V

    #@49
    .line 1774
    :goto_49
    new-instance v1, Landroid/content/Intent;

    #@4b
    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    #@4e
    .line 1775
    .local v1, intent:Landroid/content/Intent;
    const-string v4, "SetNetWorkMode"

    #@50
    invoke-virtual {v1, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@53
    .line 1776
    const-string v7, "exception"

    #@55
    iget-object v4, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@57
    if-nez v4, :cond_7b

    #@59
    move v4, v5

    #@5a
    :goto_5a
    invoke-virtual {v1, v7, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@5d
    .line 1777
    iget-object v4, p0, Lcom/android/internal/telephony/PhoneProxy$MyHandler;->this$0:Lcom/android/internal/telephony/PhoneProxy;

    #@5f
    iget-object v4, v4, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@61
    invoke-interface {v4}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@64
    move-result-object v4

    #@65
    invoke-virtual {v4, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@68
    .line 1778
    iget-object v4, p0, Lcom/android/internal/telephony/PhoneProxy$MyHandler;->this$0:Lcom/android/internal/telephony/PhoneProxy;

    #@6a
    iput-boolean v9, v4, Lcom/android/internal/telephony/PhoneProxy;->mIntentDuplicate:Z

    #@6c
    goto :goto_a

    #@6d
    .line 1771
    .end local v1           #intent:Landroid/content/Intent;
    :cond_6d
    iget-object v4, p0, Lcom/android/internal/telephony/PhoneProxy$MyHandler;->this$0:Lcom/android/internal/telephony/PhoneProxy;

    #@6f
    iget-object v7, p0, Lcom/android/internal/telephony/PhoneProxy$MyHandler;->this$0:Lcom/android/internal/telephony/PhoneProxy;

    #@71
    iget-object v7, v7, Lcom/android/internal/telephony/PhoneProxy;->mHandler:Lcom/android/internal/telephony/PhoneProxy$MyHandler;

    #@73
    invoke-virtual {v7, v5}, Lcom/android/internal/telephony/PhoneProxy$MyHandler;->obtainMessage(I)Landroid/os/Message;

    #@76
    move-result-object v7

    #@77
    invoke-virtual {v4, v7}, Lcom/android/internal/telephony/PhoneProxy;->getPreferredNetworkType(Landroid/os/Message;)V

    #@7a
    goto :goto_49

    #@7b
    .restart local v1       #intent:Landroid/content/Intent;
    :cond_7b
    move v4, v6

    #@7c
    .line 1776
    goto :goto_5a

    #@7d
    .line 1781
    .end local v0           #ar:Landroid/os/AsyncResult;
    .end local v1           #intent:Landroid/content/Intent;
    :pswitch_7d
    const-string v4, "MyHandler  MESSAGE_GET_PREFERRED_NETWORK_TYPE "

    #@7f
    invoke-static {v4}, Lcom/android/internal/telephony/PhoneProxy;->access$000(Ljava/lang/String;)V

    #@82
    .line 1782
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@84
    check-cast v0, Landroid/os/AsyncResult;

    #@86
    .line 1783
    .restart local v0       #ar:Landroid/os/AsyncResult;
    const/16 v2, 0xa

    #@88
    .line 1784
    .local v2, modemNetworkMode:I
    iget-object v4, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@8a
    if-nez v4, :cond_b4

    #@8c
    .line 1785
    iget-object v4, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@8e
    check-cast v4, [I

    #@90
    check-cast v4, [I

    #@92
    aget v2, v4, v5

    #@94
    .line 1786
    iget-object v4, p0, Lcom/android/internal/telephony/PhoneProxy$MyHandler;->this$0:Lcom/android/internal/telephony/PhoneProxy;

    #@96
    invoke-virtual {v4}, Lcom/android/internal/telephony/PhoneProxy;->getPreferredNetworkMode()I

    #@99
    move-result v3

    #@9a
    .line 1789
    .local v3, settingsNetworkMode:I
    if-ltz v2, :cond_d9

    #@9c
    const/16 v4, 0xc

    #@9e
    if-gt v2, v4, :cond_d9

    #@a0
    .line 1791
    if-eq v2, v3, :cond_b4

    #@a2
    .line 1793
    move v3, v2

    #@a3
    .line 1795
    iget-object v4, p0, Lcom/android/internal/telephony/PhoneProxy$MyHandler;->this$0:Lcom/android/internal/telephony/PhoneProxy;

    #@a5
    iget-object v4, v4, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@a7
    invoke-interface {v4}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@aa
    move-result-object v4

    #@ab
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@ae
    move-result-object v4

    #@af
    const-string v7, "preferred_network_mode"

    #@b1
    invoke-static {v4, v7, v3}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@b4
    .line 1810
    .end local v3           #settingsNetworkMode:I
    :cond_b4
    :goto_b4
    new-instance v1, Landroid/content/Intent;

    #@b6
    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    #@b9
    .line 1811
    .restart local v1       #intent:Landroid/content/Intent;
    const-string v4, "GetNetWorkMode"

    #@bb
    invoke-virtual {v1, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@be
    .line 1812
    const-string v4, "networkmode"

    #@c0
    invoke-virtual {v1, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@c3
    .line 1813
    const-string v4, "exception"

    #@c5
    iget-object v7, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@c7
    if-nez v7, :cond_f8

    #@c9
    :goto_c9
    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@cc
    .line 1814
    iget-object v4, p0, Lcom/android/internal/telephony/PhoneProxy$MyHandler;->this$0:Lcom/android/internal/telephony/PhoneProxy;

    #@ce
    iget-object v4, v4, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@d0
    invoke-interface {v4}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@d3
    move-result-object v4

    #@d4
    invoke-virtual {v4, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@d7
    goto/16 :goto_a

    #@d9
    .line 1802
    .end local v1           #intent:Landroid/content/Intent;
    .restart local v3       #settingsNetworkMode:I
    :cond_d9
    iget-object v4, p0, Lcom/android/internal/telephony/PhoneProxy$MyHandler;->this$0:Lcom/android/internal/telephony/PhoneProxy;

    #@db
    iget-object v4, v4, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@dd
    invoke-interface {v4}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@e0
    move-result-object v4

    #@e1
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@e4
    move-result-object v4

    #@e5
    const-string v7, "preferred_network_mode"

    #@e7
    invoke-static {v4, v7, v8}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@ea
    .line 1807
    iget-object v4, p0, Lcom/android/internal/telephony/PhoneProxy$MyHandler;->this$0:Lcom/android/internal/telephony/PhoneProxy;

    #@ec
    iget-object v7, p0, Lcom/android/internal/telephony/PhoneProxy$MyHandler;->this$0:Lcom/android/internal/telephony/PhoneProxy;

    #@ee
    iget-object v7, v7, Lcom/android/internal/telephony/PhoneProxy;->mHandler:Lcom/android/internal/telephony/PhoneProxy$MyHandler;

    #@f0
    invoke-virtual {v7, v9}, Lcom/android/internal/telephony/PhoneProxy$MyHandler;->obtainMessage(I)Landroid/os/Message;

    #@f3
    move-result-object v7

    #@f4
    invoke-virtual {v4, v8, v7}, Lcom/android/internal/telephony/PhoneProxy;->setPreferredNetworkType(ILandroid/os/Message;)V

    #@f7
    goto :goto_b4

    #@f8
    .end local v3           #settingsNetworkMode:I
    .restart local v1       #intent:Landroid/content/Intent;
    :cond_f8
    move v5, v6

    #@f9
    .line 1813
    goto :goto_c9

    #@fa
    .line 1759
    :pswitch_data_fa
    .packed-switch 0x0
        :pswitch_7d
        :pswitch_b
    .end packed-switch
.end method
