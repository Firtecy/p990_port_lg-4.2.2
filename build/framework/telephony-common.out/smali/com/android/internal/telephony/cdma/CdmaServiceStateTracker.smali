.class public Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;
.super Lcom/android/internal/telephony/ServiceStateTracker;
.source "CdmaServiceStateTracker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker$8;
    }
.end annotation


# static fields
.field static final LOG_TAG:Ljava/lang/String; = "CDMA"

.field protected static final LOSSOFSERVICE:I = 0x3e8

.field private static final NITZ_UPDATE_DIFF_DEFAULT:I = 0x7d0

.field private static final NITZ_UPDATE_SPACING_DEFAULT:I = 0x927c0

.field private static final UNACTIVATED_MIN2_VALUE:Ljava/lang/String; = "000000"

.field private static final UNACTIVATED_MIN_VALUE:Ljava/lang/String; = "1111110111"

.field private static final WAKELOCK_TAG:Ljava/lang/String; = "ServiceStateTracker"

.field private static mTimeZoneMonitor:Lcom/android/internal/telephony/LgeTimeZoneMonitor;

.field public static prev_alertId:I


# instance fields
.field protected CheckATTACH:Z

.field private final EXTENDED_NETWORK_PATH:Ljava/lang/String;

.field private final LOSS_OF_SERVICE_PATH:Ljava/lang/String;

.field private final NETWORK_EXTENDER_PATH:Ljava/lang/String;

.field private final ROAMING_PATH:Ljava/lang/String;

.field private final VERIZON_WIRELESS_PATH:Ljava/lang/String;

.field protected cdmaForSubscriptionInfoReadyRegistrants:Landroid/os/RegistrantList;

.field cellLoc:Landroid/telephony/cdma/CdmaCellLocation;

.field private cr:Landroid/content/ContentResolver;

.field private currentCarrier:Ljava/lang/String;

.field hasChangedRoamingIndicator:Z

.field hasChangedSystemIDNetworkID:Z

.field hasStateChanged:Z

.field public isEriRingtoneStart:Z

.field private isEriTextLoaded:Z

.field protected isInShutDown:Z

.field protected isSubscriptionFromRuim:Z

.field public isVZWERISoundPlaying:Z

.field private mAudioManager:Landroid/media/AudioManager;

.field private mAutoTimeObserver:Landroid/database/ContentObserver;

.field private mAutoTimeZoneObserver:Landroid/database/ContentObserver;

.field private mCdmaApnObserver:Landroid/database/ContentObserver;

.field private mCdmaRoaming:Z

.field private mCdmaSSM:Lcom/android/internal/telephony/cdma/CdmaSubscriptionSourceManager;

.field private mCellLocationObserver:Landroid/database/ContentObserver;

.field private mContext:Landroid/content/Context;

.field protected mCurPlmn:Ljava/lang/String;

.field mCurrentOtaspMode:I

.field protected mDataConnectionState:I

.field protected mDataRoaming:Z

.field private mDefaultRoamingIndicator:I

.field public mDefaultRoamingIndicator_data:I

.field protected mGotCountryCode:Z

.field protected mHDRRoamingIndicator:I

.field protected mHomeNetworkId:[I

.field protected mHomeSystemId:[I

.field private mIntentReceiver:Landroid/content/BroadcastReceiver;

.field private mIsInPrl:Z

.field public mIsInPrl_data:Z

.field protected mIsMinInfoReady:Z

.field protected mLteEhrpdForced:Ljava/lang/String;

.field protected mMdn:Ljava/lang/String;

.field private mMediaPlayer:Landroid/media/MediaPlayer;

.field protected mMin:Ljava/lang/String;

.field protected mNeedFixZone:Z

.field public mNetworkType_data:I

.field protected mNewDataConnectionState:I

.field private mNitzUpdateDiff:I

.field private mNitzUpdateSpacing:I

.field protected mPrlVersion:Ljava/lang/String;

.field private mRegistrationDeniedReason:Ljava/lang/String;

.field protected mRegistrationState:I

.field private mRoamingIndicator:I

.field public mRoamingIndicator_data:I

.field mSavedAtTime:J

.field mSavedTime:J

.field mSavedTimeZone:Ljava/lang/String;

.field private mVoiceRoaming:Z

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mZoneDst:Z

.field private mZoneOffset:I

.field private mZoneTime:J

.field newCellLoc:Landroid/telephony/cdma/CdmaCellLocation;

.field protected phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

.field private restoreVolume:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 181
    const/4 v0, 0x0

    #@1
    sput-object v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mTimeZoneMonitor:Lcom/android/internal/telephony/LgeTimeZoneMonitor;

    #@3
    .line 211
    const/4 v0, -0x1

    #@4
    sput v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->prev_alertId:I

    #@6
    return-void
.end method

.method public constructor <init>(Lcom/android/internal/telephony/cdma/CDMAPhone;)V
    .registers 3
    .parameter "phone"

    #@0
    .prologue
    .line 311
    new-instance v0, Landroid/telephony/CellInfoCdma;

    #@2
    invoke-direct {v0}, Landroid/telephony/CellInfoCdma;-><init>()V

    #@5
    invoke-direct {p0, p1, v0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;-><init>(Lcom/android/internal/telephony/cdma/CDMAPhone;Landroid/telephony/CellInfo;)V

    #@8
    .line 312
    return-void
.end method

.method protected constructor <init>(Lcom/android/internal/telephony/cdma/CDMAPhone;Landroid/telephony/CellInfo;)V
    .registers 13
    .parameter "phone"
    .parameter "cellInfo"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    const/4 v5, 0x1

    #@2
    const/4 v6, 0x0

    #@3
    .line 315
    iget-object v4, p1, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@5
    invoke-direct {p0, p1, v4, p2}, Lcom/android/internal/telephony/ServiceStateTracker;-><init>(Lcom/android/internal/telephony/PhoneBase;Lcom/android/internal/telephony/CommandsInterface;Landroid/telephony/CellInfo;)V

    #@8
    .line 132
    iput v6, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mCurrentOtaspMode:I

    #@a
    .line 136
    const-string v4, "ro.nitz_update_spacing"

    #@c
    const v7, 0x927c0

    #@f
    invoke-static {v4, v7}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@12
    move-result v4

    #@13
    iput v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mNitzUpdateSpacing:I

    #@15
    .line 141
    const-string v4, "ro.nitz_update_diff"

    #@17
    const/16 v7, 0x7d0

    #@19
    invoke-static {v4, v7}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@1c
    move-result v4

    #@1d
    iput v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mNitzUpdateDiff:I

    #@1f
    .line 144
    iput-boolean v6, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mCdmaRoaming:Z

    #@21
    .line 146
    iput-boolean v6, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mVoiceRoaming:Z

    #@23
    .line 148
    iput-boolean v6, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mDataRoaming:Z

    #@25
    .line 149
    iput v5, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mRoamingIndicator:I

    #@27
    .line 151
    iput v5, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mDefaultRoamingIndicator:I

    #@29
    .line 156
    iput v5, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mDataConnectionState:I

    #@2b
    .line 157
    iput v5, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mNewDataConnectionState:I

    #@2d
    .line 160
    iput-boolean v6, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->CheckATTACH:Z

    #@2f
    .line 163
    const/4 v4, -0x1

    #@30
    iput v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mRegistrationState:I

    #@32
    .line 164
    new-instance v4, Landroid/os/RegistrantList;

    #@34
    invoke-direct {v4}, Landroid/os/RegistrantList;-><init>()V

    #@37
    iput-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->cdmaForSubscriptionInfoReadyRegistrants:Landroid/os/RegistrantList;

    #@39
    .line 171
    iput-boolean v6, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mNeedFixZone:Z

    #@3b
    .line 175
    iput-boolean v6, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mGotCountryCode:Z

    #@3d
    .line 189
    iput-object v9, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mCurPlmn:Ljava/lang/String;

    #@3f
    .line 192
    iput-object v9, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mHomeSystemId:[I

    #@41
    .line 193
    iput-object v9, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mHomeNetworkId:[I

    #@43
    .line 196
    iput-boolean v6, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mIsMinInfoReady:Z

    #@45
    .line 198
    iput-boolean v6, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isEriTextLoaded:Z

    #@47
    .line 199
    iput-boolean v6, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isSubscriptionFromRuim:Z

    #@49
    .line 206
    iput-object v9, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->currentCarrier:Ljava/lang/String;

    #@4b
    .line 214
    iput v6, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->restoreVolume:I

    #@4d
    .line 215
    const-string v4, "/system/media/audio/eri/LossofService.wav"

    #@4f
    iput-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->LOSS_OF_SERVICE_PATH:Ljava/lang/String;

    #@51
    .line 216
    const-string v4, "/system/media/audio/eri/Roaming.wav"

    #@53
    iput-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->ROAMING_PATH:Ljava/lang/String;

    #@55
    .line 217
    const-string v4, "/system/media/audio/eri/ExtendedNetwork.wav"

    #@57
    iput-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->EXTENDED_NETWORK_PATH:Ljava/lang/String;

    #@59
    .line 218
    const-string v4, "/system/media/audio/eri/NetworkExtender.wav"

    #@5b
    iput-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->NETWORK_EXTENDER_PATH:Ljava/lang/String;

    #@5d
    .line 219
    const-string v4, "/system/media/audio/eri/VerizonWireless.wav"

    #@5f
    iput-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->VERIZON_WIRELESS_PATH:Ljava/lang/String;

    #@61
    .line 220
    iput-boolean v6, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isInShutDown:Z

    #@63
    .line 221
    iput-boolean v6, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isEriRingtoneStart:Z

    #@65
    .line 222
    iput-boolean v6, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isVZWERISoundPlaying:Z

    #@67
    .line 223
    iput-boolean v6, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->hasStateChanged:Z

    #@69
    .line 224
    iput-boolean v6, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->hasChangedSystemIDNetworkID:Z

    #@6b
    .line 225
    iput-boolean v6, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->hasChangedRoamingIndicator:Z

    #@6d
    .line 227
    iput v5, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mRoamingIndicator_data:I

    #@6f
    .line 235
    iput v5, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mDefaultRoamingIndicator_data:I

    #@71
    .line 236
    iput v6, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mNetworkType_data:I

    #@73
    .line 241
    new-instance v4, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker$1;

    #@75
    new-instance v7, Landroid/os/Handler;

    #@77
    invoke-direct {v7}, Landroid/os/Handler;-><init>()V

    #@7a
    invoke-direct {v4, p0, v7}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker$1;-><init>(Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;Landroid/os/Handler;)V

    #@7d
    iput-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mAutoTimeObserver:Landroid/database/ContentObserver;

    #@7f
    .line 249
    new-instance v4, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker$2;

    #@81
    new-instance v7, Landroid/os/Handler;

    #@83
    invoke-direct {v7}, Landroid/os/Handler;-><init>()V

    #@86
    invoke-direct {v4, p0, v7}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker$2;-><init>(Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;Landroid/os/Handler;)V

    #@89
    iput-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mAutoTimeZoneObserver:Landroid/database/ContentObserver;

    #@8b
    .line 258
    new-instance v4, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker$3;

    #@8d
    invoke-direct {v4, p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker$3;-><init>(Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;)V

    #@90
    iput-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@92
    .line 291
    new-instance v4, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker$4;

    #@94
    new-instance v7, Landroid/os/Handler;

    #@96
    invoke-direct {v7}, Landroid/os/Handler;-><init>()V

    #@99
    invoke-direct {v4, p0, v7}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker$4;-><init>(Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;Landroid/os/Handler;)V

    #@9c
    iput-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mCdmaApnObserver:Landroid/database/ContentObserver;

    #@9e
    .line 301
    new-instance v4, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker$5;

    #@a0
    new-instance v7, Landroid/os/Handler;

    #@a2
    invoke-direct {v7}, Landroid/os/Handler;-><init>()V

    #@a5
    invoke-direct {v4, p0, v7}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker$5;-><init>(Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;Landroid/os/Handler;)V

    #@a8
    iput-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mCellLocationObserver:Landroid/database/ContentObserver;

    #@aa
    .line 317
    iput-object p1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@ac
    .line 318
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@af
    move-result-object v4

    #@b0
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@b3
    move-result-object v4

    #@b4
    iput-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->cr:Landroid/content/ContentResolver;

    #@b6
    .line 319
    new-instance v4, Landroid/telephony/cdma/CdmaCellLocation;

    #@b8
    invoke-direct {v4}, Landroid/telephony/cdma/CdmaCellLocation;-><init>()V

    #@bb
    iput-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->cellLoc:Landroid/telephony/cdma/CdmaCellLocation;

    #@bd
    .line 320
    new-instance v4, Landroid/telephony/cdma/CdmaCellLocation;

    #@bf
    invoke-direct {v4}, Landroid/telephony/cdma/CdmaCellLocation;-><init>()V

    #@c2
    iput-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->newCellLoc:Landroid/telephony/cdma/CdmaCellLocation;

    #@c4
    .line 322
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@c7
    move-result-object v4

    #@c8
    iget-object v7, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@ca
    const/16 v8, 0x27

    #@cc
    invoke-static {v4, v7, p0, v8, v9}, Lcom/android/internal/telephony/cdma/CdmaSubscriptionSourceManager;->getInstance(Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;Landroid/os/Handler;ILjava/lang/Object;)Lcom/android/internal/telephony/cdma/CdmaSubscriptionSourceManager;

    #@cf
    move-result-object v4

    #@d0
    iput-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mCdmaSSM:Lcom/android/internal/telephony/cdma/CdmaSubscriptionSourceManager;

    #@d2
    .line 324
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mCdmaSSM:Lcom/android/internal/telephony/cdma/CdmaSubscriptionSourceManager;

    #@d4
    invoke-virtual {v4}, Lcom/android/internal/telephony/cdma/CdmaSubscriptionSourceManager;->getCdmaSubscriptionSource()I

    #@d7
    move-result v4

    #@d8
    if-nez v4, :cond_254

    #@da
    move v4, v5

    #@db
    :goto_db
    iput-boolean v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isSubscriptionFromRuim:Z

    #@dd
    .line 327
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@e0
    move-result-object v4

    #@e1
    const-string v7, "power"

    #@e3
    invoke-virtual {v4, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@e6
    move-result-object v3

    #@e7
    check-cast v3, Landroid/os/PowerManager;

    #@e9
    .line 329
    .local v3, powerManager:Landroid/os/PowerManager;
    const-string v4, "ServiceStateTracker"

    #@eb
    invoke-virtual {v3, v5, v4}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@ee
    move-result-object v4

    #@ef
    iput-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@f1
    .line 331
    iget-object v4, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@f3
    invoke-interface {v4, p0, v5, v9}, Lcom/android/internal/telephony/CommandsInterface;->registerForRadioStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@f6
    .line 333
    iget-object v4, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@f8
    const/16 v7, 0x1e

    #@fa
    invoke-interface {v4, p0, v7, v9}, Lcom/android/internal/telephony/CommandsInterface;->registerForVoiceNetworkStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@fd
    .line 334
    iget-object v4, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@ff
    const/16 v7, 0xb

    #@101
    invoke-interface {v4, p0, v7, v9}, Lcom/android/internal/telephony/CommandsInterface;->setOnNITZTime(Landroid/os/Handler;ILjava/lang/Object;)V

    #@104
    .line 336
    iget-object v4, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@106
    const/16 v7, 0x28

    #@108
    invoke-interface {v4, p0, v7, v9}, Lcom/android/internal/telephony/CommandsInterface;->registerForCdmaPrlChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@10b
    .line 337
    const/16 v4, 0x24

    #@10d
    invoke-virtual {p1, p0, v4, v9}, Lcom/android/internal/telephony/cdma/CDMAPhone;->registerForEriFileLoaded(Landroid/os/Handler;ILjava/lang/Object;)V

    #@110
    .line 338
    iget-object v4, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@112
    const/16 v7, 0x25

    #@114
    invoke-interface {v4, p0, v7, v9}, Lcom/android/internal/telephony/CommandsInterface;->registerForCdmaOtaProvision(Landroid/os/Handler;ILjava/lang/Object;)V

    #@117
    .line 340
    iget-object v4, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@119
    const/16 v7, 0x46

    #@11b
    invoke-interface {v4, p0, v7, v9}, Lcom/android/internal/telephony/CommandsInterface;->registerForLteEhrpdForcedChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@11e
    .line 344
    iget-object v4, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@120
    const/16 v7, 0x47

    #@122
    invoke-interface {v4, p0, v7, v9}, Lcom/android/internal/telephony/CommandsInterface;->registerForHDRRoamingIndicator(Landroid/os/Handler;ILjava/lang/Object;)V

    #@125
    .line 348
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->cr:Landroid/content/ContentResolver;

    #@127
    const-string v7, "airplane_mode_on"

    #@129
    invoke-static {v4, v7, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@12c
    move-result v0

    #@12d
    .line 349
    .local v0, airplaneMode:I
    if-gtz v0, :cond_257

    #@12f
    move v4, v5

    #@130
    :goto_130
    iput-boolean v4, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mDesiredPowerState:Z

    #@132
    .line 352
    const-string v4, "VZW"

    #@134
    invoke-static {v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@137
    move-result v4

    #@138
    if-eqz v4, :cond_194

    #@13a
    .line 353
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->cr:Landroid/content/ContentResolver;

    #@13c
    const-string v7, "apn2_disable"

    #@13e
    invoke-static {v4, v7, v6}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@141
    move-result v1

    #@142
    .line 354
    .local v1, apn2_disable_Mode:I
    iget-boolean v4, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mDesiredPowerState:Z

    #@144
    if-eqz v4, :cond_25a

    #@146
    if-gtz v1, :cond_25a

    #@148
    move v4, v5

    #@149
    :goto_149
    iput-boolean v4, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mDesiredPowerState:Z

    #@14b
    .line 355
    const-string v4, "CDMA"

    #@14d
    new-instance v7, Ljava/lang/StringBuilder;

    #@14f
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@152
    const-string v8, "apn2_disable_Mode: "

    #@154
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@157
    move-result-object v7

    #@158
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15b
    move-result-object v7

    #@15c
    const-string v8, " ,airplaneMode: "

    #@15e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@161
    move-result-object v7

    #@162
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@165
    move-result-object v7

    #@166
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@169
    move-result-object v7

    #@16a
    invoke-static {v4, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@16d
    .line 356
    if-lez v1, :cond_25d

    #@16f
    .line 357
    const-string v4, "ril.current.apn2-disable"

    #@171
    const-string v7, "1"

    #@173
    invoke-static {v4, v7}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@176
    .line 358
    const-string v4, "CDMA"

    #@178
    new-instance v7, Ljava/lang/StringBuilder;

    #@17a
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@17d
    const-string v8, "[APN2 Disable] ril.current.apn2-disable : "

    #@17f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@182
    move-result-object v7

    #@183
    const-string v8, "ril.current.apn2-disable"

    #@185
    invoke-static {v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@188
    move-result-object v8

    #@189
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18c
    move-result-object v7

    #@18d
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@190
    move-result-object v7

    #@191
    invoke-static {v4, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@194
    .line 365
    .end local v1           #apn2_disable_Mode:I
    :cond_194
    :goto_194
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->cr:Landroid/content/ContentResolver;

    #@196
    const-string v7, "auto_time"

    #@198
    invoke-static {v7}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@19b
    move-result-object v7

    #@19c
    iget-object v8, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mAutoTimeObserver:Landroid/database/ContentObserver;

    #@19e
    invoke-virtual {v4, v7, v5, v8}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@1a1
    .line 368
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->cr:Landroid/content/ContentResolver;

    #@1a3
    const-string v7, "auto_time_zone"

    #@1a5
    invoke-static {v7}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@1a8
    move-result-object v7

    #@1a9
    iget-object v8, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mAutoTimeZoneObserver:Landroid/database/ContentObserver;

    #@1ab
    invoke-virtual {v4, v7, v5, v8}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@1ae
    .line 372
    const-string v4, "VZW"

    #@1b0
    invoke-static {v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@1b3
    move-result v4

    #@1b4
    if-eqz v4, :cond_1bf

    #@1b6
    .line 373
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->cr:Landroid/content/ContentResolver;

    #@1b8
    sget-object v7, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    #@1ba
    iget-object v8, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mCdmaApnObserver:Landroid/database/ContentObserver;

    #@1bc
    invoke-virtual {v4, v7, v5, v8}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@1bf
    .line 380
    :cond_1bf
    const-string v4, "sprint_location_spec"

    #@1c1
    invoke-static {v9, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@1c4
    move-result v4

    #@1c5
    if-eqz v4, :cond_1d4

    #@1c7
    .line 381
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->cr:Landroid/content/ContentResolver;

    #@1c9
    const-string v7, "location_providers_allowed"

    #@1cb
    invoke-static {v7}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@1ce
    move-result-object v7

    #@1cf
    iget-object v8, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mCellLocationObserver:Landroid/database/ContentObserver;

    #@1d1
    invoke-virtual {v4, v7, v5, v8}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@1d4
    .line 387
    :cond_1d4
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->setSignalStrengthDefaultValues()V

    #@1d7
    .line 390
    new-instance v2, Landroid/content/IntentFilter;

    #@1d9
    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    #@1dc
    .line 391
    .local v2, filter:Landroid/content/IntentFilter;
    const-string v4, "android.intent.action.LOCALE_CHANGED"

    #@1de
    invoke-virtual {v2, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1e1
    .line 393
    const-string v4, "vzw_eri"

    #@1e3
    invoke-static {v9, v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@1e6
    move-result v4

    #@1e7
    if-eqz v4, :cond_203

    #@1e9
    .line 394
    new-instance v4, Landroid/media/MediaPlayer;

    #@1eb
    invoke-direct {v4}, Landroid/media/MediaPlayer;-><init>()V

    #@1ee
    iput-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@1f0
    .line 395
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@1f3
    move-result-object v4

    #@1f4
    const-string v7, "audio"

    #@1f6
    invoke-virtual {v4, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@1f9
    move-result-object v4

    #@1fa
    check-cast v4, Landroid/media/AudioManager;

    #@1fc
    iput-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mAudioManager:Landroid/media/AudioManager;

    #@1fe
    .line 396
    const-string v4, "android.intent.action.ACTION_SHUTDOWN"

    #@200
    invoke-virtual {v2, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@203
    .line 399
    :cond_203
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@206
    move-result-object v4

    #@207
    iget-object v7, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@209
    invoke-virtual {v4, v7, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@20c
    .line 403
    sget-object v4, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mTimeZoneMonitor:Lcom/android/internal/telephony/LgeTimeZoneMonitor;

    #@20e
    if-nez v4, :cond_216

    #@210
    .line 404
    invoke-static {}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->getDefault()Lcom/android/internal/telephony/LgeTimeZoneMonitor;

    #@213
    move-result-object v4

    #@214
    sput-object v4, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mTimeZoneMonitor:Lcom/android/internal/telephony/LgeTimeZoneMonitor;

    #@216
    .line 407
    :cond_216
    sget-object v4, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mTimeZoneMonitor:Lcom/android/internal/telephony/LgeTimeZoneMonitor;

    #@218
    if-eqz v4, :cond_224

    #@21a
    .line 408
    sget-object v4, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mTimeZoneMonitor:Lcom/android/internal/telephony/LgeTimeZoneMonitor;

    #@21c
    invoke-virtual {p0, v4, v5, v9}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->registerForNetworkAttached(Landroid/os/Handler;ILjava/lang/Object;)V

    #@21f
    .line 409
    sget-object v4, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mTimeZoneMonitor:Lcom/android/internal/telephony/LgeTimeZoneMonitor;

    #@221
    invoke-virtual {v4, p0}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->setServiceStateTracker(Lcom/android/internal/telephony/ServiceStateTracker;)V

    #@224
    .line 414
    :cond_224
    invoke-static {}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->getSavedNeedFixZone()Z

    #@227
    move-result v4

    #@228
    iput-boolean v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mNeedFixZone:Z

    #@22a
    .line 415
    iget-boolean v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mNeedFixZone:Z

    #@22c
    if-eqz v4, :cond_253

    #@22e
    .line 416
    invoke-static {}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->getSavedZoneOffset()I

    #@231
    move-result v4

    #@232
    iput v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mZoneOffset:I

    #@234
    .line 417
    invoke-static {}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->getSavedZoneDst()Z

    #@237
    move-result v4

    #@238
    iput-boolean v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mZoneDst:Z

    #@23a
    .line 418
    invoke-static {}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->getSavedZoneTime()J

    #@23d
    move-result-wide v4

    #@23e
    iput-wide v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mZoneTime:J

    #@240
    .line 419
    const-string v4, "TimeZone correction was delayed by Phone Switching!"

    #@242
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@245
    .line 422
    invoke-static {v6}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->setSavedNeedFixZone(Z)V

    #@248
    .line 423
    invoke-static {v6}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->setSavedZoneOffset(I)V

    #@24b
    .line 424
    invoke-static {v6}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->setSavedZoneDst(Z)V

    #@24e
    .line 425
    const-wide/16 v4, 0x0

    #@250
    invoke-static {v4, v5}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->setSavedZoneTime(J)V

    #@253
    .line 428
    :cond_253
    return-void

    #@254
    .end local v0           #airplaneMode:I
    .end local v2           #filter:Landroid/content/IntentFilter;
    .end local v3           #powerManager:Landroid/os/PowerManager;
    :cond_254
    move v4, v6

    #@255
    .line 324
    goto/16 :goto_db

    #@257
    .restart local v0       #airplaneMode:I
    .restart local v3       #powerManager:Landroid/os/PowerManager;
    :cond_257
    move v4, v6

    #@258
    .line 349
    goto/16 :goto_130

    #@25a
    .restart local v1       #apn2_disable_Mode:I
    :cond_25a
    move v4, v6

    #@25b
    .line 354
    goto/16 :goto_149

    #@25d
    .line 360
    :cond_25d
    const-string v4, "ril.current.apn2-disable"

    #@25f
    const-string v7, "0"

    #@261
    invoke-static {v4, v7}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@264
    .line 361
    const-string v4, "CDMA"

    #@266
    new-instance v7, Ljava/lang/StringBuilder;

    #@268
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@26b
    const-string v8, "[APN2 Disable] ril.current.apn2-disable : "

    #@26d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@270
    move-result-object v7

    #@271
    const-string v8, "ril.current.apn2-disable"

    #@273
    invoke-static {v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@276
    move-result-object v8

    #@277
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27a
    move-result-object v7

    #@27b
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27e
    move-result-object v7

    #@27f
    invoke-static {v4, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@282
    goto/16 :goto_194
.end method

.method static synthetic access$000(Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 120
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->revertToNitzTime()V

    #@3
    return-void
.end method

.method static synthetic access$100(Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 120
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->revertToNitzTimeZone()V

    #@3
    return-void
.end method

.method static synthetic access$200(Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;)Lcom/android/internal/telephony/CommandsInterface;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 120
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 120
    iget v0, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->restoreVolume:I

    #@2
    return v0
.end method

.method static synthetic access$400(Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;)Landroid/media/AudioManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 120
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mAudioManager:Landroid/media/AudioManager;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;)Landroid/media/MediaPlayer;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 120
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@2
    return-object v0
.end method

.method static synthetic access$502(Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 120
    iput-object p1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@2
    return-object p1
.end method

.method static synthetic access$600()Z
    .registers 1

    #@0
    .prologue
    .line 120
    sget-boolean v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->DBG_CALL:Z

    #@2
    return v0
.end method

.method private check1xRegDone()Z
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 2580
    new-instance v1, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v2, "check1xRegDone : mRegistrationState : "

    #@8
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v1

    #@c
    iget v2, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mRegistrationState:I

    #@e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    const-string v2, ", getRadioTechnology = "

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    iget-object v2, p0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@1a
    invoke-virtual {v2}, Landroid/telephony/ServiceState;->getRadioTechnology()I

    #@1d
    move-result v2

    #@1e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@21
    move-result-object v1

    #@22
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v1

    #@26
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@29
    .line 2582
    iget v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mRegistrationState:I

    #@2b
    if-eq v1, v0, :cond_32

    #@2d
    iget v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mRegistrationState:I

    #@2f
    const/4 v2, 0x5

    #@30
    if-ne v1, v2, :cond_3f

    #@32
    :cond_32
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@34
    invoke-virtual {v1}, Landroid/telephony/ServiceState;->getRadioTechnology()I

    #@37
    move-result v1

    #@38
    invoke-static {v1}, Landroid/telephony/ServiceState;->isCdmaFormat(I)Z

    #@3b
    move-result v1

    #@3c
    if-eqz v1, :cond_3f

    #@3e
    :goto_3e
    return v0

    #@3f
    :cond_3f
    const/4 v0, 0x0

    #@40
    goto :goto_3e
.end method

.method private findTimeZone(IZJ)Ljava/util/TimeZone;
    .registers 15
    .parameter "offset"
    .parameter "dst"
    .parameter "when"

    #@0
    .prologue
    .line 1743
    move v5, p1

    #@1
    .line 1744
    .local v5, rawOffset:I
    if-eqz p2, :cond_7

    #@3
    .line 1745
    const v9, 0x36ee80

    #@6
    sub-int/2addr v5, v9

    #@7
    .line 1747
    :cond_7
    invoke-static {v5}, Ljava/util/TimeZone;->getAvailableIDs(I)[Ljava/lang/String;

    #@a
    move-result-object v8

    #@b
    .line 1748
    .local v8, zones:[Ljava/lang/String;
    const/4 v2, 0x0

    #@c
    .line 1749
    .local v2, guess:Ljava/util/TimeZone;
    new-instance v1, Ljava/util/Date;

    #@e
    invoke-direct {v1, p3, p4}, Ljava/util/Date;-><init>(J)V

    #@11
    .line 1750
    .local v1, d:Ljava/util/Date;
    move-object v0, v8

    #@12
    .local v0, arr$:[Ljava/lang/String;
    array-length v4, v0

    #@13
    .local v4, len$:I
    const/4 v3, 0x0

    #@14
    .local v3, i$:I
    :goto_14
    if-ge v3, v4, :cond_29

    #@16
    aget-object v7, v0, v3

    #@18
    .line 1751
    .local v7, zone:Ljava/lang/String;
    invoke-static {v7}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    #@1b
    move-result-object v6

    #@1c
    .line 1752
    .local v6, tz:Ljava/util/TimeZone;
    invoke-virtual {v6, p3, p4}, Ljava/util/TimeZone;->getOffset(J)I

    #@1f
    move-result v9

    #@20
    if-ne v9, p1, :cond_2a

    #@22
    invoke-virtual {v6, v1}, Ljava/util/TimeZone;->inDaylightTime(Ljava/util/Date;)Z

    #@25
    move-result v9

    #@26
    if-ne v9, p2, :cond_2a

    #@28
    .line 1754
    move-object v2, v6

    #@29
    .line 1759
    .end local v6           #tz:Ljava/util/TimeZone;
    .end local v7           #zone:Ljava/lang/String;
    :cond_29
    return-object v2

    #@2a
    .line 1750
    .restart local v6       #tz:Ljava/util/TimeZone;
    .restart local v7       #zone:Ljava/lang/String;
    :cond_2a
    add-int/lit8 v3, v3, 0x1

    #@2c
    goto :goto_14
.end method

.method private getAutoTime()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 2107
    :try_start_1
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->cr:Landroid/content/ContentResolver;

    #@3
    const-string v3, "auto_time"

    #@5
    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_8
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_1 .. :try_end_8} :catch_e

    #@8
    move-result v2

    #@9
    if-lez v2, :cond_c

    #@b
    .line 2109
    :goto_b
    return v1

    #@c
    .line 2107
    :cond_c
    const/4 v1, 0x0

    #@d
    goto :goto_b

    #@e
    .line 2108
    :catch_e
    move-exception v0

    #@f
    .line 2109
    .local v0, snfe:Landroid/provider/Settings$SettingNotFoundException;
    goto :goto_b
.end method

.method private getAutoTimeZone()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 2115
    :try_start_1
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->cr:Landroid/content/ContentResolver;

    #@3
    const-string v3, "auto_time_zone"

    #@5
    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_8
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_1 .. :try_end_8} :catch_e

    #@8
    move-result v2

    #@9
    if-lez v2, :cond_c

    #@b
    .line 2117
    :goto_b
    return v1

    #@c
    .line 2115
    :cond_c
    const/4 v1, 0x0

    #@d
    goto :goto_b

    #@e
    .line 2116
    :catch_e
    move-exception v0

    #@f
    .line 2117
    .local v0, snfe:Landroid/provider/Settings$SettingNotFoundException;
    goto :goto_b
.end method

.method private getNitzTimeZone(IZJ)Ljava/util/TimeZone;
    .registers 8
    .parameter "offset"
    .parameter "dst"
    .parameter "when"

    #@0
    .prologue
    .line 1733
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->findTimeZone(IZJ)Ljava/util/TimeZone;

    #@3
    move-result-object v0

    #@4
    .line 1734
    .local v0, guess:Ljava/util/TimeZone;
    if-nez v0, :cond_d

    #@6
    .line 1736
    if-nez p2, :cond_27

    #@8
    const/4 v1, 0x1

    #@9
    :goto_9
    invoke-direct {p0, p1, v1, p3, p4}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->findTimeZone(IZJ)Ljava/util/TimeZone;

    #@c
    move-result-object v0

    #@d
    .line 1738
    :cond_d
    new-instance v1, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v2, "getNitzTimeZone returning "

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    if-nez v0, :cond_29

    #@1a
    move-object v1, v0

    #@1b
    :goto_1b
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@26
    .line 1739
    return-object v0

    #@27
    .line 1736
    :cond_27
    const/4 v1, 0x0

    #@28
    goto :goto_9

    #@29
    .line 1738
    :cond_29
    invoke-virtual {v0}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    #@2c
    move-result-object v1

    #@2d
    goto :goto_1b
.end method

.method private getSubscriptionInfoAndStartPollingThreads()V
    .registers 3

    #@0
    .prologue
    .line 522
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    const/16 v1, 0x22

    #@4
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->obtainMessage(I)Landroid/os/Message;

    #@7
    move-result-object v1

    #@8
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/CommandsInterface;->getCDMASubscription(Landroid/os/Message;)V

    #@b
    .line 525
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->pollState()V

    #@e
    .line 526
    return-void
.end method

.method private handleApnChanged()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 2588
    const-string v0, "VZW"

    #@4
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_3a

    #@a
    .line 2589
    const-string v0, "handleApnChanged()"

    #@c
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@f
    .line 2590
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isVZWAdminDisabled()Z

    #@12
    move-result v0

    #@13
    if-eqz v0, :cond_24

    #@15
    .line 2591
    const-string v0, "VZW APN Disabled"

    #@17
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@1a
    .line 2592
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->cr:Landroid/content/ContentResolver;

    #@1c
    const-string v1, "apn2_disable"

    #@1e
    invoke-static {v0, v1, v3}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@21
    .line 2593
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->setRadioPower(Z)V

    #@24
    .line 2595
    :cond_24
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isVZWAdminEnabled()Z

    #@27
    move-result v0

    #@28
    if-eqz v0, :cond_39

    #@2a
    .line 2596
    const-string v0, "VZW APN Enabled"

    #@2c
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@2f
    .line 2597
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->cr:Landroid/content/ContentResolver;

    #@31
    const-string v1, "apn2_disable"

    #@33
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@36
    .line 2598
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->setRadioPower(Z)V

    #@39
    .line 2603
    :cond_39
    :goto_39
    return-void

    #@3a
    .line 2601
    :cond_3a
    const-string v0, "Igone handleApnChanged()..."

    #@3c
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@3f
    goto :goto_39
.end method

.method private handleCdmaSubscriptionSource(I)V
    .registers 4
    .parameter "newSubscriptionSource"

    #@0
    .prologue
    .line 857
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "Subscription Source : "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@16
    .line 858
    if-nez p1, :cond_2c

    #@18
    const/4 v0, 0x1

    #@19
    :goto_19
    iput-boolean v0, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isSubscriptionFromRuim:Z

    #@1b
    .line 860
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->saveCdmaSubscriptionSource(I)V

    #@1e
    .line 861
    iget-boolean v0, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isSubscriptionFromRuim:Z

    #@20
    if-nez v0, :cond_2b

    #@22
    .line 863
    const/16 v0, 0x23

    #@24
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->obtainMessage(I)Landroid/os/Message;

    #@27
    move-result-object v0

    #@28
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->sendMessage(Landroid/os/Message;)Z

    #@2b
    .line 865
    :cond_2b
    return-void

    #@2c
    .line 858
    :cond_2c
    const/4 v0, 0x0

    #@2d
    goto :goto_19
.end method

.method private isHomeSid(I)Z
    .registers 4
    .parameter "sid"

    #@0
    .prologue
    .line 2196
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mHomeSystemId:[I

    #@2
    if-eqz v1, :cond_15

    #@4
    .line 2197
    const/4 v0, 0x0

    #@5
    .local v0, i:I
    :goto_5
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mHomeSystemId:[I

    #@7
    array-length v1, v1

    #@8
    if-ge v0, v1, :cond_15

    #@a
    .line 2198
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mHomeSystemId:[I

    #@c
    aget v1, v1, v0

    #@e
    if-ne p1, v1, :cond_12

    #@10
    .line 2199
    const/4 v1, 0x1

    #@11
    .line 2203
    .end local v0           #i:I
    :goto_11
    return v1

    #@12
    .line 2197
    .restart local v0       #i:I
    :cond_12
    add-int/lit8 v0, v0, 0x1

    #@14
    goto :goto_5

    #@15
    .line 2203
    .end local v0           #i:I
    :cond_15
    const/4 v1, 0x0

    #@16
    goto :goto_11
.end method

.method private isOemRadTestSettingTrue()Z
    .registers 6

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 2499
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@3
    invoke-virtual {v2}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@6
    move-result-object v2

    #@7
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@a
    move-result-object v2

    #@b
    const-string v3, "oem_rad_test"

    #@d
    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@10
    move-result v0

    #@11
    .line 2501
    .local v0, oemRadSettingValue:I
    const-string v2, "CDMA"

    #@13
    new-instance v3, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v4, "isOemRadTestSettingTrue : Settings.Secure.OEM_RAD_TEST = "

    #@1a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@21
    move-result-object v3

    #@22
    const-string v4, ", phone.getPhoneType() ="

    #@24
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v3

    #@28
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@2a
    invoke-virtual {v4}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getPhoneType()I

    #@2d
    move-result v4

    #@2e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@31
    move-result-object v3

    #@32
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v3

    #@36
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@39
    .line 2505
    if-lez v0, :cond_3c

    #@3b
    .line 2506
    const/4 v1, 0x1

    #@3c
    .line 2508
    :cond_3c
    return v1
.end method

.method private isRoamIndForHomeSystem(Ljava/lang/String;)Z
    .registers 10
    .parameter "roamInd"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 1853
    const-string v6, "ro.cdma.homesystem"

    #@3
    invoke-static {v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v2

    #@7
    .line 1855
    .local v2, homeRoamIndicators:Ljava/lang/String;
    const/4 v6, 0x0

    #@8
    const-string v7, "vzw_eri"

    #@a
    invoke-static {v6, v7}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@d
    move-result v6

    #@e
    if-eqz v6, :cond_1c

    #@10
    .line 1856
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@13
    move-result v6

    #@14
    if-eqz v6, :cond_36

    #@16
    .line 1857
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@18
    invoke-virtual {v6}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getCdmaEriHomeSystems()Ljava/lang/String;

    #@1b
    move-result-object v2

    #@1c
    .line 1864
    :cond_1c
    :goto_1c
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@1f
    move-result v6

    #@20
    if-nez v6, :cond_35

    #@22
    .line 1867
    const-string v6, ","

    #@24
    invoke-virtual {v2, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@27
    move-result-object v0

    #@28
    .local v0, arr$:[Ljava/lang/String;
    array-length v4, v0

    #@29
    .local v4, len$:I
    const/4 v3, 0x0

    #@2a
    .local v3, i$:I
    :goto_2a
    if-ge v3, v4, :cond_35

    #@2c
    aget-object v1, v0, v3

    #@2e
    .line 1868
    .local v1, homeRoamInd:Ljava/lang/String;
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@31
    move-result v6

    #@32
    if-eqz v6, :cond_61

    #@34
    .line 1869
    const/4 v5, 0x1

    #@35
    .line 1877
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #homeRoamInd:Ljava/lang/String;
    .end local v3           #i$:I
    .end local v4           #len$:I
    :cond_35
    return v5

    #@36
    .line 1859
    :cond_36
    new-instance v6, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v6

    #@3f
    const-string v7, ","

    #@41
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v6

    #@45
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v2

    #@49
    .line 1860
    new-instance v6, Ljava/lang/StringBuilder;

    #@4b
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@4e
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v6

    #@52
    iget-object v7, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@54
    invoke-virtual {v7}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getCdmaEriHomeSystems()Ljava/lang/String;

    #@57
    move-result-object v7

    #@58
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v6

    #@5c
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5f
    move-result-object v2

    #@60
    goto :goto_1c

    #@61
    .line 1867
    .restart local v0       #arr$:[Ljava/lang/String;
    .restart local v1       #homeRoamInd:Ljava/lang/String;
    .restart local v3       #i$:I
    .restart local v4       #len$:I
    :cond_61
    add-int/lit8 v3, v3, 0x1

    #@63
    goto :goto_2a
.end method

.method private isRoamingBetweenOperators(ZLandroid/telephony/ServiceState;)Z
    .registers 12
    .parameter "cdmaRoaming"
    .parameter "s"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 1893
    const-string v7, "gsm.sim.operator.alpha"

    #@4
    const-string v8, "empty"

    #@6
    invoke-virtual {p0, v7, v8}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->getSystemProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@9
    move-result-object v4

    #@a
    .line 1897
    .local v4, spn:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/telephony/ServiceState;->getOperatorAlphaLong()Ljava/lang/String;

    #@d
    move-result-object v2

    #@e
    .line 1898
    .local v2, onsl:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/telephony/ServiceState;->getOperatorAlphaShort()Ljava/lang/String;

    #@11
    move-result-object v3

    #@12
    .line 1900
    .local v3, onss:Ljava/lang/String;
    if-eqz v2, :cond_2b

    #@14
    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@17
    move-result v7

    #@18
    if-eqz v7, :cond_2b

    #@1a
    move v0, v5

    #@1b
    .line 1901
    .local v0, equalsOnsl:Z
    :goto_1b
    if-eqz v3, :cond_2d

    #@1d
    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@20
    move-result v7

    #@21
    if-eqz v7, :cond_2d

    #@23
    move v1, v5

    #@24
    .line 1903
    .local v1, equalsOnss:Z
    :goto_24
    if-eqz p1, :cond_2f

    #@26
    if-nez v0, :cond_2f

    #@28
    if-nez v1, :cond_2f

    #@2a
    :goto_2a
    return v5

    #@2b
    .end local v0           #equalsOnsl:Z
    .end local v1           #equalsOnss:Z
    :cond_2b
    move v0, v6

    #@2c
    .line 1900
    goto :goto_1b

    #@2d
    .restart local v0       #equalsOnsl:Z
    :cond_2d
    move v1, v6

    #@2e
    .line 1901
    goto :goto_24

    #@2f
    .restart local v1       #equalsOnss:Z
    :cond_2f
    move v5, v6

    #@30
    .line 1903
    goto :goto_2a
.end method

.method private isVZWAdminDisabled()Z
    .registers 10

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 2607
    const/4 v7, 0x0

    #@2
    .line 2608
    .local v7, isDisabled:Z
    const/4 v3, 0x0

    #@3
    .line 2609
    .local v3, selection:Ljava/lang/String;
    const-string v0, "gsm.sim.operator.numeric"

    #@5
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@8
    move-result-object v8

    #@9
    .line 2611
    .local v8, operator:Ljava/lang/String;
    const-string v0, "311480"

    #@b
    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e
    move-result v0

    #@f
    if-nez v0, :cond_13

    #@11
    .line 2612
    const/4 v0, 0x0

    #@12
    .line 2631
    :goto_12
    return v0

    #@13
    .line 2615
    :cond_13
    new-instance v0, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v1, "numeric = \'"

    #@1a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v0

    #@1e
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v0

    #@22
    const-string v1, "\'"

    #@24
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v0

    #@28
    const-string v1, " and type = \'admin\'"

    #@2a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v0

    #@2e
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v3

    #@32
    .line 2616
    new-instance v0, Ljava/lang/StringBuilder;

    #@34
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@37
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v0

    #@3b
    const-string v1, " and carrier_enabled = 1"

    #@3d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v0

    #@41
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v3

    #@45
    .line 2618
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@47
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@4a
    move-result-object v0

    #@4b
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@4e
    move-result-object v0

    #@4f
    sget-object v1, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    #@51
    move-object v4, v2

    #@52
    move-object v5, v2

    #@53
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@56
    move-result-object v6

    #@57
    .line 2621
    .local v6, cursor:Landroid/database/Cursor;
    if-eqz v6, :cond_63

    #@59
    .line 2622
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    #@5c
    move-result v0

    #@5d
    if-lez v0, :cond_7b

    #@5f
    .line 2623
    const/4 v7, 0x0

    #@60
    .line 2627
    :goto_60
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@63
    .line 2630
    :cond_63
    new-instance v0, Ljava/lang/StringBuilder;

    #@65
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@68
    const-string v1, "isVZWAdminDisabled() isDisabled = "

    #@6a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v0

    #@6e
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@71
    move-result-object v0

    #@72
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@75
    move-result-object v0

    #@76
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@79
    move v0, v7

    #@7a
    .line 2631
    goto :goto_12

    #@7b
    .line 2625
    :cond_7b
    const/4 v7, 0x1

    #@7c
    goto :goto_60
.end method

.method private isVZWAdminEnabled()Z
    .registers 10

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 2635
    const/4 v7, 0x0

    #@2
    .line 2636
    .local v7, isEnabled:Z
    const/4 v3, 0x0

    #@3
    .line 2637
    .local v3, selection:Ljava/lang/String;
    const-string v0, "gsm.sim.operator.numeric"

    #@5
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@8
    move-result-object v8

    #@9
    .line 2639
    .local v8, operator:Ljava/lang/String;
    const-string v0, "311480"

    #@b
    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e
    move-result v0

    #@f
    if-nez v0, :cond_13

    #@11
    .line 2640
    const/4 v0, 0x0

    #@12
    .line 2659
    :goto_12
    return v0

    #@13
    .line 2643
    :cond_13
    new-instance v0, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v1, "numeric = \'"

    #@1a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v0

    #@1e
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v0

    #@22
    const-string v1, "\'"

    #@24
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v0

    #@28
    const-string v1, " and type = \'admin\'"

    #@2a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v0

    #@2e
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v3

    #@32
    .line 2644
    new-instance v0, Ljava/lang/StringBuilder;

    #@34
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@37
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v0

    #@3b
    const-string v1, " and carrier_enabled = 0"

    #@3d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v0

    #@41
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v3

    #@45
    .line 2646
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@47
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@4a
    move-result-object v0

    #@4b
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@4e
    move-result-object v0

    #@4f
    sget-object v1, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    #@51
    move-object v4, v2

    #@52
    move-object v5, v2

    #@53
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@56
    move-result-object v6

    #@57
    .line 2649
    .local v6, cursor:Landroid/database/Cursor;
    if-eqz v6, :cond_63

    #@59
    .line 2650
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    #@5c
    move-result v0

    #@5d
    if-lez v0, :cond_7b

    #@5f
    .line 2651
    const/4 v7, 0x0

    #@60
    .line 2655
    :goto_60
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@63
    .line 2658
    :cond_63
    new-instance v0, Ljava/lang/StringBuilder;

    #@65
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@68
    const-string v1, "isVZWAdminEnabled() isEnabled = "

    #@6a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v0

    #@6e
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@71
    move-result-object v0

    #@72
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@75
    move-result-object v0

    #@76
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@79
    move v0, v7

    #@7a
    .line 2659
    goto :goto_12

    #@7b
    .line 2653
    :cond_7b
    const/4 v7, 0x1

    #@7c
    goto :goto_60
.end method

.method private queueNextSignalStrengthPoll()V
    .registers 4

    #@0
    .prologue
    .line 1770
    iget-boolean v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->dontPollSignalStrength:Z

    #@2
    if-eqz v1, :cond_5

    #@4
    .line 1783
    :goto_4
    return-void

    #@5
    .line 1778
    :cond_5
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->obtainMessage()Landroid/os/Message;

    #@8
    move-result-object v0

    #@9
    .line 1779
    .local v0, msg:Landroid/os/Message;
    const/16 v1, 0xa

    #@b
    iput v1, v0, Landroid/os/Message;->what:I

    #@d
    .line 1782
    const-wide/16 v1, 0x4e20

    #@f
    invoke-virtual {p0, v0, v1, v2}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@12
    goto :goto_4
.end method

.method private revertToNitzTime()V
    .registers 7

    #@0
    .prologue
    const-wide/16 v3, 0x0

    #@2
    .line 2158
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->cr:Landroid/content/ContentResolver;

    #@4
    const-string v1, "auto_time"

    #@6
    const/4 v2, 0x0

    #@7
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@a
    move-result v0

    #@b
    if-nez v0, :cond_e

    #@d
    .line 2168
    :cond_d
    :goto_d
    return-void

    #@e
    .line 2162
    :cond_e
    new-instance v0, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v1, "revertToNitzTime: mSavedTime="

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    iget-wide v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mSavedTime:J

    #@1b
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v0

    #@1f
    const-string v1, " mSavedAtTime="

    #@21
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v0

    #@25
    iget-wide v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mSavedAtTime:J

    #@27
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v0

    #@2b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v0

    #@2f
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@32
    .line 2164
    iget-wide v0, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mSavedTime:J

    #@34
    cmp-long v0, v0, v3

    #@36
    if-eqz v0, :cond_d

    #@38
    iget-wide v0, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mSavedAtTime:J

    #@3a
    cmp-long v0, v0, v3

    #@3c
    if-eqz v0, :cond_d

    #@3e
    .line 2165
    iget-wide v0, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mSavedTime:J

    #@40
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@43
    move-result-wide v2

    #@44
    iget-wide v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mSavedAtTime:J

    #@46
    sub-long/2addr v2, v4

    #@47
    add-long/2addr v0, v2

    #@48
    invoke-direct {p0, v0, v1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->setAndBroadcastNetworkSetTime(J)V

    #@4b
    goto :goto_d
.end method

.method private revertToNitzTimeZone()V
    .registers 4

    #@0
    .prologue
    .line 2171
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@9
    move-result-object v0

    #@a
    const-string v1, "auto_time_zone"

    #@c
    const/4 v2, 0x0

    #@d
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@10
    move-result v0

    #@11
    if-nez v0, :cond_14

    #@13
    .line 2179
    :cond_13
    :goto_13
    return-void

    #@14
    .line 2175
    :cond_14
    new-instance v0, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v1, "revertToNitzTimeZone: tz=\'"

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v0

    #@1f
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mSavedTimeZone:Ljava/lang/String;

    #@21
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v0

    #@25
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v0

    #@29
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@2c
    .line 2176
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mSavedTimeZone:Ljava/lang/String;

    #@2e
    if-eqz v0, :cond_13

    #@30
    .line 2177
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mSavedTimeZone:Ljava/lang/String;

    #@32
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->setAndBroadcastNetworkSetTimeZone(Ljava/lang/String;)V

    #@35
    goto :goto_13
.end method

.method private saveCdmaSubscriptionSource(I)V
    .registers 4
    .parameter "source"

    #@0
    .prologue
    .line 515
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "Storing cdma subscription source: "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@16
    .line 516
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@18
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@1b
    move-result-object v0

    #@1c
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1f
    move-result-object v0

    #@20
    const-string v1, "subscription_mode"

    #@22
    invoke-static {v0, v1, p1}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@25
    .line 519
    return-void
.end method

.method private saveNitzTimeZone(Ljava/lang/String;)V
    .registers 2
    .parameter "zoneId"

    #@0
    .prologue
    .line 2122
    iput-object p1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mSavedTimeZone:Ljava/lang/String;

    #@2
    .line 2123
    return-void
.end method

.method private setAndBroadcastNetworkSetTime(J)V
    .registers 6
    .parameter "time"

    #@0
    .prologue
    .line 2149
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v2, "setAndBroadcastNetworkSetTime: time="

    #@7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    const-string v2, "ms"

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@1c
    .line 2150
    invoke-static {p1, p2}, Landroid/os/SystemClock;->setCurrentTimeMillis(J)Z

    #@1f
    .line 2151
    new-instance v0, Landroid/content/Intent;

    #@21
    const-string v1, "android.intent.action.NETWORK_SET_TIME"

    #@23
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@26
    .line 2152
    .local v0, intent:Landroid/content/Intent;
    const/high16 v1, 0x2000

    #@28
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@2b
    .line 2153
    const-string v1, "time"

    #@2d
    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    #@30
    .line 2154
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@32
    invoke-virtual {v1}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@35
    move-result-object v1

    #@36
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@38
    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@3b
    .line 2155
    return-void
.end method

.method private setAndBroadcastNetworkSetTimeZone(Ljava/lang/String;)V
    .registers 6
    .parameter "zoneId"

    #@0
    .prologue
    .line 2132
    new-instance v2, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v3, "setAndBroadcastNetworkSetTimeZone: setTimeZone="

    #@7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v2

    #@b
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@16
    .line 2133
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@18
    invoke-virtual {v2}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@1b
    move-result-object v2

    #@1c
    const-string v3, "alarm"

    #@1e
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@21
    move-result-object v0

    #@22
    check-cast v0, Landroid/app/AlarmManager;

    #@24
    .line 2135
    .local v0, alarm:Landroid/app/AlarmManager;
    invoke-virtual {v0, p1}, Landroid/app/AlarmManager;->setTimeZone(Ljava/lang/String;)V

    #@27
    .line 2136
    new-instance v1, Landroid/content/Intent;

    #@29
    const-string v2, "android.intent.action.NETWORK_SET_TIMEZONE"

    #@2b
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@2e
    .line 2137
    .local v1, intent:Landroid/content/Intent;
    const/high16 v2, 0x2000

    #@30
    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@33
    .line 2138
    const-string v2, "time-zone"

    #@35
    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@38
    .line 2139
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@3a
    invoke-virtual {v2}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@3d
    move-result-object v2

    #@3e
    sget-object v3, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@40
    invoke-virtual {v2, v1, v3}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@43
    .line 2140
    return-void
.end method

.method private setTimeFromNITZString(Ljava/lang/String;J)V
    .registers 41
    .parameter "nitz"
    .parameter "nitzReceiveTime"

    #@0
    .prologue
    .line 1917
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@3
    move-result-wide v25

    #@4
    .line 1919
    .local v25, start:J
    new-instance v33, Ljava/lang/StringBuilder;

    #@6
    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v34, "NITZ: "

    #@b
    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v33

    #@f
    move-object/from16 v0, v33

    #@11
    move-object/from16 v1, p1

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v33

    #@17
    const-string v34, ","

    #@19
    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v33

    #@1d
    move-object/from16 v0, v33

    #@1f
    move-wide/from16 v1, p2

    #@21
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@24
    move-result-object v33

    #@25
    const-string v34, " start="

    #@27
    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v33

    #@2b
    move-object/from16 v0, v33

    #@2d
    move-wide/from16 v1, v25

    #@2f
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@32
    move-result-object v33

    #@33
    const-string v34, " delay="

    #@35
    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v33

    #@39
    sub-long v34, v25, p2

    #@3b
    invoke-virtual/range {v33 .. v35}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v33

    #@3f
    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v33

    #@43
    move-object/from16 v0, p0

    #@45
    move-object/from16 v1, v33

    #@47
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@4a
    .line 1926
    :try_start_4a
    const-string v33, "GMT"

    #@4c
    invoke-static/range {v33 .. v33}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    #@4f
    move-result-object v33

    #@50
    invoke-static/range {v33 .. v33}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    #@53
    move-result-object v5

    #@54
    .line 1928
    .local v5, c:Ljava/util/Calendar;
    invoke-virtual {v5}, Ljava/util/Calendar;->clear()V

    #@57
    .line 1929
    const/16 v33, 0x10

    #@59
    const/16 v34, 0x0

    #@5b
    move/from16 v0, v33

    #@5d
    move/from16 v1, v34

    #@5f
    invoke-virtual {v5, v0, v1}, Ljava/util/Calendar;->set(II)V

    #@62
    .line 1931
    const-string v33, "[/:,+-]"

    #@64
    move-object/from16 v0, p1

    #@66
    move-object/from16 v1, v33

    #@68
    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@6b
    move-result-object v20

    #@6c
    .line 1933
    .local v20, nitzSubs:[Ljava/lang/String;
    const/16 v33, 0x0

    #@6e
    aget-object v33, v20, v33

    #@70
    invoke-static/range {v33 .. v33}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@73
    move-result v33

    #@74
    move/from16 v0, v33

    #@76
    add-int/lit16 v0, v0, 0x7d0

    #@78
    move/from16 v31, v0

    #@7a
    .line 1934
    .local v31, year:I
    const/16 v33, 0x1

    #@7c
    move/from16 v0, v33

    #@7e
    move/from16 v1, v31

    #@80
    invoke-virtual {v5, v0, v1}, Ljava/util/Calendar;->set(II)V

    #@83
    .line 1937
    const/16 v33, 0x1

    #@85
    aget-object v33, v20, v33

    #@87
    invoke-static/range {v33 .. v33}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@8a
    move-result v33

    #@8b
    add-int/lit8 v19, v33, -0x1

    #@8d
    .line 1938
    .local v19, month:I
    const/16 v33, 0x2

    #@8f
    move/from16 v0, v33

    #@91
    move/from16 v1, v19

    #@93
    invoke-virtual {v5, v0, v1}, Ljava/util/Calendar;->set(II)V

    #@96
    .line 1940
    const/16 v33, 0x2

    #@98
    aget-object v33, v20, v33

    #@9a
    invoke-static/range {v33 .. v33}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@9d
    move-result v6

    #@9e
    .line 1941
    .local v6, date:I
    const/16 v33, 0x5

    #@a0
    move/from16 v0, v33

    #@a2
    invoke-virtual {v5, v0, v6}, Ljava/util/Calendar;->set(II)V

    #@a5
    .line 1943
    const/16 v33, 0x3

    #@a7
    aget-object v33, v20, v33

    #@a9
    invoke-static/range {v33 .. v33}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@ac
    move-result v13

    #@ad
    .line 1944
    .local v13, hour:I
    const/16 v33, 0xa

    #@af
    move/from16 v0, v33

    #@b1
    invoke-virtual {v5, v0, v13}, Ljava/util/Calendar;->set(II)V

    #@b4
    .line 1946
    const/16 v33, 0x4

    #@b6
    aget-object v33, v20, v33

    #@b8
    invoke-static/range {v33 .. v33}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@bb
    move-result v18

    #@bc
    .line 1947
    .local v18, minute:I
    const/16 v33, 0xc

    #@be
    move/from16 v0, v33

    #@c0
    move/from16 v1, v18

    #@c2
    invoke-virtual {v5, v0, v1}, Ljava/util/Calendar;->set(II)V

    #@c5
    .line 1949
    const/16 v33, 0x5

    #@c7
    aget-object v33, v20, v33

    #@c9
    invoke-static/range {v33 .. v33}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@cc
    move-result v23

    #@cd
    .line 1950
    .local v23, second:I
    const/16 v33, 0xd

    #@cf
    move/from16 v0, v33

    #@d1
    move/from16 v1, v23

    #@d3
    invoke-virtual {v5, v0, v1}, Ljava/util/Calendar;->set(II)V

    #@d6
    .line 1952
    const/16 v33, 0x2d

    #@d8
    move-object/from16 v0, p1

    #@da
    move/from16 v1, v33

    #@dc
    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    #@df
    move-result v33

    #@e0
    const/16 v34, -0x1

    #@e2
    move/from16 v0, v33

    #@e4
    move/from16 v1, v34

    #@e6
    if-ne v0, v1, :cond_24c

    #@e8
    const/16 v24, 0x1

    #@ea
    .line 1954
    .local v24, sign:Z
    :goto_ea
    const/16 v33, 0x6

    #@ec
    aget-object v33, v20, v33

    #@ee
    invoke-static/range {v33 .. v33}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@f1
    move-result v29

    #@f2
    .line 1956
    .local v29, tzOffset:I
    move-object/from16 v0, v20

    #@f4
    array-length v0, v0

    #@f5
    move/from16 v33, v0

    #@f7
    const/16 v34, 0x8

    #@f9
    move/from16 v0, v33

    #@fb
    move/from16 v1, v34

    #@fd
    if-lt v0, v1, :cond_250

    #@ff
    const/16 v33, 0x7

    #@101
    aget-object v33, v20, v33

    #@103
    invoke-static/range {v33 .. v33}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@106
    move-result v7

    #@107
    .line 1966
    .local v7, dst:I
    :goto_107
    if-eqz v24, :cond_253

    #@109
    const/16 v33, 0x1

    #@10b
    :goto_10b
    mul-int v33, v33, v29

    #@10d
    mul-int/lit8 v33, v33, 0xf

    #@10f
    mul-int/lit8 v33, v33, 0x3c

    #@111
    move/from16 v0, v33

    #@113
    mul-int/lit16 v0, v0, 0x3e8

    #@115
    move/from16 v29, v0

    #@117
    .line 1968
    const/16 v32, 0x0

    #@119
    .line 1974
    .local v32, zone:Ljava/util/TimeZone;
    move-object/from16 v0, v20

    #@11b
    array-length v0, v0

    #@11c
    move/from16 v33, v0

    #@11e
    const/16 v34, 0x9

    #@120
    move/from16 v0, v33

    #@122
    move/from16 v1, v34

    #@124
    if-lt v0, v1, :cond_136

    #@126
    .line 1975
    const/16 v33, 0x8

    #@128
    aget-object v33, v20, v33

    #@12a
    const/16 v34, 0x21

    #@12c
    const/16 v35, 0x2f

    #@12e
    invoke-virtual/range {v33 .. v35}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    #@131
    move-result-object v30

    #@132
    .line 1976
    .local v30, tzname:Ljava/lang/String;
    invoke-static/range {v30 .. v30}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    #@135
    move-result-object v32

    #@136
    .line 1979
    .end local v30           #tzname:Ljava/lang/String;
    :cond_136
    const-string v33, "gsm.operator.iso-country"

    #@138
    const-string v34, ""

    #@13a
    move-object/from16 v0, p0

    #@13c
    move-object/from16 v1, v33

    #@13e
    move-object/from16 v2, v34

    #@140
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->getSystemProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@143
    move-result-object v15

    #@144
    .line 1981
    .local v15, iso:Ljava/lang/String;
    if-nez v32, :cond_168

    #@146
    .line 1982
    move-object/from16 v0, p0

    #@148
    iget-boolean v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mGotCountryCode:Z

    #@14a
    move/from16 v33, v0

    #@14c
    if-eqz v33, :cond_168

    #@14e
    .line 1983
    if-eqz v15, :cond_25b

    #@150
    invoke-virtual {v15}, Ljava/lang/String;->length()I

    #@153
    move-result v33

    #@154
    if-lez v33, :cond_25b

    #@156
    .line 1984
    if-eqz v7, :cond_257

    #@158
    const/16 v33, 0x1

    #@15a
    :goto_15a
    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    #@15d
    move-result-wide v34

    #@15e
    move/from16 v0, v29

    #@160
    move/from16 v1, v33

    #@162
    move-wide/from16 v2, v34

    #@164
    invoke-static {v0, v1, v2, v3, v15}, Landroid/util/TimeUtils;->getTimeZone(IZJLjava/lang/String;)Ljava/util/TimeZone;

    #@167
    move-result-object v32

    #@168
    .line 1997
    :cond_168
    :goto_168
    if-eqz v32, :cond_186

    #@16a
    move-object/from16 v0, p0

    #@16c
    iget v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mZoneOffset:I

    #@16e
    move/from16 v33, v0

    #@170
    move/from16 v0, v33

    #@172
    move/from16 v1, v29

    #@174
    if-ne v0, v1, :cond_186

    #@176
    move-object/from16 v0, p0

    #@178
    iget-boolean v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mZoneDst:Z

    #@17a
    move/from16 v34, v0

    #@17c
    if-eqz v7, :cond_274

    #@17e
    const/16 v33, 0x1

    #@180
    :goto_180
    move/from16 v0, v34

    #@182
    move/from16 v1, v33

    #@184
    if-eq v0, v1, :cond_1a8

    #@186
    .line 2002
    :cond_186
    const/16 v33, 0x1

    #@188
    move/from16 v0, v33

    #@18a
    move-object/from16 v1, p0

    #@18c
    iput-boolean v0, v1, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mNeedFixZone:Z

    #@18e
    .line 2003
    move/from16 v0, v29

    #@190
    move-object/from16 v1, p0

    #@192
    iput v0, v1, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mZoneOffset:I

    #@194
    .line 2004
    if-eqz v7, :cond_278

    #@196
    const/16 v33, 0x1

    #@198
    :goto_198
    move/from16 v0, v33

    #@19a
    move-object/from16 v1, p0

    #@19c
    iput-boolean v0, v1, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mZoneDst:Z

    #@19e
    .line 2005
    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    #@1a1
    move-result-wide v33

    #@1a2
    move-wide/from16 v0, v33

    #@1a4
    move-object/from16 v2, p0

    #@1a6
    iput-wide v0, v2, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mZoneTime:J

    #@1a8
    .line 2008
    :cond_1a8
    new-instance v33, Ljava/lang/StringBuilder;

    #@1aa
    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    #@1ad
    const-string v34, "NITZ: tzOffset="

    #@1af
    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b2
    move-result-object v33

    #@1b3
    move-object/from16 v0, v33

    #@1b5
    move/from16 v1, v29

    #@1b7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1ba
    move-result-object v33

    #@1bb
    const-string v34, " dst="

    #@1bd
    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c0
    move-result-object v33

    #@1c1
    move-object/from16 v0, v33

    #@1c3
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c6
    move-result-object v33

    #@1c7
    const-string v34, " zone="

    #@1c9
    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1cc
    move-result-object v34

    #@1cd
    if-eqz v32, :cond_27c

    #@1cf
    invoke-virtual/range {v32 .. v32}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    #@1d2
    move-result-object v33

    #@1d3
    :goto_1d3
    move-object/from16 v0, v34

    #@1d5
    move-object/from16 v1, v33

    #@1d7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1da
    move-result-object v33

    #@1db
    const-string v34, " iso="

    #@1dd
    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e0
    move-result-object v33

    #@1e1
    move-object/from16 v0, v33

    #@1e3
    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e6
    move-result-object v33

    #@1e7
    const-string v34, " mGotCountryCode="

    #@1e9
    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ec
    move-result-object v33

    #@1ed
    move-object/from16 v0, p0

    #@1ef
    iget-boolean v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mGotCountryCode:Z

    #@1f1
    move/from16 v34, v0

    #@1f3
    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1f6
    move-result-object v33

    #@1f7
    const-string v34, " mNeedFixZone="

    #@1f9
    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1fc
    move-result-object v33

    #@1fd
    move-object/from16 v0, p0

    #@1ff
    iget-boolean v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mNeedFixZone:Z

    #@201
    move/from16 v34, v0

    #@203
    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@206
    move-result-object v33

    #@207
    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20a
    move-result-object v33

    #@20b
    move-object/from16 v0, p0

    #@20d
    move-object/from16 v1, v33

    #@20f
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@212
    .line 2014
    if-eqz v32, :cond_230

    #@214
    .line 2015
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->getAutoTimeZone()Z

    #@217
    move-result v33

    #@218
    if-eqz v33, :cond_225

    #@21a
    .line 2016
    invoke-virtual/range {v32 .. v32}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    #@21d
    move-result-object v33

    #@21e
    move-object/from16 v0, p0

    #@220
    move-object/from16 v1, v33

    #@222
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->setAndBroadcastNetworkSetTimeZone(Ljava/lang/String;)V

    #@225
    .line 2018
    :cond_225
    invoke-virtual/range {v32 .. v32}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    #@228
    move-result-object v33

    #@229
    move-object/from16 v0, p0

    #@22b
    move-object/from16 v1, v33

    #@22d
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->saveNitzTimeZone(Ljava/lang/String;)V

    #@230
    .line 2021
    :cond_230
    const-string v33, "gsm.ignore-nitz"

    #@232
    invoke-static/range {v33 .. v33}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@235
    move-result-object v14

    #@236
    .line 2022
    .local v14, ignore:Ljava/lang/String;
    if-eqz v14, :cond_280

    #@238
    const-string v33, "yes"

    #@23a
    move-object/from16 v0, v33

    #@23c
    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23f
    move-result v33

    #@240
    if-eqz v33, :cond_280

    #@242
    .line 2023
    const-string v33, "NITZ: Not setting clock because gsm.ignore-nitz is set"

    #@244
    move-object/from16 v0, p0

    #@246
    move-object/from16 v1, v33

    #@248
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@24b
    .line 2103
    .end local v5           #c:Ljava/util/Calendar;
    .end local v6           #date:I
    .end local v7           #dst:I
    .end local v13           #hour:I
    .end local v14           #ignore:Ljava/lang/String;
    .end local v15           #iso:Ljava/lang/String;
    .end local v18           #minute:I
    .end local v19           #month:I
    .end local v20           #nitzSubs:[Ljava/lang/String;
    .end local v23           #second:I
    .end local v24           #sign:Z
    .end local v29           #tzOffset:I
    .end local v31           #year:I
    .end local v32           #zone:Ljava/util/TimeZone;
    :goto_24b
    return-void

    #@24c
    .line 1952
    .restart local v5       #c:Ljava/util/Calendar;
    .restart local v6       #date:I
    .restart local v13       #hour:I
    .restart local v18       #minute:I
    .restart local v19       #month:I
    .restart local v20       #nitzSubs:[Ljava/lang/String;
    .restart local v23       #second:I
    .restart local v31       #year:I
    :cond_24c
    const/16 v24, 0x0

    #@24e
    goto/16 :goto_ea

    #@250
    .line 1956
    .restart local v24       #sign:Z
    .restart local v29       #tzOffset:I
    :cond_250
    const/4 v7, 0x0

    #@251
    goto/16 :goto_107

    #@253
    .line 1966
    .restart local v7       #dst:I
    :cond_253
    const/16 v33, -0x1

    #@255
    goto/16 :goto_10b

    #@257
    .line 1984
    .restart local v15       #iso:Ljava/lang/String;
    .restart local v32       #zone:Ljava/util/TimeZone;
    :cond_257
    const/16 v33, 0x0

    #@259
    goto/16 :goto_15a

    #@25b
    .line 1992
    :cond_25b
    if-eqz v7, :cond_271

    #@25d
    const/16 v33, 0x1

    #@25f
    :goto_25f
    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    #@262
    move-result-wide v34

    #@263
    move-object/from16 v0, p0

    #@265
    move/from16 v1, v29

    #@267
    move/from16 v2, v33

    #@269
    move-wide/from16 v3, v34

    #@26b
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->getNitzTimeZone(IZJ)Ljava/util/TimeZone;

    #@26e
    move-result-object v32

    #@26f
    goto/16 :goto_168

    #@271
    :cond_271
    const/16 v33, 0x0

    #@273
    goto :goto_25f

    #@274
    .line 1997
    :cond_274
    const/16 v33, 0x0

    #@276
    goto/16 :goto_180

    #@278
    .line 2004
    :cond_278
    const/16 v33, 0x0

    #@27a
    goto/16 :goto_198

    #@27c
    .line 2008
    :cond_27c
    const-string v33, "NULL"
    :try_end_27e
    .catch Ljava/lang/RuntimeException; {:try_start_4a .. :try_end_27e} :catch_2ea

    #@27e
    goto/16 :goto_1d3

    #@280
    .line 2028
    .restart local v14       #ignore:Ljava/lang/String;
    :cond_280
    :try_start_280
    move-object/from16 v0, p0

    #@282
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@284
    move-object/from16 v33, v0

    #@286
    invoke-virtual/range {v33 .. v33}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@289
    .line 2033
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@28c
    move-result-wide v33

    #@28d
    sub-long v16, v33, p2

    #@28f
    .line 2036
    .local v16, millisSinceNitzReceived:J
    const-wide/16 v33, 0x0

    #@291
    cmp-long v33, v16, v33

    #@293
    if-gez v33, :cond_317

    #@295
    .line 2039
    new-instance v33, Ljava/lang/StringBuilder;

    #@297
    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    #@29a
    const-string v34, "NITZ: not setting time, clock has rolled backwards since NITZ time was received, "

    #@29c
    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29f
    move-result-object v33

    #@2a0
    move-object/from16 v0, v33

    #@2a2
    move-object/from16 v1, p1

    #@2a4
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a7
    move-result-object v33

    #@2a8
    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2ab
    move-result-object v33

    #@2ac
    move-object/from16 v0, p0

    #@2ae
    move-object/from16 v1, v33

    #@2b0
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V
    :try_end_2b3
    .catchall {:try_start_280 .. :try_end_2b3} :catchall_503

    #@2b3
    .line 2096
    :try_start_2b3
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@2b6
    move-result-wide v8

    #@2b7
    .line 2097
    .local v8, end:J
    new-instance v33, Ljava/lang/StringBuilder;

    #@2b9
    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    #@2bc
    const-string v34, "NITZ: end="

    #@2be
    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c1
    move-result-object v33

    #@2c2
    move-object/from16 v0, v33

    #@2c4
    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@2c7
    move-result-object v33

    #@2c8
    const-string v34, " dur="

    #@2ca
    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2cd
    move-result-object v33

    #@2ce
    sub-long v34, v8, v25

    #@2d0
    invoke-virtual/range {v33 .. v35}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@2d3
    move-result-object v33

    #@2d4
    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d7
    move-result-object v33

    #@2d8
    move-object/from16 v0, p0

    #@2da
    move-object/from16 v1, v33

    #@2dc
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@2df
    .line 2098
    move-object/from16 v0, p0

    #@2e1
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2e3
    move-object/from16 v33, v0

    #@2e5
    invoke-virtual/range {v33 .. v33}, Landroid/os/PowerManager$WakeLock;->release()V
    :try_end_2e8
    .catch Ljava/lang/RuntimeException; {:try_start_2b3 .. :try_end_2e8} :catch_2ea

    #@2e8
    goto/16 :goto_24b

    #@2ea
    .line 2100
    .end local v5           #c:Ljava/util/Calendar;
    .end local v6           #date:I
    .end local v7           #dst:I
    .end local v8           #end:J
    .end local v13           #hour:I
    .end local v14           #ignore:Ljava/lang/String;
    .end local v15           #iso:Ljava/lang/String;
    .end local v16           #millisSinceNitzReceived:J
    .end local v18           #minute:I
    .end local v19           #month:I
    .end local v20           #nitzSubs:[Ljava/lang/String;
    .end local v23           #second:I
    .end local v24           #sign:Z
    .end local v29           #tzOffset:I
    .end local v31           #year:I
    .end local v32           #zone:Ljava/util/TimeZone;
    :catch_2ea
    move-exception v10

    #@2eb
    .line 2101
    .local v10, ex:Ljava/lang/RuntimeException;
    new-instance v33, Ljava/lang/StringBuilder;

    #@2ed
    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    #@2f0
    const-string v34, "NITZ: Parsing NITZ time "

    #@2f2
    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f5
    move-result-object v33

    #@2f6
    move-object/from16 v0, v33

    #@2f8
    move-object/from16 v1, p1

    #@2fa
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2fd
    move-result-object v33

    #@2fe
    const-string v34, " ex="

    #@300
    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@303
    move-result-object v33

    #@304
    move-object/from16 v0, v33

    #@306
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@309
    move-result-object v33

    #@30a
    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30d
    move-result-object v33

    #@30e
    move-object/from16 v0, p0

    #@310
    move-object/from16 v1, v33

    #@312
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->loge(Ljava/lang/String;)V

    #@315
    goto/16 :goto_24b

    #@317
    .line 2046
    .end local v10           #ex:Ljava/lang/RuntimeException;
    .restart local v5       #c:Ljava/util/Calendar;
    .restart local v6       #date:I
    .restart local v7       #dst:I
    .restart local v13       #hour:I
    .restart local v14       #ignore:Ljava/lang/String;
    .restart local v15       #iso:Ljava/lang/String;
    .restart local v16       #millisSinceNitzReceived:J
    .restart local v18       #minute:I
    .restart local v19       #month:I
    .restart local v20       #nitzSubs:[Ljava/lang/String;
    .restart local v23       #second:I
    .restart local v24       #sign:Z
    .restart local v29       #tzOffset:I
    .restart local v31       #year:I
    .restart local v32       #zone:Ljava/util/TimeZone;
    :cond_317
    const-wide/32 v33, 0x7fffffff

    #@31a
    cmp-long v33, v16, v33

    #@31c
    if-lez v33, :cond_37a

    #@31e
    .line 2049
    :try_start_31e
    new-instance v33, Ljava/lang/StringBuilder;

    #@320
    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    #@323
    const-string v34, "NITZ: not setting time, processing has taken "

    #@325
    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@328
    move-result-object v33

    #@329
    const-wide/32 v34, 0x5265c00

    #@32c
    div-long v34, v16, v34

    #@32e
    invoke-virtual/range {v33 .. v35}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@331
    move-result-object v33

    #@332
    const-string v34, " days"

    #@334
    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@337
    move-result-object v33

    #@338
    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33b
    move-result-object v33

    #@33c
    move-object/from16 v0, p0

    #@33e
    move-object/from16 v1, v33

    #@340
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V
    :try_end_343
    .catchall {:try_start_31e .. :try_end_343} :catchall_503

    #@343
    .line 2096
    :try_start_343
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@346
    move-result-wide v8

    #@347
    .line 2097
    .restart local v8       #end:J
    new-instance v33, Ljava/lang/StringBuilder;

    #@349
    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    #@34c
    const-string v34, "NITZ: end="

    #@34e
    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@351
    move-result-object v33

    #@352
    move-object/from16 v0, v33

    #@354
    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@357
    move-result-object v33

    #@358
    const-string v34, " dur="

    #@35a
    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35d
    move-result-object v33

    #@35e
    sub-long v34, v8, v25

    #@360
    invoke-virtual/range {v33 .. v35}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@363
    move-result-object v33

    #@364
    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@367
    move-result-object v33

    #@368
    move-object/from16 v0, p0

    #@36a
    move-object/from16 v1, v33

    #@36c
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@36f
    .line 2098
    move-object/from16 v0, p0

    #@371
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@373
    move-object/from16 v33, v0

    #@375
    invoke-virtual/range {v33 .. v33}, Landroid/os/PowerManager$WakeLock;->release()V
    :try_end_378
    .catch Ljava/lang/RuntimeException; {:try_start_343 .. :try_end_378} :catch_2ea

    #@378
    goto/16 :goto_24b

    #@37a
    .line 2057
    .end local v8           #end:J
    :cond_37a
    const/16 v33, 0xe

    #@37c
    move-wide/from16 v0, v16

    #@37e
    long-to-int v0, v0

    #@37f
    move/from16 v34, v0

    #@381
    :try_start_381
    move/from16 v0, v33

    #@383
    move/from16 v1, v34

    #@385
    invoke-virtual {v5, v0, v1}, Ljava/util/Calendar;->add(II)V

    #@388
    .line 2059
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->getAutoTime()Z

    #@38b
    move-result v33

    #@38c
    if-eqz v33, :cond_43b

    #@38e
    .line 2063
    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    #@391
    move-result-wide v33

    #@392
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@395
    move-result-wide v35

    #@396
    sub-long v11, v33, v35

    #@398
    .line 2064
    .local v11, gained:J
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@39b
    move-result-wide v33

    #@39c
    move-object/from16 v0, p0

    #@39e
    iget-wide v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mSavedAtTime:J

    #@3a0
    move-wide/from16 v35, v0

    #@3a2
    sub-long v27, v33, v35

    #@3a4
    .line 2065
    .local v27, timeSinceLastUpdate:J
    move-object/from16 v0, p0

    #@3a6
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->cr:Landroid/content/ContentResolver;

    #@3a8
    move-object/from16 v33, v0

    #@3aa
    const-string v34, "nitz_update_spacing"

    #@3ac
    move-object/from16 v0, p0

    #@3ae
    iget v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mNitzUpdateSpacing:I

    #@3b0
    move/from16 v35, v0

    #@3b2
    invoke-static/range {v33 .. v35}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@3b5
    move-result v22

    #@3b6
    .line 2067
    .local v22, nitzUpdateSpacing:I
    move-object/from16 v0, p0

    #@3b8
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->cr:Landroid/content/ContentResolver;

    #@3ba
    move-object/from16 v33, v0

    #@3bc
    const-string v34, "nitz_update_diff"

    #@3be
    move-object/from16 v0, p0

    #@3c0
    iget v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mNitzUpdateDiff:I

    #@3c2
    move/from16 v35, v0

    #@3c4
    invoke-static/range {v33 .. v35}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@3c7
    move-result v21

    #@3c8
    .line 2070
    .local v21, nitzUpdateDiff:I
    move-object/from16 v0, p0

    #@3ca
    iget-wide v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mSavedAtTime:J

    #@3cc
    move-wide/from16 v33, v0

    #@3ce
    const-wide/16 v35, 0x0

    #@3d0
    cmp-long v33, v33, v35

    #@3d2
    if-eqz v33, :cond_3ea

    #@3d4
    move/from16 v0, v22

    #@3d6
    int-to-long v0, v0

    #@3d7
    move-wide/from16 v33, v0

    #@3d9
    cmp-long v33, v27, v33

    #@3db
    if-gtz v33, :cond_3ea

    #@3dd
    invoke-static {v11, v12}, Ljava/lang/Math;->abs(J)J

    #@3e0
    move-result-wide v33

    #@3e1
    move/from16 v0, v21

    #@3e3
    int-to-long v0, v0

    #@3e4
    move-wide/from16 v35, v0

    #@3e6
    cmp-long v33, v33, v35

    #@3e8
    if-lez v33, :cond_49c

    #@3ea
    .line 2073
    :cond_3ea
    new-instance v33, Ljava/lang/StringBuilder;

    #@3ec
    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    #@3ef
    const-string v34, "NITZ: Auto updating time of day to "

    #@3f1
    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f4
    move-result-object v33

    #@3f5
    invoke-virtual {v5}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    #@3f8
    move-result-object v34

    #@3f9
    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3fc
    move-result-object v33

    #@3fd
    const-string v34, " NITZ receive delay="

    #@3ff
    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@402
    move-result-object v33

    #@403
    move-object/from16 v0, v33

    #@405
    move-wide/from16 v1, v16

    #@407
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@40a
    move-result-object v33

    #@40b
    const-string v34, "ms gained="

    #@40d
    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@410
    move-result-object v33

    #@411
    move-object/from16 v0, v33

    #@413
    invoke-virtual {v0, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@416
    move-result-object v33

    #@417
    const-string v34, "ms from "

    #@419
    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41c
    move-result-object v33

    #@41d
    move-object/from16 v0, v33

    #@41f
    move-object/from16 v1, p1

    #@421
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@424
    move-result-object v33

    #@425
    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@428
    move-result-object v33

    #@429
    move-object/from16 v0, p0

    #@42b
    move-object/from16 v1, v33

    #@42d
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@430
    .line 2078
    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    #@433
    move-result-wide v33

    #@434
    move-object/from16 v0, p0

    #@436
    move-wide/from16 v1, v33

    #@438
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->setAndBroadcastNetworkSetTime(J)V

    #@43b
    .line 2091
    .end local v11           #gained:J
    .end local v21           #nitzUpdateDiff:I
    .end local v22           #nitzUpdateSpacing:I
    .end local v27           #timeSinceLastUpdate:J
    :cond_43b
    const-string v33, "NITZ: update nitz time property"

    #@43d
    move-object/from16 v0, p0

    #@43f
    move-object/from16 v1, v33

    #@441
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@444
    .line 2092
    const-string v33, "gsm.nitz.time"

    #@446
    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    #@449
    move-result-wide v34

    #@44a
    invoke-static/range {v34 .. v35}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    #@44d
    move-result-object v34

    #@44e
    invoke-static/range {v33 .. v34}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@451
    .line 2093
    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    #@454
    move-result-wide v33

    #@455
    move-wide/from16 v0, v33

    #@457
    move-object/from16 v2, p0

    #@459
    iput-wide v0, v2, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mSavedTime:J

    #@45b
    .line 2094
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@45e
    move-result-wide v33

    #@45f
    move-wide/from16 v0, v33

    #@461
    move-object/from16 v2, p0

    #@463
    iput-wide v0, v2, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mSavedAtTime:J
    :try_end_465
    .catchall {:try_start_381 .. :try_end_465} :catchall_503

    #@465
    .line 2096
    :try_start_465
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@468
    move-result-wide v8

    #@469
    .line 2097
    .restart local v8       #end:J
    new-instance v33, Ljava/lang/StringBuilder;

    #@46b
    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    #@46e
    const-string v34, "NITZ: end="

    #@470
    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@473
    move-result-object v33

    #@474
    move-object/from16 v0, v33

    #@476
    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@479
    move-result-object v33

    #@47a
    const-string v34, " dur="

    #@47c
    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47f
    move-result-object v33

    #@480
    sub-long v34, v8, v25

    #@482
    invoke-virtual/range {v33 .. v35}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@485
    move-result-object v33

    #@486
    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@489
    move-result-object v33

    #@48a
    move-object/from16 v0, p0

    #@48c
    move-object/from16 v1, v33

    #@48e
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@491
    .line 2098
    move-object/from16 v0, p0

    #@493
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@495
    move-object/from16 v33, v0

    #@497
    invoke-virtual/range {v33 .. v33}, Landroid/os/PowerManager$WakeLock;->release()V
    :try_end_49a
    .catch Ljava/lang/RuntimeException; {:try_start_465 .. :try_end_49a} :catch_2ea

    #@49a
    goto/16 :goto_24b

    #@49c
    .line 2081
    .end local v8           #end:J
    .restart local v11       #gained:J
    .restart local v21       #nitzUpdateDiff:I
    .restart local v22       #nitzUpdateSpacing:I
    .restart local v27       #timeSinceLastUpdate:J
    :cond_49c
    :try_start_49c
    new-instance v33, Ljava/lang/StringBuilder;

    #@49e
    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    #@4a1
    const-string v34, "NITZ: ignore, a previous update was "

    #@4a3
    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a6
    move-result-object v33

    #@4a7
    move-object/from16 v0, v33

    #@4a9
    move-wide/from16 v1, v27

    #@4ab
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@4ae
    move-result-object v33

    #@4af
    const-string v34, "ms ago and gained="

    #@4b1
    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b4
    move-result-object v33

    #@4b5
    move-object/from16 v0, v33

    #@4b7
    invoke-virtual {v0, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@4ba
    move-result-object v33

    #@4bb
    const-string v34, "ms"

    #@4bd
    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c0
    move-result-object v33

    #@4c1
    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c4
    move-result-object v33

    #@4c5
    move-object/from16 v0, p0

    #@4c7
    move-object/from16 v1, v33

    #@4c9
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V
    :try_end_4cc
    .catchall {:try_start_49c .. :try_end_4cc} :catchall_503

    #@4cc
    .line 2096
    :try_start_4cc
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@4cf
    move-result-wide v8

    #@4d0
    .line 2097
    .restart local v8       #end:J
    new-instance v33, Ljava/lang/StringBuilder;

    #@4d2
    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    #@4d5
    const-string v34, "NITZ: end="

    #@4d7
    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4da
    move-result-object v33

    #@4db
    move-object/from16 v0, v33

    #@4dd
    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@4e0
    move-result-object v33

    #@4e1
    const-string v34, " dur="

    #@4e3
    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e6
    move-result-object v33

    #@4e7
    sub-long v34, v8, v25

    #@4e9
    invoke-virtual/range {v33 .. v35}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@4ec
    move-result-object v33

    #@4ed
    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4f0
    move-result-object v33

    #@4f1
    move-object/from16 v0, p0

    #@4f3
    move-object/from16 v1, v33

    #@4f5
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@4f8
    .line 2098
    move-object/from16 v0, p0

    #@4fa
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@4fc
    move-object/from16 v33, v0

    #@4fe
    invoke-virtual/range {v33 .. v33}, Landroid/os/PowerManager$WakeLock;->release()V

    #@501
    goto/16 :goto_24b

    #@503
    .line 2096
    .end local v8           #end:J
    .end local v11           #gained:J
    .end local v16           #millisSinceNitzReceived:J
    .end local v21           #nitzUpdateDiff:I
    .end local v22           #nitzUpdateSpacing:I
    .end local v27           #timeSinceLastUpdate:J
    :catchall_503
    move-exception v33

    #@504
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@507
    move-result-wide v8

    #@508
    .line 2097
    .restart local v8       #end:J
    new-instance v34, Ljava/lang/StringBuilder;

    #@50a
    invoke-direct/range {v34 .. v34}, Ljava/lang/StringBuilder;-><init>()V

    #@50d
    const-string v35, "NITZ: end="

    #@50f
    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@512
    move-result-object v34

    #@513
    move-object/from16 v0, v34

    #@515
    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@518
    move-result-object v34

    #@519
    const-string v35, " dur="

    #@51b
    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51e
    move-result-object v34

    #@51f
    sub-long v35, v8, v25

    #@521
    invoke-virtual/range {v34 .. v36}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@524
    move-result-object v34

    #@525
    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@528
    move-result-object v34

    #@529
    move-object/from16 v0, p0

    #@52b
    move-object/from16 v1, v34

    #@52d
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@530
    .line 2098
    move-object/from16 v0, p0

    #@532
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@534
    move-object/from16 v34, v0

    #@536
    invoke-virtual/range {v34 .. v34}, Landroid/os/PowerManager$WakeLock;->release()V

    #@539
    .line 2096
    throw v33
    :try_end_53a
    .catch Ljava/lang/RuntimeException; {:try_start_4cc .. :try_end_53a} :catch_2ea
.end method

.method private updateFemtoCellIndicator()V
    .registers 4

    #@0
    .prologue
    .line 951
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "android.provider.Telephony.SPN_STRINGS_UPDATED"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 952
    .local v0, intent:Landroid/content/Intent;
    const/high16 v1, 0x2000

    #@9
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@c
    .line 953
    const-string v1, "showSpn"

    #@e
    const/4 v2, 0x0

    #@f
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@12
    .line 954
    const-string v1, "spn"

    #@14
    const-string v2, ""

    #@16
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@19
    .line 955
    const-string v1, "showPlmn"

    #@1b
    const/4 v2, 0x1

    #@1c
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@1f
    .line 956
    const-string v1, "plmn"

    #@21
    const-string v2, "Verizon Wireless"

    #@23
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@26
    .line 957
    const-string v1, "subscription"

    #@28
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@2a
    invoke-virtual {v2}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getSubscription()I

    #@2d
    move-result v2

    #@2e
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@31
    .line 958
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@33
    invoke-virtual {v1}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@36
    move-result-object v1

    #@37
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@39
    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@3c
    .line 959
    return-void
.end method

.method private updateNetworkIndicator(Ljava/lang/String;ZLjava/lang/String;Z)V
    .registers 8
    .parameter "plmn"
    .parameter "showPlmn"
    .parameter "spn"
    .parameter "showSpn"

    #@0
    .prologue
    .line 938
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "android.provider.Telephony.SPN_STRINGS_UPDATED"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 939
    .local v0, intent:Landroid/content/Intent;
    const/high16 v1, 0x2000

    #@9
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@c
    .line 940
    const-string v1, "showSpn"

    #@e
    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@11
    .line 941
    const-string v1, "spn"

    #@13
    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@16
    .line 942
    const-string v1, "showPlmn"

    #@18
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@1b
    .line 943
    const-string v1, "plmn"

    #@1d
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@20
    .line 944
    const-string v1, "subscription"

    #@22
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@24
    invoke-virtual {v2}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getSubscription()I

    #@27
    move-result v2

    #@28
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@2b
    .line 945
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@2d
    invoke-virtual {v1}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@30
    move-result-object v1

    #@31
    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@33
    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@36
    .line 946
    return-void
.end method


# virtual methods
.method public PlayVZWERISound(I)V
    .registers 11
    .parameter "alertId"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v5, 0x1

    #@2
    .line 2326
    sget-boolean v4, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->DBG_CALL:Z

    #@4
    if-eqz v4, :cond_2a

    #@6
    const-string v4, "CDMA"

    #@8
    new-instance v6, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v7, "PlayVZWERISound Received alertId :"

    #@f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v6

    #@13
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v6

    #@17
    const-string v7, "/"

    #@19
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v6

    #@1d
    sget v7, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->prev_alertId:I

    #@1f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v6

    #@23
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v6

    #@27
    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 2329
    :cond_2a
    sget v4, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->prev_alertId:I

    #@2c
    if-ne p1, v4, :cond_33

    #@2e
    iget-boolean v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isEriRingtoneStart:Z

    #@30
    if-nez v4, :cond_33

    #@32
    .line 2446
    :cond_32
    :goto_32
    return-void

    #@33
    .line 2331
    :cond_33
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@35
    invoke-virtual {v4}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@38
    move-result-object v4

    #@39
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@3c
    move-result-object v4

    #@3d
    const-string v6, "lg_eri_set"

    #@3f
    invoke-static {v4, v6, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@42
    move-result v4

    #@43
    if-ne v4, v5, :cond_188

    #@45
    .line 2333
    :try_start_45
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mAudioManager:Landroid/media/AudioManager;

    #@47
    if-nez v4, :cond_59

    #@49
    .line 2334
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@4b
    invoke-virtual {v4}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@4e
    move-result-object v4

    #@4f
    const-string v6, "audio"

    #@51
    invoke-virtual {v4, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@54
    move-result-object v4

    #@55
    check-cast v4, Landroid/media/AudioManager;

    #@57
    iput-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mAudioManager:Landroid/media/AudioManager;

    #@59
    .line 2336
    :cond_59
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mAudioManager:Landroid/media/AudioManager;

    #@5b
    invoke-virtual {v4}, Landroid/media/AudioManager;->getRingerMode()I

    #@5e
    move-result v4

    #@5f
    if-eqz v4, :cond_69

    #@61
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mAudioManager:Landroid/media/AudioManager;

    #@63
    invoke-virtual {v4}, Landroid/media/AudioManager;->getRingerMode()I

    #@66
    move-result v4

    #@67
    if-ne v4, v5, :cond_18c

    #@69
    :cond_69
    move v3, v5

    #@6a
    .line 2338
    .local v3, isRingerMute:Z
    :goto_6a
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mAudioManager:Landroid/media/AudioManager;

    #@6c
    invoke-virtual {v4}, Landroid/media/AudioManager;->isBluetoothA2dpOn()Z

    #@6f
    move-result v4

    #@70
    if-nez v4, :cond_82

    #@72
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mAudioManager:Landroid/media/AudioManager;

    #@74
    invoke-virtual {v4}, Landroid/media/AudioManager;->isBluetoothScoOn()Z

    #@77
    move-result v4

    #@78
    if-nez v4, :cond_82

    #@7a
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mAudioManager:Landroid/media/AudioManager;

    #@7c
    invoke-virtual {v4}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    #@7f
    move-result v4

    #@80
    if-eqz v4, :cond_83

    #@82
    :cond_82
    move v2, v5

    #@83
    .line 2340
    .local v2, isBTorHeadsetConnected:Z
    :cond_83
    sget-boolean v4, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->DBG_CALL:Z

    #@85
    if-eqz v4, :cond_a5

    #@87
    const-string v4, "CDMA"

    #@89
    new-instance v6, Ljava/lang/StringBuilder;

    #@8b
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@8e
    const-string v7, "mAudioManager.getRingerMode() = "

    #@90
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@93
    move-result-object v6

    #@94
    iget-object v7, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mAudioManager:Landroid/media/AudioManager;

    #@96
    invoke-virtual {v7}, Landroid/media/AudioManager;->getRingerMode()I

    #@99
    move-result v7

    #@9a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v6

    #@9e
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a1
    move-result-object v6

    #@a2
    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a5
    .line 2341
    :cond_a5
    sget-boolean v4, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->DBG_CALL:Z

    #@a7
    if-eqz v4, :cond_c7

    #@a9
    const-string v4, "CDMA"

    #@ab
    new-instance v6, Ljava/lang/StringBuilder;

    #@ad
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@b0
    const-string v7, "mAudioManager.isBluetoothA2dpOn() = "

    #@b2
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v6

    #@b6
    iget-object v7, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mAudioManager:Landroid/media/AudioManager;

    #@b8
    invoke-virtual {v7}, Landroid/media/AudioManager;->isBluetoothA2dpOn()Z

    #@bb
    move-result v7

    #@bc
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v6

    #@c0
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c3
    move-result-object v6

    #@c4
    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c7
    .line 2342
    :cond_c7
    sget-boolean v4, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->DBG_CALL:Z

    #@c9
    if-eqz v4, :cond_e9

    #@cb
    const-string v4, "CDMA"

    #@cd
    new-instance v6, Ljava/lang/StringBuilder;

    #@cf
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@d2
    const-string v7, "mAudioManager.isBluetoothScoOn() = "

    #@d4
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d7
    move-result-object v6

    #@d8
    iget-object v7, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mAudioManager:Landroid/media/AudioManager;

    #@da
    invoke-virtual {v7}, Landroid/media/AudioManager;->isBluetoothScoOn()Z

    #@dd
    move-result v7

    #@de
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@e1
    move-result-object v6

    #@e2
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e5
    move-result-object v6

    #@e6
    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e9
    .line 2343
    :cond_e9
    sget-boolean v4, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->DBG_CALL:Z

    #@eb
    if-eqz v4, :cond_10b

    #@ed
    const-string v4, "CDMA"

    #@ef
    new-instance v6, Ljava/lang/StringBuilder;

    #@f1
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@f4
    const-string v7, "mAudioManager.isWiredHeadsetOn() = "

    #@f6
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f9
    move-result-object v6

    #@fa
    iget-object v7, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mAudioManager:Landroid/media/AudioManager;

    #@fc
    invoke-virtual {v7}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    #@ff
    move-result v7

    #@100
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@103
    move-result-object v6

    #@104
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@107
    move-result-object v6

    #@108
    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@10b
    .line 2345
    :cond_10b
    if-eqz v3, :cond_113

    #@10d
    iget-boolean v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isEriRingtoneStart:Z

    #@10f
    if-ne v4, v5, :cond_279

    #@111
    if-eqz v2, :cond_279

    #@113
    :cond_113
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@115
    invoke-virtual {v4}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getState()Lcom/android/internal/telephony/PhoneConstants$State;

    #@118
    move-result-object v4

    #@119
    sget-object v6, Lcom/android/internal/telephony/PhoneConstants$State;->IDLE:Lcom/android/internal/telephony/PhoneConstants$State;

    #@11b
    if-eq v4, v6, :cond_12b

    #@11d
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@11f
    invoke-virtual {v4}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getState()Lcom/android/internal/telephony/PhoneConstants$State;

    #@122
    move-result-object v4

    #@123
    sget-object v6, Lcom/android/internal/telephony/PhoneConstants$State;->OFFHOOK:Lcom/android/internal/telephony/PhoneConstants$State;

    #@125
    if-eq v4, v6, :cond_12b

    #@127
    iget-boolean v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isEriRingtoneStart:Z

    #@129
    if-ne v4, v5, :cond_279

    #@12b
    .line 2347
    :cond_12b
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@12d
    if-nez v4, :cond_18f

    #@12f
    .line 2348
    new-instance v4, Landroid/media/MediaPlayer;

    #@131
    invoke-direct {v4}, Landroid/media/MediaPlayer;-><init>()V

    #@134
    iput-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@136
    .line 2352
    :goto_136
    iget-boolean v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isVZWERISoundPlaying:Z

    #@138
    if-nez v4, :cond_143

    #@13a
    .line 2353
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mAudioManager:Landroid/media/AudioManager;

    #@13c
    const/4 v6, 0x3

    #@13d
    invoke-virtual {v4, v6}, Landroid/media/AudioManager;->getStreamVolume(I)I

    #@140
    move-result v4

    #@141
    iput v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->restoreVolume:I

    #@143
    .line 2356
    :cond_143
    if-eqz v2, :cond_150

    #@145
    .line 2357
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mAudioManager:Landroid/media/AudioManager;

    #@147
    const/4 v6, 0x3

    #@148
    iget v7, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->restoreVolume:I

    #@14a
    add-int/lit8 v7, v7, -0x3

    #@14c
    const/4 v8, 0x0

    #@14d
    invoke-virtual {v4, v6, v7, v8}, Landroid/media/AudioManager;->setStreamVolume(III)V

    #@150
    .line 2360
    :cond_150
    const/16 v4, 0x3e8

    #@152
    if-ne p1, v4, :cond_19c

    #@154
    .line 2361
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@156
    const-string v5, "/system/media/audio/eri/LossofService.wav"

    #@158
    invoke-virtual {v4, v5}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    #@15b
    .line 2383
    :goto_15b
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@15d
    invoke-virtual {v4}, Landroid/media/MediaPlayer;->prepare()V

    #@160
    .line 2384
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@162
    const/4 v5, 0x3

    #@163
    invoke-virtual {v4, v5}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    #@166
    .line 2385
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@168
    const/4 v5, 0x0

    #@169
    invoke-virtual {v4, v5}, Landroid/media/MediaPlayer;->setLooping(Z)V

    #@16c
    .line 2386
    const/4 v4, 0x1

    #@16d
    iput-boolean v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isVZWERISoundPlaying:Z

    #@16f
    .line 2388
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@171
    new-instance v5, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker$6;

    #@173
    invoke-direct {v5, p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker$6;-><init>(Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;)V

    #@176
    invoke-virtual {v4, v5}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    #@179
    .line 2406
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@17b
    new-instance v5, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker$7;

    #@17d
    invoke-direct {v5, p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker$7;-><init>(Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;)V

    #@180
    invoke-virtual {v4, v5}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    #@183
    .line 2427
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@185
    invoke-virtual {v4}, Landroid/media/MediaPlayer;->start()V
    :try_end_188
    .catch Ljava/lang/NullPointerException; {:try_start_45 .. :try_end_188} :catch_195
    .catch Ljava/io/FileNotFoundException; {:try_start_45 .. :try_end_188} :catch_1a7
    .catch Ljava/io/IOException; {:try_start_45 .. :try_end_188} :catch_1b9
    .catch Ljava/lang/IllegalArgumentException; {:try_start_45 .. :try_end_188} :catch_1db
    .catch Ljava/lang/IllegalStateException; {:try_start_45 .. :try_end_188} :catch_1fe

    #@188
    .line 2445
    .end local v2           #isBTorHeadsetConnected:Z
    .end local v3           #isRingerMute:Z
    :cond_188
    :goto_188
    sput p1, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->prev_alertId:I

    #@18a
    goto/16 :goto_32

    #@18c
    :cond_18c
    move v3, v2

    #@18d
    .line 2336
    goto/16 :goto_6a

    #@18f
    .line 2350
    .restart local v2       #isBTorHeadsetConnected:Z
    .restart local v3       #isRingerMute:Z
    :cond_18f
    :try_start_18f
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@191
    invoke-virtual {v4}, Landroid/media/MediaPlayer;->reset()V
    :try_end_194
    .catch Ljava/lang/NullPointerException; {:try_start_18f .. :try_end_194} :catch_195
    .catch Ljava/io/FileNotFoundException; {:try_start_18f .. :try_end_194} :catch_1a7
    .catch Ljava/io/IOException; {:try_start_18f .. :try_end_194} :catch_1b9
    .catch Ljava/lang/IllegalArgumentException; {:try_start_18f .. :try_end_194} :catch_1db
    .catch Ljava/lang/IllegalStateException; {:try_start_18f .. :try_end_194} :catch_1fe

    #@194
    goto :goto_136

    #@195
    .line 2433
    .end local v2           #isBTorHeadsetConnected:Z
    .end local v3           #isRingerMute:Z
    :catch_195
    move-exception v0

    #@196
    .line 2434
    .local v0, e:Ljava/lang/NullPointerException;
    const-string v4, "LG_SYS:Error1"

    #@198
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@19b
    goto :goto_188

    #@19c
    .line 2362
    .end local v0           #e:Ljava/lang/NullPointerException;
    .restart local v2       #isBTorHeadsetConnected:Z
    .restart local v3       #isRingerMute:Z
    :cond_19c
    const/4 v4, 0x5

    #@19d
    if-ne p1, v4, :cond_1ae

    #@19f
    .line 2363
    :try_start_19f
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@1a1
    const-string v5, "/system/media/audio/eri/Roaming.wav"

    #@1a3
    invoke-virtual {v4, v5}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V
    :try_end_1a6
    .catch Ljava/lang/NullPointerException; {:try_start_19f .. :try_end_1a6} :catch_195
    .catch Ljava/io/FileNotFoundException; {:try_start_19f .. :try_end_1a6} :catch_1a7
    .catch Ljava/io/IOException; {:try_start_19f .. :try_end_1a6} :catch_1b9
    .catch Ljava/lang/IllegalArgumentException; {:try_start_19f .. :try_end_1a6} :catch_1db
    .catch Ljava/lang/IllegalStateException; {:try_start_19f .. :try_end_1a6} :catch_1fe

    #@1a6
    goto :goto_15b

    #@1a7
    .line 2435
    .end local v2           #isBTorHeadsetConnected:Z
    .end local v3           #isRingerMute:Z
    :catch_1a7
    move-exception v0

    #@1a8
    .line 2436
    .local v0, e:Ljava/io/FileNotFoundException;
    const-string v4, "FileNotFoundException"

    #@1aa
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@1ad
    goto :goto_188

    #@1ae
    .line 2364
    .end local v0           #e:Ljava/io/FileNotFoundException;
    .restart local v2       #isBTorHeadsetConnected:Z
    .restart local v3       #isRingerMute:Z
    :cond_1ae
    const/4 v4, 0x4

    #@1af
    if-ne p1, v4, :cond_1d1

    #@1b1
    .line 2365
    :try_start_1b1
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@1b3
    const-string v5, "/system/media/audio/eri/ExtendedNetwork.wav"

    #@1b5
    invoke-virtual {v4, v5}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V
    :try_end_1b8
    .catch Ljava/lang/NullPointerException; {:try_start_1b1 .. :try_end_1b8} :catch_195
    .catch Ljava/io/FileNotFoundException; {:try_start_1b1 .. :try_end_1b8} :catch_1a7
    .catch Ljava/io/IOException; {:try_start_1b1 .. :try_end_1b8} :catch_1b9
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1b1 .. :try_end_1b8} :catch_1db
    .catch Ljava/lang/IllegalStateException; {:try_start_1b1 .. :try_end_1b8} :catch_1fe

    #@1b8
    goto :goto_15b

    #@1b9
    .line 2437
    .end local v2           #isBTorHeadsetConnected:Z
    .end local v3           #isRingerMute:Z
    :catch_1b9
    move-exception v0

    #@1ba
    .line 2438
    .local v0, e:Ljava/io/IOException;
    new-instance v4, Ljava/lang/StringBuilder;

    #@1bc
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1bf
    const-string v5, "MediaPlayer IOException: "

    #@1c1
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c4
    move-result-object v4

    #@1c5
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c8
    move-result-object v4

    #@1c9
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1cc
    move-result-object v4

    #@1cd
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@1d0
    goto :goto_188

    #@1d1
    .line 2366
    .end local v0           #e:Ljava/io/IOException;
    .restart local v2       #isBTorHeadsetConnected:Z
    .restart local v3       #isRingerMute:Z
    :cond_1d1
    if-ne p1, v5, :cond_1f3

    #@1d3
    .line 2367
    :try_start_1d3
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@1d5
    const-string v5, "/system/media/audio/eri/NetworkExtender.wav"

    #@1d7
    invoke-virtual {v4, v5}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V
    :try_end_1da
    .catch Ljava/lang/NullPointerException; {:try_start_1d3 .. :try_end_1da} :catch_195
    .catch Ljava/io/FileNotFoundException; {:try_start_1d3 .. :try_end_1da} :catch_1a7
    .catch Ljava/io/IOException; {:try_start_1d3 .. :try_end_1da} :catch_1b9
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1d3 .. :try_end_1da} :catch_1db
    .catch Ljava/lang/IllegalStateException; {:try_start_1d3 .. :try_end_1da} :catch_1fe

    #@1da
    goto :goto_15b

    #@1db
    .line 2439
    .end local v2           #isBTorHeadsetConnected:Z
    .end local v3           #isRingerMute:Z
    :catch_1db
    move-exception v0

    #@1dc
    .line 2440
    .local v0, e:Ljava/lang/IllegalArgumentException;
    new-instance v4, Ljava/lang/StringBuilder;

    #@1de
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1e1
    const-string v5, "MediaPlayer IllegalArgumentException: "

    #@1e3
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e6
    move-result-object v4

    #@1e7
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1ea
    move-result-object v4

    #@1eb
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ee
    move-result-object v4

    #@1ef
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@1f2
    goto :goto_188

    #@1f3
    .line 2368
    .end local v0           #e:Ljava/lang/IllegalArgumentException;
    .restart local v2       #isBTorHeadsetConnected:Z
    .restart local v3       #isRingerMute:Z
    :cond_1f3
    if-nez p1, :cond_217

    #@1f5
    .line 2369
    :try_start_1f5
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@1f7
    const-string v5, "/system/media/audio/eri/VerizonWireless.wav"

    #@1f9
    invoke-virtual {v4, v5}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V
    :try_end_1fc
    .catch Ljava/lang/NullPointerException; {:try_start_1f5 .. :try_end_1fc} :catch_195
    .catch Ljava/io/FileNotFoundException; {:try_start_1f5 .. :try_end_1fc} :catch_1a7
    .catch Ljava/io/IOException; {:try_start_1f5 .. :try_end_1fc} :catch_1b9
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1f5 .. :try_end_1fc} :catch_1db
    .catch Ljava/lang/IllegalStateException; {:try_start_1f5 .. :try_end_1fc} :catch_1fe

    #@1fc
    goto/16 :goto_15b

    #@1fe
    .line 2441
    .end local v2           #isBTorHeadsetConnected:Z
    .end local v3           #isRingerMute:Z
    :catch_1fe
    move-exception v0

    #@1ff
    .line 2442
    .local v0, e:Ljava/lang/IllegalStateException;
    new-instance v4, Ljava/lang/StringBuilder;

    #@201
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@204
    const-string v5, "MediaPlayer IllegalStateException: "

    #@206
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@209
    move-result-object v4

    #@20a
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@20d
    move-result-object v4

    #@20e
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@211
    move-result-object v4

    #@212
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@215
    goto/16 :goto_188

    #@217
    .line 2371
    .end local v0           #e:Ljava/lang/IllegalStateException;
    .restart local v2       #isBTorHeadsetConnected:Z
    .restart local v3       #isRingerMute:Z
    :cond_217
    :try_start_217
    new-instance v4, Ljava/lang/StringBuilder;

    #@219
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@21c
    const-string v6, "mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC) = "

    #@21e
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@221
    move-result-object v4

    #@222
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mAudioManager:Landroid/media/AudioManager;

    #@224
    const/4 v7, 0x3

    #@225
    invoke-virtual {v6, v7}, Landroid/media/AudioManager;->getStreamVolume(I)I

    #@228
    move-result v6

    #@229
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22c
    move-result-object v4

    #@22d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@230
    move-result-object v4

    #@231
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@234
    .line 2372
    new-instance v4, Ljava/lang/StringBuilder;

    #@236
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@239
    const-string v6, "restoreVolume = "

    #@23b
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23e
    move-result-object v4

    #@23f
    iget v6, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->restoreVolume:I

    #@241
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@244
    move-result-object v4

    #@245
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@248
    move-result-object v4

    #@249
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@24c
    .line 2373
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mAudioManager:Landroid/media/AudioManager;

    #@24e
    const/4 v6, 0x3

    #@24f
    iget v7, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->restoreVolume:I

    #@251
    const/4 v8, 0x0

    #@252
    invoke-virtual {v4, v6, v7, v8}, Landroid/media/AudioManager;->setStreamVolume(III)V

    #@255
    .line 2374
    iget-boolean v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isEriRingtoneStart:Z

    #@257
    if-ne v4, v5, :cond_32

    #@259
    .line 2375
    sget-boolean v4, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->DBG_CALL:Z

    #@25b
    if-eqz v4, :cond_264

    #@25d
    const-string v4, "CDMA"

    #@25f
    const-string v5, "ringtone start in the exception case without alertID"

    #@261
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@264
    .line 2376
    :cond_264
    new-instance v1, Landroid/content/Intent;

    #@266
    const-string v4, "com.android.finishEriSound"

    #@268
    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@26b
    .line 2377
    .local v1, finishEriSound:Landroid/content/Intent;
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@26d
    invoke-virtual {v4}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@270
    move-result-object v4

    #@271
    invoke-virtual {v4, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@274
    .line 2378
    const/4 v4, 0x0

    #@275
    iput-boolean v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isEriRingtoneStart:Z

    #@277
    goto/16 :goto_32

    #@279
    .line 2429
    .end local v1           #finishEriSound:Landroid/content/Intent;
    :cond_279
    sget-boolean v4, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->DBG_CALL:Z

    #@27b
    if-eqz v4, :cond_2a1

    #@27d
    const-string v4, "CDMA"

    #@27f
    new-instance v5, Ljava/lang/StringBuilder;

    #@281
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@284
    const-string v6, "PlayVZWERISound Skip alertId :"

    #@286
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@289
    move-result-object v5

    #@28a
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28d
    move-result-object v5

    #@28e
    const-string v6, "/"

    #@290
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@293
    move-result-object v5

    #@294
    sget v6, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->prev_alertId:I

    #@296
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@299
    move-result-object v5

    #@29a
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29d
    move-result-object v5

    #@29e
    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@2a1
    .line 2430
    :cond_2a1
    const/4 v4, 0x0

    #@2a2
    iput-boolean v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isEriRingtoneStart:Z
    :try_end_2a4
    .catch Ljava/lang/NullPointerException; {:try_start_217 .. :try_end_2a4} :catch_195
    .catch Ljava/io/FileNotFoundException; {:try_start_217 .. :try_end_2a4} :catch_1a7
    .catch Ljava/io/IOException; {:try_start_217 .. :try_end_2a4} :catch_1b9
    .catch Ljava/lang/IllegalArgumentException; {:try_start_217 .. :try_end_2a4} :catch_1db
    .catch Ljava/lang/IllegalStateException; {:try_start_217 .. :try_end_2a4} :catch_1fe

    #@2a4
    goto/16 :goto_32
.end method

.method public PlayVZWERISoundforMTCall(I)V
    .registers 4
    .parameter "alertId"

    #@0
    .prologue
    .line 2305
    sget-boolean v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->DBG_CALL:Z

    #@2
    if-eqz v0, :cond_b

    #@4
    const-string v0, "CDMA"

    #@6
    const-string v1, "PlayVZWERISoundforMTCall!!!"

    #@8
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 2306
    :cond_b
    const/4 v0, 0x1

    #@c
    iput-boolean v0, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isEriRingtoneStart:Z

    #@e
    .line 2307
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->PlayVZWERISound(I)V

    #@11
    .line 2308
    return-void
.end method

.method public StopVZWERISound()V
    .registers 6

    #@0
    .prologue
    .line 2312
    :try_start_0
    iget-boolean v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isEriRingtoneStart:Z

    #@2
    const/4 v2, 0x1

    #@3
    if-ne v1, v2, :cond_21

    #@5
    .line 2313
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mAudioManager:Landroid/media/AudioManager;

    #@7
    const/4 v2, 0x3

    #@8
    iget v3, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->restoreVolume:I

    #@a
    const/4 v4, 0x0

    #@b
    invoke-virtual {v1, v2, v3, v4}, Landroid/media/AudioManager;->setStreamVolume(III)V

    #@e
    .line 2314
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@10
    invoke-virtual {v1}, Landroid/media/MediaPlayer;->stop()V

    #@13
    .line 2315
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mMediaPlayer:Landroid/media/MediaPlayer;

    #@15
    invoke-virtual {v1}, Landroid/media/MediaPlayer;->release()V

    #@18
    .line 2316
    const/4 v1, 0x0

    #@19
    iput-boolean v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isVZWERISoundPlaying:Z

    #@1b
    .line 2317
    const/4 v1, 0x0

    #@1c
    iput-boolean v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isEriRingtoneStart:Z

    #@1e
    .line 2318
    const/4 v1, 0x0

    #@1f
    iput-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mMediaPlayer:Landroid/media/MediaPlayer;
    :try_end_21
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_21} :catch_22

    #@21
    .line 2323
    :cond_21
    :goto_21
    return-void

    #@22
    .line 2320
    :catch_22
    move-exception v0

    #@23
    .line 2321
    .local v0, e:Ljava/lang/NullPointerException;
    const-string v1, "mMediaPlayer has NullPointer Exception."

    #@25
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@28
    goto :goto_21
.end method

.method public dispose()V
    .registers 3

    #@0
    .prologue
    .line 432
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->checkCorrectThread()V

    #@3
    .line 433
    const-string v0, "ServiceStateTracker dispose"

    #@5
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@8
    .line 436
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@a
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForRadioStateChanged(Landroid/os/Handler;)V

    #@d
    .line 437
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@f
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForVoiceNetworkStateChanged(Landroid/os/Handler;)V

    #@12
    .line 438
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@14
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForCdmaOtaProvision(Landroid/os/Handler;)V

    #@17
    .line 439
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@19
    invoke-virtual {v0, p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->unregisterForEriFileLoaded(Landroid/os/Handler;)V

    #@1c
    .line 440
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mUiccApplcation:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@1e
    if-eqz v0, :cond_25

    #@20
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mUiccApplcation:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@22
    invoke-virtual {v0, p0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->unregisterForReady(Landroid/os/Handler;)V

    #@25
    .line 441
    :cond_25
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@27
    if-eqz v0, :cond_2e

    #@29
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@2b
    invoke-virtual {v0, p0}, Lcom/android/internal/telephony/uicc/IccRecords;->unregisterForRecordsLoaded(Landroid/os/Handler;)V

    #@2e
    .line 442
    :cond_2e
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@30
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unSetOnNITZTime(Landroid/os/Handler;)V

    #@33
    .line 443
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->cr:Landroid/content/ContentResolver;

    #@35
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mAutoTimeObserver:Landroid/database/ContentObserver;

    #@37
    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    #@3a
    .line 444
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->cr:Landroid/content/ContentResolver;

    #@3c
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mAutoTimeZoneObserver:Landroid/database/ContentObserver;

    #@3e
    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    #@41
    .line 446
    const-string v0, "VZW"

    #@43
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@46
    move-result v0

    #@47
    if-eqz v0, :cond_54

    #@49
    .line 447
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mCdmaApnObserver:Landroid/database/ContentObserver;

    #@4b
    if-eqz v0, :cond_54

    #@4d
    .line 448
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->cr:Landroid/content/ContentResolver;

    #@4f
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mCdmaApnObserver:Landroid/database/ContentObserver;

    #@51
    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    #@54
    .line 453
    :cond_54
    const/4 v0, 0x0

    #@55
    const-string v1, "sprint_location_spec"

    #@57
    invoke-static {v0, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@5a
    move-result v0

    #@5b
    if-eqz v0, :cond_68

    #@5d
    .line 454
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mCellLocationObserver:Landroid/database/ContentObserver;

    #@5f
    if-eqz v0, :cond_68

    #@61
    .line 455
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->cr:Landroid/content/ContentResolver;

    #@63
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mCellLocationObserver:Landroid/database/ContentObserver;

    #@65
    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    #@68
    .line 459
    :cond_68
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mCdmaSSM:Lcom/android/internal/telephony/cdma/CdmaSubscriptionSourceManager;

    #@6a
    invoke-virtual {v0, p0}, Lcom/android/internal/telephony/cdma/CdmaSubscriptionSourceManager;->dispose(Landroid/os/Handler;)V

    #@6d
    .line 460
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@6f
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForCdmaPrlChanged(Landroid/os/Handler;)V

    #@72
    .line 461
    invoke-super {p0}, Lcom/android/internal/telephony/ServiceStateTracker;->dispose()V

    #@75
    .line 464
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@77
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForLteEhrpdForcedChanged(Landroid/os/Handler;)V

    #@7a
    .line 468
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@7c
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForHDRRoamingIndicator(Landroid/os/Handler;)V

    #@7f
    .line 470
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@81
    if-eqz v0, :cond_8e

    #@83
    .line 471
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@85
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@88
    move-result-object v0

    #@89
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@8b
    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@8e
    .line 476
    :cond_8e
    iget-boolean v0, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mNeedFixZone:Z

    #@90
    if-eqz v0, :cond_ab

    #@92
    .line 477
    iget-boolean v0, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mNeedFixZone:Z

    #@94
    invoke-static {v0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->setSavedNeedFixZone(Z)V

    #@97
    .line 478
    iget v0, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mZoneOffset:I

    #@99
    invoke-static {v0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->setSavedZoneOffset(I)V

    #@9c
    .line 479
    iget-boolean v0, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mZoneDst:Z

    #@9e
    invoke-static {v0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->setSavedZoneDst(Z)V

    #@a1
    .line 480
    iget-wide v0, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mZoneTime:J

    #@a3
    invoke-static {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->setSavedZoneTime(J)V

    #@a6
    .line 481
    const-string v0, "TimeZone correction is needed after Phone Switching!"

    #@a8
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@ab
    .line 484
    :cond_ab
    return-void
.end method

.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 7
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 2665
    const-string v0, "CdmaServiceStateTracker extends:"

    #@2
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@5
    .line 2666
    invoke-super {p0, p1, p2, p3}, Lcom/android/internal/telephony/ServiceStateTracker;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    #@8
    .line 2667
    new-instance v0, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v1, " phone="

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v0

    #@1d
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@20
    .line 2668
    new-instance v0, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v1, " cellLoc="

    #@27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v0

    #@2b
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->cellLoc:Landroid/telephony/cdma/CdmaCellLocation;

    #@2d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v0

    #@31
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v0

    #@35
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@38
    .line 2669
    new-instance v0, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v1, " newCellLoc="

    #@3f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v0

    #@43
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->newCellLoc:Landroid/telephony/cdma/CdmaCellLocation;

    #@45
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v0

    #@49
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c
    move-result-object v0

    #@4d
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@50
    .line 2670
    new-instance v0, Ljava/lang/StringBuilder;

    #@52
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@55
    const-string v1, " mCurrentOtaspMode="

    #@57
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v0

    #@5b
    iget v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mCurrentOtaspMode:I

    #@5d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@60
    move-result-object v0

    #@61
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v0

    #@65
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@68
    .line 2671
    new-instance v0, Ljava/lang/StringBuilder;

    #@6a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@6d
    const-string v1, " mCdmaRoaming="

    #@6f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v0

    #@73
    iget-boolean v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mCdmaRoaming:Z

    #@75
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@78
    move-result-object v0

    #@79
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7c
    move-result-object v0

    #@7d
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@80
    .line 2672
    new-instance v0, Ljava/lang/StringBuilder;

    #@82
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@85
    const-string v1, " mRoamingIndicator="

    #@87
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v0

    #@8b
    iget v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mRoamingIndicator:I

    #@8d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@90
    move-result-object v0

    #@91
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@94
    move-result-object v0

    #@95
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@98
    .line 2673
    new-instance v0, Ljava/lang/StringBuilder;

    #@9a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@9d
    const-string v1, " mIsInPrl="

    #@9f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v0

    #@a3
    iget-boolean v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mIsInPrl:Z

    #@a5
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v0

    #@a9
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ac
    move-result-object v0

    #@ad
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@b0
    .line 2674
    new-instance v0, Ljava/lang/StringBuilder;

    #@b2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@b5
    const-string v1, " mDefaultRoamingIndicator="

    #@b7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v0

    #@bb
    iget v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mDefaultRoamingIndicator:I

    #@bd
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v0

    #@c1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c4
    move-result-object v0

    #@c5
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@c8
    .line 2675
    new-instance v0, Ljava/lang/StringBuilder;

    #@ca
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@cd
    const-string v1, " mDataConnectionState="

    #@cf
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d2
    move-result-object v0

    #@d3
    iget v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mDataConnectionState:I

    #@d5
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@d8
    move-result-object v0

    #@d9
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@dc
    move-result-object v0

    #@dd
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@e0
    .line 2676
    new-instance v0, Ljava/lang/StringBuilder;

    #@e2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@e5
    const-string v1, " mNewDataConnectionState="

    #@e7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ea
    move-result-object v0

    #@eb
    iget v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mNewDataConnectionState:I

    #@ed
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f0
    move-result-object v0

    #@f1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f4
    move-result-object v0

    #@f5
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@f8
    .line 2677
    new-instance v0, Ljava/lang/StringBuilder;

    #@fa
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@fd
    const-string v1, " mRegistrationState="

    #@ff
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@102
    move-result-object v0

    #@103
    iget v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mRegistrationState:I

    #@105
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@108
    move-result-object v0

    #@109
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10c
    move-result-object v0

    #@10d
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@110
    .line 2678
    new-instance v0, Ljava/lang/StringBuilder;

    #@112
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@115
    const-string v1, " mNeedFixZone="

    #@117
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11a
    move-result-object v0

    #@11b
    iget-boolean v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mNeedFixZone:Z

    #@11d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@120
    move-result-object v0

    #@121
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@124
    move-result-object v0

    #@125
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@128
    .line 2679
    new-instance v0, Ljava/lang/StringBuilder;

    #@12a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@12d
    const-string v1, " mZoneOffset="

    #@12f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@132
    move-result-object v0

    #@133
    iget v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mZoneOffset:I

    #@135
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@138
    move-result-object v0

    #@139
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13c
    move-result-object v0

    #@13d
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@140
    .line 2680
    new-instance v0, Ljava/lang/StringBuilder;

    #@142
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@145
    const-string v1, " mZoneDst="

    #@147
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14a
    move-result-object v0

    #@14b
    iget-boolean v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mZoneDst:Z

    #@14d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@150
    move-result-object v0

    #@151
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@154
    move-result-object v0

    #@155
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@158
    .line 2681
    new-instance v0, Ljava/lang/StringBuilder;

    #@15a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@15d
    const-string v1, " mZoneTime="

    #@15f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@162
    move-result-object v0

    #@163
    iget-wide v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mZoneTime:J

    #@165
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@168
    move-result-object v0

    #@169
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16c
    move-result-object v0

    #@16d
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@170
    .line 2682
    new-instance v0, Ljava/lang/StringBuilder;

    #@172
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@175
    const-string v1, " mGotCountryCode="

    #@177
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17a
    move-result-object v0

    #@17b
    iget-boolean v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mGotCountryCode:Z

    #@17d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@180
    move-result-object v0

    #@181
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@184
    move-result-object v0

    #@185
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@188
    .line 2683
    new-instance v0, Ljava/lang/StringBuilder;

    #@18a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@18d
    const-string v1, " mSavedTimeZone="

    #@18f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@192
    move-result-object v0

    #@193
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mSavedTimeZone:Ljava/lang/String;

    #@195
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@198
    move-result-object v0

    #@199
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19c
    move-result-object v0

    #@19d
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1a0
    .line 2684
    new-instance v0, Ljava/lang/StringBuilder;

    #@1a2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@1a5
    const-string v1, " mSavedTime="

    #@1a7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1aa
    move-result-object v0

    #@1ab
    iget-wide v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mSavedTime:J

    #@1ad
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1b0
    move-result-object v0

    #@1b1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b4
    move-result-object v0

    #@1b5
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1b8
    .line 2685
    new-instance v0, Ljava/lang/StringBuilder;

    #@1ba
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@1bd
    const-string v1, " mSavedAtTime="

    #@1bf
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c2
    move-result-object v0

    #@1c3
    iget-wide v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mSavedAtTime:J

    #@1c5
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1c8
    move-result-object v0

    #@1c9
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1cc
    move-result-object v0

    #@1cd
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1d0
    .line 2686
    new-instance v0, Ljava/lang/StringBuilder;

    #@1d2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@1d5
    const-string v1, " mWakeLock="

    #@1d7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1da
    move-result-object v0

    #@1db
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@1dd
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1e0
    move-result-object v0

    #@1e1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e4
    move-result-object v0

    #@1e5
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1e8
    .line 2687
    new-instance v0, Ljava/lang/StringBuilder;

    #@1ea
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@1ed
    const-string v1, " mCurPlmn="

    #@1ef
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f2
    move-result-object v0

    #@1f3
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mCurPlmn:Ljava/lang/String;

    #@1f5
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f8
    move-result-object v0

    #@1f9
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1fc
    move-result-object v0

    #@1fd
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@200
    .line 2688
    new-instance v0, Ljava/lang/StringBuilder;

    #@202
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@205
    const-string v1, " mMdn="

    #@207
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20a
    move-result-object v0

    #@20b
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mMdn:Ljava/lang/String;

    #@20d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@210
    move-result-object v0

    #@211
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@214
    move-result-object v0

    #@215
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@218
    .line 2689
    new-instance v0, Ljava/lang/StringBuilder;

    #@21a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@21d
    const-string v1, " mHomeSystemId="

    #@21f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@222
    move-result-object v0

    #@223
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mHomeSystemId:[I

    #@225
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@228
    move-result-object v0

    #@229
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22c
    move-result-object v0

    #@22d
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@230
    .line 2690
    new-instance v0, Ljava/lang/StringBuilder;

    #@232
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@235
    const-string v1, " mHomeNetworkId="

    #@237
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23a
    move-result-object v0

    #@23b
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mHomeNetworkId:[I

    #@23d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@240
    move-result-object v0

    #@241
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@244
    move-result-object v0

    #@245
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@248
    .line 2691
    new-instance v0, Ljava/lang/StringBuilder;

    #@24a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@24d
    const-string v1, " mMin="

    #@24f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@252
    move-result-object v0

    #@253
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mMin:Ljava/lang/String;

    #@255
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@258
    move-result-object v0

    #@259
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25c
    move-result-object v0

    #@25d
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@260
    .line 2692
    new-instance v0, Ljava/lang/StringBuilder;

    #@262
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@265
    const-string v1, " mPrlVersion="

    #@267
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26a
    move-result-object v0

    #@26b
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mPrlVersion:Ljava/lang/String;

    #@26d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@270
    move-result-object v0

    #@271
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@274
    move-result-object v0

    #@275
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@278
    .line 2693
    new-instance v0, Ljava/lang/StringBuilder;

    #@27a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@27d
    const-string v1, " mIsMinInfoReady="

    #@27f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@282
    move-result-object v0

    #@283
    iget-boolean v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mIsMinInfoReady:Z

    #@285
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@288
    move-result-object v0

    #@289
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28c
    move-result-object v0

    #@28d
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@290
    .line 2694
    new-instance v0, Ljava/lang/StringBuilder;

    #@292
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@295
    const-string v1, " isEriTextLoaded="

    #@297
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29a
    move-result-object v0

    #@29b
    iget-boolean v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isEriTextLoaded:Z

    #@29d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2a0
    move-result-object v0

    #@2a1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a4
    move-result-object v0

    #@2a5
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@2a8
    .line 2695
    new-instance v0, Ljava/lang/StringBuilder;

    #@2aa
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@2ad
    const-string v1, " isSubscriptionFromRuim="

    #@2af
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b2
    move-result-object v0

    #@2b3
    iget-boolean v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isSubscriptionFromRuim:Z

    #@2b5
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2b8
    move-result-object v0

    #@2b9
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2bc
    move-result-object v0

    #@2bd
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@2c0
    .line 2696
    new-instance v0, Ljava/lang/StringBuilder;

    #@2c2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@2c5
    const-string v1, " mCdmaSSM="

    #@2c7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2ca
    move-result-object v0

    #@2cb
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mCdmaSSM:Lcom/android/internal/telephony/cdma/CdmaSubscriptionSourceManager;

    #@2cd
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2d0
    move-result-object v0

    #@2d1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d4
    move-result-object v0

    #@2d5
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@2d8
    .line 2697
    new-instance v0, Ljava/lang/StringBuilder;

    #@2da
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@2dd
    const-string v1, " mRegistrationDeniedReason="

    #@2df
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e2
    move-result-object v0

    #@2e3
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mRegistrationDeniedReason:Ljava/lang/String;

    #@2e5
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e8
    move-result-object v0

    #@2e9
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2ec
    move-result-object v0

    #@2ed
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@2f0
    .line 2698
    new-instance v0, Ljava/lang/StringBuilder;

    #@2f2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@2f5
    const-string v1, " currentCarrier="

    #@2f7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2fa
    move-result-object v0

    #@2fb
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->currentCarrier:Ljava/lang/String;

    #@2fd
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@300
    move-result-object v0

    #@301
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@304
    move-result-object v0

    #@305
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@308
    .line 2699
    return-void
.end method

.method protected finalize()V
    .registers 2

    #@0
    .prologue
    .line 488
    const-string v0, "CdmaServiceStateTracker finalized"

    #@2
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@5
    .line 489
    return-void
.end method

.method protected fixTimeZone(Ljava/lang/String;)V
    .registers 14
    .parameter "isoCountryCode"

    #@0
    .prologue
    .line 1392
    const/4 v6, 0x0

    #@1
    .line 1395
    .local v6, zone:Ljava/util/TimeZone;
    const-string v8, "persist.sys.timezone"

    #@3
    invoke-static {v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v7

    #@7
    .line 1397
    .local v7, zoneName:Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v9, "fixTimeZone zoneName=\'"

    #@e
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v8

    #@12
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v8

    #@16
    const-string v9, "\' mZoneOffset="

    #@18
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v8

    #@1c
    iget v9, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mZoneOffset:I

    #@1e
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@21
    move-result-object v8

    #@22
    const-string v9, " mZoneDst="

    #@24
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v8

    #@28
    iget-boolean v9, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mZoneDst:Z

    #@2a
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v8

    #@2e
    const-string v9, " iso-cc=\'"

    #@30
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v8

    #@34
    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v8

    #@38
    const-string v9, "\' iso-cc-idx="

    #@3a
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v8

    #@3e
    sget-object v9, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->GMT_COUNTRY_CODES:[Ljava/lang/String;

    #@40
    invoke-static {v9, p1}, Ljava/util/Arrays;->binarySearch([Ljava/lang/Object;Ljava/lang/Object;)I

    #@43
    move-result v9

    #@44
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@47
    move-result-object v8

    #@48
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v8

    #@4c
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@4f
    .line 1403
    iget v8, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mZoneOffset:I

    #@51
    if-nez v8, :cond_118

    #@53
    iget-boolean v8, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mZoneDst:Z

    #@55
    if-nez v8, :cond_118

    #@57
    if-eqz v7, :cond_118

    #@59
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    #@5c
    move-result v8

    #@5d
    if-lez v8, :cond_118

    #@5f
    sget-object v8, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->GMT_COUNTRY_CODES:[Ljava/lang/String;

    #@61
    invoke-static {v8, p1}, Ljava/util/Arrays;->binarySearch([Ljava/lang/Object;Ljava/lang/Object;)I

    #@64
    move-result v8

    #@65
    if-gez v8, :cond_118

    #@67
    .line 1408
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    #@6a
    move-result-object v6

    #@6b
    .line 1409
    iget-boolean v8, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mNeedFixZone:Z

    #@6d
    if-eqz v8, :cond_c1

    #@6f
    .line 1410
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@72
    move-result-wide v2

    #@73
    .line 1411
    .local v2, ctm:J
    invoke-virtual {v6, v2, v3}, Ljava/util/TimeZone;->getOffset(J)I

    #@76
    move-result v8

    #@77
    int-to-long v4, v8

    #@78
    .line 1413
    .local v4, tzOffset:J
    new-instance v8, Ljava/lang/StringBuilder;

    #@7a
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@7d
    const-string v9, "fixTimeZone: tzOffset="

    #@7f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v8

    #@83
    invoke-virtual {v8, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@86
    move-result-object v8

    #@87
    const-string v9, " ltod="

    #@89
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v8

    #@8d
    invoke-static {v2, v3}, Landroid/util/TimeUtils;->logTimeOfDay(J)Ljava/lang/String;

    #@90
    move-result-object v9

    #@91
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v8

    #@95
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@98
    move-result-object v8

    #@99
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@9c
    .line 1416
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->getAutoTime()Z

    #@9f
    move-result v8

    #@a0
    if-eqz v8, :cond_fa

    #@a2
    .line 1417
    sub-long v0, v2, v4

    #@a4
    .line 1418
    .local v0, adj:J
    new-instance v8, Ljava/lang/StringBuilder;

    #@a6
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@a9
    const-string v9, "fixTimeZone: adj ltod="

    #@ab
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v8

    #@af
    invoke-static {v0, v1}, Landroid/util/TimeUtils;->logTimeOfDay(J)Ljava/lang/String;

    #@b2
    move-result-object v9

    #@b3
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b6
    move-result-object v8

    #@b7
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ba
    move-result-object v8

    #@bb
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@be
    .line 1419
    invoke-direct {p0, v0, v1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->setAndBroadcastNetworkSetTime(J)V

    #@c1
    .line 1426
    .end local v0           #adj:J
    .end local v2           #ctm:J
    .end local v4           #tzOffset:J
    :cond_c1
    :goto_c1
    const-string v8, "fixTimeZone: using default TimeZone"

    #@c3
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@c6
    .line 1445
    :cond_c6
    :goto_c6
    const/4 v8, 0x0

    #@c7
    iput-boolean v8, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mNeedFixZone:Z

    #@c9
    .line 1447
    if-eqz v6, :cond_167

    #@cb
    .line 1448
    new-instance v8, Ljava/lang/StringBuilder;

    #@cd
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@d0
    const-string v9, "fixTimeZone: zone != null zone.getID="

    #@d2
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v8

    #@d6
    invoke-virtual {v6}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    #@d9
    move-result-object v9

    #@da
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dd
    move-result-object v8

    #@de
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e1
    move-result-object v8

    #@e2
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@e5
    .line 1449
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->getAutoTimeZone()Z

    #@e8
    move-result v8

    #@e9
    if-eqz v8, :cond_161

    #@eb
    .line 1450
    invoke-virtual {v6}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    #@ee
    move-result-object v8

    #@ef
    invoke-direct {p0, v8}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->setAndBroadcastNetworkSetTimeZone(Ljava/lang/String;)V

    #@f2
    .line 1454
    :goto_f2
    invoke-virtual {v6}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    #@f5
    move-result-object v8

    #@f6
    invoke-direct {p0, v8}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->saveNitzTimeZone(Ljava/lang/String;)V

    #@f9
    .line 1458
    :goto_f9
    return-void

    #@fa
    .line 1422
    .restart local v2       #ctm:J
    .restart local v4       #tzOffset:J
    :cond_fa
    iget-wide v8, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mSavedTime:J

    #@fc
    sub-long/2addr v8, v4

    #@fd
    iput-wide v8, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mSavedTime:J

    #@ff
    .line 1423
    new-instance v8, Ljava/lang/StringBuilder;

    #@101
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@104
    const-string v9, "fixTimeZone: adj mSavedTime="

    #@106
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@109
    move-result-object v8

    #@10a
    iget-wide v9, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mSavedTime:J

    #@10c
    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@10f
    move-result-object v8

    #@110
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@113
    move-result-object v8

    #@114
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@117
    goto :goto_c1

    #@118
    .line 1427
    .end local v2           #ctm:J
    .end local v4           #tzOffset:J
    :cond_118
    const-string v8, ""

    #@11a
    invoke-virtual {p1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11d
    move-result v8

    #@11e
    if-eqz v8, :cond_130

    #@120
    .line 1430
    iget v8, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mZoneOffset:I

    #@122
    iget-boolean v9, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mZoneDst:Z

    #@124
    iget-wide v10, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mZoneTime:J

    #@126
    invoke-direct {p0, v8, v9, v10, v11}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->getNitzTimeZone(IZJ)Ljava/util/TimeZone;

    #@129
    move-result-object v6

    #@12a
    .line 1431
    const-string v8, "fixTimeZone: using NITZ TimeZone"

    #@12c
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@12f
    goto :goto_c6

    #@130
    .line 1433
    :cond_130
    iget v8, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mZoneOffset:I

    #@132
    iget-boolean v9, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mZoneDst:Z

    #@134
    iget-wide v10, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mZoneTime:J

    #@136
    invoke-static {v8, v9, v10, v11, p1}, Landroid/util/TimeUtils;->getTimeZone(IZJLjava/lang/String;)Ljava/util/TimeZone;

    #@139
    move-result-object v6

    #@13a
    .line 1434
    const-string v8, "fixTimeZone: using getTimeZone(off, dst, time, iso)"

    #@13c
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@13f
    .line 1437
    const-string v8, "US"

    #@141
    const-string v9, "VZW"

    #@143
    invoke-static {v8, v9}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@146
    move-result v8

    #@147
    if-nez v8, :cond_153

    #@149
    const-string v8, "US"

    #@14b
    const-string v9, "USC"

    #@14d
    invoke-static {v8, v9}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@150
    move-result v8

    #@151
    if-eqz v8, :cond_c6

    #@153
    .line 1438
    :cond_153
    if-nez v6, :cond_c6

    #@155
    .line 1439
    iget v8, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mZoneOffset:I

    #@157
    iget-boolean v9, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mZoneDst:Z

    #@159
    iget-wide v10, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mZoneTime:J

    #@15b
    invoke-direct {p0, v8, v9, v10, v11}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->getNitzTimeZone(IZJ)Ljava/util/TimeZone;

    #@15e
    move-result-object v6

    #@15f
    goto/16 :goto_c6

    #@161
    .line 1452
    :cond_161
    const-string v8, "fixTimeZone: skip changing zone as getAutoTimeZone was false"

    #@163
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@166
    goto :goto_f2

    #@167
    .line 1456
    :cond_167
    const-string v8, "fixTimeZone: zone == null, do nothing for zone"

    #@169
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@16c
    goto :goto_f9
.end method

.method public getAllCellInfo()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/telephony/CellInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 2553
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public getCdmaMin()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 2222
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mMin:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getCurrentDataConnectionState()I
    .registers 2

    #@0
    .prologue
    .line 1831
    iget v0, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mDataConnectionState:I

    #@2
    return v0
.end method

.method public getHDRRoamingIndicator()I
    .registers 2

    #@0
    .prologue
    .line 2702
    iget v0, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mHDRRoamingIndicator:I

    #@2
    return v0
.end method

.method public getImsi()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 2235
    const-string v1, "gsm.sim.operator.numeric"

    #@2
    const-string v2, ""

    #@4
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->getSystemProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    .line 2241
    .local v0, operatorNumeric:Ljava/lang/String;
    const-string v1, "US"

    #@a
    const-string v2, "SPR"

    #@c
    invoke-static {v1, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@f
    move-result v1

    #@10
    if-eqz v1, :cond_14

    #@12
    .line 2242
    const-string v0, "31000"

    #@14
    .line 2246
    :cond_14
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@17
    move-result v1

    #@18
    if-nez v1, :cond_36

    #@1a
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->getCdmaMin()Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    if-eqz v1, :cond_36

    #@20
    .line 2247
    new-instance v1, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v1

    #@29
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->getCdmaMin()Ljava/lang/String;

    #@2c
    move-result-object v2

    #@2d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v1

    #@31
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v1

    #@35
    .line 2249
    :goto_35
    return-object v1

    #@36
    :cond_36
    const/4 v1, 0x0

    #@37
    goto :goto_35
.end method

.method public getLteEhrpdForced()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 2730
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mLteEhrpdForced:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getMdnNumber()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 2218
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mMdn:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method getOtasp()I
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x6

    #@1
    const/4 v3, 0x0

    #@2
    .line 2267
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mMin:Ljava/lang/String;

    #@4
    if-eqz v1, :cond_e

    #@6
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mMin:Ljava/lang/String;

    #@8
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@b
    move-result v1

    #@c
    if-ge v1, v4, :cond_44

    #@e
    .line 2268
    :cond_e
    new-instance v1, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v2, "getOtasp: bad mMin=\'"

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mMin:Ljava/lang/String;

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    const-string v2, "\'"

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v1

    #@29
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@2c
    .line 2269
    const/4 v0, 0x1

    #@2d
    .line 2284
    .local v0, provisioningState:I
    :goto_2d
    new-instance v1, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    const-string v2, "getOtasp: state="

    #@34
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v1

    #@38
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v1

    #@3c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v1

    #@40
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@43
    .line 2285
    return v0

    #@44
    .line 2271
    .end local v0           #provisioningState:I
    :cond_44
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mMin:Ljava/lang/String;

    #@46
    const-string v2, "1111110111"

    #@48
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4b
    move-result v1

    #@4c
    if-nez v1, :cond_64

    #@4e
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mMin:Ljava/lang/String;

    #@50
    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@53
    move-result-object v1

    #@54
    const-string v2, "000000"

    #@56
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@59
    move-result v1

    #@5a
    if-nez v1, :cond_64

    #@5c
    const-string v1, "test_cdma_setup"

    #@5e
    invoke-static {v1, v3}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@61
    move-result v1

    #@62
    if-eqz v1, :cond_66

    #@64
    .line 2274
    :cond_64
    const/4 v0, 0x2

    #@65
    .restart local v0       #provisioningState:I
    goto :goto_2d

    #@66
    .line 2276
    .end local v0           #provisioningState:I
    :cond_66
    const-string v1, "KDDI"

    #@68
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@6b
    move-result v1

    #@6c
    if-eqz v1, :cond_7d

    #@6e
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mMdn:Ljava/lang/String;

    #@70
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@73
    move-result v1

    #@74
    if-eqz v1, :cond_7d

    #@76
    .line 2277
    const-string v1, "Provisioning need and then no datacall"

    #@78
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@7b
    .line 2278
    const/4 v0, 0x2

    #@7c
    .restart local v0       #provisioningState:I
    goto :goto_2d

    #@7d
    .line 2281
    .end local v0           #provisioningState:I
    :cond_7d
    const/4 v0, 0x3

    #@7e
    .restart local v0       #provisioningState:I
    goto :goto_2d
.end method

.method protected getPhone()Lcom/android/internal/telephony/Phone;
    .registers 2

    #@0
    .prologue
    .line 964
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@2
    return-object v0
.end method

.method public getPrlVersion()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 2227
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mPrlVersion:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method protected getSystemProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "property"
    .parameter "defValue"

    #@0
    .prologue
    .line 977
    invoke-static {p1, p2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method protected getUiccCardApplication()Lcom/android/internal/telephony/uicc/UiccCardApplication;
    .registers 3

    #@0
    .prologue
    .line 2513
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@2
    const/4 v1, 0x2

    #@3
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/uicc/UiccController;->getUiccCardApplication(I)Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 27
    .parameter "msg"

    #@0
    .prologue
    .line 534
    move-object/from16 v0, p0

    #@2
    iget-object v4, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@4
    iget-boolean v4, v4, Lcom/android/internal/telephony/PhoneBase;->mIsTheCurrentActivePhone:Z

    #@6
    if-nez v4, :cond_7c

    #@8
    .line 535
    new-instance v4, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v21, "Received message "

    #@f
    move-object/from16 v0, v21

    #@11
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v4

    #@15
    move-object/from16 v0, p1

    #@17
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v4

    #@1b
    const-string v21, "["

    #@1d
    move-object/from16 v0, v21

    #@1f
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v4

    #@23
    move-object/from16 v0, p1

    #@25
    iget v0, v0, Landroid/os/Message;->what:I

    #@27
    move/from16 v21, v0

    #@29
    move/from16 v0, v21

    #@2b
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v4

    #@2f
    const-string v21, "]"

    #@31
    move-object/from16 v0, v21

    #@33
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v4

    #@37
    const-string v21, " while being destroyed. Ignoring."

    #@39
    move-object/from16 v0, v21

    #@3b
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v4

    #@3f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v4

    #@43
    move-object/from16 v0, p0

    #@45
    invoke-virtual {v0, v4}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->loge(Ljava/lang/String;)V

    #@48
    .line 538
    if-eqz p1, :cond_7b

    #@4a
    move-object/from16 v0, p1

    #@4c
    iget v4, v0, Landroid/os/Message;->what:I

    #@4e
    const/16 v21, 0xb

    #@50
    move/from16 v0, v21

    #@52
    if-ne v4, v0, :cond_7b

    #@54
    .line 539
    const-string v4, "NITZ received while disposing CDMAPhone!!"

    #@56
    move-object/from16 v0, p0

    #@58
    invoke-virtual {v0, v4}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@5b
    .line 541
    sget-object v4, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mTimeZoneMonitor:Lcom/android/internal/telephony/LgeTimeZoneMonitor;

    #@5d
    if-nez v4, :cond_65

    #@5f
    .line 542
    invoke-static {}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->getDefault()Lcom/android/internal/telephony/LgeTimeZoneMonitor;

    #@62
    move-result-object v4

    #@63
    sput-object v4, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mTimeZoneMonitor:Lcom/android/internal/telephony/LgeTimeZoneMonitor;

    #@65
    .line 545
    :cond_65
    sget-object v4, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mTimeZoneMonitor:Lcom/android/internal/telephony/LgeTimeZoneMonitor;

    #@67
    if-eqz v4, :cond_7b

    #@69
    .line 546
    const-string v4, "Save NITZ info. to restore it after phone-switching"

    #@6b
    move-object/from16 v0, p0

    #@6d
    invoke-virtual {v0, v4}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@70
    .line 547
    sget-object v4, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mTimeZoneMonitor:Lcom/android/internal/telephony/LgeTimeZoneMonitor;

    #@72
    move-object/from16 v0, p1

    #@74
    iget-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@76
    check-cast v4, Landroid/os/AsyncResult;

    #@78
    invoke-static {v4}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->setLostNitzInfo(Landroid/os/AsyncResult;)V

    #@7b
    .line 852
    :cond_7b
    :goto_7b
    return-void

    #@7c
    .line 554
    :cond_7c
    move-object/from16 v0, p1

    #@7e
    iget v4, v0, Landroid/os/Message;->what:I

    #@80
    sparse-switch v4, :sswitch_data_50a

    #@83
    .line 849
    invoke-super/range {p0 .. p1}, Lcom/android/internal/telephony/ServiceStateTracker;->handleMessage(Landroid/os/Message;)V

    #@86
    goto :goto_7b

    #@87
    .line 556
    :sswitch_87
    move-object/from16 v0, p0

    #@89
    iget-object v4, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mCdmaSSM:Lcom/android/internal/telephony/cdma/CdmaSubscriptionSourceManager;

    #@8b
    invoke-virtual {v4}, Lcom/android/internal/telephony/cdma/CdmaSubscriptionSourceManager;->getCdmaSubscriptionSource()I

    #@8e
    move-result v4

    #@8f
    move-object/from16 v0, p0

    #@91
    invoke-direct {v0, v4}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->handleCdmaSubscriptionSource(I)V

    #@94
    goto :goto_7b

    #@95
    .line 563
    :sswitch_95
    move-object/from16 v0, p0

    #@97
    iget-object v4, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@99
    invoke-virtual {v4}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getLteOnCdmaMode()I

    #@9c
    move-result v4

    #@9d
    const/16 v21, 0x1

    #@9f
    move/from16 v0, v21

    #@a1
    if-ne v4, v0, :cond_b5

    #@a3
    .line 565
    const-string v4, "Receive EVENT_RUIM_READY"

    #@a5
    move-object/from16 v0, p0

    #@a7
    invoke-virtual {v0, v4}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@aa
    .line 566
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->pollState()V

    #@ad
    .line 571
    :goto_ad
    move-object/from16 v0, p0

    #@af
    iget-object v4, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@b1
    invoke-virtual {v4}, Lcom/android/internal/telephony/cdma/CDMAPhone;->prepareEri()V

    #@b4
    goto :goto_7b

    #@b5
    .line 568
    :cond_b5
    const-string v4, "Receive EVENT_RUIM_READY and Send Request getCDMASubscription."

    #@b7
    move-object/from16 v0, p0

    #@b9
    invoke-virtual {v0, v4}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@bc
    .line 569
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->getSubscriptionInfoAndStartPollingThreads()V

    #@bf
    goto :goto_ad

    #@c0
    .line 578
    :sswitch_c0
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->getSubscriptionInfoAndStartPollingThreads()V

    #@c3
    goto :goto_7b

    #@c4
    .line 582
    :sswitch_c4
    move-object/from16 v0, p0

    #@c6
    iget-object v4, v0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@c8
    invoke-interface {v4}, Lcom/android/internal/telephony/CommandsInterface;->getRadioState()Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@cb
    move-result-object v4

    #@cc
    sget-object v21, Lcom/android/internal/telephony/CommandsInterface$RadioState;->RADIO_ON:Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@ce
    move-object/from16 v0, v21

    #@d0
    if-ne v4, v0, :cond_124

    #@d2
    .line 583
    move-object/from16 v0, p0

    #@d4
    iget-object v4, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mCdmaSSM:Lcom/android/internal/telephony/cdma/CdmaSubscriptionSourceManager;

    #@d6
    invoke-virtual {v4}, Lcom/android/internal/telephony/cdma/CdmaSubscriptionSourceManager;->getCdmaSubscriptionSource()I

    #@d9
    move-result v4

    #@da
    move-object/from16 v0, p0

    #@dc
    invoke-direct {v0, v4}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->handleCdmaSubscriptionSource(I)V

    #@df
    .line 586
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->queueNextSignalStrengthPoll()V

    #@e2
    .line 588
    const-string v4, "VZW"

    #@e4
    invoke-static {v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@e7
    move-result v4

    #@e8
    if-eqz v4, :cond_124

    #@ea
    .line 589
    move-object/from16 v0, p0

    #@ec
    iget-object v4, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->cr:Landroid/content/ContentResolver;

    #@ee
    const-string v21, "apn2_disable"

    #@f0
    const/16 v22, 0x0

    #@f2
    move-object/from16 v0, v21

    #@f4
    move/from16 v1, v22

    #@f6
    invoke-static {v4, v0, v1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@f9
    move-result v4

    #@fa
    const/16 v21, 0x1

    #@fc
    move/from16 v0, v21

    #@fe
    if-ne v4, v0, :cond_12c

    #@100
    const/4 v10, 0x1

    #@101
    .line 590
    .local v10, apn2_disable_Mode:Z
    :goto_101
    const-string v4, "CDMA"

    #@103
    new-instance v21, Ljava/lang/StringBuilder;

    #@105
    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    #@108
    const-string v22, "apn2_disable_Mode: "

    #@10a
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10d
    move-result-object v21

    #@10e
    move-object/from16 v0, v21

    #@110
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@113
    move-result-object v21

    #@114
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@117
    move-result-object v21

    #@118
    move-object/from16 v0, v21

    #@11a
    invoke-static {v4, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@11d
    .line 591
    if-eqz v10, :cond_124

    #@11f
    .line 592
    const/4 v4, 0x1

    #@120
    move-object/from16 v0, p0

    #@122
    iput-boolean v4, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mDesiredPowerState:Z

    #@124
    .line 598
    .end local v10           #apn2_disable_Mode:Z
    :cond_124
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->setPowerStateToDesired()V

    #@127
    .line 599
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->pollState()V

    #@12a
    goto/16 :goto_7b

    #@12c
    .line 589
    :cond_12c
    const/4 v10, 0x0

    #@12d
    goto :goto_101

    #@12e
    .line 603
    :sswitch_12e
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->pollState()V

    #@131
    goto/16 :goto_7b

    #@133
    .line 610
    :sswitch_133
    move-object/from16 v0, p0

    #@135
    iget-object v4, v0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@137
    invoke-interface {v4}, Lcom/android/internal/telephony/CommandsInterface;->getRadioState()Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@13a
    move-result-object v4

    #@13b
    invoke-virtual {v4}, Lcom/android/internal/telephony/CommandsInterface$RadioState;->isOn()Z

    #@13e
    move-result v4

    #@13f
    if-eqz v4, :cond_7b

    #@141
    .line 614
    move-object/from16 v0, p1

    #@143
    iget-object v11, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@145
    check-cast v11, Landroid/os/AsyncResult;

    #@147
    .line 615
    .local v11, ar:Landroid/os/AsyncResult;
    const/4 v4, 0x0

    #@148
    move-object/from16 v0, p0

    #@14a
    invoke-virtual {v0, v11, v4}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->onSignalStrengthResult(Landroid/os/AsyncResult;Z)Z

    #@14d
    .line 616
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->queueNextSignalStrengthPoll()V

    #@150
    goto/16 :goto_7b

    #@152
    .line 621
    .end local v11           #ar:Landroid/os/AsyncResult;
    :sswitch_152
    move-object/from16 v0, p1

    #@154
    iget-object v11, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@156
    check-cast v11, Landroid/os/AsyncResult;

    #@158
    .line 623
    .restart local v11       #ar:Landroid/os/AsyncResult;
    iget-object v4, v11, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@15a
    if-nez v4, :cond_1f0

    #@15c
    .line 624
    iget-object v4, v11, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@15e
    check-cast v4, [Ljava/lang/String;

    #@160
    move-object/from16 v19, v4

    #@162
    check-cast v19, [Ljava/lang/String;

    #@164
    .line 625
    .local v19, states:[Ljava/lang/String;
    const/4 v5, -0x1

    #@165
    .line 626
    .local v5, baseStationId:I
    const v6, 0x7fffffff

    #@168
    .line 627
    .local v6, baseStationLatitude:I
    const v7, 0x7fffffff

    #@16b
    .line 628
    .local v7, baseStationLongitude:I
    const/4 v8, -0x1

    #@16c
    .line 629
    .local v8, systemId:I
    const/4 v9, -0x1

    #@16d
    .line 631
    .local v9, networkId:I
    move-object/from16 v0, v19

    #@16f
    array-length v4, v0

    #@170
    const/16 v21, 0x9

    #@172
    move/from16 v0, v21

    #@174
    if-le v4, v0, :cond_1e2

    #@176
    .line 633
    const/4 v4, 0x4

    #@177
    :try_start_177
    aget-object v4, v19, v4

    #@179
    if-eqz v4, :cond_182

    #@17b
    .line 634
    const/4 v4, 0x4

    #@17c
    aget-object v4, v19, v4

    #@17e
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@181
    move-result v5

    #@182
    .line 636
    :cond_182
    const/4 v4, 0x5

    #@183
    aget-object v4, v19, v4

    #@185
    if-eqz v4, :cond_18e

    #@187
    .line 637
    const/4 v4, 0x5

    #@188
    aget-object v4, v19, v4

    #@18a
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@18d
    move-result v6

    #@18e
    .line 639
    :cond_18e
    const/4 v4, 0x6

    #@18f
    aget-object v4, v19, v4

    #@191
    if-eqz v4, :cond_19a

    #@193
    .line 640
    const/4 v4, 0x6

    #@194
    aget-object v4, v19, v4

    #@196
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@199
    move-result v7

    #@19a
    .line 643
    :cond_19a
    if-nez v6, :cond_1a4

    #@19c
    if-nez v7, :cond_1a4

    #@19e
    .line 644
    const v6, 0x7fffffff

    #@1a1
    .line 645
    const v7, 0x7fffffff

    #@1a4
    .line 647
    :cond_1a4
    const/16 v4, 0x8

    #@1a6
    aget-object v4, v19, v4

    #@1a8
    if-eqz v4, :cond_1b2

    #@1aa
    .line 648
    const/16 v4, 0x8

    #@1ac
    aget-object v4, v19, v4

    #@1ae
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@1b1
    move-result v8

    #@1b2
    .line 650
    :cond_1b2
    const/16 v4, 0x9

    #@1b4
    aget-object v4, v19, v4

    #@1b6
    if-eqz v4, :cond_1c0

    #@1b8
    .line 651
    const/16 v4, 0x9

    #@1ba
    aget-object v4, v19, v4

    #@1bc
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@1bf
    move-result v9

    #@1c0
    .line 657
    :cond_1c0
    const/4 v4, 0x0

    #@1c1
    const-string v21, "sprint_location_spec"

    #@1c3
    move-object/from16 v0, v21

    #@1c5
    invoke-static {v4, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@1c8
    move-result v4

    #@1c9
    if-eqz v4, :cond_1e2

    #@1cb
    .line 658
    move-object/from16 v0, p0

    #@1cd
    iget-object v4, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->cr:Landroid/content/ContentResolver;

    #@1cf
    const-string v21, "network"

    #@1d1
    move-object/from16 v0, v21

    #@1d3
    invoke-static {v4, v0}, Landroid/provider/Settings$Secure;->isLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;)Z
    :try_end_1d6
    .catch Ljava/lang/NumberFormatException; {:try_start_177 .. :try_end_1d6} :catch_1f5

    #@1d6
    move-result v4

    #@1d7
    if-nez v4, :cond_1e2

    #@1d9
    .line 659
    const/4 v5, -0x1

    #@1da
    .line 660
    const v6, 0x7fffffff

    #@1dd
    .line 661
    const v7, 0x7fffffff

    #@1e0
    .line 662
    const/4 v8, -0x1

    #@1e1
    .line 663
    const/4 v9, -0x1

    #@1e2
    .line 673
    :cond_1e2
    :goto_1e2
    move-object/from16 v0, p0

    #@1e4
    iget-object v4, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->cellLoc:Landroid/telephony/cdma/CdmaCellLocation;

    #@1e6
    invoke-virtual/range {v4 .. v9}, Landroid/telephony/cdma/CdmaCellLocation;->setCellLocationData(IIIII)V

    #@1e9
    .line 675
    move-object/from16 v0, p0

    #@1eb
    iget-object v4, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@1ed
    invoke-virtual {v4}, Lcom/android/internal/telephony/cdma/CDMAPhone;->notifyLocationChanged()V

    #@1f0
    .line 680
    .end local v5           #baseStationId:I
    .end local v6           #baseStationLatitude:I
    .end local v7           #baseStationLongitude:I
    .end local v8           #systemId:I
    .end local v9           #networkId:I
    .end local v19           #states:[Ljava/lang/String;
    :cond_1f0
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->disableSingleLocationUpdate()V

    #@1f3
    goto/16 :goto_7b

    #@1f5
    .line 668
    .restart local v5       #baseStationId:I
    .restart local v6       #baseStationLatitude:I
    .restart local v7       #baseStationLongitude:I
    .restart local v8       #systemId:I
    .restart local v9       #networkId:I
    .restart local v19       #states:[Ljava/lang/String;
    :catch_1f5
    move-exception v13

    #@1f6
    .line 669
    .local v13, ex:Ljava/lang/NumberFormatException;
    new-instance v4, Ljava/lang/StringBuilder;

    #@1f8
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1fb
    const-string v21, "error parsing cell location data: "

    #@1fd
    move-object/from16 v0, v21

    #@1ff
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@202
    move-result-object v4

    #@203
    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@206
    move-result-object v4

    #@207
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20a
    move-result-object v4

    #@20b
    move-object/from16 v0, p0

    #@20d
    invoke-virtual {v0, v4}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->loge(Ljava/lang/String;)V

    #@210
    goto :goto_1e2

    #@211
    .line 685
    .end local v5           #baseStationId:I
    .end local v6           #baseStationLatitude:I
    .end local v7           #baseStationLongitude:I
    .end local v8           #systemId:I
    .end local v9           #networkId:I
    .end local v11           #ar:Landroid/os/AsyncResult;
    .end local v13           #ex:Ljava/lang/NumberFormatException;
    .end local v19           #states:[Ljava/lang/String;
    :sswitch_211
    move-object/from16 v0, p1

    #@213
    iget-object v11, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@215
    check-cast v11, Landroid/os/AsyncResult;

    #@217
    .line 686
    .restart local v11       #ar:Landroid/os/AsyncResult;
    move-object/from16 v0, p1

    #@219
    iget v4, v0, Landroid/os/Message;->what:I

    #@21b
    move-object/from16 v0, p0

    #@21d
    invoke-virtual {v0, v4, v11}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->handlePollStateResult(ILandroid/os/AsyncResult;)V

    #@220
    goto/16 :goto_7b

    #@222
    .line 690
    .end local v11           #ar:Landroid/os/AsyncResult;
    :sswitch_222
    move-object/from16 v0, p1

    #@224
    iget-object v11, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@226
    check-cast v11, Landroid/os/AsyncResult;

    #@228
    .line 692
    .restart local v11       #ar:Landroid/os/AsyncResult;
    iget-object v4, v11, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@22a
    if-nez v4, :cond_7b

    #@22c
    .line 693
    iget-object v4, v11, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@22e
    check-cast v4, [Ljava/lang/String;

    #@230
    move-object v12, v4

    #@231
    check-cast v12, [Ljava/lang/String;

    #@233
    .line 694
    .local v12, cdmaSubscription:[Ljava/lang/String;
    if-eqz v12, :cond_37c

    #@235
    array-length v4, v12

    #@236
    const/16 v21, 0x5

    #@238
    move/from16 v0, v21

    #@23a
    if-lt v4, v0, :cond_37c

    #@23c
    .line 695
    const/4 v4, 0x0

    #@23d
    aget-object v4, v12, v4

    #@23f
    move-object/from16 v0, p0

    #@241
    iput-object v4, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mMdn:Ljava/lang/String;

    #@243
    .line 698
    move-object/from16 v0, p0

    #@245
    iget-object v4, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@247
    invoke-virtual {v4}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@24a
    move-result-object v4

    #@24b
    const-string v21, "support_assisted_dialing"

    #@24d
    move-object/from16 v0, v21

    #@24f
    invoke-static {v4, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@252
    move-result v4

    #@253
    if-eqz v4, :cond_30b

    #@255
    .line 699
    new-instance v4, Ljava/lang/StringBuilder;

    #@257
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@25a
    const-string v21, "EVENT_POLL_STATE_CDMA_SUBSCRIPTION : "

    #@25c
    move-object/from16 v0, v21

    #@25e
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@261
    move-result-object v4

    #@262
    move-object/from16 v0, p0

    #@264
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mMdn:Ljava/lang/String;

    #@266
    move-object/from16 v21, v0

    #@268
    move-object/from16 v0, v21

    #@26a
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26d
    move-result-object v4

    #@26e
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@271
    move-result-object v4

    #@272
    move-object/from16 v0, p0

    #@274
    invoke-virtual {v0, v4}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@277
    .line 701
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isIccIdChanged()Z

    #@27a
    move-result v4

    #@27b
    if-eqz v4, :cond_30b

    #@27d
    .line 702
    const-string v4, "isIccIdChanged = true, Area/Length Update"

    #@27f
    move-object/from16 v0, p0

    #@281
    invoke-virtual {v0, v4}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@284
    .line 703
    move-object/from16 v0, p0

    #@286
    iget-object v4, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mMdn:Ljava/lang/String;

    #@288
    if-eqz v4, :cond_30b

    #@28a
    move-object/from16 v0, p0

    #@28c
    iget-object v4, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mMdn:Ljava/lang/String;

    #@28e
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    #@291
    move-result v4

    #@292
    const/16 v21, 0x3

    #@294
    move/from16 v0, v21

    #@296
    if-lt v4, v0, :cond_30b

    #@298
    .line 704
    move-object/from16 v0, p0

    #@29a
    iget-object v4, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@29c
    invoke-virtual {v4}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@29f
    move-result-object v4

    #@2a0
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@2a3
    move-result-object v4

    #@2a4
    const-string v21, "area"

    #@2a6
    move-object/from16 v0, p0

    #@2a8
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mMdn:Ljava/lang/String;

    #@2aa
    move-object/from16 v22, v0

    #@2ac
    const/16 v23, 0x0

    #@2ae
    const/16 v24, 0x3

    #@2b0
    invoke-virtual/range {v22 .. v24}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@2b3
    move-result-object v22

    #@2b4
    move-object/from16 v0, v21

    #@2b6
    move-object/from16 v1, v22

    #@2b8
    invoke-static {v4, v0, v1}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    #@2bb
    .line 708
    new-instance v20, Landroid/content/ContentValues;

    #@2bd
    invoke-direct/range {v20 .. v20}, Landroid/content/ContentValues;-><init>()V

    #@2c0
    .line 709
    .local v20, values:Landroid/content/ContentValues;
    const-string v4, "area"

    #@2c2
    move-object/from16 v0, p0

    #@2c4
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mMdn:Ljava/lang/String;

    #@2c6
    move-object/from16 v21, v0

    #@2c8
    const/16 v22, 0x0

    #@2ca
    const/16 v23, 0x3

    #@2cc
    invoke-virtual/range {v21 .. v23}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@2cf
    move-result-object v21

    #@2d0
    move-object/from16 v0, v20

    #@2d2
    move-object/from16 v1, v21

    #@2d4
    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@2d7
    .line 710
    const-string v4, "length"

    #@2d9
    move-object/from16 v0, p0

    #@2db
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mMdn:Ljava/lang/String;

    #@2dd
    move-object/from16 v21, v0

    #@2df
    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    #@2e2
    move-result v21

    #@2e3
    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2e6
    move-result-object v21

    #@2e7
    move-object/from16 v0, v20

    #@2e9
    move-object/from16 v1, v21

    #@2eb
    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@2ee
    .line 711
    move-object/from16 v0, p0

    #@2f0
    iget-object v4, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@2f2
    invoke-virtual {v4}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@2f5
    move-result-object v4

    #@2f6
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@2f9
    move-result-object v4

    #@2fa
    sget-object v21, Landroid/provider/Settings$AssistDial;->CONTENT_URI:Landroid/net/Uri;

    #@2fc
    const/16 v22, 0x0

    #@2fe
    const/16 v23, 0x0

    #@300
    move-object/from16 v0, v21

    #@302
    move-object/from16 v1, v20

    #@304
    move-object/from16 v2, v22

    #@306
    move-object/from16 v3, v23

    #@308
    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@30b
    .line 719
    .end local v20           #values:Landroid/content/ContentValues;
    :cond_30b
    const/4 v4, 0x1

    #@30c
    aget-object v4, v12, v4

    #@30e
    const/16 v21, 0x2

    #@310
    aget-object v21, v12, v21

    #@312
    move-object/from16 v0, p0

    #@314
    move-object/from16 v1, v21

    #@316
    invoke-virtual {v0, v4, v1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->parseSidNid(Ljava/lang/String;Ljava/lang/String;)V

    #@319
    .line 721
    const/4 v4, 0x3

    #@31a
    aget-object v4, v12, v4

    #@31c
    move-object/from16 v0, p0

    #@31e
    iput-object v4, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mMin:Ljava/lang/String;

    #@320
    .line 722
    const/4 v4, 0x4

    #@321
    aget-object v4, v12, v4

    #@323
    move-object/from16 v0, p0

    #@325
    iput-object v4, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mPrlVersion:Ljava/lang/String;

    #@327
    .line 723
    new-instance v4, Ljava/lang/StringBuilder;

    #@329
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@32c
    const-string v21, "GET_CDMA_SUBSCRIPTION: MDN="

    #@32e
    move-object/from16 v0, v21

    #@330
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@333
    move-result-object v4

    #@334
    move-object/from16 v0, p0

    #@336
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mMdn:Ljava/lang/String;

    #@338
    move-object/from16 v21, v0

    #@33a
    move-object/from16 v0, v21

    #@33c
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33f
    move-result-object v4

    #@340
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@343
    move-result-object v4

    #@344
    move-object/from16 v0, p0

    #@346
    invoke-virtual {v0, v4}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@349
    .line 725
    const/4 v4, 0x1

    #@34a
    move-object/from16 v0, p0

    #@34c
    iput-boolean v4, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mIsMinInfoReady:Z

    #@34e
    .line 727
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->updateOtaspState()V

    #@351
    .line 728
    move-object/from16 v0, p0

    #@353
    iget-boolean v4, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isSubscriptionFromRuim:Z

    #@355
    if-nez v4, :cond_373

    #@357
    move-object/from16 v0, p0

    #@359
    iget-object v4, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@35b
    if-eqz v4, :cond_373

    #@35d
    .line 730
    const-string v4, "GET_CDMA_SUBSCRIPTION set imsi in mIccRecords"

    #@35f
    move-object/from16 v0, p0

    #@361
    invoke-virtual {v0, v4}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@364
    .line 732
    move-object/from16 v0, p0

    #@366
    iget-object v4, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@368
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->getImsi()Ljava/lang/String;

    #@36b
    move-result-object v21

    #@36c
    move-object/from16 v0, v21

    #@36e
    invoke-virtual {v4, v0}, Lcom/android/internal/telephony/uicc/IccRecords;->setImsi(Ljava/lang/String;)V

    #@371
    goto/16 :goto_7b

    #@373
    .line 735
    :cond_373
    const-string v4, "GET_CDMA_SUBSCRIPTION either mIccRecords is null  or NV type device - not setting Imsi in mIccRecords"

    #@375
    move-object/from16 v0, p0

    #@377
    invoke-virtual {v0, v4}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@37a
    goto/16 :goto_7b

    #@37c
    .line 741
    :cond_37c
    new-instance v4, Ljava/lang/StringBuilder;

    #@37e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@381
    const-string v21, "GET_CDMA_SUBSCRIPTION: error parsing cdmaSubscription params num="

    #@383
    move-object/from16 v0, v21

    #@385
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@388
    move-result-object v4

    #@389
    array-length v0, v12

    #@38a
    move/from16 v21, v0

    #@38c
    move/from16 v0, v21

    #@38e
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@391
    move-result-object v4

    #@392
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@395
    move-result-object v4

    #@396
    move-object/from16 v0, p0

    #@398
    invoke-virtual {v0, v4}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@39b
    goto/16 :goto_7b

    #@39d
    .line 751
    .end local v11           #ar:Landroid/os/AsyncResult;
    .end local v12           #cdmaSubscription:[Ljava/lang/String;
    :sswitch_39d
    move-object/from16 v0, p0

    #@39f
    iget-object v4, v0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@3a1
    const/16 v21, 0x3

    #@3a3
    move-object/from16 v0, p0

    #@3a5
    move/from16 v1, v21

    #@3a7
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->obtainMessage(I)Landroid/os/Message;

    #@3aa
    move-result-object v21

    #@3ab
    move-object/from16 v0, v21

    #@3ad
    invoke-interface {v4, v0}, Lcom/android/internal/telephony/CommandsInterface;->getSignalStrength(Landroid/os/Message;)V

    #@3b0
    goto/16 :goto_7b

    #@3b2
    .line 755
    :sswitch_3b2
    move-object/from16 v0, p1

    #@3b4
    iget-object v11, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@3b6
    check-cast v11, Landroid/os/AsyncResult;

    #@3b8
    .line 757
    .restart local v11       #ar:Landroid/os/AsyncResult;
    iget-object v4, v11, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@3ba
    check-cast v4, [Ljava/lang/Object;

    #@3bc
    check-cast v4, [Ljava/lang/Object;

    #@3be
    const/16 v21, 0x0

    #@3c0
    aget-object v17, v4, v21

    #@3c2
    check-cast v17, Ljava/lang/String;

    #@3c4
    .line 758
    .local v17, nitzString:Ljava/lang/String;
    iget-object v4, v11, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@3c6
    check-cast v4, [Ljava/lang/Object;

    #@3c8
    check-cast v4, [Ljava/lang/Object;

    #@3ca
    const/16 v21, 0x1

    #@3cc
    aget-object v4, v4, v21

    #@3ce
    check-cast v4, Ljava/lang/Long;

    #@3d0
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    #@3d3
    move-result-wide v15

    #@3d4
    .line 760
    .local v15, nitzReceiveTime:J
    move-object/from16 v0, p0

    #@3d6
    move-object/from16 v1, v17

    #@3d8
    move-wide v2, v15

    #@3d9
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->setTimeFromNITZString(Ljava/lang/String;J)V

    #@3dc
    goto/16 :goto_7b

    #@3de
    .line 766
    .end local v11           #ar:Landroid/os/AsyncResult;
    .end local v15           #nitzReceiveTime:J
    .end local v17           #nitzString:Ljava/lang/String;
    :sswitch_3de
    move-object/from16 v0, p1

    #@3e0
    iget-object v11, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@3e2
    check-cast v11, Landroid/os/AsyncResult;

    #@3e4
    .line 770
    .restart local v11       #ar:Landroid/os/AsyncResult;
    const/4 v4, 0x1

    #@3e5
    move-object/from16 v0, p0

    #@3e7
    iput-boolean v4, v0, Lcom/android/internal/telephony/ServiceStateTracker;->dontPollSignalStrength:Z

    #@3e9
    .line 772
    const/4 v4, 0x0

    #@3ea
    move-object/from16 v0, p0

    #@3ec
    invoke-virtual {v0, v11, v4}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->onSignalStrengthResult(Landroid/os/AsyncResult;Z)Z

    #@3ef
    goto/16 :goto_7b

    #@3f1
    .line 776
    .end local v11           #ar:Landroid/os/AsyncResult;
    :sswitch_3f1
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->updateSpnDisplay()V

    #@3f4
    goto/16 :goto_7b

    #@3f6
    .line 780
    :sswitch_3f6
    move-object/from16 v0, p1

    #@3f8
    iget-object v11, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@3fa
    check-cast v11, Landroid/os/AsyncResult;

    #@3fc
    .line 782
    .restart local v11       #ar:Landroid/os/AsyncResult;
    iget-object v4, v11, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@3fe
    if-nez v4, :cond_7b

    #@400
    .line 783
    move-object/from16 v0, p0

    #@402
    iget-object v4, v0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@404
    const/16 v21, 0x1f

    #@406
    const/16 v22, 0x0

    #@408
    move-object/from16 v0, p0

    #@40a
    move/from16 v1, v21

    #@40c
    move-object/from16 v2, v22

    #@40e
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@411
    move-result-object v21

    #@412
    move-object/from16 v0, v21

    #@414
    invoke-interface {v4, v0}, Lcom/android/internal/telephony/CommandsInterface;->getVoiceRegistrationState(Landroid/os/Message;)V

    #@417
    goto/16 :goto_7b

    #@419
    .line 789
    .end local v11           #ar:Landroid/os/AsyncResult;
    :sswitch_419
    const-string v4, "[CdmaServiceStateTracker] ERI file has been loaded, repolling."

    #@41b
    move-object/from16 v0, p0

    #@41d
    invoke-virtual {v0, v4}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@420
    .line 790
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->pollState()V

    #@423
    goto/16 :goto_7b

    #@425
    .line 794
    :sswitch_425
    move-object/from16 v0, p1

    #@427
    iget-object v11, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@429
    check-cast v11, Landroid/os/AsyncResult;

    #@42b
    .line 795
    .restart local v11       #ar:Landroid/os/AsyncResult;
    iget-object v4, v11, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@42d
    if-nez v4, :cond_7b

    #@42f
    .line 796
    iget-object v4, v11, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@431
    check-cast v4, [I

    #@433
    move-object v14, v4

    #@434
    check-cast v14, [I

    #@436
    .line 797
    .local v14, ints:[I
    const/4 v4, 0x0

    #@437
    aget v18, v14, v4

    #@439
    .line 798
    .local v18, otaStatus:I
    const/16 v4, 0x8

    #@43b
    move/from16 v0, v18

    #@43d
    if-eq v0, v4, :cond_445

    #@43f
    const/16 v4, 0xa

    #@441
    move/from16 v0, v18

    #@443
    if-ne v0, v4, :cond_7b

    #@445
    .line 800
    :cond_445
    const-string v4, "EVENT_OTA_PROVISION_STATUS_CHANGE: Complete, Reload MDN"

    #@447
    move-object/from16 v0, p0

    #@449
    invoke-virtual {v0, v4}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@44c
    .line 801
    move-object/from16 v0, p0

    #@44e
    iget-object v4, v0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@450
    const/16 v21, 0x22

    #@452
    move-object/from16 v0, p0

    #@454
    move/from16 v1, v21

    #@456
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->obtainMessage(I)Landroid/os/Message;

    #@459
    move-result-object v21

    #@45a
    move-object/from16 v0, v21

    #@45c
    invoke-interface {v4, v0}, Lcom/android/internal/telephony/CommandsInterface;->getCDMASubscription(Landroid/os/Message;)V

    #@45f
    goto/16 :goto_7b

    #@461
    .line 807
    .end local v11           #ar:Landroid/os/AsyncResult;
    .end local v14           #ints:[I
    .end local v18           #otaStatus:I
    :sswitch_461
    move-object/from16 v0, p1

    #@463
    iget-object v11, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@465
    check-cast v11, Landroid/os/AsyncResult;

    #@467
    .line 808
    .restart local v11       #ar:Landroid/os/AsyncResult;
    iget-object v4, v11, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@469
    if-nez v4, :cond_7b

    #@46b
    .line 809
    iget-object v4, v11, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@46d
    check-cast v4, [I

    #@46f
    move-object v14, v4

    #@470
    check-cast v14, [I

    #@472
    .line 810
    .restart local v14       #ints:[I
    const/4 v4, 0x0

    #@473
    aget v4, v14, v4

    #@475
    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@478
    move-result-object v4

    #@479
    move-object/from16 v0, p0

    #@47b
    iput-object v4, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mPrlVersion:Ljava/lang/String;

    #@47d
    goto/16 :goto_7b

    #@47f
    .line 815
    .end local v11           #ar:Landroid/os/AsyncResult;
    .end local v14           #ints:[I
    :sswitch_47f
    move-object/from16 v0, p1

    #@481
    iget-object v11, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@483
    check-cast v11, Landroid/os/AsyncResult;

    #@485
    .line 816
    .restart local v11       #ar:Landroid/os/AsyncResult;
    iget-object v4, v11, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@487
    if-nez v4, :cond_7b

    #@489
    .line 817
    iget-object v4, v11, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@48b
    check-cast v4, [I

    #@48d
    move-object v14, v4

    #@48e
    check-cast v14, [I

    #@490
    .line 818
    .restart local v14       #ints:[I
    const/4 v4, 0x0

    #@491
    aget v4, v14, v4

    #@493
    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@496
    move-result-object v4

    #@497
    move-object/from16 v0, p0

    #@499
    iput-object v4, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mLteEhrpdForced:Ljava/lang/String;

    #@49b
    .line 819
    const-string v4, "CDMA"

    #@49d
    new-instance v21, Ljava/lang/StringBuilder;

    #@49f
    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    #@4a2
    const-string v22, "[CDMAServiceStateTracker] mLteEhrpdForced = "

    #@4a4
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a7
    move-result-object v21

    #@4a8
    move-object/from16 v0, p0

    #@4aa
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mLteEhrpdForced:Ljava/lang/String;

    #@4ac
    move-object/from16 v22, v0

    #@4ae
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b1
    move-result-object v21

    #@4b2
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b5
    move-result-object v21

    #@4b6
    move-object/from16 v0, v21

    #@4b8
    invoke-static {v4, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4bb
    goto/16 :goto_7b

    #@4bd
    .line 827
    .end local v11           #ar:Landroid/os/AsyncResult;
    .end local v14           #ints:[I
    :sswitch_4bd
    const-string v4, "CDMA"

    #@4bf
    const-string v21, "[CdmaServiceStateTracker] case EVENT_CHANGE_IMS_STATE:"

    #@4c1
    move-object/from16 v0, v21

    #@4c3
    invoke-static {v4, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4c6
    .line 828
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->setPowerStateToDesired()V

    #@4c9
    goto/16 :goto_7b

    #@4cb
    .line 834
    :sswitch_4cb
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->handleApnChanged()V

    #@4ce
    goto/16 :goto_7b

    #@4d0
    .line 840
    :sswitch_4d0
    move-object/from16 v0, p1

    #@4d2
    iget-object v11, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@4d4
    check-cast v11, Landroid/os/AsyncResult;

    #@4d6
    .line 841
    .restart local v11       #ar:Landroid/os/AsyncResult;
    iget-object v4, v11, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@4d8
    if-nez v4, :cond_7b

    #@4da
    .line 842
    iget-object v4, v11, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@4dc
    check-cast v4, [I

    #@4de
    move-object v14, v4

    #@4df
    check-cast v14, [I

    #@4e1
    .line 843
    .restart local v14       #ints:[I
    const/4 v4, 0x0

    #@4e2
    aget v4, v14, v4

    #@4e4
    move-object/from16 v0, p0

    #@4e6
    iput v4, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mHDRRoamingIndicator:I

    #@4e8
    .line 844
    const-string v4, "CDMA"

    #@4ea
    new-instance v21, Ljava/lang/StringBuilder;

    #@4ec
    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    #@4ef
    const-string v22, "[CDMAServiceStateTracker] mHDRRoamingIndicator = "

    #@4f1
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f4
    move-result-object v21

    #@4f5
    move-object/from16 v0, p0

    #@4f7
    iget v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mHDRRoamingIndicator:I

    #@4f9
    move/from16 v22, v0

    #@4fb
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4fe
    move-result-object v21

    #@4ff
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@502
    move-result-object v21

    #@503
    move-object/from16 v0, v21

    #@505
    invoke-static {v4, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@508
    goto/16 :goto_7b

    #@50a
    .line 554
    :sswitch_data_50a
    .sparse-switch
        0x1 -> :sswitch_c4
        0x3 -> :sswitch_133
        0xa -> :sswitch_39d
        0xb -> :sswitch_3b2
        0xc -> :sswitch_3de
        0x12 -> :sswitch_3f6
        0x18 -> :sswitch_211
        0x19 -> :sswitch_211
        0x1a -> :sswitch_95
        0x1b -> :sswitch_3f1
        0x1e -> :sswitch_12e
        0x1f -> :sswitch_152
        0x22 -> :sswitch_222
        0x23 -> :sswitch_c0
        0x24 -> :sswitch_419
        0x25 -> :sswitch_425
        0x27 -> :sswitch_87
        0x28 -> :sswitch_461
        0x2c -> :sswitch_4bd
        0x34 -> :sswitch_4cb
        0x46 -> :sswitch_47f
        0x47 -> :sswitch_4d0
    .end sparse-switch
.end method

.method protected handlePollStateResult(ILandroid/os/AsyncResult;)V
    .registers 13
    .parameter "what"
    .parameter "ar"

    #@0
    .prologue
    .line 1161
    iget-object v7, p2, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@2
    iget-object v8, p0, Lcom/android/internal/telephony/ServiceStateTracker;->pollingContext:[I

    #@4
    if-eq v7, v8, :cond_7

    #@6
    .line 1334
    :cond_6
    :goto_6
    return-void

    #@7
    .line 1163
    :cond_7
    iget-object v7, p2, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@9
    if-eqz v7, :cond_206

    #@b
    .line 1164
    const/4 v0, 0x0

    #@c
    .line 1166
    .local v0, err:Lcom/android/internal/telephony/CommandException$Error;
    iget-object v7, p2, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@e
    instance-of v7, v7, Lcom/android/internal/telephony/CommandException;

    #@10
    if-eqz v7, :cond_1c

    #@12
    .line 1167
    iget-object v7, p2, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@14
    check-cast v7, Lcom/android/internal/telephony/CommandException;

    #@16
    check-cast v7, Lcom/android/internal/telephony/CommandException;

    #@18
    invoke-virtual {v7}, Lcom/android/internal/telephony/CommandException;->getCommandError()Lcom/android/internal/telephony/CommandException$Error;

    #@1b
    move-result-object v0

    #@1c
    .line 1170
    :cond_1c
    sget-object v7, Lcom/android/internal/telephony/CommandException$Error;->RADIO_NOT_AVAILABLE:Lcom/android/internal/telephony/CommandException$Error;

    #@1e
    if-ne v0, v7, :cond_24

    #@20
    .line 1172
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->cancelPollState()V

    #@23
    goto :goto_6

    #@24
    .line 1176
    :cond_24
    iget-object v7, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@26
    invoke-interface {v7}, Lcom/android/internal/telephony/CommandsInterface;->getRadioState()Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@29
    move-result-object v7

    #@2a
    invoke-virtual {v7}, Lcom/android/internal/telephony/CommandsInterface$RadioState;->isOn()Z

    #@2d
    move-result v7

    #@2e
    if-nez v7, :cond_34

    #@30
    .line 1178
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->cancelPollState()V

    #@33
    goto :goto_6

    #@34
    .line 1182
    :cond_34
    sget-object v7, Lcom/android/internal/telephony/CommandException$Error;->OP_NOT_ALLOWED_BEFORE_REG_NW:Lcom/android/internal/telephony/CommandException$Error;

    #@36
    if-eq v0, v7, :cond_50

    #@38
    .line 1183
    new-instance v7, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v8, "handlePollStateResult: RIL returned an error where it must succeed"

    #@3f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v7

    #@43
    iget-object v8, p2, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@45
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v7

    #@49
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c
    move-result-object v7

    #@4d
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->loge(Ljava/lang/String;)V

    #@50
    .line 1193
    .end local v0           #err:Lcom/android/internal/telephony/CommandException$Error;
    :cond_50
    :goto_50
    iget-object v7, p0, Lcom/android/internal/telephony/ServiceStateTracker;->pollingContext:[I

    #@52
    const/4 v8, 0x0

    #@53
    aget v9, v7, v8

    #@55
    add-int/lit8 v9, v9, -0x1

    #@57
    aput v9, v7, v8

    #@59
    .line 1195
    iget-object v7, p0, Lcom/android/internal/telephony/ServiceStateTracker;->pollingContext:[I

    #@5b
    const/4 v8, 0x0

    #@5c
    aget v7, v7, v8

    #@5e
    if-nez v7, :cond_6

    #@60
    .line 1196
    const/4 v4, 0x0

    #@61
    .line 1197
    .local v4, namMatch:Z
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isSidsAllZeros()Z

    #@64
    move-result v7

    #@65
    if-nez v7, :cond_74

    #@67
    iget-object v7, p0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@69
    invoke-virtual {v7}, Landroid/telephony/ServiceState;->getSystemId()I

    #@6c
    move-result v7

    #@6d
    invoke-direct {p0, v7}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isHomeSid(I)Z

    #@70
    move-result v7

    #@71
    if-eqz v7, :cond_74

    #@73
    .line 1198
    const/4 v4, 0x1

    #@74
    .line 1202
    :cond_74
    iget-boolean v7, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isSubscriptionFromRuim:Z

    #@76
    if-eqz v7, :cond_224

    #@78
    .line 1203
    iget-object v7, p0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@7a
    iget-boolean v8, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mCdmaRoaming:Z

    #@7c
    iget-object v9, p0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@7e
    invoke-direct {p0, v8, v9}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isRoamingBetweenOperators(ZLandroid/telephony/ServiceState;)Z

    #@81
    move-result v8

    #@82
    invoke-virtual {v7, v8}, Landroid/telephony/ServiceState;->setRoaming(Z)V

    #@85
    .line 1209
    :goto_85
    const/4 v7, 0x0

    #@86
    const-string v8, "vzw_roaming_state"

    #@88
    invoke-static {v7, v8}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@8b
    move-result v7

    #@8c
    if-eqz v7, :cond_ac

    #@8e
    .line 1210
    iget-boolean v7, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isSubscriptionFromRuim:Z

    #@90
    if-eqz v7, :cond_22d

    #@92
    .line 1211
    iget-object v7, p0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@94
    iget-boolean v8, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mVoiceRoaming:Z

    #@96
    iget-object v9, p0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@98
    invoke-direct {p0, v8, v9}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isRoamingBetweenOperators(ZLandroid/telephony/ServiceState;)Z

    #@9b
    move-result v8

    #@9c
    invoke-virtual {v7, v8}, Landroid/telephony/ServiceState;->setVoiceRoaming(Z)V

    #@9f
    .line 1212
    iget-object v7, p0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@a1
    iget-boolean v8, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mDataRoaming:Z

    #@a3
    iget-object v9, p0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@a5
    invoke-direct {p0, v8, v9}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isRoamingBetweenOperators(ZLandroid/telephony/ServiceState;)Z

    #@a8
    move-result v8

    #@a9
    invoke-virtual {v7, v8}, Landroid/telephony/ServiceState;->setDataRoaming(Z)V

    #@ac
    .line 1221
    :cond_ac
    :goto_ac
    const/4 v3, 0x0

    #@ad
    .line 1222
    .local v3, mIsRoamingIndicator_data:Z
    const/4 v7, 0x0

    #@ae
    const-string v8, "vzw_eri"

    #@b0
    invoke-static {v7, v8}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@b3
    move-result v7

    #@b4
    if-eqz v7, :cond_108

    #@b6
    .line 1223
    iget-object v7, p0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@b8
    invoke-virtual {v7}, Landroid/telephony/ServiceState;->getState()I

    #@bb
    move-result v7

    #@bc
    if-eqz v7, :cond_108

    #@be
    iget-object v7, p0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@c0
    invoke-virtual {v7}, Landroid/telephony/ServiceState;->getDataState()I

    #@c3
    move-result v7

    #@c4
    if-nez v7, :cond_108

    #@c6
    .line 1225
    iget v7, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mDefaultRoamingIndicator_data:I

    #@c8
    iput v7, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mDefaultRoamingIndicator:I

    #@ca
    .line 1226
    iget-boolean v7, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mIsInPrl_data:Z

    #@cc
    iput-boolean v7, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mIsInPrl:Z

    #@ce
    .line 1227
    iget v7, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mRoamingIndicator_data:I

    #@d0
    iput v7, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mRoamingIndicator:I

    #@d2
    .line 1228
    const/4 v3, 0x1

    #@d3
    .line 1230
    const-string v7, "[ERI] 1x no service and do only case !"

    #@d5
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@d8
    .line 1231
    new-instance v7, Ljava/lang/StringBuilder;

    #@da
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@dd
    const-string v8, "mDefaultRoamingIndicator = "

    #@df
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e2
    move-result-object v7

    #@e3
    iget v8, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mDefaultRoamingIndicator:I

    #@e5
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e8
    move-result-object v7

    #@e9
    const-string v8, ",  mIsInPrl = "

    #@eb
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ee
    move-result-object v7

    #@ef
    iget-boolean v8, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mIsInPrl:Z

    #@f1
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@f4
    move-result-object v7

    #@f5
    const-string v8, ",  mRoamingIndicator = "

    #@f7
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fa
    move-result-object v7

    #@fb
    iget v8, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mRoamingIndicator:I

    #@fd
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@100
    move-result-object v7

    #@101
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@104
    move-result-object v7

    #@105
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@108
    .line 1236
    :cond_108
    iget-object v7, p0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@10a
    iget v8, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mDefaultRoamingIndicator:I

    #@10c
    invoke-virtual {v7, v8}, Landroid/telephony/ServiceState;->setCdmaDefaultRoamingIndicator(I)V

    #@10f
    .line 1237
    iget-object v7, p0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@111
    iget v8, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mRoamingIndicator:I

    #@113
    invoke-virtual {v7, v8}, Landroid/telephony/ServiceState;->setCdmaRoamingIndicator(I)V

    #@116
    .line 1238
    const/4 v2, 0x1

    #@117
    .line 1239
    .local v2, isPrlLoaded:Z
    iget-object v7, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mPrlVersion:Ljava/lang/String;

    #@119
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@11c
    move-result v7

    #@11d
    if-eqz v7, :cond_120

    #@11f
    .line 1240
    const/4 v2, 0x0

    #@120
    .line 1242
    :cond_120
    if-nez v2, :cond_23d

    #@122
    .line 1243
    iget-object v7, p0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@124
    const/4 v8, 0x1

    #@125
    invoke-virtual {v7, v8}, Landroid/telephony/ServiceState;->setCdmaRoamingIndicator(I)V

    #@128
    .line 1278
    :cond_128
    :goto_128
    const/4 v7, 0x0

    #@129
    const-string v8, "vzw_eri"

    #@12b
    invoke-static {v7, v8}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@12e
    move-result v7

    #@12f
    if-eqz v7, :cond_155

    #@131
    .line 1279
    const/4 v7, 0x1

    #@132
    if-ne v3, v7, :cond_155

    #@134
    .line 1280
    iget v7, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mRoamingIndicator:I

    #@136
    const/16 v8, 0x63

    #@138
    if-ne v7, v8, :cond_2c5

    #@13a
    .line 1281
    iget-object v7, p0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@13c
    iget v8, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mRoamingIndicator:I

    #@13e
    invoke-virtual {v7, v8}, Landroid/telephony/ServiceState;->setCdmaRoamingIndicator(I)V

    #@141
    .line 1288
    :goto_141
    iget v7, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mNetworkType_data:I

    #@143
    const/16 v8, 0xe

    #@145
    if-ne v7, v8, :cond_155

    #@147
    .line 1289
    iget-object v7, p0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@149
    invoke-virtual {v7}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@14c
    move-result v7

    #@14d
    if-eqz v7, :cond_2db

    #@14f
    .line 1290
    iget-object v7, p0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@151
    const/4 v8, 0x0

    #@152
    invoke-virtual {v7, v8}, Landroid/telephony/ServiceState;->setCdmaRoamingIndicator(I)V

    #@155
    .line 1298
    :cond_155
    :goto_155
    iget-object v7, p0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@157
    invoke-virtual {v7}, Landroid/telephony/ServiceState;->getCdmaRoamingIndicator()I

    #@15a
    move-result v5

    #@15b
    .line 1299
    .local v5, roamingIndicator:I
    iget-object v7, p0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@15d
    iget-object v8, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@15f
    iget-object v8, v8, Lcom/android/internal/telephony/cdma/CDMAPhone;->mEriManager:Lcom/android/internal/telephony/cdma/EriManager;

    #@161
    iget v9, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mDefaultRoamingIndicator:I

    #@163
    invoke-virtual {v8, v5, v9}, Lcom/android/internal/telephony/cdma/EriManager;->getCdmaEriIconIndex(II)I

    #@166
    move-result v8

    #@167
    invoke-virtual {v7, v8}, Landroid/telephony/ServiceState;->setCdmaEriIconIndex(I)V

    #@16a
    .line 1301
    iget-object v7, p0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@16c
    iget-object v8, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@16e
    iget-object v8, v8, Lcom/android/internal/telephony/cdma/CDMAPhone;->mEriManager:Lcom/android/internal/telephony/cdma/EriManager;

    #@170
    iget v9, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mDefaultRoamingIndicator:I

    #@172
    invoke-virtual {v8, v5, v9}, Lcom/android/internal/telephony/cdma/EriManager;->getCdmaEriIconMode(II)I

    #@175
    move-result v8

    #@176
    invoke-virtual {v7, v8}, Landroid/telephony/ServiceState;->setCdmaEriIconMode(I)V

    #@179
    .line 1305
    const/4 v7, 0x0

    #@17a
    const-string v8, "KR_RAD_TEST"

    #@17c
    invoke-static {v7, v8}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@17f
    move-result v7

    #@180
    if-eqz v7, :cond_18e

    #@182
    .line 1306
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isOemRadTestSettingTrue()Z

    #@185
    move-result v7

    #@186
    if-eqz v7, :cond_18e

    #@188
    .line 1309
    iget-object v7, p0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@18a
    const/4 v8, 0x1

    #@18b
    invoke-virtual {v7, v8}, Landroid/telephony/ServiceState;->setRoaming(Z)V

    #@18e
    .line 1315
    :cond_18e
    invoke-static {}, Lcom/lge/wifi/WifiLgeConfig;->isWifiChameleonFeaturedCarrier()Z

    #@191
    move-result v7

    #@192
    if-eqz v7, :cond_1a1

    #@194
    .line 1316
    iget-object v7, p0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@196
    invoke-virtual {v7}, Landroid/telephony/ServiceState;->getRoamingType()I

    #@199
    move-result v6

    #@19a
    .line 1317
    .local v6, roamingtype:I
    iget-object v7, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->cr:Landroid/content/ContentResolver;

    #@19c
    const-string v8, "wifi_chameleon_roaming_type"

    #@19e
    invoke-static {v7, v8, v6}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@1a1
    .line 1325
    .end local v6           #roamingtype:I
    :cond_1a1
    new-instance v7, Ljava/lang/StringBuilder;

    #@1a3
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@1a6
    const-string v8, "Set CDMA Roaming Indicator to: "

    #@1a8
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ab
    move-result-object v7

    #@1ac
    iget-object v8, p0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@1ae
    invoke-virtual {v8}, Landroid/telephony/ServiceState;->getCdmaRoamingIndicator()I

    #@1b1
    move-result v8

    #@1b2
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b5
    move-result-object v7

    #@1b6
    const-string v8, ". mCdmaRoaming = "

    #@1b8
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1bb
    move-result-object v7

    #@1bc
    iget-boolean v8, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mCdmaRoaming:Z

    #@1be
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1c1
    move-result-object v7

    #@1c2
    const-string v8, ", isPrlLoaded = "

    #@1c4
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c7
    move-result-object v7

    #@1c8
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1cb
    move-result-object v7

    #@1cc
    const-string v8, ". namMatch = "

    #@1ce
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d1
    move-result-object v7

    #@1d2
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1d5
    move-result-object v7

    #@1d6
    const-string v8, " , mIsInPrl = "

    #@1d8
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1db
    move-result-object v7

    #@1dc
    iget-boolean v8, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mIsInPrl:Z

    #@1de
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1e1
    move-result-object v7

    #@1e2
    const-string v8, ", mRoamingIndicator = "

    #@1e4
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e7
    move-result-object v7

    #@1e8
    iget v8, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mRoamingIndicator:I

    #@1ea
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1ed
    move-result-object v7

    #@1ee
    const-string v8, ", mDefaultRoamingIndicator= "

    #@1f0
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f3
    move-result-object v7

    #@1f4
    iget v8, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mDefaultRoamingIndicator:I

    #@1f6
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f9
    move-result-object v7

    #@1fa
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1fd
    move-result-object v7

    #@1fe
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@201
    .line 1331
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->pollStateDone()V

    #@204
    goto/16 :goto_6

    #@206
    .line 1187
    .end local v2           #isPrlLoaded:Z
    .end local v3           #mIsRoamingIndicator_data:Z
    .end local v4           #namMatch:Z
    .end local v5           #roamingIndicator:I
    :cond_206
    :try_start_206
    invoke-virtual {p0, p1, p2}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->handlePollStateResultMessage(ILandroid/os/AsyncResult;)V
    :try_end_209
    .catch Ljava/lang/RuntimeException; {:try_start_206 .. :try_end_209} :catch_20b

    #@209
    goto/16 :goto_50

    #@20b
    .line 1188
    :catch_20b
    move-exception v1

    #@20c
    .line 1189
    .local v1, ex:Ljava/lang/RuntimeException;
    new-instance v7, Ljava/lang/StringBuilder;

    #@20e
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@211
    const-string v8, "handlePollStateResult: Exception while polling service state. Probably malformed RIL response."

    #@213
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@216
    move-result-object v7

    #@217
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@21a
    move-result-object v7

    #@21b
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21e
    move-result-object v7

    #@21f
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->loge(Ljava/lang/String;)V

    #@222
    goto/16 :goto_50

    #@224
    .line 1205
    .end local v1           #ex:Ljava/lang/RuntimeException;
    .restart local v4       #namMatch:Z
    :cond_224
    iget-object v7, p0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@226
    iget-boolean v8, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mCdmaRoaming:Z

    #@228
    invoke-virtual {v7, v8}, Landroid/telephony/ServiceState;->setRoaming(Z)V

    #@22b
    goto/16 :goto_85

    #@22d
    .line 1214
    :cond_22d
    iget-object v7, p0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@22f
    iget-boolean v8, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mVoiceRoaming:Z

    #@231
    invoke-virtual {v7, v8}, Landroid/telephony/ServiceState;->setVoiceRoaming(Z)V

    #@234
    .line 1215
    iget-object v7, p0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@236
    iget-boolean v8, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mDataRoaming:Z

    #@238
    invoke-virtual {v7, v8}, Landroid/telephony/ServiceState;->setDataRoaming(Z)V

    #@23b
    goto/16 :goto_ac

    #@23d
    .line 1245
    .restart local v2       #isPrlLoaded:Z
    .restart local v3       #mIsRoamingIndicator_data:Z
    :cond_23d
    const-string v7, "KDDI"

    #@23f
    invoke-static {v7}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@242
    move-result v7

    #@243
    if-eqz v7, :cond_251

    #@245
    iget-boolean v7, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mCdmaRoaming:Z

    #@247
    if-nez v7, :cond_251

    #@249
    .line 1246
    iget-object v7, p0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@24b
    const/4 v8, 0x1

    #@24c
    invoke-virtual {v7, v8}, Landroid/telephony/ServiceState;->setCdmaRoamingIndicator(I)V

    #@24f
    goto/16 :goto_128

    #@251
    .line 1248
    :cond_251
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isSidsAllZeros()Z

    #@254
    move-result v7

    #@255
    if-nez v7, :cond_128

    #@257
    .line 1249
    if-nez v4, :cond_296

    #@259
    iget-boolean v7, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mIsInPrl:Z

    #@25b
    if-nez v7, :cond_296

    #@25d
    .line 1251
    iget-object v7, p0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@25f
    iget v8, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mDefaultRoamingIndicator:I

    #@261
    invoke-virtual {v7, v8}, Landroid/telephony/ServiceState;->setCdmaRoamingIndicator(I)V

    #@264
    .line 1268
    :goto_264
    iget v7, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mRoamingIndicator:I

    #@266
    const/16 v8, 0x63

    #@268
    if-ne v7, v8, :cond_128

    #@26a
    .line 1269
    const/4 v7, 0x0

    #@26b
    const-string v8, "vzw_eri"

    #@26d
    invoke-static {v7, v8}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@270
    move-result v7

    #@271
    if-eqz v7, :cond_128

    #@273
    .line 1270
    iget-object v7, p0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@275
    iget v8, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mRoamingIndicator:I

    #@277
    invoke-virtual {v7, v8}, Landroid/telephony/ServiceState;->setCdmaRoamingIndicator(I)V

    #@27a
    .line 1271
    const-string v7, "CDMA"

    #@27c
    new-instance v8, Ljava/lang/StringBuilder;

    #@27e
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@281
    const-string v9, "set femto ON, ignore other ERI mapping"

    #@283
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@286
    move-result-object v8

    #@287
    iget v9, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mRoamingIndicator:I

    #@289
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28c
    move-result-object v8

    #@28d
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@290
    move-result-object v8

    #@291
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@294
    goto/16 :goto_128

    #@296
    .line 1252
    :cond_296
    if-eqz v4, :cond_2a3

    #@298
    iget-boolean v7, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mIsInPrl:Z

    #@29a
    if-nez v7, :cond_2a3

    #@29c
    .line 1253
    iget-object v7, p0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@29e
    const/4 v8, 0x2

    #@29f
    invoke-virtual {v7, v8}, Landroid/telephony/ServiceState;->setCdmaRoamingIndicator(I)V

    #@2a2
    goto :goto_264

    #@2a3
    .line 1254
    :cond_2a3
    if-nez v4, :cond_2b1

    #@2a5
    iget-boolean v7, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mIsInPrl:Z

    #@2a7
    if-eqz v7, :cond_2b1

    #@2a9
    .line 1256
    iget-object v7, p0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@2ab
    iget v8, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mRoamingIndicator:I

    #@2ad
    invoke-virtual {v7, v8}, Landroid/telephony/ServiceState;->setCdmaRoamingIndicator(I)V

    #@2b0
    goto :goto_264

    #@2b1
    .line 1259
    :cond_2b1
    iget v7, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mRoamingIndicator:I

    #@2b3
    const/4 v8, 0x2

    #@2b4
    if-gt v7, v8, :cond_2bd

    #@2b6
    .line 1260
    iget-object v7, p0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@2b8
    const/4 v8, 0x1

    #@2b9
    invoke-virtual {v7, v8}, Landroid/telephony/ServiceState;->setCdmaRoamingIndicator(I)V

    #@2bc
    goto :goto_264

    #@2bd
    .line 1263
    :cond_2bd
    iget-object v7, p0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@2bf
    iget v8, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mRoamingIndicator:I

    #@2c1
    invoke-virtual {v7, v8}, Landroid/telephony/ServiceState;->setCdmaRoamingIndicator(I)V

    #@2c4
    goto :goto_264

    #@2c5
    .line 1282
    :cond_2c5
    iget-boolean v7, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mIsInPrl:Z

    #@2c7
    if-nez v7, :cond_2d2

    #@2c9
    .line 1283
    iget-object v7, p0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@2cb
    iget v8, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mDefaultRoamingIndicator:I

    #@2cd
    invoke-virtual {v7, v8}, Landroid/telephony/ServiceState;->setCdmaRoamingIndicator(I)V

    #@2d0
    goto/16 :goto_141

    #@2d2
    .line 1285
    :cond_2d2
    iget-object v7, p0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@2d4
    iget v8, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mRoamingIndicator:I

    #@2d6
    invoke-virtual {v7, v8}, Landroid/telephony/ServiceState;->setCdmaRoamingIndicator(I)V

    #@2d9
    goto/16 :goto_141

    #@2db
    .line 1292
    :cond_2db
    iget-object v7, p0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@2dd
    const/4 v8, 0x1

    #@2de
    invoke-virtual {v7, v8}, Landroid/telephony/ServiceState;->setCdmaRoamingIndicator(I)V

    #@2e1
    goto/16 :goto_155
.end method

.method protected handlePollStateResultMessage(ILandroid/os/AsyncResult;)V
    .registers 25
    .parameter "what"
    .parameter "ar"

    #@0
    .prologue
    .line 986
    packed-switch p1, :pswitch_data_2f4

    #@3
    .line 1149
    const-string v3, "handlePollStateResultMessage: RIL response handle in wrong phone! Expected CDMA RIL request and get GSM RIL request."

    #@5
    move-object/from16 v0, p0

    #@7
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->loge(Ljava/lang/String;)V

    #@a
    .line 1153
    :cond_a
    :goto_a
    return-void

    #@b
    .line 988
    :pswitch_b
    move-object/from16 v0, p2

    #@d
    iget-object v3, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@f
    check-cast v3, [Ljava/lang/String;

    #@11
    move-object/from16 v17, v3

    #@13
    check-cast v17, [Ljava/lang/String;

    #@15
    .line 990
    .local v17, states:[Ljava/lang/String;
    const/4 v15, 0x4

    #@16
    .line 991
    .local v15, registrationState:I
    const/4 v13, -0x1

    #@17
    .line 992
    .local v13, radioTechnology:I
    const/4 v4, -0x1

    #@18
    .line 994
    .local v4, baseStationId:I
    const v5, 0x7fffffff

    #@1b
    .line 996
    .local v5, baseStationLatitude:I
    const v6, 0x7fffffff

    #@1e
    .line 997
    .local v6, baseStationLongitude:I
    const/4 v9, 0x0

    #@1f
    .line 998
    .local v9, cssIndicator:I
    const/4 v7, 0x0

    #@20
    .line 999
    .local v7, systemId:I
    const/4 v8, 0x0

    #@21
    .line 1000
    .local v8, networkId:I
    const/16 v16, 0x1

    #@23
    .line 1001
    .local v16, roamingIndicator:I
    const/16 v18, 0x0

    #@25
    .line 1002
    .local v18, systemIsInPrl:I
    const/4 v10, 0x1

    #@26
    .line 1003
    .local v10, defaultRoamingIndicator:I
    const/4 v14, 0x0

    #@27
    .line 1005
    .local v14, reasonForDenial:I
    move-object/from16 v0, v17

    #@29
    array-length v3, v0

    #@2a
    const/16 v19, 0xe

    #@2c
    move/from16 v0, v19

    #@2e
    if-lt v3, v0, :cond_201

    #@30
    .line 1007
    const/4 v3, 0x0

    #@31
    :try_start_31
    aget-object v3, v17, v3

    #@33
    if-eqz v3, :cond_3c

    #@35
    .line 1008
    const/4 v3, 0x0

    #@36
    aget-object v3, v17, v3

    #@38
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@3b
    move-result v15

    #@3c
    .line 1010
    :cond_3c
    const/4 v3, 0x3

    #@3d
    aget-object v3, v17, v3

    #@3f
    if-eqz v3, :cond_48

    #@41
    .line 1011
    const/4 v3, 0x3

    #@42
    aget-object v3, v17, v3

    #@44
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@47
    move-result v13

    #@48
    .line 1013
    :cond_48
    const/4 v3, 0x4

    #@49
    aget-object v3, v17, v3

    #@4b
    if-eqz v3, :cond_54

    #@4d
    .line 1014
    const/4 v3, 0x4

    #@4e
    aget-object v3, v17, v3

    #@50
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@53
    move-result v4

    #@54
    .line 1016
    :cond_54
    const/4 v3, 0x5

    #@55
    aget-object v3, v17, v3

    #@57
    if-eqz v3, :cond_60

    #@59
    .line 1017
    const/4 v3, 0x5

    #@5a
    aget-object v3, v17, v3

    #@5c
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@5f
    move-result v5

    #@60
    .line 1019
    :cond_60
    const/4 v3, 0x6

    #@61
    aget-object v3, v17, v3

    #@63
    if-eqz v3, :cond_6c

    #@65
    .line 1020
    const/4 v3, 0x6

    #@66
    aget-object v3, v17, v3

    #@68
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@6b
    move-result v6

    #@6c
    .line 1023
    :cond_6c
    if-nez v5, :cond_76

    #@6e
    if-nez v6, :cond_76

    #@70
    .line 1024
    const v5, 0x7fffffff

    #@73
    .line 1025
    const v6, 0x7fffffff

    #@76
    .line 1027
    :cond_76
    const/4 v3, 0x7

    #@77
    aget-object v3, v17, v3

    #@79
    if-eqz v3, :cond_82

    #@7b
    .line 1028
    const/4 v3, 0x7

    #@7c
    aget-object v3, v17, v3

    #@7e
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@81
    move-result v9

    #@82
    .line 1030
    :cond_82
    const/16 v3, 0x8

    #@84
    aget-object v3, v17, v3

    #@86
    if-eqz v3, :cond_90

    #@88
    .line 1031
    const/16 v3, 0x8

    #@8a
    aget-object v3, v17, v3

    #@8c
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@8f
    move-result v7

    #@90
    .line 1033
    :cond_90
    const/16 v3, 0x9

    #@92
    aget-object v3, v17, v3

    #@94
    if-eqz v3, :cond_9e

    #@96
    .line 1034
    const/16 v3, 0x9

    #@98
    aget-object v3, v17, v3

    #@9a
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@9d
    move-result v8

    #@9e
    .line 1036
    :cond_9e
    const/16 v3, 0xa

    #@a0
    aget-object v3, v17, v3

    #@a2
    if-eqz v3, :cond_ca

    #@a4
    .line 1037
    const/16 v3, 0xa

    #@a6
    aget-object v3, v17, v3

    #@a8
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@ab
    move-result v16

    #@ac
    .line 1039
    const-string v3, "CDMA"

    #@ae
    new-instance v19, Ljava/lang/StringBuilder;

    #@b0
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@b3
    const-string v20, "[CDMAServiceStateTracker] roamingIndicator(1X) = "

    #@b5
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b8
    move-result-object v19

    #@b9
    move-object/from16 v0, v19

    #@bb
    move/from16 v1, v16

    #@bd
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v19

    #@c1
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c4
    move-result-object v19

    #@c5
    move-object/from16 v0, v19

    #@c7
    invoke-static {v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ca
    .line 1042
    :cond_ca
    const/16 v3, 0xb

    #@cc
    aget-object v3, v17, v3

    #@ce
    if-eqz v3, :cond_d8

    #@d0
    .line 1043
    const/16 v3, 0xb

    #@d2
    aget-object v3, v17, v3

    #@d4
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@d7
    move-result v18

    #@d8
    .line 1045
    :cond_d8
    const/16 v3, 0xc

    #@da
    aget-object v3, v17, v3

    #@dc
    if-eqz v3, :cond_e6

    #@de
    .line 1046
    const/16 v3, 0xc

    #@e0
    aget-object v3, v17, v3

    #@e2
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@e5
    move-result v10

    #@e6
    .line 1048
    :cond_e6
    const/16 v3, 0xd

    #@e8
    aget-object v3, v17, v3

    #@ea
    if-eqz v3, :cond_f4

    #@ec
    .line 1049
    const/16 v3, 0xd

    #@ee
    aget-object v3, v17, v3

    #@f0
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_f3
    .catch Ljava/lang/NumberFormatException; {:try_start_31 .. :try_end_f3} :catch_1e4

    #@f3
    move-result v14

    #@f4
    .line 1060
    :cond_f4
    :goto_f4
    move-object/from16 v0, p0

    #@f6
    iput v15, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mRegistrationState:I

    #@f8
    .line 1064
    move-object/from16 v0, p0

    #@fa
    invoke-virtual {v0, v15}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->regCodeIsRoaming(I)Z

    #@fd
    move-result v3

    #@fe
    if-eqz v3, :cond_227

    #@100
    const/16 v3, 0xa

    #@102
    aget-object v3, v17, v3

    #@104
    move-object/from16 v0, p0

    #@106
    invoke-direct {v0, v3}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isRoamIndForHomeSystem(Ljava/lang/String;)Z

    #@109
    move-result v3

    #@10a
    if-nez v3, :cond_227

    #@10c
    const/4 v3, 0x1

    #@10d
    :goto_10d
    move-object/from16 v0, p0

    #@10f
    iput-boolean v3, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mCdmaRoaming:Z

    #@111
    .line 1067
    const/4 v3, 0x0

    #@112
    const-string v19, "vzw_roaming_state"

    #@114
    move-object/from16 v0, v19

    #@116
    invoke-static {v3, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@119
    move-result v3

    #@11a
    if-eqz v3, :cond_124

    #@11c
    .line 1068
    move-object/from16 v0, p0

    #@11e
    iget-boolean v3, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mCdmaRoaming:Z

    #@120
    move-object/from16 v0, p0

    #@122
    iput-boolean v3, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mVoiceRoaming:Z

    #@124
    .line 1071
    :cond_124
    move-object/from16 v0, p0

    #@126
    iget-boolean v3, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mCdmaRoaming:Z

    #@128
    if-nez v3, :cond_130

    #@12a
    move-object/from16 v0, p0

    #@12c
    iget-boolean v3, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mDataRoaming:Z

    #@12e
    if-eqz v3, :cond_22a

    #@130
    :cond_130
    const/4 v3, 0x1

    #@131
    :goto_131
    move-object/from16 v0, p0

    #@133
    iput-boolean v3, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mCdmaRoaming:Z

    #@135
    .line 1072
    move-object/from16 v0, p0

    #@137
    iget-object v3, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@139
    move-object/from16 v0, p0

    #@13b
    invoke-virtual {v0, v15}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->regCodeToServiceState(I)I

    #@13e
    move-result v19

    #@13f
    move/from16 v0, v19

    #@141
    invoke-virtual {v3, v0}, Landroid/telephony/ServiceState;->setState(I)V

    #@144
    .line 1074
    move-object/from16 v0, p0

    #@146
    invoke-virtual {v0, v13}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->setCdmaTechnology(I)V

    #@149
    .line 1076
    move-object/from16 v0, p0

    #@14b
    iget-object v3, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@14d
    invoke-virtual {v3, v9}, Landroid/telephony/ServiceState;->setCssIndicator(I)V

    #@150
    .line 1077
    move-object/from16 v0, p0

    #@152
    iget-object v3, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@154
    invoke-virtual {v3, v7, v8}, Landroid/telephony/ServiceState;->setSystemAndNetworkId(II)V

    #@157
    .line 1078
    move/from16 v0, v16

    #@159
    move-object/from16 v1, p0

    #@15b
    iput v0, v1, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mRoamingIndicator:I

    #@15d
    .line 1079
    if-nez v18, :cond_22d

    #@15f
    const/4 v3, 0x0

    #@160
    :goto_160
    move-object/from16 v0, p0

    #@162
    iput-boolean v3, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mIsInPrl:Z

    #@164
    .line 1080
    move-object/from16 v0, p0

    #@166
    iput v10, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mDefaultRoamingIndicator:I

    #@168
    .line 1083
    const-string v3, "KDDI"

    #@16a
    invoke-static {v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@16d
    move-result v3

    #@16e
    if-eqz v3, :cond_185

    #@170
    .line 1085
    const-string v3, "ril.cdma.sid"

    #@172
    move-object/from16 v0, p0

    #@174
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@176
    move-object/from16 v19, v0

    #@178
    invoke-virtual/range {v19 .. v19}, Landroid/telephony/ServiceState;->getSystemId()I

    #@17b
    move-result v19

    #@17c
    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@17f
    move-result-object v19

    #@180
    move-object/from16 v0, v19

    #@182
    invoke-static {v3, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@185
    .line 1092
    :cond_185
    const/4 v3, 0x0

    #@186
    const-string v19, "sprint_location_spec"

    #@188
    move-object/from16 v0, v19

    #@18a
    invoke-static {v3, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@18d
    move-result v3

    #@18e
    if-eqz v3, :cond_1a7

    #@190
    .line 1093
    move-object/from16 v0, p0

    #@192
    iget-object v3, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->cr:Landroid/content/ContentResolver;

    #@194
    const-string v19, "network"

    #@196
    move-object/from16 v0, v19

    #@198
    invoke-static {v3, v0}, Landroid/provider/Settings$Secure;->isLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    #@19b
    move-result v3

    #@19c
    if-nez v3, :cond_1a7

    #@19e
    .line 1094
    const/4 v4, -0x1

    #@19f
    .line 1095
    const v5, 0x7fffffff

    #@1a2
    .line 1096
    const v6, 0x7fffffff

    #@1a5
    .line 1097
    const/4 v7, -0x1

    #@1a6
    .line 1098
    const/4 v8, -0x1

    #@1a7
    .line 1104
    :cond_1a7
    move-object/from16 v0, p0

    #@1a9
    iget-object v3, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->newCellLoc:Landroid/telephony/cdma/CdmaCellLocation;

    #@1ab
    invoke-virtual/range {v3 .. v8}, Landroid/telephony/cdma/CdmaCellLocation;->setCellLocationData(IIIII)V

    #@1ae
    .line 1107
    if-nez v14, :cond_230

    #@1b0
    .line 1108
    const-string v3, "General"

    #@1b2
    move-object/from16 v0, p0

    #@1b4
    iput-object v3, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mRegistrationDeniedReason:Ljava/lang/String;

    #@1b6
    .line 1115
    :goto_1b6
    move-object/from16 v0, p0

    #@1b8
    iget v3, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mRegistrationState:I

    #@1ba
    const/16 v19, 0x3

    #@1bc
    move/from16 v0, v19

    #@1be
    if-ne v3, v0, :cond_a

    #@1c0
    .line 1116
    new-instance v3, Ljava/lang/StringBuilder;

    #@1c2
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1c5
    const-string v19, "Registration denied, "

    #@1c7
    move-object/from16 v0, v19

    #@1c9
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1cc
    move-result-object v3

    #@1cd
    move-object/from16 v0, p0

    #@1cf
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mRegistrationDeniedReason:Ljava/lang/String;

    #@1d1
    move-object/from16 v19, v0

    #@1d3
    move-object/from16 v0, v19

    #@1d5
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d8
    move-result-object v3

    #@1d9
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1dc
    move-result-object v3

    #@1dd
    move-object/from16 v0, p0

    #@1df
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@1e2
    goto/16 :goto_a

    #@1e4
    .line 1051
    :catch_1e4
    move-exception v11

    #@1e5
    .line 1052
    .local v11, ex:Ljava/lang/NumberFormatException;
    new-instance v3, Ljava/lang/StringBuilder;

    #@1e7
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1ea
    const-string v19, "EVENT_POLL_STATE_REGISTRATION_CDMA: error parsing: "

    #@1ec
    move-object/from16 v0, v19

    #@1ee
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f1
    move-result-object v3

    #@1f2
    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1f5
    move-result-object v3

    #@1f6
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f9
    move-result-object v3

    #@1fa
    move-object/from16 v0, p0

    #@1fc
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->loge(Ljava/lang/String;)V

    #@1ff
    goto/16 :goto_f4

    #@201
    .line 1055
    .end local v11           #ex:Ljava/lang/NumberFormatException;
    :cond_201
    new-instance v3, Ljava/lang/RuntimeException;

    #@203
    new-instance v19, Ljava/lang/StringBuilder;

    #@205
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@208
    const-string v20, "Warning! Wrong number of parameters returned from RIL_REQUEST_REGISTRATION_STATE: expected 14 or more strings and got "

    #@20a
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20d
    move-result-object v19

    #@20e
    move-object/from16 v0, v17

    #@210
    array-length v0, v0

    #@211
    move/from16 v20, v0

    #@213
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@216
    move-result-object v19

    #@217
    const-string v20, " strings"

    #@219
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21c
    move-result-object v19

    #@21d
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@220
    move-result-object v19

    #@221
    move-object/from16 v0, v19

    #@223
    invoke-direct {v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@226
    throw v3

    #@227
    .line 1064
    :cond_227
    const/4 v3, 0x0

    #@228
    goto/16 :goto_10d

    #@22a
    .line 1071
    :cond_22a
    const/4 v3, 0x0

    #@22b
    goto/16 :goto_131

    #@22d
    .line 1079
    :cond_22d
    const/4 v3, 0x1

    #@22e
    goto/16 :goto_160

    #@230
    .line 1109
    :cond_230
    const/4 v3, 0x1

    #@231
    if-ne v14, v3, :cond_23b

    #@233
    .line 1110
    const-string v3, "Authentication Failure"

    #@235
    move-object/from16 v0, p0

    #@237
    iput-object v3, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mRegistrationDeniedReason:Ljava/lang/String;

    #@239
    goto/16 :goto_1b6

    #@23b
    .line 1112
    :cond_23b
    const-string v3, ""

    #@23d
    move-object/from16 v0, p0

    #@23f
    iput-object v3, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mRegistrationDeniedReason:Ljava/lang/String;

    #@241
    goto/16 :goto_1b6

    #@243
    .line 1121
    .end local v4           #baseStationId:I
    .end local v5           #baseStationLatitude:I
    .end local v6           #baseStationLongitude:I
    .end local v7           #systemId:I
    .end local v8           #networkId:I
    .end local v9           #cssIndicator:I
    .end local v10           #defaultRoamingIndicator:I
    .end local v13           #radioTechnology:I
    .end local v14           #reasonForDenial:I
    .end local v15           #registrationState:I
    .end local v16           #roamingIndicator:I
    .end local v17           #states:[Ljava/lang/String;
    .end local v18           #systemIsInPrl:I
    :pswitch_243
    move-object/from16 v0, p2

    #@245
    iget-object v3, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@247
    check-cast v3, [Ljava/lang/String;

    #@249
    move-object v12, v3

    #@24a
    check-cast v12, [Ljava/lang/String;

    #@24c
    .line 1123
    .local v12, opNames:[Ljava/lang/String;
    if-eqz v12, :cond_2ea

    #@24e
    array-length v3, v12

    #@24f
    const/16 v19, 0x3

    #@251
    move/from16 v0, v19

    #@253
    if-lt v3, v0, :cond_2ea

    #@255
    .line 1125
    const/4 v3, 0x2

    #@256
    aget-object v3, v12, v3

    #@258
    if-eqz v3, :cond_275

    #@25a
    const/4 v3, 0x2

    #@25b
    aget-object v3, v12, v3

    #@25d
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@260
    move-result v3

    #@261
    const/16 v19, 0x5

    #@263
    move/from16 v0, v19

    #@265
    if-lt v3, v0, :cond_275

    #@267
    const-string v3, "00000"

    #@269
    const/16 v19, 0x2

    #@26b
    aget-object v19, v12, v19

    #@26d
    move-object/from16 v0, v19

    #@26f
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@272
    move-result v3

    #@273
    if-eqz v3, :cond_2b0

    #@275
    .line 1127
    :cond_275
    const/4 v3, 0x2

    #@276
    sget-object v19, Lcom/android/internal/telephony/cdma/CDMAPhone;->PROPERTY_CDMA_HOME_OPERATOR_NUMERIC:Ljava/lang/String;

    #@278
    const-string v20, "00000"

    #@27a
    invoke-static/range {v19 .. v20}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@27d
    move-result-object v19

    #@27e
    aput-object v19, v12, v3

    #@280
    .line 1130
    new-instance v3, Ljava/lang/StringBuilder;

    #@282
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@285
    const-string v19, "RIL_REQUEST_OPERATOR.response[2], the numeric,  is bad. Using SystemProperties \'"

    #@287
    move-object/from16 v0, v19

    #@289
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28c
    move-result-object v3

    #@28d
    sget-object v19, Lcom/android/internal/telephony/cdma/CDMAPhone;->PROPERTY_CDMA_HOME_OPERATOR_NUMERIC:Ljava/lang/String;

    #@28f
    move-object/from16 v0, v19

    #@291
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@294
    move-result-object v3

    #@295
    const-string v19, "\'= "

    #@297
    move-object/from16 v0, v19

    #@299
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29c
    move-result-object v3

    #@29d
    const/16 v19, 0x2

    #@29f
    aget-object v19, v12, v19

    #@2a1
    move-object/from16 v0, v19

    #@2a3
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a6
    move-result-object v3

    #@2a7
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2aa
    move-result-object v3

    #@2ab
    move-object/from16 v0, p0

    #@2ad
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@2b0
    .line 1137
    :cond_2b0
    move-object/from16 v0, p0

    #@2b2
    iget-boolean v3, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isSubscriptionFromRuim:Z

    #@2b4
    if-nez v3, :cond_2cf

    #@2b6
    .line 1140
    move-object/from16 v0, p0

    #@2b8
    iget-object v3, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@2ba
    const/16 v19, 0x0

    #@2bc
    const/16 v20, 0x1

    #@2be
    aget-object v20, v12, v20

    #@2c0
    const/16 v21, 0x2

    #@2c2
    aget-object v21, v12, v21

    #@2c4
    move-object/from16 v0, v19

    #@2c6
    move-object/from16 v1, v20

    #@2c8
    move-object/from16 v2, v21

    #@2ca
    invoke-virtual {v3, v0, v1, v2}, Landroid/telephony/ServiceState;->setOperatorName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@2cd
    goto/16 :goto_a

    #@2cf
    .line 1142
    :cond_2cf
    move-object/from16 v0, p0

    #@2d1
    iget-object v3, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@2d3
    const/16 v19, 0x0

    #@2d5
    aget-object v19, v12, v19

    #@2d7
    const/16 v20, 0x1

    #@2d9
    aget-object v20, v12, v20

    #@2db
    const/16 v21, 0x2

    #@2dd
    aget-object v21, v12, v21

    #@2df
    move-object/from16 v0, v19

    #@2e1
    move-object/from16 v1, v20

    #@2e3
    move-object/from16 v2, v21

    #@2e5
    invoke-virtual {v3, v0, v1, v2}, Landroid/telephony/ServiceState;->setOperatorName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@2e8
    goto/16 :goto_a

    #@2ea
    .line 1145
    :cond_2ea
    const-string v3, "EVENT_POLL_STATE_OPERATOR_CDMA: error parsing opNames"

    #@2ec
    move-object/from16 v0, p0

    #@2ee
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@2f1
    goto/16 :goto_a

    #@2f3
    .line 986
    nop

    #@2f4
    :pswitch_data_2f4
    .packed-switch 0x18
        :pswitch_b
        :pswitch_243
    .end packed-switch
.end method

.method protected hangupAndPowerOff()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 2291
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@4
    iget-boolean v0, v0, Lcom/android/internal/telephony/PhoneBase;->mIsTheCurrentActivePhone:Z

    #@6
    if-nez v0, :cond_13

    #@8
    .line 2292
    const-string v0, "hangupAndPowerOff : hangup is Ignoring."

    #@a
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@d
    .line 2293
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@f
    invoke-interface {v0, v1, v2}, Lcom/android/internal/telephony/CommandsInterface;->setRadioPower(ZLandroid/os/Message;)V

    #@12
    .line 2302
    :goto_12
    return-void

    #@13
    .line 2298
    :cond_13
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@15
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mCT:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@17
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->ringingCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@19
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CdmaCall;->hangupIfAlive()V

    #@1c
    .line 2299
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@1e
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mCT:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@20
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->backgroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@22
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CdmaCall;->hangupIfAlive()V

    #@25
    .line 2300
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@27
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mCT:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@29
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->foregroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@2b
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CdmaCall;->hangupIfAlive()V

    #@2e
    .line 2301
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@30
    invoke-interface {v0, v1, v2}, Lcom/android/internal/telephony/CommandsInterface;->setRadioPower(ZLandroid/os/Message;)V

    #@33
    goto :goto_12
.end method

.method public isConcurrentVoiceAndDataAllowed()Z
    .registers 2

    #@0
    .prologue
    .line 2214
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public isMinInfoReady()Z
    .registers 2

    #@0
    .prologue
    .line 2259
    iget-boolean v0, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mIsMinInfoReady:Z

    #@2
    return v0
.end method

.method protected isRoamIndForHomeSystem_data(Ljava/lang/String;)Z
    .registers 3
    .parameter "roamInd"

    #@0
    .prologue
    .line 1881
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isRoamIndForHomeSystem(Ljava/lang/String;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method protected isSidsAllZeros()Z
    .registers 3

    #@0
    .prologue
    .line 2182
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mHomeSystemId:[I

    #@2
    if-eqz v1, :cond_15

    #@4
    .line 2183
    const/4 v0, 0x0

    #@5
    .local v0, i:I
    :goto_5
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mHomeSystemId:[I

    #@7
    array-length v1, v1

    #@8
    if-ge v0, v1, :cond_15

    #@a
    .line 2184
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mHomeSystemId:[I

    #@c
    aget v1, v1, v0

    #@e
    if-eqz v1, :cond_12

    #@10
    .line 2185
    const/4 v1, 0x0

    #@11
    .line 2189
    .end local v0           #i:I
    :goto_11
    return v1

    #@12
    .line 2183
    .restart local v0       #i:I
    :cond_12
    add-int/lit8 v0, v0, 0x1

    #@14
    goto :goto_5

    #@15
    .line 2189
    .end local v0           #i:I
    :cond_15
    const/4 v1, 0x1

    #@16
    goto :goto_11
.end method

.method protected log(Ljava/lang/String;)V
    .registers 5
    .parameter "s"

    #@0
    .prologue
    .line 2570
    const-string v0, "CDMA"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[CdmaSST] "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 2571
    return-void
.end method

.method protected loge(Ljava/lang/String;)V
    .registers 5
    .parameter "s"

    #@0
    .prologue
    .line 2575
    const-string v0, "CDMA"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[CdmaSST] "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 2576
    return-void
.end method

.method protected onUpdateIccAvailability()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 2518
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@3
    if-nez v1, :cond_6

    #@5
    .line 2546
    :cond_5
    :goto_5
    return-void

    #@6
    .line 2522
    :cond_6
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->getUiccCardApplication()Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@9
    move-result-object v0

    #@a
    .line 2524
    .local v0, newUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mUiccApplcation:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@c
    if-eq v1, v0, :cond_5

    #@e
    .line 2525
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mUiccApplcation:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@10
    if-eqz v1, :cond_29

    #@12
    .line 2526
    const-string v1, "Removing stale icc objects."

    #@14
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@17
    .line 2527
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mUiccApplcation:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@19
    invoke-virtual {v1, p0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->unregisterForReady(Landroid/os/Handler;)V

    #@1c
    .line 2528
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@1e
    if-eqz v1, :cond_25

    #@20
    .line 2529
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@22
    invoke-virtual {v1, p0}, Lcom/android/internal/telephony/uicc/IccRecords;->unregisterForRecordsLoaded(Landroid/os/Handler;)V

    #@25
    .line 2531
    :cond_25
    iput-object v3, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@27
    .line 2532
    iput-object v3, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mUiccApplcation:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@29
    .line 2534
    :cond_29
    if-eqz v0, :cond_5

    #@2b
    .line 2535
    const-string v1, "New card found"

    #@2d
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@30
    .line 2536
    iput-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mUiccApplcation:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@32
    .line 2537
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mUiccApplcation:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@34
    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getIccRecords()Lcom/android/internal/telephony/uicc/IccRecords;

    #@37
    move-result-object v1

    #@38
    iput-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@3a
    .line 2538
    iget-boolean v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isSubscriptionFromRuim:Z

    #@3c
    if-eqz v1, :cond_5

    #@3e
    .line 2539
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mUiccApplcation:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@40
    const/16 v2, 0x1a

    #@42
    invoke-virtual {v1, p0, v2, v3}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->registerForReady(Landroid/os/Handler;ILjava/lang/Object;)V

    #@45
    .line 2540
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@47
    if-eqz v1, :cond_5

    #@49
    .line 2541
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@4b
    const/16 v2, 0x1b

    #@4d
    invoke-virtual {v1, p0, v2, v3}, Lcom/android/internal/telephony/uicc/IccRecords;->registerForRecordsLoaded(Landroid/os/Handler;ILjava/lang/Object;)V

    #@50
    goto :goto_5
.end method

.method protected parseSidNid(Ljava/lang/String;Ljava/lang/String;)V
    .registers 9
    .parameter "sidStr"
    .parameter "nidStr"

    #@0
    .prologue
    .line 2450
    if-eqz p1, :cond_36

    #@2
    .line 2451
    const-string v4, ","

    #@4
    invoke-virtual {p1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@7
    move-result-object v3

    #@8
    .line 2452
    .local v3, sid:[Ljava/lang/String;
    array-length v4, v3

    #@9
    new-array v4, v4, [I

    #@b
    iput-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mHomeSystemId:[I

    #@d
    .line 2453
    const/4 v1, 0x0

    #@e
    .local v1, i:I
    :goto_e
    array-length v4, v3

    #@f
    if-ge v1, v4, :cond_36

    #@11
    .line 2455
    :try_start_11
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mHomeSystemId:[I

    #@13
    aget-object v5, v3, v1

    #@15
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@18
    move-result v5

    #@19
    aput v5, v4, v1
    :try_end_1b
    .catch Ljava/lang/NumberFormatException; {:try_start_11 .. :try_end_1b} :catch_1e

    #@1b
    .line 2453
    :goto_1b
    add-int/lit8 v1, v1, 0x1

    #@1d
    goto :goto_e

    #@1e
    .line 2456
    :catch_1e
    move-exception v0

    #@1f
    .line 2457
    .local v0, ex:Ljava/lang/NumberFormatException;
    new-instance v4, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v5, "error parsing system id: "

    #@26
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v4

    #@2a
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v4

    #@2e
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v4

    #@32
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->loge(Ljava/lang/String;)V

    #@35
    goto :goto_1b

    #@36
    .line 2461
    .end local v0           #ex:Ljava/lang/NumberFormatException;
    .end local v1           #i:I
    .end local v3           #sid:[Ljava/lang/String;
    :cond_36
    new-instance v4, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v5, "CDMA_SUBSCRIPTION: SID="

    #@3d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v4

    #@41
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v4

    #@45
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v4

    #@49
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@4c
    .line 2463
    if-eqz p2, :cond_82

    #@4e
    .line 2464
    const-string v4, ","

    #@50
    invoke-virtual {p2, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@53
    move-result-object v2

    #@54
    .line 2465
    .local v2, nid:[Ljava/lang/String;
    array-length v4, v2

    #@55
    new-array v4, v4, [I

    #@57
    iput-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mHomeNetworkId:[I

    #@59
    .line 2466
    const/4 v1, 0x0

    #@5a
    .restart local v1       #i:I
    :goto_5a
    array-length v4, v2

    #@5b
    if-ge v1, v4, :cond_82

    #@5d
    .line 2468
    :try_start_5d
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mHomeNetworkId:[I

    #@5f
    aget-object v5, v2, v1

    #@61
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@64
    move-result v5

    #@65
    aput v5, v4, v1
    :try_end_67
    .catch Ljava/lang/NumberFormatException; {:try_start_5d .. :try_end_67} :catch_6a

    #@67
    .line 2466
    :goto_67
    add-int/lit8 v1, v1, 0x1

    #@69
    goto :goto_5a

    #@6a
    .line 2469
    :catch_6a
    move-exception v0

    #@6b
    .line 2470
    .restart local v0       #ex:Ljava/lang/NumberFormatException;
    new-instance v4, Ljava/lang/StringBuilder;

    #@6d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@70
    const-string v5, "CDMA_SUBSCRIPTION: error parsing network id: "

    #@72
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v4

    #@76
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v4

    #@7a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7d
    move-result-object v4

    #@7e
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->loge(Ljava/lang/String;)V

    #@81
    goto :goto_67

    #@82
    .line 2474
    .end local v0           #ex:Ljava/lang/NumberFormatException;
    .end local v1           #i:I
    .end local v2           #nid:[Ljava/lang/String;
    :cond_82
    new-instance v4, Ljava/lang/StringBuilder;

    #@84
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@87
    const-string v5, "CDMA_SUBSCRIPTION: NID="

    #@89
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v4

    #@8d
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v4

    #@91
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@94
    move-result-object v4

    #@95
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@98
    .line 2475
    return-void
.end method

.method protected pollState()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1350
    const/4 v0, 0x1

    #@2
    new-array v0, v0, [I

    #@4
    iput-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->pollingContext:[I

    #@6
    .line 1351
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->pollingContext:[I

    #@8
    aput v3, v0, v3

    #@a
    .line 1353
    sget-object v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker$8;->$SwitchMap$com$android$internal$telephony$CommandsInterface$RadioState:[I

    #@c
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@e
    invoke-interface {v1}, Lcom/android/internal/telephony/CommandsInterface;->getRadioState()Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1}, Lcom/android/internal/telephony/CommandsInterface$RadioState;->ordinal()I

    #@15
    move-result v1

    #@16
    aget v0, v0, v1

    #@18
    packed-switch v0, :pswitch_data_6c

    #@1b
    .line 1377
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->pollingContext:[I

    #@1d
    aget v1, v0, v3

    #@1f
    add-int/lit8 v1, v1, 0x1

    #@21
    aput v1, v0, v3

    #@23
    .line 1379
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@25
    const/16 v1, 0x19

    #@27
    iget-object v2, p0, Lcom/android/internal/telephony/ServiceStateTracker;->pollingContext:[I

    #@29
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@2c
    move-result-object v1

    #@2d
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/CommandsInterface;->getOperator(Landroid/os/Message;)V

    #@30
    .line 1382
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->pollingContext:[I

    #@32
    aget v1, v0, v3

    #@34
    add-int/lit8 v1, v1, 0x1

    #@36
    aput v1, v0, v3

    #@38
    .line 1384
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@3a
    const/16 v1, 0x18

    #@3c
    iget-object v2, p0, Lcom/android/internal/telephony/ServiceStateTracker;->pollingContext:[I

    #@3e
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@41
    move-result-object v1

    #@42
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/CommandsInterface;->getVoiceRegistrationState(Landroid/os/Message;)V

    #@45
    .line 1389
    :goto_45
    return-void

    #@46
    .line 1355
    :pswitch_46
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@48
    invoke-virtual {v0}, Landroid/telephony/ServiceState;->setStateOutOfService()V

    #@4b
    .line 1356
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->newCellLoc:Landroid/telephony/cdma/CdmaCellLocation;

    #@4d
    invoke-virtual {v0}, Landroid/telephony/cdma/CdmaCellLocation;->setStateInvalid()V

    #@50
    .line 1357
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->setSignalStrengthDefaultValues()V

    #@53
    .line 1358
    iput-boolean v3, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mGotCountryCode:Z

    #@55
    .line 1360
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->pollStateDone()V

    #@58
    goto :goto_45

    #@59
    .line 1364
    :pswitch_59
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@5b
    invoke-virtual {v0}, Landroid/telephony/ServiceState;->setStateOff()V

    #@5e
    .line 1365
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->newCellLoc:Landroid/telephony/cdma/CdmaCellLocation;

    #@60
    invoke-virtual {v0}, Landroid/telephony/cdma/CdmaCellLocation;->setStateInvalid()V

    #@63
    .line 1366
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->setSignalStrengthDefaultValues()V

    #@66
    .line 1367
    iput-boolean v3, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mGotCountryCode:Z

    #@68
    .line 1369
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->pollStateDone()V

    #@6b
    goto :goto_45

    #@6c
    .line 1353
    :pswitch_data_6c
    .packed-switch 0x1
        :pswitch_46
        :pswitch_59
    .end packed-switch
.end method

.method protected pollStateDone()V
    .registers 33

    #@0
    .prologue
    .line 1461
    new-instance v28, Ljava/lang/StringBuilder;

    #@2
    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v29, "pollStateDone: oldSS=["

    #@7
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v28

    #@b
    move-object/from16 v0, p0

    #@d
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@f
    move-object/from16 v29, v0

    #@11
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v28

    #@15
    const-string v29, "] newSS=["

    #@17
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v28

    #@1b
    move-object/from16 v0, p0

    #@1d
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@1f
    move-object/from16 v29, v0

    #@21
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v28

    #@25
    const-string v29, "]"

    #@27
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v28

    #@2b
    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v28

    #@2f
    move-object/from16 v0, p0

    #@31
    move-object/from16 v1, v28

    #@33
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@36
    .line 1463
    move-object/from16 v0, p0

    #@38
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@3a
    move-object/from16 v28, v0

    #@3c
    invoke-virtual/range {v28 .. v28}, Landroid/telephony/ServiceState;->getState()I

    #@3f
    move-result v28

    #@40
    if-eqz v28, :cond_63b

    #@42
    move-object/from16 v0, p0

    #@44
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@46
    move-object/from16 v28, v0

    #@48
    invoke-virtual/range {v28 .. v28}, Landroid/telephony/ServiceState;->getState()I

    #@4b
    move-result v28

    #@4c
    if-nez v28, :cond_63b

    #@4e
    const/16 v18, 0x1

    #@50
    .line 1467
    .local v18, hasRegistered:Z
    :goto_50
    move-object/from16 v0, p0

    #@52
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@54
    move-object/from16 v28, v0

    #@56
    invoke-virtual/range {v28 .. v28}, Landroid/telephony/ServiceState;->getState()I

    #@59
    move-result v28

    #@5a
    if-nez v28, :cond_63f

    #@5c
    move-object/from16 v0, p0

    #@5e
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@60
    move-object/from16 v28, v0

    #@62
    invoke-virtual/range {v28 .. v28}, Landroid/telephony/ServiceState;->getState()I

    #@65
    move-result v28

    #@66
    if-eqz v28, :cond_63f

    #@68
    const/4 v15, 0x1

    #@69
    .line 1471
    .local v15, hasDeregistered:Z
    :goto_69
    move-object/from16 v0, p0

    #@6b
    iget v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mDataConnectionState:I

    #@6d
    move/from16 v28, v0

    #@6f
    if-eqz v28, :cond_642

    #@71
    move-object/from16 v0, p0

    #@73
    iget v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mNewDataConnectionState:I

    #@75
    move/from16 v28, v0

    #@77
    if-nez v28, :cond_642

    #@79
    const/4 v11, 0x1

    #@7a
    .line 1475
    .local v11, hasCdmaDataConnectionAttached:Z
    :goto_7a
    move-object/from16 v0, p0

    #@7c
    iget v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mDataConnectionState:I

    #@7e
    move/from16 v28, v0

    #@80
    if-nez v28, :cond_645

    #@82
    move-object/from16 v0, p0

    #@84
    iget v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mNewDataConnectionState:I

    #@86
    move/from16 v28, v0

    #@88
    if-eqz v28, :cond_645

    #@8a
    const/4 v13, 0x1

    #@8b
    .line 1479
    .local v13, hasCdmaDataConnectionDetached:Z
    :goto_8b
    move-object/from16 v0, p0

    #@8d
    iget v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mDataConnectionState:I

    #@8f
    move/from16 v28, v0

    #@91
    move-object/from16 v0, p0

    #@93
    iget v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mNewDataConnectionState:I

    #@95
    move/from16 v29, v0

    #@97
    move/from16 v0, v28

    #@99
    move/from16 v1, v29

    #@9b
    if-eq v0, v1, :cond_648

    #@9d
    const/4 v12, 0x1

    #@9e
    .line 1482
    .local v12, hasCdmaDataConnectionChanged:Z
    :goto_9e
    move-object/from16 v0, p0

    #@a0
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mRilRadioTechnology:I

    #@a2
    move/from16 v28, v0

    #@a4
    move-object/from16 v0, p0

    #@a6
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mNewRilRadioTechnology:I

    #@a8
    move/from16 v29, v0

    #@aa
    move/from16 v0, v28

    #@ac
    move/from16 v1, v29

    #@ae
    if-eq v0, v1, :cond_64b

    #@b0
    const/16 v17, 0x1

    #@b2
    .line 1484
    .local v17, hasRadioTechnologyChanged:Z
    :goto_b2
    move-object/from16 v0, p0

    #@b4
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@b6
    move-object/from16 v28, v0

    #@b8
    move-object/from16 v0, p0

    #@ba
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@bc
    move-object/from16 v29, v0

    #@be
    invoke-virtual/range {v28 .. v29}, Landroid/telephony/ServiceState;->equals(Ljava/lang/Object;)Z

    #@c1
    move-result v28

    #@c2
    if-nez v28, :cond_64f

    #@c4
    const/4 v14, 0x1

    #@c5
    .line 1486
    .local v14, hasChanged:Z
    :goto_c5
    move-object/from16 v0, p0

    #@c7
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@c9
    move-object/from16 v28, v0

    #@cb
    invoke-virtual/range {v28 .. v28}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@ce
    move-result v28

    #@cf
    if-nez v28, :cond_652

    #@d1
    move-object/from16 v0, p0

    #@d3
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@d5
    move-object/from16 v28, v0

    #@d7
    invoke-virtual/range {v28 .. v28}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@da
    move-result v28

    #@db
    if-eqz v28, :cond_652

    #@dd
    const/16 v20, 0x1

    #@df
    .line 1488
    .local v20, hasRoamingOn:Z
    :goto_df
    move-object/from16 v0, p0

    #@e1
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@e3
    move-object/from16 v28, v0

    #@e5
    invoke-virtual/range {v28 .. v28}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@e8
    move-result v28

    #@e9
    if-eqz v28, :cond_656

    #@eb
    move-object/from16 v0, p0

    #@ed
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@ef
    move-object/from16 v28, v0

    #@f1
    invoke-virtual/range {v28 .. v28}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@f4
    move-result v28

    #@f5
    if-nez v28, :cond_656

    #@f7
    const/16 v19, 0x1

    #@f9
    .line 1490
    .local v19, hasRoamingOff:Z
    :goto_f9
    move-object/from16 v0, p0

    #@fb
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->newCellLoc:Landroid/telephony/cdma/CdmaCellLocation;

    #@fd
    move-object/from16 v28, v0

    #@ff
    move-object/from16 v0, p0

    #@101
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->cellLoc:Landroid/telephony/cdma/CdmaCellLocation;

    #@103
    move-object/from16 v29, v0

    #@105
    invoke-virtual/range {v28 .. v29}, Landroid/telephony/cdma/CdmaCellLocation;->equals(Ljava/lang/Object;)Z

    #@108
    move-result v28

    #@109
    if-nez v28, :cond_65a

    #@10b
    const/16 v16, 0x1

    #@10d
    .line 1494
    .local v16, hasLocationChanged:Z
    :goto_10d
    move-object/from16 v0, p0

    #@10f
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@111
    move-object/from16 v28, v0

    #@113
    move-object/from16 v0, v28

    #@115
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@117
    move-object/from16 v28, v0

    #@119
    invoke-interface/range {v28 .. v28}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@11c
    move-result-object v28

    #@11d
    move-object/from16 v0, v28

    #@11f
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_BUGFIX_SETUP_DATACALL_ON_UNKNOWN_TECH:Z

    #@121
    move/from16 v28, v0

    #@123
    if-eqz v28, :cond_145

    #@125
    .line 1496
    move-object/from16 v0, p0

    #@127
    iget v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mDataConnectionState:I

    #@129
    move/from16 v28, v0

    #@12b
    if-eqz v28, :cond_65e

    #@12d
    move-object/from16 v0, p0

    #@12f
    iget v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mNewDataConnectionState:I

    #@131
    move/from16 v28, v0

    #@133
    if-nez v28, :cond_65e

    #@135
    move-object/from16 v0, p0

    #@137
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mNewRilRadioTechnology:I

    #@139
    move/from16 v28, v0

    #@13b
    if-nez v28, :cond_65e

    #@13d
    .line 1498
    const/16 v28, 0x1

    #@13f
    move/from16 v0, v28

    #@141
    move-object/from16 v1, p0

    #@143
    iput-boolean v0, v1, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->CheckATTACH:Z

    #@145
    .line 1512
    :cond_145
    :goto_145
    const/16 v28, 0x0

    #@147
    const-string v29, "vzw_eri"

    #@149
    invoke-static/range {v28 .. v29}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@14c
    move-result v28

    #@14d
    if-eqz v28, :cond_1e2

    #@14f
    .line 1513
    move-object/from16 v0, p0

    #@151
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@153
    move-object/from16 v28, v0

    #@155
    move-object/from16 v0, p0

    #@157
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@159
    move-object/from16 v29, v0

    #@15b
    invoke-virtual/range {v29 .. v29}, Landroid/telephony/ServiceState;->getOperatorAlphaLong()Ljava/lang/String;

    #@15e
    move-result-object v29

    #@15f
    invoke-virtual/range {v28 .. v29}, Landroid/telephony/ServiceState;->setOperatorAlphaLong(Ljava/lang/String;)V

    #@162
    .line 1514
    move-object/from16 v0, p0

    #@164
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@166
    move-object/from16 v28, v0

    #@168
    invoke-virtual/range {v28 .. v28}, Landroid/telephony/ServiceState;->getState()I

    #@16b
    move-result v28

    #@16c
    move-object/from16 v0, p0

    #@16e
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@170
    move-object/from16 v29, v0

    #@172
    invoke-virtual/range {v29 .. v29}, Landroid/telephony/ServiceState;->getState()I

    #@175
    move-result v29

    #@176
    move/from16 v0, v28

    #@178
    move/from16 v1, v29

    #@17a
    if-eq v0, v1, :cond_69a

    #@17c
    const/16 v28, 0x1

    #@17e
    :goto_17e
    move/from16 v0, v28

    #@180
    move-object/from16 v1, p0

    #@182
    iput-boolean v0, v1, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->hasStateChanged:Z

    #@184
    .line 1515
    move-object/from16 v0, p0

    #@186
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@188
    move-object/from16 v28, v0

    #@18a
    invoke-virtual/range {v28 .. v28}, Landroid/telephony/ServiceState;->getNetworkId()I

    #@18d
    move-result v28

    #@18e
    move-object/from16 v0, p0

    #@190
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@192
    move-object/from16 v29, v0

    #@194
    invoke-virtual/range {v29 .. v29}, Landroid/telephony/ServiceState;->getNetworkId()I

    #@197
    move-result v29

    #@198
    move/from16 v0, v28

    #@19a
    move/from16 v1, v29

    #@19c
    if-ne v0, v1, :cond_1b8

    #@19e
    move-object/from16 v0, p0

    #@1a0
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@1a2
    move-object/from16 v28, v0

    #@1a4
    invoke-virtual/range {v28 .. v28}, Landroid/telephony/ServiceState;->getSystemId()I

    #@1a7
    move-result v28

    #@1a8
    move-object/from16 v0, p0

    #@1aa
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@1ac
    move-object/from16 v29, v0

    #@1ae
    invoke-virtual/range {v29 .. v29}, Landroid/telephony/ServiceState;->getSystemId()I

    #@1b1
    move-result v29

    #@1b2
    move/from16 v0, v28

    #@1b4
    move/from16 v1, v29

    #@1b6
    if-eq v0, v1, :cond_69e

    #@1b8
    :cond_1b8
    const/16 v28, 0x1

    #@1ba
    :goto_1ba
    move/from16 v0, v28

    #@1bc
    move-object/from16 v1, p0

    #@1be
    iput-boolean v0, v1, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->hasChangedSystemIDNetworkID:Z

    #@1c0
    .line 1517
    move-object/from16 v0, p0

    #@1c2
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@1c4
    move-object/from16 v28, v0

    #@1c6
    invoke-virtual/range {v28 .. v28}, Landroid/telephony/ServiceState;->getCdmaRoamingIndicator()I

    #@1c9
    move-result v28

    #@1ca
    move-object/from16 v0, p0

    #@1cc
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@1ce
    move-object/from16 v29, v0

    #@1d0
    invoke-virtual/range {v29 .. v29}, Landroid/telephony/ServiceState;->getCdmaRoamingIndicator()I

    #@1d3
    move-result v29

    #@1d4
    move/from16 v0, v28

    #@1d6
    move/from16 v1, v29

    #@1d8
    if-eq v0, v1, :cond_6a2

    #@1da
    const/16 v28, 0x1

    #@1dc
    :goto_1dc
    move/from16 v0, v28

    #@1de
    move-object/from16 v1, p0

    #@1e0
    iput-boolean v0, v1, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->hasChangedRoamingIndicator:Z

    #@1e2
    .line 1521
    :cond_1e2
    move-object/from16 v0, p0

    #@1e4
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@1e6
    move-object/from16 v28, v0

    #@1e8
    invoke-virtual/range {v28 .. v28}, Landroid/telephony/ServiceState;->getState()I

    #@1eb
    move-result v28

    #@1ec
    move-object/from16 v0, p0

    #@1ee
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@1f0
    move-object/from16 v29, v0

    #@1f2
    invoke-virtual/range {v29 .. v29}, Landroid/telephony/ServiceState;->getState()I

    #@1f5
    move-result v29

    #@1f6
    move/from16 v0, v28

    #@1f8
    move/from16 v1, v29

    #@1fa
    if-ne v0, v1, :cond_20e

    #@1fc
    move-object/from16 v0, p0

    #@1fe
    iget v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mDataConnectionState:I

    #@200
    move/from16 v28, v0

    #@202
    move-object/from16 v0, p0

    #@204
    iget v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mNewDataConnectionState:I

    #@206
    move/from16 v29, v0

    #@208
    move/from16 v0, v28

    #@20a
    move/from16 v1, v29

    #@20c
    if-eq v0, v1, :cond_25c

    #@20e
    .line 1523
    :cond_20e
    const v28, 0xc3c4

    #@211
    const/16 v29, 0x4

    #@213
    move/from16 v0, v29

    #@215
    new-array v0, v0, [Ljava/lang/Object;

    #@217
    move-object/from16 v29, v0

    #@219
    const/16 v30, 0x0

    #@21b
    move-object/from16 v0, p0

    #@21d
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@21f
    move-object/from16 v31, v0

    #@221
    invoke-virtual/range {v31 .. v31}, Landroid/telephony/ServiceState;->getState()I

    #@224
    move-result v31

    #@225
    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@228
    move-result-object v31

    #@229
    aput-object v31, v29, v30

    #@22b
    const/16 v30, 0x1

    #@22d
    move-object/from16 v0, p0

    #@22f
    iget v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mDataConnectionState:I

    #@231
    move/from16 v31, v0

    #@233
    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@236
    move-result-object v31

    #@237
    aput-object v31, v29, v30

    #@239
    const/16 v30, 0x2

    #@23b
    move-object/from16 v0, p0

    #@23d
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@23f
    move-object/from16 v31, v0

    #@241
    invoke-virtual/range {v31 .. v31}, Landroid/telephony/ServiceState;->getState()I

    #@244
    move-result v31

    #@245
    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@248
    move-result-object v31

    #@249
    aput-object v31, v29, v30

    #@24b
    const/16 v30, 0x3

    #@24d
    move-object/from16 v0, p0

    #@24f
    iget v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mNewDataConnectionState:I

    #@251
    move/from16 v31, v0

    #@253
    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@256
    move-result-object v31

    #@257
    aput-object v31, v29, v30

    #@259
    invoke-static/range {v28 .. v29}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@25c
    .line 1529
    :cond_25c
    move-object/from16 v0, p0

    #@25e
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@260
    move-object/from16 v28, v0

    #@262
    invoke-virtual/range {v28 .. v28}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@265
    move-result-object v28

    #@266
    const-string v29, "support_assisted_dialing"

    #@268
    invoke-static/range {v28 .. v29}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@26b
    move-result v28

    #@26c
    if-nez v28, :cond_280

    #@26e
    move-object/from16 v0, p0

    #@270
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@272
    move-object/from16 v28, v0

    #@274
    invoke-virtual/range {v28 .. v28}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@277
    move-result-object v28

    #@278
    const-string v29, "support_smart_dialing"

    #@27a
    invoke-static/range {v28 .. v29}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@27d
    move-result v28

    #@27e
    if-eqz v28, :cond_2a1

    #@280
    .line 1533
    :cond_280
    move-object/from16 v0, p0

    #@282
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@284
    move-object/from16 v28, v0

    #@286
    invoke-virtual/range {v28 .. v28}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@289
    move-result-object v28

    #@28a
    invoke-virtual/range {v28 .. v28}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@28d
    move-result-object v28

    #@28e
    const-string v29, "assist_dial_ota_sid"

    #@290
    move-object/from16 v0, p0

    #@292
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@294
    move-object/from16 v30, v0

    #@296
    invoke-virtual/range {v30 .. v30}, Landroid/telephony/ServiceState;->getSystemId()I

    #@299
    move-result v30

    #@29a
    invoke-static/range {v30 .. v30}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@29d
    move-result-object v30

    #@29e
    invoke-static/range {v28 .. v30}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    #@2a1
    .line 1541
    :cond_2a1
    move-object/from16 v0, p0

    #@2a3
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@2a5
    move-object/from16 v27, v0

    #@2a7
    .line 1542
    .local v27, tss:Landroid/telephony/ServiceState;
    move-object/from16 v0, p0

    #@2a9
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@2ab
    move-object/from16 v28, v0

    #@2ad
    move-object/from16 v0, v28

    #@2af
    move-object/from16 v1, p0

    #@2b1
    iput-object v0, v1, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@2b3
    .line 1543
    move-object/from16 v0, v27

    #@2b5
    move-object/from16 v1, p0

    #@2b7
    iput-object v0, v1, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@2b9
    .line 1545
    move-object/from16 v0, p0

    #@2bb
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@2bd
    move-object/from16 v28, v0

    #@2bf
    invoke-virtual/range {v28 .. v28}, Landroid/telephony/ServiceState;->setStateOutOfService()V

    #@2c2
    .line 1547
    move-object/from16 v0, p0

    #@2c4
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->cellLoc:Landroid/telephony/cdma/CdmaCellLocation;

    #@2c6
    move-object/from16 v26, v0

    #@2c8
    .line 1548
    .local v26, tcl:Landroid/telephony/cdma/CdmaCellLocation;
    move-object/from16 v0, p0

    #@2ca
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->newCellLoc:Landroid/telephony/cdma/CdmaCellLocation;

    #@2cc
    move-object/from16 v28, v0

    #@2ce
    move-object/from16 v0, v28

    #@2d0
    move-object/from16 v1, p0

    #@2d2
    iput-object v0, v1, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->cellLoc:Landroid/telephony/cdma/CdmaCellLocation;

    #@2d4
    .line 1549
    move-object/from16 v0, v26

    #@2d6
    move-object/from16 v1, p0

    #@2d8
    iput-object v0, v1, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->newCellLoc:Landroid/telephony/cdma/CdmaCellLocation;

    #@2da
    .line 1551
    move-object/from16 v0, p0

    #@2dc
    iget v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mNewDataConnectionState:I

    #@2de
    move/from16 v28, v0

    #@2e0
    move/from16 v0, v28

    #@2e2
    move-object/from16 v1, p0

    #@2e4
    iput v0, v1, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mDataConnectionState:I

    #@2e6
    .line 1552
    move-object/from16 v0, p0

    #@2e8
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mNewRilRadioTechnology:I

    #@2ea
    move/from16 v28, v0

    #@2ec
    move/from16 v0, v28

    #@2ee
    move-object/from16 v1, p0

    #@2f0
    iput v0, v1, Lcom/android/internal/telephony/ServiceStateTracker;->mRilRadioTechnology:I

    #@2f2
    .line 1554
    const/16 v28, 0x0

    #@2f4
    move/from16 v0, v28

    #@2f6
    move-object/from16 v1, p0

    #@2f8
    iput v0, v1, Lcom/android/internal/telephony/ServiceStateTracker;->mNewRilRadioTechnology:I

    #@2fa
    .line 1556
    move-object/from16 v0, p0

    #@2fc
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@2fe
    move-object/from16 v28, v0

    #@300
    invoke-virtual/range {v28 .. v28}, Landroid/telephony/ServiceState;->setStateOutOfService()V

    #@303
    .line 1558
    if-eqz v17, :cond_31a

    #@305
    .line 1559
    move-object/from16 v0, p0

    #@307
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@309
    move-object/from16 v28, v0

    #@30b
    const-string v29, "gsm.network.type"

    #@30d
    move-object/from16 v0, p0

    #@30f
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mRilRadioTechnology:I

    #@311
    move/from16 v30, v0

    #@313
    invoke-static/range {v30 .. v30}, Landroid/telephony/ServiceState;->rilRadioTechnologyToString(I)Ljava/lang/String;

    #@316
    move-result-object v30

    #@317
    invoke-virtual/range {v28 .. v30}, Lcom/android/internal/telephony/cdma/CDMAPhone;->setSystemProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@31a
    .line 1563
    :cond_31a
    if-eqz v18, :cond_325

    #@31c
    .line 1564
    move-object/from16 v0, p0

    #@31e
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mNetworkAttachedRegistrants:Landroid/os/RegistrantList;

    #@320
    move-object/from16 v28, v0

    #@322
    invoke-virtual/range {v28 .. v28}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@325
    .line 1567
    :cond_325
    if-eqz v14, :cond_58a

    #@327
    .line 1568
    move-object/from16 v0, p0

    #@329
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@32b
    move-object/from16 v28, v0

    #@32d
    invoke-interface/range {v28 .. v28}, Lcom/android/internal/telephony/CommandsInterface;->getRadioState()Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@330
    move-result-object v28

    #@331
    invoke-virtual/range {v28 .. v28}, Lcom/android/internal/telephony/CommandsInterface$RadioState;->isOn()Z

    #@334
    move-result v28

    #@335
    if-eqz v28, :cond_491

    #@337
    move-object/from16 v0, p0

    #@339
    iget-boolean v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isSubscriptionFromRuim:Z

    #@33b
    move/from16 v28, v0

    #@33d
    if-nez v28, :cond_491

    #@33f
    .line 1571
    move-object/from16 v0, p0

    #@341
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@343
    move-object/from16 v28, v0

    #@345
    invoke-virtual/range {v28 .. v28}, Landroid/telephony/ServiceState;->getState()I

    #@348
    move-result v28

    #@349
    if-nez v28, :cond_6a6

    #@34b
    .line 1572
    move-object/from16 v0, p0

    #@34d
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@34f
    move-object/from16 v28, v0

    #@351
    invoke-virtual/range {v28 .. v28}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getCdmaEriText()Ljava/lang/String;

    #@354
    move-result-object v8

    #@355
    .line 1579
    .local v8, eriText:Ljava/lang/String;
    :goto_355
    move-object/from16 v0, p0

    #@357
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@359
    move-object/from16 v28, v0

    #@35b
    move-object/from16 v0, v28

    #@35d
    invoke-virtual {v0, v8}, Landroid/telephony/ServiceState;->setOperatorAlphaLong(Ljava/lang/String;)V

    #@360
    .line 1581
    const/16 v28, 0x0

    #@362
    const-string v29, "vzw_eri"

    #@364
    invoke-static/range {v28 .. v29}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@367
    move-result v28

    #@368
    if-eqz v28, :cond_491

    #@36a
    .line 1582
    const/4 v5, -0x1

    #@36b
    .line 1583
    .local v5, alertId:I
    move-object/from16 v0, p0

    #@36d
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@36f
    move-object/from16 v28, v0

    #@371
    invoke-virtual/range {v28 .. v28}, Landroid/telephony/ServiceState;->getState()I

    #@374
    move-result v28

    #@375
    if-nez v28, :cond_6bd

    #@377
    .line 1584
    move-object/from16 v0, p0

    #@379
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@37b
    move-object/from16 v28, v0

    #@37d
    invoke-virtual/range {v28 .. v28}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getAlertId()I

    #@380
    move-result v5

    #@381
    .line 1589
    :cond_381
    :goto_381
    move-object/from16 v0, p0

    #@383
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@385
    move-object/from16 v28, v0

    #@387
    if-eqz v28, :cond_394

    #@389
    move-object/from16 v0, p0

    #@38b
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@38d
    move-object/from16 v28, v0

    #@38f
    move-object/from16 v0, v28

    #@391
    invoke-virtual {v0, v8}, Landroid/telephony/ServiceState;->setOperatorAlphaLong(Ljava/lang/String;)V

    #@394
    .line 1591
    :cond_394
    sget-boolean v28, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->DBG_CALL:Z

    #@396
    if-eqz v28, :cond_3ce

    #@398
    const-string v28, "CDMA"

    #@39a
    new-instance v29, Ljava/lang/StringBuilder;

    #@39c
    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    #@39f
    const-string v30, "newSS.getNetworkId() = "

    #@3a1
    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a4
    move-result-object v29

    #@3a5
    move-object/from16 v0, p0

    #@3a7
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@3a9
    move-object/from16 v30, v0

    #@3ab
    invoke-virtual/range {v30 .. v30}, Landroid/telephony/ServiceState;->getNetworkId()I

    #@3ae
    move-result v30

    #@3af
    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3b2
    move-result-object v29

    #@3b3
    const-string v30, ", ss.getNetworkId() = "

    #@3b5
    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b8
    move-result-object v29

    #@3b9
    move-object/from16 v0, p0

    #@3bb
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@3bd
    move-object/from16 v30, v0

    #@3bf
    invoke-virtual/range {v30 .. v30}, Landroid/telephony/ServiceState;->getNetworkId()I

    #@3c2
    move-result v30

    #@3c3
    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3c6
    move-result-object v29

    #@3c7
    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3ca
    move-result-object v29

    #@3cb
    invoke-static/range {v28 .. v29}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3ce
    .line 1592
    :cond_3ce
    sget-boolean v28, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->DBG_CALL:Z

    #@3d0
    if-eqz v28, :cond_408

    #@3d2
    const-string v28, "CDMA"

    #@3d4
    new-instance v29, Ljava/lang/StringBuilder;

    #@3d6
    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    #@3d9
    const-string v30, "newSS.getSystemId() = "

    #@3db
    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3de
    move-result-object v29

    #@3df
    move-object/from16 v0, p0

    #@3e1
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@3e3
    move-object/from16 v30, v0

    #@3e5
    invoke-virtual/range {v30 .. v30}, Landroid/telephony/ServiceState;->getSystemId()I

    #@3e8
    move-result v30

    #@3e9
    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3ec
    move-result-object v29

    #@3ed
    const-string v30, ", ss.getSystemId() = "

    #@3ef
    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f2
    move-result-object v29

    #@3f3
    move-object/from16 v0, p0

    #@3f5
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@3f7
    move-object/from16 v30, v0

    #@3f9
    invoke-virtual/range {v30 .. v30}, Landroid/telephony/ServiceState;->getSystemId()I

    #@3fc
    move-result v30

    #@3fd
    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@400
    move-result-object v29

    #@401
    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@404
    move-result-object v29

    #@405
    invoke-static/range {v28 .. v29}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@408
    .line 1593
    :cond_408
    move-object/from16 v0, p0

    #@40a
    iget-boolean v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->hasStateChanged:Z

    #@40c
    move/from16 v28, v0

    #@40e
    if-nez v28, :cond_418

    #@410
    move-object/from16 v0, p0

    #@412
    iget-boolean v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->hasChangedRoamingIndicator:Z

    #@414
    move/from16 v28, v0

    #@416
    if-eqz v28, :cond_41c

    #@418
    :cond_418
    const/16 v28, -0x1

    #@41a
    sput v28, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->prev_alertId:I

    #@41c
    .line 1594
    :cond_41c
    move-object/from16 v0, p0

    #@41e
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@420
    move-object/from16 v28, v0

    #@422
    invoke-virtual/range {v28 .. v28}, Landroid/telephony/ServiceState;->getState()I

    #@425
    move-result v28

    #@426
    const/16 v29, 0x1

    #@428
    move/from16 v0, v28

    #@42a
    move/from16 v1, v29

    #@42c
    if-ne v0, v1, :cond_6d2

    #@42e
    move-object/from16 v0, p0

    #@430
    iget-boolean v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->hasStateChanged:Z

    #@432
    move/from16 v28, v0

    #@434
    if-eqz v28, :cond_6d2

    #@436
    move-object/from16 v0, p0

    #@438
    iget-boolean v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isInShutDown:Z

    #@43a
    move/from16 v28, v0

    #@43c
    if-nez v28, :cond_6d2

    #@43e
    .line 1595
    sget-boolean v28, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->DBG_CALL:Z

    #@440
    if-eqz v28, :cond_470

    #@442
    const-string v28, "CDMA"

    #@444
    new-instance v29, Ljava/lang/StringBuilder;

    #@446
    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    #@449
    const-string v30, "isInShutDown : "

    #@44b
    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44e
    move-result-object v29

    #@44f
    move-object/from16 v0, p0

    #@451
    iget-boolean v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isInShutDown:Z

    #@453
    move/from16 v30, v0

    #@455
    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@458
    move-result-object v29

    #@459
    const-string v30, ", hasStateChanged : "

    #@45b
    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45e
    move-result-object v29

    #@45f
    move-object/from16 v0, p0

    #@461
    iget-boolean v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->hasStateChanged:Z

    #@463
    move/from16 v30, v0

    #@465
    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@468
    move-result-object v29

    #@469
    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46c
    move-result-object v29

    #@46d
    invoke-static/range {v28 .. v29}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@470
    .line 1596
    :cond_470
    const/16 v28, 0x3e8

    #@472
    move-object/from16 v0, p0

    #@474
    move/from16 v1, v28

    #@476
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->PlayVZWERISound(I)V

    #@479
    .line 1605
    :cond_479
    :goto_479
    new-instance v9, Landroid/content/Intent;

    #@47b
    const-string v28, "com.android.erichanged"

    #@47d
    move-object/from16 v0, v28

    #@47f
    invoke-direct {v9, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@482
    .line 1606
    .local v9, erichanged:Landroid/content/Intent;
    move-object/from16 v0, p0

    #@484
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@486
    move-object/from16 v28, v0

    #@488
    invoke-virtual/range {v28 .. v28}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@48b
    move-result-object v28

    #@48c
    move-object/from16 v0, v28

    #@48e
    invoke-virtual {v0, v9}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@491
    .line 1613
    .end local v5           #alertId:I
    .end local v8           #eriText:Ljava/lang/String;
    .end local v9           #erichanged:Landroid/content/Intent;
    :cond_491
    move-object/from16 v0, p0

    #@493
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@495
    move-object/from16 v28, v0

    #@497
    const-string v29, "gsm.operator.alpha"

    #@499
    move-object/from16 v0, p0

    #@49b
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@49d
    move-object/from16 v30, v0

    #@49f
    invoke-virtual/range {v30 .. v30}, Landroid/telephony/ServiceState;->getOperatorAlphaLong()Ljava/lang/String;

    #@4a2
    move-result-object v30

    #@4a3
    invoke-virtual/range {v28 .. v30}, Lcom/android/internal/telephony/cdma/CDMAPhone;->setSystemProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@4a6
    .line 1616
    const-string v28, "gsm.operator.numeric"

    #@4a8
    const-string v29, ""

    #@4aa
    invoke-static/range {v28 .. v29}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@4ad
    move-result-object v25

    #@4ae
    .line 1618
    .local v25, prevOperatorNumeric:Ljava/lang/String;
    move-object/from16 v0, p0

    #@4b0
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@4b2
    move-object/from16 v28, v0

    #@4b4
    invoke-virtual/range {v28 .. v28}, Landroid/telephony/ServiceState;->getOperatorNumeric()Ljava/lang/String;

    #@4b7
    move-result-object v24

    #@4b8
    .line 1619
    .local v24, operatorNumeric:Ljava/lang/String;
    move-object/from16 v0, p0

    #@4ba
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@4bc
    move-object/from16 v28, v0

    #@4be
    const-string v29, "gsm.operator.numeric"

    #@4c0
    move-object/from16 v0, v28

    #@4c2
    move-object/from16 v1, v29

    #@4c4
    move-object/from16 v2, v24

    #@4c6
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/cdma/CDMAPhone;->setSystemProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@4c9
    .line 1621
    if-nez v24, :cond_6fd

    #@4cb
    .line 1622
    const-string v28, "operatorNumeric is null"

    #@4cd
    move-object/from16 v0, p0

    #@4cf
    move-object/from16 v1, v28

    #@4d1
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@4d4
    .line 1623
    move-object/from16 v0, p0

    #@4d6
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@4d8
    move-object/from16 v28, v0

    #@4da
    const-string v29, "gsm.operator.iso-country"

    #@4dc
    const-string v30, ""

    #@4de
    invoke-virtual/range {v28 .. v30}, Lcom/android/internal/telephony/cdma/CDMAPhone;->setSystemProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@4e1
    .line 1624
    const/16 v28, 0x0

    #@4e3
    move/from16 v0, v28

    #@4e5
    move-object/from16 v1, p0

    #@4e7
    iput-boolean v0, v1, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mGotCountryCode:Z

    #@4e9
    .line 1648
    :cond_4e9
    :goto_4e9
    move-object/from16 v0, p0

    #@4eb
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@4ed
    move-object/from16 v28, v0

    #@4ef
    invoke-virtual/range {v28 .. v28}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@4f2
    move-result-object v28

    #@4f3
    const-string v29, "support_assisted_dialing"

    #@4f5
    invoke-static/range {v28 .. v29}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@4f8
    move-result v28

    #@4f9
    if-nez v28, :cond_50d

    #@4fb
    move-object/from16 v0, p0

    #@4fd
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@4ff
    move-object/from16 v28, v0

    #@501
    invoke-virtual/range {v28 .. v28}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@504
    move-result-object v28

    #@505
    const-string v29, "support_smart_dialing"

    #@507
    invoke-static/range {v28 .. v29}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@50a
    move-result v28

    #@50b
    if-eqz v28, :cond_559

    #@50d
    .line 1652
    :cond_50d
    invoke-static/range {v24 .. v24}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@510
    move-result v28

    #@511
    if-nez v28, :cond_559

    #@513
    .line 1655
    const/16 v28, 0x0

    #@515
    const/16 v29, 0x3

    #@517
    :try_start_517
    move-object/from16 v0, v24

    #@519
    move/from16 v1, v28

    #@51b
    move/from16 v2, v29

    #@51d
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@520
    move-result-object v6

    #@521
    .line 1656
    .local v6, assistedDialingMcc:Ljava/lang/String;
    const-string v28, "CdmaServiceStateTracker"

    #@523
    const-string v29, " ***** put System.ASSIST_DIAL_OTA_MCC "

    #@525
    invoke-static/range {v28 .. v29}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@528
    .line 1657
    move-object/from16 v0, p0

    #@52a
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@52c
    move-object/from16 v28, v0

    #@52e
    invoke-virtual/range {v28 .. v28}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@531
    move-result-object v28

    #@532
    invoke-virtual/range {v28 .. v28}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@535
    move-result-object v28

    #@536
    const-string v29, "assist_dial_ota_mcc"

    #@538
    move-object/from16 v0, v28

    #@53a
    move-object/from16 v1, v29

    #@53c
    invoke-static {v0, v1, v6}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    #@53f
    .line 1659
    const-string v28, "CdmaServiceStateTracker"

    #@541
    new-instance v29, Ljava/lang/StringBuilder;

    #@543
    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    #@546
    const-string v30, " ***** MCC  "

    #@548
    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54b
    move-result-object v29

    #@54c
    move-object/from16 v0, v29

    #@54e
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@551
    move-result-object v29

    #@552
    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@555
    move-result-object v29

    #@556
    invoke-static/range {v28 .. v29}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_559
    .catch Ljava/lang/NumberFormatException; {:try_start_517 .. :try_end_559} :catch_79d

    #@559
    .line 1667
    .end local v6           #assistedDialingMcc:Ljava/lang/String;
    :cond_559
    :goto_559
    move-object/from16 v0, p0

    #@55b
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@55d
    move-object/from16 v29, v0

    #@55f
    const-string v30, "gsm.operator.isroaming"

    #@561
    move-object/from16 v0, p0

    #@563
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@565
    move-object/from16 v28, v0

    #@567
    invoke-virtual/range {v28 .. v28}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@56a
    move-result v28

    #@56b
    if-eqz v28, :cond_7a7

    #@56d
    const-string v28, "true"

    #@56f
    :goto_56f
    move-object/from16 v0, v29

    #@571
    move-object/from16 v1, v30

    #@573
    move-object/from16 v2, v28

    #@575
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/cdma/CDMAPhone;->setSystemProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@578
    .line 1670
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->updateSpnDisplay()V

    #@57b
    .line 1671
    move-object/from16 v0, p0

    #@57d
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@57f
    move-object/from16 v28, v0

    #@581
    move-object/from16 v0, p0

    #@583
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@585
    move-object/from16 v29, v0

    #@587
    invoke-virtual/range {v28 .. v29}, Lcom/android/internal/telephony/cdma/CDMAPhone;->notifyServiceStateChanged(Landroid/telephony/ServiceState;)V

    #@58a
    .line 1674
    .end local v24           #operatorNumeric:Ljava/lang/String;
    .end local v25           #prevOperatorNumeric:Ljava/lang/String;
    :cond_58a
    if-eqz v11, :cond_7ab

    #@58c
    .line 1675
    move-object/from16 v0, p0

    #@58e
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mAttachedRegistrants:Landroid/os/RegistrantList;

    #@590
    move-object/from16 v28, v0

    #@592
    invoke-virtual/range {v28 .. v28}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@595
    .line 1687
    :cond_595
    :goto_595
    if-eqz v13, :cond_5a0

    #@597
    .line 1688
    move-object/from16 v0, p0

    #@599
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mDetachedRegistrants:Landroid/os/RegistrantList;

    #@59b
    move-object/from16 v28, v0

    #@59d
    invoke-virtual/range {v28 .. v28}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@5a0
    .line 1691
    :cond_5a0
    if-nez v12, :cond_5a4

    #@5a2
    if-eqz v17, :cond_5af

    #@5a4
    .line 1692
    :cond_5a4
    move-object/from16 v0, p0

    #@5a6
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@5a8
    move-object/from16 v28, v0

    #@5aa
    const/16 v29, 0x0

    #@5ac
    invoke-virtual/range {v28 .. v29}, Lcom/android/internal/telephony/cdma/CDMAPhone;->notifyDataConnection(Ljava/lang/String;)V

    #@5af
    .line 1695
    :cond_5af
    if-eqz v20, :cond_60f

    #@5b1
    .line 1696
    move-object/from16 v0, p0

    #@5b3
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mRoamingOnRegistrants:Landroid/os/RegistrantList;

    #@5b5
    move-object/from16 v28, v0

    #@5b7
    invoke-virtual/range {v28 .. v28}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@5ba
    .line 1698
    move-object/from16 v0, p0

    #@5bc
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@5be
    move-object/from16 v28, v0

    #@5c0
    invoke-interface/range {v28 .. v28}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@5c3
    move-result-object v28

    #@5c4
    move-object/from16 v0, v28

    #@5c6
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_SEND_DATA_ROAM_POPUP_INTENT_VZW:Z

    #@5c8
    move/from16 v28, v0

    #@5ca
    if-eqz v28, :cond_60f

    #@5cc
    .line 1700
    move-object/from16 v0, p0

    #@5ce
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@5d0
    move-object/from16 v28, v0

    #@5d2
    move-object/from16 v0, v28

    #@5d4
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@5d6
    move-object/from16 v28, v0

    #@5d8
    invoke-virtual/range {v28 .. v28}, Lcom/android/internal/telephony/DataConnectionTracker;->getDataOnRoamingEnabled()Z

    #@5db
    move-result v28

    #@5dc
    if-nez v28, :cond_60f

    #@5de
    move-object/from16 v0, p0

    #@5e0
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@5e2
    move-object/from16 v28, v0

    #@5e4
    invoke-virtual/range {v28 .. v28}, Landroid/telephony/ServiceState;->getState()I

    #@5e7
    move-result v28

    #@5e8
    if-nez v28, :cond_60f

    #@5ea
    .line 1702
    const-string v28, " ***** Send Roam intent  ACTION_DATA_ROAMING_MENU"

    #@5ec
    move-object/from16 v0, p0

    #@5ee
    move-object/from16 v1, v28

    #@5f0
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@5f3
    .line 1703
    new-instance v21, Landroid/content/Intent;

    #@5f5
    const-string v28, "com.lge.android.intent.action.ACTION_DATA_ROAMING_MENU"

    #@5f7
    move-object/from16 v0, v21

    #@5f9
    move-object/from16 v1, v28

    #@5fb
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@5fe
    .line 1704
    .local v21, intent:Landroid/content/Intent;
    move-object/from16 v0, p0

    #@600
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@602
    move-object/from16 v28, v0

    #@604
    invoke-virtual/range {v28 .. v28}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@607
    move-result-object v28

    #@608
    move-object/from16 v0, v28

    #@60a
    move-object/from16 v1, v21

    #@60c
    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@60f
    .line 1710
    .end local v21           #intent:Landroid/content/Intent;
    :cond_60f
    if-eqz v19, :cond_61a

    #@611
    .line 1711
    move-object/from16 v0, p0

    #@613
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mRoamingOffRegistrants:Landroid/os/RegistrantList;

    #@615
    move-object/from16 v28, v0

    #@617
    invoke-virtual/range {v28 .. v28}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@61a
    .line 1714
    :cond_61a
    if-eqz v16, :cond_625

    #@61c
    .line 1715
    move-object/from16 v0, p0

    #@61e
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@620
    move-object/from16 v28, v0

    #@622
    invoke-virtual/range {v28 .. v28}, Lcom/android/internal/telephony/cdma/CDMAPhone;->notifyLocationChanged()V

    #@625
    .line 1719
    :cond_625
    const/16 v28, 0x0

    #@627
    const-string v29, "vzw_gfit"

    #@629
    invoke-static/range {v28 .. v29}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@62c
    move-result v28

    #@62d
    if-eqz v28, :cond_63a

    #@62f
    .line 1720
    if-eqz v15, :cond_63a

    #@631
    .line 1721
    move-object/from16 v0, p0

    #@633
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mNoServiceChangedRegistrants:Landroid/os/RegistrantList;

    #@635
    move-object/from16 v28, v0

    #@637
    invoke-virtual/range {v28 .. v28}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@63a
    .line 1727
    :cond_63a
    return-void

    #@63b
    .line 1463
    .end local v11           #hasCdmaDataConnectionAttached:Z
    .end local v12           #hasCdmaDataConnectionChanged:Z
    .end local v13           #hasCdmaDataConnectionDetached:Z
    .end local v14           #hasChanged:Z
    .end local v15           #hasDeregistered:Z
    .end local v16           #hasLocationChanged:Z
    .end local v17           #hasRadioTechnologyChanged:Z
    .end local v18           #hasRegistered:Z
    .end local v19           #hasRoamingOff:Z
    .end local v20           #hasRoamingOn:Z
    .end local v26           #tcl:Landroid/telephony/cdma/CdmaCellLocation;
    .end local v27           #tss:Landroid/telephony/ServiceState;
    :cond_63b
    const/16 v18, 0x0

    #@63d
    goto/16 :goto_50

    #@63f
    .line 1467
    .restart local v18       #hasRegistered:Z
    :cond_63f
    const/4 v15, 0x0

    #@640
    goto/16 :goto_69

    #@642
    .line 1471
    .restart local v15       #hasDeregistered:Z
    :cond_642
    const/4 v11, 0x0

    #@643
    goto/16 :goto_7a

    #@645
    .line 1475
    .restart local v11       #hasCdmaDataConnectionAttached:Z
    :cond_645
    const/4 v13, 0x0

    #@646
    goto/16 :goto_8b

    #@648
    .line 1479
    .restart local v13       #hasCdmaDataConnectionDetached:Z
    :cond_648
    const/4 v12, 0x0

    #@649
    goto/16 :goto_9e

    #@64b
    .line 1482
    .restart local v12       #hasCdmaDataConnectionChanged:Z
    :cond_64b
    const/16 v17, 0x0

    #@64d
    goto/16 :goto_b2

    #@64f
    .line 1484
    .restart local v17       #hasRadioTechnologyChanged:Z
    :cond_64f
    const/4 v14, 0x0

    #@650
    goto/16 :goto_c5

    #@652
    .line 1486
    .restart local v14       #hasChanged:Z
    :cond_652
    const/16 v20, 0x0

    #@654
    goto/16 :goto_df

    #@656
    .line 1488
    .restart local v20       #hasRoamingOn:Z
    :cond_656
    const/16 v19, 0x0

    #@658
    goto/16 :goto_f9

    #@65a
    .line 1490
    .restart local v19       #hasRoamingOff:Z
    :cond_65a
    const/16 v16, 0x0

    #@65c
    goto/16 :goto_10d

    #@65e
    .line 1500
    .restart local v16       #hasLocationChanged:Z
    :cond_65e
    move-object/from16 v0, p0

    #@660
    iget v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mDataConnectionState:I

    #@662
    move/from16 v28, v0

    #@664
    if-nez v28, :cond_680

    #@666
    move-object/from16 v0, p0

    #@668
    iget v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mNewDataConnectionState:I

    #@66a
    move/from16 v28, v0

    #@66c
    if-nez v28, :cond_680

    #@66e
    move-object/from16 v0, p0

    #@670
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mNewRilRadioTechnology:I

    #@672
    move/from16 v28, v0

    #@674
    if-nez v28, :cond_680

    #@676
    .line 1502
    const/16 v28, 0x1

    #@678
    move/from16 v0, v28

    #@67a
    move-object/from16 v1, p0

    #@67c
    iput-boolean v0, v1, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->CheckATTACH:Z

    #@67e
    goto/16 :goto_145

    #@680
    .line 1504
    :cond_680
    move-object/from16 v0, p0

    #@682
    iget v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mDataConnectionState:I

    #@684
    move/from16 v28, v0

    #@686
    if-nez v28, :cond_145

    #@688
    move-object/from16 v0, p0

    #@68a
    iget v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mNewDataConnectionState:I

    #@68c
    move/from16 v28, v0

    #@68e
    if-eqz v28, :cond_145

    #@690
    .line 1506
    const/16 v28, 0x0

    #@692
    move/from16 v0, v28

    #@694
    move-object/from16 v1, p0

    #@696
    iput-boolean v0, v1, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->CheckATTACH:Z

    #@698
    goto/16 :goto_145

    #@69a
    .line 1514
    :cond_69a
    const/16 v28, 0x0

    #@69c
    goto/16 :goto_17e

    #@69e
    .line 1515
    :cond_69e
    const/16 v28, 0x0

    #@6a0
    goto/16 :goto_1ba

    #@6a2
    .line 1517
    :cond_6a2
    const/16 v28, 0x0

    #@6a4
    goto/16 :goto_1dc

    #@6a6
    .line 1576
    .restart local v26       #tcl:Landroid/telephony/cdma/CdmaCellLocation;
    .restart local v27       #tss:Landroid/telephony/ServiceState;
    :cond_6a6
    move-object/from16 v0, p0

    #@6a8
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@6aa
    move-object/from16 v28, v0

    #@6ac
    invoke-virtual/range {v28 .. v28}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@6af
    move-result-object v28

    #@6b0
    const v29, 0x10400d6

    #@6b3
    invoke-virtual/range {v28 .. v29}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@6b6
    move-result-object v28

    #@6b7
    invoke-virtual/range {v28 .. v28}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@6ba
    move-result-object v8

    #@6bb
    .restart local v8       #eriText:Ljava/lang/String;
    goto/16 :goto_355

    #@6bd
    .line 1585
    .restart local v5       #alertId:I
    :cond_6bd
    move-object/from16 v0, p0

    #@6bf
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@6c1
    move-object/from16 v28, v0

    #@6c3
    invoke-virtual/range {v28 .. v28}, Landroid/telephony/ServiceState;->getState()I

    #@6c6
    move-result v28

    #@6c7
    const/16 v29, 0x3

    #@6c9
    move/from16 v0, v28

    #@6cb
    move/from16 v1, v29

    #@6cd
    if-ne v0, v1, :cond_381

    #@6cf
    .line 1586
    const/4 v8, 0x0

    #@6d0
    goto/16 :goto_381

    #@6d2
    .line 1597
    :cond_6d2
    move-object/from16 v0, p0

    #@6d4
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@6d6
    move-object/from16 v28, v0

    #@6d8
    invoke-virtual/range {v28 .. v28}, Landroid/telephony/ServiceState;->getState()I

    #@6db
    move-result v28

    #@6dc
    if-nez v28, :cond_479

    #@6de
    .line 1600
    move-object/from16 v0, p0

    #@6e0
    iget-boolean v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->hasChangedSystemIDNetworkID:Z

    #@6e2
    move/from16 v28, v0

    #@6e4
    if-nez v28, :cond_6f6

    #@6e6
    move-object/from16 v0, p0

    #@6e8
    iget-boolean v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->hasChangedRoamingIndicator:Z

    #@6ea
    move/from16 v28, v0

    #@6ec
    if-nez v28, :cond_6f6

    #@6ee
    move-object/from16 v0, p0

    #@6f0
    iget-boolean v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->hasStateChanged:Z

    #@6f2
    move/from16 v28, v0

    #@6f4
    if-eqz v28, :cond_479

    #@6f6
    .line 1602
    :cond_6f6
    move-object/from16 v0, p0

    #@6f8
    invoke-virtual {v0, v5}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->PlayVZWERISound(I)V

    #@6fb
    goto/16 :goto_479

    #@6fd
    .line 1626
    .end local v5           #alertId:I
    .end local v8           #eriText:Ljava/lang/String;
    .restart local v24       #operatorNumeric:Ljava/lang/String;
    .restart local v25       #prevOperatorNumeric:Ljava/lang/String;
    :cond_6fd
    const-string v22, ""

    #@6ff
    .line 1627
    .local v22, isoCountryCode:Ljava/lang/String;
    const/16 v28, 0x0

    #@701
    const/16 v29, 0x3

    #@703
    move-object/from16 v0, v24

    #@705
    move/from16 v1, v28

    #@707
    move/from16 v2, v29

    #@709
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@70c
    move-result-object v23

    #@70d
    .line 1629
    .local v23, mcc:Ljava/lang/String;
    const/16 v28, 0x0

    #@70f
    const/16 v29, 0x3

    #@711
    :try_start_711
    move-object/from16 v0, v24

    #@713
    move/from16 v1, v28

    #@715
    move/from16 v2, v29

    #@717
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@71a
    move-result-object v28

    #@71b
    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@71e
    move-result v28

    #@71f
    invoke-static/range {v28 .. v28}, Lcom/android/internal/telephony/MccTable;->countryCodeForMcc(I)Ljava/lang/String;
    :try_end_722
    .catch Ljava/lang/NumberFormatException; {:try_start_711 .. :try_end_722} :catch_761
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_711 .. :try_end_722} :catch_77f

    #@722
    move-result-object v22

    #@723
    .line 1637
    :goto_723
    move-object/from16 v0, p0

    #@725
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@727
    move-object/from16 v28, v0

    #@729
    const-string v29, "gsm.operator.iso-country"

    #@72b
    move-object/from16 v0, v28

    #@72d
    move-object/from16 v1, v29

    #@72f
    move-object/from16 v2, v22

    #@731
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/cdma/CDMAPhone;->setSystemProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@734
    .line 1639
    const/16 v28, 0x1

    #@736
    move/from16 v0, v28

    #@738
    move-object/from16 v1, p0

    #@73a
    iput-boolean v0, v1, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mGotCountryCode:Z

    #@73c
    .line 1641
    move-object/from16 v0, p0

    #@73e
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@740
    move-object/from16 v28, v0

    #@742
    move-object/from16 v0, p0

    #@744
    iget-boolean v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mNeedFixZone:Z

    #@746
    move/from16 v29, v0

    #@748
    move-object/from16 v0, p0

    #@74a
    move-object/from16 v1, v28

    #@74c
    move-object/from16 v2, v24

    #@74e
    move-object/from16 v3, v25

    #@750
    move/from16 v4, v29

    #@752
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->shouldFixTimeZoneNow(Lcom/android/internal/telephony/PhoneBase;Ljava/lang/String;Ljava/lang/String;Z)Z

    #@755
    move-result v28

    #@756
    if-eqz v28, :cond_4e9

    #@758
    .line 1643
    move-object/from16 v0, p0

    #@75a
    move-object/from16 v1, v22

    #@75c
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->fixTimeZone(Ljava/lang/String;)V

    #@75f
    goto/16 :goto_4e9

    #@761
    .line 1631
    :catch_761
    move-exception v10

    #@762
    .line 1632
    .local v10, ex:Ljava/lang/NumberFormatException;
    new-instance v28, Ljava/lang/StringBuilder;

    #@764
    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    #@767
    const-string v29, "pollStateDone: countryCodeForMcc error"

    #@769
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76c
    move-result-object v28

    #@76d
    move-object/from16 v0, v28

    #@76f
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@772
    move-result-object v28

    #@773
    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@776
    move-result-object v28

    #@777
    move-object/from16 v0, p0

    #@779
    move-object/from16 v1, v28

    #@77b
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->loge(Ljava/lang/String;)V

    #@77e
    goto :goto_723

    #@77f
    .line 1633
    .end local v10           #ex:Ljava/lang/NumberFormatException;
    :catch_77f
    move-exception v10

    #@780
    .line 1634
    .local v10, ex:Ljava/lang/StringIndexOutOfBoundsException;
    new-instance v28, Ljava/lang/StringBuilder;

    #@782
    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    #@785
    const-string v29, "pollStateDone: countryCodeForMcc error"

    #@787
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78a
    move-result-object v28

    #@78b
    move-object/from16 v0, v28

    #@78d
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@790
    move-result-object v28

    #@791
    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@794
    move-result-object v28

    #@795
    move-object/from16 v0, p0

    #@797
    move-object/from16 v1, v28

    #@799
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->loge(Ljava/lang/String;)V

    #@79c
    goto :goto_723

    #@79d
    .line 1660
    .end local v10           #ex:Ljava/lang/StringIndexOutOfBoundsException;
    .end local v22           #isoCountryCode:Ljava/lang/String;
    .end local v23           #mcc:Ljava/lang/String;
    :catch_79d
    move-exception v7

    #@79e
    .line 1661
    .local v7, e:Ljava/lang/NumberFormatException;
    const-string v28, "CDMA"

    #@7a0
    const-string v29, "Error"

    #@7a2
    invoke-static/range {v28 .. v29}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7a5
    goto/16 :goto_559

    #@7a7
    .line 1667
    .end local v7           #e:Ljava/lang/NumberFormatException;
    :cond_7a7
    const-string v28, "false"

    #@7a9
    goto/16 :goto_56f

    #@7ab
    .line 1678
    .end local v24           #operatorNumeric:Ljava/lang/String;
    .end local v25           #prevOperatorNumeric:Ljava/lang/String;
    :cond_7ab
    move-object/from16 v0, p0

    #@7ad
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@7af
    move-object/from16 v28, v0

    #@7b1
    move-object/from16 v0, v28

    #@7b3
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@7b5
    move-object/from16 v28, v0

    #@7b7
    invoke-interface/range {v28 .. v28}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@7ba
    move-result-object v28

    #@7bb
    move-object/from16 v0, v28

    #@7bd
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_BUGFIX_SETUP_DATACALL_ON_UNKNOWN_TECH:Z

    #@7bf
    move/from16 v28, v0

    #@7c1
    if-eqz v28, :cond_595

    #@7c3
    move-object/from16 v0, p0

    #@7c5
    iget-boolean v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->CheckATTACH:Z

    #@7c7
    move/from16 v28, v0

    #@7c9
    const/16 v29, 0x1

    #@7cb
    move/from16 v0, v28

    #@7cd
    move/from16 v1, v29

    #@7cf
    if-ne v0, v1, :cond_595

    #@7d1
    move-object/from16 v0, p0

    #@7d3
    iget v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mRilRadioTechnology:I

    #@7d5
    move/from16 v28, v0

    #@7d7
    if-eqz v28, :cond_595

    #@7d9
    .line 1681
    const/16 v28, 0x0

    #@7db
    move/from16 v0, v28

    #@7dd
    move-object/from16 v1, p0

    #@7df
    iput-boolean v0, v1, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->CheckATTACH:Z

    #@7e1
    .line 1682
    new-instance v28, Ljava/lang/StringBuilder;

    #@7e3
    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    #@7e6
    const-string v29, "mAttachedRegistrants.notifyRegistrants() CheckATTACH :: "

    #@7e8
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7eb
    move-result-object v28

    #@7ec
    move-object/from16 v0, p0

    #@7ee
    iget-boolean v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->CheckATTACH:Z

    #@7f0
    move/from16 v29, v0

    #@7f2
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@7f5
    move-result-object v28

    #@7f6
    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7f9
    move-result-object v28

    #@7fa
    move-object/from16 v0, p0

    #@7fc
    move-object/from16 v1, v28

    #@7fe
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@801
    .line 1683
    move-object/from16 v0, p0

    #@803
    iget-object v0, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mAttachedRegistrants:Landroid/os/RegistrantList;

    #@805
    move-object/from16 v28, v0

    #@807
    invoke-virtual/range {v28 .. v28}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@80a
    goto/16 :goto_595
.end method

.method protected radioTechnologyToDataServiceState(I)I
    .registers 4
    .parameter "code"

    #@0
    .prologue
    .line 1786
    const/4 v0, 0x1

    #@1
    .line 1787
    .local v0, retVal:I
    packed-switch p1, :pswitch_data_c

    #@4
    .line 1803
    :pswitch_4
    const-string v1, "radioTechnologyToDataServiceState: Wrong radioTechnology code."

    #@6
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->loge(Ljava/lang/String;)V

    #@9
    .line 1806
    :goto_9
    :pswitch_9
    return v0

    #@a
    .line 1800
    :pswitch_a
    const/4 v0, 0x0

    #@b
    .line 1801
    goto :goto_9

    #@c
    .line 1787
    :pswitch_data_c
    .packed-switch 0x0
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_a
        :pswitch_a
        :pswitch_a
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_a
        :pswitch_a
    .end packed-switch
.end method

.method protected regCodeIsRoaming(I)Z
    .registers 3
    .parameter "code"

    #@0
    .prologue
    .line 1841
    const/4 v0, 0x5

    #@1
    if-ne v0, p1, :cond_5

    #@3
    const/4 v0, 0x1

    #@4
    :goto_4
    return v0

    #@5
    :cond_5
    const/4 v0, 0x0

    #@6
    goto :goto_4
.end method

.method protected regCodeToServiceState(I)I
    .registers 5
    .parameter "code"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x1

    #@2
    .line 1812
    packed-switch p1, :pswitch_data_20

    #@5
    .line 1825
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v2, "regCodeToServiceState: unexpected service state "

    #@c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->loge(Ljava/lang/String;)V

    #@1b
    .line 1826
    :goto_1b
    :pswitch_1b
    return v0

    #@1c
    :pswitch_1c
    move v0, v1

    #@1d
    .line 1816
    goto :goto_1b

    #@1e
    :pswitch_1e
    move v0, v1

    #@1f
    .line 1822
    goto :goto_1b

    #@20
    .line 1812
    :pswitch_data_20
    .packed-switch 0x0
        :pswitch_1b
        :pswitch_1c
        :pswitch_1b
        :pswitch_1b
        :pswitch_1b
        :pswitch_1e
    .end packed-switch
.end method

.method public registerForSubscriptionInfoReady(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 498
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 499
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->cdmaForSubscriptionInfoReadyRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 501
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isMinInfoReady()Z

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 502
    invoke-virtual {v0}, Landroid/os/Registrant;->notifyRegistrant()V

    #@13
    .line 504
    :cond_13
    return-void
.end method

.method protected setCdmaTechnology(I)V
    .registers 3
    .parameter "radioTech"

    #@0
    .prologue
    .line 971
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->radioTechnologyToDataServiceState(I)I

    #@3
    move-result v0

    #@4
    iput v0, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mNewDataConnectionState:I

    #@6
    .line 972
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@8
    invoke-virtual {v0, p1}, Landroid/telephony/ServiceState;->setRadioTechnology(I)V

    #@b
    .line 973
    iput p1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mNewRilRadioTechnology:I

    #@d
    .line 974
    return-void
.end method

.method public setIMSRegistate(Z)V
    .registers 7
    .parameter "Registate"

    #@0
    .prologue
    .line 2709
    const-string v2, "CDMA"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "[IMS_AFW][CdmaServiceStateTracker] setIMSRegistate - Registate : "

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v3

    #@15
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 2710
    iget-boolean v2, p0, Lcom/android/internal/telephony/ServiceStateTracker;->IMSRegiOnOff:Z

    #@1a
    if-eqz v2, :cond_53

    #@1c
    if-nez p1, :cond_53

    #@1e
    .line 2711
    const-string v2, "CDMA"

    #@20
    const-string v3, "[CdmaServiceStateTracker]IMSRegiOnOff == true && Registate == false"

    #@22
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 2712
    iget-boolean v2, p0, Lcom/android/internal/telephony/ServiceStateTracker;->alarmSwitch:Z

    #@27
    const/4 v3, 0x1

    #@28
    if-ne v2, v3, :cond_53

    #@2a
    .line 2713
    const-string v2, "CDMA"

    #@2c
    const-string v3, "[CdmaServiceStateTracker]alarmSwitch == true"

    #@2e
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@31
    .line 2714
    iput-boolean p1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->IMSRegiOnOff:Z

    #@33
    .line 2716
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@35
    invoke-virtual {v2}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@38
    move-result-object v1

    #@39
    .line 2717
    .local v1, mContext:Landroid/content/Context;
    const-string v2, "alarm"

    #@3b
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@3e
    move-result-object v0

    #@3f
    check-cast v0, Landroid/app/AlarmManager;

    #@41
    .line 2718
    .local v0, am:Landroid/app/AlarmManager;
    iget-object v2, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mRadioOffIntent:Landroid/app/PendingIntent;

    #@43
    invoke-virtual {v0, v2}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    #@46
    .line 2719
    const/4 v2, 0x0

    #@47
    iput-boolean v2, p0, Lcom/android/internal/telephony/ServiceStateTracker;->alarmSwitch:Z

    #@49
    .line 2721
    const/16 v2, 0x2c

    #@4b
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->obtainMessage(I)Landroid/os/Message;

    #@4e
    move-result-object v2

    #@4f
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->sendMessage(Landroid/os/Message;)Z

    #@52
    .line 2726
    .end local v0           #am:Landroid/app/AlarmManager;
    .end local v1           #mContext:Landroid/content/Context;
    :goto_52
    return-void

    #@53
    .line 2725
    :cond_53
    iput-boolean p1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->IMSRegiOnOff:Z

    #@55
    goto :goto_52
.end method

.method protected setPowerStateToDesired()V
    .registers 5

    #@0
    .prologue
    .line 870
    iget-boolean v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mDesiredPowerState:Z

    #@2
    if-eqz v1, :cond_16

    #@4
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@6
    invoke-interface {v1}, Lcom/android/internal/telephony/CommandsInterface;->getRadioState()Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@9
    move-result-object v1

    #@a
    sget-object v2, Lcom/android/internal/telephony/CommandsInterface$RadioState;->RADIO_OFF:Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@c
    if-ne v1, v2, :cond_16

    #@e
    .line 872
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@10
    const/4 v2, 0x1

    #@11
    const/4 v3, 0x0

    #@12
    invoke-interface {v1, v2, v3}, Lcom/android/internal/telephony/CommandsInterface;->setRadioPower(ZLandroid/os/Message;)V

    #@15
    .line 879
    :cond_15
    :goto_15
    return-void

    #@16
    .line 873
    :cond_16
    iget-boolean v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mDesiredPowerState:Z

    #@18
    if-nez v1, :cond_15

    #@1a
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@1c
    invoke-interface {v1}, Lcom/android/internal/telephony/CommandsInterface;->getRadioState()Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1}, Lcom/android/internal/telephony/CommandsInterface$RadioState;->isOn()Z

    #@23
    move-result v1

    #@24
    if-eqz v1, :cond_15

    #@26
    .line 874
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@28
    iget-object v0, v1, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@2a
    .line 877
    .local v0, dcTracker:Lcom/android/internal/telephony/DataConnectionTracker;
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->powerOffRadioSafely(Lcom/android/internal/telephony/DataConnectionTracker;)V

    #@2d
    goto :goto_15
.end method

.method protected setSignalStrengthDefaultValues()V
    .registers 3

    #@0
    .prologue
    .line 1337
    new-instance v0, Landroid/telephony/SignalStrength;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-direct {v0, v1}, Landroid/telephony/SignalStrength;-><init>(Z)V

    #@6
    iput-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mSignalStrength:Landroid/telephony/SignalStrength;

    #@8
    .line 1338
    return-void
.end method

.method public set_internetpdn_ipv6_blocked_by_ip6table(Z)V
    .registers 4
    .parameter "blocked"

    #@0
    .prologue
    .line 2558
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "set_internetpdn_ipv6_blocked_by_ip6table current :"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    sget-boolean v1, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->is_ehrpd_internetpdn_ipv6_blocked_by_ip6tables:Z

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, ", requested "

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v0

    #@1b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v0

    #@1f
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@22
    .line 2559
    sput-boolean p1, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->is_ehrpd_internetpdn_ipv6_blocked_by_ip6tables:Z

    #@24
    .line 2560
    return-void
.end method

.method public set_internetpdn_ipv6_blocked_iface(Ljava/lang/String;)V
    .registers 4
    .parameter "blocked_iface"

    #@0
    .prologue
    .line 2563
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "set_internetpdn_ipv6_blocked_iface current :"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    sget-object v1, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->ehrpd_ipv6_block_iface:Ljava/lang/String;

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, ", requested "

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v0

    #@1b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v0

    #@1f
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@22
    .line 2564
    sput-object p1, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->ehrpd_ipv6_block_iface:Ljava/lang/String;

    #@24
    .line 2565
    return-void
.end method

.method public unregisterForSubscriptionInfoReady(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 507
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->cdmaForSubscriptionInfoReadyRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 508
    return-void
.end method

.method protected updateOtaspState()V
    .registers 5

    #@0
    .prologue
    .line 2478
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->getOtasp()I

    #@3
    move-result v1

    #@4
    .line 2479
    .local v1, otaspMode:I
    iget v0, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mCurrentOtaspMode:I

    #@6
    .line 2480
    .local v0, oldOtaspMode:I
    iput v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mCurrentOtaspMode:I

    #@8
    .line 2483
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->cdmaForSubscriptionInfoReadyRegistrants:Landroid/os/RegistrantList;

    #@a
    if-eqz v2, :cond_16

    #@c
    .line 2484
    const-string v2, "CDMA_SUBSCRIPTION: call notifyRegistrants()"

    #@e
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@11
    .line 2485
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->cdmaForSubscriptionInfoReadyRegistrants:Landroid/os/RegistrantList;

    #@13
    invoke-virtual {v2}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@16
    .line 2487
    :cond_16
    iget v2, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mCurrentOtaspMode:I

    #@18
    if-eq v0, v2, :cond_43

    #@1a
    .line 2489
    new-instance v2, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v3, "CDMA_SUBSCRIPTION: call notifyOtaspChanged old otaspMode="

    #@21
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v2

    #@29
    const-string v3, " new otaspMode="

    #@2b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    iget v3, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mCurrentOtaspMode:I

    #@31
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@34
    move-result-object v2

    #@35
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v2

    #@39
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@3c
    .line 2492
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@3e
    iget v3, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mCurrentOtaspMode:I

    #@40
    invoke-virtual {v2, v3}, Lcom/android/internal/telephony/cdma/CDMAPhone;->notifyOtaspChanged(I)V

    #@43
    .line 2494
    :cond_43
    return-void
.end method

.method protected updateSpnDisplay()V
    .registers 10

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 884
    iget-object v6, p0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@4
    invoke-virtual {v6}, Landroid/telephony/ServiceState;->getOperatorAlphaLong()Ljava/lang/String;

    #@7
    move-result-object v1

    #@8
    .line 887
    .local v1, plmn:Ljava/lang/String;
    const-string v6, "SPR"

    #@a
    invoke-static {v6}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@d
    move-result v6

    #@e
    if-eqz v6, :cond_1f

    #@10
    .line 888
    const-string v6, "Chameleon"

    #@12
    invoke-static {v1, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@15
    move-result v6

    #@16
    if-eqz v6, :cond_1f

    #@18
    .line 890
    const-string v1, "LG"

    #@1a
    .line 891
    iget-object v6, p0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@1c
    invoke-virtual {v6, v1}, Landroid/telephony/ServiceState;->setOperatorAlphaLong(Ljava/lang/String;)V

    #@1f
    .line 896
    :cond_1f
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mCurPlmn:Ljava/lang/String;

    #@21
    invoke-static {v1, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@24
    move-result v6

    #@25
    if-nez v6, :cond_6b

    #@27
    .line 901
    if-eqz v1, :cond_cc

    #@29
    move v3, v4

    #@2a
    .line 903
    .local v3, showPlmn:Z
    :goto_2a
    const-string v6, "updateSpnDisplay: changed sending intent showPlmn=\'%b\' plmn=\'%s\'"

    #@2c
    const/4 v7, 0x2

    #@2d
    new-array v7, v7, [Ljava/lang/Object;

    #@2f
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@32
    move-result-object v8

    #@33
    aput-object v8, v7, v5

    #@35
    aput-object v1, v7, v4

    #@37
    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@3a
    move-result-object v6

    #@3b
    invoke-virtual {p0, v6}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@3e
    .line 906
    new-instance v0, Landroid/content/Intent;

    #@40
    const-string v6, "android.provider.Telephony.SPN_STRINGS_UPDATED"

    #@42
    invoke-direct {v0, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@45
    .line 907
    .local v0, intent:Landroid/content/Intent;
    const/high16 v6, 0x2000

    #@47
    invoke-virtual {v0, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@4a
    .line 908
    const-string v6, "showSpn"

    #@4c
    invoke-virtual {v0, v6, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@4f
    .line 909
    const-string v6, "spn"

    #@51
    const-string v7, ""

    #@53
    invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@56
    .line 910
    const-string v6, "showPlmn"

    #@58
    invoke-virtual {v0, v6, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@5b
    .line 911
    const-string v6, "plmn"

    #@5d
    invoke-virtual {v0, v6, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@60
    .line 912
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@62
    invoke-virtual {v6}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@65
    move-result-object v6

    #@66
    sget-object v7, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@68
    invoke-virtual {v6, v0, v7}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@6b
    .line 915
    .end local v0           #intent:Landroid/content/Intent;
    .end local v3           #showPlmn:Z
    :cond_6b
    const-string v6, "VZW"

    #@6d
    invoke-static {v6}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@70
    move-result v6

    #@71
    if-eqz v6, :cond_9d

    #@73
    .line 916
    iget-object v6, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@75
    invoke-interface {v6}, Lcom/android/internal/telephony/CommandsInterface;->getRadioState()Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@78
    move-result-object v6

    #@79
    invoke-virtual {v6}, Lcom/android/internal/telephony/CommandsInterface$RadioState;->isOn()Z

    #@7c
    move-result v6

    #@7d
    if-nez v6, :cond_9d

    #@7f
    .line 917
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->cr:Landroid/content/ContentResolver;

    #@81
    const-string v7, "apn2_disable"

    #@83
    invoke-static {v6, v7, v5}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@86
    move-result v6

    #@87
    if-ne v6, v4, :cond_9d

    #@89
    .line 918
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@8c
    move-result-object v6

    #@8d
    const v7, 0x1040325

    #@90
    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@93
    move-result-object v6

    #@94
    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@97
    move-result-object v2

    #@98
    .line 920
    .local v2, plmnEco:Ljava/lang/String;
    const-string v6, ""

    #@9a
    invoke-direct {p0, v2, v4, v6, v5}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->updateNetworkIndicator(Ljava/lang/String;ZLjava/lang/String;Z)V

    #@9d
    .line 927
    .end local v2           #plmnEco:Ljava/lang/String;
    :cond_9d
    const/4 v4, 0x0

    #@9e
    const-string v5, "vzw_eri"

    #@a0
    invoke-static {v4, v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@a3
    move-result v4

    #@a4
    if-eqz v4, :cond_c9

    #@a6
    .line 928
    iget-object v4, p0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@a8
    invoke-virtual {v4}, Landroid/telephony/ServiceState;->getCdmaRoamingIndicator()I

    #@ab
    move-result v4

    #@ac
    const/16 v5, 0x63

    #@ae
    if-ne v4, v5, :cond_c9

    #@b0
    .line 929
    new-instance v4, Ljava/lang/StringBuilder;

    #@b2
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@b5
    const-string v5, "updateFemtoCellIndicator() : plmn = "

    #@b7
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v4

    #@bb
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@be
    move-result-object v4

    #@bf
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c2
    move-result-object v4

    #@c3
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->log(Ljava/lang/String;)V

    #@c6
    .line 930
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->updateFemtoCellIndicator()V

    #@c9
    .line 934
    :cond_c9
    iput-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->mCurPlmn:Ljava/lang/String;

    #@cb
    .line 935
    return-void

    #@cc
    :cond_cc
    move v3, v5

    #@cd
    .line 901
    goto/16 :goto_2a
.end method
