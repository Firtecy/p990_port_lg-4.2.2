.class public Lcom/android/internal/telephony/cdma/CDMAPhone;
.super Lcom/android/internal/telephony/PhoneBase;
.source "CDMAPhone.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/cdma/CDMAPhone$3;
    }
.end annotation


# static fields
.field static final CANCEL_ECM_TIMER:I = 0x1

.field private static final DBG:Z = true

.field private static final DEFAULT_ECM_EXIT_TIMER_VALUE:I = 0x493e0

.field private static final INVALID_SYSTEM_SELECTION_CODE:I = -0x1

.field private static final IS683A_FEATURE_CODE:Ljava/lang/String; = "*228"

.field private static final IS683A_FEATURE_CODE_NUM_DIGITS:I = 0x4

.field private static final IS683A_SYS_SEL_CODE_NUM_DIGITS:I = 0x2

.field private static final IS683A_SYS_SEL_CODE_OFFSET:I = 0x4

.field private static final IS683_CONST_1900MHZ_A_BLOCK:I = 0x2

.field private static final IS683_CONST_1900MHZ_B_BLOCK:I = 0x3

.field private static final IS683_CONST_1900MHZ_C_BLOCK:I = 0x4

.field private static final IS683_CONST_1900MHZ_D_BLOCK:I = 0x5

.field private static final IS683_CONST_1900MHZ_E_BLOCK:I = 0x6

.field private static final IS683_CONST_1900MHZ_F_BLOCK:I = 0x7

.field private static final IS683_CONST_800MHZ_A_BAND:I = 0x0

.field private static final IS683_CONST_800MHZ_B_BAND:I = 0x1

.field private static final IS683_CONST_BC10_ABAND:I = 0x1e

.field private static final IS683_CONST_BC10_BBAND:I = 0x1f

.field private static final IS683_CONST_BC10_CBAND:I = 0x20

.field private static final IS683_CONST_BC10_DBAND:I = 0x21

.field private static final IS683_CONST_BC10_EBAND:I = 0x22

.field static final LOG_TAG:Ljava/lang/String; = "CDMA"

.field protected static PROPERTY_CDMA_HOME_OPERATOR_NUMERIC:Ljava/lang/String; = null

.field static final RESTART_ECM_TIMER:I = 0x0

.field private static final VDBG:Z = false

.field protected static final VM_NUMBER_CDMA:Ljava/lang/String; = "vm_number_key_cdma"

.field private static pOtaSpNumSchema:Ljava/util/regex/Pattern;


# instance fields
.field private SimStateReceiver:Landroid/content/BroadcastReceiver;

.field gfUtils:Lcom/android/internal/telephony/gfit/GfitUtils;

.field mAssistDialPhoneNumberUtils:Landroid/telephony/AssistDialPhoneNumberUtils;

.field protected mCT:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

.field protected mCarrierOtaSpNumSchema:Ljava/lang/String;

.field protected mCdmaSSM:Lcom/android/internal/telephony/cdma/CdmaSubscriptionSourceManager;

.field protected mCdmaSubscriptionSource:I

.field private mEcmExitRespRegistrant:Landroid/os/Registrant;

.field private final mEcmTimerResetRegistrants:Landroid/os/RegistrantList;

.field private final mEriFileLoadedRegistrants:Landroid/os/RegistrantList;

.field protected mEriManager:Lcom/android/internal/telephony/cdma/EriManager;

.field protected mEsn:Ljava/lang/String;

.field private mExitEcmRunnable:Ljava/lang/Runnable;

.field protected mImei:Ljava/lang/String;

.field protected mImeiSv:Ljava/lang/String;

.field protected mIsPhoneInEcmExitDelayState:Z

.field protected mIsPhoneInEcmState:Z

.field protected mMeid:Ljava/lang/String;

.field mPendingMmis:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/telephony/cdma/CdmaMmiCode;",
            ">;"
        }
    .end annotation
.end field

.field mPostDialHandler:Landroid/os/Registrant;

.field protected mRuimCard:Lcom/android/internal/telephony/uicc/UiccCard;

.field protected mRuimPhoneBookInterfaceManager:Lcom/android/internal/telephony/cdma/RuimPhoneBookInterfaceManager;

.field protected mSST:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

.field protected mSubInfo:Lcom/android/internal/telephony/PhoneSubInfo;

.field private mVmNumber:Ljava/lang/String;

.field protected mWakeLock:Landroid/os/PowerManager$WakeLock;

.field protected retryNum:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 195
    const-string v0, "ro.cdma.home.operator.numeric"

    #@2
    sput-object v0, Lcom/android/internal/telephony/cdma/CDMAPhone;->PROPERTY_CDMA_HOME_OPERATOR_NUMERIC:Ljava/lang/String;

    #@4
    .line 1849
    const-string v0, "[,\\s]+"

    #@6
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@9
    move-result-object v0

    #@a
    sput-object v0, Lcom/android/internal/telephony/cdma/CDMAPhone;->pOtaSpNumSchema:Ljava/util/regex/Pattern;

    #@c
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;Lcom/android/internal/telephony/PhoneNotifier;)V
    .registers 7
    .parameter "context"
    .parameter "ci"
    .parameter "notifier"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 208
    invoke-direct {p0, p3, p1, p2, v1}, Lcom/android/internal/telephony/PhoneBase;-><init>(Lcom/android/internal/telephony/PhoneNotifier;Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;Z)V

    #@5
    .line 144
    iput-object v2, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mVmNumber:Ljava/lang/String;

    #@7
    .line 153
    new-instance v0, Ljava/util/ArrayList;

    #@9
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@c
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mPendingMmis:Ljava/util/ArrayList;

    #@e
    .line 155
    const/4 v0, -0x1

    #@f
    iput v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mCdmaSubscriptionSource:I

    #@11
    .line 159
    iput-object v2, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mRuimCard:Lcom/android/internal/telephony/uicc/UiccCard;

    #@13
    .line 165
    new-instance v0, Landroid/os/RegistrantList;

    #@15
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@18
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mEriFileLoadedRegistrants:Landroid/os/RegistrantList;

    #@1a
    .line 168
    new-instance v0, Landroid/os/RegistrantList;

    #@1c
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@1f
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mEcmTimerResetRegistrants:Landroid/os/RegistrantList;

    #@21
    .line 186
    new-instance v0, Lcom/android/internal/telephony/cdma/CDMAPhone$1;

    #@23
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/cdma/CDMAPhone$1;-><init>(Lcom/android/internal/telephony/cdma/CDMAPhone;)V

    #@26
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mExitEcmRunnable:Ljava/lang/Runnable;

    #@28
    .line 199
    iput v1, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->retryNum:I

    #@2a
    .line 203
    new-instance v0, Landroid/telephony/AssistDialPhoneNumberUtils;

    #@2c
    invoke-direct {v0}, Landroid/telephony/AssistDialPhoneNumberUtils;-><init>()V

    #@2f
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mAssistDialPhoneNumberUtils:Landroid/telephony/AssistDialPhoneNumberUtils;

    #@31
    .line 361
    new-instance v0, Lcom/android/internal/telephony/cdma/CDMAPhone$2;

    #@33
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/cdma/CDMAPhone$2;-><init>(Lcom/android/internal/telephony/cdma/CDMAPhone;)V

    #@36
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->SimStateReceiver:Landroid/content/BroadcastReceiver;

    #@38
    .line 209
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->initSstIcc()V

    #@3b
    .line 210
    invoke-virtual {p0, p1, p3}, Lcom/android/internal/telephony/cdma/CDMAPhone;->init(Landroid/content/Context;Lcom/android/internal/telephony/PhoneNotifier;)V

    #@3e
    .line 211
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;Lcom/android/internal/telephony/PhoneNotifier;Z)V
    .registers 7
    .parameter "context"
    .parameter "ci"
    .parameter "notifier"
    .parameter "unitTestMode"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 215
    invoke-direct {p0, p3, p1, p2, p4}, Lcom/android/internal/telephony/PhoneBase;-><init>(Lcom/android/internal/telephony/PhoneNotifier;Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;Z)V

    #@4
    .line 144
    iput-object v1, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mVmNumber:Ljava/lang/String;

    #@6
    .line 153
    new-instance v0, Ljava/util/ArrayList;

    #@8
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@b
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mPendingMmis:Ljava/util/ArrayList;

    #@d
    .line 155
    const/4 v0, -0x1

    #@e
    iput v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mCdmaSubscriptionSource:I

    #@10
    .line 159
    iput-object v1, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mRuimCard:Lcom/android/internal/telephony/uicc/UiccCard;

    #@12
    .line 165
    new-instance v0, Landroid/os/RegistrantList;

    #@14
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@17
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mEriFileLoadedRegistrants:Landroid/os/RegistrantList;

    #@19
    .line 168
    new-instance v0, Landroid/os/RegistrantList;

    #@1b
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@1e
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mEcmTimerResetRegistrants:Landroid/os/RegistrantList;

    #@20
    .line 186
    new-instance v0, Lcom/android/internal/telephony/cdma/CDMAPhone$1;

    #@22
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/cdma/CDMAPhone$1;-><init>(Lcom/android/internal/telephony/cdma/CDMAPhone;)V

    #@25
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mExitEcmRunnable:Ljava/lang/Runnable;

    #@27
    .line 199
    const/4 v0, 0x0

    #@28
    iput v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->retryNum:I

    #@2a
    .line 203
    new-instance v0, Landroid/telephony/AssistDialPhoneNumberUtils;

    #@2c
    invoke-direct {v0}, Landroid/telephony/AssistDialPhoneNumberUtils;-><init>()V

    #@2f
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mAssistDialPhoneNumberUtils:Landroid/telephony/AssistDialPhoneNumberUtils;

    #@31
    .line 361
    new-instance v0, Lcom/android/internal/telephony/cdma/CDMAPhone$2;

    #@33
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/cdma/CDMAPhone$2;-><init>(Lcom/android/internal/telephony/cdma/CDMAPhone;)V

    #@36
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->SimStateReceiver:Landroid/content/BroadcastReceiver;

    #@38
    .line 216
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->initSstIcc()V

    #@3b
    .line 217
    invoke-virtual {p0, p1, p3}, Lcom/android/internal/telephony/cdma/CDMAPhone;->init(Landroid/content/Context;Lcom/android/internal/telephony/PhoneNotifier;)V

    #@3e
    .line 218
    return-void
.end method

.method static synthetic access$000(Lcom/android/internal/telephony/cdma/CDMAPhone;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 135
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/internal/telephony/cdma/CDMAPhone;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 135
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/internal/telephony/cdma/CDMAPhone;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 135
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$1100(Lcom/android/internal/telephony/cdma/CDMAPhone;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 135
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$1200(Lcom/android/internal/telephony/cdma/CDMAPhone;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 135
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$1300(Lcom/android/internal/telephony/cdma/CDMAPhone;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 135
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Lcom/android/internal/telephony/cdma/CDMAPhone;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 135
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/android/internal/telephony/cdma/CDMAPhone;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 135
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Lcom/android/internal/telephony/cdma/CDMAPhone;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 135
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Lcom/android/internal/telephony/cdma/CDMAPhone;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 135
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$600(Lcom/android/internal/telephony/cdma/CDMAPhone;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 135
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Lcom/android/internal/telephony/cdma/CDMAPhone;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 135
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$800(Lcom/android/internal/telephony/cdma/CDMAPhone;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 135
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$900(Lcom/android/internal/telephony/cdma/CDMAPhone;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 135
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method private static checkOtaSpNumBasedOnSysSelCode(I[Ljava/lang/String;)Z
    .registers 10
    .parameter "sysSelCodeInt"
    .parameter "sch"

    #@0
    .prologue
    .line 1821
    const/4 v2, 0x0

    #@1
    .line 1824
    .local v2, isOtaSpNum:Z
    const/4 v6, 0x1

    #@2
    :try_start_2
    aget-object v6, p1, v6

    #@4
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@7
    move-result v5

    #@8
    .line 1825
    .local v5, selRc:I
    const/4 v1, 0x0

    #@9
    .local v1, i:I
    :goto_9
    if-ge v1, v5, :cond_3c

    #@b
    .line 1827
    mul-int/lit8 v6, v1, 0x2

    #@d
    add-int/lit8 v6, v6, 0x2

    #@f
    aget-object v6, p1, v6

    #@11
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@14
    move-result v6

    #@15
    if-nez v6, :cond_3d

    #@17
    mul-int/lit8 v6, v1, 0x2

    #@19
    add-int/lit8 v6, v6, 0x3

    #@1b
    aget-object v6, p1, v6

    #@1d
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@20
    move-result v6

    #@21
    if-nez v6, :cond_3d

    #@23
    .line 1828
    mul-int/lit8 v6, v1, 0x2

    #@25
    add-int/lit8 v6, v6, 0x2

    #@27
    aget-object v6, p1, v6

    #@29
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@2c
    move-result v4

    #@2d
    .line 1829
    .local v4, selMin:I
    mul-int/lit8 v6, v1, 0x2

    #@2f
    add-int/lit8 v6, v6, 0x3

    #@31
    aget-object v6, p1, v6

    #@33
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_36
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_36} :catch_40

    #@36
    move-result v3

    #@37
    .line 1833
    .local v3, selMax:I
    if-lt p0, v4, :cond_3d

    #@39
    if-gt p0, v3, :cond_3d

    #@3b
    .line 1834
    const/4 v2, 0x1

    #@3c
    .line 1844
    .end local v1           #i:I
    .end local v3           #selMax:I
    .end local v4           #selMin:I
    .end local v5           #selRc:I
    :cond_3c
    :goto_3c
    return v2

    #@3d
    .line 1825
    .restart local v1       #i:I
    .restart local v5       #selRc:I
    :cond_3d
    add-int/lit8 v1, v1, 0x1

    #@3f
    goto :goto_9

    #@40
    .line 1839
    .end local v1           #i:I
    .end local v5           #selRc:I
    :catch_40
    move-exception v0

    #@41
    .line 1842
    .local v0, ex:Ljava/lang/NumberFormatException;
    const-string v6, "CDMA"

    #@43
    const-string v7, "checkOtaSpNumBasedOnSysSelCode, error"

    #@45
    invoke-static {v6, v7, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@48
    goto :goto_3c
.end method

.method private static extractSelCodeFromOtaSpNum(Ljava/lang/String;)I
    .registers 9
    .parameter "dialStr"

    #@0
    .prologue
    const/4 v7, 0x6

    #@1
    .line 1790
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@4
    move-result v0

    #@5
    .line 1791
    .local v0, dialStrLen:I
    const/4 v2, -0x1

    #@6
    .line 1795
    .local v2, sysSelCodeInt:I
    const/4 v3, 0x0

    #@7
    :try_start_7
    const-string v4, "*228"

    #@9
    const/4 v5, 0x0

    #@a
    const/4 v6, 0x4

    #@b
    invoke-virtual {p0, v3, v4, v5, v6}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    #@e
    move-result v3

    #@f
    if-eqz v3, :cond_1d

    #@11
    if-lt v0, v7, :cond_1d

    #@13
    .line 1801
    const/4 v3, 0x4

    #@14
    const/4 v4, 0x6

    #@15
    invoke-virtual {p0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@18
    move-result-object v3

    #@19
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1c
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_1c} :catch_36

    #@1c
    move-result v2

    #@1d
    .line 1810
    :cond_1d
    :goto_1d
    const-string v3, "CDMA"

    #@1f
    new-instance v4, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v5, "extractSelCodeFromOtaSpNum "

    #@26
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v4

    #@2a
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v4

    #@2e
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v4

    #@32
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@35
    .line 1811
    return v2

    #@36
    .line 1805
    :catch_36
    move-exception v1

    #@37
    .line 1806
    .local v1, ex:Ljava/lang/Exception;
    const/4 v2, -0x1

    #@38
    goto :goto_1d
.end method

.method private getStoredVoiceMessageCount()I
    .registers 4

    #@0
    .prologue
    .line 1150
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@2
    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    #@5
    move-result-object v0

    #@6
    .line 1151
    .local v0, sp:Landroid/content/SharedPreferences;
    const-string v1, "vm_count_key"

    #@8
    const/4 v2, 0x0

    #@9
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    #@c
    move-result v1

    #@d
    return v1
.end method

.method private handleCdmaSubscriptionSource(I)V
    .registers 3
    .parameter "newSubscriptionSource"

    #@0
    .prologue
    .line 1650
    iget v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mCdmaSubscriptionSource:I

    #@2
    if-eq p1, v0, :cond_12

    #@4
    .line 1651
    iput p1, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mCdmaSubscriptionSource:I

    #@6
    .line 1652
    const/4 v0, 0x1

    #@7
    if-ne p1, v0, :cond_12

    #@9
    .line 1654
    const/16 v0, 0x17

    #@b
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->obtainMessage(I)Landroid/os/Message;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->sendMessage(Landroid/os/Message;)Z

    #@12
    .line 1657
    :cond_12
    return-void
.end method

.method private handleEnterEmergencyCallbackMode(J)V
    .registers 6
    .parameter "delayInMillis"

    #@0
    .prologue
    .line 1317
    iget-boolean v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mIsPhoneInEcmState:Z

    #@2
    const/4 v1, 0x1

    #@3
    if-ne v0, v1, :cond_27

    #@5
    .line 1318
    const-string v0, "CDMA"

    #@7
    new-instance v1, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v2, "handleEnterEmergencyCallbackMode, delayInMillis= "

    #@e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v1

    #@1a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    .line 1320
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mExitEcmRunnable:Ljava/lang/Runnable;

    #@1f
    invoke-virtual {p0, v0, p1, p2}, Lcom/android/internal/telephony/cdma/CDMAPhone;->postDelayed(Ljava/lang/Runnable;J)Z

    #@22
    .line 1321
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@24
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@27
    .line 1323
    :cond_27
    return-void
.end method

.method private handleEnterEmergencyCallbackMode(Landroid/os/Message;)V
    .registers 7
    .parameter "msg"

    #@0
    .prologue
    .line 1288
    const-string v2, "CDMA"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "handleEnterEmergencyCallbackMode,mIsPhoneInEcmState= "

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    iget-boolean v4, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mIsPhoneInEcmState:Z

    #@f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@12
    move-result-object v3

    #@13
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v3

    #@17
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 1292
    iget-boolean v2, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mIsPhoneInEcmState:Z

    #@1c
    if-nez v2, :cond_4d

    #@1e
    .line 1293
    const/4 v2, 0x1

    #@1f
    iput-boolean v2, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mIsPhoneInEcmState:Z

    #@21
    .line 1295
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->sendEmergencyCallbackModeChange()V

    #@24
    .line 1296
    const-string v2, "ril.cdma.inecmmode"

    #@26
    const-string v3, "true"

    #@28
    invoke-virtual {p0, v2, v3}, Lcom/android/internal/telephony/cdma/CDMAPhone;->setSystemProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@2b
    .line 1300
    const-string v2, "ro.cdma.ecmexittimer"

    #@2d
    const-wide/32 v3, 0x493e0

    #@30
    invoke-static {v2, v3, v4}, Landroid/os/SystemProperties;->getLong(Ljava/lang/String;J)J

    #@33
    move-result-wide v0

    #@34
    .line 1302
    .local v0, delayInMillis:J
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mExitEcmRunnable:Ljava/lang/Runnable;

    #@36
    invoke-virtual {p0, v2, v0, v1}, Lcom/android/internal/telephony/cdma/CDMAPhone;->postDelayed(Ljava/lang/Runnable;J)Z

    #@39
    .line 1305
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@3c
    move-result-object v2

    #@3d
    const-string v3, "support_emergency_callback_mode_for_gsm"

    #@3f
    invoke-static {v2, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@42
    move-result v2

    #@43
    if-eqz v2, :cond_48

    #@45
    .line 1306
    invoke-static {v0, v1}, Lcom/android/internal/telephony/cdma/CDMAPhone;->setCurrentTimeForEcm(J)V

    #@48
    .line 1311
    :cond_48
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@4a
    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@4d
    .line 1313
    .end local v0           #delayInMillis:J
    :cond_4d
    return-void
.end method

.method private handleExitEmergencyCallbackMode(Landroid/os/Message;)V
    .registers 9
    .parameter "msg"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 1327
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@4
    check-cast v1, Landroid/os/AsyncResult;

    #@6
    .line 1329
    .local v1, ar:Landroid/os/AsyncResult;
    const-string v4, "CDMA"

    #@8
    new-instance v5, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v6, "handleExitEmergencyCallbackMode,ar.exception , mIsPhoneInEcmState "

    #@f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v5

    #@13
    iget-object v6, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@15
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v5

    #@19
    iget-boolean v6, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mIsPhoneInEcmState:Z

    #@1b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v5

    #@1f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v5

    #@23
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 1333
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mExitEcmRunnable:Ljava/lang/Runnable;

    #@28
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/cdma/CDMAPhone;->removeCallbacks(Ljava/lang/Runnable;)V

    #@2b
    .line 1335
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mEcmExitRespRegistrant:Landroid/os/Registrant;

    #@2d
    if-eqz v4, :cond_34

    #@2f
    .line 1336
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mEcmExitRespRegistrant:Landroid/os/Registrant;

    #@31
    invoke-virtual {v4, v1}, Landroid/os/Registrant;->notifyRegistrant(Landroid/os/AsyncResult;)V

    #@34
    .line 1339
    :cond_34
    iget-object v4, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@36
    if-nez v4, :cond_a8

    #@38
    .line 1340
    iget-boolean v4, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mIsPhoneInEcmState:Z

    #@3a
    if-eqz v4, :cond_a0

    #@3c
    .line 1341
    iput-boolean v3, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mIsPhoneInEcmState:Z

    #@3e
    .line 1343
    iput-boolean v3, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mIsPhoneInEcmExitDelayState:Z

    #@40
    .line 1345
    const-string v4, "ril.cdma.inecmmode"

    #@42
    const-string v5, "false"

    #@44
    invoke-virtual {p0, v4, v5}, Lcom/android/internal/telephony/cdma/CDMAPhone;->setSystemProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@47
    .line 1348
    const/4 v4, 0x0

    #@48
    const-string v5, "support_network_change_auto_retry"

    #@4a
    invoke-static {v4, v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@4d
    move-result v4

    #@4e
    if-eqz v4, :cond_5d

    #@50
    .line 1349
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@53
    move-result-object v4

    #@54
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@57
    move-result-object v4

    #@58
    const-string v5, "network_change_auto_retry"

    #@5a
    invoke-static {v4, v5, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@5d
    .line 1355
    :cond_5d
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@60
    move-result-object v4

    #@61
    const-string v5, "support_emergency_callback_mode_for_gsm"

    #@63
    invoke-static {v4, v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@66
    move-result v4

    #@67
    if-eqz v4, :cond_6c

    #@69
    .line 1356
    invoke-static {}, Lcom/android/internal/telephony/cdma/CDMAPhone;->resetEcmExitTime()V

    #@6c
    .line 1361
    :cond_6c
    const-string v4, "VZW"

    #@6e
    invoke-static {v4}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@71
    move-result v4

    #@72
    if-eqz v4, :cond_a0

    #@74
    .line 1362
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@77
    move-result-object v4

    #@78
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7b
    move-result-object v4

    #@7c
    const-string v5, "apn2_disable"

    #@7e
    invoke-static {v4, v5, v3}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@81
    move-result v4

    #@82
    if-ne v4, v2, :cond_a9

    #@84
    move v0, v2

    #@85
    .line 1366
    .local v0, apn2Disabled:Z
    :goto_85
    if-eqz v0, :cond_a0

    #@87
    .line 1371
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getState()Lcom/android/internal/telephony/PhoneConstants$State;

    #@8a
    move-result-object v4

    #@8b
    sget-object v5, Lcom/android/internal/telephony/PhoneConstants$State;->IDLE:Lcom/android/internal/telephony/PhoneConstants$State;

    #@8d
    if-eq v4, v5, :cond_9d

    #@8f
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getState()Lcom/android/internal/telephony/PhoneConstants$State;

    #@92
    move-result-object v4

    #@93
    sget-object v5, Lcom/android/internal/telephony/PhoneConstants$State;->IDLE:Lcom/android/internal/telephony/PhoneConstants$State;

    #@95
    if-eq v4, v5, :cond_a0

    #@97
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->isInEmergencyCall()Z

    #@9a
    move-result v4

    #@9b
    if-nez v4, :cond_a0

    #@9d
    .line 1373
    :cond_9d
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/cdma/CDMAPhone;->setRadioPower(Z)V

    #@a0
    .line 1380
    .end local v0           #apn2Disabled:Z
    :cond_a0
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->sendEmergencyCallbackModeChange()V

    #@a3
    .line 1382
    iget-object v3, p0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@a5
    invoke-virtual {v3, v2}, Lcom/android/internal/telephony/DataConnectionTracker;->setInternalDataEnabled(Z)Z

    #@a8
    .line 1384
    :cond_a8
    return-void

    #@a9
    :cond_a9
    move v0, v3

    #@aa
    .line 1362
    goto :goto_85
.end method

.method private isCarrierOtaSpNum(Ljava/lang/String;)Z
    .registers 14
    .parameter "dialStr"

    #@0
    .prologue
    const/4 v11, -0x1

    #@1
    const/4 v10, 0x0

    #@2
    .line 1870
    const/4 v2, 0x0

    #@3
    .line 1871
    .local v2, isOtaSpNum:Z
    invoke-static {p1}, Lcom/android/internal/telephony/cdma/CDMAPhone;->extractSelCodeFromOtaSpNum(Ljava/lang/String;)I

    #@6
    move-result v6

    #@7
    .line 1872
    .local v6, sysSelCodeInt:I
    if-ne v6, v11, :cond_b

    #@9
    move v3, v2

    #@a
    .line 1915
    .end local v2           #isOtaSpNum:Z
    .local v3, isOtaSpNum:I
    :goto_a
    return v3

    #@b
    .line 1876
    .end local v3           #isOtaSpNum:I
    .restart local v2       #isOtaSpNum:Z
    :cond_b
    iget-object v7, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mCarrierOtaSpNumSchema:Ljava/lang/String;

    #@d
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@10
    move-result v7

    #@11
    if-nez v7, :cond_c7

    #@13
    .line 1877
    sget-object v7, Lcom/android/internal/telephony/cdma/CDMAPhone;->pOtaSpNumSchema:Ljava/util/regex/Pattern;

    #@15
    iget-object v8, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mCarrierOtaSpNumSchema:Ljava/lang/String;

    #@17
    invoke-virtual {v7, v8}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    #@1a
    move-result-object v4

    #@1b
    .line 1879
    .local v4, m:Ljava/util/regex/Matcher;
    const-string v7, "CDMA"

    #@1d
    new-instance v8, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    const-string v9, "isCarrierOtaSpNum,schema"

    #@24
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v8

    #@28
    iget-object v9, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mCarrierOtaSpNumSchema:Ljava/lang/String;

    #@2a
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v8

    #@2e
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v8

    #@32
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@35
    .line 1882
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->find()Z

    #@38
    move-result v7

    #@39
    if-eqz v7, :cond_ac

    #@3b
    .line 1883
    sget-object v7, Lcom/android/internal/telephony/cdma/CDMAPhone;->pOtaSpNumSchema:Ljava/util/regex/Pattern;

    #@3d
    iget-object v8, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mCarrierOtaSpNumSchema:Ljava/lang/String;

    #@3f
    invoke-virtual {v7, v8}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;)[Ljava/lang/String;

    #@42
    move-result-object v5

    #@43
    .line 1885
    .local v5, sch:[Ljava/lang/String;
    aget-object v7, v5, v10

    #@45
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@48
    move-result v7

    #@49
    if-nez v7, :cond_65

    #@4b
    aget-object v7, v5, v10

    #@4d
    const-string v8, "SELC"

    #@4f
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@52
    move-result v7

    #@53
    if-eqz v7, :cond_65

    #@55
    .line 1886
    if-eq v6, v11, :cond_5d

    #@57
    .line 1887
    invoke-static {v6, v5}, Lcom/android/internal/telephony/cdma/CDMAPhone;->checkOtaSpNumBasedOnSysSelCode(I[Ljava/lang/String;)Z

    #@5a
    move-result v2

    #@5b
    .end local v4           #m:Ljava/util/regex/Matcher;
    .end local v5           #sch:[Ljava/lang/String;
    :goto_5b
    move v3, v2

    #@5c
    .line 1915
    .restart local v3       #isOtaSpNum:I
    goto :goto_a

    #@5d
    .line 1890
    .end local v3           #isOtaSpNum:I
    .restart local v4       #m:Ljava/util/regex/Matcher;
    .restart local v5       #sch:[Ljava/lang/String;
    :cond_5d
    const-string v7, "CDMA"

    #@5f
    const-string v8, "isCarrierOtaSpNum,sysSelCodeInt is invalid"

    #@61
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@64
    goto :goto_5b

    #@65
    .line 1893
    :cond_65
    aget-object v7, v5, v10

    #@67
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@6a
    move-result v7

    #@6b
    if-nez v7, :cond_91

    #@6d
    aget-object v7, v5, v10

    #@6f
    const-string v8, "FC"

    #@71
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@74
    move-result v7

    #@75
    if-eqz v7, :cond_91

    #@77
    .line 1894
    const/4 v7, 0x1

    #@78
    aget-object v7, v5, v7

    #@7a
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@7d
    move-result v1

    #@7e
    .line 1895
    .local v1, fcLen:I
    const/4 v7, 0x2

    #@7f
    aget-object v0, v5, v7

    #@81
    .line 1896
    .local v0, fc:Ljava/lang/String;
    invoke-virtual {p1, v10, v0, v10, v1}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    #@84
    move-result v7

    #@85
    if-eqz v7, :cond_89

    #@87
    .line 1897
    const/4 v2, 0x1

    #@88
    goto :goto_5b

    #@89
    .line 1899
    :cond_89
    const-string v7, "CDMA"

    #@8b
    const-string v8, "isCarrierOtaSpNum,not otasp number"

    #@8d
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@90
    goto :goto_5b

    #@91
    .line 1903
    .end local v0           #fc:Ljava/lang/String;
    .end local v1           #fcLen:I
    :cond_91
    const-string v7, "CDMA"

    #@93
    new-instance v8, Ljava/lang/StringBuilder;

    #@95
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@98
    const-string v9, "isCarrierOtaSpNum,ota schema not supported"

    #@9a
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v8

    #@9e
    aget-object v9, v5, v10

    #@a0
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v8

    #@a4
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a7
    move-result-object v8

    #@a8
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ab
    goto :goto_5b

    #@ac
    .line 1908
    .end local v5           #sch:[Ljava/lang/String;
    :cond_ac
    const-string v7, "CDMA"

    #@ae
    new-instance v8, Ljava/lang/StringBuilder;

    #@b0
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@b3
    const-string v9, "isCarrierOtaSpNum,ota schema pattern not right"

    #@b5
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b8
    move-result-object v8

    #@b9
    iget-object v9, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mCarrierOtaSpNumSchema:Ljava/lang/String;

    #@bb
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@be
    move-result-object v8

    #@bf
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c2
    move-result-object v8

    #@c3
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c6
    goto :goto_5b

    #@c7
    .line 1913
    .end local v4           #m:Ljava/util/regex/Matcher;
    :cond_c7
    const-string v7, "CDMA"

    #@c9
    const-string v8, "isCarrierOtaSpNum,ota schema pattern empty"

    #@cb
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ce
    goto :goto_5b
.end method

.method private isCbEnable(I)Z
    .registers 3
    .parameter "action"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 875
    if-ne p1, v0, :cond_4

    #@3
    :goto_3
    return v0

    #@4
    :cond_4
    const/4 v0, 0x0

    #@5
    goto :goto_3
.end method

.method private static isIs683OtaSpDialStr(Ljava/lang/String;)Z
    .registers 5
    .parameter "dialStr"

    #@0
    .prologue
    .line 1753
    const/4 v1, 0x0

    #@1
    .line 1754
    .local v1, isOtaspDialString:Z
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@4
    move-result v0

    #@5
    .line 1756
    .local v0, dialStrLen:I
    const/4 v3, 0x4

    #@6
    if-ne v0, v3, :cond_12

    #@8
    .line 1757
    const-string v3, "*228"

    #@a
    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d
    move-result v3

    #@e
    if-eqz v3, :cond_11

    #@10
    .line 1758
    const/4 v1, 0x1

    #@11
    .line 1784
    :cond_11
    :goto_11
    return v1

    #@12
    .line 1761
    :cond_12
    invoke-static {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->extractSelCodeFromOtaSpNum(Ljava/lang/String;)I

    #@15
    move-result v2

    #@16
    .line 1762
    .local v2, sysSelCodeInt:I
    sparse-switch v2, :sswitch_data_1c

    #@19
    goto :goto_11

    #@1a
    .line 1778
    :sswitch_1a
    const/4 v1, 0x1

    #@1b
    .line 1779
    goto :goto_11

    #@1c
    .line 1762
    :sswitch_data_1c
    .sparse-switch
        0x0 -> :sswitch_1a
        0x1 -> :sswitch_1a
        0x2 -> :sswitch_1a
        0x3 -> :sswitch_1a
        0x4 -> :sswitch_1a
        0x5 -> :sswitch_1a
        0x6 -> :sswitch_1a
        0x7 -> :sswitch_1a
        0x1e -> :sswitch_1a
        0x1f -> :sswitch_1a
        0x20 -> :sswitch_1a
        0x21 -> :sswitch_1a
        0x22 -> :sswitch_1a
    .end sparse-switch
.end method

.method private updateVoiceMail()V
    .registers 2

    #@0
    .prologue
    .line 1127
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getStoredVoiceMessageCount()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->setVoiceMessageCount(I)V

    #@7
    .line 1128
    return-void
.end method


# virtual methods
.method public IsVMNumberNotInSIM()Z
    .registers 2

    #@0
    .prologue
    .line 2159
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public PlayVZWERISound()V
    .registers 3

    #@0
    .prologue
    .line 925
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSST:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@2
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getAlertId()I

    #@5
    move-result v1

    #@6
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->PlayVZWERISoundforMTCall(I)V

    #@9
    .line 926
    return-void
.end method

.method public StopVZWERISound()V
    .registers 2

    #@0
    .prologue
    .line 929
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSST:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->StopVZWERISound()V

    #@5
    .line 930
    return-void
.end method

.method public acceptCall()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 656
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mCT:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->acceptCall()V

    #@5
    .line 657
    return-void
.end method

.method public activateCellBroadcastSms(ILandroid/os/Message;)V
    .registers 5
    .parameter "activate"
    .parameter "response"

    #@0
    .prologue
    .line 1697
    const-string v0, "CDMA"

    #@2
    const-string v1, "[CDMAPhone] activateCellBroadcastSms() is obsolete; use SmsManager"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1698
    invoke-virtual {p2}, Landroid/os/Message;->sendToTarget()V

    #@a
    .line 1699
    return-void
.end method

.method public akaAuthenticate([B[BLandroid/os/Message;)V
    .registers 4
    .parameter "rand"
    .parameter "autn"
    .parameter "onComplete"

    #@0
    .prologue
    .line 2286
    if-eqz p3, :cond_5

    #@2
    invoke-virtual {p3}, Landroid/os/Message;->sendToTarget()V

    #@5
    .line 2287
    :cond_5
    return-void
.end method

.method public canConference()Z
    .registers 3

    #@0
    .prologue
    .line 769
    const-string v0, "CDMA"

    #@2
    const-string v1, "canConference: not possible in CDMA"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 770
    const/4 v0, 0x0

    #@8
    return v0
.end method

.method public canTransfer()Z
    .registers 3

    #@0
    .prologue
    .line 528
    const-string v0, "CDMA"

    #@2
    const-string v1, "canTransfer: not possible in CDMA"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 529
    const/4 v0, 0x0

    #@8
    return v0
.end method

.method public cancelManualSearchingRequest()V
    .registers 3

    #@0
    .prologue
    .line 795
    const-string v0, "CDMA"

    #@2
    const-string v1, "cancelManualSearchingRequest: not possible in CDMA"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 796
    return-void
.end method

.method public checkDataProfileEx(II)Z
    .registers 4
    .parameter "type"
    .parameter "Q_IPv"

    #@0
    .prologue
    .line 2250
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@2
    invoke-virtual {v0, p1, p2}, Lcom/android/internal/telephony/DataConnectionTracker;->checkDataProfileEx(II)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public clearDisconnected()V
    .registers 2

    #@0
    .prologue
    .line 558
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mCT:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->clearDisconnected()V

    #@5
    .line 559
    return-void
.end method

.method public conference()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 546
    const-string v0, "CDMA"

    #@2
    const-string v1, "conference: not possible in CDMA"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 547
    return-void
.end method

.method public dial(Ljava/lang/String;)Lcom/android/internal/telephony/Connection;
    .registers 6
    .parameter "dialString"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 590
    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->stripSeparators(Ljava/lang/String;)Ljava/lang/String;

    #@4
    move-result-object v1

    #@5
    .line 594
    .local v1, newDialString:Ljava/lang/String;
    const-string v2, "vzw_gfit"

    #@7
    invoke-static {v3, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@a
    move-result v2

    #@b
    if-eqz v2, :cond_1d

    #@d
    .line 595
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneBase;->mUiccApplication:Ljava/util/concurrent/atomic/AtomicReference;

    #@f
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@12
    move-result-object v2

    #@13
    check-cast v2, Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@15
    invoke-static {v1, p0, v2}, Lcom/android/internal/telephony/cdma/CdmaMmiCode;->newFromVZWDialString(Ljava/lang/String;Lcom/android/internal/telephony/cdma/CDMAPhone;Lcom/android/internal/telephony/uicc/UiccCardApplication;)Lcom/android/internal/telephony/cdma/CdmaMmiCode;

    #@18
    move-result-object v0

    #@19
    .line 596
    .local v0, mmiCode:Lcom/android/internal/telephony/cdma/CdmaMmiCode;
    if-eqz v0, :cond_1d

    #@1b
    move-object v2, v3

    #@1c
    .line 602
    .end local v0           #mmiCode:Lcom/android/internal/telephony/cdma/CdmaMmiCode;
    :goto_1c
    return-object v2

    #@1d
    :cond_1d
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mCT:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@1f
    invoke-virtual {v2, v1}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->dial(Ljava/lang/String;)Lcom/android/internal/telephony/Connection;

    #@22
    move-result-object v2

    #@23
    goto :goto_1c
.end method

.method public dial(Ljava/lang/String;Lcom/android/internal/telephony/UUSInfo;)Lcom/android/internal/telephony/Connection;
    .registers 5
    .parameter "dialString"
    .parameter "uusInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 606
    new-instance v0, Lcom/android/internal/telephony/CallStateException;

    #@2
    const-string v1, "Sending UUS information NOT supported in CDMA!"

    #@4
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CallStateException;-><init>(Ljava/lang/String;)V

    #@7
    throw v0
.end method

.method public disableLocationUpdates()V
    .registers 2

    #@0
    .prologue
    .line 1062
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSST:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->disableLocationUpdates()V

    #@5
    .line 1063
    return-void
.end method

.method public dispose()V
    .registers 4

    #@0
    .prologue
    .line 423
    sget-object v1, Lcom/android/internal/telephony/PhoneProxy;->lockForRadioTechnologyChange:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 424
    :try_start_3
    invoke-super {p0}, Lcom/android/internal/telephony/PhoneBase;->dispose()V

    #@6
    .line 425
    const-string v0, "dispose"

    #@8
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->log(Ljava/lang/String;)V

    #@b
    .line 428
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->unregisterForRuimRecordEvents()V

    #@e
    .line 429
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@10
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForAvailable(Landroid/os/Handler;)V

    #@13
    .line 430
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@15
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForOffOrNotAvailable(Landroid/os/Handler;)V

    #@18
    .line 431
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@1a
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForOn(Landroid/os/Handler;)V

    #@1d
    .line 432
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSST:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@1f
    invoke-virtual {v0, p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->unregisterForNetworkAttached(Landroid/os/Handler;)V

    #@22
    .line 433
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@24
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unSetOnSuppServiceNotification(Landroid/os/Handler;)V

    #@27
    .line 434
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@29
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForExitEmergencyCallbackMode(Landroid/os/Handler;)V

    #@2c
    .line 436
    const/4 v0, 0x0

    #@2d
    const-string v2, "ota_for_vzw"

    #@2f
    invoke-static {v0, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@32
    move-result v0

    #@33
    if-eqz v0, :cond_3a

    #@35
    .line 437
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@37
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unSetOnGstkSetupOtaspCall(Landroid/os/Handler;)V

    #@3a
    .line 440
    :cond_3a
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mExitEcmRunnable:Ljava/lang/Runnable;

    #@3c
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->removeCallbacks(Ljava/lang/Runnable;)V

    #@3f
    .line 442
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mPendingMmis:Ljava/util/ArrayList;

    #@41
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    #@44
    .line 445
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mCT:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@46
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->dispose()V

    #@49
    .line 446
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@4b
    invoke-virtual {v0}, Lcom/android/internal/telephony/DataConnectionTracker;->dispose()V

    #@4e
    .line 447
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSST:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@50
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->dispose()V

    #@53
    .line 448
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mCdmaSSM:Lcom/android/internal/telephony/cdma/CdmaSubscriptionSourceManager;

    #@55
    invoke-virtual {v0, p0}, Lcom/android/internal/telephony/cdma/CdmaSubscriptionSourceManager;->dispose(Landroid/os/Handler;)V

    #@58
    .line 449
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mRuimPhoneBookInterfaceManager:Lcom/android/internal/telephony/cdma/RuimPhoneBookInterfaceManager;

    #@5a
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/RuimPhoneBookInterfaceManager;->dispose()V

    #@5d
    .line 450
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSubInfo:Lcom/android/internal/telephony/PhoneSubInfo;

    #@5f
    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneSubInfo;->dispose()V

    #@62
    .line 451
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mEriManager:Lcom/android/internal/telephony/cdma/EriManager;

    #@64
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/EriManager;->dispose()V

    #@67
    .line 453
    const/4 v0, 0x0

    #@68
    const-string v2, "vzw_gfit"

    #@6a
    invoke-static {v0, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@6d
    move-result v0

    #@6e
    if-eqz v0, :cond_75

    #@70
    .line 454
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->gfUtils:Lcom/android/internal/telephony/gfit/GfitUtils;

    #@72
    invoke-virtual {v0}, Lcom/android/internal/telephony/gfit/GfitUtils;->dispose()V

    #@75
    .line 460
    :cond_75
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@77
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->SimStateReceiver:Landroid/content/BroadcastReceiver;

    #@79
    invoke-virtual {v0, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@7c
    .line 463
    monitor-exit v1

    #@7d
    .line 464
    return-void

    #@7e
    .line 463
    :catchall_7e
    move-exception v0

    #@7f
    monitor-exit v1
    :try_end_80
    .catchall {:try_start_3 .. :try_end_80} :catchall_7e

    #@80
    throw v0
.end method

.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 6
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 2132
    const-string v0, "CDMAPhone extends:"

    #@2
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@5
    .line 2133
    invoke-super {p0, p1, p2, p3}, Lcom/android/internal/telephony/PhoneBase;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    #@8
    .line 2134
    new-instance v0, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v1, " mVmNumber="

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mVmNumber:Ljava/lang/String;

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v0

    #@1d
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@20
    .line 2135
    new-instance v0, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v1, " mCT="

    #@27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v0

    #@2b
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mCT:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@2d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v0

    #@31
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v0

    #@35
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@38
    .line 2136
    new-instance v0, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v1, " mSST="

    #@3f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v0

    #@43
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSST:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@45
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v0

    #@49
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c
    move-result-object v0

    #@4d
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@50
    .line 2137
    new-instance v0, Ljava/lang/StringBuilder;

    #@52
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@55
    const-string v1, " mCdmaSSM="

    #@57
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v0

    #@5b
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mCdmaSSM:Lcom/android/internal/telephony/cdma/CdmaSubscriptionSourceManager;

    #@5d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v0

    #@61
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v0

    #@65
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@68
    .line 2138
    new-instance v0, Ljava/lang/StringBuilder;

    #@6a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@6d
    const-string v1, " mPendingMmis="

    #@6f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v0

    #@73
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mPendingMmis:Ljava/util/ArrayList;

    #@75
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v0

    #@79
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7c
    move-result-object v0

    #@7d
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@80
    .line 2139
    new-instance v0, Ljava/lang/StringBuilder;

    #@82
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@85
    const-string v1, " mRuimPhoneBookInterfaceManager="

    #@87
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v0

    #@8b
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mRuimPhoneBookInterfaceManager:Lcom/android/internal/telephony/cdma/RuimPhoneBookInterfaceManager;

    #@8d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v0

    #@91
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@94
    move-result-object v0

    #@95
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@98
    .line 2140
    new-instance v0, Ljava/lang/StringBuilder;

    #@9a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@9d
    const-string v1, " mCdmaSubscriptionSource="

    #@9f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v0

    #@a3
    iget v1, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mCdmaSubscriptionSource:I

    #@a5
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v0

    #@a9
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ac
    move-result-object v0

    #@ad
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@b0
    .line 2141
    new-instance v0, Ljava/lang/StringBuilder;

    #@b2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@b5
    const-string v1, " mSubInfo="

    #@b7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v0

    #@bb
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSubInfo:Lcom/android/internal/telephony/PhoneSubInfo;

    #@bd
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v0

    #@c1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c4
    move-result-object v0

    #@c5
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@c8
    .line 2142
    new-instance v0, Ljava/lang/StringBuilder;

    #@ca
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@cd
    const-string v1, " mEriManager="

    #@cf
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d2
    move-result-object v0

    #@d3
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mEriManager:Lcom/android/internal/telephony/cdma/EriManager;

    #@d5
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@d8
    move-result-object v0

    #@d9
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@dc
    move-result-object v0

    #@dd
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@e0
    .line 2143
    new-instance v0, Ljava/lang/StringBuilder;

    #@e2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@e5
    const-string v1, " mWakeLock="

    #@e7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ea
    move-result-object v0

    #@eb
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@ed
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@f0
    move-result-object v0

    #@f1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f4
    move-result-object v0

    #@f5
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@f8
    .line 2144
    new-instance v0, Ljava/lang/StringBuilder;

    #@fa
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@fd
    const-string v1, " mIsPhoneInEcmState="

    #@ff
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@102
    move-result-object v0

    #@103
    iget-boolean v1, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mIsPhoneInEcmState:Z

    #@105
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@108
    move-result-object v0

    #@109
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10c
    move-result-object v0

    #@10d
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@110
    .line 2149
    new-instance v0, Ljava/lang/StringBuilder;

    #@112
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@115
    const-string v1, " mCarrierOtaSpNumSchema="

    #@117
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11a
    move-result-object v0

    #@11b
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mCarrierOtaSpNumSchema:Ljava/lang/String;

    #@11d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@120
    move-result-object v0

    #@121
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@124
    move-result-object v0

    #@125
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@128
    .line 2150
    new-instance v0, Ljava/lang/StringBuilder;

    #@12a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@12d
    const-string v1, " getCdmaEriIconIndex()="

    #@12f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@132
    move-result-object v0

    #@133
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getCdmaEriIconIndex()I

    #@136
    move-result v1

    #@137
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13a
    move-result-object v0

    #@13b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13e
    move-result-object v0

    #@13f
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@142
    .line 2151
    new-instance v0, Ljava/lang/StringBuilder;

    #@144
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@147
    const-string v1, " getCdmaEriIconMode()="

    #@149
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14c
    move-result-object v0

    #@14d
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getCdmaEriIconMode()I

    #@150
    move-result v1

    #@151
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@154
    move-result-object v0

    #@155
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@158
    move-result-object v0

    #@159
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@15c
    .line 2152
    new-instance v0, Ljava/lang/StringBuilder;

    #@15e
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@161
    const-string v1, " getCdmaEriText()="

    #@163
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@166
    move-result-object v0

    #@167
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getCdmaEriText()Ljava/lang/String;

    #@16a
    move-result-object v1

    #@16b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16e
    move-result-object v0

    #@16f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@172
    move-result-object v0

    #@173
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@176
    .line 2153
    new-instance v0, Ljava/lang/StringBuilder;

    #@178
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@17b
    const-string v1, " isMinInfoReady()="

    #@17d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@180
    move-result-object v0

    #@181
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->isMinInfoReady()Z

    #@184
    move-result v1

    #@185
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@188
    move-result-object v0

    #@189
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18c
    move-result-object v0

    #@18d
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@190
    .line 2154
    new-instance v0, Ljava/lang/StringBuilder;

    #@192
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@195
    const-string v1, " isCspPlmnEnabled()="

    #@197
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19a
    move-result-object v0

    #@19b
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->isCspPlmnEnabled()Z

    #@19e
    move-result v1

    #@19f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1a2
    move-result-object v0

    #@1a3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a6
    move-result-object v0

    #@1a7
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1aa
    .line 2155
    return-void
.end method

.method public enableEnhancedVoicePrivacy(ZLandroid/os/Message;)V
    .registers 4
    .parameter "enable"
    .parameter "onComplete"

    #@0
    .prologue
    .line 550
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/CommandsInterface;->setPreferredVoicePrivacy(ZLandroid/os/Message;)V

    #@5
    .line 551
    return-void
.end method

.method public enableLocationUpdates()V
    .registers 2

    #@0
    .prologue
    .line 1058
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSST:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->enableLocationUpdates()V

    #@5
    .line 1059
    return-void
.end method

.method public exitEmergencyCallbackMode()V
    .registers 3

    #@0
    .prologue
    .line 1271
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_d

    #@8
    .line 1272
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@a
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    #@d
    .line 1276
    :cond_d
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getState()Lcom/android/internal/telephony/PhoneConstants$State;

    #@10
    move-result-object v0

    #@11
    sget-object v1, Lcom/android/internal/telephony/PhoneConstants$State;->IDLE:Lcom/android/internal/telephony/PhoneConstants$State;

    #@13
    if-eq v0, v1, :cond_19

    #@15
    .line 1277
    const/4 v0, 0x1

    #@16
    iput-boolean v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mIsPhoneInEcmExitDelayState:Z

    #@18
    .line 1284
    :goto_18
    return-void

    #@19
    .line 1283
    :cond_19
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@1b
    const/16 v1, 0x1a

    #@1d
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/cdma/CDMAPhone;->obtainMessage(I)Landroid/os/Message;

    #@20
    move-result-object v1

    #@21
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/CommandsInterface;->exitEmergencyCallbackMode(Landroid/os/Message;)V

    #@24
    goto :goto_18
.end method

.method public explicitCallTransfer()V
    .registers 3

    #@0
    .prologue
    .line 1202
    const-string v0, "CDMA"

    #@2
    const-string v1, "explicitCallTransfer: not possible in CDMA"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1203
    return-void
.end method

.method protected finalize()V
    .registers 3

    #@0
    .prologue
    .line 487
    const-string v0, "CDMA"

    #@2
    const-string v1, "CDMAPhone finalized"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 488
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@9
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@c
    move-result v0

    #@d
    if-eqz v0, :cond_1b

    #@f
    .line 489
    const-string v0, "CDMA"

    #@11
    const-string v1, "UNEXPECTED; mWakeLock is held when finalizing."

    #@13
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 490
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@18
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    #@1b
    .line 492
    :cond_1b
    return-void
.end method

.method public gbaAuthenticateBootstrap([B[BLandroid/os/Message;)V
    .registers 4
    .parameter "rand"
    .parameter "autn"
    .parameter "onComplete"

    #@0
    .prologue
    .line 2290
    if-eqz p3, :cond_5

    #@2
    invoke-virtual {p3}, Landroid/os/Message;->sendToTarget()V

    #@5
    .line 2291
    :cond_5
    return-void
.end method

.method public gbaAuthenticateNaf([BLandroid/os/Message;)V
    .registers 3
    .parameter "nafId"
    .parameter "onComplete"

    #@0
    .prologue
    .line 2294
    if-eqz p2, :cond_5

    #@2
    invoke-virtual {p2}, Landroid/os/Message;->sendToTarget()V

    #@5
    .line 2295
    :cond_5
    return-void
.end method

.method public getAPNList()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 2259
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/DataConnectionTracker;->getAPNList()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getAlertId()I
    .registers 4

    #@0
    .prologue
    .line 1978
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getServiceState()Landroid/telephony/ServiceState;

    #@3
    move-result-object v2

    #@4
    invoke-virtual {v2}, Landroid/telephony/ServiceState;->getCdmaRoamingIndicator()I

    #@7
    move-result v1

    #@8
    .line 1979
    .local v1, roamInd:I
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getServiceState()Landroid/telephony/ServiceState;

    #@b
    move-result-object v2

    #@c
    invoke-virtual {v2}, Landroid/telephony/ServiceState;->getCdmaDefaultRoamingIndicator()I

    #@f
    move-result v0

    #@10
    .line 1980
    .local v0, defRoamInd:I
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mEriManager:Lcom/android/internal/telephony/cdma/EriManager;

    #@12
    invoke-virtual {v2, v1, v0}, Lcom/android/internal/telephony/cdma/EriManager;->getAlertId(II)I

    #@15
    move-result v2

    #@16
    return v2
.end method

.method public getAvailableNetworks(Landroid/os/Message;)V
    .registers 5
    .parameter "response"

    #@0
    .prologue
    .line 1044
    const-string v1, "CDMA"

    #@2
    const-string v2, "getAvailableNetworks: not possible in CDMA"

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1045
    if-eqz p1, :cond_19

    #@9
    .line 1046
    new-instance v0, Lcom/android/internal/telephony/CommandException;

    #@b
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->REQUEST_NOT_SUPPORTED:Lcom/android/internal/telephony/CommandException$Error;

    #@d
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CommandException;-><init>(Lcom/android/internal/telephony/CommandException$Error;)V

    #@10
    .line 1048
    .local v0, ce:Lcom/android/internal/telephony/CommandException;
    invoke-static {p1}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;)Landroid/os/AsyncResult;

    #@13
    move-result-object v1

    #@14
    iput-object v0, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@16
    .line 1049
    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    #@19
    .line 1051
    .end local v0           #ce:Lcom/android/internal/telephony/CommandException;
    :cond_19
    return-void
.end method

.method public bridge synthetic getBackgroundCall()Lcom/android/internal/telephony/Call;
    .registers 2

    #@0
    .prologue
    .line 135
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getBackgroundCall()Lcom/android/internal/telephony/cdma/CdmaCall;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public getBackgroundCall()Lcom/android/internal/telephony/cdma/CdmaCall;
    .registers 2

    #@0
    .prologue
    .line 620
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mCT:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@2
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->backgroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@4
    return-object v0
.end method

.method public getCallBarringOption(Ljava/lang/String;Landroid/os/Message;)V
    .registers 6
    .parameter "commandInterfaceCBReason"
    .parameter "onComplete"

    #@0
    .prologue
    .line 862
    const-string v0, "Radhika"

    #@2
    const-string v1, "Setting the service class to None"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 863
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@9
    const/4 v1, 0x0

    #@a
    const/4 v2, 0x0

    #@b
    invoke-interface {v0, p1, v1, v2, p2}, Lcom/android/internal/telephony/CommandsInterface;->queryFacilityLock(Ljava/lang/String;Ljava/lang/String;ILandroid/os/Message;)V

    #@e
    .line 864
    return-void
.end method

.method public getCallForwardingIndicator()Z
    .registers 3

    #@0
    .prologue
    .line 1197
    const-string v0, "CDMA"

    #@2
    const-string v1, "getCallForwardingIndicator: not possible in CDMA"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1198
    const/4 v0, 0x0

    #@8
    return v0
.end method

.method public getCallForwardingOption(ILandroid/os/Message;)V
    .registers 5
    .parameter "commandInterfaceCFReason"
    .parameter "onComplete"

    #@0
    .prologue
    .line 1169
    const-string v0, "CDMA"

    #@2
    const-string v1, "getCallForwardingOption: not possible in CDMA"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1170
    return-void
.end method

.method public getCallTracker()Lcom/android/internal/telephony/CallTracker;
    .registers 2

    #@0
    .prologue
    .line 508
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mCT:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@2
    return-object v0
.end method

.method public getCallWaiting(Landroid/os/Message;)V
    .registers 4
    .parameter "onComplete"

    #@0
    .prologue
    .line 712
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    const/4 v1, 0x1

    #@3
    invoke-interface {v0, v1, p1}, Lcom/android/internal/telephony/CommandsInterface;->queryCallWaiting(ILandroid/os/Message;)V

    #@6
    .line 713
    return-void
.end method

.method public getCdmaEriHomeSystems()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1974
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mEriManager:Lcom/android/internal/telephony/cdma/EriManager;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/EriManager;->getCdmaEriHomeSystems()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getCdmaEriIconIndex()I
    .registers 2

    #@0
    .prologue
    .line 1946
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getServiceState()Landroid/telephony/ServiceState;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/telephony/ServiceState;->getCdmaEriIconIndex()I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getCdmaEriIconMode()I
    .registers 2

    #@0
    .prologue
    .line 1956
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getServiceState()Landroid/telephony/ServiceState;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/telephony/ServiceState;->getCdmaEriIconMode()I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getCdmaEriText()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 1964
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getServiceState()Landroid/telephony/ServiceState;

    #@3
    move-result-object v2

    #@4
    invoke-virtual {v2}, Landroid/telephony/ServiceState;->getCdmaRoamingIndicator()I

    #@7
    move-result v1

    #@8
    .line 1965
    .local v1, roamInd:I
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getServiceState()Landroid/telephony/ServiceState;

    #@b
    move-result-object v2

    #@c
    invoke-virtual {v2}, Landroid/telephony/ServiceState;->getCdmaDefaultRoamingIndicator()I

    #@f
    move-result v0

    #@10
    .line 1966
    .local v0, defRoamInd:I
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mEriManager:Lcom/android/internal/telephony/cdma/EriManager;

    #@12
    invoke-virtual {v2, v1, v0}, Lcom/android/internal/telephony/cdma/EriManager;->getCdmaEriText(II)Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    return-object v2
.end method

.method public getCdmaLteEhrpdForced()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 2242
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public getCdmaMin()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 704
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSST:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->getCdmaMin()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getCdmaPrlVersion()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 700
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSST:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->getPrlVersion()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getCellBroadcastSmsConfig(Landroid/os/Message;)V
    .registers 4
    .parameter "response"

    #@0
    .prologue
    .line 1707
    const-string v0, "CDMA"

    #@2
    const-string v1, "[CDMAPhone] getCellBroadcastSmsConfig() is obsolete; use SmsManager"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1708
    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    #@a
    .line 1709
    return-void
.end method

.method public getCellLocation()Landroid/telephony/CellLocation;
    .registers 2

    #@0
    .prologue
    .line 774
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSST:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@2
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->cellLoc:Landroid/telephony/cdma/CdmaCellLocation;

    #@4
    return-object v0
.end method

.method public getDataActivityState()Lcom/android/internal/telephony/Phone$DataActivityState;
    .registers 4

    #@0
    .prologue
    .line 562
    sget-object v0, Lcom/android/internal/telephony/Phone$DataActivityState;->NONE:Lcom/android/internal/telephony/Phone$DataActivityState;

    #@2
    .line 564
    .local v0, ret:Lcom/android/internal/telephony/Phone$DataActivityState;
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSST:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@4
    invoke-virtual {v1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->getCurrentDataConnectionState()I

    #@7
    move-result v1

    #@8
    if-nez v1, :cond_1b

    #@a
    .line 566
    sget-object v1, Lcom/android/internal/telephony/cdma/CDMAPhone$3;->$SwitchMap$com$android$internal$telephony$DctConstants$Activity:[I

    #@c
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@e
    invoke-virtual {v2}, Lcom/android/internal/telephony/DataConnectionTracker;->getActivity()Lcom/android/internal/telephony/DctConstants$Activity;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2}, Lcom/android/internal/telephony/DctConstants$Activity;->ordinal()I

    #@15
    move-result v2

    #@16
    aget v1, v1, v2

    #@18
    packed-switch v1, :pswitch_data_28

    #@1b
    .line 584
    :cond_1b
    :goto_1b
    return-object v0

    #@1c
    .line 568
    :pswitch_1c
    sget-object v0, Lcom/android/internal/telephony/Phone$DataActivityState;->DATAIN:Lcom/android/internal/telephony/Phone$DataActivityState;

    #@1e
    .line 569
    goto :goto_1b

    #@1f
    .line 572
    :pswitch_1f
    sget-object v0, Lcom/android/internal/telephony/Phone$DataActivityState;->DATAOUT:Lcom/android/internal/telephony/Phone$DataActivityState;

    #@21
    .line 573
    goto :goto_1b

    #@22
    .line 576
    :pswitch_22
    sget-object v0, Lcom/android/internal/telephony/Phone$DataActivityState;->DATAINANDOUT:Lcom/android/internal/telephony/Phone$DataActivityState;

    #@24
    .line 577
    goto :goto_1b

    #@25
    .line 580
    :pswitch_25
    sget-object v0, Lcom/android/internal/telephony/Phone$DataActivityState;->DORMANT:Lcom/android/internal/telephony/Phone$DataActivityState;

    #@27
    goto :goto_1b

    #@28
    .line 566
    :pswitch_data_28
    .packed-switch 0x1
        :pswitch_1c
        :pswitch_1f
        :pswitch_22
        :pswitch_25
    .end packed-switch
.end method

.method public getDataCallList(Landroid/os/Message;)V
    .registers 3
    .parameter "response"

    #@0
    .prologue
    .line 1066
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/CommandsInterface;->getDataCallList(Landroid/os/Message;)V

    #@5
    .line 1067
    return-void
.end method

.method public getDataConnectionState(Ljava/lang/String;)Lcom/android/internal/telephony/PhoneConstants$DataState;
    .registers 5
    .parameter "apnType"

    #@0
    .prologue
    .line 951
    sget-object v0, Lcom/android/internal/telephony/PhoneConstants$DataState;->DISCONNECTED:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@2
    .line 953
    .local v0, ret:Lcom/android/internal/telephony/PhoneConstants$DataState;
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSST:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@4
    if-nez v1, :cond_29

    #@6
    .line 957
    sget-object v0, Lcom/android/internal/telephony/PhoneConstants$DataState;->DISCONNECTED:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@8
    .line 996
    :goto_8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "getDataConnectionState apnType="

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    const-string v2, " ret="

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/cdma/CDMAPhone;->log(Ljava/lang/String;)V

    #@28
    .line 997
    return-object v0

    #@29
    .line 958
    :cond_29
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSST:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@2b
    invoke-virtual {v1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->getCurrentDataConnectionState()I

    #@2e
    move-result v1

    #@2f
    if-eqz v1, :cond_4e

    #@31
    iget-boolean v1, p0, Lcom/android/internal/telephony/PhoneBase;->mOosIsDisconnect:Z

    #@33
    if-eqz v1, :cond_4e

    #@35
    .line 962
    sget-object v0, Lcom/android/internal/telephony/PhoneConstants$DataState;->DISCONNECTED:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@37
    .line 963
    new-instance v1, Ljava/lang/StringBuilder;

    #@39
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@3c
    const-string v2, "getDataConnectionState: Data is Out of Service. ret = "

    #@3e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v1

    #@42
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v1

    #@46
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@49
    move-result-object v1

    #@4a
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/cdma/CDMAPhone;->log(Ljava/lang/String;)V

    #@4d
    goto :goto_8

    #@4e
    .line 964
    :cond_4e
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@50
    invoke-virtual {v1, p1}, Lcom/android/internal/telephony/DataConnectionTracker;->isApnTypeEnabled(Ljava/lang/String;)Z

    #@53
    move-result v1

    #@54
    if-eqz v1, :cond_5e

    #@56
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@58
    invoke-virtual {v1, p1}, Lcom/android/internal/telephony/DataConnectionTracker;->isApnTypeActive(Ljava/lang/String;)Z

    #@5b
    move-result v1

    #@5c
    if-nez v1, :cond_61

    #@5e
    .line 966
    :cond_5e
    sget-object v0, Lcom/android/internal/telephony/PhoneConstants$DataState;->DISCONNECTED:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@60
    goto :goto_8

    #@61
    .line 968
    :cond_61
    sget-object v1, Lcom/android/internal/telephony/cdma/CDMAPhone$3;->$SwitchMap$com$android$internal$telephony$DctConstants$State:[I

    #@63
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@65
    invoke-virtual {v2, p1}, Lcom/android/internal/telephony/DataConnectionTracker;->getState(Ljava/lang/String;)Lcom/android/internal/telephony/DctConstants$State;

    #@68
    move-result-object v2

    #@69
    invoke-virtual {v2}, Lcom/android/internal/telephony/DctConstants$State;->ordinal()I

    #@6c
    move-result v2

    #@6d
    aget v1, v1, v2

    #@6f
    packed-switch v1, :pswitch_data_92

    #@72
    goto :goto_8

    #@73
    .line 974
    :pswitch_73
    sget-object v0, Lcom/android/internal/telephony/PhoneConstants$DataState;->DISCONNECTED:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@75
    .line 975
    goto :goto_8

    #@76
    .line 979
    :pswitch_76
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mCT:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@78
    iget-object v1, v1, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->state:Lcom/android/internal/telephony/PhoneConstants$State;

    #@7a
    sget-object v2, Lcom/android/internal/telephony/PhoneConstants$State;->IDLE:Lcom/android/internal/telephony/PhoneConstants$State;

    #@7c
    if-eq v1, v2, :cond_89

    #@7e
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSST:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@80
    invoke-virtual {v1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isConcurrentVoiceAndDataAllowed()Z

    #@83
    move-result v1

    #@84
    if-nez v1, :cond_89

    #@86
    .line 981
    sget-object v0, Lcom/android/internal/telephony/PhoneConstants$DataState;->SUSPENDED:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@88
    goto :goto_8

    #@89
    .line 983
    :cond_89
    sget-object v0, Lcom/android/internal/telephony/PhoneConstants$DataState;->CONNECTED:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@8b
    .line 985
    goto/16 :goto_8

    #@8d
    .line 990
    :pswitch_8d
    sget-object v0, Lcom/android/internal/telephony/PhoneConstants$DataState;->CONNECTING:Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@8f
    goto/16 :goto_8

    #@91
    .line 968
    nop

    #@92
    :pswitch_data_92
    .packed-switch 0x1
        :pswitch_73
        :pswitch_73
        :pswitch_73
        :pswitch_73
        :pswitch_76
        :pswitch_8d
        :pswitch_8d
    .end packed-switch
.end method

.method public getDataRoamingEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 1070
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/DataConnectionTracker;->getDataOnRoamingEnabled()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getDeviceId()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 731
    const-string v2, "VZW"

    #@2
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@5
    move-result v2

    #@6
    if-eqz v2, :cond_25

    #@8
    .line 732
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getImei()Ljava/lang/String;

    #@b
    move-result-object v1

    #@c
    .line 733
    .local v1, mDeviceId:Ljava/lang/String;
    if-eqz v1, :cond_25

    #@e
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@11
    move-result v2

    #@12
    const/16 v3, 0xf

    #@14
    if-ne v2, v3, :cond_25

    #@16
    .line 734
    const-string v2, "CDMA"

    #@18
    const-string v3, "getDeviceId(): returns IMEI(14) in CDMA-LTE Phone"

    #@1a
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    .line 735
    const/4 v2, 0x0

    #@1e
    const/16 v3, 0xe

    #@20
    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@23
    move-result-object v0

    #@24
    .line 745
    .end local v1           #mDeviceId:Ljava/lang/String;
    :cond_24
    :goto_24
    return-object v0

    #@25
    .line 740
    :cond_25
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getMeid()Ljava/lang/String;

    #@28
    move-result-object v0

    #@29
    .line 741
    .local v0, id:Ljava/lang/String;
    if-eqz v0, :cond_33

    #@2b
    const-string v2, "^0*$"

    #@2d
    invoke-virtual {v0, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    #@30
    move-result v2

    #@31
    if-eqz v2, :cond_24

    #@33
    .line 742
    :cond_33
    const-string v2, "CDMA"

    #@35
    const-string v3, "getDeviceId(): MEID is not initialized use ESN"

    #@37
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 743
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getEsn()Ljava/lang/String;

    #@3d
    move-result-object v0

    #@3e
    goto :goto_24
.end method

.method public getDeviceId(I)Ljava/lang/String;
    .registers 10
    .parameter "type"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/16 v6, 0xe

    #@3
    .line 2198
    const/4 v1, 0x0

    #@4
    .line 2199
    .local v1, mDeviceId:Ljava/lang/String;
    const-string v3, "gsm.sim.state"

    #@6
    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@9
    move-result-object v2

    #@a
    .line 2200
    .local v2, prop:Ljava/lang/String;
    const-string v3, "READY"

    #@c
    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f
    move-result v0

    #@10
    .line 2201
    .local v0, isSIMReady:Z
    const-string v3, "CDMA"

    #@12
    new-instance v4, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v5, "mImei = "

    #@19
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v4

    #@1d
    iget-object v5, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mImei:Ljava/lang/String;

    #@1f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v4

    #@23
    const-string v5, "   mImeiSv = "

    #@25
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v4

    #@29
    iget-object v5, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mImeiSv:Ljava/lang/String;

    #@2b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v4

    #@2f
    const-string v5, "   mEsn = "

    #@31
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v4

    #@35
    iget-object v5, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mEsn:Ljava/lang/String;

    #@37
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v4

    #@3b
    const-string v5, "   mMeid = "

    #@3d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v4

    #@41
    iget-object v5, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mMeid:Ljava/lang/String;

    #@43
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v4

    #@47
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v4

    #@4b
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    .line 2203
    packed-switch p1, :pswitch_data_b4

    #@51
    .line 2225
    :cond_51
    :goto_51
    const-string v3, "CDMA"

    #@53
    new-instance v4, Ljava/lang/StringBuilder;

    #@55
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@58
    const-string v5, "getDeviceId("

    #@5a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v4

    #@5e
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@61
    move-result-object v4

    #@62
    const-string v5, ") = "

    #@64
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v4

    #@68
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v4

    #@6c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6f
    move-result-object v4

    #@70
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@73
    .line 2226
    return-object v1

    #@74
    .line 2205
    :pswitch_74
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mImei:Ljava/lang/String;

    #@76
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@79
    move-result v3

    #@7a
    if-le v3, v6, :cond_82

    #@7c
    .line 2206
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mImei:Ljava/lang/String;

    #@7e
    invoke-virtual {v3, v7, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@81
    move-result-object v1

    #@82
    .line 2208
    :cond_82
    if-eqz v1, :cond_8c

    #@84
    const-string v3, "^0*$"

    #@86
    invoke-virtual {v1, v3}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    #@89
    move-result v3

    #@8a
    if-eqz v3, :cond_51

    #@8c
    .line 2209
    :cond_8c
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getEsn()Ljava/lang/String;

    #@8f
    move-result-object v1

    #@90
    goto :goto_51

    #@91
    .line 2213
    :pswitch_91
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mImei:Ljava/lang/String;

    #@93
    .line 2214
    goto :goto_51

    #@94
    .line 2216
    :pswitch_94
    if-nez v0, :cond_98

    #@96
    .line 2217
    const/4 v1, 0x0

    #@97
    goto :goto_51

    #@98
    .line 2218
    :cond_98
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mImei:Ljava/lang/String;

    #@9a
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@9d
    move-result v3

    #@9e
    if-lt v3, v6, :cond_b1

    #@a0
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mImei:Ljava/lang/String;

    #@a2
    invoke-virtual {v3, v7, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@a5
    move-result-object v3

    #@a6
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mMeid:Ljava/lang/String;

    #@a8
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ab
    move-result v3

    #@ac
    if-nez v3, :cond_b1

    #@ae
    .line 2219
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mMeid:Ljava/lang/String;

    #@b0
    goto :goto_51

    #@b1
    .line 2221
    :cond_b1
    const/4 v1, 0x0

    #@b2
    goto :goto_51

    #@b3
    .line 2203
    nop

    #@b4
    :pswitch_data_b4
    .packed-switch 0x1
        :pswitch_74
        :pswitch_91
        :pswitch_94
    .end packed-switch
.end method

.method public getDeviceSvn()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 751
    const-string v0, "US"

    #@2
    const-string v1, "SPR"

    #@4
    invoke-static {v0, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_13

    #@a
    .line 752
    const-string v0, "ro.lge.swversion"

    #@c
    const-string v1, "0"

    #@e
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@11
    move-result-object v0

    #@12
    .line 756
    :goto_12
    return-object v0

    #@13
    .line 755
    :cond_13
    const-string v0, "CDMA"

    #@15
    const-string v1, "getDeviceSvn(): return 0"

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 756
    const-string v0, "0"

    #@1c
    goto :goto_12
.end method

.method public getEmodeInfoPage(I)Ljava/lang/String;
    .registers 3
    .parameter "EngIndex"

    #@0
    .prologue
    .line 2024
    const-string v0, "Dummy"

    #@2
    return-object v0
.end method

.method public getEngineeringModeInfo(ILandroid/os/Message;)I
    .registers 4
    .parameter "data"
    .parameter "result"

    #@0
    .prologue
    .line 2017
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getEnhancedVoicePrivacy(Landroid/os/Message;)V
    .registers 3
    .parameter "onComplete"

    #@0
    .prologue
    .line 554
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/CommandsInterface;->getPreferredVoicePrivacy(Landroid/os/Message;)V

    #@5
    .line 555
    return-void
.end method

.method public getEriFileVersion()I
    .registers 2

    #@0
    .prologue
    .line 2116
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mEriManager:Lcom/android/internal/telephony/cdma/EriManager;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/EriManager;->getEriFileVersion()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getEriManager()Lcom/android/internal/telephony/cdma/EriManager;
    .registers 2

    #@0
    .prologue
    .line 2119
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mEriManager:Lcom/android/internal/telephony/cdma/EriManager;

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 2120
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mEriManager:Lcom/android/internal/telephony/cdma/EriManager;

    #@6
    .line 2122
    :goto_6
    return-object v0

    #@7
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method

.method public getEsn()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 721
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mEsn:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public bridge synthetic getForegroundCall()Lcom/android/internal/telephony/Call;
    .registers 2

    #@0
    .prologue
    .line 135
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getForegroundCall()Lcom/android/internal/telephony/cdma/CdmaCall;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public getForegroundCall()Lcom/android/internal/telephony/cdma/CdmaCall;
    .registers 2

    #@0
    .prologue
    .line 778
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mCT:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@2
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->foregroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@4
    return-object v0
.end method

.method public getHDRRoamingIndicator()I
    .registers 2

    #@0
    .prologue
    .line 2232
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getIccPhoneBookInterfaceManager()Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;
    .registers 2

    #@0
    .prologue
    .line 1670
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mRuimPhoneBookInterfaceManager:Lcom/android/internal/telephony/cdma/RuimPhoneBookInterfaceManager;

    #@2
    return-object v0
.end method

.method public getImei()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 764
    const-string v0, "CDMA"

    #@2
    const-string v1, "IMEI is not available in CDMA"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 765
    const/4 v0, 0x0

    #@8
    return-object v0
.end method

.method public getInternationalMdnVoiceMailNumberForVZW()Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    .line 1133
    const/4 v2, 0x0

    #@1
    .line 1135
    .local v2, mdnVmNumber:Ljava/lang/String;
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mAssistDialPhoneNumberUtils:Landroid/telephony/AssistDialPhoneNumberUtils;

    #@3
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@6
    move-result-object v4

    #@7
    invoke-virtual {v3, v4}, Landroid/telephony/AssistDialPhoneNumberUtils;->getAssistedDialCurrentCountry(Landroid/content/Context;)Landroid/provider/ReferenceCountry;

    #@a
    move-result-object v0

    #@b
    .line 1137
    .local v0, currentCountry:Landroid/provider/ReferenceCountry;
    if-eqz v0, :cond_39

    #@d
    .line 1138
    invoke-virtual {v0}, Landroid/provider/ReferenceCountry;->getNanp()Ljava/lang/String;

    #@10
    move-result-object v3

    #@11
    const-string v4, "0"

    #@13
    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@16
    move-result v3

    #@17
    const/4 v4, 0x1

    #@18
    if-ne v3, v4, :cond_39

    #@1a
    .line 1139
    invoke-virtual {v0}, Landroid/provider/ReferenceCountry;->getIddPrefix()Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    .line 1140
    .local v1, current_Idd:Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    const-string v4, "1"

    #@29
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v3

    #@2d
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getLine1Number()Ljava/lang/String;

    #@30
    move-result-object v4

    #@31
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v3

    #@35
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v2

    #@39
    .line 1144
    .end local v1           #current_Idd:Ljava/lang/String;
    :cond_39
    return-object v2
.end method

.method public getLine1AlphaTag()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 1206
    const-string v0, "CDMA"

    #@2
    const-string v1, "getLine1AlphaTag: not possible in CDMA"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1207
    const/4 v0, 0x0

    #@8
    return-object v0
.end method

.method public getLine1Number()Ljava/lang/String;
    .registers 9

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 671
    const-string v5, "VZW"

    #@3
    invoke-static {v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@6
    move-result v5

    #@7
    if-eqz v5, :cond_9f

    #@9
    .line 672
    iget-object v5, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSST:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@b
    invoke-virtual {v5}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->getMdnNumber()Ljava/lang/String;

    #@e
    move-result-object v2

    #@f
    .line 673
    .local v2, mdnNumber:Ljava/lang/String;
    if-eqz v2, :cond_58

    #@11
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@14
    move-result v3

    #@15
    .line 674
    .local v3, mdnNumberLength:I
    :goto_15
    const/16 v5, 0xa

    #@17
    if-lt v3, v5, :cond_5a

    #@19
    .line 675
    add-int/lit8 v5, v3, -0xa

    #@1b
    invoke-virtual {v2, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    .line 681
    invoke-virtual {v2}, Ljava/lang/String;->toCharArray()[C

    #@22
    move-result-object v0

    #@23
    .line 682
    .local v0, chArr:[C
    const/4 v1, 0x0

    #@24
    .local v1, i:I
    :goto_24
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@27
    move-result v5

    #@28
    if-ge v1, v5, :cond_7d

    #@2a
    .line 683
    aget-char v5, v0, v1

    #@2c
    invoke-static {v5}, Landroid/telephony/PhoneNumberUtils;->isISODigit(C)Z

    #@2f
    move-result v5

    #@30
    if-nez v5, :cond_7a

    #@32
    .line 684
    const-string v5, "CDMA"

    #@34
    new-instance v6, Ljava/lang/StringBuilder;

    #@36
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    const-string v7, "chArr["

    #@3b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v6

    #@3f
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@42
    move-result-object v6

    #@43
    const-string v7, "]"

    #@45
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v6

    #@49
    aget-char v7, v0, v1

    #@4b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v6

    #@4f
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v6

    #@53
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@56
    move-object v2, v4

    #@57
    .line 696
    .end local v0           #chArr:[C
    .end local v1           #i:I
    .end local v2           #mdnNumber:Ljava/lang/String;
    .end local v3           #mdnNumberLength:I
    :cond_57
    :goto_57
    return-object v2

    #@58
    .line 673
    .restart local v2       #mdnNumber:Ljava/lang/String;
    :cond_58
    const/4 v3, 0x0

    #@59
    goto :goto_15

    #@5a
    .line 677
    .restart local v3       #mdnNumberLength:I
    :cond_5a
    const-string v5, "CDMA"

    #@5c
    new-instance v6, Ljava/lang/StringBuilder;

    #@5e
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@61
    const-string v7, "MDN length is "

    #@63
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v6

    #@67
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v6

    #@6b
    const-string v7, ", so return null"

    #@6d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v6

    #@71
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@74
    move-result-object v6

    #@75
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@78
    move-object v2, v4

    #@79
    .line 678
    goto :goto_57

    #@7a
    .line 682
    .restart local v0       #chArr:[C
    .restart local v1       #i:I
    :cond_7a
    add-int/lit8 v1, v1, 0x1

    #@7c
    goto :goto_24

    #@7d
    .line 689
    :cond_7d
    const-string v5, "0000000000"

    #@7f
    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@82
    move-result v5

    #@83
    if-eqz v5, :cond_57

    #@85
    .line 690
    const-string v5, "CDMA"

    #@87
    new-instance v6, Ljava/lang/StringBuilder;

    #@89
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@8c
    const-string v7, "mdnNumber = "

    #@8e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    move-result-object v6

    #@92
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v6

    #@96
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@99
    move-result-object v6

    #@9a
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9d
    move-object v2, v4

    #@9e
    .line 691
    goto :goto_57

    #@9f
    .line 696
    .end local v0           #chArr:[C
    .end local v1           #i:I
    .end local v2           #mdnNumber:Ljava/lang/String;
    .end local v3           #mdnNumberLength:I
    :cond_9f
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSST:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@a1
    invoke-virtual {v4}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->getMdnNumber()Ljava/lang/String;

    #@a4
    move-result-object v2

    #@a5
    goto :goto_57
.end method

.method public getMSIN()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 2275
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public getMeid()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 725
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mMeid:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getMipErrorCode(Landroid/os/Message;)V
    .registers 3
    .parameter "result"

    #@0
    .prologue
    .line 2269
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/CommandsInterface;->getMipErrorCode(Landroid/os/Message;)V

    #@5
    .line 2270
    return-void
.end method

.method public getMute()Z
    .registers 2

    #@0
    .prologue
    .line 541
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mCT:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->getMute()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getNeighboringCids(Landroid/os/Message;)V
    .registers 4
    .parameter "response"

    #@0
    .prologue
    .line 942
    if-eqz p1, :cond_12

    #@2
    .line 943
    new-instance v0, Lcom/android/internal/telephony/CommandException;

    #@4
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->REQUEST_NOT_SUPPORTED:Lcom/android/internal/telephony/CommandException$Error;

    #@6
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CommandException;-><init>(Lcom/android/internal/telephony/CommandException$Error;)V

    #@9
    .line 945
    .local v0, ce:Lcom/android/internal/telephony/CommandException;
    invoke-static {p1}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;)Landroid/os/AsyncResult;

    #@c
    move-result-object v1

    #@d
    iput-object v0, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@f
    .line 946
    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    #@12
    .line 948
    .end local v0           #ce:Lcom/android/internal/telephony/CommandException;
    :cond_12
    return-void
.end method

.method public getOutgoingCallerIdDisplay(Landroid/os/Message;)V
    .registers 4
    .parameter "onComplete"

    #@0
    .prologue
    .line 1192
    const-string v0, "CDMA"

    #@2
    const-string v1, "getOutgoingCallerIdDisplay: not possible in CDMA"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1193
    return-void
.end method

.method public getPendingMmiCodes()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/android/internal/telephony/MmiCode;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 611
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mPendingMmis:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method public getPhoneName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 520
    const-string v0, "CDMA"

    #@2
    return-object v0
.end method

.method public getPhoneSubInfo()Lcom/android/internal/telephony/PhoneSubInfo;
    .registers 2

    #@0
    .prologue
    .line 1663
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSubInfo:Lcom/android/internal/telephony/PhoneSubInfo;

    #@2
    return-object v0
.end method

.method public getPhoneType()I
    .registers 2

    #@0
    .prologue
    .line 524
    const/4 v0, 0x2

    #@1
    return v0
.end method

.method public bridge synthetic getRingingCall()Lcom/android/internal/telephony/Call;
    .registers 2

    #@0
    .prologue
    .line 135
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getRingingCall()Lcom/android/internal/telephony/cdma/CdmaCall;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public getRingingCall()Lcom/android/internal/telephony/cdma/CdmaCall;
    .registers 2

    #@0
    .prologue
    .line 533
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mCT:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@2
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->ringingCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@4
    return-object v0
.end method

.method public getSearchStatus(ILandroid/os/Message;)V
    .registers 5
    .parameter "ReqType"
    .parameter "response"

    #@0
    .prologue
    .line 2032
    const-string v0, "CDMA"

    #@2
    const-string v1, "method getSearchStatus is NOT supported in CDMA!"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 2033
    return-void
.end method

.method public getSearchStatus(Landroid/os/Message;)V
    .registers 4
    .parameter "response"

    #@0
    .prologue
    .line 807
    const-string v0, "CDMA"

    #@2
    const-string v1, "getSearchStatus: not possible in CDMA"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 808
    return-void
.end method

.method public getServiceState()Landroid/telephony/ServiceState;
    .registers 4

    #@0
    .prologue
    .line 496
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSST:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@2
    if-eqz v1, :cond_f

    #@4
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSST:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@6
    iget-object v1, v1, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@8
    if-eqz v1, :cond_f

    #@a
    .line 497
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSST:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@c
    iget-object v0, v1, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@e
    .line 502
    :goto_e
    return-object v0

    #@f
    .line 499
    :cond_f
    const-string v1, "CDMA"

    #@11
    const-string v2, "Current ServiceState is null, return default state"

    #@13
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 500
    new-instance v0, Landroid/telephony/ServiceState;

    #@18
    invoke-direct {v0}, Landroid/telephony/ServiceState;-><init>()V

    #@1b
    .line 501
    .local v0, DefaultSs:Landroid/telephony/ServiceState;
    invoke-virtual {v0}, Landroid/telephony/ServiceState;->setStateOutOfService()V

    #@1e
    goto :goto_e
.end method

.method public getServiceStateTracker()Lcom/android/internal/telephony/ServiceStateTracker;
    .registers 2

    #@0
    .prologue
    .line 516
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSST:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@2
    return-object v0
.end method

.method public getState()Lcom/android/internal/telephony/PhoneConstants$State;
    .registers 2

    #@0
    .prologue
    .line 512
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mCT:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@2
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->state:Lcom/android/internal/telephony/PhoneConstants$State;

    #@4
    return-object v0
.end method

.method public getSubscriberId()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 760
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSST:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->getImsi()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method protected getUiccCardApplication()Lcom/android/internal/telephony/uicc/UiccCardApplication;
    .registers 3

    #@0
    .prologue
    .line 1598
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    const-string v1, "SPR"

    #@6
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_14

    #@c
    .line 1599
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@e
    const/4 v1, 0x1

    #@f
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/uicc/UiccController;->getUiccCardApplication(I)Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@12
    move-result-object v0

    #@13
    .line 1601
    :goto_13
    return-object v0

    #@14
    :cond_14
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@16
    const/4 v1, 0x2

    #@17
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/uicc/UiccController;->getUiccCardApplication(I)Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@1a
    move-result-object v0

    #@1b
    goto :goto_13
.end method

.method public getVoiceMailAlphaTag()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 1156
    const-string v0, ""

    #@2
    .line 1160
    .local v0, ret:Ljava/lang/String;
    if-eqz v0, :cond_a

    #@4
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@7
    move-result v1

    #@8
    if-nez v1, :cond_17

    #@a
    .line 1161
    :cond_a
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@c
    const v2, 0x1040004

    #@f
    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@16
    move-result-object v0

    #@17
    .line 1165
    .end local v0           #ret:Ljava/lang/String;
    :cond_17
    return-object v0
.end method

.method public getVoiceMailNumber()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 1097
    const/4 v0, 0x0

    #@1
    .line 1098
    .local v0, number:Ljava/lang/String;
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@4
    move-result-object v2

    #@5
    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    #@8
    move-result-object v1

    #@9
    .line 1102
    .local v1, sp:Landroid/content/SharedPreferences;
    const/4 v2, 0x0

    #@a
    const-string v3, "show_Mdn_For_VMS"

    #@c
    invoke-static {v2, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@f
    move-result v2

    #@10
    if-eqz v2, :cond_1d

    #@12
    .line 1103
    const-string v2, "vm_number_key_cdma"

    #@14
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getLine1Number()Ljava/lang/String;

    #@17
    move-result-object v3

    #@18
    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1b
    move-result-object v0

    #@1c
    .line 1122
    :cond_1c
    :goto_1c
    return-object v0

    #@1d
    .line 1105
    :cond_1d
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@20
    move-result-object v2

    #@21
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->getVMS(Landroid/content/Context;)Ljava/lang/String;

    #@24
    move-result-object v0

    #@25
    .line 1106
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@28
    move-result v2

    #@29
    if-eqz v2, :cond_1c

    #@2b
    .line 1107
    const-string v2, "vm_number_key_cdma"

    #@2d
    const-string v3, "*86"

    #@2f
    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@32
    move-result-object v0

    #@33
    goto :goto_1c
.end method

.method public handleInCallMmiCommands(Ljava/lang/String;)Z
    .registers 4
    .parameter "dialString"

    #@0
    .prologue
    .line 624
    const-string v0, "CDMA"

    #@2
    const-string v1, "method handleInCallMmiCommands is NOT supported in CDMA!"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 625
    const/4 v0, 0x0

    #@8
    return v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 11
    .parameter "msg"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v7, 0x0

    #@2
    const/16 v8, 0x15

    #@4
    .line 1427
    iget-boolean v5, p0, Lcom/android/internal/telephony/PhoneBase;->mIsTheCurrentActivePhone:Z

    #@6
    if-nez v5, :cond_33

    #@8
    .line 1428
    const-string v5, "CDMA"

    #@a
    new-instance v6, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v7, "Received message "

    #@11
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v6

    #@15
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v6

    #@19
    const-string v7, "["

    #@1b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v6

    #@1f
    iget v7, p1, Landroid/os/Message;->what:I

    #@21
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@24
    move-result-object v6

    #@25
    const-string v7, "] while being destroyed. Ignoring."

    #@27
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v6

    #@2b
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v6

    #@2f
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 1594
    :cond_32
    :goto_32
    return-void

    #@33
    .line 1432
    :cond_33
    iget v5, p1, Landroid/os/Message;->what:I

    #@35
    sparse-switch v5, :sswitch_data_1ea

    #@38
    .line 1591
    invoke-super {p0, p1}, Lcom/android/internal/telephony/PhoneBase;->handleMessage(Landroid/os/Message;)V

    #@3b
    goto :goto_32

    #@3c
    .line 1434
    :sswitch_3c
    iget-object v5, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@3e
    const/4 v6, 0x6

    #@3f
    invoke-virtual {p0, v6}, Lcom/android/internal/telephony/cdma/CDMAPhone;->obtainMessage(I)Landroid/os/Message;

    #@42
    move-result-object v6

    #@43
    invoke-interface {v5, v6}, Lcom/android/internal/telephony/CommandsInterface;->getBasebandVersion(Landroid/os/Message;)V

    #@46
    .line 1436
    iget-object v5, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@48
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/cdma/CDMAPhone;->obtainMessage(I)Landroid/os/Message;

    #@4b
    move-result-object v6

    #@4c
    invoke-interface {v5, v6}, Lcom/android/internal/telephony/CommandsInterface;->getDeviceIdentity(Landroid/os/Message;)V

    #@4f
    .line 1438
    const-string v5, "vzw_eri"

    #@51
    invoke-static {v7, v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@54
    move-result v5

    #@55
    if-eqz v5, :cond_32

    #@57
    .line 1439
    iget-object v5, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mEriManager:Lcom/android/internal/telephony/cdma/EriManager;

    #@59
    invoke-virtual {v5}, Lcom/android/internal/telephony/cdma/EriManager;->isEriFileLoaded()Z

    #@5c
    move-result v5

    #@5d
    if-nez v5, :cond_69

    #@5f
    .line 1440
    const-string v5, "CDMA"

    #@61
    const-string v6, "Eri file load start!!"

    #@63
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@66
    .line 1441
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->prepareEri()V

    #@69
    .line 1443
    :cond_69
    iget-object v5, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@6b
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mEriManager:Lcom/android/internal/telephony/cdma/EriManager;

    #@6d
    invoke-virtual {v6}, Lcom/android/internal/telephony/cdma/EriManager;->getEriFileVersion()I

    #@70
    move-result v6

    #@71
    const/16 v7, 0x2e

    #@73
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/cdma/CDMAPhone;->obtainMessage(I)Landroid/os/Message;

    #@76
    move-result-object v7

    #@77
    invoke-interface {v5, v6, v7}, Lcom/android/internal/telephony/CommandsInterface;->setCdmaEriVersion(ILandroid/os/Message;)V

    #@7a
    goto :goto_32

    #@7b
    .line 1451
    :sswitch_7b
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@7d
    check-cast v1, Landroid/os/AsyncResult;

    #@7f
    .line 1452
    .local v1, ar:Landroid/os/AsyncResult;
    iget-object v5, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@81
    if-eqz v5, :cond_8b

    #@83
    .line 1453
    const-string v5, "CDMA"

    #@85
    const-string v6, "EVENT_SET_ERI_VERSION : FAIL"

    #@87
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8a
    goto :goto_32

    #@8b
    .line 1455
    :cond_8b
    const-string v5, "CDMA"

    #@8d
    new-instance v6, Ljava/lang/StringBuilder;

    #@8f
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@92
    const-string v7, "EVENT_SET_ERI_VERSION: "

    #@94
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@97
    move-result-object v6

    #@98
    iget-object v7, v1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@9a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v6

    #@9e
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a1
    move-result-object v6

    #@a2
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a5
    goto :goto_32

    #@a6
    .line 1461
    .end local v1           #ar:Landroid/os/AsyncResult;
    :sswitch_a6
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@a8
    check-cast v1, Landroid/os/AsyncResult;

    #@aa
    .line 1463
    .restart local v1       #ar:Landroid/os/AsyncResult;
    iget-object v5, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@ac
    if-nez v5, :cond_32

    #@ae
    .line 1467
    const-string v5, "CDMA"

    #@b0
    new-instance v6, Ljava/lang/StringBuilder;

    #@b2
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@b5
    const-string v7, "Baseband version: "

    #@b7
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v6

    #@bb
    iget-object v7, v1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@bd
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v6

    #@c1
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c4
    move-result-object v6

    #@c5
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c8
    .line 1468
    const-string v6, "gsm.version.baseband"

    #@ca
    iget-object v5, v1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@cc
    check-cast v5, Ljava/lang/String;

    #@ce
    invoke-virtual {p0, v6, v5}, Lcom/android/internal/telephony/cdma/CDMAPhone;->setSystemProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@d1
    goto/16 :goto_32

    #@d3
    .line 1473
    .end local v1           #ar:Landroid/os/AsyncResult;
    :sswitch_d3
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@d5
    check-cast v1, Landroid/os/AsyncResult;

    #@d7
    .line 1475
    .restart local v1       #ar:Landroid/os/AsyncResult;
    iget-object v5, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@d9
    if-eqz v5, :cond_ea

    #@db
    .line 1479
    iget v5, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->retryNum:I

    #@dd
    const/16 v6, 0xa

    #@df
    if-gt v5, v6, :cond_32

    #@e1
    .line 1480
    const/16 v5, 0x38

    #@e3
    const-wide/16 v6, 0x3e8

    #@e5
    invoke-virtual {p0, v5, v6, v7}, Lcom/android/internal/telephony/cdma/CDMAPhone;->sendEmptyMessageDelayed(IJ)Z

    #@e8
    goto/16 :goto_32

    #@ea
    .line 1485
    :cond_ea
    iget-object v5, v1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@ec
    check-cast v5, [Ljava/lang/String;

    #@ee
    move-object v4, v5

    #@ef
    check-cast v4, [Ljava/lang/String;

    #@f1
    .line 1486
    .local v4, respId:[Ljava/lang/String;
    aget-object v5, v4, v6

    #@f3
    iput-object v5, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mImei:Ljava/lang/String;

    #@f5
    .line 1487
    const/4 v5, 0x1

    #@f6
    aget-object v5, v4, v5

    #@f8
    iput-object v5, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mImeiSv:Ljava/lang/String;

    #@fa
    .line 1488
    const/4 v5, 0x2

    #@fb
    aget-object v5, v4, v5

    #@fd
    iput-object v5, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mEsn:Ljava/lang/String;

    #@ff
    .line 1489
    const/4 v5, 0x3

    #@100
    aget-object v5, v4, v5

    #@102
    iput-object v5, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mMeid:Ljava/lang/String;

    #@104
    .line 1494
    iput v6, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->retryNum:I

    #@106
    goto/16 :goto_32

    #@108
    .line 1502
    .end local v1           #ar:Landroid/os/AsyncResult;
    .end local v4           #respId:[Ljava/lang/String;
    :sswitch_108
    const-string v5, "CDMA"

    #@10a
    new-instance v6, Ljava/lang/StringBuilder;

    #@10c
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@10f
    const-string v7, "EVENT_RETRY_GET_DEVICE_IDENTITY : retryNum = "

    #@111
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@114
    move-result-object v6

    #@115
    iget v7, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->retryNum:I

    #@117
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11a
    move-result-object v6

    #@11b
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11e
    move-result-object v6

    #@11f
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@122
    .line 1503
    iget v5, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->retryNum:I

    #@124
    add-int/lit8 v5, v5, 0x1

    #@126
    iput v5, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->retryNum:I

    #@128
    .line 1504
    iget-object v5, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@12a
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/cdma/CDMAPhone;->obtainMessage(I)Landroid/os/Message;

    #@12d
    move-result-object v6

    #@12e
    invoke-interface {v5, v6}, Lcom/android/internal/telephony/CommandsInterface;->getDeviceIdentity(Landroid/os/Message;)V

    #@131
    goto/16 :goto_32

    #@133
    .line 1511
    :sswitch_133
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@135
    check-cast v1, Landroid/os/AsyncResult;

    #@137
    .line 1512
    .restart local v1       #ar:Landroid/os/AsyncResult;
    iget-object v5, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@139
    if-nez v5, :cond_14f

    #@13b
    .line 1513
    const-string v5, "CDMA"

    #@13d
    const-string v6, "Event EVENT_EGSTK_SETUP_OTASP_CALL Received"

    #@13f
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@142
    .line 1514
    const-string v0, "lge.phone.action.OTA_TRIGGERED"

    #@144
    .line 1515
    .local v0, ACTION_OTA_TRIGGERED:Ljava/lang/String;
    new-instance v3, Landroid/content/Intent;

    #@146
    invoke-direct {v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@149
    .line 1516
    .local v3, ota_intent:Landroid/content/Intent;
    const/4 v5, -0x1

    #@14a
    invoke-static {v3, v7, v5}, Landroid/app/ActivityManagerNative;->broadcastStickyIntent(Landroid/content/Intent;Ljava/lang/String;I)V

    #@14d
    goto/16 :goto_32

    #@14f
    .line 1518
    .end local v0           #ACTION_OTA_TRIGGERED:Ljava/lang/String;
    .end local v3           #ota_intent:Landroid/content/Intent;
    :cond_14f
    const-string v5, "CDMA"

    #@151
    const-string v6, "exception in EVENT_EGSTK_SETUP_OTASP_CALL!"

    #@153
    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@156
    goto/16 :goto_32

    #@158
    .line 1524
    .end local v1           #ar:Landroid/os/AsyncResult;
    :sswitch_158
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/cdma/CDMAPhone;->handleEnterEmergencyCallbackMode(Landroid/os/Message;)V

    #@15b
    goto/16 :goto_32

    #@15d
    .line 1529
    :sswitch_15d
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/cdma/CDMAPhone;->handleExitEmergencyCallbackMode(Landroid/os/Message;)V

    #@160
    goto/16 :goto_32

    #@162
    .line 1534
    :sswitch_162
    const-string v5, "CDMA"

    #@164
    const-string v6, "Event EVENT_RUIM_RECORDS_LOADED Received"

    #@166
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@169
    .line 1535
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->updateCurrentCarrierInProvider()Z

    #@16c
    .line 1538
    iget-object v5, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@16e
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/cdma/CDMAPhone;->obtainMessage(I)Landroid/os/Message;

    #@171
    move-result-object v6

    #@172
    invoke-interface {v5, v6}, Lcom/android/internal/telephony/CommandsInterface;->getDeviceIdentity(Landroid/os/Message;)V

    #@175
    goto/16 :goto_32

    #@177
    .line 1544
    :sswitch_177
    const-string v5, "CDMA"

    #@179
    const-string v6, "Event EVENT_RADIO_OFF_OR_NOT_AVAILABLE Received"

    #@17b
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@17e
    goto/16 :goto_32

    #@180
    .line 1549
    :sswitch_180
    const-string v5, "CDMA"

    #@182
    const-string v6, "Event EVENT_RADIO_ON Received"

    #@184
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@187
    .line 1550
    iget-object v5, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mCdmaSSM:Lcom/android/internal/telephony/cdma/CdmaSubscriptionSourceManager;

    #@189
    invoke-virtual {v5}, Lcom/android/internal/telephony/cdma/CdmaSubscriptionSourceManager;->getCdmaSubscriptionSource()I

    #@18c
    move-result v5

    #@18d
    invoke-direct {p0, v5}, Lcom/android/internal/telephony/cdma/CDMAPhone;->handleCdmaSubscriptionSource(I)V

    #@190
    goto/16 :goto_32

    #@192
    .line 1555
    :sswitch_192
    const-string v5, "CDMA"

    #@194
    const-string v6, "EVENT_CDMA_SUBSCRIPTION_SOURCE_CHANGED"

    #@196
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@199
    .line 1556
    iget-object v5, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mCdmaSSM:Lcom/android/internal/telephony/cdma/CdmaSubscriptionSourceManager;

    #@19b
    invoke-virtual {v5}, Lcom/android/internal/telephony/cdma/CdmaSubscriptionSourceManager;->getCdmaSubscriptionSource()I

    #@19e
    move-result v5

    #@19f
    invoke-direct {p0, v5}, Lcom/android/internal/telephony/cdma/CDMAPhone;->handleCdmaSubscriptionSource(I)V

    #@1a2
    goto/16 :goto_32

    #@1a4
    .line 1561
    :sswitch_1a4
    const-string v5, "CDMA"

    #@1a6
    const-string v6, "Event EVENT_SSN Received"

    #@1a8
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1ab
    goto/16 :goto_32

    #@1ad
    .line 1566
    :sswitch_1ad
    const-string v5, "CDMA"

    #@1af
    const-string v6, "Event EVENT_REGISTERED_TO_NETWORK Received"

    #@1b1
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1b4
    goto/16 :goto_32

    #@1b6
    .line 1571
    :sswitch_1b6
    const-string v5, "CDMA"

    #@1b8
    const-string v6, "Event EVENT_NV_READY Received"

    #@1ba
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1bd
    .line 1572
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->prepareEri()V

    #@1c0
    goto/16 :goto_32

    #@1c2
    .line 1577
    :sswitch_1c2
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1c4
    check-cast v1, Landroid/os/AsyncResult;

    #@1c6
    .line 1578
    .restart local v1       #ar:Landroid/os/AsyncResult;
    const-class v5, Lcom/android/internal/telephony/uicc/IccException;

    #@1c8
    iget-object v6, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@1ca
    invoke-virtual {v5, v6}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    #@1cd
    move-result v5

    #@1ce
    if-eqz v5, :cond_1d7

    #@1d0
    .line 1579
    iget-object v5, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mVmNumber:Ljava/lang/String;

    #@1d2
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/cdma/CDMAPhone;->storeVoiceMailNumber(Ljava/lang/String;)V

    #@1d5
    .line 1580
    iput-object v7, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@1d7
    .line 1582
    :cond_1d7
    iget-object v2, v1, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@1d9
    check-cast v2, Landroid/os/Message;

    #@1db
    .line 1583
    .local v2, onComplete:Landroid/os/Message;
    if-eqz v2, :cond_32

    #@1dd
    .line 1584
    iget-object v5, v1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@1df
    iget-object v6, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@1e1
    invoke-static {v2, v5, v6}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)Landroid/os/AsyncResult;

    #@1e4
    .line 1585
    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    #@1e7
    goto/16 :goto_32

    #@1e9
    .line 1432
    nop

    #@1ea
    :sswitch_data_1ea
    .sparse-switch
        0x1 -> :sswitch_3c
        0x2 -> :sswitch_1a4
        0x5 -> :sswitch_180
        0x6 -> :sswitch_a6
        0x8 -> :sswitch_177
        0x13 -> :sswitch_1ad
        0x14 -> :sswitch_1c2
        0x15 -> :sswitch_d3
        0x16 -> :sswitch_162
        0x17 -> :sswitch_1b6
        0x19 -> :sswitch_158
        0x1a -> :sswitch_15d
        0x1b -> :sswitch_192
        0x2e -> :sswitch_7b
        0x35 -> :sswitch_133
        0x38 -> :sswitch_108
    .end sparse-switch
.end method

.method public handlePinMmi(Ljava/lang/String;)Z
    .registers 6
    .parameter "dialString"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x0

    #@2
    .line 816
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mUiccApplication:Ljava/util/concurrent/atomic/AtomicReference;

    #@4
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@7
    move-result-object v1

    #@8
    check-cast v1, Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@a
    invoke-static {p1, p0, v1}, Lcom/android/internal/telephony/cdma/CdmaMmiCode;->newFromDialString(Ljava/lang/String;Lcom/android/internal/telephony/cdma/CDMAPhone;Lcom/android/internal/telephony/uicc/UiccCardApplication;)Lcom/android/internal/telephony/cdma/CdmaMmiCode;

    #@d
    move-result-object v0

    #@e
    .line 818
    .local v0, mmi:Lcom/android/internal/telephony/cdma/CdmaMmiCode;
    if-nez v0, :cond_19

    #@10
    .line 819
    const-string v1, "CDMA"

    #@12
    const-string v3, "Mmi is NULL!"

    #@14
    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@17
    move v1, v2

    #@18
    .line 828
    :goto_18
    return v1

    #@19
    .line 821
    :cond_19
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CdmaMmiCode;->isPinCommand()Z

    #@1c
    move-result v1

    #@1d
    if-eqz v1, :cond_33

    #@1f
    .line 822
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mPendingMmis:Ljava/util/ArrayList;

    #@21
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@24
    .line 823
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mMmiRegistrants:Landroid/os/RegistrantList;

    #@26
    new-instance v2, Landroid/os/AsyncResult;

    #@28
    invoke-direct {v2, v3, v0, v3}, Landroid/os/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@2b
    invoke-virtual {v1, v2}, Landroid/os/RegistrantList;->notifyRegistrants(Landroid/os/AsyncResult;)V

    #@2e
    .line 824
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CdmaMmiCode;->processCode()V

    #@31
    .line 825
    const/4 v1, 0x1

    #@32
    goto :goto_18

    #@33
    .line 827
    :cond_33
    const-string v1, "CDMA"

    #@35
    const-string v3, "Unrecognized mmi!"

    #@37
    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    move v1, v2

    #@3b
    .line 828
    goto :goto_18
.end method

.method handleTimerInEmergencyCallbackMode(I)V
    .registers 7
    .parameter "action"

    #@0
    .prologue
    .line 1392
    packed-switch p1, :pswitch_data_40

    #@3
    .line 1404
    const-string v2, "CDMA"

    #@5
    new-instance v3, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v4, "handleTimerInEmergencyCallbackMode, unsupported action "

    #@c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v3

    #@10
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v3

    #@14
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v3

    #@18
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 1406
    :goto_1b
    return-void

    #@1c
    .line 1394
    :pswitch_1c
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mExitEcmRunnable:Ljava/lang/Runnable;

    #@1e
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/cdma/CDMAPhone;->removeCallbacks(Ljava/lang/Runnable;)V

    #@21
    .line 1395
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mEcmTimerResetRegistrants:Landroid/os/RegistrantList;

    #@23
    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    #@25
    invoke-virtual {v2, v3}, Landroid/os/RegistrantList;->notifyResult(Ljava/lang/Object;)V

    #@28
    goto :goto_1b

    #@29
    .line 1398
    :pswitch_29
    const-string v2, "ro.cdma.ecmexittimer"

    #@2b
    const-wide/32 v3, 0x493e0

    #@2e
    invoke-static {v2, v3, v4}, Landroid/os/SystemProperties;->getLong(Ljava/lang/String;J)J

    #@31
    move-result-wide v0

    #@32
    .line 1400
    .local v0, delayInMillis:J
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mExitEcmRunnable:Ljava/lang/Runnable;

    #@34
    invoke-virtual {p0, v2, v0, v1}, Lcom/android/internal/telephony/cdma/CDMAPhone;->postDelayed(Ljava/lang/Runnable;J)Z

    #@37
    .line 1401
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mEcmTimerResetRegistrants:Landroid/os/RegistrantList;

    #@39
    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    #@3b
    invoke-virtual {v2, v3}, Landroid/os/RegistrantList;->notifyResult(Ljava/lang/Object;)V

    #@3e
    goto :goto_1b

    #@3f
    .line 1392
    nop

    #@40
    :pswitch_data_40
    .packed-switch 0x0
        :pswitch_29
        :pswitch_1c
    .end packed-switch
.end method

.method public hasIsim()Z
    .registers 2

    #@0
    .prologue
    .line 2282
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method protected init(Landroid/content/Context;Lcom/android/internal/telephony/PhoneNotifier;)V
    .registers 14
    .parameter "context"
    .parameter "notifier"

    #@0
    .prologue
    .line 225
    iget-object v7, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    const/4 v8, 0x2

    #@3
    invoke-interface {v7, v8}, Lcom/android/internal/telephony/CommandsInterface;->setPhoneType(I)V

    #@6
    .line 226
    new-instance v7, Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@8
    invoke-direct {v7, p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;-><init>(Lcom/android/internal/telephony/cdma/CDMAPhone;)V

    #@b
    iput-object v7, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mCT:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@d
    .line 227
    iget-object v7, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@f
    const/16 v8, 0x1b

    #@11
    const/4 v9, 0x0

    #@12
    invoke-static {p1, v7, p0, v8, v9}, Lcom/android/internal/telephony/cdma/CdmaSubscriptionSourceManager;->getInstance(Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;Landroid/os/Handler;ILjava/lang/Object;)Lcom/android/internal/telephony/cdma/CdmaSubscriptionSourceManager;

    #@15
    move-result-object v7

    #@16
    iput-object v7, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mCdmaSSM:Lcom/android/internal/telephony/cdma/CdmaSubscriptionSourceManager;

    #@18
    .line 230
    const-string v7, "KDDI"

    #@1a
    invoke-static {v7}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@1d
    move-result v7

    #@1e
    if-eqz v7, :cond_1ac

    #@20
    .line 231
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@23
    move-result-object v7

    #@24
    const-string v8, "preferred_network_mode"

    #@26
    const/16 v9, 0xa

    #@28
    invoke-static {v7, v8, v9}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@2b
    move-result v3

    #@2c
    .line 236
    .local v3, networkMode:I
    const/4 v7, 0x5

    #@2d
    if-eq v3, v7, :cond_1a3

    #@2f
    .line 237
    new-instance v7, Lcom/android/internal/telephony/gsm/GsmDataConnectionTracker;

    #@31
    invoke-direct {v7, p0}, Lcom/android/internal/telephony/gsm/GsmDataConnectionTracker;-><init>(Lcom/android/internal/telephony/PhoneBase;)V

    #@34
    iput-object v7, p0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@36
    .line 245
    .end local v3           #networkMode:I
    :goto_36
    new-instance v7, Lcom/android/internal/telephony/cdma/RuimPhoneBookInterfaceManager;

    #@38
    invoke-direct {v7, p0}, Lcom/android/internal/telephony/cdma/RuimPhoneBookInterfaceManager;-><init>(Lcom/android/internal/telephony/cdma/CDMAPhone;)V

    #@3b
    iput-object v7, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mRuimPhoneBookInterfaceManager:Lcom/android/internal/telephony/cdma/RuimPhoneBookInterfaceManager;

    #@3d
    .line 246
    new-instance v7, Lcom/android/internal/telephony/PhoneSubInfo;

    #@3f
    invoke-direct {v7, p0}, Lcom/android/internal/telephony/PhoneSubInfo;-><init>(Lcom/android/internal/telephony/Phone;)V

    #@42
    iput-object v7, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSubInfo:Lcom/android/internal/telephony/PhoneSubInfo;

    #@44
    .line 248
    const/4 v7, 0x0

    #@45
    const-string v8, "vzw_eri"

    #@47
    invoke-static {v7, v8}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@4a
    move-result v7

    #@4b
    if-eqz v7, :cond_1b5

    #@4d
    .line 249
    new-instance v7, Lcom/android/internal/telephony/cdma/EriManager;

    #@4f
    const/4 v8, 0x1

    #@50
    invoke-direct {v7, p0, p1, v8}, Lcom/android/internal/telephony/cdma/EriManager;-><init>(Lcom/android/internal/telephony/PhoneBase;Landroid/content/Context;I)V

    #@53
    iput-object v7, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mEriManager:Lcom/android/internal/telephony/cdma/EriManager;

    #@55
    .line 255
    :goto_55
    iget-object v7, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@57
    const/4 v8, 0x1

    #@58
    const/4 v9, 0x0

    #@59
    invoke-interface {v7, p0, v8, v9}, Lcom/android/internal/telephony/CommandsInterface;->registerForAvailable(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5c
    .line 256
    iget-object v7, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@5e
    const/16 v8, 0x8

    #@60
    const/4 v9, 0x0

    #@61
    invoke-interface {v7, p0, v8, v9}, Lcom/android/internal/telephony/CommandsInterface;->registerForOffOrNotAvailable(Landroid/os/Handler;ILjava/lang/Object;)V

    #@64
    .line 257
    iget-object v7, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@66
    const/4 v8, 0x5

    #@67
    const/4 v9, 0x0

    #@68
    invoke-interface {v7, p0, v8, v9}, Lcom/android/internal/telephony/CommandsInterface;->registerForOn(Landroid/os/Handler;ILjava/lang/Object;)V

    #@6b
    .line 258
    iget-object v7, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@6d
    const/4 v8, 0x2

    #@6e
    const/4 v9, 0x0

    #@6f
    invoke-interface {v7, p0, v8, v9}, Lcom/android/internal/telephony/CommandsInterface;->setOnSuppServiceNotification(Landroid/os/Handler;ILjava/lang/Object;)V

    #@72
    .line 259
    iget-object v7, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSST:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@74
    const/16 v8, 0x13

    #@76
    const/4 v9, 0x0

    #@77
    invoke-virtual {v7, p0, v8, v9}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->registerForNetworkAttached(Landroid/os/Handler;ILjava/lang/Object;)V

    #@7a
    .line 260
    iget-object v7, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@7c
    const/16 v8, 0x19

    #@7e
    const/4 v9, 0x0

    #@7f
    invoke-interface {v7, p0, v8, v9}, Lcom/android/internal/telephony/CommandsInterface;->setEmergencyCallbackMode(Landroid/os/Handler;ILjava/lang/Object;)V

    #@82
    .line 261
    iget-object v7, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@84
    const/16 v8, 0x1a

    #@86
    const/4 v9, 0x0

    #@87
    invoke-interface {v7, p0, v8, v9}, Lcom/android/internal/telephony/CommandsInterface;->registerForExitEmergencyCallbackMode(Landroid/os/Handler;ILjava/lang/Object;)V

    #@8a
    .line 264
    const/4 v7, 0x0

    #@8b
    const-string v8, "ota_for_vzw"

    #@8d
    invoke-static {v7, v8}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@90
    move-result v7

    #@91
    if-eqz v7, :cond_9b

    #@93
    .line 265
    iget-object v7, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@95
    const/16 v8, 0x35

    #@97
    const/4 v9, 0x0

    #@98
    invoke-interface {v7, p0, v8, v9}, Lcom/android/internal/telephony/CommandsInterface;->setOnGstkSetupOtaspCall(Landroid/os/Handler;ILjava/lang/Object;)V

    #@9b
    .line 269
    :cond_9b
    const-string v7, "power"

    #@9d
    invoke-virtual {p1, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@a0
    move-result-object v6

    #@a1
    check-cast v6, Landroid/os/PowerManager;

    #@a3
    .line 271
    .local v6, pm:Landroid/os/PowerManager;
    const/4 v7, 0x1

    #@a4
    const-string v8, "CDMA"

    #@a6
    invoke-virtual {v6, v7, v8}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@a9
    move-result-object v7

    #@aa
    iput-object v7, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@ac
    .line 274
    const-string v7, "gsm.current.phone-type"

    #@ae
    const/4 v8, 0x2

    #@af
    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@b2
    move-result-object v8

    #@b3
    invoke-static {v7, v8}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@b6
    .line 278
    const-string v7, "ril.cdma.inecmmode"

    #@b8
    const-string v8, "false"

    #@ba
    invoke-static {v7, v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@bd
    move-result-object v0

    #@be
    .line 279
    .local v0, inEcm:Ljava/lang/String;
    const-string v7, "true"

    #@c0
    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c3
    move-result v7

    #@c4
    iput-boolean v7, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mIsPhoneInEcmState:Z

    #@c6
    .line 280
    iget-boolean v7, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mIsPhoneInEcmState:Z

    #@c8
    if-eqz v7, :cond_df

    #@ca
    .line 282
    const-string v7, "support_emergency_callback_mode_for_gsm"

    #@cc
    invoke-static {p1, v7}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@cf
    move-result v7

    #@d0
    if-eqz v7, :cond_1cc

    #@d2
    .line 283
    invoke-static {}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getEndTimeForEcm()J

    #@d5
    move-result-wide v1

    #@d6
    .line 284
    .local v1, mEcmTimeout:J
    const-wide/16 v7, 0x0

    #@d8
    cmp-long v7, v1, v7

    #@da
    if-lez v7, :cond_1bf

    #@dc
    .line 285
    invoke-direct {p0, v1, v2}, Lcom/android/internal/telephony/cdma/CDMAPhone;->handleEnterEmergencyCallbackMode(J)V

    #@df
    .line 298
    .end local v1           #mEcmTimeout:J
    :cond_df
    :goto_df
    const/4 v7, 0x0

    #@e0
    iput-boolean v7, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mIsPhoneInEcmExitDelayState:Z

    #@e2
    .line 302
    const-string v7, "ro.cdma.otaspnumschema"

    #@e4
    const-string v8, ""

    #@e6
    invoke-static {v7, v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@e9
    move-result-object v7

    #@ea
    iput-object v7, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mCarrierOtaSpNumSchema:Ljava/lang/String;

    #@ec
    .line 306
    const-string v7, "ro.cdma.home.operator.alpha"

    #@ee
    invoke-static {v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@f1
    move-result-object v4

    #@f2
    .line 307
    .local v4, operatorAlpha:Ljava/lang/String;
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@f5
    move-result-object v7

    #@f6
    const-string v8, "LGU"

    #@f8
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@fb
    move-result v7

    #@fc
    if-nez v7, :cond_103

    #@fe
    .line 308
    const-string v7, "gsm.sim.operator.alpha"

    #@100
    invoke-virtual {p0, v7, v4}, Lcom/android/internal/telephony/cdma/CDMAPhone;->setSystemProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@103
    .line 311
    :cond_103
    sget-object v7, Lcom/android/internal/telephony/cdma/CDMAPhone;->PROPERTY_CDMA_HOME_OPERATOR_NUMERIC:Ljava/lang/String;

    #@105
    invoke-static {v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@108
    move-result-object v5

    #@109
    .line 313
    .local v5, operatorNumeric:Ljava/lang/String;
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@10c
    move-result-object v7

    #@10d
    const-string v8, "LGU"

    #@10f
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@112
    move-result v7

    #@113
    if-nez v7, :cond_121

    #@115
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@118
    move-result-object v7

    #@119
    const-string v8, "VZW"

    #@11b
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11e
    move-result v7

    #@11f
    if-eqz v7, :cond_127

    #@121
    .line 314
    :cond_121
    const-string v7, "gsm.sim.operator.numeric"

    #@123
    invoke-static {v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@126
    move-result-object v5

    #@127
    .line 318
    :cond_127
    new-instance v7, Ljava/lang/StringBuilder;

    #@129
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@12c
    const-string v8, "CDMAPhone: init set \'gsm.sim.operator.numeric\' to operator=\'"

    #@12e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@131
    move-result-object v7

    #@132
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@135
    move-result-object v7

    #@136
    const-string v8, "\'"

    #@138
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13b
    move-result-object v7

    #@13c
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13f
    move-result-object v7

    #@140
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/cdma/CDMAPhone;->log(Ljava/lang/String;)V

    #@143
    .line 321
    const-string v7, "VZW"

    #@145
    invoke-static {v7}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@148
    move-result v7

    #@149
    if-eqz v7, :cond_1d9

    #@14b
    const/4 v7, 0x1

    #@14c
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@14f
    move-result-object v8

    #@150
    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@153
    move-result-object v8

    #@154
    const-string v9, "apn2_disable"

    #@156
    const/4 v10, 0x0

    #@157
    invoke-static {v8, v9, v10}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@15a
    move-result v8

    #@15b
    if-ne v7, v8, :cond_1d9

    #@15d
    .line 323
    const-string v7, "gsm.sim.operator.numeric"

    #@15f
    const-string v8, "311480"

    #@161
    invoke-virtual {p0, v7, v8}, Lcom/android/internal/telephony/cdma/CDMAPhone;->setSystemProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@164
    .line 329
    :goto_164
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/cdma/CDMAPhone;->setIsoCountryProperty(Ljava/lang/String;)V

    #@167
    .line 332
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/cdma/CDMAPhone;->updateCurrentCarrierInProvider(Ljava/lang/String;)Z

    #@16a
    .line 335
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->updateVoiceMail()V

    #@16d
    .line 339
    const/4 v7, 0x2

    #@16e
    invoke-static {p1, v7}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->init(Landroid/content/Context;I)V

    #@171
    .line 342
    iget-object v7, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->SimStateReceiver:Landroid/content/BroadcastReceiver;

    #@173
    new-instance v8, Landroid/content/IntentFilter;

    #@175
    const-string v9, "android.intent.action.SIM_STATE_CHANGED"

    #@177
    invoke-direct {v8, v9}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@17a
    const/4 v9, 0x0

    #@17b
    const/4 v10, 0x0

    #@17c
    invoke-virtual {p1, v7, v8, v9, v10}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@17f
    .line 345
    sget-boolean v7, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->MULTI_SIM_ENABLED:Z

    #@181
    if-eqz v7, :cond_191

    #@183
    .line 346
    iget-object v7, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->SimStateReceiver:Landroid/content/BroadcastReceiver;

    #@185
    new-instance v8, Landroid/content/IntentFilter;

    #@187
    const-string v9, "qualcomm.intent.action.ACTION_DEFAULT_SUBSCRIPTION_CHANGED"

    #@189
    invoke-direct {v8, v9}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@18c
    const/4 v9, 0x0

    #@18d
    const/4 v10, 0x0

    #@18e
    invoke-virtual {p1, v7, v8, v9, v10}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@191
    .line 354
    :cond_191
    const-string v7, "vzw_gfit"

    #@193
    invoke-static {p1, v7}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@196
    move-result v7

    #@197
    if-eqz v7, :cond_1a2

    #@199
    .line 355
    new-instance v7, Lcom/android/internal/telephony/gfit/GfitUtils;

    #@19b
    iget-object v8, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSST:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@19d
    invoke-direct {v7, v8, p0}, Lcom/android/internal/telephony/gfit/GfitUtils;-><init>(Lcom/android/internal/telephony/ServiceStateTracker;Lcom/android/internal/telephony/PhoneBase;)V

    #@1a0
    iput-object v7, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->gfUtils:Lcom/android/internal/telephony/gfit/GfitUtils;

    #@1a2
    .line 358
    :cond_1a2
    return-void

    #@1a3
    .line 239
    .end local v0           #inEcm:Ljava/lang/String;
    .end local v4           #operatorAlpha:Ljava/lang/String;
    .end local v5           #operatorNumeric:Ljava/lang/String;
    .end local v6           #pm:Landroid/os/PowerManager;
    .restart local v3       #networkMode:I
    :cond_1a3
    new-instance v7, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;

    #@1a5
    invoke-direct {v7, p0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;-><init>(Lcom/android/internal/telephony/cdma/CDMAPhone;)V

    #@1a8
    iput-object v7, p0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@1aa
    goto/16 :goto_36

    #@1ac
    .line 244
    .end local v3           #networkMode:I
    :cond_1ac
    new-instance v7, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;

    #@1ae
    invoke-direct {v7, p0}, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;-><init>(Lcom/android/internal/telephony/cdma/CDMAPhone;)V

    #@1b1
    iput-object v7, p0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@1b3
    goto/16 :goto_36

    #@1b5
    .line 251
    :cond_1b5
    new-instance v7, Lcom/android/internal/telephony/cdma/EriManager;

    #@1b7
    const/4 v8, 0x0

    #@1b8
    invoke-direct {v7, p0, p1, v8}, Lcom/android/internal/telephony/cdma/EriManager;-><init>(Lcom/android/internal/telephony/PhoneBase;Landroid/content/Context;I)V

    #@1bb
    iput-object v7, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mEriManager:Lcom/android/internal/telephony/cdma/EriManager;

    #@1bd
    goto/16 :goto_55

    #@1bf
    .line 288
    .restart local v0       #inEcm:Ljava/lang/String;
    .restart local v1       #mEcmTimeout:J
    .restart local v6       #pm:Landroid/os/PowerManager;
    :cond_1bf
    iget-object v7, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@1c1
    const/16 v8, 0x1a

    #@1c3
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/cdma/CDMAPhone;->obtainMessage(I)Landroid/os/Message;

    #@1c6
    move-result-object v8

    #@1c7
    invoke-interface {v7, v8}, Lcom/android/internal/telephony/CommandsInterface;->exitEmergencyCallbackMode(Landroid/os/Message;)V

    #@1ca
    goto/16 :goto_df

    #@1cc
    .line 294
    .end local v1           #mEcmTimeout:J
    :cond_1cc
    iget-object v7, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@1ce
    const/16 v8, 0x1a

    #@1d0
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/cdma/CDMAPhone;->obtainMessage(I)Landroid/os/Message;

    #@1d3
    move-result-object v8

    #@1d4
    invoke-interface {v7, v8}, Lcom/android/internal/telephony/CommandsInterface;->exitEmergencyCallbackMode(Landroid/os/Message;)V

    #@1d7
    goto/16 :goto_df

    #@1d9
    .line 325
    .restart local v4       #operatorAlpha:Ljava/lang/String;
    .restart local v5       #operatorNumeric:Ljava/lang/String;
    :cond_1d9
    const-string v7, "gsm.sim.operator.numeric"

    #@1db
    invoke-virtual {p0, v7, v5}, Lcom/android/internal/telephony/cdma/CDMAPhone;->setSystemProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@1de
    goto :goto_164
.end method

.method protected initSstIcc()V
    .registers 2

    #@0
    .prologue
    .line 221
    new-instance v0, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@2
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;-><init>(Lcom/android/internal/telephony/cdma/CDMAPhone;)V

    #@5
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSST:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@7
    .line 222
    return-void
.end method

.method public isEriFileLoaded()Z
    .registers 2

    #@0
    .prologue
    .line 2094
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mEriManager:Lcom/android/internal/telephony/cdma/EriManager;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/EriManager;->isEriFileLoaded()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method isInCall()Z
    .registers 5

    #@0
    .prologue
    .line 629
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getForegroundCall()Lcom/android/internal/telephony/cdma/CdmaCall;

    #@3
    move-result-object v3

    #@4
    invoke-virtual {v3}, Lcom/android/internal/telephony/cdma/CdmaCall;->getState()Lcom/android/internal/telephony/Call$State;

    #@7
    move-result-object v1

    #@8
    .line 630
    .local v1, foregroundCallState:Lcom/android/internal/telephony/Call$State;
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getBackgroundCall()Lcom/android/internal/telephony/cdma/CdmaCall;

    #@b
    move-result-object v3

    #@c
    invoke-virtual {v3}, Lcom/android/internal/telephony/cdma/CdmaCall;->getState()Lcom/android/internal/telephony/Call$State;

    #@f
    move-result-object v0

    #@10
    .line 631
    .local v0, backgroundCallState:Lcom/android/internal/telephony/Call$State;
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getRingingCall()Lcom/android/internal/telephony/cdma/CdmaCall;

    #@13
    move-result-object v3

    #@14
    invoke-virtual {v3}, Lcom/android/internal/telephony/cdma/CdmaCall;->getState()Lcom/android/internal/telephony/Call$State;

    #@17
    move-result-object v2

    #@18
    .line 633
    .local v2, ringingCallState:Lcom/android/internal/telephony/Call$State;
    invoke-virtual {v1}, Lcom/android/internal/telephony/Call$State;->isAlive()Z

    #@1b
    move-result v3

    #@1c
    if-nez v3, :cond_2a

    #@1e
    invoke-virtual {v0}, Lcom/android/internal/telephony/Call$State;->isAlive()Z

    #@21
    move-result v3

    #@22
    if-nez v3, :cond_2a

    #@24
    invoke-virtual {v2}, Lcom/android/internal/telephony/Call$State;->isAlive()Z

    #@27
    move-result v3

    #@28
    if-eqz v3, :cond_2c

    #@2a
    :cond_2a
    const/4 v3, 0x1

    #@2b
    :goto_2b
    return v3

    #@2c
    :cond_2c
    const/4 v3, 0x0

    #@2d
    goto :goto_2b
.end method

.method public isInEcm()Z
    .registers 2

    #@0
    .prologue
    .line 1252
    iget-boolean v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mIsPhoneInEcmState:Z

    #@2
    return v0
.end method

.method public isInEcmExitDelay()Z
    .registers 2

    #@0
    .prologue
    .line 1257
    iget-boolean v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mIsPhoneInEcmExitDelayState:Z

    #@2
    return v0
.end method

.method public isInEmergencyCall()Z
    .registers 2

    #@0
    .prologue
    .line 1248
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mCT:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->isInEmergencyCall()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isMinInfoReady()Z
    .registers 2

    #@0
    .prologue
    .line 708
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSST:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->isMinInfoReady()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isOtaSpNumber(Ljava/lang/String;)Z
    .registers 7
    .parameter "dialStr"

    #@0
    .prologue
    .line 1928
    const/4 v2, 0x0

    #@1
    const-string v3, "NotSupportOtaSpNumber"

    #@3
    invoke-static {v2, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@6
    move-result v2

    #@7
    if-eqz v2, :cond_b

    #@9
    .line 1929
    const/4 v1, 0x0

    #@a
    .line 1941
    :goto_a
    return v1

    #@b
    .line 1932
    :cond_b
    const/4 v1, 0x0

    #@c
    .line 1933
    .local v1, isOtaSpNum:Z
    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->extractNetworkPortionAlt(Ljava/lang/String;)Ljava/lang/String;

    #@f
    move-result-object v0

    #@10
    .line 1934
    .local v0, dialableStr:Ljava/lang/String;
    if-eqz v0, :cond_1c

    #@12
    .line 1935
    invoke-static {v0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->isIs683OtaSpDialStr(Ljava/lang/String;)Z

    #@15
    move-result v1

    #@16
    .line 1936
    if-nez v1, :cond_1c

    #@18
    .line 1937
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->isCarrierOtaSpNum(Ljava/lang/String;)Z

    #@1b
    move-result v1

    #@1c
    .line 1940
    :cond_1c
    const-string v2, "CDMA"

    #@1e
    new-instance v3, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v4, "isOtaSpNumber "

    #@25
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v3

    #@29
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v3

    #@2d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v3

    #@31
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    goto :goto_a
.end method

.method protected log(Ljava/lang/String;)V
    .registers 5
    .parameter "s"

    #@0
    .prologue
    .line 2127
    const-string v0, "CDMA"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[CDMAPhone] "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 2128
    return-void
.end method

.method public makeEngineeringModeInfo()V
    .registers 1

    #@0
    .prologue
    .line 2021
    return-void
.end method

.method public needsOtaServiceProvisioning()Z
    .registers 3

    #@0
    .prologue
    .line 1726
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSST:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->getOtasp()I

    #@5
    move-result v0

    #@6
    const/4 v1, 0x3

    #@7
    if-eq v0, v1, :cond_b

    #@9
    const/4 v0, 0x1

    #@a
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method notifyDisconnect(Lcom/android/internal/telephony/Connection;)V
    .registers 3
    .parameter "cn"

    #@0
    .prologue
    .line 1240
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mDisconnectRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->notifyResult(Ljava/lang/Object;)V

    #@5
    .line 1241
    return-void
.end method

.method notifyLocationChanged()V
    .registers 2

    #@0
    .prologue
    .line 1231
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mNotifier:Lcom/android/internal/telephony/PhoneNotifier;

    #@2
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/PhoneNotifier;->notifyCellLocation(Lcom/android/internal/telephony/Phone;)V

    #@5
    .line 1232
    return-void
.end method

.method notifyNewRingingConnection(Lcom/android/internal/telephony/Connection;)V
    .registers 2
    .parameter "c"

    #@0
    .prologue
    .line 1236
    invoke-super {p0, p1}, Lcom/android/internal/telephony/PhoneBase;->notifyNewRingingConnectionP(Lcom/android/internal/telephony/Connection;)V

    #@3
    .line 1237
    return-void
.end method

.method notifyPhoneStateChanged()V
    .registers 2

    #@0
    .prologue
    .line 1214
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mNotifier:Lcom/android/internal/telephony/PhoneNotifier;

    #@2
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/PhoneNotifier;->notifyPhoneState(Lcom/android/internal/telephony/Phone;)V

    #@5
    .line 1215
    return-void
.end method

.method notifyPreciseCallStateChanged()V
    .registers 1

    #@0
    .prologue
    .line 1223
    invoke-super {p0}, Lcom/android/internal/telephony/PhoneBase;->notifyPreciseCallStateChangedP()V

    #@3
    .line 1224
    return-void
.end method

.method notifyServiceStateChanged(Landroid/telephony/ServiceState;)V
    .registers 2
    .parameter "ss"

    #@0
    .prologue
    .line 1227
    invoke-super {p0, p1}, Lcom/android/internal/telephony/PhoneBase;->notifyServiceStateChangedP(Landroid/telephony/ServiceState;)V

    #@3
    .line 1228
    return-void
.end method

.method notifyUnknownConnection()V
    .registers 2

    #@0
    .prologue
    .line 1244
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mUnknownConnectionRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p0}, Landroid/os/RegistrantList;->notifyResult(Ljava/lang/Object;)V

    #@5
    .line 1245
    return-void
.end method

.method onMMIDone(Lcom/android/internal/telephony/cdma/CdmaMmiCode;)V
    .registers 5
    .parameter "mmi"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 842
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mPendingMmis:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_13

    #@9
    .line 843
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mMmiCompleteRegistrants:Landroid/os/RegistrantList;

    #@b
    new-instance v1, Landroid/os/AsyncResult;

    #@d
    invoke-direct {v1, v2, p1, v2}, Landroid/os/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@10
    invoke-virtual {v0, v1}, Landroid/os/RegistrantList;->notifyRegistrants(Landroid/os/AsyncResult;)V

    #@13
    .line 845
    :cond_13
    return-void
.end method

.method protected onUpdateIccAvailability()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1615
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneBase;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@3
    if-nez v2, :cond_6

    #@5
    .line 1642
    :cond_5
    :goto_5
    return-void

    #@6
    .line 1621
    :cond_6
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->setCardInPhoneBook()V

    #@9
    .line 1623
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getUiccCardApplication()Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@c
    move-result-object v1

    #@d
    .line 1625
    .local v1, newUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneBase;->mUiccApplication:Ljava/util/concurrent/atomic/AtomicReference;

    #@f
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@12
    move-result-object v0

    #@13
    check-cast v0, Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@15
    .line 1626
    .local v0, app:Lcom/android/internal/telephony/uicc/UiccCardApplication;
    if-eq v0, v1, :cond_5

    #@17
    .line 1627
    if-eqz v0, :cond_33

    #@19
    .line 1628
    const-string v2, "Removing stale icc objects."

    #@1b
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/cdma/CDMAPhone;->log(Ljava/lang/String;)V

    #@1e
    .line 1629
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@20
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@23
    move-result-object v2

    #@24
    if-eqz v2, :cond_29

    #@26
    .line 1630
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->unregisterForRuimRecordEvents()V

    #@29
    .line 1632
    :cond_29
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@2b
    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    #@2e
    .line 1633
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneBase;->mUiccApplication:Ljava/util/concurrent/atomic/AtomicReference;

    #@30
    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    #@33
    .line 1635
    :cond_33
    if-eqz v1, :cond_5

    #@35
    .line 1636
    const-string v2, "New Uicc application found"

    #@37
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/cdma/CDMAPhone;->log(Ljava/lang/String;)V

    #@3a
    .line 1637
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneBase;->mUiccApplication:Ljava/util/concurrent/atomic/AtomicReference;

    #@3c
    invoke-virtual {v2, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    #@3f
    .line 1638
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@41
    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getIccRecords()Lcom/android/internal/telephony/uicc/IccRecords;

    #@44
    move-result-object v3

    #@45
    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    #@48
    .line 1639
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->registerForRuimRecordEvents()V

    #@4b
    goto :goto_5
.end method

.method public prepareEri()V
    .registers 3

    #@0
    .prologue
    .line 2081
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mEriManager:Lcom/android/internal/telephony/cdma/EriManager;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 2082
    const-string v0, "CDMA"

    #@6
    const-string v1, "Trying to access stale objects"

    #@8
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 2091
    :cond_b
    :goto_b
    return-void

    #@c
    .line 2085
    :cond_c
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mEriManager:Lcom/android/internal/telephony/cdma/EriManager;

    #@e
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/EriManager;->loadEriFile()V

    #@11
    .line 2086
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mEriManager:Lcom/android/internal/telephony/cdma/EriManager;

    #@13
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/EriManager;->isEriFileLoaded()Z

    #@16
    move-result v0

    #@17
    if-eqz v0, :cond_b

    #@19
    .line 2088
    const-string v0, "ERI read, notify registrants"

    #@1b
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->log(Ljava/lang/String;)V

    #@1e
    .line 2089
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mEriFileLoadedRegistrants:Landroid/os/RegistrantList;

    #@20
    invoke-virtual {v0}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@23
    goto :goto_b
.end method

.method public registerForCallWaiting(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 916
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mCT:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->registerForCallWaiting(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 917
    return-void
.end method

.method public registerForCdmaOtaStatusChange(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 892
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/CommandsInterface;->registerForCdmaOtaProvision(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 893
    return-void
.end method

.method public registerForEcmTimerReset(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1415
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mEcmTimerResetRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/os/RegistrantList;->addUnique(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1416
    return-void
.end method

.method public registerForEriFileLoaded(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1674
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1675
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mEriFileLoadedRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 1676
    return-void
.end method

.method protected registerForRuimRecordEvents()V
    .registers 4

    #@0
    .prologue
    .line 2098
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@2
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/telephony/uicc/IccRecords;

    #@8
    .line 2099
    .local v0, r:Lcom/android/internal/telephony/uicc/IccRecords;
    if-nez v0, :cond_b

    #@a
    .line 2103
    :goto_a
    return-void

    #@b
    .line 2102
    :cond_b
    const/16 v1, 0x16

    #@d
    const/4 v2, 0x0

    #@e
    invoke-virtual {v0, p0, v1, v2}, Lcom/android/internal/telephony/uicc/IccRecords;->registerForRecordsLoaded(Landroid/os/Handler;ILjava/lang/Object;)V

    #@11
    goto :goto_a
.end method

.method public registerForSubscriptionInfoReady(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 900
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSST:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->registerForSubscriptionInfoReady(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 901
    return-void
.end method

.method public registerForSuppServiceNotification(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 616
    const-string v0, "CDMA"

    #@2
    const-string v1, "method registerForSuppServiceNotification is NOT supported in CDMA!"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 617
    return-void
.end method

.method public rejectCall()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 661
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mCT:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->rejectCall()V

    #@5
    .line 662
    return-void
.end method

.method public removeReferences()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 468
    const-string v0, "removeReferences"

    #@3
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->log(Ljava/lang/String;)V

    #@6
    .line 469
    iput-object v1, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mRuimPhoneBookInterfaceManager:Lcom/android/internal/telephony/cdma/RuimPhoneBookInterfaceManager;

    #@8
    .line 470
    iput-object v1, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSubInfo:Lcom/android/internal/telephony/PhoneSubInfo;

    #@a
    .line 471
    iput-object v1, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mCT:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@c
    .line 472
    iput-object v1, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSST:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@e
    .line 473
    iput-object v1, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mEriManager:Lcom/android/internal/telephony/cdma/EriManager;

    #@10
    .line 474
    iput-object v1, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mExitEcmRunnable:Ljava/lang/Runnable;

    #@12
    .line 475
    invoke-super {p0}, Lcom/android/internal/telephony/PhoneBase;->removeReferences()V

    #@15
    .line 479
    const-string v0, "vzw_gfit"

    #@17
    invoke-static {v1, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@1a
    move-result v0

    #@1b
    if-eqz v0, :cond_1f

    #@1d
    .line 480
    iput-object v1, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->gfUtils:Lcom/android/internal/telephony/gfit/GfitUtils;

    #@1f
    .line 483
    :cond_1f
    return-void
.end method

.method public resetVoiceMessageCount()V
    .registers 2

    #@0
    .prologue
    .line 2170
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->setVoiceMessageCount(I)V

    #@4
    .line 2171
    return-void
.end method

.method public selectNetworkManually(Lcom/android/internal/telephony/OperatorInfo;Landroid/os/Message;)V
    .registers 6
    .parameter "network"
    .parameter "response"

    #@0
    .prologue
    .line 784
    const-string v1, "CDMA"

    #@2
    const-string v2, "selectNetworkManually: not possible in CDMA"

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 785
    if-eqz p2, :cond_19

    #@9
    .line 786
    new-instance v0, Lcom/android/internal/telephony/CommandException;

    #@b
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->REQUEST_NOT_SUPPORTED:Lcom/android/internal/telephony/CommandException$Error;

    #@d
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CommandException;-><init>(Lcom/android/internal/telephony/CommandException$Error;)V

    #@10
    .line 788
    .local v0, ce:Lcom/android/internal/telephony/CommandException;
    invoke-static {p2}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;)Landroid/os/AsyncResult;

    #@13
    move-result-object v1

    #@14
    iput-object v0, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@16
    .line 789
    invoke-virtual {p2}, Landroid/os/Message;->sendToTarget()V

    #@19
    .line 791
    .end local v0           #ce:Lcom/android/internal/telephony/CommandException;
    :cond_19
    return-void
.end method

.method public selectPreviousNetworkManually(Lcom/android/internal/telephony/OperatorInfo;Landroid/os/Message;)V
    .registers 5
    .parameter "network"
    .parameter "response"

    #@0
    .prologue
    .line 801
    const-string v0, "CDMA"

    #@2
    const-string v1, "selectPreviousNetworkManually: not possible in CDMA"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 802
    return-void
.end method

.method public sendBurstDtmf(Ljava/lang/String;IILandroid/os/Message;)V
    .registers 10
    .parameter "dtmfString"
    .parameter "on"
    .parameter "off"
    .parameter "onComplete"

    #@0
    .prologue
    .line 1029
    const/4 v0, 0x1

    #@1
    .line 1030
    .local v0, check:Z
    const/4 v1, 0x0

    #@2
    .local v1, itr:I
    :goto_2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@5
    move-result v2

    #@6
    if-ge v1, v2, :cond_35

    #@8
    .line 1031
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    #@b
    move-result v2

    #@c
    invoke-static {v2}, Landroid/telephony/PhoneNumberUtils;->is12Key(C)Z

    #@f
    move-result v2

    #@10
    if-nez v2, :cond_45

    #@12
    .line 1032
    const-string v2, "CDMA"

    #@14
    new-instance v3, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v4, "sendDtmf called with invalid character \'"

    #@1b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    #@22
    move-result v4

    #@23
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    const-string v4, "\'"

    #@29
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v3

    #@2d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v3

    #@31
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    .line 1034
    const/4 v0, 0x0

    #@35
    .line 1038
    :cond_35
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mCT:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@37
    iget-object v2, v2, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->state:Lcom/android/internal/telephony/PhoneConstants$State;

    #@39
    sget-object v3, Lcom/android/internal/telephony/PhoneConstants$State;->OFFHOOK:Lcom/android/internal/telephony/PhoneConstants$State;

    #@3b
    if-ne v2, v3, :cond_44

    #@3d
    if-eqz v0, :cond_44

    #@3f
    .line 1039
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@41
    invoke-interface {v2, p1, p2, p3, p4}, Lcom/android/internal/telephony/CommandsInterface;->sendBurstDtmf(Ljava/lang/String;IILandroid/os/Message;)V

    #@44
    .line 1041
    :cond_44
    return-void

    #@45
    .line 1030
    :cond_45
    add-int/lit8 v1, v1, 0x1

    #@47
    goto :goto_2
.end method

.method public sendDtmf(C)V
    .registers 5
    .parameter "c"

    #@0
    .prologue
    .line 1005
    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->is12Key(C)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_25

    #@6
    .line 1006
    const-string v0, "CDMA"

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "sendDtmf called with invalid character \'"

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    const-string v2, "\'"

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 1013
    :cond_24
    :goto_24
    return-void

    #@25
    .line 1009
    :cond_25
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mCT:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@27
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->state:Lcom/android/internal/telephony/PhoneConstants$State;

    #@29
    sget-object v1, Lcom/android/internal/telephony/PhoneConstants$State;->OFFHOOK:Lcom/android/internal/telephony/PhoneConstants$State;

    #@2b
    if-ne v0, v1, :cond_24

    #@2d
    .line 1010
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2f
    const/4 v1, 0x0

    #@30
    invoke-interface {v0, p1, v1}, Lcom/android/internal/telephony/CommandsInterface;->sendDtmf(CLandroid/os/Message;)V

    #@33
    goto :goto_24
.end method

.method protected sendEmergencyCallbackModeChange()V
    .registers 4

    #@0
    .prologue
    .line 1263
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "android.intent.action.EMERGENCY_CALLBACK_MODE_CHANGED"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 1264
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "phoneinECMState"

    #@9
    iget-boolean v2, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mIsPhoneInEcmState:Z

    #@b
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@e
    .line 1265
    const/4 v1, 0x0

    #@f
    const/4 v2, -0x1

    #@10
    invoke-static {v0, v1, v2}, Landroid/app/ActivityManagerNative;->broadcastStickyIntent(Landroid/content/Intent;Ljava/lang/String;I)V

    #@13
    .line 1266
    const-string v1, "CDMA"

    #@15
    const-string v2, "sendEmergencyCallbackModeChange"

    #@17
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 1267
    return-void
.end method

.method public sendUssdResponse(Ljava/lang/String;)V
    .registers 4
    .parameter "ussdMessge"

    #@0
    .prologue
    .line 1001
    const-string v0, "CDMA"

    #@2
    const-string v1, "sendUssdResponse: not possible in CDMA"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1002
    return-void
.end method

.method public setCallBarringOption(ILjava/lang/String;ILjava/lang/String;Landroid/os/Message;)V
    .registers 12
    .parameter "commandInterfaceCBAction"
    .parameter "commandInterfaceCBReason"
    .parameter "serviceClass"
    .parameter "password"
    .parameter "onComplete"

    #@0
    .prologue
    .line 871
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/cdma/CDMAPhone;->isCbEnable(I)Z

    #@5
    move-result v2

    #@6
    move-object v1, p2

    #@7
    move-object v3, p4

    #@8
    move v4, p3

    #@9
    move-object v5, p5

    #@a
    invoke-interface/range {v0 .. v5}, Lcom/android/internal/telephony/CommandsInterface;->setFacilityLock(Ljava/lang/String;ZLjava/lang/String;ILandroid/os/Message;)V

    #@d
    .line 872
    return-void
.end method

.method public setCallBarringPass(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 6
    .parameter "oldPwd"
    .parameter "newPwd"
    .parameter "onComplete"

    #@0
    .prologue
    .line 879
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    const-string v1, "AB"

    #@4
    invoke-interface {v0, v1, p1, p2, p3}, Lcom/android/internal/telephony/CommandsInterface;->changeBarringPassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    #@7
    .line 880
    return-void
.end method

.method public setCallForwardingOption(IILjava/lang/String;IILandroid/os/Message;)V
    .registers 9
    .parameter "commandInterfaceCFAction"
    .parameter "commandInterfaceCFReason"
    .parameter "dialingNumber"
    .parameter "serviceClass"
    .parameter "timerSeconds"
    .parameter "onComplete"

    #@0
    .prologue
    .line 1186
    const-string v0, "CDMA"

    #@2
    const-string v1, "setCallForwardingOption: not possible in CDMA"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1187
    return-void
.end method

.method public setCallForwardingOption(IILjava/lang/String;ILandroid/os/Message;)V
    .registers 8
    .parameter "commandInterfaceCFAction"
    .parameter "commandInterfaceCFReason"
    .parameter "dialingNumber"
    .parameter "timerSeconds"
    .parameter "onComplete"

    #@0
    .prologue
    .line 1177
    const-string v0, "CDMA"

    #@2
    const-string v1, "setCallForwardingOption: not possible in CDMA"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1178
    return-void
.end method

.method public setCallWaiting(ZLandroid/os/Message;)V
    .registers 5
    .parameter "enable"
    .parameter "onComplete"

    #@0
    .prologue
    .line 852
    const-string v0, "CDMA"

    #@2
    const-string v1, "method setCallWaiting is NOT supported in CDMA!"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 853
    return-void
.end method

.method protected setCardInPhoneBook()V
    .registers 3

    #@0
    .prologue
    .line 1606
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@2
    if-nez v0, :cond_5

    #@4
    .line 1611
    :goto_4
    return-void

    #@5
    .line 1610
    :cond_5
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mRuimPhoneBookInterfaceManager:Lcom/android/internal/telephony/cdma/RuimPhoneBookInterfaceManager;

    #@7
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@9
    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/UiccController;->getUiccCard()Lcom/android/internal/telephony/uicc/UiccCard;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/RuimPhoneBookInterfaceManager;->setIccCard(Lcom/android/internal/telephony/uicc/UiccCard;)V

    #@10
    goto :goto_4
.end method

.method public setCdmaEriVersion(ILandroid/os/Message;)V
    .registers 4
    .parameter "value"
    .parameter "result"

    #@0
    .prologue
    .line 2040
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/CommandsInterface;->setCdmaEriVersion(ILandroid/os/Message;)V

    #@5
    .line 2041
    return-void
.end method

.method public setCdmaFactoryReset(Landroid/os/Message;)V
    .registers 4
    .parameter "onComplete"

    #@0
    .prologue
    .line 2176
    const-string v0, "CDMA"

    #@2
    const-string v1, "setCdmaFactoryReset"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 2177
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@9
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/CommandsInterface;->setCdmaFactoryReset(Landroid/os/Message;)V

    #@c
    .line 2178
    return-void
.end method

.method public setCellBroadcastSmsConfig([ILandroid/os/Message;)V
    .registers 5
    .parameter "configValuesArray"
    .parameter "response"

    #@0
    .prologue
    .line 1717
    const-string v0, "CDMA"

    #@2
    const-string v1, "[CDMAPhone] setCellBroadcastSmsConfig() is obsolete; use SmsManager"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1718
    invoke-virtual {p2}, Landroid/os/Message;->sendToTarget()V

    #@a
    .line 1719
    return-void
.end method

.method public setDataConnection(Z)V
    .registers 3
    .parameter "enable"

    #@0
    .prologue
    .line 2237
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/DataConnectionTracker;->setDataConnection(Z)V

    #@5
    .line 2238
    return-void
.end method

.method public setDataRoamingEnabled(Z)V
    .registers 3
    .parameter "enable"

    #@0
    .prologue
    .line 888
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/DataConnectionTracker;->setDataOnRoamingEnabled(Z)V

    #@5
    .line 889
    return-void
.end method

.method public setGbaBootstrappingParams([BLjava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 5
    .parameter "rand"
    .parameter "btid"
    .parameter "keyLifetime"
    .parameter "onComplete"

    #@0
    .prologue
    .line 2299
    if-eqz p4, :cond_5

    #@2
    invoke-virtual {p4}, Landroid/os/Message;->sendToTarget()V

    #@5
    .line 2300
    :cond_5
    return-void
.end method

.method protected setIsoCountryProperty(Ljava/lang/String;)V
    .registers 7
    .parameter "operatorNumeric"

    #@0
    .prologue
    .line 1999
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@3
    move-result v2

    #@4
    if-eqz v2, :cond_e

    #@6
    .line 2000
    const-string v2, "gsm.sim.operator.iso-country"

    #@8
    const-string v3, ""

    #@a
    invoke-virtual {p0, v2, v3}, Lcom/android/internal/telephony/cdma/CDMAPhone;->setSystemProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@d
    .line 2014
    :goto_d
    return-void

    #@e
    .line 2002
    :cond_e
    const-string v1, ""

    #@10
    .line 2004
    .local v1, iso:Ljava/lang/String;
    const/4 v2, 0x0

    #@11
    const/4 v3, 0x3

    #@12
    :try_start_12
    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@19
    move-result v2

    #@1a
    invoke-static {v2}, Lcom/android/internal/telephony/MccTable;->countryCodeForMcc(I)Ljava/lang/String;
    :try_end_1d
    .catch Ljava/lang/NumberFormatException; {:try_start_12 .. :try_end_1d} :catch_24
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_12 .. :try_end_1d} :catch_3e

    #@1d
    move-result-object v1

    #@1e
    .line 2012
    :goto_1e
    const-string v2, "gsm.sim.operator.iso-country"

    #@20
    invoke-virtual {p0, v2, v1}, Lcom/android/internal/telephony/cdma/CDMAPhone;->setSystemProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@23
    goto :goto_d

    #@24
    .line 2006
    :catch_24
    move-exception v0

    #@25
    .line 2007
    .local v0, ex:Ljava/lang/NumberFormatException;
    const-string v2, "CDMA"

    #@27
    new-instance v3, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const-string v4, "countryCodeForMcc error"

    #@2e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v3

    #@32
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v3

    #@36
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v3

    #@3a
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3d
    goto :goto_1e

    #@3e
    .line 2008
    .end local v0           #ex:Ljava/lang/NumberFormatException;
    :catch_3e
    move-exception v0

    #@3f
    .line 2009
    .local v0, ex:Ljava/lang/StringIndexOutOfBoundsException;
    const-string v2, "CDMA"

    #@41
    new-instance v3, Ljava/lang/StringBuilder;

    #@43
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@46
    const-string v4, "countryCodeForMcc error"

    #@48
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v3

    #@4c
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v3

    #@50
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@53
    move-result-object v3

    #@54
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@57
    goto :goto_1e
.end method

.method public setLine1Number(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 6
    .parameter "alphaTag"
    .parameter "number"
    .parameter "onComplete"

    #@0
    .prologue
    .line 848
    const-string v0, "CDMA"

    #@2
    const-string v1, "setLine1Number: not possible in CDMA"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 849
    return-void
.end method

.method public setMute(Z)V
    .registers 3
    .parameter "muted"

    #@0
    .prologue
    .line 537
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mCT:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->setMute(Z)V

    #@5
    .line 538
    return-void
.end method

.method public setNetworkSelectionModeAutomatic(Landroid/os/Message;)V
    .registers 5
    .parameter "response"

    #@0
    .prologue
    .line 639
    const-string v1, "CDMA"

    #@2
    const-string v2, "method setNetworkSelectionModeAutomatic is NOT supported in CDMA!"

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 640
    if-eqz p1, :cond_20

    #@9
    .line 641
    const-string v1, "CDMA"

    #@b
    const-string v2, "setNetworkSelectionModeAutomatic: not possible in CDMA- Posting exception"

    #@d
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 643
    new-instance v0, Lcom/android/internal/telephony/CommandException;

    #@12
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->REQUEST_NOT_SUPPORTED:Lcom/android/internal/telephony/CommandException$Error;

    #@14
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CommandException;-><init>(Lcom/android/internal/telephony/CommandException$Error;)V

    #@17
    .line 645
    .local v0, ce:Lcom/android/internal/telephony/CommandException;
    invoke-static {p1}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;)Landroid/os/AsyncResult;

    #@1a
    move-result-object v1

    #@1b
    iput-object v0, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@1d
    .line 646
    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    #@20
    .line 648
    .end local v0           #ce:Lcom/android/internal/telephony/CommandException;
    :cond_20
    return-void
.end method

.method public setOnEcbModeExitResponse(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 908
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mEcmExitRespRegistrant:Landroid/os/Registrant;

    #@7
    .line 909
    return-void
.end method

.method public setOnPostDialCharacter(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 812
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mPostDialHandler:Landroid/os/Registrant;

    #@7
    .line 813
    return-void
.end method

.method public setOutgoingCallerIdDisplay(ILandroid/os/Message;)V
    .registers 5
    .parameter "commandInterfaceCLIRMode"
    .parameter "onComplete"

    #@0
    .prologue
    .line 1054
    const-string v0, "CDMA"

    #@2
    const-string v1, "setOutgoingCallerIdDisplay: not possible in CDMA"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1055
    return-void
.end method

.method public setRadioPower(Z)V
    .registers 3
    .parameter "power"

    #@0
    .prologue
    .line 717
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSST:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->setRadioPower(Z)V

    #@5
    .line 718
    return-void
.end method

.method public setSystemProperty(Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .parameter "property"
    .parameter "value"

    #@0
    .prologue
    .line 1687
    invoke-super {p0, p1, p2}, Lcom/android/internal/telephony/PhoneBase;->setSystemProperty(Ljava/lang/String;Ljava/lang/String;)V

    #@3
    .line 1688
    return-void
.end method

.method public setVoiceMailNumber(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 9
    .parameter "alphaTag"
    .parameter "voiceMailNumber"
    .parameter "onComplete"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1077
    iput-object p2, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mVmNumber:Ljava/lang/String;

    #@3
    .line 1078
    const/16 v3, 0x14

    #@5
    invoke-virtual {p0, v3, v4, v4, p3}, Lcom/android/internal/telephony/cdma/CDMAPhone;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@8
    move-result-object v2

    #@9
    .line 1079
    .local v2, resp:Landroid/os/Message;
    iget-object v3, p0, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@b
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@e
    move-result-object v1

    #@f
    check-cast v1, Lcom/android/internal/telephony/uicc/IccRecords;

    #@11
    .line 1080
    .local v1, r:Lcom/android/internal/telephony/uicc/IccRecords;
    if-eqz v1, :cond_19

    #@13
    .line 1081
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mVmNumber:Ljava/lang/String;

    #@15
    invoke-virtual {v1, p1, v3, v2}, Lcom/android/internal/telephony/uicc/IccRecords;->setVoiceMailNumber(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    #@18
    .line 1094
    :cond_18
    :goto_18
    return-void

    #@19
    .line 1085
    :cond_19
    if-eqz p3, :cond_18

    #@1b
    .line 1086
    const-string v3, "CDMA"

    #@1d
    const-string v4, "setVoiceMailNumber() : Ruim is absent."

    #@1f
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 1087
    new-instance v0, Ljava/lang/RuntimeException;

    #@24
    const-string v3, "Ruim is absent."

    #@26
    invoke-direct {v0, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@29
    .line 1088
    .local v0, e:Ljava/lang/Exception;
    invoke-static {p3}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;)Landroid/os/AsyncResult;

    #@2c
    move-result-object v3

    #@2d
    iput-object v0, v3, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@2f
    .line 1089
    invoke-virtual {p3}, Landroid/os/Message;->sendToTarget()V

    #@32
    goto :goto_18
.end method

.method public startDtmf(C)V
    .registers 5
    .parameter "c"

    #@0
    .prologue
    .line 1016
    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->is12Key(C)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_25

    #@6
    .line 1017
    const-string v0, "CDMA"

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "startDtmf called with invalid character \'"

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    const-string v2, "\'"

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 1022
    :goto_24
    return-void

    #@25
    .line 1020
    :cond_25
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@27
    const/4 v1, 0x0

    #@28
    invoke-interface {v0, p1, v1}, Lcom/android/internal/telephony/CommandsInterface;->startDtmf(CLandroid/os/Message;)V

    #@2b
    goto :goto_24
.end method

.method public stopDtmf()V
    .registers 3

    #@0
    .prologue
    .line 1025
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/CommandsInterface;->stopDtmf(Landroid/os/Message;)V

    #@6
    .line 1026
    return-void
.end method

.method protected storeVoiceMailNumber(Ljava/lang/String;)V
    .registers 5
    .parameter "number"

    #@0
    .prologue
    .line 1988
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@3
    move-result-object v2

    #@4
    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    #@7
    move-result-object v1

    #@8
    .line 1989
    .local v1, sp:Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    #@b
    move-result-object v0

    #@c
    .line 1990
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v2, "vm_number_key_cdma"

    #@e
    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@11
    .line 1991
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    #@14
    .line 1992
    return-void
.end method

.method public switchHoldingAndActive()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 666
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mCT:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->switchWaitingOrHoldingAndActive()V

    #@5
    .line 667
    return-void
.end method

.method public unregisterForCallWaiting(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 920
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mCT:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->unregisterForCallWaiting(Landroid/os/Handler;)V

    #@5
    .line 921
    return-void
.end method

.method public unregisterForCdmaOtaStatusChange(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 896
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForCdmaOtaProvision(Landroid/os/Handler;)V

    #@5
    .line 897
    return-void
.end method

.method public unregisterForEcmTimerReset(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1419
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mEcmTimerResetRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 1420
    return-void
.end method

.method public unregisterForEriFileLoaded(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1679
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mEriFileLoadedRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 1680
    return-void
.end method

.method protected unregisterForRuimRecordEvents()V
    .registers 3

    #@0
    .prologue
    .line 2106
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@2
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/telephony/uicc/IccRecords;

    #@8
    .line 2107
    .local v0, r:Lcom/android/internal/telephony/uicc/IccRecords;
    if-nez v0, :cond_b

    #@a
    .line 2112
    :goto_a
    return-void

    #@b
    .line 2110
    :cond_b
    invoke-virtual {v0, p0}, Lcom/android/internal/telephony/uicc/IccRecords;->unregisterForRecordsEvents(Landroid/os/Handler;)V

    #@e
    .line 2111
    invoke-virtual {v0, p0}, Lcom/android/internal/telephony/uicc/IccRecords;->unregisterForRecordsLoaded(Landroid/os/Handler;)V

    #@11
    goto :goto_a
.end method

.method public unregisterForSubscriptionInfoReady(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 904
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSST:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->unregisterForSubscriptionInfoReady(Landroid/os/Handler;)V

    #@5
    .line 905
    return-void
.end method

.method public unregisterForSuppServiceNotification(Landroid/os/Handler;)V
    .registers 4
    .parameter "h"

    #@0
    .prologue
    .line 651
    const-string v0, "CDMA"

    #@2
    const-string v1, "method unregisterForSuppServiceNotification is NOT supported in CDMA!"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 652
    return-void
.end method

.method public unsetOnEcbModeExitResponse(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 912
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mEcmExitRespRegistrant:Landroid/os/Registrant;

    #@2
    invoke-virtual {v0}, Landroid/os/Registrant;->clear()V

    #@5
    .line 913
    return-void
.end method

.method updateCurrentCarrierInProvider()Z
    .registers 2

    #@0
    .prologue
    .line 2077
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method updateCurrentCarrierInProvider(Ljava/lang/String;)Z
    .registers 7
    .parameter "operatorNumeric"

    #@0
    .prologue
    .line 2051
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@3
    move-result v3

    #@4
    if-nez v3, :cond_48

    #@6
    .line 2053
    :try_start_6
    sget-object v3, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    #@8
    const-string v4, "current"

    #@a
    invoke-static {v3, v4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@d
    move-result-object v2

    #@e
    .line 2054
    .local v2, uri:Landroid/net/Uri;
    new-instance v1, Landroid/content/ContentValues;

    #@10
    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    #@13
    .line 2055
    .local v1, map:Landroid/content/ContentValues;
    const-string v3, "numeric"

    #@15
    invoke-virtual {v1, v3, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@18
    .line 2056
    new-instance v3, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v4, "updateCurrentCarrierInProvider from system: numeric="

    #@1f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v3

    #@2b
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/cdma/CDMAPhone;->log(Ljava/lang/String;)V

    #@2e
    .line 2057
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@31
    move-result-object v3

    #@32
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@35
    move-result-object v3

    #@36
    invoke-virtual {v3, v2, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@39
    .line 2060
    iget-object v3, p0, Lcom/android/internal/telephony/PhoneBase;->mContext:Landroid/content/Context;

    #@3b
    invoke-static {v3, p1}, Lcom/android/internal/telephony/MccTable;->updateMccMncConfiguration(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_3e
    .catch Landroid/database/SQLException; {:try_start_6 .. :try_end_3e} :catch_40

    #@3e
    .line 2062
    const/4 v3, 0x1

    #@3f
    .line 2067
    .end local v1           #map:Landroid/content/ContentValues;
    .end local v2           #uri:Landroid/net/Uri;
    :goto_3f
    return v3

    #@40
    .line 2063
    :catch_40
    move-exception v0

    #@41
    .line 2064
    .local v0, e:Landroid/database/SQLException;
    const-string v3, "CDMA"

    #@43
    const-string v4, "Can\'t store current operator"

    #@45
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@48
    .line 2067
    .end local v0           #e:Landroid/database/SQLException;
    :cond_48
    const/4 v3, 0x0

    #@49
    goto :goto_3f
.end method

.method public updateServiceLocation()V
    .registers 2

    #@0
    .prologue
    .line 884
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CDMAPhone;->mSST:Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CdmaServiceStateTracker;->enableSingleLocationUpdate()V

    #@5
    .line 885
    return-void
.end method
