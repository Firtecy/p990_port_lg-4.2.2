.class public final enum Lcom/android/internal/telephony/DataProfile$DataProfileType;
.super Ljava/lang/Enum;
.source "DataProfile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/DataProfile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DataProfileType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/internal/telephony/DataProfile$DataProfileType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/internal/telephony/DataProfile$DataProfileType;

.field public static final enum PROFILE_TYPE_APN:Lcom/android/internal/telephony/DataProfile$DataProfileType;

.field public static final enum PROFILE_TYPE_CDMA:Lcom/android/internal/telephony/DataProfile$DataProfileType;

.field public static final enum PROFILE_TYPE_OMH:Lcom/android/internal/telephony/DataProfile$DataProfileType;


# instance fields
.field id:I


# direct methods
.method static constructor <clinit>()V
    .registers 5

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    const/4 v3, 0x1

    #@2
    const/4 v2, 0x0

    #@3
    .line 49
    new-instance v0, Lcom/android/internal/telephony/DataProfile$DataProfileType;

    #@5
    const-string v1, "PROFILE_TYPE_APN"

    #@7
    invoke-direct {v0, v1, v2, v2}, Lcom/android/internal/telephony/DataProfile$DataProfileType;-><init>(Ljava/lang/String;II)V

    #@a
    sput-object v0, Lcom/android/internal/telephony/DataProfile$DataProfileType;->PROFILE_TYPE_APN:Lcom/android/internal/telephony/DataProfile$DataProfileType;

    #@c
    .line 50
    new-instance v0, Lcom/android/internal/telephony/DataProfile$DataProfileType;

    #@e
    const-string v1, "PROFILE_TYPE_CDMA"

    #@10
    invoke-direct {v0, v1, v3, v3}, Lcom/android/internal/telephony/DataProfile$DataProfileType;-><init>(Ljava/lang/String;II)V

    #@13
    sput-object v0, Lcom/android/internal/telephony/DataProfile$DataProfileType;->PROFILE_TYPE_CDMA:Lcom/android/internal/telephony/DataProfile$DataProfileType;

    #@15
    .line 51
    new-instance v0, Lcom/android/internal/telephony/DataProfile$DataProfileType;

    #@17
    const-string v1, "PROFILE_TYPE_OMH"

    #@19
    invoke-direct {v0, v1, v4, v4}, Lcom/android/internal/telephony/DataProfile$DataProfileType;-><init>(Ljava/lang/String;II)V

    #@1c
    sput-object v0, Lcom/android/internal/telephony/DataProfile$DataProfileType;->PROFILE_TYPE_OMH:Lcom/android/internal/telephony/DataProfile$DataProfileType;

    #@1e
    .line 48
    const/4 v0, 0x3

    #@1f
    new-array v0, v0, [Lcom/android/internal/telephony/DataProfile$DataProfileType;

    #@21
    sget-object v1, Lcom/android/internal/telephony/DataProfile$DataProfileType;->PROFILE_TYPE_APN:Lcom/android/internal/telephony/DataProfile$DataProfileType;

    #@23
    aput-object v1, v0, v2

    #@25
    sget-object v1, Lcom/android/internal/telephony/DataProfile$DataProfileType;->PROFILE_TYPE_CDMA:Lcom/android/internal/telephony/DataProfile$DataProfileType;

    #@27
    aput-object v1, v0, v3

    #@29
    sget-object v1, Lcom/android/internal/telephony/DataProfile$DataProfileType;->PROFILE_TYPE_OMH:Lcom/android/internal/telephony/DataProfile$DataProfileType;

    #@2b
    aput-object v1, v0, v4

    #@2d
    sput-object v0, Lcom/android/internal/telephony/DataProfile$DataProfileType;->$VALUES:[Lcom/android/internal/telephony/DataProfile$DataProfileType;

    #@2f
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .registers 4
    .parameter
    .parameter
    .parameter "i"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    .line 56
    iput p3, p0, Lcom/android/internal/telephony/DataProfile$DataProfileType;->id:I

    #@5
    .line 57
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/internal/telephony/DataProfile$DataProfileType;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 48
    const-class v0, Lcom/android/internal/telephony/DataProfile$DataProfileType;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/telephony/DataProfile$DataProfileType;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/android/internal/telephony/DataProfile$DataProfileType;
    .registers 1

    #@0
    .prologue
    .line 48
    sget-object v0, Lcom/android/internal/telephony/DataProfile$DataProfileType;->$VALUES:[Lcom/android/internal/telephony/DataProfile$DataProfileType;

    #@2
    invoke-virtual {v0}, [Lcom/android/internal/telephony/DataProfile$DataProfileType;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/android/internal/telephony/DataProfile$DataProfileType;

    #@8
    return-object v0
.end method


# virtual methods
.method public getid()I
    .registers 2

    #@0
    .prologue
    .line 60
    iget v0, p0, Lcom/android/internal/telephony/DataProfile$DataProfileType;->id:I

    #@2
    return v0
.end method
