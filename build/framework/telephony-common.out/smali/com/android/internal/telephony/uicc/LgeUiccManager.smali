.class public final Lcom/android/internal/telephony/uicc/LgeUiccManager;
.super Ljava/lang/Object;
.source "LgeUiccManager.java"


# static fields
.field private static final DBG:Z = true

.field private static final LGEUICC_IMPL_CLASS_NAME:Ljava/lang/String; = "com.lge.uicc.LgeUiccImpl"

.field private static final LOG_TAG:Ljava/lang/String; = "LgeUiccManager"

.field private static mLUClass:Ljava/lang/Class;

.field private static mLUCons:Ljava/lang/reflect/Constructor;

.field private static mLgeUicc:Lcom/android/internal/telephony/uicc/ILgeUicc;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 27
    sput-object v1, Lcom/android/internal/telephony/uicc/LgeUiccManager;->mLUCons:Ljava/lang/reflect/Constructor;

    #@3
    .line 28
    sput-object v1, Lcom/android/internal/telephony/uicc/LgeUiccManager;->mLUClass:Ljava/lang/Class;

    #@5
    .line 29
    sput-object v1, Lcom/android/internal/telephony/uicc/LgeUiccManager;->mLgeUicc:Lcom/android/internal/telephony/uicc/ILgeUicc;

    #@7
    .line 33
    :try_start_7
    const-string v1, "com.lge.uicc.LgeUiccImpl"

    #@9
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    #@c
    move-result-object v1

    #@d
    sput-object v1, Lcom/android/internal/telephony/uicc/LgeUiccManager;->mLUClass:Ljava/lang/Class;

    #@f
    .line 34
    sget-object v1, Lcom/android/internal/telephony/uicc/LgeUiccManager;->mLUClass:Ljava/lang/Class;

    #@11
    const/4 v2, 0x1

    #@12
    new-array v2, v2, [Ljava/lang/Class;

    #@14
    const/4 v3, 0x0

    #@15
    const-class v4, Lcom/android/internal/telephony/Phone;

    #@17
    aput-object v4, v2, v3

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    #@1c
    move-result-object v1

    #@1d
    sput-object v1, Lcom/android/internal/telephony/uicc/LgeUiccManager;->mLUCons:Ljava/lang/reflect/Constructor;
    :try_end_1f
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_1f} :catch_20

    #@1f
    .line 38
    .local v0, ex:Ljava/lang/Exception;
    :goto_1f
    return-void

    #@20
    .line 35
    .end local v0           #ex:Ljava/lang/Exception;
    :catch_20
    move-exception v0

    #@21
    .line 36
    .restart local v0       #ex:Ljava/lang/Exception;
    const-string v1, "LgeUiccManager"

    #@23
    new-instance v2, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v3, "get constructor failure : "

    #@2a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v2

    #@2e
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v2

    #@32
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v2

    #@36
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@39
    goto :goto_1f
.end method

.method public constructor <init>(Lcom/android/internal/telephony/Phone;)V
    .registers 6
    .parameter "mPhone"

    #@0
    .prologue
    .line 40
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 41
    sget-object v1, Lcom/android/internal/telephony/uicc/LgeUiccManager;->mLgeUicc:Lcom/android/internal/telephony/uicc/ILgeUicc;

    #@5
    if-nez v1, :cond_17

    #@7
    .line 43
    :try_start_7
    sget-object v1, Lcom/android/internal/telephony/uicc/LgeUiccManager;->mLUCons:Ljava/lang/reflect/Constructor;

    #@9
    const/4 v2, 0x1

    #@a
    new-array v2, v2, [Ljava/lang/Object;

    #@c
    const/4 v3, 0x0

    #@d
    aput-object p1, v2, v3

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    #@12
    move-result-object v1

    #@13
    check-cast v1, Lcom/android/internal/telephony/uicc/ILgeUicc;

    #@15
    sput-object v1, Lcom/android/internal/telephony/uicc/LgeUiccManager;->mLgeUicc:Lcom/android/internal/telephony/uicc/ILgeUicc;
    :try_end_17
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_17} :catch_18

    #@17
    .line 48
    :cond_17
    :goto_17
    return-void

    #@18
    .line 44
    :catch_18
    move-exception v0

    #@19
    .line 45
    .local v0, ex:Ljava/lang/Exception;
    const-string v1, "LgeUiccManager"

    #@1b
    new-instance v2, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v3, "Instance creating fail! : "

    #@22
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v2

    #@26
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v2

    #@2a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v2

    #@2e
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@31
    goto :goto_17
.end method

.method public static getInstance(Lcom/android/internal/telephony/Phone;)Lcom/android/internal/telephony/uicc/ILgeUicc;
    .registers 5
    .parameter "mPhone"

    #@0
    .prologue
    .line 51
    sget-object v1, Lcom/android/internal/telephony/uicc/LgeUiccManager;->mLgeUicc:Lcom/android/internal/telephony/uicc/ILgeUicc;

    #@2
    if-eqz v1, :cond_7

    #@4
    .line 52
    sget-object v1, Lcom/android/internal/telephony/uicc/LgeUiccManager;->mLgeUicc:Lcom/android/internal/telephony/uicc/ILgeUicc;

    #@6
    .line 59
    :goto_6
    return-object v1

    #@7
    .line 55
    :cond_7
    :try_start_7
    sget-object v1, Lcom/android/internal/telephony/uicc/LgeUiccManager;->mLUCons:Ljava/lang/reflect/Constructor;

    #@9
    const/4 v2, 0x1

    #@a
    new-array v2, v2, [Ljava/lang/Object;

    #@c
    const/4 v3, 0x0

    #@d
    aput-object p0, v2, v3

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    #@12
    move-result-object v1

    #@13
    check-cast v1, Lcom/android/internal/telephony/uicc/ILgeUicc;

    #@15
    sput-object v1, Lcom/android/internal/telephony/uicc/LgeUiccManager;->mLgeUicc:Lcom/android/internal/telephony/uicc/ILgeUicc;
    :try_end_17
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_17} :catch_1a

    #@17
    .line 59
    :goto_17
    sget-object v1, Lcom/android/internal/telephony/uicc/LgeUiccManager;->mLgeUicc:Lcom/android/internal/telephony/uicc/ILgeUicc;

    #@19
    goto :goto_6

    #@1a
    .line 56
    :catch_1a
    move-exception v0

    #@1b
    .line 57
    .local v0, ex:Ljava/lang/Exception;
    const-string v1, "LgeUiccManager"

    #@1d
    new-instance v2, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    const-string v3, "Instance creating fail! : "

    #@24
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v2

    #@28
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v2

    #@2c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v2

    #@30
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@33
    goto :goto_17
.end method
