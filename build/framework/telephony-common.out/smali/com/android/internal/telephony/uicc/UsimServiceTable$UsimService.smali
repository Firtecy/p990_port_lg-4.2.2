.class public final enum Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;
.super Ljava/lang/Enum;
.source "UsimServiceTable.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/uicc/UsimServiceTable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "UsimService"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum ADVICE_OF_CHARGE:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum ALLOWED_CSG_LISTS_AND_INDICATIONS:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum APN_CONTROL_LIST:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum BDN:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum BDN_EXTENSION:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum CALL_CONTROL_BY_USIM:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum CALL_CONTROL_ON_EPS_PDN_CONNECTION_BY_USIM:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum CAP_CONFIG_PARAMS_2:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum CB_MESSAGE_ID:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum CB_MESSAGE_ID_RANGES:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum CFI_STATUS:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum COOPERATIVE_NETWORK_LIST:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum CPBCCH_INFO:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum CSG_DISPLAY_CONTROL:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum DATA_DL_VIA_SMS_CB:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum DATA_DL_VIA_SMS_PP:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum DATA_DL_VIA_USSD:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum DEPERSONALISATION_CONTROL_KEYS:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum ECALL_DATA:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum EMLPP:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum EMLPP_AUTO_ANSWER:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum ENABLED_SERVICES_TABLE:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum EPS_MOBILITY_MANAGEMENT_INFO:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum EQUIVALENT_HPLMN:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum EQUIVALENT_HPLMN_PRESENTATION:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum EXTENDED_TERMINAL_APPLICATIONS:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum EXTENSION_5:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum FDN:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum FDN_EXTENSION:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum GBA:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum GBA_LOCAL_KEY_ESTABLISHMENT:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum GPRS_CALL_CONTROL_BY_USIM:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum GROUP_ID_LEVEL_1:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum GROUP_ID_LEVEL_2:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum GSM_ACCESS:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum GSM_SECURITY_CONTEXT:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum HPLMN_DIRECT_ACCESS:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum HPLMN_SELECT:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum IGNORED_1:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum IGNORED_2:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum IMAGE:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum IMS_COMMUNICATION_CONTROL_BY_USIM:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum INCOMING_CALL_INFO:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum INVESTIGATION_SCAN:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum IWLAN_EQUIVALENT_HPLMN_PRESENTATION:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum IWLAN_HOME_ID_LIST:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum IWLAN_HPLMN_PRIORITY_INDICATION:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum IWLAN_LAST_REGISTERED_PLMN:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum IWLAN_OPERATOR_PLMN_SELECT:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum IWLAN_USER_PLMN_SELECT:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum LAST_RPLMN_SELECTION_INDICATION:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum LOCALISED_SERVICE_AREAS:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum MBDN:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum MBMS_SECURITY:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum MEXE:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum MMS_CONNECTIVITY_PARAMS:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum MMS_NOTIFICATION:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum MMS_NOTIFICATION_EXTENSION:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum MM_STORAGE:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum MO_SMS_CONTROL_BY_USIM:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum MSISDN:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum MWI_STATUS:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum NAS_CONFIG_BY_USIM:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum NETWORK_INDICATION_OF_ALERTING:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum OMA_BCAST_PROFILE:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum OPERATOR_CSG_LISTS_AND_INDICATIONS:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum OPERATOR_PLMN_LIST:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum OPERATOR_PLMN_SELECT:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum OPERATOR_WSID_LIST:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum OUTGOING_CALL_INFO:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum PHONEBOOK:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum PLMN_NETWORK_NAME:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum PLMN_NETWORK_NAME_ICON:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum PSEUDONYM:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum RFU:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum RUN_AT_COMMAND:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum SDN:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum SDN_EXTENSION:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum SERVICE_PROVIDER_DISPLAY_INFO:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum SM_OVER_IP:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum SM_SERVICE_PARAMS:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum SM_STATUS_REPORTS:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum SM_STORAGE:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum SPN:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum SPN_ICON:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum TERMINAL_APPLICATIONS:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum TERMINAL_PROFILE_AFTER_UICC_ACTIVATION:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum UICC_ACCESS_TO_IMS:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum USER_PLMN_SELECT:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum USER_WSID_LIST:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum USIM_IP_CONNECTION_PARAMS:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum VBS_GROUP_ID_LIST:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum VBS_SECURITY:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum VGCS_GROUP_ID_LIST:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum VGCS_SECURITY:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

.field public static final enum WLAN_REAUTH_IDENTITY:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 26
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@7
    const-string v1, "PHONEBOOK"

    #@9
    invoke-direct {v0, v1, v3}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@c
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->PHONEBOOK:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@e
    .line 27
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@10
    const-string v1, "FDN"

    #@12
    invoke-direct {v0, v1, v4}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@15
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->FDN:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@17
    .line 28
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@19
    const-string v1, "FDN_EXTENSION"

    #@1b
    invoke-direct {v0, v1, v5}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@1e
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->FDN_EXTENSION:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@20
    .line 29
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@22
    const-string v1, "SDN"

    #@24
    invoke-direct {v0, v1, v6}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@27
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->SDN:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@29
    .line 30
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@2b
    const-string v1, "SDN_EXTENSION"

    #@2d
    invoke-direct {v0, v1, v7}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@30
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->SDN_EXTENSION:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@32
    .line 31
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@34
    const-string v1, "BDN"

    #@36
    const/4 v2, 0x5

    #@37
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@3a
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->BDN:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@3c
    .line 32
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@3e
    const-string v1, "BDN_EXTENSION"

    #@40
    const/4 v2, 0x6

    #@41
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@44
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->BDN_EXTENSION:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@46
    .line 33
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@48
    const-string v1, "OUTGOING_CALL_INFO"

    #@4a
    const/4 v2, 0x7

    #@4b
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@4e
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->OUTGOING_CALL_INFO:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@50
    .line 34
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@52
    const-string v1, "INCOMING_CALL_INFO"

    #@54
    const/16 v2, 0x8

    #@56
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@59
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->INCOMING_CALL_INFO:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@5b
    .line 35
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@5d
    const-string v1, "SM_STORAGE"

    #@5f
    const/16 v2, 0x9

    #@61
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@64
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->SM_STORAGE:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@66
    .line 36
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@68
    const-string v1, "SM_STATUS_REPORTS"

    #@6a
    const/16 v2, 0xa

    #@6c
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@6f
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->SM_STATUS_REPORTS:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@71
    .line 37
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@73
    const-string v1, "SM_SERVICE_PARAMS"

    #@75
    const/16 v2, 0xb

    #@77
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@7a
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->SM_SERVICE_PARAMS:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@7c
    .line 38
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@7e
    const-string v1, "ADVICE_OF_CHARGE"

    #@80
    const/16 v2, 0xc

    #@82
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@85
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->ADVICE_OF_CHARGE:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@87
    .line 39
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@89
    const-string v1, "CAP_CONFIG_PARAMS_2"

    #@8b
    const/16 v2, 0xd

    #@8d
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@90
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->CAP_CONFIG_PARAMS_2:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@92
    .line 40
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@94
    const-string v1, "CB_MESSAGE_ID"

    #@96
    const/16 v2, 0xe

    #@98
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@9b
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->CB_MESSAGE_ID:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@9d
    .line 41
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@9f
    const-string v1, "CB_MESSAGE_ID_RANGES"

    #@a1
    const/16 v2, 0xf

    #@a3
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@a6
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->CB_MESSAGE_ID_RANGES:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@a8
    .line 42
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@aa
    const-string v1, "GROUP_ID_LEVEL_1"

    #@ac
    const/16 v2, 0x10

    #@ae
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@b1
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->GROUP_ID_LEVEL_1:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@b3
    .line 43
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@b5
    const-string v1, "GROUP_ID_LEVEL_2"

    #@b7
    const/16 v2, 0x11

    #@b9
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@bc
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->GROUP_ID_LEVEL_2:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@be
    .line 44
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@c0
    const-string v1, "SPN"

    #@c2
    const/16 v2, 0x12

    #@c4
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@c7
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->SPN:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@c9
    .line 45
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@cb
    const-string v1, "USER_PLMN_SELECT"

    #@cd
    const/16 v2, 0x13

    #@cf
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@d2
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->USER_PLMN_SELECT:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@d4
    .line 46
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@d6
    const-string v1, "MSISDN"

    #@d8
    const/16 v2, 0x14

    #@da
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@dd
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->MSISDN:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@df
    .line 47
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@e1
    const-string v1, "IMAGE"

    #@e3
    const/16 v2, 0x15

    #@e5
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@e8
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->IMAGE:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@ea
    .line 48
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@ec
    const-string v1, "LOCALISED_SERVICE_AREAS"

    #@ee
    const/16 v2, 0x16

    #@f0
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@f3
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->LOCALISED_SERVICE_AREAS:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@f5
    .line 49
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@f7
    const-string v1, "EMLPP"

    #@f9
    const/16 v2, 0x17

    #@fb
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@fe
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->EMLPP:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@100
    .line 50
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@102
    const-string v1, "EMLPP_AUTO_ANSWER"

    #@104
    const/16 v2, 0x18

    #@106
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@109
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->EMLPP_AUTO_ANSWER:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@10b
    .line 51
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@10d
    const-string v1, "RFU"

    #@10f
    const/16 v2, 0x19

    #@111
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@114
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->RFU:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@116
    .line 52
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@118
    const-string v1, "GSM_ACCESS"

    #@11a
    const/16 v2, 0x1a

    #@11c
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@11f
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->GSM_ACCESS:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@121
    .line 53
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@123
    const-string v1, "DATA_DL_VIA_SMS_PP"

    #@125
    const/16 v2, 0x1b

    #@127
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@12a
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->DATA_DL_VIA_SMS_PP:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@12c
    .line 54
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@12e
    const-string v1, "DATA_DL_VIA_SMS_CB"

    #@130
    const/16 v2, 0x1c

    #@132
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@135
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->DATA_DL_VIA_SMS_CB:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@137
    .line 55
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@139
    const-string v1, "CALL_CONTROL_BY_USIM"

    #@13b
    const/16 v2, 0x1d

    #@13d
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@140
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->CALL_CONTROL_BY_USIM:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@142
    .line 56
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@144
    const-string v1, "MO_SMS_CONTROL_BY_USIM"

    #@146
    const/16 v2, 0x1e

    #@148
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@14b
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->MO_SMS_CONTROL_BY_USIM:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@14d
    .line 57
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@14f
    const-string v1, "RUN_AT_COMMAND"

    #@151
    const/16 v2, 0x1f

    #@153
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@156
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->RUN_AT_COMMAND:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@158
    .line 58
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@15a
    const-string v1, "IGNORED_1"

    #@15c
    const/16 v2, 0x20

    #@15e
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@161
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->IGNORED_1:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@163
    .line 59
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@165
    const-string v1, "ENABLED_SERVICES_TABLE"

    #@167
    const/16 v2, 0x21

    #@169
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@16c
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->ENABLED_SERVICES_TABLE:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@16e
    .line 60
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@170
    const-string v1, "APN_CONTROL_LIST"

    #@172
    const/16 v2, 0x22

    #@174
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@177
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->APN_CONTROL_LIST:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@179
    .line 61
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@17b
    const-string v1, "DEPERSONALISATION_CONTROL_KEYS"

    #@17d
    const/16 v2, 0x23

    #@17f
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@182
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->DEPERSONALISATION_CONTROL_KEYS:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@184
    .line 62
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@186
    const-string v1, "COOPERATIVE_NETWORK_LIST"

    #@188
    const/16 v2, 0x24

    #@18a
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@18d
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->COOPERATIVE_NETWORK_LIST:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@18f
    .line 63
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@191
    const-string v1, "GSM_SECURITY_CONTEXT"

    #@193
    const/16 v2, 0x25

    #@195
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@198
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->GSM_SECURITY_CONTEXT:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@19a
    .line 64
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@19c
    const-string v1, "CPBCCH_INFO"

    #@19e
    const/16 v2, 0x26

    #@1a0
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@1a3
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->CPBCCH_INFO:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@1a5
    .line 65
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@1a7
    const-string v1, "INVESTIGATION_SCAN"

    #@1a9
    const/16 v2, 0x27

    #@1ab
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@1ae
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->INVESTIGATION_SCAN:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@1b0
    .line 66
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@1b2
    const-string v1, "MEXE"

    #@1b4
    const/16 v2, 0x28

    #@1b6
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@1b9
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->MEXE:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@1bb
    .line 67
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@1bd
    const-string v1, "OPERATOR_PLMN_SELECT"

    #@1bf
    const/16 v2, 0x29

    #@1c1
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@1c4
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->OPERATOR_PLMN_SELECT:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@1c6
    .line 68
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@1c8
    const-string v1, "HPLMN_SELECT"

    #@1ca
    const/16 v2, 0x2a

    #@1cc
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@1cf
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->HPLMN_SELECT:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@1d1
    .line 69
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@1d3
    const-string v1, "EXTENSION_5"

    #@1d5
    const/16 v2, 0x2b

    #@1d7
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@1da
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->EXTENSION_5:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@1dc
    .line 70
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@1de
    const-string v1, "PLMN_NETWORK_NAME"

    #@1e0
    const/16 v2, 0x2c

    #@1e2
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@1e5
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->PLMN_NETWORK_NAME:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@1e7
    .line 71
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@1e9
    const-string v1, "OPERATOR_PLMN_LIST"

    #@1eb
    const/16 v2, 0x2d

    #@1ed
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@1f0
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->OPERATOR_PLMN_LIST:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@1f2
    .line 72
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@1f4
    const-string v1, "MBDN"

    #@1f6
    const/16 v2, 0x2e

    #@1f8
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@1fb
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->MBDN:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@1fd
    .line 73
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@1ff
    const-string v1, "MWI_STATUS"

    #@201
    const/16 v2, 0x2f

    #@203
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@206
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->MWI_STATUS:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@208
    .line 74
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@20a
    const-string v1, "CFI_STATUS"

    #@20c
    const/16 v2, 0x30

    #@20e
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@211
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->CFI_STATUS:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@213
    .line 75
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@215
    const-string v1, "IGNORED_2"

    #@217
    const/16 v2, 0x31

    #@219
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@21c
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->IGNORED_2:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@21e
    .line 76
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@220
    const-string v1, "SERVICE_PROVIDER_DISPLAY_INFO"

    #@222
    const/16 v2, 0x32

    #@224
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@227
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->SERVICE_PROVIDER_DISPLAY_INFO:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@229
    .line 77
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@22b
    const-string v1, "MMS_NOTIFICATION"

    #@22d
    const/16 v2, 0x33

    #@22f
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@232
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->MMS_NOTIFICATION:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@234
    .line 78
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@236
    const-string v1, "MMS_NOTIFICATION_EXTENSION"

    #@238
    const/16 v2, 0x34

    #@23a
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@23d
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->MMS_NOTIFICATION_EXTENSION:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@23f
    .line 79
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@241
    const-string v1, "GPRS_CALL_CONTROL_BY_USIM"

    #@243
    const/16 v2, 0x35

    #@245
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@248
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->GPRS_CALL_CONTROL_BY_USIM:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@24a
    .line 80
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@24c
    const-string v1, "MMS_CONNECTIVITY_PARAMS"

    #@24e
    const/16 v2, 0x36

    #@250
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@253
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->MMS_CONNECTIVITY_PARAMS:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@255
    .line 81
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@257
    const-string v1, "NETWORK_INDICATION_OF_ALERTING"

    #@259
    const/16 v2, 0x37

    #@25b
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@25e
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->NETWORK_INDICATION_OF_ALERTING:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@260
    .line 82
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@262
    const-string v1, "VGCS_GROUP_ID_LIST"

    #@264
    const/16 v2, 0x38

    #@266
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@269
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->VGCS_GROUP_ID_LIST:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@26b
    .line 83
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@26d
    const-string v1, "VBS_GROUP_ID_LIST"

    #@26f
    const/16 v2, 0x39

    #@271
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@274
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->VBS_GROUP_ID_LIST:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@276
    .line 84
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@278
    const-string v1, "PSEUDONYM"

    #@27a
    const/16 v2, 0x3a

    #@27c
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@27f
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->PSEUDONYM:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@281
    .line 85
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@283
    const-string v1, "IWLAN_USER_PLMN_SELECT"

    #@285
    const/16 v2, 0x3b

    #@287
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@28a
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->IWLAN_USER_PLMN_SELECT:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@28c
    .line 86
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@28e
    const-string v1, "IWLAN_OPERATOR_PLMN_SELECT"

    #@290
    const/16 v2, 0x3c

    #@292
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@295
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->IWLAN_OPERATOR_PLMN_SELECT:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@297
    .line 87
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@299
    const-string v1, "USER_WSID_LIST"

    #@29b
    const/16 v2, 0x3d

    #@29d
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@2a0
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->USER_WSID_LIST:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@2a2
    .line 88
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@2a4
    const-string v1, "OPERATOR_WSID_LIST"

    #@2a6
    const/16 v2, 0x3e

    #@2a8
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@2ab
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->OPERATOR_WSID_LIST:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@2ad
    .line 89
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@2af
    const-string v1, "VGCS_SECURITY"

    #@2b1
    const/16 v2, 0x3f

    #@2b3
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@2b6
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->VGCS_SECURITY:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@2b8
    .line 90
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@2ba
    const-string v1, "VBS_SECURITY"

    #@2bc
    const/16 v2, 0x40

    #@2be
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@2c1
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->VBS_SECURITY:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@2c3
    .line 91
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@2c5
    const-string v1, "WLAN_REAUTH_IDENTITY"

    #@2c7
    const/16 v2, 0x41

    #@2c9
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@2cc
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->WLAN_REAUTH_IDENTITY:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@2ce
    .line 92
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@2d0
    const-string v1, "MM_STORAGE"

    #@2d2
    const/16 v2, 0x42

    #@2d4
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@2d7
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->MM_STORAGE:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@2d9
    .line 93
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@2db
    const-string v1, "GBA"

    #@2dd
    const/16 v2, 0x43

    #@2df
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@2e2
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->GBA:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@2e4
    .line 94
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@2e6
    const-string v1, "MBMS_SECURITY"

    #@2e8
    const/16 v2, 0x44

    #@2ea
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@2ed
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->MBMS_SECURITY:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@2ef
    .line 95
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@2f1
    const-string v1, "DATA_DL_VIA_USSD"

    #@2f3
    const/16 v2, 0x45

    #@2f5
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@2f8
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->DATA_DL_VIA_USSD:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@2fa
    .line 96
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@2fc
    const-string v1, "EQUIVALENT_HPLMN"

    #@2fe
    const/16 v2, 0x46

    #@300
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@303
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->EQUIVALENT_HPLMN:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@305
    .line 97
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@307
    const-string v1, "TERMINAL_PROFILE_AFTER_UICC_ACTIVATION"

    #@309
    const/16 v2, 0x47

    #@30b
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@30e
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->TERMINAL_PROFILE_AFTER_UICC_ACTIVATION:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@310
    .line 98
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@312
    const-string v1, "EQUIVALENT_HPLMN_PRESENTATION"

    #@314
    const/16 v2, 0x48

    #@316
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@319
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->EQUIVALENT_HPLMN_PRESENTATION:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@31b
    .line 99
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@31d
    const-string v1, "LAST_RPLMN_SELECTION_INDICATION"

    #@31f
    const/16 v2, 0x49

    #@321
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@324
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->LAST_RPLMN_SELECTION_INDICATION:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@326
    .line 100
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@328
    const-string v1, "OMA_BCAST_PROFILE"

    #@32a
    const/16 v2, 0x4a

    #@32c
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@32f
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->OMA_BCAST_PROFILE:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@331
    .line 101
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@333
    const-string v1, "GBA_LOCAL_KEY_ESTABLISHMENT"

    #@335
    const/16 v2, 0x4b

    #@337
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@33a
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->GBA_LOCAL_KEY_ESTABLISHMENT:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@33c
    .line 102
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@33e
    const-string v1, "TERMINAL_APPLICATIONS"

    #@340
    const/16 v2, 0x4c

    #@342
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@345
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->TERMINAL_APPLICATIONS:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@347
    .line 103
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@349
    const-string v1, "SPN_ICON"

    #@34b
    const/16 v2, 0x4d

    #@34d
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@350
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->SPN_ICON:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@352
    .line 104
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@354
    const-string v1, "PLMN_NETWORK_NAME_ICON"

    #@356
    const/16 v2, 0x4e

    #@358
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@35b
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->PLMN_NETWORK_NAME_ICON:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@35d
    .line 105
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@35f
    const-string v1, "USIM_IP_CONNECTION_PARAMS"

    #@361
    const/16 v2, 0x4f

    #@363
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@366
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->USIM_IP_CONNECTION_PARAMS:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@368
    .line 106
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@36a
    const-string v1, "IWLAN_HOME_ID_LIST"

    #@36c
    const/16 v2, 0x50

    #@36e
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@371
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->IWLAN_HOME_ID_LIST:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@373
    .line 107
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@375
    const-string v1, "IWLAN_EQUIVALENT_HPLMN_PRESENTATION"

    #@377
    const/16 v2, 0x51

    #@379
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@37c
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->IWLAN_EQUIVALENT_HPLMN_PRESENTATION:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@37e
    .line 108
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@380
    const-string v1, "IWLAN_HPLMN_PRIORITY_INDICATION"

    #@382
    const/16 v2, 0x52

    #@384
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@387
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->IWLAN_HPLMN_PRIORITY_INDICATION:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@389
    .line 109
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@38b
    const-string v1, "IWLAN_LAST_REGISTERED_PLMN"

    #@38d
    const/16 v2, 0x53

    #@38f
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@392
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->IWLAN_LAST_REGISTERED_PLMN:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@394
    .line 110
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@396
    const-string v1, "EPS_MOBILITY_MANAGEMENT_INFO"

    #@398
    const/16 v2, 0x54

    #@39a
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@39d
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->EPS_MOBILITY_MANAGEMENT_INFO:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@39f
    .line 111
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@3a1
    const-string v1, "ALLOWED_CSG_LISTS_AND_INDICATIONS"

    #@3a3
    const/16 v2, 0x55

    #@3a5
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@3a8
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->ALLOWED_CSG_LISTS_AND_INDICATIONS:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@3aa
    .line 112
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@3ac
    const-string v1, "CALL_CONTROL_ON_EPS_PDN_CONNECTION_BY_USIM"

    #@3ae
    const/16 v2, 0x56

    #@3b0
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@3b3
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->CALL_CONTROL_ON_EPS_PDN_CONNECTION_BY_USIM:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@3b5
    .line 113
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@3b7
    const-string v1, "HPLMN_DIRECT_ACCESS"

    #@3b9
    const/16 v2, 0x57

    #@3bb
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@3be
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->HPLMN_DIRECT_ACCESS:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@3c0
    .line 114
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@3c2
    const-string v1, "ECALL_DATA"

    #@3c4
    const/16 v2, 0x58

    #@3c6
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@3c9
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->ECALL_DATA:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@3cb
    .line 115
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@3cd
    const-string v1, "OPERATOR_CSG_LISTS_AND_INDICATIONS"

    #@3cf
    const/16 v2, 0x59

    #@3d1
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@3d4
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->OPERATOR_CSG_LISTS_AND_INDICATIONS:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@3d6
    .line 116
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@3d8
    const-string v1, "SM_OVER_IP"

    #@3da
    const/16 v2, 0x5a

    #@3dc
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@3df
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->SM_OVER_IP:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@3e1
    .line 117
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@3e3
    const-string v1, "CSG_DISPLAY_CONTROL"

    #@3e5
    const/16 v2, 0x5b

    #@3e7
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@3ea
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->CSG_DISPLAY_CONTROL:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@3ec
    .line 118
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@3ee
    const-string v1, "IMS_COMMUNICATION_CONTROL_BY_USIM"

    #@3f0
    const/16 v2, 0x5c

    #@3f2
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@3f5
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->IMS_COMMUNICATION_CONTROL_BY_USIM:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@3f7
    .line 119
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@3f9
    const-string v1, "EXTENDED_TERMINAL_APPLICATIONS"

    #@3fb
    const/16 v2, 0x5d

    #@3fd
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@400
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->EXTENDED_TERMINAL_APPLICATIONS:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@402
    .line 120
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@404
    const-string v1, "UICC_ACCESS_TO_IMS"

    #@406
    const/16 v2, 0x5e

    #@408
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@40b
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->UICC_ACCESS_TO_IMS:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@40d
    .line 121
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@40f
    const-string v1, "NAS_CONFIG_BY_USIM"

    #@411
    const/16 v2, 0x5f

    #@413
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;-><init>(Ljava/lang/String;I)V

    #@416
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->NAS_CONFIG_BY_USIM:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@418
    .line 25
    const/16 v0, 0x60

    #@41a
    new-array v0, v0, [Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@41c
    sget-object v1, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->PHONEBOOK:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@41e
    aput-object v1, v0, v3

    #@420
    sget-object v1, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->FDN:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@422
    aput-object v1, v0, v4

    #@424
    sget-object v1, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->FDN_EXTENSION:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@426
    aput-object v1, v0, v5

    #@428
    sget-object v1, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->SDN:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@42a
    aput-object v1, v0, v6

    #@42c
    sget-object v1, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->SDN_EXTENSION:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@42e
    aput-object v1, v0, v7

    #@430
    const/4 v1, 0x5

    #@431
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->BDN:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@433
    aput-object v2, v0, v1

    #@435
    const/4 v1, 0x6

    #@436
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->BDN_EXTENSION:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@438
    aput-object v2, v0, v1

    #@43a
    const/4 v1, 0x7

    #@43b
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->OUTGOING_CALL_INFO:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@43d
    aput-object v2, v0, v1

    #@43f
    const/16 v1, 0x8

    #@441
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->INCOMING_CALL_INFO:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@443
    aput-object v2, v0, v1

    #@445
    const/16 v1, 0x9

    #@447
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->SM_STORAGE:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@449
    aput-object v2, v0, v1

    #@44b
    const/16 v1, 0xa

    #@44d
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->SM_STATUS_REPORTS:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@44f
    aput-object v2, v0, v1

    #@451
    const/16 v1, 0xb

    #@453
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->SM_SERVICE_PARAMS:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@455
    aput-object v2, v0, v1

    #@457
    const/16 v1, 0xc

    #@459
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->ADVICE_OF_CHARGE:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@45b
    aput-object v2, v0, v1

    #@45d
    const/16 v1, 0xd

    #@45f
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->CAP_CONFIG_PARAMS_2:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@461
    aput-object v2, v0, v1

    #@463
    const/16 v1, 0xe

    #@465
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->CB_MESSAGE_ID:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@467
    aput-object v2, v0, v1

    #@469
    const/16 v1, 0xf

    #@46b
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->CB_MESSAGE_ID_RANGES:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@46d
    aput-object v2, v0, v1

    #@46f
    const/16 v1, 0x10

    #@471
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->GROUP_ID_LEVEL_1:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@473
    aput-object v2, v0, v1

    #@475
    const/16 v1, 0x11

    #@477
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->GROUP_ID_LEVEL_2:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@479
    aput-object v2, v0, v1

    #@47b
    const/16 v1, 0x12

    #@47d
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->SPN:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@47f
    aput-object v2, v0, v1

    #@481
    const/16 v1, 0x13

    #@483
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->USER_PLMN_SELECT:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@485
    aput-object v2, v0, v1

    #@487
    const/16 v1, 0x14

    #@489
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->MSISDN:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@48b
    aput-object v2, v0, v1

    #@48d
    const/16 v1, 0x15

    #@48f
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->IMAGE:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@491
    aput-object v2, v0, v1

    #@493
    const/16 v1, 0x16

    #@495
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->LOCALISED_SERVICE_AREAS:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@497
    aput-object v2, v0, v1

    #@499
    const/16 v1, 0x17

    #@49b
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->EMLPP:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@49d
    aput-object v2, v0, v1

    #@49f
    const/16 v1, 0x18

    #@4a1
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->EMLPP_AUTO_ANSWER:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@4a3
    aput-object v2, v0, v1

    #@4a5
    const/16 v1, 0x19

    #@4a7
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->RFU:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@4a9
    aput-object v2, v0, v1

    #@4ab
    const/16 v1, 0x1a

    #@4ad
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->GSM_ACCESS:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@4af
    aput-object v2, v0, v1

    #@4b1
    const/16 v1, 0x1b

    #@4b3
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->DATA_DL_VIA_SMS_PP:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@4b5
    aput-object v2, v0, v1

    #@4b7
    const/16 v1, 0x1c

    #@4b9
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->DATA_DL_VIA_SMS_CB:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@4bb
    aput-object v2, v0, v1

    #@4bd
    const/16 v1, 0x1d

    #@4bf
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->CALL_CONTROL_BY_USIM:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@4c1
    aput-object v2, v0, v1

    #@4c3
    const/16 v1, 0x1e

    #@4c5
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->MO_SMS_CONTROL_BY_USIM:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@4c7
    aput-object v2, v0, v1

    #@4c9
    const/16 v1, 0x1f

    #@4cb
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->RUN_AT_COMMAND:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@4cd
    aput-object v2, v0, v1

    #@4cf
    const/16 v1, 0x20

    #@4d1
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->IGNORED_1:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@4d3
    aput-object v2, v0, v1

    #@4d5
    const/16 v1, 0x21

    #@4d7
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->ENABLED_SERVICES_TABLE:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@4d9
    aput-object v2, v0, v1

    #@4db
    const/16 v1, 0x22

    #@4dd
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->APN_CONTROL_LIST:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@4df
    aput-object v2, v0, v1

    #@4e1
    const/16 v1, 0x23

    #@4e3
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->DEPERSONALISATION_CONTROL_KEYS:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@4e5
    aput-object v2, v0, v1

    #@4e7
    const/16 v1, 0x24

    #@4e9
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->COOPERATIVE_NETWORK_LIST:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@4eb
    aput-object v2, v0, v1

    #@4ed
    const/16 v1, 0x25

    #@4ef
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->GSM_SECURITY_CONTEXT:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@4f1
    aput-object v2, v0, v1

    #@4f3
    const/16 v1, 0x26

    #@4f5
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->CPBCCH_INFO:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@4f7
    aput-object v2, v0, v1

    #@4f9
    const/16 v1, 0x27

    #@4fb
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->INVESTIGATION_SCAN:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@4fd
    aput-object v2, v0, v1

    #@4ff
    const/16 v1, 0x28

    #@501
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->MEXE:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@503
    aput-object v2, v0, v1

    #@505
    const/16 v1, 0x29

    #@507
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->OPERATOR_PLMN_SELECT:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@509
    aput-object v2, v0, v1

    #@50b
    const/16 v1, 0x2a

    #@50d
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->HPLMN_SELECT:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@50f
    aput-object v2, v0, v1

    #@511
    const/16 v1, 0x2b

    #@513
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->EXTENSION_5:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@515
    aput-object v2, v0, v1

    #@517
    const/16 v1, 0x2c

    #@519
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->PLMN_NETWORK_NAME:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@51b
    aput-object v2, v0, v1

    #@51d
    const/16 v1, 0x2d

    #@51f
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->OPERATOR_PLMN_LIST:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@521
    aput-object v2, v0, v1

    #@523
    const/16 v1, 0x2e

    #@525
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->MBDN:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@527
    aput-object v2, v0, v1

    #@529
    const/16 v1, 0x2f

    #@52b
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->MWI_STATUS:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@52d
    aput-object v2, v0, v1

    #@52f
    const/16 v1, 0x30

    #@531
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->CFI_STATUS:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@533
    aput-object v2, v0, v1

    #@535
    const/16 v1, 0x31

    #@537
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->IGNORED_2:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@539
    aput-object v2, v0, v1

    #@53b
    const/16 v1, 0x32

    #@53d
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->SERVICE_PROVIDER_DISPLAY_INFO:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@53f
    aput-object v2, v0, v1

    #@541
    const/16 v1, 0x33

    #@543
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->MMS_NOTIFICATION:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@545
    aput-object v2, v0, v1

    #@547
    const/16 v1, 0x34

    #@549
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->MMS_NOTIFICATION_EXTENSION:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@54b
    aput-object v2, v0, v1

    #@54d
    const/16 v1, 0x35

    #@54f
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->GPRS_CALL_CONTROL_BY_USIM:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@551
    aput-object v2, v0, v1

    #@553
    const/16 v1, 0x36

    #@555
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->MMS_CONNECTIVITY_PARAMS:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@557
    aput-object v2, v0, v1

    #@559
    const/16 v1, 0x37

    #@55b
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->NETWORK_INDICATION_OF_ALERTING:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@55d
    aput-object v2, v0, v1

    #@55f
    const/16 v1, 0x38

    #@561
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->VGCS_GROUP_ID_LIST:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@563
    aput-object v2, v0, v1

    #@565
    const/16 v1, 0x39

    #@567
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->VBS_GROUP_ID_LIST:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@569
    aput-object v2, v0, v1

    #@56b
    const/16 v1, 0x3a

    #@56d
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->PSEUDONYM:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@56f
    aput-object v2, v0, v1

    #@571
    const/16 v1, 0x3b

    #@573
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->IWLAN_USER_PLMN_SELECT:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@575
    aput-object v2, v0, v1

    #@577
    const/16 v1, 0x3c

    #@579
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->IWLAN_OPERATOR_PLMN_SELECT:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@57b
    aput-object v2, v0, v1

    #@57d
    const/16 v1, 0x3d

    #@57f
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->USER_WSID_LIST:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@581
    aput-object v2, v0, v1

    #@583
    const/16 v1, 0x3e

    #@585
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->OPERATOR_WSID_LIST:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@587
    aput-object v2, v0, v1

    #@589
    const/16 v1, 0x3f

    #@58b
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->VGCS_SECURITY:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@58d
    aput-object v2, v0, v1

    #@58f
    const/16 v1, 0x40

    #@591
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->VBS_SECURITY:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@593
    aput-object v2, v0, v1

    #@595
    const/16 v1, 0x41

    #@597
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->WLAN_REAUTH_IDENTITY:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@599
    aput-object v2, v0, v1

    #@59b
    const/16 v1, 0x42

    #@59d
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->MM_STORAGE:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@59f
    aput-object v2, v0, v1

    #@5a1
    const/16 v1, 0x43

    #@5a3
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->GBA:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@5a5
    aput-object v2, v0, v1

    #@5a7
    const/16 v1, 0x44

    #@5a9
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->MBMS_SECURITY:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@5ab
    aput-object v2, v0, v1

    #@5ad
    const/16 v1, 0x45

    #@5af
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->DATA_DL_VIA_USSD:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@5b1
    aput-object v2, v0, v1

    #@5b3
    const/16 v1, 0x46

    #@5b5
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->EQUIVALENT_HPLMN:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@5b7
    aput-object v2, v0, v1

    #@5b9
    const/16 v1, 0x47

    #@5bb
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->TERMINAL_PROFILE_AFTER_UICC_ACTIVATION:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@5bd
    aput-object v2, v0, v1

    #@5bf
    const/16 v1, 0x48

    #@5c1
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->EQUIVALENT_HPLMN_PRESENTATION:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@5c3
    aput-object v2, v0, v1

    #@5c5
    const/16 v1, 0x49

    #@5c7
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->LAST_RPLMN_SELECTION_INDICATION:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@5c9
    aput-object v2, v0, v1

    #@5cb
    const/16 v1, 0x4a

    #@5cd
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->OMA_BCAST_PROFILE:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@5cf
    aput-object v2, v0, v1

    #@5d1
    const/16 v1, 0x4b

    #@5d3
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->GBA_LOCAL_KEY_ESTABLISHMENT:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@5d5
    aput-object v2, v0, v1

    #@5d7
    const/16 v1, 0x4c

    #@5d9
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->TERMINAL_APPLICATIONS:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@5db
    aput-object v2, v0, v1

    #@5dd
    const/16 v1, 0x4d

    #@5df
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->SPN_ICON:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@5e1
    aput-object v2, v0, v1

    #@5e3
    const/16 v1, 0x4e

    #@5e5
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->PLMN_NETWORK_NAME_ICON:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@5e7
    aput-object v2, v0, v1

    #@5e9
    const/16 v1, 0x4f

    #@5eb
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->USIM_IP_CONNECTION_PARAMS:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@5ed
    aput-object v2, v0, v1

    #@5ef
    const/16 v1, 0x50

    #@5f1
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->IWLAN_HOME_ID_LIST:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@5f3
    aput-object v2, v0, v1

    #@5f5
    const/16 v1, 0x51

    #@5f7
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->IWLAN_EQUIVALENT_HPLMN_PRESENTATION:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@5f9
    aput-object v2, v0, v1

    #@5fb
    const/16 v1, 0x52

    #@5fd
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->IWLAN_HPLMN_PRIORITY_INDICATION:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@5ff
    aput-object v2, v0, v1

    #@601
    const/16 v1, 0x53

    #@603
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->IWLAN_LAST_REGISTERED_PLMN:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@605
    aput-object v2, v0, v1

    #@607
    const/16 v1, 0x54

    #@609
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->EPS_MOBILITY_MANAGEMENT_INFO:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@60b
    aput-object v2, v0, v1

    #@60d
    const/16 v1, 0x55

    #@60f
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->ALLOWED_CSG_LISTS_AND_INDICATIONS:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@611
    aput-object v2, v0, v1

    #@613
    const/16 v1, 0x56

    #@615
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->CALL_CONTROL_ON_EPS_PDN_CONNECTION_BY_USIM:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@617
    aput-object v2, v0, v1

    #@619
    const/16 v1, 0x57

    #@61b
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->HPLMN_DIRECT_ACCESS:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@61d
    aput-object v2, v0, v1

    #@61f
    const/16 v1, 0x58

    #@621
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->ECALL_DATA:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@623
    aput-object v2, v0, v1

    #@625
    const/16 v1, 0x59

    #@627
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->OPERATOR_CSG_LISTS_AND_INDICATIONS:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@629
    aput-object v2, v0, v1

    #@62b
    const/16 v1, 0x5a

    #@62d
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->SM_OVER_IP:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@62f
    aput-object v2, v0, v1

    #@631
    const/16 v1, 0x5b

    #@633
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->CSG_DISPLAY_CONTROL:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@635
    aput-object v2, v0, v1

    #@637
    const/16 v1, 0x5c

    #@639
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->IMS_COMMUNICATION_CONTROL_BY_USIM:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@63b
    aput-object v2, v0, v1

    #@63d
    const/16 v1, 0x5d

    #@63f
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->EXTENDED_TERMINAL_APPLICATIONS:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@641
    aput-object v2, v0, v1

    #@643
    const/16 v1, 0x5e

    #@645
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->UICC_ACCESS_TO_IMS:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@647
    aput-object v2, v0, v1

    #@649
    const/16 v1, 0x5f

    #@64b
    sget-object v2, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->NAS_CONFIG_BY_USIM:Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@64d
    aput-object v2, v0, v1

    #@64f
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->$VALUES:[Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@651
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 25
    const-class v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;
    .registers 1

    #@0
    .prologue
    .line 25
    sget-object v0, Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->$VALUES:[Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@2
    invoke-virtual {v0}, [Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/android/internal/telephony/uicc/UsimServiceTable$UsimService;

    #@8
    return-object v0
.end method
