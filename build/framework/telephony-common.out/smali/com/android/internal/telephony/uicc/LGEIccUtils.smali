.class public Lcom/android/internal/telephony/uicc/LGEIccUtils;
.super Ljava/lang/Object;
.source "LGEIccUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;,
        Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;
    }
.end annotation


# static fields
.field private static final FPLMN_ENTRY_SIZE:I = 0x3

.field static final LOG_TAG:Ljava/lang/String; = "LGEIccUtils"

.field private static final PLMNWACT_ENTRY_MAX:I = 0x3c

.field private static final PLMNWACT_ENTRY_SIZE:I = 0x5

.field private static final SIZE_OF_SMSP_NONE_ALPHA_ID:I = 0x1c

.field private static final SMSP_ADDRESS_SIZE:I = 0xc

.field private static final mCountry:Ljava/lang/String;

.field private static final mOperator:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 17
    const-string v0, "ro.build.target_operator"

    #@2
    const-string v1, "OPEN"

    #@4
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    sput-object v0, Lcom/android/internal/telephony/uicc/LGEIccUtils;->mOperator:Ljava/lang/String;

    #@a
    .line 18
    const-string v0, "ro.build.target_country"

    #@c
    const-string v1, "COM"

    #@e
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@11
    move-result-object v0

    #@12
    sput-object v0, Lcom/android/internal/telephony/uicc/LGEIccUtils;->mCountry:Ljava/lang/String;

    #@14
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 13
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 351
    return-void
.end method

.method public static decodeMdn([B)Ljava/lang/String;
    .registers 9
    .parameter "mMdn"

    #@0
    .prologue
    const/16 v7, 0xa

    #@2
    const/16 v6, 0x9

    #@4
    .line 569
    const-string v2, ""

    #@6
    .line 571
    .local v2, mEFMdn:Ljava/lang/String;
    const/4 v5, 0x0

    #@7
    :try_start_7
    aget-byte v5, p0, v5

    #@9
    if-gtz v5, :cond_d

    #@b
    .line 572
    const/4 v5, 0x0

    #@c
    .line 597
    :goto_c
    return-object v5

    #@d
    .line 574
    :cond_d
    new-instance v3, Ljava/lang/StringBuilder;

    #@f
    const/16 v5, 0x10

    #@11
    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    #@14
    .line 575
    .local v3, ret:Ljava/lang/StringBuilder;
    const/4 v1, 0x1

    #@15
    .local v1, i:I
    :goto_15
    const/16 v5, 0x11

    #@17
    if-ge v1, v5, :cond_22

    #@19
    .line 579
    aget-byte v5, p0, v1

    #@1b
    and-int/lit8 v4, v5, 0xf

    #@1d
    .line 580
    .local v4, v:I
    if-ne v4, v7, :cond_20

    #@1f
    const/4 v4, 0x0

    #@20
    .line 581
    :cond_20
    if-le v4, v6, :cond_40

    #@22
    .line 590
    .end local v4           #v:I
    :cond_22
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v2

    #@26
    .line 591
    const-string v5, "LGEIccUtils"

    #@28
    new-instance v6, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v7, "mEFMdn : "

    #@2f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v6

    #@33
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v6

    #@37
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v6

    #@3b
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    .end local v1           #i:I
    .end local v3           #ret:Ljava/lang/StringBuilder;
    :goto_3e
    move-object v5, v2

    #@3f
    .line 597
    goto :goto_c

    #@40
    .line 582
    .restart local v1       #i:I
    .restart local v3       #ret:Ljava/lang/StringBuilder;
    .restart local v4       #v:I
    :cond_40
    add-int/lit8 v5, v4, 0x30

    #@42
    int-to-char v5, v5

    #@43
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@46
    .line 584
    aget-byte v5, p0, v1

    #@48
    shr-int/lit8 v5, v5, 0x4

    #@4a
    and-int/lit8 v4, v5, 0xf

    #@4c
    .line 585
    if-ne v4, v7, :cond_4f

    #@4e
    const/4 v4, 0x0

    #@4f
    .line 586
    :cond_4f
    if-gt v4, v6, :cond_22

    #@51
    .line 587
    add-int/lit8 v5, v4, 0x30

    #@53
    int-to-char v5, v5

    #@54
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_57
    .catch Ljava/lang/RuntimeException; {:try_start_7 .. :try_end_57} :catch_5a

    #@57
    .line 575
    add-int/lit8 v1, v1, 0x1

    #@59
    goto :goto_15

    #@5a
    .line 593
    .end local v1           #i:I
    .end local v3           #ret:Ljava/lang/StringBuilder;
    .end local v4           #v:I
    :catch_5a
    move-exception v0

    #@5b
    .line 594
    .local v0, ex:Ljava/lang/RuntimeException;
    const-string v5, "LGEIccUtils"

    #@5d
    const-string v6, "readEF_Mdn() RuntimeException  occur"

    #@5f
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@62
    goto :goto_3e
.end method

.method public static encodeMdn(Ljava/lang/String;)[B
    .registers 8
    .parameter "mdn"

    #@0
    .prologue
    .line 601
    const/4 v2, 0x0

    #@1
    .line 602
    .local v2, i:I
    const/4 v3, 0x0

    #@2
    .line 603
    .local v3, rawData:[B
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@5
    move-result v4

    #@6
    .line 606
    .local v4, sz:I
    const/16 v5, 0x30

    #@8
    const/16 v6, 0x41

    #@a
    :try_start_a
    invoke-virtual {p0, v5, v6}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    #@d
    move-result-object p0

    #@e
    .line 608
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    #@10
    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    #@13
    .line 611
    .local v0, buf:Ljava/io/ByteArrayOutputStream;
    const/16 v5, 0xb

    #@15
    invoke-virtual {v0, v5}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@18
    .line 614
    const/4 v2, 0x0

    #@19
    :goto_19
    const/16 v5, 0x10

    #@1b
    if-ge v2, v5, :cond_5c

    #@1d
    .line 616
    add-int/lit8 v5, v4, -0x1

    #@1f
    if-ne v2, v5, :cond_32

    #@21
    .line 617
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    #@24
    move-result v5

    #@25
    invoke-static {v5}, Lcom/android/internal/telephony/uicc/IccUtils;->hexCharToInt(C)I

    #@28
    move-result v5

    #@29
    or-int/lit16 v5, v5, 0xf0

    #@2b
    int-to-byte v5, v5

    #@2c
    invoke-virtual {v0, v5}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@2f
    .line 614
    :goto_2f
    add-int/lit8 v2, v2, 0x2

    #@31
    goto :goto_19

    #@32
    .line 618
    :cond_32
    if-lt v2, v4, :cond_42

    #@34
    .line 619
    const/4 v5, -0x1

    #@35
    invoke-virtual {v0, v5}, Ljava/io/ByteArrayOutputStream;->write(I)V
    :try_end_38
    .catch Ljava/lang/RuntimeException; {:try_start_a .. :try_end_38} :catch_39

    #@38
    goto :goto_2f

    #@39
    .line 633
    .end local v0           #buf:Ljava/io/ByteArrayOutputStream;
    :catch_39
    move-exception v1

    #@3a
    .line 634
    .local v1, ex:Ljava/lang/RuntimeException;
    const-string v5, "LGEIccUtils"

    #@3c
    const-string v6, "writeEF_Mdn() RuntimeException  occur"

    #@3e
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@41
    .line 636
    .end local v1           #ex:Ljava/lang/RuntimeException;
    :goto_41
    return-object v3

    #@42
    .line 621
    .restart local v0       #buf:Ljava/io/ByteArrayOutputStream;
    :cond_42
    :try_start_42
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    #@45
    move-result v5

    #@46
    invoke-static {v5}, Lcom/android/internal/telephony/uicc/IccUtils;->hexCharToInt(C)I

    #@49
    move-result v5

    #@4a
    add-int/lit8 v6, v2, 0x1

    #@4c
    invoke-virtual {p0, v6}, Ljava/lang/String;->charAt(I)C

    #@4f
    move-result v6

    #@50
    invoke-static {v6}, Lcom/android/internal/telephony/uicc/IccUtils;->hexCharToInt(C)I

    #@53
    move-result v6

    #@54
    shl-int/lit8 v6, v6, 0x4

    #@56
    or-int/2addr v5, v6

    #@57
    int-to-byte v5, v5

    #@58
    invoke-virtual {v0, v5}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@5b
    goto :goto_2f

    #@5c
    .line 626
    :cond_5c
    const/16 v5, 0xa

    #@5e
    invoke-virtual {v0, v5}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@61
    .line 629
    const/4 v5, 0x0

    #@62
    invoke-virtual {v0, v5}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@65
    .line 631
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_68
    .catch Ljava/lang/RuntimeException; {:try_start_42 .. :try_end_68} :catch_39

    #@68
    move-result-object v3

    #@69
    goto :goto_41
.end method

.method public static getCdmaUsimOperator()Z
    .registers 3

    #@0
    .prologue
    .line 34
    const/4 v0, 0x0

    #@1
    .line 35
    .local v0, retValue:Z
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@4
    move-result-object v1

    #@5
    const-string v2, "USC"

    #@7
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a
    move-result v1

    #@b
    if-nez v1, :cond_25

    #@d
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@10
    move-result-object v1

    #@11
    const-string v2, "MPCS"

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@16
    move-result v1

    #@17
    if-nez v1, :cond_25

    #@19
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    const-string v2, "SPR"

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@22
    move-result v1

    #@23
    if-eqz v1, :cond_26

    #@25
    .line 37
    :cond_25
    const/4 v0, 0x1

    #@26
    .line 39
    :cond_26
    return v0
.end method

.method public static getCountry()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 29
    sget-object v0, Lcom/android/internal/telephony/uicc/LGEIccUtils;->mCountry:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public static getOperator()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 25
    sget-object v0, Lcom/android/internal/telephony/uicc/LGEIccUtils;->mOperator:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method private getPLMNfromSimData([B)Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;
    .registers 16
    .parameter "SimData"

    #@0
    .prologue
    const/4 v11, -0x1

    #@1
    const/4 v13, 0x1

    #@2
    const/4 v12, 0x0

    #@3
    .line 99
    if-nez p1, :cond_7

    #@5
    const/4 v0, 0x0

    #@6
    .line 152
    :goto_6
    return-object v0

    #@7
    .line 101
    :cond_7
    aget-byte v7, p1, v12

    #@9
    .line 102
    .local v7, plmn1:B
    aget-byte v8, p1, v13

    #@b
    .line 103
    .local v8, plmn2:B
    const/4 v10, 0x2

    #@c
    aget-byte v9, p1, v10

    #@e
    .line 105
    .local v9, plmn3:B
    new-instance v0, Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;

    #@10
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;-><init>(Lcom/android/internal/telephony/uicc/LGEIccUtils;)V

    #@13
    .line 107
    .local v0, bufPLMN:Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;
    const/4 v6, 0x0

    #@14
    .local v6, mnc_digit_3:I
    move v5, v6

    #@15
    .local v5, mnc_digit_2:I
    move v4, v6

    #@16
    .line 109
    .local v4, mnc_digit_1:I
    if-ne v7, v11, :cond_3e

    #@18
    if-ne v8, v11, :cond_3e

    #@1a
    if-ne v9, v11, :cond_3e

    #@1c
    .line 111
    iput v12, v0, Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;->mcc:I

    #@1e
    .line 112
    iput v12, v0, Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;->mnc:I

    #@20
    .line 113
    iput-boolean v13, v0, Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;->includepcs:Z

    #@22
    .line 139
    :goto_22
    array-length v10, p1

    #@23
    const/4 v11, 0x5

    #@24
    if-ne v10, v11, :cond_6f

    #@26
    .line 141
    iget-object v10, v0, Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;->act:[B

    #@28
    const/4 v11, 0x3

    #@29
    aget-byte v11, p1, v11

    #@2b
    aput-byte v11, v10, v12

    #@2d
    .line 142
    iget-object v10, v0, Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;->act:[B

    #@2f
    const/4 v11, 0x4

    #@30
    aget-byte v11, p1, v11

    #@32
    aput-byte v11, v10, v13

    #@34
    .line 150
    :goto_34
    const-string v10, "LGEIccUtils"

    #@36
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->logPLMNtoString(Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;)Ljava/lang/String;

    #@39
    move-result-object v11

    #@3a
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3d
    goto :goto_6

    #@3e
    .line 117
    :cond_3e
    and-int/lit8 v1, v7, 0xf

    #@40
    .line 118
    .local v1, mcc_digit_1:I
    shr-int/lit8 v10, v7, 0x4

    #@42
    and-int/lit8 v2, v10, 0xf

    #@44
    .line 119
    .local v2, mcc_digit_2:I
    and-int/lit8 v3, v8, 0xf

    #@46
    .line 121
    .local v3, mcc_digit_3:I
    and-int/lit8 v4, v9, 0xf

    #@48
    .line 122
    shr-int/lit8 v10, v9, 0x4

    #@4a
    and-int/lit8 v5, v10, 0xf

    #@4c
    .line 123
    shr-int/lit8 v10, v8, 0x4

    #@4e
    and-int/lit8 v6, v10, 0xf

    #@50
    .line 125
    mul-int/lit8 v10, v1, 0x64

    #@52
    mul-int/lit8 v11, v2, 0xa

    #@54
    add-int/2addr v10, v11

    #@55
    add-int/2addr v10, v3

    #@56
    iput v10, v0, Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;->mcc:I

    #@58
    .line 127
    const/16 v10, 0xf

    #@5a
    if-ne v6, v10, :cond_64

    #@5c
    .line 129
    iput-boolean v12, v0, Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;->includepcs:Z

    #@5e
    .line 130
    mul-int/lit8 v10, v4, 0xa

    #@60
    add-int/2addr v10, v5

    #@61
    iput v10, v0, Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;->mnc:I

    #@63
    goto :goto_22

    #@64
    .line 134
    :cond_64
    iput-boolean v13, v0, Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;->includepcs:Z

    #@66
    .line 135
    mul-int/lit8 v10, v4, 0x64

    #@68
    mul-int/lit8 v11, v5, 0xa

    #@6a
    add-int/2addr v10, v11

    #@6b
    add-int/2addr v10, v6

    #@6c
    iput v10, v0, Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;->mnc:I

    #@6e
    goto :goto_22

    #@6f
    .line 146
    .end local v1           #mcc_digit_1:I
    .end local v2           #mcc_digit_2:I
    .end local v3           #mcc_digit_3:I
    :cond_6f
    iget-object v10, v0, Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;->act:[B

    #@71
    aput-byte v12, v10, v12

    #@73
    .line 147
    iget-object v10, v0, Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;->act:[B

    #@75
    aput-byte v12, v10, v13

    #@77
    goto :goto_34
.end method

.method public static subarray([BII)[B
    .registers 7
    .parameter "array"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 56
    if-eqz p0, :cond_7

    #@2
    if-gt p1, p2, :cond_7

    #@4
    array-length v3, p0

    #@5
    if-ge v3, p2, :cond_9

    #@7
    :cond_7
    const/4 v0, 0x0

    #@8
    .line 63
    :cond_8
    return-object v0

    #@9
    .line 58
    :cond_9
    sub-int v3, p2, p1

    #@b
    add-int/lit8 v3, v3, 0x1

    #@d
    new-array v0, v3, [B

    #@f
    .line 59
    .local v0, arrayBuf:[B
    move v1, p1

    #@10
    .local v1, i:I
    const/4 v2, 0x0

    #@11
    .local v2, j:I
    :goto_11
    if-gt v1, p2, :cond_8

    #@13
    .line 61
    aget-byte v3, p0, v1

    #@15
    aput-byte v3, v0, v2

    #@17
    .line 59
    add-int/lit8 v1, v1, 0x1

    #@19
    add-int/lit8 v2, v2, 0x1

    #@1b
    goto :goto_11
.end method


# virtual methods
.method public SMSPtoString(Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;)Ljava/lang/String;
    .registers 4
    .parameter "smspData"

    #@0
    .prologue
    .line 473
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "alphaID:"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    iget-object v0, p1, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;->alphaID:[B

    #@d
    if-nez v0, :cond_6a

    #@f
    const-string v0, "NULL"

    #@11
    :goto_11
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    const-string v1, " ,ParamIndi:"

    #@17
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v0

    #@1b
    iget-byte v1, p1, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;->ParamIndicator:B

    #@1d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20
    move-result-object v0

    #@21
    const-string v1, " ,Dest:"

    #@23
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v0

    #@27
    iget-object v1, p1, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;->DestAddr:[B

    #@29
    invoke-static {v1}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@2c
    move-result-object v1

    #@2d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v0

    #@31
    const-string v1, " ,Center:"

    #@33
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v0

    #@37
    iget-object v1, p1, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;->SVCCenterAddr:[B

    #@39
    invoke-static {v1}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@3c
    move-result-object v1

    #@3d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v0

    #@41
    const-string v1, " ,ProtoclID:"

    #@43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v0

    #@47
    iget-byte v1, p1, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;->ProtoclID:B

    #@49
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v0

    #@4d
    const-string v1, " ,Code:"

    #@4f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v0

    #@53
    iget-byte v1, p1, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;->CodeScheme:B

    #@55
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@58
    move-result-object v0

    #@59
    const-string v1, " ,Period:"

    #@5b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v0

    #@5f
    iget-byte v1, p1, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;->ValidPeriod:B

    #@61
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@64
    move-result-object v0

    #@65
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@68
    move-result-object v0

    #@69
    return-object v0

    #@6a
    :cond_6a
    iget-object v0, p1, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;->alphaID:[B

    #@6c
    invoke-static {v0}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@6f
    move-result-object v0

    #@70
    goto :goto_11
.end method

.method public analyzeFPLMN([B)Ljava/util/ArrayList;
    .registers 11
    .parameter "SimData"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    const/16 v4, 0x3c

    #@2
    const/4 v8, 0x0

    #@3
    .line 269
    new-instance v3, Ljava/util/ArrayList;

    #@5
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@8
    .line 271
    .local v3, mPLMNListType:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;>;"
    if-nez p1, :cond_c

    #@a
    .line 272
    const/4 v3, 0x0

    #@b
    .line 292
    .end local v3           #mPLMNListType:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;>;"
    :cond_b
    return-object v3

    #@c
    .line 274
    .restart local v3       #mPLMNListType:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;>;"
    :cond_c
    const/4 v6, 0x5

    #@d
    new-array v0, v6, [B

    #@f
    .line 276
    .local v0, PLMNdata:[B
    array-length v6, p1

    #@10
    div-int/lit8 v6, v6, 0x3

    #@12
    if-lt v6, v4, :cond_33

    #@14
    .line 279
    .local v4, max_loop_count:I
    :goto_14
    const/4 v2, 0x0

    #@15
    .local v2, i:I
    :goto_15
    if-ge v2, v4, :cond_b

    #@17
    .line 281
    mul-int/lit8 v6, v2, 0x3

    #@19
    add-int/lit8 v7, v2, 0x1

    #@1b
    mul-int/lit8 v7, v7, 0x3

    #@1d
    add-int/lit8 v7, v7, -0x1

    #@1f
    invoke-static {p1, v6, v7}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->subarray([BII)[B

    #@22
    move-result-object v5

    #@23
    .line 283
    .local v5, subSimData:[B
    const/4 v6, 0x3

    #@24
    invoke-static {v5, v8, v0, v8, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@27
    .line 285
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getPLMNfromSimData([B)Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;

    #@2a
    move-result-object v1

    #@2b
    .line 286
    .local v1, TempPLMN:Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;
    if-eqz v1, :cond_30

    #@2d
    .line 288
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@30
    .line 279
    :cond_30
    add-int/lit8 v2, v2, 0x1

    #@32
    goto :goto_15

    #@33
    .line 276
    .end local v1           #TempPLMN:Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;
    .end local v2           #i:I
    .end local v4           #max_loop_count:I
    .end local v5           #subSimData:[B
    :cond_33
    array-length v6, p1

    #@34
    div-int/lit8 v4, v6, 0x3

    #@36
    goto :goto_14
.end method

.method public analyzePLMN([B)Ljava/util/ArrayList;
    .registers 9
    .parameter "SimData"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    const/16 v3, 0x3c

    #@2
    .line 71
    new-instance v2, Ljava/util/ArrayList;

    #@4
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@7
    .line 73
    .local v2, mPLMNListType:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;>;"
    if-nez p1, :cond_b

    #@9
    .line 74
    const/4 v2, 0x0

    #@a
    .line 89
    .end local v2           #mPLMNListType:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;>;"
    :cond_a
    return-object v2

    #@b
    .line 76
    .restart local v2       #mPLMNListType:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;>;"
    :cond_b
    array-length v5, p1

    #@c
    div-int/lit8 v5, v5, 0x5

    #@e
    if-lt v5, v3, :cond_24

    #@10
    .line 78
    .local v3, max_loop_count:I
    :goto_10
    const/4 v1, 0x0

    #@11
    .local v1, i:I
    :goto_11
    if-ge v1, v3, :cond_a

    #@13
    .line 80
    mul-int/lit8 v5, v1, 0x5

    #@15
    add-int/lit8 v6, v1, 0x1

    #@17
    mul-int/lit8 v6, v6, 0x5

    #@19
    add-int/lit8 v6, v6, -0x1

    #@1b
    invoke-static {p1, v5, v6}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->subarray([BII)[B

    #@1e
    move-result-object v4

    #@1f
    .line 81
    .local v4, subSimData:[B
    if-nez v4, :cond_28

    #@21
    .line 78
    :cond_21
    :goto_21
    add-int/lit8 v1, v1, 0x1

    #@23
    goto :goto_11

    #@24
    .line 76
    .end local v1           #i:I
    .end local v3           #max_loop_count:I
    .end local v4           #subSimData:[B
    :cond_24
    array-length v5, p1

    #@25
    div-int/lit8 v3, v5, 0x5

    #@27
    goto :goto_10

    #@28
    .line 83
    .restart local v1       #i:I
    .restart local v3       #max_loop_count:I
    .restart local v4       #subSimData:[B
    :cond_28
    invoke-direct {p0, v4}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getPLMNfromSimData([B)Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;

    #@2b
    move-result-object v0

    #@2c
    .line 84
    .local v0, TempPLMN:Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;
    if-eqz v0, :cond_21

    #@2e
    .line 86
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@31
    goto :goto_21
.end method

.method public analyzeSMSP([B)Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;
    .registers 10
    .parameter "SimData"

    #@0
    .prologue
    const/16 v7, 0xc

    #@2
    const/4 v6, 0x0

    #@3
    .line 378
    new-instance v1, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;

    #@5
    invoke-direct {v1, p0}, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;-><init>(Lcom/android/internal/telephony/uicc/LGEIccUtils;)V

    #@8
    .line 380
    .local v1, TempSMSP:Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;
    if-nez p1, :cond_c

    #@a
    const/4 v1, 0x0

    #@b
    .line 423
    .end local v1           #TempSMSP:Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;
    :goto_b
    return-object v1

    #@c
    .line 382
    .restart local v1       #TempSMSP:Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;
    :cond_c
    array-length v4, p1

    #@d
    add-int/lit8 v0, v4, -0x1c

    #@f
    .line 383
    .local v0, NoneAlphaStartPoint:I
    const/4 v2, 0x0

    #@10
    .line 385
    .local v2, arrayPoint:I
    if-lez v0, :cond_1b

    #@12
    .line 387
    add-int/lit8 v4, v0, -0x1

    #@14
    invoke-static {p1, v2, v4}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->subarray([BII)[B

    #@17
    move-result-object v4

    #@18
    iput-object v4, v1, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;->alphaID:[B

    #@1a
    .line 388
    move v2, v0

    #@1b
    .line 391
    :cond_1b
    add-int/lit8 v3, v2, 0x1

    #@1d
    .end local v2           #arrayPoint:I
    .local v3, arrayPoint:I
    aget-byte v4, p1, v2

    #@1f
    iput-byte v4, v1, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;->ParamIndicator:B

    #@21
    .line 393
    iget-object v4, v1, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;->DestAddr:[B

    #@23
    invoke-static {p1, v3, v4, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@26
    .line 394
    add-int/lit8 v2, v3, 0xc

    #@28
    .line 396
    .end local v3           #arrayPoint:I
    .restart local v2       #arrayPoint:I
    iget-byte v4, v1, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;->ParamIndicator:B

    #@2a
    and-int/lit8 v4, v4, 0x2

    #@2c
    const/4 v5, 0x2

    #@2d
    if-eq v4, v5, :cond_34

    #@2f
    .line 399
    iget-object v4, v1, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;->SVCCenterAddr:[B

    #@31
    invoke-static {p1, v2, v4, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@34
    .line 401
    :cond_34
    add-int/lit8 v2, v2, 0xc

    #@36
    .line 403
    iget-byte v4, v1, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;->ParamIndicator:B

    #@38
    and-int/lit8 v4, v4, 0x4

    #@3a
    const/4 v5, 0x4

    #@3b
    if-eq v4, v5, :cond_76

    #@3d
    .line 405
    add-int/lit8 v3, v2, 0x1

    #@3f
    .end local v2           #arrayPoint:I
    .restart local v3       #arrayPoint:I
    aget-byte v4, p1, v2

    #@41
    iput-byte v4, v1, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;->ProtoclID:B

    #@43
    move v2, v3

    #@44
    .line 411
    .end local v3           #arrayPoint:I
    .restart local v2       #arrayPoint:I
    :goto_44
    iget-byte v4, v1, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;->ParamIndicator:B

    #@46
    and-int/lit8 v4, v4, 0x8

    #@48
    const/16 v5, 0x8

    #@4a
    if-eq v4, v5, :cond_79

    #@4c
    .line 413
    add-int/lit8 v3, v2, 0x1

    #@4e
    .end local v2           #arrayPoint:I
    .restart local v3       #arrayPoint:I
    aget-byte v4, p1, v2

    #@50
    iput-byte v4, v1, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;->CodeScheme:B

    #@52
    move v2, v3

    #@53
    .line 419
    .end local v3           #arrayPoint:I
    .restart local v2       #arrayPoint:I
    :goto_53
    add-int/lit8 v3, v2, 0x1

    #@55
    .end local v2           #arrayPoint:I
    .restart local v3       #arrayPoint:I
    aget-byte v4, p1, v2

    #@57
    iput-byte v4, v1, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;->ValidPeriod:B

    #@59
    .line 421
    const-string v4, "LGEIccUtils"

    #@5b
    new-instance v5, Ljava/lang/StringBuilder;

    #@5d
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@60
    const-string v6, "SMSP: "

    #@62
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v5

    #@66
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->SMSPtoString(Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;)Ljava/lang/String;

    #@69
    move-result-object v6

    #@6a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v5

    #@6e
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@71
    move-result-object v5

    #@72
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@75
    goto :goto_b

    #@76
    .line 408
    .end local v3           #arrayPoint:I
    .restart local v2       #arrayPoint:I
    :cond_76
    add-int/lit8 v2, v2, 0x1

    #@78
    goto :goto_44

    #@79
    .line 416
    :cond_79
    add-int/lit8 v2, v2, 0x1

    #@7b
    goto :goto_53
.end method

.method public composeFPLMN(Ljava/util/ArrayList;)[B
    .registers 15
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;",
            ">;)[B"
        }
    .end annotation

    #@0
    .prologue
    .local p1, FplmnList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;>;"
    const/16 v12, 0x3e7

    #@2
    const/4 v11, -0x1

    #@3
    .line 299
    const/4 v5, 0x0

    #@4
    .line 302
    .local v5, mnc_includes_pcs_digit:Z
    if-nez p1, :cond_8

    #@6
    const/4 v0, 0x0

    #@7
    .line 344
    :goto_7
    return-object v0

    #@8
    .line 304
    :cond_8
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    #@b
    move-result v6

    #@c
    .line 306
    .local v6, num:I
    mul-int/lit8 v9, v6, 0x3

    #@e
    new-array v0, v9, [B

    #@10
    .line 308
    .local v0, SimData:[B
    const/4 v1, 0x0

    #@11
    .line 309
    .local v1, i:I
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@14
    move-result-object v2

    #@15
    .local v2, i$:Ljava/util/Iterator;
    :goto_15
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@18
    move-result v9

    #@19
    if-eqz v9, :cond_63

    #@1b
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1e
    move-result-object v7

    #@1f
    check-cast v7, Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;

    #@21
    .line 311
    .local v7, tempPLMN:Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;
    iget v3, v7, Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;->mcc:I

    #@23
    .line 312
    .local v3, mcc:I
    iget v4, v7, Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;->mnc:I

    #@25
    .line 313
    .local v4, mnc:I
    iget-boolean v5, v7, Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;->includepcs:Z

    #@27
    .line 315
    if-gt v3, v12, :cond_37

    #@29
    if-lez v3, :cond_37

    #@2b
    if-ltz v4, :cond_37

    #@2d
    if-eqz v5, :cond_31

    #@2f
    if-gt v4, v12, :cond_37

    #@31
    :cond_31
    if-nez v5, :cond_41

    #@33
    const/16 v9, 0x63

    #@35
    if-le v4, v9, :cond_41

    #@37
    .line 319
    :cond_37
    const-string v9, "LGEIccUtils"

    #@39
    const-string v10, "invalid mcc mnc "

    #@3b
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    .line 320
    const/4 v3, 0x0

    #@3f
    .line 321
    const/4 v4, 0x0

    #@40
    .line 322
    const/4 v5, 0x1

    #@41
    .line 325
    :cond_41
    const-string v9, "LGEIccUtils"

    #@43
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->logPLMNtoString(Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;)Ljava/lang/String;

    #@46
    move-result-object v10

    #@47
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4a
    .line 327
    invoke-virtual {p0, v5, v3, v4}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->setPLMNfromMCCMNC(ZII)[B

    #@4d
    move-result-object v8

    #@4e
    .line 328
    .local v8, tempSim:[B
    if-nez v8, :cond_5d

    #@50
    .line 330
    aput-byte v11, v0, v1

    #@52
    .line 331
    add-int/lit8 v9, v1, 0x1

    #@54
    aput-byte v11, v0, v9

    #@56
    .line 332
    add-int/lit8 v9, v1, 0x2

    #@58
    aput-byte v11, v0, v9

    #@5a
    .line 339
    :goto_5a
    add-int/lit8 v1, v1, 0x3

    #@5c
    goto :goto_15

    #@5d
    .line 336
    :cond_5d
    const/4 v9, 0x0

    #@5e
    const/4 v10, 0x3

    #@5f
    invoke-static {v8, v9, v0, v1, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@62
    goto :goto_5a

    #@63
    .line 342
    .end local v3           #mcc:I
    .end local v4           #mnc:I
    .end local v7           #tempPLMN:Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;
    .end local v8           #tempSim:[B
    :cond_63
    const-string v9, "LGEIccUtils"

    #@65
    new-instance v10, Ljava/lang/StringBuilder;

    #@67
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@6a
    const-string v11, "FPLMNData "

    #@6c
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v10

    #@70
    invoke-static {v0}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@73
    move-result-object v11

    #@74
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v10

    #@78
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7b
    move-result-object v10

    #@7c
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7f
    goto :goto_7
.end method

.method public composePLMN(Ljava/util/ArrayList;)[B
    .registers 15
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;",
            ">;)[B"
        }
    .end annotation

    #@0
    .prologue
    .line 159
    .local p1, PlmnList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;>;"
    const/4 v6, 0x0

    #@1
    .line 162
    .local v6, mnc_includes_pcs_digit:Z
    if-nez p1, :cond_5

    #@3
    const/4 v0, 0x0

    #@4
    .line 205
    :goto_4
    return-object v0

    #@5
    .line 164
    :cond_5
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    #@8
    move-result v7

    #@9
    .line 165
    .local v7, num:I
    mul-int/lit8 v10, v7, 0x5

    #@b
    new-array v0, v10, [B

    #@d
    .line 167
    .local v0, SimData:[B
    const/4 v1, 0x0

    #@e
    .line 168
    .local v1, i:I
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@11
    move-result-object v3

    #@12
    .local v3, i$:Ljava/util/Iterator;
    :goto_12
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@15
    move-result v10

    #@16
    if-eqz v10, :cond_79

    #@18
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1b
    move-result-object v8

    #@1c
    check-cast v8, Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;

    #@1e
    .line 170
    .local v8, tempPLMN:Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;
    iget v4, v8, Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;->mcc:I

    #@20
    .line 171
    .local v4, mcc:I
    iget v5, v8, Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;->mnc:I

    #@22
    .line 172
    .local v5, mnc:I
    iget-boolean v6, v8, Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;->includepcs:Z

    #@24
    .line 174
    const/16 v10, 0x3e7

    #@26
    if-gt v4, v10, :cond_38

    #@28
    if-lez v4, :cond_38

    #@2a
    if-ltz v5, :cond_38

    #@2c
    if-eqz v6, :cond_32

    #@2e
    const/16 v10, 0x3e7

    #@30
    if-gt v5, v10, :cond_38

    #@32
    :cond_32
    if-nez v6, :cond_42

    #@34
    const/16 v10, 0x63

    #@36
    if-le v5, v10, :cond_42

    #@38
    .line 178
    :cond_38
    const-string v10, "LGEIccUtils"

    #@3a
    const-string v11, "invalid mcc mnc "

    #@3c
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    .line 179
    const/4 v4, 0x0

    #@40
    .line 180
    const/4 v5, 0x0

    #@41
    .line 181
    const/4 v6, 0x1

    #@42
    .line 184
    :cond_42
    const-string v10, "LGEIccUtils"

    #@44
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->logPLMNtoString(Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;)Ljava/lang/String;

    #@47
    move-result-object v11

    #@48
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4b
    .line 186
    invoke-virtual {p0, v6, v4, v5}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->setPLMNfromMCCMNC(ZII)[B

    #@4e
    move-result-object v9

    #@4f
    .line 187
    .local v9, tempSim:[B
    if-nez v9, :cond_73

    #@51
    .line 189
    const/4 v10, -0x1

    #@52
    aput-byte v10, v0, v1

    #@54
    .line 190
    add-int/lit8 v10, v1, 0x1

    #@56
    const/4 v11, -0x1

    #@57
    aput-byte v11, v0, v10

    #@59
    .line 191
    add-int/lit8 v10, v1, 0x2

    #@5b
    const/4 v11, -0x1

    #@5c
    aput-byte v11, v0, v10

    #@5e
    .line 198
    :goto_5e
    add-int/lit8 v1, v1, 0x3

    #@60
    .line 199
    add-int/lit8 v2, v1, 0x1

    #@62
    .end local v1           #i:I
    .local v2, i:I
    iget-object v10, v8, Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;->act:[B

    #@64
    const/4 v11, 0x0

    #@65
    aget-byte v10, v10, v11

    #@67
    aput-byte v10, v0, v1

    #@69
    .line 200
    add-int/lit8 v1, v2, 0x1

    #@6b
    .end local v2           #i:I
    .restart local v1       #i:I
    iget-object v10, v8, Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;->act:[B

    #@6d
    const/4 v11, 0x1

    #@6e
    aget-byte v10, v10, v11

    #@70
    aput-byte v10, v0, v2

    #@72
    goto :goto_12

    #@73
    .line 195
    :cond_73
    const/4 v10, 0x0

    #@74
    const/4 v11, 0x3

    #@75
    invoke-static {v9, v10, v0, v1, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@78
    goto :goto_5e

    #@79
    .line 203
    .end local v4           #mcc:I
    .end local v5           #mnc:I
    .end local v8           #tempPLMN:Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;
    .end local v9           #tempSim:[B
    :cond_79
    const-string v10, "LGEIccUtils"

    #@7b
    new-instance v11, Ljava/lang/StringBuilder;

    #@7d
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@80
    const-string v12, "SimData "

    #@82
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v11

    #@86
    invoke-static {v0}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@89
    move-result-object v12

    #@8a
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8d
    move-result-object v11

    #@8e
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@91
    move-result-object v11

    #@92
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@95
    goto/16 :goto_4
.end method

.method public composeSMSP(Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;)[B
    .registers 9
    .parameter "smspData"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 431
    if-nez p1, :cond_5

    #@3
    const/4 v0, 0x0

    #@4
    .line 468
    :goto_4
    return-object v0

    #@5
    .line 432
    :cond_5
    const-string v3, "LGEIccUtils"

    #@7
    new-instance v5, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v6, "SMSP: "

    #@e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v5

    #@12
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->SMSPtoString(Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;)Ljava/lang/String;

    #@15
    move-result-object v6

    #@16
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v5

    #@1a
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v5

    #@1e
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    .line 434
    iget-object v3, p1, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;->alphaID:[B

    #@23
    if-nez v3, :cond_cc

    #@25
    move v3, v4

    #@26
    :goto_26
    iget-object v5, p1, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;->DestAddr:[B

    #@28
    array-length v5, v5

    #@29
    add-int/2addr v3, v5

    #@2a
    iget-object v5, p1, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;->SVCCenterAddr:[B

    #@2c
    array-length v5, v5

    #@2d
    add-int/2addr v3, v5

    #@2e
    add-int/lit8 v3, v3, 0x4

    #@30
    new-array v0, v3, [B

    #@32
    .line 435
    .local v0, SimData:[B
    const/4 v1, 0x0

    #@33
    .line 437
    .local v1, arrayPoint:I
    iget-object v3, p1, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;->alphaID:[B

    #@35
    if-eqz v3, :cond_43

    #@37
    .line 438
    iget-object v3, p1, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;->alphaID:[B

    #@39
    iget-object v5, p1, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;->alphaID:[B

    #@3b
    array-length v5, v5

    #@3c
    invoke-static {v3, v4, v0, v1, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@3f
    .line 439
    iget-object v3, p1, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;->alphaID:[B

    #@41
    array-length v3, v3

    #@42
    add-int/2addr v1, v3

    #@43
    .line 442
    :cond_43
    iget-object v3, p1, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;->SVCCenterAddr:[B

    #@45
    aget-byte v3, v3, v4

    #@47
    const/4 v5, -0x1

    #@48
    if-eq v3, v5, :cond_50

    #@4a
    iget-object v3, p1, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;->SVCCenterAddr:[B

    #@4c
    aget-byte v3, v3, v4

    #@4e
    if-nez v3, :cond_d1

    #@50
    .line 444
    :cond_50
    iget-byte v3, p1, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;->ParamIndicator:B

    #@52
    or-int/lit8 v3, v3, 0x2

    #@54
    int-to-byte v3, v3

    #@55
    iput-byte v3, p1, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;->ParamIndicator:B

    #@57
    .line 450
    :goto_57
    const-string v3, "LGEIccUtils"

    #@59
    new-instance v5, Ljava/lang/StringBuilder;

    #@5b
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@5e
    const-string v6, "SVCCenterAddr[0] : "

    #@60
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v5

    #@64
    iget-object v6, p1, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;->SVCCenterAddr:[B

    #@66
    aget-byte v6, v6, v4

    #@68
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v5

    #@6c
    const-string v6, " ParamIndicator: "

    #@6e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v5

    #@72
    iget-byte v6, p1, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;->ParamIndicator:B

    #@74
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@77
    move-result-object v5

    #@78
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7b
    move-result-object v5

    #@7c
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7f
    .line 452
    add-int/lit8 v2, v1, 0x1

    #@81
    .end local v1           #arrayPoint:I
    .local v2, arrayPoint:I
    iget-byte v3, p1, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;->ParamIndicator:B

    #@83
    aput-byte v3, v0, v1

    #@85
    .line 455
    iget-object v3, p1, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;->DestAddr:[B

    #@87
    iget-object v5, p1, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;->DestAddr:[B

    #@89
    array-length v5, v5

    #@8a
    invoke-static {v3, v4, v0, v2, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@8d
    .line 456
    iget-object v3, p1, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;->DestAddr:[B

    #@8f
    array-length v3, v3

    #@90
    add-int v1, v2, v3

    #@92
    .line 458
    .end local v2           #arrayPoint:I
    .restart local v1       #arrayPoint:I
    iget-object v3, p1, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;->SVCCenterAddr:[B

    #@94
    iget-object v5, p1, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;->SVCCenterAddr:[B

    #@96
    array-length v5, v5

    #@97
    invoke-static {v3, v4, v0, v1, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@9a
    .line 459
    iget-object v3, p1, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;->SVCCenterAddr:[B

    #@9c
    array-length v3, v3

    #@9d
    add-int/2addr v1, v3

    #@9e
    .line 462
    add-int/lit8 v2, v1, 0x1

    #@a0
    .end local v1           #arrayPoint:I
    .restart local v2       #arrayPoint:I
    iget-byte v3, p1, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;->ProtoclID:B

    #@a2
    aput-byte v3, v0, v1

    #@a4
    .line 463
    add-int/lit8 v1, v2, 0x1

    #@a6
    .end local v2           #arrayPoint:I
    .restart local v1       #arrayPoint:I
    iget-byte v3, p1, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;->CodeScheme:B

    #@a8
    aput-byte v3, v0, v2

    #@aa
    .line 464
    iget-byte v3, p1, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;->ValidPeriod:B

    #@ac
    aput-byte v3, v0, v1

    #@ae
    .line 466
    const-string v3, "LGEIccUtils"

    #@b0
    new-instance v4, Ljava/lang/StringBuilder;

    #@b2
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@b5
    const-string v5, "SMSPData "

    #@b7
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v4

    #@bb
    invoke-static {v0}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@be
    move-result-object v5

    #@bf
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c2
    move-result-object v4

    #@c3
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c6
    move-result-object v4

    #@c7
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ca
    goto/16 :goto_4

    #@cc
    .line 434
    .end local v0           #SimData:[B
    .end local v1           #arrayPoint:I
    :cond_cc
    iget-object v3, p1, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;->alphaID:[B

    #@ce
    array-length v3, v3

    #@cf
    goto/16 :goto_26

    #@d1
    .line 447
    .restart local v0       #SimData:[B
    .restart local v1       #arrayPoint:I
    :cond_d1
    iget-byte v3, p1, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;->ParamIndicator:B

    #@d3
    and-int/lit16 v3, v3, 0xfd

    #@d5
    int-to-byte v3, v3

    #@d6
    iput-byte v3, p1, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;->ParamIndicator:B

    #@d8
    goto/16 :goto_57
.end method

.method public decodeSCAddr([B)Ljava/lang/String;
    .registers 8
    .parameter "scData"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 485
    const/4 v0, 0x0

    #@2
    .line 488
    .local v0, InternationalIndicator:C
    if-nez p1, :cond_6

    #@4
    const/4 v2, 0x0

    #@5
    .line 504
    :cond_5
    :goto_5
    return-object v2

    #@6
    .line 490
    :cond_6
    const/4 v3, 0x0

    #@7
    aget-byte v1, p1, v3

    #@9
    .line 492
    .local v1, length:I
    if-gtz v1, :cond_e

    #@b
    .line 493
    const-string v2, ""

    #@d
    .local v2, scAddrString:Ljava/lang/String;
    goto :goto_5

    #@e
    .line 496
    .end local v2           #scAddrString:Ljava/lang/String;
    :cond_e
    const/4 v3, 0x2

    #@f
    add-int/lit8 v4, v1, -0x1

    #@11
    invoke-static {p1, v3, v4}, Lcom/android/internal/telephony/uicc/IccUtils;->bcdToString([BII)Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    .line 498
    .restart local v2       #scAddrString:Ljava/lang/String;
    aget-byte v3, p1, v5

    #@17
    and-int/lit8 v3, v3, 0x70

    #@19
    shr-int/lit8 v3, v3, 0x4

    #@1b
    if-ne v3, v5, :cond_5

    #@1d
    .line 500
    const-string v3, "+"

    #@1f
    invoke-virtual {v3, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    goto :goto_5
.end method

.method public encodeSCAddr(Ljava/lang/String;[B)[B
    .registers 15
    .parameter "newSCAddr"
    .parameter "oldSCAddr"

    #@0
    .prologue
    const/4 v11, 0x0

    #@1
    const/4 v10, -0x1

    #@2
    const/4 v9, 0x1

    #@3
    .line 516
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@6
    move-result v7

    #@7
    if-nez v7, :cond_18

    #@9
    .line 518
    const/16 v7, 0xc

    #@b
    new-array v4, v7, [B

    #@d
    .line 519
    .local v4, scData:[B
    invoke-static {v4, v10}, Ljava/util/Arrays;->fill([BB)V

    #@10
    .line 520
    const-string v7, "LGEIccUtils"

    #@12
    const-string v8, "SMSPData SCAddr is Null: "

    #@14
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@17
    .line 564
    .end local v4           #scData:[B
    :goto_17
    return-object v4

    #@18
    .line 524
    :cond_18
    const/16 v7, 0x2b

    #@1a
    invoke-virtual {p1, v11}, Ljava/lang/String;->charAt(I)C

    #@1d
    move-result v8

    #@1e
    if-ne v7, v8, :cond_6b

    #@20
    .line 525
    const/4 v0, 0x1

    #@21
    .line 529
    .local v0, InternationalIndicator:B
    :goto_21
    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@24
    move-result-object v6

    #@25
    .line 532
    .local v6, tempString:Ljava/lang/String;
    :try_start_25
    const-string v7, "UTF-8"

    #@27
    invoke-virtual {v6, v7}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_2a
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_25 .. :try_end_2a} :catch_6d

    #@2a
    move-result-object v5

    #@2b
    .line 538
    .local v5, tempData:[B
    array-length v7, p2

    #@2c
    new-array v4, v7, [B

    #@2e
    .line 540
    .restart local v4       #scData:[B
    invoke-static {v4, v10}, Ljava/util/Arrays;->fill([BB)V

    #@31
    .line 542
    array-length v7, v5

    #@32
    div-int/lit8 v7, v7, 0x2

    #@34
    array-length v8, v5

    #@35
    rem-int/lit8 v8, v8, 0x2

    #@37
    add-int/2addr v7, v8

    #@38
    add-int/lit8 v7, v7, 0x1

    #@3a
    int-to-byte v7, v7

    #@3b
    aput-byte v7, v4, v11

    #@3d
    .line 544
    aget-byte v7, p2, v9

    #@3f
    const/16 v8, 0xff

    #@41
    if-eq v7, v8, :cond_70

    #@43
    .line 546
    shl-int/lit8 v7, v0, 0x4

    #@45
    aget-byte v8, p2, v9

    #@47
    and-int/lit16 v8, v8, 0x8f

    #@49
    or-int/2addr v7, v8

    #@4a
    int-to-byte v7, v7

    #@4b
    aput-byte v7, v4, v9

    #@4d
    .line 553
    :goto_4d
    const/4 v3, 0x0

    #@4e
    .local v3, j:I
    move v2, v3

    #@4f
    .local v2, i:I
    :goto_4f
    add-int/lit8 v7, v2, 0x1

    #@51
    array-length v8, v5

    #@52
    if-ge v7, v8, :cond_78

    #@54
    .line 556
    add-int/lit8 v7, v3, 0x2

    #@56
    aget-byte v8, v5, v2

    #@58
    and-int/lit8 v8, v8, 0xf

    #@5a
    add-int/lit8 v9, v2, 0x1

    #@5c
    aget-byte v9, v5, v9

    #@5e
    and-int/lit8 v9, v9, 0xf

    #@60
    shl-int/lit8 v9, v9, 0x4

    #@62
    or-int/2addr v8, v9

    #@63
    int-to-byte v8, v8

    #@64
    aput-byte v8, v4, v7

    #@66
    .line 553
    add-int/lit8 v2, v2, 0x2

    #@68
    add-int/lit8 v3, v3, 0x1

    #@6a
    goto :goto_4f

    #@6b
    .line 527
    .end local v0           #InternationalIndicator:B
    .end local v2           #i:I
    .end local v3           #j:I
    .end local v4           #scData:[B
    .end local v5           #tempData:[B
    .end local v6           #tempString:Ljava/lang/String;
    :cond_6b
    const/4 v0, 0x0

    #@6c
    .restart local v0       #InternationalIndicator:B
    goto :goto_21

    #@6d
    .line 534
    .restart local v6       #tempString:Ljava/lang/String;
    :catch_6d
    move-exception v1

    #@6e
    .local v1, e:Ljava/io/UnsupportedEncodingException;
    move-object v4, p2

    #@6f
    .line 535
    goto :goto_17

    #@70
    .line 550
    .end local v1           #e:Ljava/io/UnsupportedEncodingException;
    .restart local v4       #scData:[B
    .restart local v5       #tempData:[B
    :cond_70
    shl-int/lit8 v7, v0, 0x4

    #@72
    or-int/lit16 v7, v7, 0x81

    #@74
    int-to-byte v7, v7

    #@75
    aput-byte v7, v4, v9

    #@77
    goto :goto_4d

    #@78
    .line 559
    .restart local v2       #i:I
    .restart local v3       #j:I
    :cond_78
    array-length v7, v5

    #@79
    rem-int/lit8 v7, v7, 0x2

    #@7b
    if-eqz v7, :cond_88

    #@7d
    .line 560
    add-int/lit8 v7, v3, 0x2

    #@7f
    aget-byte v8, v5, v2

    #@81
    and-int/lit8 v8, v8, 0xf

    #@83
    or-int/lit16 v8, v8, 0xf0

    #@85
    int-to-byte v8, v8

    #@86
    aput-byte v8, v4, v7

    #@88
    .line 562
    :cond_88
    const-string v7, "LGEIccUtils"

    #@8a
    new-instance v8, Ljava/lang/StringBuilder;

    #@8c
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@8f
    const-string v9, "scData"

    #@91
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v8

    #@95
    invoke-static {v4}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@98
    move-result-object v9

    #@99
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v8

    #@9d
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a0
    move-result-object v8

    #@a1
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a4
    goto/16 :goto_17
.end method

.method public logPLMNtoString(Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;)Ljava/lang/String;
    .registers 5
    .parameter "logPLMN"

    #@0
    .prologue
    .line 260
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "mcc:"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget v1, p1, Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;->mcc:I

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, " ,mnc:"

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget v1, p1, Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;->mnc:I

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, " ,pcs:"

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    iget-boolean v1, p1, Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;->includepcs:Z

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    const-string v1, " ,act0:"

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    iget-object v1, p1, Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;->act:[B

    #@31
    const/4 v2, 0x0

    #@32
    aget-byte v1, v1, v2

    #@34
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@37
    move-result-object v0

    #@38
    const-string v1, " ,act1:"

    #@3a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v0

    #@3e
    iget-object v1, p1, Lcom/android/internal/telephony/uicc/LGEIccUtils$PLMNListType;->act:[B

    #@40
    const/4 v2, 0x1

    #@41
    aget-byte v1, v1, v2

    #@43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@46
    move-result-object v0

    #@47
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v0

    #@4b
    return-object v0
.end method

.method public setPLMNfromMCCMNC(ZII)[B
    .registers 14
    .parameter "mnc_includes_pcs_digit"
    .parameter "mcc"
    .parameter "mnc"

    #@0
    .prologue
    .line 217
    const/4 v7, 0x3

    #@1
    new-array v0, v7, [B

    #@3
    .line 219
    .local v0, arrayBuff:[B
    const/4 v6, 0x0

    #@4
    .local v6, mnc_digit_3:I
    move v5, v6

    #@5
    .local v5, mnc_digit_2:I
    move v4, v6

    #@6
    .line 221
    .local v4, mnc_digit_1:I
    const/16 v7, 0x3e7

    #@8
    if-gt p2, v7, :cond_1a

    #@a
    if-eqz p2, :cond_1a

    #@c
    if-ltz p3, :cond_1a

    #@e
    if-eqz p1, :cond_14

    #@10
    const/16 v7, 0x3e7

    #@12
    if-gt p3, v7, :cond_1a

    #@14
    :cond_14
    if-nez p1, :cond_53

    #@16
    const/16 v7, 0x63

    #@18
    if-le p3, v7, :cond_53

    #@1a
    .line 225
    :cond_1a
    const/4 v7, 0x0

    #@1b
    const/4 v8, -0x1

    #@1c
    aput-byte v8, v0, v7

    #@1e
    .line 226
    const/4 v7, 0x1

    #@1f
    const/4 v8, -0x1

    #@20
    aput-byte v8, v0, v7

    #@22
    .line 227
    const/4 v7, 0x2

    #@23
    const/4 v8, -0x1

    #@24
    aput-byte v8, v0, v7

    #@26
    .line 253
    :goto_26
    const-string v7, "LGEIccUtils"

    #@28
    new-instance v8, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v9, "mnc "

    #@2f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v8

    #@33
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@36
    move-result-object v8

    #@37
    const-string v9, " "

    #@39
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v8

    #@3d
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40
    move-result-object v8

    #@41
    const-string v9, " "

    #@43
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v8

    #@47
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v8

    #@4b
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v8

    #@4f
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@52
    .line 255
    return-object v0

    #@53
    .line 231
    :cond_53
    div-int/lit8 v1, p2, 0x64

    #@55
    .line 232
    .local v1, mcc_digit_1:I
    mul-int/lit8 v7, v1, 0x64

    #@57
    sub-int v7, p2, v7

    #@59
    div-int/lit8 v2, v7, 0xa

    #@5b
    .line 233
    .local v2, mcc_digit_2:I
    mul-int/lit8 v7, v1, 0x64

    #@5d
    sub-int v7, p2, v7

    #@5f
    mul-int/lit8 v8, v2, 0xa

    #@61
    sub-int v3, v7, v8

    #@63
    .line 235
    .local v3, mcc_digit_3:I
    if-eqz p1, :cond_8b

    #@65
    .line 237
    div-int/lit8 v4, p3, 0x64

    #@67
    .line 238
    mul-int/lit8 v7, v4, 0x64

    #@69
    sub-int v7, p3, v7

    #@6b
    div-int/lit8 v5, v7, 0xa

    #@6d
    .line 239
    mul-int/lit8 v7, v4, 0x64

    #@6f
    sub-int v7, p3, v7

    #@71
    mul-int/lit8 v8, v5, 0xa

    #@73
    sub-int v6, v7, v8

    #@75
    .line 248
    :goto_75
    const/4 v7, 0x0

    #@76
    shl-int/lit8 v8, v2, 0x4

    #@78
    add-int/2addr v8, v1

    #@79
    int-to-byte v8, v8

    #@7a
    aput-byte v8, v0, v7

    #@7c
    .line 249
    const/4 v7, 0x1

    #@7d
    shl-int/lit8 v8, v6, 0x4

    #@7f
    add-int/2addr v8, v3

    #@80
    int-to-byte v8, v8

    #@81
    aput-byte v8, v0, v7

    #@83
    .line 250
    const/4 v7, 0x2

    #@84
    shl-int/lit8 v8, v5, 0x4

    #@86
    add-int/2addr v8, v4

    #@87
    int-to-byte v8, v8

    #@88
    aput-byte v8, v0, v7

    #@8a
    goto :goto_26

    #@8b
    .line 243
    :cond_8b
    div-int/lit8 v4, p3, 0xa

    #@8d
    .line 244
    mul-int/lit8 v7, v4, 0xa

    #@8f
    sub-int v5, p3, v7

    #@91
    .line 245
    const/16 v6, 0xf

    #@93
    goto :goto_75
.end method
