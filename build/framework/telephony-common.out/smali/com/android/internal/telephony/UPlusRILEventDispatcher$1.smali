.class Lcom/android/internal/telephony/UPlusRILEventDispatcher$1;
.super Landroid/content/BroadcastReceiver;
.source "UPlusRILEventDispatcher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/UPlusRILEventDispatcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/UPlusRILEventDispatcher;


# direct methods
.method constructor <init>(Lcom/android/internal/telephony/UPlusRILEventDispatcher;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 500
    iput-object p1, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher$1;->this$0:Lcom/android/internal/telephony/UPlusRILEventDispatcher;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 6
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 503
    iget-object v0, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher$1;->this$0:Lcom/android/internal/telephony/UPlusRILEventDispatcher;

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "mBroadcastReceiver : "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@18
    .line 505
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@1b
    move-result-object v0

    #@1c
    const-string v1, "android.intent.action.LGT_ROAMING_UI_TEST_LGT"

    #@1e
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@21
    move-result v0

    #@22
    if-eqz v0, :cond_2a

    #@24
    .line 506
    iget-object v0, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher$1;->this$0:Lcom/android/internal/telephony/UPlusRILEventDispatcher;

    #@26
    const/4 v1, 0x0

    #@27
    invoke-static {v0, v1}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->access$000(Lcom/android/internal/telephony/UPlusRILEventDispatcher;I)V

    #@2a
    .line 508
    :cond_2a
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@2d
    move-result-object v0

    #@2e
    const-string v1, "android.intent.action.LGT_ROAMING_UI_TEST_KT"

    #@30
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@33
    move-result v0

    #@34
    if-eqz v0, :cond_3c

    #@36
    .line 509
    iget-object v0, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher$1;->this$0:Lcom/android/internal/telephony/UPlusRILEventDispatcher;

    #@38
    const/4 v1, 0x1

    #@39
    invoke-static {v0, v1}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->access$000(Lcom/android/internal/telephony/UPlusRILEventDispatcher;I)V

    #@3c
    .line 511
    :cond_3c
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3f
    move-result-object v0

    #@40
    const-string v1, "android.intent.action.LGT_ROAMING_UI_TEST_JCDMA"

    #@42
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@45
    move-result v0

    #@46
    if-eqz v0, :cond_4e

    #@48
    .line 512
    iget-object v0, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher$1;->this$0:Lcom/android/internal/telephony/UPlusRILEventDispatcher;

    #@4a
    const/4 v1, 0x2

    #@4b
    invoke-static {v0, v1}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->access$000(Lcom/android/internal/telephony/UPlusRILEventDispatcher;I)V

    #@4e
    .line 514
    :cond_4e
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@51
    move-result-object v0

    #@52
    const-string v1, "android.intent.action.LGT_ROAMING_UI_TEST_DCN"

    #@54
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@57
    move-result v0

    #@58
    if-eqz v0, :cond_60

    #@5a
    .line 515
    iget-object v0, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher$1;->this$0:Lcom/android/internal/telephony/UPlusRILEventDispatcher;

    #@5c
    const/4 v1, 0x3

    #@5d
    invoke-static {v0, v1}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->access$000(Lcom/android/internal/telephony/UPlusRILEventDispatcher;I)V

    #@60
    .line 517
    :cond_60
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@63
    move-result-object v0

    #@64
    const-string v1, "android.intent.action.LG_NVITEM_RESET"

    #@66
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@69
    move-result v0

    #@6a
    if-eqz v0, :cond_71

    #@6c
    .line 518
    iget-object v0, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher$1;->this$0:Lcom/android/internal/telephony/UPlusRILEventDispatcher;

    #@6e
    invoke-static {v0}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->access$100(Lcom/android/internal/telephony/UPlusRILEventDispatcher;)V

    #@71
    .line 520
    :cond_71
    return-void
.end method
