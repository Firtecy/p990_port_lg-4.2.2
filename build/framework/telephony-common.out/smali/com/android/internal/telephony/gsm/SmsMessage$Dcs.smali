.class public Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;
.super Ljava/lang/Object;
.source "SmsMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/gsm/SmsMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Dcs"
.end annotation


# instance fields
.field private mAutomaticDeletion:Z

.field private mDataCodingScheme:I

.field private mEncodingType:I

.field private mIsMwi:Z

.field private mMessageClass:Lcom/android/internal/telephony/SmsConstants$MessageClass;

.field protected mMwiDontStore:Z

.field private mMwiSense:Z

.field private mUserDataCompressed:Z


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .registers 5
    .parameter "dataCodingScheme"
    .parameter "msgBody"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 241
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 210
    iput v1, p0, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->mDataCodingScheme:I

    #@6
    .line 211
    iput-boolean v1, p0, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->mUserDataCompressed:Z

    #@8
    .line 212
    iput-boolean v1, p0, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->mAutomaticDeletion:Z

    #@a
    .line 213
    sget-object v0, Lcom/android/internal/telephony/SmsConstants$MessageClass;->UNKNOWN:Lcom/android/internal/telephony/SmsConstants$MessageClass;

    #@c
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->mMessageClass:Lcom/android/internal/telephony/SmsConstants$MessageClass;

    #@e
    .line 214
    iput v1, p0, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->mEncodingType:I

    #@10
    .line 215
    iput-boolean v1, p0, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->mIsMwi:Z

    #@12
    .line 216
    iput-boolean v1, p0, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->mMwiSense:Z

    #@14
    .line 217
    iput-boolean v1, p0, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->mMwiDontStore:Z

    #@16
    .line 242
    const/4 v0, -0x1

    #@17
    if-ne p1, v0, :cond_24

    #@19
    .line 243
    invoke-static {p2}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->generateDcs(Ljava/lang/String;)I

    #@1c
    move-result v0

    #@1d
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->setDataCodingScheme(I)V

    #@20
    .line 247
    :goto_20
    invoke-direct {p0, p2}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->parseDcs(Ljava/lang/String;)V

    #@23
    .line 248
    return-void

    #@24
    .line 245
    :cond_24
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->setDataCodingScheme(I)V

    #@27
    goto :goto_20
.end method

.method public static generateDcs(Ljava/lang/String;)I
    .registers 8
    .parameter "messageBody"

    #@0
    .prologue
    const/16 v5, 0x8

    #@2
    const/4 v4, 0x0

    #@3
    .line 460
    if-nez p0, :cond_6

    #@5
    .line 488
    :cond_5
    :goto_5
    return v4

    #@6
    .line 465
    :cond_6
    :try_start_6
    invoke-static {p0}, Lcom/android/internal/telephony/GsmAlphabet;->stringToGsm7BitPacked(Ljava/lang/String;)[B

    #@9
    .line 467
    invoke-static {}, Lcom/android/internal/telephony/gsm/SmsMessage;->isCountCharIndexInsteadOfSeptets()Z

    #@c
    move-result v6

    #@d
    if-eqz v6, :cond_5

    #@f
    .line 468
    const/4 v6, 0x0

    #@10
    invoke-static {p0, v6}, Lcom/android/internal/telephony/GsmAlphabet;->countGsmSeptets(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@13
    move-result-object v2

    #@14
    .line 469
    .local v2, ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    if-nez v2, :cond_2c

    #@16
    .line 470
    new-instance v4, Lcom/android/internal/telephony/EncodeException;

    #@18
    invoke-direct {v4, p0}, Lcom/android/internal/telephony/EncodeException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v4
    :try_end_1c
    .catch Lcom/android/internal/telephony/EncodeException; {:try_start_6 .. :try_end_1c} :catch_1c

    #@1c
    .line 478
    .end local v2           #ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    :catch_1c
    move-exception v0

    #@1d
    .line 479
    .local v0, ex:Lcom/android/internal/telephony/EncodeException;
    invoke-static {}, Lcom/android/internal/telephony/gsm/SmsMessage;->isKSC5601Encoding()Z

    #@20
    move-result v4

    #@21
    const/4 v6, 0x1

    #@22
    if-ne v4, v6, :cond_3b

    #@24
    .line 481
    :try_start_24
    const-string v4, "euc-kr"

    #@26
    invoke-virtual {p0, v4}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_29
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_24 .. :try_end_29} :catch_38

    #@29
    .line 486
    const/16 v4, 0x84

    #@2b
    goto :goto_5

    #@2c
    .line 472
    .end local v0           #ex:Lcom/android/internal/telephony/EncodeException;
    .restart local v2       #ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    :cond_2c
    :try_start_2c
    iget v1, v2, Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;->codeUnitCount:I

    #@2e
    .line 473
    .local v1, septets:I
    sget v6, Landroid/telephony/SmsMessage;->LIMIT_USER_DATA_SEPTETS:I

    #@30
    if-le v1, v6, :cond_5

    #@32
    .line 474
    new-instance v4, Lcom/android/internal/telephony/EncodeException;

    #@34
    invoke-direct {v4, p0}, Lcom/android/internal/telephony/EncodeException;-><init>(Ljava/lang/String;)V

    #@37
    throw v4
    :try_end_38
    .catch Lcom/android/internal/telephony/EncodeException; {:try_start_2c .. :try_end_38} :catch_1c

    #@38
    .line 482
    .end local v1           #septets:I
    .end local v2           #ted:Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    .restart local v0       #ex:Lcom/android/internal/telephony/EncodeException;
    :catch_38
    move-exception v3

    #@39
    .local v3, uex:Ljava/io/UnsupportedEncodingException;
    move v4, v5

    #@3a
    .line 484
    goto :goto_5

    #@3b
    .end local v3           #uex:Ljava/io/UnsupportedEncodingException;
    :cond_3b
    move v4, v5

    #@3c
    .line 488
    goto :goto_5
.end method

.method public static getGeneralPublicDataCodingScheme(I)I
    .registers 2
    .parameter "encodingType"

    #@0
    .prologue
    .line 494
    packed-switch p0, :pswitch_data_e

    #@3
    .line 502
    const/4 v0, -0x1

    #@4
    :goto_4
    return v0

    #@5
    .line 496
    :pswitch_5
    const/4 v0, 0x0

    #@6
    goto :goto_4

    #@7
    .line 498
    :pswitch_7
    const/16 v0, 0x84

    #@9
    goto :goto_4

    #@a
    .line 500
    :pswitch_a
    const/16 v0, 0x8

    #@c
    goto :goto_4

    #@d
    .line 494
    nop

    #@e
    :pswitch_data_e
    .packed-switch 0x1
        :pswitch_5
        :pswitch_7
        :pswitch_a
    .end packed-switch
.end method

.method private parseDcs(Ljava/lang/String;)V
    .registers 11
    .parameter "msgBody"

    #@0
    .prologue
    const/16 v8, 0xe0

    #@2
    const/4 v7, 0x3

    #@3
    const/4 v6, 0x2

    #@4
    const/4 v3, 0x1

    #@5
    const/4 v4, 0x0

    #@6
    .line 251
    const/4 v1, 0x0

    #@7
    .line 258
    .local v1, hasMessageClass:Z
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->setEncodingType(I)V

    #@a
    .line 259
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->getDataCodingScheme()I

    #@d
    move-result v2

    #@e
    and-int/lit16 v2, v2, 0x80

    #@10
    if-nez v2, :cond_7f

    #@12
    .line 261
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->getDataCodingScheme()I

    #@15
    move-result v2

    #@16
    and-int/lit8 v2, v2, 0x40

    #@18
    if-eqz v2, :cond_5d

    #@1a
    move v2, v3

    #@1b
    :goto_1b
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->setAutomaticDeletion(Z)V

    #@1e
    .line 262
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->getDataCodingScheme()I

    #@21
    move-result v2

    #@22
    and-int/lit8 v2, v2, 0x20

    #@24
    if-eqz v2, :cond_5f

    #@26
    move v2, v3

    #@27
    :goto_27
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->setUserDataCompressed(Z)V

    #@2a
    .line 263
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->getDataCodingScheme()I

    #@2d
    move-result v2

    #@2e
    and-int/lit8 v2, v2, 0x10

    #@30
    if-eqz v2, :cond_61

    #@32
    move v1, v3

    #@33
    .line 265
    :goto_33
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->isUserDataCompressed()Z

    #@36
    move-result v2

    #@37
    if-eqz v2, :cond_63

    #@39
    .line 266
    new-instance v2, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string v3, "parseDcs(), 4 - Unsupported SMS data coding scheme (compression) "

    #@40
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v2

    #@44
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->getDataCodingScheme()I

    #@47
    move-result v3

    #@48
    and-int/lit16 v3, v3, 0xff

    #@4a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v2

    #@4e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@51
    move-result-object v2

    #@52
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->w(Ljava/lang/String;)I

    #@55
    .line 340
    :cond_55
    :goto_55
    if-nez v1, :cond_117

    #@57
    .line 341
    sget-object v2, Lcom/android/internal/telephony/SmsConstants$MessageClass;->UNKNOWN:Lcom/android/internal/telephony/SmsConstants$MessageClass;

    #@59
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->setMessageClass(Lcom/android/internal/telephony/SmsConstants$MessageClass;)V

    #@5c
    .line 358
    :goto_5c
    return-void

    #@5d
    :cond_5d
    move v2, v4

    #@5e
    .line 261
    goto :goto_1b

    #@5f
    :cond_5f
    move v2, v4

    #@60
    .line 262
    goto :goto_27

    #@61
    :cond_61
    move v1, v4

    #@62
    .line 263
    goto :goto_33

    #@63
    .line 269
    :cond_63
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->getDataCodingScheme()I

    #@66
    move-result v2

    #@67
    shr-int/lit8 v2, v2, 0x2

    #@69
    and-int/lit8 v2, v2, 0x3

    #@6b
    packed-switch v2, :pswitch_data_13e

    #@6e
    goto :goto_55

    #@6f
    .line 271
    :pswitch_6f
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->setEncodingType(I)V

    #@72
    goto :goto_55

    #@73
    .line 275
    :pswitch_73
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->setEncodingType(I)V

    #@76
    goto :goto_55

    #@77
    .line 279
    :pswitch_77
    invoke-virtual {p0, v6}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->setEncodingType(I)V

    #@7a
    goto :goto_55

    #@7b
    .line 282
    :pswitch_7b
    invoke-virtual {p0, v6}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->setEncodingType(I)V

    #@7e
    goto :goto_55

    #@7f
    .line 286
    :cond_7f
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->getDataCodingScheme()I

    #@82
    move-result v2

    #@83
    and-int/lit16 v2, v2, 0xf0

    #@85
    const/16 v5, 0xf0

    #@87
    if-ne v2, v5, :cond_a0

    #@89
    .line 287
    invoke-direct {p0, v4}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->setAutomaticDeletion(Z)V

    #@8c
    .line 288
    const/4 v1, 0x1

    #@8d
    .line 289
    invoke-direct {p0, v4}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->setUserDataCompressed(Z)V

    #@90
    .line 291
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->getDataCodingScheme()I

    #@93
    move-result v2

    #@94
    and-int/lit8 v2, v2, 0x4

    #@96
    if-nez v2, :cond_9c

    #@98
    .line 293
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->setEncodingType(I)V

    #@9b
    goto :goto_55

    #@9c
    .line 296
    :cond_9c
    invoke-virtual {p0, v6}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->setEncodingType(I)V

    #@9f
    goto :goto_55

    #@a0
    .line 298
    :cond_a0
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->getDataCodingScheme()I

    #@a3
    move-result v2

    #@a4
    and-int/lit16 v2, v2, 0xf0

    #@a6
    const/16 v5, 0xc0

    #@a8
    if-eq v2, v5, :cond_bc

    #@aa
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->getDataCodingScheme()I

    #@ad
    move-result v2

    #@ae
    and-int/lit16 v2, v2, 0xf0

    #@b0
    const/16 v5, 0xd0

    #@b2
    if-eq v2, v5, :cond_bc

    #@b4
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->getDataCodingScheme()I

    #@b7
    move-result v2

    #@b8
    and-int/lit16 v2, v2, 0xf0

    #@ba
    if-ne v2, v8, :cond_cf

    #@bc
    .line 307
    :cond_bc
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->getDataCodingScheme()I

    #@bf
    move-result v2

    #@c0
    and-int/lit16 v2, v2, 0xf0

    #@c2
    if-ne v2, v8, :cond_cb

    #@c4
    .line 308
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->setEncodingType(I)V

    #@c7
    .line 312
    :goto_c7
    invoke-direct {p0, v4}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->setMwi(Z)V

    #@ca
    goto :goto_55

    #@cb
    .line 310
    :cond_cb
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->setEncodingType(I)V

    #@ce
    goto :goto_c7

    #@cf
    .line 317
    :cond_cf
    invoke-direct {p0, v4}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->setAutomaticDeletion(Z)V

    #@d2
    .line 318
    const/4 v1, 0x0

    #@d3
    .line 319
    invoke-direct {p0, v4}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->setUserDataCompressed(Z)V

    #@d6
    .line 320
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->getDataCodingScheme()I

    #@d9
    move-result v2

    #@da
    and-int/lit16 v2, v2, 0xff

    #@dc
    const/16 v4, 0xc

    #@de
    if-ne v2, v4, :cond_e5

    #@e0
    .line 321
    invoke-virtual {p0, v6}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->setEncodingType(I)V

    #@e3
    goto/16 :goto_55

    #@e5
    .line 323
    :cond_e5
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->getDataCodingScheme()I

    #@e8
    move-result v2

    #@e9
    and-int/lit16 v0, v2, 0xf0

    #@eb
    .line 324
    .local v0, dataCodingScheme_byte:I
    const/16 v2, 0x80

    #@ed
    if-eq v0, v2, :cond_f3

    #@ef
    const/16 v2, 0x90

    #@f1
    if-ne v0, v2, :cond_10a

    #@f3
    .line 325
    :cond_f3
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->getDataCodingScheme()I

    #@f6
    move-result v2

    #@f7
    shr-int/lit8 v2, v2, 0x2

    #@f9
    and-int/lit8 v2, v2, 0x3

    #@fb
    packed-switch v2, :pswitch_data_14a

    #@fe
    goto/16 :goto_55

    #@100
    .line 329
    :pswitch_100
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->setEncodingType(I)V

    #@103
    goto/16 :goto_55

    #@105
    .line 332
    :pswitch_105
    invoke-virtual {p0, v6}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->setEncodingType(I)V

    #@108
    goto/16 :goto_55

    #@10a
    .line 335
    :cond_10a
    const/16 v2, 0xa0

    #@10c
    if-eq v0, v2, :cond_112

    #@10e
    const/16 v2, 0xb0

    #@110
    if-ne v0, v2, :cond_55

    #@112
    .line 336
    :cond_112
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->setEncodingType(I)V

    #@115
    goto/16 :goto_55

    #@117
    .line 343
    .end local v0           #dataCodingScheme_byte:I
    :cond_117
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->getDataCodingScheme()I

    #@11a
    move-result v2

    #@11b
    and-int/lit8 v2, v2, 0x3

    #@11d
    packed-switch v2, :pswitch_data_156

    #@120
    goto/16 :goto_5c

    #@122
    .line 345
    :pswitch_122
    sget-object v2, Lcom/android/internal/telephony/SmsConstants$MessageClass;->CLASS_0:Lcom/android/internal/telephony/SmsConstants$MessageClass;

    #@124
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->setMessageClass(Lcom/android/internal/telephony/SmsConstants$MessageClass;)V

    #@127
    goto/16 :goto_5c

    #@129
    .line 348
    :pswitch_129
    sget-object v2, Lcom/android/internal/telephony/SmsConstants$MessageClass;->CLASS_1:Lcom/android/internal/telephony/SmsConstants$MessageClass;

    #@12b
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->setMessageClass(Lcom/android/internal/telephony/SmsConstants$MessageClass;)V

    #@12e
    goto/16 :goto_5c

    #@130
    .line 351
    :pswitch_130
    sget-object v2, Lcom/android/internal/telephony/SmsConstants$MessageClass;->CLASS_2:Lcom/android/internal/telephony/SmsConstants$MessageClass;

    #@132
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->setMessageClass(Lcom/android/internal/telephony/SmsConstants$MessageClass;)V

    #@135
    goto/16 :goto_5c

    #@137
    .line 354
    :pswitch_137
    sget-object v2, Lcom/android/internal/telephony/SmsConstants$MessageClass;->CLASS_3:Lcom/android/internal/telephony/SmsConstants$MessageClass;

    #@139
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->setMessageClass(Lcom/android/internal/telephony/SmsConstants$MessageClass;)V

    #@13c
    goto/16 :goto_5c

    #@13e
    .line 269
    :pswitch_data_13e
    .packed-switch 0x0
        :pswitch_6f
        :pswitch_77
        :pswitch_73
        :pswitch_7b
    .end packed-switch

    #@14a
    .line 325
    :pswitch_data_14a
    .packed-switch 0x0
        :pswitch_100
        :pswitch_105
        :pswitch_100
        :pswitch_100
    .end packed-switch

    #@156
    .line 343
    :pswitch_data_156
    .packed-switch 0x0
        :pswitch_122
        :pswitch_129
        :pswitch_130
        :pswitch_137
    .end packed-switch
.end method

.method private setAutomaticDeletion(Z)V
    .registers 2
    .parameter "automaticDeletion"

    #@0
    .prologue
    .line 392
    iput-boolean p1, p0, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->mAutomaticDeletion:Z

    #@2
    .line 393
    return-void
.end method

.method private setDataCodingScheme(I)V
    .registers 2
    .parameter "dataCodingScheme"

    #@0
    .prologue
    .line 364
    iput p1, p0, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->mDataCodingScheme:I

    #@2
    .line 365
    return-void
.end method

.method private setMessageClass(Lcom/android/internal/telephony/SmsConstants$MessageClass;)V
    .registers 2
    .parameter "messageClass"

    #@0
    .prologue
    .line 406
    iput-object p1, p0, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->mMessageClass:Lcom/android/internal/telephony/SmsConstants$MessageClass;

    #@2
    .line 407
    return-void
.end method

.method private setMwi(Z)V
    .registers 2
    .parameter "isMwi"

    #@0
    .prologue
    .line 434
    iput-boolean p1, p0, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->mIsMwi:Z

    #@2
    .line 435
    return-void
.end method

.method private setMwiSense(Z)V
    .registers 2
    .parameter "mwiSense"

    #@0
    .prologue
    .line 448
    iput-boolean p1, p0, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->mMwiSense:Z

    #@2
    .line 449
    return-void
.end method

.method private setUserDataCompressed(Z)V
    .registers 2
    .parameter "userDataCompressed"

    #@0
    .prologue
    .line 378
    iput-boolean p1, p0, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->mUserDataCompressed:Z

    #@2
    .line 379
    return-void
.end method


# virtual methods
.method public getDataCodingScheme()I
    .registers 2

    #@0
    .prologue
    .line 371
    iget v0, p0, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->mDataCodingScheme:I

    #@2
    return v0
.end method

.method public getEncodingType()I
    .registers 2

    #@0
    .prologue
    .line 427
    iget v0, p0, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->mEncodingType:I

    #@2
    return v0
.end method

.method public getMessageClass()Lcom/android/internal/telephony/SmsConstants$MessageClass;
    .registers 2

    #@0
    .prologue
    .line 413
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->mMessageClass:Lcom/android/internal/telephony/SmsConstants$MessageClass;

    #@2
    return-object v0
.end method

.method public isAutomaticDeletion()Z
    .registers 2

    #@0
    .prologue
    .line 399
    iget-boolean v0, p0, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->mAutomaticDeletion:Z

    #@2
    return v0
.end method

.method public isMwi()Z
    .registers 2

    #@0
    .prologue
    .line 441
    iget-boolean v0, p0, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->mIsMwi:Z

    #@2
    return v0
.end method

.method public isMwiSense()Z
    .registers 2

    #@0
    .prologue
    .line 455
    iget-boolean v0, p0, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->mMwiSense:Z

    #@2
    return v0
.end method

.method public isUserDataCompressed()Z
    .registers 2

    #@0
    .prologue
    .line 385
    iget-boolean v0, p0, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->mUserDataCompressed:Z

    #@2
    return v0
.end method

.method public setEncodingType(I)V
    .registers 2
    .parameter "encodingType"

    #@0
    .prologue
    .line 420
    iput p1, p0, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->mEncodingType:I

    #@2
    .line 421
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 220
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "DCS: dataCodingScheme = "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->getDataCodingScheme()I

    #@e
    move-result v1

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    const-string v1, ", this.userDataCompressed = "

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->isUserDataCompressed()Z

    #@1c
    move-result v1

    #@1d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@20
    move-result-object v0

    #@21
    const-string v1, ", this.automaticDeletion = "

    #@23
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v0

    #@27
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->isAutomaticDeletion()Z

    #@2a
    move-result v1

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    const-string v1, ", this.messageClass = "

    #@31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v0

    #@35
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->getMessageClass()Lcom/android/internal/telephony/SmsConstants$MessageClass;

    #@38
    move-result-object v1

    #@39
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v0

    #@3d
    const-string v1, ", encodingType = "

    #@3f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v0

    #@43
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->getEncodingType()I

    #@46
    move-result v1

    #@47
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v0

    #@4b
    const-string v1, ", isMwi = "

    #@4d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v0

    #@51
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->isMwi()Z

    #@54
    move-result v1

    #@55
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@58
    move-result-object v0

    #@59
    const-string v1, ", mwiSense = "

    #@5b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v0

    #@5f
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->isMwiSense()Z

    #@62
    move-result v1

    #@63
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@66
    move-result-object v0

    #@67
    const-string v1, ", mwiDontStore = "

    #@69
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v0

    #@6d
    iget-boolean v1, p0, Lcom/android/internal/telephony/gsm/SmsMessage$Dcs;->mMwiDontStore:Z

    #@6f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@72
    move-result-object v0

    #@73
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@76
    move-result-object v0

    #@77
    return-object v0
.end method
