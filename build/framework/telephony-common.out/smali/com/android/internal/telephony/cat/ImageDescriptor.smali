.class public Lcom/android/internal/telephony/cat/ImageDescriptor;
.super Ljava/lang/Object;
.source "ImageDescriptor.java"


# static fields
.field static final CODING_SCHEME_BASIC:I = 0x11

.field static final CODING_SCHEME_COLOUR:I = 0x21


# instance fields
.field codingScheme:I

.field height:I

.field highOffset:I

.field imageId:I

.field length:I

.field lowOffset:I

.field width:I


# direct methods
.method constructor <init>()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 39
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 40
    iput v0, p0, Lcom/android/internal/telephony/cat/ImageDescriptor;->width:I

    #@6
    .line 41
    iput v0, p0, Lcom/android/internal/telephony/cat/ImageDescriptor;->height:I

    #@8
    .line 42
    iput v0, p0, Lcom/android/internal/telephony/cat/ImageDescriptor;->codingScheme:I

    #@a
    .line 43
    iput v0, p0, Lcom/android/internal/telephony/cat/ImageDescriptor;->imageId:I

    #@c
    .line 44
    iput v0, p0, Lcom/android/internal/telephony/cat/ImageDescriptor;->highOffset:I

    #@e
    .line 45
    iput v0, p0, Lcom/android/internal/telephony/cat/ImageDescriptor;->lowOffset:I

    #@10
    .line 46
    iput v0, p0, Lcom/android/internal/telephony/cat/ImageDescriptor;->length:I

    #@12
    .line 47
    return-void
.end method

.method static parse([BI)Lcom/android/internal/telephony/cat/ImageDescriptor;
    .registers 8
    .parameter "rawData"
    .parameter "valueIndex"

    #@0
    .prologue
    .line 57
    new-instance v0, Lcom/android/internal/telephony/cat/ImageDescriptor;

    #@2
    invoke-direct {v0}, Lcom/android/internal/telephony/cat/ImageDescriptor;-><init>()V

    #@5
    .line 59
    .local v0, d:Lcom/android/internal/telephony/cat/ImageDescriptor;
    add-int/lit8 v2, p1, 0x1

    #@7
    .end local p1
    .local v2, valueIndex:I
    :try_start_7
    aget-byte v3, p0, p1

    #@9
    and-int/lit16 v3, v3, 0xff

    #@b
    iput v3, v0, Lcom/android/internal/telephony/cat/ImageDescriptor;->width:I
    :try_end_d
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_7 .. :try_end_d} :catch_bb

    #@d
    .line 60
    add-int/lit8 p1, v2, 0x1

    #@f
    .end local v2           #valueIndex:I
    .restart local p1
    :try_start_f
    aget-byte v3, p0, v2

    #@11
    and-int/lit16 v3, v3, 0xff

    #@13
    iput v3, v0, Lcom/android/internal/telephony/cat/ImageDescriptor;->height:I
    :try_end_15
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_f .. :try_end_15} :catch_c6

    #@15
    .line 61
    add-int/lit8 v2, p1, 0x1

    #@17
    .end local p1
    .restart local v2       #valueIndex:I
    :try_start_17
    aget-byte v3, p0, p1

    #@19
    and-int/lit16 v3, v3, 0xff

    #@1b
    iput v3, v0, Lcom/android/internal/telephony/cat/ImageDescriptor;->codingScheme:I
    :try_end_1d
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_17 .. :try_end_1d} :catch_bb

    #@1d
    .line 64
    add-int/lit8 p1, v2, 0x1

    #@1f
    .end local v2           #valueIndex:I
    .restart local p1
    :try_start_1f
    aget-byte v3, p0, v2

    #@21
    and-int/lit16 v3, v3, 0xff

    #@23
    shl-int/lit8 v3, v3, 0x8

    #@25
    iput v3, v0, Lcom/android/internal/telephony/cat/ImageDescriptor;->imageId:I

    #@27
    .line 65
    iget v3, v0, Lcom/android/internal/telephony/cat/ImageDescriptor;->imageId:I
    :try_end_29
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1f .. :try_end_29} :catch_c6

    #@29
    add-int/lit8 v2, p1, 0x1

    #@2b
    .end local p1
    .restart local v2       #valueIndex:I
    :try_start_2b
    aget-byte v4, p0, p1

    #@2d
    and-int/lit16 v4, v4, 0xff

    #@2f
    or-int/2addr v3, v4

    #@30
    iput v3, v0, Lcom/android/internal/telephony/cat/ImageDescriptor;->imageId:I
    :try_end_32
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_2b .. :try_end_32} :catch_bb

    #@32
    .line 67
    add-int/lit8 p1, v2, 0x1

    #@34
    .end local v2           #valueIndex:I
    .restart local p1
    :try_start_34
    aget-byte v3, p0, v2

    #@36
    and-int/lit16 v3, v3, 0xff

    #@38
    iput v3, v0, Lcom/android/internal/telephony/cat/ImageDescriptor;->highOffset:I
    :try_end_3a
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_34 .. :try_end_3a} :catch_c6

    #@3a
    .line 68
    add-int/lit8 v2, p1, 0x1

    #@3c
    .end local p1
    .restart local v2       #valueIndex:I
    :try_start_3c
    aget-byte v3, p0, p1

    #@3e
    and-int/lit16 v3, v3, 0xff

    #@40
    iput v3, v0, Lcom/android/internal/telephony/cat/ImageDescriptor;->lowOffset:I
    :try_end_42
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_3c .. :try_end_42} :catch_bb

    #@42
    .line 70
    add-int/lit8 p1, v2, 0x1

    #@44
    .end local v2           #valueIndex:I
    .restart local p1
    :try_start_44
    aget-byte v3, p0, v2
    :try_end_46
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_44 .. :try_end_46} :catch_c6

    #@46
    and-int/lit16 v3, v3, 0xff

    #@48
    shl-int/lit8 v3, v3, 0x8

    #@4a
    add-int/lit8 v2, p1, 0x1

    #@4c
    .end local p1
    .restart local v2       #valueIndex:I
    :try_start_4c
    aget-byte v4, p0, p1

    #@4e
    and-int/lit16 v4, v4, 0xff

    #@50
    or-int/2addr v3, v4

    #@51
    iput v3, v0, Lcom/android/internal/telephony/cat/ImageDescriptor;->length:I

    #@53
    .line 72
    const-string v3, "ImageDescripter"

    #@55
    new-instance v4, Ljava/lang/StringBuilder;

    #@57
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@5a
    const-string v5, "parse; Descriptor : "

    #@5c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v4

    #@60
    iget v5, v0, Lcom/android/internal/telephony/cat/ImageDescriptor;->width:I

    #@62
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@65
    move-result-object v4

    #@66
    const-string v5, ", "

    #@68
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v4

    #@6c
    iget v5, v0, Lcom/android/internal/telephony/cat/ImageDescriptor;->height:I

    #@6e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@71
    move-result-object v4

    #@72
    const-string v5, ", "

    #@74
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v4

    #@78
    iget v5, v0, Lcom/android/internal/telephony/cat/ImageDescriptor;->codingScheme:I

    #@7a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v4

    #@7e
    const-string v5, ", 0x"

    #@80
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v4

    #@84
    iget v5, v0, Lcom/android/internal/telephony/cat/ImageDescriptor;->imageId:I

    #@86
    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@89
    move-result-object v5

    #@8a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8d
    move-result-object v4

    #@8e
    const-string v5, ", "

    #@90
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@93
    move-result-object v4

    #@94
    iget v5, v0, Lcom/android/internal/telephony/cat/ImageDescriptor;->highOffset:I

    #@96
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@99
    move-result-object v4

    #@9a
    const-string v5, ", "

    #@9c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v4

    #@a0
    iget v5, v0, Lcom/android/internal/telephony/cat/ImageDescriptor;->lowOffset:I

    #@a2
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v4

    #@a6
    const-string v5, ", "

    #@a8
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v4

    #@ac
    iget v5, v0, Lcom/android/internal/telephony/cat/ImageDescriptor;->length:I

    #@ae
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v4

    #@b2
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b5
    move-result-object v4

    #@b6
    invoke-static {v3, v4}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_b9
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_4c .. :try_end_b9} :catch_bb

    #@b9
    move p1, v2

    #@ba
    .line 80
    .end local v2           #valueIndex:I
    .restart local p1
    :goto_ba
    return-object v0

    #@bb
    .line 76
    .end local p1
    .restart local v2       #valueIndex:I
    :catch_bb
    move-exception v1

    #@bc
    move p1, v2

    #@bd
    .line 77
    .end local v2           #valueIndex:I
    .local v1, e:Ljava/lang/IndexOutOfBoundsException;
    .restart local p1
    :goto_bd
    const-string v3, "ImageDescripter"

    #@bf
    const-string v4, "parse; failed parsing image descriptor"

    #@c1
    invoke-static {v3, v4}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@c4
    .line 78
    const/4 v0, 0x0

    #@c5
    goto :goto_ba

    #@c6
    .line 76
    .end local v1           #e:Ljava/lang/IndexOutOfBoundsException;
    :catch_c6
    move-exception v1

    #@c7
    goto :goto_bd
.end method
