.class public Lcom/android/internal/telephony/uicc/UiccCardApplication;
.super Ljava/lang/Object;
.source "UiccCardApplication.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/uicc/UiccCardApplication$2;
    }
.end annotation


# static fields
.field private static final DBG:Z = true

.field private static ENABLE_PRIVACY_LOG:Z = false

.field private static final EVENT_CHANGE_FACILITY_FDN_DONE:I = 0x5

.field private static final EVENT_CHANGE_FACILITY_LOCK_DONE:I = 0x7

.field private static final EVENT_CHANGE_PIN1_DONE:I = 0x2

.field private static final EVENT_CHANGE_PIN2_DONE:I = 0x3

.field private static final EVENT_INTERNAL_PIN_VERIFY:I = 0x9

.field private static final EVENT_PIN1_PUK1_DONE:I = 0x1

.field private static final EVENT_PIN2_PUK2_DONE:I = 0x8

.field private static final EVENT_QUERY_FACILITY_FDN_DONE:I = 0x4

.field private static final EVENT_QUERY_FACILITY_LOCK_DONE:I = 0x6

.field private static final LOG_TAG:Ljava/lang/String; = "RIL_UiccCardApplication"


# instance fields
.field private hidden_reset_flag:Z

.field private internalPin:Ljava/lang/String;

.field private isPinReq:Z

.field private isPuk1:Z

.field private isPuk2:Z

.field private mAid:Ljava/lang/String;

.field private mAppLabel:Ljava/lang/String;

.field private mAppState:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;

.field private mAppType:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;

.field private mCi:Lcom/android/internal/telephony/CommandsInterface;

.field private mContext:Landroid/content/Context;

.field private mDesiredFdnEnabled:Z

.field private mDesiredPinLocked:Z

.field private mDestroyed:Z

.field private mHandler:Landroid/os/Handler;

.field private mIccFdnAvailable:Z

.field private mIccFdnEnabled:Z

.field private mIccFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

.field private mIccLockEnabled:Z

.field private mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

.field private final mLock:Ljava/lang/Object;

.field private mPersoLockedRegistrants:Landroid/os/RegistrantList;

.field private mPersoSubState:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;

.field private mPin1InitRetryCount:I

.field private mPin1Replaced:Z

.field private mPin1RetryCount:I

.field private mPin1State:Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;

.field private mPin2InitRetryCount:I

.field private mPin2RetryCount:I

.field private mPin2State:Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;

.field private mPinLockedRegistrants:Landroid/os/RegistrantList;

.field private mPuk1RetryCount:I

.field private mPuk2RetryCount:I

.field private mReadyRegistrants:Landroid/os/RegistrantList;

.field private mUiccCard:Lcom/android/internal/telephony/uicc/UiccCard;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 52
    const-string v1, "persist.service.privacy.enable"

    #@2
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    const-string v2, "ATT"

    #@8
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v0

    #@c
    if-nez v0, :cond_1a

    #@e
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@11
    move-result-object v0

    #@12
    const-string v2, "TMO"

    #@14
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@17
    move-result v0

    #@18
    if-eqz v0, :cond_2e

    #@1a
    :cond_1a
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getCountry()Ljava/lang/String;

    #@1d
    move-result-object v0

    #@1e
    const-string v2, "US"

    #@20
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23
    move-result v0

    #@24
    if-eqz v0, :cond_2e

    #@26
    const/4 v0, 0x0

    #@27
    :goto_27
    invoke-static {v1, v0}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@2a
    move-result v0

    #@2b
    sput-boolean v0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->ENABLE_PRIVACY_LOG:Z

    #@2d
    return-void

    #@2e
    :cond_2e
    const/4 v0, 0x1

    #@2f
    goto :goto_27
.end method

.method constructor <init>(Lcom/android/internal/telephony/uicc/UiccCard;Lcom/android/internal/telephony/uicc/IccCardApplicationStatus;Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;)V
    .registers 9
    .parameter "uiccCard"
    .parameter "as"
    .parameter "c"
    .parameter "ci"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    const/4 v3, -0x1

    #@3
    .line 113
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 62
    iput v3, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPin1RetryCount:I

    #@8
    .line 63
    iput v3, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPin2RetryCount:I

    #@a
    .line 71
    new-instance v2, Ljava/lang/Object;

    #@c
    invoke-direct/range {v2 .. v2}, Ljava/lang/Object;-><init>()V

    #@f
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mLock:Ljava/lang/Object;

    #@11
    .line 83
    iput v3, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPin1InitRetryCount:I

    #@13
    .line 84
    iput v3, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPin2InitRetryCount:I

    #@15
    .line 86
    iput v3, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPuk1RetryCount:I

    #@17
    .line 87
    iput v3, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPuk2RetryCount:I

    #@19
    .line 88
    iput-boolean v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->isPuk1:Z

    #@1b
    .line 89
    iput-boolean v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->isPuk2:Z

    #@1d
    .line 90
    iput-boolean v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->hidden_reset_flag:Z

    #@1f
    .line 91
    const/4 v2, 0x0

    #@20
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->internalPin:Ljava/lang/String;

    #@22
    .line 92
    iput-boolean v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->isPinReq:Z

    #@24
    .line 97
    iput-boolean v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mIccFdnAvailable:Z

    #@26
    .line 106
    new-instance v2, Landroid/os/RegistrantList;

    #@28
    invoke-direct {v2}, Landroid/os/RegistrantList;-><init>()V

    #@2b
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mReadyRegistrants:Landroid/os/RegistrantList;

    #@2d
    .line 107
    new-instance v2, Landroid/os/RegistrantList;

    #@2f
    invoke-direct {v2}, Landroid/os/RegistrantList;-><init>()V

    #@32
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPinLockedRegistrants:Landroid/os/RegistrantList;

    #@34
    .line 108
    new-instance v2, Landroid/os/RegistrantList;

    #@36
    invoke-direct {v2}, Landroid/os/RegistrantList;-><init>()V

    #@39
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPersoLockedRegistrants:Landroid/os/RegistrantList;

    #@3b
    .line 451
    new-instance v2, Lcom/android/internal/telephony/uicc/UiccCardApplication$1;

    #@3d
    invoke-direct {v2, p0}, Lcom/android/internal/telephony/uicc/UiccCardApplication$1;-><init>(Lcom/android/internal/telephony/uicc/UiccCardApplication;)V

    #@40
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mHandler:Landroid/os/Handler;

    #@42
    .line 114
    new-instance v2, Ljava/lang/StringBuilder;

    #@44
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    const-string v3, "Creating UiccApp: "

    #@49
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v2

    #@4d
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v2

    #@51
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v2

    #@55
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->log(Ljava/lang/String;)V

    #@58
    .line 115
    iput-object p1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mUiccCard:Lcom/android/internal/telephony/uicc/UiccCard;

    #@5a
    .line 116
    iget-object v2, p2, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus;->app_state:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;

    #@5c
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mAppState:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;

    #@5e
    .line 117
    iget-object v2, p2, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus;->app_type:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;

    #@60
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mAppType:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;

    #@62
    .line 118
    iget-object v2, p2, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus;->perso_substate:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;

    #@64
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPersoSubState:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;

    #@66
    .line 119
    iget-object v2, p2, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus;->aid:Ljava/lang/String;

    #@68
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mAid:Ljava/lang/String;

    #@6a
    .line 120
    iget-object v2, p2, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus;->app_label:Ljava/lang/String;

    #@6c
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mAppLabel:Ljava/lang/String;

    #@6e
    .line 121
    iget v2, p2, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus;->pin1_replaced:I

    #@70
    if-eqz v2, :cond_bf

    #@72
    :goto_72
    iput-boolean v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPin1Replaced:Z

    #@74
    .line 122
    iget-object v0, p2, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus;->pin1:Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;

    #@76
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPin1State:Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;

    #@78
    .line 123
    iget-object v0, p2, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus;->pin2:Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;

    #@7a
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPin2State:Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;

    #@7c
    .line 125
    iget v0, p2, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus;->remaining_count_pin1:I

    #@7e
    iput v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPin1RetryCount:I

    #@80
    .line 126
    iget v0, p2, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus;->remaining_count_puk1:I

    #@82
    iput v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPuk1RetryCount:I

    #@84
    .line 127
    iget v0, p2, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus;->remaining_count_pin2:I

    #@86
    iput v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPin2RetryCount:I

    #@88
    .line 128
    iget v0, p2, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus;->remaining_count_puk2:I

    #@8a
    iput v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPuk2RetryCount:I

    #@8c
    .line 129
    iget v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPin1RetryCount:I

    #@8e
    iput v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPin1InitRetryCount:I

    #@90
    .line 130
    iget v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPin2RetryCount:I

    #@92
    iput v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPin2InitRetryCount:I

    #@94
    .line 133
    iput-object p3, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mContext:Landroid/content/Context;

    #@96
    .line 134
    iput-object p4, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@98
    .line 136
    iget-object v0, p2, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus;->app_type:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;

    #@9a
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->createIccFileHandler(Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;)Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@9d
    move-result-object v0

    #@9e
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mIccFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@a0
    .line 137
    iget-object v0, p2, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus;->app_type:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;

    #@a2
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mContext:Landroid/content/Context;

    #@a4
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@a6
    invoke-direct {p0, v0, v1, v2}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->createIccRecords(Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;)Lcom/android/internal/telephony/uicc/IccRecords;

    #@a9
    move-result-object v0

    #@aa
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@ac
    .line 138
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mAppState:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;

    #@ae
    sget-object v1, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;->APPSTATE_READY:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;

    #@b0
    if-ne v0, v1, :cond_b8

    #@b2
    .line 139
    invoke-direct {p0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->queryFdn()V

    #@b5
    .line 140
    invoke-direct {p0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->queryPin1State()V

    #@b8
    .line 143
    :cond_b8
    invoke-direct {p0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->isHiddenReset()Z

    #@bb
    move-result v0

    #@bc
    iput-boolean v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->hidden_reset_flag:Z

    #@be
    .line 145
    return-void

    #@bf
    :cond_bf
    move v0, v1

    #@c0
    .line 121
    goto :goto_72
.end method

.method static synthetic access$000(Lcom/android/internal/telephony/uicc/UiccCardApplication;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mDestroyed:Z

    #@2
    return v0
.end method

.method static synthetic access$100(Lcom/android/internal/telephony/uicc/UiccCardApplication;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->loge(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$1000(Lcom/android/internal/telephony/uicc/UiccCardApplication;Landroid/os/AsyncResult;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->onQueryFdnEnabled(Landroid/os/AsyncResult;)V

    #@3
    return-void
.end method

.method static synthetic access$1100(Lcom/android/internal/telephony/uicc/UiccCardApplication;Landroid/os/AsyncResult;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->onChangeFdnDone(Landroid/os/AsyncResult;)V

    #@3
    return-void
.end method

.method static synthetic access$1200(Lcom/android/internal/telephony/uicc/UiccCardApplication;Landroid/os/AsyncResult;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->onQueryFacilityLock(Landroid/os/AsyncResult;)V

    #@3
    return-void
.end method

.method static synthetic access$1300(Lcom/android/internal/telephony/uicc/UiccCardApplication;Landroid/os/AsyncResult;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->onChangeFacilityLock(Landroid/os/AsyncResult;)V

    #@3
    return-void
.end method

.method static synthetic access$200(Lcom/android/internal/telephony/uicc/UiccCardApplication;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 49
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->internalPin:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$202(Lcom/android/internal/telephony/uicc/UiccCardApplication;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 49
    iput-object p1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->internalPin:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$302(Lcom/android/internal/telephony/uicc/UiccCardApplication;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 49
    iput p1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPin1RetryCount:I

    #@2
    return p1
.end method

.method static synthetic access$400(Lcom/android/internal/telephony/uicc/UiccCardApplication;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 49
    iget v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPin1InitRetryCount:I

    #@2
    return v0
.end method

.method static synthetic access$502(Lcom/android/internal/telephony/uicc/UiccCardApplication;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 49
    iput p1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPin2RetryCount:I

    #@2
    return p1
.end method

.method static synthetic access$600(Lcom/android/internal/telephony/uicc/UiccCardApplication;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 49
    iget v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPin2InitRetryCount:I

    #@2
    return v0
.end method

.method static synthetic access$700(Lcom/android/internal/telephony/uicc/UiccCardApplication;Landroid/os/AsyncResult;Z)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->parsePinPukErrorResult(Landroid/os/AsyncResult;Z)V

    #@3
    return-void
.end method

.method static synthetic access$802(Lcom/android/internal/telephony/uicc/UiccCardApplication;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->isPuk1:Z

    #@2
    return p1
.end method

.method static synthetic access$902(Lcom/android/internal/telephony/uicc/UiccCardApplication;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->isPuk2:Z

    #@2
    return p1
.end method

.method private createIccFileHandler(Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;)Lcom/android/internal/telephony/uicc/IccFileHandler;
    .registers 5
    .parameter "type"

    #@0
    .prologue
    .line 236
    sget-object v0, Lcom/android/internal/telephony/uicc/UiccCardApplication$2;->$SwitchMap$com$android$internal$telephony$uicc$IccCardApplicationStatus$AppType:[I

    #@2
    invoke-virtual {p1}, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;->ordinal()I

    #@5
    move-result v1

    #@6
    aget v0, v0, v1

    #@8
    packed-switch v0, :pswitch_data_40

    #@b
    .line 248
    const/4 v0, 0x0

    #@c
    :goto_c
    return-object v0

    #@d
    .line 238
    :pswitch_d
    new-instance v0, Lcom/android/internal/telephony/uicc/SIMFileHandler;

    #@f
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mAid:Ljava/lang/String;

    #@11
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@13
    invoke-direct {v0, p0, v1, v2}, Lcom/android/internal/telephony/uicc/SIMFileHandler;-><init>(Lcom/android/internal/telephony/uicc/UiccCardApplication;Ljava/lang/String;Lcom/android/internal/telephony/CommandsInterface;)V

    #@16
    goto :goto_c

    #@17
    .line 240
    :pswitch_17
    new-instance v0, Lcom/android/internal/telephony/uicc/RuimFileHandler;

    #@19
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mAid:Ljava/lang/String;

    #@1b
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@1d
    invoke-direct {v0, p0, v1, v2}, Lcom/android/internal/telephony/uicc/RuimFileHandler;-><init>(Lcom/android/internal/telephony/uicc/UiccCardApplication;Ljava/lang/String;Lcom/android/internal/telephony/CommandsInterface;)V

    #@20
    goto :goto_c

    #@21
    .line 242
    :pswitch_21
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimFileHandler;

    #@23
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mAid:Ljava/lang/String;

    #@25
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@27
    invoke-direct {v0, p0, v1, v2}, Lcom/android/internal/telephony/uicc/UsimFileHandler;-><init>(Lcom/android/internal/telephony/uicc/UiccCardApplication;Ljava/lang/String;Lcom/android/internal/telephony/CommandsInterface;)V

    #@2a
    goto :goto_c

    #@2b
    .line 244
    :pswitch_2b
    new-instance v0, Lcom/android/internal/telephony/uicc/CsimFileHandler;

    #@2d
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mAid:Ljava/lang/String;

    #@2f
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@31
    invoke-direct {v0, p0, v1, v2}, Lcom/android/internal/telephony/uicc/CsimFileHandler;-><init>(Lcom/android/internal/telephony/uicc/UiccCardApplication;Ljava/lang/String;Lcom/android/internal/telephony/CommandsInterface;)V

    #@34
    goto :goto_c

    #@35
    .line 246
    :pswitch_35
    new-instance v0, Lcom/android/internal/telephony/uicc/IsimFileHandler;

    #@37
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mAid:Ljava/lang/String;

    #@39
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@3b
    invoke-direct {v0, p0, v1, v2}, Lcom/android/internal/telephony/uicc/IsimFileHandler;-><init>(Lcom/android/internal/telephony/uicc/UiccCardApplication;Ljava/lang/String;Lcom/android/internal/telephony/CommandsInterface;)V

    #@3e
    goto :goto_c

    #@3f
    .line 236
    nop

    #@40
    :pswitch_data_40
    .packed-switch 0x1
        :pswitch_d
        :pswitch_17
        :pswitch_21
        :pswitch_2b
        :pswitch_35
    .end packed-switch
.end method

.method private createIccRecords(Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;)Lcom/android/internal/telephony/uicc/IccRecords;
    .registers 5
    .parameter "type"
    .parameter "c"
    .parameter "ci"

    #@0
    .prologue
    .line 223
    sget-object v0, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;->APPTYPE_USIM:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;

    #@2
    if-eq p1, v0, :cond_8

    #@4
    sget-object v0, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;->APPTYPE_SIM:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;

    #@6
    if-ne p1, v0, :cond_e

    #@8
    .line 224
    :cond_8
    new-instance v0, Lcom/android/internal/telephony/uicc/SIMRecords;

    #@a
    invoke-direct {v0, p0, p2, p3}, Lcom/android/internal/telephony/uicc/SIMRecords;-><init>(Lcom/android/internal/telephony/uicc/UiccCardApplication;Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;)V

    #@d
    .line 231
    :goto_d
    return-object v0

    #@e
    .line 225
    :cond_e
    sget-object v0, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;->APPTYPE_RUIM:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;

    #@10
    if-eq p1, v0, :cond_16

    #@12
    sget-object v0, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;->APPTYPE_CSIM:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;

    #@14
    if-ne p1, v0, :cond_1c

    #@16
    .line 226
    :cond_16
    new-instance v0, Lcom/android/internal/telephony/uicc/RuimRecords;

    #@18
    invoke-direct {v0, p0, p2, p3}, Lcom/android/internal/telephony/uicc/RuimRecords;-><init>(Lcom/android/internal/telephony/uicc/UiccCardApplication;Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;)V

    #@1b
    goto :goto_d

    #@1c
    .line 227
    :cond_1c
    sget-object v0, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;->APPTYPE_ISIM:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;

    #@1e
    if-ne p1, v0, :cond_26

    #@20
    .line 228
    new-instance v0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;

    #@22
    invoke-direct {v0, p0, p2, p3}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;-><init>(Lcom/android/internal/telephony/uicc/UiccCardApplication;Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;)V

    #@25
    goto :goto_d

    #@26
    .line 231
    :cond_26
    const/4 v0, 0x0

    #@27
    goto :goto_d
.end method

.method private isHiddenReset()Z
    .registers 14

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    const/4 v9, 0x1

    #@2
    .line 1003
    const/4 v5, 0x0

    #@3
    .line 1004
    .local v5, modem_rst_cnt:I
    const/4 v0, 0x0

    #@4
    .line 1005
    .local v0, alreadyInsert:I
    const/4 v4, 0x0

    #@5
    .line 1007
    .local v4, line:Ljava/lang/String;
    :try_start_5
    new-instance v2, Ljava/io/FileReader;

    #@7
    const-string v11, "/sys/module/subsystem_restart/parameters/modem_reboot_cnt"

    #@9
    invoke-direct {v2, v11}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    #@c
    .line 1008
    .local v2, fr:Ljava/io/FileReader;
    new-instance v7, Ljava/io/BufferedReader;

    #@e
    invoke-direct {v7, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    #@11
    .line 1009
    .local v7, reader:Ljava/io/BufferedReader;
    const-string v11, "[LGE] - modem_reboot_cnt read "

    #@13
    invoke-direct {p0, v11}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->loge(Ljava/lang/String;)V

    #@16
    .line 1011
    invoke-virtual {v7}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    #@19
    move-result-object v4

    #@1a
    if-eqz v4, :cond_32

    #@1c
    .line 1012
    new-instance v11, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v12, "[LGE] - readLine rst_cnt_string"

    #@23
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v11

    #@27
    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v11

    #@2b
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v11

    #@2f
    invoke-direct {p0, v11}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->loge(Ljava/lang/String;)V

    #@32
    .line 1016
    :cond_32
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@35
    move-result v5

    #@36
    .line 1017
    new-instance v11, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v12, "[LGE] - modem_rst_cnt "

    #@3d
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v11

    #@41
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@44
    move-result-object v11

    #@45
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v11

    #@49
    invoke-direct {p0, v11}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->loge(Ljava/lang/String;)V

    #@4c
    .line 1030
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getInternalPIN()Ljava/lang/String;

    #@4f
    move-result-object v8

    #@50
    .line 1031
    .local v8, temp:Ljava/lang/String;
    if-eqz v8, :cond_5f

    #@52
    .line 1032
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    #@55
    move-result v11

    #@56
    const/4 v12, 0x4

    #@57
    if-lt v11, v12, :cond_5f

    #@59
    .line 1033
    const-string v11, "[LGE] - already setpinternalPin"

    #@5b
    invoke-direct {p0, v11}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->loge(Ljava/lang/String;)V

    #@5e
    .line 1034
    const/4 v0, 0x1

    #@5f
    .line 1037
    :cond_5f
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_62
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_62} :catch_76
    .catch Ljava/lang/NumberFormatException; {:try_start_5 .. :try_end_62} :catch_7d
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_62} :catch_84

    #@62
    .line 1046
    .end local v2           #fr:Ljava/io/FileReader;
    .end local v7           #reader:Ljava/io/BufferedReader;
    .end local v8           #temp:Ljava/lang/String;
    :goto_62
    const-string v11, "1"

    #@64
    const-string v12, "ro.lge.hiddenreset"

    #@66
    invoke-static {v12}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@69
    move-result-object v12

    #@6a
    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6d
    move-result v11

    #@6e
    if-eqz v11, :cond_9c

    #@70
    .line 1047
    const-string v10, "[LGE] - hiddenreset occured "

    #@72
    invoke-direct {p0, v10}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->loge(Ljava/lang/String;)V

    #@75
    .line 1060
    :goto_75
    return v9

    #@76
    .line 1038
    :catch_76
    move-exception v3

    #@77
    .line 1039
    .local v3, ie:Ljava/io/IOException;
    const-string v11, "[LGE] - file not exist "

    #@79
    invoke-direct {p0, v11}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->loge(Ljava/lang/String;)V

    #@7c
    goto :goto_62

    #@7d
    .line 1040
    .end local v3           #ie:Ljava/io/IOException;
    :catch_7d
    move-exception v6

    #@7e
    .line 1041
    .local v6, nfe:Ljava/lang/NumberFormatException;
    const-string v11, "[LGE] - not number "

    #@80
    invoke-direct {p0, v11}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->loge(Ljava/lang/String;)V

    #@83
    goto :goto_62

    #@84
    .line 1042
    .end local v6           #nfe:Ljava/lang/NumberFormatException;
    :catch_84
    move-exception v1

    #@85
    .line 1043
    .local v1, e:Ljava/lang/Exception;
    new-instance v11, Ljava/lang/StringBuilder;

    #@87
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@8a
    const-string v12, "[LGE] -error "

    #@8c
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v11

    #@90
    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@93
    move-result-object v11

    #@94
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@97
    move-result-object v11

    #@98
    invoke-direct {p0, v11}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->loge(Ljava/lang/String;)V

    #@9b
    goto :goto_62

    #@9c
    .line 1049
    .end local v1           #e:Ljava/lang/Exception;
    :cond_9c
    if-lez v5, :cond_ad

    #@9e
    .line 1050
    if-ne v0, v9, :cond_a6

    #@a0
    .line 1052
    const-string v10, "[LGE] - modem crash arise! hidden reset flag on! "

    #@a2
    invoke-direct {p0, v10}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->loge(Ljava/lang/String;)V

    #@a5
    goto :goto_75

    #@a6
    .line 1055
    :cond_a6
    const-string v9, "[LGE] - modem crash arise! but stored pin is empty! "

    #@a8
    invoke-direct {p0, v9}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->loge(Ljava/lang/String;)V

    #@ab
    move v9, v10

    #@ac
    .line 1056
    goto :goto_75

    #@ad
    .line 1059
    :cond_ad
    const-string v9, "[LGE] - hiddenreset not occured "

    #@af
    invoke-direct {p0, v9}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->loge(Ljava/lang/String;)V

    #@b2
    move v9, v10

    #@b3
    .line 1060
    goto :goto_75
.end method

.method private log(Ljava/lang/String;)V
    .registers 3
    .parameter "msg"

    #@0
    .prologue
    .line 951
    const-string v0, "RIL_UiccCardApplication"

    #@2
    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 952
    return-void
.end method

.method private loge(Ljava/lang/String;)V
    .registers 3
    .parameter "msg"

    #@0
    .prologue
    .line 955
    const-string v0, "RIL_UiccCardApplication"

    #@2
    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 956
    return-void
.end method

.method private declared-synchronized notifyPersoLockedRegistrantsIfNeeded(Landroid/os/Registrant;)V
    .registers 6
    .parameter "r"

    #@0
    .prologue
    .line 646
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mDestroyed:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_31

    #@3
    if-eqz v1, :cond_7

    #@5
    .line 661
    :cond_5
    :goto_5
    monitor-exit p0

    #@6
    return-void

    #@7
    .line 650
    :cond_7
    :try_start_7
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mAppState:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;

    #@9
    sget-object v2, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;->APPSTATE_SUBSCRIPTION_PERSO:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;

    #@b
    if-ne v1, v2, :cond_5

    #@d
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->isPersoLocked()Z

    #@10
    move-result v1

    #@11
    if-eqz v1, :cond_5

    #@13
    .line 652
    new-instance v0, Landroid/os/AsyncResult;

    #@15
    const/4 v1, 0x0

    #@16
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPersoSubState:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;

    #@18
    invoke-virtual {v2}, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;->ordinal()I

    #@1b
    move-result v2

    #@1c
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1f
    move-result-object v2

    #@20
    const/4 v3, 0x0

    #@21
    invoke-direct {v0, v1, v2, v3}, Landroid/os/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@24
    .line 653
    .local v0, ar:Landroid/os/AsyncResult;
    if-nez p1, :cond_34

    #@26
    .line 654
    const-string v1, "Notifying registrants: PERSO_LOCKED"

    #@28
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->log(Ljava/lang/String;)V

    #@2b
    .line 655
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPersoLockedRegistrants:Landroid/os/RegistrantList;

    #@2d
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->notifyRegistrants(Landroid/os/AsyncResult;)V
    :try_end_30
    .catchall {:try_start_7 .. :try_end_30} :catchall_31

    #@30
    goto :goto_5

    #@31
    .line 646
    .end local v0           #ar:Landroid/os/AsyncResult;
    :catchall_31
    move-exception v1

    #@32
    monitor-exit p0

    #@33
    throw v1

    #@34
    .line 657
    .restart local v0       #ar:Landroid/os/AsyncResult;
    :cond_34
    :try_start_34
    const-string v1, "Notifying 1 registrant: PERSO_LOCKED"

    #@36
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->log(Ljava/lang/String;)V

    #@39
    .line 658
    invoke-virtual {p1, v0}, Landroid/os/Registrant;->notifyRegistrant(Landroid/os/AsyncResult;)V
    :try_end_3c
    .catchall {:try_start_34 .. :try_end_3c} :catchall_31

    #@3c
    goto :goto_5
.end method

.method private notifyPinLockedRegistrantsIfNeeded(Landroid/os/Registrant;)V
    .registers 5
    .parameter "r"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 618
    iget-boolean v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mDestroyed:Z

    #@3
    if-eqz v0, :cond_6

    #@5
    .line 638
    :cond_5
    :goto_5
    return-void

    #@6
    .line 622
    :cond_6
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mAppState:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;

    #@8
    sget-object v1, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;->APPSTATE_PIN:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;

    #@a
    if-eq v0, v1, :cond_12

    #@c
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mAppState:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;

    #@e
    sget-object v1, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;->APPSTATE_PUK:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;

    #@10
    if-ne v0, v1, :cond_5

    #@12
    .line 624
    :cond_12
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPin1State:Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;

    #@14
    sget-object v1, Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;->PINSTATE_ENABLED_VERIFIED:Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;

    #@16
    if-eq v0, v1, :cond_1e

    #@18
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPin1State:Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;

    #@1a
    sget-object v1, Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;->PINSTATE_DISABLED:Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;

    #@1c
    if-ne v0, v1, :cond_24

    #@1e
    .line 626
    :cond_1e
    const-string v0, "Sanity check failed! APPSTATE is locked while PIN1 is not!!!"

    #@20
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->loge(Ljava/lang/String;)V

    #@23
    goto :goto_5

    #@24
    .line 630
    :cond_24
    if-nez p1, :cond_31

    #@26
    .line 631
    const-string v0, "Notifying registrants: LOCKED"

    #@28
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->log(Ljava/lang/String;)V

    #@2b
    .line 632
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPinLockedRegistrants:Landroid/os/RegistrantList;

    #@2d
    invoke-virtual {v0}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@30
    goto :goto_5

    #@31
    .line 634
    :cond_31
    const-string v0, "Notifying 1 registrant: LOCKED"

    #@33
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->log(Ljava/lang/String;)V

    #@36
    .line 635
    new-instance v0, Landroid/os/AsyncResult;

    #@38
    invoke-direct {v0, v2, v2, v2}, Landroid/os/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@3b
    invoke-virtual {p1, v0}, Landroid/os/Registrant;->notifyRegistrant(Landroid/os/AsyncResult;)V

    #@3e
    goto :goto_5
.end method

.method private notifyReadyRegistrantsIfNeeded(Landroid/os/Registrant;)V
    .registers 5
    .parameter "r"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 591
    iget-boolean v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mDestroyed:Z

    #@3
    if-eqz v0, :cond_6

    #@5
    .line 610
    :cond_5
    :goto_5
    return-void

    #@6
    .line 594
    :cond_6
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mAppState:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;

    #@8
    sget-object v1, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;->APPSTATE_READY:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;

    #@a
    if-ne v0, v1, :cond_5

    #@c
    .line 595
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPin1State:Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;

    #@e
    sget-object v1, Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;->PINSTATE_ENABLED_NOT_VERIFIED:Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;

    #@10
    if-eq v0, v1, :cond_1e

    #@12
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPin1State:Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;

    #@14
    sget-object v1, Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;->PINSTATE_ENABLED_BLOCKED:Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;

    #@16
    if-eq v0, v1, :cond_1e

    #@18
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPin1State:Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;

    #@1a
    sget-object v1, Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;->PINSTATE_ENABLED_PERM_BLOCKED:Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;

    #@1c
    if-ne v0, v1, :cond_24

    #@1e
    .line 598
    :cond_1e
    const-string v0, "Sanity check failed! APPSTATE is ready while PIN1 is not verified!!!"

    #@20
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->loge(Ljava/lang/String;)V

    #@23
    goto :goto_5

    #@24
    .line 602
    :cond_24
    if-nez p1, :cond_31

    #@26
    .line 603
    const-string v0, "Notifying registrants: READY"

    #@28
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->log(Ljava/lang/String;)V

    #@2b
    .line 604
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mReadyRegistrants:Landroid/os/RegistrantList;

    #@2d
    invoke-virtual {v0}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@30
    goto :goto_5

    #@31
    .line 606
    :cond_31
    const-string v0, "Notifying 1 registrant: READY"

    #@33
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->log(Ljava/lang/String;)V

    #@36
    .line 607
    new-instance v0, Landroid/os/AsyncResult;

    #@38
    invoke-direct {v0, v2, v2, v2}, Landroid/os/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@3b
    invoke-virtual {p1, v0}, Landroid/os/Registrant;->notifyRegistrant(Landroid/os/AsyncResult;)V

    #@3e
    goto :goto_5
.end method

.method private onChangeFacilityLock(Landroid/os/AsyncResult;)V
    .registers 5
    .parameter "ar"

    #@0
    .prologue
    .line 371
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 372
    :try_start_3
    iget-object v0, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@5
    if-nez v0, :cond_44

    #@7
    .line 374
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->internalPin:Ljava/lang/String;

    #@9
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->setInternalPIN(Ljava/lang/String;)V

    #@c
    .line 376
    iget-boolean v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mDesiredPinLocked:Z

    #@e
    iput-boolean v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mIccLockEnabled:Z

    #@10
    .line 377
    new-instance v0, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v2, "EVENT_CHANGE_FACILITY_LOCK_DONE: mIccLockEnabled= "

    #@17
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v0

    #@1b
    iget-boolean v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mIccLockEnabled:Z

    #@1d
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@20
    move-result-object v0

    #@21
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v0

    #@25
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->log(Ljava/lang/String;)V

    #@28
    .line 380
    iget v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPin1InitRetryCount:I

    #@2a
    iput v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPin1RetryCount:I

    #@2c
    .line 388
    :goto_2c
    iget-object v0, p1, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@2e
    check-cast v0, Landroid/os/Message;

    #@30
    invoke-static {v0}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;)Landroid/os/AsyncResult;

    #@33
    move-result-object v0

    #@34
    iget-object v2, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@36
    iput-object v2, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@38
    .line 389
    iget-object v0, p1, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@3a
    check-cast v0, Landroid/os/Message;

    #@3c
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@3f
    .line 391
    const/4 v0, 0x0

    #@40
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->internalPin:Ljava/lang/String;

    #@42
    .line 394
    monitor-exit v1

    #@43
    .line 395
    return-void

    #@44
    .line 383
    :cond_44
    iget-object v0, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@46
    if-eqz v0, :cond_4c

    #@48
    .line 384
    const/4 v0, 0x1

    #@49
    invoke-direct {p0, p1, v0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->parsePinPukErrorResult(Landroid/os/AsyncResult;Z)V

    #@4c
    .line 386
    :cond_4c
    new-instance v0, Ljava/lang/StringBuilder;

    #@4e
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@51
    const-string v2, "Error change facility lock with exception "

    #@53
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v0

    #@57
    iget-object v2, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@59
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v0

    #@5d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@60
    move-result-object v0

    #@61
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->loge(Ljava/lang/String;)V

    #@64
    goto :goto_2c

    #@65
    .line 394
    :catchall_65
    move-exception v0

    #@66
    monitor-exit v1
    :try_end_67
    .catchall {:try_start_3 .. :try_end_67} :catchall_65

    #@67
    throw v0
.end method

.method private onChangeFdnDone(Landroid/os/AsyncResult;)V
    .registers 6
    .parameter "ar"

    #@0
    .prologue
    .line 294
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 295
    :try_start_3
    iget-object v1, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@5
    if-nez v1, :cond_34

    #@7
    .line 296
    iget-boolean v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mDesiredFdnEnabled:Z

    #@9
    iput-boolean v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mIccFdnEnabled:Z

    #@b
    .line 297
    new-instance v1, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v3, "EVENT_CHANGE_FACILITY_FDN_DONE: mIccFdnEnabled="

    #@12
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    iget-boolean v3, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mIccFdnEnabled:Z

    #@18
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->log(Ljava/lang/String;)V

    #@23
    .line 307
    :goto_23
    iget-object v0, p1, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@25
    check-cast v0, Landroid/os/Message;

    #@27
    .line 308
    .local v0, response:Landroid/os/Message;
    invoke-static {v0}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;)Landroid/os/AsyncResult;

    #@2a
    move-result-object v1

    #@2b
    iget-object v3, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@2d
    iput-object v3, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@2f
    .line 309
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@32
    .line 310
    monitor-exit v2

    #@33
    .line 311
    return-void

    #@34
    .line 301
    .end local v0           #response:Landroid/os/Message;
    :cond_34
    iget-object v1, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@36
    if-eqz v1, :cond_3c

    #@38
    .line 302
    const/4 v1, 0x0

    #@39
    invoke-direct {p0, p1, v1}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->parsePinPukErrorResult(Landroid/os/AsyncResult;Z)V

    #@3c
    .line 305
    :cond_3c
    new-instance v1, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v3, "Error change facility fdn with exception "

    #@43
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v1

    #@47
    iget-object v3, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@49
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v1

    #@4d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50
    move-result-object v1

    #@51
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->loge(Ljava/lang/String;)V

    #@54
    goto :goto_23

    #@55
    .line 310
    :catchall_55
    move-exception v1

    #@56
    monitor-exit v2
    :try_end_57
    .catchall {:try_start_3 .. :try_end_57} :catchall_55

    #@57
    throw v1
.end method

.method private onQueryFacilityLock(Landroid/os/AsyncResult;)V
    .registers 8
    .parameter "ar"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 325
    iget-object v4, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mLock:Ljava/lang/Object;

    #@3
    monitor-enter v4

    #@4
    .line 326
    :try_start_4
    iget-object v2, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@6
    if-eqz v2, :cond_22

    #@8
    .line 327
    new-instance v2, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v3, "Error in querying facility lock:"

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    iget-object v3, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->log(Ljava/lang/String;)V

    #@20
    .line 328
    monitor-exit v4

    #@21
    .line 367
    :goto_21
    return-void

    #@22
    .line 331
    :cond_22
    iget-object v2, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@24
    check-cast v2, [I

    #@26
    move-object v0, v2

    #@27
    check-cast v0, [I

    #@29
    move-object v1, v0

    #@2a
    .line 332
    .local v1, ints:[I
    array-length v2, v1

    #@2b
    if-eqz v2, :cond_7f

    #@2d
    .line 333
    new-instance v2, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    const-string v5, "Query facility lock : "

    #@34
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v2

    #@38
    const/4 v5, 0x0

    #@39
    aget v5, v1, v5

    #@3b
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v2

    #@3f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v2

    #@43
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->log(Ljava/lang/String;)V

    #@46
    .line 335
    const/4 v2, 0x0

    #@47
    aget v2, v1, v2

    #@49
    if-eqz v2, :cond_69

    #@4b
    const/4 v2, 0x1

    #@4c
    :goto_4c
    iput-boolean v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mIccLockEnabled:Z

    #@4e
    .line 337
    iget-boolean v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mIccLockEnabled:Z

    #@50
    if-eqz v2, :cond_57

    #@52
    .line 338
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPinLockedRegistrants:Landroid/os/RegistrantList;

    #@54
    invoke-virtual {v2}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@57
    .line 347
    :cond_57
    sget-object v2, Lcom/android/internal/telephony/uicc/UiccCardApplication$2;->$SwitchMap$com$android$internal$telephony$uicc$IccCardStatus$PinState:[I

    #@59
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPin1State:Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;

    #@5b
    invoke-virtual {v3}, Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;->ordinal()I

    #@5e
    move-result v3

    #@5f
    aget v2, v2, v3

    #@61
    packed-switch v2, :pswitch_data_86

    #@64
    .line 366
    :cond_64
    :goto_64
    monitor-exit v4

    #@65
    goto :goto_21

    #@66
    .end local v1           #ints:[I
    :catchall_66
    move-exception v2

    #@67
    monitor-exit v4
    :try_end_68
    .catchall {:try_start_4 .. :try_end_68} :catchall_66

    #@68
    throw v2

    #@69
    .restart local v1       #ints:[I
    :cond_69
    move v2, v3

    #@6a
    .line 335
    goto :goto_4c

    #@6b
    .line 349
    :pswitch_6b
    :try_start_6b
    iget-boolean v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mIccLockEnabled:Z

    #@6d
    if-eqz v2, :cond_64

    #@6f
    .line 350
    const-string v2, "QUERY_FACILITY_LOCK:enabled GET_SIM_STATUS.Pin1:disabled. Fixme"

    #@71
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->loge(Ljava/lang/String;)V

    #@74
    goto :goto_64

    #@75
    .line 358
    :pswitch_75
    iget-boolean v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mIccLockEnabled:Z

    #@77
    if-nez v2, :cond_64

    #@79
    .line 359
    const-string v2, "QUERY_FACILITY_LOCK:disabled GET_SIM_STATUS.Pin1:enabled. Fixme"

    #@7b
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->loge(Ljava/lang/String;)V

    #@7e
    goto :goto_64

    #@7f
    .line 364
    :cond_7f
    const-string v2, "Bogus facility lock response"

    #@81
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->loge(Ljava/lang/String;)V
    :try_end_84
    .catchall {:try_start_6b .. :try_end_84} :catchall_66

    #@84
    goto :goto_64

    #@85
    .line 347
    nop

    #@86
    :pswitch_data_86
    .packed-switch 0x1
        :pswitch_6b
        :pswitch_75
        :pswitch_75
        :pswitch_75
        :pswitch_75
    .end packed-switch
.end method

.method private onQueryFdnEnabled(Landroid/os/AsyncResult;)V
    .registers 9
    .parameter "ar"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 269
    iget-object v5, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mLock:Ljava/lang/Object;

    #@4
    monitor-enter v5

    #@5
    .line 270
    :try_start_5
    iget-object v2, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@7
    if-eqz v2, :cond_23

    #@9
    .line 271
    new-instance v2, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v3, "Error in querying facility lock:"

    #@10
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    iget-object v3, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->log(Ljava/lang/String;)V

    #@21
    .line 272
    monitor-exit v5

    #@22
    .line 291
    :goto_22
    return-void

    #@23
    .line 275
    :cond_23
    iget-object v2, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@25
    check-cast v2, [I

    #@27
    move-object v0, v2

    #@28
    check-cast v0, [I

    #@2a
    move-object v1, v0

    #@2b
    .line 276
    .local v1, result:[I
    array-length v2, v1

    #@2c
    if-eqz v2, :cond_71

    #@2e
    .line 278
    const/4 v2, 0x0

    #@2f
    aget v2, v1, v2

    #@31
    const/4 v6, 0x2

    #@32
    if-ne v2, v6, :cond_63

    #@34
    .line 279
    const/4 v2, 0x0

    #@35
    iput-boolean v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mIccFdnEnabled:Z

    #@37
    .line 280
    const/4 v2, 0x0

    #@38
    iput-boolean v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mIccFdnAvailable:Z

    #@3a
    .line 285
    :goto_3a
    new-instance v2, Ljava/lang/StringBuilder;

    #@3c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3f
    const-string v3, "Query facility FDN : FDN service available: "

    #@41
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v2

    #@45
    iget-boolean v3, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mIccFdnAvailable:Z

    #@47
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v2

    #@4b
    const-string v3, " enabled: "

    #@4d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v2

    #@51
    iget-boolean v3, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mIccFdnEnabled:Z

    #@53
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@56
    move-result-object v2

    #@57
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v2

    #@5b
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->log(Ljava/lang/String;)V

    #@5e
    .line 290
    :goto_5e
    monitor-exit v5

    #@5f
    goto :goto_22

    #@60
    .end local v1           #result:[I
    :catchall_60
    move-exception v2

    #@61
    monitor-exit v5
    :try_end_62
    .catchall {:try_start_5 .. :try_end_62} :catchall_60

    #@62
    throw v2

    #@63
    .line 282
    .restart local v1       #result:[I
    :cond_63
    const/4 v2, 0x0

    #@64
    :try_start_64
    aget v2, v1, v2

    #@66
    if-ne v2, v3, :cond_6f

    #@68
    move v2, v3

    #@69
    :goto_69
    iput-boolean v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mIccFdnEnabled:Z

    #@6b
    .line 283
    const/4 v2, 0x1

    #@6c
    iput-boolean v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mIccFdnAvailable:Z

    #@6e
    goto :goto_3a

    #@6f
    :cond_6f
    move v2, v4

    #@70
    .line 282
    goto :goto_69

    #@71
    .line 288
    :cond_71
    const-string v2, "Bogus facility lock response"

    #@73
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->loge(Ljava/lang/String;)V
    :try_end_76
    .catchall {:try_start_64 .. :try_end_76} :catchall_60

    #@76
    goto :goto_5e
.end method

.method private parsePinPukErrorResult(Landroid/os/AsyncResult;Z)V
    .registers 7
    .parameter "ar"
    .parameter "isPin1"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 415
    iget-object v2, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@3
    check-cast v2, [I

    #@5
    move-object v0, v2

    #@6
    check-cast v0, [I

    #@8
    .line 416
    .local v0, intArray:[I
    array-length v1, v0

    #@9
    .line 419
    .local v1, length:I
    if-lez v1, :cond_15

    #@b
    .line 420
    if-eqz p2, :cond_1f

    #@d
    .line 421
    iget-boolean v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->isPuk1:Z

    #@f
    if-eqz v2, :cond_1a

    #@11
    .line 422
    aget v2, v0, v3

    #@13
    iput v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPuk1RetryCount:I

    #@15
    .line 432
    :cond_15
    :goto_15
    iput-boolean v3, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->isPuk1:Z

    #@17
    .line 433
    iput-boolean v3, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->isPuk2:Z

    #@19
    .line 435
    return-void

    #@1a
    .line 424
    :cond_1a
    aget v2, v0, v3

    #@1c
    iput v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPin1RetryCount:I

    #@1e
    goto :goto_15

    #@1f
    .line 426
    :cond_1f
    iget-boolean v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->isPuk2:Z

    #@21
    if-eqz v2, :cond_28

    #@23
    .line 427
    aget v2, v0, v3

    #@25
    iput v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPuk2RetryCount:I

    #@27
    goto :goto_15

    #@28
    .line 429
    :cond_28
    aget v2, v0, v3

    #@2a
    iput v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPin2RetryCount:I

    #@2c
    goto :goto_15
.end method

.method private queryFdn()V
    .registers 8

    #@0
    .prologue
    .line 257
    const/4 v3, 0x7

    #@1
    .line 260
    .local v3, serviceClassX:I
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@3
    const-string v1, "FD"

    #@5
    const-string v2, ""

    #@7
    iget-object v4, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mAid:Ljava/lang/String;

    #@9
    iget-object v5, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mHandler:Landroid/os/Handler;

    #@b
    const/4 v6, 0x4

    #@c
    invoke-virtual {v5, v6}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@f
    move-result-object v5

    #@10
    invoke-interface/range {v0 .. v5}, Lcom/android/internal/telephony/CommandsInterface;->queryFacilityLockForApp(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Landroid/os/Message;)V

    #@13
    .line 263
    return-void
.end method

.method private queryPin1State()V
    .registers 8

    #@0
    .prologue
    .line 315
    const/4 v3, 0x7

    #@1
    .line 318
    .local v3, serviceClassX:I
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@3
    const-string v1, "SC"

    #@5
    const-string v2, ""

    #@7
    iget-object v4, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mAid:Ljava/lang/String;

    #@9
    iget-object v5, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mHandler:Landroid/os/Handler;

    #@b
    const/4 v6, 0x6

    #@c
    invoke-virtual {v5, v6}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@f
    move-result-object v5

    #@10
    invoke-interface/range {v0 .. v5}, Lcom/android/internal/telephony/CommandsInterface;->queryFacilityLockForApp(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Landroid/os/Message;)V

    #@13
    .line 321
    return-void
.end method


# virtual methods
.method public HiddensupplyPin(Ljava/lang/String;)V
    .registers 7
    .parameter "pin"

    #@0
    .prologue
    .line 980
    sget-boolean v0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->ENABLE_PRIVACY_LOG:Z

    #@2
    if-eqz v0, :cond_1a

    #@4
    .line 982
    new-instance v0, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v1, "[LGE] - HiddensupplyPin : "

    #@b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v0

    #@17
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->log(Ljava/lang/String;)V

    #@1a
    .line 984
    :cond_1a
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@1c
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mAid:Ljava/lang/String;

    #@1e
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mHandler:Landroid/os/Handler;

    #@20
    const/16 v3, 0x9

    #@22
    const/4 v4, 0x0

    #@23
    invoke-virtual {v2, v3, v4}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@26
    move-result-object v2

    #@27
    invoke-interface {v0, p1, v1, v2}, Lcom/android/internal/telephony/CommandsInterface;->supplyIccPinForApp(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    #@2a
    .line 985
    return-void
.end method

.method public changeIccFdnPassword(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 9
    .parameter "oldPassword"
    .parameter "newPassword"
    .parameter "onComplete"

    #@0
    .prologue
    .line 927
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 928
    :try_start_3
    const-string v0, "changeIccFdnPassword"

    #@5
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->log(Ljava/lang/String;)V

    #@8
    .line 930
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@a
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mAid:Ljava/lang/String;

    #@c
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mHandler:Landroid/os/Handler;

    #@e
    const/4 v4, 0x3

    #@f
    invoke-virtual {v3, v4, p3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@12
    move-result-object v3

    #@13
    invoke-interface {v0, p1, p2, v2, v3}, Lcom/android/internal/telephony/CommandsInterface;->changeIccPin2ForApp(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    #@16
    .line 933
    monitor-exit v1

    #@17
    .line 934
    return-void

    #@18
    .line 933
    :catchall_18
    move-exception v0

    #@19
    monitor-exit v1
    :try_end_1a
    .catchall {:try_start_3 .. :try_end_1a} :catchall_18

    #@1a
    throw v0
.end method

.method public changeIccLockPassword(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 9
    .parameter "oldPassword"
    .parameter "newPassword"
    .parameter "onComplete"

    #@0
    .prologue
    .line 902
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 903
    :try_start_3
    const-string v0, "changeIccLockPassword"

    #@5
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->log(Ljava/lang/String;)V

    #@8
    .line 905
    iput-object p2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->internalPin:Ljava/lang/String;

    #@a
    .line 908
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@c
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mAid:Ljava/lang/String;

    #@e
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mHandler:Landroid/os/Handler;

    #@10
    const/4 v4, 0x2

    #@11
    invoke-virtual {v3, v4, p3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@14
    move-result-object v3

    #@15
    invoke-interface {v0, p1, p2, v2, v3}, Lcom/android/internal/telephony/CommandsInterface;->changeIccPinForApp(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    #@18
    .line 911
    monitor-exit v1

    #@19
    .line 912
    return-void

    #@1a
    .line 911
    :catchall_1a
    move-exception v0

    #@1b
    monitor-exit v1
    :try_end_1c
    .catchall {:try_start_3 .. :try_end_1c} :catchall_1a

    #@1c
    throw v0
.end method

.method dispose()V
    .registers 4

    #@0
    .prologue
    .line 212
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 213
    :try_start_3
    new-instance v0, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mAppType:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;

    #@a
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v0

    #@e
    const-string v2, " being Disposed"

    #@10
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v0

    #@14
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v0

    #@18
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->log(Ljava/lang/String;)V

    #@1b
    .line 214
    const/4 v0, 0x1

    #@1c
    iput-boolean v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mDestroyed:Z

    #@1e
    .line 215
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@20
    if-eqz v0, :cond_27

    #@22
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@24
    invoke-virtual {v0}, Lcom/android/internal/telephony/uicc/IccRecords;->dispose()V

    #@27
    .line 216
    :cond_27
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mIccFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@29
    if-eqz v0, :cond_30

    #@2b
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mIccFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@2d
    invoke-virtual {v0}, Lcom/android/internal/telephony/uicc/IccFileHandler;->dispose()V

    #@30
    .line 217
    :cond_30
    const/4 v0, 0x0

    #@31
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@33
    .line 218
    const/4 v0, 0x0

    #@34
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mIccFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@36
    .line 219
    monitor-exit v1

    #@37
    .line 220
    return-void

    #@38
    .line 219
    :catchall_38
    move-exception v0

    #@39
    monitor-exit v1
    :try_end_3a
    .catchall {:try_start_3 .. :try_end_3a} :catchall_38

    #@3a
    throw v0
.end method

.method public getAid()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 682
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 683
    :try_start_3
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mAid:Ljava/lang/String;

    #@5
    monitor-exit v1

    #@6
    return-object v0

    #@7
    .line 684
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method public getAppLabel()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 688
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mAppLabel:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getHiddenResetFlag()Z
    .registers 2

    #@0
    .prologue
    .line 976
    iget-boolean v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->hidden_reset_flag:Z

    #@2
    return v0
.end method

.method public getIccFdnAvailable()Z
    .registers 2

    #@0
    .prologue
    .line 829
    iget-boolean v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mIccFdnAvailable:Z

    #@2
    return v0
.end method

.method public getIccFdnEnabled()Z
    .registers 3

    #@0
    .prologue
    .line 818
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 819
    :try_start_3
    iget-boolean v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mIccFdnEnabled:Z

    #@5
    monitor-exit v1

    #@6
    return v0

    #@7
    .line 820
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method public getIccFileHandler()Lcom/android/internal/telephony/uicc/IccFileHandler;
    .registers 3

    #@0
    .prologue
    .line 701
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 702
    :try_start_3
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mIccFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@5
    monitor-exit v1

    #@6
    return-object v0

    #@7
    .line 703
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method public getIccLockEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 799
    iget-boolean v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mIccLockEnabled:Z

    #@2
    return v0
.end method

.method public getIccPin1RetryCount()I
    .registers 2

    #@0
    .prologue
    .line 441
    iget v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPin1RetryCount:I

    #@2
    return v0
.end method

.method public getIccPin2Blocked()Z
    .registers 3

    #@0
    .prologue
    .line 940
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPin2State:Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;

    #@2
    sget-object v1, Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;->PINSTATE_ENABLED_BLOCKED:Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;

    #@4
    if-ne v0, v1, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public getIccPin2RetryCount()I
    .registers 2

    #@0
    .prologue
    .line 448
    iget v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPin2RetryCount:I

    #@2
    return v0
.end method

.method public getIccPuk1RetryCount()I
    .registers 3

    #@0
    .prologue
    .line 959
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "getIccPuk1RetryCount : "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPuk1RetryCount:I

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->log(Ljava/lang/String;)V

    #@18
    .line 960
    iget v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPuk1RetryCount:I

    #@1a
    return v0
.end method

.method public getIccPuk2Blocked()Z
    .registers 3

    #@0
    .prologue
    .line 947
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPin2State:Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;

    #@2
    sget-object v1, Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;->PINSTATE_ENABLED_PERM_BLOCKED:Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;

    #@4
    if-ne v0, v1, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public getIccPuk2RetryCount()I
    .registers 3

    #@0
    .prologue
    .line 964
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "getIccPuk2RetryCount : "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPuk2RetryCount:I

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->log(Ljava/lang/String;)V

    #@18
    .line 965
    iget v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPuk2RetryCount:I

    #@1a
    return v0
.end method

.method public getIccRecords()Lcom/android/internal/telephony/uicc/IccRecords;
    .registers 3

    #@0
    .prologue
    .line 707
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 708
    :try_start_3
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@5
    monitor-exit v1

    #@6
    return-object v0

    #@7
    .line 709
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method public getInternalPIN()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 987
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mContext:Landroid/content/Context;

    #@2
    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    #@5
    move-result-object v0

    #@6
    .line 988
    .local v0, sp:Landroid/content/SharedPreferences;
    const-string v1, "internal_pin_value_key"

    #@8
    const/4 v2, 0x0

    #@9
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@c
    move-result-object v1

    #@d
    return-object v1
.end method

.method public getPersoSubState()Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;
    .registers 3

    #@0
    .prologue
    .line 676
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 677
    :try_start_3
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPersoSubState:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;

    #@5
    monitor-exit v1

    #@6
    return-object v0

    #@7
    .line 678
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method public getPin1State()Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;
    .registers 3

    #@0
    .prologue
    .line 692
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 693
    :try_start_3
    iget-boolean v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPin1Replaced:Z

    #@5
    if-eqz v0, :cond_f

    #@7
    .line 694
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mUiccCard:Lcom/android/internal/telephony/uicc/UiccCard;

    #@9
    invoke-virtual {v0}, Lcom/android/internal/telephony/uicc/UiccCard;->getUniversalPinState()Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;

    #@c
    move-result-object v0

    #@d
    monitor-exit v1

    #@e
    .line 696
    :goto_e
    return-object v0

    #@f
    :cond_f
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPin1State:Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;

    #@11
    monitor-exit v1

    #@12
    goto :goto_e

    #@13
    .line 697
    :catchall_13
    move-exception v0

    #@14
    monitor-exit v1
    :try_end_15
    .catchall {:try_start_3 .. :try_end_15} :catchall_13

    #@15
    throw v0
.end method

.method public getPin2State()Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;
    .registers 3

    #@0
    .prologue
    .line 969
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 970
    :try_start_3
    const-string v0, "[lge_uicc] getPin2State"

    #@5
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->log(Ljava/lang/String;)V

    #@8
    .line 971
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPin2State:Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;

    #@a
    monitor-exit v1

    #@b
    return-object v0

    #@c
    .line 972
    :catchall_c
    move-exception v0

    #@d
    monitor-exit v1
    :try_end_e
    .catchall {:try_start_3 .. :try_end_e} :catchall_c

    #@e
    throw v0
.end method

.method public getState()Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;
    .registers 3

    #@0
    .prologue
    .line 664
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 665
    :try_start_3
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mAppState:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;

    #@5
    monitor-exit v1

    #@6
    return-object v0

    #@7
    .line 666
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method public getType()Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;
    .registers 3

    #@0
    .prologue
    .line 670
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 671
    :try_start_3
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mAppType:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;

    #@5
    monitor-exit v1

    #@6
    return-object v0

    #@7
    .line 672
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method public isPersoLocked()Z
    .registers 3

    #@0
    .prologue
    .line 713
    sget-object v0, Lcom/android/internal/telephony/uicc/UiccCardApplication$2;->$SwitchMap$com$android$internal$telephony$uicc$IccCardApplicationStatus$PersoSubState:[I

    #@2
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPersoSubState:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;

    #@4
    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;->ordinal()I

    #@7
    move-result v1

    #@8
    aget v0, v0, v1

    #@a
    packed-switch v0, :pswitch_data_12

    #@d
    .line 719
    const/4 v0, 0x1

    #@e
    :goto_e
    return v0

    #@f
    .line 717
    :pswitch_f
    const/4 v0, 0x0

    #@10
    goto :goto_e

    #@11
    .line 713
    nop

    #@12
    :pswitch_data_12
    .packed-switch 0x1
        :pswitch_f
        :pswitch_f
        :pswitch_f
    .end packed-switch
.end method

.method public registerForLocked(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 7
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 555
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 556
    :try_start_3
    new-instance v0, Landroid/os/Registrant;

    #@5
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@8
    .line 557
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPinLockedRegistrants:Landroid/os/RegistrantList;

    #@a
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@d
    .line 558
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->notifyPinLockedRegistrantsIfNeeded(Landroid/os/Registrant;)V

    #@10
    .line 559
    monitor-exit v2

    #@11
    .line 560
    return-void

    #@12
    .line 559
    .end local v0           #r:Landroid/os/Registrant;
    :catchall_12
    move-exception v1

    #@13
    monitor-exit v2
    :try_end_14
    .catchall {:try_start_3 .. :try_end_14} :catchall_12

    #@14
    throw v1
.end method

.method public registerForPersoLocked(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 7
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 572
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 573
    :try_start_3
    new-instance v0, Landroid/os/Registrant;

    #@5
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@8
    .line 574
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPersoLockedRegistrants:Landroid/os/RegistrantList;

    #@a
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@d
    .line 575
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->notifyPersoLockedRegistrantsIfNeeded(Landroid/os/Registrant;)V

    #@10
    .line 576
    monitor-exit v2

    #@11
    .line 577
    return-void

    #@12
    .line 576
    .end local v0           #r:Landroid/os/Registrant;
    :catchall_12
    move-exception v1

    #@13
    monitor-exit v2
    :try_end_14
    .catchall {:try_start_3 .. :try_end_14} :catchall_12

    #@14
    throw v1
.end method

.method public registerForReady(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 7
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 538
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 539
    :try_start_3
    new-instance v0, Landroid/os/Registrant;

    #@5
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@8
    .line 540
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mReadyRegistrants:Landroid/os/RegistrantList;

    #@a
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@d
    .line 541
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->notifyReadyRegistrantsIfNeeded(Landroid/os/Registrant;)V

    #@10
    .line 542
    monitor-exit v2

    #@11
    .line 543
    return-void

    #@12
    .line 542
    .end local v0           #r:Landroid/os/Registrant;
    :catchall_12
    move-exception v1

    #@13
    monitor-exit v2
    :try_end_14
    .catchall {:try_start_3 .. :try_end_14} :catchall_12

    #@14
    throw v1
.end method

.method public setIccFdnEnabled(ZLjava/lang/String;Landroid/os/Message;)V
    .registers 12
    .parameter "enabled"
    .parameter "password"
    .parameter "onComplete"

    #@0
    .prologue
    .line 874
    iget-object v7, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v7

    #@3
    .line 876
    const/16 v4, 0xf

    #@5
    .line 881
    .local v4, serviceClassX:I
    :try_start_5
    iput-boolean p1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mDesiredFdnEnabled:Z

    #@7
    .line 883
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@9
    const-string v1, "FD"

    #@b
    iget-object v5, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mAid:Ljava/lang/String;

    #@d
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mHandler:Landroid/os/Handler;

    #@f
    const/4 v3, 0x5

    #@10
    invoke-virtual {v2, v3, p3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@13
    move-result-object v6

    #@14
    move v2, p1

    #@15
    move-object v3, p2

    #@16
    invoke-interface/range {v0 .. v6}, Lcom/android/internal/telephony/CommandsInterface;->setFacilityLockForApp(Ljava/lang/String;ZLjava/lang/String;ILjava/lang/String;Landroid/os/Message;)V

    #@19
    .line 886
    monitor-exit v7

    #@1a
    .line 887
    return-void

    #@1b
    .line 886
    :catchall_1b
    move-exception v0

    #@1c
    monitor-exit v7
    :try_end_1d
    .catchall {:try_start_5 .. :try_end_1d} :catchall_1b

    #@1d
    throw v0
.end method

.method public setIccLockEnabled(ZLjava/lang/String;Landroid/os/Message;)V
    .registers 12
    .parameter "enabled"
    .parameter "password"
    .parameter "onComplete"

    #@0
    .prologue
    .line 845
    iget-object v7, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v7

    #@3
    .line 847
    const/4 v4, 0x7

    #@4
    .line 851
    .local v4, serviceClassX:I
    :try_start_4
    iput-boolean p1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mDesiredPinLocked:Z

    #@6
    .line 853
    iput-object p2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->internalPin:Ljava/lang/String;

    #@8
    .line 855
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@a
    const-string v1, "SC"

    #@c
    iget-object v5, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mAid:Ljava/lang/String;

    #@e
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mHandler:Landroid/os/Handler;

    #@10
    const/4 v3, 0x7

    #@11
    invoke-virtual {v2, v3, p3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@14
    move-result-object v6

    #@15
    move v2, p1

    #@16
    move-object v3, p2

    #@17
    invoke-interface/range {v0 .. v6}, Lcom/android/internal/telephony/CommandsInterface;->setFacilityLockForApp(Ljava/lang/String;ZLjava/lang/String;ILjava/lang/String;Landroid/os/Message;)V

    #@1a
    .line 858
    monitor-exit v7

    #@1b
    .line 859
    return-void

    #@1c
    .line 858
    :catchall_1c
    move-exception v0

    #@1d
    monitor-exit v7
    :try_end_1e
    .catchall {:try_start_4 .. :try_end_1e} :catchall_1c

    #@1e
    throw v0
.end method

.method public setInternalPIN(Ljava/lang/String;)V
    .registers 6
    .parameter "pin"

    #@0
    .prologue
    .line 992
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mContext:Landroid/content/Context;

    #@2
    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    #@5
    move-result-object v1

    #@6
    .line 993
    .local v1, sp:Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    #@9
    move-result-object v0

    #@a
    .line 994
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v2, "internal_pin_value_key"

    #@c
    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@f
    .line 995
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    #@12
    .line 996
    sget-boolean v2, Lcom/android/internal/telephony/uicc/UiccCardApplication;->ENABLE_PRIVACY_LOG:Z

    #@14
    if-eqz v2, :cond_2c

    #@16
    .line 998
    new-instance v2, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v3, "[LGE] setInternalPIN : "

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v2

    #@29
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->log(Ljava/lang/String;)V

    #@2c
    .line 1000
    :cond_2c
    return-void
.end method

.method public supplyDepersonalization(Ljava/lang/String;ILandroid/os/Message;)V
    .registers 7
    .parameter "pin"
    .parameter "type"
    .parameter "onComplete"

    #@0
    .prologue
    .line 785
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 786
    :try_start_3
    new-instance v0, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v2, "Network Despersonalization: pin = "

    #@a
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v0

    #@e
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v0

    #@12
    const-string v2, " , type = "

    #@14
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v0

    #@18
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v0

    #@1c
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v0

    #@20
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->log(Ljava/lang/String;)V

    #@23
    .line 787
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@25
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/CommandsInterface;->supplyDepersonalization(Ljava/lang/String;ILandroid/os/Message;)V

    #@28
    .line 788
    monitor-exit v1

    #@29
    .line 789
    return-void

    #@2a
    .line 788
    :catchall_2a
    move-exception v0

    #@2b
    monitor-exit v1
    :try_end_2c
    .catchall {:try_start_3 .. :try_end_2c} :catchall_2a

    #@2c
    throw v0
.end method

.method public supplyPin(Ljava/lang/String;Landroid/os/Message;)V
    .registers 8
    .parameter "pin"
    .parameter "onComplete"

    #@0
    .prologue
    .line 744
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 746
    :try_start_3
    iput-object p1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->internalPin:Ljava/lang/String;

    #@5
    .line 749
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@7
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mAid:Ljava/lang/String;

    #@9
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mHandler:Landroid/os/Handler;

    #@b
    const/4 v4, 0x1

    #@c
    invoke-virtual {v3, v4, p2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@f
    move-result-object v3

    #@10
    invoke-interface {v0, p1, v2, v3}, Lcom/android/internal/telephony/CommandsInterface;->supplyIccPinForApp(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    #@13
    .line 751
    monitor-exit v1

    #@14
    .line 752
    return-void

    #@15
    .line 751
    :catchall_15
    move-exception v0

    #@16
    monitor-exit v1
    :try_end_17
    .catchall {:try_start_3 .. :try_end_17} :catchall_15

    #@17
    throw v0
.end method

.method public supplyPin2(Ljava/lang/String;Landroid/os/Message;)V
    .registers 8
    .parameter "pin2"
    .parameter "onComplete"

    #@0
    .prologue
    .line 768
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 769
    :try_start_3
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@5
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mAid:Ljava/lang/String;

    #@7
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mHandler:Landroid/os/Handler;

    #@9
    const/16 v4, 0x8

    #@b
    invoke-virtual {v3, v4, p2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@e
    move-result-object v3

    #@f
    invoke-interface {v0, p1, v2, v3}, Lcom/android/internal/telephony/CommandsInterface;->supplyIccPin2ForApp(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    #@12
    .line 771
    monitor-exit v1

    #@13
    .line 772
    return-void

    #@14
    .line 771
    :catchall_14
    move-exception v0

    #@15
    monitor-exit v1
    :try_end_16
    .catchall {:try_start_3 .. :try_end_16} :catchall_14

    #@16
    throw v0
.end method

.method public supplyPuk(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 9
    .parameter "puk"
    .parameter "newPin"
    .parameter "onComplete"

    #@0
    .prologue
    .line 755
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 757
    const/4 v0, 0x1

    #@4
    :try_start_4
    iput-boolean v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->isPuk1:Z

    #@6
    .line 759
    iput-object p2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->internalPin:Ljava/lang/String;

    #@8
    .line 762
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@a
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mAid:Ljava/lang/String;

    #@c
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mHandler:Landroid/os/Handler;

    #@e
    const/4 v4, 0x1

    #@f
    invoke-virtual {v3, v4, p3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@12
    move-result-object v3

    #@13
    invoke-interface {v0, p1, p2, v2, v3}, Lcom/android/internal/telephony/CommandsInterface;->supplyIccPukForApp(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    #@16
    .line 764
    monitor-exit v1

    #@17
    .line 765
    return-void

    #@18
    .line 764
    :catchall_18
    move-exception v0

    #@19
    monitor-exit v1
    :try_end_1a
    .catchall {:try_start_4 .. :try_end_1a} :catchall_18

    #@1a
    throw v0
.end method

.method public supplyPuk2(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 9
    .parameter "puk2"
    .parameter "newPin2"
    .parameter "onComplete"

    #@0
    .prologue
    .line 775
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 777
    const/4 v0, 0x1

    #@4
    :try_start_4
    iput-boolean v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->isPuk2:Z

    #@6
    .line 779
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@8
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mAid:Ljava/lang/String;

    #@a
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mHandler:Landroid/os/Handler;

    #@c
    const/16 v4, 0x8

    #@e
    invoke-virtual {v3, v4, p3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@11
    move-result-object v3

    #@12
    invoke-interface {v0, p1, p2, v2, v3}, Lcom/android/internal/telephony/CommandsInterface;->supplyIccPuk2ForApp(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    #@15
    .line 781
    monitor-exit v1

    #@16
    .line 782
    return-void

    #@17
    .line 781
    :catchall_17
    move-exception v0

    #@18
    monitor-exit v1
    :try_end_19
    .catchall {:try_start_4 .. :try_end_19} :catchall_17

    #@19
    throw v0
.end method

.method public unregisterForLocked(Landroid/os/Handler;)V
    .registers 4
    .parameter "h"

    #@0
    .prologue
    .line 563
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 564
    :try_start_3
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPinLockedRegistrants:Landroid/os/RegistrantList;

    #@5
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@8
    .line 565
    monitor-exit v1

    #@9
    .line 566
    return-void

    #@a
    .line 565
    :catchall_a
    move-exception v0

    #@b
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method

.method public unregisterForPersoLocked(Landroid/os/Handler;)V
    .registers 4
    .parameter "h"

    #@0
    .prologue
    .line 580
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 581
    :try_start_3
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPersoLockedRegistrants:Landroid/os/RegistrantList;

    #@5
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@8
    .line 582
    monitor-exit v1

    #@9
    .line 583
    return-void

    #@a
    .line 582
    :catchall_a
    move-exception v0

    #@b
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method

.method public unregisterForReady(Landroid/os/Handler;)V
    .registers 4
    .parameter "h"

    #@0
    .prologue
    .line 546
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 547
    :try_start_3
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mReadyRegistrants:Landroid/os/RegistrantList;

    #@5
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@8
    .line 548
    monitor-exit v1

    #@9
    .line 549
    return-void

    #@a
    .line 548
    :catchall_a
    move-exception v0

    #@b
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method

.method update(Lcom/android/internal/telephony/uicc/IccCardApplicationStatus;Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;)V
    .registers 10
    .parameter "as"
    .parameter "c"
    .parameter "ci"

    #@0
    .prologue
    .line 148
    iget-object v4, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v4

    #@3
    .line 149
    :try_start_3
    iget-boolean v3, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mDestroyed:Z

    #@5
    if-eqz v3, :cond_e

    #@7
    .line 150
    const-string v3, "Application updated after destroyed! Fix me!"

    #@9
    invoke-direct {p0, v3}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->loge(Ljava/lang/String;)V

    #@c
    .line 151
    monitor-exit v4

    #@d
    .line 209
    :goto_d
    return-void

    #@e
    .line 154
    :cond_e
    new-instance v3, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    iget-object v5, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mAppType:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;

    #@15
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    const-string v5, " update. New "

    #@1b
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v3

    #@27
    invoke-direct {p0, v3}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->log(Ljava/lang/String;)V

    #@2a
    .line 155
    iput-object p2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mContext:Landroid/content/Context;

    #@2c
    .line 156
    iput-object p3, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@2e
    .line 157
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mAppType:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;

    #@30
    .line 158
    .local v1, oldAppType:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mAppState:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;

    #@32
    .line 159
    .local v0, oldAppState:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPersoSubState:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;

    #@34
    .line 160
    .local v2, oldPersoSubState:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;
    iget-object v3, p1, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus;->app_type:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;

    #@36
    iput-object v3, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mAppType:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;

    #@38
    .line 161
    iget-object v3, p1, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus;->app_state:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;

    #@3a
    iput-object v3, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mAppState:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;

    #@3c
    .line 162
    iget-object v3, p1, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus;->perso_substate:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;

    #@3e
    iput-object v3, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPersoSubState:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;

    #@40
    .line 163
    iget-object v3, p1, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus;->aid:Ljava/lang/String;

    #@42
    iput-object v3, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mAid:Ljava/lang/String;

    #@44
    .line 164
    iget-object v3, p1, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus;->app_label:Ljava/lang/String;

    #@46
    iput-object v3, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mAppLabel:Ljava/lang/String;

    #@48
    .line 165
    iget v3, p1, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus;->pin1_replaced:I

    #@4a
    if-eqz v3, :cond_f1

    #@4c
    const/4 v3, 0x1

    #@4d
    :goto_4d
    iput-boolean v3, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPin1Replaced:Z

    #@4f
    .line 166
    iget-object v3, p1, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus;->pin1:Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;

    #@51
    iput-object v3, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPin1State:Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;

    #@53
    .line 167
    iget-object v3, p1, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus;->pin2:Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;

    #@55
    iput-object v3, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPin2State:Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;

    #@57
    .line 169
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mAppType:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;

    #@59
    if-eq v3, v1, :cond_7d

    #@5b
    .line 170
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mIccFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@5d
    if-eqz v3, :cond_64

    #@5f
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mIccFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@61
    invoke-virtual {v3}, Lcom/android/internal/telephony/uicc/IccFileHandler;->dispose()V

    #@64
    .line 171
    :cond_64
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@66
    if-eqz v3, :cond_6d

    #@68
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@6a
    invoke-virtual {v3}, Lcom/android/internal/telephony/uicc/IccRecords;->dispose()V

    #@6d
    .line 172
    :cond_6d
    iget-object v3, p1, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus;->app_type:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;

    #@6f
    invoke-direct {p0, v3}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->createIccFileHandler(Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;)Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@72
    move-result-object v3

    #@73
    iput-object v3, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mIccFh:Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@75
    .line 173
    iget-object v3, p1, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus;->app_type:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;

    #@77
    invoke-direct {p0, v3, p2, p3}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->createIccRecords(Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;)Lcom/android/internal/telephony/uicc/IccRecords;

    #@7a
    move-result-object v3

    #@7b
    iput-object v3, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@7d
    .line 176
    :cond_7d
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPersoSubState:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;

    #@7f
    if-eq v3, v2, :cond_8b

    #@81
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->isPersoLocked()Z

    #@84
    move-result v3

    #@85
    if-eqz v3, :cond_8b

    #@87
    .line 178
    const/4 v3, 0x0

    #@88
    invoke-direct {p0, v3}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->notifyPersoLockedRegistrantsIfNeeded(Landroid/os/Registrant;)V

    #@8b
    .line 185
    :cond_8b
    iget-object v3, p1, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus;->app_state:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;

    #@8d
    if-ne v3, v0, :cond_95

    #@8f
    iget-object v3, p1, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus;->pin1:Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;

    #@91
    sget-object v5, Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;->PINSTATE_ENABLED_PERM_BLOCKED:Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;

    #@93
    if-ne v3, v5, :cond_cf

    #@95
    .line 187
    :cond_95
    new-instance v3, Ljava/lang/StringBuilder;

    #@97
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@9a
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v3

    #@9e
    const-string v5, " changed state: "

    #@a0
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v3

    #@a4
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v3

    #@a8
    const-string v5, " -> "

    #@aa
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v3

    #@ae
    iget-object v5, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mAppState:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;

    #@b0
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@b3
    move-result-object v3

    #@b4
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b7
    move-result-object v3

    #@b8
    invoke-direct {p0, v3}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->log(Ljava/lang/String;)V

    #@bb
    .line 190
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mAppState:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;

    #@bd
    sget-object v5, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;->APPSTATE_READY:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;

    #@bf
    if-ne v3, v5, :cond_c7

    #@c1
    .line 191
    invoke-direct {p0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->queryFdn()V

    #@c4
    .line 192
    invoke-direct {p0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->queryPin1State()V

    #@c7
    .line 194
    :cond_c7
    const/4 v3, 0x0

    #@c8
    invoke-direct {p0, v3}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->notifyPinLockedRegistrantsIfNeeded(Landroid/os/Registrant;)V

    #@cb
    .line 195
    const/4 v3, 0x0

    #@cc
    invoke-direct {p0, v3}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->notifyReadyRegistrantsIfNeeded(Landroid/os/Registrant;)V

    #@cf
    .line 198
    :cond_cf
    iget v3, p1, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus;->remaining_count_pin1:I

    #@d1
    iput v3, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPin1RetryCount:I

    #@d3
    .line 199
    iget v3, p1, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus;->remaining_count_puk1:I

    #@d5
    iput v3, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPuk1RetryCount:I

    #@d7
    .line 200
    iget v3, p1, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus;->remaining_count_pin2:I

    #@d9
    iput v3, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPin2RetryCount:I

    #@db
    .line 201
    iget v3, p1, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus;->remaining_count_puk2:I

    #@dd
    iput v3, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPuk2RetryCount:I

    #@df
    .line 203
    sget-object v3, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;->APPSTATE_READY:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;

    #@e1
    if-eq v0, v3, :cond_eb

    #@e3
    .line 204
    iget v3, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPin1RetryCount:I

    #@e5
    iput v3, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPin1InitRetryCount:I

    #@e7
    .line 205
    iget v3, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPin2RetryCount:I

    #@e9
    iput v3, p0, Lcom/android/internal/telephony/uicc/UiccCardApplication;->mPin2InitRetryCount:I

    #@eb
    .line 208
    :cond_eb
    monitor-exit v4

    #@ec
    goto/16 :goto_d

    #@ee
    .end local v0           #oldAppState:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;
    .end local v1           #oldAppType:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;
    .end local v2           #oldPersoSubState:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;
    :catchall_ee
    move-exception v3

    #@ef
    monitor-exit v4
    :try_end_f0
    .catchall {:try_start_3 .. :try_end_f0} :catchall_ee

    #@f0
    throw v3

    #@f1
    .line 165
    .restart local v0       #oldAppState:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;
    .restart local v1       #oldAppType:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;
    .restart local v2       #oldPersoSubState:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;
    :cond_f1
    const/4 v3, 0x0

    #@f2
    goto/16 :goto_4d
.end method
