.class public Lcom/android/internal/telephony/gsm/GsmSmsAddress;
.super Lcom/android/internal/telephony/SmsAddress;
.source "GsmSmsAddress.java"


# static fields
.field static final OFFSET_ADDRESS_LENGTH:I = 0x0

.field static final OFFSET_ADDRESS_VALUE:I = 0x2

.field static final OFFSET_TOA:I = 0x1


# direct methods
.method public constructor <init>([BII)V
    .registers 13
    .parameter "data"
    .parameter "offset"
    .parameter "length"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v8, 0x1

    #@2
    .line 46
    invoke-direct {p0}, Lcom/android/internal/telephony/SmsAddress;-><init>()V

    #@5
    .line 47
    new-array v5, p3, [B

    #@7
    iput-object v5, p0, Lcom/android/internal/telephony/SmsAddress;->origBytes:[B

    #@9
    .line 48
    iget-object v5, p0, Lcom/android/internal/telephony/SmsAddress;->origBytes:[B

    #@b
    invoke-static {p1, p2, v5, v6, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@e
    .line 51
    iget-object v5, p0, Lcom/android/internal/telephony/SmsAddress;->origBytes:[B

    #@10
    aget-byte v5, v5, v6

    #@12
    and-int/lit16 v1, v5, 0xff

    #@14
    .line 53
    .local v1, addressLength:I
    iget-object v5, p0, Lcom/android/internal/telephony/SmsAddress;->origBytes:[B

    #@16
    aget-byte v5, v5, v8

    #@18
    and-int/lit16 v4, v5, 0xff

    #@1a
    .line 54
    .local v4, toa:I
    shr-int/lit8 v5, v4, 0x4

    #@1c
    and-int/lit8 v5, v5, 0x7

    #@1e
    iput v5, p0, Lcom/android/internal/telephony/SmsAddress;->ton:I

    #@20
    .line 57
    and-int/lit16 v5, v4, 0x80

    #@22
    const/16 v6, 0x80

    #@24
    if-eq v5, v6, :cond_41

    #@26
    .line 58
    new-instance v5, Ljava/text/ParseException;

    #@28
    new-instance v6, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v7, "Invalid TOA - high bit must be set. toa = "

    #@2f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v6

    #@33
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@36
    move-result-object v6

    #@37
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v6

    #@3b
    add-int/lit8 v7, p2, 0x1

    #@3d
    invoke-direct {v5, v6, v7}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    #@40
    throw v5

    #@41
    .line 62
    :cond_41
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmSmsAddress;->isAlphanumeric()Z

    #@44
    move-result v5

    #@45
    if-eqz v5, :cond_55

    #@47
    .line 64
    mul-int/lit8 v5, v1, 0x4

    #@49
    div-int/lit8 v2, v5, 0x7

    #@4b
    .line 66
    .local v2, countSeptets:I
    iget-object v5, p0, Lcom/android/internal/telephony/SmsAddress;->origBytes:[B

    #@4d
    const/4 v6, 0x2

    #@4e
    invoke-static {v5, v6, v2}, Lcom/android/internal/telephony/GsmAlphabet;->gsm7BitPackedToString([BII)Ljava/lang/String;

    #@51
    move-result-object v5

    #@52
    iput-object v5, p0, Lcom/android/internal/telephony/SmsAddress;->address:Ljava/lang/String;

    #@54
    .line 99
    .end local v2           #countSeptets:I
    :goto_54
    return-void

    #@55
    .line 73
    :cond_55
    iget-object v5, p0, Lcom/android/internal/telephony/SmsAddress;->origBytes:[B

    #@57
    add-int/lit8 v6, p3, -0x1

    #@59
    aget-byte v3, v5, v6

    #@5b
    .line 75
    .local v3, lastByte:B
    and-int/lit8 v5, v1, 0x1

    #@5d
    if-ne v5, v8, :cond_6a

    #@5f
    .line 77
    iget-object v5, p0, Lcom/android/internal/telephony/SmsAddress;->origBytes:[B

    #@61
    add-int/lit8 v6, p3, -0x1

    #@63
    aget-byte v7, v5, v6

    #@65
    or-int/lit16 v7, v7, 0xf0

    #@67
    int-to-byte v7, v7

    #@68
    aput-byte v7, v5, v6

    #@6a
    .line 81
    :cond_6a
    const/4 v5, 0x0

    #@6b
    const-string v6, "BCDaddedABCandWild"

    #@6d
    invoke-static {v5, v6}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@70
    move-result v5

    #@71
    if-ne v5, v8, :cond_99

    #@73
    .line 82
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@76
    move-result-object v5

    #@77
    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    #@7a
    move-result v0

    #@7b
    .line 84
    .local v0, activePhone:I
    if-ne v8, v0, :cond_8e

    #@7d
    .line 85
    iget-object v5, p0, Lcom/android/internal/telephony/SmsAddress;->origBytes:[B

    #@7f
    add-int/lit8 v6, p3, -0x1

    #@81
    invoke-static {v5, v8, v6}, Landroid/telephony/PhoneNumberUtils;->KRsmscalledPartyBCDToString([BII)Ljava/lang/String;

    #@84
    move-result-object v5

    #@85
    iput-object v5, p0, Lcom/android/internal/telephony/SmsAddress;->address:Ljava/lang/String;

    #@87
    .line 97
    .end local v0           #activePhone:I
    :goto_87
    iget-object v5, p0, Lcom/android/internal/telephony/SmsAddress;->origBytes:[B

    #@89
    add-int/lit8 v6, p3, -0x1

    #@8b
    aput-byte v3, v5, v6

    #@8d
    goto :goto_54

    #@8e
    .line 88
    .restart local v0       #activePhone:I
    :cond_8e
    iget-object v5, p0, Lcom/android/internal/telephony/SmsAddress;->origBytes:[B

    #@90
    add-int/lit8 v6, p3, -0x1

    #@92
    invoke-static {v5, v8, v6}, Landroid/telephony/PhoneNumberUtils;->calledPartyBCDToString([BII)Ljava/lang/String;

    #@95
    move-result-object v5

    #@96
    iput-object v5, p0, Lcom/android/internal/telephony/SmsAddress;->address:Ljava/lang/String;

    #@98
    goto :goto_87

    #@99
    .line 93
    .end local v0           #activePhone:I
    :cond_99
    iget-object v5, p0, Lcom/android/internal/telephony/SmsAddress;->origBytes:[B

    #@9b
    add-int/lit8 v6, p3, -0x1

    #@9d
    invoke-static {v5, v8, v6}, Landroid/telephony/PhoneNumberUtils;->calledPartyBCDToString([BII)Ljava/lang/String;

    #@a0
    move-result-object v5

    #@a1
    iput-object v5, p0, Lcom/android/internal/telephony/SmsAddress;->address:Ljava/lang/String;

    #@a3
    goto :goto_87
.end method


# virtual methods
.method public getAddressString()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 102
    iget-object v0, p0, Lcom/android/internal/telephony/SmsAddress;->address:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public isAlphanumeric()Z
    .registers 3

    #@0
    .prologue
    .line 109
    iget v0, p0, Lcom/android/internal/telephony/SmsAddress;->ton:I

    #@2
    const/4 v1, 0x5

    #@3
    if-ne v0, v1, :cond_7

    #@5
    const/4 v0, 0x1

    #@6
    :goto_6
    return v0

    #@7
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method

.method public isCphsVoiceMessageClear()Z
    .registers 3

    #@0
    .prologue
    .line 165
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmSmsAddress;->isCphsVoiceMessageIndicatorAddress()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_13

    #@6
    iget-object v0, p0, Lcom/android/internal/telephony/SmsAddress;->origBytes:[B

    #@8
    const/4 v1, 0x2

    #@9
    aget-byte v0, v0, v1

    #@b
    and-int/lit16 v0, v0, 0xff

    #@d
    const/16 v1, 0x10

    #@f
    if-ne v0, v1, :cond_13

    #@11
    const/4 v0, 0x1

    #@12
    :goto_12
    return v0

    #@13
    :cond_13
    const/4 v0, 0x0

    #@14
    goto :goto_12
.end method

.method public isCphsVoiceMessageIndicatorAddress()Z
    .registers 5

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 142
    iget-object v2, p0, Lcom/android/internal/telephony/SmsAddress;->origBytes:[B

    #@4
    aget-byte v2, v2, v1

    #@6
    and-int/lit16 v2, v2, 0xff

    #@8
    const/4 v3, 0x4

    #@9
    if-ne v2, v3, :cond_1a

    #@b
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmSmsAddress;->isAlphanumeric()Z

    #@e
    move-result v2

    #@f
    if-eqz v2, :cond_1a

    #@11
    iget-object v2, p0, Lcom/android/internal/telephony/SmsAddress;->origBytes:[B

    #@13
    aget-byte v2, v2, v0

    #@15
    and-int/lit8 v2, v2, 0xf

    #@17
    if-nez v2, :cond_1a

    #@19
    :goto_19
    return v0

    #@1a
    :cond_1a
    move v0, v1

    #@1b
    goto :goto_19
.end method

.method public isCphsVoiceMessageSet()Z
    .registers 3

    #@0
    .prologue
    .line 153
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/GsmSmsAddress;->isCphsVoiceMessageIndicatorAddress()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_13

    #@6
    iget-object v0, p0, Lcom/android/internal/telephony/SmsAddress;->origBytes:[B

    #@8
    const/4 v1, 0x2

    #@9
    aget-byte v0, v0, v1

    #@b
    and-int/lit16 v0, v0, 0xff

    #@d
    const/16 v1, 0x11

    #@f
    if-ne v0, v1, :cond_13

    #@11
    const/4 v0, 0x1

    #@12
    :goto_12
    return v0

    #@13
    :cond_13
    const/4 v0, 0x0

    #@14
    goto :goto_12
.end method

.method public isNetworkSpecific()Z
    .registers 3

    #@0
    .prologue
    .line 113
    iget v0, p0, Lcom/android/internal/telephony/SmsAddress;->ton:I

    #@2
    const/4 v1, 0x3

    #@3
    if-ne v0, v1, :cond_7

    #@5
    const/4 v0, 0x1

    #@6
    :goto_6
    return v0

    #@7
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method
