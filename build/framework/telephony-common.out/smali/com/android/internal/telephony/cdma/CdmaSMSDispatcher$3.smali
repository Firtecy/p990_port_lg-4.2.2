.class Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher$3;
.super Landroid/content/BroadcastReceiver;
.source "CdmaSMSDispatcher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;


# direct methods
.method constructor <init>(Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 2581
    iput-object p1, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher$3;->this$0:Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 7
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/4 v3, -0x1

    #@1
    .line 2584
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@4
    move-result-object v1

    #@5
    const-string v2, "com.lge.kddi.intent.action.DAN_SENT"

    #@7
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a
    move-result v1

    #@b
    if-eqz v1, :cond_7b

    #@d
    .line 2585
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher$3;->this$0:Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;

    #@f
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher$3;->getResultCode()I

    #@12
    move-result v2

    #@13
    invoke-static {v1, v2}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->access$202(Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;I)I

    #@16
    .line 2586
    new-instance v1, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v2, "cdmaDanSentReceiver, [KDDI][DAN] DAN Send Complete? ResultCode = "

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher$3;->this$0:Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;

    #@23
    invoke-static {v2}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->access$200(Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;)I

    #@26
    move-result v2

    #@27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v1

    #@2b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v1

    #@2f
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@32
    .line 2587
    new-instance v1, Ljava/lang/StringBuilder;

    #@34
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@37
    const-string v2, "cdmaDanSentReceiver, [KDDI][DAN] errorCode = "

    #@39
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v1

    #@3d
    const-string v2, "errorCode"

    #@3f
    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@42
    move-result v2

    #@43
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@46
    move-result-object v1

    #@47
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v1

    #@4b
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@4e
    .line 2588
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher$3;->this$0:Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;

    #@50
    invoke-static {v1}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->access$200(Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;)I

    #@53
    move-result v1

    #@54
    if-eq v1, v3, :cond_7b

    #@56
    .line 2590
    new-instance v0, Landroid/content/Intent;

    #@58
    const-string v1, "com.lge.kddi.intent.action.DAN_FAIL_RETRY"

    #@5a
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@5d
    .line 2591
    .local v0, mIntent:Landroid/content/Intent;
    new-instance v1, Ljava/lang/StringBuilder;

    #@5f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@62
    const-string v2, "cdmaDanSentReceiver, [KDDI][Domain Notification] DAN Send FAIL, send Intent : "

    #@64
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v1

    #@68
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@6b
    move-result-object v2

    #@6c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v1

    #@70
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@73
    move-result-object v1

    #@74
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@77
    .line 2592
    const/4 v1, 0x0

    #@78
    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    #@7b
    .line 2595
    .end local v0           #mIntent:Landroid/content/Intent;
    :cond_7b
    return-void
.end method
