.class public Lcom/android/internal/telephony/gfit/GfitUtils$PlmnList;
.super Ljava/lang/Object;
.source "GfitUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/gfit/GfitUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "PlmnList"
.end annotation


# instance fields
.field private RAT:Ljava/lang/String;

.field private nMCC:I

.field private nMNC:I

.field private operatorAlphaLong:Ljava/lang/String;

.field private operatorNumeric:Ljava/lang/String;

.field final synthetic this$0:Lcom/android/internal/telephony/gfit/GfitUtils;


# direct methods
.method constructor <init>(Lcom/android/internal/telephony/gfit/GfitUtils;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 188
    iput-object p1, p0, Lcom/android/internal/telephony/gfit/GfitUtils$PlmnList;->this$0:Lcom/android/internal/telephony/gfit/GfitUtils;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method getMCC()I
    .registers 2

    #@0
    .prologue
    .line 235
    iget v0, p0, Lcom/android/internal/telephony/gfit/GfitUtils$PlmnList;->nMCC:I

    #@2
    return v0
.end method

.method getMNC()I
    .registers 2

    #@0
    .prologue
    .line 234
    iget v0, p0, Lcom/android/internal/telephony/gfit/GfitUtils$PlmnList;->nMNC:I

    #@2
    return v0
.end method

.method getOperatorAlphaLong()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 238
    iget-object v0, p0, Lcom/android/internal/telephony/gfit/GfitUtils$PlmnList;->operatorAlphaLong:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method getOperatorNumeric()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 237
    iget-object v0, p0, Lcom/android/internal/telephony/gfit/GfitUtils$PlmnList;->operatorNumeric:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method getRAT()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 236
    iget-object v0, p0, Lcom/android/internal/telephony/gfit/GfitUtils$PlmnList;->RAT:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method setMCC(I)V
    .registers 2
    .parameter "nMCC"

    #@0
    .prologue
    .line 191
    iput p1, p0, Lcom/android/internal/telephony/gfit/GfitUtils$PlmnList;->nMCC:I

    #@2
    return-void
.end method

.method setMNC(I)V
    .registers 2
    .parameter "nMNC"

    #@0
    .prologue
    .line 190
    iput p1, p0, Lcom/android/internal/telephony/gfit/GfitUtils$PlmnList;->nMNC:I

    #@2
    return-void
.end method

.method setOperatorAlphaLong(Ljava/lang/String;)V
    .registers 2
    .parameter "operatorAlphaLong"

    #@0
    .prologue
    .line 232
    iput-object p1, p0, Lcom/android/internal/telephony/gfit/GfitUtils$PlmnList;->operatorAlphaLong:Ljava/lang/String;

    #@2
    return-void
.end method

.method setOperatorNumeric(Ljava/lang/String;)V
    .registers 2
    .parameter "operatorNumeric"

    #@0
    .prologue
    .line 231
    iput-object p1, p0, Lcom/android/internal/telephony/gfit/GfitUtils$PlmnList;->operatorNumeric:Ljava/lang/String;

    #@2
    return-void
.end method

.method setRAT(Ljava/lang/String;)V
    .registers 6
    .parameter "RAT"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 195
    const/4 v0, 0x0

    #@2
    .line 197
    .local v0, tempRAT:Ljava/lang/String;
    if-eqz p1, :cond_1d

    #@4
    .line 198
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    #@7
    move-result v1

    #@8
    const/16 v2, 0x57

    #@a
    if-eq v1, v2, :cond_1c

    #@c
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    #@f
    move-result v1

    #@10
    const/16 v2, 0x47

    #@12
    if-eq v1, v2, :cond_1c

    #@14
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    #@17
    move-result v1

    #@18
    const/16 v2, 0x4c

    #@1a
    if-ne v1, v2, :cond_20

    #@1c
    .line 199
    :cond_1c
    move-object v0, p1

    #@1d
    .line 228
    :cond_1d
    :goto_1d
    iput-object v0, p0, Lcom/android/internal/telephony/gfit/GfitUtils$PlmnList;->RAT:Ljava/lang/String;

    #@1f
    .line 229
    return-void

    #@20
    .line 202
    :cond_20
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@23
    move-result v1

    #@24
    packed-switch v1, :pswitch_data_46

    #@27
    :pswitch_27
    goto :goto_1d

    #@28
    .line 206
    :pswitch_28
    iget-object v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils$PlmnList;->this$0:Lcom/android/internal/telephony/gfit/GfitUtils;

    #@2a
    const-string v2, "setRAT - GSM"

    #@2c
    invoke-static {v1, v2}, Lcom/android/internal/telephony/gfit/GfitUtils;->access$000(Lcom/android/internal/telephony/gfit/GfitUtils;Ljava/lang/String;)V

    #@2f
    .line 207
    const-string v0, "GSM"

    #@31
    .line 208
    goto :goto_1d

    #@32
    .line 215
    :pswitch_32
    iget-object v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils$PlmnList;->this$0:Lcom/android/internal/telephony/gfit/GfitUtils;

    #@34
    const-string v2, "setRAT - WCDMA"

    #@36
    invoke-static {v1, v2}, Lcom/android/internal/telephony/gfit/GfitUtils;->access$000(Lcom/android/internal/telephony/gfit/GfitUtils;Ljava/lang/String;)V

    #@39
    .line 216
    const-string v0, "WCDMA"

    #@3b
    .line 217
    goto :goto_1d

    #@3c
    .line 220
    :pswitch_3c
    iget-object v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils$PlmnList;->this$0:Lcom/android/internal/telephony/gfit/GfitUtils;

    #@3e
    const-string v2, "setRAT - LTE"

    #@40
    invoke-static {v1, v2}, Lcom/android/internal/telephony/gfit/GfitUtils;->access$000(Lcom/android/internal/telephony/gfit/GfitUtils;Ljava/lang/String;)V

    #@43
    .line 221
    const-string v0, "LTE"

    #@45
    .line 222
    goto :goto_1d

    #@46
    .line 202
    :pswitch_data_46
    .packed-switch 0x1
        :pswitch_28
        :pswitch_28
        :pswitch_32
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_27
        :pswitch_27
        :pswitch_3c
        :pswitch_32
        :pswitch_28
    .end packed-switch
.end method
