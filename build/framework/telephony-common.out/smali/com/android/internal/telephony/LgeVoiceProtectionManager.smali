.class public Lcom/android/internal/telephony/LgeVoiceProtectionManager;
.super Lcom/android/internal/util/StateMachine;
.source "LgeVoiceProtectionManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/LgeVoiceProtectionManager$ActiveState;,
        Lcom/android/internal/telephony/LgeVoiceProtectionManager$IdleState;,
        Lcom/android/internal/telephony/LgeVoiceProtectionManager$BaseState;
    }
.end annotation


# static fields
.field private static final DATA_CONNECTION_ACTIVE_PH_LINK_DOWN:I = 0x1

.field private static final DATA_CONNECTION_ACTIVE_PH_LINK_INACTIVE:I = 0x0

.field private static final DATA_CONNECTION_ACTIVE_PH_LINK_UP:I = 0x2

.field protected static final EVENT_DATA_STATE_CHANGED:I = 0x42004

.field protected static final EVENT_ICC_CHANGED:I = 0x42021

.field private static final EVENT_INTERNAL_DEFINED:I = 0x25ce9

.field private static final EVENT_OEM_RIL_MESSAGE_GO_DORMANT_RESPONSE:I = 0x25cec

.field protected static final EVENT_RECORDS_LOADED:I = 0x42002

.field private static final EVENT_UPDATE_VP_STATUS:I = 0x25ceb

.field protected static final EVENT_VOICE_CALL_ENDED:I = 0x42008

.field protected static final EVENT_VOICE_CALL_STARTED:I = 0x42007

.field private static final GO_DORMANT_INTERFACE_ALL:Ljava/lang/String; = ""

.field private static final INTENT_SIGNAL_REPORT:Ljava/lang/String; = "com.lge.internal.telephony.SIGNAL_REPORT"

.field private static final LOG_TAG:Ljava/lang/String; = "VPMANAGER"

.field private static final QCRILHOOK_CLASS:Ljava/lang/String; = "com.qualcomm.qcrilhook.QcRilHook"

.field private static final VP_STATUS_IS_CALLING:I = 0x2

.field private static final VP_STATUS_IS_PHYLINK_UP:I = 0x4

.field private static final VP_STATUS_IS_POOR_SIGNAL:I = 0x10

.field private static final VP_STATUS_IS_SCREEN_ON:I = 0x8

.field private static final VP_STATUS_IS_TETHERING:I = 0x20

.field private static final VP_STATUS_IS_UMTS:I = 0x1


# instance fields
.field private mActiveState:Lcom/android/internal/telephony/LgeVoiceProtectionManager$ActiveState;

.field mCm:Lcom/android/internal/telephony/CommandsInterface;

.field mContext:Landroid/content/Context;

.field mCt:Lcom/android/internal/telephony/CallTracker;

.field mHandler:Landroid/os/Handler;

.field protected mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

.field private mIdleState:Lcom/android/internal/telephony/LgeVoiceProtectionManager$IdleState;

.field mIntentReceiver:Landroid/content/BroadcastReceiver;

.field mIsReady:Z

.field mNetManageService:Landroid/os/INetworkManagementService;

.field mP:Lcom/android/internal/telephony/PhoneBase;

.field private mQcRilGoDormantFunc:Ljava/lang/reflect/Method;

.field private mQcRilHookObject:Ljava/lang/Object;

.field protected mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

.field private mVpStatus:I

.field pm:Landroid/os/PowerManager;

.field sWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;Lcom/android/internal/telephony/PhoneBase;)V
    .registers 16
    .parameter "context"
    .parameter "ci"
    .parameter "p"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v11, 0x1

    #@2
    const/4 v10, 0x0

    #@3
    .line 156
    const-string v6, "LgeVoiceProtectionManager"

    #@5
    invoke-direct {p0, v6}, Lcom/android/internal/util/StateMachine;-><init>(Ljava/lang/String;)V

    #@8
    .line 91
    iput-boolean v7, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mIsReady:Z

    #@a
    .line 96
    iput-object v10, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mHandler:Landroid/os/Handler;

    #@c
    .line 100
    iput-object v10, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mQcRilHookObject:Ljava/lang/Object;

    #@e
    .line 101
    iput-object v10, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mQcRilGoDormantFunc:Ljava/lang/reflect/Method;

    #@10
    .line 103
    const/16 v6, 0x8

    #@12
    iput v6, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mVpStatus:I

    #@14
    .line 112
    new-instance v6, Lcom/android/internal/telephony/LgeVoiceProtectionManager$1;

    #@16
    invoke-direct {v6, p0}, Lcom/android/internal/telephony/LgeVoiceProtectionManager$1;-><init>(Lcom/android/internal/telephony/LgeVoiceProtectionManager;)V

    #@19
    iput-object v6, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@1b
    .line 346
    new-instance v6, Lcom/android/internal/telephony/LgeVoiceProtectionManager$IdleState;

    #@1d
    invoke-direct {v6, p0, v10}, Lcom/android/internal/telephony/LgeVoiceProtectionManager$IdleState;-><init>(Lcom/android/internal/telephony/LgeVoiceProtectionManager;Lcom/android/internal/telephony/LgeVoiceProtectionManager$1;)V

    #@20
    iput-object v6, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mIdleState:Lcom/android/internal/telephony/LgeVoiceProtectionManager$IdleState;

    #@22
    .line 439
    new-instance v6, Lcom/android/internal/telephony/LgeVoiceProtectionManager$ActiveState;

    #@24
    invoke-direct {v6, p0, v10}, Lcom/android/internal/telephony/LgeVoiceProtectionManager$ActiveState;-><init>(Lcom/android/internal/telephony/LgeVoiceProtectionManager;Lcom/android/internal/telephony/LgeVoiceProtectionManager$1;)V

    #@27
    iput-object v6, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mActiveState:Lcom/android/internal/telephony/LgeVoiceProtectionManager$ActiveState;

    #@29
    .line 158
    iput-object p1, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mContext:Landroid/content/Context;

    #@2b
    .line 159
    iput-object p2, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@2d
    .line 160
    iput-object p3, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mP:Lcom/android/internal/telephony/PhoneBase;

    #@2f
    .line 161
    iget-object v6, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mP:Lcom/android/internal/telephony/PhoneBase;

    #@31
    if-eqz v6, :cond_3b

    #@33
    iget-object v6, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mP:Lcom/android/internal/telephony/PhoneBase;

    #@35
    invoke-virtual {v6}, Lcom/android/internal/telephony/PhoneBase;->getCallTracker()Lcom/android/internal/telephony/CallTracker;

    #@38
    move-result-object v6

    #@39
    iput-object v6, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mCt:Lcom/android/internal/telephony/CallTracker;

    #@3b
    .line 163
    :cond_3b
    iget-object v6, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mContext:Landroid/content/Context;

    #@3d
    const-string v7, "power"

    #@3f
    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@42
    move-result-object v6

    #@43
    check-cast v6, Landroid/os/PowerManager;

    #@45
    iput-object v6, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->pm:Landroid/os/PowerManager;

    #@47
    .line 164
    iget-object v6, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->pm:Landroid/os/PowerManager;

    #@49
    const-string v7, "VPManager"

    #@4b
    invoke-virtual {v6, v11, v7}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@4e
    move-result-object v6

    #@4f
    iput-object v6, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    #@51
    .line 166
    const-string v6, "network_management"

    #@53
    invoke-static {v6}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@56
    move-result-object v0

    #@57
    .line 167
    .local v0, b:Landroid/os/IBinder;
    invoke-static {v0}, Landroid/os/INetworkManagementService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/INetworkManagementService;

    #@5a
    move-result-object v6

    #@5b
    iput-object v6, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mNetManageService:Landroid/os/INetworkManagementService;

    #@5d
    .line 169
    iget-object v6, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mIdleState:Lcom/android/internal/telephony/LgeVoiceProtectionManager$IdleState;

    #@5f
    invoke-virtual {p0, v6}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->addState(Lcom/android/internal/util/State;)V

    #@62
    .line 170
    iget-object v6, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mActiveState:Lcom/android/internal/telephony/LgeVoiceProtectionManager$ActiveState;

    #@64
    invoke-virtual {p0, v6}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->addState(Lcom/android/internal/util/State;)V

    #@67
    .line 171
    iget-object v6, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mIdleState:Lcom/android/internal/telephony/LgeVoiceProtectionManager$IdleState;

    #@69
    invoke-virtual {p0, v6}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->setInitialState(Lcom/android/internal/util/State;)V

    #@6c
    .line 173
    invoke-virtual {p0}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->getHandler()Landroid/os/Handler;

    #@6f
    move-result-object v6

    #@70
    iput-object v6, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mHandler:Landroid/os/Handler;

    #@72
    .line 176
    :try_start_72
    new-instance v5, Ldalvik/system/PathClassLoader;

    #@74
    const-string v6, "/system/framework/qcrilhook.jar"

    #@76
    invoke-static {}, Ljava/lang/ClassLoader;->getSystemClassLoader()Ljava/lang/ClassLoader;

    #@79
    move-result-object v7

    #@7a
    invoke-direct {v5, v6, v7}, Ldalvik/system/PathClassLoader;-><init>(Ljava/lang/String;Ljava/lang/ClassLoader;)V

    #@7d
    .line 178
    .local v5, qcRilHookClassLoader:Ldalvik/system/PathClassLoader;
    const-string v6, "com.qualcomm.qcrilhook.QcRilHook"

    #@7f
    invoke-virtual {v5, v6}, Ldalvik/system/PathClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    #@82
    move-result-object v2

    #@83
    .line 179
    .local v2, cl:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    const/4 v6, 0x1

    #@84
    new-array v6, v6, [Ljava/lang/Class;

    #@86
    const/4 v7, 0x0

    #@87
    const-class v8, Landroid/content/Context;

    #@89
    aput-object v8, v6, v7

    #@8b
    invoke-virtual {v2, v6}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    #@8e
    move-result-object v1

    #@8f
    .line 180
    .local v1, c:Ljava/lang/reflect/Constructor;
    const/4 v6, 0x1

    #@90
    new-array v6, v6, [Ljava/lang/Object;

    #@92
    const/4 v7, 0x0

    #@93
    iget-object v8, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mContext:Landroid/content/Context;

    #@95
    aput-object v8, v6, v7

    #@97
    invoke-virtual {v1, v6}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    #@9a
    move-result-object v6

    #@9b
    iput-object v6, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mQcRilHookObject:Ljava/lang/Object;

    #@9d
    .line 181
    const-string v6, "qcRilGoDormant"

    #@9f
    const/4 v7, 0x1

    #@a0
    new-array v7, v7, [Ljava/lang/Class;

    #@a2
    const/4 v8, 0x0

    #@a3
    const-class v9, Ljava/lang/String;

    #@a5
    aput-object v9, v7, v8

    #@a7
    invoke-virtual {v2, v6, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@aa
    move-result-object v6

    #@ab
    iput-object v6, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mQcRilGoDormantFunc:Ljava/lang/reflect/Method;
    :try_end_ad
    .catch Ljava/lang/Exception; {:try_start_72 .. :try_end_ad} :catch_111

    #@ad
    .line 188
    .end local v1           #c:Ljava/lang/reflect/Constructor;
    .end local v2           #cl:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    .end local v5           #qcRilHookClassLoader:Ldalvik/system/PathClassLoader;
    :goto_ad
    invoke-virtual {p0}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->start()V

    #@b0
    .line 189
    iput-boolean v11, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mIsReady:Z

    #@b2
    .line 192
    iget-object v6, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@b4
    iget-object v7, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mHandler:Landroid/os/Handler;

    #@b6
    const v8, 0x42004

    #@b9
    invoke-interface {v6, v7, v8, v10}, Lcom/android/internal/telephony/CommandsInterface;->registerForDataNetworkStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@bc
    .line 193
    invoke-static {}, Lcom/android/internal/telephony/uicc/UiccController;->getInstance()Lcom/android/internal/telephony/uicc/UiccController;

    #@bf
    move-result-object v6

    #@c0
    iput-object v6, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@c2
    .line 194
    iget-object v6, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@c4
    iget-object v7, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mHandler:Landroid/os/Handler;

    #@c6
    const v8, 0x42021

    #@c9
    invoke-virtual {v6, v7, v8, v10}, Lcom/android/internal/telephony/uicc/UiccController;->registerForIccChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@cc
    .line 196
    iget-object v6, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mCt:Lcom/android/internal/telephony/CallTracker;

    #@ce
    if-eqz v6, :cond_e4

    #@d0
    .line 197
    iget-object v6, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mCt:Lcom/android/internal/telephony/CallTracker;

    #@d2
    iget-object v7, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mHandler:Landroid/os/Handler;

    #@d4
    const v8, 0x42007

    #@d7
    invoke-virtual {v6, v7, v8, v10}, Lcom/android/internal/telephony/CallTracker;->registerForVoiceCallStarted(Landroid/os/Handler;ILjava/lang/Object;)V

    #@da
    .line 198
    iget-object v6, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mCt:Lcom/android/internal/telephony/CallTracker;

    #@dc
    iget-object v7, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mHandler:Landroid/os/Handler;

    #@de
    const v8, 0x42008

    #@e1
    invoke-virtual {v6, v7, v8, v10}, Lcom/android/internal/telephony/CallTracker;->registerForVoiceCallEnded(Landroid/os/Handler;ILjava/lang/Object;)V

    #@e4
    .line 201
    :cond_e4
    new-instance v4, Landroid/content/IntentFilter;

    #@e6
    invoke-direct {v4}, Landroid/content/IntentFilter;-><init>()V

    #@e9
    .line 202
    .local v4, filter:Landroid/content/IntentFilter;
    const-string v6, "android.intent.action.SCREEN_ON"

    #@eb
    invoke-virtual {v4, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@ee
    .line 203
    const-string v6, "android.intent.action.SCREEN_OFF"

    #@f0
    invoke-virtual {v4, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@f3
    .line 204
    const-string v6, "android.intent.action.SERVICE_STATE"

    #@f5
    invoke-virtual {v4, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@f8
    .line 205
    const-string v6, "android.net.conn.TETHER_STATE_CHANGED"

    #@fa
    invoke-virtual {v4, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@fd
    .line 206
    const-string v6, "com.lge.internal.telephony.SIGNAL_REPORT"

    #@ff
    invoke-virtual {v4, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@102
    .line 207
    iget-object v6, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mContext:Landroid/content/Context;

    #@104
    iget-object v7, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@106
    iget-object v8, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mHandler:Landroid/os/Handler;

    #@108
    invoke-virtual {v6, v7, v4, v10, v8}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@10b
    .line 209
    const-string v6, "VP Manager is ready."

    #@10d
    invoke-direct {p0, v6}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->logi(Ljava/lang/String;)V

    #@110
    .line 211
    return-void

    #@111
    .line 182
    .end local v4           #filter:Landroid/content/IntentFilter;
    :catch_111
    move-exception v3

    #@112
    .line 183
    .local v3, e:Ljava/lang/Exception;
    new-instance v6, Ljava/lang/StringBuilder;

    #@114
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@117
    const-string v7, "Failed to load QCRILHOOK_CLASS() class, exception: "

    #@119
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11c
    move-result-object v6

    #@11d
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@120
    move-result-object v6

    #@121
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@124
    move-result-object v6

    #@125
    invoke-direct {p0, v6}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->loge(Ljava/lang/String;)V

    #@128
    .line 184
    iput-object v10, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mQcRilHookObject:Ljava/lang/Object;

    #@12a
    .line 185
    iput-object v10, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mQcRilGoDormantFunc:Ljava/lang/reflect/Method;

    #@12c
    goto :goto_ad
.end method

.method static synthetic access$000(Lcom/android/internal/telephony/LgeVoiceProtectionManager;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->logv(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$100(Lcom/android/internal/telephony/LgeVoiceProtectionManager;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->setStatusFlag(I)V

    #@3
    return-void
.end method

.method static synthetic access$1000(Lcom/android/internal/telephony/LgeVoiceProtectionManager;)Lcom/android/internal/telephony/LgeVoiceProtectionManager$IdleState;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mIdleState:Lcom/android/internal/telephony/LgeVoiceProtectionManager$IdleState;

    #@2
    return-object v0
.end method

.method static synthetic access$1100(Lcom/android/internal/telephony/LgeVoiceProtectionManager;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 55
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$1200(Lcom/android/internal/telephony/LgeVoiceProtectionManager;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 55
    iget v0, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mVpStatus:I

    #@2
    return v0
.end method

.method static synthetic access$1300(Lcom/android/internal/telephony/LgeVoiceProtectionManager;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 55
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$1400(Lcom/android/internal/telephony/LgeVoiceProtectionManager;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 55
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$200(Lcom/android/internal/telephony/LgeVoiceProtectionManager;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->clearStatusFlag(I)V

    #@3
    return-void
.end method

.method static synthetic access$300(Lcom/android/internal/telephony/LgeVoiceProtectionManager;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->logi(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$400(Lcom/android/internal/telephony/LgeVoiceProtectionManager;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->logd(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$700(Lcom/android/internal/telephony/LgeVoiceProtectionManager;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mQcRilHookObject:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$800(Lcom/android/internal/telephony/LgeVoiceProtectionManager;)Ljava/lang/reflect/Method;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mQcRilGoDormantFunc:Ljava/lang/reflect/Method;

    #@2
    return-object v0
.end method

.method static synthetic access$900(Lcom/android/internal/telephony/LgeVoiceProtectionManager;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->loge(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method private clearStatusFlag(I)V
    .registers 4
    .parameter "statusFlag"

    #@0
    .prologue
    .line 250
    iget v0, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mVpStatus:I

    #@2
    xor-int/lit8 v1, p1, -0x1

    #@4
    and-int/2addr v0, v1

    #@5
    iput v0, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mVpStatus:I

    #@7
    .line 251
    return-void
.end method

.method private logd(Ljava/lang/String;)V
    .registers 5
    .parameter "string"

    #@0
    .prologue
    .line 591
    iget-boolean v0, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mIsReady:Z

    #@2
    if-eqz v0, :cond_1f

    #@4
    .line 592
    const-string v0, "VPMANAGER"

    #@6
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    invoke-virtual {p0}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->getCurrentState()Lcom/android/internal/util/IState;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 596
    :goto_1e
    return-void

    #@1f
    .line 594
    :cond_1f
    const-string v0, "VPMANAGER"

    #@21
    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    goto :goto_1e
.end method

.method private loge(Ljava/lang/String;)V
    .registers 5
    .parameter "string"

    #@0
    .prologue
    .line 575
    iget-boolean v0, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mIsReady:Z

    #@2
    if-eqz v0, :cond_1f

    #@4
    .line 576
    const-string v0, "VPMANAGER"

    #@6
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    invoke-virtual {p0}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->getCurrentState()Lcom/android/internal/util/IState;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 580
    :goto_1e
    return-void

    #@1f
    .line 578
    :cond_1f
    const-string v0, "VPMANAGER"

    #@21
    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    goto :goto_1e
.end method

.method private logi(Ljava/lang/String;)V
    .registers 5
    .parameter "string"

    #@0
    .prologue
    .line 607
    iget-boolean v0, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mIsReady:Z

    #@2
    if-eqz v0, :cond_1f

    #@4
    .line 608
    const-string v0, "VPMANAGER"

    #@6
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    invoke-virtual {p0}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->getCurrentState()Lcom/android/internal/util/IState;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 612
    :goto_1e
    return-void

    #@1f
    .line 610
    :cond_1f
    const-string v0, "VPMANAGER"

    #@21
    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    goto :goto_1e
.end method

.method private logv(Ljava/lang/String;)V
    .registers 5
    .parameter "string"

    #@0
    .prologue
    .line 599
    iget-boolean v0, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mIsReady:Z

    #@2
    if-eqz v0, :cond_1f

    #@4
    .line 600
    const-string v0, "VPMANAGER"

    #@6
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    invoke-virtual {p0}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->getCurrentState()Lcom/android/internal/util/IState;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 604
    :goto_1e
    return-void

    #@1f
    .line 602
    :cond_1f
    const-string v0, "VPMANAGER"

    #@21
    invoke-static {v0, p1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    goto :goto_1e
.end method

.method private logw(Ljava/lang/String;)V
    .registers 5
    .parameter "string"

    #@0
    .prologue
    .line 583
    iget-boolean v0, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mIsReady:Z

    #@2
    if-eqz v0, :cond_1f

    #@4
    .line 584
    const-string v0, "VPMANAGER"

    #@6
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    invoke-virtual {p0}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->getCurrentState()Lcom/android/internal/util/IState;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 588
    :goto_1e
    return-void

    #@1f
    .line 586
    :cond_1f
    const-string v0, "VPMANAGER"

    #@21
    invoke-static {v0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    goto :goto_1e
.end method

.method private setStatusFlag(I)V
    .registers 3
    .parameter "statusFlag"

    #@0
    .prologue
    .line 245
    iget v0, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mVpStatus:I

    #@2
    or-int/2addr v0, p1

    #@3
    iput v0, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mVpStatus:I

    #@5
    .line 246
    return-void
.end method


# virtual methods
.method public dispose()V
    .registers 3

    #@0
    .prologue
    .line 216
    iget-boolean v0, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mIsReady:Z

    #@2
    if-eqz v0, :cond_49

    #@4
    .line 217
    const/4 v0, 0x0

    #@5
    iput-boolean v0, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mIsReady:Z

    #@7
    .line 219
    iget-object v0, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mCt:Lcom/android/internal/telephony/CallTracker;

    #@9
    if-eqz v0, :cond_19

    #@b
    .line 220
    iget-object v0, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mCt:Lcom/android/internal/telephony/CallTracker;

    #@d
    iget-object v1, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mHandler:Landroid/os/Handler;

    #@f
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/CallTracker;->unregisterForVoiceCallStarted(Landroid/os/Handler;)V

    #@12
    .line 221
    iget-object v0, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mCt:Lcom/android/internal/telephony/CallTracker;

    #@14
    iget-object v1, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mHandler:Landroid/os/Handler;

    #@16
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/CallTracker;->unregisterForVoiceCallEnded(Landroid/os/Handler;)V

    #@19
    .line 224
    :cond_19
    iget-object v0, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@1b
    iget-object v1, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mHandler:Landroid/os/Handler;

    #@1d
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForDataNetworkStateChanged(Landroid/os/Handler;)V

    #@20
    .line 226
    iget-object v0, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@22
    if-eqz v0, :cond_2b

    #@24
    .line 227
    iget-object v0, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@26
    iget-object v1, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mHandler:Landroid/os/Handler;

    #@28
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/uicc/IccRecords;->unregisterForRecordsLoaded(Landroid/os/Handler;)V

    #@2b
    .line 229
    :cond_2b
    iget-object v0, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@2d
    iget-object v1, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mHandler:Landroid/os/Handler;

    #@2f
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/uicc/UiccController;->unregisterForIccChanged(Landroid/os/Handler;)V

    #@32
    .line 231
    iget-object v0, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mContext:Landroid/content/Context;

    #@34
    iget-object v1, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@36
    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@39
    .line 233
    iget-object v0, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    #@3b
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@3e
    move-result v0

    #@3f
    if-eqz v0, :cond_46

    #@41
    .line 234
    iget-object v0, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    #@43
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    #@46
    .line 237
    :cond_46
    invoke-virtual {p0}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->quit()V

    #@49
    .line 241
    :cond_49
    return-void
.end method

.method isUMTS()Z
    .registers 3

    #@0
    .prologue
    .line 451
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@3
    move-result-object v1

    #@4
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    #@7
    move-result v0

    #@8
    .line 453
    .local v0, tech:I
    sparse-switch v0, :sswitch_data_10

    #@b
    .line 461
    const/4 v1, 0x0

    #@c
    :goto_c
    return v1

    #@d
    .line 459
    :sswitch_d
    const/4 v1, 0x1

    #@e
    goto :goto_c

    #@f
    .line 453
    nop

    #@10
    :sswitch_data_10
    .sparse-switch
        0x3 -> :sswitch_d
        0x8 -> :sswitch_d
        0x9 -> :sswitch_d
        0xa -> :sswitch_d
        0xf -> :sswitch_d
    .end sparse-switch
.end method

.method public declared-synchronized isVpAllowed()Z
    .registers 5

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 467
    monitor-enter p0

    #@3
    :try_start_3
    new-instance v2, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v3, "isVpAllowed() : mVpStatus : "

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    iget v3, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mVpStatus:I

    #@10
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    const-string v3, ", VP_STATUS_IS_UMTS: "

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    iget v3, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mVpStatus:I

    #@1c
    and-int/lit8 v3, v3, 0x1

    #@1e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@21
    move-result-object v2

    #@22
    const-string v3, ", VP_STATUS_IS_CALLING: "

    #@24
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v2

    #@28
    iget v3, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mVpStatus:I

    #@2a
    and-int/lit8 v3, v3, 0x2

    #@2c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v2

    #@30
    const-string v3, ", VP_STATUS_IS_SCREEN_ON: "

    #@32
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v2

    #@36
    iget v3, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mVpStatus:I

    #@38
    and-int/lit8 v3, v3, 0x8

    #@3a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v2

    #@3e
    const-string v3, ", VP_STATUS_IS_POOR_SIGNAL: "

    #@40
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v2

    #@44
    iget v3, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mVpStatus:I

    #@46
    and-int/lit8 v3, v3, 0x10

    #@48
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v2

    #@4c
    const-string v3, ", VP_STATUS_IS_TETHERING: "

    #@4e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v2

    #@52
    iget v3, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mVpStatus:I

    #@54
    and-int/lit8 v3, v3, 0x20

    #@56
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@59
    move-result-object v2

    #@5a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5d
    move-result-object v2

    #@5e
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->logd(Ljava/lang/String;)V

    #@61
    .line 475
    iget v2, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mVpStatus:I

    #@63
    and-int/lit8 v2, v2, 0x1

    #@65
    if-eqz v2, :cond_86

    #@67
    iget v2, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mVpStatus:I

    #@69
    and-int/lit8 v2, v2, 0x2

    #@6b
    if-eqz v2, :cond_86

    #@6d
    iget v2, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mVpStatus:I

    #@6f
    and-int/lit8 v2, v2, 0x8

    #@71
    if-nez v2, :cond_86

    #@73
    iget v2, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mVpStatus:I

    #@75
    and-int/lit8 v2, v2, 0x10

    #@77
    if-eqz v2, :cond_86

    #@79
    iget v2, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mVpStatus:I

    #@7b
    and-int/lit8 v2, v2, 0x20

    #@7d
    if-nez v2, :cond_86

    #@7f
    .line 481
    const-string v1, "isVpAllowed() : return true"

    #@81
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->logd(Ljava/lang/String;)V
    :try_end_84
    .catchall {:try_start_3 .. :try_end_84} :catchall_ad

    #@84
    .line 495
    :goto_84
    monitor-exit p0

    #@85
    return v0

    #@86
    .line 484
    :cond_86
    :try_start_86
    const-string v2, "persist.lg.data.vptest"

    #@88
    const/4 v3, 0x0

    #@89
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@8c
    move-result v2

    #@8d
    if-eqz v2, :cond_b0

    #@8f
    iget v2, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mVpStatus:I

    #@91
    and-int/lit8 v2, v2, 0x1

    #@93
    if-eqz v2, :cond_b0

    #@95
    iget v2, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mVpStatus:I

    #@97
    and-int/lit8 v2, v2, 0x2

    #@99
    if-eqz v2, :cond_b0

    #@9b
    iget v2, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mVpStatus:I

    #@9d
    and-int/lit8 v2, v2, 0x8

    #@9f
    if-nez v2, :cond_b0

    #@a1
    iget v2, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mVpStatus:I

    #@a3
    and-int/lit8 v2, v2, 0x20

    #@a5
    if-nez v2, :cond_b0

    #@a7
    .line 490
    const-string v1, "isVpAllowed() for VP Test: return true"

    #@a9
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->logd(Ljava/lang/String;)V
    :try_end_ac
    .catchall {:try_start_86 .. :try_end_ac} :catchall_ad

    #@ac
    goto :goto_84

    #@ad
    .line 467
    :catchall_ad
    move-exception v0

    #@ae
    monitor-exit p0

    #@af
    throw v0

    #@b0
    .line 494
    :cond_b0
    :try_start_b0
    const-string v0, "isVpAllowed() : return false"

    #@b2
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->logd(Ljava/lang/String;)V
    :try_end_b5
    .catchall {:try_start_b0 .. :try_end_b5} :catchall_ad

    #@b5
    move v0, v1

    #@b6
    .line 495
    goto :goto_84
.end method

.method protected onDataCallListChanged(Landroid/os/AsyncResult;)V
    .registers 9
    .parameter "ar"

    #@0
    .prologue
    const/4 v6, 0x4

    #@1
    .line 533
    iget-object v4, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@3
    check-cast v4, Ljava/util/ArrayList;

    #@5
    move-object v0, v4

    #@6
    check-cast v0, Ljava/util/ArrayList;

    #@8
    .line 535
    .local v0, dcStates:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/DataCallState;>;"
    iget-object v4, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@a
    if-eqz v4, :cond_d

    #@c
    .line 560
    :goto_c
    return-void

    #@d
    .line 542
    :cond_d
    const/4 v2, 0x1

    #@e
    .line 544
    .local v2, isDormant:Z
    invoke-virtual {v0}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    #@11
    move-result-object v4

    #@12
    invoke-direct {p0, v4}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->logi(Ljava/lang/String;)V

    #@15
    .line 546
    const/4 v1, 0x0

    #@16
    .local v1, i:I
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@19
    move-result v3

    #@1a
    .local v3, s:I
    :goto_1a
    if-ge v1, v3, :cond_28

    #@1c
    .line 548
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1f
    move-result-object v4

    #@20
    check-cast v4, Lcom/android/internal/telephony/DataCallState;

    #@22
    iget v4, v4, Lcom/android/internal/telephony/DataCallState;->active:I

    #@24
    const/4 v5, 0x2

    #@25
    if-ne v4, v5, :cond_53

    #@27
    .line 549
    const/4 v2, 0x0

    #@28
    .line 555
    :cond_28
    new-instance v4, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v5, "prev:"

    #@2f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v4

    #@33
    invoke-virtual {p0}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->getCurrentState()Lcom/android/internal/util/IState;

    #@36
    move-result-object v5

    #@37
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v4

    #@3b
    const-string v5, ", curr:"

    #@3d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v4

    #@41
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@44
    move-result-object v4

    #@45
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v4

    #@49
    invoke-direct {p0, v4}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->logi(Ljava/lang/String;)V

    #@4c
    .line 557
    const/4 v4, 0x1

    #@4d
    if-ne v2, v4, :cond_56

    #@4f
    invoke-direct {p0, v6}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->clearStatusFlag(I)V

    #@52
    goto :goto_c

    #@53
    .line 546
    :cond_53
    add-int/lit8 v1, v1, 0x1

    #@55
    goto :goto_1a

    #@56
    .line 558
    :cond_56
    invoke-direct {p0, v6}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->setStatusFlag(I)V

    #@59
    goto :goto_c
.end method

.method protected onGoDormantReponse(Landroid/os/AsyncResult;)V
    .registers 4
    .parameter "ar"

    #@0
    .prologue
    .line 563
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "GoDormantResult"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget-object v1, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->logv(Ljava/lang/String;)V

    #@18
    .line 564
    return-void
.end method

.method protected onRecordsLoaded()V
    .registers 1

    #@0
    .prologue
    .line 528
    return-void
.end method

.method protected onVpStatusChanged()V
    .registers 4

    #@0
    .prologue
    .line 499
    invoke-virtual {p0}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->isVpAllowed()Z

    #@3
    move-result v0

    #@4
    .line 501
    .local v0, vpAllowed:Z
    if-eqz v0, :cond_14

    #@6
    invoke-virtual {p0}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->getCurrentState()Lcom/android/internal/util/IState;

    #@9
    move-result-object v1

    #@a
    iget-object v2, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mIdleState:Lcom/android/internal/telephony/LgeVoiceProtectionManager$IdleState;

    #@c
    if-ne v1, v2, :cond_14

    #@e
    iget-object v1, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mActiveState:Lcom/android/internal/telephony/LgeVoiceProtectionManager$ActiveState;

    #@10
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->transitionTo(Lcom/android/internal/util/IState;)V

    #@13
    .line 503
    :cond_13
    :goto_13
    return-void

    #@14
    .line 502
    :cond_14
    if-nez v0, :cond_13

    #@16
    invoke-virtual {p0}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->getCurrentState()Lcom/android/internal/util/IState;

    #@19
    move-result-object v1

    #@1a
    iget-object v2, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mActiveState:Lcom/android/internal/telephony/LgeVoiceProtectionManager$ActiveState;

    #@1c
    if-ne v1, v2, :cond_13

    #@1e
    iget-object v1, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mIdleState:Lcom/android/internal/telephony/LgeVoiceProtectionManager$IdleState;

    #@20
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->transitionTo(Lcom/android/internal/util/IState;)V

    #@23
    goto :goto_13
.end method

.method protected updateIccAvailability()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 506
    iget-object v1, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@3
    if-nez v1, :cond_6

    #@5
    .line 524
    :cond_5
    :goto_5
    return-void

    #@6
    .line 510
    :cond_6
    iget-object v1, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@8
    iget-object v2, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@a
    const/4 v2, 0x1

    #@b
    invoke-virtual {v1, v2}, Lcom/android/internal/telephony/uicc/UiccController;->getIccRecords(I)Lcom/android/internal/telephony/uicc/IccRecords;

    #@e
    move-result-object v0

    #@f
    .line 512
    .local v0, newIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;
    iget-object v1, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@11
    if-eq v1, v0, :cond_5

    #@13
    .line 513
    iget-object v1, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@15
    if-eqz v1, :cond_25

    #@17
    .line 514
    const-string v1, "Removing stale icc objects."

    #@19
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->logd(Ljava/lang/String;)V

    #@1c
    .line 515
    iget-object v1, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@1e
    iget-object v2, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mHandler:Landroid/os/Handler;

    #@20
    invoke-virtual {v1, v2}, Lcom/android/internal/telephony/uicc/IccRecords;->unregisterForRecordsLoaded(Landroid/os/Handler;)V

    #@23
    .line 516
    iput-object v4, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@25
    .line 518
    :cond_25
    if-eqz v0, :cond_5

    #@27
    .line 519
    const-string v1, "New records found"

    #@29
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->logd(Ljava/lang/String;)V

    #@2c
    .line 520
    iput-object v0, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@2e
    .line 521
    iget-object v1, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@30
    iget-object v2, p0, Lcom/android/internal/telephony/LgeVoiceProtectionManager;->mHandler:Landroid/os/Handler;

    #@32
    const v3, 0x42002

    #@35
    invoke-virtual {v1, v2, v3, v4}, Lcom/android/internal/telephony/uicc/IccRecords;->registerForRecordsLoaded(Landroid/os/Handler;ILjava/lang/Object;)V

    #@38
    goto :goto_5
.end method
