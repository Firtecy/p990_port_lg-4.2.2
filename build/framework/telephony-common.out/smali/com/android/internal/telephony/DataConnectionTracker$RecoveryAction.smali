.class public Lcom/android/internal/telephony/DataConnectionTracker$RecoveryAction;
.super Ljava/lang/Object;
.source "DataConnectionTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/DataConnectionTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "RecoveryAction"
.end annotation


# static fields
.field public static final CLEANUP:I = 0x1

.field public static final GET_DATA_CALL_LIST:I = 0x0

.field public static final RADIO_RESTART:I = 0x3

.field public static final RADIO_RESTART_WITH_PROP:I = 0x4

.field public static final REREGISTER:I = 0x2


# direct methods
.method protected constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 2512
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method static synthetic access$100(I)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 2512
    invoke-static {p0}, Lcom/android/internal/telephony/DataConnectionTracker$RecoveryAction;->isAggressiveRecovery(I)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method private static isAggressiveRecovery(I)Z
    .registers 3
    .parameter "value"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 2520
    if-eq p0, v0, :cond_c

    #@3
    const/4 v1, 0x2

    #@4
    if-eq p0, v1, :cond_c

    #@6
    const/4 v1, 0x3

    #@7
    if-eq p0, v1, :cond_c

    #@9
    const/4 v1, 0x4

    #@a
    if-ne p0, v1, :cond_d

    #@c
    :cond_c
    :goto_c
    return v0

    #@d
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_c
.end method
