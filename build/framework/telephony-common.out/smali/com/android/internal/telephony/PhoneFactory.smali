.class public Lcom/android/internal/telephony/PhoneFactory;
.super Ljava/lang/Object;
.source "PhoneFactory.java"


# static fields
.field static final LOG_TAG:Ljava/lang/String; = "PHONE"

.field protected static final SOCKET_OPEN_MAX_RETRY:I = 0x3

.field protected static final SOCKET_OPEN_RETRY_MILLIS:I = 0x7d0

.field static phoneMgr_lg:Lcom/android/internal/telephony/LGPhoneInterfaceManager;

.field protected static final preferredCdmaSubscription:I

.field protected static sCommandsInterface:Lcom/android/internal/telephony/CommandsInterface;

.field protected static sContext:Landroid/content/Context;

.field protected static sLooper:Landroid/os/Looper;

.field protected static sMadeDefaults:Z

.field protected static sPhoneNotifier:Lcom/android/internal/telephony/PhoneNotifier;

.field protected static sProxyPhone:Lcom/android/internal/telephony/Phone;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 55
    sput-object v0, Lcom/android/internal/telephony/PhoneFactory;->sProxyPhone:Lcom/android/internal/telephony/Phone;

    #@3
    .line 56
    sput-object v0, Lcom/android/internal/telephony/PhoneFactory;->sCommandsInterface:Lcom/android/internal/telephony/CommandsInterface;

    #@5
    .line 58
    const/4 v0, 0x0

    #@6
    sput-boolean v0, Lcom/android/internal/telephony/PhoneFactory;->sMadeDefaults:Z

    #@8
    .line 66
    sget v0, Lcom/android/internal/telephony/cdma/CdmaSubscriptionSourceManager;->PREFERRED_CDMA_SUBSCRIPTION:I

    #@a
    sput v0, Lcom/android/internal/telephony/PhoneFactory;->preferredCdmaSubscription:I

    #@c
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 48
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static getCdmaPhone()Lcom/android/internal/telephony/Phone;
    .registers 5

    #@0
    .prologue
    .line 278
    sget-object v2, Lcom/android/internal/telephony/PhoneProxy;->lockForRadioTechnologyChange:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 279
    :try_start_3
    invoke-static {}, Landroid/telephony/TelephonyManager;->getLteOnCdmaModeStatic()I

    #@6
    move-result v1

    #@7
    packed-switch v1, :pswitch_data_26

    #@a
    .line 287
    new-instance v0, Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@c
    sget-object v1, Lcom/android/internal/telephony/PhoneFactory;->sContext:Landroid/content/Context;

    #@e
    sget-object v3, Lcom/android/internal/telephony/PhoneFactory;->sCommandsInterface:Lcom/android/internal/telephony/CommandsInterface;

    #@10
    sget-object v4, Lcom/android/internal/telephony/PhoneFactory;->sPhoneNotifier:Lcom/android/internal/telephony/PhoneNotifier;

    #@12
    invoke-direct {v0, v1, v3, v4}, Lcom/android/internal/telephony/cdma/CDMAPhone;-><init>(Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;Lcom/android/internal/telephony/PhoneNotifier;)V

    #@15
    .line 291
    .local v0, phone:Lcom/android/internal/telephony/Phone;
    :goto_15
    monitor-exit v2

    #@16
    .line 292
    return-object v0

    #@17
    .line 281
    .end local v0           #phone:Lcom/android/internal/telephony/Phone;
    :pswitch_17
    new-instance v0, Lcom/android/internal/telephony/cdma/CDMALTEPhone;

    #@19
    sget-object v1, Lcom/android/internal/telephony/PhoneFactory;->sContext:Landroid/content/Context;

    #@1b
    sget-object v3, Lcom/android/internal/telephony/PhoneFactory;->sCommandsInterface:Lcom/android/internal/telephony/CommandsInterface;

    #@1d
    sget-object v4, Lcom/android/internal/telephony/PhoneFactory;->sPhoneNotifier:Lcom/android/internal/telephony/PhoneNotifier;

    #@1f
    invoke-direct {v0, v1, v3, v4}, Lcom/android/internal/telephony/cdma/CDMALTEPhone;-><init>(Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;Lcom/android/internal/telephony/PhoneNotifier;)V

    #@22
    .line 282
    .restart local v0       #phone:Lcom/android/internal/telephony/Phone;
    goto :goto_15

    #@23
    .line 291
    .end local v0           #phone:Lcom/android/internal/telephony/Phone;
    :catchall_23
    move-exception v1

    #@24
    monitor-exit v2
    :try_end_25
    .catchall {:try_start_3 .. :try_end_25} :catchall_23

    #@25
    throw v1

    #@26
    .line 279
    :pswitch_data_26
    .packed-switch 0x1
        :pswitch_17
    .end packed-switch
.end method

.method public static getCommandsInterface()Lcom/android/internal/telephony/CommandsInterface;
    .registers 1

    #@0
    .prologue
    .line 314
    sget-object v0, Lcom/android/internal/telephony/PhoneFactory;->sCommandsInterface:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    return-object v0
.end method

.method public static getDefaultPhone()Lcom/android/internal/telephony/Phone;
    .registers 2

    #@0
    .prologue
    .line 265
    sget-object v0, Lcom/android/internal/telephony/PhoneFactory;->sLooper:Landroid/os/Looper;

    #@2
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@5
    move-result-object v1

    #@6
    if-eq v0, v1, :cond_10

    #@8
    .line 266
    new-instance v0, Ljava/lang/RuntimeException;

    #@a
    const-string v1, "PhoneFactory.getDefaultPhone must be called from Looper thread"

    #@c
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@f
    throw v0

    #@10
    .line 270
    :cond_10
    sget-boolean v0, Lcom/android/internal/telephony/PhoneFactory;->sMadeDefaults:Z

    #@12
    if-nez v0, :cond_1c

    #@14
    .line 271
    new-instance v0, Ljava/lang/IllegalStateException;

    #@16
    const-string v1, "Default phones haven\'t been made yet!"

    #@18
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v0

    #@1c
    .line 273
    :cond_1c
    sget-object v0, Lcom/android/internal/telephony/PhoneFactory;->sProxyPhone:Lcom/android/internal/telephony/Phone;

    #@1e
    return-object v0
.end method

.method public static getGsmPhone()Lcom/android/internal/telephony/Phone;
    .registers 5

    #@0
    .prologue
    .line 296
    sget-object v2, Lcom/android/internal/telephony/PhoneProxy;->lockForRadioTechnologyChange:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 297
    :try_start_3
    new-instance v0, Lcom/android/internal/telephony/gsm/GSMPhone;

    #@5
    sget-object v1, Lcom/android/internal/telephony/PhoneFactory;->sContext:Landroid/content/Context;

    #@7
    sget-object v3, Lcom/android/internal/telephony/PhoneFactory;->sCommandsInterface:Lcom/android/internal/telephony/CommandsInterface;

    #@9
    sget-object v4, Lcom/android/internal/telephony/PhoneFactory;->sPhoneNotifier:Lcom/android/internal/telephony/PhoneNotifier;

    #@b
    invoke-direct {v0, v1, v3, v4}, Lcom/android/internal/telephony/gsm/GSMPhone;-><init>(Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;Lcom/android/internal/telephony/PhoneNotifier;)V

    #@e
    .line 298
    .local v0, phone:Lcom/android/internal/telephony/Phone;
    monitor-exit v2

    #@f
    return-object v0

    #@10
    .line 299
    :catchall_10
    move-exception v1

    #@11
    monitor-exit v2
    :try_end_12
    .catchall {:try_start_3 .. :try_end_12} :catchall_10

    #@12
    throw v1
.end method

.method public static makeDefaultPhone(Landroid/content/Context;)V
    .registers 23
    .parameter "context"

    #@0
    .prologue
    .line 80
    const-class v18, Lcom/android/internal/telephony/Phone;

    #@2
    monitor-enter v18

    #@3
    .line 81
    :try_start_3
    sget-boolean v17, Lcom/android/internal/telephony/PhoneFactory;->sMadeDefaults:Z

    #@5
    if-nez v17, :cond_1f7

    #@7
    .line 82
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@a
    move-result-object v17

    #@b
    sput-object v17, Lcom/android/internal/telephony/PhoneFactory;->sLooper:Landroid/os/Looper;

    #@d
    .line 83
    sput-object p0, Lcom/android/internal/telephony/PhoneFactory;->sContext:Landroid/content/Context;

    #@f
    .line 85
    sget-object v17, Lcom/android/internal/telephony/PhoneFactory;->sLooper:Landroid/os/Looper;

    #@11
    if-nez v17, :cond_22

    #@13
    .line 86
    new-instance v17, Ljava/lang/RuntimeException;

    #@15
    const-string v19, "PhoneFactory.makeDefaultPhone must be called from Looper thread"

    #@17
    move-object/from16 v0, v17

    #@19
    move-object/from16 v1, v19

    #@1b
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@1e
    throw v17

    #@1f
    .line 261
    :catchall_1f
    move-exception v17

    #@20
    monitor-exit v18
    :try_end_21
    .catchall {:try_start_3 .. :try_end_21} :catchall_1f

    #@21
    throw v17

    #@22
    .line 90
    :cond_22
    const/4 v15, 0x0

    #@23
    .line 92
    .local v15, retryCount:I
    :goto_23
    const/4 v10, 0x0

    #@24
    .line 93
    .local v10, hasException:Z
    add-int/lit8 v15, v15, 0x1

    #@26
    .line 98
    :try_start_26
    new-instance v17, Landroid/net/LocalServerSocket;

    #@28
    const-string v19, "com.android.internal.telephony"

    #@2a
    move-object/from16 v0, v17

    #@2c
    move-object/from16 v1, v19

    #@2e
    invoke-direct {v0, v1}, Landroid/net/LocalServerSocket;-><init>(Ljava/lang/String;)V
    :try_end_31
    .catchall {:try_start_26 .. :try_end_31} :catchall_1f
    .catch Ljava/io/IOException; {:try_start_26 .. :try_end_31} :catch_1f9

    #@31
    .line 103
    :goto_31
    if-nez v10, :cond_1fd

    #@33
    .line 116
    :try_start_33
    invoke-static {}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->getDefault()Lcom/android/internal/telephony/LgeTimeZoneMonitor;

    #@36
    move-result-object v16

    #@37
    .line 117
    .local v16, tzmonitor:Lcom/android/internal/telephony/LgeTimeZoneMonitor;
    if-eqz v16, :cond_40

    #@39
    move-object/from16 v0, v16

    #@3b
    move-object/from16 v1, p0

    #@3d
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->setContext(Landroid/content/Context;)V

    #@40
    .line 120
    :cond_40
    new-instance v17, Lcom/android/internal/telephony/DefaultPhoneNotifier;

    #@42
    invoke-direct/range {v17 .. v17}, Lcom/android/internal/telephony/DefaultPhoneNotifier;-><init>()V

    #@45
    sput-object v17, Lcom/android/internal/telephony/PhoneFactory;->sPhoneNotifier:Lcom/android/internal/telephony/PhoneNotifier;

    #@47
    .line 135
    sget v14, Lcom/android/internal/telephony/RILConstants;->PREFERRED_NETWORK_MODE:I

    #@49
    .line 146
    .local v14, preferredNetworkMode:I
    const-string v17, "ro.afwdata.LGfeatureset"

    #@4b
    const-string v19, "none"

    #@4d
    move-object/from16 v0, v17

    #@4f
    move-object/from16 v1, v19

    #@51
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@54
    move-result-object v9

    #@55
    .line 149
    .local v9, featureset:Ljava/lang/String;
    const-string v17, "persist.sys.iccid-mcc"

    #@57
    const-string v19, "FFF"

    #@59
    move-object/from16 v0, v17

    #@5b
    move-object/from16 v1, v19

    #@5d
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@60
    move-result-object v11

    #@61
    .line 150
    .local v11, iccid_mcc:Ljava/lang/String;
    const/4 v5, 0x0

    #@62
    .line 151
    .local v5, ICCID_MCC:I
    const-string v17, "PHONE"

    #@64
    new-instance v19, Ljava/lang/StringBuilder;

    #@66
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@69
    const-string v20, "iccid_mcc = "

    #@6b
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v19

    #@6f
    move-object/from16 v0, v19

    #@71
    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v19

    #@75
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@78
    move-result-object v19

    #@79
    move-object/from16 v0, v17

    #@7b
    move-object/from16 v1, v19

    #@7d
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_80
    .catchall {:try_start_33 .. :try_end_80} :catchall_1f

    #@80
    .line 153
    :try_start_80
    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_83
    .catchall {:try_start_80 .. :try_end_83} :catchall_1f
    .catch Ljava/lang/NumberFormatException; {:try_start_80 .. :try_end_83} :catch_219

    #@83
    move-result v5

    #@84
    .line 158
    :goto_84
    :try_start_84
    const-string v17, "PHONE"

    #@86
    new-instance v19, Ljava/lang/StringBuilder;

    #@88
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@8b
    const-string v20, "ICCID_MCC--integer = "

    #@8d
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v19

    #@91
    move-object/from16 v0, v19

    #@93
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@96
    move-result-object v19

    #@97
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9a
    move-result-object v19

    #@9b
    move-object/from16 v0, v17

    #@9d
    move-object/from16 v1, v19

    #@9f
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@a2
    .line 160
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@a5
    move-result-object v17

    #@a6
    const-string v19, "preferred_network_mode"

    #@a8
    move-object/from16 v0, v17

    #@aa
    move-object/from16 v1, v19

    #@ac
    invoke-static {v0, v1, v14}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@af
    move-result v4

    #@b0
    .line 162
    .local v4, DBnetworkMode:I
    const-string v17, "PHONE"

    #@b2
    new-instance v19, Ljava/lang/StringBuilder;

    #@b4
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@b7
    const-string v20, "DBnetworkMode--gcv =  "

    #@b9
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bc
    move-result-object v19

    #@bd
    move-object/from16 v0, v19

    #@bf
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c2
    move-result-object v19

    #@c3
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c6
    move-result-object v19

    #@c7
    move-object/from16 v0, v17

    #@c9
    move-object/from16 v1, v19

    #@cb
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@ce
    .line 163
    const-string v17, "ro.telephony.default_network"

    #@d0
    const/16 v19, 0x0

    #@d2
    move-object/from16 v0, v17

    #@d4
    move/from16 v1, v19

    #@d6
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@d9
    move-result v17

    #@da
    const/16 v19, 0x9

    #@dc
    move/from16 v0, v17

    #@de
    move/from16 v1, v19

    #@e0
    if-ne v0, v1, :cond_114

    #@e2
    const-string v17, "ro.build.target_region"

    #@e4
    invoke-static/range {v17 .. v17}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@e7
    move-result-object v17

    #@e8
    const-string v19, "ESA"

    #@ea
    move-object/from16 v0, v17

    #@ec
    move-object/from16 v1, v19

    #@ee
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f1
    move-result v17

    #@f2
    if-nez v17, :cond_106

    #@f4
    const-string v17, "ro.build.target_region"

    #@f6
    invoke-static/range {v17 .. v17}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@f9
    move-result-object v17

    #@fa
    const-string v19, "AME"

    #@fc
    move-object/from16 v0, v17

    #@fe
    move-object/from16 v1, v19

    #@100
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@103
    move-result v17

    #@104
    if-eqz v17, :cond_114

    #@106
    .line 166
    :cond_106
    sparse-switch v5, :sswitch_data_2a2

    #@109
    .line 192
    const-string v17, "PHONE"

    #@10b
    const-string v19, "NETWORK_MODE_LTE_GSM_WCDMA "

    #@10d
    move-object/from16 v0, v17

    #@10f
    move-object/from16 v1, v19

    #@111
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@114
    .line 200
    :cond_114
    :goto_114
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@117
    move-result-object v17

    #@118
    const-string v19, "preferred_network_mode"

    #@11a
    move-object/from16 v0, v17

    #@11c
    move-object/from16 v1, v19

    #@11e
    invoke-static {v0, v1, v14}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@121
    move-result v12

    #@122
    .line 202
    .local v12, networkMode:I
    const-string v17, "PHONE"

    #@124
    new-instance v19, Ljava/lang/StringBuilder;

    #@126
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@129
    const-string v20, "Network Mode set to "

    #@12b
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12e
    move-result-object v19

    #@12f
    invoke-static {v12}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@132
    move-result-object v20

    #@133
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@136
    move-result-object v19

    #@137
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13a
    move-result-object v19

    #@13b
    move-object/from16 v0, v17

    #@13d
    move-object/from16 v1, v19

    #@13f
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@142
    .line 208
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@145
    move-result-object v17

    #@146
    const-string v19, "subscription_mode"

    #@148
    sget v20, Lcom/android/internal/telephony/PhoneFactory;->preferredCdmaSubscription:I

    #@14a
    move-object/from16 v0, v17

    #@14c
    move-object/from16 v1, v19

    #@14e
    move/from16 v2, v20

    #@150
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@153
    move-result v6

    #@154
    .line 211
    .local v6, cdmaSubscription:I
    const-string v17, "PHONE"

    #@156
    new-instance v19, Ljava/lang/StringBuilder;

    #@158
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@15b
    const-string v20, "Cdma Subscription set to "

    #@15d
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@160
    move-result-object v19

    #@161
    move-object/from16 v0, v19

    #@163
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@166
    move-result-object v19

    #@167
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16a
    move-result-object v19

    #@16b
    move-object/from16 v0, v17

    #@16d
    move-object/from16 v1, v19

    #@16f
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@172
    .line 214
    new-instance v17, Lcom/android/internal/telephony/LgeRIL;

    #@174
    move-object/from16 v0, v17

    #@176
    move-object/from16 v1, p0

    #@178
    invoke-direct {v0, v1, v12, v6, v9}, Lcom/android/internal/telephony/LgeRIL;-><init>(Landroid/content/Context;IILjava/lang/String;)V

    #@17b
    sput-object v17, Lcom/android/internal/telephony/PhoneFactory;->sCommandsInterface:Lcom/android/internal/telephony/CommandsInterface;

    #@17d
    .line 217
    sget-object v17, Lcom/android/internal/telephony/PhoneFactory;->sCommandsInterface:Lcom/android/internal/telephony/CommandsInterface;

    #@17f
    move-object/from16 v0, p0

    #@181
    move-object/from16 v1, v17

    #@183
    invoke-static {v0, v1}, Lcom/android/internal/telephony/uicc/UiccController;->make(Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;)Lcom/android/internal/telephony/uicc/UiccController;

    #@186
    .line 219
    invoke-static {v12}, Landroid/telephony/TelephonyManager;->getPhoneType(I)I

    #@189
    move-result v13

    #@18a
    .line 220
    .local v13, phoneType:I
    const/16 v17, 0x1

    #@18c
    move/from16 v0, v17

    #@18e
    if-ne v13, v0, :cond_242

    #@190
    .line 221
    const-string v17, "PHONE"

    #@192
    const-string v19, "Creating GSMPhone"

    #@194
    move-object/from16 v0, v17

    #@196
    move-object/from16 v1, v19

    #@198
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@19b
    .line 224
    invoke-static {}, Lcom/android/internal/telephony/TelephonyUtils;->isIPPhoneSupported()Z

    #@19e
    move-result v17

    #@19f
    if-eqz v17, :cond_224

    #@1a1
    .line 225
    new-instance v17, Lcom/movial/ipphone/IPPhoneProxy;

    #@1a3
    sget-object v19, Lcom/android/internal/telephony/PhoneFactory;->sCommandsInterface:Lcom/android/internal/telephony/CommandsInterface;

    #@1a5
    sget-object v20, Lcom/android/internal/telephony/PhoneFactory;->sPhoneNotifier:Lcom/android/internal/telephony/PhoneNotifier;

    #@1a7
    move-object/from16 v0, v17

    #@1a9
    move-object/from16 v1, p0

    #@1ab
    move-object/from16 v2, v19

    #@1ad
    move-object/from16 v3, v20

    #@1af
    invoke-direct {v0, v1, v2, v3}, Lcom/movial/ipphone/IPPhoneProxy;-><init>(Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;Lcom/android/internal/telephony/PhoneNotifier;)V

    #@1b2
    sput-object v17, Lcom/android/internal/telephony/PhoneFactory;->sProxyPhone:Lcom/android/internal/telephony/Phone;

    #@1b4
    .line 249
    :cond_1b4
    :goto_1b4
    invoke-static/range {p0 .. p0}, Lcom/android/internal/telephony/IMSPhone;->isPhoneRequired(Landroid/content/Context;)Z

    #@1b7
    move-result v17

    #@1b8
    if-eqz v17, :cond_1eb

    #@1ba
    .line 250
    const-string v17, "LGIMS"

    #@1bc
    new-instance v19, Ljava/lang/StringBuilder;

    #@1be
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@1c1
    const-string v20, "Creating IMSPhone ... Phone Type = "

    #@1c3
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c6
    move-result-object v19

    #@1c7
    move-object/from16 v0, v19

    #@1c9
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1cc
    move-result-object v19

    #@1cd
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d0
    move-result-object v19

    #@1d1
    move-object/from16 v0, v17

    #@1d3
    move-object/from16 v1, v19

    #@1d5
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1d8
    .line 251
    invoke-static {}, Lcom/android/internal/telephony/IMSPhone;->getInstance()Lcom/android/internal/telephony/IMSPhone;

    #@1db
    move-result-object v17

    #@1dc
    sget-object v19, Lcom/android/internal/telephony/PhoneFactory;->sProxyPhone:Lcom/android/internal/telephony/Phone;

    #@1de
    sget-object v20, Lcom/android/internal/telephony/PhoneFactory;->sCommandsInterface:Lcom/android/internal/telephony/CommandsInterface;

    #@1e0
    move-object/from16 v0, v17

    #@1e2
    move-object/from16 v1, p0

    #@1e4
    move-object/from16 v2, v19

    #@1e6
    move-object/from16 v3, v20

    #@1e8
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/internal/telephony/IMSPhone;->setDefaultPhone(Landroid/content/Context;Lcom/android/internal/telephony/Phone;Lcom/android/internal/telephony/CommandsInterface;)V

    #@1eb
    .line 255
    :cond_1eb
    const/16 v17, 0x1

    #@1ed
    sput-boolean v17, Lcom/android/internal/telephony/PhoneFactory;->sMadeDefaults:Z

    #@1ef
    .line 258
    sget-object v17, Lcom/android/internal/telephony/PhoneFactory;->sProxyPhone:Lcom/android/internal/telephony/Phone;

    #@1f1
    invoke-static/range {v17 .. v17}, Lcom/android/internal/telephony/LGPhoneInterfaceManager;->init(Lcom/android/internal/telephony/Phone;)Lcom/android/internal/telephony/LGPhoneInterfaceManager;

    #@1f4
    move-result-object v17

    #@1f5
    sput-object v17, Lcom/android/internal/telephony/PhoneFactory;->phoneMgr_lg:Lcom/android/internal/telephony/LGPhoneInterfaceManager;

    #@1f7
    .line 261
    .end local v4           #DBnetworkMode:I
    .end local v5           #ICCID_MCC:I
    .end local v6           #cdmaSubscription:I
    .end local v9           #featureset:Ljava/lang/String;
    .end local v10           #hasException:Z
    .end local v11           #iccid_mcc:Ljava/lang/String;
    .end local v12           #networkMode:I
    .end local v13           #phoneType:I
    .end local v14           #preferredNetworkMode:I
    .end local v15           #retryCount:I
    .end local v16           #tzmonitor:Lcom/android/internal/telephony/LgeTimeZoneMonitor;
    :cond_1f7
    monitor-exit v18

    #@1f8
    .line 262
    return-void

    #@1f9
    .line 99
    .restart local v10       #hasException:Z
    .restart local v15       #retryCount:I
    :catch_1f9
    move-exception v8

    #@1fa
    .line 100
    .local v8, ex:Ljava/io/IOException;
    const/4 v10, 0x1

    #@1fb
    goto/16 :goto_31

    #@1fd
    .line 105
    .end local v8           #ex:Ljava/io/IOException;
    :cond_1fd
    const/16 v17, 0x3

    #@1ff
    move/from16 v0, v17

    #@201
    if-le v15, v0, :cond_20f

    #@203
    .line 106
    new-instance v17, Ljava/lang/RuntimeException;

    #@205
    const-string v19, "PhoneFactory probably already running"

    #@207
    move-object/from16 v0, v17

    #@209
    move-object/from16 v1, v19

    #@20b
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@20e
    throw v17
    :try_end_20f
    .catchall {:try_start_84 .. :try_end_20f} :catchall_1f

    #@20f
    .line 109
    :cond_20f
    const-wide/16 v19, 0x7d0

    #@211
    :try_start_211
    invoke-static/range {v19 .. v20}, Ljava/lang/Thread;->sleep(J)V
    :try_end_214
    .catchall {:try_start_211 .. :try_end_214} :catchall_1f
    .catch Ljava/lang/InterruptedException; {:try_start_211 .. :try_end_214} :catch_216

    #@214
    goto/16 :goto_23

    #@216
    .line 110
    :catch_216
    move-exception v17

    #@217
    goto/16 :goto_23

    #@219
    .line 155
    .restart local v5       #ICCID_MCC:I
    .restart local v9       #featureset:Ljava/lang/String;
    .restart local v11       #iccid_mcc:Ljava/lang/String;
    .restart local v14       #preferredNetworkMode:I
    .restart local v16       #tzmonitor:Lcom/android/internal/telephony/LgeTimeZoneMonitor;
    :catch_219
    move-exception v7

    #@21a
    .line 156
    .local v7, e:Ljava/lang/NumberFormatException;
    const/4 v5, 0x0

    #@21b
    goto/16 :goto_84

    #@21d
    .line 189
    .end local v7           #e:Ljava/lang/NumberFormatException;
    .restart local v4       #DBnetworkMode:I
    :sswitch_21d
    :try_start_21d
    move-object/from16 v0, p0

    #@21f
    invoke-static {v0, v4}, Lcom/android/internal/telephony/PhoneFactory;->setGsmWcdmaNetworkmode(Landroid/content/Context;I)V

    #@222
    goto/16 :goto_114

    #@224
    .line 227
    .restart local v6       #cdmaSubscription:I
    .restart local v12       #networkMode:I
    .restart local v13       #phoneType:I
    :cond_224
    new-instance v17, Lcom/android/internal/telephony/PhoneProxy;

    #@226
    new-instance v19, Lcom/android/internal/telephony/gsm/GSMPhone;

    #@228
    sget-object v20, Lcom/android/internal/telephony/PhoneFactory;->sCommandsInterface:Lcom/android/internal/telephony/CommandsInterface;

    #@22a
    sget-object v21, Lcom/android/internal/telephony/PhoneFactory;->sPhoneNotifier:Lcom/android/internal/telephony/PhoneNotifier;

    #@22c
    move-object/from16 v0, v19

    #@22e
    move-object/from16 v1, p0

    #@230
    move-object/from16 v2, v20

    #@232
    move-object/from16 v3, v21

    #@234
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/gsm/GSMPhone;-><init>(Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;Lcom/android/internal/telephony/PhoneNotifier;)V

    #@237
    move-object/from16 v0, v17

    #@239
    move-object/from16 v1, v19

    #@23b
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/PhoneProxy;-><init>(Lcom/android/internal/telephony/PhoneBase;)V

    #@23e
    sput-object v17, Lcom/android/internal/telephony/PhoneFactory;->sProxyPhone:Lcom/android/internal/telephony/Phone;

    #@240
    goto/16 :goto_1b4

    #@242
    .line 232
    :cond_242
    const/16 v17, 0x2

    #@244
    move/from16 v0, v17

    #@246
    if-ne v13, v0, :cond_1b4

    #@248
    .line 233
    invoke-static {}, Landroid/telephony/TelephonyManager;->getLteOnCdmaModeStatic()I

    #@24b
    move-result v17

    #@24c
    packed-switch v17, :pswitch_data_2fc

    #@24f
    .line 241
    const-string v17, "PHONE"

    #@251
    const-string v19, "Creating CDMAPhone"

    #@253
    move-object/from16 v0, v17

    #@255
    move-object/from16 v1, v19

    #@257
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@25a
    .line 242
    new-instance v17, Lcom/android/internal/telephony/PhoneProxy;

    #@25c
    new-instance v19, Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@25e
    sget-object v20, Lcom/android/internal/telephony/PhoneFactory;->sCommandsInterface:Lcom/android/internal/telephony/CommandsInterface;

    #@260
    sget-object v21, Lcom/android/internal/telephony/PhoneFactory;->sPhoneNotifier:Lcom/android/internal/telephony/PhoneNotifier;

    #@262
    move-object/from16 v0, v19

    #@264
    move-object/from16 v1, p0

    #@266
    move-object/from16 v2, v20

    #@268
    move-object/from16 v3, v21

    #@26a
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cdma/CDMAPhone;-><init>(Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;Lcom/android/internal/telephony/PhoneNotifier;)V

    #@26d
    move-object/from16 v0, v17

    #@26f
    move-object/from16 v1, v19

    #@271
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/PhoneProxy;-><init>(Lcom/android/internal/telephony/PhoneBase;)V

    #@274
    sput-object v17, Lcom/android/internal/telephony/PhoneFactory;->sProxyPhone:Lcom/android/internal/telephony/Phone;

    #@276
    goto/16 :goto_1b4

    #@278
    .line 235
    :pswitch_278
    const-string v17, "PHONE"

    #@27a
    const-string v19, "Creating CDMALTEPhone"

    #@27c
    move-object/from16 v0, v17

    #@27e
    move-object/from16 v1, v19

    #@280
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@283
    .line 236
    new-instance v17, Lcom/android/internal/telephony/PhoneProxy;

    #@285
    new-instance v19, Lcom/android/internal/telephony/cdma/CDMALTEPhone;

    #@287
    sget-object v20, Lcom/android/internal/telephony/PhoneFactory;->sCommandsInterface:Lcom/android/internal/telephony/CommandsInterface;

    #@289
    sget-object v21, Lcom/android/internal/telephony/PhoneFactory;->sPhoneNotifier:Lcom/android/internal/telephony/PhoneNotifier;

    #@28b
    move-object/from16 v0, v19

    #@28d
    move-object/from16 v1, p0

    #@28f
    move-object/from16 v2, v20

    #@291
    move-object/from16 v3, v21

    #@293
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cdma/CDMALTEPhone;-><init>(Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;Lcom/android/internal/telephony/PhoneNotifier;)V

    #@296
    move-object/from16 v0, v17

    #@298
    move-object/from16 v1, v19

    #@29a
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/PhoneProxy;-><init>(Lcom/android/internal/telephony/PhoneBase;)V

    #@29d
    sput-object v17, Lcom/android/internal/telephony/PhoneFactory;->sProxyPhone:Lcom/android/internal/telephony/Phone;
    :try_end_29f
    .catchall {:try_start_21d .. :try_end_29f} :catchall_1f

    #@29f
    goto/16 :goto_1b4

    #@2a1
    .line 166
    nop

    #@2a2
    :sswitch_data_2a2
    .sparse-switch
        0x11e -> :sswitch_21d
        0x194 -> :sswitch_21d
        0x195 -> :sswitch_21d
        0x19f -> :sswitch_21d
        0x1a0 -> :sswitch_21d
        0x1a1 -> :sswitch_21d
        0x1a2 -> :sswitch_21d
        0x1a9 -> :sswitch_21d
        0x1b0 -> :sswitch_21d
        0x1c4 -> :sswitch_21d
        0x1fe -> :sswitch_21d
        0x208 -> :sswitch_21d
        0x212 -> :sswitch_21d
        0x25a -> :sswitch_21d
        0x25b -> :sswitch_21d
        0x25d -> :sswitch_21d
        0x264 -> :sswitch_21d
        0x26c -> :sswitch_21d
        0x26d -> :sswitch_21d
        0x277 -> :sswitch_21d
        0x27a -> :sswitch_21d
        0x27f -> :sswitch_21d
    .end sparse-switch

    #@2fc
    .line 233
    :pswitch_data_2fc
    .packed-switch 0x1
        :pswitch_278
    .end packed-switch
.end method

.method public static makeDefaultPhones(Landroid/content/Context;)V
    .registers 1
    .parameter "context"

    #@0
    .prologue
    .line 72
    invoke-static {p0}, Lcom/android/internal/telephony/PhoneFactory;->makeDefaultPhone(Landroid/content/Context;)V

    #@3
    .line 73
    return-void
.end method

.method public static makeSipPhone(Ljava/lang/String;)Lcom/android/internal/telephony/sip/SipPhone;
    .registers 3
    .parameter "sipUri"

    #@0
    .prologue
    .line 308
    sget-object v0, Lcom/android/internal/telephony/PhoneFactory;->sContext:Landroid/content/Context;

    #@2
    sget-object v1, Lcom/android/internal/telephony/PhoneFactory;->sPhoneNotifier:Lcom/android/internal/telephony/PhoneNotifier;

    #@4
    invoke-static {p0, v0, v1}, Lcom/android/internal/telephony/sip/SipPhoneFactory;->makePhone(Ljava/lang/String;Landroid/content/Context;Lcom/android/internal/telephony/PhoneNotifier;)Lcom/android/internal/telephony/sip/SipPhone;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method private static setGsmWcdmaNetworkmode(Landroid/content/Context;I)V
    .registers 5
    .parameter "context"
    .parameter "DBnetworkMode"

    #@0
    .prologue
    .line 322
    const/4 v1, 0x1

    #@1
    if-lt p1, v1, :cond_6

    #@3
    const/4 v1, 0x3

    #@4
    if-le p1, v1, :cond_17

    #@6
    .line 324
    :cond_6
    const/4 v0, 0x3

    #@7
    .line 325
    .local v0, preferredNetworkMode:I
    const-string v1, "PHONE"

    #@9
    const-string v2, "setGsmWcdmaNetworkmode---NETWORK_MODE_GSM_UMTS "

    #@b
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 326
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@11
    move-result-object v1

    #@12
    const-string v2, "preferred_network_mode"

    #@14
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@17
    .line 328
    .end local v0           #preferredNetworkMode:I
    :cond_17
    return-void
.end method
