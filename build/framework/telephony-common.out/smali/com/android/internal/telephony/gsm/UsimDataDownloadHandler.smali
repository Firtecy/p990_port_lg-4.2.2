.class public Lcom/android/internal/telephony/gsm/UsimDataDownloadHandler;
.super Landroid/os/Handler;
.source "UsimDataDownloadHandler.java"


# static fields
.field private static final BER_SMS_PP_DOWNLOAD_TAG:I = 0xd1

.field private static final DEV_ID_NETWORK:I = 0x83

.field private static final DEV_ID_UICC:I = 0x81

.field private static final EVENT_SEND_ENVELOPE_RESPONSE:I = 0x2

.field private static final EVENT_START_DATA_DOWNLOAD:I = 0x1

.field private static final TAG:Ljava/lang/String; = "UsimDataDownloadHandler"


# instance fields
.field private final mCI:Lcom/android/internal/telephony/CommandsInterface;


# direct methods
.method public constructor <init>(Lcom/android/internal/telephony/CommandsInterface;)V
    .registers 2
    .parameter "commandsInterface"

    #@0
    .prologue
    .line 55
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@3
    .line 56
    iput-object p1, p0, Lcom/android/internal/telephony/gsm/UsimDataDownloadHandler;->mCI:Lcom/android/internal/telephony/CommandsInterface;

    #@5
    .line 57
    return-void
.end method

.method private acknowledgeSmsWithError(I)V
    .registers 5
    .parameter "cause"

    #@0
    .prologue
    .line 223
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/UsimDataDownloadHandler;->mCI:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    const/4 v1, 0x0

    #@3
    const/4 v2, 0x0

    #@4
    invoke-interface {v0, v1, p1, v2}, Lcom/android/internal/telephony/CommandsInterface;->acknowledgeLastIncomingGsmSms(ZILandroid/os/Message;)V

    #@7
    .line 224
    return-void
.end method

.method private static getEnvelopeBodyLength(II)I
    .registers 4
    .parameter "scAddressLength"
    .parameter "tpduLength"

    #@0
    .prologue
    .line 146
    add-int/lit8 v0, p1, 0x5

    #@2
    .line 148
    .local v0, length:I
    const/16 v1, 0x7f

    #@4
    if-le p1, v1, :cond_f

    #@6
    const/4 v1, 0x2

    #@7
    :goto_7
    add-int/2addr v0, v1

    #@8
    .line 150
    if-eqz p0, :cond_e

    #@a
    .line 151
    add-int/lit8 v1, v0, 0x2

    #@c
    add-int v0, v1, p0

    #@e
    .line 153
    :cond_e
    return v0

    #@f
    .line 148
    :cond_f
    const/4 v1, 0x1

    #@10
    goto :goto_7
.end method

.method private handleDataDownload(Lcom/android/internal/telephony/gsm/SmsMessage;)V
    .registers 19
    .parameter "smsMessage"

    #@0
    .prologue
    .line 76
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/gsm/SmsMessage;->getDataCodingScheme()I

    #@3
    move-result v2

    #@4
    .line 77
    .local v2, dcs:I
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/gsm/SmsMessage;->getProtocolIdentifier()I

    #@7
    move-result v8

    #@8
    .line 78
    .local v8, pid:I
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/telephony/gsm/SmsMessage;->getPdu()[B

    #@b
    move-result-object v7

    #@c
    .line 80
    .local v7, pdu:[B
    const/4 v13, 0x0

    #@d
    aget-byte v13, v7, v13

    #@f
    and-int/lit16 v9, v13, 0xff

    #@11
    .line 81
    .local v9, scAddressLength:I
    add-int/lit8 v11, v9, 0x1

    #@13
    .line 82
    .local v11, tpduIndex:I
    array-length v13, v7

    #@14
    sub-int v12, v13, v11

    #@16
    .line 84
    .local v12, tpduLength:I
    invoke-static {v9, v12}, Lcom/android/internal/telephony/gsm/UsimDataDownloadHandler;->getEnvelopeBodyLength(II)I

    #@19
    move-result v1

    #@1a
    .line 88
    .local v1, bodyLength:I
    add-int/lit8 v14, v1, 0x1

    #@1c
    const/16 v13, 0x7f

    #@1e
    if-le v1, v13, :cond_a4

    #@20
    const/4 v13, 0x2

    #@21
    :goto_21
    add-int v10, v14, v13

    #@23
    .line 90
    .local v10, totalLength:I
    new-array v4, v10, [B

    #@25
    .line 91
    .local v4, envelope:[B
    const/4 v5, 0x0

    #@26
    .line 94
    .local v5, index:I
    add-int/lit8 v6, v5, 0x1

    #@28
    .end local v5           #index:I
    .local v6, index:I
    const/16 v13, -0x2f

    #@2a
    aput-byte v13, v4, v5

    #@2c
    .line 95
    const/16 v13, 0x7f

    #@2e
    if-le v1, v13, :cond_c9

    #@30
    .line 96
    add-int/lit8 v5, v6, 0x1

    #@32
    .end local v6           #index:I
    .restart local v5       #index:I
    const/16 v13, -0x7f

    #@34
    aput-byte v13, v4, v6

    #@36
    .line 98
    :goto_36
    add-int/lit8 v6, v5, 0x1

    #@38
    .end local v5           #index:I
    .restart local v6       #index:I
    int-to-byte v13, v1

    #@39
    aput-byte v13, v4, v5

    #@3b
    .line 101
    add-int/lit8 v5, v6, 0x1

    #@3d
    .end local v6           #index:I
    .restart local v5       #index:I
    sget-object v13, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->DEVICE_IDENTITIES:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@3f
    invoke-virtual {v13}, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->value()I

    #@42
    move-result v13

    #@43
    or-int/lit16 v13, v13, 0x80

    #@45
    int-to-byte v13, v13

    #@46
    aput-byte v13, v4, v6

    #@48
    .line 102
    add-int/lit8 v6, v5, 0x1

    #@4a
    .end local v5           #index:I
    .restart local v6       #index:I
    const/4 v13, 0x2

    #@4b
    aput-byte v13, v4, v5

    #@4d
    .line 103
    add-int/lit8 v5, v6, 0x1

    #@4f
    .end local v6           #index:I
    .restart local v5       #index:I
    const/16 v13, -0x7d

    #@51
    aput-byte v13, v4, v6

    #@53
    .line 104
    add-int/lit8 v6, v5, 0x1

    #@55
    .end local v5           #index:I
    .restart local v6       #index:I
    const/16 v13, -0x7f

    #@57
    aput-byte v13, v4, v5

    #@59
    .line 107
    if-eqz v9, :cond_c7

    #@5b
    .line 108
    add-int/lit8 v5, v6, 0x1

    #@5d
    .end local v6           #index:I
    .restart local v5       #index:I
    sget-object v13, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->ADDRESS:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@5f
    invoke-virtual {v13}, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->value()I

    #@62
    move-result v13

    #@63
    int-to-byte v13, v13

    #@64
    aput-byte v13, v4, v6

    #@66
    .line 109
    add-int/lit8 v6, v5, 0x1

    #@68
    .end local v5           #index:I
    .restart local v6       #index:I
    int-to-byte v13, v9

    #@69
    aput-byte v13, v4, v5

    #@6b
    .line 110
    const/4 v13, 0x1

    #@6c
    invoke-static {v7, v13, v4, v6, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@6f
    .line 111
    add-int v5, v6, v9

    #@71
    .line 115
    .end local v6           #index:I
    .restart local v5       #index:I
    :goto_71
    add-int/lit8 v6, v5, 0x1

    #@73
    .end local v5           #index:I
    .restart local v6       #index:I
    sget-object v13, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->SMS_TPDU:Lcom/android/internal/telephony/cat/ComprehensionTlvTag;

    #@75
    invoke-virtual {v13}, Lcom/android/internal/telephony/cat/ComprehensionTlvTag;->value()I

    #@78
    move-result v13

    #@79
    or-int/lit16 v13, v13, 0x80

    #@7b
    int-to-byte v13, v13

    #@7c
    aput-byte v13, v4, v5

    #@7e
    .line 116
    const/16 v13, 0x7f

    #@80
    if-le v12, v13, :cond_c5

    #@82
    .line 117
    add-int/lit8 v5, v6, 0x1

    #@84
    .end local v6           #index:I
    .restart local v5       #index:I
    const/16 v13, -0x7f

    #@86
    aput-byte v13, v4, v6

    #@88
    .line 119
    :goto_88
    add-int/lit8 v6, v5, 0x1

    #@8a
    .end local v5           #index:I
    .restart local v6       #index:I
    int-to-byte v13, v12

    #@8b
    aput-byte v13, v4, v5

    #@8d
    .line 120
    invoke-static {v7, v11, v4, v6, v12}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@90
    .line 121
    add-int v5, v6, v12

    #@92
    .line 124
    .end local v6           #index:I
    .restart local v5       #index:I
    array-length v13, v4

    #@93
    if-eq v5, v13, :cond_a7

    #@95
    .line 125
    const-string v13, "UsimDataDownloadHandler"

    #@97
    const-string v14, "startDataDownload() calculated incorrect envelope length, aborting."

    #@99
    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@9c
    .line 126
    const/16 v13, 0xff

    #@9e
    move-object/from16 v0, p0

    #@a0
    invoke-direct {v0, v13}, Lcom/android/internal/telephony/gsm/UsimDataDownloadHandler;->acknowledgeSmsWithError(I)V

    #@a3
    .line 133
    :goto_a3
    return-void

    #@a4
    .line 88
    .end local v4           #envelope:[B
    .end local v5           #index:I
    .end local v10           #totalLength:I
    :cond_a4
    const/4 v13, 0x1

    #@a5
    goto/16 :goto_21

    #@a7
    .line 130
    .restart local v4       #envelope:[B
    .restart local v5       #index:I
    .restart local v10       #totalLength:I
    :cond_a7
    invoke-static {v4}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@aa
    move-result-object v3

    #@ab
    .line 131
    .local v3, encodedEnvelope:Ljava/lang/String;
    move-object/from16 v0, p0

    #@ad
    iget-object v13, v0, Lcom/android/internal/telephony/gsm/UsimDataDownloadHandler;->mCI:Lcom/android/internal/telephony/CommandsInterface;

    #@af
    const/4 v14, 0x2

    #@b0
    const/4 v15, 0x2

    #@b1
    new-array v15, v15, [I

    #@b3
    const/16 v16, 0x0

    #@b5
    aput v2, v15, v16

    #@b7
    const/16 v16, 0x1

    #@b9
    aput v8, v15, v16

    #@bb
    move-object/from16 v0, p0

    #@bd
    invoke-virtual {v0, v14, v15}, Lcom/android/internal/telephony/gsm/UsimDataDownloadHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@c0
    move-result-object v14

    #@c1
    invoke-interface {v13, v3, v14}, Lcom/android/internal/telephony/CommandsInterface;->sendEnvelopeWithStatus(Ljava/lang/String;Landroid/os/Message;)V

    #@c4
    goto :goto_a3

    #@c5
    .end local v3           #encodedEnvelope:Ljava/lang/String;
    .end local v5           #index:I
    .restart local v6       #index:I
    :cond_c5
    move v5, v6

    #@c6
    .end local v6           #index:I
    .restart local v5       #index:I
    goto :goto_88

    #@c7
    .end local v5           #index:I
    .restart local v6       #index:I
    :cond_c7
    move v5, v6

    #@c8
    .end local v6           #index:I
    .restart local v5       #index:I
    goto :goto_71

    #@c9
    .end local v5           #index:I
    .restart local v6       #index:I
    :cond_c9
    move v5, v6

    #@ca
    .end local v6           #index:I
    .restart local v5       #index:I
    goto/16 :goto_36
.end method

.method private static is7bitDcs(I)Z
    .registers 3
    .parameter "dcs"

    #@0
    .prologue
    .line 234
    and-int/lit16 v0, p0, 0x8c

    #@2
    if-eqz v0, :cond_a

    #@4
    and-int/lit16 v0, p0, 0xf4

    #@6
    const/16 v1, 0xf0

    #@8
    if-ne v0, v1, :cond_c

    #@a
    :cond_a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method private sendSmsAckForEnvelopeResponse(Lcom/android/internal/telephony/uicc/IccIoResult;II)V
    .registers 16
    .parameter "response"
    .parameter "dcs"
    .parameter "pid"

    #@0
    .prologue
    .line 162
    iget v6, p1, Lcom/android/internal/telephony/uicc/IccIoResult;->sw1:I

    #@2
    .line 163
    .local v6, sw1:I
    iget v7, p1, Lcom/android/internal/telephony/uicc/IccIoResult;->sw2:I

    #@4
    .line 166
    .local v7, sw2:I
    const/16 v8, 0x90

    #@6
    if-ne v6, v8, :cond_a

    #@8
    if-eqz v7, :cond_e

    #@a
    :cond_a
    const/16 v8, 0x91

    #@c
    if-ne v6, v8, :cond_3d

    #@e
    .line 167
    :cond_e
    const-string v8, "UsimDataDownloadHandler"

    #@10
    new-instance v9, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v10, "USIM data download succeeded: "

    #@17
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v9

    #@1b
    invoke-virtual {p1}, Lcom/android/internal/telephony/uicc/IccIoResult;->toString()Ljava/lang/String;

    #@1e
    move-result-object v10

    #@1f
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v9

    #@23
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v9

    #@27
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 168
    const/4 v5, 0x1

    #@2b
    .line 181
    .local v5, success:Z
    :goto_2b
    iget-object v2, p1, Lcom/android/internal/telephony/uicc/IccIoResult;->payload:[B

    #@2d
    .line 182
    .local v2, responseBytes:[B
    if-eqz v2, :cond_32

    #@2f
    array-length v8, v2

    #@30
    if-nez v8, :cond_9a

    #@32
    .line 183
    :cond_32
    if-eqz v5, :cond_94

    #@34
    .line 184
    iget-object v8, p0, Lcom/android/internal/telephony/gsm/UsimDataDownloadHandler;->mCI:Lcom/android/internal/telephony/CommandsInterface;

    #@36
    const/4 v9, 0x1

    #@37
    const/4 v10, 0x0

    #@38
    const/4 v11, 0x0

    #@39
    invoke-interface {v8, v9, v10, v11}, Lcom/android/internal/telephony/CommandsInterface;->acknowledgeLastIncomingGsmSms(ZILandroid/os/Message;)V

    #@3c
    .line 220
    .end local v2           #responseBytes:[B
    .end local v5           #success:Z
    :goto_3c
    return-void

    #@3d
    .line 169
    :cond_3d
    const/16 v8, 0x93

    #@3f
    if-ne v6, v8, :cond_50

    #@41
    if-nez v7, :cond_50

    #@43
    .line 170
    const-string v8, "UsimDataDownloadHandler"

    #@45
    const-string v9, "USIM data download failed: Toolkit busy"

    #@47
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4a
    .line 171
    const/16 v8, 0xd4

    #@4c
    invoke-direct {p0, v8}, Lcom/android/internal/telephony/gsm/UsimDataDownloadHandler;->acknowledgeSmsWithError(I)V

    #@4f
    goto :goto_3c

    #@50
    .line 173
    :cond_50
    const/16 v8, 0x62

    #@52
    if-eq v6, v8, :cond_58

    #@54
    const/16 v8, 0x63

    #@56
    if-ne v6, v8, :cond_76

    #@58
    .line 174
    :cond_58
    const-string v8, "UsimDataDownloadHandler"

    #@5a
    new-instance v9, Ljava/lang/StringBuilder;

    #@5c
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@5f
    const-string v10, "USIM data download failed: "

    #@61
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v9

    #@65
    invoke-virtual {p1}, Lcom/android/internal/telephony/uicc/IccIoResult;->toString()Ljava/lang/String;

    #@68
    move-result-object v10

    #@69
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v9

    #@6d
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@70
    move-result-object v9

    #@71
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@74
    .line 175
    const/4 v5, 0x0

    #@75
    .restart local v5       #success:Z
    goto :goto_2b

    #@76
    .line 177
    .end local v5           #success:Z
    :cond_76
    const-string v8, "UsimDataDownloadHandler"

    #@78
    new-instance v9, Ljava/lang/StringBuilder;

    #@7a
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@7d
    const-string v10, "Unexpected SW1/SW2 response from UICC: "

    #@7f
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v9

    #@83
    invoke-virtual {p1}, Lcom/android/internal/telephony/uicc/IccIoResult;->toString()Ljava/lang/String;

    #@86
    move-result-object v10

    #@87
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v9

    #@8b
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8e
    move-result-object v9

    #@8f
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@92
    .line 178
    const/4 v5, 0x0

    #@93
    .restart local v5       #success:Z
    goto :goto_2b

    #@94
    .line 186
    .restart local v2       #responseBytes:[B
    :cond_94
    const/16 v8, 0xd5

    #@96
    invoke-direct {p0, v8}, Lcom/android/internal/telephony/gsm/UsimDataDownloadHandler;->acknowledgeSmsWithError(I)V

    #@99
    goto :goto_3c

    #@9a
    .line 193
    :cond_9a
    const/4 v0, 0x0

    #@9b
    .line 194
    .local v0, index:I
    if-eqz v5, :cond_d8

    #@9d
    .line 195
    array-length v8, v2

    #@9e
    add-int/lit8 v8, v8, 0x5

    #@a0
    new-array v4, v8, [B

    #@a2
    .line 196
    .local v4, smsAckPdu:[B
    add-int/lit8 v1, v0, 0x1

    #@a4
    .end local v0           #index:I
    .local v1, index:I
    const/4 v8, 0x0

    #@a5
    aput-byte v8, v4, v0

    #@a7
    .line 197
    add-int/lit8 v0, v1, 0x1

    #@a9
    .end local v1           #index:I
    .restart local v0       #index:I
    const/4 v8, 0x7

    #@aa
    aput-byte v8, v4, v1

    #@ac
    .line 206
    :goto_ac
    add-int/lit8 v1, v0, 0x1

    #@ae
    .end local v0           #index:I
    .restart local v1       #index:I
    int-to-byte v8, p3

    #@af
    aput-byte v8, v4, v0

    #@b1
    .line 207
    add-int/lit8 v0, v1, 0x1

    #@b3
    .end local v1           #index:I
    .restart local v0       #index:I
    int-to-byte v8, p2

    #@b4
    aput-byte v8, v4, v1

    #@b6
    .line 209
    invoke-static {p2}, Lcom/android/internal/telephony/gsm/UsimDataDownloadHandler;->is7bitDcs(I)Z

    #@b9
    move-result v8

    #@ba
    if-eqz v8, :cond_ef

    #@bc
    .line 210
    array-length v8, v2

    #@bd
    mul-int/lit8 v8, v8, 0x8

    #@bf
    div-int/lit8 v3, v8, 0x7

    #@c1
    .line 211
    .local v3, septetCount:I
    add-int/lit8 v1, v0, 0x1

    #@c3
    .end local v0           #index:I
    .restart local v1       #index:I
    int-to-byte v8, v3

    #@c4
    aput-byte v8, v4, v0

    #@c6
    move v0, v1

    #@c7
    .line 216
    .end local v1           #index:I
    .end local v3           #septetCount:I
    .restart local v0       #index:I
    :goto_c7
    const/4 v8, 0x0

    #@c8
    array-length v9, v2

    #@c9
    invoke-static {v2, v8, v4, v0, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@cc
    .line 218
    iget-object v8, p0, Lcom/android/internal/telephony/gsm/UsimDataDownloadHandler;->mCI:Lcom/android/internal/telephony/CommandsInterface;

    #@ce
    invoke-static {v4}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@d1
    move-result-object v9

    #@d2
    const/4 v10, 0x0

    #@d3
    invoke-interface {v8, v5, v9, v10}, Lcom/android/internal/telephony/CommandsInterface;->acknowledgeIncomingGsmSmsWithPdu(ZLjava/lang/String;Landroid/os/Message;)V

    #@d6
    goto/16 :goto_3c

    #@d8
    .line 199
    .end local v4           #smsAckPdu:[B
    :cond_d8
    array-length v8, v2

    #@d9
    add-int/lit8 v8, v8, 0x6

    #@db
    new-array v4, v8, [B

    #@dd
    .line 200
    .restart local v4       #smsAckPdu:[B
    add-int/lit8 v1, v0, 0x1

    #@df
    .end local v0           #index:I
    .restart local v1       #index:I
    const/4 v8, 0x0

    #@e0
    aput-byte v8, v4, v0

    #@e2
    .line 201
    add-int/lit8 v0, v1, 0x1

    #@e4
    .end local v1           #index:I
    .restart local v0       #index:I
    const/16 v8, -0x2b

    #@e6
    aput-byte v8, v4, v1

    #@e8
    .line 203
    add-int/lit8 v1, v0, 0x1

    #@ea
    .end local v0           #index:I
    .restart local v1       #index:I
    const/4 v8, 0x7

    #@eb
    aput-byte v8, v4, v0

    #@ed
    move v0, v1

    #@ee
    .end local v1           #index:I
    .restart local v0       #index:I
    goto :goto_ac

    #@ef
    .line 213
    :cond_ef
    add-int/lit8 v1, v0, 0x1

    #@f1
    .end local v0           #index:I
    .restart local v1       #index:I
    array-length v8, v2

    #@f2
    int-to-byte v8, v8

    #@f3
    aput-byte v8, v4, v0

    #@f5
    move v0, v1

    #@f6
    .end local v1           #index:I
    .restart local v0       #index:I
    goto :goto_c7
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 7
    .parameter "msg"

    #@0
    .prologue
    .line 244
    iget v2, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v2, :pswitch_data_66

    #@5
    .line 264
    const-string v2, "UsimDataDownloadHandler"

    #@7
    new-instance v3, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v4, "Ignoring unexpected message, what="

    #@e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v3

    #@12
    iget v4, p1, Landroid/os/Message;->what:I

    #@14
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v3

    #@18
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v3

    #@1c
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 266
    :goto_1f
    return-void

    #@20
    .line 246
    :pswitch_20
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@22
    check-cast v2, Lcom/android/internal/telephony/gsm/SmsMessage;

    #@24
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/gsm/UsimDataDownloadHandler;->handleDataDownload(Lcom/android/internal/telephony/gsm/SmsMessage;)V

    #@27
    goto :goto_1f

    #@28
    .line 250
    :pswitch_28
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2a
    check-cast v0, Landroid/os/AsyncResult;

    #@2c
    .line 252
    .local v0, ar:Landroid/os/AsyncResult;
    iget-object v2, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@2e
    if-eqz v2, :cond_50

    #@30
    .line 253
    const-string v2, "UsimDataDownloadHandler"

    #@32
    new-instance v3, Ljava/lang/StringBuilder;

    #@34
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@37
    const-string v4, "UICC Send Envelope failure, exception: "

    #@39
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v3

    #@3d
    iget-object v4, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@3f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v3

    #@43
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v3

    #@47
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4a
    .line 254
    const/16 v2, 0xd5

    #@4c
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/gsm/UsimDataDownloadHandler;->acknowledgeSmsWithError(I)V

    #@4f
    goto :goto_1f

    #@50
    .line 259
    :cond_50
    iget-object v2, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@52
    check-cast v2, [I

    #@54
    move-object v1, v2

    #@55
    check-cast v1, [I

    #@57
    .line 260
    .local v1, dcsPid:[I
    iget-object v2, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@59
    check-cast v2, Lcom/android/internal/telephony/uicc/IccIoResult;

    #@5b
    const/4 v3, 0x0

    #@5c
    aget v3, v1, v3

    #@5e
    const/4 v4, 0x1

    #@5f
    aget v4, v1, v4

    #@61
    invoke-direct {p0, v2, v3, v4}, Lcom/android/internal/telephony/gsm/UsimDataDownloadHandler;->sendSmsAckForEnvelopeResponse(Lcom/android/internal/telephony/uicc/IccIoResult;II)V

    #@64
    goto :goto_1f

    #@65
    .line 244
    nop

    #@66
    :pswitch_data_66
    .packed-switch 0x1
        :pswitch_20
        :pswitch_28
    .end packed-switch
.end method

.method public startDataDownload(Lcom/android/internal/telephony/gsm/SmsMessage;)I
    .registers 4
    .parameter "smsMessage"

    #@0
    .prologue
    .line 67
    const/4 v0, 0x1

    #@1
    invoke-virtual {p0, v0, p1}, Lcom/android/internal/telephony/gsm/UsimDataDownloadHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@4
    move-result-object v0

    #@5
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/UsimDataDownloadHandler;->sendMessage(Landroid/os/Message;)Z

    #@8
    move-result v0

    #@9
    if-eqz v0, :cond_d

    #@b
    .line 68
    const/4 v0, -0x1

    #@c
    .line 71
    :goto_c
    return v0

    #@d
    .line 70
    :cond_d
    const-string v0, "UsimDataDownloadHandler"

    #@f
    const-string v1, "startDataDownload failed to send message to start data download."

    #@11
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@14
    .line 71
    const/4 v0, 0x2

    #@15
    goto :goto_c
.end method
