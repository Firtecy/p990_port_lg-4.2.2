.class public Lcom/android/internal/telephony/PhoneProxy;
.super Landroid/os/Handler;
.source "PhoneProxy.java"

# interfaces
.implements Lcom/android/internal/telephony/Phone;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/PhoneProxy$MyHandler;
    }
.end annotation


# static fields
.field private static final ACTION_PREFERRED_NETWORK_GET:Ljava/lang/String; = "GetNetworkMode_KDDI_LTE"

.field private static final ACTION_PREFERRED_NETWORK_SET:Ljava/lang/String; = "SetNetworkMode_KDDI_LTE"

.field private static final EVENT_RADIO_ON:I = 0x2

.field private static final EVENT_REQUEST_VOICE_RADIO_TECH_DONE:I = 0x3

.field private static final EVENT_RIL_CONNECTED:I = 0x4

.field private static final EVENT_VOICE_RADIO_TECH_CHANGED:I = 0x1

.field private static final EXTRA_NATWORK_RESPONSE:Ljava/lang/String; = "response"

.field private static final EXTRA_NETWORK_TYPE:Ljava/lang/String; = "NetworkType"

.field protected static final LOG_TAG:Ljava/lang/String; = "PHONE"

.field public static final NT_MODE_DEFAULT:I = 0x0

.field public static final NT_MODE_LTEOFF:I = 0x1

.field public static final lockForRadioTechnologyChange:Ljava/lang/Object;


# instance fields
.field protected mActivePhone:Lcom/android/internal/telephony/Phone;

.field protected mCommandsInterface:Lcom/android/internal/telephony/CommandsInterface;

.field public mHandler:Lcom/android/internal/telephony/PhoneProxy$MyHandler;

.field protected mIccCardProxy:Lcom/android/internal/telephony/uicc/IccCardProxy;

.field protected mIccPhoneBookInterfaceManagerProxy:Lcom/android/internal/telephony/IccPhoneBookInterfaceManagerProxy;

.field protected mIccSmsInterfaceManager:Lcom/android/internal/telephony/IccSmsInterfaceManager;

.field mIntentDuplicate:Z

.field protected mLgeRssiData:Lcom/android/internal/telephony/LgeRssiData;

.field private mLgeUiccMgr:Lcom/android/internal/telephony/uicc/LgeUiccManager;

.field protected mPhoneSubInfoProxy:Lcom/android/internal/telephony/PhoneSubInfoProxy;

.field private mPreferredNetworkBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mResetModemOnRadioTechnologyChange:Z

.field private mRilVersion:I

.field setNetworkType:I

.field setPreferredNetworkType:I

.field settingsNetworkMode:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 76
    new-instance v0, Ljava/lang/Object;

    #@2
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@5
    sput-object v0, Lcom/android/internal/telephony/PhoneProxy;->lockForRadioTechnologyChange:Ljava/lang/Object;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 1271
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@3
    .line 88
    const/4 v0, 0x0

    #@4
    iput-boolean v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mResetModemOnRadioTechnologyChange:Z

    #@6
    .line 116
    const/4 v0, 0x1

    #@7
    iput-boolean v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mIntentDuplicate:Z

    #@9
    .line 1705
    new-instance v0, Lcom/android/internal/telephony/PhoneProxy$1;

    #@b
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/PhoneProxy$1;-><init>(Lcom/android/internal/telephony/PhoneProxy;)V

    #@e
    iput-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mPreferredNetworkBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@10
    .line 1272
    return-void
.end method

.method public constructor <init>(Lcom/android/internal/telephony/PhoneBase;)V
    .registers 7
    .parameter "phone"

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    const/4 v1, 0x0

    #@2
    const/4 v3, 0x0

    #@3
    const/4 v2, 0x1

    #@4
    .line 122
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@7
    .line 88
    iput-boolean v1, p0, Lcom/android/internal/telephony/PhoneProxy;->mResetModemOnRadioTechnologyChange:Z

    #@9
    .line 116
    iput-boolean v2, p0, Lcom/android/internal/telephony/PhoneProxy;->mIntentDuplicate:Z

    #@b
    .line 1705
    new-instance v0, Lcom/android/internal/telephony/PhoneProxy$1;

    #@d
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/PhoneProxy$1;-><init>(Lcom/android/internal/telephony/PhoneProxy;)V

    #@10
    iput-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mPreferredNetworkBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@12
    .line 123
    iput-object p1, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@14
    .line 124
    const-string v0, "persist.radio.reset_on_switch"

    #@16
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@19
    move-result v0

    #@1a
    iput-boolean v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mResetModemOnRadioTechnologyChange:Z

    #@1c
    .line 126
    new-instance v0, Lcom/android/internal/telephony/IccPhoneBookInterfaceManagerProxy;

    #@1e
    invoke-virtual {p1}, Lcom/android/internal/telephony/PhoneBase;->getIccPhoneBookInterfaceManager()Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;

    #@21
    move-result-object v1

    #@22
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/IccPhoneBookInterfaceManagerProxy;-><init>(Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;)V

    #@25
    iput-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mIccPhoneBookInterfaceManagerProxy:Lcom/android/internal/telephony/IccPhoneBookInterfaceManagerProxy;

    #@27
    .line 128
    new-instance v0, Lcom/android/internal/telephony/PhoneSubInfoProxy;

    #@29
    invoke-virtual {p1}, Lcom/android/internal/telephony/PhoneBase;->getPhoneSubInfo()Lcom/android/internal/telephony/PhoneSubInfo;

    #@2c
    move-result-object v1

    #@2d
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/PhoneSubInfoProxy;-><init>(Lcom/android/internal/telephony/PhoneSubInfo;)V

    #@30
    iput-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mPhoneSubInfoProxy:Lcom/android/internal/telephony/PhoneSubInfoProxy;

    #@32
    .line 129
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@34
    check-cast v0, Lcom/android/internal/telephony/PhoneBase;

    #@36
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@38
    iput-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mCommandsInterface:Lcom/android/internal/telephony/CommandsInterface;

    #@3a
    .line 131
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mCommandsInterface:Lcom/android/internal/telephony/CommandsInterface;

    #@3c
    const/4 v1, 0x4

    #@3d
    invoke-interface {v0, p0, v1, v3}, Lcom/android/internal/telephony/CommandsInterface;->registerForRilConnected(Landroid/os/Handler;ILjava/lang/Object;)V

    #@40
    .line 132
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mCommandsInterface:Lcom/android/internal/telephony/CommandsInterface;

    #@42
    invoke-interface {v0, p0, v4, v3}, Lcom/android/internal/telephony/CommandsInterface;->registerForOn(Landroid/os/Handler;ILjava/lang/Object;)V

    #@45
    .line 133
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mCommandsInterface:Lcom/android/internal/telephony/CommandsInterface;

    #@47
    invoke-interface {v0, p0, v2, v3}, Lcom/android/internal/telephony/CommandsInterface;->registerForVoiceRadioTechChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@4a
    .line 136
    invoke-virtual {p0}, Lcom/android/internal/telephony/PhoneProxy;->init()V

    #@4d
    .line 138
    invoke-virtual {p1}, Lcom/android/internal/telephony/PhoneBase;->getPhoneType()I

    #@50
    move-result v0

    #@51
    if-ne v0, v2, :cond_77

    #@53
    .line 140
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mIccCardProxy:Lcom/android/internal/telephony/uicc/IccCardProxy;

    #@55
    const/4 v1, 0x3

    #@56
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/uicc/IccCardProxy;->setVoiceRadioTech(I)V

    #@59
    .line 146
    :cond_59
    :goto_59
    new-instance v0, Lcom/android/internal/telephony/uicc/LgeUiccManager;

    #@5b
    invoke-direct {v0, p1}, Lcom/android/internal/telephony/uicc/LgeUiccManager;-><init>(Lcom/android/internal/telephony/Phone;)V

    #@5e
    iput-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mLgeUiccMgr:Lcom/android/internal/telephony/uicc/LgeUiccManager;

    #@60
    .line 150
    invoke-static {}, Lcom/android/internal/telephony/LgeRssiData;->getInstance()Lcom/android/internal/telephony/LgeRssiData;

    #@63
    move-result-object v0

    #@64
    iput-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mLgeRssiData:Lcom/android/internal/telephony/LgeRssiData;

    #@66
    .line 151
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mLgeRssiData:Lcom/android/internal/telephony/LgeRssiData;

    #@68
    invoke-virtual {v0}, Lcom/android/internal/telephony/LgeRssiData;->init()V

    #@6b
    .line 152
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@6d
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getServiceState()Landroid/telephony/ServiceState;

    #@70
    move-result-object v0

    #@71
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneProxy;->mLgeRssiData:Lcom/android/internal/telephony/LgeRssiData;

    #@73
    invoke-virtual {v0, v1}, Landroid/telephony/ServiceState;->setLgeRssiData(Lcom/android/internal/telephony/LgeRssiData;)V

    #@76
    .line 154
    return-void

    #@77
    .line 141
    :cond_77
    invoke-virtual {p1}, Lcom/android/internal/telephony/PhoneBase;->getPhoneType()I

    #@7a
    move-result v0

    #@7b
    if-ne v0, v4, :cond_59

    #@7d
    .line 142
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mIccCardProxy:Lcom/android/internal/telephony/uicc/IccCardProxy;

    #@7f
    const/4 v1, 0x6

    #@80
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/uicc/IccCardProxy;->setVoiceRadioTech(I)V

    #@83
    goto :goto_59
.end method

.method static synthetic access$000(Ljava/lang/String;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 75
    invoke-static {p0}, Lcom/android/internal/telephony/PhoneProxy;->logd(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method private deleteAndCreatePhone(I)V
    .registers 6
    .parameter "newVoiceRadioTech"

    #@0
    .prologue
    .line 334
    const-string v1, "Unknown"

    #@2
    .line 335
    .local v1, outgoingPhoneName:Ljava/lang/String;
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@4
    .line 337
    .local v0, oldPhone:Lcom/android/internal/telephony/Phone;
    if-eqz v0, :cond_d

    #@6
    move-object v2, v0

    #@7
    .line 338
    check-cast v2, Lcom/android/internal/telephony/PhoneBase;

    #@9
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getPhoneName()Ljava/lang/String;

    #@c
    move-result-object v1

    #@d
    .line 341
    :cond_d
    new-instance v2, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v3, "Switching Voice Phone : "

    #@14
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    const-string v3, " >>> "

    #@1e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v3

    #@22
    invoke-static {p1}, Landroid/telephony/ServiceState;->isGsm(I)Z

    #@25
    move-result v2

    #@26
    if-eqz v2, :cond_5d

    #@28
    const-string v2, "GSM"

    #@2a
    :goto_2a
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v2

    #@2e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v2

    #@32
    invoke-static {v2}, Lcom/android/internal/telephony/PhoneProxy;->logd(Ljava/lang/String;)V

    #@35
    .line 344
    if-eqz v0, :cond_46

    #@37
    .line 345
    invoke-static {}, Lcom/android/internal/telephony/CallManager;->getInstance()Lcom/android/internal/telephony/CallManager;

    #@3a
    move-result-object v2

    #@3b
    invoke-virtual {v2, v0}, Lcom/android/internal/telephony/CallManager;->unregisterPhone(Lcom/android/internal/telephony/Phone;)V

    #@3e
    .line 346
    const-string v2, "Disposing old phone.."

    #@40
    invoke-static {v2}, Lcom/android/internal/telephony/PhoneProxy;->logd(Ljava/lang/String;)V

    #@43
    .line 347
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->dispose()V

    #@46
    .line 357
    :cond_46
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/PhoneProxy;->createNewPhone(I)V

    #@49
    .line 359
    if-eqz v0, :cond_4e

    #@4b
    .line 360
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->removeReferences()V

    #@4e
    .line 363
    :cond_4e
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@50
    if-eqz v2, :cond_5b

    #@52
    .line 364
    invoke-static {}, Lcom/android/internal/telephony/CallManager;->getInstance()Lcom/android/internal/telephony/CallManager;

    #@55
    move-result-object v2

    #@56
    iget-object v3, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@58
    invoke-virtual {v2, v3}, Lcom/android/internal/telephony/CallManager;->registerPhone(Lcom/android/internal/telephony/Phone;)Z

    #@5b
    .line 367
    :cond_5b
    const/4 v0, 0x0

    #@5c
    .line 368
    return-void

    #@5d
    .line 341
    :cond_5d
    const-string v2, "CDMA"

    #@5f
    goto :goto_2a
.end method

.method private getPhoneType_inLTE()I
    .registers 6

    #@0
    .prologue
    const/16 v4, 0xa

    #@2
    .line 1687
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@4
    invoke-interface {v2}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@7
    move-result-object v2

    #@8
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@b
    move-result-object v2

    #@c
    const-string v3, "preferred_network_mode"

    #@e
    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@11
    move-result v0

    #@12
    .line 1691
    .local v0, networkMode:I
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@15
    invoke-static {v0}, Landroid/telephony/TelephonyManager;->getPhoneType(I)I

    #@18
    move-result v1

    #@19
    .line 1693
    .local v1, phoneType:I
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->isNetworkRoaming()Z

    #@20
    move-result v2

    #@21
    if-eqz v2, :cond_26

    #@23
    .line 1694
    if-ne v0, v4, :cond_26

    #@25
    .line 1695
    const/4 v1, 0x1

    #@26
    .line 1699
    :cond_26
    new-instance v2, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    const-string v3, "getPhoneType_inLTE()  phoneType="

    #@2d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v2

    #@31
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@34
    move-result-object v2

    #@35
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v2

    #@39
    invoke-static {v2}, Lcom/android/internal/telephony/PhoneProxy;->logd(Ljava/lang/String;)V

    #@3c
    .line 1700
    return v1
.end method

.method private static logd(Ljava/lang/String;)V
    .registers 4
    .parameter "msg"

    #@0
    .prologue
    .line 213
    const-string v0, "PHONE"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[PhoneProxy] "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 214
    return-void
.end method

.method private loge(Ljava/lang/String;)V
    .registers 5
    .parameter "msg"

    #@0
    .prologue
    .line 221
    const-string v0, "PHONE"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[PhoneProxy] "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 222
    return-void
.end method

.method private logw(Ljava/lang/String;)V
    .registers 5
    .parameter "msg"

    #@0
    .prologue
    .line 217
    const-string v0, "PHONE"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[PhoneProxy] "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 218
    return-void
.end method


# virtual methods
.method public IsVMNumberNotInSIM()Z
    .registers 2

    #@0
    .prologue
    .line 794
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->IsVMNumberNotInSIM()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public PlayVZWERISound()V
    .registers 2

    #@0
    .prologue
    .line 1089
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->PlayVZWERISound()V

    #@5
    .line 1090
    return-void
.end method

.method public StopVZWERISound()V
    .registers 2

    #@0
    .prologue
    .line 1093
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->StopVZWERISound()V

    #@5
    .line 1094
    return-void
.end method

.method public acceptCall()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 625
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->acceptCall()V

    #@5
    .line 626
    return-void
.end method

.method public acceptCall(I)V
    .registers 3
    .parameter "callType"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 629
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->acceptCall(I)V

    #@5
    .line 630
    return-void
.end method

.method public acceptConnectionTypeChange(Lcom/android/internal/telephony/Connection;Ljava/util/Map;)V
    .registers 4
    .parameter "conn"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/internal/telephony/Connection;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 663
    .local p2, newExtras:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/Phone;->acceptConnectionTypeChange(Lcom/android/internal/telephony/Connection;Ljava/util/Map;)V

    #@5
    .line 664
    return-void
.end method

.method public activateCellBroadcastSms(ILandroid/os/Message;)V
    .registers 4
    .parameter "activate"
    .parameter "response"

    #@0
    .prologue
    .line 1059
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/Phone;->activateCellBroadcastSms(ILandroid/os/Message;)V

    #@5
    .line 1060
    return-void
.end method

.method public akaAuthenticate([B[BLandroid/os/Message;)V
    .registers 5
    .parameter "rand"
    .parameter "autn"
    .parameter "onComplete"

    #@0
    .prologue
    .line 1256
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/Phone;->akaAuthenticate([B[BLandroid/os/Message;)V

    #@5
    .line 1257
    return-void
.end method

.method public canConference()Z
    .registers 2

    #@0
    .prologue
    .line 649
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->canConference()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public canTransfer()Z
    .registers 2

    #@0
    .prologue
    .line 683
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->canTransfer()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public cancelManualSearchingRequest()V
    .registers 2

    #@0
    .prologue
    .line 886
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->cancelManualSearchingRequest()V

    #@5
    .line 887
    return-void
.end method

.method public changeConnectionType(Landroid/os/Message;Lcom/android/internal/telephony/Connection;ILjava/util/Map;)V
    .registers 6
    .parameter "msg"
    .parameter "conn"
    .parameter "newCallType"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Message;",
            "Lcom/android/internal/telephony/Connection;",
            "I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 658
    .local p4, newExtras:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2, p3, p4}, Lcom/android/internal/telephony/Phone;->changeConnectionType(Landroid/os/Message;Lcom/android/internal/telephony/Connection;ILjava/util/Map;)V

    #@5
    .line 659
    return-void
.end method

.method public checkDataProfileEx(II)Z
    .registers 4
    .parameter "type"
    .parameter "Q_IPv"

    #@0
    .prologue
    .line 1592
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/Phone;->checkDataProfileEx(II)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public clearDisconnected()V
    .registers 2

    #@0
    .prologue
    .line 691
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->clearDisconnected()V

    #@5
    .line 692
    return-void
.end method

.method public closeImsPdn(I)V
    .registers 3
    .parameter "reason"

    #@0
    .prologue
    .line 1548
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->closeImsPdn(I)V

    #@5
    .line 1549
    return-void
.end method

.method public conference()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 653
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->conference()V

    #@5
    .line 654
    return-void
.end method

.method protected createNewPhone(I)V
    .registers 4
    .parameter "newVoiceRadioTech"

    #@0
    .prologue
    .line 372
    const-string v1, "KDDI"

    #@2
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@5
    move-result v1

    #@6
    if-eqz v1, :cond_21

    #@8
    const/16 v1, 0xe

    #@a
    if-ne p1, v1, :cond_21

    #@c
    .line 374
    invoke-direct {p0}, Lcom/android/internal/telephony/PhoneProxy;->getPhoneType_inLTE()I

    #@f
    move-result v0

    #@10
    .line 376
    .local v0, phoneType:I
    const/4 v1, 0x2

    #@11
    if-ne v0, v1, :cond_1a

    #@13
    .line 377
    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getCdmaPhone()Lcom/android/internal/telephony/Phone;

    #@16
    move-result-object v1

    #@17
    iput-object v1, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@19
    .line 390
    .end local v0           #phoneType:I
    :cond_19
    :goto_19
    return-void

    #@1a
    .line 380
    .restart local v0       #phoneType:I
    :cond_1a
    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getGsmPhone()Lcom/android/internal/telephony/Phone;

    #@1d
    move-result-object v1

    #@1e
    iput-object v1, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@20
    goto :goto_19

    #@21
    .line 385
    .end local v0           #phoneType:I
    :cond_21
    invoke-static {p1}, Landroid/telephony/ServiceState;->isCdma(I)Z

    #@24
    move-result v1

    #@25
    if-eqz v1, :cond_2e

    #@27
    .line 386
    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getCdmaPhone()Lcom/android/internal/telephony/Phone;

    #@2a
    move-result-object v1

    #@2b
    iput-object v1, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2d
    goto :goto_19

    #@2e
    .line 387
    :cond_2e
    invoke-static {p1}, Landroid/telephony/ServiceState;->isGsm(I)Z

    #@31
    move-result v1

    #@32
    if-eqz v1, :cond_19

    #@34
    .line 388
    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getGsmPhone()Lcom/android/internal/telephony/Phone;

    #@37
    move-result-object v1

    #@38
    iput-object v1, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@3a
    goto :goto_19
.end method

.method public dial(Ljava/lang/String;)Lcom/android/internal/telephony/Connection;
    .registers 3
    .parameter "dialString"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 707
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->dial(Ljava/lang/String;)Lcom/android/internal/telephony/Connection;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public dial(Ljava/lang/String;I[Ljava/lang/String;)Lcom/android/internal/telephony/Connection;
    .registers 5
    .parameter "dialString"
    .parameter "callType"
    .parameter "extras"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 716
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/Phone;->dial(Ljava/lang/String;I[Ljava/lang/String;)Lcom/android/internal/telephony/Connection;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public dial(Ljava/lang/String;Lcom/android/internal/telephony/UUSInfo;)Lcom/android/internal/telephony/Connection;
    .registers 4
    .parameter "dialString"
    .parameter "uusInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 711
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/Phone;->dial(Ljava/lang/String;Lcom/android/internal/telephony/UUSInfo;)Lcom/android/internal/telephony/Connection;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public dial(Ljava/lang/String;Z)Lcom/android/internal/telephony/Connection;
    .registers 4
    .parameter "dialString"
    .parameter "subaddress"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 721
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/Phone;->dial(Ljava/lang/String;Z)Lcom/android/internal/telephony/Connection;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public disableApnType(Ljava/lang/String;)I
    .registers 3
    .parameter "type"

    #@0
    .prologue
    .line 999
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->disableApnType(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public disableDnsCheck(Z)V
    .registers 3
    .parameter "b"

    #@0
    .prologue
    .line 425
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->disableDnsCheck(Z)V

    #@5
    .line 426
    return-void
.end method

.method public disableLocationUpdates()V
    .registers 2

    #@0
    .prologue
    .line 951
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->disableLocationUpdates()V

    #@5
    .line 952
    return-void
.end method

.method public dispose()V
    .registers 2

    #@0
    .prologue
    .line 1281
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mCommandsInterface:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForOn(Landroid/os/Handler;)V

    #@5
    .line 1282
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mCommandsInterface:Lcom/android/internal/telephony/CommandsInterface;

    #@7
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForVoiceRadioTechChanged(Landroid/os/Handler;)V

    #@a
    .line 1283
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mCommandsInterface:Lcom/android/internal/telephony/CommandsInterface;

    #@c
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForRilConnected(Landroid/os/Handler;)V

    #@f
    .line 1284
    return-void
.end method

.method public enableApnType(Ljava/lang/String;)I
    .registers 3
    .parameter "type"

    #@0
    .prologue
    .line 995
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->enableApnType(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public enableEnhancedVoicePrivacy(ZLandroid/os/Message;)V
    .registers 4
    .parameter "enable"
    .parameter "onComplete"

    #@0
    .prologue
    .line 675
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/Phone;->enableEnhancedVoicePrivacy(ZLandroid/os/Message;)V

    #@5
    .line 676
    return-void
.end method

.method public enableLocationUpdates()V
    .registers 2

    #@0
    .prologue
    .line 947
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->enableLocationUpdates()V

    #@5
    .line 948
    return-void
.end method

.method public exitEmergencyCallbackMode()V
    .registers 2

    #@0
    .prologue
    .line 1137
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->exitEmergencyCallbackMode()V

    #@5
    .line 1138
    return-void
.end method

.method public explicitCallTransfer()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 687
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->explicitCallTransfer()V

    #@5
    .line 688
    return-void
.end method

.method public gbaAuthenticateBootstrap([B[BLandroid/os/Message;)V
    .registers 5
    .parameter "rand"
    .parameter "autn"
    .parameter "onComplete"

    #@0
    .prologue
    .line 1260
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/Phone;->gbaAuthenticateBootstrap([B[BLandroid/os/Message;)V

    #@5
    .line 1261
    return-void
.end method

.method public gbaAuthenticateNaf([BLandroid/os/Message;)V
    .registers 4
    .parameter "nafId"
    .parameter "onComplete"

    #@0
    .prologue
    .line 1264
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/Phone;->gbaAuthenticateNaf([BLandroid/os/Message;)V

    #@5
    .line 1265
    return-void
.end method

.method public getAPNList()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1618
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getAPNList()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getActiveApnHost(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "apnType"

    #@0
    .prologue
    .line 449
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->getActiveApnHost(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getActiveApnTypes()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 445
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getActiveApnTypes()[Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getActivePhone()Lcom/android/internal/telephony/Phone;
    .registers 2

    #@0
    .prologue
    .line 1129
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    return-object v0
.end method

.method public getAlertId()I
    .registers 2

    #@0
    .prologue
    .line 1098
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getAlertId()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getAllCellInfo()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/telephony/CellInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 405
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getAllCellInfo()Ljava/util/List;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getAvailableNetworks(Landroid/os/Message;)V
    .registers 3
    .parameter "response"

    #@0
    .prologue
    .line 873
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->getAvailableNetworks(Landroid/os/Message;)V

    #@5
    .line 874
    return-void
.end method

.method public getBackgroundCall()Lcom/android/internal/telephony/Call;
    .registers 2

    #@0
    .prologue
    .line 699
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getBackgroundCall()Lcom/android/internal/telephony/Call;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getCLIRSettingValue()I
    .registers 3

    #@0
    .prologue
    .line 1434
    const-string v0, "PHONE"

    #@2
    const-string v1, "dummy! getCLIRSettingValue"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1435
    const/4 v0, 0x0

    #@8
    return v0
.end method

.method public getCallBarringOption(Ljava/lang/String;Landroid/os/Message;)V
    .registers 4
    .parameter "commandInterfaceCBReason"
    .parameter "onComplete"

    #@0
    .prologue
    .line 860
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/Phone;->getCallBarringOption(Ljava/lang/String;Landroid/os/Message;)V

    #@5
    .line 861
    return-void
.end method

.method public getCallDomain(Lcom/android/internal/telephony/Call;)I
    .registers 3
    .parameter "call"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 637
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->getCallDomain(Lcom/android/internal/telephony/Call;)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getCallForwardingIndicator()Z
    .registers 2

    #@0
    .prologue
    .line 754
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getCallForwardingIndicator()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getCallForwardingOption(ILandroid/os/Message;)V
    .registers 4
    .parameter "commandInterfaceCFReason"
    .parameter "onComplete"

    #@0
    .prologue
    .line 821
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/Phone;->getCallForwardingOption(ILandroid/os/Message;)V

    #@5
    .line 823
    return-void
.end method

.method public getCallType(Lcom/android/internal/telephony/Call;)I
    .registers 3
    .parameter "call"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 633
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->getCallType(Lcom/android/internal/telephony/Call;)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getCallWaiting(Landroid/os/Message;)V
    .registers 3
    .parameter "onComplete"

    #@0
    .prologue
    .line 851
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->getCallWaiting(Landroid/os/Message;)V

    #@5
    .line 852
    return-void
.end method

.method public getCdmaEriHomeSystems()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1102
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getCdmaEriHomeSystems()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getCdmaEriIconIndex()I
    .registers 2

    #@0
    .prologue
    .line 1117
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getCdmaEriIconIndex()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getCdmaEriIconMode()I
    .registers 2

    #@0
    .prologue
    .line 1125
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getCdmaEriIconMode()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getCdmaEriText()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1121
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getCdmaEriText()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getCdmaInfo()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1579
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getCdmaInfo()[Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getCdmaLteEhrpdForced()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1531
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getCdmaLteEhrpdForced()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getCdmaMin()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 762
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getCdmaMin()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getCdmaPrlVersion()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 770
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getCdmaPrlVersion()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getCellBroadcastSmsConfig(Landroid/os/Message;)V
    .registers 3
    .parameter "response"

    #@0
    .prologue
    .line 1063
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->getCellBroadcastSmsConfig(Landroid/os/Message;)V

    #@5
    .line 1064
    return-void
.end method

.method public getCellLocation()Landroid/telephony/CellLocation;
    .registers 2

    #@0
    .prologue
    .line 397
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getCellLocation()Landroid/telephony/CellLocation;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getContext()Landroid/content/Context;
    .registers 2

    #@0
    .prologue
    .line 421
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getCurrentLine()I
    .registers 3

    #@0
    .prologue
    .line 1451
    const-string v0, "PHONE"

    #@2
    const-string v1, "dummy! getCurrentLine"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1452
    const/4 v0, 0x2

    #@8
    return v0
.end method

.method public getCurrentVoiceClass()I
    .registers 2

    #@0
    .prologue
    .line 1471
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getCurrentVoiceClass()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getDataActivityState()Lcom/android/internal/telephony/Phone$DataActivityState;
    .registers 2

    #@0
    .prologue
    .line 417
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getDataActivityState()Lcom/android/internal/telephony/Phone$DataActivityState;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getDataCallList(Landroid/os/Message;)V
    .registers 3
    .parameter "response"

    #@0
    .prologue
    .line 939
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->getDataCallList(Landroid/os/Message;)V

    #@5
    .line 940
    return-void
.end method

.method public getDataConnectionState()Lcom/android/internal/telephony/PhoneConstants$DataState;
    .registers 3

    #@0
    .prologue
    .line 409
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    const-string v1, "default"

    #@4
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/Phone;->getDataConnectionState(Ljava/lang/String;)Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public getDataConnectionState(Ljava/lang/String;)Lcom/android/internal/telephony/PhoneConstants$DataState;
    .registers 3
    .parameter "apnType"

    #@0
    .prologue
    .line 413
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->getDataConnectionState(Ljava/lang/String;)Lcom/android/internal/telephony/PhoneConstants$DataState;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getDataRoamingEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 971
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getDataRoamingEnabled()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getDebugInfo(II)[I
    .registers 4
    .parameter "type"
    .parameter "num"

    #@0
    .prologue
    .line 1610
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/Phone;->getDebugInfo(II)[I

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getDeviceId()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1011
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getDeviceId()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getDeviceId(I)Ljava/lang/String;
    .registers 3
    .parameter "type"

    #@0
    .prologue
    .line 1424
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->getDeviceId(I)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getDeviceSvn()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1015
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getDeviceSvn()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getEmodeInfoPage(I)Ljava/lang/String;
    .registers 4
    .parameter "EngIndex"

    #@0
    .prologue
    .line 1389
    const-string v0, "PHONE"

    #@2
    const-string v1, "getEmodeInfoPage"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1390
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@9
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->getEmodeInfoPage(I)Ljava/lang/String;

    #@c
    move-result-object v0

    #@d
    return-object v0
.end method

.method public getEngineeringModeInfo(ILandroid/os/Message;)I
    .registers 5
    .parameter "data"
    .parameter "result"

    #@0
    .prologue
    .line 1385
    const-string v0, "PHONE"

    #@2
    const-string v1, "getEngineeringModeInfo"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1386
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@9
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/Phone;->getEngineeringModeInfo(ILandroid/os/Message;)I

    #@c
    move-result v0

    #@d
    return v0
.end method

.method public getEnhancedVoicePrivacy(Landroid/os/Message;)V
    .registers 3
    .parameter "onComplete"

    #@0
    .prologue
    .line 679
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->getEnhancedVoicePrivacy(Landroid/os/Message;)V

    #@5
    .line 680
    return-void
.end method

.method public getEriFileVersion()I
    .registers 2

    #@0
    .prologue
    .line 1112
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getEriFileVersion()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getEsn()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1027
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getEsn()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getForegroundCall()Lcom/android/internal/telephony/Call;
    .registers 2

    #@0
    .prologue
    .line 695
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getForegroundCall()Lcom/android/internal/telephony/Call;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getHDRRoamingIndicator()I
    .registers 2

    #@0
    .prologue
    .line 1524
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getHDRRoamingIndicator()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getIPPhoneState()Z
    .registers 2

    #@0
    .prologue
    .line 1681
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getIPPhoneState()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getIccCard()Lcom/android/internal/telephony/IccCard;
    .registers 2

    #@0
    .prologue
    .line 621
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mIccCardProxy:Lcom/android/internal/telephony/uicc/IccCardProxy;

    #@2
    return-object v0
.end method

.method public getIccFileHandler()Lcom/android/internal/telephony/uicc/IccFileHandler;
    .registers 2

    #@0
    .prologue
    .line 1511
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getIccFileHandler()Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getIccPhoneBookInterfaceManager()Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;
    .registers 2

    #@0
    .prologue
    .line 1047
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getIccPhoneBookInterfaceManager()Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getIccRecordsLoaded()Z
    .registers 2

    #@0
    .prologue
    .line 617
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mIccCardProxy:Lcom/android/internal/telephony/uicc/IccCardProxy;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/uicc/IccCardProxy;->getIccRecordsLoaded()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getIccSerialNumber()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1023
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getIccSerialNumber()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getImei()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1039
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getImei()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getInternationalMdnVoiceMailNumberForVZW()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 788
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getInternationalMdnVoiceMailNumberForVZW()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getIsUpdateRoamingCountry()Z
    .registers 2

    #@0
    .prologue
    .line 1337
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getIsUpdateRoamingCountry()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getIsimRecords()Lcom/android/internal/telephony/uicc/IsimRecords;
    .registers 2

    #@0
    .prologue
    .line 1229
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getIsimRecords()Lcom/android/internal/telephony/uicc/IsimRecords;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getLTEDataRoamingEnable()Z
    .registers 2

    #@0
    .prologue
    .line 1661
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getLTEDataRoamingEnable()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getLine1AlphaTag()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 774
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getLine1AlphaTag()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getLine1Number()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 758
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getLine1Number()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getLinkCapabilities(Ljava/lang/String;)Landroid/net/LinkCapabilities;
    .registers 3
    .parameter "apnType"

    #@0
    .prologue
    .line 457
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->getLinkCapabilities(Ljava/lang/String;)Landroid/net/LinkCapabilities;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getLinkProperties(Ljava/lang/String;)Landroid/net/LinkProperties;
    .registers 3
    .parameter "apnType"

    #@0
    .prologue
    .line 453
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->getLinkProperties(Ljava/lang/String;)Landroid/net/LinkProperties;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getLteInfo()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1575
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getLteInfo()[Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getLteOnCdmaMode()I
    .registers 2

    #@0
    .prologue
    .line 1241
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getLteOnCdmaMode()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getMSIN()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1668
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public getMeid()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1031
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getMeid()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getMessageWaitingIndicator()Z
    .registers 2

    #@0
    .prologue
    .line 750
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getMessageWaitingIndicator()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getMipErrorCode(Landroid/os/Message;)V
    .registers 3
    .parameter "result"

    #@0
    .prologue
    .line 1624
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->getMipErrorCode(Landroid/os/Message;)V

    #@5
    .line 1625
    return-void
.end method

.method public getModemIntegerItem(ILandroid/os/Message;)V
    .registers 4
    .parameter "item_index"
    .parameter "response"

    #@0
    .prologue
    .line 1371
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/Phone;->getModemIntegerItem(ILandroid/os/Message;)V

    #@5
    .line 1372
    return-void
.end method

.method public getModemStringItem(ILandroid/os/Message;)V
    .registers 4
    .parameter "item_index"
    .parameter "response"

    #@0
    .prologue
    .line 1379
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/Phone;->getModemStringItem(ILandroid/os/Message;)V

    #@5
    .line 1380
    return-void
.end method

.method public getMsisdn()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1035
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getMsisdn()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getMute()Z
    .registers 2

    #@0
    .prologue
    .line 923
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getMute()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getNeighboringCids(Landroid/os/Message;)V
    .registers 3
    .parameter "response"

    #@0
    .prologue
    .line 911
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->getNeighboringCids(Landroid/os/Message;)V

    #@5
    .line 912
    return-void
.end method

.method public getOutgoingCallerIdDisplay(Landroid/os/Message;)V
    .registers 3
    .parameter "onComplete"

    #@0
    .prologue
    .line 841
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->getOutgoingCallerIdDisplay(Landroid/os/Message;)V

    #@5
    .line 842
    return-void
.end method

.method public getPcscfAddress(Ljava/lang/String;)[Ljava/lang/String;
    .registers 3
    .parameter "ipv"

    #@0
    .prologue
    .line 1544
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->getPcscfAddress(Ljava/lang/String;)[Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getPcscfAddress(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .registers 4
    .parameter "ipv"
    .parameter "apnType"

    #@0
    .prologue
    .line 1539
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/Phone;->getPcscfAddress(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getPendingMmiCodes()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/android/internal/telephony/MmiCode;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 521
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getPendingMmiCodes()Ljava/util/List;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getPhoneName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 437
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getPhoneName()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getPhoneSubInfo()Lcom/android/internal/telephony/PhoneSubInfo;
    .registers 2

    #@0
    .prologue
    .line 1043
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getPhoneSubInfo()Lcom/android/internal/telephony/PhoneSubInfo;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getPhoneType()I
    .registers 2

    #@0
    .prologue
    .line 441
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getPreferredNetworkMode()I
    .registers 5

    #@0
    .prologue
    .line 1823
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v1}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@9
    move-result-object v1

    #@a
    const-string v2, "preferred_network_mode"

    #@c
    iget v3, p0, Lcom/android/internal/telephony/PhoneProxy;->setPreferredNetworkType:I

    #@e
    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@11
    move-result v0

    #@12
    .line 1828
    .local v0, nwMode:I
    return v0
.end method

.method public getPreferredNetworkType(Landroid/os/Message;)V
    .registers 3
    .parameter "response"

    #@0
    .prologue
    .line 907
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->getPreferredNetworkType(Landroid/os/Message;)V

    #@5
    .line 908
    return-void
.end method

.method public getProposedConnectionType(Lcom/android/internal/telephony/Connection;)I
    .registers 3
    .parameter "conn"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 671
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->getProposedConnectionType(Lcom/android/internal/telephony/Connection;)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getRingingCall()Lcom/android/internal/telephony/Call;
    .registers 2

    #@0
    .prologue
    .line 703
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getRingingCall()Lcom/android/internal/telephony/Call;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getSearchStatus(ILandroid/os/Message;)V
    .registers 4
    .parameter "ReqType"
    .parameter "response"

    #@0
    .prologue
    .line 1503
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/Phone;->getSearchStatus(ILandroid/os/Message;)V

    #@5
    .line 1504
    return-void
.end method

.method public getSearchStatus(Landroid/os/Message;)V
    .registers 3
    .parameter "response"

    #@0
    .prologue
    .line 898
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->getSearchStatus(Landroid/os/Message;)V

    #@5
    .line 899
    return-void
.end method

.method public getServiceLine(Landroid/os/Message;)V
    .registers 4
    .parameter "onComplete"

    #@0
    .prologue
    .line 1439
    const-string v0, "PHONE"

    #@2
    const-string v1, "dummy! getServiceLine"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1440
    return-void
.end method

.method public getServiceState()Landroid/telephony/ServiceState;
    .registers 2

    #@0
    .prologue
    .line 393
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getServiceState()Landroid/telephony/ServiceState;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getSignalStrength()Landroid/telephony/SignalStrength;
    .registers 2

    #@0
    .prologue
    .line 461
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getSignalStrength()Landroid/telephony/SignalStrength;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getSimulatedRadioControl()Lcom/android/internal/telephony/test/SimulatedRadioControl;
    .registers 2

    #@0
    .prologue
    .line 991
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getSimulatedRadioControl()Lcom/android/internal/telephony/test/SimulatedRadioControl;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getSmscAddress(Landroid/os/Message;)V
    .registers 3
    .parameter "result"

    #@0
    .prologue
    .line 1075
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->getSmscAddress(Landroid/os/Message;)V

    #@5
    .line 1076
    return-void
.end method

.method public getState()Lcom/android/internal/telephony/PhoneConstants$State;
    .registers 2

    #@0
    .prologue
    .line 433
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getState()Lcom/android/internal/telephony/PhoneConstants$State;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getStatusId()I
    .registers 2

    #@0
    .prologue
    .line 1518
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getStatusId()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getSubscriberId()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1019
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getSubscriberId()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getSubscription()I
    .registers 2

    #@0
    .prologue
    .line 1497
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getSubscription()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getUnitTestMode()Z
    .registers 2

    #@0
    .prologue
    .line 959
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getUnitTestMode()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getUsimServiceTable()Lcom/android/internal/telephony/uicc/UsimServiceTable;
    .registers 2

    #@0
    .prologue
    .line 1246
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getUsimServiceTable()Lcom/android/internal/telephony/uicc/UsimServiceTable;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getVoiceCallForwardingFlag()Z
    .registers 3

    #@0
    .prologue
    .line 1456
    const-string v0, "PHONE"

    #@2
    const-string v1, "dummy! getVoiceCallForwardingFlag"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1457
    const/4 v0, 0x0

    #@8
    return v0
.end method

.method public getVoiceCallForwardingFlagLine2()Z
    .registers 3

    #@0
    .prologue
    .line 1461
    const-string v0, "PHONE"

    #@2
    const-string v1, "dummy! getVoiceCallForwardingFlagLine2"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1462
    const/4 v0, 0x0

    #@8
    return v0
.end method

.method public getVoiceMailAlphaTag()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 811
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getVoiceMailAlphaTag()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getVoiceMailAlphaTagForALS(I)Ljava/lang/String;
    .registers 4
    .parameter "line"

    #@0
    .prologue
    .line 1486
    const-string v0, "PHONE"

    #@2
    const-string v1, "dummy! getVoiceMailAlphaTagForALS"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1487
    const/4 v0, 0x0

    #@8
    return-object v0
.end method

.method public getVoiceMailNumber()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 782
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getVoiceMailNumber()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getVoiceMailNumberForALS(I)Ljava/lang/String;
    .registers 4
    .parameter "line"

    #@0
    .prologue
    .line 1480
    const-string v0, "PHONE"

    #@2
    const-string v1, "dummy! getVoiceMailNumberForALS"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1481
    const/4 v0, 0x0

    #@8
    return-object v0
.end method

.method public getVoiceMessageCount()I
    .registers 2

    #@0
    .prologue
    .line 799
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getVoiceMessageCount()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getVoiceMessageUrgent()Z
    .registers 2

    #@0
    .prologue
    .line 806
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getVoiceMessageUrgent()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public handleInCallMmiCommands(Ljava/lang/String;)Z
    .registers 3
    .parameter "command"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 730
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->handleInCallMmiCommands(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 6
    .parameter "msg"

    #@0
    .prologue
    .line 164
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2
    check-cast v0, Landroid/os/AsyncResult;

    #@4
    .line 165
    .local v0, ar:Landroid/os/AsyncResult;
    iget v2, p1, Landroid/os/Message;->what:I

    #@6
    packed-switch v2, :pswitch_data_ba

    #@9
    .line 205
    new-instance v2, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v3, "Error! This handler was not registered for this message type. Message: "

    #@10
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    iget v3, p1, Landroid/os/Message;->what:I

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/PhoneProxy;->loge(Ljava/lang/String;)V

    #@21
    .line 209
    :goto_21
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    #@24
    .line 210
    return-void

    #@25
    .line 168
    :pswitch_25
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneProxy;->mCommandsInterface:Lcom/android/internal/telephony/CommandsInterface;

    #@27
    const/4 v3, 0x3

    #@28
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/PhoneProxy;->obtainMessage(I)Landroid/os/Message;

    #@2b
    move-result-object v3

    #@2c
    invoke-interface {v2, v3}, Lcom/android/internal/telephony/CommandsInterface;->getVoiceRadioTechnology(Landroid/os/Message;)V

    #@2f
    goto :goto_21

    #@30
    .line 173
    :pswitch_30
    iget-object v2, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@32
    if-nez v2, :cond_43

    #@34
    iget-object v2, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@36
    if-eqz v2, :cond_43

    #@38
    .line 174
    iget-object v2, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@3a
    check-cast v2, Ljava/lang/Integer;

    #@3c
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    #@3f
    move-result v2

    #@40
    iput v2, p0, Lcom/android/internal/telephony/PhoneProxy;->mRilVersion:I

    #@42
    goto :goto_21

    #@43
    .line 176
    :cond_43
    const-string v2, "Unexpected exception on EVENT_RIL_CONNECTED"

    #@45
    invoke-static {v2}, Lcom/android/internal/telephony/PhoneProxy;->logd(Ljava/lang/String;)V

    #@48
    .line 177
    const/4 v2, -0x1

    #@49
    iput v2, p0, Lcom/android/internal/telephony/PhoneProxy;->mRilVersion:I

    #@4b
    goto :goto_21

    #@4c
    .line 184
    :pswitch_4c
    iget-object v2, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@4e
    if-nez v2, :cond_94

    #@50
    .line 185
    iget-object v2, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@52
    if-eqz v2, :cond_75

    #@54
    iget-object v2, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@56
    check-cast v2, [I

    #@58
    check-cast v2, [I

    #@5a
    array-length v2, v2

    #@5b
    if-eqz v2, :cond_75

    #@5d
    .line 186
    iget-object v2, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@5f
    check-cast v2, [I

    #@61
    check-cast v2, [I

    #@63
    const/4 v3, 0x0

    #@64
    aget v1, v2, v3

    #@66
    .line 191
    .local v1, newVoiceTech:I
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneProxy;->mCommandsInterface:Lcom/android/internal/telephony/CommandsInterface;

    #@68
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@6b
    move-result-object v2

    #@6c
    iget-boolean v2, v2, Lcom/android/internal/telephony/LGfeature;->fixthephonetypetoCDMA:Z

    #@6e
    if-eqz v2, :cond_71

    #@70
    .line 192
    const/4 v1, 0x6

    #@71
    .line 195
    :cond_71
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/PhoneProxy;->updatePhoneObject(I)V

    #@74
    goto :goto_21

    #@75
    .line 197
    .end local v1           #newVoiceTech:I
    :cond_75
    new-instance v2, Ljava/lang/StringBuilder;

    #@77
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7a
    const-string v3, "Voice Radio Technology event "

    #@7c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v2

    #@80
    iget v3, p1, Landroid/os/Message;->what:I

    #@82
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@85
    move-result-object v2

    #@86
    const-string v3, " has no tech!"

    #@88
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v2

    #@8c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8f
    move-result-object v2

    #@90
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/PhoneProxy;->loge(Ljava/lang/String;)V

    #@93
    goto :goto_21

    #@94
    .line 200
    :cond_94
    new-instance v2, Ljava/lang/StringBuilder;

    #@96
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@99
    const-string v3, "Voice Radio Technology event "

    #@9b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v2

    #@9f
    iget v3, p1, Landroid/os/Message;->what:I

    #@a1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v2

    #@a5
    const-string v3, " exception!"

    #@a7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v2

    #@ab
    iget-object v3, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@ad
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v2

    #@b1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b4
    move-result-object v2

    #@b5
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/PhoneProxy;->loge(Ljava/lang/String;)V

    #@b8
    goto/16 :goto_21

    #@ba
    .line 165
    :pswitch_data_ba
    .packed-switch 0x1
        :pswitch_4c
        :pswitch_25
        :pswitch_4c
        :pswitch_30
    .end packed-switch
.end method

.method protected handleMessageInternal(Landroid/os/Message;)V
    .registers 2
    .parameter "msg"

    #@0
    .prologue
    .line 1275
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    #@3
    .line 1276
    return-void
.end method

.method public handlePinMmi(Ljava/lang/String;)Z
    .registers 3
    .parameter "dialString"

    #@0
    .prologue
    .line 726
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->handlePinMmi(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public hasIsim()Z
    .registers 2

    #@0
    .prologue
    .line 1252
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->hasIsim()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method protected init()V
    .registers 4

    #@0
    .prologue
    .line 157
    new-instance v1, Lcom/android/internal/telephony/IccSmsInterfaceManager;

    #@2
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@4
    check-cast v0, Lcom/android/internal/telephony/PhoneBase;

    #@6
    invoke-direct {v1, v0}, Lcom/android/internal/telephony/IccSmsInterfaceManager;-><init>(Lcom/android/internal/telephony/PhoneBase;)V

    #@9
    iput-object v1, p0, Lcom/android/internal/telephony/PhoneProxy;->mIccSmsInterfaceManager:Lcom/android/internal/telephony/IccSmsInterfaceManager;

    #@b
    .line 159
    new-instance v0, Lcom/android/internal/telephony/uicc/IccCardProxy;

    #@d
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@f
    invoke-interface {v1}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@12
    move-result-object v1

    #@13
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneProxy;->mCommandsInterface:Lcom/android/internal/telephony/CommandsInterface;

    #@15
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/uicc/IccCardProxy;-><init>(Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;)V

    #@18
    iput-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mIccCardProxy:Lcom/android/internal/telephony/uicc/IccCardProxy;

    #@1a
    .line 160
    return-void
.end method

.method public invokeOemRilRequestRaw([BLandroid/os/Message;)V
    .registers 4
    .parameter "data"
    .parameter "response"

    #@0
    .prologue
    .line 931
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/Phone;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    #@5
    .line 932
    return-void
.end method

.method public invokeOemRilRequestStrings([Ljava/lang/String;Landroid/os/Message;)V
    .registers 4
    .parameter "strings"
    .parameter "response"

    #@0
    .prologue
    .line 935
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/Phone;->invokeOemRilRequestStrings([Ljava/lang/String;Landroid/os/Message;)V

    #@5
    .line 936
    return-void
.end method

.method public isCspPlmnEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 1225
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->isCspPlmnEnabled()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isDataConnectivityPossible()Z
    .registers 3

    #@0
    .prologue
    .line 1003
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    const-string v1, "default"

    #@4
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/Phone;->isDataConnectivityPossible(Ljava/lang/String;)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public isDataConnectivityPossible(Ljava/lang/String;)Z
    .registers 3
    .parameter "apnType"

    #@0
    .prologue
    .line 1007
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->isDataConnectivityPossible(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isDnsCheckDisabled()Z
    .registers 2

    #@0
    .prologue
    .line 429
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->isDnsCheckDisabled()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isEmergencyAttachSupportedOnLte()Z
    .registers 2

    #@0
    .prologue
    .line 1638
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->isEmergencyAttachSupportedOnLte()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isEmergencyCallSupportedOnLte()Z
    .registers 2

    #@0
    .prologue
    .line 1634
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->isEmergencyCallSupportedOnLte()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isInternetPDNconnected()Z
    .registers 2

    #@0
    .prologue
    .line 1648
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->isInternetPDNconnected()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isLTEDataRoamingAvailable()Z
    .registers 2

    #@0
    .prologue
    .line 1653
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->isLTEDataRoamingAvailable()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isManualNetSelAllowed()Z
    .registers 2

    #@0
    .prologue
    .line 1221
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->isManualNetSelAllowed()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isMinInfoReady()Z
    .registers 2

    #@0
    .prologue
    .line 766
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->isMinInfoReady()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isOtaAttachedOnLte()Z
    .registers 2

    #@0
    .prologue
    .line 1643
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->isOtaAttachedOnLte()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isOtaSpNumber(Ljava/lang/String;)Z
    .registers 3
    .parameter "dialStr"

    #@0
    .prologue
    .line 1145
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->isOtaSpNumber(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isTwoLineSupported()Z
    .registers 3

    #@0
    .prologue
    .line 1475
    const-string v0, "PHONE"

    #@2
    const-string v1, "dummy! isTwoLineSupported"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1476
    const/4 v0, 0x0

    #@8
    return v0
.end method

.method public isVoiceCallSupprotedOnLte()Z
    .registers 2

    #@0
    .prologue
    .line 1630
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->isVoiceCallSupprotedOnLte()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public needsOtaServiceProvisioning()Z
    .registers 2

    #@0
    .prologue
    .line 1141
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->needsOtaServiceProvisioning()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public notifyDataActivity()V
    .registers 2

    #@0
    .prologue
    .line 1071
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->notifyDataActivity()V

    #@5
    .line 1072
    return-void
.end method

.method public prepareEri()V
    .registers 2

    #@0
    .prologue
    .line 1084
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->prepareEri()V

    #@5
    .line 1085
    return-void
.end method

.method public processECCNotiRep(Lcom/android/internal/telephony/MmiCode;)Z
    .registers 3
    .parameter "mmicode"

    #@0
    .prologue
    .line 1411
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->processECCNotiRep(Lcom/android/internal/telephony/MmiCode;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public processECCNotiUSSD(Ljava/lang/String;)V
    .registers 3
    .parameter "dialString"

    #@0
    .prologue
    .line 1407
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->processECCNotiUSSD(Ljava/lang/String;)V

    #@5
    .line 1408
    return-void
.end method

.method public queryAvailableBandMode(Landroid/os/Message;)V
    .registers 3
    .parameter "response"

    #@0
    .prologue
    .line 967
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->queryAvailableBandMode(Landroid/os/Message;)V

    #@5
    .line 968
    return-void
.end method

.method public queryCdmaRoamingPreference(Landroid/os/Message;)V
    .registers 3
    .parameter "response"

    #@0
    .prologue
    .line 979
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->queryCdmaRoamingPreference(Landroid/os/Message;)V

    #@5
    .line 980
    return-void
.end method

.method public queryTTYMode(Landroid/os/Message;)V
    .registers 3
    .parameter "onComplete"

    #@0
    .prologue
    .line 1055
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->queryTTYMode(Landroid/os/Message;)V

    #@5
    .line 1056
    return-void
.end method

.method public registerFoT53ClirlInfo(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1197
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/Phone;->registerFoT53ClirlInfo(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1198
    return-void
.end method

.method public registerForAvpUpgradeFailure(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1302
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/Phone;->registerForAvpUpgradeFailure(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1303
    return-void
.end method

.method public registerForCallWaiting(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1149
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/Phone;->registerForCallWaiting(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1150
    return-void
.end method

.method public registerForCdmaOtaStatusChange(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 569
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/Phone;->registerForCdmaOtaStatusChange(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 570
    return-void
.end method

.method public registerForDisconnect(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 497
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/Phone;->registerForDisconnect(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 498
    return-void
.end method

.method public registerForDisplayInfo(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1165
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/Phone;->registerForDisplayInfo(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1166
    return-void
.end method

.method public registerForEcmTimerReset(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 585
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/Phone;->registerForEcmTimerReset(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 586
    return-void
.end method

.method public registerForInCallVoicePrivacyOff(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 561
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/Phone;->registerForInCallVoicePrivacyOff(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 562
    return-void
.end method

.method public registerForInCallVoicePrivacyOn(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 553
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/Phone;->registerForInCallVoicePrivacyOn(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 554
    return-void
.end method

.method public registerForIncomingRing(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 489
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/Phone;->registerForIncomingRing(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 490
    return-void
.end method

.method public registerForLineControlInfo(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1189
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/Phone;->registerForLineControlInfo(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1190
    return-void
.end method

.method public registerForMmiComplete(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 513
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/Phone;->registerForMmiComplete(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 514
    return-void
.end method

.method public registerForMmiInitiate(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 505
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/Phone;->registerForMmiInitiate(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 506
    return-void
.end method

.method public registerForModifyCallRequest(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1293
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/Phone;->registerForModifyCallRequest(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1294
    return-void
.end method

.method public registerForNewRingingConnection(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 481
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/Phone;->registerForNewRingingConnection(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 482
    return-void
.end method

.method public registerForNumberInfo(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1173
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/Phone;->registerForNumberInfo(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1174
    return-void
.end method

.method public registerForPreciseCallStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 473
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/Phone;->registerForPreciseCallStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 474
    return-void
.end method

.method public registerForRedirectedNumberInfo(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1181
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/Phone;->registerForRedirectedNumberInfo(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1182
    return-void
.end method

.method public registerForResendIncallMute(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 601
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/Phone;->registerForResendIncallMute(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 602
    return-void
.end method

.method public registerForRingbackTone(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 593
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/Phone;->registerForRingbackTone(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 594
    return-void
.end method

.method public registerForServiceStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 529
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/Phone;->registerForServiceStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 530
    return-void
.end method

.method public registerForSignalInfo(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1157
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/Phone;->registerForSignalInfo(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1158
    return-void
.end method

.method public registerForSimRecordsLoaded(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 609
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/Phone;->registerForSimRecordsLoaded(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 610
    return-void
.end method

.method public registerForSubscriptionInfoReady(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 577
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/Phone;->registerForSubscriptionInfoReady(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 578
    return-void
.end method

.method public registerForSuppServiceFailed(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 545
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/Phone;->registerForSuppServiceFailed(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 546
    return-void
.end method

.method public registerForSuppServiceNotification(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 537
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/Phone;->registerForSuppServiceNotification(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 538
    return-void
.end method

.method public registerForT53AudioControlInfo(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1205
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/Phone;->registerForT53AudioControlInfo(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1206
    return-void
.end method

.method public registerForUnknownConnection(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 465
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/Phone;->registerForUnknownConnection(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 466
    return-void
.end method

.method public registerLGECipheringInd(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1343
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/Phone;->registerLGECipheringInd(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1344
    return-void
.end method

.method public registerLGEUnsol(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1355
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/Phone;->registerLGEUnsol(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1356
    return-void
.end method

.method public rejectCall()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 641
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->rejectCall()V

    #@5
    .line 642
    return-void
.end method

.method public rejectConnectionTypeChange(Lcom/android/internal/telephony/Connection;)V
    .registers 3
    .parameter "conn"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 667
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->rejectConnectionTypeChange(Lcom/android/internal/telephony/Connection;)V

    #@5
    .line 668
    return-void
.end method

.method public removeReferences()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 1287
    iput-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@3
    .line 1288
    iput-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mCommandsInterface:Lcom/android/internal/telephony/CommandsInterface;

    #@5
    .line 1289
    return-void
.end method

.method public requestIsimAuthentication(Ljava/lang/String;Landroid/os/Message;)V
    .registers 4
    .parameter "nonce"
    .parameter "response"

    #@0
    .prologue
    .line 1233
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/Phone;->requestIsimAuthentication(Ljava/lang/String;Landroid/os/Message;)V

    #@5
    .line 1234
    return-void
.end method

.method public resetVoiceMessageCount()V
    .registers 2

    #@0
    .prologue
    .line 1603
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->resetVoiceMessageCount()V

    #@5
    .line 1604
    return-void
.end method

.method public selectNetworkManually(Lcom/android/internal/telephony/OperatorInfo;Landroid/os/Message;)V
    .registers 4
    .parameter "network"
    .parameter "response"

    #@0
    .prologue
    .line 881
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/Phone;->selectNetworkManually(Lcom/android/internal/telephony/OperatorInfo;Landroid/os/Message;)V

    #@5
    .line 882
    return-void
.end method

.method public selectPreviousNetworkManually(Lcom/android/internal/telephony/OperatorInfo;Landroid/os/Message;)V
    .registers 4
    .parameter "network"
    .parameter "response"

    #@0
    .prologue
    .line 892
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/Phone;->selectPreviousNetworkManually(Lcom/android/internal/telephony/OperatorInfo;Landroid/os/Message;)V

    #@5
    .line 893
    return-void
.end method

.method protected sendBroadcastStickyIntent()V
    .registers 4

    #@0
    .prologue
    .line 325
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "android.intent.action.RADIO_TECHNOLOGY"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 326
    .local v0, intent:Landroid/content/Intent;
    const/high16 v1, 0x2000

    #@9
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@c
    .line 327
    const-string v1, "phoneName"

    #@e
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@10
    invoke-interface {v2}, Lcom/android/internal/telephony/Phone;->getPhoneName()Ljava/lang/String;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@17
    .line 328
    const/4 v1, 0x0

    #@18
    const/4 v2, -0x1

    #@19
    invoke-static {v0, v1, v2}, Landroid/app/ActivityManagerNative;->broadcastStickyIntent(Landroid/content/Intent;Ljava/lang/String;I)V

    #@1c
    .line 330
    return-void
.end method

.method public sendBurstDtmf(Ljava/lang/String;IILandroid/os/Message;)V
    .registers 6
    .parameter "dtmfString"
    .parameter "on"
    .parameter "off"
    .parameter "onComplete"

    #@0
    .prologue
    .line 1133
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2, p3, p4}, Lcom/android/internal/telephony/Phone;->sendBurstDtmf(Ljava/lang/String;IILandroid/os/Message;)V

    #@5
    .line 1134
    return-void
.end method

.method public sendDefaultAttachProfile(I)V
    .registers 3
    .parameter "profileId"

    #@0
    .prologue
    .line 1585
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->sendDefaultAttachProfile(I)V

    #@5
    .line 1586
    return-void
.end method

.method public sendDtmf(C)V
    .registers 3
    .parameter "c"

    #@0
    .prologue
    .line 734
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->sendDtmf(C)V

    #@5
    .line 735
    return-void
.end method

.method public sendUssdResponse(Ljava/lang/String;)V
    .registers 3
    .parameter "ussdMessge"

    #@0
    .prologue
    .line 525
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->sendUssdResponse(Ljava/lang/String;)V

    #@5
    .line 526
    return-void
.end method

.method public setBandMode(ILandroid/os/Message;)V
    .registers 4
    .parameter "bandMode"
    .parameter "response"

    #@0
    .prologue
    .line 963
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/Phone;->setBandMode(ILandroid/os/Message;)V

    #@5
    .line 964
    return-void
.end method

.method public setCSGSelectionManual(ILandroid/os/Message;)V
    .registers 5
    .parameter "data"
    .parameter "result"

    #@0
    .prologue
    .line 1397
    const-string v0, "PHONE"

    #@2
    const-string v1, "setCSGSelectionManual"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1398
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@9
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/Phone;->setCSGSelectionManual(ILandroid/os/Message;)V

    #@c
    .line 1399
    return-void
.end method

.method public setCallBarringOption(ILjava/lang/String;ILjava/lang/String;Landroid/os/Message;)V
    .registers 12
    .parameter "commandInterfaceCBAction"
    .parameter "commandInterfaceCBReason"
    .parameter "serviceClass"
    .parameter "password"
    .parameter "onComplete"

    #@0
    .prologue
    .line 864
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    move v1, p1

    #@3
    move-object v2, p2

    #@4
    move v3, p3

    #@5
    move-object v4, p4

    #@6
    move-object v5, p5

    #@7
    invoke-interface/range {v0 .. v5}, Lcom/android/internal/telephony/Phone;->setCallBarringOption(ILjava/lang/String;ILjava/lang/String;Landroid/os/Message;)V

    #@a
    .line 865
    return-void
.end method

.method public setCallBarringPass(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 5
    .parameter "oldPwd"
    .parameter "newPwd"
    .parameter "onComplete"

    #@0
    .prologue
    .line 868
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/Phone;->setCallBarringPass(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    #@5
    .line 869
    return-void
.end method

.method public setCallForwardingOption(IILjava/lang/String;IILandroid/os/Message;)V
    .registers 14
    .parameter "commandInterfaceCFReason"
    .parameter "commandInterfaceCFAction"
    .parameter "dialingNumber"
    .parameter "serviceClass"
    .parameter "timerSeconds"
    .parameter "onComplete"

    #@0
    .prologue
    .line 836
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    move v1, p1

    #@3
    move v2, p2

    #@4
    move-object v3, p3

    #@5
    move v4, p4

    #@6
    move v5, p5

    #@7
    move-object v6, p6

    #@8
    invoke-interface/range {v0 .. v6}, Lcom/android/internal/telephony/Phone;->setCallForwardingOption(IILjava/lang/String;IILandroid/os/Message;)V

    #@b
    .line 838
    return-void
.end method

.method public setCallForwardingOption(IILjava/lang/String;ILandroid/os/Message;)V
    .registers 12
    .parameter "commandInterfaceCFReason"
    .parameter "commandInterfaceCFAction"
    .parameter "dialingNumber"
    .parameter "timerSeconds"
    .parameter "onComplete"

    #@0
    .prologue
    .line 828
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    move v1, p1

    #@3
    move v2, p2

    #@4
    move-object v3, p3

    #@5
    move v4, p4

    #@6
    move-object v5, p5

    #@7
    invoke-interface/range {v0 .. v5}, Lcom/android/internal/telephony/Phone;->setCallForwardingOption(IILjava/lang/String;ILandroid/os/Message;)V

    #@a
    .line 830
    return-void
.end method

.method public setCallWaiting(ZLandroid/os/Message;)V
    .registers 4
    .parameter "enable"
    .parameter "onComplete"

    #@0
    .prologue
    .line 855
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/Phone;->setCallWaiting(ZLandroid/os/Message;)V

    #@5
    .line 856
    return-void
.end method

.method public setCdmaEriVersion(ILandroid/os/Message;)V
    .registers 4
    .parameter "value"
    .parameter "result"

    #@0
    .prologue
    .line 1107
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/Phone;->setCdmaEriVersion(ILandroid/os/Message;)V

    #@5
    .line 1108
    return-void
.end method

.method public setCdmaFactoryReset(Landroid/os/Message;)V
    .registers 3
    .parameter "result"

    #@0
    .prologue
    .line 1418
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->setCdmaFactoryReset(Landroid/os/Message;)V

    #@5
    .line 1419
    return-void
.end method

.method public setCdmaRoamingPreference(ILandroid/os/Message;)V
    .registers 4
    .parameter "cdmaRoamingType"
    .parameter "response"

    #@0
    .prologue
    .line 983
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/Phone;->setCdmaRoamingPreference(ILandroid/os/Message;)V

    #@5
    .line 984
    return-void
.end method

.method public setCdmaSubscription(ILandroid/os/Message;)V
    .registers 4
    .parameter "cdmaSubscriptionType"
    .parameter "response"

    #@0
    .prologue
    .line 987
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/Phone;->setCdmaSubscription(ILandroid/os/Message;)V

    #@5
    .line 988
    return-void
.end method

.method public setCellBroadcastSmsConfig([ILandroid/os/Message;)V
    .registers 4
    .parameter "configValuesArray"
    .parameter "response"

    #@0
    .prologue
    .line 1067
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/Phone;->setCellBroadcastSmsConfig([ILandroid/os/Message;)V

    #@5
    .line 1068
    return-void
.end method

.method public setDataConnection(Z)V
    .registers 3
    .parameter "enable"

    #@0
    .prologue
    .line 1404
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->setDataConnection(Z)V

    #@5
    .line 1405
    return-void
.end method

.method public setDataRoamingEnabled(Z)V
    .registers 3
    .parameter "enable"

    #@0
    .prologue
    .line 975
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->setDataRoamingEnabled(Z)V

    #@5
    .line 976
    return-void
.end method

.method public setEchoSuppressionEnabled(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 927
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->setEchoSuppressionEnabled(Z)V

    #@5
    .line 928
    return-void
.end method

.method public setGbaBootstrappingParams([BLjava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 6
    .parameter "rand"
    .parameter "btid"
    .parameter "keyLifetime"
    .parameter "onComplete"

    #@0
    .prologue
    .line 1269
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2, p3, p4}, Lcom/android/internal/telephony/Phone;->setGbaBootstrappingParams([BLjava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    #@5
    .line 1270
    return-void
.end method

.method public setIMSRegistate(Z)V
    .registers 6
    .parameter "Registate"

    #@0
    .prologue
    .line 1552
    new-instance v2, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v3, "[IMS_AFW] setIMSRegistate - Registate : "

    #@7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v2

    #@b
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v2

    #@13
    invoke-static {v2}, Lcom/android/internal/telephony/PhoneProxy;->logd(Ljava/lang/String;)V

    #@16
    .line 1554
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@18
    invoke-interface {v2, p1}, Lcom/android/internal/telephony/Phone;->setIMSRegistate(Z)V

    #@1b
    .line 1560
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@1d
    invoke-interface {v2}, Lcom/android/internal/telephony/Phone;->getPhoneName()Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    const-string v3, "GSM"

    #@23
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@26
    move-result v2

    #@27
    if-eqz v2, :cond_35

    #@29
    .line 1562
    iget-object v1, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2b
    check-cast v1, Lcom/android/internal/telephony/gsm/GSMPhone;

    #@2d
    .line 1563
    .local v1, GP:Lcom/android/internal/telephony/gsm/GSMPhone;
    invoke-virtual {v1}, Lcom/android/internal/telephony/gsm/GSMPhone;->getServiceStateTracker()Lcom/android/internal/telephony/ServiceStateTracker;

    #@30
    move-result-object v2

    #@31
    invoke-virtual {v2, p1}, Lcom/android/internal/telephony/ServiceStateTracker;->setIMSRegistate(Z)V

    #@34
    .line 1572
    .end local v1           #GP:Lcom/android/internal/telephony/gsm/GSMPhone;
    :cond_34
    :goto_34
    return-void

    #@35
    .line 1566
    :cond_35
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@37
    invoke-interface {v2}, Lcom/android/internal/telephony/Phone;->getPhoneName()Ljava/lang/String;

    #@3a
    move-result-object v2

    #@3b
    const-string v3, "CDMA"

    #@3d
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@40
    move-result v2

    #@41
    if-eqz v2, :cond_34

    #@43
    .line 1568
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@45
    check-cast v0, Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@47
    .line 1569
    .local v0, CP:Lcom/android/internal/telephony/cdma/CDMAPhone;
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getServiceStateTracker()Lcom/android/internal/telephony/ServiceStateTracker;

    #@4a
    move-result-object v2

    #@4b
    invoke-virtual {v2, p1}, Lcom/android/internal/telephony/ServiceStateTracker;->setIMSRegistate(Z)V

    #@4e
    goto :goto_34
.end method

.method public setImsStatusForDan(ILandroid/os/Message;)V
    .registers 4
    .parameter "ims_status"
    .parameter "result"

    #@0
    .prologue
    .line 1834
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/Phone;->setImsStatusForDan(ILandroid/os/Message;)V

    #@5
    .line 1835
    return-void
.end method

.method public setIsUpdateRoamingCountry(Z)V
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 1333
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->setIsUpdateRoamingCountry(Z)V

    #@5
    .line 1334
    return-void
.end method

.method public setLTEDataRoamingEnable(Z)V
    .registers 3
    .parameter "enable"

    #@0
    .prologue
    .line 1657
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->setLTEDataRoamingEnable(Z)V

    #@5
    .line 1658
    return-void
.end method

.method public setLine1Number(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 5
    .parameter "alphaTag"
    .parameter "number"
    .parameter "onComplete"

    #@0
    .prologue
    .line 778
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/Phone;->setLine1Number(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    #@5
    .line 779
    return-void
.end method

.method public setModemIntegerItem(IILandroid/os/Message;)V
    .registers 5
    .parameter "item_index"
    .parameter "data"
    .parameter "response"

    #@0
    .prologue
    .line 1367
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/Phone;->setModemIntegerItem(IILandroid/os/Message;)V

    #@5
    .line 1368
    return-void
.end method

.method public setModemStringItem(ILjava/lang/String;Landroid/os/Message;)V
    .registers 5
    .parameter "item_index"
    .parameter "data"
    .parameter "response"

    #@0
    .prologue
    .line 1375
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/Phone;->setModemStringItem(ILjava/lang/String;Landroid/os/Message;)V

    #@5
    .line 1376
    return-void
.end method

.method public setMute(Z)V
    .registers 3
    .parameter "muted"

    #@0
    .prologue
    .line 919
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->setMute(Z)V

    #@5
    .line 920
    return-void
.end method

.method public setNetworkSelectionModeAutomatic(Landroid/os/Message;)V
    .registers 3
    .parameter "response"

    #@0
    .prologue
    .line 877
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->setNetworkSelectionModeAutomatic(Landroid/os/Message;)V

    #@5
    .line 878
    return-void
.end method

.method public setOnEcbModeExitResponse(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1213
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/Phone;->setOnEcbModeExitResponse(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1214
    return-void
.end method

.method public setOnPostDialCharacter(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 915
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/Phone;->setOnPostDialCharacter(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 916
    return-void
.end method

.method public setOutgoingCallerIdDisplay(ILandroid/os/Message;)V
    .registers 4
    .parameter "commandInterfaceCLIRMode"
    .parameter "onComplete"

    #@0
    .prologue
    .line 846
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/Phone;->setOutgoingCallerIdDisplay(ILandroid/os/Message;)V

    #@5
    .line 848
    return-void
.end method

.method public setPreferredNetworkType(ILandroid/os/Message;)V
    .registers 4
    .parameter "networkType"
    .parameter "response"

    #@0
    .prologue
    .line 903
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/Phone;->setPreferredNetworkType(ILandroid/os/Message;)V

    #@5
    .line 904
    return-void
.end method

.method public setRadioPower(Z)V
    .registers 3
    .parameter "power"

    #@0
    .prologue
    .line 746
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->setRadioPower(Z)V

    #@5
    .line 747
    return-void
.end method

.method public setRmnetAutoconnect(ILandroid/os/Message;)V
    .registers 4
    .parameter "param"
    .parameter "result"

    #@0
    .prologue
    .line 1674
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/Phone;->setRmnetAutoconnect(ILandroid/os/Message;)V

    #@5
    .line 1675
    return-void
.end method

.method public setServiceLine(ILandroid/os/Message;)V
    .registers 5
    .parameter "line"
    .parameter "onComplete"

    #@0
    .prologue
    .line 1443
    const-string v0, "PHONE"

    #@2
    const-string v1, "dummy! setServiceLine"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1444
    return-void
.end method

.method public setSmscAddress(Ljava/lang/String;Landroid/os/Message;)V
    .registers 4
    .parameter "address"
    .parameter "result"

    #@0
    .prologue
    .line 1079
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/Phone;->setSmscAddress(Ljava/lang/String;Landroid/os/Message;)V

    #@5
    .line 1080
    return-void
.end method

.method public setTTYMode(ILandroid/os/Message;)V
    .registers 4
    .parameter "ttyMode"
    .parameter "onComplete"

    #@0
    .prologue
    .line 1051
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/Phone;->setTTYMode(ILandroid/os/Message;)V

    #@5
    .line 1052
    return-void
.end method

.method public setUnitTestMode(Z)V
    .registers 3
    .parameter "f"

    #@0
    .prologue
    .line 955
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->setUnitTestMode(Z)V

    #@5
    .line 956
    return-void
.end method

.method public setVoiceMailNumber(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 5
    .parameter "alphaTag"
    .parameter "voiceMailNumber"
    .parameter "onComplete"

    #@0
    .prologue
    .line 816
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/telephony/Phone;->setVoiceMailNumber(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    #@5
    .line 817
    return-void
.end method

.method public setVoiceMailNumberForALS(ILjava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 7
    .parameter "line"
    .parameter "alphaTag"
    .parameter "voiceMailNumber"
    .parameter "onComplete"

    #@0
    .prologue
    .line 1492
    const-string v0, "PHONE"

    #@2
    const-string v1, "dummy! setVoiceMailNumberForALS"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1493
    return-void
.end method

.method public startDtmf(C)V
    .registers 3
    .parameter "c"

    #@0
    .prologue
    .line 738
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->startDtmf(C)V

    #@5
    .line 739
    return-void
.end method

.method public stopDtmf()V
    .registers 2

    #@0
    .prologue
    .line 742
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->stopDtmf()V

    #@5
    .line 743
    return-void
.end method

.method public storeALSSettingValue(I)V
    .registers 4
    .parameter "value"

    #@0
    .prologue
    .line 1447
    const-string v0, "PHONE"

    #@2
    const-string v1, "dummy! storeALSSettingValue"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1448
    return-void
.end method

.method public storeCLIRSettingValue(I)V
    .registers 4
    .parameter "value"

    #@0
    .prologue
    .line 1430
    const-string v0, "PHONE"

    #@2
    const-string v1, "dummy! storeCLIRSettingValue"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1431
    return-void
.end method

.method public switchHoldingAndActive()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 645
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->switchHoldingAndActive()V

    #@5
    .line 646
    return-void
.end method

.method public toggleCurrentLine()I
    .registers 3

    #@0
    .prologue
    .line 1466
    const-string v0, "PHONE"

    #@2
    const-string v1, "dummy! toggleCurrentLine"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1467
    const/4 v0, 0x0

    #@8
    return v0
.end method

.method public uknightEventSet([BLandroid/os/Message;)V
    .registers 4
    .parameter "data"
    .parameter "response"

    #@0
    .prologue
    .line 1314
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/Phone;->uknightEventSet([BLandroid/os/Message;)V

    #@5
    .line 1315
    return-void
.end method

.method public uknightGetData(ILandroid/os/Message;)V
    .registers 4
    .parameter "buf_num"
    .parameter "response"

    #@0
    .prologue
    .line 1324
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/Phone;->uknightGetData(ILandroid/os/Message;)V

    #@5
    .line 1325
    return-void
.end method

.method public uknightLogSet([BLandroid/os/Message;)V
    .registers 4
    .parameter "data"
    .parameter "response"

    #@0
    .prologue
    .line 1311
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/Phone;->uknightLogSet([BLandroid/os/Message;)V

    #@5
    .line 1312
    return-void
.end method

.method public uknightMemCheck(Landroid/os/Message;)V
    .registers 3
    .parameter "response"

    #@0
    .prologue
    .line 1327
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->uknightMemCheck(Landroid/os/Message;)V

    #@5
    .line 1328
    return-void
.end method

.method public uknightMemSet(ILandroid/os/Message;)V
    .registers 4
    .parameter "percent"
    .parameter "response"

    #@0
    .prologue
    .line 1321
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/Phone;->uknightMemSet(ILandroid/os/Message;)V

    #@5
    .line 1322
    return-void
.end method

.method public uknightStateChangeSet(ILandroid/os/Message;)V
    .registers 4
    .parameter "event"
    .parameter "response"

    #@0
    .prologue
    .line 1317
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1, p2}, Lcom/android/internal/telephony/Phone;->uknightStateChangeSet(ILandroid/os/Message;)V

    #@5
    .line 1318
    return-void
.end method

.method public unregisterForAvpUpgradeFailure(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1306
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->unregisterForAvpUpgradeFailure(Landroid/os/Handler;)V

    #@5
    .line 1307
    return-void
.end method

.method public unregisterForCallWaiting(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1153
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->unregisterForCallWaiting(Landroid/os/Handler;)V

    #@5
    .line 1154
    return-void
.end method

.method public unregisterForCdmaOtaStatusChange(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 573
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->unregisterForCdmaOtaStatusChange(Landroid/os/Handler;)V

    #@5
    .line 574
    return-void
.end method

.method public unregisterForDisconnect(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 501
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->unregisterForDisconnect(Landroid/os/Handler;)V

    #@5
    .line 502
    return-void
.end method

.method public unregisterForDisplayInfo(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1169
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->unregisterForDisplayInfo(Landroid/os/Handler;)V

    #@5
    .line 1170
    return-void
.end method

.method public unregisterForEcmTimerReset(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 589
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->unregisterForEcmTimerReset(Landroid/os/Handler;)V

    #@5
    .line 590
    return-void
.end method

.method public unregisterForInCallVoicePrivacyOff(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 565
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->unregisterForInCallVoicePrivacyOff(Landroid/os/Handler;)V

    #@5
    .line 566
    return-void
.end method

.method public unregisterForInCallVoicePrivacyOn(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 557
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->unregisterForInCallVoicePrivacyOn(Landroid/os/Handler;)V

    #@5
    .line 558
    return-void
.end method

.method public unregisterForIncomingRing(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 493
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->unregisterForIncomingRing(Landroid/os/Handler;)V

    #@5
    .line 494
    return-void
.end method

.method public unregisterForLineControlInfo(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1193
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->unregisterForLineControlInfo(Landroid/os/Handler;)V

    #@5
    .line 1194
    return-void
.end method

.method public unregisterForMmiComplete(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 517
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->unregisterForMmiComplete(Landroid/os/Handler;)V

    #@5
    .line 518
    return-void
.end method

.method public unregisterForMmiInitiate(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 509
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->unregisterForMmiInitiate(Landroid/os/Handler;)V

    #@5
    .line 510
    return-void
.end method

.method public unregisterForModifyCallRequest(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1297
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->unregisterForModifyCallRequest(Landroid/os/Handler;)V

    #@5
    .line 1298
    return-void
.end method

.method public unregisterForNewRingingConnection(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 485
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->unregisterForNewRingingConnection(Landroid/os/Handler;)V

    #@5
    .line 486
    return-void
.end method

.method public unregisterForNumberInfo(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1177
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->unregisterForNumberInfo(Landroid/os/Handler;)V

    #@5
    .line 1178
    return-void
.end method

.method public unregisterForPreciseCallStateChanged(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 477
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->unregisterForPreciseCallStateChanged(Landroid/os/Handler;)V

    #@5
    .line 478
    return-void
.end method

.method public unregisterForRedirectedNumberInfo(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1185
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->unregisterForRedirectedNumberInfo(Landroid/os/Handler;)V

    #@5
    .line 1186
    return-void
.end method

.method public unregisterForResendIncallMute(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 605
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->unregisterForResendIncallMute(Landroid/os/Handler;)V

    #@5
    .line 606
    return-void
.end method

.method public unregisterForRingbackTone(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 597
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->unregisterForRingbackTone(Landroid/os/Handler;)V

    #@5
    .line 598
    return-void
.end method

.method public unregisterForServiceStateChanged(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 533
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->unregisterForServiceStateChanged(Landroid/os/Handler;)V

    #@5
    .line 534
    return-void
.end method

.method public unregisterForSignalInfo(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1161
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->unregisterForSignalInfo(Landroid/os/Handler;)V

    #@5
    .line 1162
    return-void
.end method

.method public unregisterForSimRecordsLoaded(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 613
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->unregisterForSimRecordsLoaded(Landroid/os/Handler;)V

    #@5
    .line 614
    return-void
.end method

.method public unregisterForSubscriptionInfoReady(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 581
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->unregisterForSubscriptionInfoReady(Landroid/os/Handler;)V

    #@5
    .line 582
    return-void
.end method

.method public unregisterForSuppServiceFailed(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 549
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->unregisterForSuppServiceFailed(Landroid/os/Handler;)V

    #@5
    .line 550
    return-void
.end method

.method public unregisterForSuppServiceNotification(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 541
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->unregisterForSuppServiceNotification(Landroid/os/Handler;)V

    #@5
    .line 542
    return-void
.end method

.method public unregisterForT53AudioControlInfo(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1209
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->unregisterForT53AudioControlInfo(Landroid/os/Handler;)V

    #@5
    .line 1210
    return-void
.end method

.method public unregisterForT53ClirInfo(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1201
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->unregisterForT53ClirInfo(Landroid/os/Handler;)V

    #@5
    .line 1202
    return-void
.end method

.method public unregisterForUnknownConnection(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 469
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->unregisterForUnknownConnection(Landroid/os/Handler;)V

    #@5
    .line 470
    return-void
.end method

.method public unregisterLGECipheringInd(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1347
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->unregisterLGECipheringInd(Landroid/os/Handler;)V

    #@5
    .line 1348
    return-void
.end method

.method public unregisterLGEUnsol(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1362
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->unregisterLGEUnsol(Landroid/os/Handler;)V

    #@5
    .line 1363
    return-void
.end method

.method public unsetOnEcbModeExitResponse(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1217
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->unsetOnEcbModeExitResponse(Landroid/os/Handler;)V

    #@5
    .line 1218
    return-void
.end method

.method public updatePhoneObject(I)V
    .registers 9
    .parameter "newVoiceRadioTech"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v5, 0x2

    #@2
    const/4 v4, 0x1

    #@3
    .line 226
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@5
    if-eqz v2, :cond_58

    #@7
    .line 228
    const-string v2, "KDDI"

    #@9
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@c
    move-result v2

    #@d
    if-eqz v2, :cond_80

    #@f
    const/16 v2, 0xe

    #@11
    if-ne p1, v2, :cond_80

    #@13
    .line 230
    invoke-direct {p0}, Lcom/android/internal/telephony/PhoneProxy;->getPhoneType_inLTE()I

    #@16
    move-result v1

    #@17
    .line 232
    .local v1, phoneType:I
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@19
    invoke-interface {v2}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    #@1c
    move-result v2

    #@1d
    if-ne v1, v2, :cond_3c

    #@1f
    .line 233
    new-instance v2, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v3, "KDDI Ignoring voice radio technology changed message. newVoiceRadioTech = RIL_RADIO_TECHNOLOGY_LTE Active Phone = "

    #@26
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v2

    #@2a
    iget-object v3, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2c
    invoke-interface {v3}, Lcom/android/internal/telephony/Phone;->getPhoneName()Ljava/lang/String;

    #@2f
    move-result-object v3

    #@30
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v2

    #@34
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v2

    #@38
    invoke-static {v2}, Lcom/android/internal/telephony/PhoneProxy;->logd(Ljava/lang/String;)V

    #@3b
    .line 321
    .end local v1           #phoneType:I
    :goto_3b
    return-void

    #@3c
    .line 239
    .restart local v1       #phoneType:I
    :cond_3c
    new-instance v2, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v3, "KDDI update phone object newVoiceRadioTech = RIL_RADIO_TECHNOLOGY_LTE current Active Phone is "

    #@43
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v2

    #@47
    iget-object v3, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@49
    invoke-interface {v3}, Lcom/android/internal/telephony/Phone;->getPhoneName()Ljava/lang/String;

    #@4c
    move-result-object v3

    #@4d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v2

    #@51
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v2

    #@55
    invoke-static {v2}, Lcom/android/internal/telephony/PhoneProxy;->logd(Ljava/lang/String;)V

    #@58
    .line 280
    .end local v1           #phoneType:I
    :cond_58
    :goto_58
    if-nez p1, :cond_136

    #@5a
    .line 283
    new-instance v2, Ljava/lang/StringBuilder;

    #@5c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5f
    const-string v3, "Ignoring voice radio technology changed message. newVoiceRadioTech = Unknown. Active Phone = "

    #@61
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v3

    #@65
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@67
    if-eqz v2, :cond_132

    #@69
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@6b
    invoke-interface {v2}, Lcom/android/internal/telephony/Phone;->getPhoneName()Ljava/lang/String;

    #@6e
    move-result-object v2

    #@6f
    :goto_6f
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v2

    #@73
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@76
    move-result-object v2

    #@77
    invoke-static {v2}, Lcom/android/internal/telephony/PhoneProxy;->logd(Ljava/lang/String;)V

    #@7a
    .line 288
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneProxy;->mIccCardProxy:Lcom/android/internal/telephony/uicc/IccCardProxy;

    #@7c
    invoke-virtual {v2, p1}, Lcom/android/internal/telephony/uicc/IccCardProxy;->setVoiceRadioTech(I)V

    #@7f
    goto :goto_3b

    #@80
    .line 246
    :cond_80
    iget v2, p0, Lcom/android/internal/telephony/PhoneProxy;->mRilVersion:I

    #@82
    const/4 v3, 0x6

    #@83
    if-ne v2, v3, :cond_e9

    #@85
    invoke-virtual {p0}, Lcom/android/internal/telephony/PhoneProxy;->getLteOnCdmaMode()I

    #@88
    move-result v2

    #@89
    if-ne v2, v4, :cond_e9

    #@8b
    .line 251
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@8d
    invoke-interface {v2}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    #@90
    move-result v2

    #@91
    if-ne v2, v5, :cond_c0

    #@93
    .line 252
    new-instance v2, Ljava/lang/StringBuilder;

    #@95
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@98
    const-string v3, "LTE ON CDMA property is set. Use CDMA Phone newVoiceRadioTech = "

    #@9a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v2

    #@9e
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v2

    #@a2
    const-string v3, " Active Phone = "

    #@a4
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v2

    #@a8
    iget-object v3, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@aa
    invoke-interface {v3}, Lcom/android/internal/telephony/Phone;->getPhoneName()Ljava/lang/String;

    #@ad
    move-result-object v3

    #@ae
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v2

    #@b2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b5
    move-result-object v2

    #@b6
    invoke-static {v2}, Lcom/android/internal/telephony/PhoneProxy;->logd(Ljava/lang/String;)V

    #@b9
    .line 256
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneProxy;->mIccCardProxy:Lcom/android/internal/telephony/uicc/IccCardProxy;

    #@bb
    invoke-virtual {v2, p1}, Lcom/android/internal/telephony/uicc/IccCardProxy;->setVoiceRadioTech(I)V

    #@be
    goto/16 :goto_3b

    #@c0
    .line 259
    :cond_c0
    new-instance v2, Ljava/lang/StringBuilder;

    #@c2
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@c5
    const-string v3, "LTE ON CDMA property is set. Switch to CDMALTEPhone newVoiceRadioTech = "

    #@c7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ca
    move-result-object v2

    #@cb
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v2

    #@cf
    const-string v3, " Active Phone = "

    #@d1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d4
    move-result-object v2

    #@d5
    iget-object v3, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@d7
    invoke-interface {v3}, Lcom/android/internal/telephony/Phone;->getPhoneName()Ljava/lang/String;

    #@da
    move-result-object v3

    #@db
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@de
    move-result-object v2

    #@df
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e2
    move-result-object v2

    #@e3
    invoke-static {v2}, Lcom/android/internal/telephony/PhoneProxy;->logd(Ljava/lang/String;)V

    #@e6
    .line 262
    const/4 p1, 0x6

    #@e7
    goto/16 :goto_58

    #@e9
    .line 265
    :cond_e9
    invoke-static {p1}, Landroid/telephony/ServiceState;->isCdma(I)Z

    #@ec
    move-result v2

    #@ed
    if-eqz v2, :cond_f7

    #@ef
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@f1
    invoke-interface {v2}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    #@f4
    move-result v2

    #@f5
    if-eq v2, v5, :cond_105

    #@f7
    :cond_f7
    invoke-static {p1}, Landroid/telephony/ServiceState;->isGsm(I)Z

    #@fa
    move-result v2

    #@fb
    if-eqz v2, :cond_58

    #@fd
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@ff
    invoke-interface {v2}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    #@102
    move-result v2

    #@103
    if-ne v2, v4, :cond_58

    #@105
    .line 270
    :cond_105
    new-instance v2, Ljava/lang/StringBuilder;

    #@107
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@10a
    const-string v3, "Ignoring voice radio technology changed message. newVoiceRadioTech = "

    #@10c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10f
    move-result-object v2

    #@110
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@113
    move-result-object v2

    #@114
    const-string v3, " Active Phone = "

    #@116
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@119
    move-result-object v2

    #@11a
    iget-object v3, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@11c
    invoke-interface {v3}, Lcom/android/internal/telephony/Phone;->getPhoneName()Ljava/lang/String;

    #@11f
    move-result-object v3

    #@120
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@123
    move-result-object v2

    #@124
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@127
    move-result-object v2

    #@128
    invoke-static {v2}, Lcom/android/internal/telephony/PhoneProxy;->logd(Ljava/lang/String;)V

    #@12b
    .line 274
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneProxy;->mIccCardProxy:Lcom/android/internal/telephony/uicc/IccCardProxy;

    #@12d
    invoke-virtual {v2, p1}, Lcom/android/internal/telephony/uicc/IccCardProxy;->setVoiceRadioTech(I)V

    #@130
    goto/16 :goto_3b

    #@132
    .line 283
    :cond_132
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@134
    goto/16 :goto_6f

    #@136
    .line 292
    :cond_136
    const/4 v0, 0x0

    #@137
    .line 293
    .local v0, oldPowerState:Z
    iget-boolean v2, p0, Lcom/android/internal/telephony/PhoneProxy;->mResetModemOnRadioTechnologyChange:Z

    #@139
    if-eqz v2, :cond_153

    #@13b
    .line 294
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneProxy;->mCommandsInterface:Lcom/android/internal/telephony/CommandsInterface;

    #@13d
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getRadioState()Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@140
    move-result-object v2

    #@141
    invoke-virtual {v2}, Lcom/android/internal/telephony/CommandsInterface$RadioState;->isOn()Z

    #@144
    move-result v2

    #@145
    if-eqz v2, :cond_153

    #@147
    .line 295
    const/4 v0, 0x1

    #@148
    .line 296
    const-string v2, "Setting Radio Power to Off"

    #@14a
    invoke-static {v2}, Lcom/android/internal/telephony/PhoneProxy;->logd(Ljava/lang/String;)V

    #@14d
    .line 297
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneProxy;->mCommandsInterface:Lcom/android/internal/telephony/CommandsInterface;

    #@14f
    const/4 v3, 0x0

    #@150
    invoke-interface {v2, v3, v6}, Lcom/android/internal/telephony/CommandsInterface;->setRadioPower(ZLandroid/os/Message;)V

    #@153
    .line 301
    :cond_153
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/PhoneProxy;->deleteAndCreatePhone(I)V

    #@156
    .line 303
    iget-boolean v2, p0, Lcom/android/internal/telephony/PhoneProxy;->mResetModemOnRadioTechnologyChange:Z

    #@158
    if-eqz v2, :cond_166

    #@15a
    if-eqz v0, :cond_166

    #@15c
    .line 304
    const-string v2, "Resetting Radio"

    #@15e
    invoke-static {v2}, Lcom/android/internal/telephony/PhoneProxy;->logd(Ljava/lang/String;)V

    #@161
    .line 305
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneProxy;->mCommandsInterface:Lcom/android/internal/telephony/CommandsInterface;

    #@163
    invoke-interface {v2, v0, v6}, Lcom/android/internal/telephony/CommandsInterface;->setRadioPower(ZLandroid/os/Message;)V

    #@166
    .line 309
    :cond_166
    iget-object v3, p0, Lcom/android/internal/telephony/PhoneProxy;->mIccSmsInterfaceManager:Lcom/android/internal/telephony/IccSmsInterfaceManager;

    #@168
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@16a
    check-cast v2, Lcom/android/internal/telephony/PhoneBase;

    #@16c
    invoke-virtual {v3, v2}, Lcom/android/internal/telephony/IccSmsInterfaceManager;->updatePhoneObject(Lcom/android/internal/telephony/PhoneBase;)V

    #@16f
    .line 310
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneProxy;->mIccPhoneBookInterfaceManagerProxy:Lcom/android/internal/telephony/IccPhoneBookInterfaceManagerProxy;

    #@171
    iget-object v3, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@173
    invoke-interface {v3}, Lcom/android/internal/telephony/Phone;->getIccPhoneBookInterfaceManager()Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;

    #@176
    move-result-object v3

    #@177
    invoke-virtual {v2, v3}, Lcom/android/internal/telephony/IccPhoneBookInterfaceManagerProxy;->setmIccPhoneBookInterfaceManager(Lcom/android/internal/telephony/IccPhoneBookInterfaceManager;)V

    #@17a
    .line 312
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneProxy;->mPhoneSubInfoProxy:Lcom/android/internal/telephony/PhoneSubInfoProxy;

    #@17c
    iget-object v3, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@17e
    invoke-interface {v3}, Lcom/android/internal/telephony/Phone;->getPhoneSubInfo()Lcom/android/internal/telephony/PhoneSubInfo;

    #@181
    move-result-object v3

    #@182
    invoke-virtual {v2, v3}, Lcom/android/internal/telephony/PhoneSubInfoProxy;->setmPhoneSubInfo(Lcom/android/internal/telephony/PhoneSubInfo;)V

    #@185
    .line 314
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@187
    check-cast v2, Lcom/android/internal/telephony/PhoneBase;

    #@189
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@18b
    iput-object v2, p0, Lcom/android/internal/telephony/PhoneProxy;->mCommandsInterface:Lcom/android/internal/telephony/CommandsInterface;

    #@18d
    .line 315
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneProxy;->mIccCardProxy:Lcom/android/internal/telephony/uicc/IccCardProxy;

    #@18f
    invoke-virtual {v2, p1}, Lcom/android/internal/telephony/uicc/IccCardProxy;->setVoiceRadioTech(I)V

    #@192
    .line 316
    invoke-virtual {p0}, Lcom/android/internal/telephony/PhoneProxy;->sendBroadcastStickyIntent()V

    #@195
    .line 319
    iget-object v2, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@197
    invoke-interface {v2}, Lcom/android/internal/telephony/Phone;->getServiceState()Landroid/telephony/ServiceState;

    #@19a
    move-result-object v2

    #@19b
    iget-object v3, p0, Lcom/android/internal/telephony/PhoneProxy;->mLgeRssiData:Lcom/android/internal/telephony/LgeRssiData;

    #@19d
    invoke-virtual {v2, v3}, Landroid/telephony/ServiceState;->setLgeRssiData(Lcom/android/internal/telephony/LgeRssiData;)V

    #@1a0
    goto/16 :goto_3b
.end method

.method public updateServiceLocation()V
    .registers 2

    #@0
    .prologue
    .line 943
    iget-object v0, p0, Lcom/android/internal/telephony/PhoneProxy;->mActivePhone:Lcom/android/internal/telephony/Phone;

    #@2
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->updateServiceLocation()V

    #@5
    .line 944
    return-void
.end method
