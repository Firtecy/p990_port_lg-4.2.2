.class Lcom/android/internal/telephony/gfit/GfitUtils$4;
.super Ljava/lang/Object;
.source "GfitUtils.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/telephony/gfit/GfitUtils;->createNotePopupGlobal()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/gfit/GfitUtils;

.field final synthetic val$linear:Landroid/widget/LinearLayout;


# direct methods
.method constructor <init>(Lcom/android/internal/telephony/gfit/GfitUtils;Landroid/widget/LinearLayout;)V
    .registers 3
    .parameter
    .parameter

    #@0
    .prologue
    .line 1119
    iput-object p1, p0, Lcom/android/internal/telephony/gfit/GfitUtils$4;->this$0:Lcom/android/internal/telephony/gfit/GfitUtils;

    #@2
    iput-object p2, p0, Lcom/android/internal/telephony/gfit/GfitUtils$4;->val$linear:Landroid/widget/LinearLayout;

    #@4
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@7
    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .registers 7
    .parameter "dialog"
    .parameter "which"

    #@0
    .prologue
    .line 1121
    iget-object v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils$4;->val$linear:Landroid/widget/LinearLayout;

    #@2
    const v2, 0x20d003f

    #@5
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Landroid/widget/CheckBox;

    #@b
    .line 1122
    .local v0, ignoreGlobalPopup:Landroid/widget/CheckBox;
    packed-switch p2, :pswitch_data_44

    #@e
    .line 1137
    :goto_e
    return-void

    #@f
    .line 1125
    :pswitch_f
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    #@12
    move-result v1

    #@13
    if-eqz v1, :cond_42

    #@15
    const/4 v1, 0x1

    #@16
    :goto_16
    sput-boolean v1, Lcom/android/internal/telephony/gfit/GfitUtils;->isNoGlobalPopupNeeded:Z

    #@18
    .line 1126
    iget-object v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils$4;->this$0:Lcom/android/internal/telephony/gfit/GfitUtils;

    #@1a
    new-instance v2, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v3, "isNoGlobalPopupNeeded = "

    #@21
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    sget-boolean v3, Lcom/android/internal/telephony/gfit/GfitUtils;->isNoGlobalPopupNeeded:Z

    #@27
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v2

    #@2f
    invoke-static {v1, v2}, Lcom/android/internal/telephony/gfit/GfitUtils;->access$000(Lcom/android/internal/telephony/gfit/GfitUtils;Ljava/lang/String;)V

    #@32
    .line 1128
    iget-object v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils$4;->this$0:Lcom/android/internal/telephony/gfit/GfitUtils;

    #@34
    const-string v2, "Set global mode"

    #@36
    invoke-static {v1, v2}, Lcom/android/internal/telephony/gfit/GfitUtils;->access$300(Lcom/android/internal/telephony/gfit/GfitUtils;Ljava/lang/String;)V

    #@39
    .line 1131
    iget-object v1, p0, Lcom/android/internal/telephony/gfit/GfitUtils$4;->this$0:Lcom/android/internal/telephony/gfit/GfitUtils;

    #@3b
    invoke-virtual {v1}, Lcom/android/internal/telephony/gfit/GfitUtils;->switchToGlobalMode()V

    #@3e
    .line 1134
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    #@41
    goto :goto_e

    #@42
    .line 1125
    :cond_42
    const/4 v1, 0x0

    #@43
    goto :goto_16

    #@44
    .line 1122
    :pswitch_data_44
    .packed-switch -0x2
        :pswitch_f
        :pswitch_f
    .end packed-switch
.end method
