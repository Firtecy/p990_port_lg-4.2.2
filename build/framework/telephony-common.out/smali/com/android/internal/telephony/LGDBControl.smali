.class public Lcom/android/internal/telephony/LGDBControl;
.super Ljava/lang/Object;
.source "LGDBControl.java"


# static fields
.field private static APN_INDEX:I = 0x0

.field private static AUTH_TYPE_INDEX:I = 0x0

.field private static BEARER_INDEX:I = 0x0

.field private static ID_INDEX:I = 0x0

.field private static IP_INDEX:I = 0x0

.field static final LOG_TAG:Ljava/lang/String; = "LGDBControl"

.field private static MCC_INDEX:I = 0x0

.field private static MMSC_INDEX:I = 0x0

.field private static MMSPORT_INDEX:I = 0x0

.field private static MMSPROXY_INDEX:I = 0x0

.field private static MNC_INDEX:I = 0x0

.field private static NAME_INDEX:I = 0x0

.field private static NUMERIC_INDEX:I = 0x0

.field private static PASSWORD_INDEX:I = 0x0

.field private static PORT_INDEX:I = 0x0

.field private static PROXY_INDEX:I = 0x0

.field private static final QUERY_WHERE_CLAUSE:Ljava/lang/String; = "_id=?"

.field private static SERVER_INDEX:I

.field private static SYSPROP_NETWORK_CODE_SPRINT:Ljava/lang/String;

.field private static TYPE_INDEX:I

.field private static USER_INDEX:I

.field private static mUri:Landroid/net/Uri;

.field private static networkOperator:Ljava/lang/String;

.field private static sProjection:[Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field public mfeatureset:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    #@0
    .prologue
    const/4 v6, 0x4

    #@1
    const/4 v5, 0x3

    #@2
    const/4 v4, 0x2

    #@3
    const/4 v3, 0x1

    #@4
    const/4 v2, 0x0

    #@5
    .line 84
    const/4 v0, 0x0

    #@6
    sput-object v0, Lcom/android/internal/telephony/LGDBControl;->networkOperator:Ljava/lang/String;

    #@8
    .line 86
    const-string v0, "310120"

    #@a
    sput-object v0, Lcom/android/internal/telephony/LGDBControl;->SYSPROP_NETWORK_CODE_SPRINT:Ljava/lang/String;

    #@c
    .line 89
    sput v2, Lcom/android/internal/telephony/LGDBControl;->ID_INDEX:I

    #@e
    .line 90
    sput v3, Lcom/android/internal/telephony/LGDBControl;->NAME_INDEX:I

    #@10
    .line 91
    sput v4, Lcom/android/internal/telephony/LGDBControl;->APN_INDEX:I

    #@12
    .line 92
    sput v5, Lcom/android/internal/telephony/LGDBControl;->PROXY_INDEX:I

    #@14
    .line 93
    sput v6, Lcom/android/internal/telephony/LGDBControl;->PORT_INDEX:I

    #@16
    .line 94
    const/4 v0, 0x5

    #@17
    sput v0, Lcom/android/internal/telephony/LGDBControl;->USER_INDEX:I

    #@19
    .line 95
    const/4 v0, 0x6

    #@1a
    sput v0, Lcom/android/internal/telephony/LGDBControl;->SERVER_INDEX:I

    #@1c
    .line 96
    const/4 v0, 0x7

    #@1d
    sput v0, Lcom/android/internal/telephony/LGDBControl;->PASSWORD_INDEX:I

    #@1f
    .line 97
    const/16 v0, 0x8

    #@21
    sput v0, Lcom/android/internal/telephony/LGDBControl;->MMSC_INDEX:I

    #@23
    .line 98
    const/16 v0, 0x9

    #@25
    sput v0, Lcom/android/internal/telephony/LGDBControl;->MCC_INDEX:I

    #@27
    .line 99
    const/16 v0, 0xa

    #@29
    sput v0, Lcom/android/internal/telephony/LGDBControl;->MNC_INDEX:I

    #@2b
    .line 100
    const/16 v0, 0xb

    #@2d
    sput v0, Lcom/android/internal/telephony/LGDBControl;->NUMERIC_INDEX:I

    #@2f
    .line 101
    const/16 v0, 0xc

    #@31
    sput v0, Lcom/android/internal/telephony/LGDBControl;->MMSPROXY_INDEX:I

    #@33
    .line 102
    const/16 v0, 0xd

    #@35
    sput v0, Lcom/android/internal/telephony/LGDBControl;->MMSPORT_INDEX:I

    #@37
    .line 103
    const/16 v0, 0xe

    #@39
    sput v0, Lcom/android/internal/telephony/LGDBControl;->AUTH_TYPE_INDEX:I

    #@3b
    .line 104
    const/16 v0, 0xf

    #@3d
    sput v0, Lcom/android/internal/telephony/LGDBControl;->TYPE_INDEX:I

    #@3f
    .line 105
    const/16 v0, 0x10

    #@41
    sput v0, Lcom/android/internal/telephony/LGDBControl;->IP_INDEX:I

    #@43
    .line 108
    const/16 v0, 0x11

    #@45
    sput v0, Lcom/android/internal/telephony/LGDBControl;->BEARER_INDEX:I

    #@47
    .line 111
    const-string v0, "content://telephony/carriers"

    #@49
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@4c
    move-result-object v0

    #@4d
    sput-object v0, Lcom/android/internal/telephony/LGDBControl;->mUri:Landroid/net/Uri;

    #@4f
    .line 116
    const/16 v0, 0x12

    #@51
    new-array v0, v0, [Ljava/lang/String;

    #@53
    const-string v1, "_id"

    #@55
    aput-object v1, v0, v2

    #@57
    const-string v1, "name"

    #@59
    aput-object v1, v0, v3

    #@5b
    const-string v1, "apn"

    #@5d
    aput-object v1, v0, v4

    #@5f
    const-string v1, "proxy"

    #@61
    aput-object v1, v0, v5

    #@63
    const-string v1, "port"

    #@65
    aput-object v1, v0, v6

    #@67
    const/4 v1, 0x5

    #@68
    const-string v2, "user"

    #@6a
    aput-object v2, v0, v1

    #@6c
    const/4 v1, 0x6

    #@6d
    const-string v2, "server"

    #@6f
    aput-object v2, v0, v1

    #@71
    const/4 v1, 0x7

    #@72
    const-string v2, "password"

    #@74
    aput-object v2, v0, v1

    #@76
    const/16 v1, 0x8

    #@78
    const-string v2, "mmsc"

    #@7a
    aput-object v2, v0, v1

    #@7c
    const/16 v1, 0x9

    #@7e
    const-string v2, "mcc"

    #@80
    aput-object v2, v0, v1

    #@82
    const/16 v1, 0xa

    #@84
    const-string v2, "mnc"

    #@86
    aput-object v2, v0, v1

    #@88
    const/16 v1, 0xb

    #@8a
    const-string v2, "numeric"

    #@8c
    aput-object v2, v0, v1

    #@8e
    const/16 v1, 0xc

    #@90
    const-string v2, "mmsproxy"

    #@92
    aput-object v2, v0, v1

    #@94
    const/16 v1, 0xd

    #@96
    const-string v2, "mmsport"

    #@98
    aput-object v2, v0, v1

    #@9a
    const/16 v1, 0xe

    #@9c
    const-string v2, "authtype"

    #@9e
    aput-object v2, v0, v1

    #@a0
    const/16 v1, 0xf

    #@a2
    const-string v2, "type"

    #@a4
    aput-object v2, v0, v1

    #@a6
    const/16 v1, 0x10

    #@a8
    const-string v2, "protocol"

    #@aa
    aput-object v2, v0, v1

    #@ac
    const/16 v1, 0x11

    #@ae
    const-string v2, "bearer"

    #@b0
    aput-object v2, v0, v1

    #@b2
    sput-object v0, Lcom/android/internal/telephony/LGDBControl;->sProjection:[Ljava/lang/String;

    #@b4
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 5
    .parameter "context"

    #@0
    .prologue
    .line 146
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 88
    const-string v0, "ro.afwdata.LGfeatureset"

    #@5
    const-string v1, "none"

    #@7
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    iput-object v0, p0, Lcom/android/internal/telephony/LGDBControl;->mfeatureset:Ljava/lang/String;

    #@d
    .line 147
    iput-object p1, p0, Lcom/android/internal/telephony/LGDBControl;->mContext:Landroid/content/Context;

    #@f
    .line 153
    iget-object v0, p0, Lcom/android/internal/telephony/LGDBControl;->mfeatureset:Ljava/lang/String;

    #@11
    const-string v1, "SPCSBASE"

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@16
    move-result v0

    #@17
    if-eqz v0, :cond_38

    #@19
    .line 155
    sget-object v0, Lcom/android/internal/telephony/LGDBControl;->SYSPROP_NETWORK_CODE_SPRINT:Ljava/lang/String;

    #@1b
    sput-object v0, Lcom/android/internal/telephony/LGDBControl;->networkOperator:Ljava/lang/String;

    #@1d
    .line 156
    const-string v0, "LGDBControl"

    #@1f
    new-instance v1, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v2, "!!! SPCSBASE networkOperator :: "

    #@26
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v1

    #@2a
    sget-object v2, Lcom/android/internal/telephony/LGDBControl;->networkOperator:Ljava/lang/String;

    #@2c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v1

    #@30
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v1

    #@34
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    .line 165
    :goto_37
    return-void

    #@38
    .line 160
    :cond_38
    const-string v0, "gsm.sim.operator.numeric"

    #@3a
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@3d
    move-result-object v0

    #@3e
    sput-object v0, Lcom/android/internal/telephony/LGDBControl;->networkOperator:Ljava/lang/String;

    #@40
    .line 161
    const-string v0, "LGDBControl"

    #@42
    new-instance v1, Ljava/lang/StringBuilder;

    #@44
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    const-string v2, "!!! NOT SPCSBASE, networkOperator :: "

    #@49
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v1

    #@4d
    sget-object v2, Lcom/android/internal/telephony/LGDBControl;->networkOperator:Ljava/lang/String;

    #@4f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v1

    #@53
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@56
    move-result-object v1

    #@57
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5a
    goto :goto_37
.end method

.method public static getSpLTEApnFromDb(Landroid/content/Context;IZ)Ljava/lang/String;
    .registers 16
    .parameter "c"
    .parameter "row"
    .parameter "dummy"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 708
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@4
    move-result-object v7

    #@5
    .line 711
    .local v7, cr:Landroid/content/ContentResolver;
    new-instance v0, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v1, "numeric=\""

    #@c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v0

    #@10
    sget-object v1, Lcom/android/internal/telephony/LGDBControl;->networkOperator:Ljava/lang/String;

    #@12
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v0

    #@16
    const-string v1, "\""

    #@18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v0

    #@1c
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v3

    #@20
    .line 715
    .local v3, where:Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@23
    move-result-object v0

    #@24
    sget-object v1, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    #@26
    sget-object v2, Lcom/android/internal/telephony/LGDBControl;->sProjection:[Ljava/lang/String;

    #@28
    const-string v5, "_id"

    #@2a
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@2d
    move-result-object v9

    #@2e
    .line 719
    .local v9, mCursor:Landroid/database/Cursor;
    if-nez v9, :cond_38

    #@30
    .line 721
    const-string v0, "LGDBControl"

    #@32
    const-string v1, " Cursor is null"

    #@34
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    .line 791
    :goto_37
    return-object v4

    #@38
    .line 724
    :cond_38
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    #@3b
    .line 725
    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    #@3e
    move-result v6

    #@3f
    .line 726
    .local v6, count:I
    const-string v0, "LGDBControl"

    #@41
    new-instance v1, Ljava/lang/StringBuilder;

    #@43
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@46
    const-string v2, "now in the db for "

    #@48
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v1

    #@4c
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v1

    #@50
    const-string v2, " \'s count is "

    #@52
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v1

    #@56
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@59
    move-result-object v1

    #@5a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5d
    move-result-object v1

    #@5e
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@61
    .line 729
    if-gt v6, p1, :cond_69

    #@63
    .line 731
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@66
    .line 732
    const-string v4, "0"

    #@68
    goto :goto_37

    #@69
    .line 734
    :cond_69
    invoke-interface {v9, p1}, Landroid/database/Cursor;->move(I)Z

    #@6c
    .line 736
    sget v0, Lcom/android/internal/telephony/LGDBControl;->ID_INDEX:I

    #@6e
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@71
    move-result-object v8

    #@72
    .line 737
    .local v8, key:Ljava/lang/String;
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@75
    move-result v11

    #@76
    .line 738
    .local v11, pos:I
    const-string v0, "LGDBControl"

    #@78
    new-instance v1, Ljava/lang/StringBuilder;

    #@7a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7d
    const-string v2, "your pos"

    #@7f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v1

    #@83
    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@86
    move-result-object v1

    #@87
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8a
    move-result-object v1

    #@8b
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@8e
    .line 743
    sget v0, Lcom/android/internal/telephony/LGDBControl;->TYPE_INDEX:I

    #@90
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@93
    move-result-object v0

    #@94
    const-string v1, "Dummy"

    #@96
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@99
    move-result v0

    #@9a
    if-nez v0, :cond_aa

    #@9c
    sget v0, Lcom/android/internal/telephony/LGDBControl;->TYPE_INDEX:I

    #@9e
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@a1
    move-result-object v0

    #@a2
    const-string v1, "dummy"

    #@a4
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a7
    move-result v0

    #@a8
    if-eqz v0, :cond_b7

    #@aa
    :cond_aa
    const/4 v0, 0x1

    #@ab
    if-ne p2, v0, :cond_b7

    #@ad
    .line 744
    const-string v0, "LGDBControl"

    #@af
    const-string v1, "[DATA] Dummy APN return 0"

    #@b1
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@b4
    .line 745
    const-string v4, "0"

    #@b6
    goto :goto_37

    #@b7
    .line 751
    :cond_b7
    const-string v10, "LTE|EHRPD"

    #@b9
    .line 752
    .local v10, myBearer:Ljava/lang/String;
    sget v0, Lcom/android/internal/telephony/LGDBControl;->BEARER_INDEX:I

    #@bb
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@be
    move-result-object v0

    #@bf
    const-string v1, "14"

    #@c1
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c4
    move-result v0

    #@c5
    if-eqz v0, :cond_170

    #@c7
    .line 754
    const-string v10, "LTE"

    #@c9
    .line 770
    :cond_c9
    :goto_c9
    new-instance v0, Ljava/lang/StringBuilder;

    #@cb
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@ce
    sget v1, Lcom/android/internal/telephony/LGDBControl;->TYPE_INDEX:I

    #@d0
    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@d3
    move-result-object v1

    #@d4
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d7
    move-result-object v0

    #@d8
    const-string v1, ","

    #@da
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dd
    move-result-object v0

    #@de
    sget v1, Lcom/android/internal/telephony/LGDBControl;->APN_INDEX:I

    #@e0
    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@e3
    move-result-object v1

    #@e4
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e7
    move-result-object v0

    #@e8
    const-string v1, ","

    #@ea
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ed
    move-result-object v0

    #@ee
    sget v1, Lcom/android/internal/telephony/LGDBControl;->IP_INDEX:I

    #@f0
    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@f3
    move-result-object v1

    #@f4
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f7
    move-result-object v0

    #@f8
    const-string v1, ","

    #@fa
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fd
    move-result-object v0

    #@fe
    const-string v1, "0"

    #@100
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@103
    move-result-object v0

    #@104
    const-string v1, ","

    #@106
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@109
    move-result-object v0

    #@10a
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10d
    move-result-object v0

    #@10e
    const-string v1, ","

    #@110
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@113
    move-result-object v0

    #@114
    sget v1, Lcom/android/internal/telephony/LGDBControl;->AUTH_TYPE_INDEX:I

    #@116
    invoke-interface {v9, v1}, Landroid/database/Cursor;->getInt(I)I

    #@119
    move-result v1

    #@11a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11d
    move-result-object v0

    #@11e
    const-string v1, ","

    #@120
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@123
    move-result-object v0

    #@124
    sget v1, Lcom/android/internal/telephony/LGDBControl;->USER_INDEX:I

    #@126
    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@129
    move-result-object v1

    #@12a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12d
    move-result-object v0

    #@12e
    const-string v1, ","

    #@130
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@133
    move-result-object v0

    #@134
    sget v1, Lcom/android/internal/telephony/LGDBControl;->PASSWORD_INDEX:I

    #@136
    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@139
    move-result-object v1

    #@13a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13d
    move-result-object v0

    #@13e
    const-string v1, ","

    #@140
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@143
    move-result-object v0

    #@144
    sget v1, Lcom/android/internal/telephony/LGDBControl;->NAME_INDEX:I

    #@146
    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@149
    move-result-object v1

    #@14a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14d
    move-result-object v0

    #@14e
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@151
    move-result-object v12

    #@152
    .line 789
    .local v12, result:Ljava/lang/String;
    const-string v0, "LGDBControl"

    #@154
    new-instance v1, Ljava/lang/StringBuilder;

    #@156
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@159
    const-string v2, "getting info is !! "

    #@15b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15e
    move-result-object v1

    #@15f
    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@162
    move-result-object v1

    #@163
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@166
    move-result-object v1

    #@167
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@16a
    .line 790
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@16d
    move-object v4, v12

    #@16e
    .line 791
    goto/16 :goto_37

    #@170
    .line 756
    .end local v12           #result:Ljava/lang/String;
    :cond_170
    sget v0, Lcom/android/internal/telephony/LGDBControl;->BEARER_INDEX:I

    #@172
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@175
    move-result-object v0

    #@176
    const-string v1, "13"

    #@178
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@17b
    move-result v0

    #@17c
    if-eqz v0, :cond_182

    #@17e
    .line 758
    const-string v10, "EHRPD"

    #@180
    goto/16 :goto_c9

    #@182
    .line 761
    :cond_182
    sget v0, Lcom/android/internal/telephony/LGDBControl;->BEARER_INDEX:I

    #@184
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@187
    move-result-object v0

    #@188
    const-string v1, "3"

    #@18a
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@18d
    move-result v0

    #@18e
    if-eqz v0, :cond_c9

    #@190
    .line 763
    const-string v10, "GSM"

    #@192
    goto/16 :goto_c9
.end method

.method public static updateApnDB(Landroid/content/Context;Ljava/lang/String;I)Z
    .registers 24
    .parameter "c"
    .parameter "s"
    .parameter "Set_id"

    #@0
    .prologue
    .line 312
    const-string v2, "phone"

    #@2
    move-object/from16 v0, p0

    #@4
    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@7
    move-result-object v13

    #@8
    check-cast v13, Landroid/telephony/TelephonyManager;

    #@a
    .line 315
    .local v13, mTelephonyManager:Landroid/telephony/TelephonyManager;
    const-string v2, ","

    #@c
    move-object/from16 v0, p1

    #@e
    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@11
    move-result-object v16

    #@12
    .line 317
    .local v16, parm:[Ljava/lang/String;
    const-string v2, "LGDBControl"

    #@14
    new-instance v3, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v4, "parm length = "

    #@1b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v3

    #@1f
    move-object/from16 v0, v16

    #@21
    array-length v4, v0

    #@22
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@25
    move-result-object v3

    #@26
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v3

    #@2a
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 319
    const/4 v9, 0x0

    #@2e
    .local v9, i:I
    :goto_2e
    move-object/from16 v0, v16

    #@30
    array-length v2, v0

    #@31
    if-ge v9, v2, :cond_3d

    #@33
    .line 320
    const-string v2, "LGDBControl"

    #@35
    aget-object v3, v16, v9

    #@37
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 319
    add-int/lit8 v9, v9, 0x1

    #@3c
    goto :goto_2e

    #@3d
    .line 322
    :cond_3d
    const-string v14, "0"

    #@3f
    .line 323
    .local v14, mcc:Ljava/lang/String;
    const-string v15, "0"

    #@41
    .line 326
    .local v15, mnc:Ljava/lang/String;
    sget-object v2, Lcom/android/internal/telephony/LGDBControl;->networkOperator:Ljava/lang/String;

    #@43
    if-eqz v2, :cond_4e

    #@45
    sget-object v2, Lcom/android/internal/telephony/LGDBControl;->networkOperator:Ljava/lang/String;

    #@47
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@4a
    move-result v2

    #@4b
    const/4 v3, 0x5

    #@4c
    if-ge v2, v3, :cond_57

    #@4e
    .line 329
    :cond_4e
    const-string v2, "LGDBControl"

    #@50
    const-string v3, "[updateApnDBupdateApnDB]home oper is bad. error "

    #@52
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@55
    .line 330
    const/4 v2, 0x0

    #@56
    .line 498
    :goto_56
    return v2

    #@57
    .line 335
    :cond_57
    sget-object v2, Lcom/android/internal/telephony/LGDBControl;->networkOperator:Ljava/lang/String;

    #@59
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@5c
    move-result v11

    #@5d
    .line 337
    .local v11, length:I
    const/4 v2, 0x5

    #@5e
    if-ge v11, v2, :cond_7c

    #@60
    .line 340
    const-string v2, "LGDBControl"

    #@62
    new-instance v3, Ljava/lang/StringBuilder;

    #@64
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@67
    const-string v4, "home oper is bad. error "

    #@69
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v3

    #@6d
    sget-object v4, Lcom/android/internal/telephony/LGDBControl;->networkOperator:Ljava/lang/String;

    #@6f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v3

    #@73
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@76
    move-result-object v3

    #@77
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7a
    .line 341
    const/4 v2, 0x0

    #@7b
    goto :goto_56

    #@7c
    .line 346
    :cond_7c
    sget-object v2, Lcom/android/internal/telephony/LGDBControl;->networkOperator:Ljava/lang/String;

    #@7e
    const/4 v3, 0x0

    #@7f
    const/4 v4, 0x3

    #@80
    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@83
    move-result-object v14

    #@84
    .line 347
    sget-object v2, Lcom/android/internal/telephony/LGDBControl;->networkOperator:Ljava/lang/String;

    #@86
    const/4 v3, 0x3

    #@87
    invoke-virtual {v2, v3, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@8a
    move-result-object v15

    #@8b
    .line 349
    const-string v2, "LGDBControl"

    #@8d
    new-instance v3, Ljava/lang/StringBuilder;

    #@8f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@92
    const-string v4, "SIM Info reading Success "

    #@94
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@97
    move-result-object v3

    #@98
    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v3

    #@9c
    const-string v4, ","

    #@9e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v3

    #@a2
    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v3

    #@a6
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a9
    move-result-object v3

    #@aa
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@ad
    .line 355
    new-instance v2, Ljava/lang/StringBuilder;

    #@af
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b2
    const-string v3, "numeric=\""

    #@b4
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b7
    move-result-object v2

    #@b8
    sget-object v3, Lcom/android/internal/telephony/LGDBControl;->networkOperator:Ljava/lang/String;

    #@ba
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v2

    #@be
    const-string v3, "\""

    #@c0
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v2

    #@c4
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c7
    move-result-object v5

    #@c8
    .line 361
    .local v5, where:Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@cb
    move-result-object v2

    #@cc
    sget-object v3, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    #@ce
    sget-object v4, Lcom/android/internal/telephony/LGDBControl;->sProjection:[Ljava/lang/String;

    #@d0
    const/4 v6, 0x0

    #@d1
    const-string v7, "_id"

    #@d3
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@d6
    move-result-object v12

    #@d7
    .line 364
    .local v12, mCursor:Landroid/database/Cursor;
    if-nez v12, :cond_e3

    #@d9
    .line 366
    const-string v2, "LGDBControl"

    #@db
    const-string v3, " Cursor is null"

    #@dd
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@e0
    .line 369
    const/4 v2, 0x0

    #@e1
    goto/16 :goto_56

    #@e3
    .line 372
    :cond_e3
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    #@e6
    .line 373
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    #@e9
    move-result v8

    #@ea
    .line 376
    .local v8, count:I
    move/from16 v0, p2

    #@ec
    if-ge v8, v0, :cond_10e

    #@ee
    .line 378
    const-string v2, "LGDBControl"

    #@f0
    new-instance v3, Ljava/lang/StringBuilder;

    #@f2
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@f5
    const-string v4, "set id is bad id : "

    #@f7
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fa
    move-result-object v3

    #@fb
    move/from16 v0, p2

    #@fd
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@100
    move-result-object v3

    #@101
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@104
    move-result-object v3

    #@105
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@108
    .line 379
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    #@10b
    .line 380
    const/4 v2, 0x0

    #@10c
    goto/16 :goto_56

    #@10e
    .line 383
    :cond_10e
    move/from16 v0, p2

    #@110
    invoke-interface {v12, v0}, Landroid/database/Cursor;->move(I)Z

    #@113
    .line 386
    sget v2, Lcom/android/internal/telephony/LGDBControl;->ID_INDEX:I

    #@115
    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@118
    move-result-object v10

    #@119
    .line 387
    .local v10, key:Ljava/lang/String;
    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@11c
    move-result v17

    #@11d
    .line 388
    .local v17, pos:I
    sget-object v2, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    #@11f
    move/from16 v0, v17

    #@121
    int-to-long v3, v0

    #@122
    invoke-static {v2, v3, v4}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    #@125
    move-result-object v19

    #@126
    .line 390
    .local v19, url:Landroid/net/Uri;
    const-string v2, "LGDBControl"

    #@128
    new-instance v3, Ljava/lang/StringBuilder;

    #@12a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@12d
    const-string v4, "[bycho,updateApnDB]numeric : "

    #@12f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@132
    move-result-object v3

    #@133
    sget-object v4, Lcom/android/internal/telephony/LGDBControl;->networkOperator:Ljava/lang/String;

    #@135
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@138
    move-result-object v3

    #@139
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13c
    move-result-object v3

    #@13d
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@140
    .line 391
    const-string v2, "LGDBControl"

    #@142
    new-instance v3, Ljava/lang/StringBuilder;

    #@144
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@147
    const-string v4, "[bycho,updateApnDB]your pos "

    #@149
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14c
    move-result-object v3

    #@14d
    move/from16 v0, v17

    #@14f
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@152
    move-result-object v3

    #@153
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@156
    move-result-object v3

    #@157
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@15a
    .line 394
    new-instance v20, Landroid/content/ContentValues;

    #@15c
    invoke-direct/range {v20 .. v20}, Landroid/content/ContentValues;-><init>()V

    #@15f
    .line 395
    .local v20, values:Landroid/content/ContentValues;
    const/4 v2, 0x0

    #@160
    aget-object v2, v16, v2

    #@162
    const-string v3, "1"

    #@164
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@167
    move-result v2

    #@168
    if-eqz v2, :cond_28b

    #@16a
    .line 398
    const-string v2, "LGDBControl"

    #@16c
    const-string v3, "[bycho,updateApnDB]parm[0].equals(1)"

    #@16e
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@171
    .line 399
    const/4 v2, 0x1

    #@172
    aget-object v2, v16, v2

    #@174
    const-string v3, "ota"

    #@176
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@179
    move-result v2

    #@17a
    if-eqz v2, :cond_20e

    #@17c
    .line 401
    const-string v2, "type"

    #@17e
    const-string v3, "fota"

    #@180
    move-object/from16 v0, v20

    #@182
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@185
    .line 402
    const-string v2, "LGDBControl"

    #@187
    const-string v3, "Telephony.Carriers.TYPE:: ota ->fota  "

    #@189
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@18c
    .line 420
    :goto_18c
    const-string v2, "apn"

    #@18e
    const/4 v3, 0x2

    #@18f
    aget-object v3, v16, v3

    #@191
    move-object/from16 v0, v20

    #@193
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@196
    .line 424
    const-string v2, "protocol"

    #@198
    const-string v3, "IP"

    #@19a
    move-object/from16 v0, v20

    #@19c
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@19f
    .line 433
    const/4 v2, 0x5

    #@1a0
    aget-object v2, v16, v2

    #@1a2
    const-string v3, "LTE"

    #@1a4
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1a7
    move-result v2

    #@1a8
    if-eqz v2, :cond_254

    #@1aa
    .line 435
    const-string v2, "bearer"

    #@1ac
    const-string v3, "14"

    #@1ae
    move-object/from16 v0, v20

    #@1b0
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@1b3
    .line 453
    :goto_1b3
    const-string v2, "authtype"

    #@1b5
    const/4 v3, 0x6

    #@1b6
    aget-object v3, v16, v3

    #@1b8
    move-object/from16 v0, v20

    #@1ba
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@1bd
    .line 454
    const-string v2, "user"

    #@1bf
    const/4 v3, 0x7

    #@1c0
    aget-object v3, v16, v3

    #@1c2
    move-object/from16 v0, v20

    #@1c4
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@1c7
    .line 455
    const-string v2, "password"

    #@1c9
    const/16 v3, 0x8

    #@1cb
    aget-object v3, v16, v3

    #@1cd
    move-object/from16 v0, v20

    #@1cf
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@1d2
    .line 456
    const-string v2, "name"

    #@1d4
    const/4 v3, 0x1

    #@1d5
    aget-object v3, v16, v3

    #@1d7
    move-object/from16 v0, v20

    #@1d9
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@1dc
    .line 457
    const-string v2, "mcc"

    #@1de
    move-object/from16 v0, v20

    #@1e0
    invoke-virtual {v0, v2, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@1e3
    .line 458
    const-string v2, "mnc"

    #@1e5
    move-object/from16 v0, v20

    #@1e7
    invoke-virtual {v0, v2, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@1ea
    .line 459
    const-string v2, "numeric"

    #@1ec
    sget-object v3, Lcom/android/internal/telephony/LGDBControl;->networkOperator:Ljava/lang/String;

    #@1ee
    move-object/from16 v0, v20

    #@1f0
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@1f3
    .line 491
    :goto_1f3
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1f6
    move-result-object v2

    #@1f7
    const/4 v3, 0x0

    #@1f8
    const/4 v4, 0x0

    #@1f9
    move-object/from16 v0, v19

    #@1fb
    move-object/from16 v1, v20

    #@1fd
    invoke-virtual {v2, v0, v1, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@200
    move-result v18

    #@201
    .line 494
    .local v18, result:I
    const-string v2, "LGDBControl"

    #@203
    const-string v3, "updata success : "

    #@205
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@208
    .line 496
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    #@20b
    .line 498
    const/4 v2, 0x1

    #@20c
    goto/16 :goto_56

    #@20e
    .line 404
    .end local v18           #result:I
    :cond_20e
    const/4 v2, 0x1

    #@20f
    aget-object v2, v16, v2

    #@211
    const-string v3, "internet"

    #@213
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@216
    move-result v2

    #@217
    if-eqz v2, :cond_22b

    #@219
    .line 406
    const-string v2, "type"

    #@21b
    const-string v3, "default"

    #@21d
    move-object/from16 v0, v20

    #@21f
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@222
    .line 407
    const-string v2, "LGDBControl"

    #@224
    const-string v3, "Telephony.Carriers.TYPE:: internet ->default  "

    #@226
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@229
    goto/16 :goto_18c

    #@22b
    .line 409
    :cond_22b
    const/4 v2, 0x1

    #@22c
    aget-object v2, v16, v2

    #@22e
    const-string v3, "pam"

    #@230
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@233
    move-result v2

    #@234
    if-eqz v2, :cond_248

    #@236
    .line 411
    const-string v2, "type"

    #@238
    const-string v3, "dun"

    #@23a
    move-object/from16 v0, v20

    #@23c
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@23f
    .line 412
    const-string v2, "LGDBControl"

    #@241
    const-string v3, "Telephony.Carriers.TYPE:: pam ->dun"

    #@243
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@246
    goto/16 :goto_18c

    #@248
    .line 416
    :cond_248
    const-string v2, "type"

    #@24a
    const/4 v3, 0x1

    #@24b
    aget-object v3, v16, v3

    #@24d
    move-object/from16 v0, v20

    #@24f
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@252
    goto/16 :goto_18c

    #@254
    .line 437
    :cond_254
    const/4 v2, 0x5

    #@255
    aget-object v2, v16, v2

    #@257
    const-string v3, "EHRPD"

    #@259
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@25c
    move-result v2

    #@25d
    if-eqz v2, :cond_26a

    #@25f
    .line 439
    const-string v2, "bearer"

    #@261
    const-string v3, "13"

    #@263
    move-object/from16 v0, v20

    #@265
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@268
    goto/16 :goto_1b3

    #@26a
    .line 442
    :cond_26a
    const/4 v2, 0x5

    #@26b
    aget-object v2, v16, v2

    #@26d
    const-string v3, "GSM"

    #@26f
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@272
    move-result v2

    #@273
    if-eqz v2, :cond_280

    #@275
    .line 444
    const-string v2, "bearer"

    #@277
    const-string v3, "3"

    #@279
    move-object/from16 v0, v20

    #@27b
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@27e
    goto/16 :goto_1b3

    #@280
    .line 449
    :cond_280
    const-string v2, "bearer"

    #@282
    const-string v3, "0"

    #@284
    move-object/from16 v0, v20

    #@286
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@289
    goto/16 :goto_1b3

    #@28b
    .line 462
    :cond_28b
    const/4 v2, 0x0

    #@28c
    aget-object v2, v16, v2

    #@28e
    const-string v3, "0"

    #@290
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@293
    move-result v2

    #@294
    if-eqz v2, :cond_305

    #@296
    .line 464
    const-string v2, "LGDBControl"

    #@298
    const-string v3, "[bycho,updateApnDB]parm[0].equals(0)"

    #@29a
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@29d
    .line 466
    const-string v2, "type"

    #@29f
    const-string v3, "Dummy"

    #@2a1
    move-object/from16 v0, v20

    #@2a3
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@2a6
    .line 467
    const-string v2, "apn"

    #@2a8
    const-string v3, "dummy"

    #@2aa
    move-object/from16 v0, v20

    #@2ac
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@2af
    .line 468
    const-string v2, "protocol"

    #@2b1
    const-string v3, "IP"

    #@2b3
    move-object/from16 v0, v20

    #@2b5
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@2b8
    .line 470
    const-string v2, "bearer"

    #@2ba
    const-string v3, "0"

    #@2bc
    move-object/from16 v0, v20

    #@2be
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@2c1
    .line 473
    const-string v2, "authtype"

    #@2c3
    const-string v3, "1"

    #@2c5
    move-object/from16 v0, v20

    #@2c7
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@2ca
    .line 474
    const-string v2, "user"

    #@2cc
    const-string v3, "ncc"

    #@2ce
    move-object/from16 v0, v20

    #@2d0
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@2d3
    .line 475
    const-string v2, "password"

    #@2d5
    const-string v3, "ncc"

    #@2d7
    move-object/from16 v0, v20

    #@2d9
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@2dc
    .line 476
    const-string v2, "name"

    #@2de
    const-string v3, "none"

    #@2e0
    move-object/from16 v0, v20

    #@2e2
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@2e5
    .line 477
    const-string v2, "mcc"

    #@2e7
    move-object/from16 v0, v20

    #@2e9
    invoke-virtual {v0, v2, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@2ec
    .line 478
    const-string v2, "mnc"

    #@2ee
    move-object/from16 v0, v20

    #@2f0
    invoke-virtual {v0, v2, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@2f3
    .line 479
    const-string v2, "numeric"

    #@2f5
    sget-object v3, Lcom/android/internal/telephony/LGDBControl;->networkOperator:Ljava/lang/String;

    #@2f7
    move-object/from16 v0, v20

    #@2f9
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@2fc
    .line 481
    const-string v2, "LGDBControl"

    #@2fe
    const-string v3, "delete success "

    #@300
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@303
    goto/16 :goto_1f3

    #@305
    .line 485
    :cond_305
    const-string v2, "LGDBControl"

    #@307
    const-string v3, "[bycho,updateApnDB]parm[0] else"

    #@309
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@30c
    .line 486
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    #@30f
    .line 487
    const/4 v2, 0x0

    #@310
    goto/16 :goto_56
.end method


# virtual methods
.method public backupAPN(Landroid/content/Context;Ljava/lang/String;)V
    .registers 11
    .parameter "context"
    .parameter "apnBackup"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 509
    if-nez p2, :cond_28

    #@2
    .line 511
    :try_start_2
    new-instance v4, Ljava/io/BufferedWriter;

    #@4
    new-instance v5, Ljava/io/FileWriter;

    #@6
    new-instance v6, Ljava/lang/String;

    #@8
    const-string v7, "/persist-lg/LGE_RC/apn_backup"

    #@a
    invoke-direct {v6, v7}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    #@d
    invoke-direct {v5, v6}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V

    #@10
    invoke-direct {v4, v5}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    #@13
    .line 512
    .local v4, outApnFile:Ljava/io/BufferedWriter;
    const-string v1, ""

    #@15
    .line 514
    .local v1, buffer:Ljava/lang/String;
    const/4 v3, 0x0

    #@16
    .local v3, i:I
    :goto_16
    const/16 v5, 0x9

    #@18
    if-ge v3, v5, :cond_4f

    #@1a
    .line 517
    const/4 v5, 0x0

    #@1b
    invoke-virtual {p0, v3, v5}, Lcom/android/internal/telephony/LGDBControl;->getAPNString(IZ)Ljava/lang/String;

    #@1e
    move-result-object v0

    #@1f
    .line 520
    .local v0, APNValue:Ljava/lang/String;
    if-nez v0, :cond_29

    #@21
    .line 522
    const-string v5, "APN Backup"

    #@23
    const-string v6, "APNValue is NULL"

    #@25
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 544
    .end local v0           #APNValue:Ljava/lang/String;
    .end local v1           #buffer:Ljava/lang/String;
    .end local v3           #i:I
    .end local v4           #outApnFile:Ljava/io/BufferedWriter;
    :cond_28
    :goto_28
    return-void

    #@29
    .line 525
    .restart local v0       #APNValue:Ljava/lang/String;
    .restart local v1       #buffer:Ljava/lang/String;
    .restart local v3       #i:I
    .restart local v4       #outApnFile:Ljava/io/BufferedWriter;
    :cond_29
    const-string v5, "0"

    #@2b
    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2e
    move-result v5

    #@2f
    if-eqz v5, :cond_8b

    #@31
    .line 527
    const-string v5, "APN Backup"

    #@33
    new-instance v6, Ljava/lang/StringBuilder;

    #@35
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    const-string v7, " Backup APN table : "

    #@3a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v6

    #@3e
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@41
    move-result-object v6

    #@42
    const-string v7, " None"

    #@44
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v6

    #@48
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v6

    #@4c
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    .line 532
    .end local v0           #APNValue:Ljava/lang/String;
    :cond_4f
    invoke-virtual {v4, v1}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    #@52
    .line 533
    invoke-virtual {v4}, Ljava/io/BufferedWriter;->flush()V

    #@55
    .line 534
    invoke-virtual {v4}, Ljava/io/BufferedWriter;->close()V

    #@58
    .line 535
    const-string v5, "APN Backup"

    #@5a
    new-instance v6, Ljava/lang/StringBuilder;

    #@5c
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@5f
    const-string v7, " Backup APN table! "

    #@61
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v6

    #@65
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v6

    #@69
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6c
    move-result-object v6

    #@6d
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_70
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_70} :catch_71

    #@70
    goto :goto_28

    #@71
    .line 539
    .end local v1           #buffer:Ljava/lang/String;
    .end local v3           #i:I
    .end local v4           #outApnFile:Ljava/io/BufferedWriter;
    :catch_71
    move-exception v2

    #@72
    .line 540
    .local v2, e:Ljava/lang/Exception;
    const-string v5, "APN Backup"

    #@74
    new-instance v6, Ljava/lang/StringBuilder;

    #@76
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@79
    const-string v7, "Backup APN table"

    #@7b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v6

    #@7f
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v6

    #@83
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@86
    move-result-object v6

    #@87
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@8a
    goto :goto_28

    #@8b
    .line 530
    .end local v2           #e:Ljava/lang/Exception;
    .restart local v0       #APNValue:Ljava/lang/String;
    .restart local v1       #buffer:Ljava/lang/String;
    .restart local v3       #i:I
    .restart local v4       #outApnFile:Ljava/io/BufferedWriter;
    :cond_8b
    :try_start_8b
    new-instance v5, Ljava/lang/StringBuilder;

    #@8d
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@90
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@93
    move-result-object v5

    #@94
    const-string v6, " 1,"

    #@96
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@99
    move-result-object v5

    #@9a
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v5

    #@9e
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_a1
    .catch Ljava/lang/Exception; {:try_start_8b .. :try_end_a1} :catch_71

    #@a1
    move-result-object v1

    #@a2
    .line 514
    add-int/lit8 v3, v3, 0x1

    #@a4
    goto/16 :goto_16
.end method

.method public backupAPN(Ljava/lang/String;)V
    .registers 11
    .parameter "filename"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 546
    if-eqz p1, :cond_34

    #@2
    .line 549
    :try_start_2
    const-string v4, "/persist-lg/LGE_RC/"

    #@4
    .line 550
    .local v4, mFilePath:Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v6

    #@d
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v6

    #@11
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v4

    #@15
    .line 551
    new-instance v5, Ljava/io/BufferedWriter;

    #@17
    new-instance v6, Ljava/io/FileWriter;

    #@19
    invoke-direct {v6, v4}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V

    #@1c
    invoke-direct {v5, v6}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    #@1f
    .line 552
    .local v5, outApnFile:Ljava/io/BufferedWriter;
    const-string v1, ""

    #@21
    .line 554
    .local v1, buffer:Ljava/lang/String;
    const/4 v3, 0x0

    #@22
    .local v3, i:I
    :goto_22
    const/16 v6, 0x9

    #@24
    if-ge v3, v6, :cond_5b

    #@26
    .line 557
    const/4 v6, 0x0

    #@27
    invoke-virtual {p0, v3, v6}, Lcom/android/internal/telephony/LGDBControl;->getAPNString(IZ)Ljava/lang/String;

    #@2a
    move-result-object v0

    #@2b
    .line 560
    .local v0, APNValue:Ljava/lang/String;
    if-nez v0, :cond_35

    #@2d
    .line 562
    const-string v6, "APN Backup"

    #@2f
    const-string v7, "APNValue is NULL"

    #@31
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    .line 582
    .end local v0           #APNValue:Ljava/lang/String;
    .end local v1           #buffer:Ljava/lang/String;
    .end local v3           #i:I
    .end local v4           #mFilePath:Ljava/lang/String;
    .end local v5           #outApnFile:Ljava/io/BufferedWriter;
    :cond_34
    :goto_34
    return-void

    #@35
    .line 565
    .restart local v0       #APNValue:Ljava/lang/String;
    .restart local v1       #buffer:Ljava/lang/String;
    .restart local v3       #i:I
    .restart local v4       #mFilePath:Ljava/lang/String;
    .restart local v5       #outApnFile:Ljava/io/BufferedWriter;
    :cond_35
    const-string v6, "0"

    #@37
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3a
    move-result v6

    #@3b
    if-eqz v6, :cond_9b

    #@3d
    .line 567
    const-string v6, "APN Backup"

    #@3f
    new-instance v7, Ljava/lang/StringBuilder;

    #@41
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@44
    const-string v8, " Backup APN table : "

    #@46
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v7

    #@4a
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v7

    #@4e
    const-string v8, " None"

    #@50
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v7

    #@54
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@57
    move-result-object v7

    #@58
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5b
    .line 572
    .end local v0           #APNValue:Ljava/lang/String;
    :cond_5b
    invoke-virtual {v5, v1}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    #@5e
    .line 573
    invoke-virtual {v5}, Ljava/io/BufferedWriter;->flush()V

    #@61
    .line 574
    invoke-virtual {v5}, Ljava/io/BufferedWriter;->close()V

    #@64
    .line 575
    const-string v6, "APN Backup"

    #@66
    new-instance v7, Ljava/lang/StringBuilder;

    #@68
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@6b
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v7

    #@6f
    const-string v8, " Backup APN table! "

    #@71
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v7

    #@75
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v7

    #@79
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7c
    move-result-object v7

    #@7d
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_80
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_80} :catch_81

    #@80
    goto :goto_34

    #@81
    .line 578
    .end local v1           #buffer:Ljava/lang/String;
    .end local v3           #i:I
    .end local v4           #mFilePath:Ljava/lang/String;
    .end local v5           #outApnFile:Ljava/io/BufferedWriter;
    :catch_81
    move-exception v2

    #@82
    .line 579
    .local v2, e:Ljava/lang/Exception;
    const-string v6, "APN Backup"

    #@84
    new-instance v7, Ljava/lang/StringBuilder;

    #@86
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@89
    const-string v8, "Backup APN table"

    #@8b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v7

    #@8f
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@92
    move-result-object v7

    #@93
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@96
    move-result-object v7

    #@97
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@9a
    goto :goto_34

    #@9b
    .line 570
    .end local v2           #e:Ljava/lang/Exception;
    .restart local v0       #APNValue:Ljava/lang/String;
    .restart local v1       #buffer:Ljava/lang/String;
    .restart local v3       #i:I
    .restart local v4       #mFilePath:Ljava/lang/String;
    .restart local v5       #outApnFile:Ljava/io/BufferedWriter;
    :cond_9b
    :try_start_9b
    new-instance v6, Ljava/lang/StringBuilder;

    #@9d
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@a0
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v6

    #@a4
    const-string v7, " 1,"

    #@a6
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v6

    #@aa
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v6

    #@ae
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_b1
    .catch Ljava/lang/Exception; {:try_start_9b .. :try_end_b1} :catch_81

    #@b1
    move-result-object v1

    #@b2
    .line 554
    add-int/lit8 v3, v3, 0x1

    #@b4
    goto/16 :goto_22
.end method

.method public checkId(I)Z
    .registers 11
    .parameter "Set_id"

    #@0
    .prologue
    .line 200
    const-string v0, "LGDBControl"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "checkId networkOperator :: "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    sget-object v2, Lcom/android/internal/telephony/LGDBControl;->networkOperator:Ljava/lang/String;

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 201
    new-instance v0, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v1, "numeric=\""

    #@21
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v0

    #@25
    sget-object v1, Lcom/android/internal/telephony/LGDBControl;->networkOperator:Ljava/lang/String;

    #@27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v0

    #@2b
    const-string v1, "\""

    #@2d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v0

    #@31
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v3

    #@35
    .line 208
    .local v3, where:Ljava/lang/String;
    iget-object v0, p0, Lcom/android/internal/telephony/LGDBControl;->mContext:Landroid/content/Context;

    #@37
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@3a
    move-result-object v0

    #@3b
    sget-object v1, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    #@3d
    sget-object v2, Lcom/android/internal/telephony/LGDBControl;->sProjection:[Ljava/lang/String;

    #@3f
    const/4 v4, 0x0

    #@40
    const-string v5, "_id"

    #@42
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@45
    move-result-object v8

    #@46
    .line 211
    .local v8, mCursor:Landroid/database/Cursor;
    if-nez v8, :cond_51

    #@48
    .line 213
    const-string v0, "LGDBControl"

    #@4a
    const-string v1, " Cursor is null"

    #@4c
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    .line 216
    const/4 v0, 0x0

    #@50
    .line 234
    :goto_50
    return v0

    #@51
    .line 219
    :cond_51
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    #@54
    .line 220
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    #@57
    move-result v6

    #@58
    .line 224
    .local v6, count:I
    if-gt v6, p1, :cond_7d

    #@5a
    .line 226
    const-string v0, "LGDBControl"

    #@5c
    new-instance v1, Ljava/lang/StringBuilder;

    #@5e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@61
    const-string v2, "setting num is bigger than real num so make dummy  mCursor.count :: "

    #@63
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v1

    #@67
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v1

    #@6b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6e
    move-result-object v1

    #@6f
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@72
    .line 227
    const/4 v7, 0x0

    #@73
    .local v7, i:I
    :goto_73
    sub-int v0, p1, v6

    #@75
    if-gt v7, v0, :cond_7d

    #@77
    .line 229
    invoke-virtual {p0}, Lcom/android/internal/telephony/LGDBControl;->makedummy()Z

    #@7a
    .line 227
    add-int/lit8 v7, v7, 0x1

    #@7c
    goto :goto_73

    #@7d
    .line 233
    .end local v7           #i:I
    :cond_7d
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@80
    .line 234
    const/4 v0, 0x1

    #@81
    goto :goto_50
.end method

.method public getAPNString(IZ)Ljava/lang/String;
    .registers 6
    .parameter "id"
    .parameter "dummy"

    #@0
    .prologue
    .line 170
    const-string v0, "LGDBControl"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "!!! get APN Called "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 172
    iget-object v0, p0, Lcom/android/internal/telephony/LGDBControl;->mContext:Landroid/content/Context;

    #@1a
    invoke-static {v0, p1, p2}, Lcom/android/internal/telephony/LGDBControl;->getSpLTEApnFromDb(Landroid/content/Context;IZ)Ljava/lang/String;

    #@1d
    move-result-object v0

    #@1e
    return-object v0
.end method

.method public makedummy()Z
    .registers 13

    #@0
    .prologue
    const/4 v11, 0x5

    #@1
    const/4 v10, 0x3

    #@2
    const/4 v7, 0x0

    #@3
    .line 243
    iget-object v8, p0, Lcom/android/internal/telephony/LGDBControl;->mContext:Landroid/content/Context;

    #@5
    const-string v9, "phone"

    #@7
    invoke-virtual {v8, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@a
    move-result-object v2

    #@b
    check-cast v2, Landroid/telephony/TelephonyManager;

    #@d
    .line 247
    .local v2, mTelephonyManager:Landroid/telephony/TelephonyManager;
    const-string v3, "0"

    #@f
    .line 248
    .local v3, mcc:Ljava/lang/String;
    const-string v4, "0"

    #@11
    .line 250
    .local v4, mnc:Ljava/lang/String;
    sget-object v8, Lcom/android/internal/telephony/LGDBControl;->networkOperator:Ljava/lang/String;

    #@13
    if-eqz v8, :cond_1d

    #@15
    sget-object v8, Lcom/android/internal/telephony/LGDBControl;->networkOperator:Ljava/lang/String;

    #@17
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    #@1a
    move-result v8

    #@1b
    if-ge v8, v11, :cond_25

    #@1d
    .line 253
    :cond_1d
    const-string v8, "LGDBControl"

    #@1f
    const-string v9, "[makedummy]home oper is bad. error "

    #@21
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 303
    :goto_24
    return v7

    #@25
    .line 259
    :cond_25
    sget-object v8, Lcom/android/internal/telephony/LGDBControl;->networkOperator:Ljava/lang/String;

    #@27
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    #@2a
    move-result v1

    #@2b
    .line 261
    .local v1, length:I
    if-ge v1, v11, :cond_35

    #@2d
    .line 264
    const-string v8, "LGDBControl"

    #@2f
    const-string v9, "[makedummy]home oper is bad. error, length<5 "

    #@31
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    goto :goto_24

    #@35
    .line 269
    :cond_35
    sget-object v8, Lcom/android/internal/telephony/LGDBControl;->networkOperator:Ljava/lang/String;

    #@37
    invoke-virtual {v8, v7, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@3a
    move-result-object v3

    #@3b
    .line 270
    sget-object v8, Lcom/android/internal/telephony/LGDBControl;->networkOperator:Ljava/lang/String;

    #@3d
    invoke-virtual {v8, v10, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@40
    move-result-object v4

    #@41
    .line 272
    new-instance v6, Landroid/content/ContentValues;

    #@43
    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    #@46
    .line 275
    .local v6, values:Landroid/content/ContentValues;
    const-string v8, "type"

    #@48
    const-string v9, "Dummy"

    #@4a
    invoke-virtual {v6, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@4d
    .line 276
    const-string v8, "apn"

    #@4f
    const-string v9, "dummy"

    #@51
    invoke-virtual {v6, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@54
    .line 277
    const-string v8, "protocol"

    #@56
    const-string v9, "IP"

    #@58
    invoke-virtual {v6, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@5b
    .line 279
    const-string v8, "bearer"

    #@5d
    const-string v9, "0"

    #@5f
    invoke-virtual {v6, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@62
    .line 282
    const-string v8, "authtype"

    #@64
    const-string v9, "0"

    #@66
    invoke-virtual {v6, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@69
    .line 283
    const-string v8, "user"

    #@6b
    const-string v9, "ncc"

    #@6d
    invoke-virtual {v6, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@70
    .line 284
    const-string v8, "password"

    #@72
    const-string v9, "ncc"

    #@74
    invoke-virtual {v6, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@77
    .line 285
    const-string v8, "name"

    #@79
    const-string v9, "none"

    #@7b
    invoke-virtual {v6, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@7e
    .line 286
    const-string v8, "mcc"

    #@80
    invoke-virtual {v6, v8, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@83
    .line 287
    const-string v8, "mnc"

    #@85
    invoke-virtual {v6, v8, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@88
    .line 288
    const-string v8, "numeric"

    #@8a
    sget-object v9, Lcom/android/internal/telephony/LGDBControl;->networkOperator:Ljava/lang/String;

    #@8c
    invoke-virtual {v6, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@8f
    .line 291
    iget-object v8, p0, Lcom/android/internal/telephony/LGDBControl;->mContext:Landroid/content/Context;

    #@91
    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@94
    move-result-object v0

    #@95
    .line 293
    .local v0, cr:Landroid/content/ContentResolver;
    sget-object v8, Lcom/android/internal/telephony/LGDBControl;->mUri:Landroid/net/Uri;

    #@97
    invoke-virtual {v0, v8, v6}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@9a
    move-result-object v5

    #@9b
    .line 295
    .local v5, result:Landroid/net/Uri;
    if-nez v5, :cond_a5

    #@9d
    .line 297
    const-string v8, "LGDBControl"

    #@9f
    const-string v9, "make dummy fail"

    #@a1
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@a4
    goto :goto_24

    #@a5
    .line 302
    :cond_a5
    const-string v7, "LGDBControl"

    #@a7
    const-string v8, "make dummy success"

    #@a9
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@ac
    .line 303
    const/4 v7, 0x1

    #@ad
    goto/16 :goto_24
.end method

.method public restoreAPNs()V
    .registers 10

    #@0
    .prologue
    .line 588
    const/4 v6, 0x0

    #@1
    new-array v5, v6, [Ljava/lang/String;

    #@3
    .line 591
    .local v5, intiDBs:[Ljava/lang/String;
    :try_start_3
    new-instance v4, Ljava/io/BufferedReader;

    #@5
    new-instance v6, Ljava/io/FileReader;

    #@7
    const-string v7, "/persist-lg/LGE_RC/apn_backup"

    #@9
    invoke-direct {v6, v7}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    #@c
    invoke-direct {v4, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    #@f
    .line 594
    .local v4, inApnFile:Ljava/io/BufferedReader;
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    .line 595
    .local v0, buffer:Ljava/lang/String;
    const-string v6, "LGDBControl"

    #@15
    const-string v7, "!!!!![restoreAPNs]LGFactoryReset"

    #@17
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 597
    if-eqz v0, :cond_8a

    #@1c
    .line 598
    const-string v6, "LGDBControl"

    #@1e
    new-instance v7, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v8, "!!!!![restoreAPNs]LGFactoryReset buffer :"

    #@25
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v7

    #@29
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v7

    #@2d
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v7

    #@31
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    .line 599
    const-string v6, " "

    #@36
    invoke-virtual {v0, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@39
    move-result-object v5

    #@3a
    .line 600
    const-string v6, "LGDBControl"

    #@3c
    new-instance v7, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v8, "!!!!!LGFactoryReset intiDBs.length : "

    #@43
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v7

    #@47
    array-length v8, v5

    #@48
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v7

    #@4c
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4f
    move-result-object v7

    #@50
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@53
    .line 602
    array-length v6, v5

    #@54
    if-lez v6, :cond_79

    #@56
    .line 605
    const/4 v3, 0x1

    #@57
    .local v3, i:I
    :goto_57
    array-length v6, v5

    #@58
    if-ge v3, v6, :cond_64

    #@5a
    .line 607
    add-int/lit8 v6, v3, -0x1

    #@5c
    aget-object v7, v5, v3

    #@5e
    invoke-virtual {p0, v6, v7}, Lcom/android/internal/telephony/LGDBControl;->setAPNString(ILjava/lang/String;)Z

    #@61
    .line 605
    add-int/lit8 v3, v3, 0x1

    #@63
    goto :goto_57

    #@64
    .line 609
    :cond_64
    const-string v6, "LGDBControl"

    #@66
    const-string v7, "!!!!!LGFactoryReset: Restore APN table!"

    #@68
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@6b
    .line 620
    .end local v3           #i:I
    :goto_6b
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V

    #@6e
    .line 622
    new-instance v2, Ljava/io/File;

    #@70
    const-string v6, "/persist-lg/LGE_RC/apn_backup"

    #@72
    invoke-direct {v2, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@75
    .line 624
    .local v2, f:Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    #@78
    .line 629
    .end local v0           #buffer:Ljava/lang/String;
    .end local v2           #f:Ljava/io/File;
    .end local v4           #inApnFile:Ljava/io/BufferedReader;
    :goto_78
    return-void

    #@79
    .line 613
    .restart local v0       #buffer:Ljava/lang/String;
    .restart local v4       #inApnFile:Ljava/io/BufferedReader;
    :cond_79
    const-string v6, "LGDBControl"

    #@7b
    const-string v7, "!!!!!!No Backup APNs apn num 0 "

    #@7d
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_80
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_80} :catch_81

    #@80
    goto :goto_6b

    #@81
    .line 626
    .end local v0           #buffer:Ljava/lang/String;
    .end local v4           #inApnFile:Ljava/io/BufferedReader;
    :catch_81
    move-exception v1

    #@82
    .line 627
    .local v1, e:Ljava/lang/Exception;
    const-string v6, "LGDBControl"

    #@84
    const-string v7, "!!!!!!No Backup APNs: Do not need to apn backup"

    #@86
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@89
    goto :goto_78

    #@8a
    .line 616
    .end local v1           #e:Ljava/lang/Exception;
    .restart local v0       #buffer:Ljava/lang/String;
    .restart local v4       #inApnFile:Ljava/io/BufferedReader;
    :cond_8a
    :try_start_8a
    const-string v6, "LGDBControl"

    #@8c
    const-string v7, "buffer is NULL"

    #@8e
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_91
    .catch Ljava/lang/Exception; {:try_start_8a .. :try_end_91} :catch_81

    #@91
    goto :goto_6b
.end method

.method public restoreAPNs(Ljava/lang/String;)V
    .registers 11
    .parameter "filename"

    #@0
    .prologue
    .line 634
    const/4 v6, 0x0

    #@1
    new-array v4, v6, [Ljava/lang/String;

    #@3
    .line 635
    .local v4, intiDBs:[Ljava/lang/String;
    const-string v5, "/persist-lg/LGE_RC/"

    #@5
    .line 636
    .local v5, mFilePath:Ljava/lang/String;
    if-eqz p1, :cond_85

    #@7
    .line 639
    :try_start_7
    new-instance v6, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v6

    #@10
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v6

    #@14
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v5

    #@18
    .line 640
    new-instance v3, Ljava/io/BufferedReader;

    #@1a
    new-instance v6, Ljava/io/FileReader;

    #@1c
    invoke-direct {v6, v5}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    #@1f
    invoke-direct {v3, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    #@22
    .line 643
    .local v3, inApnFile:Ljava/io/BufferedReader;
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    #@25
    move-result-object v0

    #@26
    .line 644
    .local v0, buffer:Ljava/lang/String;
    const-string v6, "LGDBControl"

    #@28
    const-string v7, "!!!!![restoreAPNs]LGFactoryReset"

    #@2a
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 646
    if-eqz v0, :cond_97

    #@2f
    .line 647
    const-string v6, "LGDBControl"

    #@31
    new-instance v7, Ljava/lang/StringBuilder;

    #@33
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@36
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v7

    #@3a
    const-string v8, " !!!!![restoreAPNs]LGFactoryReset buffer :"

    #@3c
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v7

    #@40
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v7

    #@44
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v7

    #@48
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4b
    .line 648
    const-string v6, " "

    #@4d
    invoke-virtual {v0, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@50
    move-result-object v4

    #@51
    .line 649
    const-string v6, "LGDBControl"

    #@53
    new-instance v7, Ljava/lang/StringBuilder;

    #@55
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@58
    const-string v8, "!!!!!LGFactoryReset intiDBs.length : "

    #@5a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v7

    #@5e
    array-length v8, v4

    #@5f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@62
    move-result-object v7

    #@63
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@66
    move-result-object v7

    #@67
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@6a
    .line 651
    array-length v6, v4

    #@6b
    if-lez v6, :cond_86

    #@6d
    .line 653
    const/4 v2, 0x1

    #@6e
    .local v2, i:I
    :goto_6e
    array-length v6, v4

    #@6f
    if-ge v2, v6, :cond_7b

    #@71
    .line 655
    add-int/lit8 v6, v2, -0x1

    #@73
    aget-object v7, v4, v2

    #@75
    invoke-virtual {p0, v6, v7}, Lcom/android/internal/telephony/LGDBControl;->setAPNString(ILjava/lang/String;)Z

    #@78
    .line 653
    add-int/lit8 v2, v2, 0x1

    #@7a
    goto :goto_6e

    #@7b
    .line 657
    :cond_7b
    const-string v6, "LGDBControl"

    #@7d
    const-string v7, "!!!!!LGFactoryReset: Restore APN table!"

    #@7f
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@82
    .line 668
    .end local v2           #i:I
    :goto_82
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V

    #@85
    .line 674
    .end local v0           #buffer:Ljava/lang/String;
    .end local v3           #inApnFile:Ljava/io/BufferedReader;
    :cond_85
    :goto_85
    return-void

    #@86
    .line 661
    .restart local v0       #buffer:Ljava/lang/String;
    .restart local v3       #inApnFile:Ljava/io/BufferedReader;
    :cond_86
    const-string v6, "LGDBControl"

    #@88
    const-string v7, "!!!!!!No Backup APNs apn num 0 "

    #@8a
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8d
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_8d} :catch_8e

    #@8d
    goto :goto_82

    #@8e
    .line 670
    .end local v0           #buffer:Ljava/lang/String;
    .end local v3           #inApnFile:Ljava/io/BufferedReader;
    :catch_8e
    move-exception v1

    #@8f
    .line 671
    .local v1, e:Ljava/lang/Exception;
    const-string v6, "LGDBControl"

    #@91
    const-string v7, "!!!!!!No Backup APNs: Do not need to apn backup"

    #@93
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@96
    goto :goto_85

    #@97
    .line 665
    .end local v1           #e:Ljava/lang/Exception;
    .restart local v0       #buffer:Ljava/lang/String;
    .restart local v3       #inApnFile:Ljava/io/BufferedReader;
    :cond_97
    :try_start_97
    const-string v6, "LGDBControl"

    #@99
    const-string v7, "buffer is NULL"

    #@9b
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9e
    .catch Ljava/lang/Exception; {:try_start_97 .. :try_end_9e} :catch_8e

    #@9e
    goto :goto_82
.end method

.method public setAPNString(ILjava/lang/String;)Z
    .registers 6
    .parameter "id"
    .parameter "s"

    #@0
    .prologue
    .line 180
    const-string v0, "LGDBControl"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "!!! set APN Called "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 181
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/LGDBControl;->checkId(I)Z

    #@1b
    move-result v0

    #@1c
    if-nez v0, :cond_3e

    #@1e
    .line 183
    const-string v0, "LGDBControl"

    #@20
    new-instance v1, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v2, "!!! set APN Called "

    #@27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v1

    #@2b
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v1

    #@2f
    const-string v2, "return false "

    #@31
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v1

    #@35
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v1

    #@39
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3c
    .line 184
    const/4 v0, 0x0

    #@3d
    .line 187
    :goto_3d
    return v0

    #@3e
    :cond_3e
    iget-object v0, p0, Lcom/android/internal/telephony/LGDBControl;->mContext:Landroid/content/Context;

    #@40
    invoke-static {v0, p2, p1}, Lcom/android/internal/telephony/LGDBControl;->updateApnDB(Landroid/content/Context;Ljava/lang/String;I)Z

    #@43
    move-result v0

    #@44
    goto :goto_3d
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 796
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 797
    .local v0, sb:Ljava/lang/StringBuilder;
    const-string v1, "LGDBControl"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    .line 798
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d
    move-result-object v1

    #@e
    return-object v1
.end method
