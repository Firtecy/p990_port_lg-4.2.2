.class final enum Lcom/android/internal/telephony/test/CallInfo$State;
.super Ljava/lang/Enum;
.source "SimulatedGsmCallState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/test/CallInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/internal/telephony/test/CallInfo$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/internal/telephony/test/CallInfo$State;

.field public static final enum ACTIVE:Lcom/android/internal/telephony/test/CallInfo$State;

.field public static final enum ALERTING:Lcom/android/internal/telephony/test/CallInfo$State;

.field public static final enum DIALING:Lcom/android/internal/telephony/test/CallInfo$State;

.field public static final enum HOLDING:Lcom/android/internal/telephony/test/CallInfo$State;

.field public static final enum INCOMING:Lcom/android/internal/telephony/test/CallInfo$State;

.field public static final enum WAITING:Lcom/android/internal/telephony/test/CallInfo$State;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .registers 9

    #@0
    .prologue
    const/4 v8, 0x4

    #@1
    const/4 v7, 0x3

    #@2
    const/4 v6, 0x2

    #@3
    const/4 v5, 0x1

    #@4
    const/4 v4, 0x0

    #@5
    .line 32
    new-instance v0, Lcom/android/internal/telephony/test/CallInfo$State;

    #@7
    const-string v1, "ACTIVE"

    #@9
    invoke-direct {v0, v1, v4, v4}, Lcom/android/internal/telephony/test/CallInfo$State;-><init>(Ljava/lang/String;II)V

    #@c
    sput-object v0, Lcom/android/internal/telephony/test/CallInfo$State;->ACTIVE:Lcom/android/internal/telephony/test/CallInfo$State;

    #@e
    .line 33
    new-instance v0, Lcom/android/internal/telephony/test/CallInfo$State;

    #@10
    const-string v1, "HOLDING"

    #@12
    invoke-direct {v0, v1, v5, v5}, Lcom/android/internal/telephony/test/CallInfo$State;-><init>(Ljava/lang/String;II)V

    #@15
    sput-object v0, Lcom/android/internal/telephony/test/CallInfo$State;->HOLDING:Lcom/android/internal/telephony/test/CallInfo$State;

    #@17
    .line 34
    new-instance v0, Lcom/android/internal/telephony/test/CallInfo$State;

    #@19
    const-string v1, "DIALING"

    #@1b
    invoke-direct {v0, v1, v6, v6}, Lcom/android/internal/telephony/test/CallInfo$State;-><init>(Ljava/lang/String;II)V

    #@1e
    sput-object v0, Lcom/android/internal/telephony/test/CallInfo$State;->DIALING:Lcom/android/internal/telephony/test/CallInfo$State;

    #@20
    .line 35
    new-instance v0, Lcom/android/internal/telephony/test/CallInfo$State;

    #@22
    const-string v1, "ALERTING"

    #@24
    invoke-direct {v0, v1, v7, v7}, Lcom/android/internal/telephony/test/CallInfo$State;-><init>(Ljava/lang/String;II)V

    #@27
    sput-object v0, Lcom/android/internal/telephony/test/CallInfo$State;->ALERTING:Lcom/android/internal/telephony/test/CallInfo$State;

    #@29
    .line 36
    new-instance v0, Lcom/android/internal/telephony/test/CallInfo$State;

    #@2b
    const-string v1, "INCOMING"

    #@2d
    invoke-direct {v0, v1, v8, v8}, Lcom/android/internal/telephony/test/CallInfo$State;-><init>(Ljava/lang/String;II)V

    #@30
    sput-object v0, Lcom/android/internal/telephony/test/CallInfo$State;->INCOMING:Lcom/android/internal/telephony/test/CallInfo$State;

    #@32
    .line 37
    new-instance v0, Lcom/android/internal/telephony/test/CallInfo$State;

    #@34
    const-string v1, "WAITING"

    #@36
    const/4 v2, 0x5

    #@37
    const/4 v3, 0x5

    #@38
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/test/CallInfo$State;-><init>(Ljava/lang/String;II)V

    #@3b
    sput-object v0, Lcom/android/internal/telephony/test/CallInfo$State;->WAITING:Lcom/android/internal/telephony/test/CallInfo$State;

    #@3d
    .line 31
    const/4 v0, 0x6

    #@3e
    new-array v0, v0, [Lcom/android/internal/telephony/test/CallInfo$State;

    #@40
    sget-object v1, Lcom/android/internal/telephony/test/CallInfo$State;->ACTIVE:Lcom/android/internal/telephony/test/CallInfo$State;

    #@42
    aput-object v1, v0, v4

    #@44
    sget-object v1, Lcom/android/internal/telephony/test/CallInfo$State;->HOLDING:Lcom/android/internal/telephony/test/CallInfo$State;

    #@46
    aput-object v1, v0, v5

    #@48
    sget-object v1, Lcom/android/internal/telephony/test/CallInfo$State;->DIALING:Lcom/android/internal/telephony/test/CallInfo$State;

    #@4a
    aput-object v1, v0, v6

    #@4c
    sget-object v1, Lcom/android/internal/telephony/test/CallInfo$State;->ALERTING:Lcom/android/internal/telephony/test/CallInfo$State;

    #@4e
    aput-object v1, v0, v7

    #@50
    sget-object v1, Lcom/android/internal/telephony/test/CallInfo$State;->INCOMING:Lcom/android/internal/telephony/test/CallInfo$State;

    #@52
    aput-object v1, v0, v8

    #@54
    const/4 v1, 0x5

    #@55
    sget-object v2, Lcom/android/internal/telephony/test/CallInfo$State;->WAITING:Lcom/android/internal/telephony/test/CallInfo$State;

    #@57
    aput-object v2, v0, v1

    #@59
    sput-object v0, Lcom/android/internal/telephony/test/CallInfo$State;->$VALUES:[Lcom/android/internal/telephony/test/CallInfo$State;

    #@5b
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .registers 4
    .parameter
    .parameter
    .parameter "value"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    iput p3, p0, Lcom/android/internal/telephony/test/CallInfo$State;->value:I

    #@5
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/internal/telephony/test/CallInfo$State;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 31
    const-class v0, Lcom/android/internal/telephony/test/CallInfo$State;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/telephony/test/CallInfo$State;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/android/internal/telephony/test/CallInfo$State;
    .registers 1

    #@0
    .prologue
    .line 31
    sget-object v0, Lcom/android/internal/telephony/test/CallInfo$State;->$VALUES:[Lcom/android/internal/telephony/test/CallInfo$State;

    #@2
    invoke-virtual {v0}, [Lcom/android/internal/telephony/test/CallInfo$State;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/android/internal/telephony/test/CallInfo$State;

    #@8
    return-object v0
.end method


# virtual methods
.method public value()I
    .registers 2

    #@0
    .prologue
    .line 42
    iget v0, p0, Lcom/android/internal/telephony/test/CallInfo$State;->value:I

    #@2
    return v0
.end method
