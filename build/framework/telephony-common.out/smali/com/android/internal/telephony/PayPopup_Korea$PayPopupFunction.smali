.class public final enum Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;
.super Ljava/lang/Enum;
.source "PayPopup_Korea.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/PayPopup_Korea;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PayPopupFunction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;

.field public static final enum PayPopupforKT:Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;

.field public static final enum PayPopupforLGT:Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;

.field public static final enum PayPopupforSKT:Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;

.field public static final enum showDialog:Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;

.field public static final enum showToast:Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    #@0
    .prologue
    const/4 v6, 0x4

    #@1
    const/4 v5, 0x3

    #@2
    const/4 v4, 0x2

    #@3
    const/4 v3, 0x1

    #@4
    const/4 v2, 0x0

    #@5
    .line 586
    new-instance v0, Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;

    #@7
    const-string v1, "PayPopupforSKT"

    #@9
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;-><init>(Ljava/lang/String;I)V

    #@c
    sput-object v0, Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;->PayPopupforSKT:Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;

    #@e
    .line 587
    new-instance v0, Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;

    #@10
    const-string v1, "PayPopupforKT"

    #@12
    invoke-direct {v0, v1, v3}, Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;-><init>(Ljava/lang/String;I)V

    #@15
    sput-object v0, Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;->PayPopupforKT:Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;

    #@17
    .line 588
    new-instance v0, Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;

    #@19
    const-string v1, "PayPopupforLGT"

    #@1b
    invoke-direct {v0, v1, v4}, Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;-><init>(Ljava/lang/String;I)V

    #@1e
    sput-object v0, Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;->PayPopupforLGT:Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;

    #@20
    .line 590
    new-instance v0, Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;

    #@22
    const-string v1, "showToast"

    #@24
    invoke-direct {v0, v1, v5}, Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;-><init>(Ljava/lang/String;I)V

    #@27
    sput-object v0, Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;->showToast:Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;

    #@29
    .line 591
    new-instance v0, Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;

    #@2b
    const-string v1, "showDialog"

    #@2d
    invoke-direct {v0, v1, v6}, Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;-><init>(Ljava/lang/String;I)V

    #@30
    sput-object v0, Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;->showDialog:Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;

    #@32
    .line 585
    const/4 v0, 0x5

    #@33
    new-array v0, v0, [Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;

    #@35
    sget-object v1, Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;->PayPopupforSKT:Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;

    #@37
    aput-object v1, v0, v2

    #@39
    sget-object v1, Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;->PayPopupforKT:Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;

    #@3b
    aput-object v1, v0, v3

    #@3d
    sget-object v1, Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;->PayPopupforLGT:Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;

    #@3f
    aput-object v1, v0, v4

    #@41
    sget-object v1, Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;->showToast:Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;

    #@43
    aput-object v1, v0, v5

    #@45
    sget-object v1, Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;->showDialog:Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;

    #@47
    aput-object v1, v0, v6

    #@49
    sput-object v0, Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;->$VALUES:[Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;

    #@4b
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 585
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 585
    const-class v0, Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;
    .registers 1

    #@0
    .prologue
    .line 585
    sget-object v0, Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;->$VALUES:[Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;

    #@2
    invoke-virtual {v0}, [Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/android/internal/telephony/PayPopup_Korea$PayPopupFunction;

    #@8
    return-object v0
.end method
