.class public Lcom/android/internal/telephony/cdma/DataProfileCdma;
.super Lcom/android/internal/telephony/DataProfile;
.source "DataProfileCdma.java"


# instance fields
.field private mProfileId:I


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .registers 12
    .parameter "id"
    .parameter "numeric"
    .parameter "name"
    .parameter "user"
    .parameter "password"
    .parameter "authType"
    .parameter "types"
    .parameter "protocol"
    .parameter "roamingProtocol"
    .parameter "bearer"

    #@0
    .prologue
    .line 37
    invoke-direct/range {p0 .. p10}, Lcom/android/internal/telephony/DataProfile;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    #@3
    .line 33
    const/4 v0, 0x0

    #@4
    iput v0, p0, Lcom/android/internal/telephony/cdma/DataProfileCdma;->mProfileId:I

    #@6
    .line 39
    return-void
.end method


# virtual methods
.method public canHandleType(Ljava/lang/String;)Z
    .registers 6
    .parameter "type"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 44
    sget-boolean v1, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_KDDI_SUPPORT_DYNAMIC_APN_NAI_SETTING_FOR_CPA:Z

    #@3
    if-ne v1, v0, :cond_26

    #@5
    .line 46
    const-string v1, "CDMA"

    #@7
    new-instance v2, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v3, "canHandleType type="

    #@e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v2

    #@1a
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    .line 47
    const-string v1, "dun"

    #@1f
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@22
    move-result v1

    #@23
    if-eqz v1, :cond_26

    #@25
    .line 51
    :goto_25
    return v0

    #@26
    :cond_26
    iget-object v0, p0, Lcom/android/internal/telephony/DataProfile;->types:[Ljava/lang/String;

    #@28
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    #@2b
    move-result-object v0

    #@2c
    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    #@2f
    move-result v0

    #@30
    goto :goto_25
.end method

.method public getDataProfileType()Lcom/android/internal/telephony/DataProfile$DataProfileType;
    .registers 2

    #@0
    .prologue
    .line 56
    sget-object v0, Lcom/android/internal/telephony/DataProfile$DataProfileType;->PROFILE_TYPE_CDMA:Lcom/android/internal/telephony/DataProfile$DataProfileType;

    #@2
    return-object v0
.end method

.method public getProfileId()I
    .registers 2

    #@0
    .prologue
    .line 61
    iget v0, p0, Lcom/android/internal/telephony/cdma/DataProfileCdma;->mProfileId:I

    #@2
    return v0
.end method

.method public setProfileId(I)V
    .registers 2
    .parameter "profileId"

    #@0
    .prologue
    .line 65
    iput p1, p0, Lcom/android/internal/telephony/cdma/DataProfileCdma;->mProfileId:I

    #@2
    .line 66
    return-void
.end method

.method public toHash()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 75
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/DataProfileCdma;->toString()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public toShortString()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 70
    const-string v0, "DataProfileCdma"

    #@2
    return-object v0
.end method
