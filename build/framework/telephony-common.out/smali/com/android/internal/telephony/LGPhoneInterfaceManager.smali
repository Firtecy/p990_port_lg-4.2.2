.class public Lcom/android/internal/telephony/LGPhoneInterfaceManager;
.super Lcom/android/internal/telephony/ILGTelephony$Stub;
.source "LGPhoneInterfaceManager.java"


# static fields
.field private static final DBG:Z = true

.field private static final LOG_TAG:Ljava/lang/String; = "LGPhoneInterfaceManager"

.field private static sInstance:Lcom/android/internal/telephony/LGPhoneInterfaceManager;


# instance fields
.field mPhone:Lcom/android/internal/telephony/Phone;


# direct methods
.method private constructor <init>(Lcom/android/internal/telephony/Phone;)V
    .registers 2
    .parameter "phone"

    #@0
    .prologue
    .line 50
    invoke-direct {p0}, Lcom/android/internal/telephony/ILGTelephony$Stub;-><init>()V

    #@3
    .line 51
    iput-object p1, p0, Lcom/android/internal/telephony/LGPhoneInterfaceManager;->mPhone:Lcom/android/internal/telephony/Phone;

    #@5
    .line 52
    invoke-direct {p0}, Lcom/android/internal/telephony/LGPhoneInterfaceManager;->publish()V

    #@8
    .line 53
    return-void
.end method

.method static init(Lcom/android/internal/telephony/Phone;)Lcom/android/internal/telephony/LGPhoneInterfaceManager;
    .registers 5
    .parameter "phone"

    #@0
    .prologue
    .line 39
    const-class v1, Lcom/android/internal/telephony/LGPhoneInterfaceManager;

    #@2
    monitor-enter v1

    #@3
    .line 40
    :try_start_3
    sget-object v0, Lcom/android/internal/telephony/LGPhoneInterfaceManager;->sInstance:Lcom/android/internal/telephony/LGPhoneInterfaceManager;

    #@5
    if-nez v0, :cond_12

    #@7
    .line 41
    new-instance v0, Lcom/android/internal/telephony/LGPhoneInterfaceManager;

    #@9
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/LGPhoneInterfaceManager;-><init>(Lcom/android/internal/telephony/Phone;)V

    #@c
    sput-object v0, Lcom/android/internal/telephony/LGPhoneInterfaceManager;->sInstance:Lcom/android/internal/telephony/LGPhoneInterfaceManager;

    #@e
    .line 45
    :goto_e
    sget-object v0, Lcom/android/internal/telephony/LGPhoneInterfaceManager;->sInstance:Lcom/android/internal/telephony/LGPhoneInterfaceManager;

    #@10
    monitor-exit v1

    #@11
    return-object v0

    #@12
    .line 43
    :cond_12
    const-string v0, "LGPhoneInterfaceManager"

    #@14
    new-instance v2, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v3, "init() called multiple times!\tsInstance = "

    #@1b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    sget-object v3, Lcom/android/internal/telephony/LGPhoneInterfaceManager;->sInstance:Lcom/android/internal/telephony/LGPhoneInterfaceManager;

    #@21
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v2

    #@29
    invoke-static {v0, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    goto :goto_e

    #@2d
    .line 46
    :catchall_2d
    move-exception v0

    #@2e
    monitor-exit v1
    :try_end_2f
    .catchall {:try_start_3 .. :try_end_2f} :catchall_2d

    #@2f
    throw v0
.end method

.method private log(Ljava/lang/String;)V
    .registers 5
    .parameter "msg"

    #@0
    .prologue
    .line 68
    const-string v0, "LGPhoneInterfaceManager"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[LPhoneMgr] "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 69
    return-void
.end method

.method private publish()V
    .registers 3

    #@0
    .prologue
    .line 56
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "publish: "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/LGPhoneInterfaceManager;->log(Ljava/lang/String;)V

    #@16
    .line 57
    const-string v0, "Lphone"

    #@18
    invoke-static {v0, p0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    #@1b
    .line 58
    return-void
.end method


# virtual methods
.method public sendDefaultAttachProfile(I)V
    .registers 4
    .parameter "profileId"

    #@0
    .prologue
    .line 62
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "sendDefaultAttachProfile: "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/LGPhoneInterfaceManager;->log(Ljava/lang/String;)V

    #@16
    .line 63
    iget-object v0, p0, Lcom/android/internal/telephony/LGPhoneInterfaceManager;->mPhone:Lcom/android/internal/telephony/Phone;

    #@18
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->sendDefaultAttachProfile(I)V

    #@1b
    .line 64
    return-void
.end method
