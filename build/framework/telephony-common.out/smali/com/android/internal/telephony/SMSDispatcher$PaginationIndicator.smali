.class Lcom/android/internal/telephony/SMSDispatcher$PaginationIndicator;
.super Ljava/lang/Object;
.source "SMSDispatcher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/SMSDispatcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PaginationIndicator"
.end annotation


# instance fields
.field private mLength:I

.field private mSequence:I

.field private mTotalCount:I


# direct methods
.method public constructor <init>(III)V
    .registers 4
    .parameter "sequence"
    .parameter "totalCount"
    .parameter "length"

    #@0
    .prologue
    .line 4134
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 4135
    iput p1, p0, Lcom/android/internal/telephony/SMSDispatcher$PaginationIndicator;->mSequence:I

    #@5
    .line 4136
    iput p2, p0, Lcom/android/internal/telephony/SMSDispatcher$PaginationIndicator;->mTotalCount:I

    #@7
    .line 4137
    iput p3, p0, Lcom/android/internal/telephony/SMSDispatcher$PaginationIndicator;->mLength:I

    #@9
    .line 4138
    return-void
.end method


# virtual methods
.method public getLength()I
    .registers 2

    #@0
    .prologue
    .line 4149
    iget v0, p0, Lcom/android/internal/telephony/SMSDispatcher$PaginationIndicator;->mLength:I

    #@2
    return v0
.end method

.method public getSequence()I
    .registers 2

    #@0
    .prologue
    .line 4141
    iget v0, p0, Lcom/android/internal/telephony/SMSDispatcher$PaginationIndicator;->mSequence:I

    #@2
    return v0
.end method

.method public getTotalCount()I
    .registers 2

    #@0
    .prologue
    .line 4145
    iget v0, p0, Lcom/android/internal/telephony/SMSDispatcher$PaginationIndicator;->mTotalCount:I

    #@2
    return v0
.end method
