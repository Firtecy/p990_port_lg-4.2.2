.class public Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaDisplayInfoRec;
.super Ljava/lang/Object;
.source "CdmaInformationRecords.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/cdma/CdmaInformationRecords;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CdmaDisplayInfoRec"
.end annotation


# instance fields
.field public alpha:Ljava/lang/String;

.field public id:I

.field public itemrecs:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayItemRec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 271
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 272
    const/4 v0, 0x0

    #@4
    iput v0, p0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaDisplayInfoRec;->id:I

    #@6
    .line 273
    const-string v0, ""

    #@8
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaDisplayInfoRec;->alpha:Ljava/lang/String;

    #@a
    .line 274
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;)V
    .registers 3
    .parameter "id"
    .parameter "alpha"

    #@0
    .prologue
    .line 277
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 278
    iput p1, p0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaDisplayInfoRec;->id:I

    #@5
    .line 279
    iput-object p2, p0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaDisplayInfoRec;->alpha:Ljava/lang/String;

    #@7
    .line 280
    return-void
.end method

.method public constructor <init>(I[B)V
    .registers 6
    .parameter "id"
    .parameter "data"

    #@0
    .prologue
    .line 281
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 282
    const-string v0, "CdmaInformationRecords"

    #@5
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v2, "CdmaDisplayInfoRec("

    #@c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    const-string v2, ", data: "

    #@16
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    const-string v2, ")"

    #@20
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v1

    #@24
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v1

    #@28
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 283
    iput p1, p0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaDisplayInfoRec;->id:I

    #@2d
    .line 284
    const-string v0, ""

    #@2f
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaDisplayInfoRec;->alpha:Ljava/lang/String;

    #@31
    .line 285
    invoke-direct {p0, p2}, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaDisplayInfoRec;->readItems([B)V

    #@34
    .line 286
    return-void
.end method

.method private readItems([B)V
    .registers 15
    .parameter "data"

    #@0
    .prologue
    .line 289
    const-string v10, "CdmaInformationRecords"

    #@2
    new-instance v11, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v12, "CdmaDisplayInfoRec.readItems(len: "

    #@9
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v11

    #@d
    array-length v12, p1

    #@e
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v11

    #@12
    const-string v12, ")"

    #@14
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v11

    #@18
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v11

    #@1c
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 291
    const-string v10, "CdmaInformationRecords"

    #@21
    invoke-static {p1}, Lcom/android/internal/util/HexDump;->dumpHexString([B)Ljava/lang/String;

    #@24
    move-result-object v11

    #@25
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 293
    const/4 v7, 0x0

    #@29
    .line 294
    .local v7, read:I
    const/4 v6, 0x0

    #@2a
    .line 295
    .local v6, linelen:I
    new-instance v0, Ljava/lang/StringBuffer;

    #@2c
    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    #@2f
    .line 296
    .local v0, buffer:Ljava/lang/StringBuffer;
    iget-object v10, p0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaDisplayInfoRec;->itemrecs:Ljava/util/Vector;

    #@31
    if-nez v10, :cond_3a

    #@33
    .line 297
    new-instance v10, Ljava/util/Vector;

    #@35
    invoke-direct {v10}, Ljava/util/Vector;-><init>()V

    #@38
    iput-object v10, p0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaDisplayInfoRec;->itemrecs:Ljava/util/Vector;

    #@3a
    .line 298
    :cond_3a
    const/4 v7, 0x0

    #@3b
    :goto_3b
    array-length v10, p1

    #@3c
    if-ge v7, v10, :cond_105

    #@3e
    .line 299
    add-int/lit8 v8, v7, 0x1

    #@40
    .end local v7           #read:I
    .local v8, read:I
    aget-byte v10, p1, v7

    #@42
    invoke-static {v10}, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->fromByte(B)Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@45
    move-result-object v4

    #@46
    .line 300
    .local v4, itag:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;
    if-nez v4, :cond_6a

    #@48
    .line 301
    const-string v10, "CdmaInformationRecords"

    #@4a
    new-instance v11, Ljava/lang/StringBuilder;

    #@4c
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@4f
    const-string v12, "itag for ["

    #@51
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v11

    #@55
    add-int/lit8 v12, v8, -0x1

    #@57
    aget-byte v12, p1, v12

    #@59
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v11

    #@5d
    const-string v12, "] is null!!!"

    #@5f
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v11

    #@63
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@66
    move-result-object v11

    #@67
    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@6a
    .line 303
    :cond_6a
    add-int/lit8 v7, v8, 0x1

    #@6c
    .end local v8           #read:I
    .restart local v7       #read:I
    aget-byte v3, p1, v8

    #@6e
    .line 304
    .local v3, ilen:B
    const/4 v2, 0x0

    #@6f
    .line 305
    .local v2, idata:[B
    sget-object v10, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$1;->$SwitchMap$com$android$internal$telephony$cdma$CdmaInformationRecords$ExtendedDisplayTag:[I

    #@71
    invoke-virtual {v4}, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->ordinal()I

    #@74
    move-result v11

    #@75
    aget v10, v10, v11

    #@77
    packed-switch v10, :pswitch_data_10e

    #@7a
    .line 329
    new-array v2, v3, [B

    #@7c
    .line 330
    const/4 v1, 0x0

    #@7d
    .local v1, i:I
    move v8, v7

    #@7e
    .end local v7           #read:I
    .restart local v8       #read:I
    :goto_7e
    if-ge v1, v3, :cond_10c

    #@80
    .line 331
    add-int/lit8 v7, v8, 0x1

    #@82
    .end local v8           #read:I
    .restart local v7       #read:I
    aget-byte v10, p1, v8

    #@84
    aput-byte v10, v2, v1

    #@86
    .line 330
    add-int/lit8 v1, v1, 0x1

    #@88
    move v8, v7

    #@89
    .end local v7           #read:I
    .restart local v8       #read:I
    goto :goto_7e

    #@8a
    .line 310
    .end local v1           #i:I
    .end local v8           #read:I
    .restart local v7       #read:I
    :pswitch_8a
    const-string v10, "CdmaInformationRecords"

    #@8c
    new-instance v11, Ljava/lang/StringBuilder;

    #@8e
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@91
    const-string v12, "readItems: got a DISPLAY_TAG_BLANK(ilen:"

    #@93
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v11

    #@97
    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v11

    #@9b
    const-string v12, ")"

    #@9d
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a0
    move-result-object v11

    #@a1
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a4
    move-result-object v11

    #@a5
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a8
    .line 312
    const/4 v1, 0x0

    #@a9
    .restart local v1       #i:I
    :goto_a9
    if-ge v1, v3, :cond_dd

    #@ab
    .line 313
    const-string v10, " "

    #@ad
    invoke-virtual {v0, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@b0
    .line 316
    const/16 v10, 0x28

    #@b2
    if-lt v6, v10, :cond_ba

    #@b4
    .line 317
    const-string v10, "\r\n"

    #@b6
    invoke-virtual {v0, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@b9
    .line 318
    const/4 v6, 0x0

    #@ba
    .line 312
    :cond_ba
    add-int/lit8 v1, v1, 0x1

    #@bc
    add-int/lit8 v6, v6, 0x1

    #@be
    goto :goto_a9

    #@bf
    .line 323
    .end local v1           #i:I
    :pswitch_bf
    const-string v10, "CdmaInformationRecords"

    #@c1
    new-instance v11, Ljava/lang/StringBuilder;

    #@c3
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@c6
    const-string v12, "readItems: got a DISPLAY_TAG_SKIP(ilen: "

    #@c8
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cb
    move-result-object v11

    #@cc
    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@cf
    move-result-object v11

    #@d0
    const-string v12, ")"

    #@d2
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v11

    #@d6
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d9
    move-result-object v11

    #@da
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@dd
    .line 334
    :cond_dd
    :goto_dd
    const-string v10, "CdmaInformationRecords"

    #@df
    const-string v11, "readItems: Creating a new DisplayItemRec"

    #@e1
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e4
    .line 335
    new-instance v5, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayItemRec;

    #@e6
    invoke-direct {v5, v4, v3, v2}, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayItemRec;-><init>(Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;B[B)V

    #@e9
    .line 336
    .local v5, item:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayItemRec;
    invoke-virtual {v5}, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayItemRec;->getDataAsString()Ljava/lang/String;

    #@ec
    move-result-object v9

    #@ed
    .line 337
    .local v9, s:Ljava/lang/String;
    if-eqz v9, :cond_f7

    #@ef
    .line 338
    invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@f2
    .line 339
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    #@f5
    move-result v10

    #@f6
    add-int/2addr v6, v10

    #@f7
    .line 341
    :cond_f7
    iget-object v10, p0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaDisplayInfoRec;->itemrecs:Ljava/util/Vector;

    #@f9
    invoke-virtual {v10, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    #@fc
    .line 342
    const-string v10, "CdmaInformationRecords"

    #@fe
    const-string v11, "readItems: Added a new DisplayItemRec"

    #@100
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@103
    goto/16 :goto_3b

    #@105
    .line 344
    .end local v2           #idata:[B
    .end local v3           #ilen:B
    .end local v4           #itag:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;
    .end local v5           #item:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayItemRec;
    .end local v9           #s:Ljava/lang/String;
    :cond_105
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@108
    move-result-object v10

    #@109
    iput-object v10, p0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaDisplayInfoRec;->alpha:Ljava/lang/String;

    #@10b
    .line 345
    return-void

    #@10c
    .end local v7           #read:I
    .restart local v1       #i:I
    .restart local v2       #idata:[B
    .restart local v3       #ilen:B
    .restart local v4       #itag:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;
    .restart local v8       #read:I
    :cond_10c
    move v7, v8

    #@10d
    .end local v8           #read:I
    .restart local v7       #read:I
    goto :goto_dd

    #@10e
    .line 305
    :pswitch_data_10e
    .packed-switch 0x1
        :pswitch_8a
        :pswitch_bf
    .end packed-switch
.end method


# virtual methods
.method public isExtended()Z
    .registers 3

    #@0
    .prologue
    .line 348
    iget v0, p0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaDisplayInfoRec;->id:I

    #@2
    const/4 v1, 0x7

    #@3
    if-ne v0, v1, :cond_7

    #@5
    const/4 v0, 0x1

    #@6
    :goto_6
    return v0

    #@7
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 353
    new-instance v0, Ljava/lang/StringBuffer;

    #@2
    const/16 v3, 0xc8

    #@4
    invoke-direct {v0, v3}, Ljava/lang/StringBuffer;-><init>(I)V

    #@7
    .line 354
    .local v0, buffer:Ljava/lang/StringBuffer;
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaDisplayInfoRec;->isExtended()Z

    #@a
    move-result v3

    #@b
    if-nez v3, :cond_2f

    #@d
    .line 355
    const-string v3, "CdmaDisplayInfoRec: { id: "

    #@f
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@12
    .line 356
    iget v3, p0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaDisplayInfoRec;->id:I

    #@14
    invoke-static {v3}, Lcom/android/internal/telephony/cdma/CdmaInformationRecords;->idToString(I)Ljava/lang/String;

    #@17
    move-result-object v3

    #@18
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@1b
    .line 357
    const-string v3, ", alpha: "

    #@1d
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@20
    .line 358
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaDisplayInfoRec;->alpha:Ljava/lang/String;

    #@22
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@25
    .line 359
    const-string v3, " }"

    #@27
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@2a
    .line 360
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@2d
    move-result-object v3

    #@2e
    .line 370
    :goto_2e
    return-object v3

    #@2f
    .line 362
    :cond_2f
    const-string v3, "CdmaDisplayInfoRec(extended): { id: "

    #@31
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@34
    .line 363
    iget v3, p0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaDisplayInfoRec;->id:I

    #@36
    invoke-static {v3}, Lcom/android/internal/telephony/cdma/CdmaInformationRecords;->idToString(I)Ljava/lang/String;

    #@39
    move-result-object v3

    #@3a
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@3d
    .line 364
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$CdmaDisplayInfoRec;->itemrecs:Ljava/util/Vector;

    #@3f
    invoke-virtual {v3}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    #@42
    move-result-object v1

    #@43
    .local v1, i$:Ljava/util/Iterator;
    :goto_43
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@46
    move-result v3

    #@47
    if-eqz v3, :cond_61

    #@49
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@4c
    move-result-object v2

    #@4d
    check-cast v2, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayItemRec;

    #@4f
    .line 365
    .local v2, rec:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayItemRec;
    const-string v3, " ["

    #@51
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@54
    .line 366
    invoke-virtual {v2}, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayItemRec;->toString()Ljava/lang/String;

    #@57
    move-result-object v3

    #@58
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@5b
    .line 367
    const-string v3, "]"

    #@5d
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@60
    goto :goto_43

    #@61
    .line 369
    .end local v2           #rec:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayItemRec;
    :cond_61
    const-string v3, " }"

    #@63
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@66
    .line 370
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@69
    move-result-object v3

    #@6a
    goto :goto_2e
.end method
