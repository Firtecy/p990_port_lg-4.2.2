.class Lcom/android/internal/telephony/RILRequest;
.super Ljava/lang/Object;
.source "RIL.java"


# static fields
.field static final LOG_TAG:Ljava/lang/String; = "RILJ"

.field private static final MAX_POOL_SIZE:I = 0x4

.field static sNextSerial:I

.field private static sPool:Lcom/android/internal/telephony/RILRequest;

.field private static sPoolSize:I

.field private static sPoolSync:Ljava/lang/Object;

.field static sSerialMonitor:Ljava/lang/Object;


# instance fields
.field mNext:Lcom/android/internal/telephony/RILRequest;

.field mRequest:I

.field mResult:Landroid/os/Message;

.field mSerial:I

.field mp:Landroid/os/Parcel;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 151
    sput v1, Lcom/android/internal/telephony/RILRequest;->sNextSerial:I

    #@3
    .line 152
    new-instance v0, Ljava/lang/Object;

    #@5
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@8
    sput-object v0, Lcom/android/internal/telephony/RILRequest;->sSerialMonitor:Ljava/lang/Object;

    #@a
    .line 153
    new-instance v0, Ljava/lang/Object;

    #@c
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@f
    sput-object v0, Lcom/android/internal/telephony/RILRequest;->sPoolSync:Ljava/lang/Object;

    #@11
    .line 154
    const/4 v0, 0x0

    #@12
    sput-object v0, Lcom/android/internal/telephony/RILRequest;->sPool:Lcom/android/internal/telephony/RILRequest;

    #@14
    .line 155
    sput v1, Lcom/android/internal/telephony/RILRequest;->sPoolSize:I

    #@16
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 222
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 223
    return-void
.end method

.method static obtain(ILandroid/os/Message;)Lcom/android/internal/telephony/RILRequest;
    .registers 6
    .parameter "request"
    .parameter "result"

    #@0
    .prologue
    .line 173
    const/4 v0, 0x0

    #@1
    .line 175
    .local v0, rr:Lcom/android/internal/telephony/RILRequest;
    sget-object v2, Lcom/android/internal/telephony/RILRequest;->sPoolSync:Ljava/lang/Object;

    #@3
    monitor-enter v2

    #@4
    .line 176
    :try_start_4
    sget-object v1, Lcom/android/internal/telephony/RILRequest;->sPool:Lcom/android/internal/telephony/RILRequest;

    #@6
    if-eqz v1, :cond_17

    #@8
    .line 177
    sget-object v0, Lcom/android/internal/telephony/RILRequest;->sPool:Lcom/android/internal/telephony/RILRequest;

    #@a
    .line 178
    iget-object v1, v0, Lcom/android/internal/telephony/RILRequest;->mNext:Lcom/android/internal/telephony/RILRequest;

    #@c
    sput-object v1, Lcom/android/internal/telephony/RILRequest;->sPool:Lcom/android/internal/telephony/RILRequest;

    #@e
    .line 179
    const/4 v1, 0x0

    #@f
    iput-object v1, v0, Lcom/android/internal/telephony/RILRequest;->mNext:Lcom/android/internal/telephony/RILRequest;

    #@11
    .line 180
    sget v1, Lcom/android/internal/telephony/RILRequest;->sPoolSize:I

    #@13
    add-int/lit8 v1, v1, -0x1

    #@15
    sput v1, Lcom/android/internal/telephony/RILRequest;->sPoolSize:I

    #@17
    .line 182
    :cond_17
    monitor-exit v2
    :try_end_18
    .catchall {:try_start_4 .. :try_end_18} :catchall_45

    #@18
    .line 184
    if-nez v0, :cond_1f

    #@1a
    .line 185
    new-instance v0, Lcom/android/internal/telephony/RILRequest;

    #@1c
    .end local v0           #rr:Lcom/android/internal/telephony/RILRequest;
    invoke-direct {v0}, Lcom/android/internal/telephony/RILRequest;-><init>()V

    #@1f
    .line 188
    .restart local v0       #rr:Lcom/android/internal/telephony/RILRequest;
    :cond_1f
    sget-object v2, Lcom/android/internal/telephony/RILRequest;->sSerialMonitor:Ljava/lang/Object;

    #@21
    monitor-enter v2

    #@22
    .line 189
    :try_start_22
    sget v1, Lcom/android/internal/telephony/RILRequest;->sNextSerial:I

    #@24
    add-int/lit8 v3, v1, 0x1

    #@26
    sput v3, Lcom/android/internal/telephony/RILRequest;->sNextSerial:I

    #@28
    iput v1, v0, Lcom/android/internal/telephony/RILRequest;->mSerial:I

    #@2a
    .line 190
    monitor-exit v2
    :try_end_2b
    .catchall {:try_start_22 .. :try_end_2b} :catchall_48

    #@2b
    .line 191
    iput p0, v0, Lcom/android/internal/telephony/RILRequest;->mRequest:I

    #@2d
    .line 192
    iput-object p1, v0, Lcom/android/internal/telephony/RILRequest;->mResult:Landroid/os/Message;

    #@2f
    .line 193
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@32
    move-result-object v1

    #@33
    iput-object v1, v0, Lcom/android/internal/telephony/RILRequest;->mp:Landroid/os/Parcel;

    #@35
    .line 195
    if-eqz p1, :cond_4b

    #@37
    invoke-virtual {p1}, Landroid/os/Message;->getTarget()Landroid/os/Handler;

    #@3a
    move-result-object v1

    #@3b
    if-nez v1, :cond_4b

    #@3d
    .line 196
    new-instance v1, Ljava/lang/NullPointerException;

    #@3f
    const-string v2, "Message target must not be null"

    #@41
    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@44
    throw v1

    #@45
    .line 182
    :catchall_45
    move-exception v1

    #@46
    :try_start_46
    monitor-exit v2
    :try_end_47
    .catchall {:try_start_46 .. :try_end_47} :catchall_45

    #@47
    throw v1

    #@48
    .line 190
    :catchall_48
    move-exception v1

    #@49
    :try_start_49
    monitor-exit v2
    :try_end_4a
    .catchall {:try_start_49 .. :try_end_4a} :catchall_48

    #@4a
    throw v1

    #@4b
    .line 200
    :cond_4b
    iget-object v1, v0, Lcom/android/internal/telephony/RILRequest;->mp:Landroid/os/Parcel;

    #@4d
    invoke-virtual {v1, p0}, Landroid/os/Parcel;->writeInt(I)V

    #@50
    .line 201
    iget-object v1, v0, Lcom/android/internal/telephony/RILRequest;->mp:Landroid/os/Parcel;

    #@52
    iget v2, v0, Lcom/android/internal/telephony/RILRequest;->mSerial:I

    #@54
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@57
    .line 203
    return-object v0
.end method

.method static resetSerial()V
    .registers 2

    #@0
    .prologue
    .line 227
    sget-object v1, Lcom/android/internal/telephony/RILRequest;->sSerialMonitor:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 228
    const/4 v0, 0x0

    #@4
    :try_start_4
    sput v0, Lcom/android/internal/telephony/RILRequest;->sNextSerial:I

    #@6
    .line 229
    monitor-exit v1

    #@7
    .line 230
    return-void

    #@8
    .line 229
    :catchall_8
    move-exception v0

    #@9
    monitor-exit v1
    :try_end_a
    .catchall {:try_start_4 .. :try_end_a} :catchall_8

    #@a
    throw v0
.end method


# virtual methods
.method onError(ILjava/lang/Object;)V
    .registers 7
    .parameter "error"
    .parameter "ret"

    #@0
    .prologue
    .line 255
    invoke-static {p1}, Lcom/android/internal/telephony/CommandException;->fromRilErrno(I)Lcom/android/internal/telephony/CommandException;

    #@3
    move-result-object v0

    #@4
    .line 257
    .local v0, ex:Lcom/android/internal/telephony/CommandException;
    sget-boolean v1, Lcom/android/internal/telephony/LgeRIL;->DBG:Z

    #@6
    if-nez v1, :cond_10

    #@8
    const/16 v1, 0x1b

    #@a
    if-lt p1, v1, :cond_10

    #@c
    const/16 v1, 0x1d

    #@e
    if-le p1, v1, :cond_40

    #@10
    .line 260
    :cond_10
    const-string v1, "RILJ"

    #@12
    new-instance v2, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    invoke-virtual {p0}, Lcom/android/internal/telephony/RILRequest;->serialString()Ljava/lang/String;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    const-string v3, "< "

    #@21
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    iget v3, p0, Lcom/android/internal/telephony/RILRequest;->mRequest:I

    #@27
    invoke-static {v3}, Lcom/android/internal/telephony/RIL;->requestToString(I)Ljava/lang/String;

    #@2a
    move-result-object v3

    #@2b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    const-string v3, " error: "

    #@31
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v2

    #@35
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v2

    #@39
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v2

    #@3d
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    .line 265
    :cond_40
    iget-object v1, p0, Lcom/android/internal/telephony/RILRequest;->mResult:Landroid/os/Message;

    #@42
    if-eqz v1, :cond_4e

    #@44
    .line 266
    iget-object v1, p0, Lcom/android/internal/telephony/RILRequest;->mResult:Landroid/os/Message;

    #@46
    invoke-static {v1, p2, v0}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)Landroid/os/AsyncResult;

    #@49
    .line 267
    iget-object v1, p0, Lcom/android/internal/telephony/RILRequest;->mResult:Landroid/os/Message;

    #@4b
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    #@4e
    .line 270
    :cond_4e
    iget-object v1, p0, Lcom/android/internal/telephony/RILRequest;->mp:Landroid/os/Parcel;

    #@50
    if-eqz v1, :cond_5a

    #@52
    .line 271
    iget-object v1, p0, Lcom/android/internal/telephony/RILRequest;->mp:Landroid/os/Parcel;

    #@54
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@57
    .line 272
    const/4 v1, 0x0

    #@58
    iput-object v1, p0, Lcom/android/internal/telephony/RILRequest;->mp:Landroid/os/Parcel;

    #@5a
    .line 274
    :cond_5a
    return-void
.end method

.method release()V
    .registers 4

    #@0
    .prologue
    .line 212
    sget-object v1, Lcom/android/internal/telephony/RILRequest;->sPoolSync:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 213
    :try_start_3
    sget v0, Lcom/android/internal/telephony/RILRequest;->sPoolSize:I

    #@5
    const/4 v2, 0x4

    #@6
    if-ge v0, v2, :cond_17

    #@8
    .line 214
    sget-object v0, Lcom/android/internal/telephony/RILRequest;->sPool:Lcom/android/internal/telephony/RILRequest;

    #@a
    iput-object v0, p0, Lcom/android/internal/telephony/RILRequest;->mNext:Lcom/android/internal/telephony/RILRequest;

    #@c
    .line 215
    sput-object p0, Lcom/android/internal/telephony/RILRequest;->sPool:Lcom/android/internal/telephony/RILRequest;

    #@e
    .line 216
    sget v0, Lcom/android/internal/telephony/RILRequest;->sPoolSize:I

    #@10
    add-int/lit8 v0, v0, 0x1

    #@12
    sput v0, Lcom/android/internal/telephony/RILRequest;->sPoolSize:I

    #@14
    .line 217
    const/4 v0, 0x0

    #@15
    iput-object v0, p0, Lcom/android/internal/telephony/RILRequest;->mResult:Landroid/os/Message;

    #@17
    .line 219
    :cond_17
    monitor-exit v1

    #@18
    .line 220
    return-void

    #@19
    .line 219
    :catchall_19
    move-exception v0

    #@1a
    monitor-exit v1
    :try_end_1b
    .catchall {:try_start_3 .. :try_end_1b} :catchall_19

    #@1b
    throw v0
.end method

.method serialString()Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    .line 235
    new-instance v2, Ljava/lang/StringBuilder;

    #@2
    const/16 v4, 0x8

    #@4
    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    #@7
    .line 238
    .local v2, sb:Ljava/lang/StringBuilder;
    iget v4, p0, Lcom/android/internal/telephony/RILRequest;->mSerial:I

    #@9
    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@c
    move-result-object v3

    #@d
    .line 241
    .local v3, sn:Ljava/lang/String;
    const/16 v4, 0x5b

    #@f
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@12
    .line 242
    const/4 v0, 0x0

    #@13
    .local v0, i:I
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@16
    move-result v1

    #@17
    .local v1, s:I
    :goto_17
    rsub-int/lit8 v4, v1, 0x4

    #@19
    if-ge v0, v4, :cond_23

    #@1b
    .line 243
    const/16 v4, 0x30

    #@1d
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@20
    .line 242
    add-int/lit8 v0, v0, 0x1

    #@22
    goto :goto_17

    #@23
    .line 246
    :cond_23
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    .line 247
    const/16 v4, 0x5d

    #@28
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@2b
    .line 248
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v4

    #@2f
    return-object v4
.end method
