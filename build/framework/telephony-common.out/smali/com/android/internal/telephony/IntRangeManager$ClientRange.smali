.class Lcom/android/internal/telephony/IntRangeManager$ClientRange;
.super Ljava/lang/Object;
.source "IntRangeManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/IntRangeManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ClientRange"
.end annotation


# instance fields
.field final client:Ljava/lang/String;

.field final endId:I

.field final startId:I

.field final synthetic this$0:Lcom/android/internal/telephony/IntRangeManager;


# direct methods
.method constructor <init>(Lcom/android/internal/telephony/IntRangeManager;IILjava/lang/String;)V
    .registers 5
    .parameter
    .parameter "startId"
    .parameter "endId"
    .parameter "client"

    #@0
    .prologue
    .line 152
    iput-object p1, p0, Lcom/android/internal/telephony/IntRangeManager$ClientRange;->this$0:Lcom/android/internal/telephony/IntRangeManager;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 153
    iput p2, p0, Lcom/android/internal/telephony/IntRangeManager$ClientRange;->startId:I

    #@7
    .line 154
    iput p3, p0, Lcom/android/internal/telephony/IntRangeManager$ClientRange;->endId:I

    #@9
    .line 155
    iput-object p4, p0, Lcom/android/internal/telephony/IntRangeManager$ClientRange;->client:Ljava/lang/String;

    #@b
    .line 156
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 6
    .parameter "o"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 160
    if-eqz p1, :cond_21

    #@3
    instance-of v2, p1, Lcom/android/internal/telephony/IntRangeManager$ClientRange;

    #@5
    if-eqz v2, :cond_21

    #@7
    move-object v0, p1

    #@8
    .line 161
    check-cast v0, Lcom/android/internal/telephony/IntRangeManager$ClientRange;

    #@a
    .line 162
    .local v0, other:Lcom/android/internal/telephony/IntRangeManager$ClientRange;
    iget v2, p0, Lcom/android/internal/telephony/IntRangeManager$ClientRange;->startId:I

    #@c
    iget v3, v0, Lcom/android/internal/telephony/IntRangeManager$ClientRange;->startId:I

    #@e
    if-ne v2, v3, :cond_21

    #@10
    iget v2, p0, Lcom/android/internal/telephony/IntRangeManager$ClientRange;->endId:I

    #@12
    iget v3, v0, Lcom/android/internal/telephony/IntRangeManager$ClientRange;->endId:I

    #@14
    if-ne v2, v3, :cond_21

    #@16
    iget-object v2, p0, Lcom/android/internal/telephony/IntRangeManager$ClientRange;->client:Ljava/lang/String;

    #@18
    iget-object v3, v0, Lcom/android/internal/telephony/IntRangeManager$ClientRange;->client:Ljava/lang/String;

    #@1a
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1d
    move-result v2

    #@1e
    if-eqz v2, :cond_21

    #@20
    const/4 v1, 0x1

    #@21
    .line 166
    .end local v0           #other:Lcom/android/internal/telephony/IntRangeManager$ClientRange;
    :cond_21
    return v1
.end method

.method public hashCode()I
    .registers 3

    #@0
    .prologue
    .line 172
    iget v0, p0, Lcom/android/internal/telephony/IntRangeManager$ClientRange;->startId:I

    #@2
    mul-int/lit8 v0, v0, 0x1f

    #@4
    iget v1, p0, Lcom/android/internal/telephony/IntRangeManager$ClientRange;->endId:I

    #@6
    add-int/2addr v0, v1

    #@7
    mul-int/lit8 v0, v0, 0x1f

    #@9
    iget-object v1, p0, Lcom/android/internal/telephony/IntRangeManager$ClientRange;->client:Ljava/lang/String;

    #@b
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    #@e
    move-result v1

    #@f
    add-int/2addr v0, v1

    #@10
    return v0
.end method
