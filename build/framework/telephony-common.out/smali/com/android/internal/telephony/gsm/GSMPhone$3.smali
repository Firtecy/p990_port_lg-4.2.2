.class Lcom/android/internal/telephony/gsm/GSMPhone$3;
.super Landroid/content/BroadcastReceiver;
.source "GSMPhone.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/gsm/GSMPhone;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/gsm/GSMPhone;


# direct methods
.method constructor <init>(Lcom/android/internal/telephony/gsm/GSMPhone;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 444
    iput-object p1, p0, Lcom/android/internal/telephony/gsm/GSMPhone$3;->this$0:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 11
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    .line 447
    invoke-static {p1, p2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->updateProfile(Landroid/content/Context;Landroid/content/Intent;)V

    #@4
    .line 449
    const-string v2, "ss"

    #@6
    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@9
    move-result-object v1

    #@a
    .line 450
    .local v1, stateExtra:Ljava/lang/String;
    const-string v2, "GSM"

    #@c
    new-instance v3, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v4, "[GSMPhone] SimStateReceiver - stateExtra: "

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v3

    #@1f
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 451
    const-string v2, "LOADED"

    #@24
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@27
    move-result v2

    #@28
    if-eqz v2, :cond_34

    #@2a
    .line 453
    const-string v2, "GSM"

    #@2c
    const-string v3, "[GSMPhone] SimStateReceiver - ICC LOADED"

    #@2e
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@31
    .line 455
    invoke-static {}, Lcom/android/internal/telephony/GsmAlphabet;->setEnabledShiftTablesLG()V

    #@34
    .line 459
    :cond_34
    const-string v2, "READY"

    #@36
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@39
    move-result v2

    #@3a
    if-eqz v2, :cond_97

    #@3c
    .line 461
    const-string v2, "GSM"

    #@3e
    const-string v3, "[GSMPhone] SimStateReceiver - ICC READY"

    #@40
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@43
    .line 463
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GSMPhone$3;->this$0:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@45
    invoke-static {v2}, Lcom/android/internal/telephony/gsm/GSMPhone;->access$000(Lcom/android/internal/telephony/gsm/GSMPhone;)Landroid/content/Context;

    #@48
    move-result-object v2

    #@49
    const-string v3, "seperate_processing_sms_uicc"

    #@4b
    invoke-static {v2, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@4e
    move-result v2

    #@4f
    if-eqz v2, :cond_97

    #@51
    .line 465
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GSMPhone$3;->this$0:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@53
    invoke-static {v2}, Lcom/android/internal/telephony/gsm/GSMPhone;->access$100(Lcom/android/internal/telephony/gsm/GSMPhone;)Landroid/content/Context;

    #@56
    move-result-object v2

    #@57
    const-string v3, "uicc_csim"

    #@59
    invoke-static {v2, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@5c
    move-result v2

    #@5d
    if-nez v2, :cond_97

    #@5f
    .line 467
    :try_start_5f
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@62
    move-result-object v2

    #@63
    invoke-virtual {v2}, Landroid/telephony/MSimTelephonyManager;->isMultiSimEnabled()Z

    #@66
    move-result v2

    #@67
    if-ne v2, v7, :cond_ed

    #@69
    .line 468
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GSMPhone$3;->this$0:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@6b
    invoke-static {v2}, Lcom/android/internal/telephony/gsm/GSMPhone;->access$200(Lcom/android/internal/telephony/gsm/GSMPhone;)Landroid/content/Context;

    #@6e
    move-result-object v2

    #@6f
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GSMPhone$3;->this$0:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@71
    invoke-static {v3}, Lcom/android/internal/telephony/gsm/GSMPhone;->access$300(Lcom/android/internal/telephony/gsm/GSMPhone;)Landroid/content/Context;

    #@74
    move-result-object v3

    #@75
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@78
    move-result-object v3

    #@79
    sget-object v4, Lcom/android/internal/telephony/uicc/SIMRecords;->ALL_ICC_DS_URI:Landroid/net/Uri;

    #@7b
    const/4 v5, 0x0

    #@7c
    const/4 v6, 0x0

    #@7d
    invoke-static {v2, v3, v4, v5, v6}, Landroid/database/sqlite/SqliteWrapper;->delete(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@80
    .line 473
    :goto_80
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GSMPhone$3;->this$0:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@82
    invoke-static {v2}, Lcom/android/internal/telephony/gsm/GSMPhone;->access$600(Lcom/android/internal/telephony/gsm/GSMPhone;)Landroid/content/Context;

    #@85
    move-result-object v2

    #@86
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GSMPhone$3;->this$0:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@88
    invoke-static {v3}, Lcom/android/internal/telephony/gsm/GSMPhone;->access$700(Lcom/android/internal/telephony/gsm/GSMPhone;)Landroid/content/Context;

    #@8b
    move-result-object v3

    #@8c
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@8f
    move-result-object v3

    #@90
    sget-object v4, Lcom/android/internal/telephony/uicc/SIMRecords;->SMS_CONCAT_URI:Landroid/net/Uri;

    #@92
    const/4 v5, 0x0

    #@93
    const/4 v6, 0x0

    #@94
    invoke-static {v2, v3, v4, v5, v6}, Landroid/database/sqlite/SqliteWrapper;->delete(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_97
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_5f .. :try_end_97} :catch_10d

    #@97
    .line 483
    :cond_97
    :goto_97
    const-string v2, "ABSENT"

    #@99
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9c
    move-result v2

    #@9d
    if-eqz v2, :cond_ec

    #@9f
    .line 485
    const-string v2, "GSM"

    #@a1
    const-string v3, "[GSMPhone] SimStateReceiver - ICC ABSENT"

    #@a3
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a6
    .line 487
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GSMPhone$3;->this$0:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@a8
    invoke-static {v2}, Lcom/android/internal/telephony/gsm/GSMPhone;->access$800(Lcom/android/internal/telephony/gsm/GSMPhone;)Landroid/content/Context;

    #@ab
    move-result-object v2

    #@ac
    const-string v3, "seperate_processing_sms_uicc"

    #@ae
    invoke-static {v2, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@b1
    move-result v2

    #@b2
    if-eqz v2, :cond_ec

    #@b4
    .line 489
    :try_start_b4
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@b7
    move-result-object v2

    #@b8
    invoke-virtual {v2}, Landroid/telephony/MSimTelephonyManager;->isMultiSimEnabled()Z

    #@bb
    move-result v2

    #@bc
    if-ne v2, v7, :cond_128

    #@be
    .line 490
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GSMPhone$3;->this$0:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@c0
    invoke-static {v2}, Lcom/android/internal/telephony/gsm/GSMPhone;->access$900(Lcom/android/internal/telephony/gsm/GSMPhone;)Landroid/content/Context;

    #@c3
    move-result-object v2

    #@c4
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GSMPhone$3;->this$0:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@c6
    invoke-static {v3}, Lcom/android/internal/telephony/gsm/GSMPhone;->access$1000(Lcom/android/internal/telephony/gsm/GSMPhone;)Landroid/content/Context;

    #@c9
    move-result-object v3

    #@ca
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@cd
    move-result-object v3

    #@ce
    sget-object v4, Lcom/android/internal/telephony/uicc/SIMRecords;->ALL_ICC_DS_URI:Landroid/net/Uri;

    #@d0
    const/4 v5, 0x0

    #@d1
    const/4 v6, 0x0

    #@d2
    invoke-static {v2, v3, v4, v5, v6}, Landroid/database/sqlite/SqliteWrapper;->delete(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@d5
    .line 495
    :goto_d5
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GSMPhone$3;->this$0:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@d7
    invoke-static {v2}, Lcom/android/internal/telephony/gsm/GSMPhone;->access$1300(Lcom/android/internal/telephony/gsm/GSMPhone;)Landroid/content/Context;

    #@da
    move-result-object v2

    #@db
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GSMPhone$3;->this$0:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@dd
    invoke-static {v3}, Lcom/android/internal/telephony/gsm/GSMPhone;->access$1400(Lcom/android/internal/telephony/gsm/GSMPhone;)Landroid/content/Context;

    #@e0
    move-result-object v3

    #@e1
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@e4
    move-result-object v3

    #@e5
    sget-object v4, Lcom/android/internal/telephony/uicc/SIMRecords;->SMS_CONCAT_URI:Landroid/net/Uri;

    #@e7
    const/4 v5, 0x0

    #@e8
    const/4 v6, 0x0

    #@e9
    invoke-static {v2, v3, v4, v5, v6}, Landroid/database/sqlite/SqliteWrapper;->delete(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_ec
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_b4 .. :try_end_ec} :catch_147

    #@ec
    .line 503
    :cond_ec
    :goto_ec
    return-void

    #@ed
    .line 470
    :cond_ed
    :try_start_ed
    const-string v2, "GSM"

    #@ef
    const-string v3, "[GSMPhone] SimStateReceiver - Delete SIMRecords.ICC_URI"

    #@f1
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f4
    .line 471
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GSMPhone$3;->this$0:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@f6
    invoke-static {v2}, Lcom/android/internal/telephony/gsm/GSMPhone;->access$400(Lcom/android/internal/telephony/gsm/GSMPhone;)Landroid/content/Context;

    #@f9
    move-result-object v2

    #@fa
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GSMPhone$3;->this$0:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@fc
    invoke-static {v3}, Lcom/android/internal/telephony/gsm/GSMPhone;->access$500(Lcom/android/internal/telephony/gsm/GSMPhone;)Landroid/content/Context;

    #@ff
    move-result-object v3

    #@100
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@103
    move-result-object v3

    #@104
    sget-object v4, Lcom/android/internal/telephony/uicc/SIMRecords;->ICC_URI:Landroid/net/Uri;

    #@106
    const/4 v5, 0x0

    #@107
    const/4 v6, 0x0

    #@108
    invoke-static {v2, v3, v4, v5, v6}, Landroid/database/sqlite/SqliteWrapper;->delete(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_10b
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_ed .. :try_end_10b} :catch_10d

    #@10b
    goto/16 :goto_80

    #@10d
    .line 475
    :catch_10d
    move-exception v0

    #@10e
    .line 476
    .local v0, e:Landroid/database/sqlite/SQLiteException;
    const-string v2, "GSM"

    #@110
    new-instance v3, Ljava/lang/StringBuilder;

    #@112
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@115
    const-string v4, "[TEL-SMS] sql exception -"

    #@117
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11a
    move-result-object v3

    #@11b
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11e
    move-result-object v3

    #@11f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@122
    move-result-object v3

    #@123
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@126
    goto/16 :goto_97

    #@128
    .line 492
    .end local v0           #e:Landroid/database/sqlite/SQLiteException;
    :cond_128
    :try_start_128
    const-string v2, "GSM"

    #@12a
    const-string v3, "[GSMPhone] SimStateReceiver - Delete SIMRecords.ICC_URI"

    #@12c
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@12f
    .line 493
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/GSMPhone$3;->this$0:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@131
    invoke-static {v2}, Lcom/android/internal/telephony/gsm/GSMPhone;->access$1100(Lcom/android/internal/telephony/gsm/GSMPhone;)Landroid/content/Context;

    #@134
    move-result-object v2

    #@135
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/GSMPhone$3;->this$0:Lcom/android/internal/telephony/gsm/GSMPhone;

    #@137
    invoke-static {v3}, Lcom/android/internal/telephony/gsm/GSMPhone;->access$1200(Lcom/android/internal/telephony/gsm/GSMPhone;)Landroid/content/Context;

    #@13a
    move-result-object v3

    #@13b
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@13e
    move-result-object v3

    #@13f
    sget-object v4, Lcom/android/internal/telephony/uicc/SIMRecords;->ICC_URI:Landroid/net/Uri;

    #@141
    const/4 v5, 0x0

    #@142
    const/4 v6, 0x0

    #@143
    invoke-static {v2, v3, v4, v5, v6}, Landroid/database/sqlite/SqliteWrapper;->delete(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_146
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_128 .. :try_end_146} :catch_147

    #@146
    goto :goto_d5

    #@147
    .line 497
    :catch_147
    move-exception v0

    #@148
    .line 498
    .restart local v0       #e:Landroid/database/sqlite/SQLiteException;
    const-string v2, "GSM"

    #@14a
    new-instance v3, Ljava/lang/StringBuilder;

    #@14c
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@14f
    const-string v4, "[TEL-SMS] sql exception -"

    #@151
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@154
    move-result-object v3

    #@155
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@158
    move-result-object v3

    #@159
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15c
    move-result-object v3

    #@15d
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@160
    goto :goto_ec
.end method
