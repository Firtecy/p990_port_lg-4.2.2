.class public final enum Lcom/android/internal/telephony/cat/TextColor;
.super Ljava/lang/Enum;
.source "TextColor.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/internal/telephony/cat/TextColor;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/internal/telephony/cat/TextColor;

.field public static final enum BLACK:Lcom/android/internal/telephony/cat/TextColor;

.field public static final enum BRIGHT_BLUE:Lcom/android/internal/telephony/cat/TextColor;

.field public static final enum BRIGHT_CYAN:Lcom/android/internal/telephony/cat/TextColor;

.field public static final enum BRIGHT_GREEN:Lcom/android/internal/telephony/cat/TextColor;

.field public static final enum BRIGHT_MAGENTA:Lcom/android/internal/telephony/cat/TextColor;

.field public static final enum BRIGHT_RED:Lcom/android/internal/telephony/cat/TextColor;

.field public static final enum BRIGHT_YELLOW:Lcom/android/internal/telephony/cat/TextColor;

.field public static final enum DARK_BLUE:Lcom/android/internal/telephony/cat/TextColor;

.field public static final enum DARK_CYAN:Lcom/android/internal/telephony/cat/TextColor;

.field public static final enum DARK_GRAY:Lcom/android/internal/telephony/cat/TextColor;

.field public static final enum DARK_GREEN:Lcom/android/internal/telephony/cat/TextColor;

.field public static final enum DARK_MAGENTA:Lcom/android/internal/telephony/cat/TextColor;

.field public static final enum DARK_RED:Lcom/android/internal/telephony/cat/TextColor;

.field public static final enum DARK_YELLOW:Lcom/android/internal/telephony/cat/TextColor;

.field public static final enum GRAY:Lcom/android/internal/telephony/cat/TextColor;

.field public static final enum WHITE:Lcom/android/internal/telephony/cat/TextColor;


# instance fields
.field private mValue:I


# direct methods
.method static constructor <clinit>()V
    .registers 9

    #@0
    .prologue
    const/4 v8, 0x4

    #@1
    const/4 v7, 0x3

    #@2
    const/4 v6, 0x2

    #@3
    const/4 v5, 0x1

    #@4
    const/4 v4, 0x0

    #@5
    .line 26
    new-instance v0, Lcom/android/internal/telephony/cat/TextColor;

    #@7
    const-string v1, "BLACK"

    #@9
    invoke-direct {v0, v1, v4, v4}, Lcom/android/internal/telephony/cat/TextColor;-><init>(Ljava/lang/String;II)V

    #@c
    sput-object v0, Lcom/android/internal/telephony/cat/TextColor;->BLACK:Lcom/android/internal/telephony/cat/TextColor;

    #@e
    .line 27
    new-instance v0, Lcom/android/internal/telephony/cat/TextColor;

    #@10
    const-string v1, "DARK_GRAY"

    #@12
    invoke-direct {v0, v1, v5, v5}, Lcom/android/internal/telephony/cat/TextColor;-><init>(Ljava/lang/String;II)V

    #@15
    sput-object v0, Lcom/android/internal/telephony/cat/TextColor;->DARK_GRAY:Lcom/android/internal/telephony/cat/TextColor;

    #@17
    .line 28
    new-instance v0, Lcom/android/internal/telephony/cat/TextColor;

    #@19
    const-string v1, "DARK_RED"

    #@1b
    invoke-direct {v0, v1, v6, v6}, Lcom/android/internal/telephony/cat/TextColor;-><init>(Ljava/lang/String;II)V

    #@1e
    sput-object v0, Lcom/android/internal/telephony/cat/TextColor;->DARK_RED:Lcom/android/internal/telephony/cat/TextColor;

    #@20
    .line 29
    new-instance v0, Lcom/android/internal/telephony/cat/TextColor;

    #@22
    const-string v1, "DARK_YELLOW"

    #@24
    invoke-direct {v0, v1, v7, v7}, Lcom/android/internal/telephony/cat/TextColor;-><init>(Ljava/lang/String;II)V

    #@27
    sput-object v0, Lcom/android/internal/telephony/cat/TextColor;->DARK_YELLOW:Lcom/android/internal/telephony/cat/TextColor;

    #@29
    .line 30
    new-instance v0, Lcom/android/internal/telephony/cat/TextColor;

    #@2b
    const-string v1, "DARK_GREEN"

    #@2d
    invoke-direct {v0, v1, v8, v8}, Lcom/android/internal/telephony/cat/TextColor;-><init>(Ljava/lang/String;II)V

    #@30
    sput-object v0, Lcom/android/internal/telephony/cat/TextColor;->DARK_GREEN:Lcom/android/internal/telephony/cat/TextColor;

    #@32
    .line 31
    new-instance v0, Lcom/android/internal/telephony/cat/TextColor;

    #@34
    const-string v1, "DARK_CYAN"

    #@36
    const/4 v2, 0x5

    #@37
    const/4 v3, 0x5

    #@38
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/TextColor;-><init>(Ljava/lang/String;II)V

    #@3b
    sput-object v0, Lcom/android/internal/telephony/cat/TextColor;->DARK_CYAN:Lcom/android/internal/telephony/cat/TextColor;

    #@3d
    .line 32
    new-instance v0, Lcom/android/internal/telephony/cat/TextColor;

    #@3f
    const-string v1, "DARK_BLUE"

    #@41
    const/4 v2, 0x6

    #@42
    const/4 v3, 0x6

    #@43
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/TextColor;-><init>(Ljava/lang/String;II)V

    #@46
    sput-object v0, Lcom/android/internal/telephony/cat/TextColor;->DARK_BLUE:Lcom/android/internal/telephony/cat/TextColor;

    #@48
    .line 33
    new-instance v0, Lcom/android/internal/telephony/cat/TextColor;

    #@4a
    const-string v1, "DARK_MAGENTA"

    #@4c
    const/4 v2, 0x7

    #@4d
    const/4 v3, 0x7

    #@4e
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/TextColor;-><init>(Ljava/lang/String;II)V

    #@51
    sput-object v0, Lcom/android/internal/telephony/cat/TextColor;->DARK_MAGENTA:Lcom/android/internal/telephony/cat/TextColor;

    #@53
    .line 34
    new-instance v0, Lcom/android/internal/telephony/cat/TextColor;

    #@55
    const-string v1, "GRAY"

    #@57
    const/16 v2, 0x8

    #@59
    const/16 v3, 0x8

    #@5b
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/TextColor;-><init>(Ljava/lang/String;II)V

    #@5e
    sput-object v0, Lcom/android/internal/telephony/cat/TextColor;->GRAY:Lcom/android/internal/telephony/cat/TextColor;

    #@60
    .line 35
    new-instance v0, Lcom/android/internal/telephony/cat/TextColor;

    #@62
    const-string v1, "WHITE"

    #@64
    const/16 v2, 0x9

    #@66
    const/16 v3, 0x9

    #@68
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/TextColor;-><init>(Ljava/lang/String;II)V

    #@6b
    sput-object v0, Lcom/android/internal/telephony/cat/TextColor;->WHITE:Lcom/android/internal/telephony/cat/TextColor;

    #@6d
    .line 36
    new-instance v0, Lcom/android/internal/telephony/cat/TextColor;

    #@6f
    const-string v1, "BRIGHT_RED"

    #@71
    const/16 v2, 0xa

    #@73
    const/16 v3, 0xa

    #@75
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/TextColor;-><init>(Ljava/lang/String;II)V

    #@78
    sput-object v0, Lcom/android/internal/telephony/cat/TextColor;->BRIGHT_RED:Lcom/android/internal/telephony/cat/TextColor;

    #@7a
    .line 37
    new-instance v0, Lcom/android/internal/telephony/cat/TextColor;

    #@7c
    const-string v1, "BRIGHT_YELLOW"

    #@7e
    const/16 v2, 0xb

    #@80
    const/16 v3, 0xb

    #@82
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/TextColor;-><init>(Ljava/lang/String;II)V

    #@85
    sput-object v0, Lcom/android/internal/telephony/cat/TextColor;->BRIGHT_YELLOW:Lcom/android/internal/telephony/cat/TextColor;

    #@87
    .line 38
    new-instance v0, Lcom/android/internal/telephony/cat/TextColor;

    #@89
    const-string v1, "BRIGHT_GREEN"

    #@8b
    const/16 v2, 0xc

    #@8d
    const/16 v3, 0xc

    #@8f
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/TextColor;-><init>(Ljava/lang/String;II)V

    #@92
    sput-object v0, Lcom/android/internal/telephony/cat/TextColor;->BRIGHT_GREEN:Lcom/android/internal/telephony/cat/TextColor;

    #@94
    .line 39
    new-instance v0, Lcom/android/internal/telephony/cat/TextColor;

    #@96
    const-string v1, "BRIGHT_CYAN"

    #@98
    const/16 v2, 0xd

    #@9a
    const/16 v3, 0xd

    #@9c
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/TextColor;-><init>(Ljava/lang/String;II)V

    #@9f
    sput-object v0, Lcom/android/internal/telephony/cat/TextColor;->BRIGHT_CYAN:Lcom/android/internal/telephony/cat/TextColor;

    #@a1
    .line 40
    new-instance v0, Lcom/android/internal/telephony/cat/TextColor;

    #@a3
    const-string v1, "BRIGHT_BLUE"

    #@a5
    const/16 v2, 0xe

    #@a7
    const/16 v3, 0xe

    #@a9
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/TextColor;-><init>(Ljava/lang/String;II)V

    #@ac
    sput-object v0, Lcom/android/internal/telephony/cat/TextColor;->BRIGHT_BLUE:Lcom/android/internal/telephony/cat/TextColor;

    #@ae
    .line 41
    new-instance v0, Lcom/android/internal/telephony/cat/TextColor;

    #@b0
    const-string v1, "BRIGHT_MAGENTA"

    #@b2
    const/16 v2, 0xf

    #@b4
    const/16 v3, 0xf

    #@b6
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cat/TextColor;-><init>(Ljava/lang/String;II)V

    #@b9
    sput-object v0, Lcom/android/internal/telephony/cat/TextColor;->BRIGHT_MAGENTA:Lcom/android/internal/telephony/cat/TextColor;

    #@bb
    .line 25
    const/16 v0, 0x10

    #@bd
    new-array v0, v0, [Lcom/android/internal/telephony/cat/TextColor;

    #@bf
    sget-object v1, Lcom/android/internal/telephony/cat/TextColor;->BLACK:Lcom/android/internal/telephony/cat/TextColor;

    #@c1
    aput-object v1, v0, v4

    #@c3
    sget-object v1, Lcom/android/internal/telephony/cat/TextColor;->DARK_GRAY:Lcom/android/internal/telephony/cat/TextColor;

    #@c5
    aput-object v1, v0, v5

    #@c7
    sget-object v1, Lcom/android/internal/telephony/cat/TextColor;->DARK_RED:Lcom/android/internal/telephony/cat/TextColor;

    #@c9
    aput-object v1, v0, v6

    #@cb
    sget-object v1, Lcom/android/internal/telephony/cat/TextColor;->DARK_YELLOW:Lcom/android/internal/telephony/cat/TextColor;

    #@cd
    aput-object v1, v0, v7

    #@cf
    sget-object v1, Lcom/android/internal/telephony/cat/TextColor;->DARK_GREEN:Lcom/android/internal/telephony/cat/TextColor;

    #@d1
    aput-object v1, v0, v8

    #@d3
    const/4 v1, 0x5

    #@d4
    sget-object v2, Lcom/android/internal/telephony/cat/TextColor;->DARK_CYAN:Lcom/android/internal/telephony/cat/TextColor;

    #@d6
    aput-object v2, v0, v1

    #@d8
    const/4 v1, 0x6

    #@d9
    sget-object v2, Lcom/android/internal/telephony/cat/TextColor;->DARK_BLUE:Lcom/android/internal/telephony/cat/TextColor;

    #@db
    aput-object v2, v0, v1

    #@dd
    const/4 v1, 0x7

    #@de
    sget-object v2, Lcom/android/internal/telephony/cat/TextColor;->DARK_MAGENTA:Lcom/android/internal/telephony/cat/TextColor;

    #@e0
    aput-object v2, v0, v1

    #@e2
    const/16 v1, 0x8

    #@e4
    sget-object v2, Lcom/android/internal/telephony/cat/TextColor;->GRAY:Lcom/android/internal/telephony/cat/TextColor;

    #@e6
    aput-object v2, v0, v1

    #@e8
    const/16 v1, 0x9

    #@ea
    sget-object v2, Lcom/android/internal/telephony/cat/TextColor;->WHITE:Lcom/android/internal/telephony/cat/TextColor;

    #@ec
    aput-object v2, v0, v1

    #@ee
    const/16 v1, 0xa

    #@f0
    sget-object v2, Lcom/android/internal/telephony/cat/TextColor;->BRIGHT_RED:Lcom/android/internal/telephony/cat/TextColor;

    #@f2
    aput-object v2, v0, v1

    #@f4
    const/16 v1, 0xb

    #@f6
    sget-object v2, Lcom/android/internal/telephony/cat/TextColor;->BRIGHT_YELLOW:Lcom/android/internal/telephony/cat/TextColor;

    #@f8
    aput-object v2, v0, v1

    #@fa
    const/16 v1, 0xc

    #@fc
    sget-object v2, Lcom/android/internal/telephony/cat/TextColor;->BRIGHT_GREEN:Lcom/android/internal/telephony/cat/TextColor;

    #@fe
    aput-object v2, v0, v1

    #@100
    const/16 v1, 0xd

    #@102
    sget-object v2, Lcom/android/internal/telephony/cat/TextColor;->BRIGHT_CYAN:Lcom/android/internal/telephony/cat/TextColor;

    #@104
    aput-object v2, v0, v1

    #@106
    const/16 v1, 0xe

    #@108
    sget-object v2, Lcom/android/internal/telephony/cat/TextColor;->BRIGHT_BLUE:Lcom/android/internal/telephony/cat/TextColor;

    #@10a
    aput-object v2, v0, v1

    #@10c
    const/16 v1, 0xf

    #@10e
    sget-object v2, Lcom/android/internal/telephony/cat/TextColor;->BRIGHT_MAGENTA:Lcom/android/internal/telephony/cat/TextColor;

    #@110
    aput-object v2, v0, v1

    #@112
    sput-object v0, Lcom/android/internal/telephony/cat/TextColor;->$VALUES:[Lcom/android/internal/telephony/cat/TextColor;

    #@114
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .registers 4
    .parameter
    .parameter
    .parameter "value"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    .line 46
    iput p3, p0, Lcom/android/internal/telephony/cat/TextColor;->mValue:I

    #@5
    .line 47
    return-void
.end method

.method public static fromInt(I)Lcom/android/internal/telephony/cat/TextColor;
    .registers 6
    .parameter "value"

    #@0
    .prologue
    .line 56
    invoke-static {}, Lcom/android/internal/telephony/cat/TextColor;->values()[Lcom/android/internal/telephony/cat/TextColor;

    #@3
    move-result-object v0

    #@4
    .local v0, arr$:[Lcom/android/internal/telephony/cat/TextColor;
    array-length v3, v0

    #@5
    .local v3, len$:I
    const/4 v2, 0x0

    #@6
    .local v2, i$:I
    :goto_6
    if-ge v2, v3, :cond_12

    #@8
    aget-object v1, v0, v2

    #@a
    .line 57
    .local v1, e:Lcom/android/internal/telephony/cat/TextColor;
    iget v4, v1, Lcom/android/internal/telephony/cat/TextColor;->mValue:I

    #@c
    if-ne v4, p0, :cond_f

    #@e
    .line 61
    .end local v1           #e:Lcom/android/internal/telephony/cat/TextColor;
    :goto_e
    return-object v1

    #@f
    .line 56
    .restart local v1       #e:Lcom/android/internal/telephony/cat/TextColor;
    :cond_f
    add-int/lit8 v2, v2, 0x1

    #@11
    goto :goto_6

    #@12
    .line 61
    .end local v1           #e:Lcom/android/internal/telephony/cat/TextColor;
    :cond_12
    const/4 v1, 0x0

    #@13
    goto :goto_e
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/internal/telephony/cat/TextColor;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 25
    const-class v0, Lcom/android/internal/telephony/cat/TextColor;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/telephony/cat/TextColor;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/android/internal/telephony/cat/TextColor;
    .registers 1

    #@0
    .prologue
    .line 25
    sget-object v0, Lcom/android/internal/telephony/cat/TextColor;->$VALUES:[Lcom/android/internal/telephony/cat/TextColor;

    #@2
    invoke-virtual {v0}, [Lcom/android/internal/telephony/cat/TextColor;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/android/internal/telephony/cat/TextColor;

    #@8
    return-object v0
.end method
