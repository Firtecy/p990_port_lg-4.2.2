.class public Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;
.super Ljava/lang/Object;
.source "LGDataRecovery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/LGDataRecovery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "TrafficStat"
.end annotation


# instance fields
.field public rxCnt:J

.field public systime:J

.field final synthetic this$0:Lcom/android/internal/telephony/LGDataRecovery;

.field public txCnt:J


# direct methods
.method public constructor <init>(Lcom/android/internal/telephony/LGDataRecovery;)V
    .registers 4
    .parameter

    #@0
    .prologue
    const-wide/16 v0, 0x0

    #@2
    .line 931
    iput-object p1, p0, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->this$0:Lcom/android/internal/telephony/LGDataRecovery;

    #@4
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@7
    .line 932
    iput-wide v0, p0, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->txCnt:J

    #@9
    .line 933
    iput-wide v0, p0, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->rxCnt:J

    #@b
    .line 934
    iput-wide v0, p0, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->systime:J

    #@d
    .line 935
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    .line 939
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "txCnt= "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget-wide v1, p0, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->txCnt:J

    #@d
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, " rxCnt= "

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget-wide v1, p0, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->rxCnt:J

    #@19
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, " SystemTime= "

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    iget-wide v1, p0, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->systime:J

    #@25
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    const-string v1, "("

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    const-string v1, "yyyy-MM-dd kk:mm:ss"

    #@31
    new-instance v2, Ljava/util/Date;

    #@33
    iget-wide v3, p0, Lcom/android/internal/telephony/LGDataRecovery$TrafficStat;->systime:J

    #@35
    invoke-direct {v2, v3, v4}, Ljava/util/Date;-><init>(J)V

    #@38
    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    #@3b
    move-result-wide v2

    #@3c
    invoke-static {v1, v2, v3}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    #@3f
    move-result-object v1

    #@40
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v0

    #@44
    const-string v1, ")"

    #@46
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v0

    #@4a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v0

    #@4e
    return-object v0
.end method
