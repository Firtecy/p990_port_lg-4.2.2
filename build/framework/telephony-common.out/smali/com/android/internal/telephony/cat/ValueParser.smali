.class abstract Lcom/android/internal/telephony/cat/ValueParser;
.super Ljava/lang/Object;
.source "ValueParser.java"


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 28
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method static retrieveAlphaId(Lcom/android/internal/telephony/cat/ComprehensionTlv;)Ljava/lang/String;
    .registers 10
    .parameter "ctlv"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/cat/ResultException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 277
    if-eqz p0, :cond_27

    #@3
    .line 278
    invoke-virtual {p0}, Lcom/android/internal/telephony/cat/ComprehensionTlv;->getRawValue()[B

    #@6
    move-result-object v3

    #@7
    .line 279
    .local v3, rawValue:[B
    invoke-virtual {p0}, Lcom/android/internal/telephony/cat/ComprehensionTlv;->getValueIndex()I

    #@a
    move-result v4

    #@b
    .line 280
    .local v4, valueIndex:I
    invoke-virtual {p0}, Lcom/android/internal/telephony/cat/ComprehensionTlv;->getLength()I

    #@e
    move-result v2

    #@f
    .line 281
    .local v2, length:I
    if-eqz v2, :cond_1f

    #@11
    .line 283
    :try_start_11
    invoke-static {v3, v4, v2}, Lcom/android/internal/telephony/uicc/IccUtils;->adnStringFieldToString([BII)Ljava/lang/String;
    :try_end_14
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_11 .. :try_end_14} :catch_16

    #@14
    move-result-object v5

    #@15
    .line 316
    .end local v2           #length:I
    .end local v3           #rawValue:[B
    .end local v4           #valueIndex:I
    :cond_15
    :goto_15
    return-object v5

    #@16
    .line 285
    .restart local v2       #length:I
    .restart local v3       #rawValue:[B
    .restart local v4       #valueIndex:I
    :catch_16
    move-exception v1

    #@17
    .line 286
    .local v1, e:Ljava/lang/IndexOutOfBoundsException;
    new-instance v5, Lcom/android/internal/telephony/cat/ResultException;

    #@19
    sget-object v6, Lcom/android/internal/telephony/cat/ResultCode;->CMD_DATA_NOT_UNDERSTOOD:Lcom/android/internal/telephony/cat/ResultCode;

    #@1b
    invoke-direct {v5, v6}, Lcom/android/internal/telephony/cat/ResultException;-><init>(Lcom/android/internal/telephony/cat/ResultCode;)V

    #@1e
    throw v5

    #@1f
    .line 298
    .end local v1           #e:Ljava/lang/IndexOutOfBoundsException;
    :cond_1f
    const-string v6, "ValueParser"

    #@21
    const-string v7, "NULL ALPHA id (length = 0) "

    #@23
    invoke-static {v6, v7}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@26
    goto :goto_15

    #@27
    .line 312
    .end local v2           #length:I
    .end local v3           #rawValue:[B
    .end local v4           #valueIndex:I
    :cond_27
    const-string v6, "persist.atel.noalpha.usrcnf"

    #@29
    const/4 v7, 0x0

    #@2a
    invoke-static {v6, v7}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@2d
    move-result v0

    #@2e
    .line 314
    .local v0, alphaUsrCnf:Z
    const-string v6, "ValueParser"

    #@30
    new-instance v7, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    const-string v8, "NO ALPHA id, alphaUsrCnf: "

    #@37
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v7

    #@3b
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v7

    #@3f
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v7

    #@43
    invoke-static {v6, v7}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@46
    .line 316
    if-eqz v0, :cond_15

    #@48
    const-string v5, "Default Message"

    #@4a
    goto :goto_15
.end method

.method static retrieveCommandDetails(Lcom/android/internal/telephony/cat/ComprehensionTlv;)Lcom/android/internal/telephony/cat/CommandDetails;
    .registers 7
    .parameter "ctlv"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/cat/ResultException;
        }
    .end annotation

    #@0
    .prologue
    .line 41
    new-instance v0, Lcom/android/internal/telephony/cat/CommandDetails;

    #@2
    invoke-direct {v0}, Lcom/android/internal/telephony/cat/CommandDetails;-><init>()V

    #@5
    .line 42
    .local v0, cmdDet:Lcom/android/internal/telephony/cat/CommandDetails;
    invoke-virtual {p0}, Lcom/android/internal/telephony/cat/ComprehensionTlv;->getRawValue()[B

    #@8
    move-result-object v2

    #@9
    .line 43
    .local v2, rawValue:[B
    invoke-virtual {p0}, Lcom/android/internal/telephony/cat/ComprehensionTlv;->getValueIndex()I

    #@c
    move-result v3

    #@d
    .line 45
    .local v3, valueIndex:I
    :try_start_d
    invoke-virtual {p0}, Lcom/android/internal/telephony/cat/ComprehensionTlv;->isComprehensionRequired()Z

    #@10
    move-result v4

    #@11
    iput-boolean v4, v0, Lcom/android/internal/telephony/cat/CommandDetails;->compRequired:Z

    #@13
    .line 46
    aget-byte v4, v2, v3

    #@15
    and-int/lit16 v4, v4, 0xff

    #@17
    iput v4, v0, Lcom/android/internal/telephony/cat/CommandDetails;->commandNumber:I

    #@19
    .line 47
    add-int/lit8 v4, v3, 0x1

    #@1b
    aget-byte v4, v2, v4

    #@1d
    and-int/lit16 v4, v4, 0xff

    #@1f
    iput v4, v0, Lcom/android/internal/telephony/cat/CommandDetails;->typeOfCommand:I

    #@21
    .line 48
    add-int/lit8 v4, v3, 0x2

    #@23
    aget-byte v4, v2, v4

    #@25
    and-int/lit16 v4, v4, 0xff

    #@27
    iput v4, v0, Lcom/android/internal/telephony/cat/CommandDetails;->commandQualifier:I
    :try_end_29
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_d .. :try_end_29} :catch_2a

    #@29
    .line 49
    return-object v0

    #@2a
    .line 50
    :catch_2a
    move-exception v1

    #@2b
    .line 51
    .local v1, e:Ljava/lang/IndexOutOfBoundsException;
    new-instance v4, Lcom/android/internal/telephony/cat/ResultException;

    #@2d
    sget-object v5, Lcom/android/internal/telephony/cat/ResultCode;->CMD_DATA_NOT_UNDERSTOOD:Lcom/android/internal/telephony/cat/ResultCode;

    #@2f
    invoke-direct {v4, v5}, Lcom/android/internal/telephony/cat/ResultException;-><init>(Lcom/android/internal/telephony/cat/ResultCode;)V

    #@32
    throw v4
.end method

.method static retrieveDeviceIdentities(Lcom/android/internal/telephony/cat/ComprehensionTlv;)Lcom/android/internal/telephony/cat/DeviceIdentities;
    .registers 7
    .parameter "ctlv"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/cat/ResultException;
        }
    .end annotation

    #@0
    .prologue
    .line 66
    new-instance v0, Lcom/android/internal/telephony/cat/DeviceIdentities;

    #@2
    invoke-direct {v0}, Lcom/android/internal/telephony/cat/DeviceIdentities;-><init>()V

    #@5
    .line 67
    .local v0, devIds:Lcom/android/internal/telephony/cat/DeviceIdentities;
    invoke-virtual {p0}, Lcom/android/internal/telephony/cat/ComprehensionTlv;->getRawValue()[B

    #@8
    move-result-object v2

    #@9
    .line 68
    .local v2, rawValue:[B
    invoke-virtual {p0}, Lcom/android/internal/telephony/cat/ComprehensionTlv;->getValueIndex()I

    #@c
    move-result v3

    #@d
    .line 70
    .local v3, valueIndex:I
    :try_start_d
    aget-byte v4, v2, v3

    #@f
    and-int/lit16 v4, v4, 0xff

    #@11
    iput v4, v0, Lcom/android/internal/telephony/cat/DeviceIdentities;->sourceId:I

    #@13
    .line 71
    add-int/lit8 v4, v3, 0x1

    #@15
    aget-byte v4, v2, v4

    #@17
    and-int/lit16 v4, v4, 0xff

    #@19
    iput v4, v0, Lcom/android/internal/telephony/cat/DeviceIdentities;->destinationId:I
    :try_end_1b
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_d .. :try_end_1b} :catch_1c

    #@1b
    .line 72
    return-object v0

    #@1c
    .line 73
    :catch_1c
    move-exception v1

    #@1d
    .line 74
    .local v1, e:Ljava/lang/IndexOutOfBoundsException;
    new-instance v4, Lcom/android/internal/telephony/cat/ResultException;

    #@1f
    sget-object v5, Lcom/android/internal/telephony/cat/ResultCode;->REQUIRED_VALUES_MISSING:Lcom/android/internal/telephony/cat/ResultCode;

    #@21
    invoke-direct {v4, v5}, Lcom/android/internal/telephony/cat/ResultException;-><init>(Lcom/android/internal/telephony/cat/ResultCode;)V

    #@24
    throw v4
.end method

.method static retrieveDuration(Lcom/android/internal/telephony/cat/ComprehensionTlv;)Lcom/android/internal/telephony/cat/Duration;
    .registers 8
    .parameter "ctlv"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/cat/ResultException;
        }
    .end annotation

    #@0
    .prologue
    .line 87
    const/4 v2, 0x0

    #@1
    .line 88
    .local v2, timeInterval:I
    sget-object v3, Lcom/android/internal/telephony/cat/Duration$TimeUnit;->SECOND:Lcom/android/internal/telephony/cat/Duration$TimeUnit;

    #@3
    .line 90
    .local v3, timeUnit:Lcom/android/internal/telephony/cat/Duration$TimeUnit;
    invoke-virtual {p0}, Lcom/android/internal/telephony/cat/ComprehensionTlv;->getRawValue()[B

    #@6
    move-result-object v1

    #@7
    .line 91
    .local v1, rawValue:[B
    invoke-virtual {p0}, Lcom/android/internal/telephony/cat/ComprehensionTlv;->getValueIndex()I

    #@a
    move-result v4

    #@b
    .line 94
    .local v4, valueIndex:I
    :try_start_b
    invoke-static {}, Lcom/android/internal/telephony/cat/Duration$TimeUnit;->values()[Lcom/android/internal/telephony/cat/Duration$TimeUnit;

    #@e
    move-result-object v5

    #@f
    aget-byte v6, v1, v4

    #@11
    and-int/lit16 v6, v6, 0xff

    #@13
    aget-object v3, v5, v6

    #@15
    .line 95
    add-int/lit8 v5, v4, 0x1

    #@17
    aget-byte v5, v1, v5
    :try_end_19
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_b .. :try_end_19} :catch_21

    #@19
    and-int/lit16 v2, v5, 0xff

    #@1b
    .line 99
    new-instance v5, Lcom/android/internal/telephony/cat/Duration;

    #@1d
    invoke-direct {v5, v2, v3}, Lcom/android/internal/telephony/cat/Duration;-><init>(ILcom/android/internal/telephony/cat/Duration$TimeUnit;)V

    #@20
    return-object v5

    #@21
    .line 96
    :catch_21
    move-exception v0

    #@22
    .line 97
    .local v0, e:Ljava/lang/IndexOutOfBoundsException;
    new-instance v5, Lcom/android/internal/telephony/cat/ResultException;

    #@24
    sget-object v6, Lcom/android/internal/telephony/cat/ResultCode;->CMD_DATA_NOT_UNDERSTOOD:Lcom/android/internal/telephony/cat/ResultCode;

    #@26
    invoke-direct {v5, v6}, Lcom/android/internal/telephony/cat/ResultException;-><init>(Lcom/android/internal/telephony/cat/ResultCode;)V

    #@29
    throw v5
.end method

.method static retrieveIconId(Lcom/android/internal/telephony/cat/ComprehensionTlv;)Lcom/android/internal/telephony/cat/IconId;
    .registers 8
    .parameter "ctlv"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/cat/ResultException;
        }
    .end annotation

    #@0
    .prologue
    .line 162
    new-instance v1, Lcom/android/internal/telephony/cat/IconId;

    #@2
    invoke-direct {v1}, Lcom/android/internal/telephony/cat/IconId;-><init>()V

    #@5
    .line 164
    .local v1, id:Lcom/android/internal/telephony/cat/IconId;
    invoke-virtual {p0}, Lcom/android/internal/telephony/cat/ComprehensionTlv;->getRawValue()[B

    #@8
    move-result-object v2

    #@9
    .line 165
    .local v2, rawValue:[B
    invoke-virtual {p0}, Lcom/android/internal/telephony/cat/ComprehensionTlv;->getValueIndex()I

    #@c
    move-result v3

    #@d
    .line 167
    .local v3, valueIndex:I
    add-int/lit8 v4, v3, 0x1

    #@f
    .end local v3           #valueIndex:I
    .local v4, valueIndex:I
    :try_start_f
    aget-byte v5, v2, v3

    #@11
    and-int/lit16 v5, v5, 0xff

    #@13
    if-nez v5, :cond_1f

    #@15
    const/4 v5, 0x1

    #@16
    :goto_16
    iput-boolean v5, v1, Lcom/android/internal/telephony/cat/IconId;->selfExplanatory:Z

    #@18
    .line 168
    aget-byte v5, v2, v4

    #@1a
    and-int/lit16 v5, v5, 0xff

    #@1c
    iput v5, v1, Lcom/android/internal/telephony/cat/IconId;->recordNumber:I
    :try_end_1e
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_f .. :try_end_1e} :catch_21

    #@1e
    .line 173
    return-object v1

    #@1f
    .line 167
    :cond_1f
    const/4 v5, 0x0

    #@20
    goto :goto_16

    #@21
    .line 169
    :catch_21
    move-exception v0

    #@22
    .line 170
    .local v0, e:Ljava/lang/IndexOutOfBoundsException;
    new-instance v5, Lcom/android/internal/telephony/cat/ResultException;

    #@24
    sget-object v6, Lcom/android/internal/telephony/cat/ResultCode;->CMD_DATA_NOT_UNDERSTOOD:Lcom/android/internal/telephony/cat/ResultCode;

    #@26
    invoke-direct {v5, v6}, Lcom/android/internal/telephony/cat/ResultException;-><init>(Lcom/android/internal/telephony/cat/ResultCode;)V

    #@29
    throw v5
.end method

.method static retrieveItem(Lcom/android/internal/telephony/cat/ComprehensionTlv;)Lcom/android/internal/telephony/cat/Item;
    .registers 11
    .parameter "ctlv"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/cat/ResultException;
        }
    .end annotation

    #@0
    .prologue
    .line 110
    const/4 v2, 0x0

    #@1
    .line 112
    .local v2, item:Lcom/android/internal/telephony/cat/Item;
    invoke-virtual {p0}, Lcom/android/internal/telephony/cat/ComprehensionTlv;->getRawValue()[B

    #@4
    move-result-object v4

    #@5
    .line 113
    .local v4, rawValue:[B
    invoke-virtual {p0}, Lcom/android/internal/telephony/cat/ComprehensionTlv;->getValueIndex()I

    #@8
    move-result v7

    #@9
    .line 114
    .local v7, valueIndex:I
    invoke-virtual {p0}, Lcom/android/internal/telephony/cat/ComprehensionTlv;->getLength()I

    #@c
    move-result v3

    #@d
    .line 116
    .local v3, length:I
    if-eqz v3, :cond_20

    #@f
    .line 117
    add-int/lit8 v6, v3, -0x1

    #@11
    .line 120
    .local v6, textLen:I
    :try_start_11
    aget-byte v8, v4, v7

    #@13
    and-int/lit16 v1, v8, 0xff

    #@15
    .line 121
    .local v1, id:I
    add-int/lit8 v8, v7, 0x1

    #@17
    invoke-static {v4, v8, v6}, Lcom/android/internal/telephony/uicc/IccUtils;->adnStringFieldToString([BII)Ljava/lang/String;

    #@1a
    move-result-object v5

    #@1b
    .line 123
    .local v5, text:Ljava/lang/String;
    new-instance v2, Lcom/android/internal/telephony/cat/Item;

    #@1d
    .end local v2           #item:Lcom/android/internal/telephony/cat/Item;
    invoke-direct {v2, v1, v5}, Lcom/android/internal/telephony/cat/Item;-><init>(ILjava/lang/String;)V
    :try_end_20
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_11 .. :try_end_20} :catch_21

    #@20
    .line 129
    .end local v1           #id:I
    .end local v5           #text:Ljava/lang/String;
    .end local v6           #textLen:I
    .restart local v2       #item:Lcom/android/internal/telephony/cat/Item;
    :cond_20
    return-object v2

    #@21
    .line 124
    .end local v2           #item:Lcom/android/internal/telephony/cat/Item;
    .restart local v6       #textLen:I
    :catch_21
    move-exception v0

    #@22
    .line 125
    .local v0, e:Ljava/lang/IndexOutOfBoundsException;
    new-instance v8, Lcom/android/internal/telephony/cat/ResultException;

    #@24
    sget-object v9, Lcom/android/internal/telephony/cat/ResultCode;->CMD_DATA_NOT_UNDERSTOOD:Lcom/android/internal/telephony/cat/ResultCode;

    #@26
    invoke-direct {v8, v9}, Lcom/android/internal/telephony/cat/ResultException;-><init>(Lcom/android/internal/telephony/cat/ResultCode;)V

    #@29
    throw v8
.end method

.method static retrieveItemId(Lcom/android/internal/telephony/cat/ComprehensionTlv;)I
    .registers 7
    .parameter "ctlv"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/cat/ResultException;
        }
    .end annotation

    #@0
    .prologue
    .line 140
    const/4 v1, 0x0

    #@1
    .line 142
    .local v1, id:I
    invoke-virtual {p0}, Lcom/android/internal/telephony/cat/ComprehensionTlv;->getRawValue()[B

    #@4
    move-result-object v2

    #@5
    .line 143
    .local v2, rawValue:[B
    invoke-virtual {p0}, Lcom/android/internal/telephony/cat/ComprehensionTlv;->getValueIndex()I

    #@8
    move-result v3

    #@9
    .line 146
    .local v3, valueIndex:I
    :try_start_9
    aget-byte v4, v2, v3
    :try_end_b
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_9 .. :try_end_b} :catch_e

    #@b
    and-int/lit16 v1, v4, 0xff

    #@d
    .line 151
    return v1

    #@e
    .line 147
    :catch_e
    move-exception v0

    #@f
    .line 148
    .local v0, e:Ljava/lang/IndexOutOfBoundsException;
    new-instance v4, Lcom/android/internal/telephony/cat/ResultException;

    #@11
    sget-object v5, Lcom/android/internal/telephony/cat/ResultCode;->CMD_DATA_NOT_UNDERSTOOD:Lcom/android/internal/telephony/cat/ResultCode;

    #@13
    invoke-direct {v4, v5}, Lcom/android/internal/telephony/cat/ResultException;-><init>(Lcom/android/internal/telephony/cat/ResultCode;)V

    #@16
    throw v4
.end method

.method static retrieveItemsIconId(Lcom/android/internal/telephony/cat/ComprehensionTlv;)Lcom/android/internal/telephony/cat/ItemsIconId;
    .registers 11
    .parameter "ctlv"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/cat/ResultException;
        }
    .end annotation

    #@0
    .prologue
    .line 186
    const-string v8, "ValueParser"

    #@2
    const-string v9, "retrieveItemsIconId:"

    #@4
    invoke-static {v8, v9}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 187
    new-instance v1, Lcom/android/internal/telephony/cat/ItemsIconId;

    #@9
    invoke-direct {v1}, Lcom/android/internal/telephony/cat/ItemsIconId;-><init>()V

    #@c
    .line 189
    .local v1, id:Lcom/android/internal/telephony/cat/ItemsIconId;
    invoke-virtual {p0}, Lcom/android/internal/telephony/cat/ComprehensionTlv;->getRawValue()[B

    #@f
    move-result-object v5

    #@10
    .line 190
    .local v5, rawValue:[B
    invoke-virtual {p0}, Lcom/android/internal/telephony/cat/ComprehensionTlv;->getValueIndex()I

    #@13
    move-result v6

    #@14
    .line 191
    .local v6, valueIndex:I
    invoke-virtual {p0}, Lcom/android/internal/telephony/cat/ComprehensionTlv;->getLength()I

    #@17
    move-result v8

    #@18
    add-int/lit8 v4, v8, -0x1

    #@1a
    .line 192
    .local v4, numOfItems:I
    new-array v8, v4, [I

    #@1c
    iput-object v8, v1, Lcom/android/internal/telephony/cat/ItemsIconId;->recordNumbers:[I

    #@1e
    .line 196
    add-int/lit8 v7, v6, 0x1

    #@20
    .end local v6           #valueIndex:I
    .local v7, valueIndex:I
    :try_start_20
    aget-byte v8, v5, v6

    #@22
    and-int/lit16 v8, v8, 0xff

    #@24
    if-nez v8, :cond_3a

    #@26
    const/4 v8, 0x1

    #@27
    :goto_27
    iput-boolean v8, v1, Lcom/android/internal/telephony/cat/ItemsIconId;->selfExplanatory:Z

    #@29
    .line 198
    const/4 v2, 0x0

    #@2a
    .local v2, index:I
    move v3, v2

    #@2b
    .end local v2           #index:I
    .local v3, index:I
    :goto_2b
    if-ge v3, v4, :cond_46

    #@2d
    .line 199
    iget-object v8, v1, Lcom/android/internal/telephony/cat/ItemsIconId;->recordNumbers:[I
    :try_end_2f
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_20 .. :try_end_2f} :catch_3c

    #@2f
    add-int/lit8 v2, v3, 0x1

    #@31
    .end local v3           #index:I
    .restart local v2       #index:I
    add-int/lit8 v6, v7, 0x1

    #@33
    .end local v7           #valueIndex:I
    .restart local v6       #valueIndex:I
    :try_start_33
    aget-byte v9, v5, v7

    #@35
    aput v9, v8, v3
    :try_end_37
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_33 .. :try_end_37} :catch_47

    #@37
    move v3, v2

    #@38
    .end local v2           #index:I
    .restart local v3       #index:I
    move v7, v6

    #@39
    .end local v6           #valueIndex:I
    .restart local v7       #valueIndex:I
    goto :goto_2b

    #@3a
    .line 196
    .end local v3           #index:I
    :cond_3a
    const/4 v8, 0x0

    #@3b
    goto :goto_27

    #@3c
    .line 201
    :catch_3c
    move-exception v0

    #@3d
    move v6, v7

    #@3e
    .line 202
    .end local v7           #valueIndex:I
    .local v0, e:Ljava/lang/IndexOutOfBoundsException;
    .restart local v6       #valueIndex:I
    :goto_3e
    new-instance v8, Lcom/android/internal/telephony/cat/ResultException;

    #@40
    sget-object v9, Lcom/android/internal/telephony/cat/ResultCode;->CMD_DATA_NOT_UNDERSTOOD:Lcom/android/internal/telephony/cat/ResultCode;

    #@42
    invoke-direct {v8, v9}, Lcom/android/internal/telephony/cat/ResultException;-><init>(Lcom/android/internal/telephony/cat/ResultCode;)V

    #@45
    throw v8

    #@46
    .line 204
    .end local v0           #e:Ljava/lang/IndexOutOfBoundsException;
    .end local v6           #valueIndex:I
    .restart local v3       #index:I
    .restart local v7       #valueIndex:I
    :cond_46
    return-object v1

    #@47
    .line 201
    .end local v3           #index:I
    .end local v7           #valueIndex:I
    .restart local v2       #index:I
    .restart local v6       #valueIndex:I
    :catch_47
    move-exception v0

    #@48
    goto :goto_3e
.end method

.method static retrieveTextAttribute(Lcom/android/internal/telephony/cat/ComprehensionTlv;)Ljava/util/List;
    .registers 25
    .parameter "ctlv"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/internal/telephony/cat/ComprehensionTlv;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/cat/TextAttribute;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/cat/ResultException;
        }
    .end annotation

    #@0
    .prologue
    .line 217
    new-instance v18, Ljava/util/ArrayList;

    #@2
    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 219
    .local v18, lst:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/cat/TextAttribute;>;"
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/cat/ComprehensionTlv;->getRawValue()[B

    #@8
    move-result-object v19

    #@9
    .line 220
    .local v19, rawValue:[B
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/cat/ComprehensionTlv;->getValueIndex()I

    #@c
    move-result v21

    #@d
    .line 221
    .local v21, valueIndex:I
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/cat/ComprehensionTlv;->getLength()I

    #@10
    move-result v17

    #@11
    .line 223
    .local v17, length:I
    if-eqz v17, :cond_84

    #@13
    .line 225
    div-int/lit8 v16, v17, 0x4

    #@15
    .line 228
    .local v16, itemCount:I
    const/4 v15, 0x0

    #@16
    .local v15, i:I
    :goto_16
    move/from16 v0, v16

    #@18
    if-ge v15, v0, :cond_86

    #@1a
    .line 229
    :try_start_1a
    aget-byte v22, v19, v21

    #@1c
    move/from16 v0, v22

    #@1e
    and-int/lit16 v2, v0, 0xff

    #@20
    .line 230
    .local v2, start:I
    add-int/lit8 v22, v21, 0x1

    #@22
    aget-byte v22, v19, v22

    #@24
    move/from16 v0, v22

    #@26
    and-int/lit16 v3, v0, 0xff

    #@28
    .line 231
    .local v3, textLength:I
    add-int/lit8 v22, v21, 0x2

    #@2a
    aget-byte v22, v19, v22

    #@2c
    move/from16 v0, v22

    #@2e
    and-int/lit16 v14, v0, 0xff

    #@30
    .line 232
    .local v14, format:I
    add-int/lit8 v22, v21, 0x3

    #@32
    aget-byte v22, v19, v22

    #@34
    move/from16 v0, v22

    #@36
    and-int/lit16 v12, v0, 0xff

    #@38
    .line 234
    .local v12, colorValue:I
    and-int/lit8 v11, v14, 0x3

    #@3a
    .line 235
    .local v11, alignValue:I
    invoke-static {v11}, Lcom/android/internal/telephony/cat/TextAlignment;->fromInt(I)Lcom/android/internal/telephony/cat/TextAlignment;

    #@3d
    move-result-object v4

    #@3e
    .line 237
    .local v4, align:Lcom/android/internal/telephony/cat/TextAlignment;
    shr-int/lit8 v22, v14, 0x2

    #@40
    and-int/lit8 v20, v22, 0x3

    #@42
    .line 238
    .local v20, sizeValue:I
    invoke-static/range {v20 .. v20}, Lcom/android/internal/telephony/cat/FontSize;->fromInt(I)Lcom/android/internal/telephony/cat/FontSize;

    #@45
    move-result-object v5

    #@46
    .line 239
    .local v5, size:Lcom/android/internal/telephony/cat/FontSize;
    if-nez v5, :cond_4a

    #@48
    .line 241
    sget-object v5, Lcom/android/internal/telephony/cat/FontSize;->NORMAL:Lcom/android/internal/telephony/cat/FontSize;

    #@4a
    .line 244
    :cond_4a
    and-int/lit8 v22, v14, 0x10

    #@4c
    if-eqz v22, :cond_73

    #@4e
    const/4 v6, 0x1

    #@4f
    .line 245
    .local v6, bold:Z
    :goto_4f
    and-int/lit8 v22, v14, 0x20

    #@51
    if-eqz v22, :cond_75

    #@53
    const/4 v7, 0x1

    #@54
    .line 246
    .local v7, italic:Z
    :goto_54
    and-int/lit8 v22, v14, 0x40

    #@56
    if-eqz v22, :cond_77

    #@58
    const/4 v8, 0x1

    #@59
    .line 247
    .local v8, underlined:Z
    :goto_59
    and-int/lit16 v0, v14, 0x80

    #@5b
    move/from16 v22, v0

    #@5d
    if-eqz v22, :cond_79

    #@5f
    const/4 v9, 0x1

    #@60
    .line 249
    .local v9, strikeThrough:Z
    :goto_60
    invoke-static {v12}, Lcom/android/internal/telephony/cat/TextColor;->fromInt(I)Lcom/android/internal/telephony/cat/TextColor;

    #@63
    move-result-object v10

    #@64
    .line 251
    .local v10, color:Lcom/android/internal/telephony/cat/TextColor;
    new-instance v1, Lcom/android/internal/telephony/cat/TextAttribute;

    #@66
    invoke-direct/range {v1 .. v10}, Lcom/android/internal/telephony/cat/TextAttribute;-><init>(IILcom/android/internal/telephony/cat/TextAlignment;Lcom/android/internal/telephony/cat/FontSize;ZZZZLcom/android/internal/telephony/cat/TextColor;)V

    #@69
    .line 254
    .local v1, attr:Lcom/android/internal/telephony/cat/TextAttribute;
    move-object/from16 v0, v18

    #@6b
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_6e
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1a .. :try_end_6e} :catch_7b

    #@6e
    .line 228
    add-int/lit8 v15, v15, 0x1

    #@70
    add-int/lit8 v21, v21, 0x4

    #@72
    goto :goto_16

    #@73
    .line 244
    .end local v1           #attr:Lcom/android/internal/telephony/cat/TextAttribute;
    .end local v6           #bold:Z
    .end local v7           #italic:Z
    .end local v8           #underlined:Z
    .end local v9           #strikeThrough:Z
    .end local v10           #color:Lcom/android/internal/telephony/cat/TextColor;
    :cond_73
    const/4 v6, 0x0

    #@74
    goto :goto_4f

    #@75
    .line 245
    .restart local v6       #bold:Z
    :cond_75
    const/4 v7, 0x0

    #@76
    goto :goto_54

    #@77
    .line 246
    .restart local v7       #italic:Z
    :cond_77
    const/4 v8, 0x0

    #@78
    goto :goto_59

    #@79
    .line 247
    .restart local v8       #underlined:Z
    :cond_79
    const/4 v9, 0x0

    #@7a
    goto :goto_60

    #@7b
    .line 259
    .end local v2           #start:I
    .end local v3           #textLength:I
    .end local v4           #align:Lcom/android/internal/telephony/cat/TextAlignment;
    .end local v5           #size:Lcom/android/internal/telephony/cat/FontSize;
    .end local v6           #bold:Z
    .end local v7           #italic:Z
    .end local v8           #underlined:Z
    .end local v11           #alignValue:I
    .end local v12           #colorValue:I
    .end local v14           #format:I
    .end local v20           #sizeValue:I
    :catch_7b
    move-exception v13

    #@7c
    .line 260
    .local v13, e:Ljava/lang/IndexOutOfBoundsException;
    new-instance v22, Lcom/android/internal/telephony/cat/ResultException;

    #@7e
    sget-object v23, Lcom/android/internal/telephony/cat/ResultCode;->CMD_DATA_NOT_UNDERSTOOD:Lcom/android/internal/telephony/cat/ResultCode;

    #@80
    invoke-direct/range {v22 .. v23}, Lcom/android/internal/telephony/cat/ResultException;-><init>(Lcom/android/internal/telephony/cat/ResultCode;)V

    #@83
    throw v22

    #@84
    .line 263
    .end local v13           #e:Ljava/lang/IndexOutOfBoundsException;
    .end local v15           #i:I
    .end local v16           #itemCount:I
    :cond_84
    const/16 v18, 0x0

    #@86
    .end local v18           #lst:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/cat/TextAttribute;>;"
    :cond_86
    return-object v18
.end method

.method static retrieveTextString(Lcom/android/internal/telephony/cat/ComprehensionTlv;)Ljava/lang/String;
    .registers 10
    .parameter "ctlv"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/cat/ResultException;
        }
    .end annotation

    #@0
    .prologue
    .line 329
    invoke-virtual {p0}, Lcom/android/internal/telephony/cat/ComprehensionTlv;->getRawValue()[B

    #@3
    move-result-object v2

    #@4
    .line 330
    .local v2, rawValue:[B
    invoke-virtual {p0}, Lcom/android/internal/telephony/cat/ComprehensionTlv;->getValueIndex()I

    #@7
    move-result v6

    #@8
    .line 331
    .local v6, valueIndex:I
    const/4 v0, 0x0

    #@9
    .line 332
    .local v0, codingScheme:B
    const/4 v3, 0x0

    #@a
    .line 333
    .local v3, text:Ljava/lang/String;
    invoke-virtual {p0}, Lcom/android/internal/telephony/cat/ComprehensionTlv;->getLength()I

    #@d
    move-result v5

    #@e
    .line 336
    .local v5, textLen:I
    if-nez v5, :cond_12

    #@10
    move-object v4, v3

    #@11
    .line 358
    .end local v3           #text:Ljava/lang/String;
    .local v4, text:Ljava/lang/String;
    :goto_11
    return-object v4

    #@12
    .line 340
    .end local v4           #text:Ljava/lang/String;
    .restart local v3       #text:Ljava/lang/String;
    :cond_12
    add-int/lit8 v5, v5, -0x1

    #@14
    .line 344
    :try_start_14
    aget-byte v7, v2, v6

    #@16
    and-int/lit8 v7, v7, 0xc

    #@18
    int-to-byte v0, v7

    #@19
    .line 346
    if-nez v0, :cond_27

    #@1b
    .line 347
    add-int/lit8 v7, v6, 0x1

    #@1d
    mul-int/lit8 v8, v5, 0x8

    #@1f
    div-int/lit8 v8, v8, 0x7

    #@21
    invoke-static {v2, v7, v8}, Lcom/android/internal/telephony/GsmAlphabet;->gsm7BitPackedToString([BII)Ljava/lang/String;

    #@24
    move-result-object v3

    #@25
    :goto_25
    move-object v4, v3

    #@26
    .line 358
    .end local v3           #text:Ljava/lang/String;
    .restart local v4       #text:Ljava/lang/String;
    goto :goto_11

    #@27
    .line 349
    .end local v4           #text:Ljava/lang/String;
    .restart local v3       #text:Ljava/lang/String;
    :cond_27
    const/4 v7, 0x4

    #@28
    if-ne v0, v7, :cond_31

    #@2a
    .line 350
    add-int/lit8 v7, v6, 0x1

    #@2c
    invoke-static {v2, v7, v5}, Lcom/android/internal/telephony/GsmAlphabet;->gsm8BitUnpackedToString([BII)Ljava/lang/String;

    #@2f
    move-result-object v3

    #@30
    goto :goto_25

    #@31
    .line 352
    :cond_31
    const/16 v7, 0x8

    #@33
    if-ne v0, v7, :cond_3f

    #@35
    .line 353
    new-instance v3, Ljava/lang/String;

    #@37
    .end local v3           #text:Ljava/lang/String;
    add-int/lit8 v7, v6, 0x1

    #@39
    const-string v8, "UTF-16"

    #@3b
    invoke-direct {v3, v2, v7, v5, v8}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    #@3e
    .restart local v3       #text:Ljava/lang/String;
    goto :goto_25

    #@3f
    .line 355
    :cond_3f
    new-instance v7, Lcom/android/internal/telephony/cat/ResultException;

    #@41
    sget-object v8, Lcom/android/internal/telephony/cat/ResultCode;->CMD_DATA_NOT_UNDERSTOOD:Lcom/android/internal/telephony/cat/ResultCode;

    #@43
    invoke-direct {v7, v8}, Lcom/android/internal/telephony/cat/ResultException;-><init>(Lcom/android/internal/telephony/cat/ResultCode;)V

    #@46
    throw v7
    :try_end_47
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_14 .. :try_end_47} :catch_47
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_14 .. :try_end_47} :catch_50

    #@47
    .line 359
    .end local v3           #text:Ljava/lang/String;
    :catch_47
    move-exception v1

    #@48
    .line 360
    .local v1, e:Ljava/lang/IndexOutOfBoundsException;
    new-instance v7, Lcom/android/internal/telephony/cat/ResultException;

    #@4a
    sget-object v8, Lcom/android/internal/telephony/cat/ResultCode;->CMD_DATA_NOT_UNDERSTOOD:Lcom/android/internal/telephony/cat/ResultCode;

    #@4c
    invoke-direct {v7, v8}, Lcom/android/internal/telephony/cat/ResultException;-><init>(Lcom/android/internal/telephony/cat/ResultCode;)V

    #@4f
    throw v7

    #@50
    .line 361
    .end local v1           #e:Ljava/lang/IndexOutOfBoundsException;
    :catch_50
    move-exception v1

    #@51
    .line 363
    .local v1, e:Ljava/io/UnsupportedEncodingException;
    new-instance v7, Lcom/android/internal/telephony/cat/ResultException;

    #@53
    sget-object v8, Lcom/android/internal/telephony/cat/ResultCode;->CMD_DATA_NOT_UNDERSTOOD:Lcom/android/internal/telephony/cat/ResultCode;

    #@55
    invoke-direct {v7, v8}, Lcom/android/internal/telephony/cat/ResultException;-><init>(Lcom/android/internal/telephony/cat/ResultCode;)V

    #@58
    throw v7
.end method
