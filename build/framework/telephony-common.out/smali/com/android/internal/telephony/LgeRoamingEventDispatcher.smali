.class public final Lcom/android/internal/telephony/LgeRoamingEventDispatcher;
.super Landroid/os/Handler;
.source "LgeRoamingEventDispatcher.java"


# static fields
.field private static final CAMPED_MCC:Ljava/lang/String; = "CAMPED_MCC"

.field private static DBG:Z = false

.field protected static final EVENT_NETWORK_MODE_QUERY_DONE:I = 0x3ee

.field protected static final EVENT_NETWORK_MODE_TO_GW_GWL_DONE:I = 0x3ef

.field protected static final EVENT_SET_NT_MODE_TO_WPREF_DONE:I = 0x44d

.field protected static final EVENT_WCDMA_NET_CHANGED:I = 0x64

.field protected static final EVENT_WCDMA_NET_TO_KOREA_CHANGED:I = 0x65

.field private static final LOG_TAG:Ljava/lang/String; = "CALL_FRW"

.field private static final MCC_KR:Ljava/lang/String; = "450"

.field private static final SHAREDPREF_SAVED_CAMPED_MCC:Ljava/lang/String; = "saved_CAMPED_MCC"

.field private static roamingEventDispatcher:Lcom/android/internal/telephony/LgeRoamingEventDispatcher;


# instance fields
.field private final mCm:Lcom/android/internal/telephony/CommandsInterface;

.field private final mContext:Landroid/content/Context;

.field private mCurCampedMcc:Ljava/lang/String;

.field private mDesiredNwMode:I

.field private mFirstImmigration:Z

.field private mOldCampedMcc:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 54
    const/4 v0, 0x1

    #@1
    sput-boolean v0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->DBG:Z

    #@3
    .line 112
    const/4 v0, 0x0

    #@4
    sput-object v0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->roamingEventDispatcher:Lcom/android/internal/telephony/LgeRoamingEventDispatcher;

    #@6
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;)V
    .registers 8
    .parameter "ctx"
    .parameter "commandsInterface"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 84
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@4
    .line 61
    const-string v2, ""

    #@6
    iput-object v2, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mOldCampedMcc:Ljava/lang/String;

    #@8
    .line 62
    const-string v2, ""

    #@a
    iput-object v2, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mCurCampedMcc:Ljava/lang/String;

    #@c
    .line 74
    const/16 v2, 0x9

    #@e
    iput v2, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mDesiredNwMode:I

    #@10
    .line 75
    const/4 v2, 0x1

    #@11
    iput-boolean v2, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mFirstImmigration:Z

    #@13
    .line 86
    const-string v2, "LgeRoamingEventDispatcher created"

    #@15
    invoke-static {v2}, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->log(Ljava/lang/String;)V

    #@18
    .line 87
    iput-object p1, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mContext:Landroid/content/Context;

    #@1a
    .line 88
    iput-object p2, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@1c
    .line 89
    iget-object v2, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@1e
    if-eqz v2, :cond_2e

    #@20
    .line 90
    iget-object v2, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@22
    const/16 v3, 0x64

    #@24
    invoke-interface {v2, p0, v3, v4}, Lcom/android/internal/telephony/CommandsInterface;->registerForWcdmaNetChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@27
    .line 91
    iget-object v2, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@29
    const/16 v3, 0x65

    #@2b
    invoke-interface {v2, p0, v3, v4}, Lcom/android/internal/telephony/CommandsInterface;->registerForWcdmaNetToKoreaChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@2e
    .line 99
    :cond_2e
    iget-object v2, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mContext:Landroid/content/Context;

    #@30
    const-string v3, "saved_CAMPED_MCC"

    #@32
    const/4 v4, 0x0

    #@33
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    #@36
    move-result-object v0

    #@37
    .line 100
    .local v0, prefs:Landroid/content/SharedPreferences;
    const-string v2, "CAMPED_MCC"

    #@39
    const-string v3, ""

    #@3b
    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@3e
    move-result-object v1

    #@3f
    .line 101
    .local v1, savedMcc:Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    #@41
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@44
    const-string v3, "SharedPreference(SHAREDPREF_SAVED_CAMPED_MCC) saved mcc = "

    #@46
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v2

    #@4a
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v2

    #@4e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@51
    move-result-object v2

    #@52
    invoke-static {v2}, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->log(Ljava/lang/String;)V

    #@55
    .line 102
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@58
    move-result v2

    #@59
    if-nez v2, :cond_63

    #@5b
    const-string v2, "000"

    #@5d
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@60
    move-result v2

    #@61
    if-eqz v2, :cond_90

    #@63
    .line 105
    :cond_63
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    #@66
    move-result-object v2

    #@67
    const-string v3, "CAMPED_MCC"

    #@69
    const-string v4, "450"

    #@6b
    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@6e
    move-result-object v2

    #@6f
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    #@72
    .line 106
    new-instance v2, Ljava/lang/StringBuilder;

    #@74
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@77
    const-string v3, "Init SharedPreference(SHAREDPREF_SAVED_CAMPED_MCC) cammped mcc = "

    #@79
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v2

    #@7d
    const-string v3, "CAMPED_MCC"

    #@7f
    const-string v4, ""

    #@81
    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@84
    move-result-object v3

    #@85
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@88
    move-result-object v2

    #@89
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8c
    move-result-object v2

    #@8d
    invoke-static {v2}, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->log(Ljava/lang/String;)V

    #@90
    .line 110
    :cond_90
    return-void
.end method

.method private checkAndNWModeChange()V
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v5, -0x1

    #@2
    .line 448
    const/4 v2, -0x1

    #@3
    .line 449
    .local v2, mobile_data:I
    const/4 v0, -0x1

    #@4
    .line 450
    .local v0, data_roaming:I
    const/4 v1, -0x1

    #@5
    .line 452
    .local v1, lte_roaming:I
    new-instance v3, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v4, "checkAndNWModeChange:: hasRegistered, mOldCampedMcc = "

    #@c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v3

    #@10
    iget-object v4, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mOldCampedMcc:Ljava/lang/String;

    #@12
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v3

    #@16
    const-string v4, ", mCurCampedMcc = "

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    iget-object v4, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mCurCampedMcc:Ljava/lang/String;

    #@1e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v3

    #@22
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v3

    #@26
    invoke-static {v3}, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->log(Ljava/lang/String;)V

    #@29
    .line 454
    const-string v3, "450"

    #@2b
    iget-object v4, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mOldCampedMcc:Ljava/lang/String;

    #@2d
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@30
    move-result v3

    #@31
    if-eqz v3, :cond_98

    #@33
    const-string v3, "450"

    #@35
    iget-object v4, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mCurCampedMcc:Ljava/lang/String;

    #@37
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3a
    move-result v3

    #@3b
    if-nez v3, :cond_98

    #@3d
    .line 456
    iget-object v3, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mContext:Landroid/content/Context;

    #@3f
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@42
    move-result-object v3

    #@43
    const-string v4, "mobile_data"

    #@45
    invoke-static {v3, v4, v5}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@48
    move-result v2

    #@49
    .line 457
    iget-object v3, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mContext:Landroid/content/Context;

    #@4b
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@4e
    move-result-object v3

    #@4f
    const-string v4, "data_roaming"

    #@51
    invoke-static {v3, v4, v5}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@54
    move-result v0

    #@55
    .line 458
    iget-object v3, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mContext:Landroid/content/Context;

    #@57
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5a
    move-result-object v3

    #@5b
    const-string v4, "lte_roaming"

    #@5d
    invoke-static {v3, v4, v5}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@60
    move-result v1

    #@61
    .line 460
    new-instance v3, Ljava/lang/StringBuilder;

    #@63
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@66
    const-string v4, "mobile_data = "

    #@68
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v3

    #@6c
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v3

    #@70
    const-string v4, ", data_roaming = "

    #@72
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v3

    #@76
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@79
    move-result-object v3

    #@7a
    const-string v4, ", lte_roaming="

    #@7c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v3

    #@80
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@83
    move-result-object v3

    #@84
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@87
    move-result-object v3

    #@88
    invoke-static {v3}, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->log(Ljava/lang/String;)V

    #@8b
    .line 462
    if-ne v2, v6, :cond_99

    #@8d
    if-ne v0, v6, :cond_99

    #@8f
    if-ne v1, v6, :cond_99

    #@91
    .line 463
    const/16 v3, 0x9

    #@93
    iput v3, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mDesiredNwMode:I

    #@95
    .line 464
    invoke-direct {p0, v5}, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->setPreferredNetworkForRoaming(I)V

    #@98
    .line 471
    :cond_98
    :goto_98
    return-void

    #@99
    .line 466
    :cond_99
    const/4 v3, 0x0

    #@9a
    iput v3, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mDesiredNwMode:I

    #@9c
    .line 467
    invoke-direct {p0, v5}, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->setPreferredNetworkForRoaming(I)V

    #@9f
    goto :goto_98
.end method

.method public static getLgeRoamingEventDispatcher(Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;)Lcom/android/internal/telephony/LgeRoamingEventDispatcher;
    .registers 4
    .parameter "ctx"
    .parameter "commandsInterface"

    #@0
    .prologue
    .line 119
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "getLgeRoamingEventDispatcher : roamingEventDispatcher="

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    sget-object v1, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->roamingEventDispatcher:Lcom/android/internal/telephony/LgeRoamingEventDispatcher;

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, "ctx="

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v0

    #@1b
    const-string v1, "commandsInterface="

    #@1d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v0

    #@21
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v0

    #@25
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v0

    #@29
    invoke-static {v0}, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->log(Ljava/lang/String;)V

    #@2c
    .line 123
    if-eqz p0, :cond_30

    #@2e
    if-nez p1, :cond_32

    #@30
    :cond_30
    const/4 v0, 0x0

    #@31
    .line 129
    :goto_31
    return-object v0

    #@32
    .line 125
    :cond_32
    sget-object v0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->roamingEventDispatcher:Lcom/android/internal/telephony/LgeRoamingEventDispatcher;

    #@34
    if-nez v0, :cond_3d

    #@36
    .line 126
    new-instance v0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;

    #@38
    invoke-direct {v0, p0, p1}, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;-><init>(Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;)V

    #@3b
    sput-object v0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->roamingEventDispatcher:Lcom/android/internal/telephony/LgeRoamingEventDispatcher;

    #@3d
    .line 129
    :cond_3d
    sget-object v0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->roamingEventDispatcher:Lcom/android/internal/telephony/LgeRoamingEventDispatcher;

    #@3f
    goto :goto_31
.end method

.method protected static log(Ljava/lang/String;)V
    .registers 4
    .parameter "s"

    #@0
    .prologue
    .line 486
    sget-boolean v0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->DBG:Z

    #@2
    if-eqz v0, :cond_1c

    #@4
    const-string v0, "CALL_FRW"

    #@6
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v2, "[LgeRoamingEventDispatcher] "

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 487
    :cond_1c
    return-void
.end method

.method private saveNetworkInfo(I)Z
    .registers 8
    .parameter "new_mcc"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 392
    new-instance v4, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v5, "saveNetworkInfo : new_mcc = "

    #@8
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v4

    #@c
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f
    move-result-object v4

    #@10
    const-string v5, ", mOldCampedMcc = "

    #@12
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v4

    #@16
    iget-object v5, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mOldCampedMcc:Ljava/lang/String;

    #@18
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v4

    #@1c
    const-string v5, ", mCurCampedMcc = "

    #@1e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v4

    #@22
    iget-object v5, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mCurCampedMcc:Ljava/lang/String;

    #@24
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v4

    #@28
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v4

    #@2c
    invoke-static {v4}, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->log(Ljava/lang/String;)V

    #@2f
    .line 394
    iget-object v4, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mContext:Landroid/content/Context;

    #@31
    const-string v5, "saved_CAMPED_MCC"

    #@33
    invoke-virtual {v4, v5, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    #@36
    move-result-object v2

    #@37
    .line 395
    .local v2, prefs:Landroid/content/SharedPreferences;
    const-string v4, "CAMPED_MCC"

    #@39
    const-string v5, ""

    #@3b
    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@3e
    move-result-object v1

    #@3f
    .line 396
    .local v1, oldMccStr:Ljava/lang/String;
    const-string v0, ""

    #@41
    .line 399
    .local v0, newMccStr:Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    #@43
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@46
    const-string v5, "prefs get = "

    #@48
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v4

    #@4c
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v4

    #@50
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@53
    move-result-object v4

    #@54
    invoke-static {v4}, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->log(Ljava/lang/String;)V

    #@57
    .line 400
    if-eqz p1, :cond_be

    #@59
    .line 401
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@5c
    move-result-object v0

    #@5d
    .line 403
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@60
    move-result v4

    #@61
    if-nez v4, :cond_be

    #@63
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@66
    move-result v4

    #@67
    if-nez v4, :cond_be

    #@69
    .line 405
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    #@6c
    move-result-object v3

    #@6d
    const-string v4, "CAMPED_MCC"

    #@6f
    invoke-interface {v3, v4, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@72
    move-result-object v3

    #@73
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    #@76
    .line 406
    new-instance v3, Ljava/lang/StringBuilder;

    #@78
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7b
    const-string v4, "prefs put = "

    #@7d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v3

    #@81
    const-string v4, "CAMPED_MCC"

    #@83
    const-string v5, ""

    #@85
    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@88
    move-result-object v4

    #@89
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v3

    #@8d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@90
    move-result-object v3

    #@91
    invoke-static {v3}, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->log(Ljava/lang/String;)V

    #@94
    .line 409
    iput-object v1, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mOldCampedMcc:Ljava/lang/String;

    #@96
    .line 410
    iput-object v0, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mCurCampedMcc:Ljava/lang/String;

    #@98
    .line 412
    new-instance v3, Ljava/lang/StringBuilder;

    #@9a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@9d
    const-string v4, "mOldCampedMcc = "

    #@9f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v3

    #@a3
    iget-object v4, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mOldCampedMcc:Ljava/lang/String;

    #@a5
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v3

    #@a9
    const-string v4, ", mCurCampedMcc = "

    #@ab
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v3

    #@af
    iget-object v4, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mCurCampedMcc:Ljava/lang/String;

    #@b1
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v3

    #@b5
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b8
    move-result-object v3

    #@b9
    invoke-static {v3}, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->log(Ljava/lang/String;)V

    #@bc
    .line 414
    const/4 v3, 0x1

    #@bd
    .line 419
    :goto_bd
    return v3

    #@be
    .line 418
    :cond_be
    const-string v4, "mOldCampedMcc, mCurCampedMcc is not changed"

    #@c0
    invoke-static {v4}, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->log(Ljava/lang/String;)V

    #@c3
    goto :goto_bd
.end method

.method private setPreferredNetworkForRoaming(I)V
    .registers 6
    .parameter "mPrevNetworkMode"

    #@0
    .prologue
    .line 360
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "Current Network Mode : "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-static {v0}, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->log(Ljava/lang/String;)V

    #@16
    .line 361
    const/4 v0, -0x1

    #@17
    if-ne p1, v0, :cond_25

    #@19
    .line 362
    iget-object v0, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@1b
    const/16 v1, 0x3ee

    #@1d
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->obtainMessage(I)Landroid/os/Message;

    #@20
    move-result-object v1

    #@21
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/CommandsInterface;->getPreferredNetworkType(Landroid/os/Message;)V

    #@24
    .line 375
    :cond_24
    :goto_24
    return-void

    #@25
    .line 364
    :cond_25
    iget v0, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mDesiredNwMode:I

    #@27
    if-ne p1, v0, :cond_58

    #@29
    .line 365
    new-instance v0, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v1, "Aleady mDesiredNwMode :: mPrevNetworkMode = "

    #@30
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v0

    #@34
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@37
    move-result-object v0

    #@38
    const-string v1, ", mDesiredNwMode = "

    #@3a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v0

    #@3e
    iget v1, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mDesiredNwMode:I

    #@40
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@43
    move-result-object v0

    #@44
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v0

    #@48
    invoke-static {v0}, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->log(Ljava/lang/String;)V

    #@4b
    .line 366
    const/4 v0, 0x0

    #@4c
    const-string v1, "LTE_ROAMING_SKT"

    #@4e
    invoke-static {v0, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@51
    move-result v0

    #@52
    if-eqz v0, :cond_24

    #@54
    .line 367
    const/4 v0, 0x0

    #@55
    iput-boolean v0, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mFirstImmigration:Z

    #@57
    goto :goto_24

    #@58
    .line 371
    :cond_58
    new-instance v0, Ljava/lang/StringBuilder;

    #@5a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5d
    const-string v1, "Change GW/GWL Network Mode : "

    #@5f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v0

    #@63
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@66
    move-result-object v0

    #@67
    const-string v1, " -> "

    #@69
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v0

    #@6d
    iget v1, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mDesiredNwMode:I

    #@6f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@72
    move-result-object v0

    #@73
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@76
    move-result-object v0

    #@77
    invoke-static {v0}, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->log(Ljava/lang/String;)V

    #@7a
    .line 372
    iget-object v0, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@7c
    iget v1, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mDesiredNwMode:I

    #@7e
    const/16 v2, 0x3ef

    #@80
    iget v3, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mDesiredNwMode:I

    #@82
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@85
    move-result-object v3

    #@86
    invoke-virtual {p0, v2, v3}, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@89
    move-result-object v2

    #@8a
    invoke-interface {v0, v1, v2}, Lcom/android/internal/telephony/CommandsInterface;->setPreferredNetworkType(ILandroid/os/Message;)V

    #@8d
    goto :goto_24
.end method


# virtual methods
.method protected finalize()V
    .registers 2

    #@0
    .prologue
    .line 477
    const-string v0, "LgeRoamingEventDispatcher finalized"

    #@2
    invoke-static {v0}, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->log(Ljava/lang/String;)V

    #@5
    .line 479
    iget-object v0, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@7
    if-eqz v0, :cond_13

    #@9
    .line 480
    iget-object v0, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@b
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForWcdmaNetChanged(Landroid/os/Handler;)V

    #@e
    .line 481
    iget-object v0, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@10
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForWcdmaNetToKoreaChanged(Landroid/os/Handler;)V

    #@13
    .line 483
    :cond_13
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 14
    .parameter "msg"

    #@0
    .prologue
    .line 137
    new-instance v9, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v10, "New Roaming Event Message Received : "

    #@7
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v9

    #@b
    iget v10, p1, Landroid/os/Message;->what:I

    #@d
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v9

    #@11
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v9

    #@15
    invoke-static {v9}, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->log(Ljava/lang/String;)V

    #@18
    .line 139
    iget v9, p1, Landroid/os/Message;->what:I

    #@1a
    sparse-switch v9, :sswitch_data_3fa

    #@1d
    .line 350
    const-string v9, "LgeRoamingEventDispatcher unexpected handleMessage case"

    #@1f
    invoke-static {v9}, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->log(Ljava/lang/String;)V

    #@22
    .line 354
    :cond_22
    :goto_22
    return-void

    #@23
    .line 143
    :sswitch_23
    const-string v9, "KR"

    #@25
    const-string v10, "LGU"

    #@27
    invoke-static {v9, v10}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@2a
    move-result v9

    #@2b
    if-eqz v9, :cond_22

    #@2d
    .line 144
    const-string v9, "Network mode change (GWL->GW) completed"

    #@2f
    invoke-static {v9}, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->log(Ljava/lang/String;)V

    #@32
    .line 146
    iget-object v9, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mContext:Landroid/content/Context;

    #@34
    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@37
    move-result-object v9

    #@38
    const-string v10, "preferred_network_mode"

    #@3a
    const/4 v11, 0x0

    #@3b
    invoke-static {v9, v10, v11}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@3e
    goto :goto_22

    #@3f
    .line 158
    :sswitch_3f
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@41
    check-cast v0, Landroid/os/AsyncResult;

    #@43
    .line 159
    .local v0, ar:Landroid/os/AsyncResult;
    const/4 v3, 0x3

    #@44
    .line 160
    .local v3, mPrevNetworkType:I
    iget-object v9, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@46
    if-nez v9, :cond_51

    #@48
    .line 161
    iget-object v9, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@4a
    check-cast v9, [I

    #@4c
    check-cast v9, [I

    #@4e
    const/4 v10, 0x0

    #@4f
    aget v3, v9, v10

    #@51
    .line 163
    :cond_51
    invoke-direct {p0, v3}, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->setPreferredNetworkForRoaming(I)V

    #@54
    goto :goto_22

    #@55
    .line 167
    .end local v0           #ar:Landroid/os/AsyncResult;
    .end local v3           #mPrevNetworkType:I
    :sswitch_55
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@57
    check-cast v0, Landroid/os/AsyncResult;

    #@59
    .line 168
    .restart local v0       #ar:Landroid/os/AsyncResult;
    iget-object v9, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@5b
    if-nez v9, :cond_93

    #@5d
    .line 169
    iget-object v9, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@5f
    check-cast v9, Ljava/lang/Integer;

    #@61
    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    #@64
    move-result v5

    #@65
    .line 170
    .local v5, nwMode:I
    new-instance v9, Ljava/lang/StringBuilder;

    #@67
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@6a
    const-string v10, "EVENT_NETWORK_MODE_TO_GW_GWL_DONE received nwMod e= "

    #@6c
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v9

    #@70
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@73
    move-result-object v9

    #@74
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@77
    move-result-object v9

    #@78
    invoke-static {v9}, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->log(Ljava/lang/String;)V

    #@7b
    .line 171
    iget-object v9, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mContext:Landroid/content/Context;

    #@7d
    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@80
    move-result-object v9

    #@81
    const-string v10, "preferred_network_mode"

    #@83
    invoke-static {v9, v10, v5}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@86
    .line 172
    const/4 v9, 0x0

    #@87
    const-string v10, "LTE_ROAMING_SKT"

    #@89
    invoke-static {v9, v10}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@8c
    move-result v9

    #@8d
    if-eqz v9, :cond_22

    #@8f
    .line 173
    const/4 v9, 0x0

    #@90
    iput-boolean v9, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mFirstImmigration:Z

    #@92
    goto :goto_22

    #@93
    .line 176
    .end local v5           #nwMode:I
    :cond_93
    new-instance v9, Ljava/lang/StringBuilder;

    #@95
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@98
    const-string v10, "EVENT_NETWORK_MODE_TO_GW_GWL_DONE ERROR ar.exception = "

    #@9a
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v9

    #@9e
    iget-object v10, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@a0
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v9

    #@a4
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a7
    move-result-object v9

    #@a8
    invoke-static {v9}, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->log(Ljava/lang/String;)V

    #@ab
    goto/16 :goto_22

    #@ad
    .line 183
    .end local v0           #ar:Landroid/os/AsyncResult;
    :sswitch_ad
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@af
    check-cast v0, Landroid/os/AsyncResult;

    #@b1
    .line 184
    .restart local v0       #ar:Landroid/os/AsyncResult;
    iget-object v9, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@b3
    if-nez v9, :cond_22

    #@b5
    iget-object v9, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@b7
    check-cast v9, [I

    #@b9
    check-cast v9, [I

    #@bb
    array-length v9, v9

    #@bc
    const/4 v10, 0x1

    #@bd
    if-le v9, v10, :cond_22

    #@bf
    .line 185
    iget-object v9, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@c1
    check-cast v9, [I

    #@c3
    check-cast v9, [I

    #@c5
    const/4 v10, 0x0

    #@c6
    aget v4, v9, v10

    #@c8
    .line 186
    .local v4, mcc:I
    iget-object v9, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@ca
    check-cast v9, [I

    #@cc
    check-cast v9, [I

    #@ce
    const/4 v10, 0x1

    #@cf
    aget v6, v9, v10

    #@d1
    .line 187
    .local v6, rat:I
    new-instance v9, Ljava/lang/StringBuilder;

    #@d3
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@d6
    const-string v10, "EVENT_WCDMA_NET_CHANGED : mcc = "

    #@d8
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@db
    move-result-object v9

    #@dc
    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@df
    move-result-object v9

    #@e0
    const-string v10, ", rat = "

    #@e2
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e5
    move-result-object v9

    #@e6
    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e9
    move-result-object v9

    #@ea
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ed
    move-result-object v9

    #@ee
    invoke-static {v9}, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->log(Ljava/lang/String;)V

    #@f1
    .line 189
    invoke-direct {p0, v4}, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->saveNetworkInfo(I)Z

    #@f4
    move-result v2

    #@f5
    .line 190
    .local v2, isCampedMccChanged:Z
    new-instance v9, Ljava/lang/StringBuilder;

    #@f7
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@fa
    const-string v10, "EVENT_WCDMA_NET_CHANGED : isCampedMccChanged = "

    #@fc
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ff
    move-result-object v9

    #@100
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@103
    move-result-object v9

    #@104
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@107
    move-result-object v9

    #@108
    invoke-static {v9}, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->log(Ljava/lang/String;)V

    #@10b
    .line 193
    const-string v9, "persist.radio.isroaming"

    #@10d
    const-string v10, "true"

    #@10f
    invoke-static {v9, v10}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@112
    .line 197
    if-eqz v2, :cond_176

    #@114
    .line 198
    iget-object v9, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mOldCampedMcc:Ljava/lang/String;

    #@116
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@119
    move-result v9

    #@11a
    if-eqz v9, :cond_126

    #@11c
    const-string v9, "450"

    #@11e
    iget-object v10, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mCurCampedMcc:Ljava/lang/String;

    #@120
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@123
    move-result v9

    #@124
    if-nez v9, :cond_176

    #@126
    .line 199
    :cond_126
    const-string v9, "001"

    #@128
    iget-object v10, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mCurCampedMcc:Ljava/lang/String;

    #@12a
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@12d
    move-result v9

    #@12e
    if-nez v9, :cond_176

    #@130
    .line 200
    iget-object v8, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mOldCampedMcc:Ljava/lang/String;

    #@132
    .line 201
    .local v8, validOldCampedMcc:Ljava/lang/String;
    iget-object v9, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mOldCampedMcc:Ljava/lang/String;

    #@134
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@137
    move-result v9

    #@138
    if-eqz v9, :cond_13c

    #@13a
    .line 202
    const-string v8, "450"

    #@13c
    .line 204
    :cond_13c
    new-instance v1, Landroid/content/Intent;

    #@13e
    const-string v9, "com.lge.intent.action.LGE_CAMPED_MCC_CHANGE"

    #@140
    invoke-direct {v1, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@143
    .line 205
    .local v1, intent:Landroid/content/Intent;
    const-string v9, "newmcc"

    #@145
    iget-object v10, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mCurCampedMcc:Ljava/lang/String;

    #@147
    invoke-virtual {v1, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@14a
    .line 206
    const-string v9, "oldmcc"

    #@14c
    invoke-virtual {v1, v9, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@14f
    .line 207
    new-instance v9, Ljava/lang/StringBuilder;

    #@151
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@154
    const-string v10, "ACTION_CAMPED_MCC_CHANGE : EXTRA_NEW_MCC = "

    #@156
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@159
    move-result-object v9

    #@15a
    iget-object v10, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mCurCampedMcc:Ljava/lang/String;

    #@15c
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15f
    move-result-object v9

    #@160
    const-string v10, ", EXTRA_OLD_MCC = "

    #@162
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@165
    move-result-object v9

    #@166
    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@169
    move-result-object v9

    #@16a
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16d
    move-result-object v9

    #@16e
    invoke-static {v9}, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->log(Ljava/lang/String;)V

    #@171
    .line 208
    iget-object v9, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mContext:Landroid/content/Context;

    #@173
    invoke-virtual {v9, v1}, Landroid/content/Context;->sendStickyBroadcast(Landroid/content/Intent;)V

    #@176
    .line 215
    .end local v1           #intent:Landroid/content/Intent;
    .end local v8           #validOldCampedMcc:Ljava/lang/String;
    :cond_176
    const-string v9, "KR"

    #@178
    const-string v10, "LGU"

    #@17a
    invoke-static {v9, v10}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@17d
    move-result v9

    #@17e
    if-eqz v9, :cond_209

    #@180
    .line 217
    if-eqz v2, :cond_22

    #@182
    .line 218
    const-string v9, "450"

    #@184
    iget-object v10, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mOldCampedMcc:Ljava/lang/String;

    #@186
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@189
    move-result v9

    #@18a
    if-eqz v9, :cond_22

    #@18c
    .line 219
    const/4 v9, 0x0

    #@18d
    const-string v10, "lgu_lte_roaming"

    #@18f
    invoke-static {v9, v10}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@192
    move-result v9

    #@193
    if-eqz v9, :cond_1c0

    #@195
    .line 220
    const-string v9, "Maintain network mode as GWL to support LTE roaming."

    #@197
    invoke-static {v9}, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->log(Ljava/lang/String;)V

    #@19a
    .line 230
    :goto_19a
    const/4 v9, 0x0

    #@19b
    const-string v10, "OEM_RAD_DIALER_POPUP"

    #@19d
    invoke-static {v9, v10}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@1a0
    move-result v9

    #@1a1
    if-eqz v9, :cond_22

    #@1a3
    .line 231
    const-string v9, "KR"

    #@1a5
    const-string v10, "LGU"

    #@1a7
    invoke-static {v9, v10}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@1aa
    move-result v9

    #@1ab
    if-eqz v9, :cond_22

    #@1ad
    .line 233
    iget-object v9, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mContext:Landroid/content/Context;

    #@1af
    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1b2
    move-result-object v9

    #@1b3
    const-string v10, "oem_rad_dialer_popup"

    #@1b5
    const/4 v11, 0x1

    #@1b6
    invoke-static {v9, v10, v11}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@1b9
    .line 234
    const-string v9, "TelephonyUtils.OEM_RAD_DIALER_POPUP set 1 by LGU+ scenario"

    #@1bb
    invoke-static {v9}, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->log(Ljava/lang/String;)V

    #@1be
    goto/16 :goto_22

    #@1c0
    .line 222
    :cond_1c0
    const-string v9, "001"

    #@1c2
    iget-object v10, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mCurCampedMcc:Ljava/lang/String;

    #@1c4
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1c7
    move-result v9

    #@1c8
    if-nez v9, :cond_1de

    #@1ca
    const-string v9, "1"

    #@1cc
    iget-object v10, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mCurCampedMcc:Ljava/lang/String;

    #@1ce
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1d1
    move-result v9

    #@1d2
    if-nez v9, :cond_1de

    #@1d4
    const-string v9, "000"

    #@1d6
    iget-object v10, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mCurCampedMcc:Ljava/lang/String;

    #@1d8
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1db
    move-result v9

    #@1dc
    if-eqz v9, :cond_1f7

    #@1de
    .line 223
    :cond_1de
    new-instance v9, Ljava/lang/StringBuilder;

    #@1e0
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@1e3
    iget-object v10, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mCurCampedMcc:Ljava/lang/String;

    #@1e5
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e8
    move-result-object v9

    #@1e9
    const-string v10, " is invalid MCC, Do NOT change preferred network mode."

    #@1eb
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ee
    move-result-object v9

    #@1ef
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f2
    move-result-object v9

    #@1f3
    invoke-static {v9}, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->log(Ljava/lang/String;)V

    #@1f6
    goto :goto_19a

    #@1f7
    .line 225
    :cond_1f7
    iget-object v9, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@1f9
    const/4 v10, 0x0

    #@1fa
    const/16 v11, 0x44d

    #@1fc
    invoke-virtual {p0, v11}, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->obtainMessage(I)Landroid/os/Message;

    #@1ff
    move-result-object v11

    #@200
    invoke-interface {v9, v10, v11}, Lcom/android/internal/telephony/CommandsInterface;->setPreferredNetworkType(ILandroid/os/Message;)V

    #@203
    .line 226
    const-string v9, "Change network mode (GWL->GW) arriving at roaming area."

    #@205
    invoke-static {v9}, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->log(Ljava/lang/String;)V

    #@208
    goto :goto_19a

    #@209
    .line 248
    :cond_209
    const-string v9, "KR"

    #@20b
    const-string v10, "SKT"

    #@20d
    invoke-static {v9, v10}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@210
    move-result v9

    #@211
    if-eqz v9, :cond_2d7

    #@213
    .line 249
    const-string v9, "450"

    #@215
    iget-object v10, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mCurCampedMcc:Ljava/lang/String;

    #@217
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@21a
    move-result v9

    #@21b
    if-nez v9, :cond_22

    #@21d
    const-string v9, "001"

    #@21f
    iget-object v10, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mCurCampedMcc:Ljava/lang/String;

    #@221
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@224
    move-result v9

    #@225
    if-nez v9, :cond_22

    #@227
    const-string v9, "000"

    #@229
    iget-object v10, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mCurCampedMcc:Ljava/lang/String;

    #@22b
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@22e
    move-result v9

    #@22f
    if-nez v9, :cond_22

    #@231
    .line 251
    const/4 v9, 0x0

    #@232
    const-string v10, "LTE_ROAMING_SKT"

    #@234
    invoke-static {v9, v10}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@237
    move-result v9

    #@238
    if-eqz v9, :cond_22

    #@23a
    iget-boolean v9, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mFirstImmigration:Z

    #@23c
    if-eqz v9, :cond_22

    #@23e
    .line 252
    iget-object v9, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mContext:Landroid/content/Context;

    #@240
    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@243
    move-result-object v9

    #@244
    const-string v10, "preferred_network_mode"

    #@246
    const/16 v11, 0x9

    #@248
    invoke-static {v9, v10, v11}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@24b
    move-result v7

    #@24c
    .line 255
    .local v7, settingNetworkMode:I
    if-eqz v2, :cond_292

    #@24e
    .line 257
    const/4 v9, 0x0

    #@24f
    const-string v10, "OEM_RAD_DIALER_POPUP"

    #@251
    invoke-static {v9, v10}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@254
    move-result v9

    #@255
    if-eqz v9, :cond_272

    #@257
    .line 258
    const-string v9, "450"

    #@259
    iget-object v10, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mOldCampedMcc:Ljava/lang/String;

    #@25b
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@25e
    move-result v9

    #@25f
    if-eqz v9, :cond_272

    #@261
    .line 259
    iget-object v9, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mContext:Landroid/content/Context;

    #@263
    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@266
    move-result-object v9

    #@267
    const-string v10, "oem_rad_dialer_popup"

    #@269
    const/4 v11, 0x1

    #@26a
    invoke-static {v9, v10, v11}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@26d
    .line 260
    const-string v9, "TelephonyUtils.OEM_RAD_DIALER_POPUP set 1 by SKT scenario"

    #@26f
    invoke-static {v9}, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->log(Ljava/lang/String;)V

    #@272
    .line 264
    :cond_272
    new-instance v9, Ljava/lang/StringBuilder;

    #@274
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@277
    const-string v10, "EVENT_WCDMA_NET_TO_KOREA_CHANGED KEY_LTE_ROAMING_SKT mFirstImmigration = "

    #@279
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27c
    move-result-object v9

    #@27d
    iget-boolean v10, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mFirstImmigration:Z

    #@27f
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@282
    move-result-object v9

    #@283
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@286
    move-result-object v9

    #@287
    invoke-static {v9}, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->log(Ljava/lang/String;)V

    #@28a
    .line 265
    const/4 v9, 0x0

    #@28b
    iput v9, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mDesiredNwMode:I

    #@28d
    .line 266
    invoke-direct {p0, v7}, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->setPreferredNetworkForRoaming(I)V

    #@290
    goto/16 :goto_22

    #@292
    .line 268
    :cond_292
    const/4 v9, 0x1

    #@293
    if-eq v7, v9, :cond_29c

    #@295
    const/4 v9, 0x2

    #@296
    if-eq v7, v9, :cond_29c

    #@298
    const/16 v9, 0xb

    #@29a
    if-ne v7, v9, :cond_2b7

    #@29c
    .line 271
    :cond_29c
    const/4 v9, 0x0

    #@29d
    iput-boolean v9, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mFirstImmigration:Z

    #@29f
    .line 272
    new-instance v9, Ljava/lang/StringBuilder;

    #@2a1
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@2a4
    const-string v10, "EVENT_WCDMA_NET_TO_KOREA_CHANGED Keep the NW mode for engineer test, settingNetworkMode = "

    #@2a6
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a9
    move-result-object v9

    #@2aa
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2ad
    move-result-object v9

    #@2ae
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b1
    move-result-object v9

    #@2b2
    invoke-static {v9}, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->log(Ljava/lang/String;)V

    #@2b5
    goto/16 :goto_22

    #@2b7
    .line 274
    :cond_2b7
    new-instance v9, Ljava/lang/StringBuilder;

    #@2b9
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@2bc
    const-string v10, "EVENT_WCDMA_NET_TO_KOREA_CHANGED KEY_LTE_ROAMING_SKT mFirstImmigration = "

    #@2be
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c1
    move-result-object v9

    #@2c2
    iget-boolean v10, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mFirstImmigration:Z

    #@2c4
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2c7
    move-result-object v9

    #@2c8
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2cb
    move-result-object v9

    #@2cc
    invoke-static {v9}, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->log(Ljava/lang/String;)V

    #@2cf
    .line 275
    const/4 v9, 0x0

    #@2d0
    iput v9, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mDesiredNwMode:I

    #@2d2
    .line 276
    invoke-direct {p0, v7}, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->setPreferredNetworkForRoaming(I)V

    #@2d5
    goto/16 :goto_22

    #@2d7
    .line 283
    .end local v7           #settingNetworkMode:I
    :cond_2d7
    const-string v9, "KR"

    #@2d9
    const-string v10, "KT"

    #@2db
    invoke-static {v9, v10}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@2de
    move-result v9

    #@2df
    if-eqz v9, :cond_22

    #@2e1
    .line 286
    const/4 v9, 0x0

    #@2e2
    const-string v10, "LTE_ROAMING_KT"

    #@2e4
    invoke-static {v9, v10}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@2e7
    move-result v9

    #@2e8
    if-eqz v9, :cond_22

    #@2ea
    .line 287
    const-string v9, "EVENT_WCDMA_NET_TO_KOREA_CHANGED KEY_LTE_ROAMING_KT"

    #@2ec
    invoke-static {v9}, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->log(Ljava/lang/String;)V

    #@2ef
    .line 288
    invoke-direct {p0}, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->checkAndNWModeChange()V

    #@2f2
    goto/16 :goto_22

    #@2f4
    .line 298
    .end local v0           #ar:Landroid/os/AsyncResult;
    .end local v2           #isCampedMccChanged:Z
    .end local v4           #mcc:I
    .end local v6           #rat:I
    :sswitch_2f4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2f6
    check-cast v0, Landroid/os/AsyncResult;

    #@2f8
    .line 299
    .restart local v0       #ar:Landroid/os/AsyncResult;
    iget-object v9, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@2fa
    if-nez v9, :cond_22

    #@2fc
    iget-object v9, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@2fe
    check-cast v9, [I

    #@300
    check-cast v9, [I

    #@302
    array-length v9, v9

    #@303
    const/4 v10, 0x1

    #@304
    if-le v9, v10, :cond_22

    #@306
    .line 300
    iget-object v9, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@308
    check-cast v9, [I

    #@30a
    check-cast v9, [I

    #@30c
    const/4 v10, 0x0

    #@30d
    aget v4, v9, v10

    #@30f
    .line 301
    .restart local v4       #mcc:I
    iget-object v9, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@311
    check-cast v9, [I

    #@313
    check-cast v9, [I

    #@315
    const/4 v10, 0x1

    #@316
    aget v6, v9, v10

    #@318
    .line 302
    .restart local v6       #rat:I
    new-instance v9, Ljava/lang/StringBuilder;

    #@31a
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@31d
    const-string v10, "EVENT_WCDMA_NET_TO_KOREA_CHANGED mcc = "

    #@31f
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@322
    move-result-object v9

    #@323
    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@326
    move-result-object v9

    #@327
    const-string v10, ", rat = "

    #@329
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32c
    move-result-object v9

    #@32d
    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@330
    move-result-object v9

    #@331
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@334
    move-result-object v9

    #@335
    invoke-static {v9}, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->log(Ljava/lang/String;)V

    #@338
    .line 304
    invoke-direct {p0, v4}, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->saveNetworkInfo(I)Z

    #@33b
    move-result v2

    #@33c
    .line 305
    .restart local v2       #isCampedMccChanged:Z
    new-instance v9, Ljava/lang/StringBuilder;

    #@33e
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@341
    const-string v10, "EVENT_WCDMA_NET_TO_KOREA_CHANGED : isCampedMccChanged = "

    #@343
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@346
    move-result-object v9

    #@347
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@34a
    move-result-object v9

    #@34b
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34e
    move-result-object v9

    #@34f
    invoke-static {v9}, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->log(Ljava/lang/String;)V

    #@352
    .line 308
    const-string v9, "persist.radio.isroaming"

    #@354
    const-string v10, "false"

    #@356
    invoke-static {v9, v10}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@359
    .line 312
    if-eqz v2, :cond_3ad

    #@35b
    .line 313
    const-string v9, "450"

    #@35d
    iget-object v10, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mCurCampedMcc:Ljava/lang/String;

    #@35f
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@362
    move-result v9

    #@363
    if-eqz v9, :cond_3ad

    #@365
    const-string v9, "001"

    #@367
    iget-object v10, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mOldCampedMcc:Ljava/lang/String;

    #@369
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@36c
    move-result v9

    #@36d
    if-nez v9, :cond_3ad

    #@36f
    .line 314
    new-instance v1, Landroid/content/Intent;

    #@371
    const-string v9, "com.lge.intent.action.LGE_CAMPED_MCC_CHANGE"

    #@373
    invoke-direct {v1, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@376
    .line 315
    .restart local v1       #intent:Landroid/content/Intent;
    const-string v9, "newmcc"

    #@378
    iget-object v10, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mCurCampedMcc:Ljava/lang/String;

    #@37a
    invoke-virtual {v1, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@37d
    .line 316
    const-string v9, "oldmcc"

    #@37f
    iget-object v10, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mOldCampedMcc:Ljava/lang/String;

    #@381
    invoke-virtual {v1, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@384
    .line 317
    new-instance v9, Ljava/lang/StringBuilder;

    #@386
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@389
    const-string v10, "ACTION_CAMPED_MCC_CHANGE : EXTRA_NEW_MCC = "

    #@38b
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38e
    move-result-object v9

    #@38f
    iget-object v10, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mCurCampedMcc:Ljava/lang/String;

    #@391
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@394
    move-result-object v9

    #@395
    const-string v10, ", EXTRA_OLD_MCC = "

    #@397
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39a
    move-result-object v9

    #@39b
    iget-object v10, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mOldCampedMcc:Ljava/lang/String;

    #@39d
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a0
    move-result-object v9

    #@3a1
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a4
    move-result-object v9

    #@3a5
    invoke-static {v9}, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->log(Ljava/lang/String;)V

    #@3a8
    .line 318
    iget-object v9, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mContext:Landroid/content/Context;

    #@3aa
    invoke-virtual {v9, v1}, Landroid/content/Context;->sendStickyBroadcast(Landroid/content/Intent;)V

    #@3ad
    .line 324
    .end local v1           #intent:Landroid/content/Intent;
    :cond_3ad
    if-eqz v2, :cond_3c9

    #@3af
    const/4 v9, 0x0

    #@3b0
    const-string v10, "OEM_RAD_DIALER_POPUP"

    #@3b2
    invoke-static {v9, v10}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@3b5
    move-result v9

    #@3b6
    if-eqz v9, :cond_3c9

    #@3b8
    .line 325
    iget-object v9, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mContext:Landroid/content/Context;

    #@3ba
    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@3bd
    move-result-object v9

    #@3be
    const-string v10, "oem_rad_dialer_popup"

    #@3c0
    const/4 v11, 0x0

    #@3c1
    invoke-static {v9, v10, v11}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@3c4
    .line 326
    const-string v9, "TelephonyUtils.OEM_RAD_DIALER_POPUP set 0 by SKT scenario"

    #@3c6
    invoke-static {v9}, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->log(Ljava/lang/String;)V

    #@3c9
    .line 331
    :cond_3c9
    const/4 v9, 0x0

    #@3ca
    const-string v10, "LTE_ROAMING_SKT"

    #@3cc
    invoke-static {v9, v10}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@3cf
    move-result v9

    #@3d0
    if-eqz v9, :cond_3da

    #@3d2
    .line 332
    const/4 v9, 0x1

    #@3d3
    iput-boolean v9, p0, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->mFirstImmigration:Z

    #@3d5
    .line 333
    const-string v9, "Set true the mFirstImmigration for next roaming"

    #@3d7
    invoke-static {v9}, Lcom/android/internal/telephony/LgeRoamingEventDispatcher;->log(Ljava/lang/String;)V

    #@3da
    .line 337
    :cond_3da
    const-string v9, "KR"

    #@3dc
    const-string v10, "LGU"

    #@3de
    invoke-static {v9, v10}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@3e1
    move-result v9

    #@3e2
    if-nez v9, :cond_22

    #@3e4
    .line 340
    const-string v9, "KR"

    #@3e6
    const-string v10, "SKT"

    #@3e8
    invoke-static {v9, v10}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@3eb
    move-result v9

    #@3ec
    if-nez v9, :cond_22

    #@3ee
    .line 343
    const-string v9, "KR"

    #@3f0
    const-string v10, "KT"

    #@3f2
    invoke-static {v9, v10}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@3f5
    move-result v9

    #@3f6
    if-eqz v9, :cond_22

    #@3f8
    goto/16 :goto_22

    #@3fa
    .line 139
    :sswitch_data_3fa
    .sparse-switch
        0x64 -> :sswitch_ad
        0x65 -> :sswitch_2f4
        0x3ee -> :sswitch_3f
        0x3ef -> :sswitch_55
        0x44d -> :sswitch_23
    .end sparse-switch
.end method
