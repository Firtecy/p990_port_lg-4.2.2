.class public Lcom/android/internal/telephony/CallModify;
.super Ljava/lang/Object;
.source "CallModify.java"


# static fields
.field public static E_CANCELLED:I

.field public static E_SUCCESS:I

.field public static E_UNUSED:I


# instance fields
.field public call_details:Lcom/android/internal/telephony/CallDetails;

.field public call_index:I

.field public error:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 33
    const/4 v0, 0x0

    #@1
    sput v0, Lcom/android/internal/telephony/CallModify;->E_SUCCESS:I

    #@3
    .line 34
    const/4 v0, 0x7

    #@4
    sput v0, Lcom/android/internal/telephony/CallModify;->E_CANCELLED:I

    #@6
    .line 35
    const/16 v0, 0x10

    #@8
    sput v0, Lcom/android/internal/telephony/CallModify;->E_UNUSED:I

    #@a
    return-void
.end method

.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    .line 44
    new-instance v0, Lcom/android/internal/telephony/CallDetails;

    #@2
    invoke-direct {v0}, Lcom/android/internal/telephony/CallDetails;-><init>()V

    #@5
    const/4 v1, 0x0

    #@6
    invoke-direct {p0, v0, v1}, Lcom/android/internal/telephony/CallModify;-><init>(Lcom/android/internal/telephony/CallDetails;I)V

    #@9
    .line 45
    return-void
.end method

.method public constructor <init>(Lcom/android/internal/telephony/CallDetails;I)V
    .registers 4
    .parameter "callDetails"
    .parameter "callIndex"

    #@0
    .prologue
    .line 48
    sget v0, Lcom/android/internal/telephony/CallModify;->E_SUCCESS:I

    #@2
    invoke-direct {p0, p1, p2, v0}, Lcom/android/internal/telephony/CallModify;-><init>(Lcom/android/internal/telephony/CallDetails;II)V

    #@5
    .line 49
    return-void
.end method

.method public constructor <init>(Lcom/android/internal/telephony/CallDetails;II)V
    .registers 4
    .parameter "callDetails"
    .parameter "callIndex"
    .parameter "err"

    #@0
    .prologue
    .line 51
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 52
    iput-object p1, p0, Lcom/android/internal/telephony/CallModify;->call_details:Lcom/android/internal/telephony/CallDetails;

    #@5
    .line 53
    iput p2, p0, Lcom/android/internal/telephony/CallModify;->call_index:I

    #@7
    .line 54
    iput p3, p0, Lcom/android/internal/telephony/CallModify;->error:I

    #@9
    .line 55
    return-void
.end method


# virtual methods
.method public error()Z
    .registers 3

    #@0
    .prologue
    .line 65
    iget v0, p0, Lcom/android/internal/telephony/CallModify;->error:I

    #@2
    sget v1, Lcom/android/internal/telephony/CallModify;->E_UNUSED:I

    #@4
    if-eq v0, v1, :cond_e

    #@6
    iget v0, p0, Lcom/android/internal/telephony/CallModify;->error:I

    #@8
    sget v1, Lcom/android/internal/telephony/CallModify;->E_SUCCESS:I

    #@a
    if-eq v0, v1, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public setCallDetails(Lcom/android/internal/telephony/CallDetails;)V
    .registers 3
    .parameter "calldetails"

    #@0
    .prologue
    .line 58
    new-instance v0, Lcom/android/internal/telephony/CallDetails;

    #@2
    invoke-direct {v0, p1}, Lcom/android/internal/telephony/CallDetails;-><init>(Lcom/android/internal/telephony/CallDetails;)V

    #@5
    iput-object v0, p0, Lcom/android/internal/telephony/CallModify;->call_details:Lcom/android/internal/telephony/CallDetails;

    #@7
    .line 59
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 73
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, " "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget v1, p0, Lcom/android/internal/telephony/CallModify;->call_index:I

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, " "

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget-object v1, p0, Lcom/android/internal/telephony/CallModify;->call_details:Lcom/android/internal/telephony/CallDetails;

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, " "

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    iget v1, p0, Lcom/android/internal/telephony/CallModify;->error:I

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v0

    #@2d
    return-object v0
.end method
