.class final enum Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;
.super Ljava/lang/Enum;
.source "ApnSettingHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/ApnSettingHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "enum_APN_class"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;

.field public static final enum ENUM_ERROR:Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;

.field public static final enum ENUM_VZW800:Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;

.field public static final enum ENUM_VZWADMIN:Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;

.field public static final enum ENUM_VZWAPP:Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;

.field public static final enum ENUM_VZWIMS:Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;

.field public static final enum ENUM_VZWINTERNET:Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 38
    new-instance v0, Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;

    #@7
    const-string v1, "ENUM_ERROR"

    #@9
    invoke-direct {v0, v1, v3}, Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;-><init>(Ljava/lang/String;I)V

    #@c
    sput-object v0, Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;->ENUM_ERROR:Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;

    #@e
    new-instance v0, Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;

    #@10
    const-string v1, "ENUM_VZWIMS"

    #@12
    invoke-direct {v0, v1, v4}, Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;-><init>(Ljava/lang/String;I)V

    #@15
    sput-object v0, Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;->ENUM_VZWIMS:Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;

    #@17
    new-instance v0, Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;

    #@19
    const-string v1, "ENUM_VZWADMIN"

    #@1b
    invoke-direct {v0, v1, v5}, Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;-><init>(Ljava/lang/String;I)V

    #@1e
    sput-object v0, Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;->ENUM_VZWADMIN:Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;

    #@20
    new-instance v0, Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;

    #@22
    const-string v1, "ENUM_VZWINTERNET"

    #@24
    invoke-direct {v0, v1, v6}, Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;-><init>(Ljava/lang/String;I)V

    #@27
    sput-object v0, Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;->ENUM_VZWINTERNET:Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;

    #@29
    new-instance v0, Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;

    #@2b
    const-string v1, "ENUM_VZWAPP"

    #@2d
    invoke-direct {v0, v1, v7}, Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;-><init>(Ljava/lang/String;I)V

    #@30
    sput-object v0, Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;->ENUM_VZWAPP:Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;

    #@32
    new-instance v0, Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;

    #@34
    const-string v1, "ENUM_VZW800"

    #@36
    const/4 v2, 0x5

    #@37
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;-><init>(Ljava/lang/String;I)V

    #@3a
    sput-object v0, Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;->ENUM_VZW800:Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;

    #@3c
    .line 37
    const/4 v0, 0x6

    #@3d
    new-array v0, v0, [Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;

    #@3f
    sget-object v1, Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;->ENUM_ERROR:Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;

    #@41
    aput-object v1, v0, v3

    #@43
    sget-object v1, Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;->ENUM_VZWIMS:Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;

    #@45
    aput-object v1, v0, v4

    #@47
    sget-object v1, Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;->ENUM_VZWADMIN:Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;

    #@49
    aput-object v1, v0, v5

    #@4b
    sget-object v1, Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;->ENUM_VZWINTERNET:Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;

    #@4d
    aput-object v1, v0, v6

    #@4f
    sget-object v1, Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;->ENUM_VZWAPP:Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;

    #@51
    aput-object v1, v0, v7

    #@53
    const/4 v1, 0x5

    #@54
    sget-object v2, Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;->ENUM_VZW800:Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;

    #@56
    aput-object v2, v0, v1

    #@58
    sput-object v0, Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;->$VALUES:[Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;

    #@5a
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 37
    const-class v0, Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;
    .registers 1

    #@0
    .prologue
    .line 37
    sget-object v0, Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;->$VALUES:[Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;

    #@2
    invoke-virtual {v0}, [Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/android/internal/telephony/ApnSettingHelper$enum_APN_class;

    #@8
    return-object v0
.end method
