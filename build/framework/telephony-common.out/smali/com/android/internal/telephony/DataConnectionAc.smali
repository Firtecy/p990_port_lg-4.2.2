.class public Lcom/android/internal/telephony/DataConnectionAc;
.super Lcom/android/internal/util/AsyncChannel;
.source "DataConnectionAc.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/DataConnectionAc$LinkPropertyChangeAction;
    }
.end annotation


# static fields
.field public static final BASE:I = 0x41000

.field private static final CMD_TO_STRING_COUNT:I = 0x1e

.field private static final DBG:Z = false

.field public static final REQ_ADD_APNCONTEXT:I = 0x41012

.field public static final REQ_GET_APNCONTEXT_LIST:I = 0x41016

.field public static final REQ_GET_APNSETTING:I = 0x41004

.field public static final REQ_GET_CID:I = 0x41002

.field public static final REQ_GET_LINK_CAPABILITIES:I = 0x4100a

.field public static final REQ_GET_LINK_PROPERTIES:I = 0x41006

.field public static final REQ_GET_RECONNECT_INTENT:I = 0x4101a

.field public static final REQ_GET_REFCOUNT:I = 0x41010

.field public static final REQ_IS_INACTIVE:I = 0x41000

.field public static final REQ_REMOVE_APNCONTEXT:I = 0x41014

.field public static final REQ_RESET:I = 0x4100e

.field public static final REQ_SET_LINK_PROPERTIES_HTTP_PROXY:I = 0x41008

.field public static final REQ_SET_RECONNECT_INTENT:I = 0x41018

.field public static final REQ_UPDATE_LINK_PROPERTIES_DATA_CALL_STATE:I = 0x4100c

.field public static final REQ_UPDATE_PCSCF_ADDRESS:I = 0x4101c

.field public static final RSP_ADD_APNCONTEXT:I = 0x41013

.field public static final RSP_GET_APNCONTEXT_LIST:I = 0x41017

.field public static final RSP_GET_APNSETTING:I = 0x41005

.field public static final RSP_GET_CID:I = 0x41003

.field public static final RSP_GET_LINK_CAPABILITIES:I = 0x4100b

.field public static final RSP_GET_LINK_PROPERTIES:I = 0x41007

.field public static final RSP_GET_RECONNECT_INTENT:I = 0x4101b

.field public static final RSP_GET_REFCOUNT:I = 0x41011

.field public static final RSP_IS_INACTIVE:I = 0x41001

.field public static final RSP_REMOVE_APNCONTEXT:I = 0x41015

.field public static final RSP_RESET:I = 0x4100f

.field public static final RSP_SET_LINK_PROPERTIES_HTTP_PROXY:I = 0x41009

.field public static final RSP_SET_RECONNECT_INTENT:I = 0x41019

.field public static final RSP_UPDATE_LINK_PROPERTIES_DATA_CALL_STATE:I = 0x4100d

.field public static final RSP_UPDATE_PCSCF_ADDRESS:I = 0x4101d

.field private static sCmdToString:[Ljava/lang/String;


# instance fields
.field public dataConnection:Lcom/android/internal/telephony/DataConnection;

.field private mLogTag:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 95
    const/16 v0, 0x1e

    #@2
    new-array v0, v0, [Ljava/lang/String;

    #@4
    sput-object v0, Lcom/android/internal/telephony/DataConnectionAc;->sCmdToString:[Ljava/lang/String;

    #@6
    .line 97
    sget-object v0, Lcom/android/internal/telephony/DataConnectionAc;->sCmdToString:[Ljava/lang/String;

    #@8
    const/4 v1, 0x0

    #@9
    const-string v2, "REQ_IS_INACTIVE"

    #@b
    aput-object v2, v0, v1

    #@d
    .line 98
    sget-object v0, Lcom/android/internal/telephony/DataConnectionAc;->sCmdToString:[Ljava/lang/String;

    #@f
    const/4 v1, 0x1

    #@10
    const-string v2, "RSP_IS_INACTIVE"

    #@12
    aput-object v2, v0, v1

    #@14
    .line 99
    sget-object v0, Lcom/android/internal/telephony/DataConnectionAc;->sCmdToString:[Ljava/lang/String;

    #@16
    const/4 v1, 0x2

    #@17
    const-string v2, "REQ_GET_CID"

    #@19
    aput-object v2, v0, v1

    #@1b
    .line 100
    sget-object v0, Lcom/android/internal/telephony/DataConnectionAc;->sCmdToString:[Ljava/lang/String;

    #@1d
    const/4 v1, 0x3

    #@1e
    const-string v2, "RSP_GET_CID"

    #@20
    aput-object v2, v0, v1

    #@22
    .line 101
    sget-object v0, Lcom/android/internal/telephony/DataConnectionAc;->sCmdToString:[Ljava/lang/String;

    #@24
    const/4 v1, 0x4

    #@25
    const-string v2, "REQ_GET_APNSETTING"

    #@27
    aput-object v2, v0, v1

    #@29
    .line 102
    sget-object v0, Lcom/android/internal/telephony/DataConnectionAc;->sCmdToString:[Ljava/lang/String;

    #@2b
    const/4 v1, 0x5

    #@2c
    const-string v2, "RSP_GET_APNSETTING"

    #@2e
    aput-object v2, v0, v1

    #@30
    .line 103
    sget-object v0, Lcom/android/internal/telephony/DataConnectionAc;->sCmdToString:[Ljava/lang/String;

    #@32
    const/4 v1, 0x6

    #@33
    const-string v2, "REQ_GET_LINK_PROPERTIES"

    #@35
    aput-object v2, v0, v1

    #@37
    .line 104
    sget-object v0, Lcom/android/internal/telephony/DataConnectionAc;->sCmdToString:[Ljava/lang/String;

    #@39
    const/4 v1, 0x7

    #@3a
    const-string v2, "RSP_GET_LINK_PROPERTIES"

    #@3c
    aput-object v2, v0, v1

    #@3e
    .line 105
    sget-object v0, Lcom/android/internal/telephony/DataConnectionAc;->sCmdToString:[Ljava/lang/String;

    #@40
    const/16 v1, 0x8

    #@42
    const-string v2, "REQ_SET_LINK_PROPERTIES_HTTP_PROXY"

    #@44
    aput-object v2, v0, v1

    #@46
    .line 107
    sget-object v0, Lcom/android/internal/telephony/DataConnectionAc;->sCmdToString:[Ljava/lang/String;

    #@48
    const/16 v1, 0x9

    #@4a
    const-string v2, "RSP_SET_LINK_PROPERTIES_HTTP_PROXY"

    #@4c
    aput-object v2, v0, v1

    #@4e
    .line 109
    sget-object v0, Lcom/android/internal/telephony/DataConnectionAc;->sCmdToString:[Ljava/lang/String;

    #@50
    const/16 v1, 0xa

    #@52
    const-string v2, "REQ_GET_LINK_CAPABILITIES"

    #@54
    aput-object v2, v0, v1

    #@56
    .line 110
    sget-object v0, Lcom/android/internal/telephony/DataConnectionAc;->sCmdToString:[Ljava/lang/String;

    #@58
    const/16 v1, 0xb

    #@5a
    const-string v2, "RSP_GET_LINK_CAPABILITIES"

    #@5c
    aput-object v2, v0, v1

    #@5e
    .line 111
    sget-object v0, Lcom/android/internal/telephony/DataConnectionAc;->sCmdToString:[Ljava/lang/String;

    #@60
    const/16 v1, 0xc

    #@62
    const-string v2, "REQ_UPDATE_LINK_PROPERTIES_DATA_CALL_STATE"

    #@64
    aput-object v2, v0, v1

    #@66
    .line 113
    sget-object v0, Lcom/android/internal/telephony/DataConnectionAc;->sCmdToString:[Ljava/lang/String;

    #@68
    const/16 v1, 0xd

    #@6a
    const-string v2, "RSP_UPDATE_LINK_PROPERTIES_DATA_CALL_STATE"

    #@6c
    aput-object v2, v0, v1

    #@6e
    .line 115
    sget-object v0, Lcom/android/internal/telephony/DataConnectionAc;->sCmdToString:[Ljava/lang/String;

    #@70
    const/16 v1, 0xe

    #@72
    const-string v2, "REQ_RESET"

    #@74
    aput-object v2, v0, v1

    #@76
    .line 116
    sget-object v0, Lcom/android/internal/telephony/DataConnectionAc;->sCmdToString:[Ljava/lang/String;

    #@78
    const/16 v1, 0xf

    #@7a
    const-string v2, "RSP_RESET"

    #@7c
    aput-object v2, v0, v1

    #@7e
    .line 117
    sget-object v0, Lcom/android/internal/telephony/DataConnectionAc;->sCmdToString:[Ljava/lang/String;

    #@80
    const/16 v1, 0x10

    #@82
    const-string v2, "REQ_GET_REFCOUNT"

    #@84
    aput-object v2, v0, v1

    #@86
    .line 118
    sget-object v0, Lcom/android/internal/telephony/DataConnectionAc;->sCmdToString:[Ljava/lang/String;

    #@88
    const/16 v1, 0x11

    #@8a
    const-string v2, "RSP_GET_REFCOUNT"

    #@8c
    aput-object v2, v0, v1

    #@8e
    .line 119
    sget-object v0, Lcom/android/internal/telephony/DataConnectionAc;->sCmdToString:[Ljava/lang/String;

    #@90
    const/16 v1, 0x12

    #@92
    const-string v2, "REQ_ADD_APNCONTEXT"

    #@94
    aput-object v2, v0, v1

    #@96
    .line 120
    sget-object v0, Lcom/android/internal/telephony/DataConnectionAc;->sCmdToString:[Ljava/lang/String;

    #@98
    const/16 v1, 0x13

    #@9a
    const-string v2, "RSP_ADD_APNCONTEXT"

    #@9c
    aput-object v2, v0, v1

    #@9e
    .line 121
    sget-object v0, Lcom/android/internal/telephony/DataConnectionAc;->sCmdToString:[Ljava/lang/String;

    #@a0
    const/16 v1, 0x14

    #@a2
    const-string v2, "REQ_REMOVE_APNCONTEXT"

    #@a4
    aput-object v2, v0, v1

    #@a6
    .line 122
    sget-object v0, Lcom/android/internal/telephony/DataConnectionAc;->sCmdToString:[Ljava/lang/String;

    #@a8
    const/16 v1, 0x15

    #@aa
    const-string v2, "RSP_REMOVE_APNCONTEXT"

    #@ac
    aput-object v2, v0, v1

    #@ae
    .line 123
    sget-object v0, Lcom/android/internal/telephony/DataConnectionAc;->sCmdToString:[Ljava/lang/String;

    #@b0
    const/16 v1, 0x16

    #@b2
    const-string v2, "REQ_GET_APNCONTEXT_LIST"

    #@b4
    aput-object v2, v0, v1

    #@b6
    .line 124
    sget-object v0, Lcom/android/internal/telephony/DataConnectionAc;->sCmdToString:[Ljava/lang/String;

    #@b8
    const/16 v1, 0x17

    #@ba
    const-string v2, "RSP_GET_APNCONTEXT_LIST"

    #@bc
    aput-object v2, v0, v1

    #@be
    .line 125
    sget-object v0, Lcom/android/internal/telephony/DataConnectionAc;->sCmdToString:[Ljava/lang/String;

    #@c0
    const/16 v1, 0x18

    #@c2
    const-string v2, "REQ_SET_RECONNECT_INTENT"

    #@c4
    aput-object v2, v0, v1

    #@c6
    .line 126
    sget-object v0, Lcom/android/internal/telephony/DataConnectionAc;->sCmdToString:[Ljava/lang/String;

    #@c8
    const/16 v1, 0x19

    #@ca
    const-string v2, "RSP_SET_RECONNECT_INTENT"

    #@cc
    aput-object v2, v0, v1

    #@ce
    .line 127
    sget-object v0, Lcom/android/internal/telephony/DataConnectionAc;->sCmdToString:[Ljava/lang/String;

    #@d0
    const/16 v1, 0x1a

    #@d2
    const-string v2, "REQ_GET_RECONNECT_INTENT"

    #@d4
    aput-object v2, v0, v1

    #@d6
    .line 128
    sget-object v0, Lcom/android/internal/telephony/DataConnectionAc;->sCmdToString:[Ljava/lang/String;

    #@d8
    const/16 v1, 0x1b

    #@da
    const-string v2, "RSP_GET_RECONNECT_INTENT"

    #@dc
    aput-object v2, v0, v1

    #@de
    .line 130
    sget-object v0, Lcom/android/internal/telephony/DataConnectionAc;->sCmdToString:[Ljava/lang/String;

    #@e0
    const/16 v1, 0x1c

    #@e2
    const-string v2, "REQ_UPDATE_PCSCF_ADDRESS"

    #@e4
    aput-object v2, v0, v1

    #@e6
    .line 131
    sget-object v0, Lcom/android/internal/telephony/DataConnectionAc;->sCmdToString:[Ljava/lang/String;

    #@e8
    const/16 v1, 0x1d

    #@ea
    const-string v2, "RSP_UPDATE_PCSCF_ADDRESS"

    #@ec
    aput-object v2, v0, v1

    #@ee
    .line 133
    return-void
.end method

.method public constructor <init>(Lcom/android/internal/telephony/DataConnection;Ljava/lang/String;)V
    .registers 3
    .parameter "dc"
    .parameter "logTag"

    #@0
    .prologue
    .line 163
    invoke-direct {p0}, Lcom/android/internal/util/AsyncChannel;-><init>()V

    #@3
    .line 164
    iput-object p1, p0, Lcom/android/internal/telephony/DataConnectionAc;->dataConnection:Lcom/android/internal/telephony/DataConnection;

    #@5
    .line 165
    iput-object p2, p0, Lcom/android/internal/telephony/DataConnectionAc;->mLogTag:Ljava/lang/String;

    #@7
    .line 166
    return-void
.end method

.method protected static cmdToString(I)Ljava/lang/String;
    .registers 3
    .parameter "cmd"

    #@0
    .prologue
    const v1, 0x41000

    #@3
    .line 135
    sub-int/2addr p0, v1

    #@4
    .line 136
    if-ltz p0, :cond_10

    #@6
    sget-object v0, Lcom/android/internal/telephony/DataConnectionAc;->sCmdToString:[Ljava/lang/String;

    #@8
    array-length v0, v0

    #@9
    if-ge p0, v0, :cond_10

    #@b
    .line 137
    sget-object v0, Lcom/android/internal/telephony/DataConnectionAc;->sCmdToString:[Ljava/lang/String;

    #@d
    aget-object v0, v0, p0

    #@f
    .line 139
    :goto_f
    return-object v0

    #@10
    :cond_10
    add-int v0, p0, v1

    #@12
    invoke-static {v0}, Lcom/android/internal/util/AsyncChannel;->cmdToString(I)Ljava/lang/String;

    #@15
    move-result-object v0

    #@16
    goto :goto_f
.end method

.method private log(Ljava/lang/String;)V
    .registers 5
    .parameter "s"

    #@0
    .prologue
    .line 622
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionAc;->mLogTag:Ljava/lang/String;

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "DataConnectionAc "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 623
    return-void
.end method


# virtual methods
.method public addApnContextSync(Lcom/android/internal/telephony/ApnContext;)V
    .registers 5
    .parameter "apnContext"

    #@0
    .prologue
    .line 467
    const v1, 0x41012

    #@3
    invoke-virtual {p0, v1, p1}, Lcom/android/internal/telephony/DataConnectionAc;->sendMessageSynchronously(ILjava/lang/Object;)Landroid/os/Message;

    #@6
    move-result-object v0

    #@7
    .line 468
    .local v0, response:Landroid/os/Message;
    if-eqz v0, :cond_11

    #@9
    iget v1, v0, Landroid/os/Message;->what:I

    #@b
    const v2, 0x41013

    #@e
    if-ne v1, v2, :cond_11

    #@10
    .line 473
    :goto_10
    return-void

    #@11
    .line 471
    :cond_11
    new-instance v1, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v2, "addApnContext error response="

    #@18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v1

    #@24
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/DataConnectionAc;->log(Ljava/lang/String;)V

    #@27
    goto :goto_10
.end method

.method public getApnListSync()Ljava/util/Collection;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/internal/telephony/ApnContext;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 526
    const v1, 0x41016

    #@3
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionAc;->sendMessageSynchronously(I)Landroid/os/Message;

    #@6
    move-result-object v0

    #@7
    .line 527
    .local v0, response:Landroid/os/Message;
    if-eqz v0, :cond_15

    #@9
    iget v1, v0, Landroid/os/Message;->what:I

    #@b
    const v2, 0x41017

    #@e
    if-ne v1, v2, :cond_15

    #@10
    .line 529
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionAc;->rspApnList(Landroid/os/Message;)Ljava/util/Collection;

    #@13
    move-result-object v1

    #@14
    .line 533
    :goto_14
    return-object v1

    #@15
    .line 531
    :cond_15
    new-instance v1, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v2, "getApnList error response="

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v1

    #@24
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v1

    #@28
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/DataConnectionAc;->log(Ljava/lang/String;)V

    #@2b
    .line 533
    new-instance v1, Ljava/util/ArrayList;

    #@2d
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@30
    goto :goto_14
.end method

.method public getApnSettingSync()Lcom/android/internal/telephony/DataProfile;
    .registers 4

    #@0
    .prologue
    .line 296
    const v1, 0x41004

    #@3
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionAc;->sendMessageSynchronously(I)Landroid/os/Message;

    #@6
    move-result-object v0

    #@7
    .line 297
    .local v0, response:Landroid/os/Message;
    if-eqz v0, :cond_15

    #@9
    iget v1, v0, Landroid/os/Message;->what:I

    #@b
    const v2, 0x41005

    #@e
    if-ne v1, v2, :cond_15

    #@10
    .line 298
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionAc;->rspApnSetting(Landroid/os/Message;)Lcom/android/internal/telephony/DataProfile;

    #@13
    move-result-object v1

    #@14
    .line 301
    :goto_14
    return-object v1

    #@15
    .line 300
    :cond_15
    new-instance v1, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v2, "getApnSetting error response="

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v1

    #@24
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v1

    #@28
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/DataConnectionAc;->log(Ljava/lang/String;)V

    #@2b
    .line 301
    const/4 v1, 0x0

    #@2c
    goto :goto_14
.end method

.method public getCidSync()I
    .registers 4

    #@0
    .prologue
    .line 226
    const v1, 0x41002

    #@3
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionAc;->sendMessageSynchronously(I)Landroid/os/Message;

    #@6
    move-result-object v0

    #@7
    .line 227
    .local v0, response:Landroid/os/Message;
    if-eqz v0, :cond_15

    #@9
    iget v1, v0, Landroid/os/Message;->what:I

    #@b
    const v2, 0x41003

    #@e
    if-ne v1, v2, :cond_15

    #@10
    .line 228
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionAc;->rspCid(Landroid/os/Message;)I

    #@13
    move-result v1

    #@14
    .line 231
    :goto_14
    return v1

    #@15
    .line 230
    :cond_15
    new-instance v1, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v2, "rspCid error response="

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v1

    #@24
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v1

    #@28
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/DataConnectionAc;->log(Ljava/lang/String;)V

    #@2b
    .line 231
    const/4 v1, -0x1

    #@2c
    goto :goto_14
.end method

.method public getLinkCapabilitiesSync()Landroid/net/LinkCapabilities;
    .registers 4

    #@0
    .prologue
    .line 422
    const v1, 0x4100a

    #@3
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionAc;->sendMessageSynchronously(I)Landroid/os/Message;

    #@6
    move-result-object v0

    #@7
    .line 423
    .local v0, response:Landroid/os/Message;
    if-eqz v0, :cond_15

    #@9
    iget v1, v0, Landroid/os/Message;->what:I

    #@b
    const v2, 0x4100b

    #@e
    if-ne v1, v2, :cond_15

    #@10
    .line 424
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionAc;->rspLinkCapabilities(Landroid/os/Message;)Landroid/net/LinkCapabilities;

    #@13
    move-result-object v1

    #@14
    .line 427
    :goto_14
    return-object v1

    #@15
    .line 426
    :cond_15
    new-instance v1, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v2, "getLinkCapabilities error response="

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v1

    #@24
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v1

    #@28
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/DataConnectionAc;->log(Ljava/lang/String;)V

    #@2b
    .line 427
    const/4 v1, 0x0

    #@2c
    goto :goto_14
.end method

.method public getLinkPropertiesSync()Landroid/net/LinkProperties;
    .registers 4

    #@0
    .prologue
    .line 332
    const v1, 0x41006

    #@3
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionAc;->sendMessageSynchronously(I)Landroid/os/Message;

    #@6
    move-result-object v0

    #@7
    .line 333
    .local v0, response:Landroid/os/Message;
    if-eqz v0, :cond_15

    #@9
    iget v1, v0, Landroid/os/Message;->what:I

    #@b
    const v2, 0x41007

    #@e
    if-ne v1, v2, :cond_15

    #@10
    .line 334
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionAc;->rspLinkProperties(Landroid/os/Message;)Landroid/net/LinkProperties;

    #@13
    move-result-object v1

    #@14
    .line 337
    :goto_14
    return-object v1

    #@15
    .line 336
    :cond_15
    new-instance v1, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v2, "getLinkProperties error response="

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v1

    #@24
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v1

    #@28
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/DataConnectionAc;->log(Ljava/lang/String;)V

    #@2b
    .line 337
    const/4 v1, 0x0

    #@2c
    goto :goto_14
.end method

.method public getReconnectIntentSync()Landroid/app/PendingIntent;
    .registers 4

    #@0
    .prologue
    .line 586
    const v1, 0x4101a

    #@3
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionAc;->sendMessageSynchronously(I)Landroid/os/Message;

    #@6
    move-result-object v0

    #@7
    .line 587
    .local v0, response:Landroid/os/Message;
    if-eqz v0, :cond_15

    #@9
    iget v1, v0, Landroid/os/Message;->what:I

    #@b
    const v2, 0x4101b

    #@e
    if-ne v1, v2, :cond_15

    #@10
    .line 589
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionAc;->rspReconnectIntent(Landroid/os/Message;)Landroid/app/PendingIntent;

    #@13
    move-result-object v1

    #@14
    .line 592
    :goto_14
    return-object v1

    #@15
    .line 591
    :cond_15
    new-instance v1, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v2, "getReconnectIntent error response="

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v1

    #@24
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v1

    #@28
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/DataConnectionAc;->log(Ljava/lang/String;)V

    #@2b
    .line 592
    const/4 v1, 0x0

    #@2c
    goto :goto_14
.end method

.method public getRefCountSync()I
    .registers 4

    #@0
    .prologue
    .line 260
    const v1, 0x41010

    #@3
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionAc;->sendMessageSynchronously(I)Landroid/os/Message;

    #@6
    move-result-object v0

    #@7
    .line 261
    .local v0, response:Landroid/os/Message;
    if-eqz v0, :cond_15

    #@9
    iget v1, v0, Landroid/os/Message;->what:I

    #@b
    const v2, 0x41011

    #@e
    if-ne v1, v2, :cond_15

    #@10
    .line 262
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionAc;->rspRefCount(Landroid/os/Message;)I

    #@13
    move-result v1

    #@14
    .line 265
    :goto_14
    return v1

    #@15
    .line 264
    :cond_15
    new-instance v1, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v2, "rspRefCount error response="

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v1

    #@24
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v1

    #@28
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/DataConnectionAc;->log(Ljava/lang/String;)V

    #@2b
    .line 265
    const/4 v1, -0x1

    #@2c
    goto :goto_14
.end method

.method public isInactiveSync()Z
    .registers 4

    #@0
    .prologue
    .line 192
    const v1, 0x41000

    #@3
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionAc;->sendMessageSynchronously(I)Landroid/os/Message;

    #@6
    move-result-object v0

    #@7
    .line 193
    .local v0, response:Landroid/os/Message;
    if-eqz v0, :cond_15

    #@9
    iget v1, v0, Landroid/os/Message;->what:I

    #@b
    const v2, 0x41001

    #@e
    if-ne v1, v2, :cond_15

    #@10
    .line 194
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionAc;->rspIsInactive(Landroid/os/Message;)Z

    #@13
    move-result v1

    #@14
    .line 197
    :goto_14
    return v1

    #@15
    .line 196
    :cond_15
    new-instance v1, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v2, "rspIsInactive error response="

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v1

    #@24
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v1

    #@28
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/DataConnectionAc;->log(Ljava/lang/String;)V

    #@2b
    .line 197
    const/4 v1, 0x0

    #@2c
    goto :goto_14
.end method

.method public removeApnContextSync(Lcom/android/internal/telephony/ApnContext;)V
    .registers 5
    .parameter "apnContext"

    #@0
    .prologue
    .line 490
    const v1, 0x41014

    #@3
    invoke-virtual {p0, v1, p1}, Lcom/android/internal/telephony/DataConnectionAc;->sendMessageSynchronously(ILjava/lang/Object;)Landroid/os/Message;

    #@6
    move-result-object v0

    #@7
    .line 491
    .local v0, response:Landroid/os/Message;
    if-eqz v0, :cond_11

    #@9
    iget v1, v0, Landroid/os/Message;->what:I

    #@b
    const v2, 0x41015

    #@e
    if-ne v1, v2, :cond_11

    #@10
    .line 496
    :goto_10
    return-void

    #@11
    .line 494
    :cond_11
    new-instance v1, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v2, "removeApnContext error response="

    #@18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v1

    #@24
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/DataConnectionAc;->log(Ljava/lang/String;)V

    #@27
    goto :goto_10
.end method

.method public reqAddApnContext(Lcom/android/internal/telephony/ApnContext;)V
    .registers 4
    .parameter "apnContext"

    #@0
    .prologue
    .line 457
    const v1, 0x41012

    #@3
    invoke-virtual {p0, v1, p1}, Lcom/android/internal/telephony/DataConnectionAc;->sendMessageSynchronously(ILjava/lang/Object;)Landroid/os/Message;

    #@6
    move-result-object v0

    #@7
    .line 459
    .local v0, response:Landroid/os/Message;
    return-void
.end method

.method public reqApnSetting()V
    .registers 2

    #@0
    .prologue
    .line 274
    const v0, 0x41004

    #@3
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionAc;->sendMessage(I)V

    #@6
    .line 276
    return-void
.end method

.method public reqCid()V
    .registers 2

    #@0
    .prologue
    .line 206
    const v0, 0x41002

    #@3
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionAc;->sendMessage(I)V

    #@6
    .line 208
    return-void
.end method

.method public reqGetApnList(Lcom/android/internal/telephony/ApnContext;)V
    .registers 4
    .parameter "apnContext"

    #@0
    .prologue
    .line 503
    const v1, 0x41016

    #@3
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionAc;->sendMessageSynchronously(I)Landroid/os/Message;

    #@6
    move-result-object v0

    #@7
    .line 505
    .local v0, response:Landroid/os/Message;
    return-void
.end method

.method public reqGetReconnectIntent()V
    .registers 3

    #@0
    .prologue
    .line 565
    const v1, 0x4101a

    #@3
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionAc;->sendMessageSynchronously(I)Landroid/os/Message;

    #@6
    move-result-object v0

    #@7
    .line 567
    .local v0, response:Landroid/os/Message;
    return-void
.end method

.method public reqIsInactive()V
    .registers 2

    #@0
    .prologue
    .line 173
    const v0, 0x41000

    #@3
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionAc;->sendMessage(I)V

    #@6
    .line 175
    return-void
.end method

.method public reqLinkCapabilities()V
    .registers 2

    #@0
    .prologue
    .line 400
    const v0, 0x4100a

    #@3
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionAc;->sendMessage(I)V

    #@6
    .line 402
    return-void
.end method

.method public reqLinkProperties()V
    .registers 2

    #@0
    .prologue
    .line 310
    const v0, 0x41006

    #@3
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionAc;->sendMessage(I)V

    #@6
    .line 312
    return-void
.end method

.method public reqRefCount()V
    .registers 2

    #@0
    .prologue
    .line 240
    const v0, 0x41010

    #@3
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionAc;->sendMessage(I)V

    #@6
    .line 242
    return-void
.end method

.method public reqRemomveApnContext(Lcom/android/internal/telephony/ApnContext;)V
    .registers 4
    .parameter "apnContext"

    #@0
    .prologue
    .line 480
    const v1, 0x41014

    #@3
    invoke-virtual {p0, v1, p1}, Lcom/android/internal/telephony/DataConnectionAc;->sendMessageSynchronously(ILjava/lang/Object;)Landroid/os/Message;

    #@6
    move-result-object v0

    #@7
    .line 482
    .local v0, response:Landroid/os/Message;
    return-void
.end method

.method public reqReset()V
    .registers 2

    #@0
    .prologue
    .line 436
    const v0, 0x4100e

    #@3
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionAc;->sendMessage(I)V

    #@6
    .line 438
    return-void
.end method

.method public reqSetLinkPropertiesHttpProxy(Landroid/net/ProxyProperties;)V
    .registers 3
    .parameter "proxy"

    #@0
    .prologue
    .line 346
    const v0, 0x41008

    #@3
    invoke-virtual {p0, v0, p1}, Lcom/android/internal/telephony/DataConnectionAc;->sendMessage(ILjava/lang/Object;)V

    #@6
    .line 348
    return-void
.end method

.method public reqSetReconnectIntent(Landroid/app/PendingIntent;)V
    .registers 4
    .parameter "intent"

    #@0
    .prologue
    .line 542
    const v1, 0x41018

    #@3
    invoke-virtual {p0, v1, p1}, Lcom/android/internal/telephony/DataConnectionAc;->sendMessageSynchronously(ILjava/lang/Object;)Landroid/os/Message;

    #@6
    move-result-object v0

    #@7
    .line 544
    .local v0, response:Landroid/os/Message;
    return-void
.end method

.method public reqUpdateLinkPropertiesDataCallState(Lcom/android/internal/telephony/DataCallState;)V
    .registers 3
    .parameter "newState"

    #@0
    .prologue
    .line 368
    const v0, 0x4100c

    #@3
    invoke-virtual {p0, v0, p1}, Lcom/android/internal/telephony/DataConnectionAc;->sendMessage(ILjava/lang/Object;)V

    #@6
    .line 370
    return-void
.end method

.method public reqUpdatePcscfAddress()Z
    .registers 2

    #@0
    .prologue
    .line 598
    const v0, 0x4101c

    #@3
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionAc;->sendMessage(I)V

    #@6
    .line 599
    const/4 v0, 0x1

    #@7
    return v0
.end method

.method public resetSync()V
    .registers 4

    #@0
    .prologue
    .line 444
    const v1, 0x4100e

    #@3
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/DataConnectionAc;->sendMessageSynchronously(I)Landroid/os/Message;

    #@6
    move-result-object v0

    #@7
    .line 445
    .local v0, response:Landroid/os/Message;
    if-eqz v0, :cond_11

    #@9
    iget v1, v0, Landroid/os/Message;->what:I

    #@b
    const v2, 0x4100f

    #@e
    if-ne v1, v2, :cond_11

    #@10
    .line 450
    :goto_10
    return-void

    #@11
    .line 448
    :cond_11
    new-instance v1, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v2, "restSync error response="

    #@18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v1

    #@24
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/DataConnectionAc;->log(Ljava/lang/String;)V

    #@27
    goto :goto_10
.end method

.method public rspApnList(Landroid/os/Message;)Ljava/util/Collection;
    .registers 3
    .parameter "response"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Message;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/internal/telephony/ApnContext;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 514
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2
    check-cast v0, Ljava/util/Collection;

    #@4
    .line 515
    .local v0, retVal:Ljava/util/Collection;,"Ljava/util/Collection<Lcom/android/internal/telephony/ApnContext;>;"
    if-nez v0, :cond_b

    #@6
    new-instance v0, Ljava/util/ArrayList;

    #@8
    .end local v0           #retVal:Ljava/util/Collection;,"Ljava/util/Collection<Lcom/android/internal/telephony/ApnContext;>;"
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@b
    .line 516
    .restart local v0       #retVal:Ljava/util/Collection;,"Ljava/util/Collection<Lcom/android/internal/telephony/ApnContext;>;"
    :cond_b
    return-object v0
.end method

.method public rspApnSetting(Landroid/os/Message;)Lcom/android/internal/telephony/DataProfile;
    .registers 3
    .parameter "response"

    #@0
    .prologue
    .line 285
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2
    check-cast v0, Lcom/android/internal/telephony/DataProfile;

    #@4
    .line 287
    .local v0, retVal:Lcom/android/internal/telephony/DataProfile;
    return-object v0
.end method

.method public rspCid(Landroid/os/Message;)I
    .registers 3
    .parameter "response"

    #@0
    .prologue
    .line 217
    iget v0, p1, Landroid/os/Message;->arg1:I

    #@2
    .line 219
    .local v0, retVal:I
    return v0
.end method

.method public rspIsInactive(Landroid/os/Message;)Z
    .registers 4
    .parameter "response"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 183
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@3
    if-ne v1, v0, :cond_6

    #@5
    .line 185
    .local v0, retVal:Z
    :goto_5
    return v0

    #@6
    .line 183
    .end local v0           #retVal:Z
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public rspLinkCapabilities(Landroid/os/Message;)Landroid/net/LinkCapabilities;
    .registers 3
    .parameter "response"

    #@0
    .prologue
    .line 411
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2
    check-cast v0, Landroid/net/LinkCapabilities;

    #@4
    .line 413
    .local v0, retVal:Landroid/net/LinkCapabilities;
    return-object v0
.end method

.method public rspLinkProperties(Landroid/os/Message;)Landroid/net/LinkProperties;
    .registers 3
    .parameter "response"

    #@0
    .prologue
    .line 321
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2
    check-cast v0, Landroid/net/LinkProperties;

    #@4
    .line 323
    .local v0, retVal:Landroid/net/LinkProperties;
    return-object v0
.end method

.method public rspReconnectIntent(Landroid/os/Message;)Landroid/app/PendingIntent;
    .registers 3
    .parameter "response"

    #@0
    .prologue
    .line 576
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2
    check-cast v0, Landroid/app/PendingIntent;

    #@4
    .line 577
    .local v0, retVal:Landroid/app/PendingIntent;
    return-object v0
.end method

.method public rspRefCount(Landroid/os/Message;)I
    .registers 3
    .parameter "response"

    #@0
    .prologue
    .line 251
    iget v0, p1, Landroid/os/Message;->arg1:I

    #@2
    .line 253
    .local v0, retVal:I
    return v0
.end method

.method public rspUpdateLinkPropertiesDataCallState(Landroid/os/Message;)Lcom/android/internal/telephony/DataConnection$UpdateLinkPropertyResult;
    .registers 3
    .parameter "response"

    #@0
    .prologue
    .line 373
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2
    check-cast v0, Lcom/android/internal/telephony/DataConnection$UpdateLinkPropertyResult;

    #@4
    .line 375
    .local v0, retVal:Lcom/android/internal/telephony/DataConnection$UpdateLinkPropertyResult;
    return-object v0
.end method

.method public setLinkPropertiesHttpProxySync(Landroid/net/ProxyProperties;)V
    .registers 5
    .parameter "proxy"

    #@0
    .prologue
    .line 354
    const v1, 0x41008

    #@3
    invoke-virtual {p0, v1, p1}, Lcom/android/internal/telephony/DataConnectionAc;->sendMessageSynchronously(ILjava/lang/Object;)Landroid/os/Message;

    #@6
    move-result-object v0

    #@7
    .line 356
    .local v0, response:Landroid/os/Message;
    if-eqz v0, :cond_11

    #@9
    iget v1, v0, Landroid/os/Message;->what:I

    #@b
    const v2, 0x41009

    #@e
    if-ne v1, v2, :cond_11

    #@10
    .line 361
    :goto_10
    return-void

    #@11
    .line 359
    :cond_11
    new-instance v1, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v2, "setLinkPropertiesHttpPoxy error response="

    #@18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v1

    #@24
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/DataConnectionAc;->log(Ljava/lang/String;)V

    #@27
    goto :goto_10
.end method

.method public setReconnectIntentSync(Landroid/app/PendingIntent;)V
    .registers 5
    .parameter "intent"

    #@0
    .prologue
    .line 552
    const v1, 0x41018

    #@3
    invoke-virtual {p0, v1, p1}, Lcom/android/internal/telephony/DataConnectionAc;->sendMessageSynchronously(ILjava/lang/Object;)Landroid/os/Message;

    #@6
    move-result-object v0

    #@7
    .line 553
    .local v0, response:Landroid/os/Message;
    if-eqz v0, :cond_11

    #@9
    iget v1, v0, Landroid/os/Message;->what:I

    #@b
    const v2, 0x41019

    #@e
    if-ne v1, v2, :cond_11

    #@10
    .line 558
    :goto_10
    return-void

    #@11
    .line 556
    :cond_11
    new-instance v1, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v2, "setReconnectIntent error response="

    #@18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v1

    #@24
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/DataConnectionAc;->log(Ljava/lang/String;)V

    #@27
    goto :goto_10
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 618
    iget-object v0, p0, Lcom/android/internal/telephony/DataConnectionAc;->dataConnection:Lcom/android/internal/telephony/DataConnection;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/DataConnection;->getName()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public updateLinkPropertiesDataCallStateSync(Lcom/android/internal/telephony/DataCallState;)Lcom/android/internal/telephony/DataConnection$UpdateLinkPropertyResult;
    .registers 5
    .parameter "newState"

    #@0
    .prologue
    .line 384
    const v1, 0x4100c

    #@3
    invoke-virtual {p0, v1, p1}, Lcom/android/internal/telephony/DataConnectionAc;->sendMessageSynchronously(ILjava/lang/Object;)Landroid/os/Message;

    #@6
    move-result-object v0

    #@7
    .line 386
    .local v0, response:Landroid/os/Message;
    if-eqz v0, :cond_15

    #@9
    iget v1, v0, Landroid/os/Message;->what:I

    #@b
    const v2, 0x4100d

    #@e
    if-ne v1, v2, :cond_15

    #@10
    .line 388
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/DataConnectionAc;->rspUpdateLinkPropertiesDataCallState(Landroid/os/Message;)Lcom/android/internal/telephony/DataConnection$UpdateLinkPropertyResult;

    #@13
    move-result-object v1

    #@14
    .line 391
    :goto_14
    return-object v1

    #@15
    .line 390
    :cond_15
    new-instance v1, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v2, "getLinkProperties error response="

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v1

    #@24
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v1

    #@28
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/DataConnectionAc;->log(Ljava/lang/String;)V

    #@2b
    .line 391
    new-instance v1, Lcom/android/internal/telephony/DataConnection$UpdateLinkPropertyResult;

    #@2d
    new-instance v2, Landroid/net/LinkProperties;

    #@2f
    invoke-direct {v2}, Landroid/net/LinkProperties;-><init>()V

    #@32
    invoke-direct {v1, v2}, Lcom/android/internal/telephony/DataConnection$UpdateLinkPropertyResult;-><init>(Landroid/net/LinkProperties;)V

    #@35
    goto :goto_14
.end method
