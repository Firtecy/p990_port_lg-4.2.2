.class Lcom/android/internal/telephony/CallManager$1;
.super Landroid/os/Handler;
.source "CallManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/CallManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/CallManager;


# direct methods
.method constructor <init>(Lcom/android/internal/telephony/CallManager;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 2358
    iput-object p1, p0, Lcom/android/internal/telephony/CallManager$1;->this$0:Lcom/android/internal/telephony/CallManager;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 13
    .parameter "msg"

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    const/4 v9, 0x1

    #@2
    .line 2363
    iget v7, p1, Landroid/os/Message;->what:I

    #@4
    sparse-switch v7, :sswitch_data_292

    #@7
    .line 2549
    :cond_7
    :goto_7
    return-void

    #@8
    .line 2366
    :sswitch_8
    iget-object v7, p0, Lcom/android/internal/telephony/CallManager$1;->this$0:Lcom/android/internal/telephony/CallManager;

    #@a
    iget-object v8, v7, Lcom/android/internal/telephony/CallManager;->mDisconnectRegistrants:Landroid/os/RegistrantList;

    #@c
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@e
    check-cast v7, Landroid/os/AsyncResult;

    #@10
    invoke-virtual {v8, v7}, Landroid/os/RegistrantList;->notifyRegistrants(Landroid/os/AsyncResult;)V

    #@13
    goto :goto_7

    #@14
    .line 2370
    :sswitch_14
    iget-object v7, p0, Lcom/android/internal/telephony/CallManager$1;->this$0:Lcom/android/internal/telephony/CallManager;

    #@16
    iget-object v8, v7, Lcom/android/internal/telephony/CallManager;->mPreciseCallStateRegistrants:Landroid/os/RegistrantList;

    #@18
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1a
    check-cast v7, Landroid/os/AsyncResult;

    #@1c
    invoke-virtual {v8, v7}, Landroid/os/RegistrantList;->notifyRegistrants(Landroid/os/AsyncResult;)V

    #@1f
    goto :goto_7

    #@20
    .line 2374
    :sswitch_20
    iget-object v7, p0, Lcom/android/internal/telephony/CallManager$1;->this$0:Lcom/android/internal/telephony/CallManager;

    #@22
    invoke-virtual {v7}, Lcom/android/internal/telephony/CallManager;->getActiveFgCallState()Lcom/android/internal/telephony/Call$State;

    #@25
    move-result-object v7

    #@26
    invoke-virtual {v7}, Lcom/android/internal/telephony/Call$State;->isDialing()Z

    #@29
    move-result v7

    #@2a
    if-nez v7, :cond_34

    #@2c
    iget-object v7, p0, Lcom/android/internal/telephony/CallManager$1;->this$0:Lcom/android/internal/telephony/CallManager;

    #@2e
    invoke-static {v7}, Lcom/android/internal/telephony/CallManager;->access$000(Lcom/android/internal/telephony/CallManager;)Z

    #@31
    move-result v7

    #@32
    if-eqz v7, :cond_69

    #@34
    .line 2375
    :cond_34
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@36
    check-cast v7, Landroid/os/AsyncResult;

    #@38
    iget-object v0, v7, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@3a
    check-cast v0, Lcom/android/internal/telephony/Connection;

    #@3c
    .line 2377
    .local v0, c:Lcom/android/internal/telephony/Connection;
    :try_start_3c
    const-string v7, "CallManager"

    #@3e
    new-instance v8, Ljava/lang/StringBuilder;

    #@40
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@43
    const-string v9, "silently drop incoming call: "

    #@45
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v8

    #@49
    invoke-virtual {v0}, Lcom/android/internal/telephony/Connection;->getCall()Lcom/android/internal/telephony/Call;

    #@4c
    move-result-object v9

    #@4d
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v8

    #@51
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v8

    #@55
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@58
    .line 2378
    invoke-virtual {v0}, Lcom/android/internal/telephony/Connection;->getCall()Lcom/android/internal/telephony/Call;

    #@5b
    move-result-object v7

    #@5c
    invoke-virtual {v7}, Lcom/android/internal/telephony/Call;->hangup()V
    :try_end_5f
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_3c .. :try_end_5f} :catch_60

    #@5f
    goto :goto_7

    #@60
    .line 2379
    :catch_60
    move-exception v2

    #@61
    .line 2380
    .local v2, e:Lcom/android/internal/telephony/CallStateException;
    const-string v7, "CallManager"

    #@63
    const-string v8, "new ringing connection"

    #@65
    invoke-static {v7, v8, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@68
    goto :goto_7

    #@69
    .line 2383
    .end local v0           #c:Lcom/android/internal/telephony/Connection;
    .end local v2           #e:Lcom/android/internal/telephony/CallStateException;
    :cond_69
    iget-object v7, p0, Lcom/android/internal/telephony/CallManager$1;->this$0:Lcom/android/internal/telephony/CallManager;

    #@6b
    iget-object v8, v7, Lcom/android/internal/telephony/CallManager;->mNewRingingConnectionRegistrants:Landroid/os/RegistrantList;

    #@6d
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@6f
    check-cast v7, Landroid/os/AsyncResult;

    #@71
    invoke-virtual {v8, v7}, Landroid/os/RegistrantList;->notifyRegistrants(Landroid/os/AsyncResult;)V

    #@74
    goto :goto_7

    #@75
    .line 2388
    :sswitch_75
    iget-object v7, p0, Lcom/android/internal/telephony/CallManager$1;->this$0:Lcom/android/internal/telephony/CallManager;

    #@77
    iget-object v8, v7, Lcom/android/internal/telephony/CallManager;->mUnknownConnectionRegistrants:Landroid/os/RegistrantList;

    #@79
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@7b
    check-cast v7, Landroid/os/AsyncResult;

    #@7d
    invoke-virtual {v8, v7}, Landroid/os/RegistrantList;->notifyRegistrants(Landroid/os/AsyncResult;)V

    #@80
    goto :goto_7

    #@81
    .line 2393
    :sswitch_81
    iget-object v7, p0, Lcom/android/internal/telephony/CallManager$1;->this$0:Lcom/android/internal/telephony/CallManager;

    #@83
    invoke-virtual {v7}, Lcom/android/internal/telephony/CallManager;->hasActiveFgCall()Z

    #@86
    move-result v7

    #@87
    if-nez v7, :cond_7

    #@89
    .line 2394
    iget-object v7, p0, Lcom/android/internal/telephony/CallManager$1;->this$0:Lcom/android/internal/telephony/CallManager;

    #@8b
    iget-object v8, v7, Lcom/android/internal/telephony/CallManager;->mIncomingRingRegistrants:Landroid/os/RegistrantList;

    #@8d
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@8f
    check-cast v7, Landroid/os/AsyncResult;

    #@91
    invoke-virtual {v8, v7}, Landroid/os/RegistrantList;->notifyRegistrants(Landroid/os/AsyncResult;)V

    #@94
    goto/16 :goto_7

    #@96
    .line 2399
    :sswitch_96
    iget-object v7, p0, Lcom/android/internal/telephony/CallManager$1;->this$0:Lcom/android/internal/telephony/CallManager;

    #@98
    iget-object v8, v7, Lcom/android/internal/telephony/CallManager;->mRingbackToneRegistrants:Landroid/os/RegistrantList;

    #@9a
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@9c
    check-cast v7, Landroid/os/AsyncResult;

    #@9e
    invoke-virtual {v8, v7}, Landroid/os/RegistrantList;->notifyRegistrants(Landroid/os/AsyncResult;)V

    #@a1
    goto/16 :goto_7

    #@a3
    .line 2403
    :sswitch_a3
    iget-object v7, p0, Lcom/android/internal/telephony/CallManager$1;->this$0:Lcom/android/internal/telephony/CallManager;

    #@a5
    iget-object v8, v7, Lcom/android/internal/telephony/CallManager;->mInCallVoicePrivacyOnRegistrants:Landroid/os/RegistrantList;

    #@a7
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@a9
    check-cast v7, Landroid/os/AsyncResult;

    #@ab
    invoke-virtual {v8, v7}, Landroid/os/RegistrantList;->notifyRegistrants(Landroid/os/AsyncResult;)V

    #@ae
    goto/16 :goto_7

    #@b0
    .line 2407
    :sswitch_b0
    iget-object v7, p0, Lcom/android/internal/telephony/CallManager$1;->this$0:Lcom/android/internal/telephony/CallManager;

    #@b2
    iget-object v8, v7, Lcom/android/internal/telephony/CallManager;->mInCallVoicePrivacyOffRegistrants:Landroid/os/RegistrantList;

    #@b4
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@b6
    check-cast v7, Landroid/os/AsyncResult;

    #@b8
    invoke-virtual {v8, v7}, Landroid/os/RegistrantList;->notifyRegistrants(Landroid/os/AsyncResult;)V

    #@bb
    goto/16 :goto_7

    #@bd
    .line 2411
    :sswitch_bd
    iget-object v7, p0, Lcom/android/internal/telephony/CallManager$1;->this$0:Lcom/android/internal/telephony/CallManager;

    #@bf
    iget-object v8, v7, Lcom/android/internal/telephony/CallManager;->mCallWaitingRegistrants:Landroid/os/RegistrantList;

    #@c1
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@c3
    check-cast v7, Landroid/os/AsyncResult;

    #@c5
    invoke-virtual {v8, v7}, Landroid/os/RegistrantList;->notifyRegistrants(Landroid/os/AsyncResult;)V

    #@c8
    goto/16 :goto_7

    #@ca
    .line 2415
    :sswitch_ca
    iget-object v7, p0, Lcom/android/internal/telephony/CallManager$1;->this$0:Lcom/android/internal/telephony/CallManager;

    #@cc
    iget-object v8, v7, Lcom/android/internal/telephony/CallManager;->mDisplayInfoRegistrants:Landroid/os/RegistrantList;

    #@ce
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@d0
    check-cast v7, Landroid/os/AsyncResult;

    #@d2
    invoke-virtual {v8, v7}, Landroid/os/RegistrantList;->notifyRegistrants(Landroid/os/AsyncResult;)V

    #@d5
    goto/16 :goto_7

    #@d7
    .line 2419
    :sswitch_d7
    iget-object v7, p0, Lcom/android/internal/telephony/CallManager$1;->this$0:Lcom/android/internal/telephony/CallManager;

    #@d9
    iget-object v8, v7, Lcom/android/internal/telephony/CallManager;->mSignalInfoRegistrants:Landroid/os/RegistrantList;

    #@db
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@dd
    check-cast v7, Landroid/os/AsyncResult;

    #@df
    invoke-virtual {v8, v7}, Landroid/os/RegistrantList;->notifyRegistrants(Landroid/os/AsyncResult;)V

    #@e2
    goto/16 :goto_7

    #@e4
    .line 2423
    :sswitch_e4
    iget-object v7, p0, Lcom/android/internal/telephony/CallManager$1;->this$0:Lcom/android/internal/telephony/CallManager;

    #@e6
    iget-object v8, v7, Lcom/android/internal/telephony/CallManager;->mCdmaOtaStatusChangeRegistrants:Landroid/os/RegistrantList;

    #@e8
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@ea
    check-cast v7, Landroid/os/AsyncResult;

    #@ec
    invoke-virtual {v8, v7}, Landroid/os/RegistrantList;->notifyRegistrants(Landroid/os/AsyncResult;)V

    #@ef
    goto/16 :goto_7

    #@f1
    .line 2427
    :sswitch_f1
    iget-object v7, p0, Lcom/android/internal/telephony/CallManager$1;->this$0:Lcom/android/internal/telephony/CallManager;

    #@f3
    iget-object v8, v7, Lcom/android/internal/telephony/CallManager;->mResendIncallMuteRegistrants:Landroid/os/RegistrantList;

    #@f5
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@f7
    check-cast v7, Landroid/os/AsyncResult;

    #@f9
    invoke-virtual {v8, v7}, Landroid/os/RegistrantList;->notifyRegistrants(Landroid/os/AsyncResult;)V

    #@fc
    goto/16 :goto_7

    #@fe
    .line 2431
    :sswitch_fe
    iget-object v7, p0, Lcom/android/internal/telephony/CallManager$1;->this$0:Lcom/android/internal/telephony/CallManager;

    #@100
    iget-object v8, v7, Lcom/android/internal/telephony/CallManager;->mMmiInitiateRegistrants:Landroid/os/RegistrantList;

    #@102
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@104
    check-cast v7, Landroid/os/AsyncResult;

    #@106
    invoke-virtual {v8, v7}, Landroid/os/RegistrantList;->notifyRegistrants(Landroid/os/AsyncResult;)V

    #@109
    goto/16 :goto_7

    #@10b
    .line 2435
    :sswitch_10b
    iget-object v7, p0, Lcom/android/internal/telephony/CallManager$1;->this$0:Lcom/android/internal/telephony/CallManager;

    #@10d
    iget-object v8, v7, Lcom/android/internal/telephony/CallManager;->mMmiCompleteRegistrants:Landroid/os/RegistrantList;

    #@10f
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@111
    check-cast v7, Landroid/os/AsyncResult;

    #@113
    invoke-virtual {v8, v7}, Landroid/os/RegistrantList;->notifyRegistrants(Landroid/os/AsyncResult;)V

    #@116
    goto/16 :goto_7

    #@118
    .line 2439
    :sswitch_118
    iget-object v7, p0, Lcom/android/internal/telephony/CallManager$1;->this$0:Lcom/android/internal/telephony/CallManager;

    #@11a
    iget-object v8, v7, Lcom/android/internal/telephony/CallManager;->mEcmTimerResetRegistrants:Landroid/os/RegistrantList;

    #@11c
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@11e
    check-cast v7, Landroid/os/AsyncResult;

    #@120
    invoke-virtual {v8, v7}, Landroid/os/RegistrantList;->notifyRegistrants(Landroid/os/AsyncResult;)V

    #@123
    goto/16 :goto_7

    #@125
    .line 2443
    :sswitch_125
    iget-object v7, p0, Lcom/android/internal/telephony/CallManager$1;->this$0:Lcom/android/internal/telephony/CallManager;

    #@127
    iget-object v8, v7, Lcom/android/internal/telephony/CallManager;->mSubscriptionInfoReadyRegistrants:Landroid/os/RegistrantList;

    #@129
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@12b
    check-cast v7, Landroid/os/AsyncResult;

    #@12d
    invoke-virtual {v8, v7}, Landroid/os/RegistrantList;->notifyRegistrants(Landroid/os/AsyncResult;)V

    #@130
    goto/16 :goto_7

    #@132
    .line 2447
    :sswitch_132
    iget-object v7, p0, Lcom/android/internal/telephony/CallManager$1;->this$0:Lcom/android/internal/telephony/CallManager;

    #@134
    iget-object v8, v7, Lcom/android/internal/telephony/CallManager;->mSuppServiceFailedRegistrants:Landroid/os/RegistrantList;

    #@136
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@138
    check-cast v7, Landroid/os/AsyncResult;

    #@13a
    invoke-virtual {v8, v7}, Landroid/os/RegistrantList;->notifyRegistrants(Landroid/os/AsyncResult;)V

    #@13d
    goto/16 :goto_7

    #@13f
    .line 2451
    :sswitch_13f
    iget-object v7, p0, Lcom/android/internal/telephony/CallManager$1;->this$0:Lcom/android/internal/telephony/CallManager;

    #@141
    iget-object v8, v7, Lcom/android/internal/telephony/CallManager;->mSuppServiceNotificationRegistrants:Landroid/os/RegistrantList;

    #@143
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@145
    check-cast v7, Landroid/os/AsyncResult;

    #@147
    invoke-virtual {v8, v7}, Landroid/os/RegistrantList;->notifyRegistrants(Landroid/os/AsyncResult;)V

    #@14a
    goto/16 :goto_7

    #@14c
    .line 2455
    :sswitch_14c
    iget-object v7, p0, Lcom/android/internal/telephony/CallManager$1;->this$0:Lcom/android/internal/telephony/CallManager;

    #@14e
    iget-object v8, v7, Lcom/android/internal/telephony/CallManager;->mServiceStateChangedRegistrants:Landroid/os/RegistrantList;

    #@150
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@152
    check-cast v7, Landroid/os/AsyncResult;

    #@154
    invoke-virtual {v8, v7}, Landroid/os/RegistrantList;->notifyRegistrants(Landroid/os/AsyncResult;)V

    #@157
    .line 2457
    invoke-static {}, Lcom/android/internal/telephony/CallManager;->access$100()Z

    #@15a
    move-result v7

    #@15b
    if-eqz v7, :cond_7

    #@15d
    .line 2458
    invoke-static {}, Lcom/android/internal/telephony/CallManager;->access$200()Lcom/movial/ipphone/IPPhoneProxy;

    #@160
    move-result-object v7

    #@161
    if-eqz v7, :cond_7

    #@163
    .line 2460
    const-string v7, "CallManager"

    #@165
    new-instance v8, Ljava/lang/StringBuilder;

    #@167
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@16a
    const-string v9, "IMS Emergency State : "

    #@16c
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16f
    move-result-object v8

    #@170
    invoke-static {}, Lcom/android/internal/telephony/CallManager;->access$200()Lcom/movial/ipphone/IPPhoneProxy;

    #@173
    move-result-object v9

    #@174
    invoke-virtual {v9}, Lcom/movial/ipphone/IPPhoneProxy;->getEmergencyState()Lcom/movial/ipphone/IPUtils$EmergencyState;

    #@177
    move-result-object v9

    #@178
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@17b
    move-result-object v8

    #@17c
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17f
    move-result-object v8

    #@180
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@183
    .line 2461
    invoke-static {}, Lcom/android/internal/telephony/CallManager;->access$200()Lcom/movial/ipphone/IPPhoneProxy;

    #@186
    move-result-object v7

    #@187
    invoke-virtual {v7}, Lcom/movial/ipphone/IPPhoneProxy;->getEmergencyState()Lcom/movial/ipphone/IPUtils$EmergencyState;

    #@18a
    move-result-object v7

    #@18b
    sget-object v8, Lcom/movial/ipphone/IPUtils$EmergencyState;->CS_TURNING_ON_RADIO:Lcom/movial/ipphone/IPUtils$EmergencyState;

    #@18d
    if-ne v7, v8, :cond_7

    #@18f
    .line 2467
    :try_start_18f
    iget-object v7, p0, Lcom/android/internal/telephony/CallManager$1;->this$0:Lcom/android/internal/telephony/CallManager;

    #@191
    const/4 v8, 0x0

    #@192
    invoke-static {v7, v8}, Lcom/android/internal/telephony/CallManager;->access$302(Lcom/android/internal/telephony/CallManager;Z)Z

    #@195
    .line 2468
    const/4 v7, 0x1

    #@196
    invoke-static {v7}, Lcom/android/internal/telephony/CallManager;->isKeepingFakeCall(Z)Z

    #@199
    move-result v4

    #@19a
    .line 2469
    .local v4, isKeepingFakeCall:Z
    const-string v7, "CallManager"

    #@19c
    new-instance v8, Ljava/lang/StringBuilder;

    #@19e
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@1a1
    const-string v9, "isKeepingFakeCall(EVENT_RADIO_ON) : "

    #@1a3
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a6
    move-result-object v8

    #@1a7
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1aa
    move-result-object v8

    #@1ab
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ae
    move-result-object v8

    #@1af
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1b2
    .line 2470
    if-eqz v4, :cond_7

    #@1b4
    .line 2472
    const/4 v7, 0x1

    #@1b5
    sput-boolean v7, Lcom/android/internal/telephony/CallManager;->mIgnoreOutOfServiceArea:Z

    #@1b7
    .line 2473
    iget-object v7, p0, Lcom/android/internal/telephony/CallManager$1;->this$0:Lcom/android/internal/telephony/CallManager;

    #@1b9
    iget-object v8, p0, Lcom/android/internal/telephony/CallManager$1;->this$0:Lcom/android/internal/telephony/CallManager;

    #@1bb
    invoke-static {v8}, Lcom/android/internal/telephony/CallManager;->access$400(Lcom/android/internal/telephony/CallManager;)Lcom/android/internal/telephony/Phone;

    #@1be
    move-result-object v8

    #@1bf
    const-string v9, "911"

    #@1c1
    invoke-virtual {v7, v8, v9}, Lcom/android/internal/telephony/CallManager;->dial(Lcom/android/internal/telephony/Phone;Ljava/lang/String;)Lcom/android/internal/telephony/Connection;

    #@1c4
    .line 2474
    const/4 v7, 0x0

    #@1c5
    invoke-static {v7}, Lcom/android/internal/telephony/gsm/EccNoti;->setProcessECCNoti(Z)V
    :try_end_1c8
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_18f .. :try_end_1c8} :catch_1ca

    #@1c8
    goto/16 :goto_7

    #@1ca
    .line 2476
    .end local v4           #isKeepingFakeCall:Z
    :catch_1ca
    move-exception v7

    #@1cb
    goto/16 :goto_7

    #@1cd
    .line 2490
    :sswitch_1cd
    const/4 v3, 0x0

    #@1ce
    .local v3, i:I
    :goto_1ce
    iget-object v7, p0, Lcom/android/internal/telephony/CallManager$1;->this$0:Lcom/android/internal/telephony/CallManager;

    #@1d0
    iget-object v7, v7, Lcom/android/internal/telephony/CallManager;->mPostDialCharacterRegistrants:Landroid/os/RegistrantList;

    #@1d2
    invoke-virtual {v7}, Landroid/os/RegistrantList;->size()I

    #@1d5
    move-result v7

    #@1d6
    if-ge v3, v7, :cond_7

    #@1d8
    .line 2492
    iget-object v7, p0, Lcom/android/internal/telephony/CallManager$1;->this$0:Lcom/android/internal/telephony/CallManager;

    #@1da
    iget-object v7, v7, Lcom/android/internal/telephony/CallManager;->mPostDialCharacterRegistrants:Landroid/os/RegistrantList;

    #@1dc
    invoke-virtual {v7, v3}, Landroid/os/RegistrantList;->get(I)Ljava/lang/Object;

    #@1df
    move-result-object v7

    #@1e0
    check-cast v7, Landroid/os/Registrant;

    #@1e2
    invoke-virtual {v7}, Landroid/os/Registrant;->messageForRegistrant()Landroid/os/Message;

    #@1e5
    move-result-object v6

    #@1e6
    .line 2493
    .local v6, notifyMsg:Landroid/os/Message;
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1e8
    iput-object v7, v6, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1ea
    .line 2494
    iget v7, p1, Landroid/os/Message;->arg1:I

    #@1ec
    iput v7, v6, Landroid/os/Message;->arg1:I

    #@1ee
    .line 2495
    invoke-virtual {v6}, Landroid/os/Message;->sendToTarget()V

    #@1f1
    .line 2490
    add-int/lit8 v3, v3, 0x1

    #@1f3
    goto :goto_1ce

    #@1f4
    .line 2501
    .end local v3           #i:I
    .end local v6           #notifyMsg:Landroid/os/Message;
    :sswitch_1f4
    iget-object v7, p0, Lcom/android/internal/telephony/CallManager$1;->this$0:Lcom/android/internal/telephony/CallManager;

    #@1f6
    iget-object v8, v7, Lcom/android/internal/telephony/CallManager;->mRedirectedNumberInfoRegistrants:Landroid/os/RegistrantList;

    #@1f8
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1fa
    check-cast v7, Landroid/os/AsyncResult;

    #@1fc
    invoke-virtual {v8, v7}, Landroid/os/RegistrantList;->notifyRegistrants(Landroid/os/AsyncResult;)V

    #@1ff
    goto/16 :goto_7

    #@201
    .line 2508
    :sswitch_201
    invoke-static {}, Lcom/android/internal/telephony/CallManager;->access$100()Z

    #@204
    move-result v7

    #@205
    if-eqz v7, :cond_7

    #@207
    .line 2511
    const/4 v7, 0x1

    #@208
    :try_start_208
    invoke-static {v7}, Lcom/android/internal/telephony/CallManager;->isKeepingFakeCall(Z)Z

    #@20b
    move-result v4

    #@20c
    .line 2512
    .restart local v4       #isKeepingFakeCall:Z
    const-string v7, "CallManager"

    #@20e
    new-instance v8, Ljava/lang/StringBuilder;

    #@210
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@213
    const-string v9, "isKeepingFakeCall(EVENT_TRY_EMERGENCY_CALL) : "

    #@215
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@218
    move-result-object v8

    #@219
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@21c
    move-result-object v8

    #@21d
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@220
    move-result-object v8

    #@221
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@224
    .line 2513
    if-eqz v4, :cond_7

    #@226
    iget-object v7, p0, Lcom/android/internal/telephony/CallManager$1;->this$0:Lcom/android/internal/telephony/CallManager;

    #@228
    iget-object v8, p0, Lcom/android/internal/telephony/CallManager$1;->this$0:Lcom/android/internal/telephony/CallManager;

    #@22a
    invoke-static {v8}, Lcom/android/internal/telephony/CallManager;->access$400(Lcom/android/internal/telephony/CallManager;)Lcom/android/internal/telephony/Phone;

    #@22d
    move-result-object v8

    #@22e
    const-string v9, "911"

    #@230
    invoke-virtual {v7, v8, v9}, Lcom/android/internal/telephony/CallManager;->dial(Lcom/android/internal/telephony/Phone;Ljava/lang/String;)Lcom/android/internal/telephony/Connection;
    :try_end_233
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_208 .. :try_end_233} :catch_235

    #@233
    goto/16 :goto_7

    #@235
    .line 2514
    .end local v4           #isKeepingFakeCall:Z
    :catch_235
    move-exception v7

    #@236
    goto/16 :goto_7

    #@238
    .line 2523
    :sswitch_238
    invoke-static {}, Lcom/android/internal/telephony/CallManager;->access$100()Z

    #@23b
    move-result v7

    #@23c
    if-eqz v7, :cond_7

    #@23e
    .line 2524
    invoke-static {}, Lcom/android/internal/telephony/CallManager;->access$508()I

    #@241
    .line 2525
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@243
    check-cast v1, Lcom/android/internal/telephony/CommandsInterface;

    #@245
    .line 2526
    .local v1, cm:Lcom/android/internal/telephony/CommandsInterface;
    if-eqz v1, :cond_25b

    #@247
    invoke-interface {v1}, Lcom/android/internal/telephony/CommandsInterface;->getRadioState()Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@24a
    move-result-object v7

    #@24b
    sget-object v8, Lcom/android/internal/telephony/CommandsInterface$RadioState;->RADIO_OFF:Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@24d
    if-ne v7, v8, :cond_25b

    #@24f
    .line 2527
    invoke-static {}, Lcom/android/internal/telephony/CallManager;->access$200()Lcom/movial/ipphone/IPPhoneProxy;

    #@252
    move-result-object v7

    #@253
    invoke-virtual {v7, v9}, Lcom/movial/ipphone/IPPhoneProxy;->setRadioPower(Z)V

    #@256
    .line 2528
    invoke-static {v10}, Lcom/android/internal/telephony/CallManager;->access$502(I)I

    #@259
    goto/16 :goto_7

    #@25b
    .line 2529
    :cond_25b
    if-eqz v1, :cond_273

    #@25d
    invoke-static {}, Lcom/android/internal/telephony/CallManager;->access$500()I

    #@260
    move-result v7

    #@261
    const/4 v8, 0x4

    #@262
    if-ge v7, v8, :cond_273

    #@264
    .line 2530
    new-instance v5, Landroid/os/Message;

    #@266
    invoke-direct {v5}, Landroid/os/Message;-><init>()V

    #@269
    .line 2531
    .local v5, newMsg:Landroid/os/Message;
    invoke-virtual {v5, p1}, Landroid/os/Message;->copyFrom(Landroid/os/Message;)V

    #@26c
    .line 2532
    const-wide/16 v7, 0x1f4

    #@26e
    invoke-virtual {p0, v5, v7, v8}, Lcom/android/internal/telephony/CallManager$1;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@271
    goto/16 :goto_7

    #@273
    .line 2534
    .end local v5           #newMsg:Landroid/os/Message;
    :cond_273
    invoke-static {v9}, Lcom/android/internal/telephony/CallManager;->access$602(Z)Z

    #@276
    .line 2535
    invoke-static {v9}, Lcom/android/internal/telephony/CallManager;->access$702(Z)Z

    #@279
    .line 2536
    invoke-static {}, Lcom/android/internal/telephony/CallManager;->access$200()Lcom/movial/ipphone/IPPhoneProxy;

    #@27c
    move-result-object v7

    #@27d
    invoke-virtual {v7}, Lcom/movial/ipphone/IPPhoneProxy;->startImsEmergencyCall()V

    #@280
    .line 2537
    invoke-static {v10}, Lcom/android/internal/telephony/CallManager;->access$502(I)I

    #@283
    goto/16 :goto_7

    #@285
    .line 2545
    .end local v1           #cm:Lcom/android/internal/telephony/CommandsInterface;
    :sswitch_285
    iget-object v7, p0, Lcom/android/internal/telephony/CallManager$1;->this$0:Lcom/android/internal/telephony/CallManager;

    #@287
    iget-object v8, v7, Lcom/android/internal/telephony/CallManager;->mCipheringNotificationRegistrants:Landroid/os/RegistrantList;

    #@289
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@28b
    check-cast v7, Landroid/os/AsyncResult;

    #@28d
    invoke-virtual {v8, v7}, Landroid/os/RegistrantList;->notifyRegistrants(Landroid/os/AsyncResult;)V

    #@290
    goto/16 :goto_7

    #@292
    .line 2363
    :sswitch_data_292
    .sparse-switch
        0x64 -> :sswitch_8
        0x65 -> :sswitch_14
        0x66 -> :sswitch_20
        0x67 -> :sswitch_75
        0x68 -> :sswitch_81
        0x69 -> :sswitch_96
        0x6a -> :sswitch_a3
        0x6b -> :sswitch_b0
        0x6c -> :sswitch_bd
        0x6d -> :sswitch_ca
        0x6e -> :sswitch_d7
        0x6f -> :sswitch_e4
        0x70 -> :sswitch_f1
        0x71 -> :sswitch_fe
        0x72 -> :sswitch_10b
        0x73 -> :sswitch_118
        0x74 -> :sswitch_125
        0x75 -> :sswitch_132
        0x76 -> :sswitch_14c
        0x77 -> :sswitch_1cd
        0x78 -> :sswitch_13f
        0x79 -> :sswitch_1f4
        0x7d -> :sswitch_285
        0xc9 -> :sswitch_201
        0xca -> :sswitch_238
    .end sparse-switch
.end method
