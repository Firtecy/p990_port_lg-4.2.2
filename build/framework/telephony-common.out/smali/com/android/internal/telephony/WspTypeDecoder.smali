.class public Lcom/android/internal/telephony/WspTypeDecoder;
.super Ljava/lang/Object;
.source "WspTypeDecoder.java"


# static fields
.field public static final CONTENT_MIME_TYPE_B_MMS:Ljava/lang/String; = "application/vnd.wap.mms-message"

.field public static final CONTENT_MIME_TYPE_B_OMADM:Ljava/lang/String; = "application/vnd.syncml.notification"

.field public static final CONTENT_MIME_TYPE_B_SYNCML_NOTI:Ljava/lang/String; = "application/vnd.syncml.notification"

.field public static final CONTENT_TYPE_B_MMS:Ljava/lang/String; = "application/vnd.wap.mms-message"

.field public static final CONTENT_TYPE_B_OMADM:I = 0x45

.field public static final CONTENT_TYPE_B_PUSH_CO:Ljava/lang/String; = "application/vnd.wap.coc"

.field public static final CONTENT_TYPE_B_PUSH_SYNCML_NOTI:Ljava/lang/String; = "application/vnd.syncml.notification"

.field public static final CONTENT_TYPE_B_SYNCML_NOTI:I = 0x44

.field public static final PARAMETER_ID_X_WAP_APPLICATION_ID:I = 0x2f

.field public static final PDU_TYPE_CONFIRMED_PUSH:I = 0x7

.field public static final PDU_TYPE_PUSH:I = 0x6

.field private static final Q_VALUE:I = 0x0

.field private static final WAP_PDU_LENGTH_QUOTE:I = 0x1f

.field private static final WAP_PDU_SHORT_LENGTH_MAX:I = 0x1e

.field private static final WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final WELL_KNOWN_PARAMETERS:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field contentParameters:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field dataLength:I

.field stringValue:Ljava/lang/String;

.field unsigned32bit:J

.field wspData:[B


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x7

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 34
    new-instance v0, Ljava/util/HashMap;

    #@7
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@a
    sput-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@c
    .line 37
    new-instance v0, Ljava/util/HashMap;

    #@e
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@11
    sput-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_PARAMETERS:Ljava/util/HashMap;

    #@13
    .line 44
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@15
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@18
    move-result-object v1

    #@19
    const-string v2, "*/*"

    #@1b
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1e
    .line 45
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@20
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@23
    move-result-object v1

    #@24
    const-string v2, "text/*"

    #@26
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@29
    .line 46
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@2b
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2e
    move-result-object v1

    #@2f
    const-string v2, "text/html"

    #@31
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@34
    .line 47
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@36
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@39
    move-result-object v1

    #@3a
    const-string v2, "text/plain"

    #@3c
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3f
    .line 48
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@41
    const/4 v1, 0x4

    #@42
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@45
    move-result-object v1

    #@46
    const-string v2, "text/x-hdml"

    #@48
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@4b
    .line 49
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@4d
    const/4 v1, 0x5

    #@4e
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@51
    move-result-object v1

    #@52
    const-string v2, "text/x-ttml"

    #@54
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@57
    .line 50
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@59
    const/4 v1, 0x6

    #@5a
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5d
    move-result-object v1

    #@5e
    const-string v2, "text/x-vCalendar"

    #@60
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@63
    .line 51
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@65
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@68
    move-result-object v1

    #@69
    const-string v2, "text/x-vCard"

    #@6b
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@6e
    .line 52
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@70
    const/16 v1, 0x8

    #@72
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@75
    move-result-object v1

    #@76
    const-string v2, "text/vnd.wap.wml"

    #@78
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@7b
    .line 53
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@7d
    const/16 v1, 0x9

    #@7f
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@82
    move-result-object v1

    #@83
    const-string v2, "text/vnd.wap.wmlscript"

    #@85
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@88
    .line 54
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@8a
    const/16 v1, 0xa

    #@8c
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@8f
    move-result-object v1

    #@90
    const-string v2, "text/vnd.wap.wta-event"

    #@92
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@95
    .line 55
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@97
    const/16 v1, 0xb

    #@99
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9c
    move-result-object v1

    #@9d
    const-string v2, "multipart/*"

    #@9f
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@a2
    .line 56
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@a4
    const/16 v1, 0xc

    #@a6
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@a9
    move-result-object v1

    #@aa
    const-string v2, "multipart/mixed"

    #@ac
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@af
    .line 57
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@b1
    const/16 v1, 0xd

    #@b3
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@b6
    move-result-object v1

    #@b7
    const-string v2, "multipart/form-data"

    #@b9
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@bc
    .line 58
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@be
    const/16 v1, 0xe

    #@c0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@c3
    move-result-object v1

    #@c4
    const-string v2, "multipart/byterantes"

    #@c6
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@c9
    .line 59
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@cb
    const/16 v1, 0xf

    #@cd
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@d0
    move-result-object v1

    #@d1
    const-string v2, "multipart/alternative"

    #@d3
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@d6
    .line 60
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@d8
    const/16 v1, 0x10

    #@da
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@dd
    move-result-object v1

    #@de
    const-string v2, "application/*"

    #@e0
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@e3
    .line 61
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@e5
    const/16 v1, 0x11

    #@e7
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@ea
    move-result-object v1

    #@eb
    const-string v2, "application/java-vm"

    #@ed
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@f0
    .line 62
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@f2
    const/16 v1, 0x12

    #@f4
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@f7
    move-result-object v1

    #@f8
    const-string v2, "application/x-www-form-urlencoded"

    #@fa
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@fd
    .line 63
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@ff
    const/16 v1, 0x13

    #@101
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@104
    move-result-object v1

    #@105
    const-string v2, "application/x-hdmlc"

    #@107
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@10a
    .line 64
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@10c
    const/16 v1, 0x14

    #@10e
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@111
    move-result-object v1

    #@112
    const-string v2, "application/vnd.wap.wmlc"

    #@114
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@117
    .line 65
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@119
    const/16 v1, 0x15

    #@11b
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@11e
    move-result-object v1

    #@11f
    const-string v2, "application/vnd.wap.wmlscriptc"

    #@121
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@124
    .line 66
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@126
    const/16 v1, 0x16

    #@128
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@12b
    move-result-object v1

    #@12c
    const-string v2, "application/vnd.wap.wta-eventc"

    #@12e
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@131
    .line 67
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@133
    const/16 v1, 0x17

    #@135
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@138
    move-result-object v1

    #@139
    const-string v2, "application/vnd.wap.uaprof"

    #@13b
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@13e
    .line 68
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@140
    const/16 v1, 0x18

    #@142
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@145
    move-result-object v1

    #@146
    const-string v2, "application/vnd.wap.wtls-ca-certificate"

    #@148
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@14b
    .line 69
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@14d
    const/16 v1, 0x19

    #@14f
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@152
    move-result-object v1

    #@153
    const-string v2, "application/vnd.wap.wtls-user-certificate"

    #@155
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@158
    .line 70
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@15a
    const/16 v1, 0x1a

    #@15c
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@15f
    move-result-object v1

    #@160
    const-string v2, "application/x-x509-ca-cert"

    #@162
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@165
    .line 71
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@167
    const/16 v1, 0x1b

    #@169
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@16c
    move-result-object v1

    #@16d
    const-string v2, "application/x-x509-user-cert"

    #@16f
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@172
    .line 72
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@174
    const/16 v1, 0x1c

    #@176
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@179
    move-result-object v1

    #@17a
    const-string v2, "image/*"

    #@17c
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@17f
    .line 73
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@181
    const/16 v1, 0x1d

    #@183
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@186
    move-result-object v1

    #@187
    const-string v2, "image/gif"

    #@189
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@18c
    .line 74
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@18e
    const/16 v1, 0x1e

    #@190
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@193
    move-result-object v1

    #@194
    const-string v2, "image/jpeg"

    #@196
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@199
    .line 75
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@19b
    const/16 v1, 0x1f

    #@19d
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1a0
    move-result-object v1

    #@1a1
    const-string v2, "image/tiff"

    #@1a3
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1a6
    .line 76
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@1a8
    const/16 v1, 0x20

    #@1aa
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1ad
    move-result-object v1

    #@1ae
    const-string v2, "image/png"

    #@1b0
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1b3
    .line 77
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@1b5
    const/16 v1, 0x21

    #@1b7
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1ba
    move-result-object v1

    #@1bb
    const-string v2, "image/vnd.wap.wbmp"

    #@1bd
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1c0
    .line 78
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@1c2
    const/16 v1, 0x22

    #@1c4
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1c7
    move-result-object v1

    #@1c8
    const-string v2, "application/vnd.wap.multipart.*"

    #@1ca
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1cd
    .line 79
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@1cf
    const/16 v1, 0x23

    #@1d1
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1d4
    move-result-object v1

    #@1d5
    const-string v2, "application/vnd.wap.multipart.mixed"

    #@1d7
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1da
    .line 80
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@1dc
    const/16 v1, 0x24

    #@1de
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1e1
    move-result-object v1

    #@1e2
    const-string v2, "application/vnd.wap.multipart.form-data"

    #@1e4
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1e7
    .line 81
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@1e9
    const/16 v1, 0x25

    #@1eb
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1ee
    move-result-object v1

    #@1ef
    const-string v2, "application/vnd.wap.multipart.byteranges"

    #@1f1
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1f4
    .line 82
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@1f6
    const/16 v1, 0x26

    #@1f8
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1fb
    move-result-object v1

    #@1fc
    const-string v2, "application/vnd.wap.multipart.alternative"

    #@1fe
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@201
    .line 83
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@203
    const/16 v1, 0x27

    #@205
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@208
    move-result-object v1

    #@209
    const-string v2, "application/xml"

    #@20b
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@20e
    .line 84
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@210
    const/16 v1, 0x28

    #@212
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@215
    move-result-object v1

    #@216
    const-string v2, "text/xml"

    #@218
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@21b
    .line 85
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@21d
    const/16 v1, 0x29

    #@21f
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@222
    move-result-object v1

    #@223
    const-string v2, "application/vnd.wap.wbxml"

    #@225
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@228
    .line 86
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@22a
    const/16 v1, 0x2a

    #@22c
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@22f
    move-result-object v1

    #@230
    const-string v2, "application/x-x968-cross-cert"

    #@232
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@235
    .line 87
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@237
    const/16 v1, 0x2b

    #@239
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@23c
    move-result-object v1

    #@23d
    const-string v2, "application/x-x968-ca-cert"

    #@23f
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@242
    .line 88
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@244
    const/16 v1, 0x2c

    #@246
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@249
    move-result-object v1

    #@24a
    const-string v2, "application/x-x968-user-cert"

    #@24c
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@24f
    .line 89
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@251
    const/16 v1, 0x2d

    #@253
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@256
    move-result-object v1

    #@257
    const-string v2, "text/vnd.wap.si"

    #@259
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@25c
    .line 90
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@25e
    const/16 v1, 0x2e

    #@260
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@263
    move-result-object v1

    #@264
    const-string v2, "application/vnd.wap.sic"

    #@266
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@269
    .line 91
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@26b
    const/16 v1, 0x2f

    #@26d
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@270
    move-result-object v1

    #@271
    const-string v2, "text/vnd.wap.sl"

    #@273
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@276
    .line 92
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@278
    const/16 v1, 0x30

    #@27a
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@27d
    move-result-object v1

    #@27e
    const-string v2, "application/vnd.wap.slc"

    #@280
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@283
    .line 93
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@285
    const/16 v1, 0x31

    #@287
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@28a
    move-result-object v1

    #@28b
    const-string v2, "text/vnd.wap.co"

    #@28d
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@290
    .line 94
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@292
    const/16 v1, 0x32

    #@294
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@297
    move-result-object v1

    #@298
    const-string v2, "application/vnd.wap.coc"

    #@29a
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@29d
    .line 95
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@29f
    const/16 v1, 0x33

    #@2a1
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2a4
    move-result-object v1

    #@2a5
    const-string v2, "application/vnd.wap.multipart.related"

    #@2a7
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2aa
    .line 96
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@2ac
    const/16 v1, 0x34

    #@2ae
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2b1
    move-result-object v1

    #@2b2
    const-string v2, "application/vnd.wap.sia"

    #@2b4
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2b7
    .line 97
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@2b9
    const/16 v1, 0x35

    #@2bb
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2be
    move-result-object v1

    #@2bf
    const-string v2, "text/vnd.wap.connectivity-xml"

    #@2c1
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2c4
    .line 98
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@2c6
    const/16 v1, 0x36

    #@2c8
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2cb
    move-result-object v1

    #@2cc
    const-string v2, "application/vnd.wap.connectivity-wbxml"

    #@2ce
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2d1
    .line 99
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@2d3
    const/16 v1, 0x37

    #@2d5
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2d8
    move-result-object v1

    #@2d9
    const-string v2, "application/pkcs7-mime"

    #@2db
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2de
    .line 100
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@2e0
    const/16 v1, 0x38

    #@2e2
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2e5
    move-result-object v1

    #@2e6
    const-string v2, "application/vnd.wap.hashed-certificate"

    #@2e8
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2eb
    .line 101
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@2ed
    const/16 v1, 0x39

    #@2ef
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2f2
    move-result-object v1

    #@2f3
    const-string v2, "application/vnd.wap.signed-certificate"

    #@2f5
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2f8
    .line 102
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@2fa
    const/16 v1, 0x3a

    #@2fc
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2ff
    move-result-object v1

    #@300
    const-string v2, "application/vnd.wap.cert-response"

    #@302
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@305
    .line 103
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@307
    const/16 v1, 0x3b

    #@309
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@30c
    move-result-object v1

    #@30d
    const-string v2, "application/xhtml+xml"

    #@30f
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@312
    .line 104
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@314
    const/16 v1, 0x3c

    #@316
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@319
    move-result-object v1

    #@31a
    const-string v2, "application/wml+xml"

    #@31c
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@31f
    .line 105
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@321
    const/16 v1, 0x3d

    #@323
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@326
    move-result-object v1

    #@327
    const-string v2, "text/css"

    #@329
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@32c
    .line 106
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@32e
    const/16 v1, 0x3e

    #@330
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@333
    move-result-object v1

    #@334
    const-string v2, "application/vnd.wap.mms-message"

    #@336
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@339
    .line 107
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@33b
    const/16 v1, 0x3f

    #@33d
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@340
    move-result-object v1

    #@341
    const-string v2, "application/vnd.wap.rollover-certificate"

    #@343
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@346
    .line 108
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@348
    const/16 v1, 0x40

    #@34a
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@34d
    move-result-object v1

    #@34e
    const-string v2, "application/vnd.wap.locc+wbxml"

    #@350
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@353
    .line 109
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@355
    const/16 v1, 0x41

    #@357
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@35a
    move-result-object v1

    #@35b
    const-string v2, "application/vnd.wap.loc+xml"

    #@35d
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@360
    .line 110
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@362
    const/16 v1, 0x42

    #@364
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@367
    move-result-object v1

    #@368
    const-string v2, "application/vnd.syncml.dm+wbxml"

    #@36a
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@36d
    .line 111
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@36f
    const/16 v1, 0x43

    #@371
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@374
    move-result-object v1

    #@375
    const-string v2, "application/vnd.syncml.dm+xml"

    #@377
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@37a
    .line 112
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@37c
    const/16 v1, 0x44

    #@37e
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@381
    move-result-object v1

    #@382
    const-string v2, "application/vnd.syncml.notification"

    #@384
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@387
    .line 113
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@389
    const/16 v1, 0x45

    #@38b
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@38e
    move-result-object v1

    #@38f
    const-string v2, "application/vnd.wap.xhtml+xml"

    #@391
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@394
    .line 114
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@396
    const/16 v1, 0x46

    #@398
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@39b
    move-result-object v1

    #@39c
    const-string v2, "application/vnd.wv.csp.cir"

    #@39e
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3a1
    .line 115
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@3a3
    const/16 v1, 0x47

    #@3a5
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3a8
    move-result-object v1

    #@3a9
    const-string v2, "application/vnd.oma.dd+xml"

    #@3ab
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3ae
    .line 116
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@3b0
    const/16 v1, 0x48

    #@3b2
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3b5
    move-result-object v1

    #@3b6
    const-string v2, "application/vnd.oma.drm.message"

    #@3b8
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3bb
    .line 117
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@3bd
    const/16 v1, 0x49

    #@3bf
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3c2
    move-result-object v1

    #@3c3
    const-string v2, "application/vnd.oma.drm.content"

    #@3c5
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3c8
    .line 118
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@3ca
    const/16 v1, 0x4a

    #@3cc
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3cf
    move-result-object v1

    #@3d0
    const-string v2, "application/vnd.oma.drm.rights+xml"

    #@3d2
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3d5
    .line 119
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@3d7
    const/16 v1, 0x4b

    #@3d9
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3dc
    move-result-object v1

    #@3dd
    const-string v2, "application/vnd.oma.drm.rights+wbxml"

    #@3df
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3e2
    .line 120
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@3e4
    const/16 v1, 0x4c

    #@3e6
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3e9
    move-result-object v1

    #@3ea
    const-string v2, "application/vnd.wv.csp+xml"

    #@3ec
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3ef
    .line 121
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@3f1
    const/16 v1, 0x4d

    #@3f3
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3f6
    move-result-object v1

    #@3f7
    const-string v2, "application/vnd.wv.csp+wbxml"

    #@3f9
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3fc
    .line 122
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@3fe
    const/16 v1, 0x4e

    #@400
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@403
    move-result-object v1

    #@404
    const-string v2, "application/vnd.syncml.ds.notification"

    #@406
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@409
    .line 123
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@40b
    const/16 v1, 0x4f

    #@40d
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@410
    move-result-object v1

    #@411
    const-string v2, "audio/*"

    #@413
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@416
    .line 124
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@418
    const/16 v1, 0x50

    #@41a
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@41d
    move-result-object v1

    #@41e
    const-string v2, "video/*"

    #@420
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@423
    .line 125
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@425
    const/16 v1, 0x51

    #@427
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@42a
    move-result-object v1

    #@42b
    const-string v2, "application/vnd.oma.dd2+xml"

    #@42d
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@430
    .line 126
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@432
    const/16 v1, 0x52

    #@434
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@437
    move-result-object v1

    #@438
    const-string v2, "application/mikey"

    #@43a
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@43d
    .line 127
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@43f
    const/16 v1, 0x53

    #@441
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@444
    move-result-object v1

    #@445
    const-string v2, "application/vnd.oma.dcd"

    #@447
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@44a
    .line 128
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@44c
    const/16 v1, 0x54

    #@44e
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@451
    move-result-object v1

    #@452
    const-string v2, "application/vnd.oma.dcdc"

    #@454
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@457
    .line 130
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@459
    const/16 v1, 0x201

    #@45b
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@45e
    move-result-object v1

    #@45f
    const-string v2, "application/vnd.uplanet.cacheop-wbxml"

    #@461
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@464
    .line 131
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@466
    const/16 v1, 0x202

    #@468
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@46b
    move-result-object v1

    #@46c
    const-string v2, "application/vnd.uplanet.signal"

    #@46e
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@471
    .line 132
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@473
    const/16 v1, 0x203

    #@475
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@478
    move-result-object v1

    #@479
    const-string v2, "application/vnd.uplanet.alert-wbxml"

    #@47b
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@47e
    .line 133
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@480
    const/16 v1, 0x204

    #@482
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@485
    move-result-object v1

    #@486
    const-string v2, "application/vnd.uplanet.list-wbxml"

    #@488
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@48b
    .line 134
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@48d
    const/16 v1, 0x205

    #@48f
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@492
    move-result-object v1

    #@493
    const-string v2, "application/vnd.uplanet.listcmd-wbxml"

    #@495
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@498
    .line 135
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@49a
    const/16 v1, 0x206

    #@49c
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@49f
    move-result-object v1

    #@4a0
    const-string v2, "application/vnd.uplanet.channel-wbxml"

    #@4a2
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@4a5
    .line 136
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@4a7
    const/16 v1, 0x207

    #@4a9
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4ac
    move-result-object v1

    #@4ad
    const-string v2, "application/vnd.uplanet.provisioning-status-uri"

    #@4af
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@4b2
    .line 137
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@4b4
    const/16 v1, 0x208

    #@4b6
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4b9
    move-result-object v1

    #@4ba
    const-string v2, "x-wap.multipart/vnd.uplanet.header-set"

    #@4bc
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@4bf
    .line 138
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@4c1
    const/16 v1, 0x209

    #@4c3
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4c6
    move-result-object v1

    #@4c7
    const-string v2, "application/vnd.uplanet.bearer-choice-wbxml"

    #@4c9
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@4cc
    .line 139
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@4ce
    const/16 v1, 0x20a

    #@4d0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4d3
    move-result-object v1

    #@4d4
    const-string v2, "application/vnd.phonecom.mmc-wbxml"

    #@4d6
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@4d9
    .line 140
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@4db
    const/16 v1, 0x20b

    #@4dd
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4e0
    move-result-object v1

    #@4e1
    const-string v2, "application/vnd.nokia.syncset+wbxml"

    #@4e3
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@4e6
    .line 141
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@4e8
    const/16 v1, 0x20c

    #@4ea
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4ed
    move-result-object v1

    #@4ee
    const-string v2, "image/x-up-wpng"

    #@4f0
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@4f3
    .line 142
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@4f5
    const/16 v1, 0x300

    #@4f7
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4fa
    move-result-object v1

    #@4fb
    const-string v2, "application/iota.mmc-wbxml"

    #@4fd
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@500
    .line 143
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@502
    const/16 v1, 0x301

    #@504
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@507
    move-result-object v1

    #@508
    const-string v2, "application/iota.mmc-xml"

    #@50a
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@50d
    .line 144
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@50f
    const/16 v1, 0x302

    #@511
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@514
    move-result-object v1

    #@515
    const-string v2, "application/vnd.syncml+xml"

    #@517
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@51a
    .line 145
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@51c
    const/16 v1, 0x303

    #@51e
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@521
    move-result-object v1

    #@522
    const-string v2, "application/vnd.syncml+wbxml"

    #@524
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@527
    .line 146
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@529
    const/16 v1, 0x304

    #@52b
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@52e
    move-result-object v1

    #@52f
    const-string v2, "text/vnd.wap.emn+xml"

    #@531
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@534
    .line 147
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@536
    const/16 v1, 0x305

    #@538
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@53b
    move-result-object v1

    #@53c
    const-string v2, "text/calendar"

    #@53e
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@541
    .line 148
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@543
    const/16 v1, 0x306

    #@545
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@548
    move-result-object v1

    #@549
    const-string v2, "application/vnd.omads-email+xml"

    #@54b
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@54e
    .line 149
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@550
    const/16 v1, 0x307

    #@552
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@555
    move-result-object v1

    #@556
    const-string v2, "application/vnd.omads-file+xml"

    #@558
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@55b
    .line 150
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@55d
    const/16 v1, 0x308

    #@55f
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@562
    move-result-object v1

    #@563
    const-string v2, "application/vnd.omads-folder+xml"

    #@565
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@568
    .line 151
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@56a
    const/16 v1, 0x309

    #@56c
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@56f
    move-result-object v1

    #@570
    const-string v2, "text/directory;profile=vCard"

    #@572
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@575
    .line 152
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@577
    const/16 v1, 0x30a

    #@579
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@57c
    move-result-object v1

    #@57d
    const-string v2, "application/vnd.wap.emn+wbxml"

    #@57f
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@582
    .line 153
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@584
    const/16 v1, 0x30b

    #@586
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@589
    move-result-object v1

    #@58a
    const-string v2, "application/vnd.nokia.ipdc-purchase-response"

    #@58c
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@58f
    .line 154
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@591
    const/16 v1, 0x30c

    #@593
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@596
    move-result-object v1

    #@597
    const-string v2, "application/vnd.motorola.screen3+xml"

    #@599
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@59c
    .line 155
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@59e
    const/16 v1, 0x30d

    #@5a0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5a3
    move-result-object v1

    #@5a4
    const-string v2, "application/vnd.motorola.screen3+gzip"

    #@5a6
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5a9
    .line 156
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@5ab
    const/16 v1, 0x30e

    #@5ad
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5b0
    move-result-object v1

    #@5b1
    const-string v2, "application/vnd.cmcc.setting+wbxml"

    #@5b3
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5b6
    .line 157
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@5b8
    const/16 v1, 0x30f

    #@5ba
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5bd
    move-result-object v1

    #@5be
    const-string v2, "application/vnd.cmcc.bombing+wbxml"

    #@5c0
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5c3
    .line 158
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@5c5
    const/16 v1, 0x310

    #@5c7
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5ca
    move-result-object v1

    #@5cb
    const-string v2, "application/vnd.docomo.pf"

    #@5cd
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5d0
    .line 159
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@5d2
    const/16 v1, 0x311

    #@5d4
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5d7
    move-result-object v1

    #@5d8
    const-string v2, "application/vnd.docomo.ub"

    #@5da
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5dd
    .line 160
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@5df
    const/16 v1, 0x312

    #@5e1
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5e4
    move-result-object v1

    #@5e5
    const-string v2, "application/vnd.omaloc-supl-init"

    #@5e7
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5ea
    .line 161
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@5ec
    const/16 v1, 0x313

    #@5ee
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5f1
    move-result-object v1

    #@5f2
    const-string v2, "application/vnd.oma.group-usage-list+xml"

    #@5f4
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5f7
    .line 162
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@5f9
    const/16 v1, 0x314

    #@5fb
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5fe
    move-result-object v1

    #@5ff
    const-string v2, "application/oma-directory+xml"

    #@601
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@604
    .line 163
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@606
    const/16 v1, 0x315

    #@608
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@60b
    move-result-object v1

    #@60c
    const-string v2, "application/vnd.docomo.pf2"

    #@60e
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@611
    .line 164
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@613
    const/16 v1, 0x316

    #@615
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@618
    move-result-object v1

    #@619
    const-string v2, "application/vnd.oma.drm.roap-trigger+wbxml"

    #@61b
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@61e
    .line 165
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@620
    const/16 v1, 0x317

    #@622
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@625
    move-result-object v1

    #@626
    const-string v2, "application/vnd.sbm.mid2"

    #@628
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@62b
    .line 166
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@62d
    const/16 v1, 0x318

    #@62f
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@632
    move-result-object v1

    #@633
    const-string v2, "application/vnd.wmf.bootstrap"

    #@635
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@638
    .line 167
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@63a
    const/16 v1, 0x319

    #@63c
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@63f
    move-result-object v1

    #@640
    const-string v2, "application/vnc.cmcc.dcd+xml"

    #@642
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@645
    .line 168
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@647
    const/16 v1, 0x31a

    #@649
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@64c
    move-result-object v1

    #@64d
    const-string v2, "application/vnd.sbm.cid"

    #@64f
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@652
    .line 169
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@654
    const/16 v1, 0x31b

    #@656
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@659
    move-result-object v1

    #@65a
    const-string v2, "application/vnd.oma.bcast.provisioningtrigger"

    #@65c
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@65f
    .line 171
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_PARAMETERS:Ljava/util/HashMap;

    #@661
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@664
    move-result-object v1

    #@665
    const-string v2, "Q"

    #@667
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@66a
    .line 172
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_PARAMETERS:Ljava/util/HashMap;

    #@66c
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@66f
    move-result-object v1

    #@670
    const-string v2, "Charset"

    #@672
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@675
    .line 173
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_PARAMETERS:Ljava/util/HashMap;

    #@677
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@67a
    move-result-object v1

    #@67b
    const-string v2, "Level"

    #@67d
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@680
    .line 174
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_PARAMETERS:Ljava/util/HashMap;

    #@682
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@685
    move-result-object v1

    #@686
    const-string v2, "Type"

    #@688
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@68b
    .line 175
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_PARAMETERS:Ljava/util/HashMap;

    #@68d
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@690
    move-result-object v1

    #@691
    const-string v2, "Differences"

    #@693
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@696
    .line 176
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_PARAMETERS:Ljava/util/HashMap;

    #@698
    const/16 v1, 0x8

    #@69a
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@69d
    move-result-object v1

    #@69e
    const-string v2, "Padding"

    #@6a0
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@6a3
    .line 177
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_PARAMETERS:Ljava/util/HashMap;

    #@6a5
    const/16 v1, 0x9

    #@6a7
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@6aa
    move-result-object v1

    #@6ab
    const-string v2, "Type"

    #@6ad
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@6b0
    .line 178
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_PARAMETERS:Ljava/util/HashMap;

    #@6b2
    const/16 v1, 0xe

    #@6b4
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@6b7
    move-result-object v1

    #@6b8
    const-string v2, "Max-Age"

    #@6ba
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@6bd
    .line 179
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_PARAMETERS:Ljava/util/HashMap;

    #@6bf
    const/16 v1, 0x10

    #@6c1
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@6c4
    move-result-object v1

    #@6c5
    const-string v2, "Secure"

    #@6c7
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@6ca
    .line 180
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_PARAMETERS:Ljava/util/HashMap;

    #@6cc
    const/16 v1, 0x11

    #@6ce
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@6d1
    move-result-object v1

    #@6d2
    const-string v2, "SEC"

    #@6d4
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@6d7
    .line 181
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_PARAMETERS:Ljava/util/HashMap;

    #@6d9
    const/16 v1, 0x12

    #@6db
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@6de
    move-result-object v1

    #@6df
    const-string v2, "MAC"

    #@6e1
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@6e4
    .line 182
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_PARAMETERS:Ljava/util/HashMap;

    #@6e6
    const/16 v1, 0x13

    #@6e8
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@6eb
    move-result-object v1

    #@6ec
    const-string v2, "Creation-date"

    #@6ee
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@6f1
    .line 183
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_PARAMETERS:Ljava/util/HashMap;

    #@6f3
    const/16 v1, 0x14

    #@6f5
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@6f8
    move-result-object v1

    #@6f9
    const-string v2, "Modification-date"

    #@6fb
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@6fe
    .line 184
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_PARAMETERS:Ljava/util/HashMap;

    #@700
    const/16 v1, 0x15

    #@702
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@705
    move-result-object v1

    #@706
    const-string v2, "Read-date"

    #@708
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@70b
    .line 185
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_PARAMETERS:Ljava/util/HashMap;

    #@70d
    const/16 v1, 0x16

    #@70f
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@712
    move-result-object v1

    #@713
    const-string v2, "Size"

    #@715
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@718
    .line 186
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_PARAMETERS:Ljava/util/HashMap;

    #@71a
    const/16 v1, 0x17

    #@71c
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@71f
    move-result-object v1

    #@720
    const-string v2, "Name"

    #@722
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@725
    .line 187
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_PARAMETERS:Ljava/util/HashMap;

    #@727
    const/16 v1, 0x18

    #@729
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@72c
    move-result-object v1

    #@72d
    const-string v2, "Filename"

    #@72f
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@732
    .line 188
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_PARAMETERS:Ljava/util/HashMap;

    #@734
    const/16 v1, 0x19

    #@736
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@739
    move-result-object v1

    #@73a
    const-string v2, "Start"

    #@73c
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@73f
    .line 189
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_PARAMETERS:Ljava/util/HashMap;

    #@741
    const/16 v1, 0x1a

    #@743
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@746
    move-result-object v1

    #@747
    const-string v2, "Start-info"

    #@749
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@74c
    .line 190
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_PARAMETERS:Ljava/util/HashMap;

    #@74e
    const/16 v1, 0x1b

    #@750
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@753
    move-result-object v1

    #@754
    const-string v2, "Comment"

    #@756
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@759
    .line 191
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_PARAMETERS:Ljava/util/HashMap;

    #@75b
    const/16 v1, 0x1c

    #@75d
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@760
    move-result-object v1

    #@761
    const-string v2, "Domain"

    #@763
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@766
    .line 192
    sget-object v0, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_PARAMETERS:Ljava/util/HashMap;

    #@768
    const/16 v1, 0x1d

    #@76a
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@76d
    move-result-object v1

    #@76e
    const-string v2, "Path"

    #@770
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@773
    .line 193
    return-void
.end method

.method public constructor <init>([B)V
    .registers 2
    .parameter "pdu"

    #@0
    .prologue
    .line 219
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 220
    iput-object p1, p0, Lcom/android/internal/telephony/WspTypeDecoder;->wspData:[B

    #@5
    .line 221
    return-void
.end method

.method private decodeNoValue(I)Z
    .registers 4
    .parameter "startIndex"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 550
    iget-object v1, p0, Lcom/android/internal/telephony/WspTypeDecoder;->wspData:[B

    #@3
    aget-byte v1, v1, p1

    #@5
    if-nez v1, :cond_a

    #@7
    .line 551
    iput v0, p0, Lcom/android/internal/telephony/WspTypeDecoder;->dataLength:I

    #@9
    .line 554
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method private expandWellKnownMimeType()V
    .registers 4

    #@0
    .prologue
    .line 564
    iget-object v1, p0, Lcom/android/internal/telephony/WspTypeDecoder;->stringValue:Ljava/lang/String;

    #@2
    if-nez v1, :cond_16

    #@4
    .line 565
    iget-wide v1, p0, Lcom/android/internal/telephony/WspTypeDecoder;->unsigned32bit:J

    #@6
    long-to-int v0, v1

    #@7
    .line 566
    .local v0, binaryContentType:I
    sget-object v1, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_MIME_TYPES:Ljava/util/HashMap;

    #@9
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@10
    move-result-object v1

    #@11
    check-cast v1, Ljava/lang/String;

    #@13
    iput-object v1, p0, Lcom/android/internal/telephony/WspTypeDecoder;->stringValue:Ljava/lang/String;

    #@15
    .line 570
    .end local v0           #binaryContentType:I
    :goto_15
    return-void

    #@16
    .line 568
    :cond_16
    const-wide/16 v1, -0x1

    #@18
    iput-wide v1, p0, Lcom/android/internal/telephony/WspTypeDecoder;->unsigned32bit:J

    #@1a
    goto :goto_15
.end method

.method private readContentParameters(III)Z
    .registers 15
    .parameter "startIndex"
    .parameter "leftToRead"
    .parameter "accumulator"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 481
    const/4 v3, 0x0

    #@3
    .line 483
    .local v3, totalRead:I
    if-lez p2, :cond_b7

    #@5
    .line 484
    iget-object v8, p0, Lcom/android/internal/telephony/WspTypeDecoder;->wspData:[B

    #@7
    aget-byte v1, v8, p1

    #@9
    .line 485
    .local v1, nextByte:B
    const/4 v4, 0x0

    #@a
    .line 486
    .local v4, value:Ljava/lang/String;
    const/4 v2, 0x0

    #@b
    .line 487
    .local v2, param:Ljava/lang/String;
    and-int/lit16 v8, v1, 0x80

    #@d
    if-nez v8, :cond_37

    #@f
    const/16 v8, 0x1f

    #@11
    if-le v1, v8, :cond_37

    #@13
    .line 488
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/WspTypeDecoder;->decodeTokenText(I)Z

    #@16
    .line 489
    iget-object v2, p0, Lcom/android/internal/telephony/WspTypeDecoder;->stringValue:Ljava/lang/String;

    #@18
    .line 490
    iget v6, p0, Lcom/android/internal/telephony/WspTypeDecoder;->dataLength:I

    #@1a
    add-int/2addr v3, v6

    #@1b
    .line 516
    :cond_1b
    add-int v6, p1, v3

    #@1d
    invoke-direct {p0, v6}, Lcom/android/internal/telephony/WspTypeDecoder;->decodeNoValue(I)Z

    #@20
    move-result v6

    #@21
    if-eqz v6, :cond_8c

    #@23
    .line 517
    iget v6, p0, Lcom/android/internal/telephony/WspTypeDecoder;->dataLength:I

    #@25
    add-int/2addr v3, v6

    #@26
    .line 518
    const/4 v4, 0x0

    #@27
    .line 532
    :cond_27
    :goto_27
    iget-object v6, p0, Lcom/android/internal/telephony/WspTypeDecoder;->contentParameters:Ljava/util/HashMap;

    #@29
    invoke-virtual {v6, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2c
    .line 533
    add-int v6, p1, v3

    #@2e
    sub-int v7, p2, v3

    #@30
    add-int v8, p3, v3

    #@32
    invoke-direct {p0, v6, v7, v8}, Lcom/android/internal/telephony/WspTypeDecoder;->readContentParameters(III)Z

    #@35
    move-result v6

    #@36
    .line 538
    .end local v1           #nextByte:B
    .end local v2           #param:Ljava/lang/String;
    .end local v4           #value:Ljava/lang/String;
    :cond_36
    :goto_36
    return v6

    #@37
    .line 492
    .restart local v1       #nextByte:B
    .restart local v2       #param:Ljava/lang/String;
    .restart local v4       #value:Ljava/lang/String;
    :cond_37
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/WspTypeDecoder;->decodeIntegerValue(I)Z

    #@3a
    move-result v8

    #@3b
    if-eqz v8, :cond_36

    #@3d
    .line 493
    iget v8, p0, Lcom/android/internal/telephony/WspTypeDecoder;->dataLength:I

    #@3f
    add-int/2addr v3, v8

    #@40
    .line 494
    iget-wide v8, p0, Lcom/android/internal/telephony/WspTypeDecoder;->unsigned32bit:J

    #@42
    long-to-int v5, v8

    #@43
    .line 495
    .local v5, wellKnownParameterValue:I
    sget-object v8, Lcom/android/internal/telephony/WspTypeDecoder;->WELL_KNOWN_PARAMETERS:Ljava/util/HashMap;

    #@45
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@48
    move-result-object v9

    #@49
    invoke-virtual {v8, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@4c
    move-result-object v2

    #@4d
    .end local v2           #param:Ljava/lang/String;
    check-cast v2, Ljava/lang/String;

    #@4f
    .line 496
    .restart local v2       #param:Ljava/lang/String;
    if-nez v2, :cond_69

    #@51
    .line 497
    new-instance v8, Ljava/lang/StringBuilder;

    #@53
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@56
    const-string v9, "unassigned/0x"

    #@58
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v8

    #@5c
    int-to-long v9, v5

    #@5d
    invoke-static {v9, v10}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    #@60
    move-result-object v9

    #@61
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v8

    #@65
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@68
    move-result-object v2

    #@69
    .line 500
    :cond_69
    if-nez v5, :cond_1b

    #@6b
    .line 501
    add-int v7, p1, v3

    #@6d
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/WspTypeDecoder;->decodeUintvarInteger(I)Z

    #@70
    move-result v7

    #@71
    if-eqz v7, :cond_36

    #@73
    .line 502
    iget v6, p0, Lcom/android/internal/telephony/WspTypeDecoder;->dataLength:I

    #@75
    add-int/2addr v3, v6

    #@76
    .line 503
    iget-wide v6, p0, Lcom/android/internal/telephony/WspTypeDecoder;->unsigned32bit:J

    #@78
    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    #@7b
    move-result-object v4

    #@7c
    .line 504
    iget-object v6, p0, Lcom/android/internal/telephony/WspTypeDecoder;->contentParameters:Ljava/util/HashMap;

    #@7e
    invoke-virtual {v6, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@81
    .line 505
    add-int v6, p1, v3

    #@83
    sub-int v7, p2, v3

    #@85
    add-int v8, p3, v3

    #@87
    invoke-direct {p0, v6, v7, v8}, Lcom/android/internal/telephony/WspTypeDecoder;->readContentParameters(III)Z

    #@8a
    move-result v6

    #@8b
    goto :goto_36

    #@8c
    .line 519
    .end local v5           #wellKnownParameterValue:I
    :cond_8c
    add-int v6, p1, v3

    #@8e
    invoke-virtual {p0, v6}, Lcom/android/internal/telephony/WspTypeDecoder;->decodeIntegerValue(I)Z

    #@91
    move-result v6

    #@92
    if-eqz v6, :cond_9f

    #@94
    .line 520
    iget v6, p0, Lcom/android/internal/telephony/WspTypeDecoder;->dataLength:I

    #@96
    add-int/2addr v3, v6

    #@97
    .line 521
    iget-wide v6, p0, Lcom/android/internal/telephony/WspTypeDecoder;->unsigned32bit:J

    #@99
    long-to-int v0, v6

    #@9a
    .line 522
    .local v0, intValue:I
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@9d
    move-result-object v4

    #@9e
    .line 523
    goto :goto_27

    #@9f
    .line 524
    .end local v0           #intValue:I
    :cond_9f
    add-int v6, p1, v3

    #@a1
    invoke-virtual {p0, v6}, Lcom/android/internal/telephony/WspTypeDecoder;->decodeTokenText(I)Z

    #@a4
    .line 525
    iget v6, p0, Lcom/android/internal/telephony/WspTypeDecoder;->dataLength:I

    #@a6
    add-int/2addr v3, v6

    #@a7
    .line 526
    iget-object v4, p0, Lcom/android/internal/telephony/WspTypeDecoder;->stringValue:Ljava/lang/String;

    #@a9
    .line 527
    const-string v6, "\""

    #@ab
    invoke-virtual {v4, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@ae
    move-result v6

    #@af
    if-eqz v6, :cond_27

    #@b1
    .line 529
    invoke-virtual {v4, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@b4
    move-result-object v4

    #@b5
    goto/16 :goto_27

    #@b7
    .line 537
    .end local v1           #nextByte:B
    .end local v2           #param:Ljava/lang/String;
    .end local v4           #value:Ljava/lang/String;
    :cond_b7
    iput p3, p0, Lcom/android/internal/telephony/WspTypeDecoder;->dataLength:I

    #@b9
    move v6, v7

    #@ba
    .line 538
    goto/16 :goto_36
.end method


# virtual methods
.method public decodeConstrainedEncoding(I)Z
    .registers 4
    .parameter "startIndex"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 408
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/WspTypeDecoder;->decodeShortInteger(I)Z

    #@4
    move-result v1

    #@5
    if-ne v1, v0, :cond_b

    #@7
    .line 409
    const/4 v1, 0x0

    #@8
    iput-object v1, p0, Lcom/android/internal/telephony/WspTypeDecoder;->stringValue:Ljava/lang/String;

    #@a
    .line 412
    :goto_a
    return v0

    #@b
    :cond_b
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/WspTypeDecoder;->decodeExtensionMedia(I)Z

    #@e
    move-result v0

    #@f
    goto :goto_a
.end method

.method public decodeContentLength(I)Z
    .registers 3
    .parameter "startIndex"

    #@0
    .prologue
    .line 582
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/WspTypeDecoder;->decodeIntegerValue(I)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public decodeContentLocation(I)Z
    .registers 3
    .parameter "startIndex"

    #@0
    .prologue
    .line 595
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/WspTypeDecoder;->decodeTextString(I)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public decodeContentType(I)Z
    .registers 14
    .parameter "startIndex"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v8, 0x0

    #@2
    .line 430
    new-instance v9, Ljava/util/HashMap;

    #@4
    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    #@7
    iput-object v9, p0, Lcom/android/internal/telephony/WspTypeDecoder;->contentParameters:Ljava/util/HashMap;

    #@9
    .line 433
    :try_start_9
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/WspTypeDecoder;->decodeValueLength(I)Z

    #@c
    move-result v9

    #@d
    if-nez v9, :cond_19

    #@f
    .line 434
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/WspTypeDecoder;->decodeConstrainedEncoding(I)Z

    #@12
    move-result v1

    #@13
    .line 435
    .local v1, found:Z
    if-eqz v1, :cond_18

    #@15
    .line 436
    invoke-direct {p0}, Lcom/android/internal/telephony/WspTypeDecoder;->expandWellKnownMimeType()V

    #@18
    .line 476
    .end local v1           #found:Z
    :cond_18
    :goto_18
    return v1

    #@19
    .line 440
    :cond_19
    iget-wide v9, p0, Lcom/android/internal/telephony/WspTypeDecoder;->unsigned32bit:J

    #@1b
    long-to-int v2, v9

    #@1c
    .line 441
    .local v2, headersLength:I
    invoke-virtual {p0}, Lcom/android/internal/telephony/WspTypeDecoder;->getDecodedDataLength()I

    #@1f
    move-result v3

    #@20
    .line 442
    .local v3, mediaPrefixLength:I
    add-int v9, p1, v3

    #@22
    invoke-virtual {p0, v9}, Lcom/android/internal/telephony/WspTypeDecoder;->decodeIntegerValue(I)Z

    #@25
    move-result v9

    #@26
    if-ne v9, v1, :cond_57

    #@28
    .line 443
    iget v9, p0, Lcom/android/internal/telephony/WspTypeDecoder;->dataLength:I

    #@2a
    add-int/2addr v9, v3

    #@2b
    iput v9, p0, Lcom/android/internal/telephony/WspTypeDecoder;->dataLength:I

    #@2d
    .line 444
    iget v5, p0, Lcom/android/internal/telephony/WspTypeDecoder;->dataLength:I

    #@2f
    .line 445
    .local v5, readLength:I
    const/4 v9, 0x0

    #@30
    iput-object v9, p0, Lcom/android/internal/telephony/WspTypeDecoder;->stringValue:Ljava/lang/String;

    #@32
    .line 446
    invoke-direct {p0}, Lcom/android/internal/telephony/WspTypeDecoder;->expandWellKnownMimeType()V

    #@35
    .line 447
    iget-wide v6, p0, Lcom/android/internal/telephony/WspTypeDecoder;->unsigned32bit:J

    #@37
    .line 448
    .local v6, wellKnownValue:J
    iget-object v4, p0, Lcom/android/internal/telephony/WspTypeDecoder;->stringValue:Ljava/lang/String;

    #@39
    .line 449
    .local v4, mimeType:Ljava/lang/String;
    iget v9, p0, Lcom/android/internal/telephony/WspTypeDecoder;->dataLength:I

    #@3b
    add-int/2addr v9, p1

    #@3c
    iget v10, p0, Lcom/android/internal/telephony/WspTypeDecoder;->dataLength:I

    #@3e
    sub-int/2addr v10, v3

    #@3f
    sub-int v10, v2, v10

    #@41
    const/4 v11, 0x0

    #@42
    invoke-direct {p0, v9, v10, v11}, Lcom/android/internal/telephony/WspTypeDecoder;->readContentParameters(III)Z

    #@45
    move-result v9

    #@46
    if-eqz v9, :cond_55

    #@48
    .line 451
    iget v9, p0, Lcom/android/internal/telephony/WspTypeDecoder;->dataLength:I

    #@4a
    add-int/2addr v9, v5

    #@4b
    iput v9, p0, Lcom/android/internal/telephony/WspTypeDecoder;->dataLength:I

    #@4d
    .line 452
    iput-wide v6, p0, Lcom/android/internal/telephony/WspTypeDecoder;->unsigned32bit:J

    #@4f
    .line 453
    iput-object v4, p0, Lcom/android/internal/telephony/WspTypeDecoder;->stringValue:Ljava/lang/String;

    #@51
    goto :goto_18

    #@52
    .line 472
    .end local v2           #headersLength:I
    .end local v3           #mediaPrefixLength:I
    .end local v4           #mimeType:Ljava/lang/String;
    .end local v5           #readLength:I
    .end local v6           #wellKnownValue:J
    :catch_52
    move-exception v0

    #@53
    .local v0, e:Ljava/lang/ArrayIndexOutOfBoundsException;
    move v1, v8

    #@54
    .line 474
    goto :goto_18

    #@55
    .end local v0           #e:Ljava/lang/ArrayIndexOutOfBoundsException;
    .restart local v2       #headersLength:I
    .restart local v3       #mediaPrefixLength:I
    .restart local v4       #mimeType:Ljava/lang/String;
    .restart local v5       #readLength:I
    .restart local v6       #wellKnownValue:J
    :cond_55
    move v1, v8

    #@56
    .line 456
    goto :goto_18

    #@57
    .line 458
    .end local v4           #mimeType:Ljava/lang/String;
    .end local v5           #readLength:I
    .end local v6           #wellKnownValue:J
    :cond_57
    add-int v9, p1, v3

    #@59
    invoke-virtual {p0, v9}, Lcom/android/internal/telephony/WspTypeDecoder;->decodeExtensionMedia(I)Z

    #@5c
    move-result v9

    #@5d
    if-ne v9, v1, :cond_86

    #@5f
    .line 459
    iget v9, p0, Lcom/android/internal/telephony/WspTypeDecoder;->dataLength:I

    #@61
    add-int/2addr v9, v3

    #@62
    iput v9, p0, Lcom/android/internal/telephony/WspTypeDecoder;->dataLength:I

    #@64
    .line 460
    iget v5, p0, Lcom/android/internal/telephony/WspTypeDecoder;->dataLength:I

    #@66
    .line 461
    .restart local v5       #readLength:I
    invoke-direct {p0}, Lcom/android/internal/telephony/WspTypeDecoder;->expandWellKnownMimeType()V

    #@69
    .line 462
    iget-wide v6, p0, Lcom/android/internal/telephony/WspTypeDecoder;->unsigned32bit:J

    #@6b
    .line 463
    .restart local v6       #wellKnownValue:J
    iget-object v4, p0, Lcom/android/internal/telephony/WspTypeDecoder;->stringValue:Ljava/lang/String;

    #@6d
    .line 464
    .restart local v4       #mimeType:Ljava/lang/String;
    iget v9, p0, Lcom/android/internal/telephony/WspTypeDecoder;->dataLength:I

    #@6f
    add-int/2addr v9, p1

    #@70
    iget v10, p0, Lcom/android/internal/telephony/WspTypeDecoder;->dataLength:I

    #@72
    sub-int/2addr v10, v3

    #@73
    sub-int v10, v2, v10

    #@75
    const/4 v11, 0x0

    #@76
    invoke-direct {p0, v9, v10, v11}, Lcom/android/internal/telephony/WspTypeDecoder;->readContentParameters(III)Z

    #@79
    move-result v9

    #@7a
    if-eqz v9, :cond_86

    #@7c
    .line 466
    iget v9, p0, Lcom/android/internal/telephony/WspTypeDecoder;->dataLength:I

    #@7e
    add-int/2addr v9, v5

    #@7f
    iput v9, p0, Lcom/android/internal/telephony/WspTypeDecoder;->dataLength:I

    #@81
    .line 467
    iput-wide v6, p0, Lcom/android/internal/telephony/WspTypeDecoder;->unsigned32bit:J

    #@83
    .line 468
    iput-object v4, p0, Lcom/android/internal/telephony/WspTypeDecoder;->stringValue:Ljava/lang/String;
    :try_end_85
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_9 .. :try_end_85} :catch_52

    #@85
    goto :goto_18

    #@86
    .end local v4           #mimeType:Ljava/lang/String;
    .end local v5           #readLength:I
    .end local v6           #wellKnownValue:J
    :cond_86
    move v1, v8

    #@87
    .line 476
    goto :goto_18
.end method

.method public decodeExtensionMedia(I)Z
    .registers 8
    .parameter "startIndex"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 382
    move v0, p1

    #@2
    .line 383
    .local v0, index:I
    iput v2, p0, Lcom/android/internal/telephony/WspTypeDecoder;->dataLength:I

    #@4
    .line 384
    const/4 v3, 0x0

    #@5
    iput-object v3, p0, Lcom/android/internal/telephony/WspTypeDecoder;->stringValue:Ljava/lang/String;

    #@7
    .line 385
    iget-object v3, p0, Lcom/android/internal/telephony/WspTypeDecoder;->wspData:[B

    #@9
    array-length v1, v3

    #@a
    .line 386
    .local v1, length:I
    if-ge v0, v1, :cond_d

    #@c
    const/4 v2, 0x1

    #@d
    .line 388
    .local v2, rtrn:Z
    :cond_d
    :goto_d
    if-ge v0, v1, :cond_18

    #@f
    iget-object v3, p0, Lcom/android/internal/telephony/WspTypeDecoder;->wspData:[B

    #@11
    aget-byte v3, v3, v0

    #@13
    if-eqz v3, :cond_18

    #@15
    .line 389
    add-int/lit8 v0, v0, 0x1

    #@17
    goto :goto_d

    #@18
    .line 392
    :cond_18
    sub-int v3, v0, p1

    #@1a
    add-int/lit8 v3, v3, 0x1

    #@1c
    iput v3, p0, Lcom/android/internal/telephony/WspTypeDecoder;->dataLength:I

    #@1e
    .line 393
    new-instance v3, Ljava/lang/String;

    #@20
    iget-object v4, p0, Lcom/android/internal/telephony/WspTypeDecoder;->wspData:[B

    #@22
    iget v5, p0, Lcom/android/internal/telephony/WspTypeDecoder;->dataLength:I

    #@24
    add-int/lit8 v5, v5, -0x1

    #@26
    invoke-direct {v3, v4, p1, v5}, Ljava/lang/String;-><init>([BII)V

    #@29
    iput-object v3, p0, Lcom/android/internal/telephony/WspTypeDecoder;->stringValue:Ljava/lang/String;

    #@2b
    .line 395
    return v2
.end method

.method public decodeIntegerValue(I)Z
    .registers 4
    .parameter "startIndex"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 317
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/WspTypeDecoder;->decodeShortInteger(I)Z

    #@4
    move-result v1

    #@5
    if-ne v1, v0, :cond_8

    #@7
    .line 320
    :goto_7
    return v0

    #@8
    :cond_8
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/WspTypeDecoder;->decodeLongInteger(I)Z

    #@b
    move-result v0

    #@c
    goto :goto_7
.end method

.method public decodeLongInteger(I)Z
    .registers 8
    .parameter "startIndex"

    #@0
    .prologue
    .line 294
    iget-object v2, p0, Lcom/android/internal/telephony/WspTypeDecoder;->wspData:[B

    #@2
    aget-byte v2, v2, p1

    #@4
    and-int/lit16 v1, v2, 0xff

    #@6
    .line 296
    .local v1, lengthMultiOctet:I
    const/16 v2, 0x1e

    #@8
    if-le v1, v2, :cond_c

    #@a
    .line 297
    const/4 v2, 0x0

    #@b
    .line 304
    :goto_b
    return v2

    #@c
    .line 299
    :cond_c
    const-wide/16 v2, 0x0

    #@e
    iput-wide v2, p0, Lcom/android/internal/telephony/WspTypeDecoder;->unsigned32bit:J

    #@10
    .line 300
    const/4 v0, 0x1

    #@11
    .local v0, i:I
    :goto_11
    if-gt v0, v1, :cond_27

    #@13
    .line 301
    iget-wide v2, p0, Lcom/android/internal/telephony/WspTypeDecoder;->unsigned32bit:J

    #@15
    const/16 v4, 0x8

    #@17
    shl-long/2addr v2, v4

    #@18
    iget-object v4, p0, Lcom/android/internal/telephony/WspTypeDecoder;->wspData:[B

    #@1a
    add-int v5, p1, v0

    #@1c
    aget-byte v4, v4, v5

    #@1e
    and-int/lit16 v4, v4, 0xff

    #@20
    int-to-long v4, v4

    #@21
    or-long/2addr v2, v4

    #@22
    iput-wide v2, p0, Lcom/android/internal/telephony/WspTypeDecoder;->unsigned32bit:J

    #@24
    .line 300
    add-int/lit8 v0, v0, 0x1

    #@26
    goto :goto_11

    #@27
    .line 303
    :cond_27
    add-int/lit8 v2, v1, 0x1

    #@29
    iput v2, p0, Lcom/android/internal/telephony/WspTypeDecoder;->dataLength:I

    #@2b
    .line 304
    const/4 v2, 0x1

    #@2c
    goto :goto_b
.end method

.method public decodeShortInteger(I)Z
    .registers 5
    .parameter "startIndex"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 276
    iget-object v1, p0, Lcom/android/internal/telephony/WspTypeDecoder;->wspData:[B

    #@3
    aget-byte v1, v1, p1

    #@5
    and-int/lit16 v1, v1, 0x80

    #@7
    if-nez v1, :cond_b

    #@9
    .line 277
    const/4 v0, 0x0

    #@a
    .line 281
    :goto_a
    return v0

    #@b
    .line 279
    :cond_b
    iget-object v1, p0, Lcom/android/internal/telephony/WspTypeDecoder;->wspData:[B

    #@d
    aget-byte v1, v1, p1

    #@f
    and-int/lit8 v1, v1, 0x7f

    #@11
    int-to-long v1, v1

    #@12
    iput-wide v1, p0, Lcom/android/internal/telephony/WspTypeDecoder;->unsigned32bit:J

    #@14
    .line 280
    iput v0, p0, Lcom/android/internal/telephony/WspTypeDecoder;->dataLength:I

    #@16
    goto :goto_a
.end method

.method public decodeTextString(I)Z
    .registers 7
    .parameter "startIndex"

    #@0
    .prologue
    .line 233
    move v0, p1

    #@1
    .line 234
    .local v0, index:I
    :goto_1
    iget-object v1, p0, Lcom/android/internal/telephony/WspTypeDecoder;->wspData:[B

    #@3
    aget-byte v1, v1, v0

    #@5
    if-eqz v1, :cond_a

    #@7
    .line 235
    add-int/lit8 v0, v0, 0x1

    #@9
    goto :goto_1

    #@a
    .line 237
    :cond_a
    sub-int v1, v0, p1

    #@c
    add-int/lit8 v1, v1, 0x1

    #@e
    iput v1, p0, Lcom/android/internal/telephony/WspTypeDecoder;->dataLength:I

    #@10
    .line 238
    iget-object v1, p0, Lcom/android/internal/telephony/WspTypeDecoder;->wspData:[B

    #@12
    aget-byte v1, v1, p1

    #@14
    const/16 v2, 0x7f

    #@16
    if-ne v1, v2, :cond_29

    #@18
    .line 239
    new-instance v1, Ljava/lang/String;

    #@1a
    iget-object v2, p0, Lcom/android/internal/telephony/WspTypeDecoder;->wspData:[B

    #@1c
    add-int/lit8 v3, p1, 0x1

    #@1e
    iget v4, p0, Lcom/android/internal/telephony/WspTypeDecoder;->dataLength:I

    #@20
    add-int/lit8 v4, v4, -0x2

    #@22
    invoke-direct {v1, v2, v3, v4}, Ljava/lang/String;-><init>([BII)V

    #@25
    iput-object v1, p0, Lcom/android/internal/telephony/WspTypeDecoder;->stringValue:Ljava/lang/String;

    #@27
    .line 243
    :goto_27
    const/4 v1, 0x1

    #@28
    return v1

    #@29
    .line 241
    :cond_29
    new-instance v1, Ljava/lang/String;

    #@2b
    iget-object v2, p0, Lcom/android/internal/telephony/WspTypeDecoder;->wspData:[B

    #@2d
    iget v3, p0, Lcom/android/internal/telephony/WspTypeDecoder;->dataLength:I

    #@2f
    add-int/lit8 v3, v3, -0x1

    #@31
    invoke-direct {v1, v2, p1, v3}, Ljava/lang/String;-><init>([BII)V

    #@34
    iput-object v1, p0, Lcom/android/internal/telephony/WspTypeDecoder;->stringValue:Ljava/lang/String;

    #@36
    goto :goto_27
.end method

.method public decodeTokenText(I)Z
    .registers 6
    .parameter "startIndex"

    #@0
    .prologue
    .line 256
    move v0, p1

    #@1
    .line 257
    .local v0, index:I
    :goto_1
    iget-object v1, p0, Lcom/android/internal/telephony/WspTypeDecoder;->wspData:[B

    #@3
    aget-byte v1, v1, v0

    #@5
    if-eqz v1, :cond_a

    #@7
    .line 258
    add-int/lit8 v0, v0, 0x1

    #@9
    goto :goto_1

    #@a
    .line 260
    :cond_a
    sub-int v1, v0, p1

    #@c
    add-int/lit8 v1, v1, 0x1

    #@e
    iput v1, p0, Lcom/android/internal/telephony/WspTypeDecoder;->dataLength:I

    #@10
    .line 261
    new-instance v1, Ljava/lang/String;

    #@12
    iget-object v2, p0, Lcom/android/internal/telephony/WspTypeDecoder;->wspData:[B

    #@14
    iget v3, p0, Lcom/android/internal/telephony/WspTypeDecoder;->dataLength:I

    #@16
    add-int/lit8 v3, v3, -0x1

    #@18
    invoke-direct {v1, v2, p1, v3}, Ljava/lang/String;-><init>([BII)V

    #@1b
    iput-object v1, p0, Lcom/android/internal/telephony/WspTypeDecoder;->stringValue:Ljava/lang/String;

    #@1d
    .line 263
    const/4 v1, 0x1

    #@1e
    return v1
.end method

.method public decodeUintvarInteger(I)Z
    .registers 8
    .parameter "startIndex"

    #@0
    .prologue
    const/4 v5, 0x7

    #@1
    .line 333
    move v0, p1

    #@2
    .line 335
    .local v0, index:I
    const-wide/16 v1, 0x0

    #@4
    iput-wide v1, p0, Lcom/android/internal/telephony/WspTypeDecoder;->unsigned32bit:J

    #@6
    .line 336
    :goto_6
    iget-object v1, p0, Lcom/android/internal/telephony/WspTypeDecoder;->wspData:[B

    #@8
    aget-byte v1, v1, v0

    #@a
    and-int/lit16 v1, v1, 0x80

    #@c
    if-eqz v1, :cond_25

    #@e
    .line 337
    sub-int v1, v0, p1

    #@10
    const/4 v2, 0x4

    #@11
    if-lt v1, v2, :cond_15

    #@13
    .line 338
    const/4 v1, 0x0

    #@14
    .line 345
    :goto_14
    return v1

    #@15
    .line 340
    :cond_15
    iget-wide v1, p0, Lcom/android/internal/telephony/WspTypeDecoder;->unsigned32bit:J

    #@17
    shl-long/2addr v1, v5

    #@18
    iget-object v3, p0, Lcom/android/internal/telephony/WspTypeDecoder;->wspData:[B

    #@1a
    aget-byte v3, v3, v0

    #@1c
    and-int/lit8 v3, v3, 0x7f

    #@1e
    int-to-long v3, v3

    #@1f
    or-long/2addr v1, v3

    #@20
    iput-wide v1, p0, Lcom/android/internal/telephony/WspTypeDecoder;->unsigned32bit:J

    #@22
    .line 341
    add-int/lit8 v0, v0, 0x1

    #@24
    goto :goto_6

    #@25
    .line 343
    :cond_25
    iget-wide v1, p0, Lcom/android/internal/telephony/WspTypeDecoder;->unsigned32bit:J

    #@27
    shl-long/2addr v1, v5

    #@28
    iget-object v3, p0, Lcom/android/internal/telephony/WspTypeDecoder;->wspData:[B

    #@2a
    aget-byte v3, v3, v0

    #@2c
    and-int/lit8 v3, v3, 0x7f

    #@2e
    int-to-long v3, v3

    #@2f
    or-long/2addr v1, v3

    #@30
    iput-wide v1, p0, Lcom/android/internal/telephony/WspTypeDecoder;->unsigned32bit:J

    #@32
    .line 344
    sub-int v1, v0, p1

    #@34
    add-int/lit8 v1, v1, 0x1

    #@36
    iput v1, p0, Lcom/android/internal/telephony/WspTypeDecoder;->dataLength:I

    #@38
    .line 345
    const/4 v1, 0x1

    #@39
    goto :goto_14
.end method

.method public decodeValueLength(I)Z
    .registers 5
    .parameter "startIndex"

    #@0
    .prologue
    const/16 v2, 0x1f

    #@2
    const/4 v0, 0x1

    #@3
    .line 358
    iget-object v1, p0, Lcom/android/internal/telephony/WspTypeDecoder;->wspData:[B

    #@5
    aget-byte v1, v1, p1

    #@7
    and-int/lit16 v1, v1, 0xff

    #@9
    if-le v1, v2, :cond_d

    #@b
    .line 359
    const/4 v0, 0x0

    #@c
    .line 368
    :goto_c
    return v0

    #@d
    .line 361
    :cond_d
    iget-object v1, p0, Lcom/android/internal/telephony/WspTypeDecoder;->wspData:[B

    #@f
    aget-byte v1, v1, p1

    #@11
    if-ge v1, v2, :cond_1d

    #@13
    .line 362
    iget-object v1, p0, Lcom/android/internal/telephony/WspTypeDecoder;->wspData:[B

    #@15
    aget-byte v1, v1, p1

    #@17
    int-to-long v1, v1

    #@18
    iput-wide v1, p0, Lcom/android/internal/telephony/WspTypeDecoder;->unsigned32bit:J

    #@1a
    .line 363
    iput v0, p0, Lcom/android/internal/telephony/WspTypeDecoder;->dataLength:I

    #@1c
    goto :goto_c

    #@1d
    .line 365
    :cond_1d
    add-int/lit8 v1, p1, 0x1

    #@1f
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/WspTypeDecoder;->decodeUintvarInteger(I)Z

    #@22
    .line 366
    iget v1, p0, Lcom/android/internal/telephony/WspTypeDecoder;->dataLength:I

    #@24
    add-int/lit8 v1, v1, 0x1

    #@26
    iput v1, p0, Lcom/android/internal/telephony/WspTypeDecoder;->dataLength:I

    #@28
    goto :goto_c
.end method

.method public decodeXWapApplicationId(I)Z
    .registers 4
    .parameter "startIndex"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 609
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/WspTypeDecoder;->decodeIntegerValue(I)Z

    #@4
    move-result v1

    #@5
    if-ne v1, v0, :cond_b

    #@7
    .line 610
    const/4 v1, 0x0

    #@8
    iput-object v1, p0, Lcom/android/internal/telephony/WspTypeDecoder;->stringValue:Ljava/lang/String;

    #@a
    .line 613
    :goto_a
    return v0

    #@b
    :cond_b
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/WspTypeDecoder;->decodeTextString(I)Z

    #@e
    move-result v0

    #@f
    goto :goto_a
.end method

.method public decodeXWapContentURI(I)Z
    .registers 3
    .parameter "startIndex"

    #@0
    .prologue
    .line 690
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/WspTypeDecoder;->decodeTextString(I)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public decodeXWapInitiatorURI(I)Z
    .registers 3
    .parameter "startIndex"

    #@0
    .prologue
    .line 703
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/WspTypeDecoder;->decodeTextString(I)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public getContentParameters()Ljava/util/HashMap;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 738
    iget-object v0, p0, Lcom/android/internal/telephony/WspTypeDecoder;->contentParameters:Ljava/util/HashMap;

    #@2
    return-object v0
.end method

.method public getDecodedDataLength()I
    .registers 2

    #@0
    .prologue
    .line 710
    iget v0, p0, Lcom/android/internal/telephony/WspTypeDecoder;->dataLength:I

    #@2
    return v0
.end method

.method public getValue32()J
    .registers 3

    #@0
    .prologue
    .line 717
    iget-wide v0, p0, Lcom/android/internal/telephony/WspTypeDecoder;->unsigned32bit:J

    #@2
    return-wide v0
.end method

.method public getValueString()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 724
    iget-object v0, p0, Lcom/android/internal/telephony/WspTypeDecoder;->stringValue:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public seekXWapApplicationId(II)Z
    .registers 11
    .parameter "startIndex"
    .parameter "endIndex"

    #@0
    .prologue
    const/16 v7, 0x1f

    #@2
    const/4 v4, 0x0

    #@3
    .line 626
    move v2, p1

    #@4
    .line 629
    .local v2, index:I
    move v2, p1

    #@5
    :goto_5
    if-gt v2, p2, :cond_1c

    #@7
    .line 634
    :try_start_7
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/WspTypeDecoder;->decodeIntegerValue(I)Z

    #@a
    move-result v5

    #@b
    if-eqz v5, :cond_1d

    #@d
    .line 635
    invoke-virtual {p0}, Lcom/android/internal/telephony/WspTypeDecoder;->getValue32()J

    #@10
    move-result-wide v5

    #@11
    long-to-int v1, v5

    #@12
    .line 637
    .local v1, fieldValue:I
    const/16 v5, 0x2f

    #@14
    if-ne v1, v5, :cond_23

    #@16
    .line 638
    add-int/lit8 v5, v2, 0x1

    #@18
    int-to-long v5, v5

    #@19
    iput-wide v5, p0, Lcom/android/internal/telephony/WspTypeDecoder;->unsigned32bit:J

    #@1b
    .line 639
    const/4 v4, 0x1

    #@1c
    .line 677
    .end local v1           #fieldValue:I
    :cond_1c
    :goto_1c
    return v4

    #@1d
    .line 642
    :cond_1d
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/WspTypeDecoder;->decodeTextString(I)Z

    #@20
    move-result v5

    #@21
    if-eqz v5, :cond_1c

    #@23
    .line 644
    :cond_23
    invoke-virtual {p0}, Lcom/android/internal/telephony/WspTypeDecoder;->getDecodedDataLength()I

    #@26
    move-result v5

    #@27
    add-int/2addr v2, v5

    #@28
    .line 645
    if-gt v2, p2, :cond_1c

    #@2a
    .line 658
    iget-object v5, p0, Lcom/android/internal/telephony/WspTypeDecoder;->wspData:[B

    #@2c
    aget-byte v3, v5, v2

    #@2e
    .line 659
    .local v3, val:B
    if-ltz v3, :cond_3c

    #@30
    const/16 v5, 0x1e

    #@32
    if-gt v3, v5, :cond_3c

    #@34
    .line 660
    iget-object v5, p0, Lcom/android/internal/telephony/WspTypeDecoder;->wspData:[B

    #@36
    aget-byte v5, v5, v2

    #@38
    add-int/lit8 v5, v5, 0x1

    #@3a
    add-int/2addr v2, v5

    #@3b
    goto :goto_5

    #@3c
    .line 661
    :cond_3c
    if-ne v3, v7, :cond_50

    #@3e
    .line 662
    add-int/lit8 v5, v2, 0x1

    #@40
    if-ge v5, p2, :cond_1c

    #@42
    .line 663
    add-int/lit8 v2, v2, 0x1

    #@44
    .line 664
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/WspTypeDecoder;->decodeUintvarInteger(I)Z

    #@47
    move-result v5

    #@48
    if-eqz v5, :cond_1c

    #@4a
    .line 665
    invoke-virtual {p0}, Lcom/android/internal/telephony/WspTypeDecoder;->getDecodedDataLength()I

    #@4d
    move-result v5

    #@4e
    add-int/2addr v2, v5

    #@4f
    goto :goto_5

    #@50
    .line 666
    :cond_50
    if-ge v7, v3, :cond_62

    #@52
    const/16 v5, 0x7f

    #@54
    if-gt v3, v5, :cond_62

    #@56
    .line 667
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/WspTypeDecoder;->decodeTextString(I)Z

    #@59
    move-result v5

    #@5a
    if-eqz v5, :cond_1c

    #@5c
    .line 668
    invoke-virtual {p0}, Lcom/android/internal/telephony/WspTypeDecoder;->getDecodedDataLength()I
    :try_end_5f
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_7 .. :try_end_5f} :catch_65

    #@5f
    move-result v5

    #@60
    add-int/2addr v2, v5

    #@61
    goto :goto_5

    #@62
    .line 670
    :cond_62
    add-int/lit8 v2, v2, 0x1

    #@64
    goto :goto_5

    #@65
    .line 673
    .end local v3           #val:B
    :catch_65
    move-exception v0

    #@66
    .line 675
    .local v0, e:Ljava/lang/ArrayIndexOutOfBoundsException;
    goto :goto_1c
.end method
