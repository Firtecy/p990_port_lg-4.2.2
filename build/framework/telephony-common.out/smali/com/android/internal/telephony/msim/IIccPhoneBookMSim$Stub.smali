.class public abstract Lcom/android/internal/telephony/msim/IIccPhoneBookMSim$Stub;
.super Landroid/os/Binder;
.source "IIccPhoneBookMSim.java"

# interfaces
.implements Lcom/android/internal/telephony/msim/IIccPhoneBookMSim;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/msim/IIccPhoneBookMSim;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/msim/IIccPhoneBookMSim$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.android.internal.telephony.msim.IIccPhoneBookMSim"

.field static final TRANSACTION_getAdnRecordsInEf:I = 0x1

.field static final TRANSACTION_getAdnRecordsSize:I = 0x4

.field static final TRANSACTION_updateAdnRecordsInEfByIndex:I = 0x3

.field static final TRANSACTION_updateAdnRecordsInEfBySearch:I = 0x2


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 28
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 29
    const-string v0, "com.android.internal.telephony.msim.IIccPhoneBookMSim"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/android/internal/telephony/msim/IIccPhoneBookMSim$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 30
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/msim/IIccPhoneBookMSim;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 37
    if-nez p0, :cond_4

    #@2
    .line 38
    const/4 v0, 0x0

    #@3
    .line 44
    :goto_3
    return-object v0

    #@4
    .line 40
    :cond_4
    const-string v1, "com.android.internal.telephony.msim.IIccPhoneBookMSim"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 41
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/android/internal/telephony/msim/IIccPhoneBookMSim;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 42
    check-cast v0, Lcom/android/internal/telephony/msim/IIccPhoneBookMSim;

    #@12
    goto :goto_3

    #@13
    .line 44
    :cond_13
    new-instance v0, Lcom/android/internal/telephony/msim/IIccPhoneBookMSim$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/msim/IIccPhoneBookMSim$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 48
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 15
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 52
    sparse-switch p1, :sswitch_data_a4

    #@3
    .line 126
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@6
    move-result v0

    #@7
    :goto_7
    return v0

    #@8
    .line 56
    :sswitch_8
    const-string v0, "com.android.internal.telephony.msim.IIccPhoneBookMSim"

    #@a
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@d
    .line 57
    const/4 v0, 0x1

    #@e
    goto :goto_7

    #@f
    .line 61
    :sswitch_f
    const-string v0, "com.android.internal.telephony.msim.IIccPhoneBookMSim"

    #@11
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 63
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@17
    move-result v1

    #@18
    .line 65
    .local v1, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1b
    move-result v2

    #@1c
    .line 66
    .local v2, _arg1:I
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/telephony/msim/IIccPhoneBookMSim$Stub;->getAdnRecordsInEf(II)Ljava/util/List;

    #@1f
    move-result-object v9

    #@20
    .line 67
    .local v9, _result:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/telephony/uicc/AdnRecord;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@23
    .line 68
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@26
    .line 69
    const/4 v0, 0x1

    #@27
    goto :goto_7

    #@28
    .line 73
    .end local v1           #_arg0:I
    .end local v2           #_arg1:I
    .end local v9           #_result:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/telephony/uicc/AdnRecord;>;"
    :sswitch_28
    const-string v0, "com.android.internal.telephony.msim.IIccPhoneBookMSim"

    #@2a
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2d
    .line 75
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@30
    move-result v1

    #@31
    .line 77
    .restart local v1       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@34
    move-result-object v2

    #@35
    .line 79
    .local v2, _arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@38
    move-result-object v3

    #@39
    .line 81
    .local v3, _arg2:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3c
    move-result-object v4

    #@3d
    .line 83
    .local v4, _arg3:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@40
    move-result-object v5

    #@41
    .line 85
    .local v5, _arg4:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@44
    move-result-object v6

    #@45
    .line 87
    .local v6, _arg5:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@48
    move-result v7

    #@49
    .local v7, _arg6:I
    move-object v0, p0

    #@4a
    .line 88
    invoke-virtual/range {v0 .. v7}, Lcom/android/internal/telephony/msim/IIccPhoneBookMSim$Stub;->updateAdnRecordsInEfBySearch(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Z

    #@4d
    move-result v8

    #@4e
    .line 89
    .local v8, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@51
    .line 90
    if-eqz v8, :cond_59

    #@53
    const/4 v0, 0x1

    #@54
    :goto_54
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@57
    .line 91
    const/4 v0, 0x1

    #@58
    goto :goto_7

    #@59
    .line 90
    :cond_59
    const/4 v0, 0x0

    #@5a
    goto :goto_54

    #@5b
    .line 95
    .end local v1           #_arg0:I
    .end local v2           #_arg1:Ljava/lang/String;
    .end local v3           #_arg2:Ljava/lang/String;
    .end local v4           #_arg3:Ljava/lang/String;
    .end local v5           #_arg4:Ljava/lang/String;
    .end local v6           #_arg5:Ljava/lang/String;
    .end local v7           #_arg6:I
    .end local v8           #_result:Z
    :sswitch_5b
    const-string v0, "com.android.internal.telephony.msim.IIccPhoneBookMSim"

    #@5d
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@60
    .line 97
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@63
    move-result v1

    #@64
    .line 99
    .restart local v1       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@67
    move-result-object v2

    #@68
    .line 101
    .restart local v2       #_arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@6b
    move-result-object v3

    #@6c
    .line 103
    .restart local v3       #_arg2:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@6f
    move-result v4

    #@70
    .line 105
    .local v4, _arg3:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@73
    move-result-object v5

    #@74
    .line 107
    .restart local v5       #_arg4:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@77
    move-result v6

    #@78
    .local v6, _arg5:I
    move-object v0, p0

    #@79
    .line 108
    invoke-virtual/range {v0 .. v6}, Lcom/android/internal/telephony/msim/IIccPhoneBookMSim$Stub;->updateAdnRecordsInEfByIndex(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;I)Z

    #@7c
    move-result v8

    #@7d
    .line 109
    .restart local v8       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@80
    .line 110
    if-eqz v8, :cond_88

    #@82
    const/4 v0, 0x1

    #@83
    :goto_83
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@86
    .line 111
    const/4 v0, 0x1

    #@87
    goto :goto_7

    #@88
    .line 110
    :cond_88
    const/4 v0, 0x0

    #@89
    goto :goto_83

    #@8a
    .line 115
    .end local v1           #_arg0:I
    .end local v2           #_arg1:Ljava/lang/String;
    .end local v3           #_arg2:Ljava/lang/String;
    .end local v4           #_arg3:I
    .end local v5           #_arg4:Ljava/lang/String;
    .end local v6           #_arg5:I
    .end local v8           #_result:Z
    :sswitch_8a
    const-string v0, "com.android.internal.telephony.msim.IIccPhoneBookMSim"

    #@8c
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@8f
    .line 117
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@92
    move-result v1

    #@93
    .line 119
    .restart local v1       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@96
    move-result v2

    #@97
    .line 120
    .local v2, _arg1:I
    invoke-virtual {p0, v1, v2}, Lcom/android/internal/telephony/msim/IIccPhoneBookMSim$Stub;->getAdnRecordsSize(II)[I

    #@9a
    move-result-object v8

    #@9b
    .line 121
    .local v8, _result:[I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@9e
    .line 122
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeIntArray([I)V

    #@a1
    .line 123
    const/4 v0, 0x1

    #@a2
    goto/16 :goto_7

    #@a4
    .line 52
    :sswitch_data_a4
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_28
        0x3 -> :sswitch_5b
        0x4 -> :sswitch_8a
        0x5f4e5446 -> :sswitch_8
    .end sparse-switch
.end method
