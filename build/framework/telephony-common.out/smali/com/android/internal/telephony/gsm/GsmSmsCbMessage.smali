.class public Lcom/android/internal/telephony/gsm/GsmSmsCbMessage;
.super Ljava/lang/Object;
.source "GsmSmsCbMessage.java"


# static fields
.field private static final CARRIAGE_RETURN:C = '\r'

.field private static final LANGUAGE_CODES_GROUP_0:[Ljava/lang/String; = null

.field private static final LANGUAGE_CODES_GROUP_2:[Ljava/lang/String; = null

.field private static final PDU_BODY_PAGE_LENGTH:I = 0x52


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x3

    #@1
    const/4 v6, 0x2

    #@2
    const/4 v5, 0x1

    #@3
    const/4 v4, 0x0

    #@4
    const/4 v3, 0x0

    #@5
    .line 37
    const/16 v0, 0x10

    #@7
    new-array v0, v0, [Ljava/lang/String;

    #@9
    const-string v1, "de"

    #@b
    aput-object v1, v0, v4

    #@d
    const-string v1, "en"

    #@f
    aput-object v1, v0, v5

    #@11
    const-string v1, "it"

    #@13
    aput-object v1, v0, v6

    #@15
    const-string v1, "fr"

    #@17
    aput-object v1, v0, v7

    #@19
    const/4 v1, 0x4

    #@1a
    const-string v2, "es"

    #@1c
    aput-object v2, v0, v1

    #@1e
    const/4 v1, 0x5

    #@1f
    const-string v2, "nl"

    #@21
    aput-object v2, v0, v1

    #@23
    const/4 v1, 0x6

    #@24
    const-string v2, "sv"

    #@26
    aput-object v2, v0, v1

    #@28
    const/4 v1, 0x7

    #@29
    const-string v2, "da"

    #@2b
    aput-object v2, v0, v1

    #@2d
    const/16 v1, 0x8

    #@2f
    const-string v2, "pt"

    #@31
    aput-object v2, v0, v1

    #@33
    const/16 v1, 0x9

    #@35
    const-string v2, "fi"

    #@37
    aput-object v2, v0, v1

    #@39
    const/16 v1, 0xa

    #@3b
    const-string v2, "no"

    #@3d
    aput-object v2, v0, v1

    #@3f
    const/16 v1, 0xb

    #@41
    const-string v2, "el"

    #@43
    aput-object v2, v0, v1

    #@45
    const/16 v1, 0xc

    #@47
    const-string v2, "tr"

    #@49
    aput-object v2, v0, v1

    #@4b
    const/16 v1, 0xd

    #@4d
    const-string v2, "hu"

    #@4f
    aput-object v2, v0, v1

    #@51
    const/16 v1, 0xe

    #@53
    const-string v2, "pl"

    #@55
    aput-object v2, v0, v1

    #@57
    const/16 v1, 0xf

    #@59
    aput-object v3, v0, v1

    #@5b
    sput-object v0, Lcom/android/internal/telephony/gsm/GsmSmsCbMessage;->LANGUAGE_CODES_GROUP_0:[Ljava/lang/String;

    #@5d
    .line 45
    const/16 v0, 0x10

    #@5f
    new-array v0, v0, [Ljava/lang/String;

    #@61
    const-string v1, "cs"

    #@63
    aput-object v1, v0, v4

    #@65
    const-string v1, "he"

    #@67
    aput-object v1, v0, v5

    #@69
    const-string v1, "ar"

    #@6b
    aput-object v1, v0, v6

    #@6d
    const-string v1, "ru"

    #@6f
    aput-object v1, v0, v7

    #@71
    const/4 v1, 0x4

    #@72
    const-string v2, "is"

    #@74
    aput-object v2, v0, v1

    #@76
    const/4 v1, 0x5

    #@77
    aput-object v3, v0, v1

    #@79
    const/4 v1, 0x6

    #@7a
    aput-object v3, v0, v1

    #@7c
    const/4 v1, 0x7

    #@7d
    aput-object v3, v0, v1

    #@7f
    const/16 v1, 0x8

    #@81
    aput-object v3, v0, v1

    #@83
    const/16 v1, 0x9

    #@85
    aput-object v3, v0, v1

    #@87
    const/16 v1, 0xa

    #@89
    aput-object v3, v0, v1

    #@8b
    const/16 v1, 0xb

    #@8d
    aput-object v3, v0, v1

    #@8f
    const/16 v1, 0xc

    #@91
    aput-object v3, v0, v1

    #@93
    const/16 v1, 0xd

    #@95
    aput-object v3, v0, v1

    #@97
    const/16 v1, 0xe

    #@99
    aput-object v3, v0, v1

    #@9b
    const/16 v1, 0xf

    #@9d
    aput-object v3, v0, v1

    #@9f
    sput-object v0, Lcom/android/internal/telephony/gsm/GsmSmsCbMessage;->LANGUAGE_CODES_GROUP_2:[Ljava/lang/String;

    #@a1
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 55
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static createSmsCbMessage(Landroid/telephony/SmsCbLocation;[[B)Landroid/telephony/SmsCbMessage;
    .registers 4
    .parameter "location"
    .parameter "pdus"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 97
    new-instance v0, Lcom/android/internal/telephony/gsm/SmsCbHeader;

    #@2
    const/4 v1, 0x0

    #@3
    aget-object v1, p1, v1

    #@5
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/gsm/SmsCbHeader;-><init>([B)V

    #@8
    .line 98
    .local v0, header:Lcom/android/internal/telephony/gsm/SmsCbHeader;
    invoke-static {v0, p0, p1}, Lcom/android/internal/telephony/gsm/GsmSmsCbMessage;->createSmsCbMessage(Lcom/android/internal/telephony/gsm/SmsCbHeader;Landroid/telephony/SmsCbLocation;[[B)Landroid/telephony/SmsCbMessage;

    #@b
    move-result-object v1

    #@c
    return-object v1
.end method

.method static createSmsCbMessage(Lcom/android/internal/telephony/gsm/SmsCbHeader;Landroid/telephony/SmsCbLocation;[[B)Landroid/telephony/SmsCbMessage;
    .registers 22
    .parameter "header"
    .parameter "location"
    .parameter "pdus"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    #@0
    .prologue
    .line 64
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/SmsCbHeader;->isEtwsPrimaryNotification()Z

    #@3
    move-result v2

    #@4
    if-eqz v2, :cond_27

    #@6
    .line 65
    new-instance v2, Landroid/telephony/SmsCbMessage;

    #@8
    const/4 v3, 0x1

    #@9
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/SmsCbHeader;->getGeographicalScope()I

    #@c
    move-result v4

    #@d
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/SmsCbHeader;->getSerialNumber()I

    #@10
    move-result v5

    #@11
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/SmsCbHeader;->getServiceCategory()I

    #@14
    move-result v7

    #@15
    const/4 v8, 0x0

    #@16
    const-string v9, "ETWS"

    #@18
    const/4 v10, 0x3

    #@19
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/SmsCbHeader;->getEtwsInfo()Landroid/telephony/SmsCbEtwsInfo;

    #@1c
    move-result-object v11

    #@1d
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/SmsCbHeader;->getCmasInfo()Landroid/telephony/SmsCbCmasInfo;

    #@20
    move-result-object v12

    #@21
    move-object/from16 v6, p1

    #@23
    invoke-direct/range {v2 .. v12}, Landroid/telephony/SmsCbMessage;-><init>(IIILandroid/telephony/SmsCbLocation;ILjava/lang/String;Ljava/lang/String;ILandroid/telephony/SmsCbEtwsInfo;Landroid/telephony/SmsCbCmasInfo;)V

    #@26
    .line 81
    :goto_26
    return-object v2

    #@27
    .line 71
    :cond_27
    const/4 v8, 0x0

    #@28
    .line 72
    .local v8, language:Ljava/lang/String;
    new-instance v18, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    .line 73
    .local v18, sb:Ljava/lang/StringBuilder;
    move-object/from16 v13, p2

    #@2f
    .local v13, arr$:[[B
    array-length v15, v13

    #@30
    .local v15, len$:I
    const/4 v14, 0x0

    #@31
    .local v14, i$:I
    :goto_31
    if-ge v14, v15, :cond_51

    #@33
    aget-object v17, v13, v14

    #@35
    .line 74
    .local v17, pdu:[B
    move-object/from16 v0, p0

    #@37
    move-object/from16 v1, v17

    #@39
    invoke-static {v0, v1}, Lcom/android/internal/telephony/gsm/GsmSmsCbMessage;->parseBody(Lcom/android/internal/telephony/gsm/SmsCbHeader;[B)Landroid/util/Pair;

    #@3c
    move-result-object v16

    #@3d
    .line 75
    .local v16, p:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    move-object/from16 v0, v16

    #@3f
    iget-object v8, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    #@41
    .end local v8           #language:Ljava/lang/String;
    check-cast v8, Ljava/lang/String;

    #@43
    .line 76
    .restart local v8       #language:Ljava/lang/String;
    move-object/from16 v0, v16

    #@45
    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    #@47
    check-cast v2, Ljava/lang/String;

    #@49
    move-object/from16 v0, v18

    #@4b
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    .line 73
    add-int/lit8 v14, v14, 0x1

    #@50
    goto :goto_31

    #@51
    .line 78
    .end local v16           #p:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v17           #pdu:[B
    :cond_51
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/SmsCbHeader;->isEmergencyMessage()Z

    #@54
    move-result v2

    #@55
    if-eqz v2, :cond_79

    #@57
    const/4 v10, 0x3

    #@58
    .line 81
    .local v10, priority:I
    :goto_58
    new-instance v2, Landroid/telephony/SmsCbMessage;

    #@5a
    const/4 v3, 0x1

    #@5b
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/SmsCbHeader;->getGeographicalScope()I

    #@5e
    move-result v4

    #@5f
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/SmsCbHeader;->getSerialNumber()I

    #@62
    move-result v5

    #@63
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/SmsCbHeader;->getServiceCategory()I

    #@66
    move-result v7

    #@67
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6a
    move-result-object v9

    #@6b
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/SmsCbHeader;->getEtwsInfo()Landroid/telephony/SmsCbEtwsInfo;

    #@6e
    move-result-object v11

    #@6f
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/gsm/SmsCbHeader;->getCmasInfo()Landroid/telephony/SmsCbCmasInfo;

    #@72
    move-result-object v12

    #@73
    move-object/from16 v6, p1

    #@75
    invoke-direct/range {v2 .. v12}, Landroid/telephony/SmsCbMessage;-><init>(IIILandroid/telephony/SmsCbLocation;ILjava/lang/String;Ljava/lang/String;ILandroid/telephony/SmsCbEtwsInfo;Landroid/telephony/SmsCbCmasInfo;)V

    #@78
    goto :goto_26

    #@79
    .line 78
    .end local v10           #priority:I
    :cond_79
    const/4 v10, 0x0

    #@7a
    goto :goto_58
.end method

.method private static parseBody(Lcom/android/internal/telephony/gsm/SmsCbHeader;[B)Landroid/util/Pair;
    .registers 16
    .parameter "header"
    .parameter "pdu"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/internal/telephony/gsm/SmsCbHeader;",
            "[B)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    const/16 v13, 0x52

    #@2
    const/4 v11, 0x1

    #@3
    .line 112
    const/4 v5, 0x0

    #@4
    .line 113
    .local v5, language:Ljava/lang/String;
    const/4 v4, 0x0

    #@5
    .line 114
    .local v4, hasLanguageIndicator:Z
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/SmsCbHeader;->getDataCodingScheme()I

    #@8
    move-result v6

    #@9
    .line 118
    .local v6, dataCodingScheme:I
    and-int/lit16 v0, v6, 0xf0

    #@b
    shr-int/lit8 v0, v0, 0x4

    #@d
    packed-switch v0, :pswitch_data_f4

    #@10
    .line 180
    :pswitch_10
    const/4 v1, 0x1

    #@11
    .line 184
    .local v1, encoding:I
    :goto_11
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/SmsCbHeader;->isUmtsFormat()Z

    #@14
    move-result v0

    #@15
    if-eqz v0, :cond_ea

    #@17
    .line 186
    const/4 v0, 0x6

    #@18
    aget-byte v8, p1, v0

    #@1a
    .line 188
    .local v8, nrPages:I
    array-length v0, p1

    #@1b
    mul-int/lit8 v11, v8, 0x53

    #@1d
    add-int/lit8 v11, v11, 0x7

    #@1f
    if-ge v0, v11, :cond_98

    #@21
    .line 190
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@23
    new-instance v11, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v12, "Pdu length "

    #@2a
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v11

    #@2e
    array-length v12, p1

    #@2f
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v11

    #@33
    const-string v12, " does not match "

    #@35
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v11

    #@39
    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v11

    #@3d
    const-string v12, " pages"

    #@3f
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v11

    #@43
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v11

    #@47
    invoke-direct {v0, v11}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@4a
    throw v0

    #@4b
    .line 120
    .end local v1           #encoding:I
    .end local v8           #nrPages:I
    :pswitch_4b
    const/4 v1, 0x1

    #@4c
    .line 121
    .restart local v1       #encoding:I
    sget-object v0, Lcom/android/internal/telephony/gsm/GsmSmsCbMessage;->LANGUAGE_CODES_GROUP_0:[Ljava/lang/String;

    #@4e
    and-int/lit8 v11, v6, 0xf

    #@50
    aget-object v5, v0, v11

    #@52
    .line 122
    goto :goto_11

    #@53
    .line 125
    .end local v1           #encoding:I
    :pswitch_53
    const/4 v4, 0x1

    #@54
    .line 126
    and-int/lit8 v0, v6, 0xf

    #@56
    if-ne v0, v11, :cond_5a

    #@58
    .line 127
    const/4 v1, 0x3

    #@59
    .restart local v1       #encoding:I
    goto :goto_11

    #@5a
    .line 129
    .end local v1           #encoding:I
    :cond_5a
    const/4 v1, 0x1

    #@5b
    .line 131
    .restart local v1       #encoding:I
    goto :goto_11

    #@5c
    .line 134
    .end local v1           #encoding:I
    :pswitch_5c
    const/4 v1, 0x1

    #@5d
    .line 135
    .restart local v1       #encoding:I
    sget-object v0, Lcom/android/internal/telephony/gsm/GsmSmsCbMessage;->LANGUAGE_CODES_GROUP_2:[Ljava/lang/String;

    #@5f
    and-int/lit8 v11, v6, 0xf

    #@61
    aget-object v5, v0, v11

    #@63
    .line 136
    goto :goto_11

    #@64
    .line 139
    .end local v1           #encoding:I
    :pswitch_64
    const/4 v1, 0x1

    #@65
    .line 140
    .restart local v1       #encoding:I
    goto :goto_11

    #@66
    .line 144
    .end local v1           #encoding:I
    :pswitch_66
    and-int/lit8 v0, v6, 0xc

    #@68
    shr-int/lit8 v0, v0, 0x2

    #@6a
    packed-switch v0, :pswitch_data_118

    #@6d
    .line 155
    const/4 v1, 0x1

    #@6e
    .line 156
    .restart local v1       #encoding:I
    goto :goto_11

    #@6f
    .line 146
    .end local v1           #encoding:I
    :pswitch_6f
    const/4 v1, 0x2

    #@70
    .line 147
    .restart local v1       #encoding:I
    goto :goto_11

    #@71
    .line 150
    .end local v1           #encoding:I
    :pswitch_71
    const/4 v1, 0x3

    #@72
    .line 151
    .restart local v1       #encoding:I
    goto :goto_11

    #@73
    .line 167
    .end local v1           #encoding:I
    :pswitch_73
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@75
    new-instance v11, Ljava/lang/StringBuilder;

    #@77
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@7a
    const-string v12, "Unsupported GSM dataCodingScheme "

    #@7c
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v11

    #@80
    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@83
    move-result-object v11

    #@84
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@87
    move-result-object v11

    #@88
    invoke-direct {v0, v11}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@8b
    throw v0

    #@8c
    .line 171
    :pswitch_8c
    and-int/lit8 v0, v6, 0x4

    #@8e
    shr-int/lit8 v0, v0, 0x2

    #@90
    if-ne v0, v11, :cond_95

    #@92
    .line 172
    const/4 v1, 0x2

    #@93
    .restart local v1       #encoding:I
    goto/16 :goto_11

    #@95
    .line 174
    .end local v1           #encoding:I
    :cond_95
    const/4 v1, 0x1

    #@96
    .line 176
    .restart local v1       #encoding:I
    goto/16 :goto_11

    #@98
    .line 194
    .restart local v8       #nrPages:I
    :cond_98
    new-instance v10, Ljava/lang/StringBuilder;

    #@9a
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@9d
    .line 196
    .local v10, sb:Ljava/lang/StringBuilder;
    const/4 v7, 0x0

    #@9e
    .local v7, i:I
    :goto_9e
    if-ge v7, v8, :cond_e0

    #@a0
    .line 199
    mul-int/lit8 v0, v7, 0x53

    #@a2
    add-int/lit8 v2, v0, 0x7

    #@a4
    .line 200
    .local v2, offset:I
    add-int/lit8 v0, v2, 0x52

    #@a6
    aget-byte v3, p1, v0

    #@a8
    .line 202
    .local v3, length:I
    if-le v3, v13, :cond_cd

    #@aa
    .line 203
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@ac
    new-instance v11, Ljava/lang/StringBuilder;

    #@ae
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@b1
    const-string v12, "Page length "

    #@b3
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b6
    move-result-object v11

    #@b7
    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v11

    #@bb
    const-string v12, " exceeds maximum value "

    #@bd
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v11

    #@c1
    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c4
    move-result-object v11

    #@c5
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c8
    move-result-object v11

    #@c9
    invoke-direct {v0, v11}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@cc
    throw v0

    #@cd
    :cond_cd
    move-object v0, p1

    #@ce
    .line 207
    invoke-static/range {v0 .. v5}, Lcom/android/internal/telephony/gsm/GsmSmsCbMessage;->unpackBody([BIIIZLjava/lang/String;)Landroid/util/Pair;

    #@d1
    move-result-object v9

    #@d2
    .line 209
    .local v9, p:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v5, v9, Landroid/util/Pair;->first:Ljava/lang/Object;

    #@d4
    .end local v5           #language:Ljava/lang/String;
    check-cast v5, Ljava/lang/String;

    #@d6
    .line 210
    .restart local v5       #language:Ljava/lang/String;
    iget-object v0, v9, Landroid/util/Pair;->second:Ljava/lang/Object;

    #@d8
    check-cast v0, Ljava/lang/String;

    #@da
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dd
    .line 196
    add-int/lit8 v7, v7, 0x1

    #@df
    goto :goto_9e

    #@e0
    .line 212
    .end local v2           #offset:I
    .end local v3           #length:I
    .end local v9           #p:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_e0
    new-instance v0, Landroid/util/Pair;

    #@e2
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e5
    move-result-object v11

    #@e6
    invoke-direct {v0, v5, v11}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    #@e9
    .line 218
    .end local v7           #i:I
    .end local v8           #nrPages:I
    .end local v10           #sb:Ljava/lang/StringBuilder;
    :goto_e9
    return-object v0

    #@ea
    .line 215
    :cond_ea
    const/4 v2, 0x6

    #@eb
    .line 216
    .restart local v2       #offset:I
    array-length v0, p1

    #@ec
    sub-int v3, v0, v2

    #@ee
    .restart local v3       #length:I
    move-object v0, p1

    #@ef
    .line 218
    invoke-static/range {v0 .. v5}, Lcom/android/internal/telephony/gsm/GsmSmsCbMessage;->unpackBody([BIIIZLjava/lang/String;)Landroid/util/Pair;

    #@f2
    move-result-object v0

    #@f3
    goto :goto_e9

    #@f4
    .line 118
    :pswitch_data_f4
    .packed-switch 0x0
        :pswitch_4b
        :pswitch_53
        :pswitch_5c
        :pswitch_64
        :pswitch_66
        :pswitch_66
        :pswitch_73
        :pswitch_73
        :pswitch_10
        :pswitch_73
        :pswitch_10
        :pswitch_10
        :pswitch_10
        :pswitch_10
        :pswitch_73
        :pswitch_8c
    .end packed-switch

    #@118
    .line 144
    :pswitch_data_118
    .packed-switch 0x1
        :pswitch_6f
        :pswitch_71
    .end packed-switch
.end method

.method private static unpackBody([BIIIZLjava/lang/String;)Landroid/util/Pair;
    .registers 13
    .parameter "pdu"
    .parameter "encoding"
    .parameter "offset"
    .parameter "length"
    .parameter "hasLanguageIndicator"
    .parameter "language"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([BIIIZ",
            "Ljava/lang/String;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v5, 0x2

    #@2
    .line 238
    const/4 v0, 0x0

    #@3
    .line 240
    .local v0, body:Ljava/lang/String;
    packed-switch p1, :pswitch_data_6a

    #@6
    .line 273
    :cond_6
    :goto_6
    :pswitch_6
    if-eqz v0, :cond_67

    #@8
    .line 275
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@b
    move-result v3

    #@c
    add-int/lit8 v2, v3, -0x1

    #@e
    .local v2, i:I
    :goto_e
    if-ltz v2, :cond_1e

    #@10
    .line 276
    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    #@13
    move-result v3

    #@14
    const/16 v4, 0xd

    #@16
    if-eq v3, v4, :cond_64

    #@18
    .line 277
    add-int/lit8 v3, v2, 0x1

    #@1a
    invoke-virtual {v0, v6, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1d
    move-result-object v0

    #@1e
    .line 285
    .end local v2           #i:I
    :cond_1e
    :goto_1e
    new-instance v3, Landroid/util/Pair;

    #@20
    invoke-direct {v3, p5, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    #@23
    return-object v3

    #@24
    .line 242
    :pswitch_24
    mul-int/lit8 v3, p3, 0x8

    #@26
    div-int/lit8 v3, v3, 0x7

    #@28
    invoke-static {p0, p2, v3}, Lcom/android/internal/telephony/GsmAlphabet;->gsm7BitPackedToString([BII)Ljava/lang/String;

    #@2b
    move-result-object v0

    #@2c
    .line 244
    if-eqz p4, :cond_6

    #@2e
    if-eqz v0, :cond_6

    #@30
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@33
    move-result v3

    #@34
    if-le v3, v5, :cond_6

    #@36
    .line 247
    invoke-virtual {v0, v6, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@39
    move-result-object p5

    #@3a
    .line 248
    const/4 v3, 0x3

    #@3b
    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@3e
    move-result-object v0

    #@3f
    goto :goto_6

    #@40
    .line 253
    :pswitch_40
    if-eqz p4, :cond_4f

    #@42
    array-length v3, p0

    #@43
    add-int/lit8 v4, p2, 0x2

    #@45
    if-lt v3, v4, :cond_4f

    #@47
    .line 256
    invoke-static {p0, p2, v5}, Lcom/android/internal/telephony/GsmAlphabet;->gsm7BitPackedToString([BII)Ljava/lang/String;

    #@4a
    move-result-object p5

    #@4b
    .line 257
    add-int/lit8 p2, p2, 0x2

    #@4d
    .line 258
    add-int/lit8 p3, p3, -0x2

    #@4f
    .line 262
    :cond_4f
    :try_start_4f
    new-instance v0, Ljava/lang/String;

    #@51
    .end local v0           #body:Ljava/lang/String;
    const v3, 0xfffe

    #@54
    and-int/2addr v3, p3

    #@55
    const-string v4, "utf-16"

    #@57
    invoke-direct {v0, p0, p2, v3, v4}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V
    :try_end_5a
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_4f .. :try_end_5a} :catch_5b

    #@5a
    .restart local v0       #body:Ljava/lang/String;
    goto :goto_6

    #@5b
    .line 263
    .end local v0           #body:Ljava/lang/String;
    :catch_5b
    move-exception v1

    #@5c
    .line 265
    .local v1, e:Ljava/io/UnsupportedEncodingException;
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@5e
    const-string v4, "Error decoding UTF-16 message"

    #@60
    invoke-direct {v3, v4, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@63
    throw v3

    #@64
    .line 275
    .end local v1           #e:Ljava/io/UnsupportedEncodingException;
    .restart local v0       #body:Ljava/lang/String;
    .restart local v2       #i:I
    :cond_64
    add-int/lit8 v2, v2, -0x1

    #@66
    goto :goto_e

    #@67
    .line 282
    .end local v2           #i:I
    :cond_67
    const-string v0, ""

    #@69
    goto :goto_1e

    #@6a
    .line 240
    :pswitch_data_6a
    .packed-switch 0x1
        :pswitch_24
        :pswitch_6
        :pswitch_40
    .end packed-switch
.end method
