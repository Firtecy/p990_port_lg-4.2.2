.class Lcom/android/internal/telephony/IIMSPhone$Stub$Proxy;
.super Ljava/lang/Object;
.source "IIMSPhone.java"

# interfaces
.implements Lcom/android/internal/telephony/IIMSPhone;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/IIMSPhone$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 91
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 92
    iput-object p1, p0, Lcom/android/internal/telephony/IIMSPhone$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 93
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 96
    iget-object v0, p0, Lcom/android/internal/telephony/IIMSPhone$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 100
    const-string v0, "com.android.internal.telephony.IIMSPhone"

    #@2
    return-object v0
.end method

.method public getSysInfo(IILjava/lang/String;)[Ljava/lang/String;
    .registers 10
    .parameter "item"
    .parameter "param"
    .parameter "paramEx"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 113
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 114
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 117
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.android.internal.telephony.IIMSPhone"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 118
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 119
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 120
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@16
    .line 121
    iget-object v3, p0, Lcom/android/internal/telephony/IIMSPhone$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/4 v4, 0x1

    #@19
    const/4 v5, 0x0

    #@1a
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1d
    .line 122
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@20
    .line 123
    invoke-virtual {v1}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;
    :try_end_23
    .catchall {:try_start_8 .. :try_end_23} :catchall_2b

    #@23
    move-result-object v2

    #@24
    .line 126
    .local v2, _result:[Ljava/lang/String;
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 127
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 129
    return-object v2

    #@2b
    .line 126
    .end local v2           #_result:[Ljava/lang/String;
    :catchall_2b
    move-exception v3

    #@2c
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2f
    .line 127
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@32
    .line 126
    throw v3
.end method

.method public setListener(Lcom/android/internal/telephony/ISysMonitor;)V
    .registers 7
    .parameter "listener"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 151
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 152
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 154
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.android.internal.telephony.IIMSPhone"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 155
    if-eqz p1, :cond_27

    #@f
    invoke-interface {p1}, Lcom/android/internal/telephony/ISysMonitor;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v2

    #@13
    :goto_13
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 156
    iget-object v2, p0, Lcom/android/internal/telephony/IIMSPhone$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/4 v3, 0x3

    #@19
    const/4 v4, 0x0

    #@1a
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1d
    .line 157
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_20
    .catchall {:try_start_8 .. :try_end_20} :catchall_29

    #@20
    .line 160
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 161
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 163
    return-void

    #@27
    .line 155
    :cond_27
    const/4 v2, 0x0

    #@28
    goto :goto_13

    #@29
    .line 160
    :catchall_29
    move-exception v2

    #@2a
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 161
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@30
    .line 160
    throw v2
.end method

.method public setSysInfo(IIILjava/lang/String;)V
    .registers 10
    .parameter "item"
    .parameter "param1"
    .parameter "param2"
    .parameter "paramEx"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 133
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 134
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 136
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.android.internal.telephony.IIMSPhone"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 137
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 138
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 139
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 140
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@19
    .line 141
    iget-object v2, p0, Lcom/android/internal/telephony/IIMSPhone$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1b
    const/4 v3, 0x2

    #@1c
    const/4 v4, 0x0

    #@1d
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@20
    .line 142
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_23
    .catchall {:try_start_8 .. :try_end_23} :catchall_2a

    #@23
    .line 145
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 146
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 148
    return-void

    #@2a
    .line 145
    :catchall_2a
    move-exception v2

    #@2b
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 146
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@31
    .line 145
    throw v2
.end method
