.class Lcom/android/internal/telephony/DataConnection$DcActivatingState;
.super Lcom/android/internal/util/State;
.source "DataConnection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/DataConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DcActivatingState"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/DataConnection;


# direct methods
.method private constructor <init>(Lcom/android/internal/telephony/DataConnection;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1383
    iput-object p1, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@2
    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/internal/telephony/DataConnection;Lcom/android/internal/telephony/DataConnection$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1383
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/DataConnection$DcActivatingState;-><init>(Lcom/android/internal/telephony/DataConnection;)V

    #@3
    return-void
.end method


# virtual methods
.method public processMessage(Landroid/os/Message;)Z
    .registers 15
    .parameter "msg"

    #@0
    .prologue
    .line 1390
    iget v8, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v8, :pswitch_data_43e

    #@5
    .line 1585
    :pswitch_5
    const/4 v6, 0x0

    #@6
    .line 1588
    .local v6, retVal:Z
    :goto_6
    return v6

    #@7
    .line 1392
    .end local v6           #retVal:Z
    :pswitch_7
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@9
    new-instance v9, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v10, "DcActivatingState deferring msg.what=EVENT_CONNECT refCount = "

    #@10
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v9

    #@14
    iget-object v10, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@16
    iget v10, v10, Lcom/android/internal/telephony/DataConnection;->mRefCount:I

    #@18
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v9

    #@1c
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v9

    #@20
    invoke-virtual {v8, v9}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@23
    .line 1394
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@25
    invoke-static {v8, p1}, Lcom/android/internal/telephony/DataConnection;->access$1700(Lcom/android/internal/telephony/DataConnection;Landroid/os/Message;)V

    #@28
    .line 1395
    const/4 v6, 0x1

    #@29
    .line 1396
    .restart local v6       #retVal:Z
    goto :goto_6

    #@2a
    .line 1399
    .end local v6           #retVal:Z
    :pswitch_2a
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@2c
    const-string v9, "DcActivatingState msg.what=EVENT_SETUP_DATA_CONNECTION_DONE"

    #@2e
    invoke-virtual {v8, v9}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@31
    .line 1401
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@33
    check-cast v1, Landroid/os/AsyncResult;

    #@35
    .line 1402
    .local v1, ar:Landroid/os/AsyncResult;
    iget-object v3, v1, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@37
    check-cast v3, Lcom/android/internal/telephony/DataConnection$ConnectionParams;

    #@39
    .line 1404
    .local v3, cp:Lcom/android/internal/telephony/DataConnection$ConnectionParams;
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@3b
    invoke-static {v8, v1}, Lcom/android/internal/telephony/DataConnection;->access$1800(Lcom/android/internal/telephony/DataConnection;Landroid/os/AsyncResult;)Lcom/android/internal/telephony/DataCallState$SetupResult;

    #@3e
    move-result-object v5

    #@3f
    .line 1405
    .local v5, result:Lcom/android/internal/telephony/DataCallState$SetupResult;
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@41
    new-instance v9, Ljava/lang/StringBuilder;

    #@43
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@46
    const-string v10, "DcActivatingState onSetupConnectionCompleted result="

    #@48
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v9

    #@4c
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v9

    #@50
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@53
    move-result-object v9

    #@54
    invoke-virtual {v8, v9}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@57
    .line 1406
    sget-object v8, Lcom/android/internal/telephony/DataConnection$1;->$SwitchMap$com$android$internal$telephony$DataCallState$SetupResult:[I

    #@59
    invoke-virtual {v5}, Lcom/android/internal/telephony/DataCallState$SetupResult;->ordinal()I

    #@5c
    move-result v9

    #@5d
    aget v8, v8, v9

    #@5f
    packed-switch v8, :pswitch_data_454

    #@62
    .line 1523
    new-instance v8, Ljava/lang/RuntimeException;

    #@64
    const-string v9, "Unknown SetupResult, should not happen"

    #@66
    invoke-direct {v8, v9}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@69
    throw v8

    #@6a
    .line 1409
    :pswitch_6a
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@6c
    iget-object v8, v8, Lcom/android/internal/telephony/DataConnection;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@6e
    iget-object v8, v8, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@70
    invoke-interface {v8}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@73
    move-result-object v8

    #@74
    iget-boolean v8, v8, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_PCSCF_INTERFACE:Z

    #@76
    if-eqz v8, :cond_145

    #@78
    iget-object v8, v3, Lcom/android/internal/telephony/DataConnection$ConnectionParams;->apn:Lcom/android/internal/telephony/DataProfile;

    #@7a
    const-string v9, "ims"

    #@7c
    invoke-virtual {v8, v9}, Lcom/android/internal/telephony/DataProfile;->canHandleType(Ljava/lang/String;)Z

    #@7f
    move-result v8

    #@80
    if-nez v8, :cond_8c

    #@82
    iget-object v8, v3, Lcom/android/internal/telephony/DataConnection$ConnectionParams;->apn:Lcom/android/internal/telephony/DataProfile;

    #@84
    const-string v9, "emergency"

    #@86
    invoke-virtual {v8, v9}, Lcom/android/internal/telephony/DataProfile;->canHandleType(Ljava/lang/String;)Z

    #@89
    move-result v8

    #@8a
    if-eqz v8, :cond_145

    #@8c
    .line 1412
    :cond_8c
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@8e
    iget-object v8, v8, Lcom/android/internal/telephony/DataConnection;->mLinkProperties:Landroid/net/LinkProperties;

    #@90
    invoke-virtual {v8}, Landroid/net/LinkProperties;->isIpv4Connected()Z

    #@93
    move-result v8

    #@94
    if-eqz v8, :cond_d2

    #@96
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@98
    iget-object v8, v8, Lcom/android/internal/telephony/DataConnection;->mLinkProperties:Landroid/net/LinkProperties;

    #@9a
    invoke-virtual {v8}, Landroid/net/LinkProperties;->isIpv6Connected()Z

    #@9d
    move-result v8

    #@9e
    if-eqz v8, :cond_d2

    #@a0
    .line 1414
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@a2
    const-string v9, "DcActivatingState Request IPv4v6 PCSCF addresses"

    #@a4
    invoke-virtual {v8, v9}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@a7
    .line 1415
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@a9
    iget-object v8, v8, Lcom/android/internal/telephony/DataConnection;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@ab
    iget-object v8, v8, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@ad
    iget-object v9, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@af
    iget v9, v9, Lcom/android/internal/telephony/DataConnection;->cid:I

    #@b1
    const-string v10, "IPV4V6"

    #@b3
    iget-object v11, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@b5
    const v12, 0x40007

    #@b8
    invoke-virtual {v11, v12, v3}, Lcom/android/internal/telephony/DataConnection;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@bb
    move-result-object v11

    #@bc
    invoke-interface {v8, v9, v10, v11}, Lcom/android/internal/telephony/CommandsInterface;->getPcscfAddress(ILjava/lang/String;Landroid/os/Message;)V

    #@bf
    .line 1433
    :goto_bf
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@c1
    iget-object v9, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@c3
    const v10, 0x40008

    #@c6
    invoke-virtual {v9, v10, v3}, Lcom/android/internal/telephony/DataConnection;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@c9
    move-result-object v9

    #@ca
    const-wide/16 v10, 0xbb8

    #@cc
    invoke-virtual {v8, v9, v10, v11}, Lcom/android/internal/telephony/DataConnection;->sendMessageDelayed(Landroid/os/Message;J)V

    #@cf
    .line 1525
    :goto_cf
    :pswitch_cf
    const/4 v6, 0x1

    #@d0
    .line 1526
    .restart local v6       #retVal:Z
    goto/16 :goto_6

    #@d2
    .line 1417
    .end local v6           #retVal:Z
    :cond_d2
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@d4
    iget-object v8, v8, Lcom/android/internal/telephony/DataConnection;->mLinkProperties:Landroid/net/LinkProperties;

    #@d6
    invoke-virtual {v8}, Landroid/net/LinkProperties;->isIpv4Connected()Z

    #@d9
    move-result v8

    #@da
    if-eqz v8, :cond_fc

    #@dc
    .line 1419
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@de
    const-string v9, "DcActivatingState Request IPv4 PCSCF addresses"

    #@e0
    invoke-virtual {v8, v9}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@e3
    .line 1420
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@e5
    iget-object v8, v8, Lcom/android/internal/telephony/DataConnection;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@e7
    iget-object v8, v8, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@e9
    iget-object v9, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@eb
    iget v9, v9, Lcom/android/internal/telephony/DataConnection;->cid:I

    #@ed
    const-string v10, "IP"

    #@ef
    iget-object v11, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@f1
    const v12, 0x40007

    #@f4
    invoke-virtual {v11, v12, v3}, Lcom/android/internal/telephony/DataConnection;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@f7
    move-result-object v11

    #@f8
    invoke-interface {v8, v9, v10, v11}, Lcom/android/internal/telephony/CommandsInterface;->getPcscfAddress(ILjava/lang/String;Landroid/os/Message;)V

    #@fb
    goto :goto_bf

    #@fc
    .line 1422
    :cond_fc
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@fe
    iget-object v8, v8, Lcom/android/internal/telephony/DataConnection;->mLinkProperties:Landroid/net/LinkProperties;

    #@100
    invoke-virtual {v8}, Landroid/net/LinkProperties;->isIpv6Connected()Z

    #@103
    move-result v8

    #@104
    if-eqz v8, :cond_126

    #@106
    .line 1424
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@108
    const-string v9, "DcActivatingState Request IPv6 PCSCF addresses"

    #@10a
    invoke-virtual {v8, v9}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@10d
    .line 1425
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@10f
    iget-object v8, v8, Lcom/android/internal/telephony/DataConnection;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@111
    iget-object v8, v8, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@113
    iget-object v9, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@115
    iget v9, v9, Lcom/android/internal/telephony/DataConnection;->cid:I

    #@117
    const-string v10, "IPV6"

    #@119
    iget-object v11, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@11b
    const v12, 0x40007

    #@11e
    invoke-virtual {v11, v12, v3}, Lcom/android/internal/telephony/DataConnection;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@121
    move-result-object v11

    #@122
    invoke-interface {v8, v9, v10, v11}, Lcom/android/internal/telephony/CommandsInterface;->getPcscfAddress(ILjava/lang/String;Landroid/os/Message;)V

    #@125
    goto :goto_bf

    #@126
    .line 1429
    :cond_126
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@128
    const-string v9, "DcActivatingState SHOULD NOT BE CALLED"

    #@12a
    invoke-virtual {v8, v9}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@12d
    .line 1430
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@12f
    invoke-static {v8}, Lcom/android/internal/telephony/DataConnection;->access$1900(Lcom/android/internal/telephony/DataConnection;)Lcom/android/internal/telephony/DataConnection$DcActiveState;

    #@132
    move-result-object v8

    #@133
    sget-object v9, Lcom/android/internal/telephony/DataConnection$FailCause;->NONE:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@135
    invoke-virtual {v8, v3, v9}, Lcom/android/internal/telephony/DataConnection$DcActiveState;->setEnterNotificationParams(Lcom/android/internal/telephony/DataConnection$ConnectionParams;Lcom/android/internal/telephony/DataConnection$FailCause;)V

    #@138
    .line 1431
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@13a
    iget-object v9, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@13c
    invoke-static {v9}, Lcom/android/internal/telephony/DataConnection;->access$1900(Lcom/android/internal/telephony/DataConnection;)Lcom/android/internal/telephony/DataConnection$DcActiveState;

    #@13f
    move-result-object v9

    #@140
    invoke-static {v8, v9}, Lcom/android/internal/telephony/DataConnection;->access$2000(Lcom/android/internal/telephony/DataConnection;Lcom/android/internal/util/IState;)V

    #@143
    goto/16 :goto_bf

    #@145
    .line 1438
    :cond_145
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@147
    invoke-static {v8}, Lcom/android/internal/telephony/DataConnection;->access$1900(Lcom/android/internal/telephony/DataConnection;)Lcom/android/internal/telephony/DataConnection$DcActiveState;

    #@14a
    move-result-object v8

    #@14b
    sget-object v9, Lcom/android/internal/telephony/DataConnection$FailCause;->NONE:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@14d
    invoke-virtual {v8, v3, v9}, Lcom/android/internal/telephony/DataConnection$DcActiveState;->setEnterNotificationParams(Lcom/android/internal/telephony/DataConnection$ConnectionParams;Lcom/android/internal/telephony/DataConnection$FailCause;)V

    #@150
    .line 1439
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@152
    iget-object v9, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@154
    invoke-static {v9}, Lcom/android/internal/telephony/DataConnection;->access$1900(Lcom/android/internal/telephony/DataConnection;)Lcom/android/internal/telephony/DataConnection$DcActiveState;

    #@157
    move-result-object v9

    #@158
    invoke-static {v8, v9}, Lcom/android/internal/telephony/DataConnection;->access$2100(Lcom/android/internal/telephony/DataConnection;Lcom/android/internal/util/IState;)V

    #@15b
    goto/16 :goto_cf

    #@15d
    .line 1447
    :pswitch_15d
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@15f
    invoke-static {v8}, Lcom/android/internal/telephony/DataConnection;->access$400(Lcom/android/internal/telephony/DataConnection;)Lcom/android/internal/telephony/DataConnection$DcInactiveState;

    #@162
    move-result-object v8

    #@163
    iget-object v9, v5, Lcom/android/internal/telephony/DataCallState$SetupResult;->mFailCause:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@165
    const/4 v10, -0x1

    #@166
    invoke-virtual {v8, v3, v9, v10}, Lcom/android/internal/telephony/DataConnection$DcInactiveState;->setEnterNotificationParams(Lcom/android/internal/telephony/DataConnection$ConnectionParams;Lcom/android/internal/telephony/DataConnection$FailCause;I)V

    #@169
    .line 1448
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@16b
    iget-object v9, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@16d
    invoke-static {v9}, Lcom/android/internal/telephony/DataConnection;->access$400(Lcom/android/internal/telephony/DataConnection;)Lcom/android/internal/telephony/DataConnection$DcInactiveState;

    #@170
    move-result-object v9

    #@171
    invoke-static {v8, v9}, Lcom/android/internal/telephony/DataConnection;->access$2200(Lcom/android/internal/telephony/DataConnection;Lcom/android/internal/util/IState;)V

    #@174
    goto/16 :goto_cf

    #@176
    .line 1452
    :pswitch_176
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@178
    invoke-static {v8, v3}, Lcom/android/internal/telephony/DataConnection;->access$2300(Lcom/android/internal/telephony/DataConnection;Ljava/lang/Object;)V

    #@17b
    .line 1453
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@17d
    iget-object v9, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@17f
    invoke-static {v9}, Lcom/android/internal/telephony/DataConnection;->access$2400(Lcom/android/internal/telephony/DataConnection;)Lcom/android/internal/telephony/DataConnection$DcDisconnectionErrorCreatingConnection;

    #@182
    move-result-object v9

    #@183
    invoke-static {v8, v9}, Lcom/android/internal/telephony/DataConnection;->access$2500(Lcom/android/internal/telephony/DataConnection;Lcom/android/internal/util/IState;)V

    #@186
    goto/16 :goto_cf

    #@188
    .line 1457
    :pswitch_188
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@18a
    new-instance v9, Ljava/lang/StringBuilder;

    #@18c
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@18f
    const-string v10, "[RIL_RESET]getRetryCount\t: "

    #@191
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@194
    move-result-object v9

    #@195
    iget-object v10, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@197
    invoke-virtual {v10}, Lcom/android/internal/telephony/DataConnection;->getRetryCount()I

    #@19a
    move-result v10

    #@19b
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19e
    move-result-object v9

    #@19f
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a2
    move-result-object v9

    #@1a3
    invoke-virtual {v8, v9}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@1a6
    .line 1458
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@1a8
    new-instance v9, Ljava/lang/StringBuilder;

    #@1aa
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@1ad
    const-string v10, "[RIL_RESET]getRetryTimer\t: "

    #@1af
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b2
    move-result-object v9

    #@1b3
    iget-object v10, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@1b5
    invoke-virtual {v10}, Lcom/android/internal/telephony/DataConnection;->getRetryTimer()I

    #@1b8
    move-result v10

    #@1b9
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1bc
    move-result-object v9

    #@1bd
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c0
    move-result-object v9

    #@1c1
    invoke-virtual {v8, v9}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@1c4
    .line 1460
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@1c6
    iget-object v8, v8, Lcom/android/internal/telephony/DataConnection;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@1c8
    iget-object v8, v8, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@1ca
    invoke-interface {v8}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@1cd
    move-result-object v8

    #@1ce
    iget-boolean v8, v8, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_RIL_RESTART_ON_RILERROR:Z

    #@1d0
    if-eqz v8, :cond_221

    #@1d2
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@1d4
    invoke-virtual {v8}, Lcom/android/internal/telephony/DataConnection;->getRetryCount()I

    #@1d7
    move-result v8

    #@1d8
    const/4 v9, 0x4

    #@1d9
    if-lt v8, v9, :cond_221

    #@1db
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@1dd
    iget-object v8, v8, Lcom/android/internal/telephony/DataConnection;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@1df
    invoke-virtual {v8}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@1e2
    move-result-object v8

    #@1e3
    invoke-virtual {v8}, Landroid/telephony/ServiceState;->getRilRadioTechnology()I

    #@1e6
    move-result v8

    #@1e7
    const/16 v9, 0xd

    #@1e9
    if-eq v8, v9, :cond_221

    #@1eb
    .line 1462
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@1ed
    iget-object v8, v8, Lcom/android/internal/telephony/DataConnection;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@1ef
    invoke-virtual {v8}, Lcom/android/internal/telephony/PhoneBase;->getForegroundCall()Lcom/android/internal/telephony/Call;

    #@1f2
    move-result-object v8

    #@1f3
    invoke-virtual {v8}, Lcom/android/internal/telephony/Call;->getState()Lcom/android/internal/telephony/Call$State;

    #@1f6
    move-result-object v8

    #@1f7
    invoke-virtual {v8}, Lcom/android/internal/telephony/Call$State;->isAlive()Z

    #@1fa
    move-result v8

    #@1fb
    if-nez v8, :cond_221

    #@1fd
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@1ff
    iget-object v8, v8, Lcom/android/internal/telephony/DataConnection;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@201
    invoke-virtual {v8}, Lcom/android/internal/telephony/PhoneBase;->getBackgroundCall()Lcom/android/internal/telephony/Call;

    #@204
    move-result-object v8

    #@205
    invoke-virtual {v8}, Lcom/android/internal/telephony/Call;->getState()Lcom/android/internal/telephony/Call$State;

    #@208
    move-result-object v8

    #@209
    invoke-virtual {v8}, Lcom/android/internal/telephony/Call$State;->isAlive()Z

    #@20c
    move-result v8

    #@20d
    if-nez v8, :cond_221

    #@20f
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@211
    iget-object v8, v8, Lcom/android/internal/telephony/DataConnection;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@213
    invoke-virtual {v8}, Lcom/android/internal/telephony/PhoneBase;->getRingingCall()Lcom/android/internal/telephony/Call;

    #@216
    move-result-object v8

    #@217
    invoke-virtual {v8}, Lcom/android/internal/telephony/Call;->getState()Lcom/android/internal/telephony/Call$State;

    #@21a
    move-result-object v8

    #@21b
    invoke-virtual {v8}, Lcom/android/internal/telephony/Call$State;->isAlive()Z

    #@21e
    move-result v8

    #@21f
    if-eqz v8, :cond_235

    #@221
    .line 1474
    :cond_221
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@223
    iget-object v8, v8, Lcom/android/internal/telephony/DataConnection;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@225
    iget-object v8, v8, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@227
    iget-object v9, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@229
    const v10, 0x40002

    #@22c
    invoke-virtual {v9, v10, v3}, Lcom/android/internal/telephony/DataConnection;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@22f
    move-result-object v9

    #@230
    invoke-interface {v8, v9}, Lcom/android/internal/telephony/CommandsInterface;->getLastDataCallFailCause(Landroid/os/Message;)V

    #@233
    goto/16 :goto_cf

    #@235
    .line 1467
    :cond_235
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@237
    const-string v9, "[LGE_DATA]reset radio!!"

    #@239
    invoke-virtual {v8, v9}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@23c
    .line 1468
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@23e
    invoke-static {v8}, Lcom/android/internal/telephony/DataConnection;->access$700(Lcom/android/internal/telephony/DataConnection;)V

    #@241
    goto/16 :goto_cf

    #@243
    .line 1479
    :pswitch_243
    iget-object v4, v3, Lcom/android/internal/telephony/DataConnection$ConnectionParams;->onCompletedMsg:Landroid/os/Message;

    #@245
    .line 1480
    .local v4, msgInfo:Landroid/os/Message;
    iget-object v8, v4, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@247
    instance-of v8, v8, Lcom/android/internal/telephony/ApnContext;

    #@249
    if-eqz v8, :cond_335

    #@24b
    .line 1481
    iget-object v0, v4, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@24d
    check-cast v0, Lcom/android/internal/telephony/ApnContext;

    #@24f
    .line 1482
    .local v0, apnContext:Lcom/android/internal/telephony/ApnContext;
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@251
    iget-object v8, v8, Lcom/android/internal/telephony/DataConnection;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@253
    iget-object v8, v8, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@255
    invoke-interface {v8}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@258
    move-result-object v8

    #@259
    iget-boolean v8, v8, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_RIL_RESTART_ON_RILERROR:Z

    #@25b
    if-eqz v8, :cond_335

    #@25d
    invoke-virtual {v0}, Lcom/android/internal/telephony/ApnContext;->getApnType()Ljava/lang/String;

    #@260
    move-result-object v8

    #@261
    const-string v9, "default"

    #@263
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@266
    move-result v8

    #@267
    if-eqz v8, :cond_335

    #@269
    iget-object v8, v5, Lcom/android/internal/telephony/DataCallState$SetupResult;->mFailCause:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@26b
    sget-object v9, Lcom/android/internal/telephony/DataConnection$FailCause;->INTERNAL_REASON:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@26d
    if-ne v8, v9, :cond_335

    #@26f
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@271
    iget-object v8, v8, Lcom/android/internal/telephony/DataConnection;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@273
    invoke-virtual {v8}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@276
    move-result-object v8

    #@277
    invoke-virtual {v8}, Landroid/telephony/ServiceState;->getRilRadioTechnology()I

    #@27a
    move-result v8

    #@27b
    const/16 v9, 0xd

    #@27d
    if-eq v8, v9, :cond_335

    #@27f
    .line 1487
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@281
    new-instance v9, Ljava/lang/StringBuilder;

    #@283
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@286
    const-string v10, "ERR_RilError apn="

    #@288
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28b
    move-result-object v9

    #@28c
    invoke-virtual {v0}, Lcom/android/internal/telephony/ApnContext;->getApnType()Ljava/lang/String;

    #@28f
    move-result-object v10

    #@290
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@293
    move-result-object v9

    #@294
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@297
    move-result-object v9

    #@298
    invoke-virtual {v8, v9}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@29b
    .line 1488
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@29d
    new-instance v9, Ljava/lang/StringBuilder;

    #@29f
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@2a2
    const-string v10, "[RIL_RESET]getRetryCount\t: "

    #@2a4
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a7
    move-result-object v9

    #@2a8
    iget-object v10, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@2aa
    invoke-virtual {v10}, Lcom/android/internal/telephony/DataConnection;->getRetryCount()I

    #@2ad
    move-result v10

    #@2ae
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2b1
    move-result-object v9

    #@2b2
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b5
    move-result-object v9

    #@2b6
    invoke-virtual {v8, v9}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@2b9
    .line 1489
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@2bb
    new-instance v9, Ljava/lang/StringBuilder;

    #@2bd
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@2c0
    const-string v10, "[RIL_RESET]getRetryTimer\t: "

    #@2c2
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c5
    move-result-object v9

    #@2c6
    iget-object v10, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@2c8
    invoke-virtual {v10}, Lcom/android/internal/telephony/DataConnection;->getRetryTimer()I

    #@2cb
    move-result v10

    #@2cc
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2cf
    move-result-object v9

    #@2d0
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d3
    move-result-object v9

    #@2d4
    invoke-virtual {v8, v9}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@2d7
    .line 1491
    sget v8, Lcom/android/internal/telephony/DataConnection;->rilFailCount:I

    #@2d9
    add-int/lit8 v8, v8, 0x1

    #@2db
    sput v8, Lcom/android/internal/telephony/DataConnection;->rilFailCount:I

    #@2dd
    .line 1492
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@2df
    new-instance v9, Ljava/lang/StringBuilder;

    #@2e1
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@2e4
    const-string v10, "[LGE_DATA]reset count = "

    #@2e6
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e9
    move-result-object v9

    #@2ea
    sget v10, Lcom/android/internal/telephony/DataConnection;->rilFailCount:I

    #@2ec
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2ef
    move-result-object v9

    #@2f0
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f3
    move-result-object v9

    #@2f4
    invoke-virtual {v8, v9}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@2f7
    .line 1493
    sget v8, Lcom/android/internal/telephony/DataConnection;->rilFailCount:I

    #@2f9
    const/4 v9, 0x6

    #@2fa
    if-lt v8, v9, :cond_335

    #@2fc
    .line 1494
    const/4 v8, 0x0

    #@2fd
    sput v8, Lcom/android/internal/telephony/DataConnection;->rilFailCount:I

    #@2ff
    .line 1496
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@301
    iget-object v8, v8, Lcom/android/internal/telephony/DataConnection;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@303
    invoke-virtual {v8}, Lcom/android/internal/telephony/PhoneBase;->getForegroundCall()Lcom/android/internal/telephony/Call;

    #@306
    move-result-object v8

    #@307
    invoke-virtual {v8}, Lcom/android/internal/telephony/Call;->getState()Lcom/android/internal/telephony/Call$State;

    #@30a
    move-result-object v8

    #@30b
    invoke-virtual {v8}, Lcom/android/internal/telephony/Call$State;->isAlive()Z

    #@30e
    move-result v8

    #@30f
    if-nez v8, :cond_335

    #@311
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@313
    iget-object v8, v8, Lcom/android/internal/telephony/DataConnection;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@315
    invoke-virtual {v8}, Lcom/android/internal/telephony/PhoneBase;->getBackgroundCall()Lcom/android/internal/telephony/Call;

    #@318
    move-result-object v8

    #@319
    invoke-virtual {v8}, Lcom/android/internal/telephony/Call;->getState()Lcom/android/internal/telephony/Call$State;

    #@31c
    move-result-object v8

    #@31d
    invoke-virtual {v8}, Lcom/android/internal/telephony/Call$State;->isAlive()Z

    #@320
    move-result v8

    #@321
    if-nez v8, :cond_335

    #@323
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@325
    iget-object v8, v8, Lcom/android/internal/telephony/DataConnection;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@327
    invoke-virtual {v8}, Lcom/android/internal/telephony/PhoneBase;->getRingingCall()Lcom/android/internal/telephony/Call;

    #@32a
    move-result-object v8

    #@32b
    invoke-virtual {v8}, Lcom/android/internal/telephony/Call;->getState()Lcom/android/internal/telephony/Call$State;

    #@32e
    move-result-object v8

    #@32f
    invoke-virtual {v8}, Lcom/android/internal/telephony/Call$State;->isAlive()Z

    #@332
    move-result v8

    #@333
    if-eqz v8, :cond_36b

    #@335
    .line 1508
    .end local v0           #apnContext:Lcom/android/internal/telephony/ApnContext;
    :cond_335
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@337
    iget-object v8, v8, Lcom/android/internal/telephony/DataConnection;->phone:Lcom/android/internal/telephony/PhoneBase;

    #@339
    iget-object v8, v8, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@33b
    invoke-interface {v8}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@33e
    move-result-object v8

    #@33f
    iget-boolean v8, v8, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_RIL_RESTART_ON_RILERROR:Z

    #@341
    if-nez v8, :cond_34d

    #@343
    iget-object v8, v5, Lcom/android/internal/telephony/DataCallState$SetupResult;->mFailCause:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@345
    sget-object v9, Lcom/android/internal/telephony/DataConnection$FailCause;->INTERNAL_REASON:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@347
    if-ne v8, v9, :cond_34d

    #@349
    .line 1509
    sget-object v8, Lcom/android/internal/telephony/DataConnection$FailCause;->ERROR_UNSPECIFIED:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@34b
    iput-object v8, v5, Lcom/android/internal/telephony/DataCallState$SetupResult;->mFailCause:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@34d
    .line 1515
    :cond_34d
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@34f
    invoke-static {v8}, Lcom/android/internal/telephony/DataConnection;->access$400(Lcom/android/internal/telephony/DataConnection;)Lcom/android/internal/telephony/DataConnection$DcInactiveState;

    #@352
    move-result-object v8

    #@353
    iget-object v9, v5, Lcom/android/internal/telephony/DataCallState$SetupResult;->mFailCause:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@355
    iget-object v10, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@357
    invoke-static {v10, v1}, Lcom/android/internal/telephony/DataConnection;->access$2600(Lcom/android/internal/telephony/DataConnection;Landroid/os/AsyncResult;)I

    #@35a
    move-result v10

    #@35b
    invoke-virtual {v8, v3, v9, v10}, Lcom/android/internal/telephony/DataConnection$DcInactiveState;->setEnterNotificationParams(Lcom/android/internal/telephony/DataConnection$ConnectionParams;Lcom/android/internal/telephony/DataConnection$FailCause;I)V

    #@35e
    .line 1517
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@360
    iget-object v9, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@362
    invoke-static {v9}, Lcom/android/internal/telephony/DataConnection;->access$400(Lcom/android/internal/telephony/DataConnection;)Lcom/android/internal/telephony/DataConnection$DcInactiveState;

    #@365
    move-result-object v9

    #@366
    invoke-static {v8, v9}, Lcom/android/internal/telephony/DataConnection;->access$2700(Lcom/android/internal/telephony/DataConnection;Lcom/android/internal/util/IState;)V

    #@369
    goto/16 :goto_cf

    #@36b
    .line 1501
    .restart local v0       #apnContext:Lcom/android/internal/telephony/ApnContext;
    :cond_36b
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@36d
    const-string v9, "[LGE_DATA]reset radio!!"

    #@36f
    invoke-virtual {v8, v9}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@372
    .line 1502
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@374
    invoke-static {v8}, Lcom/android/internal/telephony/DataConnection;->access$700(Lcom/android/internal/telephony/DataConnection;)V

    #@377
    goto/16 :goto_cf

    #@379
    .line 1529
    .end local v0           #apnContext:Lcom/android/internal/telephony/ApnContext;
    .end local v1           #ar:Landroid/os/AsyncResult;
    .end local v3           #cp:Lcom/android/internal/telephony/DataConnection$ConnectionParams;
    .end local v4           #msgInfo:Landroid/os/Message;
    .end local v5           #result:Lcom/android/internal/telephony/DataCallState$SetupResult;
    :pswitch_379
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@37b
    check-cast v1, Landroid/os/AsyncResult;

    #@37d
    .line 1530
    .restart local v1       #ar:Landroid/os/AsyncResult;
    iget-object v3, v1, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@37f
    check-cast v3, Lcom/android/internal/telephony/DataConnection$ConnectionParams;

    #@381
    .line 1531
    .restart local v3       #cp:Lcom/android/internal/telephony/DataConnection$ConnectionParams;
    sget-object v2, Lcom/android/internal/telephony/DataConnection$FailCause;->UNKNOWN:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@383
    .line 1533
    .local v2, cause:Lcom/android/internal/telephony/DataConnection$FailCause;
    iget v8, v3, Lcom/android/internal/telephony/DataConnection$ConnectionParams;->tag:I

    #@385
    iget-object v9, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@387
    iget v9, v9, Lcom/android/internal/telephony/DataConnection;->mTag:I

    #@389
    if-ne v8, v9, :cond_3bb

    #@38b
    .line 1534
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@38d
    const-string v9, "DcActivatingState msg.what=EVENT_GET_LAST_FAIL_DONE"

    #@38f
    invoke-virtual {v8, v9}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@392
    .line 1535
    iget-object v8, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@394
    if-nez v8, :cond_3a3

    #@396
    .line 1536
    iget-object v8, v1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@398
    check-cast v8, [I

    #@39a
    check-cast v8, [I

    #@39c
    const/4 v9, 0x0

    #@39d
    aget v7, v8, v9

    #@39f
    .line 1537
    .local v7, rilFailCause:I
    invoke-static {v7}, Lcom/android/internal/telephony/DataConnection$FailCause;->fromInt(I)Lcom/android/internal/telephony/DataConnection$FailCause;

    #@3a2
    move-result-object v2

    #@3a3
    .line 1541
    .end local v7           #rilFailCause:I
    :cond_3a3
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@3a5
    invoke-static {v8}, Lcom/android/internal/telephony/DataConnection;->access$400(Lcom/android/internal/telephony/DataConnection;)Lcom/android/internal/telephony/DataConnection$DcInactiveState;

    #@3a8
    move-result-object v8

    #@3a9
    const/4 v9, -0x1

    #@3aa
    invoke-virtual {v8, v3, v2, v9}, Lcom/android/internal/telephony/DataConnection$DcInactiveState;->setEnterNotificationParams(Lcom/android/internal/telephony/DataConnection$ConnectionParams;Lcom/android/internal/telephony/DataConnection$FailCause;I)V

    #@3ad
    .line 1542
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@3af
    iget-object v9, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@3b1
    invoke-static {v9}, Lcom/android/internal/telephony/DataConnection;->access$400(Lcom/android/internal/telephony/DataConnection;)Lcom/android/internal/telephony/DataConnection$DcInactiveState;

    #@3b4
    move-result-object v9

    #@3b5
    invoke-static {v8, v9}, Lcom/android/internal/telephony/DataConnection;->access$2800(Lcom/android/internal/telephony/DataConnection;Lcom/android/internal/util/IState;)V

    #@3b8
    .line 1550
    :goto_3b8
    const/4 v6, 0x1

    #@3b9
    .line 1551
    .restart local v6       #retVal:Z
    goto/16 :goto_6

    #@3bb
    .line 1545
    .end local v6           #retVal:Z
    :cond_3bb
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@3bd
    new-instance v9, Ljava/lang/StringBuilder;

    #@3bf
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@3c2
    const-string v10, "DcActivatingState EVENT_GET_LAST_FAIL_DONE is stale cp.tag="

    #@3c4
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c7
    move-result-object v9

    #@3c8
    iget v10, v3, Lcom/android/internal/telephony/DataConnection$ConnectionParams;->tag:I

    #@3ca
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3cd
    move-result-object v9

    #@3ce
    const-string v10, ", mTag="

    #@3d0
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d3
    move-result-object v9

    #@3d4
    iget-object v10, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@3d6
    iget v10, v10, Lcom/android/internal/telephony/DataConnection;->mTag:I

    #@3d8
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3db
    move-result-object v9

    #@3dc
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3df
    move-result-object v9

    #@3e0
    invoke-virtual {v8, v9}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@3e3
    goto :goto_3b8

    #@3e4
    .line 1555
    .end local v1           #ar:Landroid/os/AsyncResult;
    .end local v2           #cause:Lcom/android/internal/telephony/DataConnection$FailCause;
    .end local v3           #cp:Lcom/android/internal/telephony/DataConnection$ConnectionParams;
    :pswitch_3e4
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@3e6
    const-string v9, "DcActivatingState msg.what=EVENT_GET_PCSCF_ADDRESS_DONE"

    #@3e8
    invoke-virtual {v8, v9}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@3eb
    .line 1557
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@3ed
    check-cast v1, Landroid/os/AsyncResult;

    #@3ef
    .line 1558
    .restart local v1       #ar:Landroid/os/AsyncResult;
    iget-object v3, v1, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@3f1
    check-cast v3, Lcom/android/internal/telephony/DataConnection$ConnectionParams;

    #@3f3
    .line 1560
    .restart local v3       #cp:Lcom/android/internal/telephony/DataConnection$ConnectionParams;
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@3f5
    const v9, 0x40008

    #@3f8
    invoke-static {v8, v9}, Lcom/android/internal/telephony/DataConnection;->access$2900(Lcom/android/internal/telephony/DataConnection;I)V

    #@3fb
    .line 1561
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@3fd
    invoke-static {v8, v1}, Lcom/android/internal/telephony/DataConnection;->access$3000(Lcom/android/internal/telephony/DataConnection;Landroid/os/AsyncResult;)V

    #@400
    .line 1564
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@402
    invoke-static {v8}, Lcom/android/internal/telephony/DataConnection;->access$1900(Lcom/android/internal/telephony/DataConnection;)Lcom/android/internal/telephony/DataConnection$DcActiveState;

    #@405
    move-result-object v8

    #@406
    sget-object v9, Lcom/android/internal/telephony/DataConnection$FailCause;->NONE:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@408
    invoke-virtual {v8, v3, v9}, Lcom/android/internal/telephony/DataConnection$DcActiveState;->setEnterNotificationParams(Lcom/android/internal/telephony/DataConnection$ConnectionParams;Lcom/android/internal/telephony/DataConnection$FailCause;)V

    #@40b
    .line 1565
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@40d
    iget-object v9, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@40f
    invoke-static {v9}, Lcom/android/internal/telephony/DataConnection;->access$1900(Lcom/android/internal/telephony/DataConnection;)Lcom/android/internal/telephony/DataConnection$DcActiveState;

    #@412
    move-result-object v9

    #@413
    invoke-static {v8, v9}, Lcom/android/internal/telephony/DataConnection;->access$3100(Lcom/android/internal/telephony/DataConnection;Lcom/android/internal/util/IState;)V

    #@416
    .line 1566
    const/4 v6, 0x1

    #@417
    .line 1567
    .restart local v6       #retVal:Z
    goto/16 :goto_6

    #@419
    .line 1570
    .end local v1           #ar:Landroid/os/AsyncResult;
    .end local v3           #cp:Lcom/android/internal/telephony/DataConnection$ConnectionParams;
    .end local v6           #retVal:Z
    :pswitch_419
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@41b
    const-string v9, "DcActivatingState msg.what=EVENT_GET_PCSCF_ADDRESS_FAIL"

    #@41d
    invoke-virtual {v8, v9}, Lcom/android/internal/telephony/DataConnection;->log(Ljava/lang/String;)V

    #@420
    .line 1572
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@422
    check-cast v3, Lcom/android/internal/telephony/DataConnection$ConnectionParams;

    #@424
    .line 1574
    .restart local v3       #cp:Lcom/android/internal/telephony/DataConnection$ConnectionParams;
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@426
    invoke-static {v8}, Lcom/android/internal/telephony/DataConnection;->access$1900(Lcom/android/internal/telephony/DataConnection;)Lcom/android/internal/telephony/DataConnection$DcActiveState;

    #@429
    move-result-object v8

    #@42a
    sget-object v9, Lcom/android/internal/telephony/DataConnection$FailCause;->NONE:Lcom/android/internal/telephony/DataConnection$FailCause;

    #@42c
    invoke-virtual {v8, v3, v9}, Lcom/android/internal/telephony/DataConnection$DcActiveState;->setEnterNotificationParams(Lcom/android/internal/telephony/DataConnection$ConnectionParams;Lcom/android/internal/telephony/DataConnection$FailCause;)V

    #@42f
    .line 1575
    iget-object v8, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@431
    iget-object v9, p0, Lcom/android/internal/telephony/DataConnection$DcActivatingState;->this$0:Lcom/android/internal/telephony/DataConnection;

    #@433
    invoke-static {v9}, Lcom/android/internal/telephony/DataConnection;->access$1900(Lcom/android/internal/telephony/DataConnection;)Lcom/android/internal/telephony/DataConnection$DcActiveState;

    #@436
    move-result-object v9

    #@437
    invoke-static {v8, v9}, Lcom/android/internal/telephony/DataConnection;->access$3200(Lcom/android/internal/telephony/DataConnection;Lcom/android/internal/util/IState;)V

    #@43a
    .line 1576
    const/4 v6, 0x1

    #@43b
    .line 1577
    .restart local v6       #retVal:Z
    goto/16 :goto_6

    #@43d
    .line 1390
    nop

    #@43e
    :pswitch_data_43e
    .packed-switch 0x40000
        :pswitch_7
        :pswitch_2a
        :pswitch_379
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_3e4
        :pswitch_419
    .end packed-switch

    #@454
    .line 1406
    :pswitch_data_454
    .packed-switch 0x1
        :pswitch_6a
        :pswitch_15d
        :pswitch_176
        :pswitch_188
        :pswitch_243
        :pswitch_cf
    .end packed-switch
.end method
