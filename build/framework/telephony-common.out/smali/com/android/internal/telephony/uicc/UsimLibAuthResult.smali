.class public Lcom/android/internal/telephony/uicc/UsimLibAuthResult;
.super Ljava/lang/Object;
.source "UsimLibAuthResult.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/android/internal/telephony/uicc/UsimLibAuthResult;",
            ">;"
        }
    .end annotation
.end field

.field public static final INT_NOT_SET:I = -0x1

.field public static final STRING_NOT_SET:Ljava/lang/String; = ""


# instance fields
.field public authData:Ljava/lang/String;

.field public ret:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 22
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimLibAuthResult$1;

    #@2
    invoke-direct {v0}, Lcom/android/internal/telephony/uicc/UsimLibAuthResult$1;-><init>()V

    #@5
    sput-object v0, Lcom/android/internal/telephony/uicc/UsimLibAuthResult;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 17
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 18
    const/4 v0, -0x1

    #@4
    iput v0, p0, Lcom/android/internal/telephony/uicc/UsimLibAuthResult;->ret:I

    #@6
    .line 19
    const-string v0, ""

    #@8
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/UsimLibAuthResult;->authData:Ljava/lang/String;

    #@a
    .line 20
    return-void
.end method

.method public static createFromParcel(Landroid/os/Parcel;)Lcom/android/internal/telephony/uicc/UsimLibAuthResult;
    .registers 3
    .parameter "source"

    #@0
    .prologue
    .line 34
    new-instance v0, Lcom/android/internal/telephony/uicc/UsimLibAuthResult;

    #@2
    invoke-direct {v0}, Lcom/android/internal/telephony/uicc/UsimLibAuthResult;-><init>()V

    #@5
    .line 35
    .local v0, p:Lcom/android/internal/telephony/uicc/UsimLibAuthResult;
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@8
    move-result v1

    #@9
    iput v1, v0, Lcom/android/internal/telephony/uicc/UsimLibAuthResult;->ret:I

    #@b
    .line 36
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@e
    move-result-object v1

    #@f
    iput-object v1, v0, Lcom/android/internal/telephony/uicc/UsimLibAuthResult;->authData:Ljava/lang/String;

    #@11
    .line 37
    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 50
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 54
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@8
    move-result-object v1

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v0

    #@d
    const-string v1, "return: "

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    iget v1, p0, Lcom/android/internal/telephony/uicc/UsimLibAuthResult;->ret:I

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    const-string v1, "authData: "

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v0

    #@1f
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UsimLibAuthResult;->authData:Ljava/lang/String;

    #@21
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v0

    #@25
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v0

    #@29
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;)V
    .registers 3
    .parameter "dest"

    #@0
    .prologue
    .line 41
    iget v0, p0, Lcom/android/internal/telephony/uicc/UsimLibAuthResult;->ret:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 42
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UsimLibAuthResult;->authData:Ljava/lang/String;

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@a
    .line 43
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 3
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 46
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/uicc/UsimLibAuthResult;->writeToParcel(Landroid/os/Parcel;)V

    #@3
    .line 47
    return-void
.end method
