.class public Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;
.super Ljava/lang/Object;
.source "LGEIccUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/uicc/LGEIccUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SMSPType"
.end annotation


# instance fields
.field public CodeScheme:B

.field public DestAddr:[B

.field public ParamIndicator:B

.field public ProtoclID:B

.field public SVCCenterAddr:[B

.field public ValidPeriod:B

.field public alphaID:[B

.field final synthetic this$0:Lcom/android/internal/telephony/uicc/LGEIccUtils;


# direct methods
.method public constructor <init>(Lcom/android/internal/telephony/uicc/LGEIccUtils;)V
    .registers 7
    .parameter

    #@0
    .prologue
    const/16 v4, 0xb

    #@2
    const/4 v3, 0x0

    #@3
    const/4 v2, -0x1

    #@4
    .line 360
    iput-object p1, p0, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;->this$0:Lcom/android/internal/telephony/uicc/LGEIccUtils;

    #@6
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@9
    .line 361
    const/16 v1, 0xc

    #@b
    new-array v0, v1, [B

    #@d
    .line 362
    .local v0, InitData:[B
    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([BB)V

    #@10
    .line 364
    iput-byte v2, p0, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;->ParamIndicator:B

    #@12
    .line 365
    invoke-static {v0, v3, v4}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->subarray([BII)[B

    #@15
    move-result-object v1

    #@16
    iput-object v1, p0, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;->DestAddr:[B

    #@18
    .line 366
    invoke-static {v0, v3, v4}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->subarray([BII)[B

    #@1b
    move-result-object v1

    #@1c
    iput-object v1, p0, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;->SVCCenterAddr:[B

    #@1e
    .line 367
    iput-byte v2, p0, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;->ProtoclID:B

    #@20
    .line 368
    iput-byte v2, p0, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;->CodeScheme:B

    #@22
    .line 369
    iput-byte v2, p0, Lcom/android/internal/telephony/uicc/LGEIccUtils$SMSPType;->ValidPeriod:B

    #@24
    .line 370
    return-void
.end method
