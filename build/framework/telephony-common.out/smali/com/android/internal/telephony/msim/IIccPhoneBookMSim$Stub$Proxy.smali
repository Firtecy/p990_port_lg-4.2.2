.class Lcom/android/internal/telephony/msim/IIccPhoneBookMSim$Stub$Proxy;
.super Ljava/lang/Object;
.source "IIccPhoneBookMSim.java"

# interfaces
.implements Lcom/android/internal/telephony/msim/IIccPhoneBookMSim;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/msim/IIccPhoneBookMSim$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 132
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 133
    iput-object p1, p0, Lcom/android/internal/telephony/msim/IIccPhoneBookMSim$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 134
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 137
    iget-object v0, p0, Lcom/android/internal/telephony/msim/IIccPhoneBookMSim$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public getAdnRecordsInEf(II)Ljava/util/List;
    .registers 9
    .parameter "efid"
    .parameter "subscription"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/uicc/AdnRecord;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 153
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 154
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 157
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.android.internal.telephony.msim.IIccPhoneBookMSim"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 158
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 159
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 160
    iget-object v3, p0, Lcom/android/internal/telephony/msim/IIccPhoneBookMSim$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/4 v4, 0x1

    #@16
    const/4 v5, 0x0

    #@17
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1a
    .line 161
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1d
    .line 162
    sget-object v3, Lcom/android/internal/telephony/uicc/AdnRecord;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1f
    invoke-virtual {v1, v3}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;
    :try_end_22
    .catchall {:try_start_8 .. :try_end_22} :catchall_2a

    #@22
    move-result-object v2

    #@23
    .line 165
    .local v2, _result:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/telephony/uicc/AdnRecord;>;"
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 166
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 168
    return-object v2

    #@2a
    .line 165
    .end local v2           #_result:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/telephony/uicc/AdnRecord;>;"
    :catchall_2a
    move-exception v3

    #@2b
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 166
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@31
    .line 165
    throw v3
.end method

.method public getAdnRecordsSize(II)[I
    .registers 9
    .parameter "efid"
    .parameter "subscription"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 264
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 265
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 268
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.android.internal.telephony.msim.IIccPhoneBookMSim"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 269
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 270
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 271
    iget-object v3, p0, Lcom/android/internal/telephony/msim/IIccPhoneBookMSim$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/4 v4, 0x4

    #@16
    const/4 v5, 0x0

    #@17
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1a
    .line 272
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1d
    .line 273
    invoke-virtual {v1}, Landroid/os/Parcel;->createIntArray()[I
    :try_end_20
    .catchall {:try_start_8 .. :try_end_20} :catchall_28

    #@20
    move-result-object v2

    #@21
    .line 276
    .local v2, _result:[I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 277
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 279
    return-object v2

    #@28
    .line 276
    .end local v2           #_result:[I
    :catchall_28
    move-exception v3

    #@29
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 277
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2f
    .line 276
    throw v3
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 141
    const-string v0, "com.android.internal.telephony.msim.IIccPhoneBookMSim"

    #@2
    return-object v0
.end method

.method public updateAdnRecordsInEfByIndex(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;I)Z
    .registers 13
    .parameter "efid"
    .parameter "newTag"
    .parameter "newPhoneNumber"
    .parameter "index"
    .parameter "pin2"
    .parameter "subscription"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 231
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 232
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 235
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "com.android.internal.telephony.msim.IIccPhoneBookMSim"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 236
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 237
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@14
    .line 238
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@17
    .line 239
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@1a
    .line 240
    invoke-virtual {v0, p5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1d
    .line 241
    invoke-virtual {v0, p6}, Landroid/os/Parcel;->writeInt(I)V

    #@20
    .line 242
    iget-object v3, p0, Lcom/android/internal/telephony/msim/IIccPhoneBookMSim$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@22
    const/4 v4, 0x3

    #@23
    const/4 v5, 0x0

    #@24
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@27
    .line 243
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@2a
    .line 244
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_2d
    .catchall {:try_start_9 .. :try_end_2d} :catchall_38

    #@2d
    move-result v3

    #@2e
    if-eqz v3, :cond_31

    #@30
    const/4 v2, 0x1

    #@31
    .line 247
    .local v2, _result:Z
    :cond_31
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@34
    .line 248
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@37
    .line 250
    return v2

    #@38
    .line 247
    .end local v2           #_result:Z
    :catchall_38
    move-exception v3

    #@39
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3c
    .line 248
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3f
    .line 247
    throw v3
.end method

.method public updateAdnRecordsInEfBySearch(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Z
    .registers 14
    .parameter "efid"
    .parameter "oldTag"
    .parameter "oldPhoneNumber"
    .parameter "newTag"
    .parameter "newPhoneNumber"
    .parameter "pin2"
    .parameter "subscription"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 191
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 192
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 195
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "com.android.internal.telephony.msim.IIccPhoneBookMSim"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 196
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 197
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@14
    .line 198
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@17
    .line 199
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1a
    .line 200
    invoke-virtual {v0, p5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1d
    .line 201
    invoke-virtual {v0, p6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@20
    .line 202
    invoke-virtual {v0, p7}, Landroid/os/Parcel;->writeInt(I)V

    #@23
    .line 203
    iget-object v3, p0, Lcom/android/internal/telephony/msim/IIccPhoneBookMSim$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@25
    const/4 v4, 0x2

    #@26
    const/4 v5, 0x0

    #@27
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@2a
    .line 204
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@2d
    .line 205
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_30
    .catchall {:try_start_9 .. :try_end_30} :catchall_3b

    #@30
    move-result v3

    #@31
    if-eqz v3, :cond_34

    #@33
    const/4 v2, 0x1

    #@34
    .line 208
    .local v2, _result:Z
    :cond_34
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@37
    .line 209
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3a
    .line 211
    return v2

    #@3b
    .line 208
    .end local v2           #_result:Z
    :catchall_3b
    move-exception v3

    #@3c
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3f
    .line 209
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@42
    .line 208
    throw v3
.end method
