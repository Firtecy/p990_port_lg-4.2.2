.class Lcom/android/internal/telephony/cat/ComprehensionTlv;
.super Ljava/lang/Object;
.source "ComprehensionTlv.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "ComprehensionTlv"


# instance fields
.field private mCr:Z

.field private mLength:I

.field private mRawValue:[B

.field private mTag:I

.field private mValueIndex:I


# direct methods
.method protected constructor <init>(IZI[BI)V
    .registers 6
    .parameter "tag"
    .parameter "cr"
    .parameter "length"
    .parameter "data"
    .parameter "valueIndex"

    #@0
    .prologue
    .line 52
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 53
    iput p1, p0, Lcom/android/internal/telephony/cat/ComprehensionTlv;->mTag:I

    #@5
    .line 54
    iput-boolean p2, p0, Lcom/android/internal/telephony/cat/ComprehensionTlv;->mCr:Z

    #@7
    .line 55
    iput p3, p0, Lcom/android/internal/telephony/cat/ComprehensionTlv;->mLength:I

    #@9
    .line 56
    iput p5, p0, Lcom/android/internal/telephony/cat/ComprehensionTlv;->mValueIndex:I

    #@b
    .line 57
    iput-object p4, p0, Lcom/android/internal/telephony/cat/ComprehensionTlv;->mRawValue:[B

    #@d
    .line 58
    return-void
.end method

.method public static decode([BI)Lcom/android/internal/telephony/cat/ComprehensionTlv;
    .registers 14
    .parameter "data"
    .parameter "startIndex"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/cat/ResultException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v11, 0x80

    #@2
    const/4 v2, 0x1

    #@3
    const/4 v0, 0x0

    #@4
    .line 116
    move v5, p1

    #@5
    .line 117
    .local v5, curIndex:I
    array-length v8, p0

    #@6
    .line 123
    .local v8, endIndex:I
    add-int/lit8 v6, v5, 0x1

    #@8
    .end local v5           #curIndex:I
    .local v6, curIndex:I
    :try_start_8
    aget-byte v4, p0, v5
    :try_end_a
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_8 .. :try_end_a} :catch_cc

    #@a
    and-int/lit16 v9, v4, 0xff

    #@c
    .line 124
    .local v9, temp:I
    sparse-switch v9, :sswitch_data_1f0

    #@f
    .line 144
    move v1, v9

    #@10
    .line 145
    .local v1, tag:I
    and-int/lit16 v4, v1, 0x80

    #@12
    if-eqz v4, :cond_81

    #@14
    .line 146
    .local v2, cr:Z
    :goto_14
    and-int/lit16 v1, v1, -0x81

    #@16
    .line 152
    :goto_16
    add-int/lit8 v5, v6, 0x1

    #@18
    .end local v6           #curIndex:I
    .restart local v5       #curIndex:I
    :try_start_18
    aget-byte v0, p0, v6

    #@1a
    and-int/lit16 v9, v0, 0xff

    #@1c
    .line 153
    if-ge v9, v11, :cond_83

    #@1e
    .line 154
    move v3, v9

    #@1f
    .line 195
    .local v3, length:I
    :cond_1f
    :goto_1f
    new-instance v0, Lcom/android/internal/telephony/cat/ComprehensionTlv;

    #@21
    move-object v4, p0

    #@22
    invoke-direct/range {v0 .. v5}, Lcom/android/internal/telephony/cat/ComprehensionTlv;-><init>(IZI[BI)V
    :try_end_25
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_18 .. :try_end_25} :catch_152

    #@25
    .end local v1           #tag:I
    .end local v2           #cr:Z
    .end local v3           #length:I
    :goto_25
    return-object v0

    #@26
    .line 128
    .end local v5           #curIndex:I
    .restart local v6       #curIndex:I
    :sswitch_26
    :try_start_26
    const-string v0, "CAT     "

    #@28
    new-instance v4, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v10, "decode: unexpected first tag byte="

    #@2f
    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v4

    #@33
    invoke-static {v9}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@36
    move-result-object v10

    #@37
    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v4

    #@3b
    const-string v10, ", startIndex="

    #@3d
    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v4

    #@41
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@44
    move-result-object v4

    #@45
    const-string v10, " curIndex="

    #@47
    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v4

    #@4b
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v4

    #@4f
    const-string v10, " endIndex="

    #@51
    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v4

    #@55
    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@58
    move-result-object v4

    #@59
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5c
    move-result-object v4

    #@5d
    invoke-static {v0, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@60
    .line 133
    const/4 v0, 0x0

    #@61
    move v5, v6

    #@62
    .end local v6           #curIndex:I
    .restart local v5       #curIndex:I
    goto :goto_25

    #@63
    .line 136
    .end local v5           #curIndex:I
    .restart local v6       #curIndex:I
    :sswitch_63
    aget-byte v4, p0, v6

    #@65
    and-int/lit16 v4, v4, 0xff

    #@67
    shl-int/lit8 v4, v4, 0x8

    #@69
    add-int/lit8 v10, v6, 0x1

    #@6b
    aget-byte v10, p0, v10

    #@6d
    and-int/lit16 v10, v10, 0xff

    #@6f
    or-int v1, v4, v10

    #@71
    .line 138
    .restart local v1       #tag:I
    const v4, 0x8000

    #@74
    and-int/2addr v4, v1

    #@75
    if-eqz v4, :cond_7f

    #@77
    .line 139
    .restart local v2       #cr:Z
    :goto_77
    const v0, -0x8001

    #@7a
    and-int/2addr v1, v0

    #@7b
    .line 140
    add-int/lit8 v5, v6, 0x2

    #@7d
    .end local v6           #curIndex:I
    .restart local v5       #curIndex:I
    move v6, v5

    #@7e
    .line 141
    .end local v5           #curIndex:I
    .restart local v6       #curIndex:I
    goto :goto_16

    #@7f
    .end local v2           #cr:Z
    :cond_7f
    move v2, v0

    #@80
    .line 138
    goto :goto_77

    #@81
    :cond_81
    move v2, v0

    #@82
    .line 145
    goto :goto_14

    #@83
    .line 155
    .end local v6           #curIndex:I
    .restart local v2       #cr:Z
    .restart local v5       #curIndex:I
    :cond_83
    const/16 v0, 0x81

    #@85
    if-ne v9, v0, :cond_fd

    #@87
    .line 156
    add-int/lit8 v6, v5, 0x1

    #@89
    .end local v5           #curIndex:I
    .restart local v6       #curIndex:I
    aget-byte v0, p0, v5

    #@8b
    and-int/lit16 v3, v0, 0xff

    #@8d
    .line 157
    .restart local v3       #length:I
    if-ge v3, v11, :cond_1ec

    #@8f
    .line 158
    new-instance v0, Lcom/android/internal/telephony/cat/ResultException;

    #@91
    sget-object v4, Lcom/android/internal/telephony/cat/ResultCode;->CMD_DATA_NOT_UNDERSTOOD:Lcom/android/internal/telephony/cat/ResultCode;

    #@93
    new-instance v10, Ljava/lang/StringBuilder;

    #@95
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@98
    const-string v11, "length < 0x80 length="

    #@9a
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v10

    #@9e
    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@a1
    move-result-object v11

    #@a2
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v10

    #@a6
    const-string v11, " startIndex="

    #@a8
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v10

    #@ac
    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@af
    move-result-object v10

    #@b0
    const-string v11, " curIndex="

    #@b2
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v10

    #@b6
    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b9
    move-result-object v10

    #@ba
    const-string v11, " endIndex="

    #@bc
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v10

    #@c0
    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v10

    #@c4
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c7
    move-result-object v10

    #@c8
    invoke-direct {v0, v4, v10}, Lcom/android/internal/telephony/cat/ResultException;-><init>(Lcom/android/internal/telephony/cat/ResultCode;Ljava/lang/String;)V

    #@cb
    throw v0
    :try_end_cc
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_26 .. :try_end_cc} :catch_cc

    #@cc
    .line 197
    .end local v1           #tag:I
    .end local v2           #cr:Z
    .end local v3           #length:I
    .end local v9           #temp:I
    :catch_cc
    move-exception v7

    #@cd
    move v5, v6

    #@ce
    .line 198
    .end local v6           #curIndex:I
    .restart local v5       #curIndex:I
    .local v7, e:Ljava/lang/IndexOutOfBoundsException;
    :goto_ce
    new-instance v0, Lcom/android/internal/telephony/cat/ResultException;

    #@d0
    sget-object v4, Lcom/android/internal/telephony/cat/ResultCode;->CMD_DATA_NOT_UNDERSTOOD:Lcom/android/internal/telephony/cat/ResultCode;

    #@d2
    new-instance v10, Ljava/lang/StringBuilder;

    #@d4
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@d7
    const-string v11, "IndexOutOfBoundsException startIndex="

    #@d9
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dc
    move-result-object v10

    #@dd
    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e0
    move-result-object v10

    #@e1
    const-string v11, " curIndex="

    #@e3
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e6
    move-result-object v10

    #@e7
    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ea
    move-result-object v10

    #@eb
    const-string v11, " endIndex="

    #@ed
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f0
    move-result-object v10

    #@f1
    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f4
    move-result-object v10

    #@f5
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f8
    move-result-object v10

    #@f9
    invoke-direct {v0, v4, v10}, Lcom/android/internal/telephony/cat/ResultException;-><init>(Lcom/android/internal/telephony/cat/ResultCode;Ljava/lang/String;)V

    #@fc
    throw v0

    #@fd
    .line 164
    .end local v7           #e:Ljava/lang/IndexOutOfBoundsException;
    .restart local v1       #tag:I
    .restart local v2       #cr:Z
    .restart local v9       #temp:I
    :cond_fd
    const/16 v0, 0x82

    #@ff
    if-ne v9, v0, :cond_155

    #@101
    .line 165
    :try_start_101
    aget-byte v0, p0, v5

    #@103
    and-int/lit16 v0, v0, 0xff

    #@105
    shl-int/lit8 v0, v0, 0x8

    #@107
    add-int/lit8 v4, v5, 0x1

    #@109
    aget-byte v4, p0, v4

    #@10b
    and-int/lit16 v4, v4, 0xff

    #@10d
    or-int v3, v0, v4

    #@10f
    .line 167
    .restart local v3       #length:I
    add-int/lit8 v5, v5, 0x2

    #@111
    .line 168
    const/16 v0, 0x100

    #@113
    if-ge v3, v0, :cond_1f

    #@115
    .line 169
    new-instance v0, Lcom/android/internal/telephony/cat/ResultException;

    #@117
    sget-object v4, Lcom/android/internal/telephony/cat/ResultCode;->CMD_DATA_NOT_UNDERSTOOD:Lcom/android/internal/telephony/cat/ResultCode;

    #@119
    new-instance v10, Ljava/lang/StringBuilder;

    #@11b
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@11e
    const-string v11, "two byte length < 0x100 length="

    #@120
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@123
    move-result-object v10

    #@124
    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@127
    move-result-object v11

    #@128
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12b
    move-result-object v10

    #@12c
    const-string v11, " startIndex="

    #@12e
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@131
    move-result-object v10

    #@132
    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@135
    move-result-object v10

    #@136
    const-string v11, " curIndex="

    #@138
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13b
    move-result-object v10

    #@13c
    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13f
    move-result-object v10

    #@140
    const-string v11, " endIndex="

    #@142
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@145
    move-result-object v10

    #@146
    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@149
    move-result-object v10

    #@14a
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14d
    move-result-object v10

    #@14e
    invoke-direct {v0, v4, v10}, Lcom/android/internal/telephony/cat/ResultException;-><init>(Lcom/android/internal/telephony/cat/ResultCode;Ljava/lang/String;)V

    #@151
    throw v0

    #@152
    .line 197
    .end local v3           #length:I
    :catch_152
    move-exception v7

    #@153
    goto/16 :goto_ce

    #@155
    .line 175
    :cond_155
    const/16 v0, 0x83

    #@157
    if-ne v9, v0, :cond_1b3

    #@159
    .line 176
    aget-byte v0, p0, v5

    #@15b
    and-int/lit16 v0, v0, 0xff

    #@15d
    shl-int/lit8 v0, v0, 0x10

    #@15f
    add-int/lit8 v4, v5, 0x1

    #@161
    aget-byte v4, p0, v4

    #@163
    and-int/lit16 v4, v4, 0xff

    #@165
    shl-int/lit8 v4, v4, 0x8

    #@167
    or-int/2addr v0, v4

    #@168
    add-int/lit8 v4, v5, 0x2

    #@16a
    aget-byte v4, p0, v4

    #@16c
    and-int/lit16 v4, v4, 0xff

    #@16e
    or-int v3, v0, v4

    #@170
    .line 179
    .restart local v3       #length:I
    add-int/lit8 v5, v5, 0x3

    #@172
    .line 180
    const/high16 v0, 0x1

    #@174
    if-ge v3, v0, :cond_1f

    #@176
    .line 181
    new-instance v0, Lcom/android/internal/telephony/cat/ResultException;

    #@178
    sget-object v4, Lcom/android/internal/telephony/cat/ResultCode;->CMD_DATA_NOT_UNDERSTOOD:Lcom/android/internal/telephony/cat/ResultCode;

    #@17a
    new-instance v10, Ljava/lang/StringBuilder;

    #@17c
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@17f
    const-string v11, "three byte length < 0x10000 length=0x"

    #@181
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@184
    move-result-object v10

    #@185
    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@188
    move-result-object v11

    #@189
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18c
    move-result-object v10

    #@18d
    const-string v11, " startIndex="

    #@18f
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@192
    move-result-object v10

    #@193
    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@196
    move-result-object v10

    #@197
    const-string v11, " curIndex="

    #@199
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19c
    move-result-object v10

    #@19d
    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a0
    move-result-object v10

    #@1a1
    const-string v11, " endIndex="

    #@1a3
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a6
    move-result-object v10

    #@1a7
    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1aa
    move-result-object v10

    #@1ab
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ae
    move-result-object v10

    #@1af
    invoke-direct {v0, v4, v10}, Lcom/android/internal/telephony/cat/ResultException;-><init>(Lcom/android/internal/telephony/cat/ResultCode;Ljava/lang/String;)V

    #@1b2
    throw v0

    #@1b3
    .line 188
    .end local v3           #length:I
    :cond_1b3
    new-instance v0, Lcom/android/internal/telephony/cat/ResultException;

    #@1b5
    sget-object v4, Lcom/android/internal/telephony/cat/ResultCode;->CMD_DATA_NOT_UNDERSTOOD:Lcom/android/internal/telephony/cat/ResultCode;

    #@1b7
    new-instance v10, Ljava/lang/StringBuilder;

    #@1b9
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@1bc
    const-string v11, "Bad length modifer="

    #@1be
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c1
    move-result-object v10

    #@1c2
    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c5
    move-result-object v10

    #@1c6
    const-string v11, " startIndex="

    #@1c8
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1cb
    move-result-object v10

    #@1cc
    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1cf
    move-result-object v10

    #@1d0
    const-string v11, " curIndex="

    #@1d2
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d5
    move-result-object v10

    #@1d6
    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d9
    move-result-object v10

    #@1da
    const-string v11, " endIndex="

    #@1dc
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1df
    move-result-object v10

    #@1e0
    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e3
    move-result-object v10

    #@1e4
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e7
    move-result-object v10

    #@1e8
    invoke-direct {v0, v4, v10}, Lcom/android/internal/telephony/cat/ResultException;-><init>(Lcom/android/internal/telephony/cat/ResultCode;Ljava/lang/String;)V

    #@1eb
    throw v0
    :try_end_1ec
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_101 .. :try_end_1ec} :catch_152

    #@1ec
    .end local v5           #curIndex:I
    .restart local v3       #length:I
    .restart local v6       #curIndex:I
    :cond_1ec
    move v5, v6

    #@1ed
    .end local v6           #curIndex:I
    .restart local v5       #curIndex:I
    goto/16 :goto_1f

    #@1ef
    .line 124
    nop

    #@1f0
    :sswitch_data_1f0
    .sparse-switch
        0x0 -> :sswitch_26
        0x7f -> :sswitch_63
        0x80 -> :sswitch_26
        0xff -> :sswitch_26
    .end sparse-switch
.end method

.method public static decodeMany([BI)Ljava/util/List;
    .registers 7
    .parameter "data"
    .parameter "startIndex"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([BI)",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/cat/ComprehensionTlv;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/cat/ResultException;
        }
    .end annotation

    #@0
    .prologue
    .line 90
    new-instance v2, Ljava/util/ArrayList;

    #@2
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 91
    .local v2, items:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/cat/ComprehensionTlv;>;"
    array-length v1, p0

    #@6
    .line 92
    .local v1, endIndex:I
    :goto_6
    if-ge p1, v1, :cond_1f

    #@8
    .line 93
    invoke-static {p0, p1}, Lcom/android/internal/telephony/cat/ComprehensionTlv;->decode([BI)Lcom/android/internal/telephony/cat/ComprehensionTlv;

    #@b
    move-result-object v0

    #@c
    .line 94
    .local v0, ctlv:Lcom/android/internal/telephony/cat/ComprehensionTlv;
    if-eqz v0, :cond_18

    #@e
    .line 95
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@11
    .line 96
    iget v3, v0, Lcom/android/internal/telephony/cat/ComprehensionTlv;->mValueIndex:I

    #@13
    iget v4, v0, Lcom/android/internal/telephony/cat/ComprehensionTlv;->mLength:I

    #@15
    add-int p1, v3, v4

    #@17
    goto :goto_6

    #@18
    .line 98
    :cond_18
    const-string v3, "ComprehensionTlv"

    #@1a
    const-string v4, "decodeMany: ctlv is null, stop decoding"

    #@1c
    invoke-static {v3, v4}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@1f
    .line 103
    .end local v0           #ctlv:Lcom/android/internal/telephony/cat/ComprehensionTlv;
    :cond_1f
    return-object v2
.end method


# virtual methods
.method public getLength()I
    .registers 2

    #@0
    .prologue
    .line 69
    iget v0, p0, Lcom/android/internal/telephony/cat/ComprehensionTlv;->mLength:I

    #@2
    return v0
.end method

.method public getRawValue()[B
    .registers 2

    #@0
    .prologue
    .line 77
    iget-object v0, p0, Lcom/android/internal/telephony/cat/ComprehensionTlv;->mRawValue:[B

    #@2
    return-object v0
.end method

.method public getTag()I
    .registers 2

    #@0
    .prologue
    .line 61
    iget v0, p0, Lcom/android/internal/telephony/cat/ComprehensionTlv;->mTag:I

    #@2
    return v0
.end method

.method public getValueIndex()I
    .registers 2

    #@0
    .prologue
    .line 73
    iget v0, p0, Lcom/android/internal/telephony/cat/ComprehensionTlv;->mValueIndex:I

    #@2
    return v0
.end method

.method public isComprehensionRequired()Z
    .registers 2

    #@0
    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/android/internal/telephony/cat/ComprehensionTlv;->mCr:Z

    #@2
    return v0
.end method
