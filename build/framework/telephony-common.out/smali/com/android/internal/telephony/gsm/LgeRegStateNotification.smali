.class public Lcom/android/internal/telephony/gsm/LgeRegStateNotification;
.super Ljava/lang/Object;
.source "LgeRegStateNotification.java"


# static fields
.field protected static final LOG_TAG:Ljava/lang/String; = "CALL_FRW"

.field static final REJECTCAUSE_NOTIFICATION_ID:I = 0xc73b

.field static final SEARCHING_NOTIFICATION_ID:I = 0xc73a

.field static final SUCCESS_NOTIFICATION_ID:I = 0xc739

.field private static sInstance:Lcom/android/internal/telephony/gsm/LgeRegStateNotification;


# instance fields
.field private mFailNotification:Landroid/app/Notification;

.field private mHasShownOperatorInfo:Z

.field private mIMSI:I

.field private mNotificationManager:Landroid/app/NotificationManager;

.field private mOperatorMccMnc:Ljava/lang/String;

.field private mOperatorName:Ljava/lang/String;

.field private mPhone:Lcom/android/internal/telephony/PhoneBase;

.field private mPrevOperatorName:Ljava/lang/String;

.field private mRef:I

.field private mSearchingNotification:Landroid/app/Notification;

.field private mSimState:Lcom/android/internal/telephony/IccCardConstants$State;

.field private mSuccessNotification:Landroid/app/Notification;

.field private newSS:Landroid/telephony/ServiceState;

.field private prevSS:Landroid/telephony/ServiceState;


# direct methods
.method private constructor <init>(Lcom/android/internal/telephony/PhoneBase;)V
    .registers 4
    .parameter "phone"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 85
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 72
    new-instance v0, Landroid/telephony/ServiceState;

    #@6
    invoke-direct {v0}, Landroid/telephony/ServiceState;-><init>()V

    #@9
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->prevSS:Landroid/telephony/ServiceState;

    #@b
    .line 73
    new-instance v0, Landroid/telephony/ServiceState;

    #@d
    invoke-direct {v0}, Landroid/telephony/ServiceState;-><init>()V

    #@10
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->newSS:Landroid/telephony/ServiceState;

    #@12
    .line 78
    iput v1, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mIMSI:I

    #@14
    .line 79
    sget-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->READY:Lcom/android/internal/telephony/IccCardConstants$State;

    #@16
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mSimState:Lcom/android/internal/telephony/IccCardConstants$State;

    #@18
    .line 83
    iput v1, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mRef:I

    #@1a
    .line 86
    iput-object p1, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@1c
    .line 87
    return-void
.end method

.method public static declared-synchronized getInstance(Lcom/android/internal/telephony/PhoneBase;)Lcom/android/internal/telephony/gsm/LgeRegStateNotification;
    .registers 5
    .parameter "phone"

    #@0
    .prologue
    .line 91
    const-class v1, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;

    #@2
    monitor-enter v1

    #@3
    :try_start_3
    sget-object v0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->sInstance:Lcom/android/internal/telephony/gsm/LgeRegStateNotification;

    #@5
    if-nez v0, :cond_36

    #@7
    .line 92
    new-instance v0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;

    #@9
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;-><init>(Lcom/android/internal/telephony/PhoneBase;)V

    #@c
    sput-object v0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->sInstance:Lcom/android/internal/telephony/gsm/LgeRegStateNotification;

    #@e
    .line 96
    :goto_e
    sget-object v0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->sInstance:Lcom/android/internal/telephony/gsm/LgeRegStateNotification;

    #@10
    iget v2, v0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mRef:I

    #@12
    add-int/lit8 v2, v2, 0x1

    #@14
    iput v2, v0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mRef:I

    #@16
    .line 98
    const-string v0, "CALL_FRW"

    #@18
    new-instance v2, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v3, "[LgeRegStateNotification] getInstance():: sInstance.mRef = "

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    sget-object v3, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->sInstance:Lcom/android/internal/telephony/gsm/LgeRegStateNotification;

    #@25
    iget v3, v3, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mRef:I

    #@27
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v2

    #@2f
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 100
    sget-object v0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->sInstance:Lcom/android/internal/telephony/gsm/LgeRegStateNotification;
    :try_end_34
    .catchall {:try_start_3 .. :try_end_34} :catchall_3b

    #@34
    monitor-exit v1

    #@35
    return-object v0

    #@36
    .line 94
    :cond_36
    :try_start_36
    sget-object v0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->sInstance:Lcom/android/internal/telephony/gsm/LgeRegStateNotification;

    #@38
    iput-object p0, v0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mPhone:Lcom/android/internal/telephony/PhoneBase;
    :try_end_3a
    .catchall {:try_start_36 .. :try_end_3a} :catchall_3b

    #@3a
    goto :goto_e

    #@3b
    .line 91
    :catchall_3b
    move-exception v0

    #@3c
    monitor-exit v1

    #@3d
    throw v0
.end method

.method private getOperatorInfo()V
    .registers 2

    #@0
    .prologue
    .line 242
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->newSS:Landroid/telephony/ServiceState;

    #@2
    invoke-virtual {v0}, Landroid/telephony/ServiceState;->getOperatorAlphaShort()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mOperatorName:Ljava/lang/String;

    #@8
    .line 243
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->prevSS:Landroid/telephony/ServiceState;

    #@a
    invoke-virtual {v0}, Landroid/telephony/ServiceState;->getOperatorAlphaShort()Ljava/lang/String;

    #@d
    move-result-object v0

    #@e
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mPrevOperatorName:Ljava/lang/String;

    #@10
    .line 244
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->newSS:Landroid/telephony/ServiceState;

    #@12
    invoke-virtual {v0}, Landroid/telephony/ServiceState;->getOperatorNumeric()Ljava/lang/String;

    #@15
    move-result-object v0

    #@16
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mOperatorMccMnc:Ljava/lang/String;

    #@18
    .line 245
    return-void
.end method

.method private showOperatorInfoLGU()V
    .registers 9

    #@0
    .prologue
    const/high16 v7, 0x1000

    #@2
    .line 250
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@4
    iget-object v1, v1, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@6
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@9
    move-result-object v1

    #@a
    if-eqz v1, :cond_50

    #@c
    .line 251
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@e
    iget-object v1, v1, Lcom/android/internal/telephony/PhoneBase;->mIccRecords:Ljava/util/concurrent/atomic/AtomicReference;

    #@10
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    #@13
    move-result-object v1

    #@14
    check-cast v1, Lcom/android/internal/telephony/uicc/IccRecords;

    #@16
    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/IccRecords;->getUsimIsSponIMSI()I

    #@19
    move-result v1

    #@1a
    iput v1, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mIMSI:I

    #@1c
    .line 252
    new-instance v1, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v2, "mIMSI = "

    #@23
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v1

    #@27
    iget v2, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mIMSI:I

    #@29
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v1

    #@2d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v1

    #@31
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->log(Ljava/lang/String;)V

    #@34
    .line 253
    iget v1, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mIMSI:I

    #@36
    const/4 v2, 0x1

    #@37
    if-ne v1, v2, :cond_a4

    #@39
    .line 254
    new-instance v1, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mOperatorName:Ljava/lang/String;

    #@40
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v1

    #@44
    const-string v2, " (Zone1)"

    #@46
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v1

    #@4a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v1

    #@4e
    iput-object v1, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mOperatorName:Ljava/lang/String;

    #@50
    .line 263
    :cond_50
    :goto_50
    new-instance v0, Landroid/content/Intent;

    #@52
    const-string v1, "android.intent.action.MAIN"

    #@54
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@57
    .line 264
    .local v0, RoamingIntent:Landroid/content/Intent;
    const-string v1, "com.lge.roamingsettings"

    #@59
    const-string v2, "com.lge.roamingsettings.uplusroaming.wcdmaroaming.imsi.RoamingIMSISelect"

    #@5b
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@5e
    .line 265
    invoke-virtual {v0, v7}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@61
    .line 267
    new-instance v1, Landroid/app/Notification;

    #@63
    invoke-direct {v1}, Landroid/app/Notification;-><init>()V

    #@66
    iput-object v1, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mSuccessNotification:Landroid/app/Notification;

    #@68
    .line 268
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mSuccessNotification:Landroid/app/Notification;

    #@6a
    const-wide/16 v2, 0x0

    #@6c
    iput-wide v2, v1, Landroid/app/Notification;->when:J

    #@6e
    .line 269
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mSuccessNotification:Landroid/app/Notification;

    #@70
    const/16 v2, 0x20

    #@72
    iput v2, v1, Landroid/app/Notification;->flags:I

    #@74
    .line 270
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mSuccessNotification:Landroid/app/Notification;

    #@76
    const v2, 0x20203cb

    #@79
    iput v2, v1, Landroid/app/Notification;->icon:I

    #@7b
    .line 271
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mSuccessNotification:Landroid/app/Notification;

    #@7d
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@7f
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@82
    move-result-object v2

    #@83
    const-string v3, "UPLUS_ROAMING_SUCCESS_NOTIFICATION_TITLE"

    #@85
    invoke-static {v3}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@88
    move-result-object v3

    #@89
    iget-object v4, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mOperatorName:Ljava/lang/String;

    #@8b
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@8d
    invoke-virtual {v5}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@90
    move-result-object v5

    #@91
    const/4 v6, 0x0

    #@92
    invoke-static {v5, v6, v0, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@95
    move-result-object v5

    #@96
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    #@99
    .line 274
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mNotificationManager:Landroid/app/NotificationManager;

    #@9b
    const v2, 0xc739

    #@9e
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mSuccessNotification:Landroid/app/Notification;

    #@a0
    invoke-virtual {v1, v2, v3}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    #@a3
    .line 275
    return-void

    #@a4
    .line 255
    .end local v0           #RoamingIntent:Landroid/content/Intent;
    :cond_a4
    iget v1, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mIMSI:I

    #@a6
    if-nez v1, :cond_ae

    #@a8
    .line 256
    const-string v1, "no Imsi zone"

    #@aa
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->log(Ljava/lang/String;)V

    #@ad
    goto :goto_50

    #@ae
    .line 258
    :cond_ae
    new-instance v1, Ljava/lang/StringBuilder;

    #@b0
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b3
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mOperatorName:Ljava/lang/String;

    #@b5
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b8
    move-result-object v1

    #@b9
    const-string v2, " (Zone2)"

    #@bb
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@be
    move-result-object v1

    #@bf
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c2
    move-result-object v1

    #@c3
    iput-object v1, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mOperatorName:Ljava/lang/String;

    #@c5
    goto :goto_50
.end method

.method private showOperatorInfoSKT()V
    .registers 10

    #@0
    .prologue
    const/high16 v8, 0x1000

    #@2
    const/4 v7, 0x3

    #@3
    const/4 v6, 0x0

    #@4
    .line 278
    new-instance v0, Landroid/content/Intent;

    #@6
    const-string v1, "android.intent.action.MAIN"

    #@8
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@b
    .line 279
    .local v0, TRoamingIntent:Landroid/content/Intent;
    const-string v1, "ro.product.model"

    #@d
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@10
    move-result-object v1

    #@11
    const-string v2, "LG-F240S"

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@16
    move-result v1

    #@17
    if-nez v1, :cond_35

    #@19
    const-string v1, "ro.product.model"

    #@1b
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    const-string v2, "LG-F260S"

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@24
    move-result v1

    #@25
    if-nez v1, :cond_35

    #@27
    const-string v1, "ro.product.model"

    #@29
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@2c
    move-result-object v1

    #@2d
    const-string v2, "LG-F320S"

    #@2f
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@32
    move-result v1

    #@33
    if-eqz v1, :cond_b9

    #@35
    .line 282
    :cond_35
    const-string v1, "com.lge.roamingsettings"

    #@37
    const-string v2, "com.lge.roamingsettings.troaming.TRoamingFGK"

    #@39
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@3c
    .line 286
    :goto_3c
    invoke-virtual {v0, v8}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@3f
    .line 288
    new-instance v1, Landroid/app/Notification;

    #@41
    invoke-direct {v1}, Landroid/app/Notification;-><init>()V

    #@44
    iput-object v1, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mSuccessNotification:Landroid/app/Notification;

    #@46
    .line 289
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mSuccessNotification:Landroid/app/Notification;

    #@48
    const-wide/16 v2, 0x0

    #@4a
    iput-wide v2, v1, Landroid/app/Notification;->when:J

    #@4c
    .line 290
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mSuccessNotification:Landroid/app/Notification;

    #@4e
    const/16 v2, 0x20

    #@50
    iput v2, v1, Landroid/app/Notification;->flags:I

    #@52
    .line 291
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mSuccessNotification:Landroid/app/Notification;

    #@54
    const/4 v2, -0x2

    #@55
    iput v2, v1, Landroid/app/Notification;->priority:I

    #@57
    .line 292
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mSuccessNotification:Landroid/app/Notification;

    #@59
    const v2, 0x202053a

    #@5c
    iput v2, v1, Landroid/app/Notification;->icon:I

    #@5e
    .line 293
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mSuccessNotification:Landroid/app/Notification;

    #@60
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@62
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@65
    move-result-object v2

    #@66
    const-string v3, "SKT_ROAMING_SUCCESS_NOTIFICATION_TITLE"

    #@68
    invoke-static {v3}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@6b
    move-result-object v3

    #@6c
    new-instance v4, Ljava/lang/StringBuilder;

    #@6e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@71
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mOperatorName:Ljava/lang/String;

    #@73
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v4

    #@77
    const-string v5, " ["

    #@79
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v4

    #@7d
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mOperatorMccMnc:Ljava/lang/String;

    #@7f
    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@82
    move-result-object v5

    #@83
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v4

    #@87
    const-string v5, " "

    #@89
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v4

    #@8d
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mOperatorMccMnc:Ljava/lang/String;

    #@8f
    invoke-virtual {v5, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@92
    move-result-object v5

    #@93
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v4

    #@97
    const-string v5, "]"

    #@99
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v4

    #@9d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a0
    move-result-object v4

    #@a1
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@a3
    invoke-virtual {v5}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@a6
    move-result-object v5

    #@a7
    invoke-static {v5, v6, v0, v8}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@aa
    move-result-object v5

    #@ab
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    #@ae
    .line 296
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mNotificationManager:Landroid/app/NotificationManager;

    #@b0
    const v2, 0xc739

    #@b3
    iget-object v3, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mSuccessNotification:Landroid/app/Notification;

    #@b5
    invoke-virtual {v1, v2, v3}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    #@b8
    .line 297
    return-void

    #@b9
    .line 284
    :cond_b9
    const-string v1, "com.lge.roamingsettings"

    #@bb
    const-string v2, "com.lge.roamingsettings.troaming.TRoamingonLTE"

    #@bd
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@c0
    goto/16 :goto_3c
.end method

.method private showSearching()V
    .registers 6

    #@0
    .prologue
    .line 300
    new-instance v0, Landroid/app/Notification;

    #@2
    invoke-direct {v0}, Landroid/app/Notification;-><init>()V

    #@5
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mSearchingNotification:Landroid/app/Notification;

    #@7
    .line 301
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mSearchingNotification:Landroid/app/Notification;

    #@9
    const-wide/16 v1, 0x0

    #@b
    iput-wide v1, v0, Landroid/app/Notification;->when:J

    #@d
    .line 302
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mSearchingNotification:Landroid/app/Notification;

    #@f
    const/16 v1, 0x20

    #@11
    iput v1, v0, Landroid/app/Notification;->flags:I

    #@13
    .line 303
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mSearchingNotification:Landroid/app/Notification;

    #@15
    const/4 v1, -0x2

    #@16
    iput v1, v0, Landroid/app/Notification;->priority:I

    #@18
    .line 304
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mSearchingNotification:Landroid/app/Notification;

    #@1a
    const v1, 0x202054e

    #@1d
    iput v1, v0, Landroid/app/Notification;->icon:I

    #@1f
    .line 305
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mSearchingNotification:Landroid/app/Notification;

    #@21
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@23
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@26
    move-result-object v1

    #@27
    const-string v2, "UPLUS_ROAMING_SEARCHING_NOTIFICATION_TITLE"

    #@29
    invoke-static {v2}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@2c
    move-result-object v2

    #@2d
    const-string v3, "UPLUS_ROAMING_SEARCHING_NOTIFICATION_CONTENT"

    #@2f
    invoke-static {v3}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@32
    move-result-object v3

    #@33
    const/4 v4, 0x0

    #@34
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    #@37
    .line 308
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mNotificationManager:Landroid/app/NotificationManager;

    #@39
    const v1, 0xc73a

    #@3c
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mSearchingNotification:Landroid/app/Notification;

    #@3e
    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    #@41
    .line 309
    return-void
.end method


# virtual methods
.method public dispose()V
    .registers 4

    #@0
    .prologue
    .line 104
    iget v1, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mRef:I

    #@2
    add-int/lit8 v1, v1, -0x1

    #@4
    iput v1, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mRef:I

    #@6
    .line 105
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v2, "dispose mRef="

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    sget-object v2, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->sInstance:Lcom/android/internal/telephony/gsm/LgeRegStateNotification;

    #@13
    iget v2, v2, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mRef:I

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->log(Ljava/lang/String;)V

    #@20
    .line 106
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@22
    invoke-virtual {v1}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@25
    move-result-object v1

    #@26
    const-string v2, "notification"

    #@28
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@2b
    move-result-object v0

    #@2c
    check-cast v0, Landroid/app/NotificationManager;

    #@2e
    .line 107
    .local v0, notificationManager:Landroid/app/NotificationManager;
    iget v1, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mRef:I

    #@30
    if-gtz v1, :cond_43

    #@32
    .line 108
    const v1, 0xc739

    #@35
    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    #@38
    .line 109
    const v1, 0xc73a

    #@3b
    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    #@3e
    .line 110
    const-string v1, "dispose() mRef <= 0 : clear succes and searching notification"

    #@40
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->log(Ljava/lang/String;)V

    #@43
    .line 113
    :cond_43
    const v1, 0xc73b

    #@46
    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    #@49
    .line 114
    const-string v1, "CALL_FRW"

    #@4b
    const-string v2, "dispose(): clear reject cause notification"

    #@4d
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@50
    .line 116
    return-void
.end method

.method public handleNotification()V
    .registers 8

    #@0
    .prologue
    const v6, 0xc73b

    #@3
    const v5, 0xc73a

    #@6
    const/4 v4, 0x1

    #@7
    const v3, 0xc739

    #@a
    const/4 v2, 0x0

    #@b
    .line 120
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@d
    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@10
    move-result-object v0

    #@11
    const-string v1, "notification"

    #@13
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@16
    move-result-object v0

    #@17
    check-cast v0, Landroid/app/NotificationManager;

    #@19
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mNotificationManager:Landroid/app/NotificationManager;

    #@1b
    .line 121
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@1d
    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneBase;->getServiceState()Landroid/telephony/ServiceState;

    #@20
    move-result-object v0

    #@21
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->newSS:Landroid/telephony/ServiceState;

    #@23
    .line 122
    const-string v0, "KR"

    #@25
    const-string v1, "LGU"

    #@27
    invoke-static {v0, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@2a
    move-result v0

    #@2b
    if-eqz v0, :cond_b8

    #@2d
    .line 124
    new-instance v0, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    const-string v1, "prevSS : "

    #@34
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v0

    #@38
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->prevSS:Landroid/telephony/ServiceState;

    #@3a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v0

    #@3e
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v0

    #@42
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->log(Ljava/lang/String;)V

    #@45
    .line 125
    new-instance v0, Ljava/lang/StringBuilder;

    #@47
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@4a
    const-string v1, "newSS : "

    #@4c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v0

    #@50
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->newSS:Landroid/telephony/ServiceState;

    #@52
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v0

    #@56
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v0

    #@5a
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->log(Ljava/lang/String;)V

    #@5d
    .line 126
    new-instance v0, Ljava/lang/StringBuilder;

    #@5f
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@62
    const-string v1, "isVoiceSearching : "

    #@64
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v0

    #@68
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->newSS:Landroid/telephony/ServiceState;

    #@6a
    invoke-virtual {v1}, Landroid/telephony/ServiceState;->isVoiceSearching()Z

    #@6d
    move-result v1

    #@6e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@71
    move-result-object v0

    #@72
    const-string v1, ", isDataSearching : "

    #@74
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v0

    #@78
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->newSS:Landroid/telephony/ServiceState;

    #@7a
    invoke-virtual {v1}, Landroid/telephony/ServiceState;->isDataSearching()Z

    #@7d
    move-result v1

    #@7e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@81
    move-result-object v0

    #@82
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@85
    move-result-object v0

    #@86
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->log(Ljava/lang/String;)V

    #@89
    .line 128
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@8b
    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@8e
    move-result-object v0

    #@8f
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@92
    move-result-object v0

    #@93
    const-string v1, "airplane_mode_on"

    #@95
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@98
    move-result v0

    #@99
    if-lez v0, :cond_11d

    #@9b
    .line 130
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mNotificationManager:Landroid/app/NotificationManager;

    #@9d
    invoke-virtual {v0, v3}, Landroid/app/NotificationManager;->cancel(I)V

    #@a0
    .line 131
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mNotificationManager:Landroid/app/NotificationManager;

    #@a2
    invoke-virtual {v0, v5}, Landroid/app/NotificationManager;->cancel(I)V

    #@a5
    .line 133
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mNotificationManager:Landroid/app/NotificationManager;

    #@a7
    invoke-virtual {v0, v6}, Landroid/app/NotificationManager;->cancel(I)V

    #@aa
    .line 135
    const-string v0, "Airplane Mode : clear success, searching, and reject cause notification."

    #@ac
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->log(Ljava/lang/String;)V

    #@af
    .line 137
    new-instance v0, Landroid/telephony/ServiceState;

    #@b1
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->newSS:Landroid/telephony/ServiceState;

    #@b3
    invoke-direct {v0, v1}, Landroid/telephony/ServiceState;-><init>(Landroid/telephony/ServiceState;)V

    #@b6
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->prevSS:Landroid/telephony/ServiceState;

    #@b8
    .line 193
    :cond_b8
    :goto_b8
    const-string v0, "KR"

    #@ba
    const-string v1, "SKT"

    #@bc
    invoke-static {v0, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@bf
    move-result v0

    #@c0
    if-eqz v0, :cond_11c

    #@c2
    .line 195
    new-instance v0, Ljava/lang/StringBuilder;

    #@c4
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@c7
    const-string v1, "prevSS : "

    #@c9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cc
    move-result-object v0

    #@cd
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->prevSS:Landroid/telephony/ServiceState;

    #@cf
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@d2
    move-result-object v0

    #@d3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d6
    move-result-object v0

    #@d7
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->log(Ljava/lang/String;)V

    #@da
    .line 196
    new-instance v0, Ljava/lang/StringBuilder;

    #@dc
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@df
    const-string v1, "newSS : "

    #@e1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e4
    move-result-object v0

    #@e5
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->newSS:Landroid/telephony/ServiceState;

    #@e7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@ea
    move-result-object v0

    #@eb
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ee
    move-result-object v0

    #@ef
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->log(Ljava/lang/String;)V

    #@f2
    .line 198
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@f4
    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneBase;->getContext()Landroid/content/Context;

    #@f7
    move-result-object v0

    #@f8
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@fb
    move-result-object v0

    #@fc
    const-string v1, "airplane_mode_on"

    #@fe
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@101
    move-result v0

    #@102
    if-lez v0, :cond_1ce

    #@104
    .line 200
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mNotificationManager:Landroid/app/NotificationManager;

    #@106
    invoke-virtual {v0, v3}, Landroid/app/NotificationManager;->cancel(I)V

    #@109
    .line 201
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mNotificationManager:Landroid/app/NotificationManager;

    #@10b
    invoke-virtual {v0, v6}, Landroid/app/NotificationManager;->cancel(I)V

    #@10e
    .line 202
    const-string v0, "Airplane Mode : clear both success & fail notification"

    #@110
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->log(Ljava/lang/String;)V

    #@113
    .line 204
    new-instance v0, Landroid/telephony/ServiceState;

    #@115
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->newSS:Landroid/telephony/ServiceState;

    #@117
    invoke-direct {v0, v1}, Landroid/telephony/ServiceState;-><init>(Landroid/telephony/ServiceState;)V

    #@11a
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->prevSS:Landroid/telephony/ServiceState;

    #@11c
    .line 239
    :cond_11c
    :goto_11c
    return-void

    #@11d
    .line 139
    :cond_11d
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->newSS:Landroid/telephony/ServiceState;

    #@11f
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->prevSS:Landroid/telephony/ServiceState;

    #@121
    invoke-virtual {v0, v1}, Landroid/telephony/ServiceState;->equals(Ljava/lang/Object;)Z

    #@124
    move-result v0

    #@125
    if-eqz v0, :cond_12b

    #@127
    iget v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mIMSI:I

    #@129
    if-nez v0, :cond_b8

    #@12b
    .line 141
    :cond_12b
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->newSS:Landroid/telephony/ServiceState;

    #@12d
    invoke-virtual {v0}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@130
    move-result v0

    #@131
    if-eqz v0, :cond_17e

    #@133
    .line 143
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->getOperatorInfo()V

    #@136
    .line 145
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mOperatorName:Ljava/lang/String;

    #@138
    if-eqz v0, :cond_153

    #@13a
    .line 146
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mOperatorName:Ljava/lang/String;

    #@13c
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mPrevOperatorName:Ljava/lang/String;

    #@13e
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@141
    move-result v0

    #@142
    if-nez v0, :cond_16a

    #@144
    .line 147
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mNotificationManager:Landroid/app/NotificationManager;

    #@146
    invoke-virtual {v0, v5}, Landroid/app/NotificationManager;->cancel(I)V

    #@149
    .line 148
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->showOperatorInfoLGU()V

    #@14c
    .line 149
    const-string v0, "Show roaming operator info. since operator name changed"

    #@14e
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->log(Ljava/lang/String;)V

    #@151
    .line 150
    iput-boolean v4, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mHasShownOperatorInfo:Z

    #@153
    .line 160
    :cond_153
    :goto_153
    iget v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mIMSI:I

    #@155
    if-nez v0, :cond_15f

    #@157
    .line 161
    const-string v0, "update operator info since imsi is not decided yet"

    #@159
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->log(Ljava/lang/String;)V

    #@15c
    .line 162
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->showOperatorInfoLGU()V

    #@15f
    .line 187
    :cond_15f
    :goto_15f
    new-instance v0, Landroid/telephony/ServiceState;

    #@161
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->newSS:Landroid/telephony/ServiceState;

    #@163
    invoke-direct {v0, v1}, Landroid/telephony/ServiceState;-><init>(Landroid/telephony/ServiceState;)V

    #@166
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->prevSS:Landroid/telephony/ServiceState;

    #@168
    goto/16 :goto_b8

    #@16a
    .line 151
    :cond_16a
    iget-boolean v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mHasShownOperatorInfo:Z

    #@16c
    if-nez v0, :cond_153

    #@16e
    .line 152
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mNotificationManager:Landroid/app/NotificationManager;

    #@170
    invoke-virtual {v0, v5}, Landroid/app/NotificationManager;->cancel(I)V

    #@173
    .line 153
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->showOperatorInfoLGU()V

    #@176
    .line 154
    const-string v0, "Show roaming operator info. since it has not been shown before"

    #@178
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->log(Ljava/lang/String;)V

    #@17b
    .line 155
    iput-boolean v4, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mHasShownOperatorInfo:Z

    #@17d
    goto :goto_153

    #@17e
    .line 166
    :cond_17e
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->newSS:Landroid/telephony/ServiceState;

    #@180
    invoke-virtual {v0}, Landroid/telephony/ServiceState;->isVoiceSearching()Z

    #@183
    move-result v0

    #@184
    if-eqz v0, :cond_1bc

    #@186
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->newSS:Landroid/telephony/ServiceState;

    #@188
    invoke-virtual {v0}, Landroid/telephony/ServiceState;->isDataSearching()Z

    #@18b
    move-result v0

    #@18c
    if-eqz v0, :cond_1bc

    #@18e
    .line 168
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mNotificationManager:Landroid/app/NotificationManager;

    #@190
    invoke-virtual {v0, v3}, Landroid/app/NotificationManager;->cancel(I)V

    #@193
    .line 170
    new-instance v0, Ljava/lang/StringBuilder;

    #@195
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@198
    const-string v1, "mSimState = "

    #@19a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19d
    move-result-object v0

    #@19e
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mSimState:Lcom/android/internal/telephony/IccCardConstants$State;

    #@1a0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a3
    move-result-object v0

    #@1a4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a7
    move-result-object v0

    #@1a8
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->log(Ljava/lang/String;)V

    #@1ab
    .line 171
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mSimState:Lcom/android/internal/telephony/IccCardConstants$State;

    #@1ad
    sget-object v1, Lcom/android/internal/telephony/IccCardConstants$State;->ABSENT:Lcom/android/internal/telephony/IccCardConstants$State;

    #@1af
    if-eq v0, v1, :cond_1b9

    #@1b1
    .line 172
    const-string v0, "Show searching ..."

    #@1b3
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->log(Ljava/lang/String;)V

    #@1b6
    .line 173
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->showSearching()V

    #@1b9
    .line 176
    :cond_1b9
    iput-boolean v2, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mHasShownOperatorInfo:Z

    #@1bb
    goto :goto_15f

    #@1bc
    .line 180
    :cond_1bc
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mNotificationManager:Landroid/app/NotificationManager;

    #@1be
    invoke-virtual {v0, v3}, Landroid/app/NotificationManager;->cancel(I)V

    #@1c1
    .line 181
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mNotificationManager:Landroid/app/NotificationManager;

    #@1c3
    invoke-virtual {v0, v5}, Landroid/app/NotificationManager;->cancel(I)V

    #@1c6
    .line 182
    const-string v0, "clear success notification or searching notification"

    #@1c8
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->log(Ljava/lang/String;)V

    #@1cb
    .line 183
    iput-boolean v2, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mHasShownOperatorInfo:Z

    #@1cd
    goto :goto_15f

    #@1ce
    .line 206
    :cond_1ce
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->newSS:Landroid/telephony/ServiceState;

    #@1d0
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->prevSS:Landroid/telephony/ServiceState;

    #@1d2
    invoke-virtual {v0, v1}, Landroid/telephony/ServiceState;->equals(Ljava/lang/Object;)Z

    #@1d5
    move-result v0

    #@1d6
    if-nez v0, :cond_11c

    #@1d8
    .line 208
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->newSS:Landroid/telephony/ServiceState;

    #@1da
    invoke-virtual {v0}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@1dd
    move-result v0

    #@1de
    if-eqz v0, :cond_21f

    #@1e0
    .line 210
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->getOperatorInfo()V

    #@1e3
    .line 211
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mOperatorName:Ljava/lang/String;

    #@1e5
    if-eqz v0, :cond_200

    #@1e7
    .line 212
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mOperatorName:Ljava/lang/String;

    #@1e9
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mPrevOperatorName:Ljava/lang/String;

    #@1eb
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1ee
    move-result v0

    #@1ef
    if-nez v0, :cond_20b

    #@1f1
    .line 213
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mNotificationManager:Landroid/app/NotificationManager;

    #@1f3
    invoke-virtual {v0, v6}, Landroid/app/NotificationManager;->cancel(I)V

    #@1f6
    .line 214
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->showOperatorInfoSKT()V

    #@1f9
    .line 215
    const-string v0, "Show roaming operator info. since operator name changed."

    #@1fb
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->log(Ljava/lang/String;)V

    #@1fe
    .line 216
    iput-boolean v4, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mHasShownOperatorInfo:Z

    #@200
    .line 233
    :cond_200
    :goto_200
    new-instance v0, Landroid/telephony/ServiceState;

    #@202
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->newSS:Landroid/telephony/ServiceState;

    #@204
    invoke-direct {v0, v1}, Landroid/telephony/ServiceState;-><init>(Landroid/telephony/ServiceState;)V

    #@207
    iput-object v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->prevSS:Landroid/telephony/ServiceState;

    #@209
    goto/16 :goto_11c

    #@20b
    .line 217
    :cond_20b
    iget-boolean v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mHasShownOperatorInfo:Z

    #@20d
    if-nez v0, :cond_200

    #@20f
    .line 218
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mNotificationManager:Landroid/app/NotificationManager;

    #@211
    invoke-virtual {v0, v6}, Landroid/app/NotificationManager;->cancel(I)V

    #@214
    .line 219
    invoke-direct {p0}, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->showOperatorInfoSKT()V

    #@217
    .line 220
    const-string v0, "Show roaming operator info. since it has not been shown before."

    #@219
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->log(Ljava/lang/String;)V

    #@21c
    .line 221
    iput-boolean v4, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mHasShownOperatorInfo:Z

    #@21e
    goto :goto_200

    #@21f
    .line 227
    :cond_21f
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mNotificationManager:Landroid/app/NotificationManager;

    #@221
    invoke-virtual {v0, v3}, Landroid/app/NotificationManager;->cancel(I)V

    #@224
    .line 228
    const-string v0, "clear success notification"

    #@226
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->log(Ljava/lang/String;)V

    #@229
    .line 229
    iput-boolean v2, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mHasShownOperatorInfo:Z

    #@22b
    goto :goto_200
.end method

.method protected log(Ljava/lang/String;)V
    .registers 5
    .parameter "s"

    #@0
    .prologue
    .line 323
    const-string v0, "CALL_FRW"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[LgeRegStateNotification] "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 324
    return-void
.end method

.method public setSimState(Lcom/android/internal/telephony/IccCardConstants$State;)V
    .registers 4
    .parameter "simState"

    #@0
    .prologue
    .line 313
    iput-object p1, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mSimState:Lcom/android/internal/telephony/IccCardConstants$State;

    #@2
    .line 314
    new-instance v0, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v1, "update mSimState : "

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v0

    #@d
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mSimState:Lcom/android/internal/telephony/IccCardConstants$State;

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v0

    #@17
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->log(Ljava/lang/String;)V

    #@1a
    .line 315
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mSimState:Lcom/android/internal/telephony/IccCardConstants$State;

    #@1c
    sget-object v1, Lcom/android/internal/telephony/IccCardConstants$State;->ABSENT:Lcom/android/internal/telephony/IccCardConstants$State;

    #@1e
    if-ne v0, v1, :cond_2d

    #@20
    .line 316
    const-string v0, "cancel searching notification since sim is not inserted"

    #@22
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->log(Ljava/lang/String;)V

    #@25
    .line 317
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/LgeRegStateNotification;->mNotificationManager:Landroid/app/NotificationManager;

    #@27
    const v1, 0xc73a

    #@2a
    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    #@2d
    .line 319
    :cond_2d
    return-void
.end method
