.class public final Lcom/android/internal/telephony/ModemInfoResponse;
.super Ljava/lang/Object;
.source "ModemInfoResponse.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field static addrString:Ljava/lang/String;

.field static i:I

.field static parts:[Ljava/lang/String;

.field private static response:Ljava/lang/Object;

.field static st:Ljava/util/StringTokenizer;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 32
    const-class v0, Lcom/android/internal/telephony/ModemInfoResponse;

    #@2
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->TAG:Ljava/lang/String;

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 41
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 43
    return-void
.end method

.method private static convert_String_To_IntArray(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
    .registers 8
    .parameter "object"
    .parameter "expr"

    #@0
    .prologue
    .line 470
    sget-object v3, Lcom/android/internal/telephony/ModemInfoResponse;->TAG:Ljava/lang/String;

    #@2
    new-instance v4, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v5, "object : "

    #@9
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v4

    #@d
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v4

    #@11
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v4

    #@15
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 472
    if-nez p0, :cond_1c

    #@1a
    .line 473
    const/4 v1, 0x0

    #@1b
    .line 482
    :cond_1b
    return-object v1

    #@1c
    :cond_1c
    move-object v2, p0

    #@1d
    .line 475
    check-cast v2, Ljava/lang/String;

    #@1f
    .line 477
    .local v2, target:Ljava/lang/String;
    invoke-virtual {v2, p1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@22
    move-result-object v0

    #@23
    .line 478
    .local v0, parts:[Ljava/lang/String;
    array-length v3, v0

    #@24
    new-array v1, v3, [I

    #@26
    .line 479
    .local v1, retResponse:[I
    const/4 v3, 0x0

    #@27
    sput v3, Lcom/android/internal/telephony/ModemInfoResponse;->i:I

    #@29
    :goto_29
    sget v3, Lcom/android/internal/telephony/ModemInfoResponse;->i:I

    #@2b
    array-length v4, v0

    #@2c
    if-ge v3, v4, :cond_1b

    #@2e
    .line 480
    sget v3, Lcom/android/internal/telephony/ModemInfoResponse;->i:I

    #@30
    sget v4, Lcom/android/internal/telephony/ModemInfoResponse;->i:I

    #@32
    aget-object v4, v0, v4

    #@34
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@37
    move-result v4

    #@38
    aput v4, v1, v3

    #@3a
    .line 479
    sget v3, Lcom/android/internal/telephony/ModemInfoResponse;->i:I

    #@3c
    add-int/lit8 v3, v3, 0x1

    #@3e
    sput v3, Lcom/android/internal/telephony/ModemInfoResponse;->i:I

    #@40
    goto :goto_29
.end method

.method public static createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .registers 5
    .parameter "p"

    #@0
    .prologue
    .line 47
    new-instance v1, Ljava/lang/Object;

    #@2
    invoke-direct/range {v1 .. v1}, Ljava/lang/Object;-><init>()V

    #@5
    sput-object v1, Lcom/android/internal/telephony/ModemInfoResponse;->response:Ljava/lang/Object;

    #@7
    .line 49
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@a
    move-result v0

    #@b
    .line 52
    .local v0, id:I
    const-string v1, "CA"

    #@d
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isCountry(Ljava/lang/String;)Z

    #@10
    move-result v1

    #@11
    if-eqz v1, :cond_18

    #@13
    .line 53
    invoke-static {v0, p0}, Lcom/android/internal/telephony/ModemInfoResponse;->getUmtsModemInfo(ILandroid/os/Parcel;)Ljava/lang/Object;

    #@16
    move-result-object v1

    #@17
    .line 76
    :goto_17
    return-object v1

    #@18
    .line 57
    :cond_18
    const/high16 v1, 0xff

    #@1a
    and-int/2addr v1, v0

    #@1b
    sparse-switch v1, :sswitch_data_64

    #@1e
    .line 79
    new-instance v1, Ljava/lang/RuntimeException;

    #@20
    new-instance v2, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v3, "RIL_REQUEST_GET_MODEM_INFO: unsupported record. Got "

    #@27
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    invoke-static {v0}, Lcom/android/internal/telephony/ModemInfoResponse;->idToString(I)Ljava/lang/String;

    #@2e
    move-result-object v3

    #@2f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    const-string v3, " "

    #@35
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v2

    #@39
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v2

    #@3d
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@40
    throw v1

    #@41
    .line 60
    :sswitch_41
    invoke-static {v0, p0}, Lcom/android/internal/telephony/ModemInfoResponse;->getCdmaCallInfo(ILandroid/os/Parcel;)Ljava/lang/Object;

    #@44
    move-result-object v1

    #@45
    goto :goto_17

    #@46
    .line 62
    :sswitch_46
    invoke-static {v0, p0}, Lcom/android/internal/telephony/ModemInfoResponse;->getCdmaSsInfo(ILandroid/os/Parcel;)Ljava/lang/Object;

    #@49
    move-result-object v1

    #@4a
    goto :goto_17

    #@4b
    .line 64
    :sswitch_4b
    invoke-static {v0, p0}, Lcom/android/internal/telephony/ModemInfoResponse;->getCdmaPhInfo(ILandroid/os/Parcel;)Ljava/lang/Object;

    #@4e
    move-result-object v1

    #@4f
    goto :goto_17

    #@50
    .line 66
    :sswitch_50
    invoke-static {v0, p0}, Lcom/android/internal/telephony/ModemInfoResponse;->getCdmaNvInfo(ILandroid/os/Parcel;)Ljava/lang/Object;

    #@53
    move-result-object v1

    #@54
    goto :goto_17

    #@55
    .line 70
    :sswitch_55
    invoke-static {v0, p0}, Lcom/android/internal/telephony/ModemInfoResponse;->getDbgScrInfo(ILandroid/os/Parcel;)Ljava/lang/Object;

    #@58
    move-result-object v1

    #@59
    goto :goto_17

    #@5a
    .line 73
    :sswitch_5a
    invoke-static {v0, p0}, Lcom/android/internal/telephony/ModemInfoResponse;->getDsInfo(ILandroid/os/Parcel;)Ljava/lang/Object;

    #@5d
    move-result-object v1

    #@5e
    goto :goto_17

    #@5f
    .line 76
    :sswitch_5f
    invoke-static {v0, p0}, Lcom/android/internal/telephony/ModemInfoResponse;->getWBaseInfo(ILandroid/os/Parcel;)Ljava/lang/Object;

    #@62
    move-result-object v1

    #@63
    goto :goto_17

    #@64
    .line 57
    :sswitch_data_64
    .sparse-switch
        0x0 -> :sswitch_41
        0x10000 -> :sswitch_46
        0x20000 -> :sswitch_4b
        0x30000 -> :sswitch_50
        0x40000 -> :sswitch_55
        0x60000 -> :sswitch_5f
        0xb0000 -> :sswitch_5a
    .end sparse-switch
.end method

.method public static getCdmaCallInfo(ILandroid/os/Parcel;)Ljava/lang/Object;
    .registers 5
    .parameter "id"
    .parameter "p"

    #@0
    .prologue
    .line 97
    packed-switch p0, :pswitch_data_60

    #@3
    .line 115
    :pswitch_3
    new-instance v0, Ljava/lang/RuntimeException;

    #@5
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v2, "RIL_C_CALL_INFO: unsupported record. Got "

    #@c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    invoke-static {p0}, Lcom/android/internal/telephony/ModemInfoResponse;->idToString(I)Ljava/lang/String;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    const-string v2, " "

    #@1a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v1

    #@22
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@25
    throw v0

    #@26
    .line 104
    :pswitch_26
    const/4 v0, 0x1

    #@27
    new-array v0, v0, [I

    #@29
    sput-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->response:Ljava/lang/Object;

    #@2b
    .line 105
    sget-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->response:Ljava/lang/Object;

    #@2d
    check-cast v0, [I

    #@2f
    check-cast v0, [I

    #@31
    const/4 v1, 0x0

    #@32
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@35
    move-result-object v2

    #@36
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@39
    move-result v2

    #@3a
    aput v2, v0, v1

    #@3c
    .line 106
    sget-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->TAG:Ljava/lang/String;

    #@3e
    new-instance v1, Ljava/lang/StringBuilder;

    #@40
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@43
    const-string v2, "rocky :msg "

    #@45
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v1

    #@49
    sget-object v2, Lcom/android/internal/telephony/ModemInfoResponse;->response:Ljava/lang/Object;

    #@4b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v1

    #@4f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v1

    #@53
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@56
    .line 118
    :goto_56
    sget-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->response:Ljava/lang/Object;

    #@58
    return-object v0

    #@59
    .line 112
    :pswitch_59
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@5c
    move-result-object v0

    #@5d
    sput-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->response:Ljava/lang/Object;

    #@5f
    goto :goto_56

    #@60
    .line 97
    :pswitch_data_60
    .packed-switch 0x0
        :pswitch_59
        :pswitch_59
        :pswitch_3
        :pswitch_3
        :pswitch_26
        :pswitch_26
        :pswitch_3
        :pswitch_26
    .end packed-switch
.end method

.method public static getCdmaNvInfo(ILandroid/os/Parcel;)Ljava/lang/Object;
    .registers 5
    .parameter "id"
    .parameter "p"

    #@0
    .prologue
    .line 264
    sparse-switch p0, :sswitch_data_36

    #@3
    .line 285
    new-instance v0, Ljava/lang/RuntimeException;

    #@5
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v2, "RIL_C_NV_INFO: unsupported record. Got "

    #@c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    invoke-static {p0}, Lcom/android/internal/telephony/ModemInfoResponse;->idToString(I)Ljava/lang/String;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    const-string v2, " "

    #@1a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v1

    #@22
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@25
    throw v0

    #@26
    .line 275
    :sswitch_26
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@29
    move-result-object v0

    #@2a
    sput-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->response:Ljava/lang/Object;

    #@2c
    .line 288
    :goto_2c
    :sswitch_2c
    sget-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->response:Ljava/lang/Object;

    #@2e
    return-object v0

    #@2f
    .line 281
    :sswitch_2f
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@32
    move-result-object v0

    #@33
    sput-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->response:Ljava/lang/Object;

    #@35
    goto :goto_2c

    #@36
    .line 264
    :sswitch_data_36
    .sparse-switch
        0x0 -> :sswitch_2c
        0x1 -> :sswitch_2c
        0x2 -> :sswitch_2c
        0x3 -> :sswitch_2c
        0x30d0f -> :sswitch_2f
        0x3c364 -> :sswitch_26
    .end sparse-switch
.end method

.method public static getCdmaPhInfo(ILandroid/os/Parcel;)Ljava/lang/Object;
    .registers 6
    .parameter "id"
    .parameter "p"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 173
    packed-switch p0, :pswitch_data_aa

    #@4
    .line 256
    :pswitch_4
    new-instance v0, Ljava/lang/RuntimeException;

    #@6
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v2, "RIL_C_PH_INFO: unsupported record. Got "

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-static {p0}, Lcom/android/internal/telephony/ModemInfoResponse;->idToString(I)Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    const-string v2, " "

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@26
    throw v0

    #@27
    .line 215
    :pswitch_27
    const/4 v0, 0x1

    #@28
    new-array v0, v0, [I

    #@2a
    sput-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->response:Ljava/lang/Object;

    #@2c
    .line 216
    sget-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->response:Ljava/lang/Object;

    #@2e
    check-cast v0, [I

    #@30
    check-cast v0, [I

    #@32
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@35
    move-result-object v1

    #@36
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@39
    move-result v1

    #@3a
    aput v1, v0, v2

    #@3c
    .line 217
    sget-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->TAG:Ljava/lang/String;

    #@3e
    new-instance v1, Ljava/lang/StringBuilder;

    #@40
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@43
    const-string v2, "rocky :msg "

    #@45
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v1

    #@49
    sget-object v2, Lcom/android/internal/telephony/ModemInfoResponse;->response:Ljava/lang/Object;

    #@4b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v1

    #@4f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v1

    #@53
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@56
    .line 259
    :cond_56
    :goto_56
    sget-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->response:Ljava/lang/Object;

    #@58
    return-object v0

    #@59
    .line 231
    :pswitch_59
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@5c
    move-result-object v0

    #@5d
    sput-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->response:Ljava/lang/Object;

    #@5f
    goto :goto_56

    #@60
    .line 237
    :pswitch_60
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@63
    move-result-object v0

    #@64
    sput-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->addrString:Ljava/lang/String;

    #@66
    .line 238
    sget-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->addrString:Ljava/lang/String;

    #@68
    const-string v1, ","

    #@6a
    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@6d
    move-result-object v0

    #@6e
    sput-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->parts:[Ljava/lang/String;

    #@70
    .line 239
    sget-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->parts:[Ljava/lang/String;

    #@72
    array-length v0, v0

    #@73
    new-array v0, v0, [I

    #@75
    sput-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->response:Ljava/lang/Object;

    #@77
    .line 240
    sput v2, Lcom/android/internal/telephony/ModemInfoResponse;->i:I

    #@79
    :goto_79
    sget v0, Lcom/android/internal/telephony/ModemInfoResponse;->i:I

    #@7b
    sget-object v1, Lcom/android/internal/telephony/ModemInfoResponse;->parts:[Ljava/lang/String;

    #@7d
    array-length v1, v1

    #@7e
    if-ge v0, v1, :cond_56

    #@80
    .line 241
    sget-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->response:Ljava/lang/Object;

    #@82
    check-cast v0, [I

    #@84
    check-cast v0, [I

    #@86
    sget v1, Lcom/android/internal/telephony/ModemInfoResponse;->i:I

    #@88
    sget-object v2, Lcom/android/internal/telephony/ModemInfoResponse;->parts:[Ljava/lang/String;

    #@8a
    sget v3, Lcom/android/internal/telephony/ModemInfoResponse;->i:I

    #@8c
    aget-object v2, v2, v3

    #@8e
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@91
    move-result v2

    #@92
    aput v2, v0, v1

    #@94
    .line 240
    sget v0, Lcom/android/internal/telephony/ModemInfoResponse;->i:I

    #@96
    add-int/lit8 v0, v0, 0x1

    #@98
    sput v0, Lcom/android/internal/telephony/ModemInfoResponse;->i:I

    #@9a
    goto :goto_79

    #@9b
    .line 246
    :pswitch_9b
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@9e
    move-result-object v0

    #@9f
    sput-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->response:Ljava/lang/Object;

    #@a1
    goto :goto_56

    #@a2
    .line 251
    :pswitch_a2
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@a5
    move-result-object v0

    #@a6
    sput-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->response:Ljava/lang/Object;

    #@a8
    goto :goto_56

    #@a9
    .line 173
    nop

    #@aa
    :pswitch_data_aa
    .packed-switch 0x20000
        :pswitch_59
        :pswitch_4
        :pswitch_60
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_59
        :pswitch_59
        :pswitch_59
        :pswitch_59
        :pswitch_59
        :pswitch_59
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_59
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_60
        :pswitch_60
        :pswitch_27
        :pswitch_27
        :pswitch_4
        :pswitch_27
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_60
        :pswitch_59
        :pswitch_59
        :pswitch_4
        :pswitch_27
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_27
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_59
        :pswitch_59
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_9b
        :pswitch_9b
        :pswitch_a2
    .end packed-switch
.end method

.method public static getCdmaSsInfo(ILandroid/os/Parcel;)Ljava/lang/Object;
    .registers 6
    .parameter "id"
    .parameter "p"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 123
    sparse-switch p0, :sswitch_data_9c

    #@4
    .line 165
    new-instance v0, Ljava/lang/RuntimeException;

    #@6
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v2, "RIL_C_SS_INFO: unsupported record. Got "

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-static {p0}, Lcom/android/internal/telephony/ModemInfoResponse;->idToString(I)Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    const-string v2, " "

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@26
    throw v0

    #@27
    .line 146
    :sswitch_27
    const/4 v0, 0x1

    #@28
    new-array v0, v0, [I

    #@2a
    sput-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->response:Ljava/lang/Object;

    #@2c
    .line 147
    sget-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->response:Ljava/lang/Object;

    #@2e
    check-cast v0, [I

    #@30
    check-cast v0, [I

    #@32
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@35
    move-result-object v1

    #@36
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@39
    move-result v1

    #@3a
    aput v1, v0, v2

    #@3c
    .line 148
    sget-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->TAG:Ljava/lang/String;

    #@3e
    new-instance v1, Ljava/lang/StringBuilder;

    #@40
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@43
    const-string v2, "rocky :msg "

    #@45
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v1

    #@49
    sget-object v2, Lcom/android/internal/telephony/ModemInfoResponse;->response:Ljava/lang/Object;

    #@4b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v1

    #@4f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v1

    #@53
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@56
    .line 168
    :cond_56
    :goto_56
    sget-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->response:Ljava/lang/Object;

    #@58
    return-object v0

    #@59
    .line 154
    :sswitch_59
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@5c
    move-result-object v0

    #@5d
    sput-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->response:Ljava/lang/Object;

    #@5f
    goto :goto_56

    #@60
    .line 158
    :sswitch_60
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@63
    move-result-object v0

    #@64
    sput-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->addrString:Ljava/lang/String;

    #@66
    .line 159
    sget-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->addrString:Ljava/lang/String;

    #@68
    const-string v1, ","

    #@6a
    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@6d
    move-result-object v0

    #@6e
    sput-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->parts:[Ljava/lang/String;

    #@70
    .line 160
    sget-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->parts:[Ljava/lang/String;

    #@72
    array-length v0, v0

    #@73
    new-array v0, v0, [I

    #@75
    sput-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->response:Ljava/lang/Object;

    #@77
    .line 161
    sput v2, Lcom/android/internal/telephony/ModemInfoResponse;->i:I

    #@79
    :goto_79
    sget v0, Lcom/android/internal/telephony/ModemInfoResponse;->i:I

    #@7b
    sget-object v1, Lcom/android/internal/telephony/ModemInfoResponse;->parts:[Ljava/lang/String;

    #@7d
    array-length v1, v1

    #@7e
    if-ge v0, v1, :cond_56

    #@80
    .line 162
    sget-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->response:Ljava/lang/Object;

    #@82
    check-cast v0, [I

    #@84
    check-cast v0, [I

    #@86
    sget v1, Lcom/android/internal/telephony/ModemInfoResponse;->i:I

    #@88
    sget-object v2, Lcom/android/internal/telephony/ModemInfoResponse;->parts:[Ljava/lang/String;

    #@8a
    sget v3, Lcom/android/internal/telephony/ModemInfoResponse;->i:I

    #@8c
    aget-object v2, v2, v3

    #@8e
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@91
    move-result v2

    #@92
    aput v2, v0, v1

    #@94
    .line 161
    sget v0, Lcom/android/internal/telephony/ModemInfoResponse;->i:I

    #@96
    add-int/lit8 v0, v0, 0x1

    #@98
    sput v0, Lcom/android/internal/telephony/ModemInfoResponse;->i:I

    #@9a
    goto :goto_79

    #@9b
    .line 123
    nop

    #@9c
    :sswitch_data_9c
    .sparse-switch
        0x10000 -> :sswitch_27
        0x10001 -> :sswitch_27
        0x10002 -> :sswitch_27
        0x10003 -> :sswitch_27
        0x10004 -> :sswitch_27
        0x10005 -> :sswitch_27
        0x10006 -> :sswitch_59
        0x10007 -> :sswitch_59
        0x1000c -> :sswitch_60
        0x1000f -> :sswitch_27
        0x10016 -> :sswitch_59
        0x10017 -> :sswitch_59
        0x100ca -> :sswitch_60
    .end sparse-switch
.end method

.method public static getDbgScrInfo(ILandroid/os/Parcel;)Ljava/lang/Object;
    .registers 6
    .parameter "id"
    .parameter "p"

    #@0
    .prologue
    .line 298
    packed-switch p0, :pswitch_data_6c

    #@3
    .line 337
    new-instance v0, Ljava/lang/RuntimeException;

    #@5
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v2, "RIL_C_DBGSCR_INFO: unsupported record. Got "

    #@c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    invoke-static {p0}, Lcom/android/internal/telephony/ModemInfoResponse;->idToString(I)Ljava/lang/String;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    const-string v2, " "

    #@1a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v1

    #@22
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@25
    throw v0

    #@26
    .line 322
    :pswitch_26
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@29
    move-result-object v0

    #@2a
    sput-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->response:Ljava/lang/Object;

    #@2c
    .line 340
    :cond_2c
    sget-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->response:Ljava/lang/Object;

    #@2e
    return-object v0

    #@2f
    .line 328
    :pswitch_2f
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@32
    move-result-object v0

    #@33
    sput-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->addrString:Ljava/lang/String;

    #@35
    .line 329
    sget-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->addrString:Ljava/lang/String;

    #@37
    const-string v1, ","

    #@39
    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@3c
    move-result-object v0

    #@3d
    sput-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->parts:[Ljava/lang/String;

    #@3f
    .line 330
    sget-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->parts:[Ljava/lang/String;

    #@41
    array-length v0, v0

    #@42
    new-array v0, v0, [I

    #@44
    sput-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->response:Ljava/lang/Object;

    #@46
    .line 331
    const/4 v0, 0x0

    #@47
    sput v0, Lcom/android/internal/telephony/ModemInfoResponse;->i:I

    #@49
    :goto_49
    sget v0, Lcom/android/internal/telephony/ModemInfoResponse;->i:I

    #@4b
    sget-object v1, Lcom/android/internal/telephony/ModemInfoResponse;->parts:[Ljava/lang/String;

    #@4d
    array-length v1, v1

    #@4e
    if-ge v0, v1, :cond_2c

    #@50
    .line 332
    sget-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->response:Ljava/lang/Object;

    #@52
    check-cast v0, [I

    #@54
    check-cast v0, [I

    #@56
    sget v1, Lcom/android/internal/telephony/ModemInfoResponse;->i:I

    #@58
    sget-object v2, Lcom/android/internal/telephony/ModemInfoResponse;->parts:[Ljava/lang/String;

    #@5a
    sget v3, Lcom/android/internal/telephony/ModemInfoResponse;->i:I

    #@5c
    aget-object v2, v2, v3

    #@5e
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@61
    move-result v2

    #@62
    aput v2, v0, v1

    #@64
    .line 331
    sget v0, Lcom/android/internal/telephony/ModemInfoResponse;->i:I

    #@66
    add-int/lit8 v0, v0, 0x1

    #@68
    sput v0, Lcom/android/internal/telephony/ModemInfoResponse;->i:I

    #@6a
    goto :goto_49

    #@6b
    .line 298
    nop

    #@6c
    :pswitch_data_6c
    .packed-switch 0x40000
        :pswitch_26
        :pswitch_26
        :pswitch_26
        :pswitch_26
        :pswitch_2f
        :pswitch_26
        :pswitch_26
        :pswitch_26
        :pswitch_26
        :pswitch_26
        :pswitch_26
        :pswitch_26
        :pswitch_26
        :pswitch_26
        :pswitch_26
    .end packed-switch
.end method

.method public static getDsInfo(ILandroid/os/Parcel;)Ljava/lang/Object;
    .registers 5
    .parameter "id"
    .parameter "p"

    #@0
    .prologue
    .line 345
    packed-switch p0, :pswitch_data_46

    #@3
    .line 371
    :pswitch_3
    new-instance v0, Ljava/lang/RuntimeException;

    #@5
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v2, "RIL_C_DBGSCR_INFO: unsupported record. Got "

    #@c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    invoke-static {p0}, Lcom/android/internal/telephony/ModemInfoResponse;->idToString(I)Ljava/lang/String;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    const-string v2, " "

    #@1a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v1

    #@22
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@25
    throw v0

    #@26
    .line 360
    :pswitch_26
    const/4 v0, 0x1

    #@27
    new-array v0, v0, [I

    #@29
    sput-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->response:Ljava/lang/Object;

    #@2b
    .line 361
    sget-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->response:Ljava/lang/Object;

    #@2d
    check-cast v0, [I

    #@2f
    check-cast v0, [I

    #@31
    const/4 v1, 0x0

    #@32
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@35
    move-result-object v2

    #@36
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@39
    move-result v2

    #@3a
    aput v2, v0, v1

    #@3c
    .line 374
    :goto_3c
    sget-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->response:Ljava/lang/Object;

    #@3e
    return-object v0

    #@3f
    .line 366
    :pswitch_3f
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@42
    move-result-object v0

    #@43
    sput-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->response:Ljava/lang/Object;

    #@45
    goto :goto_3c

    #@46
    .line 345
    :pswitch_data_46
    .packed-switch 0xb0000
        :pswitch_26
        :pswitch_26
        :pswitch_26
        :pswitch_26
        :pswitch_26
        :pswitch_26
        :pswitch_26
        :pswitch_26
        :pswitch_26
        :pswitch_26
        :pswitch_26
        :pswitch_26
        :pswitch_26
        :pswitch_26
        :pswitch_3
        :pswitch_3f
    .end packed-switch
.end method

.method private static getUmtsModemInfo(ILandroid/os/Parcel;)Ljava/lang/Object;
    .registers 5
    .parameter "id"
    .parameter "p"

    #@0
    .prologue
    .line 494
    packed-switch p0, :pswitch_data_5a

    #@3
    .line 502
    new-instance v0, Ljava/lang/RuntimeException;

    #@5
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v2, "getUmtsCallInfo: unsupported record. Got "

    #@c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    invoke-static {p0}, Lcom/android/internal/telephony/ModemInfoResponse;->idToString(I)Ljava/lang/String;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    const-string v2, " "

    #@1a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v1

    #@22
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@25
    throw v0

    #@26
    .line 496
    :pswitch_26
    const/4 v0, 0x1

    #@27
    new-array v0, v0, [I

    #@29
    sput-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->response:Ljava/lang/Object;

    #@2b
    .line 497
    sget-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->response:Ljava/lang/Object;

    #@2d
    check-cast v0, [I

    #@2f
    check-cast v0, [I

    #@31
    const/4 v1, 0x0

    #@32
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@35
    move-result-object v2

    #@36
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@39
    move-result v2

    #@3a
    aput v2, v0, v1

    #@3c
    .line 498
    sget-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->TAG:Ljava/lang/String;

    #@3e
    new-instance v1, Ljava/lang/StringBuilder;

    #@40
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@43
    const-string v2, "getUmtsCallInfo :"

    #@45
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v1

    #@49
    sget-object v2, Lcom/android/internal/telephony/ModemInfoResponse;->response:Ljava/lang/Object;

    #@4b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v1

    #@4f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v1

    #@53
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@56
    .line 505
    sget-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->response:Ljava/lang/Object;

    #@58
    return-object v0

    #@59
    .line 494
    nop

    #@5a
    :pswitch_data_5a
    .packed-switch 0x14
        :pswitch_26
    .end packed-switch
.end method

.method public static getWBaseInfo(ILandroid/os/Parcel;)Ljava/lang/Object;
    .registers 5
    .parameter "id"
    .parameter "p"

    #@0
    .prologue
    .line 379
    sparse-switch p0, :sswitch_data_54

    #@3
    .line 451
    new-instance v0, Ljava/lang/RuntimeException;

    #@5
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v2, "RIL_C_DBGSCR_INFO: unsupported record. Got "

    #@c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    invoke-static {p0}, Lcom/android/internal/telephony/ModemInfoResponse;->idToString(I)Ljava/lang/String;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    const-string v2, " "

    #@1a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v1

    #@22
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@25
    throw v0

    #@26
    .line 436
    :sswitch_26
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@29
    move-result-object v0

    #@2a
    sput-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->response:Ljava/lang/Object;

    #@2c
    .line 454
    :goto_2c
    sget-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->response:Ljava/lang/Object;

    #@2e
    return-object v0

    #@2f
    .line 445
    :sswitch_2f
    sget-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->TAG:Ljava/lang/String;

    #@31
    new-instance v1, Ljava/lang/StringBuilder;

    #@33
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@36
    const-string v2, "id : "

    #@38
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v1

    #@3c
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v1

    #@40
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v1

    #@44
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@47
    .line 446
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@4a
    move-result-object v0

    #@4b
    const-string v1, "!"

    #@4d
    invoke-static {v0, v1}, Lcom/android/internal/telephony/ModemInfoResponse;->convert_String_To_IntArray(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    #@50
    move-result-object v0

    #@51
    sput-object v0, Lcom/android/internal/telephony/ModemInfoResponse;->response:Ljava/lang/Object;

    #@53
    goto :goto_2c

    #@54
    .line 379
    :sswitch_data_54
    .sparse-switch
        0x60000 -> :sswitch_26
        0x60001 -> :sswitch_26
        0x60002 -> :sswitch_26
        0x60003 -> :sswitch_26
        0x60004 -> :sswitch_26
        0x60005 -> :sswitch_26
        0x60006 -> :sswitch_26
        0x60007 -> :sswitch_26
        0x60008 -> :sswitch_26
        0x60009 -> :sswitch_26
        0x6000a -> :sswitch_26
        0x6000b -> :sswitch_26
        0x6000c -> :sswitch_2f
        0x6000d -> :sswitch_2f
        0x6000e -> :sswitch_26
        0x6000f -> :sswitch_26
        0x60010 -> :sswitch_26
        0x60011 -> :sswitch_26
        0x60012 -> :sswitch_26
        0x60013 -> :sswitch_26
        0x60014 -> :sswitch_26
        0x60015 -> :sswitch_26
        0x60016 -> :sswitch_26
        0x60017 -> :sswitch_26
        0x60019 -> :sswitch_26
        0x6001c -> :sswitch_2f
        0x6001f -> :sswitch_26
        0x60024 -> :sswitch_26
        0x60027 -> :sswitch_26
        0x60028 -> :sswitch_2f
        0x6002b -> :sswitch_26
        0x6002c -> :sswitch_26
        0x6002e -> :sswitch_26
        0x6002f -> :sswitch_26
        0x60031 -> :sswitch_26
        0x60034 -> :sswitch_26
        0x60035 -> :sswitch_26
        0x60036 -> :sswitch_26
        0x60037 -> :sswitch_26
        0x6dead -> :sswitch_26
    .end sparse-switch
.end method

.method public static idToString(I)Ljava/lang/String;
    .registers 2
    .parameter "id"

    #@0
    .prologue
    .line 85
    sparse-switch p0, :sswitch_data_16

    #@3
    .line 91
    const-string v0, "<unknown record>"

    #@5
    :goto_5
    return-object v0

    #@6
    .line 86
    :sswitch_6
    const-string v0, "RIL_C_CALL_INFO"

    #@8
    goto :goto_5

    #@9
    .line 87
    :sswitch_9
    const-string v0, "RIL_C_SS_INFO"

    #@b
    goto :goto_5

    #@c
    .line 88
    :sswitch_c
    const-string v0, "RIL_C_PH_INFO"

    #@e
    goto :goto_5

    #@f
    .line 89
    :sswitch_f
    const-string v0, "RIL_C_NV_INFO"

    #@11
    goto :goto_5

    #@12
    .line 90
    :sswitch_12
    const-string v0, "RIL_C_DBGSCR_INFO"

    #@14
    goto :goto_5

    #@15
    .line 85
    nop

    #@16
    :sswitch_data_16
    .sparse-switch
        0x0 -> :sswitch_6
        0x10000 -> :sswitch_9
        0x20000 -> :sswitch_c
        0x30000 -> :sswitch_f
        0x40000 -> :sswitch_12
    .end sparse-switch
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 488
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 489
    .local v0, result:Ljava/lang/String;
    return-object v0
.end method
