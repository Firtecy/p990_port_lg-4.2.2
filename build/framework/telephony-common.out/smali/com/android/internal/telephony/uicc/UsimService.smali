.class public Lcom/android/internal/telephony/uicc/UsimService;
.super Ljava/lang/Object;
.source "UsimService.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "LGE_USIM"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mUsimInfo:Lcom/android/internal/telephony/uicc/IUsimInfo;

.field private uid:J


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 53
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/uicc/UsimService;-><init>(Landroid/content/Context;)V

    #@4
    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "c"

    #@0
    .prologue
    .line 44
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 45
    iput-object p1, p0, Lcom/android/internal/telephony/uicc/UsimService;->mContext:Landroid/content/Context;

    #@5
    .line 46
    const-string v0, "iusiminfo"

    #@7
    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@a
    move-result-object v0

    #@b
    invoke-static {v0}, Lcom/android/internal/telephony/uicc/IUsimInfo$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/uicc/IUsimInfo;

    #@e
    move-result-object v0

    #@f
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/UsimService;->mUsimInfo:Lcom/android/internal/telephony/uicc/IUsimInfo;

    #@11
    .line 48
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    #@14
    move-result v0

    #@15
    int-to-long v0, v0

    #@16
    iput-wide v0, p0, Lcom/android/internal/telephony/uicc/UsimService;->uid:J

    #@18
    .line 49
    return-void
.end method

.method private resetBinder()Z
    .registers 5

    #@0
    .prologue
    .line 58
    const/4 v0, 0x6

    #@1
    .line 60
    .local v0, bucket:I
    const-string v2, "LGE_USIM"

    #@3
    const-string v3, "[UsimService] reset binder from RemoteException"

    #@5
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 62
    :cond_8
    const-string v2, "iusiminfo"

    #@a
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@d
    move-result-object v2

    #@e
    invoke-static {v2}, Lcom/android/internal/telephony/uicc/IUsimInfo$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/uicc/IUsimInfo;

    #@11
    move-result-object v2

    #@12
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/UsimService;->mUsimInfo:Lcom/android/internal/telephony/uicc/IUsimInfo;

    #@14
    .line 65
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/UsimService;->mUsimInfo:Lcom/android/internal/telephony/uicc/IUsimInfo;

    #@16
    if-eqz v2, :cond_21

    #@18
    .line 66
    const-string v2, "LGE_USIM"

    #@1a
    const-string v3, "[UsimService] reset binder ==> OK"

    #@1c
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 67
    const/4 v2, 0x1

    #@20
    .line 84
    :goto_20
    return v2

    #@21
    .line 70
    :cond_21
    add-int/lit8 v0, v0, -0x1

    #@23
    if-nez v0, :cond_31

    #@25
    .line 81
    :goto_25
    const/4 v2, 0x0

    #@26
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/UsimService;->mUsimInfo:Lcom/android/internal/telephony/uicc/IUsimInfo;

    #@28
    .line 82
    const-string v2, "LGE_USIM"

    #@2a
    const-string v3, "[UsimService] reset binder ==> FAIL"

    #@2c
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 84
    const/4 v2, 0x0

    #@30
    goto :goto_20

    #@31
    .line 73
    :cond_31
    const-string v2, "LGE_USIM"

    #@33
    const-string v3, "[UsimService] reset binder ==> sleeping..."

    #@35
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 75
    const-wide/16 v2, 0x1f4

    #@3a
    :try_start_3a
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_3d
    .catch Ljava/lang/InterruptedException; {:try_start_3a .. :try_end_3d} :catch_40

    #@3d
    .line 79
    if-gtz v0, :cond_8

    #@3f
    goto :goto_25

    #@40
    .line 76
    :catch_40
    move-exception v1

    #@41
    .line 77
    .local v1, e:Ljava/lang/InterruptedException;
    goto :goto_25
.end method


# virtual methods
.method public PBMDeleteRecord(II)V
    .registers 8
    .parameter "EFdevice"
    .parameter "rec_index"

    #@0
    .prologue
    .line 445
    const-string v2, "LGE_USIM"

    #@2
    const-string v3, "[UsimService]PBMDeleteRecord()"

    #@4
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 449
    :cond_7
    const/4 v1, 0x0

    #@8
    .line 451
    .local v1, reset:Z
    :try_start_8
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/UsimService;->mUsimInfo:Lcom/android/internal/telephony/uicc/IUsimInfo;

    #@a
    iget-wide v3, p0, Lcom/android/internal/telephony/uicc/UsimService;->uid:J

    #@c
    invoke-interface {v2, v3, v4, p1, p2}, Lcom/android/internal/telephony/uicc/IUsimInfo;->PBMDeleteRecord(JII)V
    :try_end_f
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_f} :catch_12

    #@f
    .line 455
    :goto_f
    if-nez v1, :cond_7

    #@11
    .line 456
    return-void

    #@12
    .line 452
    :catch_12
    move-exception v0

    #@13
    .line 453
    .local v0, e:Ljava/lang/Throwable;
    invoke-direct {p0}, Lcom/android/internal/telephony/uicc/UsimService;->resetBinder()Z

    #@16
    move-result v1

    #@17
    goto :goto_f
.end method

.method public PBMGetInfo(I)V
    .registers 7
    .parameter "EFdevice"

    #@0
    .prologue
    .line 460
    const-string v2, "LGE_USIM"

    #@2
    const-string v3, "[UsimService]PBMGetInfo()"

    #@4
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 464
    :cond_7
    const/4 v1, 0x0

    #@8
    .line 466
    .local v1, reset:Z
    :try_start_8
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/UsimService;->mUsimInfo:Lcom/android/internal/telephony/uicc/IUsimInfo;

    #@a
    iget-wide v3, p0, Lcom/android/internal/telephony/uicc/UsimService;->uid:J

    #@c
    invoke-interface {v2, v3, v4, p1}, Lcom/android/internal/telephony/uicc/IUsimInfo;->PBMGetInfo(JI)V
    :try_end_f
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_f} :catch_12

    #@f
    .line 470
    :goto_f
    if-nez v1, :cond_7

    #@11
    .line 471
    return-void

    #@12
    .line 467
    :catch_12
    move-exception v0

    #@13
    .line 468
    .local v0, e:Ljava/lang/Throwable;
    invoke-direct {p0}, Lcom/android/internal/telephony/uicc/UsimService;->resetBinder()Z

    #@16
    move-result v1

    #@17
    goto :goto_f
.end method

.method public PBMReadRecord(II)V
    .registers 8
    .parameter "EFdevice"
    .parameter "rec_index"

    #@0
    .prologue
    .line 417
    :cond_0
    const/4 v1, 0x0

    #@1
    .line 419
    .local v1, reset:Z
    :try_start_1
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/UsimService;->mUsimInfo:Lcom/android/internal/telephony/uicc/IUsimInfo;

    #@3
    iget-wide v3, p0, Lcom/android/internal/telephony/uicc/UsimService;->uid:J

    #@5
    invoke-interface {v2, v3, v4, p1, p2}, Lcom/android/internal/telephony/uicc/IUsimInfo;->PBMReadRecord(JII)V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_8} :catch_b

    #@8
    .line 424
    :goto_8
    if-nez v1, :cond_0

    #@a
    .line 425
    return-void

    #@b
    .line 420
    :catch_b
    move-exception v0

    #@c
    .line 421
    .local v0, e:Ljava/lang/Throwable;
    const-string v2, "LGE_USIM"

    #@e
    const-string v3, "[UsimService]PBMReadRecord() call resetBinder"

    #@10
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@13
    .line 422
    invoke-direct {p0}, Lcom/android/internal/telephony/uicc/UsimService;->resetBinder()Z

    #@16
    move-result v1

    #@17
    goto :goto_8
.end method

.method public PBMWriteRecord(Lcom/android/internal/telephony/uicc/LGE_PBM_Records;)V
    .registers 7
    .parameter "RecordData"

    #@0
    .prologue
    .line 429
    const-string v2, "LGE_USIM"

    #@2
    const-string v3, "[UsimService]PBMWriteRecord()"

    #@4
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 433
    :cond_7
    const/4 v1, 0x0

    #@8
    .line 435
    .local v1, reset:Z
    :try_start_8
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/UsimService;->mUsimInfo:Lcom/android/internal/telephony/uicc/IUsimInfo;

    #@a
    iget-wide v3, p0, Lcom/android/internal/telephony/uicc/UsimService;->uid:J

    #@c
    invoke-interface {v2, v3, v4, p1}, Lcom/android/internal/telephony/uicc/IUsimInfo;->PBMWriteRecord(JLcom/android/internal/telephony/uicc/LGE_PBM_Records;)V
    :try_end_f
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_f} :catch_12

    #@f
    .line 440
    :goto_f
    if-nez v1, :cond_7

    #@11
    .line 441
    return-void

    #@12
    .line 436
    :catch_12
    move-exception v0

    #@13
    .line 437
    .local v0, e:Ljava/lang/Throwable;
    const-string v2, "LGE_USIM"

    #@15
    const-string v3, "[UsimService]PBMWriteRecord() call resetBinder"

    #@17
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 438
    invoke-direct {p0}, Lcom/android/internal/telephony/uicc/UsimService;->resetBinder()Z

    #@1d
    move-result v1

    #@1e
    goto :goto_f
.end method

.method public ReadFromSIM(I)[B
    .registers 8
    .parameter "EF_ID"

    #@0
    .prologue
    .line 118
    const-string v3, "LGE_USIM"

    #@2
    const-string v4, "[UsimService] ReadFromSIM"

    #@4
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 119
    const/4 v1, 0x0

    #@8
    .line 123
    .local v1, read_buff:[B
    :cond_8
    const/4 v2, 0x0

    #@9
    .line 125
    .local v2, reset:Z
    :try_start_9
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/UsimService;->mUsimInfo:Lcom/android/internal/telephony/uicc/IUsimInfo;

    #@b
    invoke-interface {v3, p1}, Lcom/android/internal/telephony/uicc/IUsimInfo;->ReadFromSIM(I)[B
    :try_end_e
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_e} :catch_12

    #@e
    move-result-object v1

    #@f
    .line 130
    :goto_f
    if-nez v2, :cond_8

    #@11
    .line 132
    return-object v1

    #@12
    .line 126
    :catch_12
    move-exception v0

    #@13
    .line 127
    .local v0, e:Ljava/lang/Throwable;
    const-string v3, "LGE_USIM"

    #@15
    new-instance v4, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v5, "binder is not ready yet : "

    #@1c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v4

    #@20
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v4

    #@24
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v4

    #@28
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 128
    invoke-direct {p0}, Lcom/android/internal/telephony/uicc/UsimService;->resetBinder()Z

    #@2e
    move-result v2

    #@2f
    goto :goto_f
.end method

.method public WriteToSIM(I[B)[B
    .registers 9
    .parameter "efid"
    .parameter "data_to_be_sent"

    #@0
    .prologue
    .line 137
    const-string v4, "LGE_USIM"

    #@2
    const-string v5, "[UsimService]WriteToSIM()"

    #@4
    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 139
    const/4 v3, 0x0

    #@8
    .line 140
    .local v3, ret:[B
    const/4 v0, 0x0

    #@9
    .line 143
    .local v0, data:[B
    :cond_9
    const/4 v2, 0x0

    #@a
    .line 145
    .local v2, reset:Z
    :try_start_a
    iget-object v4, p0, Lcom/android/internal/telephony/uicc/UsimService;->mUsimInfo:Lcom/android/internal/telephony/uicc/IUsimInfo;

    #@c
    invoke-interface {v4, p1, p2}, Lcom/android/internal/telephony/uicc/IUsimInfo;->WriteToSIM(I[B)[B
    :try_end_f
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_f} :catch_13

    #@f
    move-result-object v3

    #@10
    .line 149
    :goto_10
    if-nez v2, :cond_9

    #@12
    .line 151
    return-object v3

    #@13
    .line 146
    :catch_13
    move-exception v1

    #@14
    .line 147
    .local v1, e:Ljava/lang/Throwable;
    invoke-direct {p0}, Lcom/android/internal/telephony/uicc/UsimService;->resetBinder()Z

    #@17
    move-result v2

    #@18
    goto :goto_10
.end method

.method public getEfRecordSize(I)I
    .registers 7
    .parameter "efid"

    #@0
    .prologue
    .line 222
    const-string v3, "LGE_USIM"

    #@2
    const-string v4, "[UsimService] getEfRecordSize"

    #@4
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 223
    const/4 v2, 0x0

    #@8
    .line 227
    .local v2, size:I
    :cond_8
    const/4 v1, 0x0

    #@9
    .line 229
    .local v1, reset:Z
    :try_start_9
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/UsimService;->mUsimInfo:Lcom/android/internal/telephony/uicc/IUsimInfo;

    #@b
    invoke-interface {v3, p1}, Lcom/android/internal/telephony/uicc/IUsimInfo;->getEfRecordsSize(I)I
    :try_end_e
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_e} :catch_12

    #@e
    move-result v2

    #@f
    .line 233
    :goto_f
    if-nez v1, :cond_8

    #@11
    .line 235
    return v2

    #@12
    .line 230
    :catch_12
    move-exception v0

    #@13
    .line 231
    .local v0, e:Ljava/lang/Throwable;
    invoke-direct {p0}, Lcom/android/internal/telephony/uicc/UsimService;->resetBinder()Z

    #@16
    move-result v1

    #@17
    goto :goto_f
.end method

.method public getIMSI_M()Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    .line 476
    const/4 v2, 0x0

    #@1
    .line 479
    .local v2, retVal:Ljava/lang/String;
    const-string v3, "LGE_USIM"

    #@3
    const-string v4, "[UsimService] getIMSI_M()"

    #@5
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 482
    :cond_8
    const/4 v1, 0x0

    #@9
    .line 484
    .local v1, reset:Z
    :try_start_9
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/UsimService;->mUsimInfo:Lcom/android/internal/telephony/uicc/IUsimInfo;

    #@b
    invoke-interface {v3}, Lcom/android/internal/telephony/uicc/IUsimInfo;->getIMSI_M()Ljava/lang/String;
    :try_end_e
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_e} :catch_12

    #@e
    move-result-object v2

    #@f
    .line 488
    :goto_f
    if-nez v1, :cond_8

    #@11
    .line 490
    return-object v2

    #@12
    .line 485
    :catch_12
    move-exception v0

    #@13
    .line 486
    .local v0, e:Ljava/lang/Throwable;
    invoke-direct {p0}, Lcom/android/internal/telephony/uicc/UsimService;->resetBinder()Z

    #@16
    move-result v1

    #@17
    goto :goto_f
.end method

.method public getSCAddressFromIcc()Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    .line 189
    const-string v3, "LGE_USIM"

    #@2
    const-string v4, "[UsimService] getSCAddressFromIcc"

    #@4
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 190
    const/4 v2, 0x0

    #@8
    .line 194
    .local v2, scAddr:Ljava/lang/String;
    :cond_8
    const/4 v1, 0x0

    #@9
    .line 196
    .local v1, reset:Z
    :try_start_9
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/UsimService;->mUsimInfo:Lcom/android/internal/telephony/uicc/IUsimInfo;

    #@b
    invoke-interface {v3}, Lcom/android/internal/telephony/uicc/IUsimInfo;->getSCAddressFromIcc()Ljava/lang/String;
    :try_end_e
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_e} :catch_12

    #@e
    move-result-object v2

    #@f
    .line 200
    :goto_f
    if-nez v1, :cond_8

    #@11
    .line 202
    return-object v2

    #@12
    .line 197
    :catch_12
    move-exception v0

    #@13
    .line 198
    .local v0, e:Ljava/lang/Throwable;
    invoke-direct {p0}, Lcom/android/internal/telephony/uicc/UsimService;->resetBinder()Z

    #@16
    move-result v1

    #@17
    goto :goto_f
.end method

.method public getUsimIsEmpty()I
    .registers 6

    #@0
    .prologue
    .line 156
    const-string v3, "LGE_USIM"

    #@2
    const-string v4, "[UsimService]getUsimIsEmpty()"

    #@4
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 157
    const/4 v2, -0x1

    #@8
    .line 161
    .local v2, ret_val:I
    :cond_8
    const/4 v1, 0x0

    #@9
    .line 163
    .local v1, reset:Z
    :try_start_9
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/UsimService;->mUsimInfo:Lcom/android/internal/telephony/uicc/IUsimInfo;

    #@b
    invoke-interface {v3}, Lcom/android/internal/telephony/uicc/IUsimInfo;->getUsimIsEmpty()I
    :try_end_e
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_e} :catch_12

    #@e
    move-result v2

    #@f
    .line 167
    :goto_f
    if-nez v1, :cond_8

    #@11
    .line 169
    return v2

    #@12
    .line 164
    :catch_12
    move-exception v0

    #@13
    .line 165
    .local v0, e:Ljava/lang/Throwable;
    invoke-direct {p0}, Lcom/android/internal/telephony/uicc/UsimService;->resetBinder()Z

    #@16
    move-result v1

    #@17
    goto :goto_f
.end method

.method public getUsimIsSponIMSI()I
    .registers 6

    #@0
    .prologue
    .line 243
    const-string v3, "LGE_USIM"

    #@2
    const-string v4, "[UsimService]getUsimIsSponIMSI()"

    #@4
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 244
    const/4 v2, -0x1

    #@8
    .line 248
    .local v2, ret_val:I
    :cond_8
    const/4 v1, 0x0

    #@9
    .line 250
    .local v1, reset:Z
    :try_start_9
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/UsimService;->mUsimInfo:Lcom/android/internal/telephony/uicc/IUsimInfo;

    #@b
    invoke-interface {v3}, Lcom/android/internal/telephony/uicc/IUsimInfo;->getUsimIsSponIMSI()I
    :try_end_e
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_e} :catch_12

    #@e
    move-result v2

    #@f
    .line 254
    :goto_f
    if-nez v1, :cond_8

    #@11
    .line 256
    return v2

    #@12
    .line 251
    :catch_12
    move-exception v0

    #@13
    .line 252
    .local v0, e:Ljava/lang/Throwable;
    invoke-direct {p0}, Lcom/android/internal/telephony/uicc/UsimService;->resetBinder()Z

    #@16
    move-result v1

    #@17
    goto :goto_f
.end method

.method public getUsimType()I
    .registers 5

    #@0
    .prologue
    .line 174
    const-string v1, "LGE_USIM"

    #@2
    const-string v2, "[UsimService]getUsimType()"

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 176
    const-string v1, "gsm.sim.type"

    #@9
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@c
    move-result-object v0

    #@d
    .line 177
    .local v0, gsmSimType:Ljava/lang/String;
    const-string v1, "LGE_USIM"

    #@f
    new-instance v2, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v3, "getUsimType() "

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 178
    const-string v1, "skt"

    #@27
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2a
    move-result v1

    #@2b
    if-eqz v1, :cond_2f

    #@2d
    const/4 v1, 0x1

    #@2e
    .line 184
    :goto_2e
    return v1

    #@2f
    .line 179
    :cond_2f
    const-string v1, "kt"

    #@31
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@34
    move-result v1

    #@35
    if-eqz v1, :cond_39

    #@37
    const/4 v1, 0x2

    #@38
    goto :goto_2e

    #@39
    .line 180
    :cond_39
    const-string v1, "lgu"

    #@3b
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3e
    move-result v1

    #@3f
    if-eqz v1, :cond_43

    #@41
    const/4 v1, 0x5

    #@42
    goto :goto_2e

    #@43
    .line 181
    :cond_43
    const-string v1, "test"

    #@45
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@48
    move-result v1

    #@49
    if-eqz v1, :cond_4d

    #@4b
    const/4 v1, 0x4

    #@4c
    goto :goto_2e

    #@4d
    .line 182
    :cond_4d
    const-string v1, "unknown"

    #@4f
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@52
    move-result v1

    #@53
    if-eqz v1, :cond_57

    #@55
    const/4 v1, 0x3

    #@56
    goto :goto_2e

    #@57
    .line 184
    :cond_57
    const/4 v1, 0x0

    #@58
    goto :goto_2e
.end method

.method public readEF_Mdn(I)Ljava/lang/String;
    .registers 9
    .parameter "EF_ID"

    #@0
    .prologue
    .line 326
    const-string v4, "LGE_USIM"

    #@2
    const-string v5, "[UsimService]readEF_Mdn()"

    #@4
    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 327
    const/4 v2, 0x0

    #@8
    .line 328
    .local v2, mMdn:[B
    const/4 v1, 0x0

    #@9
    .line 332
    .local v1, mEFMdn:Ljava/lang/String;
    :cond_9
    const/4 v3, 0x0

    #@a
    .line 334
    .local v3, reset:Z
    :try_start_a
    iget-object v4, p0, Lcom/android/internal/telephony/uicc/UsimService;->mUsimInfo:Lcom/android/internal/telephony/uicc/IUsimInfo;

    #@c
    invoke-interface {v4, p1}, Lcom/android/internal/telephony/uicc/IUsimInfo;->ReadFromSIM(I)[B

    #@f
    move-result-object v2

    #@10
    .line 335
    invoke-static {v2}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->decodeMdn([B)Ljava/lang/String;
    :try_end_13
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_13} :catch_17

    #@13
    move-result-object v1

    #@14
    .line 340
    :goto_14
    if-nez v3, :cond_9

    #@16
    .line 342
    return-object v1

    #@17
    .line 336
    :catch_17
    move-exception v0

    #@18
    .line 337
    .local v0, e:Ljava/lang/Throwable;
    const-string v4, "LGE_USIM"

    #@1a
    new-instance v5, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v6, "binder is not ready yet : "

    #@21
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v5

    #@25
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v5

    #@29
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v5

    #@2d
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    .line 338
    invoke-direct {p0}, Lcom/android/internal/telephony/uicc/UsimService;->resetBinder()Z

    #@33
    move-result v3

    #@34
    goto :goto_14
.end method

.method public readEF_Roaming(I)Ljava/lang/String;
    .registers 9
    .parameter "EF_ID"

    #@0
    .prologue
    .line 367
    const-string v4, "LGE_USIM"

    #@2
    const-string v5, "[UsimService]readEF_Roaming()"

    #@4
    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 368
    const/4 v2, 0x0

    #@8
    .line 369
    .local v2, mRoaming:[B
    const/4 v1, 0x0

    #@9
    .line 373
    .local v1, mEFRoaming:Ljava/lang/String;
    :cond_9
    const/4 v3, 0x0

    #@a
    .line 375
    .local v3, reset:Z
    :try_start_a
    iget-object v4, p0, Lcom/android/internal/telephony/uicc/UsimService;->mUsimInfo:Lcom/android/internal/telephony/uicc/IUsimInfo;

    #@c
    invoke-interface {v4, p1}, Lcom/android/internal/telephony/uicc/IUsimInfo;->ReadFromSIM(I)[B

    #@f
    move-result-object v2

    #@10
    .line 376
    if-eqz v2, :cond_16

    #@12
    .line 377
    invoke-static {v2}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;
    :try_end_15
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_15} :catch_19

    #@15
    move-result-object v1

    #@16
    .line 383
    :cond_16
    :goto_16
    if-nez v3, :cond_9

    #@18
    .line 385
    return-object v1

    #@19
    .line 379
    :catch_19
    move-exception v0

    #@1a
    .line 380
    .local v0, e:Ljava/lang/Throwable;
    const-string v4, "LGE_USIM"

    #@1c
    new-instance v5, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v6, "binder is not ready yet : "

    #@23
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v5

    #@27
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v5

    #@2b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v5

    #@2f
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 381
    invoke-direct {p0}, Lcom/android/internal/telephony/uicc/UsimService;->resetBinder()Z

    #@35
    move-result v3

    #@36
    goto :goto_16
.end method

.method public registerCallback(Lcom/android/internal/telephony/uicc/IUsimInfoCallback;)V
    .registers 7
    .parameter "cb"

    #@0
    .prologue
    .line 92
    :cond_0
    const/4 v1, 0x0

    #@1
    .line 94
    .local v1, reset:Z
    :try_start_1
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/UsimService;->mUsimInfo:Lcom/android/internal/telephony/uicc/IUsimInfo;

    #@3
    iget-wide v3, p0, Lcom/android/internal/telephony/uicc/UsimService;->uid:J

    #@5
    invoke-interface {v2, v3, v4, p1}, Lcom/android/internal/telephony/uicc/IUsimInfo;->registerCallback(JLcom/android/internal/telephony/uicc/IUsimInfoCallback;)V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_8} :catch_b

    #@8
    .line 98
    :goto_8
    if-nez v1, :cond_0

    #@a
    .line 99
    return-void

    #@b
    .line 95
    :catch_b
    move-exception v0

    #@c
    .line 96
    .local v0, e:Ljava/lang/Throwable;
    invoke-direct {p0}, Lcom/android/internal/telephony/uicc/UsimService;->resetBinder()Z

    #@f
    move-result v1

    #@10
    goto :goto_8
.end method

.method public sendChangeToHomeIMSI()V
    .registers 5

    #@0
    .prologue
    .line 291
    const-string v2, "LGE_USIM"

    #@2
    const-string v3, "[UsimService]sendChangeToHomeIMSI()"

    #@4
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 295
    :cond_7
    const/4 v1, 0x0

    #@8
    .line 297
    .local v1, reset:Z
    :try_start_8
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/UsimService;->mUsimInfo:Lcom/android/internal/telephony/uicc/IUsimInfo;

    #@a
    invoke-interface {v2}, Lcom/android/internal/telephony/uicc/IUsimInfo;->sendChangeToHomeIMSI()V
    :try_end_d
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_d} :catch_10

    #@d
    .line 301
    :goto_d
    if-nez v1, :cond_7

    #@f
    .line 302
    return-void

    #@10
    .line 298
    :catch_10
    move-exception v0

    #@11
    .line 299
    .local v0, e:Ljava/lang/Throwable;
    invoke-direct {p0}, Lcom/android/internal/telephony/uicc/UsimService;->resetBinder()Z

    #@14
    move-result v1

    #@15
    goto :goto_d
.end method

.method public sendChangeToSponIMSI()V
    .registers 5

    #@0
    .prologue
    .line 261
    const-string v2, "LGE_USIM"

    #@2
    const-string v3, "[UsimService]sendChangeToSponIMSI()"

    #@4
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 265
    :cond_7
    const/4 v1, 0x0

    #@8
    .line 267
    .local v1, reset:Z
    :try_start_8
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/UsimService;->mUsimInfo:Lcom/android/internal/telephony/uicc/IUsimInfo;

    #@a
    invoke-interface {v2}, Lcom/android/internal/telephony/uicc/IUsimInfo;->sendChangeToSponIMSI()V
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_d} :catch_10

    #@d
    .line 271
    :goto_d
    if-nez v1, :cond_7

    #@f
    .line 272
    return-void

    #@10
    .line 268
    :catch_10
    move-exception v0

    #@11
    .line 269
    .local v0, e:Landroid/os/RemoteException;
    invoke-direct {p0}, Lcom/android/internal/telephony/uicc/UsimService;->resetBinder()Z

    #@14
    move-result v1

    #@15
    goto :goto_d
.end method

.method public sendUpdatePLMN()V
    .registers 5

    #@0
    .prologue
    .line 276
    const-string v2, "LGE_USIM"

    #@2
    const-string v3, "[UsimService]sendUpdatePLMN()"

    #@4
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 280
    :cond_7
    const/4 v1, 0x0

    #@8
    .line 282
    .local v1, reset:Z
    :try_start_8
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/UsimService;->mUsimInfo:Lcom/android/internal/telephony/uicc/IUsimInfo;

    #@a
    invoke-interface {v2}, Lcom/android/internal/telephony/uicc/IUsimInfo;->sendUpdatePLMN()V
    :try_end_d
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_d} :catch_10

    #@d
    .line 286
    :goto_d
    if-nez v1, :cond_7

    #@f
    .line 287
    return-void

    #@10
    .line 283
    :catch_10
    move-exception v0

    #@11
    .line 284
    .local v0, e:Ljava/lang/Throwable;
    invoke-direct {p0}, Lcom/android/internal/telephony/uicc/UsimService;->resetBinder()Z

    #@14
    move-result v1

    #@15
    goto :goto_d
.end method

.method public setSCAddressToIcc(Ljava/lang/String;)V
    .registers 6
    .parameter "scAddr"

    #@0
    .prologue
    .line 207
    const-string v2, "LGE_USIM"

    #@2
    const-string v3, "[UsimService] setSCAddressToIcc"

    #@4
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 211
    :cond_7
    const/4 v1, 0x0

    #@8
    .line 213
    .local v1, reset:Z
    :try_start_8
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/UsimService;->mUsimInfo:Lcom/android/internal/telephony/uicc/IUsimInfo;

    #@a
    invoke-interface {v2, p1}, Lcom/android/internal/telephony/uicc/IUsimInfo;->setSCAddressToIcc(Ljava/lang/String;)V
    :try_end_d
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_d} :catch_10

    #@d
    .line 217
    :goto_d
    if-nez v1, :cond_7

    #@f
    .line 218
    return-void

    #@10
    .line 214
    :catch_10
    move-exception v0

    #@11
    .line 215
    .local v0, e:Ljava/lang/Throwable;
    invoke-direct {p0}, Lcom/android/internal/telephony/uicc/UsimService;->resetBinder()Z

    #@14
    move-result v1

    #@15
    goto :goto_d
.end method

.method public startOtaService()I
    .registers 6

    #@0
    .prologue
    .line 309
    const-string v3, "LGE_USIM"

    #@2
    const-string v4, "[UsimService]startOtaService()"

    #@4
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 311
    const/4 v2, -0x1

    #@8
    .line 314
    .local v2, ret_val:I
    :cond_8
    const/4 v1, 0x0

    #@9
    .line 316
    .local v1, reset:Z
    :try_start_9
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/UsimService;->mUsimInfo:Lcom/android/internal/telephony/uicc/IUsimInfo;

    #@b
    invoke-interface {v3}, Lcom/android/internal/telephony/uicc/IUsimInfo;->startOtaService()I
    :try_end_e
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_e} :catch_12

    #@e
    move-result v2

    #@f
    .line 320
    :goto_f
    if-nez v1, :cond_8

    #@11
    .line 321
    return v2

    #@12
    .line 317
    :catch_12
    move-exception v0

    #@13
    .line 318
    .local v0, e:Ljava/lang/Throwable;
    invoke-direct {p0}, Lcom/android/internal/telephony/uicc/UsimService;->resetBinder()Z

    #@16
    move-result v1

    #@17
    goto :goto_f
.end method

.method public unregisterCallback(Lcom/android/internal/telephony/uicc/IUsimInfoCallback;)V
    .registers 7
    .parameter "cb"

    #@0
    .prologue
    .line 106
    :cond_0
    const/4 v1, 0x0

    #@1
    .line 108
    .local v1, reset:Z
    :try_start_1
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/UsimService;->mUsimInfo:Lcom/android/internal/telephony/uicc/IUsimInfo;

    #@3
    iget-wide v3, p0, Lcom/android/internal/telephony/uicc/UsimService;->uid:J

    #@5
    invoke-interface {v2, v3, v4, p1}, Lcom/android/internal/telephony/uicc/IUsimInfo;->unregisterCallback(JLcom/android/internal/telephony/uicc/IUsimInfoCallback;)V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_8} :catch_b

    #@8
    .line 112
    :goto_8
    if-nez v1, :cond_0

    #@a
    .line 113
    return-void

    #@b
    .line 109
    :catch_b
    move-exception v0

    #@c
    .line 110
    .local v0, e:Ljava/lang/Throwable;
    invoke-direct {p0}, Lcom/android/internal/telephony/uicc/UsimService;->resetBinder()Z

    #@f
    move-result v1

    #@10
    goto :goto_8
.end method

.method public writeEF_Mdn(Ljava/lang/String;)V
    .registers 7
    .parameter "mdn"

    #@0
    .prologue
    .line 347
    const-string v3, "LGE_USIM"

    #@2
    const-string v4, "[UsimService]writeEF_Mdn()"

    #@4
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 348
    const/4 v1, 0x0

    #@8
    .line 352
    .local v1, rawData:[B
    :cond_8
    const/4 v2, 0x0

    #@9
    .line 354
    .local v2, reset:Z
    if-eqz p1, :cond_13

    #@b
    :try_start_b
    const-string v3, ""

    #@d
    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10
    move-result v3

    #@11
    if-eqz v3, :cond_23

    #@13
    .line 355
    :cond_13
    const-string v3, "ffffffffffffffffffffff"

    #@15
    invoke-static {v3}, Lcom/android/internal/telephony/uicc/IccUtils;->hexStringToBytes(Ljava/lang/String;)[B

    #@18
    move-result-object v1

    #@19
    .line 358
    :goto_19
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/UsimService;->mUsimInfo:Lcom/android/internal/telephony/uicc/IUsimInfo;

    #@1b
    const/16 v4, 0x6f44

    #@1d
    invoke-interface {v3, v4, v1}, Lcom/android/internal/telephony/uicc/IUsimInfo;->WriteToSIM(I[B)[B

    #@20
    .line 362
    :goto_20
    if-nez v2, :cond_8

    #@22
    .line 363
    return-void

    #@23
    .line 357
    :cond_23
    invoke-static {p1}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->encodeMdn(Ljava/lang/String;)[B
    :try_end_26
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_26} :catch_28

    #@26
    move-result-object v1

    #@27
    goto :goto_19

    #@28
    .line 359
    :catch_28
    move-exception v0

    #@29
    .line 360
    .local v0, e:Ljava/lang/Throwable;
    invoke-direct {p0}, Lcom/android/internal/telephony/uicc/UsimService;->resetBinder()Z

    #@2c
    move-result v2

    #@2d
    goto :goto_20
.end method

.method public writeEF_Roaming(Ljava/lang/String;)V
    .registers 7
    .parameter "roaming"

    #@0
    .prologue
    .line 390
    const-string v3, "LGE_USIM"

    #@2
    const-string v4, "[UsimService]writeEF_Roaming()"

    #@4
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 391
    const/4 v1, 0x0

    #@8
    .line 395
    .local v1, rawData:[B
    :cond_8
    const/4 v2, 0x0

    #@9
    .line 397
    .local v2, reset:Z
    if-eqz p1, :cond_13

    #@b
    :try_start_b
    const-string v3, ""

    #@d
    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10
    move-result v3

    #@11
    if-eqz v3, :cond_23

    #@13
    .line 398
    :cond_13
    const-string v3, "00"

    #@15
    invoke-static {v3}, Lcom/android/internal/telephony/uicc/IccUtils;->hexStringToBytes(Ljava/lang/String;)[B

    #@18
    move-result-object v1

    #@19
    .line 401
    :goto_19
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/UsimService;->mUsimInfo:Lcom/android/internal/telephony/uicc/IUsimInfo;

    #@1b
    const/16 v4, 0x2f50

    #@1d
    invoke-interface {v3, v4, v1}, Lcom/android/internal/telephony/uicc/IUsimInfo;->WriteToSIM(I[B)[B

    #@20
    .line 405
    :goto_20
    if-nez v2, :cond_8

    #@22
    .line 406
    return-void

    #@23
    .line 400
    :cond_23
    invoke-static {p1}, Lcom/android/internal/telephony/uicc/IccUtils;->hexStringToBytes(Ljava/lang/String;)[B
    :try_end_26
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_26} :catch_28

    #@26
    move-result-object v1

    #@27
    goto :goto_19

    #@28
    .line 402
    :catch_28
    move-exception v0

    #@29
    .line 403
    .local v0, e:Ljava/lang/Throwable;
    invoke-direct {p0}, Lcom/android/internal/telephony/uicc/UsimService;->resetBinder()Z

    #@2c
    move-result v2

    #@2d
    goto :goto_20
.end method
