.class public Lcom/android/internal/telephony/uicc/SpnOverride;
.super Ljava/lang/Object;
.source "SpnOverride.java"


# static fields
.field static final LOG_TAG:Ljava/lang/String; = "GSM"

.field static final PARTNER_SPN_OVERRIDE_PATH:Ljava/lang/String; = "etc/spn-conf.xml"


# instance fields
.field private CarrierSpnMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 41
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 42
    new-instance v0, Ljava/util/HashMap;

    #@5
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/SpnOverride;->CarrierSpnMap:Ljava/util/HashMap;

    #@a
    .line 43
    invoke-direct {p0}, Lcom/android/internal/telephony/uicc/SpnOverride;->loadSpnOverrides()V

    #@d
    .line 44
    return-void
.end method

.method private loadSpnOverrides()V
    .registers 11

    #@0
    .prologue
    .line 57
    new-instance v5, Ljava/io/File;

    #@2
    invoke-static {}, Landroid/os/Environment;->getRootDirectory()Ljava/io/File;

    #@5
    move-result-object v7

    #@6
    const-string v8, "etc/spn-conf.xml"

    #@8
    invoke-direct {v5, v7, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@b
    .line 61
    .local v5, spnFile:Ljava/io/File;
    :try_start_b
    new-instance v6, Ljava/io/FileReader;

    #@d
    invoke-direct {v6, v5}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V
    :try_end_10
    .catch Ljava/io/FileNotFoundException; {:try_start_b .. :try_end_10} :catch_2c

    #@10
    .line 69
    .local v6, spnReader:Ljava/io/FileReader;
    :try_start_10
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    #@13
    move-result-object v4

    #@14
    .line 70
    .local v4, parser:Lorg/xmlpull/v1/XmlPullParser;
    invoke-interface {v4, v6}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    #@17
    .line 72
    const-string v7, "spnOverrides"

    #@19
    invoke-static {v4, v7}, Lcom/android/internal/util/XmlUtils;->beginDocument(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V

    #@1c
    .line 75
    :goto_1c
    invoke-static {v4}, Lcom/android/internal/util/XmlUtils;->nextElement(Lorg/xmlpull/v1/XmlPullParser;)V

    #@1f
    .line 77
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    .line 78
    .local v2, name:Ljava/lang/String;
    const-string v7, "spnOverride"

    #@25
    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_28
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_10 .. :try_end_28} :catch_6a
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_28} :catch_84

    #@28
    move-result v7

    #@29
    if-nez v7, :cond_56

    #@2b
    .line 92
    .end local v2           #name:Ljava/lang/String;
    .end local v4           #parser:Lorg/xmlpull/v1/XmlPullParser;
    .end local v6           #spnReader:Ljava/io/FileReader;
    :goto_2b
    return-void

    #@2c
    .line 62
    :catch_2c
    move-exception v1

    #@2d
    .line 63
    .local v1, e:Ljava/io/FileNotFoundException;
    const-string v7, "GSM"

    #@2f
    new-instance v8, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v9, "Can\'t open "

    #@36
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v8

    #@3a
    invoke-static {}, Landroid/os/Environment;->getRootDirectory()Ljava/io/File;

    #@3d
    move-result-object v9

    #@3e
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v8

    #@42
    const-string v9, "/"

    #@44
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v8

    #@48
    const-string v9, "etc/spn-conf.xml"

    #@4a
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v8

    #@4e
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@51
    move-result-object v8

    #@52
    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@55
    goto :goto_2b

    #@56
    .line 82
    .end local v1           #e:Ljava/io/FileNotFoundException;
    .restart local v2       #name:Ljava/lang/String;
    .restart local v4       #parser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v6       #spnReader:Ljava/io/FileReader;
    :cond_56
    const/4 v7, 0x0

    #@57
    :try_start_57
    const-string v8, "numeric"

    #@59
    invoke-interface {v4, v7, v8}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@5c
    move-result-object v3

    #@5d
    .line 83
    .local v3, numeric:Ljava/lang/String;
    const/4 v7, 0x0

    #@5e
    const-string v8, "spn"

    #@60
    invoke-interface {v4, v7, v8}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@63
    move-result-object v0

    #@64
    .line 85
    .local v0, data:Ljava/lang/String;
    iget-object v7, p0, Lcom/android/internal/telephony/uicc/SpnOverride;->CarrierSpnMap:Ljava/util/HashMap;

    #@66
    invoke-virtual {v7, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_69
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_57 .. :try_end_69} :catch_6a
    .catch Ljava/io/IOException; {:try_start_57 .. :try_end_69} :catch_84

    #@69
    goto :goto_1c

    #@6a
    .line 87
    .end local v0           #data:Ljava/lang/String;
    .end local v2           #name:Ljava/lang/String;
    .end local v3           #numeric:Ljava/lang/String;
    .end local v4           #parser:Lorg/xmlpull/v1/XmlPullParser;
    :catch_6a
    move-exception v1

    #@6b
    .line 88
    .local v1, e:Lorg/xmlpull/v1/XmlPullParserException;
    const-string v7, "GSM"

    #@6d
    new-instance v8, Ljava/lang/StringBuilder;

    #@6f
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@72
    const-string v9, "Exception in spn-conf parser "

    #@74
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v8

    #@78
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v8

    #@7c
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7f
    move-result-object v8

    #@80
    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@83
    goto :goto_2b

    #@84
    .line 89
    .end local v1           #e:Lorg/xmlpull/v1/XmlPullParserException;
    :catch_84
    move-exception v1

    #@85
    .line 90
    .local v1, e:Ljava/io/IOException;
    const-string v7, "GSM"

    #@87
    new-instance v8, Ljava/lang/StringBuilder;

    #@89
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@8c
    const-string v9, "Exception in spn-conf parser "

    #@8e
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    move-result-object v8

    #@92
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v8

    #@96
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@99
    move-result-object v8

    #@9a
    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@9d
    goto :goto_2b
.end method


# virtual methods
.method containsCarrier(Ljava/lang/String;)Z
    .registers 3
    .parameter "carrier"

    #@0
    .prologue
    .line 47
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/SpnOverride;->CarrierSpnMap:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method getSpn(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "carrier"

    #@0
    .prologue
    .line 51
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/SpnOverride;->CarrierSpnMap:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Ljava/lang/String;

    #@8
    return-object v0
.end method
