.class Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher$1;
.super Landroid/content/BroadcastReceiver;
.source "CdmaSMSDispatcher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;


# direct methods
.method constructor <init>(Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1350
    iput-object p1, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher$1;->this$0:Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 17
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 1353
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher$1;->getResultCode()I

    #@3
    move-result v7

    #@4
    .line 1354
    .local v7, rc:I
    const/4 v11, -0x1

    #@5
    if-eq v7, v11, :cond_a

    #@7
    const/4 v11, 0x1

    #@8
    if-ne v7, v11, :cond_26

    #@a
    :cond_a
    const/4 v10, 0x1

    #@b
    .line 1355
    .local v10, success:Z
    :goto_b
    if-nez v10, :cond_28

    #@d
    .line 1356
    const-string v11, "CDMA"

    #@f
    new-instance v12, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v13, "SCP results error: result code = "

    #@16
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v12

    #@1a
    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v12

    #@1e
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v12

    #@22
    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 1412
    :goto_25
    return-void

    #@26
    .line 1354
    .end local v10           #success:Z
    :cond_26
    const/4 v10, 0x0

    #@27
    goto :goto_b

    #@28
    .line 1359
    .restart local v10       #success:Z
    :cond_28
    const/4 v11, 0x0

    #@29
    invoke-virtual {p0, v11}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher$1;->getResultExtras(Z)Landroid/os/Bundle;

    #@2c
    move-result-object v6

    #@2d
    .line 1360
    .local v6, extras:Landroid/os/Bundle;
    if-nez v6, :cond_37

    #@2f
    .line 1361
    const-string v11, "CDMA"

    #@31
    const-string v12, "SCP results error: missing extras"

    #@33
    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    goto :goto_25

    #@37
    .line 1364
    :cond_37
    const-string v11, "sender"

    #@39
    invoke-virtual {v6, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@3c
    move-result-object v9

    #@3d
    .line 1365
    .local v9, sender:Ljava/lang/String;
    if-nez v9, :cond_47

    #@3f
    .line 1366
    const-string v11, "CDMA"

    #@41
    const-string v12, "SCP results error: missing sender extra."

    #@43
    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@46
    goto :goto_25

    #@47
    .line 1369
    :cond_47
    const-string v11, "results"

    #@49
    invoke-virtual {v6, v11}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    #@4c
    move-result-object v8

    #@4d
    .line 1371
    .local v8, results:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/telephony/cdma/CdmaSmsCbProgramResults;>;"
    if-nez v8, :cond_57

    #@4f
    .line 1372
    const-string v11, "CDMA"

    #@51
    const-string v12, "SCP results error: missing results extra."

    #@53
    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@56
    goto :goto_25

    #@57
    .line 1376
    :cond_57
    new-instance v0, Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@59
    invoke-direct {v0}, Lcom/android/internal/telephony/cdma/sms/BearerData;-><init>()V

    #@5c
    .line 1377
    .local v0, bData:Lcom/android/internal/telephony/cdma/sms/BearerData;
    const/4 v11, 0x2

    #@5d
    iput v11, v0, Lcom/android/internal/telephony/cdma/sms/BearerData;->messageType:I

    #@5f
    .line 1378
    invoke-static {}, Lcom/android/internal/telephony/cdma/SmsMessage;->getNextMessageId()I

    #@62
    move-result v11

    #@63
    iput v11, v0, Lcom/android/internal/telephony/cdma/sms/BearerData;->messageId:I

    #@65
    .line 1379
    iput-object v8, v0, Lcom/android/internal/telephony/cdma/sms/BearerData;->serviceCategoryProgramResults:Ljava/util/ArrayList;

    #@67
    .line 1380
    invoke-static {v0}, Lcom/android/internal/telephony/cdma/sms/BearerData;->encode(Lcom/android/internal/telephony/cdma/sms/BearerData;)[B

    #@6a
    move-result-object v5

    #@6b
    .line 1382
    .local v5, encodedBearerData:[B
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    #@6d
    const/16 v11, 0x64

    #@6f
    invoke-direct {v1, v11}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    #@72
    .line 1383
    .local v1, baos:Ljava/io/ByteArrayOutputStream;
    new-instance v3, Ljava/io/DataOutputStream;

    #@74
    invoke-direct {v3, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    #@77
    .line 1385
    .local v3, dos:Ljava/io/DataOutputStream;
    const/16 v11, 0x1006

    #@79
    :try_start_79
    invoke-virtual {v3, v11}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@7c
    .line 1386
    const/4 v11, 0x0

    #@7d
    invoke-virtual {v3, v11}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@80
    .line 1387
    const/4 v11, 0x0

    #@81
    invoke-virtual {v3, v11}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@84
    .line 1388
    invoke-static {v9}, Landroid/telephony/PhoneNumberUtils;->cdmaCheckAndProcessPlusCode(Ljava/lang/String;)Ljava/lang/String;

    #@87
    move-result-object v11

    #@88
    invoke-static {v11}, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->parse(Ljava/lang/String;)Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;

    #@8b
    move-result-object v2

    #@8c
    .line 1390
    .local v2, destAddr:Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;
    iget v11, v2, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->digitMode:I

    #@8e
    invoke-virtual {v3, v11}, Ljava/io/DataOutputStream;->write(I)V

    #@91
    .line 1391
    iget v11, v2, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberMode:I

    #@93
    invoke-virtual {v3, v11}, Ljava/io/DataOutputStream;->write(I)V

    #@96
    .line 1392
    iget v11, v2, Lcom/android/internal/telephony/SmsAddress;->ton:I

    #@98
    invoke-virtual {v3, v11}, Ljava/io/DataOutputStream;->write(I)V

    #@9b
    .line 1393
    iget v11, v2, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberPlan:I

    #@9d
    invoke-virtual {v3, v11}, Ljava/io/DataOutputStream;->write(I)V

    #@a0
    .line 1394
    iget v11, v2, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberOfDigits:I

    #@a2
    invoke-virtual {v3, v11}, Ljava/io/DataOutputStream;->write(I)V

    #@a5
    .line 1395
    iget-object v11, v2, Lcom/android/internal/telephony/SmsAddress;->origBytes:[B

    #@a7
    const/4 v12, 0x0

    #@a8
    iget-object v13, v2, Lcom/android/internal/telephony/SmsAddress;->origBytes:[B

    #@aa
    array-length v13, v13

    #@ab
    invoke-virtual {v3, v11, v12, v13}, Ljava/io/DataOutputStream;->write([BII)V

    #@ae
    .line 1397
    const/4 v11, 0x0

    #@af
    invoke-virtual {v3, v11}, Ljava/io/DataOutputStream;->write(I)V

    #@b2
    .line 1398
    const/4 v11, 0x0

    #@b3
    invoke-virtual {v3, v11}, Ljava/io/DataOutputStream;->write(I)V

    #@b6
    .line 1399
    const/4 v11, 0x0

    #@b7
    invoke-virtual {v3, v11}, Ljava/io/DataOutputStream;->write(I)V

    #@ba
    .line 1400
    array-length v11, v5

    #@bb
    invoke-virtual {v3, v11}, Ljava/io/DataOutputStream;->write(I)V

    #@be
    .line 1401
    const/4 v11, 0x0

    #@bf
    array-length v12, v5

    #@c0
    invoke-virtual {v3, v5, v11, v12}, Ljava/io/DataOutputStream;->write([BII)V

    #@c3
    .line 1403
    iget-object v11, p0, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher$1;->this$0:Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;

    #@c5
    invoke-static {v11}, Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;->access$000(Lcom/android/internal/telephony/cdma/CdmaSMSDispatcher;)Lcom/android/internal/telephony/CommandsInterface;

    #@c8
    move-result-object v11

    #@c9
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@cc
    move-result-object v12

    #@cd
    const/4 v13, 0x0

    #@ce
    invoke-interface {v11, v12, v13}, Lcom/android/internal/telephony/CommandsInterface;->sendCdmaSms([BLandroid/os/Message;)V
    :try_end_d1
    .catchall {:try_start_79 .. :try_end_d1} :catchall_e6
    .catch Ljava/io/IOException; {:try_start_79 .. :try_end_d1} :catch_d9

    #@d1
    .line 1408
    :try_start_d1
    invoke-virtual {v3}, Ljava/io/DataOutputStream;->close()V
    :try_end_d4
    .catch Ljava/io/IOException; {:try_start_d1 .. :try_end_d4} :catch_d6

    #@d4
    goto/16 :goto_25

    #@d6
    .line 1409
    .end local v2           #destAddr:Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;
    :catch_d6
    move-exception v11

    #@d7
    goto/16 :goto_25

    #@d9
    .line 1404
    :catch_d9
    move-exception v4

    #@da
    .line 1405
    .local v4, e:Ljava/io/IOException;
    :try_start_da
    const-string v11, "CDMA"

    #@dc
    const-string v12, "exception creating SCP results PDU"

    #@de
    invoke-static {v11, v12, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_e1
    .catchall {:try_start_da .. :try_end_e1} :catchall_e6

    #@e1
    .line 1408
    :try_start_e1
    invoke-virtual {v3}, Ljava/io/DataOutputStream;->close()V
    :try_end_e4
    .catch Ljava/io/IOException; {:try_start_e1 .. :try_end_e4} :catch_d6

    #@e4
    goto/16 :goto_25

    #@e6
    .line 1407
    .end local v4           #e:Ljava/io/IOException;
    :catchall_e6
    move-exception v11

    #@e7
    .line 1408
    :try_start_e7
    invoke-virtual {v3}, Ljava/io/DataOutputStream;->close()V
    :try_end_ea
    .catch Ljava/io/IOException; {:try_start_e7 .. :try_end_ea} :catch_eb

    #@ea
    .line 1407
    :goto_ea
    throw v11

    #@eb
    .line 1409
    :catch_eb
    move-exception v12

    #@ec
    goto :goto_ea
.end method
