.class Lcom/android/internal/telephony/SMSDispatcher$SegmentExpirationRunnable;
.super Ljava/lang/Object;
.source "SMSDispatcher.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/SMSDispatcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SegmentExpirationRunnable"
.end annotation


# instance fields
.field private address:Ljava/lang/String;

.field private mFirstTime:J

.field final synthetic this$0:Lcom/android/internal/telephony/SMSDispatcher;


# direct methods
.method public constructor <init>(Lcom/android/internal/telephony/SMSDispatcher;Lcom/android/internal/telephony/SmsMessageBase;J)V
    .registers 7
    .parameter
    .parameter "sms"
    .parameter "firstTime"

    #@0
    .prologue
    .line 4610
    iput-object p1, p0, Lcom/android/internal/telephony/SMSDispatcher$SegmentExpirationRunnable;->this$0:Lcom/android/internal/telephony/SMSDispatcher;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 4611
    invoke-virtual {p2}, Lcom/android/internal/telephony/SmsMessageBase;->getOriginatingAddress()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    .line 4613
    .local v0, originatingAddress:Ljava/lang/String;
    if-eqz v0, :cond_12

    #@b
    .line 4614
    new-instance v1, Ljava/lang/String;

    #@d
    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    #@10
    iput-object v1, p0, Lcom/android/internal/telephony/SMSDispatcher$SegmentExpirationRunnable;->address:Ljava/lang/String;

    #@12
    .line 4616
    :cond_12
    iput-wide p3, p0, Lcom/android/internal/telephony/SMSDispatcher$SegmentExpirationRunnable;->mFirstTime:J

    #@14
    .line 4617
    return-void
.end method


# virtual methods
.method public run()V
    .registers 5

    #@0
    .prologue
    .line 4621
    const-wide/32 v2, 0x493e0

    #@3
    :try_start_3
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_6} :catch_44

    #@6
    .line 4627
    :goto_6
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    const-string v2, "address ="

    #@a
    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@d
    .line 4628
    .local v1, where:Ljava/lang/StringBuilder;
    new-instance v2, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v3, "\'"

    #@14
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    iget-object v3, p0, Lcom/android/internal/telephony/SMSDispatcher$SegmentExpirationRunnable;->address:Ljava/lang/String;

    #@1a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    const-string v3, "\'"

    #@20
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    .line 4629
    const-string v2, " AND "

    #@2d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    .line 4630
    const-string v2, "time ="

    #@32
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    .line 4631
    iget-wide v2, p0, Lcom/android/internal/telephony/SMSDispatcher$SegmentExpirationRunnable;->mFirstTime:J

    #@37
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@3a
    .line 4632
    iget-object v2, p0, Lcom/android/internal/telephony/SMSDispatcher$SegmentExpirationRunnable;->this$0:Lcom/android/internal/telephony/SMSDispatcher;

    #@3c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v3

    #@40
    invoke-static {v2, v3}, Lcom/android/internal/telephony/SMSDispatcher;->access$300(Lcom/android/internal/telephony/SMSDispatcher;Ljava/lang/String;)V

    #@43
    .line 4633
    return-void

    #@44
    .line 4622
    .end local v1           #where:Ljava/lang/StringBuilder;
    :catch_44
    move-exception v0

    #@45
    .line 4623
    .local v0, e:Ljava/lang/InterruptedException;
    const-string v2, "SegmentExpirationRunnable:run(), Thread Interrupted exception catch"

    #@47
    invoke-static {v2}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@4a
    goto :goto_6
.end method
