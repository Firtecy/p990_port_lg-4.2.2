.class public abstract Lcom/android/internal/telephony/msim/ISmsMSim$Stub;
.super Landroid/os/Binder;
.source "ISmsMSim.java"

# interfaces
.implements Lcom/android/internal/telephony/msim/ISmsMSim;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/msim/ISmsMSim;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/msim/ISmsMSim$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.android.internal.telephony.msim.ISmsMSim"

.field static final TRANSACTION_copyMessageToIccEf:I = 0x3

.field static final TRANSACTION_disableCellBroadcast:I = 0x8

.field static final TRANSACTION_disableCellBroadcastRange:I = 0xa

.field static final TRANSACTION_enableCellBroadcast:I = 0x7

.field static final TRANSACTION_enableCellBroadcastRange:I = 0x9

.field static final TRANSACTION_getAllMessagesFromIccEf:I = 0x1

.field static final TRANSACTION_getPreferredSmsSubscription:I = 0xb

.field static final TRANSACTION_sendData:I = 0x4

.field static final TRANSACTION_sendMultipartText:I = 0x6

.field static final TRANSACTION_sendText:I = 0x5

.field static final TRANSACTION_updateMessageOnIccEf:I = 0x2


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 28
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 29
    const-string v0, "com.android.internal.telephony.msim.ISmsMSim"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/android/internal/telephony/msim/ISmsMSim$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 30
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/msim/ISmsMSim;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 37
    if-nez p0, :cond_4

    #@2
    .line 38
    const/4 v0, 0x0

    #@3
    .line 44
    :goto_3
    return-object v0

    #@4
    .line 40
    :cond_4
    const-string v1, "com.android.internal.telephony.msim.ISmsMSim"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 41
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/android/internal/telephony/msim/ISmsMSim;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 42
    check-cast v0, Lcom/android/internal/telephony/msim/ISmsMSim;

    #@12
    goto :goto_3

    #@13
    .line 44
    :cond_13
    new-instance v0, Lcom/android/internal/telephony/msim/ISmsMSim$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/msim/ISmsMSim$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 48
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 24
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 52
    sparse-switch p1, :sswitch_data_200

    #@3
    .line 241
    invoke-super/range {p0 .. p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@6
    move-result v2

    #@7
    :goto_7
    return v2

    #@8
    .line 56
    :sswitch_8
    const-string v2, "com.android.internal.telephony.msim.ISmsMSim"

    #@a
    move-object/from16 v0, p3

    #@c
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    .line 57
    const/4 v2, 0x1

    #@10
    goto :goto_7

    #@11
    .line 61
    :sswitch_11
    const-string v2, "com.android.internal.telephony.msim.ISmsMSim"

    #@13
    move-object/from16 v0, p2

    #@15
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@18
    .line 63
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1b
    move-result v3

    #@1c
    .line 64
    .local v3, _arg0:I
    move-object/from16 v0, p0

    #@1e
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/msim/ISmsMSim$Stub;->getAllMessagesFromIccEf(I)Ljava/util/List;

    #@21
    move-result-object v18

    #@22
    .line 65
    .local v18, _result:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/telephony/SmsRawData;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@25
    .line 66
    move-object/from16 v0, p3

    #@27
    move-object/from16 v1, v18

    #@29
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@2c
    .line 67
    const/4 v2, 0x1

    #@2d
    goto :goto_7

    #@2e
    .line 71
    .end local v3           #_arg0:I
    .end local v18           #_result:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/telephony/SmsRawData;>;"
    :sswitch_2e
    const-string v2, "com.android.internal.telephony.msim.ISmsMSim"

    #@30
    move-object/from16 v0, p2

    #@32
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@35
    .line 73
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@38
    move-result v3

    #@39
    .line 75
    .restart local v3       #_arg0:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@3c
    move-result v4

    #@3d
    .line 77
    .local v4, _arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createByteArray()[B

    #@40
    move-result-object v5

    #@41
    .line 79
    .local v5, _arg2:[B
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@44
    move-result v6

    #@45
    .line 80
    .local v6, _arg3:I
    move-object/from16 v0, p0

    #@47
    invoke-virtual {v0, v3, v4, v5, v6}, Lcom/android/internal/telephony/msim/ISmsMSim$Stub;->updateMessageOnIccEf(II[BI)Z

    #@4a
    move-result v17

    #@4b
    .line 81
    .local v17, _result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@4e
    .line 82
    if-eqz v17, :cond_58

    #@50
    const/4 v2, 0x1

    #@51
    :goto_51
    move-object/from16 v0, p3

    #@53
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@56
    .line 83
    const/4 v2, 0x1

    #@57
    goto :goto_7

    #@58
    .line 82
    :cond_58
    const/4 v2, 0x0

    #@59
    goto :goto_51

    #@5a
    .line 87
    .end local v3           #_arg0:I
    .end local v4           #_arg1:I
    .end local v5           #_arg2:[B
    .end local v6           #_arg3:I
    .end local v17           #_result:Z
    :sswitch_5a
    const-string v2, "com.android.internal.telephony.msim.ISmsMSim"

    #@5c
    move-object/from16 v0, p2

    #@5e
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@61
    .line 89
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@64
    move-result v3

    #@65
    .line 91
    .restart local v3       #_arg0:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createByteArray()[B

    #@68
    move-result-object v4

    #@69
    .line 93
    .local v4, _arg1:[B
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createByteArray()[B

    #@6c
    move-result-object v5

    #@6d
    .line 95
    .restart local v5       #_arg2:[B
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@70
    move-result v6

    #@71
    .line 96
    .restart local v6       #_arg3:I
    move-object/from16 v0, p0

    #@73
    invoke-virtual {v0, v3, v4, v5, v6}, Lcom/android/internal/telephony/msim/ISmsMSim$Stub;->copyMessageToIccEf(I[B[BI)Z

    #@76
    move-result v17

    #@77
    .line 97
    .restart local v17       #_result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@7a
    .line 98
    if-eqz v17, :cond_84

    #@7c
    const/4 v2, 0x1

    #@7d
    :goto_7d
    move-object/from16 v0, p3

    #@7f
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@82
    .line 99
    const/4 v2, 0x1

    #@83
    goto :goto_7

    #@84
    .line 98
    :cond_84
    const/4 v2, 0x0

    #@85
    goto :goto_7d

    #@86
    .line 103
    .end local v3           #_arg0:I
    .end local v4           #_arg1:[B
    .end local v5           #_arg2:[B
    .end local v6           #_arg3:I
    .end local v17           #_result:Z
    :sswitch_86
    const-string v2, "com.android.internal.telephony.msim.ISmsMSim"

    #@88
    move-object/from16 v0, p2

    #@8a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@8d
    .line 105
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@90
    move-result-object v3

    #@91
    .line 107
    .local v3, _arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@94
    move-result-object v4

    #@95
    .line 109
    .local v4, _arg1:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@98
    move-result v5

    #@99
    .line 111
    .local v5, _arg2:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createByteArray()[B

    #@9c
    move-result-object v6

    #@9d
    .line 113
    .local v6, _arg3:[B
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@a0
    move-result v2

    #@a1
    if-eqz v2, :cond_cc

    #@a3
    .line 114
    sget-object v2, Landroid/app/PendingIntent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@a5
    move-object/from16 v0, p2

    #@a7
    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@aa
    move-result-object v7

    #@ab
    check-cast v7, Landroid/app/PendingIntent;

    #@ad
    .line 120
    .local v7, _arg4:Landroid/app/PendingIntent;
    :goto_ad
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@b0
    move-result v2

    #@b1
    if-eqz v2, :cond_ce

    #@b3
    .line 121
    sget-object v2, Landroid/app/PendingIntent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@b5
    move-object/from16 v0, p2

    #@b7
    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@ba
    move-result-object v8

    #@bb
    check-cast v8, Landroid/app/PendingIntent;

    #@bd
    .line 127
    .local v8, _arg5:Landroid/app/PendingIntent;
    :goto_bd
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@c0
    move-result v9

    #@c1
    .local v9, _arg6:I
    move-object/from16 v2, p0

    #@c3
    .line 128
    invoke-virtual/range {v2 .. v9}, Lcom/android/internal/telephony/msim/ISmsMSim$Stub;->sendData(Ljava/lang/String;Ljava/lang/String;I[BLandroid/app/PendingIntent;Landroid/app/PendingIntent;I)V

    #@c6
    .line 129
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@c9
    .line 130
    const/4 v2, 0x1

    #@ca
    goto/16 :goto_7

    #@cc
    .line 117
    .end local v7           #_arg4:Landroid/app/PendingIntent;
    .end local v8           #_arg5:Landroid/app/PendingIntent;
    .end local v9           #_arg6:I
    :cond_cc
    const/4 v7, 0x0

    #@cd
    .restart local v7       #_arg4:Landroid/app/PendingIntent;
    goto :goto_ad

    #@ce
    .line 124
    :cond_ce
    const/4 v8, 0x0

    #@cf
    .restart local v8       #_arg5:Landroid/app/PendingIntent;
    goto :goto_bd

    #@d0
    .line 134
    .end local v3           #_arg0:Ljava/lang/String;
    .end local v4           #_arg1:Ljava/lang/String;
    .end local v5           #_arg2:I
    .end local v6           #_arg3:[B
    .end local v7           #_arg4:Landroid/app/PendingIntent;
    .end local v8           #_arg5:Landroid/app/PendingIntent;
    :sswitch_d0
    const-string v2, "com.android.internal.telephony.msim.ISmsMSim"

    #@d2
    move-object/from16 v0, p2

    #@d4
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@d7
    .line 136
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@da
    move-result-object v3

    #@db
    .line 138
    .restart local v3       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@de
    move-result-object v4

    #@df
    .line 140
    .restart local v4       #_arg1:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@e2
    move-result-object v5

    #@e3
    .line 142
    .local v5, _arg2:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@e6
    move-result v2

    #@e7
    if-eqz v2, :cond_112

    #@e9
    .line 143
    sget-object v2, Landroid/app/PendingIntent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@eb
    move-object/from16 v0, p2

    #@ed
    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@f0
    move-result-object v6

    #@f1
    check-cast v6, Landroid/app/PendingIntent;

    #@f3
    .line 149
    .local v6, _arg3:Landroid/app/PendingIntent;
    :goto_f3
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@f6
    move-result v2

    #@f7
    if-eqz v2, :cond_114

    #@f9
    .line 150
    sget-object v2, Landroid/app/PendingIntent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@fb
    move-object/from16 v0, p2

    #@fd
    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@100
    move-result-object v7

    #@101
    check-cast v7, Landroid/app/PendingIntent;

    #@103
    .line 156
    .restart local v7       #_arg4:Landroid/app/PendingIntent;
    :goto_103
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@106
    move-result v8

    #@107
    .local v8, _arg5:I
    move-object/from16 v2, p0

    #@109
    .line 157
    invoke-virtual/range {v2 .. v8}, Lcom/android/internal/telephony/msim/ISmsMSim$Stub;->sendText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;I)V

    #@10c
    .line 158
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@10f
    .line 159
    const/4 v2, 0x1

    #@110
    goto/16 :goto_7

    #@112
    .line 146
    .end local v6           #_arg3:Landroid/app/PendingIntent;
    .end local v7           #_arg4:Landroid/app/PendingIntent;
    .end local v8           #_arg5:I
    :cond_112
    const/4 v6, 0x0

    #@113
    .restart local v6       #_arg3:Landroid/app/PendingIntent;
    goto :goto_f3

    #@114
    .line 153
    :cond_114
    const/4 v7, 0x0

    #@115
    .restart local v7       #_arg4:Landroid/app/PendingIntent;
    goto :goto_103

    #@116
    .line 163
    .end local v3           #_arg0:Ljava/lang/String;
    .end local v4           #_arg1:Ljava/lang/String;
    .end local v5           #_arg2:Ljava/lang/String;
    .end local v6           #_arg3:Landroid/app/PendingIntent;
    .end local v7           #_arg4:Landroid/app/PendingIntent;
    :sswitch_116
    const-string v2, "com.android.internal.telephony.msim.ISmsMSim"

    #@118
    move-object/from16 v0, p2

    #@11a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@11d
    .line 165
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@120
    move-result-object v3

    #@121
    .line 167
    .restart local v3       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@124
    move-result-object v4

    #@125
    .line 169
    .restart local v4       #_arg1:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    #@128
    move-result-object v13

    #@129
    .line 171
    .local v13, _arg2:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    sget-object v2, Landroid/app/PendingIntent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@12b
    move-object/from16 v0, p2

    #@12d
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    #@130
    move-result-object v14

    #@131
    .line 173
    .local v14, _arg3:Ljava/util/List;,"Ljava/util/List<Landroid/app/PendingIntent;>;"
    sget-object v2, Landroid/app/PendingIntent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@133
    move-object/from16 v0, p2

    #@135
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    #@138
    move-result-object v15

    #@139
    .line 175
    .local v15, _arg4:Ljava/util/List;,"Ljava/util/List<Landroid/app/PendingIntent;>;"
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@13c
    move-result v8

    #@13d
    .restart local v8       #_arg5:I
    move-object/from16 v10, p0

    #@13f
    move-object v11, v3

    #@140
    move-object v12, v4

    #@141
    move/from16 v16, v8

    #@143
    .line 176
    invoke-virtual/range {v10 .. v16}, Lcom/android/internal/telephony/msim/ISmsMSim$Stub;->sendMultipartText(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;I)V

    #@146
    .line 177
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@149
    .line 178
    const/4 v2, 0x1

    #@14a
    goto/16 :goto_7

    #@14c
    .line 182
    .end local v3           #_arg0:Ljava/lang/String;
    .end local v4           #_arg1:Ljava/lang/String;
    .end local v8           #_arg5:I
    .end local v13           #_arg2:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .end local v14           #_arg3:Ljava/util/List;,"Ljava/util/List<Landroid/app/PendingIntent;>;"
    .end local v15           #_arg4:Ljava/util/List;,"Ljava/util/List<Landroid/app/PendingIntent;>;"
    :sswitch_14c
    const-string v2, "com.android.internal.telephony.msim.ISmsMSim"

    #@14e
    move-object/from16 v0, p2

    #@150
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@153
    .line 184
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@156
    move-result v3

    #@157
    .line 186
    .local v3, _arg0:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@15a
    move-result v4

    #@15b
    .line 187
    .local v4, _arg1:I
    move-object/from16 v0, p0

    #@15d
    invoke-virtual {v0, v3, v4}, Lcom/android/internal/telephony/msim/ISmsMSim$Stub;->enableCellBroadcast(II)Z

    #@160
    move-result v17

    #@161
    .line 188
    .restart local v17       #_result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@164
    .line 189
    if-eqz v17, :cond_16f

    #@166
    const/4 v2, 0x1

    #@167
    :goto_167
    move-object/from16 v0, p3

    #@169
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@16c
    .line 190
    const/4 v2, 0x1

    #@16d
    goto/16 :goto_7

    #@16f
    .line 189
    :cond_16f
    const/4 v2, 0x0

    #@170
    goto :goto_167

    #@171
    .line 194
    .end local v3           #_arg0:I
    .end local v4           #_arg1:I
    .end local v17           #_result:Z
    :sswitch_171
    const-string v2, "com.android.internal.telephony.msim.ISmsMSim"

    #@173
    move-object/from16 v0, p2

    #@175
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@178
    .line 196
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@17b
    move-result v3

    #@17c
    .line 198
    .restart local v3       #_arg0:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@17f
    move-result v4

    #@180
    .line 199
    .restart local v4       #_arg1:I
    move-object/from16 v0, p0

    #@182
    invoke-virtual {v0, v3, v4}, Lcom/android/internal/telephony/msim/ISmsMSim$Stub;->disableCellBroadcast(II)Z

    #@185
    move-result v17

    #@186
    .line 200
    .restart local v17       #_result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@189
    .line 201
    if-eqz v17, :cond_194

    #@18b
    const/4 v2, 0x1

    #@18c
    :goto_18c
    move-object/from16 v0, p3

    #@18e
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@191
    .line 202
    const/4 v2, 0x1

    #@192
    goto/16 :goto_7

    #@194
    .line 201
    :cond_194
    const/4 v2, 0x0

    #@195
    goto :goto_18c

    #@196
    .line 206
    .end local v3           #_arg0:I
    .end local v4           #_arg1:I
    .end local v17           #_result:Z
    :sswitch_196
    const-string v2, "com.android.internal.telephony.msim.ISmsMSim"

    #@198
    move-object/from16 v0, p2

    #@19a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@19d
    .line 208
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1a0
    move-result v3

    #@1a1
    .line 210
    .restart local v3       #_arg0:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1a4
    move-result v4

    #@1a5
    .line 212
    .restart local v4       #_arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1a8
    move-result v5

    #@1a9
    .line 213
    .local v5, _arg2:I
    move-object/from16 v0, p0

    #@1ab
    invoke-virtual {v0, v3, v4, v5}, Lcom/android/internal/telephony/msim/ISmsMSim$Stub;->enableCellBroadcastRange(III)Z

    #@1ae
    move-result v17

    #@1af
    .line 214
    .restart local v17       #_result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1b2
    .line 215
    if-eqz v17, :cond_1bd

    #@1b4
    const/4 v2, 0x1

    #@1b5
    :goto_1b5
    move-object/from16 v0, p3

    #@1b7
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@1ba
    .line 216
    const/4 v2, 0x1

    #@1bb
    goto/16 :goto_7

    #@1bd
    .line 215
    :cond_1bd
    const/4 v2, 0x0

    #@1be
    goto :goto_1b5

    #@1bf
    .line 220
    .end local v3           #_arg0:I
    .end local v4           #_arg1:I
    .end local v5           #_arg2:I
    .end local v17           #_result:Z
    :sswitch_1bf
    const-string v2, "com.android.internal.telephony.msim.ISmsMSim"

    #@1c1
    move-object/from16 v0, p2

    #@1c3
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1c6
    .line 222
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1c9
    move-result v3

    #@1ca
    .line 224
    .restart local v3       #_arg0:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1cd
    move-result v4

    #@1ce
    .line 226
    .restart local v4       #_arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1d1
    move-result v5

    #@1d2
    .line 227
    .restart local v5       #_arg2:I
    move-object/from16 v0, p0

    #@1d4
    invoke-virtual {v0, v3, v4, v5}, Lcom/android/internal/telephony/msim/ISmsMSim$Stub;->disableCellBroadcastRange(III)Z

    #@1d7
    move-result v17

    #@1d8
    .line 228
    .restart local v17       #_result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1db
    .line 229
    if-eqz v17, :cond_1e6

    #@1dd
    const/4 v2, 0x1

    #@1de
    :goto_1de
    move-object/from16 v0, p3

    #@1e0
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@1e3
    .line 230
    const/4 v2, 0x1

    #@1e4
    goto/16 :goto_7

    #@1e6
    .line 229
    :cond_1e6
    const/4 v2, 0x0

    #@1e7
    goto :goto_1de

    #@1e8
    .line 234
    .end local v3           #_arg0:I
    .end local v4           #_arg1:I
    .end local v5           #_arg2:I
    .end local v17           #_result:Z
    :sswitch_1e8
    const-string v2, "com.android.internal.telephony.msim.ISmsMSim"

    #@1ea
    move-object/from16 v0, p2

    #@1ec
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1ef
    .line 235
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/msim/ISmsMSim$Stub;->getPreferredSmsSubscription()I

    #@1f2
    move-result v17

    #@1f3
    .line 236
    .local v17, _result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1f6
    .line 237
    move-object/from16 v0, p3

    #@1f8
    move/from16 v1, v17

    #@1fa
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@1fd
    .line 238
    const/4 v2, 0x1

    #@1fe
    goto/16 :goto_7

    #@200
    .line 52
    :sswitch_data_200
    .sparse-switch
        0x1 -> :sswitch_11
        0x2 -> :sswitch_2e
        0x3 -> :sswitch_5a
        0x4 -> :sswitch_86
        0x5 -> :sswitch_d0
        0x6 -> :sswitch_116
        0x7 -> :sswitch_14c
        0x8 -> :sswitch_171
        0x9 -> :sswitch_196
        0xa -> :sswitch_1bf
        0xb -> :sswitch_1e8
        0x5f4e5446 -> :sswitch_8
    .end sparse-switch
.end method
