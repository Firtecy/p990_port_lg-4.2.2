.class public Lcom/android/internal/telephony/uicc/IccCardProxy;
.super Landroid/os/Handler;
.source "IccCardProxy.java"

# interfaces
.implements Lcom/android/internal/telephony/IccCard;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/uicc/IccCardProxy$1;
    }
.end annotation


# static fields
.field private static final DBG:Z = true

.field private static ENABLE_PRIVACY_LOG:Z = false

.field private static final EVENT_APP_READY:I = 0x6

.field private static final EVENT_CDMA_SUBSCRIPTION_SOURCE_CHANGED:I = 0xb

.field private static final EVENT_ICC_ABSENT:I = 0x4

.field private static final EVENT_ICC_CHANGED:I = 0x3

.field private static final EVENT_ICC_LOCKED:I = 0x5

.field protected static final EVENT_ICC_RECORD_EVENTS:I = 0x33

.field private static final EVENT_IMSI_READY:I = 0x8

.field private static final EVENT_PERSO_LOCKED:I = 0x9

.field private static final EVENT_RADIO_OFF_OR_UNAVAILABLE:I = 0x1

.field private static final EVENT_RADIO_ON:I = 0x2

.field protected static final EVENT_RECORDS_LOADED:I = 0x7

.field private static final LOG_TAG:Ljava/lang/String; = "RIL_IccCardProxy"


# instance fields
.field protected mAbsentRegistrants:Landroid/os/RegistrantList;

.field private mCdmaSSM:Lcom/android/internal/telephony/cdma/CdmaSubscriptionSourceManager;

.field private mCi:Lcom/android/internal/telephony/CommandsInterface;

.field private mContext:Landroid/content/Context;

.field protected mCurrentAppType:I

.field protected mExternalState:Lcom/android/internal/telephony/IccCardConstants$State;

.field protected mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

.field private mInitialized:Z

.field protected final mLock:Ljava/lang/Object;

.field private mPersoLockedRegistrants:Landroid/os/RegistrantList;

.field private mPersoSubState:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;

.field private mPinLockedRegistrants:Landroid/os/RegistrantList;

.field protected mQuietMode:Z

.field private mRadioOn:Z

.field protected mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

.field protected mUiccCard:Lcom/android/internal/telephony/uicc/UiccCard;

.field protected mUiccController:Lcom/android/internal/telephony/uicc/UiccController;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 74
    const-string v1, "persist.service.privacy.enable"

    #@2
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    const-string v2, "ATT"

    #@8
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v0

    #@c
    if-nez v0, :cond_1a

    #@e
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@11
    move-result-object v0

    #@12
    const-string v2, "TMO"

    #@14
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@17
    move-result v0

    #@18
    if-eqz v0, :cond_2e

    #@1a
    :cond_1a
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getCountry()Ljava/lang/String;

    #@1d
    move-result-object v0

    #@1e
    const-string v2, "US"

    #@20
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23
    move-result v0

    #@24
    if-eqz v0, :cond_2e

    #@26
    const/4 v0, 0x0

    #@27
    :goto_27
    invoke-static {v1, v0}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@2a
    move-result v0

    #@2b
    sput-boolean v0, Lcom/android/internal/telephony/uicc/IccCardProxy;->ENABLE_PRIVACY_LOG:Z

    #@2d
    return-void

    #@2e
    :cond_2e
    const/4 v0, 0x1

    #@2f
    goto :goto_27
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;)V
    .registers 7
    .parameter "context"
    .parameter "ci"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    const/4 v2, 0x0

    #@3
    .line 111
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@6
    .line 90
    new-instance v0, Ljava/lang/Object;

    #@8
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@b
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mLock:Ljava/lang/Object;

    #@d
    .line 94
    new-instance v0, Landroid/os/RegistrantList;

    #@f
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@12
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mAbsentRegistrants:Landroid/os/RegistrantList;

    #@14
    .line 95
    new-instance v0, Landroid/os/RegistrantList;

    #@16
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@19
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mPinLockedRegistrants:Landroid/os/RegistrantList;

    #@1b
    .line 96
    new-instance v0, Landroid/os/RegistrantList;

    #@1d
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@20
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mPersoLockedRegistrants:Landroid/os/RegistrantList;

    #@22
    .line 98
    iput v3, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mCurrentAppType:I

    #@24
    .line 99
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@26
    .line 100
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccCard:Lcom/android/internal/telephony/uicc/UiccCard;

    #@28
    .line 101
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@2a
    .line 102
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@2c
    .line 103
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mCdmaSSM:Lcom/android/internal/telephony/cdma/CdmaSubscriptionSourceManager;

    #@2e
    .line 104
    iput-boolean v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mRadioOn:Z

    #@30
    .line 105
    iput-boolean v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mQuietMode:Z

    #@32
    .line 107
    iput-boolean v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mInitialized:Z

    #@34
    .line 108
    sget-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->UNKNOWN:Lcom/android/internal/telephony/IccCardConstants$State;

    #@36
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mExternalState:Lcom/android/internal/telephony/IccCardConstants$State;

    #@38
    .line 109
    sget-object v0, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;->PERSOSUBSTATE_UNKNOWN:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;

    #@3a
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mPersoSubState:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;

    #@3c
    .line 112
    const-string v0, "Creating"

    #@3e
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/uicc/IccCardProxy;->log(Ljava/lang/String;)V

    #@41
    .line 113
    iput-object p1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mContext:Landroid/content/Context;

    #@43
    .line 114
    iput-object p2, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@45
    .line 115
    const/16 v0, 0xb

    #@47
    invoke-static {p1, p2, p0, v0, v2}, Lcom/android/internal/telephony/cdma/CdmaSubscriptionSourceManager;->getInstance(Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;Landroid/os/Handler;ILjava/lang/Object;)Lcom/android/internal/telephony/cdma/CdmaSubscriptionSourceManager;

    #@4a
    move-result-object v0

    #@4b
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mCdmaSSM:Lcom/android/internal/telephony/cdma/CdmaSubscriptionSourceManager;

    #@4d
    .line 117
    invoke-static {}, Lcom/android/internal/telephony/uicc/UiccController;->getInstance()Lcom/android/internal/telephony/uicc/UiccController;

    #@50
    move-result-object v0

    #@51
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@53
    .line 118
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@55
    const/4 v1, 0x3

    #@56
    invoke-virtual {v0, p0, v1, v2}, Lcom/android/internal/telephony/uicc/UiccController;->registerForIccChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@59
    .line 119
    const/4 v0, 0x2

    #@5a
    invoke-interface {p2, p0, v0, v2}, Lcom/android/internal/telephony/CommandsInterface;->registerForOn(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5d
    .line 120
    invoke-interface {p2, p0, v3, v2}, Lcom/android/internal/telephony/CommandsInterface;->registerForOffOrNotAvailable(Landroid/os/Handler;ILjava/lang/Object;)V

    #@60
    .line 121
    sget-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->NOT_READY:Lcom/android/internal/telephony/IccCardConstants$State;

    #@62
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/uicc/IccCardProxy;->setExternalState(Lcom/android/internal/telephony/IccCardConstants$State;)V

    #@65
    .line 122
    return-void
.end method

.method private broadcastPersoSubState(Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;)V
    .registers 5
    .parameter "state"

    #@0
    .prologue
    .line 506
    sget-object v0, Lcom/android/internal/telephony/uicc/IccCardProxy$1;->$SwitchMap$com$android$internal$telephony$uicc$IccCardApplicationStatus$PersoSubState:[I

    #@2
    invoke-virtual {p1}, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;->ordinal()I

    #@5
    move-result v1

    #@6
    aget v0, v0, v1

    #@8
    packed-switch v0, :pswitch_data_a2

    #@b
    .line 585
    const-string v0, "RIL_IccCardProxy"

    #@d
    new-instance v1, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v2, "This Personalization substate is not handled: "

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 588
    :goto_23
    :pswitch_23
    return-void

    #@24
    .line 512
    :pswitch_24
    const-string v0, "RIL_IccCardProxy"

    #@26
    const-string v1, "Notify SIM network locked."

    #@28
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 513
    const-string v0, "LOCKED"

    #@2d
    const-string v1, "NETWORK"

    #@2f
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/uicc/IccCardProxy;->broadcastIccStateChangedIntent(Ljava/lang/String;Ljava/lang/String;)V

    #@32
    .line 515
    const-string v0, "NETWORK"

    #@34
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/uicc/IccCardProxy;->broadcastPersoStatus(Ljava/lang/String;)V

    #@37
    goto :goto_23

    #@38
    .line 519
    :pswitch_38
    const-string v0, "RIL_IccCardProxy"

    #@3a
    const-string v1, "Notify SIM network Subset locked."

    #@3c
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    .line 520
    const-string v0, "LOCKED"

    #@41
    const-string v1, "NETWORK SUBSET"

    #@43
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/uicc/IccCardProxy;->broadcastIccStateChangedIntent(Ljava/lang/String;Ljava/lang/String;)V

    #@46
    .line 522
    const-string v0, "NETWORK SUBSET"

    #@48
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/uicc/IccCardProxy;->broadcastPersoStatus(Ljava/lang/String;)V

    #@4b
    goto :goto_23

    #@4c
    .line 526
    :pswitch_4c
    const-string v0, "RIL_IccCardProxy"

    #@4e
    const-string v1, "Notify SIM Corporate locked."

    #@50
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@53
    .line 527
    const-string v0, "LOCKED"

    #@55
    const-string v1, "CORPORATE"

    #@57
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/uicc/IccCardProxy;->broadcastIccStateChangedIntent(Ljava/lang/String;Ljava/lang/String;)V

    #@5a
    .line 529
    const-string v0, "CORPORATE"

    #@5c
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/uicc/IccCardProxy;->broadcastPersoStatus(Ljava/lang/String;)V

    #@5f
    goto :goto_23

    #@60
    .line 533
    :pswitch_60
    const-string v0, "RIL_IccCardProxy"

    #@62
    const-string v1, "Notify SIM Service Provider locked."

    #@64
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@67
    .line 534
    const-string v0, "LOCKED"

    #@69
    const-string v1, "SERVICE PROVIDER"

    #@6b
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/uicc/IccCardProxy;->broadcastIccStateChangedIntent(Ljava/lang/String;Ljava/lang/String;)V

    #@6e
    .line 536
    const-string v0, "SERVICE PROVIDER"

    #@70
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/uicc/IccCardProxy;->broadcastPersoStatus(Ljava/lang/String;)V

    #@73
    goto :goto_23

    #@74
    .line 540
    :pswitch_74
    const-string v0, "RIL_IccCardProxy"

    #@76
    const-string v1, "Notify SIM SIM locked."

    #@78
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7b
    .line 541
    const-string v0, "LOCKED"

    #@7d
    const-string v1, "SIM"

    #@7f
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/uicc/IccCardProxy;->broadcastIccStateChangedIntent(Ljava/lang/String;Ljava/lang/String;)V

    #@82
    .line 543
    const-string v0, "SIM"

    #@84
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/uicc/IccCardProxy;->broadcastPersoStatus(Ljava/lang/String;)V

    #@87
    goto :goto_23

    #@88
    .line 551
    :pswitch_88
    const-string v0, "RIL_IccCardProxy"

    #@8a
    new-instance v1, Ljava/lang/StringBuilder;

    #@8c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@8f
    const-string v2, "This Personalization substate is not handled: "

    #@91
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v1

    #@95
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@98
    move-result-object v1

    #@99
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9c
    move-result-object v1

    #@9d
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@a0
    goto :goto_23

    #@a1
    .line 506
    nop

    #@a2
    :pswitch_data_a2
    .packed-switch 0x1
        :pswitch_23
        :pswitch_23
        :pswitch_23
        :pswitch_24
        :pswitch_38
        :pswitch_4c
        :pswitch_60
        :pswitch_74
        :pswitch_88
        :pswitch_88
        :pswitch_88
        :pswitch_88
        :pswitch_88
    .end packed-switch
.end method

.method private guessRadioTech()I
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 164
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@3
    invoke-virtual {v2, v1}, Lcom/android/internal/telephony/uicc/UiccController;->getUiccCardApplication(I)Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@6
    move-result-object v0

    #@7
    .line 166
    .local v0, app:Lcom/android/internal/telephony/uicc/UiccCardApplication;
    if-eqz v0, :cond_a

    #@9
    .line 169
    :goto_9
    return v1

    #@a
    :cond_a
    const/4 v1, 0x2

    #@b
    goto :goto_9
.end method

.method private processLockedState()V
    .registers 6

    #@0
    .prologue
    .line 470
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v3

    #@3
    .line 471
    :try_start_3
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@5
    if-nez v2, :cond_9

    #@7
    .line 473
    monitor-exit v3

    #@8
    .line 501
    :goto_8
    return-void

    #@9
    .line 475
    :cond_9
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@b
    invoke-virtual {v2}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getPin1State()Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;

    #@e
    move-result-object v1

    #@f
    .line 476
    .local v1, pin1State:Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;
    sget-object v2, Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;->PINSTATE_ENABLED_PERM_BLOCKED:Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;

    #@11
    if-ne v1, v2, :cond_1d

    #@13
    .line 477
    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->PERM_DISABLED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@15
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/uicc/IccCardProxy;->setExternalState(Lcom/android/internal/telephony/IccCardConstants$State;)V

    #@18
    .line 478
    monitor-exit v3

    #@19
    goto :goto_8

    #@1a
    .line 500
    .end local v1           #pin1State:Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;
    :catchall_1a
    move-exception v2

    #@1b
    monitor-exit v3
    :try_end_1c
    .catchall {:try_start_3 .. :try_end_1c} :catchall_1a

    #@1c
    throw v2

    #@1d
    .line 481
    .restart local v1       #pin1State:Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;
    :cond_1d
    :try_start_1d
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@1f
    invoke-virtual {v2}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getState()Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;

    #@22
    move-result-object v0

    #@23
    .line 482
    .local v0, appState:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;
    sget-object v2, Lcom/android/internal/telephony/uicc/IccCardProxy$1;->$SwitchMap$com$android$internal$telephony$uicc$IccCardApplicationStatus$AppState:[I

    #@25
    invoke-virtual {v0}, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;->ordinal()I

    #@28
    move-result v4

    #@29
    aget v2, v2, v4

    #@2b
    packed-switch v2, :pswitch_data_80

    #@2e
    .line 500
    :goto_2e
    monitor-exit v3

    #@2f
    goto :goto_8

    #@30
    .line 485
    :pswitch_30
    const-string v2, "[LGE] Notify SIM pin locked."

    #@32
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/uicc/IccCardProxy;->log(Ljava/lang/String;)V

    #@35
    .line 486
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@37
    invoke-virtual {v2}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getHiddenResetFlag()Z

    #@3a
    move-result v2

    #@3b
    if-eqz v2, :cond_6f

    #@3d
    .line 488
    const-string v2, "[LGE] hidden reset!!."

    #@3f
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/uicc/IccCardProxy;->log(Ljava/lang/String;)V

    #@42
    .line 489
    sget-boolean v2, Lcom/android/internal/telephony/uicc/IccCardProxy;->ENABLE_PRIVACY_LOG:Z

    #@44
    if-eqz v2, :cond_62

    #@46
    new-instance v2, Ljava/lang/StringBuilder;

    #@48
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@4b
    const-string v4, "[LGE] getInternalPIN : "

    #@4d
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v2

    #@51
    iget-object v4, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@53
    invoke-virtual {v4}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getInternalPIN()Ljava/lang/String;

    #@56
    move-result-object v4

    #@57
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v2

    #@5b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v2

    #@5f
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/uicc/IccCardProxy;->log(Ljava/lang/String;)V

    #@62
    .line 490
    :cond_62
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@64
    iget-object v4, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@66
    invoke-virtual {v4}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getInternalPIN()Ljava/lang/String;

    #@69
    move-result-object v4

    #@6a
    invoke-virtual {v2, v4}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->HiddensupplyPin(Ljava/lang/String;)V

    #@6d
    monitor-exit v3

    #@6e
    goto :goto_8

    #@6f
    .line 493
    :cond_6f
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mPinLockedRegistrants:Landroid/os/RegistrantList;

    #@71
    invoke-virtual {v2}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@74
    .line 494
    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->PIN_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@76
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/uicc/IccCardProxy;->setExternalState(Lcom/android/internal/telephony/IccCardConstants$State;)V

    #@79
    goto :goto_2e

    #@7a
    .line 497
    :pswitch_7a
    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->PUK_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@7c
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/uicc/IccCardProxy;->setExternalState(Lcom/android/internal/telephony/IccCardConstants$State;)V
    :try_end_7f
    .catchall {:try_start_1d .. :try_end_7f} :catchall_1a

    #@7f
    goto :goto_2e

    #@80
    .line 482
    :pswitch_data_80
    .packed-switch 0x3
        :pswitch_30
        :pswitch_7a
    .end packed-switch
.end method

.method private setExternalState(Lcom/android/internal/telephony/IccCardConstants$State;)V
    .registers 3
    .parameter "newState"

    #@0
    .prologue
    .line 653
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0}, Lcom/android/internal/telephony/uicc/IccCardProxy;->setExternalState(Lcom/android/internal/telephony/IccCardConstants$State;Z)V

    #@4
    .line 654
    return-void
.end method

.method private updateQuietMode()V
    .registers 8

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 178
    iget-object v4, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mLock:Ljava/lang/Object;

    #@3
    monitor-enter v4

    #@4
    .line 179
    :try_start_4
    iget-boolean v2, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mQuietMode:Z

    #@6
    .line 181
    .local v2, oldQuietMode:Z
    const/4 v0, -0x1

    #@7
    .line 182
    .local v0, cdmaSource:I
    iget v5, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mCurrentAppType:I

    #@9
    if-ne v5, v3, :cond_77

    #@b
    .line 183
    const/4 v1, 0x0

    #@c
    .line 184
    .local v1, newQuietMode:Z
    new-instance v5, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v6, "updateQuietMode: 3GPP subscription -> newQuietMode="

    #@13
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v5

    #@17
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v5

    #@1b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v5

    #@1f
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/uicc/IccCardProxy;->log(Ljava/lang/String;)V

    #@22
    .line 194
    :goto_22
    iget-boolean v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mQuietMode:Z

    #@24
    .line 197
    iget-boolean v5, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mQuietMode:Z

    #@26
    if-nez v5, :cond_8e

    #@28
    if-ne v1, v3, :cond_8e

    #@2a
    .line 200
    const-string v3, "Switching to QuietMode."

    #@2c
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/uicc/IccCardProxy;->log(Ljava/lang/String;)V

    #@2f
    .line 201
    sget-object v3, Lcom/android/internal/telephony/IccCardConstants$State;->READY:Lcom/android/internal/telephony/IccCardConstants$State;

    #@31
    invoke-direct {p0, v3}, Lcom/android/internal/telephony/uicc/IccCardProxy;->setExternalState(Lcom/android/internal/telephony/IccCardConstants$State;)V

    #@34
    .line 202
    iput-boolean v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mQuietMode:Z

    #@36
    .line 212
    :cond_36
    :goto_36
    new-instance v3, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v5, "updateQuietMode: QuietMode is "

    #@3d
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v3

    #@41
    iget-boolean v5, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mQuietMode:Z

    #@43
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@46
    move-result-object v3

    #@47
    const-string v5, " (app_type="

    #@49
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v3

    #@4d
    iget v5, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mCurrentAppType:I

    #@4f
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@52
    move-result-object v3

    #@53
    const-string v5, " cdmaSource="

    #@55
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v3

    #@59
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v3

    #@5d
    const-string v5, ")"

    #@5f
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v3

    #@63
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@66
    move-result-object v3

    #@67
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/uicc/IccCardProxy;->log(Ljava/lang/String;)V

    #@6a
    .line 215
    const/4 v3, 0x1

    #@6b
    iput-boolean v3, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mInitialized:Z

    #@6d
    .line 216
    const/4 v3, 0x3

    #@6e
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/uicc/IccCardProxy;->obtainMessage(I)Landroid/os/Message;

    #@71
    move-result-object v3

    #@72
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/uicc/IccCardProxy;->sendMessage(Landroid/os/Message;)Z

    #@75
    .line 217
    monitor-exit v4

    #@76
    .line 218
    return-void

    #@77
    .line 186
    .end local v1           #newQuietMode:Z
    :cond_77
    iget-object v5, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mCdmaSSM:Lcom/android/internal/telephony/cdma/CdmaSubscriptionSourceManager;

    #@79
    if-eqz v5, :cond_8a

    #@7b
    iget-object v5, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mCdmaSSM:Lcom/android/internal/telephony/cdma/CdmaSubscriptionSourceManager;

    #@7d
    invoke-virtual {v5}, Lcom/android/internal/telephony/cdma/CdmaSubscriptionSourceManager;->getCdmaSubscriptionSource()I

    #@80
    move-result v0

    #@81
    .line 189
    :goto_81
    if-ne v0, v3, :cond_8c

    #@83
    iget v5, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mCurrentAppType:I

    #@85
    const/4 v6, 0x2

    #@86
    if-ne v5, v6, :cond_8c

    #@88
    move v1, v3

    #@89
    .restart local v1       #newQuietMode:Z
    :goto_89
    goto :goto_22

    #@8a
    .line 186
    .end local v1           #newQuietMode:Z
    :cond_8a
    const/4 v0, -0x1

    #@8b
    goto :goto_81

    #@8c
    .line 189
    :cond_8c
    const/4 v1, 0x0

    #@8d
    goto :goto_89

    #@8e
    .line 203
    .restart local v1       #newQuietMode:Z
    :cond_8e
    iget-boolean v5, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mQuietMode:Z

    #@90
    if-ne v5, v3, :cond_36

    #@92
    if-nez v1, :cond_36

    #@94
    .line 205
    new-instance v3, Ljava/lang/StringBuilder;

    #@96
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@99
    const-string v5, "updateQuietMode: Switching out from QuietMode. Force broadcast of current state="

    #@9b
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v3

    #@9f
    iget-object v5, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mExternalState:Lcom/android/internal/telephony/IccCardConstants$State;

    #@a1
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v3

    #@a5
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a8
    move-result-object v3

    #@a9
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/uicc/IccCardProxy;->log(Ljava/lang/String;)V

    #@ac
    .line 208
    iput-boolean v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mQuietMode:Z

    #@ae
    .line 209
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mExternalState:Lcom/android/internal/telephony/IccCardConstants$State;

    #@b0
    const/4 v5, 0x1

    #@b1
    invoke-virtual {p0, v3, v5}, Lcom/android/internal/telephony/uicc/IccCardProxy;->setExternalState(Lcom/android/internal/telephony/IccCardConstants$State;Z)V

    #@b4
    goto :goto_36

    #@b5
    .line 217
    .end local v0           #cdmaSource:I
    .end local v1           #newQuietMode:Z
    .end local v2           #oldQuietMode:Z
    :catchall_b5
    move-exception v3

    #@b6
    monitor-exit v4
    :try_end_b7
    .catchall {:try_start_4 .. :try_end_b7} :catchall_b5

    #@b7
    throw v3
.end method


# virtual methods
.method public HiddensupplyPin(Ljava/lang/String;Landroid/os/Message;)V
    .registers 5
    .parameter "pin"
    .parameter "onComplete"

    #@0
    .prologue
    .line 1037
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@2
    if-eqz v1, :cond_a

    #@4
    .line 1038
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@6
    invoke-virtual {v1, p1}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->HiddensupplyPin(Ljava/lang/String;)V

    #@9
    .line 1045
    :cond_9
    :goto_9
    return-void

    #@a
    .line 1039
    :cond_a
    if-eqz p2, :cond_9

    #@c
    .line 1040
    new-instance v0, Ljava/lang/RuntimeException;

    #@e
    const-string v1, "ICC card is absent."

    #@10
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@13
    .line 1041
    .local v0, e:Ljava/lang/Exception;
    invoke-static {p2}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;)Landroid/os/AsyncResult;

    #@16
    move-result-object v1

    #@17
    iput-object v0, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@19
    .line 1042
    invoke-virtual {p2}, Landroid/os/Message;->sendToTarget()V

    #@1c
    goto :goto_9
.end method

.method protected broadcastIccStateChangedIntent(Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "value"
    .parameter "reason"

    #@0
    .prologue
    .line 449
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 450
    :try_start_3
    iget-boolean v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mQuietMode:Z

    #@5
    if-eqz v1, :cond_29

    #@7
    .line 451
    new-instance v1, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v3, "QuietMode: NOT Broadcasting intent ACTION_SIM_STATE_CHANGED "

    #@e
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    const-string v3, " reason "

    #@18
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v1

    #@24
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/uicc/IccCardProxy;->log(Ljava/lang/String;)V

    #@27
    .line 453
    monitor-exit v2

    #@28
    .line 467
    :goto_28
    return-void

    #@29
    .line 456
    :cond_29
    new-instance v0, Landroid/content/Intent;

    #@2b
    const-string v1, "android.intent.action.SIM_STATE_CHANGED"

    #@2d
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@30
    .line 457
    .local v0, intent:Landroid/content/Intent;
    const/high16 v1, 0x2000

    #@32
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@35
    .line 458
    const-string v1, "phoneName"

    #@37
    const-string v3, "Phone"

    #@39
    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@3c
    .line 459
    const-string v1, "ss"

    #@3e
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@41
    .line 460
    const-string v1, "reason"

    #@43
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@46
    .line 462
    new-instance v1, Ljava/lang/StringBuilder;

    #@48
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@4b
    const-string v3, "Broadcasting intent ACTION_SIM_STATE_CHANGED "

    #@4d
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v1

    #@51
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v1

    #@55
    const-string v3, " reason "

    #@57
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v1

    #@5b
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v1

    #@5f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@62
    move-result-object v1

    #@63
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/uicc/IccCardProxy;->log(Ljava/lang/String;)V

    #@66
    .line 464
    const-string v1, "android.permission.READ_PHONE_STATE"

    #@68
    const/4 v3, -0x1

    #@69
    invoke-static {v0, v1, v3}, Landroid/app/ActivityManagerNative;->broadcastStickyIntent(Landroid/content/Intent;Ljava/lang/String;I)V

    #@6c
    .line 466
    monitor-exit v2

    #@6d
    goto :goto_28

    #@6e
    .end local v0           #intent:Landroid/content/Intent;
    :catchall_6e
    move-exception v1

    #@6f
    monitor-exit v2
    :try_end_70
    .catchall {:try_start_3 .. :try_end_70} :catchall_6e

    #@70
    throw v1
.end method

.method public broadcastPersoStatus(Ljava/lang/String;)V
    .registers 6
    .parameter "reason"

    #@0
    .prologue
    .line 594
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "android.intent.action.PersoStatus"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 595
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "reason"

    #@9
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@c
    .line 596
    const-string v1, "RIL_IccCardProxy"

    #@e
    new-instance v2, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v3, "broadcastPersoStatus intent PersoStatus  reason - "

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 598
    const-string v1, "android.permission.READ_PHONE_STATE"

    #@26
    const/4 v2, -0x1

    #@27
    invoke-static {v0, v1, v2}, Landroid/app/ActivityManagerNative;->broadcastStickyIntent(Landroid/content/Intent;Ljava/lang/String;I)V

    #@2a
    .line 599
    return-void
.end method

.method public changeIccFdnPassword(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 7
    .parameter "oldPassword"
    .parameter "newPassword"
    .parameter "onComplete"

    #@0
    .prologue
    .line 941
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 942
    :try_start_3
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@5
    if-eqz v1, :cond_e

    #@7
    .line 943
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@9
    invoke-virtual {v1, p1, p2, p3}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->changeIccFdnPassword(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    #@c
    .line 950
    :cond_c
    monitor-exit v2

    #@d
    .line 951
    :goto_d
    return-void

    #@e
    .line 944
    :cond_e
    if-eqz p3, :cond_c

    #@10
    .line 945
    new-instance v0, Ljava/lang/RuntimeException;

    #@12
    const-string v1, "ICC card is absent."

    #@14
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@17
    .line 946
    .local v0, e:Ljava/lang/Exception;
    invoke-static {p3}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;)Landroid/os/AsyncResult;

    #@1a
    move-result-object v1

    #@1b
    iput-object v0, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@1d
    .line 947
    invoke-virtual {p3}, Landroid/os/Message;->sendToTarget()V

    #@20
    .line 948
    monitor-exit v2

    #@21
    goto :goto_d

    #@22
    .line 950
    .end local v0           #e:Ljava/lang/Exception;
    :catchall_22
    move-exception v1

    #@23
    monitor-exit v2
    :try_end_24
    .catchall {:try_start_3 .. :try_end_24} :catchall_22

    #@24
    throw v1
.end method

.method public changeIccLockPassword(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 7
    .parameter "oldPassword"
    .parameter "newPassword"
    .parameter "onComplete"

    #@0
    .prologue
    .line 927
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 928
    :try_start_3
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@5
    if-eqz v1, :cond_e

    #@7
    .line 929
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@9
    invoke-virtual {v1, p1, p2, p3}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->changeIccLockPassword(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    #@c
    .line 936
    :cond_c
    monitor-exit v2

    #@d
    .line 937
    :goto_d
    return-void

    #@e
    .line 930
    :cond_e
    if-eqz p3, :cond_c

    #@10
    .line 931
    new-instance v0, Ljava/lang/RuntimeException;

    #@12
    const-string v1, "ICC card is absent."

    #@14
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@17
    .line 932
    .local v0, e:Ljava/lang/Exception;
    invoke-static {p3}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;)Landroid/os/AsyncResult;

    #@1a
    move-result-object v1

    #@1b
    iput-object v0, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@1d
    .line 933
    invoke-virtual {p3}, Landroid/os/Message;->sendToTarget()V

    #@20
    .line 934
    monitor-exit v2

    #@21
    goto :goto_d

    #@22
    .line 936
    .end local v0           #e:Ljava/lang/Exception;
    :catchall_22
    move-exception v1

    #@23
    monitor-exit v2
    :try_end_24
    .catchall {:try_start_3 .. :try_end_24} :catchall_22

    #@24
    throw v1
.end method

.method public dispose()V
    .registers 3

    #@0
    .prologue
    .line 125
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 126
    :try_start_3
    const-string v0, "Disposing"

    #@5
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/uicc/IccCardProxy;->log(Ljava/lang/String;)V

    #@8
    .line 128
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@a
    invoke-virtual {v0, p0}, Lcom/android/internal/telephony/uicc/UiccController;->unregisterForIccChanged(Landroid/os/Handler;)V

    #@d
    .line 129
    const/4 v0, 0x0

    #@e
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@10
    .line 130
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@12
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForOn(Landroid/os/Handler;)V

    #@15
    .line 131
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@17
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForOffOrNotAvailable(Landroid/os/Handler;)V

    #@1a
    .line 132
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mCdmaSSM:Lcom/android/internal/telephony/cdma/CdmaSubscriptionSourceManager;

    #@1c
    invoke-virtual {v0, p0}, Lcom/android/internal/telephony/cdma/CdmaSubscriptionSourceManager;->dispose(Landroid/os/Handler;)V

    #@1f
    .line 133
    monitor-exit v1

    #@20
    .line 134
    return-void

    #@21
    .line 133
    :catchall_21
    move-exception v0

    #@22
    monitor-exit v1
    :try_end_23
    .catchall {:try_start_3 .. :try_end_23} :catchall_21

    #@23
    throw v0
.end method

.method public getIccFdnAvailable()Z
    .registers 3

    #@0
    .prologue
    .line 881
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@2
    if-eqz v1, :cond_b

    #@4
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@6
    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getIccFdnAvailable()Z

    #@9
    move-result v0

    #@a
    .line 882
    .local v0, retValue:Z
    :goto_a
    return v0

    #@b
    .line 881
    .end local v0           #retValue:Z
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public getIccFdnEnabled()Z
    .registers 4

    #@0
    .prologue
    .line 873
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 874
    :try_start_3
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@5
    if-eqz v1, :cond_17

    #@7
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@9
    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getIccFdnEnabled()Z

    #@c
    move-result v1

    #@d
    :goto_d
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@10
    move-result-object v0

    #@11
    .line 876
    .local v0, retValue:Ljava/lang/Boolean;
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    #@14
    move-result v1

    #@15
    monitor-exit v2

    #@16
    return v1

    #@17
    .line 874
    .end local v0           #retValue:Ljava/lang/Boolean;
    :cond_17
    const/4 v1, 0x0

    #@18
    goto :goto_d

    #@19
    .line 877
    :catchall_19
    move-exception v1

    #@1a
    monitor-exit v2
    :try_end_1b
    .catchall {:try_start_3 .. :try_end_1b} :catchall_19

    #@1b
    throw v1
.end method

.method public getIccFileHandler()Lcom/android/internal/telephony/uicc/IccFileHandler;
    .registers 3

    #@0
    .prologue
    .line 714
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 715
    :try_start_3
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@5
    if-eqz v0, :cond_f

    #@7
    .line 716
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@9
    invoke-virtual {v0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getIccFileHandler()Lcom/android/internal/telephony/uicc/IccFileHandler;

    #@c
    move-result-object v0

    #@d
    monitor-exit v1

    #@e
    .line 718
    :goto_e
    return-object v0

    #@f
    :cond_f
    const/4 v0, 0x0

    #@10
    monitor-exit v1

    #@11
    goto :goto_e

    #@12
    .line 719
    :catchall_12
    move-exception v0

    #@13
    monitor-exit v1
    :try_end_14
    .catchall {:try_start_3 .. :try_end_14} :catchall_12

    #@14
    throw v0
.end method

.method public getIccLockEnabled()Z
    .registers 4

    #@0
    .prologue
    .line 863
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 865
    :try_start_3
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@5
    if-eqz v1, :cond_17

    #@7
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@9
    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getIccLockEnabled()Z

    #@c
    move-result v1

    #@d
    :goto_d
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@10
    move-result-object v0

    #@11
    .line 867
    .local v0, retValue:Ljava/lang/Boolean;
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    #@14
    move-result v1

    #@15
    monitor-exit v2

    #@16
    return v1

    #@17
    .line 865
    .end local v0           #retValue:Ljava/lang/Boolean;
    :cond_17
    const/4 v1, 0x0

    #@18
    goto :goto_d

    #@19
    .line 868
    :catchall_19
    move-exception v1

    #@1a
    monitor-exit v2
    :try_end_1b
    .catchall {:try_start_3 .. :try_end_1b} :catchall_19

    #@1b
    throw v1
.end method

.method public getIccPin1RetryCount()I
    .registers 3

    #@0
    .prologue
    .line 990
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@2
    if-eqz v1, :cond_b

    #@4
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@6
    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getIccPin1RetryCount()I

    #@9
    move-result v0

    #@a
    .line 991
    .local v0, retValue:I
    :goto_a
    return v0

    #@b
    .line 990
    .end local v0           #retValue:I
    :cond_b
    const/4 v0, -0x1

    #@c
    goto :goto_a
.end method

.method public getIccPin2Blocked()Z
    .registers 3

    #@0
    .prologue
    .line 887
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@2
    if-eqz v1, :cond_13

    #@4
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@6
    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getIccPin2Blocked()Z

    #@9
    move-result v1

    #@a
    :goto_a
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@d
    move-result-object v0

    #@e
    .line 888
    .local v0, retValue:Ljava/lang/Boolean;
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    #@11
    move-result v1

    #@12
    return v1

    #@13
    .line 887
    .end local v0           #retValue:Ljava/lang/Boolean;
    :cond_13
    const/4 v1, 0x0

    #@14
    goto :goto_a
.end method

.method public getIccPin2RetryCount()I
    .registers 3

    #@0
    .prologue
    .line 995
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@2
    if-eqz v1, :cond_b

    #@4
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@6
    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getIccPin2RetryCount()I

    #@9
    move-result v0

    #@a
    .line 996
    .local v0, retValue:I
    :goto_a
    return v0

    #@b
    .line 995
    .end local v0           #retValue:I
    :cond_b
    const/4 v0, -0x1

    #@c
    goto :goto_a
.end method

.method public getIccPuk1RetryCount()I
    .registers 2

    #@0
    .prologue
    .line 1017
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@6
    invoke-virtual {v0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getIccPuk1RetryCount()I

    #@9
    move-result v0

    #@a
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public getIccPuk2Blocked()Z
    .registers 3

    #@0
    .prologue
    .line 893
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@2
    if-eqz v1, :cond_13

    #@4
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@6
    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getIccPuk2Blocked()Z

    #@9
    move-result v1

    #@a
    :goto_a
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@d
    move-result-object v0

    #@e
    .line 894
    .local v0, retValue:Ljava/lang/Boolean;
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    #@11
    move-result v1

    #@12
    return v1

    #@13
    .line 893
    .end local v0           #retValue:Ljava/lang/Boolean;
    :cond_13
    const/4 v1, 0x0

    #@14
    goto :goto_a
.end method

.method public getIccPuk2RetryCount()I
    .registers 2

    #@0
    .prologue
    .line 1021
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@6
    invoke-virtual {v0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getIccPuk2RetryCount()I

    #@9
    move-result v0

    #@a
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public getIccRecords()Lcom/android/internal/telephony/uicc/IccRecords;
    .registers 3

    #@0
    .prologue
    .line 707
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 708
    :try_start_3
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@5
    monitor-exit v1

    #@6
    return-object v0

    #@7
    .line 709
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method public getIccRecordsLoaded()Z
    .registers 3

    #@0
    .prologue
    .line 657
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 658
    :try_start_3
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@5
    if-eqz v0, :cond_f

    #@7
    .line 659
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@9
    invoke-virtual {v0}, Lcom/android/internal/telephony/uicc/IccRecords;->getRecordsLoaded()Z

    #@c
    move-result v0

    #@d
    monitor-exit v1

    #@e
    .line 661
    :goto_e
    return v0

    #@f
    :cond_f
    const/4 v0, 0x0

    #@10
    monitor-exit v1

    #@11
    goto :goto_e

    #@12
    .line 662
    :catchall_12
    move-exception v0

    #@13
    monitor-exit v1
    :try_end_14
    .catchall {:try_start_3 .. :try_end_14} :catchall_12

    #@14
    throw v0
.end method

.method protected getIccStateIntentString(Lcom/android/internal/telephony/IccCardConstants$State;)Ljava/lang/String;
    .registers 4
    .parameter "state"

    #@0
    .prologue
    .line 666
    sget-object v0, Lcom/android/internal/telephony/uicc/IccCardProxy$1;->$SwitchMap$com$android$internal$telephony$IccCardConstants$State:[I

    #@2
    invoke-virtual {p1}, Lcom/android/internal/telephony/IccCardConstants$State;->ordinal()I

    #@5
    move-result v1

    #@6
    aget v0, v0, v1

    #@8
    packed-switch v0, :pswitch_data_2a

    #@b
    .line 678
    const-string v0, "UNKNOWN"

    #@d
    :goto_d
    return-object v0

    #@e
    .line 667
    :pswitch_e
    const-string v0, "ABSENT"

    #@10
    goto :goto_d

    #@11
    .line 668
    :pswitch_11
    const-string v0, "LOCKED"

    #@13
    goto :goto_d

    #@14
    .line 669
    :pswitch_14
    const-string v0, "LOCKED"

    #@16
    goto :goto_d

    #@17
    .line 670
    :pswitch_17
    const-string v0, "LOCKED"

    #@19
    goto :goto_d

    #@1a
    .line 671
    :pswitch_1a
    const-string v0, "READY"

    #@1c
    goto :goto_d

    #@1d
    .line 672
    :pswitch_1d
    const-string v0, "NOT_READY"

    #@1f
    goto :goto_d

    #@20
    .line 673
    :pswitch_20
    const-string v0, "LOCKED"

    #@22
    goto :goto_d

    #@23
    .line 674
    :pswitch_23
    const-string v0, "CARD_IO_ERROR"

    #@25
    goto :goto_d

    #@26
    .line 676
    :pswitch_26
    const-string v0, "SIM_REMOVED"

    #@28
    goto :goto_d

    #@29
    .line 666
    nop

    #@2a
    :pswitch_data_2a
    .packed-switch 0x1
        :pswitch_e
        :pswitch_11
        :pswitch_14
        :pswitch_17
        :pswitch_1a
        :pswitch_1d
        :pswitch_20
        :pswitch_23
        :pswitch_26
    .end packed-switch
.end method

.method protected getIccStateReason(Lcom/android/internal/telephony/IccCardConstants$State;)Ljava/lang/String;
    .registers 4
    .parameter "state"

    #@0
    .prologue
    .line 687
    sget-object v0, Lcom/android/internal/telephony/uicc/IccCardProxy$1;->$SwitchMap$com$android$internal$telephony$IccCardConstants$State:[I

    #@2
    invoke-virtual {p1}, Lcom/android/internal/telephony/IccCardConstants$State;->ordinal()I

    #@5
    move-result v1

    #@6
    aget v0, v0, v1

    #@8
    packed-switch v0, :pswitch_data_1c

    #@b
    .line 693
    :pswitch_b
    const/4 v0, 0x0

    #@c
    :goto_c
    return-object v0

    #@d
    .line 688
    :pswitch_d
    const-string v0, "PIN"

    #@f
    goto :goto_c

    #@10
    .line 689
    :pswitch_10
    const-string v0, "PUK"

    #@12
    goto :goto_c

    #@13
    .line 690
    :pswitch_13
    const-string v0, "PERSO"

    #@15
    goto :goto_c

    #@16
    .line 691
    :pswitch_16
    const-string v0, "PERM_DISABLED"

    #@18
    goto :goto_c

    #@19
    .line 692
    :pswitch_19
    const-string v0, "CARD_IO_ERROR"

    #@1b
    goto :goto_c

    #@1c
    .line 687
    :pswitch_data_1c
    .packed-switch 0x2
        :pswitch_d
        :pswitch_10
        :pswitch_13
        :pswitch_b
        :pswitch_b
        :pswitch_16
        :pswitch_19
    .end packed-switch
.end method

.method public getPin2State()Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;
    .registers 3

    #@0
    .prologue
    .line 1000
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@2
    if-eqz v1, :cond_b

    #@4
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@6
    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getPin2State()Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;

    #@9
    move-result-object v0

    #@a
    .line 1001
    .local v0, mPin2State:Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;
    :goto_a
    return-object v0

    #@b
    .line 1000
    .end local v0           #mPin2State:Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public getSIMLockStatus()Z
    .registers 8

    #@0
    .prologue
    .line 1092
    const/4 v2, 0x0

    #@1
    .line 1094
    .local v2, retValue:Z
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@4
    move-result-object v4

    #@5
    const-string v5, "SPR"

    #@7
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a
    move-result v4

    #@b
    if-nez v4, :cond_f

    #@d
    move v3, v2

    #@e
    .line 1127
    .end local v2           #retValue:Z
    .local v3, retValue:I
    :goto_e
    return v3

    #@f
    .line 1097
    .end local v3           #retValue:I
    .restart local v2       #retValue:Z
    :cond_f
    const-string v4, "ro.chameleon.SIMLock"

    #@11
    const-string v5, "0"

    #@13
    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@16
    move-result-object v0

    #@17
    .line 1098
    .local v0, SIMLock:Ljava/lang/String;
    const-string v4, "gsm.sim.type"

    #@19
    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    .line 1100
    .local v1, SIMTYPE:Ljava/lang/String;
    const-string v4, "0"

    #@1f
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@22
    move-result v4

    #@23
    if-eqz v4, :cond_3f

    #@25
    const-string v4, "spr"

    #@27
    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2a
    move-result v4

    #@2b
    if-nez v4, :cond_3f

    #@2d
    const-string v4, "test"

    #@2f
    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@32
    move-result v4

    #@33
    if-nez v4, :cond_3f

    #@35
    .line 1102
    const-string v4, "RIL_IccCardProxy"

    #@37
    const-string v5, "SIMLock in getSIMLockStatus() by sim type"

    #@39
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3c
    .line 1103
    const/4 v2, 0x1

    #@3d
    :goto_3d
    move v3, v2

    #@3e
    .line 1127
    .restart local v3       #retValue:I
    goto :goto_e

    #@3f
    .line 1108
    .end local v3           #retValue:I
    :cond_3f
    iget-object v4, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@41
    if-eqz v4, :cond_79

    #@43
    iget-object v4, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@45
    invoke-virtual {v4}, Lcom/android/internal/telephony/uicc/IccRecords;->getIMSI()Ljava/lang/String;

    #@48
    move-result-object v4

    #@49
    if-eqz v4, :cond_79

    #@4b
    .line 1110
    const-string v4, "1"

    #@4d
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@50
    move-result v4

    #@51
    if-eqz v4, :cond_70

    #@53
    iget-object v4, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@55
    invoke-virtual {v4}, Lcom/android/internal/telephony/uicc/IccRecords;->getIMSI()Ljava/lang/String;

    #@58
    move-result-object v4

    #@59
    const/4 v5, 0x0

    #@5a
    const/4 v6, 0x3

    #@5b
    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@5e
    move-result-object v4

    #@5f
    const-string v5, "310"

    #@61
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@64
    move-result v4

    #@65
    if-eqz v4, :cond_70

    #@67
    .line 1112
    const-string v4, "RIL_IccCardProxy"

    #@69
    const-string v5, "the device will not allow an international UICC to be used, MCC of the USIM/IMSI is equal to 310"

    #@6b
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@6e
    .line 1113
    const/4 v2, 0x1

    #@6f
    goto :goto_3d

    #@70
    .line 1117
    :cond_70
    const-string v4, "RIL_IccCardProxy"

    #@72
    const-string v5, "the device will allow an UICC to be used"

    #@74
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@77
    .line 1118
    const/4 v2, 0x0

    #@78
    goto :goto_3d

    #@79
    .line 1123
    :cond_79
    const-string v4, "RIL_IccCardProxy"

    #@7b
    const-string v5, "IccRecords == null or IMSI == null"

    #@7d
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@80
    .line 1124
    const/4 v2, 0x0

    #@81
    goto :goto_3d
.end method

.method public getServiceProviderName()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 955
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 956
    :try_start_3
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@5
    if-eqz v0, :cond_f

    #@7
    .line 957
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@9
    invoke-virtual {v0}, Lcom/android/internal/telephony/uicc/IccRecords;->getServiceProviderName()Ljava/lang/String;

    #@c
    move-result-object v0

    #@d
    monitor-exit v1

    #@e
    .line 959
    :goto_e
    return-object v0

    #@f
    :cond_f
    const/4 v0, 0x0

    #@10
    monitor-exit v1

    #@11
    goto :goto_e

    #@12
    .line 960
    :catchall_12
    move-exception v0

    #@13
    monitor-exit v1
    :try_end_14
    .catchall {:try_start_3 .. :try_end_14} :catchall_12

    #@14
    throw v0
.end method

.method public getState()Lcom/android/internal/telephony/IccCardConstants$State;
    .registers 3

    #@0
    .prologue
    .line 700
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 701
    :try_start_3
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mExternalState:Lcom/android/internal/telephony/IccCardConstants$State;

    #@5
    monitor-exit v1

    #@6
    return-object v0

    #@7
    .line 702
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 7
    .parameter "msg"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    const/4 v2, 0x0

    #@3
    .line 231
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    const-string v1, "SPR"

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c
    move-result v0

    #@d
    if-eqz v0, :cond_23

    #@f
    const-string v0, "gsm.sim.state"

    #@11
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    const-string v1, "ABSENT"

    #@17
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1a
    move-result v0

    #@1b
    if-eqz v0, :cond_23

    #@1d
    .line 234
    const-string v0, "SIMLock is activated. do not handle message."

    #@1f
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/uicc/IccCardProxy;->log(Ljava/lang/String;)V

    #@22
    .line 314
    :cond_22
    :goto_22
    return-void

    #@23
    .line 240
    :cond_23
    iget v0, p1, Landroid/os/Message;->what:I

    #@25
    packed-switch v0, :pswitch_data_106

    #@28
    .line 311
    :pswitch_28
    new-instance v0, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v1, "Unhandled message with number: "

    #@2f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v0

    #@33
    iget v1, p1, Landroid/os/Message;->what:I

    #@35
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@38
    move-result-object v0

    #@39
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v0

    #@3d
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/uicc/IccCardProxy;->loge(Ljava/lang/String;)V

    #@40
    goto :goto_22

    #@41
    .line 242
    :pswitch_41
    iput-boolean v3, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mRadioOn:Z

    #@43
    .line 243
    sget-object v0, Lcom/android/internal/telephony/CommandsInterface$RadioState;->RADIO_UNAVAILABLE:Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@45
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@47
    invoke-interface {v1}, Lcom/android/internal/telephony/CommandsInterface;->getRadioState()Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@4a
    move-result-object v1

    #@4b
    if-ne v0, v1, :cond_22

    #@4d
    .line 244
    sget-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->NOT_READY:Lcom/android/internal/telephony/IccCardConstants$State;

    #@4f
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/uicc/IccCardProxy;->setExternalState(Lcom/android/internal/telephony/IccCardConstants$State;)V

    #@52
    goto :goto_22

    #@53
    .line 248
    :pswitch_53
    iput-boolean v4, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mRadioOn:Z

    #@55
    .line 249
    iget-boolean v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mInitialized:Z

    #@57
    if-nez v0, :cond_22

    #@59
    .line 250
    invoke-direct {p0}, Lcom/android/internal/telephony/uicc/IccCardProxy;->updateQuietMode()V

    #@5c
    goto :goto_22

    #@5d
    .line 254
    :pswitch_5d
    iget-boolean v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mInitialized:Z

    #@5f
    if-eqz v0, :cond_64

    #@61
    .line 255
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/IccCardProxy;->updateIccAvailability()V

    #@64
    .line 258
    :cond_64
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccCard:Lcom/android/internal/telephony/uicc/UiccCard;

    #@66
    if-eqz v0, :cond_22

    #@68
    sget-object v0, Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;->CARDSTATE_REMOVED:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    #@6a
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccCard:Lcom/android/internal/telephony/uicc/UiccCard;

    #@6c
    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/UiccCard;->getCardState()Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    #@6f
    move-result-object v1

    #@70
    if-ne v0, v1, :cond_22

    #@72
    .line 259
    sget-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->SIM_REMOVED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@74
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/uicc/IccCardProxy;->setExternalState(Lcom/android/internal/telephony/IccCardConstants$State;)V

    #@77
    .line 262
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@7a
    move-result-object v0

    #@7b
    const-string v1, "VZW"

    #@7d
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@80
    move-result v0

    #@81
    if-nez v0, :cond_9b

    #@83
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@86
    move-result-object v0

    #@87
    const-string v1, "ATT"

    #@89
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8c
    move-result v0

    #@8d
    if-nez v0, :cond_9b

    #@8f
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@92
    move-result-object v0

    #@93
    const-string v1, "LGU"

    #@95
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@98
    move-result v0

    #@99
    if-eqz v0, :cond_a0

    #@9b
    .line 264
    :cond_9b
    const-string v0, "ABSENT"

    #@9d
    invoke-virtual {p0, v0, v2}, Lcom/android/internal/telephony/uicc/IccCardProxy;->broadcastIccStateChangedIntent(Ljava/lang/String;Ljava/lang/String;)V

    #@a0
    .line 269
    :cond_a0
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@a3
    move-result-object v0

    #@a4
    const-string v1, "SPR"

    #@a6
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a9
    move-result v0

    #@aa
    if-eqz v0, :cond_b5

    #@ac
    .line 270
    iput-boolean v3, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mQuietMode:Z

    #@ae
    .line 275
    :goto_ae
    sget-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->ABSENT:Lcom/android/internal/telephony/IccCardConstants$State;

    #@b0
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/uicc/IccCardProxy;->setExternalState(Lcom/android/internal/telephony/IccCardConstants$State;)V

    #@b3
    goto/16 :goto_22

    #@b5
    .line 273
    :cond_b5
    iput-boolean v4, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mQuietMode:Z

    #@b7
    goto :goto_ae

    #@b8
    .line 280
    :pswitch_b8
    invoke-direct {p0}, Lcom/android/internal/telephony/uicc/IccCardProxy;->processLockedState()V

    #@bb
    goto/16 :goto_22

    #@bd
    .line 283
    :pswitch_bd
    sget-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->READY:Lcom/android/internal/telephony/IccCardConstants$State;

    #@bf
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/uicc/IccCardProxy;->setExternalState(Lcom/android/internal/telephony/IccCardConstants$State;)V

    #@c2
    goto/16 :goto_22

    #@c4
    .line 286
    :pswitch_c4
    const-string v0, "LOADED"

    #@c6
    invoke-virtual {p0, v0, v2}, Lcom/android/internal/telephony/uicc/IccCardProxy;->broadcastIccStateChangedIntent(Ljava/lang/String;Ljava/lang/String;)V

    #@c9
    goto/16 :goto_22

    #@cb
    .line 290
    :pswitch_cb
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/IccCardProxy;->getSIMLockStatus()Z

    #@ce
    move-result v0

    #@cf
    if-eqz v0, :cond_dd

    #@d1
    .line 292
    const-string v0, "SIMLock is activated by non-Sprint SIM. SIM state is changed to ABSENT."

    #@d3
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/uicc/IccCardProxy;->log(Ljava/lang/String;)V

    #@d6
    .line 293
    sget-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->ABSENT:Lcom/android/internal/telephony/IccCardConstants$State;

    #@d8
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/uicc/IccCardProxy;->setExternalState(Lcom/android/internal/telephony/IccCardConstants$State;)V

    #@db
    goto/16 :goto_22

    #@dd
    .line 297
    :cond_dd
    const-string v0, "IMSI"

    #@df
    invoke-virtual {p0, v0, v2}, Lcom/android/internal/telephony/uicc/IccCardProxy;->broadcastIccStateChangedIntent(Ljava/lang/String;Ljava/lang/String;)V

    #@e2
    goto/16 :goto_22

    #@e4
    .line 301
    :pswitch_e4
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@e6
    invoke-virtual {v0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getPersoSubState()Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;

    #@e9
    move-result-object v0

    #@ea
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mPersoSubState:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;

    #@ec
    .line 302
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mPersoSubState:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;

    #@ee
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/uicc/IccCardProxy;->broadcastPersoSubState(Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;)V

    #@f1
    .line 303
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mPersoLockedRegistrants:Landroid/os/RegistrantList;

    #@f3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@f5
    check-cast v0, Landroid/os/AsyncResult;

    #@f7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->notifyRegistrants(Landroid/os/AsyncResult;)V

    #@fa
    .line 304
    sget-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->PERSO_LOCKED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@fc
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/uicc/IccCardProxy;->setExternalState(Lcom/android/internal/telephony/IccCardConstants$State;)V

    #@ff
    goto/16 :goto_22

    #@101
    .line 308
    :pswitch_101
    invoke-direct {p0}, Lcom/android/internal/telephony/uicc/IccCardProxy;->updateQuietMode()V

    #@104
    goto/16 :goto_22

    #@106
    .line 240
    :pswitch_data_106
    .packed-switch 0x1
        :pswitch_41
        :pswitch_53
        :pswitch_5d
        :pswitch_28
        :pswitch_b8
        :pswitch_bd
        :pswitch_c4
        :pswitch_cb
        :pswitch_e4
        :pswitch_28
        :pswitch_101
    .end packed-switch
.end method

.method public hasIccCard()Z
    .registers 4

    #@0
    .prologue
    .line 973
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 974
    :try_start_3
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccCard:Lcom/android/internal/telephony/uicc/UiccCard;

    #@5
    if-eqz v0, :cond_14

    #@7
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccCard:Lcom/android/internal/telephony/uicc/UiccCard;

    #@9
    invoke-virtual {v0}, Lcom/android/internal/telephony/uicc/UiccCard;->getCardState()Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    #@c
    move-result-object v0

    #@d
    sget-object v2, Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;->CARDSTATE_ABSENT:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    #@f
    if-eq v0, v2, :cond_14

    #@11
    .line 975
    const/4 v0, 0x1

    #@12
    monitor-exit v1

    #@13
    .line 977
    :goto_13
    return v0

    #@14
    :cond_14
    const/4 v0, 0x0

    #@15
    monitor-exit v1

    #@16
    goto :goto_13

    #@17
    .line 978
    :catchall_17
    move-exception v0

    #@18
    monitor-exit v1
    :try_end_19
    .catchall {:try_start_3 .. :try_end_19} :catchall_17

    #@19
    throw v0
.end method

.method public is2GSIM()Z
    .registers 4

    #@0
    .prologue
    .line 1025
    const/4 v0, 0x0

    #@1
    .line 1027
    .local v0, retValue:Z
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@3
    if-eqz v1, :cond_10

    #@5
    .line 1028
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@7
    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getType()Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;

    #@a
    move-result-object v1

    #@b
    sget-object v2, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;->APPTYPE_SIM:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;

    #@d
    if-ne v1, v2, :cond_10

    #@f
    .line 1029
    const/4 v0, 0x1

    #@10
    .line 1032
    :cond_10
    return v0
.end method

.method public isApplicationOnIcc(Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;)Z
    .registers 5
    .parameter "type"

    #@0
    .prologue
    .line 965
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 966
    :try_start_3
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccCard:Lcom/android/internal/telephony/uicc/UiccCard;

    #@5
    if-eqz v1, :cond_17

    #@7
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccCard:Lcom/android/internal/telephony/uicc/UiccCard;

    #@9
    invoke-virtual {v1, p1}, Lcom/android/internal/telephony/uicc/UiccCard;->isApplicationOnIcc(Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;)Z

    #@c
    move-result v1

    #@d
    :goto_d
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@10
    move-result-object v0

    #@11
    .line 967
    .local v0, retValue:Ljava/lang/Boolean;
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    #@14
    move-result v1

    #@15
    monitor-exit v2

    #@16
    return v1

    #@17
    .line 966
    .end local v0           #retValue:Ljava/lang/Boolean;
    :cond_17
    const/4 v1, 0x0

    #@18
    goto :goto_d

    #@19
    .line 968
    :catchall_19
    move-exception v1

    #@1a
    monitor-exit v2
    :try_end_1b
    .catchall {:try_start_3 .. :try_end_1b} :catchall_19

    #@1b
    throw v1
.end method

.method protected log(Ljava/lang/String;)V
    .registers 3
    .parameter "s"

    #@0
    .prologue
    .line 982
    const-string v0, "RIL_IccCardProxy"

    #@2
    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 983
    return-void
.end method

.method protected loge(Ljava/lang/String;)V
    .registers 3
    .parameter "msg"

    #@0
    .prologue
    .line 986
    const-string v0, "RIL_IccCardProxy"

    #@2
    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 987
    return-void
.end method

.method public reReadISimRecords()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1067
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@3
    if-eqz v2, :cond_28

    #@5
    .line 1069
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@7
    const/4 v3, 0x3

    #@8
    invoke-virtual {v2, v3}, Lcom/android/internal/telephony/uicc/UiccController;->getIccRecords(I)Lcom/android/internal/telephony/uicc/IccRecords;

    #@b
    move-result-object v0

    #@c
    check-cast v0, Lcom/android/internal/telephony/uicc/IsimUiccRecords;

    #@e
    .line 1070
    .local v0, isimRecords:Lcom/android/internal/telephony/uicc/IsimUiccRecords;
    if-eqz v0, :cond_20

    #@10
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/IccCardProxy;->getState()Lcom/android/internal/telephony/IccCardConstants$State;

    #@13
    move-result-object v2

    #@14
    sget-object v3, Lcom/android/internal/telephony/IccCardConstants$State;->READY:Lcom/android/internal/telephony/IccCardConstants$State;

    #@16
    if-ne v2, v3, :cond_20

    #@18
    .line 1072
    invoke-virtual {v0}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->clearISimRecords()V

    #@1b
    .line 1073
    invoke-virtual {v0}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->fetchIsimRecords()V

    #@1e
    .line 1074
    const/4 v1, 0x1

    #@1f
    .line 1085
    .end local v0           #isimRecords:Lcom/android/internal/telephony/uicc/IsimUiccRecords;
    :goto_1f
    return v1

    #@20
    .line 1078
    .restart local v0       #isimRecords:Lcom/android/internal/telephony/uicc/IsimUiccRecords;
    :cond_20
    const-string v2, "RIL_IccCardProxy"

    #@22
    const-string v3, "isimRecords is null in reReadISimRecords()"

    #@24
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    goto :goto_1f

    #@28
    .line 1084
    .end local v0           #isimRecords:Lcom/android/internal/telephony/uicc/IsimUiccRecords;
    :cond_28
    const-string v2, "RIL_IccCardProxy"

    #@2a
    const-string v3, "mUiccController is null in reReadISimRecords()"

    #@2c
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    goto :goto_1f
.end method

.method public registerForAbsent(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 8
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 727
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 728
    :try_start_3
    new-instance v0, Landroid/os/Registrant;

    #@5
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@8
    .line 730
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mAbsentRegistrants:Landroid/os/RegistrantList;

    #@a
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@d
    .line 732
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/IccCardProxy;->getState()Lcom/android/internal/telephony/IccCardConstants$State;

    #@10
    move-result-object v1

    #@11
    sget-object v3, Lcom/android/internal/telephony/IccCardConstants$State;->ABSENT:Lcom/android/internal/telephony/IccCardConstants$State;

    #@13
    if-ne v1, v3, :cond_18

    #@15
    .line 733
    invoke-virtual {v0}, Landroid/os/Registrant;->notifyRegistrant()V

    #@18
    .line 735
    :cond_18
    monitor-exit v2

    #@19
    .line 736
    return-void

    #@1a
    .line 735
    .end local v0           #r:Landroid/os/Registrant;
    :catchall_1a
    move-exception v1

    #@1b
    monitor-exit v2
    :try_end_1c
    .catchall {:try_start_3 .. :try_end_1c} :catchall_1a

    #@1c
    throw v1
.end method

.method public registerForLocked(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 7
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 773
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 774
    :try_start_3
    new-instance v0, Landroid/os/Registrant;

    #@5
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@8
    .line 776
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mPinLockedRegistrants:Landroid/os/RegistrantList;

    #@a
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@d
    .line 778
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/IccCardProxy;->getState()Lcom/android/internal/telephony/IccCardConstants$State;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Lcom/android/internal/telephony/IccCardConstants$State;->isPinLocked()Z

    #@14
    move-result v1

    #@15
    if-eqz v1, :cond_1a

    #@17
    .line 779
    invoke-virtual {v0}, Landroid/os/Registrant;->notifyRegistrant()V

    #@1a
    .line 781
    :cond_1a
    monitor-exit v2

    #@1b
    .line 782
    return-void

    #@1c
    .line 781
    .end local v0           #r:Landroid/os/Registrant;
    :catchall_1c
    move-exception v1

    #@1d
    monitor-exit v2
    :try_end_1e
    .catchall {:try_start_3 .. :try_end_1e} :catchall_1c

    #@1e
    throw v1
.end method

.method public registerForPersoLocked(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 10
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 750
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 751
    :try_start_3
    new-instance v0, Landroid/os/Registrant;

    #@5
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@8
    .line 753
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mPersoLockedRegistrants:Landroid/os/RegistrantList;

    #@a
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@d
    .line 755
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/IccCardProxy;->getState()Lcom/android/internal/telephony/IccCardConstants$State;

    #@10
    move-result-object v1

    #@11
    sget-object v3, Lcom/android/internal/telephony/IccCardConstants$State;->PERSO_LOCKED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@13
    if-ne v1, v3, :cond_29

    #@15
    .line 756
    new-instance v1, Landroid/os/AsyncResult;

    #@17
    const/4 v3, 0x0

    #@18
    iget-object v4, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mPersoSubState:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;

    #@1a
    invoke-virtual {v4}, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;->ordinal()I

    #@1d
    move-result v4

    #@1e
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@21
    move-result-object v4

    #@22
    const/4 v5, 0x0

    #@23
    invoke-direct {v1, v3, v4, v5}, Landroid/os/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@26
    invoke-virtual {v0, v1}, Landroid/os/Registrant;->notifyRegistrant(Landroid/os/AsyncResult;)V

    #@29
    .line 758
    :cond_29
    monitor-exit v2

    #@2a
    .line 759
    return-void

    #@2b
    .line 758
    .end local v0           #r:Landroid/os/Registrant;
    :catchall_2b
    move-exception v1

    #@2c
    monitor-exit v2
    :try_end_2d
    .catchall {:try_start_3 .. :try_end_2d} :catchall_2b

    #@2d
    throw v1
.end method

.method protected registerUiccCardEvents()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 427
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccCard:Lcom/android/internal/telephony/uicc/UiccCard;

    #@3
    if-eqz v0, :cond_b

    #@5
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccCard:Lcom/android/internal/telephony/uicc/UiccCard;

    #@7
    const/4 v1, 0x4

    #@8
    invoke-virtual {v0, p0, v1, v2}, Lcom/android/internal/telephony/uicc/UiccCard;->registerForAbsent(Landroid/os/Handler;ILjava/lang/Object;)V

    #@b
    .line 428
    :cond_b
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@d
    if-eqz v0, :cond_22

    #@f
    .line 429
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@11
    const/4 v1, 0x6

    #@12
    invoke-virtual {v0, p0, v1, v2}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->registerForReady(Landroid/os/Handler;ILjava/lang/Object;)V

    #@15
    .line 430
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@17
    const/4 v1, 0x5

    #@18
    invoke-virtual {v0, p0, v1, v2}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->registerForLocked(Landroid/os/Handler;ILjava/lang/Object;)V

    #@1b
    .line 431
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@1d
    const/16 v1, 0x9

    #@1f
    invoke-virtual {v0, p0, v1, v2}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->registerForPersoLocked(Landroid/os/Handler;ILjava/lang/Object;)V

    #@22
    .line 433
    :cond_22
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@24
    if-eqz v0, :cond_33

    #@26
    .line 434
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@28
    const/16 v1, 0x8

    #@2a
    invoke-virtual {v0, p0, v1, v2}, Lcom/android/internal/telephony/uicc/IccRecords;->registerForImsiReady(Landroid/os/Handler;ILjava/lang/Object;)V

    #@2d
    .line 435
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@2f
    const/4 v1, 0x7

    #@30
    invoke-virtual {v0, p0, v1, v2}, Lcom/android/internal/telephony/uicc/IccRecords;->registerForRecordsLoaded(Landroid/os/Handler;ILjava/lang/Object;)V

    #@33
    .line 437
    :cond_33
    return-void
.end method

.method protected setExternalState(Lcom/android/internal/telephony/IccCardConstants$State;Z)V
    .registers 9
    .parameter "newState"
    .parameter "override"

    #@0
    .prologue
    .line 604
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v3

    #@3
    .line 605
    if-nez p2, :cond_b

    #@5
    :try_start_5
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mExternalState:Lcom/android/internal/telephony/IccCardConstants$State;

    #@7
    if-ne p1, v2, :cond_b

    #@9
    .line 606
    monitor-exit v3

    #@a
    .line 650
    :goto_a
    return-void

    #@b
    .line 608
    :cond_b
    iput-object p1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mExternalState:Lcom/android/internal/telephony/IccCardConstants$State;

    #@d
    .line 609
    const-string v2, "gsm.sim.state"

    #@f
    iget-object v4, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mExternalState:Lcom/android/internal/telephony/IccCardConstants$State;

    #@11
    invoke-virtual {v4}, Lcom/android/internal/telephony/IccCardConstants$State;->toString()Ljava/lang/String;

    #@14
    move-result-object v4

    #@15
    invoke-static {v2, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@18
    .line 620
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@1a
    if-eqz v2, :cond_32

    #@1c
    .line 622
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@1e
    const/4 v4, 0x3

    #@1f
    invoke-virtual {v2, v4}, Lcom/android/internal/telephony/uicc/UiccController;->getIccRecords(I)Lcom/android/internal/telephony/uicc/IccRecords;

    #@22
    move-result-object v1

    #@23
    check-cast v1, Lcom/android/internal/telephony/uicc/IsimUiccRecords;

    #@25
    .line 623
    .local v1, isimRecords:Lcom/android/internal/telephony/uicc/IsimUiccRecords;
    if-eqz v1, :cond_70

    #@27
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/IccCardProxy;->getState()Lcom/android/internal/telephony/IccCardConstants$State;

    #@2a
    move-result-object v2

    #@2b
    sget-object v4, Lcom/android/internal/telephony/IccCardConstants$State;->READY:Lcom/android/internal/telephony/IccCardConstants$State;

    #@2d
    if-ne v2, v4, :cond_70

    #@2f
    .line 624
    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/IsimUiccRecords;->fetchIsimRecords()V

    #@32
    .line 632
    .end local v1           #isimRecords:Lcom/android/internal/telephony/uicc/IsimUiccRecords;
    :cond_32
    :goto_32
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@34
    if-eqz v2, :cond_78

    #@36
    .line 634
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@38
    invoke-virtual {v2}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getHiddenResetFlag()Z

    #@3b
    move-result v0

    #@3c
    .line 635
    .local v0, ishreset:Z
    const/4 v2, 0x1

    #@3d
    if-ne v0, v2, :cond_78

    #@3f
    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->PIN_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@41
    if-ne p1, v2, :cond_78

    #@43
    .line 637
    const-string v2, "RIL_IccCardProxy"

    #@45
    new-instance v4, Ljava/lang/StringBuilder;

    #@47
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@4a
    const-string v5, "[LGE] skip pin require intent : "

    #@4c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v4

    #@50
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v4

    #@54
    const-string v5, " hidden reset flag : "

    #@56
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v4

    #@5a
    iget-object v5, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@5c
    invoke-virtual {v5}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getHiddenResetFlag()Z

    #@5f
    move-result v5

    #@60
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@63
    move-result-object v4

    #@64
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@67
    move-result-object v4

    #@68
    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@6b
    .line 639
    monitor-exit v3

    #@6c
    goto :goto_a

    #@6d
    .line 649
    .end local v0           #ishreset:Z
    :catchall_6d
    move-exception v2

    #@6e
    monitor-exit v3
    :try_end_6f
    .catchall {:try_start_5 .. :try_end_6f} :catchall_6d

    #@6f
    throw v2

    #@70
    .line 626
    .restart local v1       #isimRecords:Lcom/android/internal/telephony/uicc/IsimUiccRecords;
    :cond_70
    :try_start_70
    const-string v2, "RIL_IccCardProxy"

    #@72
    const-string v4, "isimRecords is null in setExternalState()"

    #@74
    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@77
    goto :goto_32

    #@78
    .line 643
    .end local v1           #isimRecords:Lcom/android/internal/telephony/uicc/IsimUiccRecords;
    :cond_78
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mExternalState:Lcom/android/internal/telephony/IccCardConstants$State;

    #@7a
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/uicc/IccCardProxy;->getIccStateIntentString(Lcom/android/internal/telephony/IccCardConstants$State;)Ljava/lang/String;

    #@7d
    move-result-object v2

    #@7e
    iget-object v4, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mExternalState:Lcom/android/internal/telephony/IccCardConstants$State;

    #@80
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/uicc/IccCardProxy;->getIccStateReason(Lcom/android/internal/telephony/IccCardConstants$State;)Ljava/lang/String;

    #@83
    move-result-object v4

    #@84
    invoke-virtual {p0, v2, v4}, Lcom/android/internal/telephony/uicc/IccCardProxy;->broadcastIccStateChangedIntent(Ljava/lang/String;Ljava/lang/String;)V

    #@87
    .line 646
    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->ABSENT:Lcom/android/internal/telephony/IccCardConstants$State;

    #@89
    iget-object v4, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mExternalState:Lcom/android/internal/telephony/IccCardConstants$State;

    #@8b
    if-ne v2, v4, :cond_92

    #@8d
    .line 647
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mAbsentRegistrants:Landroid/os/RegistrantList;

    #@8f
    invoke-virtual {v2}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@92
    .line 649
    :cond_92
    monitor-exit v3
    :try_end_93
    .catchall {:try_start_70 .. :try_end_93} :catchall_6d

    #@93
    goto/16 :goto_a
.end method

.method public setIccFdnEnabled(ZLjava/lang/String;Landroid/os/Message;)V
    .registers 7
    .parameter "enabled"
    .parameter "password"
    .parameter "onComplete"

    #@0
    .prologue
    .line 913
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 914
    :try_start_3
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@5
    if-eqz v1, :cond_e

    #@7
    .line 915
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@9
    invoke-virtual {v1, p1, p2, p3}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->setIccFdnEnabled(ZLjava/lang/String;Landroid/os/Message;)V

    #@c
    .line 922
    :cond_c
    monitor-exit v2

    #@d
    .line 923
    :goto_d
    return-void

    #@e
    .line 916
    :cond_e
    if-eqz p3, :cond_c

    #@10
    .line 917
    new-instance v0, Ljava/lang/RuntimeException;

    #@12
    const-string v1, "ICC card is absent."

    #@14
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@17
    .line 918
    .local v0, e:Ljava/lang/Exception;
    invoke-static {p3}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;)Landroid/os/AsyncResult;

    #@1a
    move-result-object v1

    #@1b
    iput-object v0, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@1d
    .line 919
    invoke-virtual {p3}, Landroid/os/Message;->sendToTarget()V

    #@20
    .line 920
    monitor-exit v2

    #@21
    goto :goto_d

    #@22
    .line 922
    .end local v0           #e:Ljava/lang/Exception;
    :catchall_22
    move-exception v1

    #@23
    monitor-exit v2
    :try_end_24
    .catchall {:try_start_3 .. :try_end_24} :catchall_22

    #@24
    throw v1
.end method

.method public setIccLockEnabled(ZLjava/lang/String;Landroid/os/Message;)V
    .registers 7
    .parameter "enabled"
    .parameter "password"
    .parameter "onComplete"

    #@0
    .prologue
    .line 899
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 900
    :try_start_3
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@5
    if-eqz v1, :cond_e

    #@7
    .line 901
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@9
    invoke-virtual {v1, p1, p2, p3}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->setIccLockEnabled(ZLjava/lang/String;Landroid/os/Message;)V

    #@c
    .line 908
    :cond_c
    monitor-exit v2

    #@d
    .line 909
    :goto_d
    return-void

    #@e
    .line 902
    :cond_e
    if-eqz p3, :cond_c

    #@10
    .line 903
    new-instance v0, Ljava/lang/RuntimeException;

    #@12
    const-string v1, "ICC card is absent."

    #@14
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@17
    .line 904
    .local v0, e:Ljava/lang/Exception;
    invoke-static {p3}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;)Landroid/os/AsyncResult;

    #@1a
    move-result-object v1

    #@1b
    iput-object v0, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@1d
    .line 905
    invoke-virtual {p3}, Landroid/os/Message;->sendToTarget()V

    #@20
    .line 906
    monitor-exit v2

    #@21
    goto :goto_d

    #@22
    .line 908
    .end local v0           #e:Ljava/lang/Exception;
    :catchall_22
    move-exception v1

    #@23
    monitor-exit v2
    :try_end_24
    .catchall {:try_start_3 .. :try_end_24} :catchall_22

    #@24
    throw v1
.end method

.method public setVoiceRadioTech(I)V
    .registers 5
    .parameter "radioTech"

    #@0
    .prologue
    .line 141
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 143
    :try_start_3
    new-instance v0, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v2, "Setting radio tech "

    #@a
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v0

    #@e
    invoke-static {p1}, Landroid/telephony/ServiceState;->rilRadioTechnologyToString(I)Ljava/lang/String;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v0

    #@16
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v0

    #@1a
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/uicc/IccCardProxy;->log(Ljava/lang/String;)V

    #@1d
    .line 145
    invoke-static {p1}, Landroid/telephony/ServiceState;->isGsm(I)Z

    #@20
    move-result v0

    #@21
    if-eqz v0, :cond_2b

    #@23
    .line 146
    const/4 v0, 0x1

    #@24
    iput v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mCurrentAppType:I

    #@26
    .line 156
    :goto_26
    invoke-direct {p0}, Lcom/android/internal/telephony/uicc/IccCardProxy;->updateQuietMode()V

    #@29
    .line 157
    monitor-exit v1

    #@2a
    .line 158
    return-void

    #@2b
    .line 147
    :cond_2b
    invoke-static {p1}, Landroid/telephony/ServiceState;->isCdma(I)Z

    #@2e
    move-result v0

    #@2f
    if-eqz v0, :cond_38

    #@31
    .line 148
    const/4 v0, 0x2

    #@32
    iput v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mCurrentAppType:I

    #@34
    goto :goto_26

    #@35
    .line 157
    :catchall_35
    move-exception v0

    #@36
    monitor-exit v1
    :try_end_37
    .catchall {:try_start_3 .. :try_end_37} :catchall_35

    #@37
    throw v0

    #@38
    .line 151
    :cond_38
    :try_start_38
    invoke-direct {p0}, Lcom/android/internal/telephony/uicc/IccCardProxy;->guessRadioTech()I

    #@3b
    move-result v0

    #@3c
    iput v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mCurrentAppType:I

    #@3e
    .line 153
    new-instance v0, Ljava/lang/StringBuilder;

    #@40
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@43
    const-string v2, "Radio tech is unknown. Guessed radio tech family is "

    #@45
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v0

    #@49
    iget v2, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mCurrentAppType:I

    #@4b
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v0

    #@4f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v0

    #@53
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/uicc/IccCardProxy;->log(Ljava/lang/String;)V
    :try_end_56
    .catchall {:try_start_38 .. :try_end_56} :catchall_35

    #@56
    goto :goto_26
.end method

.method public supplyDepersonalization(Ljava/lang/String;ILandroid/os/Message;)V
    .registers 7
    .parameter "pin"
    .parameter "type"
    .parameter "onComplete"

    #@0
    .prologue
    .line 849
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 850
    :try_start_3
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@5
    if-eqz v1, :cond_e

    #@7
    .line 851
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@9
    invoke-virtual {v1, p1, p2, p3}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->supplyDepersonalization(Ljava/lang/String;ILandroid/os/Message;)V

    #@c
    .line 858
    :cond_c
    monitor-exit v2

    #@d
    .line 859
    :goto_d
    return-void

    #@e
    .line 852
    :cond_e
    if-eqz p3, :cond_c

    #@10
    .line 853
    new-instance v0, Ljava/lang/RuntimeException;

    #@12
    const-string v1, "CommandsInterface is not set."

    #@14
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@17
    .line 854
    .local v0, e:Ljava/lang/Exception;
    invoke-static {p3}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;)Landroid/os/AsyncResult;

    #@1a
    move-result-object v1

    #@1b
    iput-object v0, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@1d
    .line 855
    invoke-virtual {p3}, Landroid/os/Message;->sendToTarget()V

    #@20
    .line 856
    monitor-exit v2

    #@21
    goto :goto_d

    #@22
    .line 858
    .end local v0           #e:Ljava/lang/Exception;
    :catchall_22
    move-exception v1

    #@23
    monitor-exit v2
    :try_end_24
    .catchall {:try_start_3 .. :try_end_24} :catchall_22

    #@24
    throw v1
.end method

.method public supplyPin(Ljava/lang/String;Landroid/os/Message;)V
    .registers 6
    .parameter "pin"
    .parameter "onComplete"

    #@0
    .prologue
    .line 793
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 794
    :try_start_3
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@5
    if-eqz v1, :cond_e

    #@7
    .line 795
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@9
    invoke-virtual {v1, p1, p2}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->supplyPin(Ljava/lang/String;Landroid/os/Message;)V

    #@c
    .line 802
    :cond_c
    monitor-exit v2

    #@d
    .line 803
    :goto_d
    return-void

    #@e
    .line 796
    :cond_e
    if-eqz p2, :cond_c

    #@10
    .line 797
    new-instance v0, Ljava/lang/RuntimeException;

    #@12
    const-string v1, "ICC card is absent."

    #@14
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@17
    .line 798
    .local v0, e:Ljava/lang/Exception;
    invoke-static {p2}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;)Landroid/os/AsyncResult;

    #@1a
    move-result-object v1

    #@1b
    iput-object v0, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@1d
    .line 799
    invoke-virtual {p2}, Landroid/os/Message;->sendToTarget()V

    #@20
    .line 800
    monitor-exit v2

    #@21
    goto :goto_d

    #@22
    .line 802
    .end local v0           #e:Ljava/lang/Exception;
    :catchall_22
    move-exception v1

    #@23
    monitor-exit v2
    :try_end_24
    .catchall {:try_start_3 .. :try_end_24} :catchall_22

    #@24
    throw v1
.end method

.method public supplyPin2(Ljava/lang/String;Landroid/os/Message;)V
    .registers 6
    .parameter "pin2"
    .parameter "onComplete"

    #@0
    .prologue
    .line 821
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 822
    :try_start_3
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@5
    if-eqz v1, :cond_e

    #@7
    .line 823
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@9
    invoke-virtual {v1, p1, p2}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->supplyPin2(Ljava/lang/String;Landroid/os/Message;)V

    #@c
    .line 830
    :cond_c
    monitor-exit v2

    #@d
    .line 831
    :goto_d
    return-void

    #@e
    .line 824
    :cond_e
    if-eqz p2, :cond_c

    #@10
    .line 825
    new-instance v0, Ljava/lang/RuntimeException;

    #@12
    const-string v1, "ICC card is absent."

    #@14
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@17
    .line 826
    .local v0, e:Ljava/lang/Exception;
    invoke-static {p2}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;)Landroid/os/AsyncResult;

    #@1a
    move-result-object v1

    #@1b
    iput-object v0, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@1d
    .line 827
    invoke-virtual {p2}, Landroid/os/Message;->sendToTarget()V

    #@20
    .line 828
    monitor-exit v2

    #@21
    goto :goto_d

    #@22
    .line 830
    .end local v0           #e:Ljava/lang/Exception;
    :catchall_22
    move-exception v1

    #@23
    monitor-exit v2
    :try_end_24
    .catchall {:try_start_3 .. :try_end_24} :catchall_22

    #@24
    throw v1
.end method

.method public supplyPuk(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 7
    .parameter "puk"
    .parameter "newPin"
    .parameter "onComplete"

    #@0
    .prologue
    .line 807
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 808
    :try_start_3
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@5
    if-eqz v1, :cond_e

    #@7
    .line 809
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@9
    invoke-virtual {v1, p1, p2, p3}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->supplyPuk(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    #@c
    .line 816
    :cond_c
    monitor-exit v2

    #@d
    .line 817
    :goto_d
    return-void

    #@e
    .line 810
    :cond_e
    if-eqz p3, :cond_c

    #@10
    .line 811
    new-instance v0, Ljava/lang/RuntimeException;

    #@12
    const-string v1, "ICC card is absent."

    #@14
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@17
    .line 812
    .local v0, e:Ljava/lang/Exception;
    invoke-static {p3}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;)Landroid/os/AsyncResult;

    #@1a
    move-result-object v1

    #@1b
    iput-object v0, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@1d
    .line 813
    invoke-virtual {p3}, Landroid/os/Message;->sendToTarget()V

    #@20
    .line 814
    monitor-exit v2

    #@21
    goto :goto_d

    #@22
    .line 816
    .end local v0           #e:Ljava/lang/Exception;
    :catchall_22
    move-exception v1

    #@23
    monitor-exit v2
    :try_end_24
    .catchall {:try_start_3 .. :try_end_24} :catchall_22

    #@24
    throw v1
.end method

.method public supplyPuk2(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .registers 7
    .parameter "puk2"
    .parameter "newPin2"
    .parameter "onComplete"

    #@0
    .prologue
    .line 835
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 836
    :try_start_3
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@5
    if-eqz v1, :cond_e

    #@7
    .line 837
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@9
    invoke-virtual {v1, p1, p2, p3}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->supplyPuk2(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    #@c
    .line 844
    :cond_c
    monitor-exit v2

    #@d
    .line 845
    :goto_d
    return-void

    #@e
    .line 838
    :cond_e
    if-eqz p3, :cond_c

    #@10
    .line 839
    new-instance v0, Ljava/lang/RuntimeException;

    #@12
    const-string v1, "ICC card is absent."

    #@14
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@17
    .line 840
    .local v0, e:Ljava/lang/Exception;
    invoke-static {p3}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;)Landroid/os/AsyncResult;

    #@1a
    move-result-object v1

    #@1b
    iput-object v0, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@1d
    .line 841
    invoke-virtual {p3}, Landroid/os/Message;->sendToTarget()V

    #@20
    .line 842
    monitor-exit v2

    #@21
    goto :goto_d

    #@22
    .line 844
    .end local v0           #e:Ljava/lang/Exception;
    :catchall_22
    move-exception v1

    #@23
    monitor-exit v2
    :try_end_24
    .catchall {:try_start_3 .. :try_end_24} :catchall_22

    #@24
    throw v1
.end method

.method public unregisterForAbsent(Landroid/os/Handler;)V
    .registers 4
    .parameter "h"

    #@0
    .prologue
    .line 740
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 741
    :try_start_3
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mAbsentRegistrants:Landroid/os/RegistrantList;

    #@5
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@8
    .line 742
    monitor-exit v1

    #@9
    .line 743
    return-void

    #@a
    .line 742
    :catchall_a
    move-exception v0

    #@b
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method

.method public unregisterForLocked(Landroid/os/Handler;)V
    .registers 4
    .parameter "h"

    #@0
    .prologue
    .line 786
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 787
    :try_start_3
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mPinLockedRegistrants:Landroid/os/RegistrantList;

    #@5
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@8
    .line 788
    monitor-exit v1

    #@9
    .line 789
    return-void

    #@a
    .line 788
    :catchall_a
    move-exception v0

    #@b
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method

.method public unregisterForPersoLocked(Landroid/os/Handler;)V
    .registers 4
    .parameter "h"

    #@0
    .prologue
    .line 763
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 764
    :try_start_3
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mPersoLockedRegistrants:Landroid/os/RegistrantList;

    #@5
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@8
    .line 765
    monitor-exit v1

    #@9
    .line 766
    return-void

    #@a
    .line 765
    :catchall_a
    move-exception v0

    #@b
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method

.method protected unregisterUiccCardEvents()V
    .registers 2

    #@0
    .prologue
    .line 440
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccCard:Lcom/android/internal/telephony/uicc/UiccCard;

    #@2
    if-eqz v0, :cond_9

    #@4
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccCard:Lcom/android/internal/telephony/uicc/UiccCard;

    #@6
    invoke-virtual {v0, p0}, Lcom/android/internal/telephony/uicc/UiccCard;->unregisterForAbsent(Landroid/os/Handler;)V

    #@9
    .line 441
    :cond_9
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@b
    if-eqz v0, :cond_12

    #@d
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@f
    invoke-virtual {v0, p0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->unregisterForReady(Landroid/os/Handler;)V

    #@12
    .line 442
    :cond_12
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@14
    if-eqz v0, :cond_1b

    #@16
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@18
    invoke-virtual {v0, p0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->unregisterForLocked(Landroid/os/Handler;)V

    #@1b
    .line 443
    :cond_1b
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@1d
    if-eqz v0, :cond_24

    #@1f
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@21
    invoke-virtual {v0, p0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->unregisterForPersoLocked(Landroid/os/Handler;)V

    #@24
    .line 444
    :cond_24
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@26
    if-eqz v0, :cond_2d

    #@28
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@2a
    invoke-virtual {v0, p0}, Lcom/android/internal/telephony/uicc/IccRecords;->unregisterForImsiReady(Landroid/os/Handler;)V

    #@2d
    .line 445
    :cond_2d
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@2f
    if-eqz v0, :cond_36

    #@31
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@33
    invoke-virtual {v0, p0}, Lcom/android/internal/telephony/uicc/IccRecords;->unregisterForRecordsLoaded(Landroid/os/Handler;)V

    #@36
    .line 446
    :cond_36
    return-void
.end method

.method protected updateExternalState()V
    .registers 3

    #@0
    .prologue
    .line 360
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccCard:Lcom/android/internal/telephony/uicc/UiccCard;

    #@2
    if-eqz v0, :cond_e

    #@4
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccCard:Lcom/android/internal/telephony/uicc/UiccCard;

    #@6
    invoke-virtual {v0}, Lcom/android/internal/telephony/uicc/UiccCard;->getCardState()Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    #@9
    move-result-object v0

    #@a
    sget-object v1, Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;->CARDSTATE_ABSENT:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    #@c
    if-ne v0, v1, :cond_3e

    #@e
    .line 363
    :cond_e
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccCard:Lcom/android/internal/telephony/uicc/UiccCard;

    #@10
    if-nez v0, :cond_1f

    #@12
    .line 364
    sget-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->NOT_READY:Lcom/android/internal/telephony/IccCardConstants$State;

    #@14
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/uicc/IccCardProxy;->setExternalState(Lcom/android/internal/telephony/IccCardConstants$State;)V

    #@17
    .line 365
    const-string v0, "RIL_IccCardProxy"

    #@19
    const-string v1, "mUiccCard is null. So, send not-ready intent."

    #@1b
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 424
    :goto_1e
    return-void

    #@1f
    .line 369
    :cond_1f
    const-string v0, "persist.radio.apm_sim_not_pwdn"

    #@21
    const/4 v1, 0x0

    #@22
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@25
    move-result v0

    #@26
    if-nez v0, :cond_38

    #@28
    .line 370
    iget-boolean v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mRadioOn:Z

    #@2a
    if-eqz v0, :cond_32

    #@2c
    .line 371
    sget-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->ABSENT:Lcom/android/internal/telephony/IccCardConstants$State;

    #@2e
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/uicc/IccCardProxy;->setExternalState(Lcom/android/internal/telephony/IccCardConstants$State;)V

    #@31
    goto :goto_1e

    #@32
    .line 373
    :cond_32
    sget-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->NOT_READY:Lcom/android/internal/telephony/IccCardConstants$State;

    #@34
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/uicc/IccCardProxy;->setExternalState(Lcom/android/internal/telephony/IccCardConstants$State;)V

    #@37
    goto :goto_1e

    #@38
    .line 376
    :cond_38
    sget-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->ABSENT:Lcom/android/internal/telephony/IccCardConstants$State;

    #@3a
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/uicc/IccCardProxy;->setExternalState(Lcom/android/internal/telephony/IccCardConstants$State;)V

    #@3d
    goto :goto_1e

    #@3e
    .line 381
    :cond_3e
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccCard:Lcom/android/internal/telephony/uicc/UiccCard;

    #@40
    invoke-virtual {v0}, Lcom/android/internal/telephony/uicc/UiccCard;->getCardState()Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    #@43
    move-result-object v0

    #@44
    sget-object v1, Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;->CARDSTATE_ERROR:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    #@46
    if-ne v0, v1, :cond_4e

    #@48
    .line 382
    sget-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->CARD_IO_ERROR:Lcom/android/internal/telephony/IccCardConstants$State;

    #@4a
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/uicc/IccCardProxy;->setExternalState(Lcom/android/internal/telephony/IccCardConstants$State;)V

    #@4d
    goto :goto_1e

    #@4e
    .line 386
    :cond_4e
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@50
    if-nez v0, :cond_58

    #@52
    .line 387
    sget-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->NOT_READY:Lcom/android/internal/telephony/IccCardConstants$State;

    #@54
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/uicc/IccCardProxy;->setExternalState(Lcom/android/internal/telephony/IccCardConstants$State;)V

    #@57
    goto :goto_1e

    #@58
    .line 391
    :cond_58
    sget-object v0, Lcom/android/internal/telephony/uicc/IccCardProxy$1;->$SwitchMap$com$android$internal$telephony$uicc$IccCardApplicationStatus$AppState:[I

    #@5a
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@5c
    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getState()Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;

    #@5f
    move-result-object v1

    #@60
    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;->ordinal()I

    #@63
    move-result v1

    #@64
    aget v0, v0, v1

    #@66
    packed-switch v0, :pswitch_data_b2

    #@69
    goto :goto_1e

    #@6a
    .line 394
    :pswitch_6a
    sget-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->UNKNOWN:Lcom/android/internal/telephony/IccCardConstants$State;

    #@6c
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/uicc/IccCardProxy;->setExternalState(Lcom/android/internal/telephony/IccCardConstants$State;)V

    #@6f
    goto :goto_1e

    #@70
    .line 397
    :pswitch_70
    sget-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->PIN_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@72
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/uicc/IccCardProxy;->setExternalState(Lcom/android/internal/telephony/IccCardConstants$State;)V

    #@75
    goto :goto_1e

    #@76
    .line 405
    :pswitch_76
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@78
    invoke-virtual {v0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getPin1State()Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;

    #@7b
    move-result-object v0

    #@7c
    sget-object v1, Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;->PINSTATE_ENABLED_PERM_BLOCKED:Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;

    #@7e
    if-ne v0, v1, :cond_86

    #@80
    .line 406
    sget-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->PERM_DISABLED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@82
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/uicc/IccCardProxy;->setExternalState(Lcom/android/internal/telephony/IccCardConstants$State;)V

    #@85
    goto :goto_1e

    #@86
    .line 408
    :cond_86
    sget-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->PUK_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@88
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/uicc/IccCardProxy;->setExternalState(Lcom/android/internal/telephony/IccCardConstants$State;)V

    #@8b
    goto :goto_1e

    #@8c
    .line 413
    :pswitch_8c
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@8e
    invoke-virtual {v0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->isPersoLocked()Z

    #@91
    move-result v0

    #@92
    if-eqz v0, :cond_a3

    #@94
    .line 414
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@96
    invoke-virtual {v0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getPersoSubState()Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;

    #@99
    move-result-object v0

    #@9a
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mPersoSubState:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;

    #@9c
    .line 415
    sget-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->PERSO_LOCKED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@9e
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/uicc/IccCardProxy;->setExternalState(Lcom/android/internal/telephony/IccCardConstants$State;)V

    #@a1
    goto/16 :goto_1e

    #@a3
    .line 417
    :cond_a3
    sget-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->UNKNOWN:Lcom/android/internal/telephony/IccCardConstants$State;

    #@a5
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/uicc/IccCardProxy;->setExternalState(Lcom/android/internal/telephony/IccCardConstants$State;)V

    #@a8
    goto/16 :goto_1e

    #@aa
    .line 421
    :pswitch_aa
    sget-object v0, Lcom/android/internal/telephony/IccCardConstants$State;->READY:Lcom/android/internal/telephony/IccCardConstants$State;

    #@ac
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/uicc/IccCardProxy;->setExternalState(Lcom/android/internal/telephony/IccCardConstants$State;)V

    #@af
    goto/16 :goto_1e

    #@b1
    .line 391
    nop

    #@b2
    :pswitch_data_b2
    .packed-switch 0x1
        :pswitch_6a
        :pswitch_6a
        :pswitch_70
        :pswitch_76
        :pswitch_8c
        :pswitch_aa
    .end packed-switch
.end method

.method protected updateIccAvailability()V
    .registers 7

    #@0
    .prologue
    .line 317
    iget-object v4, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v4

    #@3
    .line 318
    :try_start_3
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@5
    invoke-virtual {v3}, Lcom/android/internal/telephony/uicc/UiccController;->getUiccCard()Lcom/android/internal/telephony/uicc/UiccCard;

    #@8
    move-result-object v1

    #@9
    .line 319
    .local v1, newCard:Lcom/android/internal/telephony/uicc/UiccCard;
    const/4 v0, 0x0

    #@a
    .line 320
    .local v0, newApp:Lcom/android/internal/telephony/uicc/UiccCardApplication;
    const/4 v2, 0x0

    #@b
    .line 321
    .local v2, newRecords:Lcom/android/internal/telephony/uicc/IccRecords;
    if-eqz v1, :cond_5c

    #@d
    .line 322
    iget v3, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mCurrentAppType:I

    #@f
    invoke-virtual {v1, v3}, Lcom/android/internal/telephony/uicc/UiccCard;->getApplication(I)Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@12
    move-result-object v0

    #@13
    .line 323
    if-eqz v0, :cond_19

    #@15
    .line 324
    invoke-virtual {v0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getIccRecords()Lcom/android/internal/telephony/uicc/IccRecords;

    #@18
    move-result-object v2

    #@19
    .line 327
    :cond_19
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getCdmaUsimOperator()Z

    #@1c
    move-result v3

    #@1d
    if-eqz v3, :cond_38

    #@1f
    .line 328
    if-nez v0, :cond_38

    #@21
    iget v3, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mCurrentAppType:I

    #@23
    const/4 v5, 0x2

    #@24
    if-ne v3, v5, :cond_38

    #@26
    .line 329
    const-string v3, "RIL_IccCardProxy"

    #@28
    const-string v5, "newApp is null, and mCurrentAppType is 3GPP2. 3GPP application should be created to do.."

    #@2a
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 330
    const/4 v3, 0x1

    #@2e
    invoke-virtual {v1, v3}, Lcom/android/internal/telephony/uicc/UiccCard;->getApplication(I)Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@31
    move-result-object v0

    #@32
    .line 331
    if-eqz v0, :cond_5a

    #@34
    invoke-virtual {v0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getIccRecords()Lcom/android/internal/telephony/uicc/IccRecords;

    #@37
    move-result-object v2

    #@38
    .line 338
    :cond_38
    :goto_38
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@3a
    if-ne v3, v2, :cond_44

    #@3c
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@3e
    if-ne v3, v0, :cond_44

    #@40
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccCard:Lcom/android/internal/telephony/uicc/UiccCard;

    #@42
    if-eq v3, v1, :cond_55

    #@44
    .line 339
    :cond_44
    const-string v3, "Icc changed. Reregestering."

    #@46
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/uicc/IccCardProxy;->log(Ljava/lang/String;)V

    #@49
    .line 340
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/IccCardProxy;->unregisterUiccCardEvents()V

    #@4c
    .line 341
    iput-object v1, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccCard:Lcom/android/internal/telephony/uicc/UiccCard;

    #@4e
    .line 342
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@50
    .line 343
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/IccCardProxy;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@52
    .line 344
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/IccCardProxy;->registerUiccCardEvents()V

    #@55
    .line 347
    :cond_55
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/IccCardProxy;->updateExternalState()V

    #@58
    .line 348
    monitor-exit v4

    #@59
    .line 349
    return-void

    #@5a
    .line 331
    :cond_5a
    const/4 v2, 0x0

    #@5b
    goto :goto_38

    #@5c
    .line 336
    :cond_5c
    const-string v3, "RIL_IccCardProxy"

    #@5e
    const-string v5, "No card available"

    #@60
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@63
    goto :goto_38

    #@64
    .line 348
    .end local v0           #newApp:Lcom/android/internal/telephony/uicc/UiccCardApplication;
    .end local v1           #newCard:Lcom/android/internal/telephony/uicc/UiccCard;
    .end local v2           #newRecords:Lcom/android/internal/telephony/uicc/IccRecords;
    :catchall_64
    move-exception v3

    #@65
    monitor-exit v4
    :try_end_66
    .catchall {:try_start_3 .. :try_end_66} :catchall_64

    #@66
    throw v3
.end method
