.class public Lcom/android/internal/telephony/PlmnListParser;
.super Ljava/lang/Object;
.source "PlmnListParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/PlmnListParser$PlmnData;
    }
.end annotation


# static fields
.field private static final ATTR_LONG_NAME:Ljava/lang/String; = "long_name"

.field private static final ATTR_MCC:Ljava/lang/String; = "mcc"

.field private static final ATTR_MNC:Ljava/lang/String; = "mnc"

.field private static final ATTR_SHORT_NAME:Ljava/lang/String; = "short_name"

.field private static final CUPSS_DIR:Ljava/lang/String; = null

.field private static final CUPSS_MCC_MNC_PLMNS_FILE:Ljava/lang/String; = "/config/plmns-mcc%s-mnc%s.xml"

.field private static final CUPSS_MCC_PLMNS_FILE:Ljava/lang/String; = "/config/plmns-mcc%s.xml"

.field private static final CUPSS_PLMNS_FILE:Ljava/lang/String; = "/config/plmns.xml"

.field private static final ELEMENT_NETWORK:Ljava/lang/String; = "network"

.field private static final ELEMENT_PLMNS:Ljava/lang/String; = "plmns"

.field private static final LOG_TAG:Ljava/lang/String; = "PlmnListParser"

.field private static final PLMNS_FILE:Ljava/lang/String; = "etc/plmns.xml"

.field private static mInstance:Lcom/android/internal/telephony/PlmnListParser;

.field private static mIsSimAvailable:Z

.field private static mPlmnMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/internal/telephony/PlmnListParser$PlmnData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 40
    const-string v0, "ro.lge.capp_cupss.rootdir"

    #@3
    const-string v1, "/cust"

    #@5
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    sput-object v0, Lcom/android/internal/telephony/PlmnListParser;->CUPSS_DIR:Ljava/lang/String;

    #@b
    .line 56
    const/4 v0, 0x0

    #@c
    sput-boolean v0, Lcom/android/internal/telephony/PlmnListParser;->mIsSimAvailable:Z

    #@e
    .line 57
    sput-object v2, Lcom/android/internal/telephony/PlmnListParser;->mInstance:Lcom/android/internal/telephony/PlmnListParser;

    #@10
    .line 58
    sput-object v2, Lcom/android/internal/telephony/PlmnListParser;->mPlmnMap:Ljava/util/Map;

    #@12
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 78
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static getInstance()Lcom/android/internal/telephony/PlmnListParser;
    .registers 1

    #@0
    .prologue
    .line 82
    sget-object v0, Lcom/android/internal/telephony/PlmnListParser;->mInstance:Lcom/android/internal/telephony/PlmnListParser;

    #@2
    if-nez v0, :cond_10

    #@4
    .line 83
    new-instance v0, Lcom/android/internal/telephony/PlmnListParser;

    #@6
    invoke-direct {v0}, Lcom/android/internal/telephony/PlmnListParser;-><init>()V

    #@9
    sput-object v0, Lcom/android/internal/telephony/PlmnListParser;->mInstance:Lcom/android/internal/telephony/PlmnListParser;

    #@b
    .line 84
    sget-object v0, Lcom/android/internal/telephony/PlmnListParser;->mInstance:Lcom/android/internal/telephony/PlmnListParser;

    #@d
    invoke-direct {v0}, Lcom/android/internal/telephony/PlmnListParser;->parsePlmnsList()V

    #@10
    .line 87
    :cond_10
    sget-boolean v0, Lcom/android/internal/telephony/PlmnListParser;->mIsSimAvailable:Z

    #@12
    if-nez v0, :cond_28

    #@14
    const-string v0, "gsm.sim.operator.numeric"

    #@16
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@19
    move-result-object v0

    #@1a
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@1d
    move-result v0

    #@1e
    if-nez v0, :cond_28

    #@20
    .line 89
    sget-object v0, Lcom/android/internal/telephony/PlmnListParser;->mInstance:Lcom/android/internal/telephony/PlmnListParser;

    #@22
    invoke-direct {v0}, Lcom/android/internal/telephony/PlmnListParser;->parsePlmnsList()V

    #@25
    .line 90
    const/4 v0, 0x1

    #@26
    sput-boolean v0, Lcom/android/internal/telephony/PlmnListParser;->mIsSimAvailable:Z

    #@28
    .line 93
    :cond_28
    sget-object v0, Lcom/android/internal/telephony/PlmnListParser;->mInstance:Lcom/android/internal/telephony/PlmnListParser;

    #@2a
    return-object v0
.end method

.method private loadPlmnData(Lorg/xmlpull/v1/XmlPullParser;)Lcom/android/internal/telephony/PlmnListParser$PlmnData;
    .registers 9
    .parameter "parser"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 174
    const-string v1, "network"

    #@3
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@6
    move-result-object v6

    #@7
    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a
    move-result v1

    #@b
    if-eqz v1, :cond_2b

    #@d
    .line 176
    const-string v1, "mcc"

    #@f
    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@12
    move-result-object v2

    #@13
    .line 177
    .local v2, mcc:Ljava/lang/String;
    const-string v1, "mnc"

    #@15
    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@18
    move-result-object v3

    #@19
    .line 178
    .local v3, mnc:Ljava/lang/String;
    const-string v1, "long_name"

    #@1b
    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1e
    move-result-object v4

    #@1f
    .line 179
    .local v4, longName:Ljava/lang/String;
    const-string v1, "short_name"

    #@21
    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@24
    move-result-object v5

    #@25
    .line 181
    .local v5, shortName:Ljava/lang/String;
    new-instance v0, Lcom/android/internal/telephony/PlmnListParser$PlmnData;

    #@27
    move-object v1, p0

    #@28
    invoke-direct/range {v0 .. v5}, Lcom/android/internal/telephony/PlmnListParser$PlmnData;-><init>(Lcom/android/internal/telephony/PlmnListParser;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@2b
    .line 184
    .end local v2           #mcc:Ljava/lang/String;
    .end local v3           #mnc:Ljava/lang/String;
    .end local v4           #longName:Ljava/lang/String;
    .end local v5           #shortName:Ljava/lang/String;
    :cond_2b
    return-object v0
.end method

.method private parsePlmnsList()V
    .registers 10

    #@0
    .prologue
    .line 124
    invoke-direct {p0}, Lcom/android/internal/telephony/PlmnListParser;->resolveBestPlmnFile()Ljava/io/File;

    #@3
    move-result-object v4

    #@4
    .line 126
    .local v4, plmnFile:Ljava/io/File;
    if-eqz v4, :cond_c

    #@6
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    #@9
    move-result v7

    #@a
    if-nez v7, :cond_d

    #@c
    .line 170
    :cond_c
    :goto_c
    return-void

    #@d
    .line 132
    :cond_d
    const/4 v5, 0x0

    #@e
    .line 136
    .local v5, reader:Ljava/io/FileReader;
    :try_start_e
    new-instance v6, Ljava/io/FileReader;

    #@10
    invoke-direct {v6, v4}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V
    :try_end_13
    .catchall {:try_start_e .. :try_end_13} :catchall_65
    .catch Ljava/io/FileNotFoundException; {:try_start_e .. :try_end_13} :catch_76
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_13} :catch_57

    #@13
    .line 138
    .end local v5           #reader:Ljava/io/FileReader;
    .local v6, reader:Ljava/io/FileReader;
    :try_start_13
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    #@16
    move-result-object v1

    #@17
    .line 139
    .local v1, factory:Lorg/xmlpull/v1/XmlPullParserFactory;
    invoke-virtual {v1}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    #@1a
    move-result-object v3

    #@1b
    .line 140
    .local v3, parser:Lorg/xmlpull/v1/XmlPullParser;
    invoke-interface {v3, v6}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    #@1e
    .line 142
    const-string v7, "plmns"

    #@20
    invoke-static {v3, v7}, Lcom/android/internal/util/XmlUtils;->beginDocument(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V

    #@23
    .line 146
    :goto_23
    invoke-static {v3}, Lcom/android/internal/util/XmlUtils;->nextElement(Lorg/xmlpull/v1/XmlPullParser;)V

    #@26
    .line 148
    invoke-direct {p0, v3}, Lcom/android/internal/telephony/PlmnListParser;->loadPlmnData(Lorg/xmlpull/v1/XmlPullParser;)Lcom/android/internal/telephony/PlmnListParser$PlmnData;

    #@29
    move-result-object v2

    #@2a
    .line 150
    .local v2, item:Lcom/android/internal/telephony/PlmnListParser$PlmnData;
    if-eqz v2, :cond_50

    #@2c
    .line 152
    sget-object v7, Lcom/android/internal/telephony/PlmnListParser;->mPlmnMap:Ljava/util/Map;

    #@2e
    if-nez v7, :cond_37

    #@30
    .line 153
    new-instance v7, Ljava/util/HashMap;

    #@32
    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    #@35
    sput-object v7, Lcom/android/internal/telephony/PlmnListParser;->mPlmnMap:Ljava/util/Map;

    #@37
    .line 156
    :cond_37
    sget-object v7, Lcom/android/internal/telephony/PlmnListParser;->mPlmnMap:Ljava/util/Map;

    #@39
    iget-object v8, v2, Lcom/android/internal/telephony/PlmnListParser$PlmnData;->numeric:Ljava/lang/String;

    #@3b
    invoke-interface {v7, v8, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3e
    .catchall {:try_start_13 .. :try_end_3e} :catchall_70
    .catch Ljava/io/FileNotFoundException; {:try_start_13 .. :try_end_3e} :catch_3f
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_3e} :catch_73

    #@3e
    goto :goto_23

    #@3f
    .line 161
    .end local v1           #factory:Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v2           #item:Lcom/android/internal/telephony/PlmnListParser$PlmnData;
    .end local v3           #parser:Lorg/xmlpull/v1/XmlPullParser;
    :catch_3f
    move-exception v0

    #@40
    move-object v5, v6

    #@41
    .line 162
    .end local v6           #reader:Ljava/io/FileReader;
    .local v0, e:Ljava/io/FileNotFoundException;
    .restart local v5       #reader:Ljava/io/FileReader;
    :goto_41
    :try_start_41
    const-string v7, "PlmnListParser"

    #@43
    const-string v8, "Could not find plmns file"

    #@45
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_48
    .catchall {:try_start_41 .. :try_end_48} :catchall_65

    #@48
    .line 168
    if-eqz v5, :cond_c

    #@4a
    :try_start_4a
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V
    :try_end_4d
    .catch Ljava/io/IOException; {:try_start_4a .. :try_end_4d} :catch_4e

    #@4d
    goto :goto_c

    #@4e
    .end local v0           #e:Ljava/io/FileNotFoundException;
    :catch_4e
    move-exception v7

    #@4f
    goto :goto_c

    #@50
    .end local v5           #reader:Ljava/io/FileReader;
    .restart local v1       #factory:Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v2       #item:Lcom/android/internal/telephony/PlmnListParser$PlmnData;
    .restart local v3       #parser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v6       #reader:Ljava/io/FileReader;
    :cond_50
    if-eqz v6, :cond_55

    #@52
    :try_start_52
    invoke-virtual {v6}, Ljava/io/FileReader;->close()V
    :try_end_55
    .catch Ljava/io/IOException; {:try_start_52 .. :try_end_55} :catch_6e

    #@55
    :cond_55
    :goto_55
    move-object v5, v6

    #@56
    .line 169
    .end local v6           #reader:Ljava/io/FileReader;
    .restart local v5       #reader:Ljava/io/FileReader;
    goto :goto_c

    #@57
    .line 164
    .end local v1           #factory:Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v2           #item:Lcom/android/internal/telephony/PlmnListParser$PlmnData;
    .end local v3           #parser:Lorg/xmlpull/v1/XmlPullParser;
    :catch_57
    move-exception v0

    #@58
    .line 165
    .local v0, e:Ljava/lang/Exception;
    :goto_58
    :try_start_58
    const-string v7, "PlmnListParser"

    #@5a
    const-string v8, "Exception while parsing plmns file"

    #@5c
    invoke-static {v7, v8, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5f
    .catchall {:try_start_58 .. :try_end_5f} :catchall_65

    #@5f
    .line 168
    if-eqz v5, :cond_c

    #@61
    :try_start_61
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V
    :try_end_64
    .catch Ljava/io/IOException; {:try_start_61 .. :try_end_64} :catch_4e

    #@64
    goto :goto_c

    #@65
    .end local v0           #e:Ljava/lang/Exception;
    :catchall_65
    move-exception v7

    #@66
    :goto_66
    if-eqz v5, :cond_6b

    #@68
    :try_start_68
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V
    :try_end_6b
    .catch Ljava/io/IOException; {:try_start_68 .. :try_end_6b} :catch_6c

    #@6b
    :cond_6b
    :goto_6b
    throw v7

    #@6c
    :catch_6c
    move-exception v8

    #@6d
    goto :goto_6b

    #@6e
    .end local v5           #reader:Ljava/io/FileReader;
    .restart local v1       #factory:Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v2       #item:Lcom/android/internal/telephony/PlmnListParser$PlmnData;
    .restart local v3       #parser:Lorg/xmlpull/v1/XmlPullParser;
    .restart local v6       #reader:Ljava/io/FileReader;
    :catch_6e
    move-exception v7

    #@6f
    goto :goto_55

    #@70
    .end local v1           #factory:Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v2           #item:Lcom/android/internal/telephony/PlmnListParser$PlmnData;
    .end local v3           #parser:Lorg/xmlpull/v1/XmlPullParser;
    :catchall_70
    move-exception v7

    #@71
    move-object v5, v6

    #@72
    .end local v6           #reader:Ljava/io/FileReader;
    .restart local v5       #reader:Ljava/io/FileReader;
    goto :goto_66

    #@73
    .line 164
    .end local v5           #reader:Ljava/io/FileReader;
    .restart local v6       #reader:Ljava/io/FileReader;
    :catch_73
    move-exception v0

    #@74
    move-object v5, v6

    #@75
    .end local v6           #reader:Ljava/io/FileReader;
    .restart local v5       #reader:Ljava/io/FileReader;
    goto :goto_58

    #@76
    .line 161
    :catch_76
    move-exception v0

    #@77
    goto :goto_41
.end method

.method private resolveBestPlmnFile()Ljava/io/File;
    .registers 14

    #@0
    .prologue
    const/4 v12, 0x3

    #@1
    const/4 v11, 0x1

    #@2
    const/4 v10, 0x0

    #@3
    .line 189
    const-string v8, "gsm.sim.operator.numeric"

    #@5
    invoke-static {v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@8
    move-result-object v7

    #@9
    .line 191
    .local v7, networkOperator:Ljava/lang/String;
    if-eqz v7, :cond_70

    #@b
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    #@e
    move-result v8

    #@f
    const/4 v9, 0x5

    #@10
    if-lt v8, v9, :cond_70

    #@12
    .line 192
    invoke-virtual {v7, v10, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@15
    move-result-object v1

    #@16
    .line 193
    .local v1, mcc:Ljava/lang/String;
    invoke-virtual {v7, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@19
    move-result-object v6

    #@1a
    .line 196
    .local v6, mnc:Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    sget-object v9, Lcom/android/internal/telephony/PlmnListParser;->CUPSS_DIR:Ljava/lang/String;

    #@21
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v8

    #@25
    const-string v9, "/config/plmns-mcc%s-mnc%s.xml"

    #@27
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v8

    #@2b
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v8

    #@2f
    const/4 v9, 0x2

    #@30
    new-array v9, v9, [Ljava/lang/Object;

    #@32
    aput-object v1, v9, v10

    #@34
    aput-object v6, v9, v11

    #@36
    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@39
    move-result-object v5

    #@3a
    .line 197
    .local v5, mccmncfileName:Ljava/lang/String;
    new-instance v4, Ljava/io/File;

    #@3c
    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@3f
    .line 198
    .local v4, mccmncFile:Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    #@42
    move-result v8

    #@43
    if-eqz v8, :cond_46

    #@45
    .line 217
    .end local v1           #mcc:Ljava/lang/String;
    .end local v4           #mccmncFile:Ljava/io/File;
    .end local v5           #mccmncfileName:Ljava/lang/String;
    .end local v6           #mnc:Ljava/lang/String;
    :goto_45
    return-object v4

    #@46
    .line 203
    .restart local v1       #mcc:Ljava/lang/String;
    .restart local v4       #mccmncFile:Ljava/io/File;
    .restart local v5       #mccmncfileName:Ljava/lang/String;
    .restart local v6       #mnc:Ljava/lang/String;
    :cond_46
    new-instance v8, Ljava/lang/StringBuilder;

    #@48
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@4b
    sget-object v9, Lcom/android/internal/telephony/PlmnListParser;->CUPSS_DIR:Ljava/lang/String;

    #@4d
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v8

    #@51
    const-string v9, "/config/plmns-mcc%s.xml"

    #@53
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v8

    #@57
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v8

    #@5b
    new-array v9, v11, [Ljava/lang/Object;

    #@5d
    aput-object v1, v9, v10

    #@5f
    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@62
    move-result-object v3

    #@63
    .line 204
    .local v3, mccfileName:Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    #@65
    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@68
    .line 205
    .local v2, mccFile:Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    #@6b
    move-result v8

    #@6c
    if-eqz v8, :cond_70

    #@6e
    move-object v4, v2

    #@6f
    .line 206
    goto :goto_45

    #@70
    .line 211
    .end local v1           #mcc:Ljava/lang/String;
    .end local v2           #mccFile:Ljava/io/File;
    .end local v3           #mccfileName:Ljava/lang/String;
    .end local v4           #mccmncFile:Ljava/io/File;
    .end local v5           #mccmncfileName:Ljava/lang/String;
    .end local v6           #mnc:Ljava/lang/String;
    :cond_70
    new-instance v0, Ljava/io/File;

    #@72
    new-instance v8, Ljava/lang/StringBuilder;

    #@74
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@77
    sget-object v9, Lcom/android/internal/telephony/PlmnListParser;->CUPSS_DIR:Ljava/lang/String;

    #@79
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v8

    #@7d
    const-string v9, "/config/plmns.xml"

    #@7f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v8

    #@83
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@86
    move-result-object v8

    #@87
    invoke-direct {v0, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@8a
    .line 212
    .local v0, cupssFile:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    #@8d
    move-result v8

    #@8e
    if-eqz v8, :cond_92

    #@90
    move-object v4, v0

    #@91
    .line 213
    goto :goto_45

    #@92
    .line 217
    :cond_92
    new-instance v4, Ljava/io/File;

    #@94
    invoke-static {}, Landroid/os/Environment;->getRootDirectory()Ljava/io/File;

    #@97
    move-result-object v8

    #@98
    const-string v9, "etc/plmns.xml"

    #@9a
    invoke-direct {v4, v8, v9}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@9d
    goto :goto_45
.end method


# virtual methods
.method public getLongName(Ljava/lang/String;)Ljava/lang/String;
    .registers 7
    .parameter "numeric"

    #@0
    .prologue
    .line 101
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/PlmnListParser;->isPlmnAvailable(Ljava/lang/String;)Z

    #@3
    move-result v1

    #@4
    if-eqz v1, :cond_2b

    #@6
    .line 102
    sget-object v1, Lcom/android/internal/telephony/PlmnListParser;->mPlmnMap:Ljava/util/Map;

    #@8
    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@b
    move-result-object v1

    #@c
    check-cast v1, Lcom/android/internal/telephony/PlmnListParser$PlmnData;

    #@e
    iget-object v0, v1, Lcom/android/internal/telephony/PlmnListParser$PlmnData;->longName:Ljava/lang/String;

    #@10
    .line 103
    .local v0, longName:Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@13
    move-result v1

    #@14
    if-nez v1, :cond_2b

    #@16
    .line 104
    const-string v1, "PlmnListParser"

    #@18
    const-string v2, "Found the long name %s for numeric %s"

    #@1a
    const/4 v3, 0x2

    #@1b
    new-array v3, v3, [Ljava/lang/Object;

    #@1d
    const/4 v4, 0x0

    #@1e
    aput-object v0, v3, v4

    #@20
    const/4 v4, 0x1

    #@21
    aput-object p1, v3, v4

    #@23
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@26
    move-result-object v2

    #@27
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 108
    .end local v0           #longName:Ljava/lang/String;
    :goto_2a
    return-object v0

    #@2b
    :cond_2b
    const/4 v0, 0x0

    #@2c
    goto :goto_2a
.end method

.method public getShortName(Ljava/lang/String;)Ljava/lang/String;
    .registers 7
    .parameter "numeric"

    #@0
    .prologue
    .line 112
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/PlmnListParser;->isPlmnAvailable(Ljava/lang/String;)Z

    #@3
    move-result v1

    #@4
    if-eqz v1, :cond_2b

    #@6
    .line 113
    sget-object v1, Lcom/android/internal/telephony/PlmnListParser;->mPlmnMap:Ljava/util/Map;

    #@8
    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@b
    move-result-object v1

    #@c
    check-cast v1, Lcom/android/internal/telephony/PlmnListParser$PlmnData;

    #@e
    iget-object v0, v1, Lcom/android/internal/telephony/PlmnListParser$PlmnData;->shortName:Ljava/lang/String;

    #@10
    .line 114
    .local v0, shortName:Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@13
    move-result v1

    #@14
    if-nez v1, :cond_2b

    #@16
    .line 115
    const-string v1, "PlmnListParser"

    #@18
    const-string v2, "Found the short name %s for numeric %s"

    #@1a
    const/4 v3, 0x2

    #@1b
    new-array v3, v3, [Ljava/lang/Object;

    #@1d
    const/4 v4, 0x0

    #@1e
    aput-object v0, v3, v4

    #@20
    const/4 v4, 0x1

    #@21
    aput-object p1, v3, v4

    #@23
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@26
    move-result-object v2

    #@27
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 119
    .end local v0           #shortName:Ljava/lang/String;
    :goto_2a
    return-object v0

    #@2b
    :cond_2b
    const/4 v0, 0x0

    #@2c
    goto :goto_2a
.end method

.method public isPlmnAvailable(Ljava/lang/String;)Z
    .registers 3
    .parameter "numeric"

    #@0
    .prologue
    .line 97
    sget-object v0, Lcom/android/internal/telephony/PlmnListParser;->mPlmnMap:Ljava/util/Map;

    #@2
    if-eqz v0, :cond_16

    #@4
    sget-object v0, Lcom/android/internal/telephony/PlmnListParser;->mPlmnMap:Ljava/util/Map;

    #@6
    invoke-interface {v0}, Ljava/util/Map;->size()I

    #@9
    move-result v0

    #@a
    if-lez v0, :cond_16

    #@c
    sget-object v0, Lcom/android/internal/telephony/PlmnListParser;->mPlmnMap:Ljava/util/Map;

    #@e
    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    #@11
    move-result v0

    #@12
    if-eqz v0, :cond_16

    #@14
    const/4 v0, 0x1

    #@15
    :goto_15
    return v0

    #@16
    :cond_16
    const/4 v0, 0x0

    #@17
    goto :goto_15
.end method
