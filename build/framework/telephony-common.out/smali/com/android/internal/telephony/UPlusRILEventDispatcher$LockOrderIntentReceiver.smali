.class Lcom/android/internal/telephony/UPlusRILEventDispatcher$LockOrderIntentReceiver;
.super Landroid/content/BroadcastReceiver;
.source "UPlusRILEventDispatcher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/UPlusRILEventDispatcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LockOrderIntentReceiver"
.end annotation


# static fields
.field public static final INTENT_ACTION_CDMA_LOCK_ORDER:Ljava/lang/String; = "android.intent.action.CDMA_LOCK_ORDER"

.field public static final INTENT_ACTION_CDMA_MAINT_REQ:Ljava/lang/String; = "android.intent.action.CDMA_MAINT_REQ"

.field public static final INTENT_ACTION_EHRPD_AN_LOCK:Ljava/lang/String; = "android.intent.action.EHRPD_AN_LOCK"

.field public static final INTENT_ACTION_EHRPD_LOCK_ORDER:Ljava/lang/String; = "android.intent.action.EHRPD_LOCK_ORDER"

.field public static final INTENT_ACTION_LGT_AUTH_LOCK:Ljava/lang/String; = "android.intent.action.LGT_AUTH_LOCK"

.field public static final INTENT_ACTION_LGT_HDR_NETWORK_ERROR:Ljava/lang/String; = "android.intent.action.LGT_HDR_NETWORK_ERROR"

.field public static final INTENT_ACTION_LTE_EMM_REJECT:Ljava/lang/String; = "android.intent.action.LTE_EMM_REJECT"

.field public static final INTENT_ACTION_LTE_LOCK_ORDER:Ljava/lang/String; = "android.intent.action.LTE_LOCK_ORDER"

.field public static final INTENT_ACTION_LTE_MISSING_PHONE:Ljava/lang/String; = "com.lge.intent.action.LTE_MISSING_PHONE"

.field private static final TAG:Ljava/lang/String; = "LockOrderIntentReceiver"


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/UPlusRILEventDispatcher;


# direct methods
.method private constructor <init>(Lcom/android/internal/telephony/UPlusRILEventDispatcher;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 524
    iput-object p1, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher$LockOrderIntentReceiver;->this$0:Lcom/android/internal/telephony/UPlusRILEventDispatcher;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/internal/telephony/UPlusRILEventDispatcher;Lcom/android/internal/telephony/UPlusRILEventDispatcher$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 524
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/UPlusRILEventDispatcher$LockOrderIntentReceiver;-><init>(Lcom/android/internal/telephony/UPlusRILEventDispatcher;)V

    #@3
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 11
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v6, 0x1

    #@2
    .line 550
    iget-object v3, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher$LockOrderIntentReceiver;->this$0:Lcom/android/internal/telephony/UPlusRILEventDispatcher;

    #@4
    const-string v4, "LockOrderIntentReceiver/onReceive"

    #@6
    invoke-virtual {v3, v4}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@9
    .line 552
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@c
    move-result-object v0

    #@d
    .line 553
    .local v0, action:Ljava/lang/String;
    const-string v3, "android.intent.action.CDMA_LOCK_ORDER"

    #@f
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@12
    move-result v3

    #@13
    if-nez v3, :cond_1d

    #@15
    const-string v3, "android.intent.action.CDMA_MAINT_REQ"

    #@17
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1a
    move-result v3

    #@1b
    if-eqz v3, :cond_96

    #@1d
    .line 554
    :cond_1d
    iget-object v3, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher$LockOrderIntentReceiver;->this$0:Lcom/android/internal/telephony/UPlusRILEventDispatcher;

    #@1f
    invoke-static {v3, v6}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->access$202(Lcom/android/internal/telephony/UPlusRILEventDispatcher;Z)Z

    #@22
    .line 585
    :cond_22
    :goto_22
    iget-object v3, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher$LockOrderIntentReceiver;->this$0:Lcom/android/internal/telephony/UPlusRILEventDispatcher;

    #@24
    new-instance v4, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v5, "onReceive / result, mIsLGTUnregister : "

    #@2b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v4

    #@2f
    iget-object v5, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher$LockOrderIntentReceiver;->this$0:Lcom/android/internal/telephony/UPlusRILEventDispatcher;

    #@31
    invoke-static {v5}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->access$200(Lcom/android/internal/telephony/UPlusRILEventDispatcher;)Z

    #@34
    move-result v5

    #@35
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@38
    move-result-object v4

    #@39
    const-string v5, ", mIsLGTUnauthenticated : "

    #@3b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v4

    #@3f
    iget-object v5, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher$LockOrderIntentReceiver;->this$0:Lcom/android/internal/telephony/UPlusRILEventDispatcher;

    #@41
    invoke-static {v5}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->access$300(Lcom/android/internal/telephony/UPlusRILEventDispatcher;)Z

    #@44
    move-result v5

    #@45
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@48
    move-result-object v4

    #@49
    const-string v5, ", mIsLGTHDRNetworkError : "

    #@4b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v4

    #@4f
    iget-object v5, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher$LockOrderIntentReceiver;->this$0:Lcom/android/internal/telephony/UPlusRILEventDispatcher;

    #@51
    invoke-static {v5}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->access$400(Lcom/android/internal/telephony/UPlusRILEventDispatcher;)Z

    #@54
    move-result v5

    #@55
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@58
    move-result-object v4

    #@59
    const-string v5, ", mIsLTEAuthError : "

    #@5b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v4

    #@5f
    iget-object v5, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher$LockOrderIntentReceiver;->this$0:Lcom/android/internal/telephony/UPlusRILEventDispatcher;

    #@61
    invoke-static {v5}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->access$500(Lcom/android/internal/telephony/UPlusRILEventDispatcher;)Z

    #@64
    move-result v5

    #@65
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@68
    move-result-object v4

    #@69
    const-string v5, ", mIsLTEEMMReject : "

    #@6b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v4

    #@6f
    iget-object v5, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher$LockOrderIntentReceiver;->this$0:Lcom/android/internal/telephony/UPlusRILEventDispatcher;

    #@71
    invoke-static {v5}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->access$600(Lcom/android/internal/telephony/UPlusRILEventDispatcher;)Z

    #@74
    move-result v5

    #@75
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@78
    move-result-object v4

    #@79
    const-string v5, ", mRejectNum:"

    #@7b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v4

    #@7f
    iget-object v5, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher$LockOrderIntentReceiver;->this$0:Lcom/android/internal/telephony/UPlusRILEventDispatcher;

    #@81
    invoke-static {v5}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->access$700(Lcom/android/internal/telephony/UPlusRILEventDispatcher;)I

    #@84
    move-result v5

    #@85
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@88
    move-result-object v4

    #@89
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8c
    move-result-object v4

    #@8d
    invoke-virtual {v3, v4}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@90
    .line 590
    iget-object v3, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher$LockOrderIntentReceiver;->this$0:Lcom/android/internal/telephony/UPlusRILEventDispatcher;

    #@92
    invoke-static {v3}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->access$1000(Lcom/android/internal/telephony/UPlusRILEventDispatcher;)V

    #@95
    .line 591
    return-void

    #@96
    .line 555
    :cond_96
    const-string v3, "android.intent.action.LGT_AUTH_LOCK"

    #@98
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9b
    move-result v3

    #@9c
    if-eqz v3, :cond_a5

    #@9e
    .line 556
    iget-object v3, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher$LockOrderIntentReceiver;->this$0:Lcom/android/internal/telephony/UPlusRILEventDispatcher;

    #@a0
    invoke-static {v3, v6}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->access$302(Lcom/android/internal/telephony/UPlusRILEventDispatcher;Z)Z

    #@a3
    goto/16 :goto_22

    #@a5
    .line 557
    :cond_a5
    const-string v3, "android.intent.action.LGT_HDR_NETWORK_ERROR"

    #@a7
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@aa
    move-result v3

    #@ab
    if-eqz v3, :cond_b4

    #@ad
    .line 558
    iget-object v3, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher$LockOrderIntentReceiver;->this$0:Lcom/android/internal/telephony/UPlusRILEventDispatcher;

    #@af
    invoke-static {v3, v6}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->access$402(Lcom/android/internal/telephony/UPlusRILEventDispatcher;Z)Z

    #@b2
    goto/16 :goto_22

    #@b4
    .line 560
    :cond_b4
    const-string v3, "android.intent.action.EHRPD_LOCK_ORDER"

    #@b6
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b9
    move-result v3

    #@ba
    if-nez v3, :cond_cc

    #@bc
    const-string v3, "android.intent.action.LTE_LOCK_ORDER"

    #@be
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c1
    move-result v3

    #@c2
    if-nez v3, :cond_cc

    #@c4
    const-string v3, "android.intent.action.EHRPD_AN_LOCK"

    #@c6
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c9
    move-result v3

    #@ca
    if-eqz v3, :cond_d3

    #@cc
    .line 563
    :cond_cc
    iget-object v3, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher$LockOrderIntentReceiver;->this$0:Lcom/android/internal/telephony/UPlusRILEventDispatcher;

    #@ce
    invoke-static {v3, v6}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->access$502(Lcom/android/internal/telephony/UPlusRILEventDispatcher;Z)Z

    #@d1
    goto/16 :goto_22

    #@d3
    .line 568
    :cond_d3
    const-string v3, "android.intent.action.LTE_EMM_REJECT"

    #@d5
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d8
    move-result v3

    #@d9
    if-nez v3, :cond_e3

    #@db
    const-string v3, "com.lge.intent.action.LTE_MISSING_PHONE"

    #@dd
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e0
    move-result v3

    #@e1
    if-eqz v3, :cond_118

    #@e3
    .line 570
    :cond_e3
    iget-object v3, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher$LockOrderIntentReceiver;->this$0:Lcom/android/internal/telephony/UPlusRILEventDispatcher;

    #@e5
    new-instance v4, Ljava/lang/StringBuilder;

    #@e7
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@ea
    const-string v5, "onReceive = "

    #@ec
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ef
    move-result-object v4

    #@f0
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f3
    move-result-object v4

    #@f4
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f7
    move-result-object v4

    #@f8
    invoke-virtual {v3, v4}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@fb
    .line 571
    iget-object v3, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher$LockOrderIntentReceiver;->this$0:Lcom/android/internal/telephony/UPlusRILEventDispatcher;

    #@fd
    invoke-static {v3, v6}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->access$602(Lcom/android/internal/telephony/UPlusRILEventDispatcher;Z)Z

    #@100
    .line 572
    iget-object v3, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher$LockOrderIntentReceiver;->this$0:Lcom/android/internal/telephony/UPlusRILEventDispatcher;

    #@102
    const-string v4, "rejectCode"

    #@104
    invoke-virtual {p2, v4, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@107
    move-result v4

    #@108
    invoke-static {v3, v4}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->access$702(Lcom/android/internal/telephony/UPlusRILEventDispatcher;I)I

    #@10b
    .line 573
    iget-object v3, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher$LockOrderIntentReceiver;->this$0:Lcom/android/internal/telephony/UPlusRILEventDispatcher;

    #@10d
    const-string v4, "esmRejectCode"

    #@10f
    invoke-virtual {p2, v4, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@112
    move-result v4

    #@113
    invoke-static {v3, v4}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->access$802(Lcom/android/internal/telephony/UPlusRILEventDispatcher;I)I

    #@116
    goto/16 :goto_22

    #@118
    .line 575
    :cond_118
    const-string v3, "android.intent.action.BOOT_COMPLETED"

    #@11a
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11d
    move-result v3

    #@11e
    if-eqz v3, :cond_22

    #@120
    .line 576
    const-string v3, "persist.radio.last_ltereject"

    #@122
    invoke-static {v3, v7}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@125
    move-result v2

    #@126
    .line 577
    .local v2, lastLteRejectCause:I
    const/4 v3, 0x5

    #@127
    if-eq v2, v3, :cond_12c

    #@129
    const/4 v3, 0x6

    #@12a
    if-ne v2, v3, :cond_22

    #@12c
    .line 579
    :cond_12c
    new-instance v1, Landroid/content/Intent;

    #@12e
    const-string v3, "com.lge.intent.action.LTE_MISSING_PHONE"

    #@130
    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@133
    .line 580
    .local v1, intentMissingPhone:Landroid/content/Intent;
    const-string v3, "rejectCode"

    #@135
    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@138
    .line 581
    iget-object v3, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher$LockOrderIntentReceiver;->this$0:Lcom/android/internal/telephony/UPlusRILEventDispatcher;

    #@13a
    invoke-static {v3}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->access$900(Lcom/android/internal/telephony/UPlusRILEventDispatcher;)Landroid/content/Context;

    #@13d
    move-result-object v3

    #@13e
    invoke-virtual {v3, v1}, Landroid/content/Context;->sendStickyBroadcast(Landroid/content/Intent;)V

    #@141
    goto/16 :goto_22
.end method
