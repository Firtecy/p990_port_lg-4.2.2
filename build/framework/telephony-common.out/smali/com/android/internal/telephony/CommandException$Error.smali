.class public final enum Lcom/android/internal/telephony/CommandException$Error;
.super Ljava/lang/Enum;
.source "CommandException.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/CommandException;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Error"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/internal/telephony/CommandException$Error;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/internal/telephony/CommandException$Error;

.field public static final enum DIAL_MODIFIED_TO_DIAL:Lcom/android/internal/telephony/CommandException$Error;

.field public static final enum DIAL_MODIFIED_TO_SS:Lcom/android/internal/telephony/CommandException$Error;

.field public static final enum DIAL_MODIFIED_TO_USSD:Lcom/android/internal/telephony/CommandException$Error;

.field public static final enum FDN_CHECK_FAILURE:Lcom/android/internal/telephony/CommandException$Error;

.field public static final enum GENERIC_FAILURE:Lcom/android/internal/telephony/CommandException$Error;

.field public static final enum ILLEGAL_SIM_OR_ME:Lcom/android/internal/telephony/CommandException$Error;

.field public static final enum INVALID_PARAMETER:Lcom/android/internal/telephony/CommandException$Error;

.field public static final enum INVALID_RESPONSE:Lcom/android/internal/telephony/CommandException$Error;

.field public static final enum MISSING_RESOURCE:Lcom/android/internal/telephony/CommandException$Error;

.field public static final enum MODE_NOT_SUPPORTED:Lcom/android/internal/telephony/CommandException$Error;

.field public static final enum NET_SEL_FAILED_BY_PLMN_NOT_ALLOWED:Lcom/android/internal/telephony/CommandException$Error;

.field public static final enum NO_SUCH_ELEMENT:Lcom/android/internal/telephony/CommandException$Error;

.field public static final enum OP_NOT_ALLOWED_BEFORE_REG_NW:Lcom/android/internal/telephony/CommandException$Error;

.field public static final enum OP_NOT_ALLOWED_DURING_VOICE_CALL:Lcom/android/internal/telephony/CommandException$Error;

.field public static final enum PASSWORD_INCORRECT:Lcom/android/internal/telephony/CommandException$Error;

.field public static final enum RADIO_NOT_AVAILABLE:Lcom/android/internal/telephony/CommandException$Error;

.field public static final enum REQUEST_NOT_SUPPORTED:Lcom/android/internal/telephony/CommandException$Error;

.field public static final enum SIM_ABSENT:Lcom/android/internal/telephony/CommandException$Error;

.field public static final enum SIM_PIN2:Lcom/android/internal/telephony/CommandException$Error;

.field public static final enum SIM_PUK2:Lcom/android/internal/telephony/CommandException$Error;

.field public static final enum SMS_FAIL_RETRY:Lcom/android/internal/telephony/CommandException$Error;

.field public static final enum SS_MODIFIED_TO_DIAL:Lcom/android/internal/telephony/CommandException$Error;

.field public static final enum SS_MODIFIED_TO_SS:Lcom/android/internal/telephony/CommandException$Error;

.field public static final enum SS_MODIFIED_TO_USSD:Lcom/android/internal/telephony/CommandException$Error;

.field public static final enum SUBSCRIPTION_NOT_AVAILABLE:Lcom/android/internal/telephony/CommandException$Error;

.field public static final enum SUBSCRIPTION_NOT_SUPPORTED:Lcom/android/internal/telephony/CommandException$Error;

.field public static final enum USSD_MODIFIED_TO_DIAL:Lcom/android/internal/telephony/CommandException$Error;

.field public static final enum USSD_MODIFIED_TO_SS:Lcom/android/internal/telephony/CommandException$Error;

.field public static final enum USSD_MODIFIED_TO_USSD:Lcom/android/internal/telephony/CommandException$Error;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 35
    new-instance v0, Lcom/android/internal/telephony/CommandException$Error;

    #@7
    const-string v1, "INVALID_RESPONSE"

    #@9
    invoke-direct {v0, v1, v3}, Lcom/android/internal/telephony/CommandException$Error;-><init>(Ljava/lang/String;I)V

    #@c
    sput-object v0, Lcom/android/internal/telephony/CommandException$Error;->INVALID_RESPONSE:Lcom/android/internal/telephony/CommandException$Error;

    #@e
    .line 36
    new-instance v0, Lcom/android/internal/telephony/CommandException$Error;

    #@10
    const-string v1, "RADIO_NOT_AVAILABLE"

    #@12
    invoke-direct {v0, v1, v4}, Lcom/android/internal/telephony/CommandException$Error;-><init>(Ljava/lang/String;I)V

    #@15
    sput-object v0, Lcom/android/internal/telephony/CommandException$Error;->RADIO_NOT_AVAILABLE:Lcom/android/internal/telephony/CommandException$Error;

    #@17
    .line 37
    new-instance v0, Lcom/android/internal/telephony/CommandException$Error;

    #@19
    const-string v1, "GENERIC_FAILURE"

    #@1b
    invoke-direct {v0, v1, v5}, Lcom/android/internal/telephony/CommandException$Error;-><init>(Ljava/lang/String;I)V

    #@1e
    sput-object v0, Lcom/android/internal/telephony/CommandException$Error;->GENERIC_FAILURE:Lcom/android/internal/telephony/CommandException$Error;

    #@20
    .line 38
    new-instance v0, Lcom/android/internal/telephony/CommandException$Error;

    #@22
    const-string v1, "PASSWORD_INCORRECT"

    #@24
    invoke-direct {v0, v1, v6}, Lcom/android/internal/telephony/CommandException$Error;-><init>(Ljava/lang/String;I)V

    #@27
    sput-object v0, Lcom/android/internal/telephony/CommandException$Error;->PASSWORD_INCORRECT:Lcom/android/internal/telephony/CommandException$Error;

    #@29
    .line 39
    new-instance v0, Lcom/android/internal/telephony/CommandException$Error;

    #@2b
    const-string v1, "SIM_PIN2"

    #@2d
    invoke-direct {v0, v1, v7}, Lcom/android/internal/telephony/CommandException$Error;-><init>(Ljava/lang/String;I)V

    #@30
    sput-object v0, Lcom/android/internal/telephony/CommandException$Error;->SIM_PIN2:Lcom/android/internal/telephony/CommandException$Error;

    #@32
    .line 40
    new-instance v0, Lcom/android/internal/telephony/CommandException$Error;

    #@34
    const-string v1, "SIM_PUK2"

    #@36
    const/4 v2, 0x5

    #@37
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/CommandException$Error;-><init>(Ljava/lang/String;I)V

    #@3a
    sput-object v0, Lcom/android/internal/telephony/CommandException$Error;->SIM_PUK2:Lcom/android/internal/telephony/CommandException$Error;

    #@3c
    .line 41
    new-instance v0, Lcom/android/internal/telephony/CommandException$Error;

    #@3e
    const-string v1, "REQUEST_NOT_SUPPORTED"

    #@40
    const/4 v2, 0x6

    #@41
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/CommandException$Error;-><init>(Ljava/lang/String;I)V

    #@44
    sput-object v0, Lcom/android/internal/telephony/CommandException$Error;->REQUEST_NOT_SUPPORTED:Lcom/android/internal/telephony/CommandException$Error;

    #@46
    .line 42
    new-instance v0, Lcom/android/internal/telephony/CommandException$Error;

    #@48
    const-string v1, "OP_NOT_ALLOWED_DURING_VOICE_CALL"

    #@4a
    const/4 v2, 0x7

    #@4b
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/CommandException$Error;-><init>(Ljava/lang/String;I)V

    #@4e
    sput-object v0, Lcom/android/internal/telephony/CommandException$Error;->OP_NOT_ALLOWED_DURING_VOICE_CALL:Lcom/android/internal/telephony/CommandException$Error;

    #@50
    .line 43
    new-instance v0, Lcom/android/internal/telephony/CommandException$Error;

    #@52
    const-string v1, "OP_NOT_ALLOWED_BEFORE_REG_NW"

    #@54
    const/16 v2, 0x8

    #@56
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/CommandException$Error;-><init>(Ljava/lang/String;I)V

    #@59
    sput-object v0, Lcom/android/internal/telephony/CommandException$Error;->OP_NOT_ALLOWED_BEFORE_REG_NW:Lcom/android/internal/telephony/CommandException$Error;

    #@5b
    .line 44
    new-instance v0, Lcom/android/internal/telephony/CommandException$Error;

    #@5d
    const-string v1, "SMS_FAIL_RETRY"

    #@5f
    const/16 v2, 0x9

    #@61
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/CommandException$Error;-><init>(Ljava/lang/String;I)V

    #@64
    sput-object v0, Lcom/android/internal/telephony/CommandException$Error;->SMS_FAIL_RETRY:Lcom/android/internal/telephony/CommandException$Error;

    #@66
    .line 45
    new-instance v0, Lcom/android/internal/telephony/CommandException$Error;

    #@68
    const-string v1, "SIM_ABSENT"

    #@6a
    const/16 v2, 0xa

    #@6c
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/CommandException$Error;-><init>(Ljava/lang/String;I)V

    #@6f
    sput-object v0, Lcom/android/internal/telephony/CommandException$Error;->SIM_ABSENT:Lcom/android/internal/telephony/CommandException$Error;

    #@71
    .line 46
    new-instance v0, Lcom/android/internal/telephony/CommandException$Error;

    #@73
    const-string v1, "SUBSCRIPTION_NOT_AVAILABLE"

    #@75
    const/16 v2, 0xb

    #@77
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/CommandException$Error;-><init>(Ljava/lang/String;I)V

    #@7a
    sput-object v0, Lcom/android/internal/telephony/CommandException$Error;->SUBSCRIPTION_NOT_AVAILABLE:Lcom/android/internal/telephony/CommandException$Error;

    #@7c
    .line 47
    new-instance v0, Lcom/android/internal/telephony/CommandException$Error;

    #@7e
    const-string v1, "MODE_NOT_SUPPORTED"

    #@80
    const/16 v2, 0xc

    #@82
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/CommandException$Error;-><init>(Ljava/lang/String;I)V

    #@85
    sput-object v0, Lcom/android/internal/telephony/CommandException$Error;->MODE_NOT_SUPPORTED:Lcom/android/internal/telephony/CommandException$Error;

    #@87
    .line 48
    new-instance v0, Lcom/android/internal/telephony/CommandException$Error;

    #@89
    const-string v1, "FDN_CHECK_FAILURE"

    #@8b
    const/16 v2, 0xd

    #@8d
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/CommandException$Error;-><init>(Ljava/lang/String;I)V

    #@90
    sput-object v0, Lcom/android/internal/telephony/CommandException$Error;->FDN_CHECK_FAILURE:Lcom/android/internal/telephony/CommandException$Error;

    #@92
    .line 49
    new-instance v0, Lcom/android/internal/telephony/CommandException$Error;

    #@94
    const-string v1, "ILLEGAL_SIM_OR_ME"

    #@96
    const/16 v2, 0xe

    #@98
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/CommandException$Error;-><init>(Ljava/lang/String;I)V

    #@9b
    sput-object v0, Lcom/android/internal/telephony/CommandException$Error;->ILLEGAL_SIM_OR_ME:Lcom/android/internal/telephony/CommandException$Error;

    #@9d
    .line 50
    new-instance v0, Lcom/android/internal/telephony/CommandException$Error;

    #@9f
    const-string v1, "DIAL_MODIFIED_TO_USSD"

    #@a1
    const/16 v2, 0xf

    #@a3
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/CommandException$Error;-><init>(Ljava/lang/String;I)V

    #@a6
    sput-object v0, Lcom/android/internal/telephony/CommandException$Error;->DIAL_MODIFIED_TO_USSD:Lcom/android/internal/telephony/CommandException$Error;

    #@a8
    .line 51
    new-instance v0, Lcom/android/internal/telephony/CommandException$Error;

    #@aa
    const-string v1, "DIAL_MODIFIED_TO_SS"

    #@ac
    const/16 v2, 0x10

    #@ae
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/CommandException$Error;-><init>(Ljava/lang/String;I)V

    #@b1
    sput-object v0, Lcom/android/internal/telephony/CommandException$Error;->DIAL_MODIFIED_TO_SS:Lcom/android/internal/telephony/CommandException$Error;

    #@b3
    .line 52
    new-instance v0, Lcom/android/internal/telephony/CommandException$Error;

    #@b5
    const-string v1, "DIAL_MODIFIED_TO_DIAL"

    #@b7
    const/16 v2, 0x11

    #@b9
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/CommandException$Error;-><init>(Ljava/lang/String;I)V

    #@bc
    sput-object v0, Lcom/android/internal/telephony/CommandException$Error;->DIAL_MODIFIED_TO_DIAL:Lcom/android/internal/telephony/CommandException$Error;

    #@be
    .line 53
    new-instance v0, Lcom/android/internal/telephony/CommandException$Error;

    #@c0
    const-string v1, "USSD_MODIFIED_TO_DIAL"

    #@c2
    const/16 v2, 0x12

    #@c4
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/CommandException$Error;-><init>(Ljava/lang/String;I)V

    #@c7
    sput-object v0, Lcom/android/internal/telephony/CommandException$Error;->USSD_MODIFIED_TO_DIAL:Lcom/android/internal/telephony/CommandException$Error;

    #@c9
    .line 54
    new-instance v0, Lcom/android/internal/telephony/CommandException$Error;

    #@cb
    const-string v1, "USSD_MODIFIED_TO_SS"

    #@cd
    const/16 v2, 0x13

    #@cf
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/CommandException$Error;-><init>(Ljava/lang/String;I)V

    #@d2
    sput-object v0, Lcom/android/internal/telephony/CommandException$Error;->USSD_MODIFIED_TO_SS:Lcom/android/internal/telephony/CommandException$Error;

    #@d4
    .line 55
    new-instance v0, Lcom/android/internal/telephony/CommandException$Error;

    #@d6
    const-string v1, "USSD_MODIFIED_TO_USSD"

    #@d8
    const/16 v2, 0x14

    #@da
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/CommandException$Error;-><init>(Ljava/lang/String;I)V

    #@dd
    sput-object v0, Lcom/android/internal/telephony/CommandException$Error;->USSD_MODIFIED_TO_USSD:Lcom/android/internal/telephony/CommandException$Error;

    #@df
    .line 56
    new-instance v0, Lcom/android/internal/telephony/CommandException$Error;

    #@e1
    const-string v1, "SS_MODIFIED_TO_DIAL"

    #@e3
    const/16 v2, 0x15

    #@e5
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/CommandException$Error;-><init>(Ljava/lang/String;I)V

    #@e8
    sput-object v0, Lcom/android/internal/telephony/CommandException$Error;->SS_MODIFIED_TO_DIAL:Lcom/android/internal/telephony/CommandException$Error;

    #@ea
    .line 57
    new-instance v0, Lcom/android/internal/telephony/CommandException$Error;

    #@ec
    const-string v1, "SS_MODIFIED_TO_USSD"

    #@ee
    const/16 v2, 0x16

    #@f0
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/CommandException$Error;-><init>(Ljava/lang/String;I)V

    #@f3
    sput-object v0, Lcom/android/internal/telephony/CommandException$Error;->SS_MODIFIED_TO_USSD:Lcom/android/internal/telephony/CommandException$Error;

    #@f5
    .line 58
    new-instance v0, Lcom/android/internal/telephony/CommandException$Error;

    #@f7
    const-string v1, "SS_MODIFIED_TO_SS"

    #@f9
    const/16 v2, 0x17

    #@fb
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/CommandException$Error;-><init>(Ljava/lang/String;I)V

    #@fe
    sput-object v0, Lcom/android/internal/telephony/CommandException$Error;->SS_MODIFIED_TO_SS:Lcom/android/internal/telephony/CommandException$Error;

    #@100
    .line 60
    new-instance v0, Lcom/android/internal/telephony/CommandException$Error;

    #@102
    const-string v1, "MISSING_RESOURCE"

    #@104
    const/16 v2, 0x18

    #@106
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/CommandException$Error;-><init>(Ljava/lang/String;I)V

    #@109
    sput-object v0, Lcom/android/internal/telephony/CommandException$Error;->MISSING_RESOURCE:Lcom/android/internal/telephony/CommandException$Error;

    #@10b
    .line 61
    new-instance v0, Lcom/android/internal/telephony/CommandException$Error;

    #@10d
    const-string v1, "NO_SUCH_ELEMENT"

    #@10f
    const/16 v2, 0x19

    #@111
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/CommandException$Error;-><init>(Ljava/lang/String;I)V

    #@114
    sput-object v0, Lcom/android/internal/telephony/CommandException$Error;->NO_SUCH_ELEMENT:Lcom/android/internal/telephony/CommandException$Error;

    #@116
    .line 62
    new-instance v0, Lcom/android/internal/telephony/CommandException$Error;

    #@118
    const-string v1, "INVALID_PARAMETER"

    #@11a
    const/16 v2, 0x1a

    #@11c
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/CommandException$Error;-><init>(Ljava/lang/String;I)V

    #@11f
    sput-object v0, Lcom/android/internal/telephony/CommandException$Error;->INVALID_PARAMETER:Lcom/android/internal/telephony/CommandException$Error;

    #@121
    .line 65
    new-instance v0, Lcom/android/internal/telephony/CommandException$Error;

    #@123
    const-string v1, "NET_SEL_FAILED_BY_PLMN_NOT_ALLOWED"

    #@125
    const/16 v2, 0x1b

    #@127
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/CommandException$Error;-><init>(Ljava/lang/String;I)V

    #@12a
    sput-object v0, Lcom/android/internal/telephony/CommandException$Error;->NET_SEL_FAILED_BY_PLMN_NOT_ALLOWED:Lcom/android/internal/telephony/CommandException$Error;

    #@12c
    .line 67
    new-instance v0, Lcom/android/internal/telephony/CommandException$Error;

    #@12e
    const-string v1, "SUBSCRIPTION_NOT_SUPPORTED"

    #@130
    const/16 v2, 0x1c

    #@132
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/CommandException$Error;-><init>(Ljava/lang/String;I)V

    #@135
    sput-object v0, Lcom/android/internal/telephony/CommandException$Error;->SUBSCRIPTION_NOT_SUPPORTED:Lcom/android/internal/telephony/CommandException$Error;

    #@137
    .line 34
    const/16 v0, 0x1d

    #@139
    new-array v0, v0, [Lcom/android/internal/telephony/CommandException$Error;

    #@13b
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->INVALID_RESPONSE:Lcom/android/internal/telephony/CommandException$Error;

    #@13d
    aput-object v1, v0, v3

    #@13f
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->RADIO_NOT_AVAILABLE:Lcom/android/internal/telephony/CommandException$Error;

    #@141
    aput-object v1, v0, v4

    #@143
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->GENERIC_FAILURE:Lcom/android/internal/telephony/CommandException$Error;

    #@145
    aput-object v1, v0, v5

    #@147
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->PASSWORD_INCORRECT:Lcom/android/internal/telephony/CommandException$Error;

    #@149
    aput-object v1, v0, v6

    #@14b
    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->SIM_PIN2:Lcom/android/internal/telephony/CommandException$Error;

    #@14d
    aput-object v1, v0, v7

    #@14f
    const/4 v1, 0x5

    #@150
    sget-object v2, Lcom/android/internal/telephony/CommandException$Error;->SIM_PUK2:Lcom/android/internal/telephony/CommandException$Error;

    #@152
    aput-object v2, v0, v1

    #@154
    const/4 v1, 0x6

    #@155
    sget-object v2, Lcom/android/internal/telephony/CommandException$Error;->REQUEST_NOT_SUPPORTED:Lcom/android/internal/telephony/CommandException$Error;

    #@157
    aput-object v2, v0, v1

    #@159
    const/4 v1, 0x7

    #@15a
    sget-object v2, Lcom/android/internal/telephony/CommandException$Error;->OP_NOT_ALLOWED_DURING_VOICE_CALL:Lcom/android/internal/telephony/CommandException$Error;

    #@15c
    aput-object v2, v0, v1

    #@15e
    const/16 v1, 0x8

    #@160
    sget-object v2, Lcom/android/internal/telephony/CommandException$Error;->OP_NOT_ALLOWED_BEFORE_REG_NW:Lcom/android/internal/telephony/CommandException$Error;

    #@162
    aput-object v2, v0, v1

    #@164
    const/16 v1, 0x9

    #@166
    sget-object v2, Lcom/android/internal/telephony/CommandException$Error;->SMS_FAIL_RETRY:Lcom/android/internal/telephony/CommandException$Error;

    #@168
    aput-object v2, v0, v1

    #@16a
    const/16 v1, 0xa

    #@16c
    sget-object v2, Lcom/android/internal/telephony/CommandException$Error;->SIM_ABSENT:Lcom/android/internal/telephony/CommandException$Error;

    #@16e
    aput-object v2, v0, v1

    #@170
    const/16 v1, 0xb

    #@172
    sget-object v2, Lcom/android/internal/telephony/CommandException$Error;->SUBSCRIPTION_NOT_AVAILABLE:Lcom/android/internal/telephony/CommandException$Error;

    #@174
    aput-object v2, v0, v1

    #@176
    const/16 v1, 0xc

    #@178
    sget-object v2, Lcom/android/internal/telephony/CommandException$Error;->MODE_NOT_SUPPORTED:Lcom/android/internal/telephony/CommandException$Error;

    #@17a
    aput-object v2, v0, v1

    #@17c
    const/16 v1, 0xd

    #@17e
    sget-object v2, Lcom/android/internal/telephony/CommandException$Error;->FDN_CHECK_FAILURE:Lcom/android/internal/telephony/CommandException$Error;

    #@180
    aput-object v2, v0, v1

    #@182
    const/16 v1, 0xe

    #@184
    sget-object v2, Lcom/android/internal/telephony/CommandException$Error;->ILLEGAL_SIM_OR_ME:Lcom/android/internal/telephony/CommandException$Error;

    #@186
    aput-object v2, v0, v1

    #@188
    const/16 v1, 0xf

    #@18a
    sget-object v2, Lcom/android/internal/telephony/CommandException$Error;->DIAL_MODIFIED_TO_USSD:Lcom/android/internal/telephony/CommandException$Error;

    #@18c
    aput-object v2, v0, v1

    #@18e
    const/16 v1, 0x10

    #@190
    sget-object v2, Lcom/android/internal/telephony/CommandException$Error;->DIAL_MODIFIED_TO_SS:Lcom/android/internal/telephony/CommandException$Error;

    #@192
    aput-object v2, v0, v1

    #@194
    const/16 v1, 0x11

    #@196
    sget-object v2, Lcom/android/internal/telephony/CommandException$Error;->DIAL_MODIFIED_TO_DIAL:Lcom/android/internal/telephony/CommandException$Error;

    #@198
    aput-object v2, v0, v1

    #@19a
    const/16 v1, 0x12

    #@19c
    sget-object v2, Lcom/android/internal/telephony/CommandException$Error;->USSD_MODIFIED_TO_DIAL:Lcom/android/internal/telephony/CommandException$Error;

    #@19e
    aput-object v2, v0, v1

    #@1a0
    const/16 v1, 0x13

    #@1a2
    sget-object v2, Lcom/android/internal/telephony/CommandException$Error;->USSD_MODIFIED_TO_SS:Lcom/android/internal/telephony/CommandException$Error;

    #@1a4
    aput-object v2, v0, v1

    #@1a6
    const/16 v1, 0x14

    #@1a8
    sget-object v2, Lcom/android/internal/telephony/CommandException$Error;->USSD_MODIFIED_TO_USSD:Lcom/android/internal/telephony/CommandException$Error;

    #@1aa
    aput-object v2, v0, v1

    #@1ac
    const/16 v1, 0x15

    #@1ae
    sget-object v2, Lcom/android/internal/telephony/CommandException$Error;->SS_MODIFIED_TO_DIAL:Lcom/android/internal/telephony/CommandException$Error;

    #@1b0
    aput-object v2, v0, v1

    #@1b2
    const/16 v1, 0x16

    #@1b4
    sget-object v2, Lcom/android/internal/telephony/CommandException$Error;->SS_MODIFIED_TO_USSD:Lcom/android/internal/telephony/CommandException$Error;

    #@1b6
    aput-object v2, v0, v1

    #@1b8
    const/16 v1, 0x17

    #@1ba
    sget-object v2, Lcom/android/internal/telephony/CommandException$Error;->SS_MODIFIED_TO_SS:Lcom/android/internal/telephony/CommandException$Error;

    #@1bc
    aput-object v2, v0, v1

    #@1be
    const/16 v1, 0x18

    #@1c0
    sget-object v2, Lcom/android/internal/telephony/CommandException$Error;->MISSING_RESOURCE:Lcom/android/internal/telephony/CommandException$Error;

    #@1c2
    aput-object v2, v0, v1

    #@1c4
    const/16 v1, 0x19

    #@1c6
    sget-object v2, Lcom/android/internal/telephony/CommandException$Error;->NO_SUCH_ELEMENT:Lcom/android/internal/telephony/CommandException$Error;

    #@1c8
    aput-object v2, v0, v1

    #@1ca
    const/16 v1, 0x1a

    #@1cc
    sget-object v2, Lcom/android/internal/telephony/CommandException$Error;->INVALID_PARAMETER:Lcom/android/internal/telephony/CommandException$Error;

    #@1ce
    aput-object v2, v0, v1

    #@1d0
    const/16 v1, 0x1b

    #@1d2
    sget-object v2, Lcom/android/internal/telephony/CommandException$Error;->NET_SEL_FAILED_BY_PLMN_NOT_ALLOWED:Lcom/android/internal/telephony/CommandException$Error;

    #@1d4
    aput-object v2, v0, v1

    #@1d6
    const/16 v1, 0x1c

    #@1d8
    sget-object v2, Lcom/android/internal/telephony/CommandException$Error;->SUBSCRIPTION_NOT_SUPPORTED:Lcom/android/internal/telephony/CommandException$Error;

    #@1da
    aput-object v2, v0, v1

    #@1dc
    sput-object v0, Lcom/android/internal/telephony/CommandException$Error;->$VALUES:[Lcom/android/internal/telephony/CommandException$Error;

    #@1de
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/internal/telephony/CommandException$Error;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 34
    const-class v0, Lcom/android/internal/telephony/CommandException$Error;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/telephony/CommandException$Error;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/android/internal/telephony/CommandException$Error;
    .registers 1

    #@0
    .prologue
    .line 34
    sget-object v0, Lcom/android/internal/telephony/CommandException$Error;->$VALUES:[Lcom/android/internal/telephony/CommandException$Error;

    #@2
    invoke-virtual {v0}, [Lcom/android/internal/telephony/CommandException$Error;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/android/internal/telephony/CommandException$Error;

    #@8
    return-object v0
.end method
