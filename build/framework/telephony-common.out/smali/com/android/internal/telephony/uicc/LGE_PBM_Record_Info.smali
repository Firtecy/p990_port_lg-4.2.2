.class public Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;
.super Ljava/lang/Object;
.source "LGE_PBM_Record_Info.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final RECORD_INFO_MAX_PB_SETS:I

.field public device:I

.field public file_type:I

.field public max_num_length:I

.field public max_text_length:I

.field public num_of_free_ext:I

.field public num_of_tot_rec:I

.field public num_of_used_rec:I

.field public records_in_pb_set:[I

.field public status:I

.field public used_records_in_pb_set:[I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 26
    new-instance v0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info$1;

    #@2
    invoke-direct {v0}, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info$1;-><init>()V

    #@5
    sput-object v0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/16 v1, 0x8

    #@2
    .line 39
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 10
    iput v1, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->RECORD_INFO_MAX_PB_SETS:I

    #@7
    .line 21
    new-array v0, v1, [I

    #@9
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->records_in_pb_set:[I

    #@b
    .line 22
    new-array v0, v1, [I

    #@d
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->used_records_in_pb_set:[I

    #@f
    .line 41
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 4
    .parameter "source"

    #@0
    .prologue
    const/16 v1, 0x8

    #@2
    .line 43
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 10
    iput v1, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->RECORD_INFO_MAX_PB_SETS:I

    #@7
    .line 21
    new-array v0, v1, [I

    #@9
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->records_in_pb_set:[I

    #@b
    .line 22
    new-array v0, v1, [I

    #@d
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->used_records_in_pb_set:[I

    #@f
    .line 44
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->readFromParcel(Landroid/os/Parcel;)V

    #@12
    .line 45
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 7
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;-><init>(Landroid/os/Parcel;)V

    #@3
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 76
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .registers 3
    .parameter "source"

    #@0
    .prologue
    .line 62
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v0

    #@4
    iput v0, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->device:I

    #@6
    .line 63
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@9
    move-result v0

    #@a
    iput v0, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->status:I

    #@c
    .line 64
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@f
    move-result v0

    #@10
    iput v0, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->max_num_length:I

    #@12
    .line 65
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@15
    move-result v0

    #@16
    iput v0, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->max_text_length:I

    #@18
    .line 66
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@1b
    move-result v0

    #@1c
    iput v0, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->num_of_used_rec:I

    #@1e
    .line 67
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@21
    move-result v0

    #@22
    iput v0, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->num_of_free_ext:I

    #@24
    .line 68
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@27
    move-result v0

    #@28
    iput v0, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->num_of_tot_rec:I

    #@2a
    .line 69
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@2d
    move-result v0

    #@2e
    iput v0, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->file_type:I

    #@30
    .line 71
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->records_in_pb_set:[I

    #@32
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readIntArray([I)V

    #@35
    .line 72
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->used_records_in_pb_set:[I

    #@37
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readIntArray([I)V

    #@3a
    .line 73
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 9

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 81
    new-instance v0, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v0

    #@12
    const-string v1, "Device  "

    #@14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v0

    #@18
    iget v1, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->device:I

    #@1a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v0

    #@1e
    const-string v1, " status: "

    #@20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v0

    #@24
    iget v1, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->status:I

    #@26
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@29
    move-result-object v0

    #@2a
    const-string v1, " max_num_length: "

    #@2c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v0

    #@30
    iget v1, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->max_num_length:I

    #@32
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@35
    move-result-object v0

    #@36
    const-string v1, " max_text_length: "

    #@38
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v0

    #@3c
    iget v1, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->max_text_length:I

    #@3e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@41
    move-result-object v0

    #@42
    const-string v1, " num_of_used_rec: "

    #@44
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v0

    #@48
    iget v1, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->num_of_used_rec:I

    #@4a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v0

    #@4e
    const-string v1, " num_of_free_ext: "

    #@50
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v0

    #@54
    iget v1, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->num_of_free_ext:I

    #@56
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@59
    move-result-object v0

    #@5a
    const-string v1, " num_of_tot_rec: "

    #@5c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v0

    #@60
    iget v1, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->num_of_tot_rec:I

    #@62
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@65
    move-result-object v0

    #@66
    const-string v1, " file_type: "

    #@68
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v0

    #@6c
    iget v1, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->file_type:I

    #@6e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@71
    move-result-object v0

    #@72
    const-string v1, " records_in_pb_set: "

    #@74
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v0

    #@78
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->records_in_pb_set:[I

    #@7a
    aget v1, v1, v3

    #@7c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v0

    #@80
    const-string v1, " "

    #@82
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v0

    #@86
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->records_in_pb_set:[I

    #@88
    aget v1, v1, v4

    #@8a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8d
    move-result-object v0

    #@8e
    const-string v1, " "

    #@90
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@93
    move-result-object v0

    #@94
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->records_in_pb_set:[I

    #@96
    aget v1, v1, v5

    #@98
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v0

    #@9c
    const-string v1, " "

    #@9e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v0

    #@a2
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->records_in_pb_set:[I

    #@a4
    aget v1, v1, v6

    #@a6
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v0

    #@aa
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->records_in_pb_set:[I

    #@ac
    aget v1, v1, v7

    #@ae
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v0

    #@b2
    const-string v1, " "

    #@b4
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b7
    move-result-object v0

    #@b8
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->records_in_pb_set:[I

    #@ba
    const/4 v2, 0x5

    #@bb
    aget v1, v1, v2

    #@bd
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v0

    #@c1
    const-string v1, " "

    #@c3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v0

    #@c7
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->records_in_pb_set:[I

    #@c9
    const/4 v2, 0x6

    #@ca
    aget v1, v1, v2

    #@cc
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@cf
    move-result-object v0

    #@d0
    const-string v1, " "

    #@d2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v0

    #@d6
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->records_in_pb_set:[I

    #@d8
    const/4 v2, 0x7

    #@d9
    aget v1, v1, v2

    #@db
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@de
    move-result-object v0

    #@df
    const-string v1, " used_records_in_pb_set: "

    #@e1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e4
    move-result-object v0

    #@e5
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->used_records_in_pb_set:[I

    #@e7
    aget v1, v1, v3

    #@e9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ec
    move-result-object v0

    #@ed
    const-string v1, " "

    #@ef
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f2
    move-result-object v0

    #@f3
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->used_records_in_pb_set:[I

    #@f5
    aget v1, v1, v4

    #@f7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@fa
    move-result-object v0

    #@fb
    const-string v1, " "

    #@fd
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@100
    move-result-object v0

    #@101
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->used_records_in_pb_set:[I

    #@103
    aget v1, v1, v5

    #@105
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@108
    move-result-object v0

    #@109
    const-string v1, " "

    #@10b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10e
    move-result-object v0

    #@10f
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->used_records_in_pb_set:[I

    #@111
    aget v1, v1, v6

    #@113
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@116
    move-result-object v0

    #@117
    const-string v1, " "

    #@119
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11c
    move-result-object v0

    #@11d
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->used_records_in_pb_set:[I

    #@11f
    aget v1, v1, v7

    #@121
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@124
    move-result-object v0

    #@125
    const-string v1, " "

    #@127
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12a
    move-result-object v0

    #@12b
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->used_records_in_pb_set:[I

    #@12d
    const/4 v2, 0x5

    #@12e
    aget v1, v1, v2

    #@130
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@133
    move-result-object v0

    #@134
    const-string v1, " "

    #@136
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@139
    move-result-object v0

    #@13a
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->used_records_in_pb_set:[I

    #@13c
    const/4 v2, 0x6

    #@13d
    aget v1, v1, v2

    #@13f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@142
    move-result-object v0

    #@143
    const-string v1, " "

    #@145
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@148
    move-result-object v0

    #@149
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->used_records_in_pb_set:[I

    #@14b
    const/4 v2, 0x7

    #@14c
    aget v1, v1, v2

    #@14e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@151
    move-result-object v0

    #@152
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@155
    move-result-object v0

    #@156
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 48
    iget v0, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->device:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 49
    iget v0, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->status:I

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 50
    iget v0, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->max_num_length:I

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 51
    iget v0, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->max_text_length:I

    #@11
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 52
    iget v0, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->num_of_used_rec:I

    #@16
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 53
    iget v0, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->num_of_free_ext:I

    #@1b
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@1e
    .line 54
    iget v0, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->num_of_tot_rec:I

    #@20
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@23
    .line 55
    iget v0, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->file_type:I

    #@25
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@28
    .line 57
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->records_in_pb_set:[I

    #@2a
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeIntArray([I)V

    #@2d
    .line 58
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/LGE_PBM_Record_Info;->used_records_in_pb_set:[I

    #@2f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeIntArray([I)V

    #@32
    .line 59
    return-void
.end method
