.class Lcom/android/internal/telephony/LgeTimeZoneMonitor$3;
.super Ljava/lang/Object;
.source "LgeTimeZoneMonitor.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/LgeTimeZoneMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/LgeTimeZoneMonitor;


# direct methods
.method constructor <init>(Lcom/android/internal/telephony/LgeTimeZoneMonitor;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 283
    iput-object p1, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor$3;->this$0:Lcom/android/internal/telephony/LgeTimeZoneMonitor;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .registers 7
    .parameter "dialog"
    .parameter "which"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 285
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    #@4
    .line 287
    iget-object v1, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor$3;->this$0:Lcom/android/internal/telephony/LgeTimeZoneMonitor;

    #@6
    const/4 v2, 0x0

    #@7
    iput-object v2, v1, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->mManagedTimeZoneSettingDialog:Landroid/app/AlertDialog;

    #@9
    .line 289
    packed-switch p2, :pswitch_data_48

    #@c
    .line 301
    :goto_c
    return-void

    #@d
    .line 291
    :pswitch_d
    iget-object v1, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor$3;->this$0:Lcom/android/internal/telephony/LgeTimeZoneMonitor;

    #@f
    invoke-static {v1}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->access$100(Lcom/android/internal/telephony/LgeTimeZoneMonitor;)Landroid/content/Context;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@16
    move-result-object v1

    #@17
    const-string v2, "auto_time"

    #@19
    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@1c
    .line 292
    iget-object v1, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor$3;->this$0:Lcom/android/internal/telephony/LgeTimeZoneMonitor;

    #@1e
    invoke-static {v1}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->access$100(Lcom/android/internal/telephony/LgeTimeZoneMonitor;)Landroid/content/Context;

    #@21
    move-result-object v1

    #@22
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@25
    move-result-object v1

    #@26
    const-string v2, "auto_time_zone"

    #@28
    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@2b
    .line 293
    new-instance v0, Landroid/content/Intent;

    #@2d
    const-string v1, "android.intent.action.MAIN"

    #@2f
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@32
    .line 294
    .local v0, zoneListIntent:Landroid/content/Intent;
    const-string v1, "com.android.settings"

    #@34
    const-string v2, "com.android.settings.Settings$ZonePickerActivity"

    #@36
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@39
    .line 295
    const/high16 v1, 0x1000

    #@3b
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@3e
    .line 296
    iget-object v1, p0, Lcom/android/internal/telephony/LgeTimeZoneMonitor$3;->this$0:Lcom/android/internal/telephony/LgeTimeZoneMonitor;

    #@40
    invoke-static {v1}, Lcom/android/internal/telephony/LgeTimeZoneMonitor;->access$100(Lcom/android/internal/telephony/LgeTimeZoneMonitor;)Landroid/content/Context;

    #@43
    move-result-object v1

    #@44
    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@47
    goto :goto_c

    #@48
    .line 289
    :pswitch_data_48
    .packed-switch -0x1
        :pswitch_d
    .end packed-switch
.end method
