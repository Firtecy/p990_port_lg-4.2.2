.class public Lcom/android/internal/telephony/LgeFastDormancyHandler;
.super Ljava/lang/Object;
.source "LgeFastDormancyHandler.java"


# static fields
.field private static fastDormancyConstructor:Ljava/lang/reflect/Constructor;

.field private static mClass:Ljava/lang/Class;

.field private static mILgeFastDormancyHandler:Lcom/android/internal/telephony/ILgeFastDormancyHandler;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 32
    sput-object v1, Lcom/android/internal/telephony/LgeFastDormancyHandler;->fastDormancyConstructor:Ljava/lang/reflect/Constructor;

    #@3
    .line 33
    sput-object v1, Lcom/android/internal/telephony/LgeFastDormancyHandler;->mClass:Ljava/lang/Class;

    #@5
    .line 34
    sput-object v1, Lcom/android/internal/telephony/LgeFastDormancyHandler;->mILgeFastDormancyHandler:Lcom/android/internal/telephony/ILgeFastDormancyHandler;

    #@7
    .line 37
    :try_start_7
    const-string v1, "com.android.internal.telephony.LgeFDHandlerInterfaceImpl"

    #@9
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    #@c
    move-result-object v1

    #@d
    sput-object v1, Lcom/android/internal/telephony/LgeFastDormancyHandler;->mClass:Ljava/lang/Class;

    #@f
    .line 38
    sget-object v1, Lcom/android/internal/telephony/LgeFastDormancyHandler;->mClass:Ljava/lang/Class;

    #@11
    const/4 v2, 0x5

    #@12
    new-array v2, v2, [Ljava/lang/Class;

    #@14
    const/4 v3, 0x0

    #@15
    const-class v4, Landroid/content/Context;

    #@17
    aput-object v4, v2, v3

    #@19
    const/4 v3, 0x1

    #@1a
    const-class v4, Lcom/android/internal/telephony/CommandsInterface;

    #@1c
    aput-object v4, v2, v3

    #@1e
    const/4 v3, 0x2

    #@1f
    const-class v4, Lcom/android/internal/telephony/ServiceStateTracker;

    #@21
    aput-object v4, v2, v3

    #@23
    const/4 v3, 0x3

    #@24
    const-class v4, Lcom/android/internal/telephony/gsm/GsmDataConnectionTracker;

    #@26
    aput-object v4, v2, v3

    #@28
    const/4 v3, 0x4

    #@29
    const-class v4, Lcom/android/internal/telephony/PhoneBase;

    #@2b
    aput-object v4, v2, v3

    #@2d
    invoke-virtual {v1, v2}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    #@30
    move-result-object v1

    #@31
    sput-object v1, Lcom/android/internal/telephony/LgeFastDormancyHandler;->fastDormancyConstructor:Ljava/lang/reflect/Constructor;
    :try_end_33
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_33} :catch_34

    #@33
    .line 43
    .local v0, e:Ljava/lang/Exception;
    :goto_33
    return-void

    #@34
    .line 40
    .end local v0           #e:Ljava/lang/Exception;
    :catch_34
    move-exception v0

    #@35
    .line 41
    .restart local v0       #e:Ljava/lang/Exception;
    const-string v1, "LgeFastDormancyHandler"

    #@37
    new-instance v2, Ljava/lang/StringBuilder;

    #@39
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3c
    const-string v3, "Library loading failure: "

    #@3e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v2

    #@42
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v2

    #@46
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@49
    move-result-object v2

    #@4a
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4d
    goto :goto_33
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 31
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static newInstance(Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;Lcom/android/internal/telephony/ServiceStateTracker;Lcom/android/internal/telephony/gsm/GsmDataConnectionTracker;Lcom/android/internal/telephony/PhoneBase;)Lcom/android/internal/telephony/ILgeFastDormancyHandler;
    .registers 9
    .parameter "mContext"
    .parameter "mCm"
    .parameter "mSst"
    .parameter "mDct"
    .parameter "mP"

    #@0
    .prologue
    .line 47
    sget-object v1, Lcom/android/internal/telephony/LgeFastDormancyHandler;->mILgeFastDormancyHandler:Lcom/android/internal/telephony/ILgeFastDormancyHandler;

    #@2
    if-eqz v1, :cond_f

    #@4
    invoke-interface {p1}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@7
    move-result-object v1

    #@8
    iget-boolean v1, v1, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DORMANT_FD_LGU:Z

    #@a
    if-nez v1, :cond_f

    #@c
    .line 51
    sget-object v1, Lcom/android/internal/telephony/LgeFastDormancyHandler;->mILgeFastDormancyHandler:Lcom/android/internal/telephony/ILgeFastDormancyHandler;

    #@e
    .line 58
    :goto_e
    return-object v1

    #@f
    .line 54
    :cond_f
    :try_start_f
    sget-object v1, Lcom/android/internal/telephony/LgeFastDormancyHandler;->fastDormancyConstructor:Ljava/lang/reflect/Constructor;

    #@11
    const/4 v2, 0x5

    #@12
    new-array v2, v2, [Ljava/lang/Object;

    #@14
    const/4 v3, 0x0

    #@15
    aput-object p0, v2, v3

    #@17
    const/4 v3, 0x1

    #@18
    aput-object p1, v2, v3

    #@1a
    const/4 v3, 0x2

    #@1b
    aput-object p2, v2, v3

    #@1d
    const/4 v3, 0x3

    #@1e
    aput-object p3, v2, v3

    #@20
    const/4 v3, 0x4

    #@21
    aput-object p4, v2, v3

    #@23
    invoke-virtual {v1, v2}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    #@26
    move-result-object v1

    #@27
    check-cast v1, Lcom/android/internal/telephony/ILgeFastDormancyHandler;

    #@29
    sput-object v1, Lcom/android/internal/telephony/LgeFastDormancyHandler;->mILgeFastDormancyHandler:Lcom/android/internal/telephony/ILgeFastDormancyHandler;
    :try_end_2b
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_2b} :catch_2e

    #@2b
    .line 58
    :goto_2b
    sget-object v1, Lcom/android/internal/telephony/LgeFastDormancyHandler;->mILgeFastDormancyHandler:Lcom/android/internal/telephony/ILgeFastDormancyHandler;

    #@2d
    goto :goto_e

    #@2e
    .line 55
    :catch_2e
    move-exception v0

    #@2f
    .line 56
    .local v0, e:Ljava/lang/Exception;
    const-string v1, "LgeFastDormancyHandler"

    #@31
    new-instance v2, Ljava/lang/StringBuilder;

    #@33
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@36
    const-string v3, "Can not make newInstance of fastDormancyConstructor: "

    #@38
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v2

    #@3c
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v2

    #@40
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v2

    #@44
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@47
    goto :goto_2b
.end method
