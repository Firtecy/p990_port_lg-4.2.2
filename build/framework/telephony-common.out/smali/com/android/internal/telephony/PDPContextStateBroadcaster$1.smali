.class final Lcom/android/internal/telephony/PDPContextStateBroadcaster$1;
.super Ljava/util/HashMap;
.source "PDPContextStateBroadcaster.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/PDPContextStateBroadcaster;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 6

    #@0
    .prologue
    const/16 v4, 0x133

    #@2
    const/16 v3, 0x12d

    #@4
    .line 142
    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    #@7
    .line 143
    const/4 v0, 0x0

    #@8
    new-instance v1, Ljava/lang/Integer;

    #@a
    invoke-direct {v1, v3}, Ljava/lang/Integer;-><init>(I)V

    #@d
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/PDPContextStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@10
    .line 144
    const-string v0, "radioTurnedOff"

    #@12
    new-instance v1, Ljava/lang/Integer;

    #@14
    const/16 v2, 0x12e

    #@16
    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    #@19
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/PDPContextStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1c
    .line 145
    const-string v0, "unknownPdpDisconnect"

    #@1e
    new-instance v1, Ljava/lang/Integer;

    #@20
    const/16 v2, 0x12f

    #@22
    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    #@25
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/PDPContextStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@28
    .line 146
    const-string v0, "unknown data error"

    #@2a
    new-instance v1, Ljava/lang/Integer;

    #@2c
    const/16 v2, 0x130

    #@2e
    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    #@31
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/PDPContextStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@34
    .line 147
    const-string v0, "roamingOn"

    #@36
    new-instance v1, Ljava/lang/Integer;

    #@38
    const/16 v2, 0x131

    #@3a
    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    #@3d
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/PDPContextStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@40
    .line 148
    const-string v0, "roamingOff"

    #@42
    new-instance v1, Ljava/lang/Integer;

    #@44
    const/16 v2, 0x132

    #@46
    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    #@49
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/PDPContextStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@4c
    .line 149
    const-string v0, "dataDisabled"

    #@4e
    new-instance v1, Ljava/lang/Integer;

    #@50
    invoke-direct {v1, v4}, Ljava/lang/Integer;-><init>(I)V

    #@53
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/PDPContextStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@56
    .line 150
    const-string v0, "specificDisabled"

    #@58
    new-instance v1, Ljava/lang/Integer;

    #@5a
    invoke-direct {v1, v4}, Ljava/lang/Integer;-><init>(I)V

    #@5d
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/PDPContextStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@60
    .line 151
    const-string v0, "dataEnabled"

    #@62
    new-instance v1, Ljava/lang/Integer;

    #@64
    const/16 v2, 0x134

    #@66
    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    #@69
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/PDPContextStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@6c
    .line 152
    const-string v0, "dataAttached"

    #@6e
    new-instance v1, Ljava/lang/Integer;

    #@70
    const/16 v2, 0x135

    #@72
    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    #@75
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/PDPContextStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@78
    .line 153
    const-string v0, "dataDetached"

    #@7a
    new-instance v1, Ljava/lang/Integer;

    #@7c
    const/16 v2, 0x136

    #@7e
    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    #@81
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/PDPContextStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@84
    .line 154
    const-string v0, "cdmaDataAttached"

    #@86
    new-instance v1, Ljava/lang/Integer;

    #@88
    const/16 v2, 0x137

    #@8a
    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    #@8d
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/PDPContextStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@90
    .line 155
    const-string v0, "cdmaDataDetached"

    #@92
    new-instance v1, Ljava/lang/Integer;

    #@94
    const/16 v2, 0x138

    #@96
    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    #@99
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/PDPContextStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@9c
    .line 156
    const-string v0, "apnChanged"

    #@9e
    new-instance v1, Ljava/lang/Integer;

    #@a0
    const/16 v2, 0x139

    #@a2
    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    #@a5
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/PDPContextStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@a8
    .line 157
    const-string v0, "apnSwitched"

    #@aa
    new-instance v1, Ljava/lang/Integer;

    #@ac
    const/16 v2, 0x13a

    #@ae
    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    #@b1
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/PDPContextStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@b4
    .line 158
    const-string v0, "apnFailed"

    #@b6
    new-instance v1, Ljava/lang/Integer;

    #@b8
    const/16 v2, 0x13b

    #@ba
    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    #@bd
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/PDPContextStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@c0
    .line 159
    const-string v0, "restoreDefaultApn"

    #@c2
    new-instance v1, Ljava/lang/Integer;

    #@c4
    const/16 v2, 0x13c

    #@c6
    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    #@c9
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/PDPContextStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@cc
    .line 160
    const-string v0, "pdpReset"

    #@ce
    new-instance v1, Ljava/lang/Integer;

    #@d0
    const/16 v2, 0x13d

    #@d2
    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    #@d5
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/PDPContextStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@d8
    .line 161
    const-string v0, "2GVoiceCallEnded"

    #@da
    new-instance v1, Ljava/lang/Integer;

    #@dc
    const/16 v2, 0x13e

    #@de
    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    #@e1
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/PDPContextStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@e4
    .line 162
    const-string v0, "2GVoiceCallStarted"

    #@e6
    new-instance v1, Ljava/lang/Integer;

    #@e8
    const/16 v2, 0x13f

    #@ea
    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    #@ed
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/PDPContextStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@f0
    .line 163
    const-string v0, "psRestrictEnabled"

    #@f2
    new-instance v1, Ljava/lang/Integer;

    #@f4
    const/16 v2, 0x140

    #@f6
    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    #@f9
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/PDPContextStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@fc
    .line 164
    const-string v0, "psRestrictDisabled"

    #@fe
    new-instance v1, Ljava/lang/Integer;

    #@100
    const/16 v2, 0x141

    #@102
    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    #@105
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/PDPContextStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@108
    .line 165
    const-string v0, "simLoaded"

    #@10a
    new-instance v1, Ljava/lang/Integer;

    #@10c
    const/16 v2, 0x142

    #@10e
    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    #@111
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/PDPContextStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@114
    .line 166
    const-string v0, "apnTypeDisabled"

    #@116
    new-instance v1, Ljava/lang/Integer;

    #@118
    const/16 v2, 0x143

    #@11a
    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    #@11d
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/PDPContextStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@120
    .line 167
    const-string v0, "apnTypeEnabled"

    #@122
    new-instance v1, Ljava/lang/Integer;

    #@124
    const/16 v2, 0x144

    #@126
    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    #@129
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/PDPContextStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@12c
    .line 168
    const-string v0, "masterDataDisabled"

    #@12e
    new-instance v1, Ljava/lang/Integer;

    #@130
    const/16 v2, 0x145

    #@132
    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    #@135
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/PDPContextStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@138
    .line 169
    const-string v0, "masterDataEnabled"

    #@13a
    new-instance v1, Ljava/lang/Integer;

    #@13c
    const/16 v2, 0x146

    #@13e
    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    #@141
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/PDPContextStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@144
    .line 170
    const-string v0, "iccRecordsLoaded"

    #@146
    new-instance v1, Ljava/lang/Integer;

    #@148
    const/16 v2, 0x147

    #@14a
    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    #@14d
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/PDPContextStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@150
    .line 171
    const-string v0, "cdmaOtaProvisioning"

    #@152
    new-instance v1, Ljava/lang/Integer;

    #@154
    const/16 v2, 0x148

    #@156
    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    #@159
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/PDPContextStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@15c
    .line 172
    const-string v0, "defaultDataDisabled"

    #@15e
    new-instance v1, Ljava/lang/Integer;

    #@160
    const/16 v2, 0x149

    #@162
    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    #@165
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/PDPContextStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@168
    .line 173
    const-string v0, "defaultDataEnabled"

    #@16a
    new-instance v1, Ljava/lang/Integer;

    #@16c
    const/16 v2, 0x14a

    #@16e
    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    #@171
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/PDPContextStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@174
    .line 174
    const-string v0, "radioOn"

    #@176
    new-instance v1, Ljava/lang/Integer;

    #@178
    const/16 v2, 0x14b

    #@17a
    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    #@17d
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/PDPContextStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@180
    .line 175
    const-string v0, "radioOff"

    #@182
    new-instance v1, Ljava/lang/Integer;

    #@184
    const/16 v2, 0x14c

    #@186
    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    #@189
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/PDPContextStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@18c
    .line 176
    const-string v0, "radioTechnologyChanged"

    #@18e
    new-instance v1, Ljava/lang/Integer;

    #@190
    const/16 v2, 0x14d

    #@192
    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    #@195
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/PDPContextStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@198
    .line 177
    const-string v0, "networkOrModemDisconnect"

    #@19a
    new-instance v1, Ljava/lang/Integer;

    #@19c
    const/16 v2, 0x14e

    #@19e
    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    #@1a1
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/PDPContextStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1a4
    .line 178
    const-string v0, "dataNetworkAttached"

    #@1a6
    new-instance v1, Ljava/lang/Integer;

    #@1a8
    const/16 v2, 0x14f

    #@1aa
    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    #@1ad
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/PDPContextStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1b0
    .line 179
    const-string v0, "dataNetworkDetached"

    #@1b2
    new-instance v1, Ljava/lang/Integer;

    #@1b4
    const/16 v2, 0x150

    #@1b6
    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    #@1b9
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/PDPContextStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1bc
    .line 180
    const-string v0, "dataProfileDbChanged"

    #@1be
    new-instance v1, Ljava/lang/Integer;

    #@1c0
    const/16 v2, 0x151

    #@1c2
    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    #@1c5
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/PDPContextStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1c8
    .line 181
    const-string v0, "cdmaSubscriptionSourceChanged"

    #@1ca
    new-instance v1, Ljava/lang/Integer;

    #@1cc
    const/16 v2, 0x152

    #@1ce
    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    #@1d1
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/PDPContextStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1d4
    .line 182
    const-string v0, "tetheredModeChanged"

    #@1d6
    new-instance v1, Ljava/lang/Integer;

    #@1d8
    const/16 v2, 0x153

    #@1da
    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    #@1dd
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/PDPContextStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1e0
    .line 183
    const-string v0, "dataConnectionPropertyChanged"

    #@1e2
    new-instance v1, Ljava/lang/Integer;

    #@1e4
    const/16 v2, 0x154

    #@1e6
    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    #@1e9
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/PDPContextStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1ec
    .line 186
    const-string v0, "nwTypeChanged"

    #@1ee
    new-instance v1, Ljava/lang/Integer;

    #@1f0
    invoke-direct {v1, v3}, Ljava/lang/Integer;-><init>(I)V

    #@1f3
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/PDPContextStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1f6
    .line 187
    const-string v0, "dependencyMet"

    #@1f8
    new-instance v1, Ljava/lang/Integer;

    #@1fa
    invoke-direct {v1, v3}, Ljava/lang/Integer;-><init>(I)V

    #@1fd
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/PDPContextStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@200
    .line 188
    const-string v0, "dependencyUnmet"

    #@202
    new-instance v1, Ljava/lang/Integer;

    #@204
    invoke-direct {v1, v3}, Ljava/lang/Integer;-><init>(I)V

    #@207
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/PDPContextStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@20a
    .line 189
    const-string v0, "linkPropertiesChanged"

    #@20c
    new-instance v1, Ljava/lang/Integer;

    #@20e
    invoke-direct {v1, v3}, Ljava/lang/Integer;-><init>(I)V

    #@211
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/PDPContextStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@214
    .line 190
    return-void
.end method
