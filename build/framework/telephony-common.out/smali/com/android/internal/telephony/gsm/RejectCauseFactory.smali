.class public Lcom/android/internal/telephony/gsm/RejectCauseFactory;
.super Ljava/lang/Object;
.source "RejectCauseFactory.java"


# static fields
.field static final DBG:Z = true

.field static final LOG_TAG:Ljava/lang/String; = "RejectCauseFactory"

.field static sProxyRejectCause:Lcom/android/internal/telephony/gsm/RejectCause;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 24
    const/4 v0, 0x0

    #@1
    sput-object v0, Lcom/android/internal/telephony/gsm/RejectCauseFactory;->sProxyRejectCause:Lcom/android/internal/telephony/gsm/RejectCause;

    #@3
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 20
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static dispose()V
    .registers 1

    #@0
    .prologue
    .line 76
    sget-object v0, Lcom/android/internal/telephony/gsm/RejectCauseFactory;->sProxyRejectCause:Lcom/android/internal/telephony/gsm/RejectCause;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 77
    sget-object v0, Lcom/android/internal/telephony/gsm/RejectCauseFactory;->sProxyRejectCause:Lcom/android/internal/telephony/gsm/RejectCause;

    #@6
    invoke-interface {v0}, Lcom/android/internal/telephony/gsm/RejectCause;->initialize()V

    #@9
    .line 78
    const/4 v0, 0x0

    #@a
    sput-object v0, Lcom/android/internal/telephony/gsm/RejectCauseFactory;->sProxyRejectCause:Lcom/android/internal/telephony/gsm/RejectCause;

    #@c
    .line 80
    :cond_c
    return-void
.end method

.method public static getDefaultRejectCause(Lcom/android/internal/telephony/gsm/GSMPhone;)Lcom/android/internal/telephony/gsm/RejectCause;
    .registers 8
    .parameter "p"

    #@0
    .prologue
    .line 27
    sget-object v4, Lcom/android/internal/telephony/gsm/RejectCauseFactory;->sProxyRejectCause:Lcom/android/internal/telephony/gsm/RejectCause;

    #@2
    if-eqz v4, :cond_7

    #@4
    .line 28
    sget-object v4, Lcom/android/internal/telephony/gsm/RejectCauseFactory;->sProxyRejectCause:Lcom/android/internal/telephony/gsm/RejectCause;

    #@6
    .line 72
    :goto_6
    return-object v4

    #@7
    .line 32
    :cond_7
    :try_start_7
    new-instance v2, Ldalvik/system/PathClassLoader;

    #@9
    const-string v4, "/system/framework/com.lge.rejectcause.jar"

    #@b
    const-class v5, Lcom/android/internal/telephony/gsm/RejectCauseFactory;

    #@d
    invoke-virtual {v5}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    #@10
    move-result-object v5

    #@11
    invoke-direct {v2, v4, v5}, Ldalvik/system/PathClassLoader;-><init>(Ljava/lang/String;Ljava/lang/ClassLoader;)V

    #@14
    .line 34
    .local v2, rejectCauseClassloader:Ldalvik/system/PathClassLoader;
    const/4 v1, 0x0

    #@15
    .line 36
    .local v1, rejectCauseClass:Ljava/lang/Class;
    const-string v4, "KR"

    #@17
    const-string v5, "SKT"

    #@19
    invoke-static {v4, v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@1c
    move-result v4

    #@1d
    if-eqz v4, :cond_36

    #@1f
    .line 37
    invoke-static {}, Lcom/android/internal/telephony/TelephonyUtils;->isQCRIL()Z

    #@22
    move-result v4

    #@23
    if-eqz v4, :cond_2f

    #@25
    .line 38
    const-string v4, "com.lge.KrRejectCause.QcrilSKTRejectCause"

    #@27
    invoke-virtual {v2, v4}, Ldalvik/system/PathClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    #@2a
    move-result-object v1

    #@2b
    .line 52
    :cond_2b
    :goto_2b
    if-nez v1, :cond_65

    #@2d
    .line 53
    const/4 v4, 0x0

    #@2e
    goto :goto_6

    #@2f
    .line 40
    :cond_2f
    const-string v4, "com.lge.KrRejectCause.InfineonSKTRejectCause"

    #@31
    invoke-virtual {v2, v4}, Ldalvik/system/PathClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    #@34
    move-result-object v1

    #@35
    goto :goto_2b

    #@36
    .line 42
    :cond_36
    const-string v4, "KR"

    #@38
    const-string v5, "KT"

    #@3a
    invoke-static {v4, v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@3d
    move-result v4

    #@3e
    if-eqz v4, :cond_54

    #@40
    .line 43
    invoke-static {}, Lcom/android/internal/telephony/TelephonyUtils;->isQCRIL()Z

    #@43
    move-result v4

    #@44
    if-eqz v4, :cond_4d

    #@46
    .line 44
    const-string v4, "com.lge.KrRejectCause.QcrilKTRejectCause"

    #@48
    invoke-virtual {v2, v4}, Ldalvik/system/PathClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    #@4b
    move-result-object v1

    #@4c
    goto :goto_2b

    #@4d
    .line 46
    :cond_4d
    const-string v4, "com.lge.KrRejectCause.InfineonKTRejectCause"

    #@4f
    invoke-virtual {v2, v4}, Ldalvik/system/PathClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    #@52
    move-result-object v1

    #@53
    goto :goto_2b

    #@54
    .line 48
    :cond_54
    const-string v4, "KR"

    #@56
    const-string v5, "LGU"

    #@58
    invoke-static {v4, v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@5b
    move-result v4

    #@5c
    if-eqz v4, :cond_2b

    #@5e
    .line 49
    const-string v4, "com.lge.KrRejectCause.LGURejectCause"

    #@60
    invoke-virtual {v2, v4}, Ldalvik/system/PathClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    #@63
    move-result-object v1

    #@64
    goto :goto_2b

    #@65
    .line 55
    :cond_65
    const/4 v4, 0x1

    #@66
    new-array v4, v4, [Ljava/lang/Class;

    #@68
    const/4 v5, 0x0

    #@69
    const-class v6, Lcom/android/internal/telephony/gsm/GSMPhone;

    #@6b
    aput-object v6, v4, v5

    #@6d
    invoke-virtual {v1, v4}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    #@70
    move-result-object v3

    #@71
    .line 58
    .local v3, rejectCauseConstructor:Ljava/lang/reflect/Constructor;
    new-instance v5, Lcom/android/internal/telephony/gsm/RejectCauseProxy;

    #@73
    const/4 v4, 0x1

    #@74
    new-array v4, v4, [Ljava/lang/Object;

    #@76
    const/4 v6, 0x0

    #@77
    aput-object p0, v4, v6

    #@79
    invoke-virtual {v3, v4}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    #@7c
    move-result-object v4

    #@7d
    check-cast v4, Lcom/android/internal/telephony/gsm/RejectCause;

    #@7f
    invoke-direct {v5, v4}, Lcom/android/internal/telephony/gsm/RejectCauseProxy;-><init>(Lcom/android/internal/telephony/gsm/RejectCause;)V

    #@82
    sput-object v5, Lcom/android/internal/telephony/gsm/RejectCauseFactory;->sProxyRejectCause:Lcom/android/internal/telephony/gsm/RejectCause;
    :try_end_84
    .catch Ljava/lang/ClassNotFoundException; {:try_start_7 .. :try_end_84} :catch_87
    .catch Ljava/lang/InstantiationException; {:try_start_7 .. :try_end_84} :catch_8c
    .catch Ljava/lang/IllegalAccessException; {:try_start_7 .. :try_end_84} :catch_91
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_7 .. :try_end_84} :catch_96
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_84} :catch_9b

    #@84
    .line 72
    .end local v1           #rejectCauseClass:Ljava/lang/Class;
    .end local v2           #rejectCauseClassloader:Ldalvik/system/PathClassLoader;
    .end local v3           #rejectCauseConstructor:Ljava/lang/reflect/Constructor;
    :goto_84
    sget-object v4, Lcom/android/internal/telephony/gsm/RejectCauseFactory;->sProxyRejectCause:Lcom/android/internal/telephony/gsm/RejectCause;

    #@86
    goto :goto_6

    #@87
    .line 60
    :catch_87
    move-exception v0

    #@88
    .line 61
    .local v0, e:Ljava/lang/ClassNotFoundException;
    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    #@8b
    goto :goto_84

    #@8c
    .line 62
    .end local v0           #e:Ljava/lang/ClassNotFoundException;
    :catch_8c
    move-exception v0

    #@8d
    .line 63
    .local v0, e:Ljava/lang/InstantiationException;
    invoke-virtual {v0}, Ljava/lang/InstantiationException;->printStackTrace()V

    #@90
    goto :goto_84

    #@91
    .line 64
    .end local v0           #e:Ljava/lang/InstantiationException;
    :catch_91
    move-exception v0

    #@92
    .line 65
    .local v0, e:Ljava/lang/IllegalAccessException;
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    #@95
    goto :goto_84

    #@96
    .line 66
    .end local v0           #e:Ljava/lang/IllegalAccessException;
    :catch_96
    move-exception v0

    #@97
    .line 67
    .local v0, e:Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    #@9a
    goto :goto_84

    #@9b
    .line 68
    .end local v0           #e:Ljava/lang/reflect/InvocationTargetException;
    :catch_9b
    move-exception v0

    #@9c
    .line 69
    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    #@9f
    goto :goto_84
.end method
