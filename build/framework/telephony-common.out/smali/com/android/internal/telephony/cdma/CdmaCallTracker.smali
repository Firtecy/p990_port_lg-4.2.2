.class public final Lcom/android/internal/telephony/cdma/CdmaCallTracker;
.super Lcom/android/internal/telephony/CallTracker;
.source "CdmaCallTracker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/cdma/CdmaCallTracker$KddiCdmaInfoRec;
    }
.end annotation


# static fields
.field private static final ACTION_LGT_OTA_RES_NOTIF_FAIL:Ljava/lang/String; = "android.intent.action.LGT_OTA_RES_NOTIF_FAIL"

.field private static final DBG_POLL:Z = false

.field private static final KDDI_CLIR_COMPETITION_WITH_OTHER_SERVICE:I = 0x2

.field private static final KDDI_CLIR_PAYPHONE:I = 0x3

.field private static final KDDI_CLIR_REASON_NONE:I = 0x0

.field private static final KDDI_CLIR_REJECT_BY_USER:I = 0x1

.field private static final KDDI_CLIR_SERVICE_INVALIDITY:I = 0x4

.field static final LOG_TAG:Ljava/lang/String; = "CDMA"

.field static final MAX_CONNECTIONS:I = 0x2

.field static final MAX_CONNECTIONS_PER_CALL:I = 0x1

.field private static final REPEAT_POLLING:Z


# instance fields
.field backgroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

.field callWaitingRegistrants:Landroid/os/RegistrantList;

.field connections:[Lcom/android/internal/telephony/cdma/CdmaConnection;

.field desiredMute:Z

.field droppedDuringPoll:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/telephony/cdma/CdmaConnection;",
            ">;"
        }
    .end annotation
.end field

.field foregroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

.field hangupPendingMO:Z

.field mAssistDialPhoneNumberUtils:Landroid/telephony/AssistDialPhoneNumberUtils;

.field mIntentReceiver:Landroid/content/BroadcastReceiver;

.field private mIsEcmTimerCanceled:Z

.field mIsInEmergencyCall:Z

.field mIsReceivedAocr:Z

.field pendingCallClirMode:I

.field pendingCallInEcm:Z

.field pendingMO:Lcom/android/internal/telephony/cdma/CdmaConnection;

.field phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

.field ringingCall:Lcom/android/internal/telephony/cdma/CdmaCall;

.field state:Lcom/android/internal/telephony/PhoneConstants$State;

.field voiceCallEndedRegistrants:Landroid/os/RegistrantList;

.field voiceCallStartedRegistrants:Landroid/os/RegistrantList;


# direct methods
.method public constructor <init>(Lcom/android/internal/telephony/cdma/CDMAPhone;)V
    .registers 7
    .parameter "phone"

    #@0
    .prologue
    const/4 v2, 0x2

    #@1
    const/4 v4, 0x0

    #@2
    const/4 v3, 0x0

    #@3
    .line 152
    invoke-direct {p0}, Lcom/android/internal/telephony/CallTracker;-><init>()V

    #@6
    .line 103
    new-array v1, v2, [Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@8
    iput-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->connections:[Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@a
    .line 104
    new-instance v1, Landroid/os/RegistrantList;

    #@c
    invoke-direct {v1}, Landroid/os/RegistrantList;-><init>()V

    #@f
    iput-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->voiceCallEndedRegistrants:Landroid/os/RegistrantList;

    #@11
    .line 105
    new-instance v1, Landroid/os/RegistrantList;

    #@13
    invoke-direct {v1}, Landroid/os/RegistrantList;-><init>()V

    #@16
    iput-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->voiceCallStartedRegistrants:Landroid/os/RegistrantList;

    #@18
    .line 106
    new-instance v1, Landroid/os/RegistrantList;

    #@1a
    invoke-direct {v1}, Landroid/os/RegistrantList;-><init>()V

    #@1d
    iput-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->callWaitingRegistrants:Landroid/os/RegistrantList;

    #@1f
    .line 110
    new-instance v1, Ljava/util/ArrayList;

    #@21
    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    #@24
    iput-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->droppedDuringPoll:Ljava/util/ArrayList;

    #@26
    .line 113
    new-instance v1, Lcom/android/internal/telephony/cdma/CdmaCall;

    #@28
    invoke-direct {v1, p0}, Lcom/android/internal/telephony/cdma/CdmaCall;-><init>(Lcom/android/internal/telephony/cdma/CdmaCallTracker;)V

    #@2b
    iput-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->ringingCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@2d
    .line 115
    new-instance v1, Lcom/android/internal/telephony/cdma/CdmaCall;

    #@2f
    invoke-direct {v1, p0}, Lcom/android/internal/telephony/cdma/CdmaCall;-><init>(Lcom/android/internal/telephony/cdma/CdmaCallTracker;)V

    #@32
    iput-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->foregroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@34
    .line 116
    new-instance v1, Lcom/android/internal/telephony/cdma/CdmaCall;

    #@36
    invoke-direct {v1, p0}, Lcom/android/internal/telephony/cdma/CdmaCall;-><init>(Lcom/android/internal/telephony/cdma/CdmaCallTracker;)V

    #@39
    iput-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->backgroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@3b
    .line 120
    iput-boolean v3, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pendingCallInEcm:Z

    #@3d
    .line 121
    iput-boolean v3, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->mIsInEmergencyCall:Z

    #@3f
    .line 124
    iput-boolean v3, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->desiredMute:Z

    #@41
    .line 127
    sget-object v1, Lcom/android/internal/telephony/PhoneConstants$State;->IDLE:Lcom/android/internal/telephony/PhoneConstants$State;

    #@43
    iput-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->state:Lcom/android/internal/telephony/PhoneConstants$State;

    #@45
    .line 129
    iput-boolean v3, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->mIsEcmTimerCanceled:Z

    #@47
    .line 135
    new-instance v1, Landroid/telephony/AssistDialPhoneNumberUtils;

    #@49
    invoke-direct {v1}, Landroid/telephony/AssistDialPhoneNumberUtils;-><init>()V

    #@4c
    iput-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->mAssistDialPhoneNumberUtils:Landroid/telephony/AssistDialPhoneNumberUtils;

    #@4e
    .line 146
    iput-boolean v3, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->mIsReceivedAocr:Z

    #@50
    .line 1824
    new-instance v1, Lcom/android/internal/telephony/cdma/CdmaCallTracker$1;

    #@52
    invoke-direct {v1, p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker$1;-><init>(Lcom/android/internal/telephony/cdma/CdmaCallTracker;)V

    #@55
    iput-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@57
    .line 153
    iput-object p1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@59
    .line 154
    iget-object v1, p1, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@5b
    iput-object v1, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@5d
    .line 155
    iget-object v1, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@5f
    invoke-interface {v1, p0, v2, v4}, Lcom/android/internal/telephony/CommandsInterface;->registerForCallStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@62
    .line 156
    iget-object v1, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@64
    const/16 v2, 0x9

    #@66
    invoke-interface {v1, p0, v2, v4}, Lcom/android/internal/telephony/CommandsInterface;->registerForOn(Landroid/os/Handler;ILjava/lang/Object;)V

    #@69
    .line 157
    iget-object v1, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@6b
    const/16 v2, 0xa

    #@6d
    invoke-interface {v1, p0, v2, v4}, Lcom/android/internal/telephony/CommandsInterface;->registerForNotAvailable(Landroid/os/Handler;ILjava/lang/Object;)V

    #@70
    .line 158
    iget-object v1, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@72
    const/16 v2, 0xf

    #@74
    invoke-interface {v1, p0, v2, v4}, Lcom/android/internal/telephony/CommandsInterface;->registerForCallWaitingInfo(Landroid/os/Handler;ILjava/lang/Object;)V

    #@77
    .line 160
    const-string v1, "KDDI"

    #@79
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@7c
    move-result v1

    #@7d
    if-eqz v1, :cond_86

    #@7f
    .line 161
    iget-object v1, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@81
    const/16 v2, 0x14

    #@83
    invoke-interface {v1, p0, v2, v4}, Lcom/android/internal/telephony/CommandsInterface;->registerForCdmaInfoRecReceivedKddi(Landroid/os/Handler;ILjava/lang/Object;)V

    #@86
    .line 165
    :cond_86
    const-string v1, "KDDI"

    #@88
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@8b
    move-result v1

    #@8c
    if-eqz v1, :cond_a1

    #@8e
    .line 166
    new-instance v0, Landroid/content/IntentFilter;

    #@90
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@93
    .line 167
    .local v0, AOCRFilter:Landroid/content/IntentFilter;
    const-string v1, "com.lge.intent.action.AOCR_RECEIVED_KDDI"

    #@95
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@98
    .line 168
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@9b
    move-result-object v1

    #@9c
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@9e
    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@a1
    .line 172
    .end local v0           #AOCRFilter:Landroid/content/IntentFilter;
    :cond_a1
    const-string v1, "KDDI"

    #@a3
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@a6
    move-result v1

    #@a7
    if-eqz v1, :cond_b0

    #@a9
    .line 173
    iget-object v1, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@ab
    const/16 v2, 0x15

    #@ad
    invoke-interface {v1, p0, v2, v4}, Lcom/android/internal/telephony/CommandsInterface;->registerForT53ReleaseInfoKddi(Landroid/os/Handler;ILjava/lang/Object;)V

    #@b0
    .line 176
    :cond_b0
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->foregroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@b2
    invoke-virtual {v1, v3}, Lcom/android/internal/telephony/cdma/CdmaCall;->setGeneric(Z)V

    #@b5
    .line 177
    return-void
.end method

.method static synthetic access$000(Lcom/android/internal/telephony/cdma/CdmaCallTracker;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pollCallsWhenSafe()V

    #@3
    return-void
.end method

.method static synthetic access$100(Lcom/android/internal/telephony/cdma/CdmaCallTracker;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 85
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->updatePhoneState()V

    #@3
    return-void
.end method

.method static synthetic access$200(Lcom/android/internal/telephony/cdma/CdmaCallTracker;Lcom/android/internal/telephony/cdma/CdmaCallWaitingNotification;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 85
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->notifyCallWaitingInfo(Lcom/android/internal/telephony/cdma/CdmaCallWaitingNotification;)V

    #@3
    return-void
.end method

.method static synthetic access$300(Lcom/android/internal/telephony/cdma/CdmaCallTracker;Lcom/android/internal/telephony/cdma/CdmaCallWaitingNotification;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 85
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->handleCallWaitingInfo(Lcom/android/internal/telephony/cdma/CdmaCallWaitingNotification;)V

    #@3
    return-void
.end method

.method static synthetic access$400(Lcom/android/internal/telephony/cdma/CdmaCallTracker;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pollCallsWhenSafe()V

    #@3
    return-void
.end method

.method private checkAndEnableDataCallAfterEmergencyCallDropped()V
    .registers 4

    #@0
    .prologue
    .line 1537
    iget-boolean v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->mIsInEmergencyCall:Z

    #@2
    if-eqz v1, :cond_3c

    #@4
    .line 1538
    const/4 v1, 0x0

    #@5
    iput-boolean v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->mIsInEmergencyCall:Z

    #@7
    .line 1539
    const-string v1, "ril.cdma.inecmmode"

    #@9
    const-string v2, "false"

    #@b
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    .line 1541
    .local v0, inEcm:Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v2, "checkAndEnableDataCallAfterEmergencyCallDropped,inEcm="

    #@16
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v1

    #@22
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->log(Ljava/lang/String;)V

    #@25
    .line 1543
    const-string v1, "false"

    #@27
    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@2a
    move-result v1

    #@2b
    if-nez v1, :cond_35

    #@2d
    .line 1545
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@2f
    iget-object v1, v1, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@31
    const/4 v2, 0x1

    #@32
    invoke-virtual {v1, v2}, Lcom/android/internal/telephony/DataConnectionTracker;->setInternalDataEnabled(Z)Z

    #@35
    .line 1549
    :cond_35
    const-string v1, "ril.cdma.emergencyCall"

    #@37
    const-string v2, "false"

    #@39
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@3c
    .line 1552
    .end local v0           #inEcm:Ljava/lang/String;
    :cond_3c
    return-void
.end method

.method private checkMtFindNewRinging(Lcom/android/internal/telephony/DriverCall;I)Lcom/android/internal/telephony/Connection;
    .registers 7
    .parameter "dc"
    .parameter "i"

    #@0
    .prologue
    .line 1560
    const/4 v0, 0x0

    #@1
    .line 1562
    .local v0, newRinging:Lcom/android/internal/telephony/Connection;
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->connections:[Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@3
    new-instance v2, Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@5
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@7
    invoke-virtual {v3}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@a
    move-result-object v3

    #@b
    invoke-direct {v2, v3, p1, p0, p2}, Lcom/android/internal/telephony/cdma/CdmaConnection;-><init>(Landroid/content/Context;Lcom/android/internal/telephony/DriverCall;Lcom/android/internal/telephony/cdma/CdmaCallTracker;I)V

    #@e
    aput-object v2, v1, p2

    #@10
    .line 1564
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->connections:[Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@12
    aget-object v1, v1, p2

    #@14
    invoke-virtual {v1}, Lcom/android/internal/telephony/cdma/CdmaConnection;->getCall()Lcom/android/internal/telephony/cdma/CdmaCall;

    #@17
    move-result-object v1

    #@18
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->ringingCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@1a
    if-ne v1, v2, :cond_37

    #@1c
    .line 1565
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->connections:[Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@1e
    aget-object v0, v1, p2

    #@20
    .line 1566
    new-instance v1, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v2, "Notify new ring "

    #@27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v1

    #@2b
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v1

    #@2f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v1

    #@33
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->log(Ljava/lang/String;)V

    #@36
    .line 1584
    :cond_36
    :goto_36
    return-object v0

    #@37
    .line 1571
    :cond_37
    const-string v1, "CDMA"

    #@39
    new-instance v2, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string v3, "Phantom call appeared "

    #@40
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v2

    #@44
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v2

    #@48
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v2

    #@4c
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    .line 1575
    iget-object v1, p1, Lcom/android/internal/telephony/DriverCall;->state:Lcom/android/internal/telephony/DriverCall$State;

    #@51
    sget-object v2, Lcom/android/internal/telephony/DriverCall$State;->ALERTING:Lcom/android/internal/telephony/DriverCall$State;

    #@53
    if-eq v1, v2, :cond_36

    #@55
    iget-object v1, p1, Lcom/android/internal/telephony/DriverCall;->state:Lcom/android/internal/telephony/DriverCall$State;

    #@57
    sget-object v2, Lcom/android/internal/telephony/DriverCall$State;->DIALING:Lcom/android/internal/telephony/DriverCall$State;

    #@59
    if-eq v1, v2, :cond_36

    #@5b
    .line 1577
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->connections:[Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@5d
    aget-object v1, v1, p2

    #@5f
    invoke-virtual {v1}, Lcom/android/internal/telephony/cdma/CdmaConnection;->onConnectedInOrOut()V

    #@62
    .line 1578
    iget-object v1, p1, Lcom/android/internal/telephony/DriverCall;->state:Lcom/android/internal/telephony/DriverCall$State;

    #@64
    sget-object v2, Lcom/android/internal/telephony/DriverCall$State;->HOLDING:Lcom/android/internal/telephony/DriverCall$State;

    #@66
    if-ne v1, v2, :cond_36

    #@68
    .line 1580
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->connections:[Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@6a
    aget-object v1, v1, p2

    #@6c
    invoke-virtual {v1}, Lcom/android/internal/telephony/cdma/CdmaConnection;->onStartedHolding()V

    #@6f
    goto :goto_36
.end method

.method private dialThreeWay(Ljava/lang/String;)Lcom/android/internal/telephony/Connection;
    .registers 6
    .parameter "dialString"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 458
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->foregroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@3
    invoke-virtual {v1}, Lcom/android/internal/telephony/cdma/CdmaCall;->isIdle()Z

    #@6
    move-result v1

    #@7
    if-nez v1, :cond_66

    #@9
    .line 461
    const-string v1, "NotDisableDataCallInEmergencyCall"

    #@b
    invoke-static {v0, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@e
    move-result v0

    #@f
    if-nez v0, :cond_14

    #@11
    .line 462
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->disableDataCallInEmergencyCall(Ljava/lang/String;)V

    #@14
    .line 467
    :cond_14
    new-instance v0, Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@16
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@18
    invoke-virtual {v1}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->checkForTestEmergencyNumber(Ljava/lang/String;)Ljava/lang/String;

    #@1f
    move-result-object v2

    #@20
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->foregroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@22
    invoke-direct {v0, v1, v2, p0, v3}, Lcom/android/internal/telephony/cdma/CdmaConnection;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/android/internal/telephony/cdma/CdmaCallTracker;Lcom/android/internal/telephony/cdma/CdmaCall;)V

    #@25
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pendingMO:Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@27
    .line 471
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pendingMO:Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@29
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaConnection;->address:Ljava/lang/String;

    #@2b
    if-eqz v0, :cond_43

    #@2d
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pendingMO:Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@2f
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaConnection;->address:Ljava/lang/String;

    #@31
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@34
    move-result v0

    #@35
    if-eqz v0, :cond_43

    #@37
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pendingMO:Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@39
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaConnection;->address:Ljava/lang/String;

    #@3b
    const/16 v1, 0x4e

    #@3d
    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    #@40
    move-result v0

    #@41
    if-ltz v0, :cond_67

    #@43
    .line 474
    :cond_43
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pendingMO:Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@45
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->INVALID_NUMBER:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@47
    iput-object v1, v0, Lcom/android/internal/telephony/cdma/CdmaConnection;->cause:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@49
    .line 476
    new-instance v0, Ljava/lang/StringBuilder;

    #@4b
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@4e
    const-string v1, "dialThreeWay : Connection.DisconnectCause.INVALID_NUMBER pendingMO="

    #@50
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v0

    #@54
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pendingMO:Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@56
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v0

    #@5a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5d
    move-result-object v0

    #@5e
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->log(Ljava/lang/String;)V

    #@61
    .line 480
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pollCallsWhenSafe()V

    #@64
    .line 490
    :goto_64
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pendingMO:Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@66
    .line 492
    :cond_66
    return-object v0

    #@67
    .line 482
    :cond_67
    iget-object v0, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@69
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pendingMO:Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@6b
    iget-object v1, v1, Lcom/android/internal/telephony/cdma/CdmaConnection;->address:Ljava/lang/String;

    #@6d
    const/16 v2, 0x10

    #@6f
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->obtainMessage(I)Landroid/os/Message;

    #@72
    move-result-object v2

    #@73
    invoke-interface {v0, v1, v2}, Lcom/android/internal/telephony/CommandsInterface;->sendCDMAFeatureCode(Ljava/lang/String;Landroid/os/Message;)V

    #@76
    goto :goto_64
.end method

.method private disableDataCallInEmergencyCall(Ljava/lang/String;)V
    .registers 4
    .parameter "dialString"

    #@0
    .prologue
    .line 1506
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@5
    move-result-object v0

    #@6
    invoke-static {p1, v0}, Landroid/telephony/PhoneNumberUtils;->isLocalEmergencyNumber(Ljava/lang/String;Landroid/content/Context;)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_3f

    #@c
    .line 1507
    const-string v0, "disableDataCallInEmergencyCall"

    #@e
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->log(Ljava/lang/String;)V

    #@11
    .line 1508
    const/4 v0, 0x1

    #@12
    iput-boolean v0, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->mIsInEmergencyCall:Z

    #@14
    .line 1509
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@16
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@18
    const/4 v1, 0x0

    #@19
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->setInternalDataEnabled(Z)Z

    #@1c
    .line 1513
    const-string v0, "VZW"

    #@1e
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@21
    move-result v0

    #@22
    if-eqz v0, :cond_37

    #@24
    .line 1514
    const-string v0, "CDMA"

    #@26
    const-string v1, "Emergency Sleep 500ms start"

    #@28
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 1516
    const-wide/16 v0, 0x1f4

    #@2d
    :try_start_2d
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_30
    .catch Ljava/lang/InterruptedException; {:try_start_2d .. :try_end_30} :catch_47

    #@30
    .line 1519
    :goto_30
    const-string v0, "CDMA"

    #@32
    const-string v1, "Emergency Sleep 500ms end"

    #@34
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    .line 1525
    :cond_37
    const-string v0, "ril.cdma.emergencyCall"

    #@39
    const-string v1, "true"

    #@3b
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@3e
    .line 1530
    :goto_3e
    return-void

    #@3f
    .line 1527
    :cond_3f
    const-string v0, "ril.cdma.emergencyCall"

    #@41
    const-string v1, "false"

    #@43
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@46
    goto :goto_3e

    #@47
    .line 1517
    :catch_47
    move-exception v0

    #@48
    goto :goto_30
.end method

.method private fakeHoldForegroundBeforeDial()V
    .registers 6

    #@0
    .prologue
    .line 271
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->foregroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@2
    iget-object v4, v4, Lcom/android/internal/telephony/cdma/CdmaCall;->connections:Ljava/util/ArrayList;

    #@4
    invoke-virtual {v4}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    #@7
    move-result-object v1

    #@8
    check-cast v1, Ljava/util/List;

    #@a
    .line 273
    .local v1, connCopy:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/telephony/Connection;>;"
    const/4 v2, 0x0

    #@b
    .local v2, i:I
    invoke-interface {v1}, Ljava/util/List;->size()I

    #@e
    move-result v3

    #@f
    .local v3, s:I
    :goto_f
    if-ge v2, v3, :cond_1d

    #@11
    .line 274
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@14
    move-result-object v0

    #@15
    check-cast v0, Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@17
    .line 276
    .local v0, conn:Lcom/android/internal/telephony/cdma/CdmaConnection;
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CdmaConnection;->fakeHoldBeforeDial()V

    #@1a
    .line 273
    add-int/lit8 v2, v2, 0x1

    #@1c
    goto :goto_f

    #@1d
    .line 278
    .end local v0           #conn:Lcom/android/internal/telephony/cdma/CdmaConnection;
    :cond_1d
    return-void
.end method

.method private flashAndSetGenericTrue()V
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1269
    iget-object v0, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    const-string v1, ""

    #@4
    const/16 v2, 0x8

    #@6
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->obtainMessage(I)Landroid/os/Message;

    #@9
    move-result-object v2

    #@a
    invoke-interface {v0, v1, v2}, Lcom/android/internal/telephony/CommandsInterface;->sendCDMAFeatureCode(Ljava/lang/String;Landroid/os/Message;)V

    #@d
    .line 1274
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->foregroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@f
    const/4 v1, 0x1

    #@10
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaCall;->setGeneric(Z)V

    #@13
    .line 1275
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@15
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->notifyPreciseCallStateChanged()V

    #@18
    .line 1276
    return-void
.end method

.method private getFailedService(I)Lcom/android/internal/telephony/Phone$SuppService;
    .registers 3
    .parameter "what"

    #@0
    .prologue
    .line 1279
    packed-switch p1, :pswitch_data_12

    #@3
    .line 1289
    :pswitch_3
    sget-object v0, Lcom/android/internal/telephony/Phone$SuppService;->UNKNOWN:Lcom/android/internal/telephony/Phone$SuppService;

    #@5
    :goto_5
    return-object v0

    #@6
    .line 1281
    :pswitch_6
    sget-object v0, Lcom/android/internal/telephony/Phone$SuppService;->SWITCH:Lcom/android/internal/telephony/Phone$SuppService;

    #@8
    goto :goto_5

    #@9
    .line 1283
    :pswitch_9
    sget-object v0, Lcom/android/internal/telephony/Phone$SuppService;->CONFERENCE:Lcom/android/internal/telephony/Phone$SuppService;

    #@b
    goto :goto_5

    #@c
    .line 1285
    :pswitch_c
    sget-object v0, Lcom/android/internal/telephony/Phone$SuppService;->SEPARATE:Lcom/android/internal/telephony/Phone$SuppService;

    #@e
    goto :goto_5

    #@f
    .line 1287
    :pswitch_f
    sget-object v0, Lcom/android/internal/telephony/Phone$SuppService;->TRANSFER:Lcom/android/internal/telephony/Phone$SuppService;

    #@11
    goto :goto_5

    #@12
    .line 1279
    :pswitch_data_12
    .packed-switch 0x8
        :pswitch_6
        :pswitch_3
        :pswitch_3
        :pswitch_9
        :pswitch_c
        :pswitch_f
    .end packed-switch
.end method

.method private handleCallWaitingInfo(Lcom/android/internal/telephony/cdma/CdmaCallWaitingNotification;)V
    .registers 7
    .parameter "cw"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 1312
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->foregroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@3
    iget-object v1, v1, Lcom/android/internal/telephony/cdma/CdmaCall;->connections:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@8
    move-result v1

    #@9
    if-le v1, v4, :cond_10

    #@b
    .line 1313
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->foregroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@d
    invoke-virtual {v1, v4}, Lcom/android/internal/telephony/cdma/CdmaCall;->setGeneric(Z)V

    #@10
    .line 1317
    :cond_10
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->ringingCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@12
    const/4 v2, 0x0

    #@13
    invoke-virtual {v1, v2}, Lcom/android/internal/telephony/cdma/CdmaCall;->setGeneric(Z)V

    #@16
    .line 1318
    new-instance v1, Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@18
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@1a
    invoke-virtual {v2}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@1d
    move-result-object v2

    #@1e
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->ringingCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@20
    invoke-direct {v1, v2, p1, p0, v3}, Lcom/android/internal/telephony/cdma/CdmaConnection;-><init>(Landroid/content/Context;Lcom/android/internal/telephony/cdma/CdmaCallWaitingNotification;Lcom/android/internal/telephony/cdma/CdmaCallTracker;Lcom/android/internal/telephony/cdma/CdmaCall;)V

    #@23
    .line 1320
    const-string v1, "KDDI"

    #@25
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@28
    move-result v1

    #@29
    if-eqz v1, :cond_4c

    #@2b
    .line 1321
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@2d
    invoke-virtual {v1}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getServiceState()Landroid/telephony/ServiceState;

    #@30
    move-result-object v1

    #@31
    invoke-virtual {v1}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@34
    move-result v1

    #@35
    if-eqz v1, :cond_4b

    #@37
    .line 1323
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->ringingCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@39
    invoke-virtual {v1}, Lcom/android/internal/telephony/cdma/CdmaCall;->getLatestConnection()Lcom/android/internal/telephony/Connection;

    #@3c
    move-result-object v0

    #@3d
    check-cast v0, Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@3f
    .line 1325
    .local v0, conn:Lcom/android/internal/telephony/cdma/CdmaConnection;
    if-eqz v0, :cond_4b

    #@41
    .line 1326
    iput-boolean v4, v0, Lcom/android/internal/telephony/cdma/CdmaConnection;->isCdmaInfoRecReceivedKddi:Z

    #@43
    .line 1327
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->updatePhoneState()V

    #@46
    .line 1330
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->notifyCallWaitingInfo(Lcom/android/internal/telephony/cdma/CdmaCallWaitingNotification;)V

    #@49
    .line 1331
    iput-boolean v4, v0, Lcom/android/internal/telephony/cdma/CdmaConnection;->isNotifyRingingConnectionKddi:Z

    #@4b
    .line 1343
    .end local v0           #conn:Lcom/android/internal/telephony/cdma/CdmaConnection;
    :cond_4b
    :goto_4b
    return-void

    #@4c
    .line 1336
    :cond_4c
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->updatePhoneState()V

    #@4f
    .line 1339
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->notifyCallWaitingInfo(Lcom/android/internal/telephony/cdma/CdmaCallWaitingNotification;)V

    #@52
    goto :goto_4b
.end method

.method private handleCdmaReleaseInfoKddi()V
    .registers 4

    #@0
    .prologue
    .line 1841
    const/4 v0, 0x0

    #@1
    .line 1842
    .local v0, call:Lcom/android/internal/telephony/cdma/CdmaCall;
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->ringingCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@3
    invoke-virtual {v1}, Lcom/android/internal/telephony/cdma/CdmaCall;->isIdle()Z

    #@6
    move-result v1

    #@7
    if-nez v1, :cond_2b

    #@9
    .line 1843
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->ringingCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@b
    invoke-virtual {v1}, Lcom/android/internal/telephony/cdma/CdmaCall;->getState()Lcom/android/internal/telephony/Call$State;

    #@e
    move-result-object v1

    #@f
    sget-object v2, Lcom/android/internal/telephony/Call$State;->INCOMING:Lcom/android/internal/telephony/Call$State;

    #@11
    if-ne v1, v2, :cond_1e

    #@13
    .line 1844
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->ringingCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@15
    .line 1855
    :cond_15
    :goto_15
    if-eqz v0, :cond_1d

    #@17
    .line 1856
    :try_start_17
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->hangup(Lcom/android/internal/telephony/cdma/CdmaCall;)V

    #@1a
    .line 1859
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CdmaCall;->onHangupRemoteKddi()V
    :try_end_1d
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_17 .. :try_end_1d} :catch_41

    #@1d
    .line 1863
    :cond_1d
    :goto_1d
    return-void

    #@1e
    .line 1845
    :cond_1e
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->ringingCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@20
    invoke-virtual {v1}, Lcom/android/internal/telephony/cdma/CdmaCall;->getState()Lcom/android/internal/telephony/Call$State;

    #@23
    move-result-object v1

    #@24
    sget-object v2, Lcom/android/internal/telephony/Call$State;->WAITING:Lcom/android/internal/telephony/Call$State;

    #@26
    if-ne v1, v2, :cond_15

    #@28
    .line 1846
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->foregroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@2a
    goto :goto_15

    #@2b
    .line 1848
    :cond_2b
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->foregroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@2d
    invoke-virtual {v1}, Lcom/android/internal/telephony/cdma/CdmaCall;->isIdle()Z

    #@30
    move-result v1

    #@31
    if-nez v1, :cond_36

    #@33
    .line 1849
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->foregroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@35
    goto :goto_15

    #@36
    .line 1850
    :cond_36
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->backgroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@38
    invoke-virtual {v1}, Lcom/android/internal/telephony/cdma/CdmaCall;->isIdle()Z

    #@3b
    move-result v1

    #@3c
    if-nez v1, :cond_15

    #@3e
    .line 1851
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->backgroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@40
    goto :goto_15

    #@41
    .line 1861
    :catch_41
    move-exception v1

    #@42
    goto :goto_1d
.end method

.method private handleEcmTimer(I)V
    .registers 5
    .parameter "action"

    #@0
    .prologue
    .line 1493
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/cdma/CDMAPhone;->handleTimerInEmergencyCallbackMode(I)V

    #@5
    .line 1494
    packed-switch p1, :pswitch_data_2a

    #@8
    .line 1498
    const-string v0, "CDMA"

    #@a
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v2, "handleEcmTimer, unsupported action "

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 1500
    :goto_20
    return-void

    #@21
    .line 1495
    :pswitch_21
    const/4 v0, 0x1

    #@22
    iput-boolean v0, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->mIsEcmTimerCanceled:Z

    #@24
    goto :goto_20

    #@25
    .line 1496
    :pswitch_25
    const/4 v0, 0x0

    #@26
    iput-boolean v0, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->mIsEcmTimerCanceled:Z

    #@28
    goto :goto_20

    #@29
    .line 1494
    nop

    #@2a
    :pswitch_data_2a
    .packed-switch 0x0
        :pswitch_25
        :pswitch_21
    .end packed-switch
.end method

.method private handleRadioNotAvailable()V
    .registers 1

    #@0
    .prologue
    .line 1296
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pollCallsWhenSafe()V

    #@3
    .line 1297
    return-void
.end method

.method private internalClearDisconnected()V
    .registers 2

    #@0
    .prologue
    .line 618
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->ringingCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CdmaCall;->clearDisconnected()V

    #@5
    .line 619
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->foregroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@7
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CdmaCall;->clearDisconnected()V

    #@a
    .line 620
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->backgroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@c
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CdmaCall;->clearDisconnected()V

    #@f
    .line 621
    return-void
.end method

.method private notifyCallWaitingInfo(Lcom/android/internal/telephony/cdma/CdmaCallWaitingNotification;)V
    .registers 5
    .parameter "obj"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1300
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->callWaitingRegistrants:Landroid/os/RegistrantList;

    #@3
    if-eqz v0, :cond_f

    #@5
    .line 1301
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->callWaitingRegistrants:Landroid/os/RegistrantList;

    #@7
    new-instance v1, Landroid/os/AsyncResult;

    #@9
    invoke-direct {v1, v2, p1, v2}, Landroid/os/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@c
    invoke-virtual {v0, v1}, Landroid/os/RegistrantList;->notifyRegistrants(Landroid/os/AsyncResult;)V

    #@f
    .line 1303
    :cond_f
    return-void
.end method

.method private obtainCompleteMessage()Landroid/os/Message;
    .registers 2

    #@0
    .prologue
    .line 629
    const/4 v0, 0x4

    #@1
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->obtainCompleteMessage(I)Landroid/os/Message;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method private obtainCompleteMessage(I)Landroid/os/Message;
    .registers 3
    .parameter "what"

    #@0
    .prologue
    .line 638
    iget v0, p0, Lcom/android/internal/telephony/CallTracker;->pendingOperations:I

    #@2
    add-int/lit8 v0, v0, 0x1

    #@4
    iput v0, p0, Lcom/android/internal/telephony/CallTracker;->pendingOperations:I

    #@6
    .line 639
    const/4 v0, 0x0

    #@7
    iput-object v0, p0, Lcom/android/internal/telephony/CallTracker;->lastRelevantPoll:Landroid/os/Message;

    #@9
    .line 640
    const/4 v0, 0x1

    #@a
    iput-boolean v0, p0, Lcom/android/internal/telephony/CallTracker;->needsPoll:Z

    #@c
    .line 645
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->obtainMessage(I)Landroid/os/Message;

    #@f
    move-result-object v0

    #@10
    return-object v0
.end method

.method private operationComplete()V
    .registers 3

    #@0
    .prologue
    .line 650
    iget v0, p0, Lcom/android/internal/telephony/CallTracker;->pendingOperations:I

    #@2
    add-int/lit8 v0, v0, -0x1

    #@4
    iput v0, p0, Lcom/android/internal/telephony/CallTracker;->pendingOperations:I

    #@6
    .line 655
    iget v0, p0, Lcom/android/internal/telephony/CallTracker;->pendingOperations:I

    #@8
    if-nez v0, :cond_1d

    #@a
    iget-boolean v0, p0, Lcom/android/internal/telephony/CallTracker;->needsPoll:Z

    #@c
    if-eqz v0, :cond_1d

    #@e
    .line 656
    const/4 v0, 0x1

    #@f
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->obtainMessage(I)Landroid/os/Message;

    #@12
    move-result-object v0

    #@13
    iput-object v0, p0, Lcom/android/internal/telephony/CallTracker;->lastRelevantPoll:Landroid/os/Message;

    #@15
    .line 657
    iget-object v0, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@17
    iget-object v1, p0, Lcom/android/internal/telephony/CallTracker;->lastRelevantPoll:Landroid/os/Message;

    #@19
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/CommandsInterface;->getCurrentCalls(Landroid/os/Message;)V

    #@1c
    .line 663
    :cond_1c
    :goto_1c
    return-void

    #@1d
    .line 658
    :cond_1d
    iget v0, p0, Lcom/android/internal/telephony/CallTracker;->pendingOperations:I

    #@1f
    if-gez v0, :cond_1c

    #@21
    .line 660
    const-string v0, "CDMA"

    #@23
    const-string v1, "CdmaCallTracker.pendingOperations < 0"

    #@25
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 661
    const/4 v0, 0x0

    #@29
    iput v0, p0, Lcom/android/internal/telephony/CallTracker;->pendingOperations:I

    #@2b
    goto :goto_1c
.end method

.method private processDialString(Ljava/lang/String;)Ljava/lang/String;
    .registers 11
    .parameter "dialString"

    #@0
    .prologue
    .line 281
    const-string v6, "CDMA"

    #@2
    const-string v7, "processDialString()..."

    #@4
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 283
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@a
    move-result v6

    #@b
    if-eqz v6, :cond_16

    #@d
    .line 284
    const-string v6, "CDMA"

    #@f
    const-string v7, " Dialing Number is Null cannot dialing "

    #@11
    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@14
    move-object v2, p1

    #@15
    .line 314
    .end local p1
    .local v2, dialString:Ljava/lang/String;
    :goto_15
    return-object v2

    #@16
    .line 288
    .end local v2           #dialString:Ljava/lang/String;
    .restart local p1
    :cond_16
    const/4 v6, 0x0

    #@17
    const-string v7, "support_assisted_dialing"

    #@19
    invoke-static {v6, v7}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@1c
    move-result v6

    #@1d
    if-eqz v6, :cond_7a

    #@1f
    .line 290
    :try_start_1f
    const-string v6, "CDMA"

    #@21
    new-instance v7, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    const-string v8, "dialString matches with + :  "

    #@28
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v7

    #@2c
    const-string v8, "+"

    #@2e
    invoke-virtual {p1, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@31
    move-result v8

    #@32
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@35
    move-result-object v7

    #@36
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v7

    #@3a
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3d
    .line 291
    move-object v0, p1

    #@3e
    .line 292
    .local v0, assistedDialingBeforeNumber:Ljava/lang/String;
    const-string v6, "CDMA"

    #@40
    new-instance v7, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    const-string v8, "CdmaCallTracker Dial() *** before Assisted Dial Number: "

    #@47
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v7

    #@4b
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v7

    #@4f
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v7

    #@53
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@56
    .line 294
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->mAssistDialPhoneNumberUtils:Landroid/telephony/AssistDialPhoneNumberUtils;

    #@58
    iget-object v7, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@5a
    invoke-virtual {v7}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@5d
    move-result-object v7

    #@5e
    invoke-virtual {v6, p1, v7}, Landroid/telephony/AssistDialPhoneNumberUtils;->getAssitedDialFinalNumberForCDMA(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    #@61
    move-result-object p1

    #@62
    .line 295
    const-string v6, "CDMA"

    #@64
    new-instance v7, Ljava/lang/StringBuilder;

    #@66
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@69
    const-string v8, "CdmaCallTracker Dial()  *** after Assisted Dial Number: "

    #@6b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v7

    #@6f
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v7

    #@73
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@76
    move-result-object v7

    #@77
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7a
    .catch Ljava/util/regex/PatternSyntaxException; {:try_start_1f .. :try_end_7a} :catch_9d

    #@7a
    .line 301
    .end local v0           #assistedDialingBeforeNumber:Ljava/lang/String;
    :cond_7a
    :goto_7a
    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->isVoiceMailNumber(Ljava/lang/String;)Z

    #@7d
    move-result v5

    #@7e
    .line 302
    .local v5, isVoiceMailCall:Z
    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->is611DialNumber(Ljava/lang/String;)Z

    #@81
    move-result v4

    #@82
    .line 303
    .local v4, is611number:Z
    const/4 v1, 0x0

    #@83
    .line 305
    .local v1, dialAddress:Ljava/lang/String;
    const-string v6, "VZW"

    #@85
    invoke-static {v6}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@88
    move-result v6

    #@89
    if-eqz v6, :cond_9a

    #@8b
    .line 306
    if-eqz v5, :cond_a6

    #@8d
    .line 307
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->mAssistDialPhoneNumberUtils:Landroid/telephony/AssistDialPhoneNumberUtils;

    #@8f
    iget-object v7, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@91
    invoke-virtual {v7}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@94
    move-result-object v7

    #@95
    invoke-virtual {v6, p1, v7}, Landroid/telephony/AssistDialPhoneNumberUtils;->convertToCdmaVoiceMailNumber(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    #@98
    move-result-object v1

    #@99
    .line 308
    move-object p1, v1

    #@9a
    :cond_9a
    :goto_9a
    move-object v2, p1

    #@9b
    .line 314
    .end local p1
    .restart local v2       #dialString:Ljava/lang/String;
    goto/16 :goto_15

    #@9d
    .line 296
    .end local v1           #dialAddress:Ljava/lang/String;
    .end local v2           #dialString:Ljava/lang/String;
    .end local v4           #is611number:Z
    .end local v5           #isVoiceMailCall:Z
    .restart local p1
    :catch_9d
    move-exception v3

    #@9e
    .line 297
    .local v3, e:Ljava/util/regex/PatternSyntaxException;
    const-string v6, "CDMA"

    #@a0
    const-string v7, "PatternSyntaxException"

    #@a2
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a5
    goto :goto_7a

    #@a6
    .line 309
    .end local v3           #e:Ljava/util/regex/PatternSyntaxException;
    .restart local v1       #dialAddress:Ljava/lang/String;
    .restart local v4       #is611number:Z
    .restart local v5       #isVoiceMailCall:Z
    :cond_a6
    if-eqz v4, :cond_9a

    #@a8
    .line 310
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->mAssistDialPhoneNumberUtils:Landroid/telephony/AssistDialPhoneNumberUtils;

    #@aa
    iget-object v7, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@ac
    invoke-virtual {v7}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@af
    move-result-object v7

    #@b0
    invoke-virtual {v6, p1, v7}, Landroid/telephony/AssistDialPhoneNumberUtils;->convertToCdma611Number(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    #@b3
    move-result-object v1

    #@b4
    .line 311
    move-object p1, v1

    #@b5
    goto :goto_9a
.end method

.method private updatePhoneState()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v3, 0x0

    #@2
    .line 669
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->state:Lcom/android/internal/telephony/PhoneConstants$State;

    #@4
    .line 671
    .local v0, oldState:Lcom/android/internal/telephony/PhoneConstants$State;
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->ringingCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@6
    invoke-virtual {v1}, Lcom/android/internal/telephony/cdma/CdmaCall;->isRinging()Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_6d

    #@c
    .line 672
    sget-object v1, Lcom/android/internal/telephony/PhoneConstants$State;->RINGING:Lcom/android/internal/telephony/PhoneConstants$State;

    #@e
    iput-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->state:Lcom/android/internal/telephony/PhoneConstants$State;

    #@10
    .line 680
    :goto_10
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->state:Lcom/android/internal/telephony/PhoneConstants$State;

    #@12
    sget-object v2, Lcom/android/internal/telephony/PhoneConstants$State;->IDLE:Lcom/android/internal/telephony/PhoneConstants$State;

    #@14
    if-ne v1, v2, :cond_8b

    #@16
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->state:Lcom/android/internal/telephony/PhoneConstants$State;

    #@18
    if-eq v0, v1, :cond_8b

    #@1a
    .line 682
    iput-boolean v4, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->mIsReceivedAocr:Z

    #@1c
    .line 684
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->voiceCallEndedRegistrants:Landroid/os/RegistrantList;

    #@1e
    new-instance v2, Landroid/os/AsyncResult;

    #@20
    invoke-direct {v2, v3, v3, v3}, Landroid/os/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@23
    invoke-virtual {v1, v2}, Landroid/os/RegistrantList;->notifyRegistrants(Landroid/os/AsyncResult;)V

    #@26
    .line 694
    :cond_26
    :goto_26
    new-instance v1, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    const-string v2, "update phone state, old="

    #@2d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v1

    #@31
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v1

    #@35
    const-string v2, " new="

    #@37
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v1

    #@3b
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->state:Lcom/android/internal/telephony/PhoneConstants$State;

    #@3d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v1

    #@41
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v1

    #@45
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->log(Ljava/lang/String;)V

    #@48
    .line 696
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->state:Lcom/android/internal/telephony/PhoneConstants$State;

    #@4a
    if-eq v1, v0, :cond_6c

    #@4c
    .line 697
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@4e
    invoke-virtual {v1}, Lcom/android/internal/telephony/cdma/CDMAPhone;->notifyPhoneStateChanged()V

    #@51
    .line 700
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@53
    invoke-virtual {v1}, Lcom/android/internal/telephony/cdma/CDMAPhone;->isInEcm()Z

    #@56
    move-result v1

    #@57
    if-eqz v1, :cond_6c

    #@59
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->state:Lcom/android/internal/telephony/PhoneConstants$State;

    #@5b
    sget-object v2, Lcom/android/internal/telephony/PhoneConstants$State;->IDLE:Lcom/android/internal/telephony/PhoneConstants$State;

    #@5d
    if-ne v1, v2, :cond_6c

    #@5f
    .line 701
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@61
    invoke-virtual {v1}, Lcom/android/internal/telephony/cdma/CDMAPhone;->isInEcmExitDelay()Z

    #@64
    move-result v1

    #@65
    if-eqz v1, :cond_6c

    #@67
    .line 702
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@69
    invoke-virtual {v1}, Lcom/android/internal/telephony/cdma/CDMAPhone;->exitEmergencyCallbackMode()V

    #@6c
    .line 707
    :cond_6c
    return-void

    #@6d
    .line 673
    :cond_6d
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pendingMO:Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@6f
    if-nez v1, :cond_81

    #@71
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->foregroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@73
    invoke-virtual {v1}, Lcom/android/internal/telephony/cdma/CdmaCall;->isIdle()Z

    #@76
    move-result v1

    #@77
    if-eqz v1, :cond_81

    #@79
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->backgroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@7b
    invoke-virtual {v1}, Lcom/android/internal/telephony/cdma/CdmaCall;->isIdle()Z

    #@7e
    move-result v1

    #@7f
    if-nez v1, :cond_86

    #@81
    .line 675
    :cond_81
    sget-object v1, Lcom/android/internal/telephony/PhoneConstants$State;->OFFHOOK:Lcom/android/internal/telephony/PhoneConstants$State;

    #@83
    iput-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->state:Lcom/android/internal/telephony/PhoneConstants$State;

    #@85
    goto :goto_10

    #@86
    .line 677
    :cond_86
    sget-object v1, Lcom/android/internal/telephony/PhoneConstants$State;->IDLE:Lcom/android/internal/telephony/PhoneConstants$State;

    #@88
    iput-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->state:Lcom/android/internal/telephony/PhoneConstants$State;

    #@8a
    goto :goto_10

    #@8b
    .line 686
    :cond_8b
    sget-object v1, Lcom/android/internal/telephony/PhoneConstants$State;->IDLE:Lcom/android/internal/telephony/PhoneConstants$State;

    #@8d
    if-ne v0, v1, :cond_26

    #@8f
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->state:Lcom/android/internal/telephony/PhoneConstants$State;

    #@91
    if-eq v0, v1, :cond_26

    #@93
    .line 688
    iput-boolean v4, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->mIsReceivedAocr:Z

    #@95
    .line 690
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->voiceCallStartedRegistrants:Landroid/os/RegistrantList;

    #@97
    new-instance v2, Landroid/os/AsyncResult;

    #@99
    invoke-direct {v2, v3, v3, v3}, Landroid/os/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@9c
    invoke-virtual {v1, v2}, Landroid/os/RegistrantList;->notifyRegistrants(Landroid/os/AsyncResult;)V

    #@9f
    goto :goto_26
.end method


# virtual methods
.method acceptCall()V
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 497
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->ringingCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@2
    invoke-virtual {v1}, Lcom/android/internal/telephony/cdma/CdmaCall;->getState()Lcom/android/internal/telephony/Call$State;

    #@5
    move-result-object v1

    #@6
    sget-object v2, Lcom/android/internal/telephony/Call$State;->INCOMING:Lcom/android/internal/telephony/Call$State;

    #@8
    if-ne v1, v2, :cond_1f

    #@a
    .line 498
    const-string v1, "phone"

    #@c
    const-string v2, "acceptCall: incoming..."

    #@e
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 500
    const/4 v1, 0x0

    #@12
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->setMute(Z)V

    #@15
    .line 501
    iget-object v1, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@17
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->obtainCompleteMessage()Landroid/os/Message;

    #@1a
    move-result-object v2

    #@1b
    invoke-interface {v1, v2}, Lcom/android/internal/telephony/CommandsInterface;->acceptCall(Landroid/os/Message;)V

    #@1e
    .line 516
    :goto_1e
    return-void

    #@1f
    .line 502
    :cond_1f
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->ringingCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@21
    invoke-virtual {v1}, Lcom/android/internal/telephony/cdma/CdmaCall;->getState()Lcom/android/internal/telephony/Call$State;

    #@24
    move-result-object v1

    #@25
    sget-object v2, Lcom/android/internal/telephony/Call$State;->WAITING:Lcom/android/internal/telephony/Call$State;

    #@27
    if-ne v1, v2, :cond_45

    #@29
    .line 503
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->ringingCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@2b
    invoke-virtual {v1}, Lcom/android/internal/telephony/cdma/CdmaCall;->getLatestConnection()Lcom/android/internal/telephony/Connection;

    #@2e
    move-result-object v1

    #@2f
    check-cast v1, Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@31
    move-object v0, v1

    #@32
    check-cast v0, Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@34
    .line 509
    .local v0, cwConn:Lcom/android/internal/telephony/cdma/CdmaConnection;
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->ringingCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@36
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->foregroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@38
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/cdma/CdmaConnection;->updateParent(Lcom/android/internal/telephony/cdma/CdmaCall;Lcom/android/internal/telephony/cdma/CdmaCall;)V

    #@3b
    .line 510
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CdmaConnection;->onConnectedInOrOut()V

    #@3e
    .line 511
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->updatePhoneState()V

    #@41
    .line 512
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->switchWaitingOrHoldingAndActive()V

    #@44
    goto :goto_1e

    #@45
    .line 514
    .end local v0           #cwConn:Lcom/android/internal/telephony/cdma/CdmaConnection;
    :cond_45
    new-instance v1, Lcom/android/internal/telephony/CallStateException;

    #@47
    const-string v2, "phone not ringing"

    #@49
    invoke-direct {v1, v2}, Lcom/android/internal/telephony/CallStateException;-><init>(Ljava/lang/String;)V

    #@4c
    throw v1
.end method

.method canConference()Z
    .registers 3

    #@0
    .prologue
    .line 566
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->foregroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CdmaCall;->getState()Lcom/android/internal/telephony/Call$State;

    #@5
    move-result-object v0

    #@6
    sget-object v1, Lcom/android/internal/telephony/Call$State;->ACTIVE:Lcom/android/internal/telephony/Call$State;

    #@8
    if-ne v0, v1, :cond_26

    #@a
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->backgroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@c
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CdmaCall;->getState()Lcom/android/internal/telephony/Call$State;

    #@f
    move-result-object v0

    #@10
    sget-object v1, Lcom/android/internal/telephony/Call$State;->HOLDING:Lcom/android/internal/telephony/Call$State;

    #@12
    if-ne v0, v1, :cond_26

    #@14
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->backgroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@16
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CdmaCall;->isFull()Z

    #@19
    move-result v0

    #@1a
    if-nez v0, :cond_26

    #@1c
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->foregroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@1e
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CdmaCall;->isFull()Z

    #@21
    move-result v0

    #@22
    if-nez v0, :cond_26

    #@24
    const/4 v0, 0x1

    #@25
    :goto_25
    return v0

    #@26
    :cond_26
    const/4 v0, 0x0

    #@27
    goto :goto_25
.end method

.method canDial()Z
    .registers 11

    #@0
    .prologue
    const/4 v9, 0x3

    #@1
    const/4 v4, 0x1

    #@2
    const/4 v5, 0x0

    #@3
    .line 575
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@5
    invoke-virtual {v3}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getServiceState()Landroid/telephony/ServiceState;

    #@8
    move-result-object v3

    #@9
    invoke-virtual {v3}, Landroid/telephony/ServiceState;->getState()I

    #@c
    move-result v2

    #@d
    .line 576
    .local v2, serviceState:I
    const-string v3, "ro.telephony.disable-call"

    #@f
    const-string v6, "false"

    #@11
    invoke-static {v3, v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    .line 579
    .local v0, disableCall:Ljava/lang/String;
    if-eq v2, v9, :cond_d1

    #@17
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pendingMO:Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@19
    if-nez v3, :cond_d1

    #@1b
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->ringingCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@1d
    invoke-virtual {v3}, Lcom/android/internal/telephony/cdma/CdmaCall;->isRinging()Z

    #@20
    move-result v3

    #@21
    if-nez v3, :cond_d1

    #@23
    const-string v3, "true"

    #@25
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@28
    move-result v3

    #@29
    if-nez v3, :cond_d1

    #@2b
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->foregroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@2d
    invoke-virtual {v3}, Lcom/android/internal/telephony/cdma/CdmaCall;->getState()Lcom/android/internal/telephony/Call$State;

    #@30
    move-result-object v3

    #@31
    invoke-virtual {v3}, Lcom/android/internal/telephony/Call$State;->isAlive()Z

    #@34
    move-result v3

    #@35
    if-eqz v3, :cond_4d

    #@37
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->foregroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@39
    invoke-virtual {v3}, Lcom/android/internal/telephony/cdma/CdmaCall;->getState()Lcom/android/internal/telephony/Call$State;

    #@3c
    move-result-object v3

    #@3d
    sget-object v6, Lcom/android/internal/telephony/Call$State;->ACTIVE:Lcom/android/internal/telephony/Call$State;

    #@3f
    if-eq v3, v6, :cond_4d

    #@41
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->backgroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@43
    invoke-virtual {v3}, Lcom/android/internal/telephony/cdma/CdmaCall;->getState()Lcom/android/internal/telephony/Call$State;

    #@46
    move-result-object v3

    #@47
    invoke-virtual {v3}, Lcom/android/internal/telephony/Call$State;->isAlive()Z

    #@4a
    move-result v3

    #@4b
    if-nez v3, :cond_d1

    #@4d
    :cond_4d
    move v1, v4

    #@4e
    .line 587
    .local v1, ret:Z
    :goto_4e
    if-nez v1, :cond_d0

    #@50
    .line 588
    const-string v6, "canDial is false\n((serviceState=%d) != ServiceState.STATE_POWER_OFF)::=%s\n&& pendingMO == null::=%s\n&& !ringingCall.isRinging()::=%s\n&& !disableCall.equals(\"true\")::=%s\n&& (!foregroundCall.getState().isAlive()::=%s\n   || foregroundCall.getState() == CdmaCall.State.ACTIVE::=%s\n   ||!backgroundCall.getState().isAlive())::=%s)"

    #@52
    const/16 v3, 0x8

    #@54
    new-array v7, v3, [Ljava/lang/Object;

    #@56
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@59
    move-result-object v3

    #@5a
    aput-object v3, v7, v5

    #@5c
    if-eq v2, v9, :cond_d4

    #@5e
    move v3, v4

    #@5f
    :goto_5f
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@62
    move-result-object v3

    #@63
    aput-object v3, v7, v4

    #@65
    const/4 v8, 0x2

    #@66
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pendingMO:Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@68
    if-nez v3, :cond_d6

    #@6a
    move v3, v4

    #@6b
    :goto_6b
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@6e
    move-result-object v3

    #@6f
    aput-object v3, v7, v8

    #@71
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->ringingCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@73
    invoke-virtual {v3}, Lcom/android/internal/telephony/cdma/CdmaCall;->isRinging()Z

    #@76
    move-result v3

    #@77
    if-nez v3, :cond_d8

    #@79
    move v3, v4

    #@7a
    :goto_7a
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@7d
    move-result-object v3

    #@7e
    aput-object v3, v7, v9

    #@80
    const/4 v8, 0x4

    #@81
    const-string v3, "true"

    #@83
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@86
    move-result v3

    #@87
    if-nez v3, :cond_da

    #@89
    move v3, v4

    #@8a
    :goto_8a
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@8d
    move-result-object v3

    #@8e
    aput-object v3, v7, v8

    #@90
    const/4 v8, 0x5

    #@91
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->foregroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@93
    invoke-virtual {v3}, Lcom/android/internal/telephony/cdma/CdmaCall;->getState()Lcom/android/internal/telephony/Call$State;

    #@96
    move-result-object v3

    #@97
    invoke-virtual {v3}, Lcom/android/internal/telephony/Call$State;->isAlive()Z

    #@9a
    move-result v3

    #@9b
    if-nez v3, :cond_dc

    #@9d
    move v3, v4

    #@9e
    :goto_9e
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@a1
    move-result-object v3

    #@a2
    aput-object v3, v7, v8

    #@a4
    const/4 v8, 0x6

    #@a5
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->foregroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@a7
    invoke-virtual {v3}, Lcom/android/internal/telephony/cdma/CdmaCall;->getState()Lcom/android/internal/telephony/Call$State;

    #@aa
    move-result-object v3

    #@ab
    sget-object v9, Lcom/android/internal/telephony/Call$State;->ACTIVE:Lcom/android/internal/telephony/Call$State;

    #@ad
    if-ne v3, v9, :cond_de

    #@af
    move v3, v4

    #@b0
    :goto_b0
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@b3
    move-result-object v3

    #@b4
    aput-object v3, v7, v8

    #@b6
    const/4 v3, 0x7

    #@b7
    iget-object v8, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->backgroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@b9
    invoke-virtual {v8}, Lcom/android/internal/telephony/cdma/CdmaCall;->getState()Lcom/android/internal/telephony/Call$State;

    #@bc
    move-result-object v8

    #@bd
    invoke-virtual {v8}, Lcom/android/internal/telephony/Call$State;->isAlive()Z

    #@c0
    move-result v8

    #@c1
    if-nez v8, :cond_e0

    #@c3
    :goto_c3
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@c6
    move-result-object v4

    #@c7
    aput-object v4, v7, v3

    #@c9
    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@cc
    move-result-object v3

    #@cd
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->log(Ljava/lang/String;)V

    #@d0
    .line 605
    :cond_d0
    return v1

    #@d1
    .end local v1           #ret:Z
    :cond_d1
    move v1, v5

    #@d2
    .line 579
    goto/16 :goto_4e

    #@d4
    .restart local v1       #ret:Z
    :cond_d4
    move v3, v5

    #@d5
    .line 588
    goto :goto_5f

    #@d6
    :cond_d6
    move v3, v5

    #@d7
    goto :goto_6b

    #@d8
    :cond_d8
    move v3, v5

    #@d9
    goto :goto_7a

    #@da
    :cond_da
    move v3, v5

    #@db
    goto :goto_8a

    #@dc
    :cond_dc
    move v3, v5

    #@dd
    goto :goto_9e

    #@de
    :cond_de
    move v3, v5

    #@df
    goto :goto_b0

    #@e0
    :cond_e0
    move v4, v5

    #@e1
    goto :goto_c3
.end method

.method canTransfer()Z
    .registers 3

    #@0
    .prologue
    .line 610
    const-string v0, "CDMA"

    #@2
    const-string v1, "canTransfer: not possible in CDMA"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 611
    const/4 v0, 0x0

    #@8
    return v0
.end method

.method clearDisconnected()V
    .registers 2

    #@0
    .prologue
    .line 558
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->internalClearDisconnected()V

    #@3
    .line 560
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->updatePhoneState()V

    #@6
    .line 561
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@8
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->notifyPreciseCallStateChanged()V

    #@b
    .line 562
    return-void
.end method

.method conference()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 548
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->flashAndSetGenericTrue()V

    #@3
    .line 549
    return-void
.end method

.method dial(Ljava/lang/String;)Lcom/android/internal/telephony/Connection;
    .registers 3
    .parameter "dialString"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 453
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->dial(Ljava/lang/String;I)Lcom/android/internal/telephony/Connection;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method dial(Ljava/lang/String;I)Lcom/android/internal/telephony/Connection;
    .registers 16
    .parameter "dialString"
    .parameter "clirMode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v7, 0x1

    #@2
    const/4 v8, 0x0

    #@3
    .line 325
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->clearDisconnected()V

    #@6
    .line 327
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->canDial()Z

    #@9
    move-result v9

    #@a
    if-nez v9, :cond_14

    #@c
    .line 328
    new-instance v6, Lcom/android/internal/telephony/CallStateException;

    #@e
    const-string v7, "cannot dial in current state"

    #@10
    invoke-direct {v6, v7}, Lcom/android/internal/telephony/CallStateException;-><init>(Ljava/lang/String;)V

    #@13
    throw v6

    #@14
    .line 331
    :cond_14
    const-string v9, "support_assisted_dialing"

    #@16
    invoke-static {v6, v9}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@19
    move-result v9

    #@1a
    if-eqz v9, :cond_20

    #@1c
    .line 333
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->processDialString(Ljava/lang/String;)Ljava/lang/String;

    #@1f
    move-result-object p1

    #@20
    .line 339
    :cond_20
    const-string v9, "support_smart_dialing"

    #@22
    invoke-static {v6, v9}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@25
    move-result v9

    #@26
    if-eqz v9, :cond_81

    #@28
    .line 340
    if-eqz p1, :cond_81

    #@2a
    .line 342
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@2d
    move-result v9

    #@2e
    const/16 v10, 0xa

    #@30
    if-le v9, v10, :cond_3a

    #@32
    invoke-virtual {p1, v8}, Ljava/lang/String;->charAt(I)C

    #@35
    move-result v9

    #@36
    const/16 v10, 0x2b

    #@38
    if-eq v9, v10, :cond_40

    #@3a
    :cond_3a
    invoke-static {p1}, Landroid/telephony/AssistDialPhoneNumberUtils;->isNanpSimplified(Ljava/lang/String;)Z

    #@3d
    move-result v9

    #@3e
    if-eqz v9, :cond_81

    #@40
    .line 347
    :cond_40
    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->isVoiceMailNumber(Ljava/lang/String;)Z

    #@43
    move-result v9

    #@44
    if-eqz v9, :cond_f1

    #@46
    .line 348
    iget-object v9, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->mAssistDialPhoneNumberUtils:Landroid/telephony/AssistDialPhoneNumberUtils;

    #@48
    iget-object v10, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@4a
    invoke-virtual {v10}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@4d
    move-result-object v10

    #@4e
    invoke-virtual {v9, v10}, Landroid/telephony/AssistDialPhoneNumberUtils;->getAssistedDialCurrentCountry(Landroid/content/Context;)Landroid/provider/ReferenceCountry;

    #@51
    move-result-object v1

    #@52
    .line 349
    .local v1, currentCountry:Landroid/provider/ReferenceCountry;
    if-eqz v1, :cond_81

    #@54
    .line 350
    invoke-virtual {v1}, Landroid/provider/ReferenceCountry;->getNanp()Ljava/lang/String;

    #@57
    move-result-object v9

    #@58
    const-string v10, "0"

    #@5a
    invoke-static {v9, v10}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@5d
    move-result v9

    #@5e
    if-eqz v9, :cond_81

    #@60
    .line 351
    invoke-virtual {v1}, Landroid/provider/ReferenceCountry;->getIddPrefix()Ljava/lang/String;

    #@63
    move-result-object v2

    #@64
    .line 352
    .local v2, current_Idd:Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    #@66
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@69
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v9

    #@6d
    const-string v10, "1"

    #@6f
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v9

    #@73
    iget-object v10, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@75
    invoke-virtual {v10}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getLine1Number()Ljava/lang/String;

    #@78
    move-result-object v10

    #@79
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v9

    #@7d
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@80
    move-result-object p1

    #@81
    .line 363
    .end local v1           #currentCountry:Landroid/provider/ReferenceCountry;
    .end local v2           #current_Idd:Ljava/lang/String;
    :cond_81
    :goto_81
    const-string v9, "ril.cdma.inecmmode"

    #@83
    const-string v10, "false"

    #@85
    invoke-static {v9, v10}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@88
    move-result-object v3

    #@89
    .line 364
    .local v3, inEcm:Ljava/lang/String;
    const-string v9, "true"

    #@8b
    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8e
    move-result v5

    #@8f
    .line 365
    .local v5, isPhoneInEcmMode:Z
    iget-object v9, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@91
    invoke-virtual {v9}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@94
    move-result-object v9

    #@95
    invoke-static {p1, v9}, Landroid/telephony/PhoneNumberUtils;->isLocalEmergencyNumber(Ljava/lang/String;Landroid/content/Context;)Z

    #@98
    move-result v4

    #@99
    .line 369
    .local v4, isEmergencyCall:Z
    const-string v9, "support_network_change_auto_retry"

    #@9b
    invoke-static {v6, v9}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@9e
    move-result v9

    #@9f
    if-eqz v9, :cond_c6

    #@a1
    .line 370
    if-ne v4, v7, :cond_c6

    #@a3
    .line 371
    const-string v9, "dial() Emergency Number True!!"

    #@a5
    invoke-virtual {p0, v9}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->log(Ljava/lang/String;)V

    #@a8
    .line 372
    iget-object v9, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@aa
    invoke-virtual {v9}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@ad
    move-result-object v9

    #@ae
    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@b1
    move-result-object v9

    #@b2
    const-string v10, "network_change_auto_retry"

    #@b4
    invoke-static {v9, v10, v7}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@b7
    .line 375
    iget-object v9, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@b9
    invoke-virtual {v9}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@bc
    move-result-object v9

    #@bd
    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@c0
    move-result-object v9

    #@c1
    const-string v10, "network_change_auto_retry_number"

    #@c3
    invoke-static {v9, v10, p1}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    #@c6
    .line 382
    :cond_c6
    if-eqz v5, :cond_cf

    #@c8
    if-eqz v4, :cond_cf

    #@ca
    .line 383
    iget-object v9, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@cc
    invoke-direct {p0, v7}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->handleEcmTimer(I)V

    #@cf
    .line 386
    :cond_cf
    const-string v9, "VZW"

    #@d1
    invoke-static {v9}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@d4
    move-result v9

    #@d5
    if-eqz v9, :cond_100

    #@d7
    .line 387
    if-eqz v5, :cond_100

    #@d9
    if-nez v4, :cond_100

    #@db
    .line 388
    iget-object v9, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@dd
    invoke-virtual {v9}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@e0
    move-result-object v9

    #@e1
    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@e4
    move-result-object v9

    #@e5
    const-string v10, "apn2_disable"

    #@e7
    invoke-static {v9, v10, v8}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@ea
    move-result v9

    #@eb
    if-ne v9, v7, :cond_fe

    #@ed
    move v0, v7

    #@ee
    .line 392
    .local v0, apn2Disabled:Z
    :goto_ee
    if-eqz v0, :cond_100

    #@f0
    .line 447
    .end local v0           #apn2Disabled:Z
    :goto_f0
    return-object v6

    #@f1
    .line 356
    .end local v3           #inEcm:Ljava/lang/String;
    .end local v4           #isEmergencyCall:Z
    .end local v5           #isPhoneInEcmMode:Z
    :cond_f1
    iget-object v9, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->mAssistDialPhoneNumberUtils:Landroid/telephony/AssistDialPhoneNumberUtils;

    #@f3
    iget-object v10, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@f5
    invoke-virtual {v10}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@f8
    move-result-object v10

    #@f9
    invoke-virtual {v9, p1, v10}, Landroid/telephony/AssistDialPhoneNumberUtils;->getAssitedDialFinalNumberForCDMA(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    #@fc
    move-result-object p1

    #@fd
    goto :goto_81

    #@fe
    .restart local v3       #inEcm:Ljava/lang/String;
    .restart local v4       #isEmergencyCall:Z
    .restart local v5       #isPhoneInEcmMode:Z
    :cond_fe
    move v0, v8

    #@ff
    .line 388
    goto :goto_ee

    #@100
    .line 401
    :cond_100
    iget-object v9, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->foregroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@102
    invoke-virtual {v9, v8}, Lcom/android/internal/telephony/cdma/CdmaCall;->setGeneric(Z)V

    #@105
    .line 406
    iget-object v9, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->foregroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@107
    invoke-virtual {v9}, Lcom/android/internal/telephony/cdma/CdmaCall;->getState()Lcom/android/internal/telephony/Call$State;

    #@10a
    move-result-object v9

    #@10b
    sget-object v10, Lcom/android/internal/telephony/Call$State;->ACTIVE:Lcom/android/internal/telephony/Call$State;

    #@10d
    if-ne v9, v10, :cond_114

    #@10f
    .line 407
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->dialThreeWay(Ljava/lang/String;)Lcom/android/internal/telephony/Connection;

    #@112
    move-result-object v6

    #@113
    goto :goto_f0

    #@114
    .line 410
    :cond_114
    new-instance v9, Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@116
    iget-object v10, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@118
    invoke-virtual {v10}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@11b
    move-result-object v10

    #@11c
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->checkForTestEmergencyNumber(Ljava/lang/String;)Ljava/lang/String;

    #@11f
    move-result-object v11

    #@120
    iget-object v12, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->foregroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@122
    invoke-direct {v9, v10, v11, p0, v12}, Lcom/android/internal/telephony/cdma/CdmaConnection;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/android/internal/telephony/cdma/CdmaCallTracker;Lcom/android/internal/telephony/cdma/CdmaCall;)V

    #@125
    iput-object v9, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pendingMO:Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@127
    .line 412
    iput-boolean v8, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->hangupPendingMO:Z

    #@129
    .line 414
    iget-object v9, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pendingMO:Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@12b
    iget-object v9, v9, Lcom/android/internal/telephony/cdma/CdmaConnection;->address:Ljava/lang/String;

    #@12d
    if-eqz v9, :cond_145

    #@12f
    iget-object v9, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pendingMO:Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@131
    iget-object v9, v9, Lcom/android/internal/telephony/cdma/CdmaConnection;->address:Ljava/lang/String;

    #@133
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    #@136
    move-result v9

    #@137
    if-eqz v9, :cond_145

    #@139
    iget-object v9, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pendingMO:Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@13b
    iget-object v9, v9, Lcom/android/internal/telephony/cdma/CdmaConnection;->address:Ljava/lang/String;

    #@13d
    const/16 v10, 0x4e

    #@13f
    invoke-virtual {v9, v10}, Ljava/lang/String;->indexOf(I)I

    #@142
    move-result v9

    #@143
    if-ltz v9, :cond_159

    #@145
    .line 417
    :cond_145
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pendingMO:Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@147
    sget-object v7, Lcom/android/internal/telephony/Connection$DisconnectCause;->INVALID_NUMBER:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@149
    iput-object v7, v6, Lcom/android/internal/telephony/cdma/CdmaConnection;->cause:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@14b
    .line 421
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pollCallsWhenSafe()V

    #@14e
    .line 444
    :goto_14e
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->updatePhoneState()V

    #@151
    .line 445
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@153
    invoke-virtual {v6}, Lcom/android/internal/telephony/cdma/CDMAPhone;->notifyPreciseCallStateChanged()V

    #@156
    .line 447
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pendingMO:Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@158
    goto :goto_f0

    #@159
    .line 424
    :cond_159
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->setMute(Z)V

    #@15c
    .line 428
    const-string v8, "NotDisableDataCallInEmergencyCall"

    #@15e
    invoke-static {v6, v8}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@161
    move-result v8

    #@162
    if-nez v8, :cond_167

    #@164
    .line 429
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->disableDataCallInEmergencyCall(Ljava/lang/String;)V

    #@167
    .line 434
    :cond_167
    if-eqz v5, :cond_16d

    #@169
    if-eqz v5, :cond_17b

    #@16b
    if-eqz v4, :cond_17b

    #@16d
    .line 435
    :cond_16d
    iget-object v6, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@16f
    iget-object v7, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pendingMO:Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@171
    iget-object v7, v7, Lcom/android/internal/telephony/cdma/CdmaConnection;->address:Ljava/lang/String;

    #@173
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->obtainCompleteMessage()Landroid/os/Message;

    #@176
    move-result-object v8

    #@177
    invoke-interface {v6, v7, p2, v8}, Lcom/android/internal/telephony/CommandsInterface;->dial(Ljava/lang/String;ILandroid/os/Message;)V

    #@17a
    goto :goto_14e

    #@17b
    .line 437
    :cond_17b
    iget-object v8, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@17d
    invoke-virtual {v8}, Lcom/android/internal/telephony/cdma/CDMAPhone;->exitEmergencyCallbackMode()V

    #@180
    .line 438
    iget-object v8, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@182
    const/16 v9, 0xe

    #@184
    invoke-virtual {v8, p0, v9, v6}, Lcom/android/internal/telephony/cdma/CDMAPhone;->setOnEcbModeExitResponse(Landroid/os/Handler;ILjava/lang/Object;)V

    #@187
    .line 439
    iput p2, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pendingCallClirMode:I

    #@189
    .line 440
    iput-boolean v7, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pendingCallInEcm:Z

    #@18b
    goto :goto_14e
.end method

.method public dispose()V
    .registers 8

    #@0
    .prologue
    .line 180
    iget-object v5, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v5, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForCallStateChanged(Landroid/os/Handler;)V

    #@5
    .line 181
    iget-object v5, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@7
    invoke-interface {v5, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForOn(Landroid/os/Handler;)V

    #@a
    .line 182
    iget-object v5, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@c
    invoke-interface {v5, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForNotAvailable(Landroid/os/Handler;)V

    #@f
    .line 183
    iget-object v5, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@11
    invoke-interface {v5, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForCallWaitingInfo(Landroid/os/Handler;)V

    #@14
    .line 185
    const-string v5, "KDDI"

    #@16
    invoke-static {v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@19
    move-result v5

    #@1a
    if-eqz v5, :cond_21

    #@1c
    .line 186
    iget-object v5, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@1e
    invoke-interface {v5, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForCdmaInfoRecReceivedKddi(Landroid/os/Handler;)V

    #@21
    .line 190
    :cond_21
    const-string v5, "KDDI"

    #@23
    invoke-static {v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@26
    move-result v5

    #@27
    if-eqz v5, :cond_38

    #@29
    .line 191
    iget-object v5, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@2b
    if-eqz v5, :cond_38

    #@2d
    iget-object v5, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@2f
    invoke-virtual {v5}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@32
    move-result-object v5

    #@33
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@35
    invoke-virtual {v5, v6}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@38
    .line 195
    :cond_38
    const-string v5, "KDDI"

    #@3a
    invoke-static {v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@3d
    move-result v5

    #@3e
    if-eqz v5, :cond_45

    #@40
    .line 196
    iget-object v5, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@42
    invoke-interface {v5, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForT53ReleaseInfoKddi(Landroid/os/Handler;)V

    #@45
    .line 199
    :cond_45
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->connections:[Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@47
    .local v0, arr$:[Lcom/android/internal/telephony/cdma/CdmaConnection;
    array-length v4, v0

    #@48
    .local v4, len$:I
    const/4 v3, 0x0

    #@49
    .local v3, i$:I
    :goto_49
    if-ge v3, v4, :cond_6a

    #@4b
    aget-object v1, v0, v3

    #@4d
    .line 201
    .local v1, c:Lcom/android/internal/telephony/cdma/CdmaConnection;
    if-eqz v1, :cond_5e

    #@4f
    .line 202
    :try_start_4f
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->hangup(Lcom/android/internal/telephony/cdma/CdmaConnection;)V

    #@52
    .line 205
    const-string v5, "CDMA"

    #@54
    const-string v6, "Posting connection disconnect due to LOST_SIGNAL"

    #@56
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@59
    .line 206
    sget-object v5, Lcom/android/internal/telephony/Connection$DisconnectCause;->LOST_SIGNAL:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@5b
    invoke-virtual {v1, v5}, Lcom/android/internal/telephony/cdma/CdmaConnection;->onDisconnect(Lcom/android/internal/telephony/Connection$DisconnectCause;)V
    :try_end_5e
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_4f .. :try_end_5e} :catch_61

    #@5e
    .line 199
    :cond_5e
    :goto_5e
    add-int/lit8 v3, v3, 0x1

    #@60
    goto :goto_49

    #@61
    .line 208
    :catch_61
    move-exception v2

    #@62
    .line 209
    .local v2, ex:Lcom/android/internal/telephony/CallStateException;
    const-string v5, "CDMA"

    #@64
    const-string v6, "unexpected error on hangup during dispose"

    #@66
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@69
    goto :goto_5e

    #@6a
    .line 214
    .end local v1           #c:Lcom/android/internal/telephony/cdma/CdmaConnection;
    .end local v2           #ex:Lcom/android/internal/telephony/CallStateException;
    :cond_6a
    :try_start_6a
    iget-object v5, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pendingMO:Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@6c
    if-eqz v5, :cond_81

    #@6e
    .line 215
    iget-object v5, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pendingMO:Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@70
    invoke-virtual {p0, v5}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->hangup(Lcom/android/internal/telephony/cdma/CdmaConnection;)V

    #@73
    .line 216
    const-string v5, "CDMA"

    #@75
    const-string v6, "Posting disconnect to pendingMO due to LOST_SIGNAL"

    #@77
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7a
    .line 217
    iget-object v5, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pendingMO:Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@7c
    sget-object v6, Lcom/android/internal/telephony/Connection$DisconnectCause;->LOST_SIGNAL:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@7e
    invoke-virtual {v5, v6}, Lcom/android/internal/telephony/cdma/CdmaConnection;->onDisconnect(Lcom/android/internal/telephony/Connection$DisconnectCause;)V
    :try_end_81
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_6a .. :try_end_81} :catch_85

    #@81
    .line 223
    :cond_81
    :goto_81
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->clearDisconnected()V

    #@84
    .line 225
    return-void

    #@85
    .line 219
    :catch_85
    move-exception v2

    #@86
    .line 220
    .restart local v2       #ex:Lcom/android/internal/telephony/CallStateException;
    const-string v5, "CDMA"

    #@88
    const-string v6, "unexpected error on hangup during dispose"

    #@8a
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@8d
    goto :goto_81
.end method

.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 11
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    const/4 v6, 0x2

    #@1
    const/4 v5, 0x1

    #@2
    const/4 v4, 0x0

    #@3
    .line 1872
    const-string v1, "GsmCallTracker extends:"

    #@5
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@8
    .line 1873
    invoke-super {p0, p1, p2, p3}, Lcom/android/internal/telephony/CallTracker;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    #@b
    .line 1874
    new-instance v1, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v2, "droppedDuringPoll: length="

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->connections:[Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@18
    array-length v2, v2

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@24
    .line 1875
    const/4 v0, 0x0

    #@25
    .local v0, i:I
    :goto_25
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->connections:[Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@27
    array-length v1, v1

    #@28
    if-ge v0, v1, :cond_40

    #@2a
    .line 1876
    const-string v1, " connections[%d]=%s\n"

    #@2c
    new-array v2, v6, [Ljava/lang/Object;

    #@2e
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@31
    move-result-object v3

    #@32
    aput-object v3, v2, v4

    #@34
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->connections:[Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@36
    aget-object v3, v3, v0

    #@38
    aput-object v3, v2, v5

    #@3a
    invoke-virtual {p2, v1, v2}, Ljava/io/PrintWriter;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    #@3d
    .line 1875
    add-int/lit8 v0, v0, 0x1

    #@3f
    goto :goto_25

    #@40
    .line 1878
    :cond_40
    new-instance v1, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    const-string v2, " voiceCallEndedRegistrants="

    #@47
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v1

    #@4b
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->voiceCallEndedRegistrants:Landroid/os/RegistrantList;

    #@4d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v1

    #@51
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v1

    #@55
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@58
    .line 1879
    new-instance v1, Ljava/lang/StringBuilder;

    #@5a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5d
    const-string v2, " voiceCallStartedRegistrants="

    #@5f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v1

    #@63
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->voiceCallStartedRegistrants:Landroid/os/RegistrantList;

    #@65
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v1

    #@69
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6c
    move-result-object v1

    #@6d
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@70
    .line 1880
    new-instance v1, Ljava/lang/StringBuilder;

    #@72
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@75
    const-string v2, " callWaitingRegistrants="

    #@77
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v1

    #@7b
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->callWaitingRegistrants:Landroid/os/RegistrantList;

    #@7d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v1

    #@81
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@84
    move-result-object v1

    #@85
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@88
    .line 1881
    new-instance v1, Ljava/lang/StringBuilder;

    #@8a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@8d
    const-string v2, "droppedDuringPoll: size="

    #@8f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@92
    move-result-object v1

    #@93
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->droppedDuringPoll:Ljava/util/ArrayList;

    #@95
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@98
    move-result v2

    #@99
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v1

    #@9d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a0
    move-result-object v1

    #@a1
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@a4
    .line 1882
    const/4 v0, 0x0

    #@a5
    :goto_a5
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->droppedDuringPoll:Ljava/util/ArrayList;

    #@a7
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@aa
    move-result v1

    #@ab
    if-ge v0, v1, :cond_c5

    #@ad
    .line 1883
    const-string v1, " droppedDuringPoll[%d]=%s\n"

    #@af
    new-array v2, v6, [Ljava/lang/Object;

    #@b1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@b4
    move-result-object v3

    #@b5
    aput-object v3, v2, v4

    #@b7
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->droppedDuringPoll:Ljava/util/ArrayList;

    #@b9
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@bc
    move-result-object v3

    #@bd
    aput-object v3, v2, v5

    #@bf
    invoke-virtual {p2, v1, v2}, Ljava/io/PrintWriter;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    #@c2
    .line 1882
    add-int/lit8 v0, v0, 0x1

    #@c4
    goto :goto_a5

    #@c5
    .line 1885
    :cond_c5
    new-instance v1, Ljava/lang/StringBuilder;

    #@c7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@ca
    const-string v2, " ringingCall="

    #@cc
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cf
    move-result-object v1

    #@d0
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->ringingCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@d2
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v1

    #@d6
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d9
    move-result-object v1

    #@da
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@dd
    .line 1886
    new-instance v1, Ljava/lang/StringBuilder;

    #@df
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@e2
    const-string v2, " foregroundCall="

    #@e4
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e7
    move-result-object v1

    #@e8
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->foregroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@ea
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@ed
    move-result-object v1

    #@ee
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f1
    move-result-object v1

    #@f2
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@f5
    .line 1887
    new-instance v1, Ljava/lang/StringBuilder;

    #@f7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@fa
    const-string v2, " backgroundCall="

    #@fc
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ff
    move-result-object v1

    #@100
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->backgroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@102
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@105
    move-result-object v1

    #@106
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@109
    move-result-object v1

    #@10a
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@10d
    .line 1888
    new-instance v1, Ljava/lang/StringBuilder;

    #@10f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@112
    const-string v2, " pendingMO="

    #@114
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@117
    move-result-object v1

    #@118
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pendingMO:Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@11a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@11d
    move-result-object v1

    #@11e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@121
    move-result-object v1

    #@122
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@125
    .line 1889
    new-instance v1, Ljava/lang/StringBuilder;

    #@127
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@12a
    const-string v2, " hangupPendingMO="

    #@12c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12f
    move-result-object v1

    #@130
    iget-boolean v2, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->hangupPendingMO:Z

    #@132
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@135
    move-result-object v1

    #@136
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@139
    move-result-object v1

    #@13a
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@13d
    .line 1890
    new-instance v1, Ljava/lang/StringBuilder;

    #@13f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@142
    const-string v2, " pendingCallInEcm="

    #@144
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@147
    move-result-object v1

    #@148
    iget-boolean v2, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pendingCallInEcm:Z

    #@14a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@14d
    move-result-object v1

    #@14e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@151
    move-result-object v1

    #@152
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@155
    .line 1891
    new-instance v1, Ljava/lang/StringBuilder;

    #@157
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@15a
    const-string v2, " mIsInEmergencyCall="

    #@15c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15f
    move-result-object v1

    #@160
    iget-boolean v2, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->mIsInEmergencyCall:Z

    #@162
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@165
    move-result-object v1

    #@166
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@169
    move-result-object v1

    #@16a
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@16d
    .line 1892
    new-instance v1, Ljava/lang/StringBuilder;

    #@16f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@172
    const-string v2, " phone="

    #@174
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@177
    move-result-object v1

    #@178
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@17a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@17d
    move-result-object v1

    #@17e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@181
    move-result-object v1

    #@182
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@185
    .line 1893
    new-instance v1, Ljava/lang/StringBuilder;

    #@187
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@18a
    const-string v2, " desiredMute="

    #@18c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18f
    move-result-object v1

    #@190
    iget-boolean v2, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->desiredMute:Z

    #@192
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@195
    move-result-object v1

    #@196
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@199
    move-result-object v1

    #@19a
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@19d
    .line 1894
    new-instance v1, Ljava/lang/StringBuilder;

    #@19f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1a2
    const-string v2, " pendingCallClirMode="

    #@1a4
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a7
    move-result-object v1

    #@1a8
    iget v2, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pendingCallClirMode:I

    #@1aa
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1ad
    move-result-object v1

    #@1ae
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b1
    move-result-object v1

    #@1b2
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1b5
    .line 1895
    new-instance v1, Ljava/lang/StringBuilder;

    #@1b7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1ba
    const-string v2, " state="

    #@1bc
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1bf
    move-result-object v1

    #@1c0
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->state:Lcom/android/internal/telephony/PhoneConstants$State;

    #@1c2
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c5
    move-result-object v1

    #@1c6
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c9
    move-result-object v1

    #@1ca
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1cd
    .line 1896
    new-instance v1, Ljava/lang/StringBuilder;

    #@1cf
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1d2
    const-string v2, " mIsEcmTimerCanceled="

    #@1d4
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d7
    move-result-object v1

    #@1d8
    iget-boolean v2, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->mIsEcmTimerCanceled:Z

    #@1da
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1dd
    move-result-object v1

    #@1de
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e1
    move-result-object v1

    #@1e2
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1e5
    .line 1897
    return-void
.end method

.method explicitCallTransfer()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 553
    iget-object v0, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    const/16 v1, 0xd

    #@4
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->obtainCompleteMessage(I)Landroid/os/Message;

    #@7
    move-result-object v1

    #@8
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/CommandsInterface;->explicitCallTransfer(Landroid/os/Message;)V

    #@b
    .line 554
    return-void
.end method

.method protected finalize()V
    .registers 3

    #@0
    .prologue
    .line 229
    const-string v0, "CDMA"

    #@2
    const-string v1, "CdmaCallTracker finalized"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 230
    return-void
.end method

.method getConnectionByIndex(Lcom/android/internal/telephony/cdma/CdmaCall;I)Lcom/android/internal/telephony/cdma/CdmaConnection;
    .registers 7
    .parameter "call"
    .parameter "index"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1257
    iget-object v3, p1, Lcom/android/internal/telephony/cdma/CdmaCall;->connections:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v1

    #@6
    .line 1258
    .local v1, count:I
    const/4 v2, 0x0

    #@7
    .local v2, i:I
    :goto_7
    if-ge v2, v1, :cond_1b

    #@9
    .line 1259
    iget-object v3, p1, Lcom/android/internal/telephony/cdma/CdmaCall;->connections:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v0

    #@f
    check-cast v0, Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@11
    .line 1260
    .local v0, cn:Lcom/android/internal/telephony/cdma/CdmaConnection;
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CdmaConnection;->getCDMAIndex()I

    #@14
    move-result v3

    #@15
    if-ne v3, p2, :cond_18

    #@17
    .line 1265
    .end local v0           #cn:Lcom/android/internal/telephony/cdma/CdmaConnection;
    :goto_17
    return-object v0

    #@18
    .line 1258
    .restart local v0       #cn:Lcom/android/internal/telephony/cdma/CdmaConnection;
    :cond_18
    add-int/lit8 v2, v2, 0x1

    #@1a
    goto :goto_7

    #@1b
    .line 1265
    .end local v0           #cn:Lcom/android/internal/telephony/cdma/CdmaConnection;
    :cond_1b
    const/4 v0, 0x0

    #@1c
    goto :goto_17
.end method

.method getMute()Z
    .registers 2

    #@0
    .prologue
    .line 1095
    iget-boolean v0, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->desiredMute:Z

    #@2
    return v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 15
    .parameter "msg"

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    const/4 v12, 0x0

    #@2
    .line 1350
    iget-object v8, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@4
    iget-boolean v8, v8, Lcom/android/internal/telephony/PhoneBase;->mIsTheCurrentActivePhone:Z

    #@6
    if-nez v8, :cond_10

    #@8
    .line 1351
    const-string v8, "CDMA"

    #@a
    const-string v9, "Ignoring events received on inactive CdmaPhone"

    #@c
    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 1487
    :cond_f
    :goto_f
    :pswitch_f
    return-void

    #@10
    .line 1354
    :cond_10
    iget v8, p1, Landroid/os/Message;->what:I

    #@12
    packed-switch v8, :pswitch_data_118

    #@15
    .line 1484
    :pswitch_15
    new-instance v8, Ljava/lang/RuntimeException;

    #@17
    const-string v9, "unexpected event not handled"

    #@19
    invoke-direct {v8, v9}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@1c
    throw v8

    #@1d
    .line 1356
    :pswitch_1d
    const-string v8, "CDMA"

    #@1f
    const-string v9, "Event EVENT_POLL_CALLS_RESULT Received"

    #@21
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 1357
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@26
    check-cast v0, Landroid/os/AsyncResult;

    #@28
    .line 1359
    .local v0, ar:Landroid/os/AsyncResult;
    iget-object v8, p0, Lcom/android/internal/telephony/CallTracker;->lastRelevantPoll:Landroid/os/Message;

    #@2a
    if-ne p1, v8, :cond_f

    #@2c
    .line 1362
    iput-boolean v12, p0, Lcom/android/internal/telephony/CallTracker;->needsPoll:Z

    #@2e
    .line 1363
    iput-object v10, p0, Lcom/android/internal/telephony/CallTracker;->lastRelevantPoll:Landroid/os/Message;

    #@30
    .line 1364
    iget-object v8, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@32
    check-cast v8, Landroid/os/AsyncResult;

    #@34
    invoke-virtual {p0, v8}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->handlePollCalls(Landroid/os/AsyncResult;)V

    #@37
    goto :goto_f

    #@38
    .line 1370
    .end local v0           #ar:Landroid/os/AsyncResult;
    :pswitch_38
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->operationComplete()V

    #@3b
    goto :goto_f

    #@3c
    .line 1381
    :pswitch_3c
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@3e
    check-cast v0, Landroid/os/AsyncResult;

    #@40
    .line 1383
    .restart local v0       #ar:Landroid/os/AsyncResult;
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->operationComplete()V

    #@43
    .line 1385
    iget-object v8, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@45
    if-eqz v8, :cond_67

    #@47
    .line 1388
    const/16 v1, 0x10

    #@49
    .line 1389
    .local v1, causeCode:I
    const-string v8, "CDMA"

    #@4b
    const-string v9, "Exception during getLastCallFailCause, assuming normal disconnect"

    #@4d
    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@50
    .line 1395
    :goto_50
    const/4 v5, 0x0

    #@51
    .local v5, i:I
    iget-object v8, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->droppedDuringPoll:Ljava/util/ArrayList;

    #@53
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    #@56
    move-result v7

    #@57
    .line 1396
    .local v7, s:I
    :goto_57
    if-ge v5, v7, :cond_70

    #@59
    .line 1398
    iget-object v8, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->droppedDuringPoll:Ljava/util/ArrayList;

    #@5b
    invoke-virtual {v8, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@5e
    move-result-object v2

    #@5f
    check-cast v2, Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@61
    .line 1400
    .local v2, conn:Lcom/android/internal/telephony/cdma/CdmaConnection;
    invoke-virtual {v2, v1}, Lcom/android/internal/telephony/cdma/CdmaConnection;->onRemoteDisconnect(I)V

    #@64
    .line 1396
    add-int/lit8 v5, v5, 0x1

    #@66
    goto :goto_57

    #@67
    .line 1392
    .end local v1           #causeCode:I
    .end local v2           #conn:Lcom/android/internal/telephony/cdma/CdmaConnection;
    .end local v5           #i:I
    .end local v7           #s:I
    :cond_67
    iget-object v8, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@69
    check-cast v8, [I

    #@6b
    check-cast v8, [I

    #@6d
    aget v1, v8, v12

    #@6f
    .restart local v1       #causeCode:I
    goto :goto_50

    #@70
    .line 1404
    .restart local v5       #i:I
    .restart local v7       #s:I
    :cond_70
    const/16 v8, 0x10

    #@72
    if-eq v8, v1, :cond_8b

    #@74
    .line 1405
    new-instance v4, Landroid/content/Intent;

    #@76
    const-string v8, "android.intent.action.LGT_OTA_RES_NOTIF_FAIL"

    #@78
    invoke-direct {v4, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7b
    .line 1407
    .local v4, failIntent:Landroid/content/Intent;
    const/4 v3, 0x0

    #@7c
    .line 1409
    .local v3, ctx:Landroid/content/Context;
    iget-object v8, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@7e
    if-eqz v8, :cond_86

    #@80
    .line 1410
    iget-object v8, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@82
    invoke-virtual {v8}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@85
    move-result-object v3

    #@86
    .line 1413
    :cond_86
    if-eqz v3, :cond_8b

    #@88
    .line 1414
    invoke-virtual {v3, v4, v10}, Landroid/content/Context;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    #@8b
    .line 1418
    .end local v3           #ctx:Landroid/content/Context;
    .end local v4           #failIntent:Landroid/content/Intent;
    :cond_8b
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->updatePhoneState()V

    #@8e
    .line 1420
    iget-object v8, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@90
    invoke-virtual {v8}, Lcom/android/internal/telephony/cdma/CDMAPhone;->notifyPreciseCallStateChanged()V

    #@93
    .line 1421
    iget-object v8, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->droppedDuringPoll:Ljava/util/ArrayList;

    #@95
    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V

    #@98
    goto/16 :goto_f

    #@9a
    .line 1426
    .end local v0           #ar:Landroid/os/AsyncResult;
    .end local v1           #causeCode:I
    .end local v5           #i:I
    .end local v7           #s:I
    :pswitch_9a
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pollCallsWhenSafe()V

    #@9d
    goto/16 :goto_f

    #@9f
    .line 1430
    :pswitch_9f
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->handleRadioAvailable()V

    #@a2
    goto/16 :goto_f

    #@a4
    .line 1434
    :pswitch_a4
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->handleRadioNotAvailable()V

    #@a7
    goto/16 :goto_f

    #@a9
    .line 1439
    :pswitch_a9
    iget-boolean v8, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pendingCallInEcm:Z

    #@ab
    if-eqz v8, :cond_be

    #@ad
    .line 1440
    iget-object v8, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@af
    iget-object v9, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pendingMO:Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@b1
    iget-object v9, v9, Lcom/android/internal/telephony/cdma/CdmaConnection;->address:Ljava/lang/String;

    #@b3
    iget v10, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pendingCallClirMode:I

    #@b5
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->obtainCompleteMessage()Landroid/os/Message;

    #@b8
    move-result-object v11

    #@b9
    invoke-interface {v8, v9, v10, v11}, Lcom/android/internal/telephony/CommandsInterface;->dial(Ljava/lang/String;ILandroid/os/Message;)V

    #@bc
    .line 1441
    iput-boolean v12, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pendingCallInEcm:Z

    #@be
    .line 1443
    :cond_be
    iget-object v8, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@c0
    invoke-virtual {v8, p0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->unsetOnEcbModeExitResponse(Landroid/os/Handler;)V

    #@c3
    goto/16 :goto_f

    #@c5
    .line 1447
    :pswitch_c5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@c7
    check-cast v0, Landroid/os/AsyncResult;

    #@c9
    .line 1448
    .restart local v0       #ar:Landroid/os/AsyncResult;
    iget-object v8, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@cb
    if-nez v8, :cond_f

    #@cd
    .line 1449
    iget-object v8, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@cf
    check-cast v8, Lcom/android/internal/telephony/cdma/CdmaCallWaitingNotification;

    #@d1
    invoke-direct {p0, v8}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->handleCallWaitingInfo(Lcom/android/internal/telephony/cdma/CdmaCallWaitingNotification;)V

    #@d4
    .line 1450
    const-string v8, "CDMA"

    #@d6
    const-string v9, "Event EVENT_CALL_WAITING_INFO_CDMA Received"

    #@d8
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@db
    goto/16 :goto_f

    #@dd
    .line 1455
    .end local v0           #ar:Landroid/os/AsyncResult;
    :pswitch_dd
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@df
    check-cast v0, Landroid/os/AsyncResult;

    #@e1
    .line 1456
    .restart local v0       #ar:Landroid/os/AsyncResult;
    iget-object v8, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@e3
    if-nez v8, :cond_f

    #@e5
    .line 1458
    iget-object v8, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pendingMO:Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@e7
    invoke-virtual {v8}, Lcom/android/internal/telephony/cdma/CdmaConnection;->onConnectedInOrOut()V

    #@ea
    .line 1459
    iput-object v10, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pendingMO:Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@ec
    goto/16 :goto_f

    #@ee
    .line 1466
    .end local v0           #ar:Landroid/os/AsyncResult;
    :pswitch_ee
    new-instance v9, Lcom/android/internal/telephony/cdma/CdmaCallTracker$KddiCdmaInfoRec;

    #@f0
    iget-object v8, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@f2
    check-cast v8, Landroid/os/AsyncResult;

    #@f4
    invoke-direct {v9, p0, v8}, Lcom/android/internal/telephony/cdma/CdmaCallTracker$KddiCdmaInfoRec;-><init>(Lcom/android/internal/telephony/cdma/CdmaCallTracker;Landroid/os/AsyncResult;)V

    #@f7
    invoke-virtual {v9}, Lcom/android/internal/telephony/cdma/CdmaCallTracker$KddiCdmaInfoRec;->start()V

    #@fa
    goto/16 :goto_f

    #@fc
    .line 1472
    :pswitch_fc
    iget-object v8, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pendingMO:Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@fe
    if-nez v8, :cond_105

    #@100
    .line 1473
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->handleCdmaReleaseInfoKddi()V

    #@103
    goto/16 :goto_f

    #@105
    .line 1475
    :cond_105
    new-instance v6, Landroid/os/Message;

    #@107
    invoke-direct {v6}, Landroid/os/Message;-><init>()V

    #@10a
    .line 1476
    .local v6, newMessage:Landroid/os/Message;
    iget-object v8, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@10c
    iput-object v8, v6, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@10e
    .line 1477
    iget v8, p1, Landroid/os/Message;->what:I

    #@110
    iput v8, v6, Landroid/os/Message;->what:I

    #@112
    .line 1478
    invoke-virtual {p0, v6}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->sendMessage(Landroid/os/Message;)Z

    #@115
    goto/16 :goto_f

    #@117
    .line 1354
    nop

    #@118
    :pswitch_data_118
    .packed-switch 0x1
        :pswitch_1d
        :pswitch_9a
        :pswitch_9a
        :pswitch_38
        :pswitch_3c
        :pswitch_15
        :pswitch_15
        :pswitch_f
        :pswitch_9f
        :pswitch_a4
        :pswitch_15
        :pswitch_15
        :pswitch_15
        :pswitch_a9
        :pswitch_c5
        :pswitch_dd
        :pswitch_15
        :pswitch_15
        :pswitch_15
        :pswitch_ee
        :pswitch_fc
    .end packed-switch
.end method

.method protected handlePollCalls(Landroid/os/AsyncResult;)V
    .registers 24
    .parameter "ar"

    #@0
    .prologue
    .line 715
    move-object/from16 v0, p1

    #@2
    iget-object v0, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@4
    move-object/from16 v18, v0

    #@6
    if-nez v18, :cond_10c

    #@8
    .line 716
    move-object/from16 v0, p1

    #@a
    iget-object v0, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@c
    move-object/from16 v16, v0

    #@e
    check-cast v16, Ljava/util/List;

    #@10
    .line 728
    .local v16, polledCalls:Ljava/util/List;
    :goto_10
    const/4 v15, 0x0

    #@11
    .line 729
    .local v15, newRinging:Lcom/android/internal/telephony/Connection;
    const/4 v11, 0x0

    #@12
    .line 731
    .local v11, hasNonHangupStateChanged:Z
    const/4 v14, 0x0

    #@13
    .line 732
    .local v14, needsPollDelay:Z
    const/16 v17, 0x0

    #@15
    .line 734
    .local v17, unknownConnectionAppeared:Z
    const/4 v12, 0x0

    #@16
    .local v12, i:I
    const/4 v7, 0x0

    #@17
    .local v7, curDC:I
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    #@1a
    move-result v9

    #@1b
    .line 735
    .local v9, dcSize:I
    :goto_1b
    move-object/from16 v0, p0

    #@1d
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->connections:[Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@1f
    move-object/from16 v18, v0

    #@21
    move-object/from16 v0, v18

    #@23
    array-length v0, v0

    #@24
    move/from16 v18, v0

    #@26
    move/from16 v0, v18

    #@28
    if-ge v12, v0, :cond_2d7

    #@2a
    .line 736
    move-object/from16 v0, p0

    #@2c
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->connections:[Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@2e
    move-object/from16 v18, v0

    #@30
    aget-object v5, v18, v12

    #@32
    .line 737
    .local v5, conn:Lcom/android/internal/telephony/cdma/CdmaConnection;
    const/4 v8, 0x0

    #@33
    .line 740
    .local v8, dc:Lcom/android/internal/telephony/DriverCall;
    if-ge v7, v9, :cond_4b

    #@35
    .line 741
    move-object/from16 v0, v16

    #@37
    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@3a
    move-result-object v8

    #@3b
    .end local v8           #dc:Lcom/android/internal/telephony/DriverCall;
    check-cast v8, Lcom/android/internal/telephony/DriverCall;

    #@3d
    .line 743
    .restart local v8       #dc:Lcom/android/internal/telephony/DriverCall;
    iget v0, v8, Lcom/android/internal/telephony/DriverCall;->index:I

    #@3f
    move/from16 v18, v0

    #@41
    add-int/lit8 v19, v12, 0x1

    #@43
    move/from16 v0, v18

    #@45
    move/from16 v1, v19

    #@47
    if-ne v0, v1, :cond_127

    #@49
    .line 744
    add-int/lit8 v7, v7, 0x1

    #@4b
    .line 753
    :cond_4b
    :goto_4b
    if-eqz v5, :cond_74

    #@4d
    if-eqz v8, :cond_74

    #@4f
    iget-object v0, v5, Lcom/android/internal/telephony/cdma/CdmaConnection;->address:Ljava/lang/String;

    #@51
    move-object/from16 v18, v0

    #@53
    invoke-static/range {v18 .. v18}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@56
    move-result v18

    #@57
    if-nez v18, :cond_74

    #@59
    invoke-virtual {v5, v8}, Lcom/android/internal/telephony/cdma/CdmaConnection;->compareTo(Lcom/android/internal/telephony/DriverCall;)Z

    #@5c
    move-result v18

    #@5d
    if-nez v18, :cond_74

    #@5f
    .line 761
    const-string v18, "New call with same index. Dropping old call"

    #@61
    move-object/from16 v0, p0

    #@63
    move-object/from16 v1, v18

    #@65
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->log(Ljava/lang/String;)V

    #@68
    .line 762
    move-object/from16 v0, p0

    #@6a
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->droppedDuringPoll:Ljava/util/ArrayList;

    #@6c
    move-object/from16 v18, v0

    #@6e
    move-object/from16 v0, v18

    #@70
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@73
    .line 763
    const/4 v5, 0x0

    #@74
    .line 766
    :cond_74
    if-nez v5, :cond_171

    #@76
    if-eqz v8, :cond_171

    #@78
    .line 768
    move-object/from16 v0, p0

    #@7a
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pendingMO:Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@7c
    move-object/from16 v18, v0

    #@7e
    if-eqz v18, :cond_133

    #@80
    move-object/from16 v0, p0

    #@82
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pendingMO:Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@84
    move-object/from16 v18, v0

    #@86
    move-object/from16 v0, v18

    #@88
    invoke-virtual {v0, v8}, Lcom/android/internal/telephony/cdma/CdmaConnection;->compareTo(Lcom/android/internal/telephony/DriverCall;)Z

    #@8b
    move-result v18

    #@8c
    if-eqz v18, :cond_133

    #@8e
    .line 773
    move-object/from16 v0, p0

    #@90
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->connections:[Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@92
    move-object/from16 v18, v0

    #@94
    move-object/from16 v0, p0

    #@96
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pendingMO:Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@98
    move-object/from16 v19, v0

    #@9a
    aput-object v19, v18, v12

    #@9c
    .line 774
    move-object/from16 v0, p0

    #@9e
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pendingMO:Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@a0
    move-object/from16 v18, v0

    #@a2
    move-object/from16 v0, v18

    #@a4
    iput v12, v0, Lcom/android/internal/telephony/cdma/CdmaConnection;->index:I

    #@a6
    .line 775
    move-object/from16 v0, p0

    #@a8
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pendingMO:Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@aa
    move-object/from16 v18, v0

    #@ac
    move-object/from16 v0, v18

    #@ae
    invoke-virtual {v0, v8}, Lcom/android/internal/telephony/cdma/CdmaConnection;->update(Lcom/android/internal/telephony/DriverCall;)Z

    #@b1
    .line 776
    const/16 v18, 0x0

    #@b3
    move-object/from16 v0, v18

    #@b5
    move-object/from16 v1, p0

    #@b7
    iput-object v0, v1, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pendingMO:Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@b9
    .line 779
    move-object/from16 v0, p0

    #@bb
    iget-boolean v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->hangupPendingMO:Z

    #@bd
    move/from16 v18, v0

    #@bf
    if-eqz v18, :cond_16c

    #@c1
    .line 780
    const/16 v18, 0x0

    #@c3
    move/from16 v0, v18

    #@c5
    move-object/from16 v1, p0

    #@c7
    iput-boolean v0, v1, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->hangupPendingMO:Z

    #@c9
    .line 782
    move-object/from16 v0, p0

    #@cb
    iget-boolean v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->mIsEcmTimerCanceled:Z

    #@cd
    move/from16 v18, v0

    #@cf
    if-eqz v18, :cond_e0

    #@d1
    .line 783
    move-object/from16 v0, p0

    #@d3
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@d5
    move-object/from16 v18, v0

    #@d7
    const/16 v18, 0x0

    #@d9
    move-object/from16 v0, p0

    #@db
    move/from16 v1, v18

    #@dd
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->handleEcmTimer(I)V

    #@e0
    .line 787
    :cond_e0
    :try_start_e0
    new-instance v18, Ljava/lang/StringBuilder;

    #@e2
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@e5
    const-string v19, "poll: hangupPendingMO, hangup conn "

    #@e7
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ea
    move-result-object v18

    #@eb
    move-object/from16 v0, v18

    #@ed
    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f0
    move-result-object v18

    #@f1
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f4
    move-result-object v18

    #@f5
    move-object/from16 v0, p0

    #@f7
    move-object/from16 v1, v18

    #@f9
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->log(Ljava/lang/String;)V

    #@fc
    .line 789
    move-object/from16 v0, p0

    #@fe
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->connections:[Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@100
    move-object/from16 v18, v0

    #@102
    aget-object v18, v18, v12

    #@104
    move-object/from16 v0, p0

    #@106
    move-object/from16 v1, v18

    #@108
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->hangup(Lcom/android/internal/telephony/cdma/CdmaConnection;)V
    :try_end_10b
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_e0 .. :try_end_10b} :catch_12a

    #@10b
    .line 1021
    .end local v5           #conn:Lcom/android/internal/telephony/cdma/CdmaConnection;
    .end local v7           #curDC:I
    .end local v8           #dc:Lcom/android/internal/telephony/DriverCall;
    .end local v9           #dcSize:I
    .end local v11           #hasNonHangupStateChanged:Z
    .end local v12           #i:I
    .end local v14           #needsPollDelay:Z
    .end local v15           #newRinging:Lcom/android/internal/telephony/Connection;
    .end local v16           #polledCalls:Ljava/util/List;
    .end local v17           #unknownConnectionAppeared:Z
    :cond_10b
    :goto_10b
    return-void

    #@10c
    .line 717
    :cond_10c
    move-object/from16 v0, p1

    #@10e
    iget-object v0, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@110
    move-object/from16 v18, v0

    #@112
    move-object/from16 v0, p0

    #@114
    move-object/from16 v1, v18

    #@116
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->isCommandExceptionRadioNotAvailable(Ljava/lang/Throwable;)Z

    #@119
    move-result v18

    #@11a
    if-eqz v18, :cond_123

    #@11c
    .line 720
    new-instance v16, Ljava/util/ArrayList;

    #@11e
    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    #@121
    .restart local v16       #polledCalls:Ljava/util/List;
    goto/16 :goto_10

    #@123
    .line 724
    .end local v16           #polledCalls:Ljava/util/List;
    :cond_123
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pollCallsAfterDelay()V

    #@126
    goto :goto_10b

    #@127
    .line 746
    .restart local v5       #conn:Lcom/android/internal/telephony/cdma/CdmaConnection;
    .restart local v7       #curDC:I
    .restart local v8       #dc:Lcom/android/internal/telephony/DriverCall;
    .restart local v9       #dcSize:I
    .restart local v11       #hasNonHangupStateChanged:Z
    .restart local v12       #i:I
    .restart local v14       #needsPollDelay:Z
    .restart local v15       #newRinging:Lcom/android/internal/telephony/Connection;
    .restart local v16       #polledCalls:Ljava/util/List;
    .restart local v17       #unknownConnectionAppeared:Z
    :cond_127
    const/4 v8, 0x0

    #@128
    goto/16 :goto_4b

    #@12a
    .line 790
    :catch_12a
    move-exception v10

    #@12b
    .line 791
    .local v10, ex:Lcom/android/internal/telephony/CallStateException;
    const-string v18, "CDMA"

    #@12d
    const-string v19, "unexpected error on hangup"

    #@12f
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@132
    goto :goto_10b

    #@133
    .line 800
    .end local v10           #ex:Lcom/android/internal/telephony/CallStateException;
    :cond_133
    new-instance v18, Ljava/lang/StringBuilder;

    #@135
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@138
    const-string v19, "pendingMo="

    #@13a
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13d
    move-result-object v18

    #@13e
    move-object/from16 v0, p0

    #@140
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pendingMO:Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@142
    move-object/from16 v19, v0

    #@144
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@147
    move-result-object v18

    #@148
    const-string v19, ", dc="

    #@14a
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14d
    move-result-object v18

    #@14e
    move-object/from16 v0, v18

    #@150
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@153
    move-result-object v18

    #@154
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@157
    move-result-object v18

    #@158
    move-object/from16 v0, p0

    #@15a
    move-object/from16 v1, v18

    #@15c
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->log(Ljava/lang/String;)V

    #@15f
    .line 803
    move-object/from16 v0, p0

    #@161
    invoke-direct {v0, v8, v12}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->checkMtFindNewRinging(Lcom/android/internal/telephony/DriverCall;I)Lcom/android/internal/telephony/Connection;

    #@164
    move-result-object v15

    #@165
    .line 804
    if-nez v15, :cond_169

    #@167
    .line 805
    const/16 v17, 0x1

    #@169
    .line 807
    :cond_169
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->checkAndEnableDataCallAfterEmergencyCallDropped()V

    #@16c
    .line 809
    :cond_16c
    const/4 v11, 0x1

    #@16d
    .line 735
    :cond_16d
    :goto_16d
    add-int/lit8 v12, v12, 0x1

    #@16f
    goto/16 :goto_1b

    #@171
    .line 810
    :cond_171
    if-eqz v5, :cond_276

    #@173
    if-nez v8, :cond_276

    #@175
    .line 811
    if-eqz v9, :cond_1b0

    #@177
    .line 815
    const-string v18, "conn != null, dc == null. Still have connections in the call list"

    #@179
    move-object/from16 v0, p0

    #@17b
    move-object/from16 v1, v18

    #@17d
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->log(Ljava/lang/String;)V

    #@180
    .line 816
    move-object/from16 v0, p0

    #@182
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->droppedDuringPoll:Ljava/util/ArrayList;

    #@184
    move-object/from16 v18, v0

    #@186
    move-object/from16 v0, v18

    #@188
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@18b
    .line 843
    :goto_18b
    move-object/from16 v0, p0

    #@18d
    iget-boolean v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->mIsEcmTimerCanceled:Z

    #@18f
    move/from16 v18, v0

    #@191
    if-eqz v18, :cond_1a2

    #@193
    .line 844
    move-object/from16 v0, p0

    #@195
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@197
    move-object/from16 v18, v0

    #@199
    const/16 v18, 0x0

    #@19b
    move-object/from16 v0, p0

    #@19d
    move/from16 v1, v18

    #@19f
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->handleEcmTimer(I)V

    #@1a2
    .line 847
    :cond_1a2
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->checkAndEnableDataCallAfterEmergencyCallDropped()V

    #@1a5
    .line 851
    move-object/from16 v0, p0

    #@1a7
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->connections:[Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@1a9
    move-object/from16 v18, v0

    #@1ab
    const/16 v19, 0x0

    #@1ad
    aput-object v19, v18, v12

    #@1af
    goto :goto_16d

    #@1b0
    .line 822
    :cond_1b0
    move-object/from16 v0, p0

    #@1b2
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->foregroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@1b4
    move-object/from16 v18, v0

    #@1b6
    move-object/from16 v0, v18

    #@1b8
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCall;->connections:Ljava/util/ArrayList;

    #@1ba
    move-object/from16 v18, v0

    #@1bc
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    #@1bf
    move-result v6

    #@1c0
    .line 823
    .local v6, count:I
    const/4 v13, 0x0

    #@1c1
    .local v13, n:I
    :goto_1c1
    if-ge v13, v6, :cond_207

    #@1c3
    .line 825
    new-instance v18, Ljava/lang/StringBuilder;

    #@1c5
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@1c8
    const-string v19, "adding fgCall cn "

    #@1ca
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1cd
    move-result-object v18

    #@1ce
    move-object/from16 v0, v18

    #@1d0
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d3
    move-result-object v18

    #@1d4
    const-string v19, " to droppedDuringPoll"

    #@1d6
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d9
    move-result-object v18

    #@1da
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1dd
    move-result-object v18

    #@1de
    move-object/from16 v0, p0

    #@1e0
    move-object/from16 v1, v18

    #@1e2
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->log(Ljava/lang/String;)V

    #@1e5
    .line 826
    move-object/from16 v0, p0

    #@1e7
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->foregroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@1e9
    move-object/from16 v18, v0

    #@1eb
    move-object/from16 v0, v18

    #@1ed
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCall;->connections:Ljava/util/ArrayList;

    #@1ef
    move-object/from16 v18, v0

    #@1f1
    move-object/from16 v0, v18

    #@1f3
    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1f6
    move-result-object v4

    #@1f7
    check-cast v4, Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@1f9
    .line 827
    .local v4, cn:Lcom/android/internal/telephony/cdma/CdmaConnection;
    move-object/from16 v0, p0

    #@1fb
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->droppedDuringPoll:Ljava/util/ArrayList;

    #@1fd
    move-object/from16 v18, v0

    #@1ff
    move-object/from16 v0, v18

    #@201
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@204
    .line 823
    add-int/lit8 v13, v13, 0x1

    #@206
    goto :goto_1c1

    #@207
    .line 829
    .end local v4           #cn:Lcom/android/internal/telephony/cdma/CdmaConnection;
    :cond_207
    move-object/from16 v0, p0

    #@209
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->ringingCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@20b
    move-object/from16 v18, v0

    #@20d
    move-object/from16 v0, v18

    #@20f
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCall;->connections:Ljava/util/ArrayList;

    #@211
    move-object/from16 v18, v0

    #@213
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    #@216
    move-result v6

    #@217
    .line 832
    const/4 v13, 0x0

    #@218
    :goto_218
    if-ge v13, v6, :cond_25e

    #@21a
    .line 834
    new-instance v18, Ljava/lang/StringBuilder;

    #@21c
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@21f
    const-string v19, "adding rgCall cn "

    #@221
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@224
    move-result-object v18

    #@225
    move-object/from16 v0, v18

    #@227
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22a
    move-result-object v18

    #@22b
    const-string v19, " to droppedDuringPoll"

    #@22d
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@230
    move-result-object v18

    #@231
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@234
    move-result-object v18

    #@235
    move-object/from16 v0, p0

    #@237
    move-object/from16 v1, v18

    #@239
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->log(Ljava/lang/String;)V

    #@23c
    .line 835
    move-object/from16 v0, p0

    #@23e
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->ringingCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@240
    move-object/from16 v18, v0

    #@242
    move-object/from16 v0, v18

    #@244
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCall;->connections:Ljava/util/ArrayList;

    #@246
    move-object/from16 v18, v0

    #@248
    move-object/from16 v0, v18

    #@24a
    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@24d
    move-result-object v4

    #@24e
    check-cast v4, Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@250
    .line 836
    .restart local v4       #cn:Lcom/android/internal/telephony/cdma/CdmaConnection;
    move-object/from16 v0, p0

    #@252
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->droppedDuringPoll:Ljava/util/ArrayList;

    #@254
    move-object/from16 v18, v0

    #@256
    move-object/from16 v0, v18

    #@258
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@25b
    .line 832
    add-int/lit8 v13, v13, 0x1

    #@25d
    goto :goto_218

    #@25e
    .line 838
    .end local v4           #cn:Lcom/android/internal/telephony/cdma/CdmaConnection;
    :cond_25e
    move-object/from16 v0, p0

    #@260
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->foregroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@262
    move-object/from16 v18, v0

    #@264
    const/16 v19, 0x0

    #@266
    invoke-virtual/range {v18 .. v19}, Lcom/android/internal/telephony/cdma/CdmaCall;->setGeneric(Z)V

    #@269
    .line 839
    move-object/from16 v0, p0

    #@26b
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->ringingCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@26d
    move-object/from16 v18, v0

    #@26f
    const/16 v19, 0x0

    #@271
    invoke-virtual/range {v18 .. v19}, Lcom/android/internal/telephony/cdma/CdmaCall;->setGeneric(Z)V

    #@274
    goto/16 :goto_18b

    #@276
    .line 852
    .end local v6           #count:I
    .end local v13           #n:I
    :cond_276
    if-eqz v5, :cond_16d

    #@278
    if-eqz v8, :cond_16d

    #@27a
    .line 854
    iget-boolean v0, v5, Lcom/android/internal/telephony/cdma/CdmaConnection;->isIncoming:Z

    #@27c
    move/from16 v18, v0

    #@27e
    iget-boolean v0, v8, Lcom/android/internal/telephony/DriverCall;->isMT:Z

    #@280
    move/from16 v19, v0

    #@282
    move/from16 v0, v18

    #@284
    move/from16 v1, v19

    #@286
    if-eq v0, v1, :cond_2ca

    #@288
    .line 855
    iget-boolean v0, v8, Lcom/android/internal/telephony/DriverCall;->isMT:Z

    #@28a
    move/from16 v18, v0

    #@28c
    const/16 v19, 0x1

    #@28e
    move/from16 v0, v18

    #@290
    move/from16 v1, v19

    #@292
    if-ne v0, v1, :cond_2ae

    #@294
    .line 857
    move-object/from16 v0, p0

    #@296
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->droppedDuringPoll:Ljava/util/ArrayList;

    #@298
    move-object/from16 v18, v0

    #@29a
    move-object/from16 v0, v18

    #@29c
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@29f
    .line 859
    move-object/from16 v0, p0

    #@2a1
    invoke-direct {v0, v8, v12}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->checkMtFindNewRinging(Lcom/android/internal/telephony/DriverCall;I)Lcom/android/internal/telephony/Connection;

    #@2a4
    move-result-object v15

    #@2a5
    .line 860
    if-nez v15, :cond_2a9

    #@2a7
    .line 861
    const/16 v17, 0x1

    #@2a9
    .line 863
    :cond_2a9
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->checkAndEnableDataCallAfterEmergencyCallDropped()V

    #@2ac
    goto/16 :goto_16d

    #@2ae
    .line 869
    :cond_2ae
    const-string v18, "CDMA"

    #@2b0
    new-instance v19, Ljava/lang/StringBuilder;

    #@2b2
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@2b5
    const-string v20, "Error in RIL, Phantom call appeared "

    #@2b7
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2ba
    move-result-object v19

    #@2bb
    move-object/from16 v0, v19

    #@2bd
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2c0
    move-result-object v19

    #@2c1
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c4
    move-result-object v19

    #@2c5
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2c8
    goto/16 :goto_16d

    #@2ca
    .line 873
    :cond_2ca
    invoke-virtual {v5, v8}, Lcom/android/internal/telephony/cdma/CdmaConnection;->update(Lcom/android/internal/telephony/DriverCall;)Z

    #@2cd
    move-result v3

    #@2ce
    .line 874
    .local v3, changed:Z
    if-nez v11, :cond_2d2

    #@2d0
    if-eqz v3, :cond_2d5

    #@2d2
    :cond_2d2
    const/4 v11, 0x1

    #@2d3
    :goto_2d3
    goto/16 :goto_16d

    #@2d5
    :cond_2d5
    const/4 v11, 0x0

    #@2d6
    goto :goto_2d3

    #@2d7
    .line 901
    .end local v3           #changed:Z
    .end local v5           #conn:Lcom/android/internal/telephony/cdma/CdmaConnection;
    .end local v8           #dc:Lcom/android/internal/telephony/DriverCall;
    :cond_2d7
    move-object/from16 v0, p0

    #@2d9
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pendingMO:Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@2db
    move-object/from16 v18, v0

    #@2dd
    if-eqz v18, :cond_330

    #@2df
    .line 902
    const-string v18, "CDMA"

    #@2e1
    new-instance v19, Ljava/lang/StringBuilder;

    #@2e3
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@2e6
    const-string v20, "Pending MO dropped before poll fg state:"

    #@2e8
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2eb
    move-result-object v19

    #@2ec
    move-object/from16 v0, p0

    #@2ee
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->foregroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@2f0
    move-object/from16 v20, v0

    #@2f2
    invoke-virtual/range {v20 .. v20}, Lcom/android/internal/telephony/cdma/CdmaCall;->getState()Lcom/android/internal/telephony/Call$State;

    #@2f5
    move-result-object v20

    #@2f6
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2f9
    move-result-object v19

    #@2fa
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2fd
    move-result-object v19

    #@2fe
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@301
    .line 905
    move-object/from16 v0, p0

    #@303
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->droppedDuringPoll:Ljava/util/ArrayList;

    #@305
    move-object/from16 v18, v0

    #@307
    move-object/from16 v0, p0

    #@309
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pendingMO:Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@30b
    move-object/from16 v19, v0

    #@30d
    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@310
    .line 906
    const/16 v18, 0x0

    #@312
    move-object/from16 v0, v18

    #@314
    move-object/from16 v1, p0

    #@316
    iput-object v0, v1, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pendingMO:Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@318
    .line 907
    const/16 v18, 0x0

    #@31a
    move/from16 v0, v18

    #@31c
    move-object/from16 v1, p0

    #@31e
    iput-boolean v0, v1, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->hangupPendingMO:Z

    #@320
    .line 908
    move-object/from16 v0, p0

    #@322
    iget-boolean v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pendingCallInEcm:Z

    #@324
    move/from16 v18, v0

    #@326
    if-eqz v18, :cond_330

    #@328
    .line 909
    const/16 v18, 0x0

    #@32a
    move/from16 v0, v18

    #@32c
    move-object/from16 v1, p0

    #@32e
    iput-boolean v0, v1, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pendingCallInEcm:Z

    #@330
    .line 914
    :cond_330
    const-string v18, "KDDI"

    #@332
    invoke-static/range {v18 .. v18}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperator(Ljava/lang/String;)Z

    #@335
    move-result v18

    #@336
    if-eqz v18, :cond_49a

    #@338
    .line 915
    const-string v18, "CDMA"

    #@33a
    new-instance v19, Ljava/lang/StringBuilder;

    #@33c
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@33f
    const-string v20, "KDDI network newRinging:"

    #@341
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@344
    move-result-object v19

    #@345
    move-object/from16 v0, v19

    #@347
    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@34a
    move-result-object v19

    #@34b
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34e
    move-result-object v19

    #@34f
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@352
    .line 916
    move-object/from16 v0, p0

    #@354
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@356
    move-object/from16 v18, v0

    #@358
    invoke-virtual/range {v18 .. v18}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getServiceState()Landroid/telephony/ServiceState;

    #@35b
    move-result-object v18

    #@35c
    invoke-virtual/range {v18 .. v18}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@35f
    move-result v18

    #@360
    if-eqz v18, :cond_3f4

    #@362
    .line 917
    if-eqz v15, :cond_36f

    #@364
    .line 918
    move-object/from16 v0, p0

    #@366
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@368
    move-object/from16 v18, v0

    #@36a
    move-object/from16 v0, v18

    #@36c
    invoke-virtual {v0, v15}, Lcom/android/internal/telephony/cdma/CDMAPhone;->notifyNewRingingConnection(Lcom/android/internal/telephony/Connection;)V

    #@36f
    .line 963
    :cond_36f
    :goto_36f
    move-object/from16 v0, p0

    #@371
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->droppedDuringPoll:Ljava/util/ArrayList;

    #@373
    move-object/from16 v18, v0

    #@375
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    #@378
    move-result v18

    #@379
    add-int/lit8 v12, v18, -0x1

    #@37b
    :goto_37b
    if-ltz v12, :cond_4ed

    #@37d
    .line 964
    move-object/from16 v0, p0

    #@37f
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->droppedDuringPoll:Ljava/util/ArrayList;

    #@381
    move-object/from16 v18, v0

    #@383
    move-object/from16 v0, v18

    #@385
    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@388
    move-result-object v5

    #@389
    check-cast v5, Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@38b
    .line 966
    .restart local v5       #conn:Lcom/android/internal/telephony/cdma/CdmaConnection;
    invoke-virtual {v5}, Lcom/android/internal/telephony/cdma/CdmaConnection;->isIncoming()Z

    #@38e
    move-result v18

    #@38f
    if-eqz v18, :cond_4ad

    #@391
    invoke-virtual {v5}, Lcom/android/internal/telephony/cdma/CdmaConnection;->getConnectTime()J

    #@394
    move-result-wide v18

    #@395
    const-wide/16 v20, 0x0

    #@397
    cmp-long v18, v18, v20

    #@399
    if-nez v18, :cond_4ad

    #@39b
    .line 969
    iget-object v0, v5, Lcom/android/internal/telephony/cdma/CdmaConnection;->cause:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@39d
    move-object/from16 v18, v0

    #@39f
    sget-object v19, Lcom/android/internal/telephony/Connection$DisconnectCause;->LOCAL:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@3a1
    move-object/from16 v0, v18

    #@3a3
    move-object/from16 v1, v19

    #@3a5
    if-ne v0, v1, :cond_4a9

    #@3a7
    .line 970
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->INCOMING_REJECTED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@3a9
    .line 976
    .local v2, cause:Lcom/android/internal/telephony/Connection$DisconnectCause;
    :goto_3a9
    new-instance v18, Ljava/lang/StringBuilder;

    #@3ab
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@3ae
    const-string v19, "missed/rejected call, conn.cause="

    #@3b0
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b3
    move-result-object v18

    #@3b4
    iget-object v0, v5, Lcom/android/internal/telephony/cdma/CdmaConnection;->cause:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@3b6
    move-object/from16 v19, v0

    #@3b8
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3bb
    move-result-object v18

    #@3bc
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3bf
    move-result-object v18

    #@3c0
    move-object/from16 v0, p0

    #@3c2
    move-object/from16 v1, v18

    #@3c4
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->log(Ljava/lang/String;)V

    #@3c7
    .line 977
    new-instance v18, Ljava/lang/StringBuilder;

    #@3c9
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@3cc
    const-string v19, "setting cause to "

    #@3ce
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d1
    move-result-object v18

    #@3d2
    move-object/from16 v0, v18

    #@3d4
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3d7
    move-result-object v18

    #@3d8
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3db
    move-result-object v18

    #@3dc
    move-object/from16 v0, p0

    #@3de
    move-object/from16 v1, v18

    #@3e0
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->log(Ljava/lang/String;)V

    #@3e3
    .line 979
    move-object/from16 v0, p0

    #@3e5
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->droppedDuringPoll:Ljava/util/ArrayList;

    #@3e7
    move-object/from16 v18, v0

    #@3e9
    move-object/from16 v0, v18

    #@3eb
    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@3ee
    .line 980
    invoke-virtual {v5, v2}, Lcom/android/internal/telephony/cdma/CdmaConnection;->onDisconnect(Lcom/android/internal/telephony/Connection$DisconnectCause;)V

    #@3f1
    .line 963
    .end local v2           #cause:Lcom/android/internal/telephony/Connection$DisconnectCause;
    :cond_3f1
    :goto_3f1
    add-int/lit8 v12, v12, -0x1

    #@3f3
    goto :goto_37b

    #@3f4
    .line 923
    .end local v5           #conn:Lcom/android/internal/telephony/cdma/CdmaConnection;
    :cond_3f4
    const-string v18, "persist.radio.hwtest"

    #@3f6
    const-string v19, "false"

    #@3f8
    invoke-static/range {v18 .. v19}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@3fb
    move-result-object v18

    #@3fc
    const-string v19, "true"

    #@3fe
    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@401
    move-result v18

    #@402
    if-eqz v18, :cond_413

    #@404
    .line 924
    if-eqz v15, :cond_36f

    #@406
    .line 925
    move-object/from16 v0, p0

    #@408
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@40a
    move-object/from16 v18, v0

    #@40c
    move-object/from16 v0, v18

    #@40e
    invoke-virtual {v0, v15}, Lcom/android/internal/telephony/cdma/CDMAPhone;->notifyNewRingingConnection(Lcom/android/internal/telephony/Connection;)V

    #@411
    goto/16 :goto_36f

    #@413
    .line 931
    :cond_413
    if-nez v15, :cond_43b

    #@415
    move-object/from16 v0, p0

    #@417
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->connections:[Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@419
    move-object/from16 v18, v0

    #@41b
    const/16 v19, 0x0

    #@41d
    aget-object v18, v18, v19

    #@41f
    if-eqz v18, :cond_36f

    #@421
    move-object/from16 v0, p0

    #@423
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->connections:[Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@425
    move-object/from16 v18, v0

    #@427
    const/16 v19, 0x0

    #@429
    aget-object v18, v18, v19

    #@42b
    invoke-virtual/range {v18 .. v18}, Lcom/android/internal/telephony/cdma/CdmaConnection;->getCall()Lcom/android/internal/telephony/cdma/CdmaCall;

    #@42e
    move-result-object v18

    #@42f
    move-object/from16 v0, p0

    #@431
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->ringingCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@433
    move-object/from16 v19, v0

    #@435
    move-object/from16 v0, v18

    #@437
    move-object/from16 v1, v19

    #@439
    if-ne v0, v1, :cond_36f

    #@43b
    .line 932
    :cond_43b
    move-object/from16 v0, p0

    #@43d
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->connections:[Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@43f
    move-object/from16 v18, v0

    #@441
    const/16 v19, 0x0

    #@443
    aget-object v5, v18, v19

    #@445
    .line 934
    .restart local v5       #conn:Lcom/android/internal/telephony/cdma/CdmaConnection;
    const-string v18, "CDMA"

    #@447
    new-instance v19, Ljava/lang/StringBuilder;

    #@449
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@44c
    const-string v20, "isCdmaInfoRecReceivedKddi:"

    #@44e
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@451
    move-result-object v19

    #@452
    iget-boolean v0, v5, Lcom/android/internal/telephony/cdma/CdmaConnection;->isCdmaInfoRecReceivedKddi:Z

    #@454
    move/from16 v20, v0

    #@456
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@459
    move-result-object v19

    #@45a
    const-string v20, " isNotifyRingingConnectionKddi:"

    #@45c
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45f
    move-result-object v19

    #@460
    iget-boolean v0, v5, Lcom/android/internal/telephony/cdma/CdmaConnection;->isNotifyRingingConnectionKddi:Z

    #@462
    move/from16 v20, v0

    #@464
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@467
    move-result-object v19

    #@468
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46b
    move-result-object v19

    #@46c
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@46f
    .line 937
    iget-boolean v0, v5, Lcom/android/internal/telephony/cdma/CdmaConnection;->isCdmaInfoRecReceivedKddi:Z

    #@471
    move/from16 v18, v0

    #@473
    if-eqz v18, :cond_10b

    #@475
    .line 940
    iget-boolean v0, v5, Lcom/android/internal/telephony/cdma/CdmaConnection;->isNotifyRingingConnectionKddi:Z

    #@477
    move/from16 v18, v0

    #@479
    if-nez v18, :cond_36f

    #@47b
    .line 942
    if-nez v15, :cond_487

    #@47d
    .line 943
    move-object/from16 v0, p0

    #@47f
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->connections:[Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@481
    move-object/from16 v18, v0

    #@483
    const/16 v19, 0x0

    #@485
    aget-object v15, v18, v19

    #@487
    .line 945
    :cond_487
    move-object/from16 v0, p0

    #@489
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@48b
    move-object/from16 v18, v0

    #@48d
    move-object/from16 v0, v18

    #@48f
    invoke-virtual {v0, v15}, Lcom/android/internal/telephony/cdma/CDMAPhone;->notifyNewRingingConnection(Lcom/android/internal/telephony/Connection;)V

    #@492
    .line 946
    const/16 v18, 0x1

    #@494
    move/from16 v0, v18

    #@496
    iput-boolean v0, v5, Lcom/android/internal/telephony/cdma/CdmaConnection;->isNotifyRingingConnectionKddi:Z

    #@498
    goto/16 :goto_36f

    #@49a
    .line 956
    .end local v5           #conn:Lcom/android/internal/telephony/cdma/CdmaConnection;
    :cond_49a
    if-eqz v15, :cond_36f

    #@49c
    .line 957
    move-object/from16 v0, p0

    #@49e
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@4a0
    move-object/from16 v18, v0

    #@4a2
    move-object/from16 v0, v18

    #@4a4
    invoke-virtual {v0, v15}, Lcom/android/internal/telephony/cdma/CDMAPhone;->notifyNewRingingConnection(Lcom/android/internal/telephony/Connection;)V

    #@4a7
    goto/16 :goto_36f

    #@4a9
    .line 972
    .restart local v5       #conn:Lcom/android/internal/telephony/cdma/CdmaConnection;
    :cond_4a9
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->INCOMING_MISSED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@4ab
    .restart local v2       #cause:Lcom/android/internal/telephony/Connection$DisconnectCause;
    goto/16 :goto_3a9

    #@4ad
    .line 981
    .end local v2           #cause:Lcom/android/internal/telephony/Connection$DisconnectCause;
    :cond_4ad
    iget-object v0, v5, Lcom/android/internal/telephony/cdma/CdmaConnection;->cause:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@4af
    move-object/from16 v18, v0

    #@4b1
    sget-object v19, Lcom/android/internal/telephony/Connection$DisconnectCause;->LOCAL:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@4b3
    move-object/from16 v0, v18

    #@4b5
    move-object/from16 v1, v19

    #@4b7
    if-ne v0, v1, :cond_4cd

    #@4b9
    .line 983
    move-object/from16 v0, p0

    #@4bb
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->droppedDuringPoll:Ljava/util/ArrayList;

    #@4bd
    move-object/from16 v18, v0

    #@4bf
    move-object/from16 v0, v18

    #@4c1
    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@4c4
    .line 984
    sget-object v18, Lcom/android/internal/telephony/Connection$DisconnectCause;->LOCAL:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@4c6
    move-object/from16 v0, v18

    #@4c8
    invoke-virtual {v5, v0}, Lcom/android/internal/telephony/cdma/CdmaConnection;->onDisconnect(Lcom/android/internal/telephony/Connection$DisconnectCause;)V

    #@4cb
    goto/16 :goto_3f1

    #@4cd
    .line 985
    :cond_4cd
    iget-object v0, v5, Lcom/android/internal/telephony/cdma/CdmaConnection;->cause:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@4cf
    move-object/from16 v18, v0

    #@4d1
    sget-object v19, Lcom/android/internal/telephony/Connection$DisconnectCause;->INVALID_NUMBER:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@4d3
    move-object/from16 v0, v18

    #@4d5
    move-object/from16 v1, v19

    #@4d7
    if-ne v0, v1, :cond_3f1

    #@4d9
    .line 986
    move-object/from16 v0, p0

    #@4db
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->droppedDuringPoll:Ljava/util/ArrayList;

    #@4dd
    move-object/from16 v18, v0

    #@4df
    move-object/from16 v0, v18

    #@4e1
    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@4e4
    .line 987
    sget-object v18, Lcom/android/internal/telephony/Connection$DisconnectCause;->INVALID_NUMBER:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@4e6
    move-object/from16 v0, v18

    #@4e8
    invoke-virtual {v5, v0}, Lcom/android/internal/telephony/cdma/CdmaConnection;->onDisconnect(Lcom/android/internal/telephony/Connection$DisconnectCause;)V

    #@4eb
    goto/16 :goto_3f1

    #@4ed
    .line 992
    .end local v5           #conn:Lcom/android/internal/telephony/cdma/CdmaConnection;
    :cond_4ed
    move-object/from16 v0, p0

    #@4ef
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->droppedDuringPoll:Ljava/util/ArrayList;

    #@4f1
    move-object/from16 v18, v0

    #@4f3
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    #@4f6
    move-result v18

    #@4f7
    if-lez v18, :cond_50c

    #@4f9
    .line 993
    move-object/from16 v0, p0

    #@4fb
    iget-object v0, v0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@4fd
    move-object/from16 v18, v0

    #@4ff
    const/16 v19, 0x5

    #@501
    move-object/from16 v0, p0

    #@503
    move/from16 v1, v19

    #@505
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->obtainNoPollCompleteMessage(I)Landroid/os/Message;

    #@508
    move-result-object v19

    #@509
    invoke-interface/range {v18 .. v19}, Lcom/android/internal/telephony/CommandsInterface;->getLastCallFailCause(Landroid/os/Message;)V

    #@50c
    .line 997
    :cond_50c
    if-eqz v14, :cond_511

    #@50e
    .line 998
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pollCallsAfterDelay()V

    #@511
    .line 1006
    :cond_511
    if-nez v15, :cond_515

    #@513
    if-eqz v11, :cond_518

    #@515
    .line 1007
    :cond_515
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->internalClearDisconnected()V

    #@518
    .line 1010
    :cond_518
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->updatePhoneState()V

    #@51b
    .line 1012
    if-eqz v17, :cond_526

    #@51d
    .line 1013
    move-object/from16 v0, p0

    #@51f
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@521
    move-object/from16 v18, v0

    #@523
    invoke-virtual/range {v18 .. v18}, Lcom/android/internal/telephony/cdma/CDMAPhone;->notifyUnknownConnection()V

    #@526
    .line 1016
    :cond_526
    if-nez v11, :cond_52a

    #@528
    if-eqz v15, :cond_10b

    #@52a
    .line 1017
    :cond_52a
    move-object/from16 v0, p0

    #@52c
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@52e
    move-object/from16 v18, v0

    #@530
    invoke-virtual/range {v18 .. v18}, Lcom/android/internal/telephony/cdma/CDMAPhone;->notifyPreciseCallStateChanged()V

    #@533
    goto/16 :goto_10b
.end method

.method hangup(Lcom/android/internal/telephony/cdma/CdmaCall;)V
    .registers 5
    .parameter "call"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1103
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/CdmaCall;->getConnections()Ljava/util/List;

    #@3
    move-result-object v0

    #@4
    invoke-interface {v0}, Ljava/util/List;->size()I

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_12

    #@a
    .line 1104
    new-instance v0, Lcom/android/internal/telephony/CallStateException;

    #@c
    const-string v1, "no connections in call"

    #@e
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CallStateException;-><init>(Ljava/lang/String;)V

    #@11
    throw v0

    #@12
    .line 1107
    :cond_12
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->ringingCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@14
    if-ne p1, v0, :cond_2d

    #@16
    .line 1108
    const-string v0, "(ringing) hangup waiting or background"

    #@18
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->log(Ljava/lang/String;)V

    #@1b
    .line 1109
    iget-object v0, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@1d
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->obtainCompleteMessage()Landroid/os/Message;

    #@20
    move-result-object v1

    #@21
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/CommandsInterface;->hangupWaitingOrBackground(Landroid/os/Message;)V

    #@24
    .line 1133
    :goto_24
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/CdmaCall;->onHangupLocal()V

    #@27
    .line 1134
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@29
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->notifyPreciseCallStateChanged()V

    #@2c
    .line 1135
    return-void

    #@2d
    .line 1110
    :cond_2d
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->foregroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@2f
    if-ne p1, v0, :cond_51

    #@31
    .line 1111
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/CdmaCall;->isDialingOrAlerting()Z

    #@34
    move-result v0

    #@35
    if-eqz v0, :cond_4d

    #@37
    .line 1113
    const-string v0, "(foregnd) hangup dialing or alerting..."

    #@39
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->log(Ljava/lang/String;)V

    #@3c
    .line 1115
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/CdmaCall;->getConnections()Ljava/util/List;

    #@3f
    move-result-object v0

    #@40
    const/4 v1, 0x0

    #@41
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@44
    move-result-object v0

    #@45
    check-cast v0, Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@47
    check-cast v0, Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@49
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->hangup(Lcom/android/internal/telephony/cdma/CdmaConnection;)V

    #@4c
    goto :goto_24

    #@4d
    .line 1117
    :cond_4d
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->hangupForegroundResumeBackground()V

    #@50
    goto :goto_24

    #@51
    .line 1119
    :cond_51
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->backgroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@53
    if-ne p1, v0, :cond_6a

    #@55
    .line 1120
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->ringingCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@57
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CdmaCall;->isRinging()Z

    #@5a
    move-result v0

    #@5b
    if-eqz v0, :cond_66

    #@5d
    .line 1122
    const-string v0, "hangup all conns in background call"

    #@5f
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->log(Ljava/lang/String;)V

    #@62
    .line 1124
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->hangupAllConnections(Lcom/android/internal/telephony/cdma/CdmaCall;)V

    #@65
    goto :goto_24

    #@66
    .line 1126
    :cond_66
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->hangupWaitingOrBackground()V

    #@69
    goto :goto_24

    #@6a
    .line 1129
    :cond_6a
    new-instance v0, Ljava/lang/RuntimeException;

    #@6c
    new-instance v1, Ljava/lang/StringBuilder;

    #@6e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@71
    const-string v2, "CdmaCall "

    #@73
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v1

    #@77
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v1

    #@7b
    const-string v2, "does not belong to CdmaCallTracker "

    #@7d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v1

    #@81
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v1

    #@85
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@88
    move-result-object v1

    #@89
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@8c
    throw v0
.end method

.method hangup(Lcom/android/internal/telephony/cdma/CdmaCall;Z)V
    .registers 6
    .parameter "call"
    .parameter "answerWaiting"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1178
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/CdmaCall;->getConnections()Ljava/util/List;

    #@3
    move-result-object v0

    #@4
    invoke-interface {v0}, Ljava/util/List;->size()I

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_12

    #@a
    .line 1179
    new-instance v0, Lcom/android/internal/telephony/CallStateException;

    #@c
    const-string v1, "no connections in call"

    #@e
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CallStateException;-><init>(Ljava/lang/String;)V

    #@11
    throw v0

    #@12
    .line 1182
    :cond_12
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->ringingCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@14
    if-ne p1, v0, :cond_2d

    #@16
    .line 1183
    const-string v0, "(ringing) hangup waiting or background"

    #@18
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->log(Ljava/lang/String;)V

    #@1b
    .line 1184
    iget-object v0, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@1d
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->obtainCompleteMessage()Landroid/os/Message;

    #@20
    move-result-object v1

    #@21
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/CommandsInterface;->hangupWaitingOrBackground(Landroid/os/Message;)V

    #@24
    .line 1211
    :goto_24
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/CdmaCall;->onHangupLocal()V

    #@27
    .line 1212
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@29
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->notifyPreciseCallStateChanged()V

    #@2c
    .line 1213
    return-void

    #@2d
    .line 1185
    :cond_2d
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->foregroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@2f
    if-ne p1, v0, :cond_57

    #@31
    .line 1186
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/CdmaCall;->isDialingOrAlerting()Z

    #@34
    move-result v0

    #@35
    if-eqz v0, :cond_4d

    #@37
    .line 1188
    const-string v0, "(foregnd) hangup dialing or alerting..."

    #@39
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->log(Ljava/lang/String;)V

    #@3c
    .line 1190
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/CdmaCall;->getConnections()Ljava/util/List;

    #@3f
    move-result-object v0

    #@40
    const/4 v1, 0x0

    #@41
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@44
    move-result-object v0

    #@45
    check-cast v0, Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@47
    check-cast v0, Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@49
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->hangup(Lcom/android/internal/telephony/cdma/CdmaConnection;)V

    #@4c
    goto :goto_24

    #@4d
    .line 1192
    :cond_4d
    if-eqz p2, :cond_53

    #@4f
    .line 1193
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->hangupForegroundResumeBackground()V

    #@52
    goto :goto_24

    #@53
    .line 1195
    :cond_53
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->hangupAllConnections(Lcom/android/internal/telephony/cdma/CdmaCall;)V

    #@56
    goto :goto_24

    #@57
    .line 1197
    :cond_57
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->backgroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@59
    if-ne p1, v0, :cond_70

    #@5b
    .line 1198
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->ringingCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@5d
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CdmaCall;->isRinging()Z

    #@60
    move-result v0

    #@61
    if-eqz v0, :cond_6c

    #@63
    .line 1200
    const-string v0, "hangup all conns in background call"

    #@65
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->log(Ljava/lang/String;)V

    #@68
    .line 1202
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->hangupAllConnections(Lcom/android/internal/telephony/cdma/CdmaCall;)V

    #@6b
    goto :goto_24

    #@6c
    .line 1204
    :cond_6c
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->hangupWaitingOrBackground()V

    #@6f
    goto :goto_24

    #@70
    .line 1207
    :cond_70
    new-instance v0, Ljava/lang/RuntimeException;

    #@72
    new-instance v1, Ljava/lang/StringBuilder;

    #@74
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@77
    const-string v2, "CdmaCall "

    #@79
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v1

    #@7d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v1

    #@81
    const-string v2, "does not belong to CdmaCallTracker "

    #@83
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v1

    #@87
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v1

    #@8b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8e
    move-result-object v1

    #@8f
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@92
    throw v0
.end method

.method hangup(Lcom/android/internal/telephony/cdma/CdmaConnection;)V
    .registers 6
    .parameter "conn"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1026
    iget-object v1, p1, Lcom/android/internal/telephony/cdma/CdmaConnection;->owner:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@2
    if-eq v1, p0, :cond_27

    #@4
    .line 1027
    new-instance v1, Lcom/android/internal/telephony/CallStateException;

    #@6
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v3, "CdmaConnection "

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    const-string v3, "does not belong to CdmaCallTracker "

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    invoke-direct {v1, v2}, Lcom/android/internal/telephony/CallStateException;-><init>(Ljava/lang/String;)V

    #@26
    throw v1

    #@27
    .line 1031
    :cond_27
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->pendingMO:Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@29
    if-ne p1, v1, :cond_37

    #@2b
    .line 1035
    const-string v1, "hangup: set hangupPendingMO to true"

    #@2d
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->log(Ljava/lang/String;)V

    #@30
    .line 1036
    const/4 v1, 0x1

    #@31
    iput-boolean v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->hangupPendingMO:Z

    #@33
    .line 1065
    :goto_33
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/CdmaConnection;->onHangupLocal()V

    #@36
    .line 1066
    :goto_36
    return-void

    #@37
    .line 1037
    :cond_37
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/CdmaConnection;->getCall()Lcom/android/internal/telephony/cdma/CdmaCall;

    #@3a
    move-result-object v1

    #@3b
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->ringingCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@3d
    if-ne v1, v2, :cond_55

    #@3f
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->ringingCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@41
    invoke-virtual {v1}, Lcom/android/internal/telephony/cdma/CdmaCall;->getState()Lcom/android/internal/telephony/Call$State;

    #@44
    move-result-object v1

    #@45
    sget-object v2, Lcom/android/internal/telephony/Call$State;->WAITING:Lcom/android/internal/telephony/Call$State;

    #@47
    if-ne v1, v2, :cond_55

    #@49
    .line 1050
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/CdmaConnection;->onLocalDisconnect()V

    #@4c
    .line 1051
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->updatePhoneState()V

    #@4f
    .line 1052
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->phone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@51
    invoke-virtual {v1}, Lcom/android/internal/telephony/cdma/CDMAPhone;->notifyPreciseCallStateChanged()V

    #@54
    goto :goto_36

    #@55
    .line 1056
    :cond_55
    :try_start_55
    iget-object v1, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@57
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/CdmaConnection;->getCDMAIndex()I

    #@5a
    move-result v2

    #@5b
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->obtainCompleteMessage()Landroid/os/Message;

    #@5e
    move-result-object v3

    #@5f
    invoke-interface {v1, v2, v3}, Lcom/android/internal/telephony/CommandsInterface;->hangupConnection(ILandroid/os/Message;)V
    :try_end_62
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_55 .. :try_end_62} :catch_63

    #@62
    goto :goto_33

    #@63
    .line 1057
    :catch_63
    move-exception v0

    #@64
    .line 1060
    .local v0, ex:Lcom/android/internal/telephony/CallStateException;
    const-string v1, "CDMA"

    #@66
    new-instance v2, Ljava/lang/StringBuilder;

    #@68
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@6b
    const-string v3, "CdmaCallTracker WARN: hangup() on absent connection "

    #@6d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v2

    #@71
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v2

    #@75
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@78
    move-result-object v2

    #@79
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7c
    goto :goto_33
.end method

.method hangupAllConnections(Lcom/android/internal/telephony/cdma/CdmaCall;)V
    .registers 9
    .parameter "call"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1244
    :try_start_0
    iget-object v4, p1, Lcom/android/internal/telephony/cdma/CdmaCall;->connections:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v1

    #@6
    .line 1245
    .local v1, count:I
    const/4 v3, 0x0

    #@7
    .local v3, i:I
    :goto_7
    if-ge v3, v1, :cond_3a

    #@9
    .line 1246
    iget-object v4, p1, Lcom/android/internal/telephony/cdma/CdmaCall;->connections:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v0

    #@f
    check-cast v0, Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@11
    .line 1247
    .local v0, cn:Lcom/android/internal/telephony/cdma/CdmaConnection;
    iget-object v4, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@13
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CdmaConnection;->getCDMAIndex()I

    #@16
    move-result v5

    #@17
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->obtainCompleteMessage()Landroid/os/Message;

    #@1a
    move-result-object v6

    #@1b
    invoke-interface {v4, v5, v6}, Lcom/android/internal/telephony/CommandsInterface;->hangupConnection(ILandroid/os/Message;)V
    :try_end_1e
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_0 .. :try_end_1e} :catch_21

    #@1e
    .line 1245
    add-int/lit8 v3, v3, 0x1

    #@20
    goto :goto_7

    #@21
    .line 1249
    .end local v0           #cn:Lcom/android/internal/telephony/cdma/CdmaConnection;
    .end local v1           #count:I
    .end local v3           #i:I
    :catch_21
    move-exception v2

    #@22
    .line 1250
    .local v2, ex:Lcom/android/internal/telephony/CallStateException;
    const-string v4, "CDMA"

    #@24
    new-instance v5, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v6, "hangupConnectionByIndex caught "

    #@2b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v5

    #@2f
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v5

    #@33
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v5

    #@37
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 1252
    .end local v2           #ex:Lcom/android/internal/telephony/CallStateException;
    :cond_3a
    return-void
.end method

.method hangupConnectionByIndex(Lcom/android/internal/telephony/cdma/CdmaCall;I)V
    .registers 8
    .parameter "call"
    .parameter "index"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1230
    iget-object v3, p1, Lcom/android/internal/telephony/cdma/CdmaCall;->connections:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v1

    #@6
    .line 1231
    .local v1, count:I
    const/4 v2, 0x0

    #@7
    .local v2, i:I
    :goto_7
    if-ge v2, v1, :cond_24

    #@9
    .line 1232
    iget-object v3, p1, Lcom/android/internal/telephony/cdma/CdmaCall;->connections:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v0

    #@f
    check-cast v0, Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@11
    .line 1233
    .local v0, cn:Lcom/android/internal/telephony/cdma/CdmaConnection;
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CdmaConnection;->getCDMAIndex()I

    #@14
    move-result v3

    #@15
    if-ne v3, p2, :cond_21

    #@17
    .line 1234
    iget-object v3, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@19
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->obtainCompleteMessage()Landroid/os/Message;

    #@1c
    move-result-object v4

    #@1d
    invoke-interface {v3, p2, v4}, Lcom/android/internal/telephony/CommandsInterface;->hangupConnection(ILandroid/os/Message;)V

    #@20
    .line 1235
    return-void

    #@21
    .line 1231
    :cond_21
    add-int/lit8 v2, v2, 0x1

    #@23
    goto :goto_7

    #@24
    .line 1239
    .end local v0           #cn:Lcom/android/internal/telephony/cdma/CdmaConnection;
    :cond_24
    new-instance v3, Lcom/android/internal/telephony/CallStateException;

    #@26
    const-string v4, "no gsm index found"

    #@28
    invoke-direct {v3, v4}, Lcom/android/internal/telephony/CallStateException;-><init>(Ljava/lang/String;)V

    #@2b
    throw v3
.end method

.method hangupForVoIP(Lcom/android/internal/telephony/cdma/CdmaCall;)V
    .registers 5
    .parameter "call"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1141
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/CdmaCall;->getConnections()Ljava/util/List;

    #@3
    move-result-object v0

    #@4
    invoke-interface {v0}, Ljava/util/List;->size()I

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_12

    #@a
    .line 1142
    new-instance v0, Lcom/android/internal/telephony/CallStateException;

    #@c
    const-string v1, "no connections in call"

    #@e
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CallStateException;-><init>(Ljava/lang/String;)V

    #@11
    throw v0

    #@12
    .line 1145
    :cond_12
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->ringingCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@14
    if-ne p1, v0, :cond_28

    #@16
    .line 1146
    const-string v0, "(ringing) hangup waiting or background"

    #@18
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->log(Ljava/lang/String;)V

    #@1b
    .line 1147
    iget-object v0, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@1d
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->obtainCompleteMessage()Landroid/os/Message;

    #@20
    move-result-object v1

    #@21
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/CommandsInterface;->hangupWaitingOrBackground(Landroid/os/Message;)V

    #@24
    .line 1171
    :goto_24
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/CdmaCall;->onHangupLocalMissed()V

    #@27
    .line 1172
    return-void

    #@28
    .line 1148
    :cond_28
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->foregroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@2a
    if-ne p1, v0, :cond_4c

    #@2c
    .line 1149
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/CdmaCall;->isDialingOrAlerting()Z

    #@2f
    move-result v0

    #@30
    if-eqz v0, :cond_48

    #@32
    .line 1151
    const-string v0, "(foregnd) hangup dialing or alerting..."

    #@34
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->log(Ljava/lang/String;)V

    #@37
    .line 1153
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/CdmaCall;->getConnections()Ljava/util/List;

    #@3a
    move-result-object v0

    #@3b
    const/4 v1, 0x0

    #@3c
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@3f
    move-result-object v0

    #@40
    check-cast v0, Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@42
    check-cast v0, Lcom/android/internal/telephony/cdma/CdmaConnection;

    #@44
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->hangup(Lcom/android/internal/telephony/cdma/CdmaConnection;)V

    #@47
    goto :goto_24

    #@48
    .line 1155
    :cond_48
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->hangupForegroundResumeBackground()V

    #@4b
    goto :goto_24

    #@4c
    .line 1157
    :cond_4c
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->backgroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@4e
    if-ne p1, v0, :cond_65

    #@50
    .line 1158
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->ringingCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@52
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CdmaCall;->isRinging()Z

    #@55
    move-result v0

    #@56
    if-eqz v0, :cond_61

    #@58
    .line 1160
    const-string v0, "hangup all conns in background call"

    #@5a
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->log(Ljava/lang/String;)V

    #@5d
    .line 1162
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->hangupAllConnections(Lcom/android/internal/telephony/cdma/CdmaCall;)V

    #@60
    goto :goto_24

    #@61
    .line 1164
    :cond_61
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->hangupWaitingOrBackground()V

    #@64
    goto :goto_24

    #@65
    .line 1167
    :cond_65
    new-instance v0, Ljava/lang/RuntimeException;

    #@67
    new-instance v1, Ljava/lang/StringBuilder;

    #@69
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@6c
    const-string v2, "CdmaCall "

    #@6e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v1

    #@72
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v1

    #@76
    const-string v2, "does not belong to CdmaCallTracker "

    #@78
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v1

    #@7c
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v1

    #@80
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@83
    move-result-object v1

    #@84
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@87
    throw v0
.end method

.method hangupForegroundResumeBackground()V
    .registers 3

    #@0
    .prologue
    .line 1224
    const-string v0, "hangupForegroundResumeBackground"

    #@2
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->log(Ljava/lang/String;)V

    #@5
    .line 1225
    iget-object v0, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@7
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->obtainCompleteMessage()Landroid/os/Message;

    #@a
    move-result-object v1

    #@b
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/CommandsInterface;->hangupForegroundResumeBackground(Landroid/os/Message;)V

    #@e
    .line 1226
    return-void
.end method

.method hangupWaitingOrBackground()V
    .registers 3

    #@0
    .prologue
    .line 1218
    const-string v0, "hangupWaitingOrBackground"

    #@2
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->log(Ljava/lang/String;)V

    #@5
    .line 1219
    iget-object v0, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@7
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->obtainCompleteMessage()Landroid/os/Message;

    #@a
    move-result-object v1

    #@b
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/CommandsInterface;->hangupWaitingOrBackground(Landroid/os/Message;)V

    #@e
    .line 1220
    return-void
.end method

.method isInEmergencyCall()Z
    .registers 2

    #@0
    .prologue
    .line 1594
    iget-boolean v0, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->mIsInEmergencyCall:Z

    #@2
    return v0
.end method

.method protected log(Ljava/lang/String;)V
    .registers 5
    .parameter "msg"

    #@0
    .prologue
    .line 1867
    const-string v0, "CDMA"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[CdmaCallTracker] "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1868
    return-void
.end method

.method public registerForCallWaiting(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 257
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 258
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->callWaitingRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 259
    return-void
.end method

.method public registerForVoiceCallEnded(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 248
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 249
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->voiceCallEndedRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 250
    return-void
.end method

.method public registerForVoiceCallStarted(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 8
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 236
    new-instance v0, Landroid/os/Registrant;

    #@3
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@6
    .line 237
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->voiceCallStartedRegistrants:Landroid/os/RegistrantList;

    #@8
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@b
    .line 239
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->state:Lcom/android/internal/telephony/PhoneConstants$State;

    #@d
    sget-object v2, Lcom/android/internal/telephony/PhoneConstants$State;->IDLE:Lcom/android/internal/telephony/PhoneConstants$State;

    #@f
    if-eq v1, v2, :cond_19

    #@11
    .line 240
    new-instance v1, Landroid/os/AsyncResult;

    #@13
    invoke-direct {v1, v3, v3, v3}, Landroid/os/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@16
    invoke-virtual {v0, v1}, Landroid/os/Registrant;->notifyRegistrant(Landroid/os/AsyncResult;)V

    #@19
    .line 242
    :cond_19
    return-void
.end method

.method rejectCall()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 522
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->ringingCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CdmaCall;->getState()Lcom/android/internal/telephony/Call$State;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Lcom/android/internal/telephony/Call$State;->isRinging()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_16

    #@c
    .line 523
    iget-object v0, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@e
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->obtainCompleteMessage()Landroid/os/Message;

    #@11
    move-result-object v1

    #@12
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/CommandsInterface;->rejectCall(Landroid/os/Message;)V

    #@15
    .line 527
    return-void

    #@16
    .line 525
    :cond_16
    new-instance v0, Lcom/android/internal/telephony/CallStateException;

    #@18
    const-string v1, "phone not ringing"

    #@1a
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CallStateException;-><init>(Ljava/lang/String;)V

    #@1d
    throw v0
.end method

.method separate(Lcom/android/internal/telephony/cdma/CdmaConnection;)V
    .registers 6
    .parameter "conn"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1070
    iget-object v1, p1, Lcom/android/internal/telephony/cdma/CdmaConnection;->owner:Lcom/android/internal/telephony/cdma/CdmaCallTracker;

    #@2
    if-eq v1, p0, :cond_27

    #@4
    .line 1071
    new-instance v1, Lcom/android/internal/telephony/CallStateException;

    #@6
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v3, "CdmaConnection "

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    const-string v3, "does not belong to CdmaCallTracker "

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    invoke-direct {v1, v2}, Lcom/android/internal/telephony/CallStateException;-><init>(Ljava/lang/String;)V

    #@26
    throw v1

    #@27
    .line 1075
    :cond_27
    :try_start_27
    iget-object v1, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@29
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/CdmaConnection;->getCDMAIndex()I

    #@2c
    move-result v2

    #@2d
    const/16 v3, 0xc

    #@2f
    invoke-direct {p0, v3}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->obtainCompleteMessage(I)Landroid/os/Message;

    #@32
    move-result-object v3

    #@33
    invoke-interface {v1, v2, v3}, Lcom/android/internal/telephony/CommandsInterface;->separateConnection(ILandroid/os/Message;)V
    :try_end_36
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_27 .. :try_end_36} :catch_37

    #@36
    .line 1083
    :goto_36
    return-void

    #@37
    .line 1077
    :catch_37
    move-exception v0

    #@38
    .line 1080
    .local v0, ex:Lcom/android/internal/telephony/CallStateException;
    const-string v1, "CDMA"

    #@3a
    new-instance v2, Ljava/lang/StringBuilder;

    #@3c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3f
    const-string v3, "CdmaCallTracker WARN: separate() on absent connection "

    #@41
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v2

    #@45
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v2

    #@49
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c
    move-result-object v2

    #@4d
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@50
    goto :goto_36
.end method

.method setMute(Z)V
    .registers 5
    .parameter "mute"

    #@0
    .prologue
    .line 1089
    iput-boolean p1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->desiredMute:Z

    #@2
    .line 1090
    iget-object v0, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@4
    iget-boolean v1, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->desiredMute:Z

    #@6
    const/4 v2, 0x0

    #@7
    invoke-interface {v0, v1, v2}, Lcom/android/internal/telephony/CommandsInterface;->setMute(ZLandroid/os/Message;)V

    #@a
    .line 1091
    return-void
.end method

.method switchWaitingOrHoldingAndActive()V
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 532
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->ringingCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CdmaCall;->getState()Lcom/android/internal/telephony/Call$State;

    #@5
    move-result-object v0

    #@6
    sget-object v1, Lcom/android/internal/telephony/Call$State;->INCOMING:Lcom/android/internal/telephony/Call$State;

    #@8
    if-ne v0, v1, :cond_12

    #@a
    .line 533
    new-instance v0, Lcom/android/internal/telephony/CallStateException;

    #@c
    const-string v1, "cannot be in the incoming state"

    #@e
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/CallStateException;-><init>(Ljava/lang/String;)V

    #@11
    throw v0

    #@12
    .line 534
    :cond_12
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->foregroundCall:Lcom/android/internal/telephony/cdma/CdmaCall;

    #@14
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CdmaCall;->getConnections()Ljava/util/List;

    #@17
    move-result-object v0

    #@18
    invoke-interface {v0}, Ljava/util/List;->size()I

    #@1b
    move-result v0

    #@1c
    const/4 v1, 0x1

    #@1d
    if-le v0, v1, :cond_23

    #@1f
    .line 535
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->flashAndSetGenericTrue()V

    #@22
    .line 543
    :goto_22
    return-void

    #@23
    .line 541
    :cond_23
    iget-object v0, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@25
    const-string v1, ""

    #@27
    const/16 v2, 0x8

    #@29
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->obtainMessage(I)Landroid/os/Message;

    #@2c
    move-result-object v2

    #@2d
    invoke-interface {v0, v1, v2}, Lcom/android/internal/telephony/CommandsInterface;->sendCDMAFeatureCode(Ljava/lang/String;Landroid/os/Message;)V

    #@30
    goto :goto_22
.end method

.method public unregisterForCallWaiting(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 262
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->callWaitingRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 263
    return-void
.end method

.method public unregisterForVoiceCallEnded(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 253
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->voiceCallEndedRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 254
    return-void
.end method

.method public unregisterForVoiceCallStarted(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 244
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaCallTracker;->voiceCallStartedRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 245
    return-void
.end method
