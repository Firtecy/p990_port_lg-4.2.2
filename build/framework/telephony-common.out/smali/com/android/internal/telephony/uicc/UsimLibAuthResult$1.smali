.class final Lcom/android/internal/telephony/uicc/UsimLibAuthResult$1;
.super Ljava/lang/Object;
.source "UsimLibAuthResult.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/uicc/UsimLibAuthResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/android/internal/telephony/uicc/UsimLibAuthResult;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 23
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/android/internal/telephony/uicc/UsimLibAuthResult;
    .registers 3
    .parameter "source"

    #@0
    .prologue
    .line 25
    invoke-static {p1}, Lcom/android/internal/telephony/uicc/UsimLibAuthResult;->createFromParcel(Landroid/os/Parcel;)Lcom/android/internal/telephony/uicc/UsimLibAuthResult;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 23
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/uicc/UsimLibAuthResult$1;->createFromParcel(Landroid/os/Parcel;)Lcom/android/internal/telephony/uicc/UsimLibAuthResult;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public newArray(I)[Lcom/android/internal/telephony/uicc/UsimLibAuthResult;
    .registers 3
    .parameter "size"

    #@0
    .prologue
    .line 29
    new-array v0, p1, [Lcom/android/internal/telephony/uicc/UsimLibAuthResult;

    #@2
    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 23
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/uicc/UsimLibAuthResult$1;->newArray(I)[Lcom/android/internal/telephony/uicc/UsimLibAuthResult;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method
