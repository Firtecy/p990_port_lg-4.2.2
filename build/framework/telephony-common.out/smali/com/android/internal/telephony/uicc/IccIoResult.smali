.class public Lcom/android/internal/telephony/uicc/IccIoResult;
.super Ljava/lang/Object;
.source "IccIoResult.java"


# instance fields
.field public payload:[B

.field public sw1:I

.field public sw2:I


# direct methods
.method public constructor <init>(IILjava/lang/String;)V
    .registers 5
    .parameter "sw1"
    .parameter "sw2"
    .parameter "hexString"

    #@0
    .prologue
    .line 37
    invoke-static {p3}, Lcom/android/internal/telephony/uicc/IccUtils;->hexStringToBytes(Ljava/lang/String;)[B

    #@3
    move-result-object v0

    #@4
    invoke-direct {p0, p1, p2, v0}, Lcom/android/internal/telephony/uicc/IccIoResult;-><init>(II[B)V

    #@7
    .line 38
    return-void
.end method

.method public constructor <init>(II[B)V
    .registers 4
    .parameter "sw1"
    .parameter "sw2"
    .parameter "payload"

    #@0
    .prologue
    .line 30
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 31
    iput p1, p0, Lcom/android/internal/telephony/uicc/IccIoResult;->sw1:I

    #@5
    .line 32
    iput p2, p0, Lcom/android/internal/telephony/uicc/IccIoResult;->sw2:I

    #@7
    .line 33
    iput-object p3, p0, Lcom/android/internal/telephony/uicc/IccIoResult;->payload:[B

    #@9
    .line 34
    return-void
.end method


# virtual methods
.method public getException()Lcom/android/internal/telephony/uicc/IccException;
    .registers 4

    #@0
    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/IccIoResult;->success()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x0

    #@7
    .line 68
    :goto_7
    return-object v0

    #@8
    .line 60
    :cond_8
    iget v0, p0, Lcom/android/internal/telephony/uicc/IccIoResult;->sw1:I

    #@a
    packed-switch v0, :pswitch_data_46

    #@d
    .line 68
    new-instance v0, Lcom/android/internal/telephony/uicc/IccException;

    #@f
    new-instance v1, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v2, "sw1:"

    #@16
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    iget v2, p0, Lcom/android/internal/telephony/uicc/IccIoResult;->sw1:I

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    const-string v2, " sw2:"

    #@22
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v1

    #@26
    iget v2, p0, Lcom/android/internal/telephony/uicc/IccIoResult;->sw2:I

    #@28
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v1

    #@2c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v1

    #@30
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/uicc/IccException;-><init>(Ljava/lang/String;)V

    #@33
    goto :goto_7

    #@34
    .line 62
    :pswitch_34
    iget v0, p0, Lcom/android/internal/telephony/uicc/IccIoResult;->sw2:I

    #@36
    const/16 v1, 0x8

    #@38
    if-ne v0, v1, :cond_40

    #@3a
    .line 63
    new-instance v0, Lcom/android/internal/telephony/uicc/IccFileTypeMismatch;

    #@3c
    invoke-direct {v0}, Lcom/android/internal/telephony/uicc/IccFileTypeMismatch;-><init>()V

    #@3f
    goto :goto_7

    #@40
    .line 65
    :cond_40
    new-instance v0, Lcom/android/internal/telephony/uicc/IccFileNotFound;

    #@42
    invoke-direct {v0}, Lcom/android/internal/telephony/uicc/IccFileNotFound;-><init>()V

    #@45
    goto :goto_7

    #@46
    .line 60
    :pswitch_data_46
    .packed-switch 0x94
        :pswitch_34
    .end packed-switch
.end method

.method public success()Z
    .registers 3

    #@0
    .prologue
    .line 51
    iget v0, p0, Lcom/android/internal/telephony/uicc/IccIoResult;->sw1:I

    #@2
    const/16 v1, 0x90

    #@4
    if-eq v0, v1, :cond_18

    #@6
    iget v0, p0, Lcom/android/internal/telephony/uicc/IccIoResult;->sw1:I

    #@8
    const/16 v1, 0x91

    #@a
    if-eq v0, v1, :cond_18

    #@c
    iget v0, p0, Lcom/android/internal/telephony/uicc/IccIoResult;->sw1:I

    #@e
    const/16 v1, 0x9e

    #@10
    if-eq v0, v1, :cond_18

    #@12
    iget v0, p0, Lcom/android/internal/telephony/uicc/IccIoResult;->sw1:I

    #@14
    const/16 v1, 0x9f

    #@16
    if-ne v0, v1, :cond_1a

    #@18
    :cond_18
    const/4 v0, 0x1

    #@19
    :goto_19
    return v0

    #@1a
    :cond_1a
    const/4 v0, 0x0

    #@1b
    goto :goto_19
.end method

.method public sw1()I
    .registers 2

    #@0
    .prologue
    .line 78
    iget v0, p0, Lcom/android/internal/telephony/uicc/IccIoResult;->sw1:I

    #@2
    return v0
.end method

.method public sw2()I
    .registers 2

    #@0
    .prologue
    .line 85
    iget v0, p0, Lcom/android/internal/telephony/uicc/IccIoResult;->sw2:I

    #@2
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 41
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "IccIoResponse sw1:0x"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget v1, p0, Lcom/android/internal/telephony/uicc/IccIoResult;->sw1:I

    #@d
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    const-string v1, " sw2:0x"

    #@17
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v0

    #@1b
    iget v1, p0, Lcom/android/internal/telephony/uicc/IccIoResult;->sw2:I

    #@1d
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v0

    #@25
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v0

    #@29
    return-object v0
.end method
