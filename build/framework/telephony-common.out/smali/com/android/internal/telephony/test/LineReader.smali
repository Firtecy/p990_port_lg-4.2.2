.class Lcom/android/internal/telephony/test/LineReader;
.super Ljava/lang/Object;
.source "ModelInterpreter.java"


# static fields
.field static final BUFFER_SIZE:I = 0x1000


# instance fields
.field buffer:[B

.field inStream:Ljava/io/InputStream;


# direct methods
.method constructor <init>(Ljava/io/InputStream;)V
    .registers 3
    .parameter "s"

    #@0
    .prologue
    .line 55
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 48
    const/16 v0, 0x1000

    #@5
    new-array v0, v0, [B

    #@7
    iput-object v0, p0, Lcom/android/internal/telephony/test/LineReader;->buffer:[B

    #@9
    .line 56
    iput-object p1, p0, Lcom/android/internal/telephony/test/LineReader;->inStream:Ljava/io/InputStream;

    #@b
    .line 57
    return-void
.end method


# virtual methods
.method getNextLine()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 62
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/test/LineReader;->getNextLine(Z)Ljava/lang/String;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method getNextLine(Z)Ljava/lang/String;
    .registers 11
    .parameter "ctrlZ"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 81
    const/4 v1, 0x0

    #@2
    .local v1, i:I
    move v2, v1

    #@3
    .line 87
    .end local v1           #i:I
    .local v2, i:I
    :goto_3
    :try_start_3
    iget-object v5, p0, Lcom/android/internal/telephony/test/LineReader;->inStream:Ljava/io/InputStream;

    #@5
    invoke-virtual {v5}, Ljava/io/InputStream;->read()I
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_8} :catch_34
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_3 .. :try_end_8} :catch_37

    #@8
    move-result v3

    #@9
    .line 89
    .local v3, result:I
    if-gez v3, :cond_d

    #@b
    move v1, v2

    #@c
    .line 116
    .end local v2           #i:I
    .end local v3           #result:I
    .restart local v1       #i:I
    :goto_c
    return-object v4

    #@d
    .line 93
    .end local v1           #i:I
    .restart local v2       #i:I
    .restart local v3       #result:I
    :cond_d
    if-eqz p1, :cond_20

    #@f
    const/16 v5, 0x1a

    #@11
    if-ne v3, v5, :cond_20

    #@13
    :cond_13
    move v1, v2

    #@14
    .line 113
    .end local v2           #i:I
    .end local v3           #result:I
    .restart local v1       #i:I
    :goto_14
    :try_start_14
    new-instance v5, Ljava/lang/String;

    #@16
    iget-object v6, p0, Lcom/android/internal/telephony/test/LineReader;->buffer:[B

    #@18
    const/4 v7, 0x0

    #@19
    const-string v8, "US-ASCII"

    #@1b
    invoke-direct {v5, v6, v7, v1, v8}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V
    :try_end_1e
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_14 .. :try_end_1e} :catch_41

    #@1e
    move-object v4, v5

    #@1f
    goto :goto_c

    #@20
    .line 95
    .end local v1           #i:I
    .restart local v2       #i:I
    .restart local v3       #result:I
    :cond_20
    const/16 v5, 0xd

    #@22
    if-eq v3, v5, :cond_28

    #@24
    const/16 v5, 0xa

    #@26
    if-ne v3, v5, :cond_2b

    #@28
    .line 96
    :cond_28
    if-nez v2, :cond_13

    #@2a
    goto :goto_3

    #@2b
    .line 104
    :cond_2b
    :try_start_2b
    iget-object v5, p0, Lcom/android/internal/telephony/test/LineReader;->buffer:[B
    :try_end_2d
    .catch Ljava/io/IOException; {:try_start_2b .. :try_end_2d} :catch_34
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_2b .. :try_end_2d} :catch_37

    #@2d
    add-int/lit8 v1, v2, 0x1

    #@2f
    .end local v2           #i:I
    .restart local v1       #i:I
    int-to-byte v6, v3

    #@30
    :try_start_30
    aput-byte v6, v5, v2
    :try_end_32
    .catch Ljava/io/IOException; {:try_start_30 .. :try_end_32} :catch_4c
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_30 .. :try_end_32} :catch_4a

    #@32
    move v2, v1

    #@33
    .line 105
    .end local v1           #i:I
    .restart local v2       #i:I
    goto :goto_3

    #@34
    .line 106
    .end local v3           #result:I
    :catch_34
    move-exception v0

    #@35
    move v1, v2

    #@36
    .line 107
    .end local v2           #i:I
    .local v0, ex:Ljava/io/IOException;
    .restart local v1       #i:I
    :goto_36
    goto :goto_c

    #@37
    .line 108
    .end local v0           #ex:Ljava/io/IOException;
    .end local v1           #i:I
    .restart local v2       #i:I
    :catch_37
    move-exception v0

    #@38
    move v1, v2

    #@39
    .line 109
    .end local v2           #i:I
    .local v0, ex:Ljava/lang/IndexOutOfBoundsException;
    .restart local v1       #i:I
    :goto_39
    sget-object v5, Ljava/lang/System;->err:Ljava/io/PrintStream;

    #@3b
    const-string v6, "ATChannel: buffer overflow"

    #@3d
    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    #@40
    goto :goto_14

    #@41
    .line 114
    .end local v0           #ex:Ljava/lang/IndexOutOfBoundsException;
    :catch_41
    move-exception v0

    #@42
    .line 115
    .local v0, ex:Ljava/io/UnsupportedEncodingException;
    sget-object v5, Ljava/lang/System;->err:Ljava/io/PrintStream;

    #@44
    const-string v6, "ATChannel: implausable UnsupportedEncodingException"

    #@46
    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    #@49
    goto :goto_c

    #@4a
    .line 108
    .end local v0           #ex:Ljava/io/UnsupportedEncodingException;
    .restart local v3       #result:I
    :catch_4a
    move-exception v0

    #@4b
    goto :goto_39

    #@4c
    .line 106
    :catch_4c
    move-exception v0

    #@4d
    goto :goto_36
.end method

.method getNextLineCtrlZ()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 68
    const/4 v0, 0x1

    #@1
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/test/LineReader;->getNextLine(Z)Ljava/lang/String;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method
