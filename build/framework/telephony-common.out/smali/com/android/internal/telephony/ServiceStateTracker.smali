.class public abstract Lcom/android/internal/telephony/ServiceStateTracker;
.super Landroid/os/Handler;
.source "ServiceStateTracker.java"


# static fields
.field protected static final ACTION_RADIO_OFF:Ljava/lang/String; = "android.intent.action.ACTION_RADIO_OFF"

.field protected static final DBG:Z = true

#the value of this static final field might be set in the static constructor
.field protected static final DBG_CALL:Z = false

.field public static final DEFAULT_GPRS_CHECK_PERIOD_MILLIS:I = 0xea60

.field protected static final EVENT_APN_CHANGED:I = 0x34

.field protected static final EVENT_BLOCK_EHRPD_INTERNET_IPV6:I = 0x32

.field protected static final EVENT_CDMA_PRL_VERSION_CHANGED:I = 0x28

.field protected static final EVENT_CDMA_SUBSCRIPTION_SOURCE_CHANGED:I = 0x27

.field protected static final EVENT_CHANGE_IMS_STATE:I = 0x2c

.field protected static final EVENT_CHECK_REPORT_GPRS:I = 0x16

.field protected static final EVENT_ERI_FILE_LOADED:I = 0x24

.field protected static final EVENT_GET_EHRPD_INFO_DONE:I = 0x65

.field protected static final EVENT_GET_LOC_DONE:I = 0xf

.field protected static final EVENT_GET_LOC_DONE_CDMA:I = 0x1f

.field protected static final EVENT_GET_LTE_INFO_DONE:I = 0x64

.field protected static final EVENT_GET_PREFERRED_NETWORK_TYPE:I = 0x13

.field protected static final EVENT_GET_SERVICES_STATUS:I = 0x2d

.field protected static final EVENT_GET_SIGNAL_STRENGTH:I = 0x3

.field protected static final EVENT_GET_SIGNAL_STRENGTH_CDMA:I = 0x1d

.field protected static final EVENT_HDR_ROAMING_INDICATOR:I = 0x47

.field public static final EVENT_ICC_CHANGED:I = 0x2a

.field protected static final EVENT_LOCATION_UPDATES_ENABLED:I = 0x12

.field protected static final EVENT_LTE_EHRPD_FORCED_CHANGED:I = 0x46

.field protected static final EVENT_NETWORK_STATE_CHANGED:I = 0x2

.field protected static final EVENT_NETWORK_STATE_CHANGED_CDMA:I = 0x1e

.field protected static final EVENT_NITZ_TIME:I = 0xb

.field protected static final EVENT_NV_LOADED:I = 0x21

.field protected static final EVENT_NV_READY:I = 0x23

.field protected static final EVENT_OTA_PROVISION_STATUS_CHANGE:I = 0x25

.field protected static final EVENT_POLL_SIGNAL_STRENGTH:I = 0xa

.field protected static final EVENT_POLL_SIGNAL_STRENGTH_CDMA:I = 0x1c

.field protected static final EVENT_POLL_STATE_CDMA_SUBSCRIPTION:I = 0x22

.field protected static final EVENT_POLL_STATE_GPRS:I = 0x5

.field protected static final EVENT_POLL_STATE_NETWORK_SELECTION_MODE:I = 0xe

.field protected static final EVENT_POLL_STATE_OPERATOR:I = 0x6

.field protected static final EVENT_POLL_STATE_OPERATOR_CDMA:I = 0x19

.field protected static final EVENT_POLL_STATE_REGISTRATION:I = 0x4

.field protected static final EVENT_POLL_STATE_REGISTRATION_CDMA:I = 0x18

.field protected static final EVENT_RADIO_AVAILABLE:I = 0xd

.field protected static final EVENT_RADIO_ON:I = 0x29

.field protected static final EVENT_RADIO_STATE_CHANGED:I = 0x1

.field protected static final EVENT_RESET_PREFERRED_NETWORK_TYPE:I = 0x15

.field protected static final EVENT_RESTRICTED_STATE_CHANGED:I = 0x17

.field protected static final EVENT_RUIM_READY:I = 0x1a

.field protected static final EVENT_RUIM_RECORDS_LOADED:I = 0x1b

.field protected static final EVENT_SET_PREFERRED_NETWORK_TYPE:I = 0x14

.field protected static final EVENT_SET_RADIO_POWER_OFF:I = 0x26

.field protected static final EVENT_SIGNAL_STRENGTH_UPDATE:I = 0xc

.field protected static final EVENT_SIM_READY:I = 0x11

.field protected static final EVENT_SIM_RECORDS_LOADED:I = 0x10

.field protected static final EVENT_WCDMA_ACCEPT_RECEIVED:I = 0x31

.field protected static final EVENT_WCDMA_REJECT_RECEIVED:I = 0x30

.field protected static final GMT_COUNTRY_CODES:[Ljava/lang/String; = null

.field public static final OTASP_NEEDED:I = 0x2

.field public static final OTASP_NOT_NEEDED:I = 0x3

.field public static final OTASP_UNINITIALIZED:I = 0x0

.field public static final OTASP_UNKNOWN:I = 0x1

.field protected static final POLL_PERIOD_MILLIS:I = 0x4e20

.field protected static final REGISTRATION_DENIED_AUTH:Ljava/lang/String; = "Authentication Failure"

.field protected static final REGISTRATION_DENIED_GEN:Ljava/lang/String; = "General"

.field protected static final TIMEZONE_PROPERTY:Ljava/lang/String; = "persist.sys.timezone"

.field public static ehrpd_ipv6_block_iface:Ljava/lang/String;

.field public static is_ehrpd_internetpdn_ipv6_blocked_by_ip6tables:Z

.field private static mSavedNeedFixZone:Z

.field private static mSavedZoneDst:Z

.field private static mSavedZoneOffset:I

.field private static mSavedZoneTime:J


# instance fields
.field protected IMSRegiOnOff:Z

.field protected alarmSwitch:Z

.field protected cm:Lcom/android/internal/telephony/CommandsInterface;

.field protected context:Landroid/content/Context;

.field protected dontPollSignalStrength:Z

.field protected filter:Landroid/content/IntentFilter;

.field protected mAttachedRegistrants:Landroid/os/RegistrantList;

.field protected final mCellInfo:Landroid/telephony/CellInfo;

.field protected mDesiredPowerState:Z

.field protected mDetachedRegistrants:Landroid/os/RegistrantList;

.field protected mEhrpdInfo:[Ljava/lang/String;

.field protected mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

.field protected mLastCellInfo:Landroid/telephony/CellInfo;

.field private mLastSignalStrength:Landroid/telephony/SignalStrength;

.field protected mLockStateRegistrants:Landroid/os/RegistrantList;

.field protected mNetworkAttachedRegistrants:Landroid/os/RegistrantList;

.field protected mNewRilRadioTechnology:I

.field protected mNoServiceChangedRegistrants:Landroid/os/RegistrantList;

.field protected mPendingRadioPowerOffAfterDataOff:Z

.field protected mPendingRadioPowerOffAfterDataOffTag:I

.field protected mPhoneBase:Lcom/android/internal/telephony/PhoneBase;

.field protected mPrevDesiredPowerState:Z

.field protected mPsRestrictDisabledRegistrants:Landroid/os/RegistrantList;

.field protected mPsRestrictEnabledRegistrants:Landroid/os/RegistrantList;

.field protected mRadioOffIntent:Landroid/app/PendingIntent;

.field protected mRequestedByPhone:Z

.field public mRestrictedState:Lcom/android/internal/telephony/RestrictedState;

.field protected mRilRadioTechnology:I

.field protected mRoamingOffRegistrants:Landroid/os/RegistrantList;

.field protected mRoamingOnRegistrants:Landroid/os/RegistrantList;

.field protected mSignalStrength:Landroid/telephony/SignalStrength;

.field protected mUiccApplcation:Lcom/android/internal/telephony/uicc/UiccCardApplication;

.field protected mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

.field private mWantContinuousLocationUpdates:Z

.field private mWantSingleLocationUpdate:Z

.field protected mlteInfo:[Ljava/lang/String;

.field protected newSS:Landroid/telephony/ServiceState;

.field protected pollingContext:[I

.field protected poweroffdelayneed:Z

.field public ss:Landroid/telephony/ServiceState;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 130
    const-string v0, "ro.debuggable"

    #@4
    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@7
    move-result v0

    #@8
    if-ne v0, v1, :cond_91

    #@a
    move v0, v1

    #@b
    :goto_b
    sput-boolean v0, Lcom/android/internal/telephony/ServiceStateTracker;->DBG_CALL:Z

    #@d
    .line 220
    sput-boolean v2, Lcom/android/internal/telephony/ServiceStateTracker;->is_ehrpd_internetpdn_ipv6_blocked_by_ip6tables:Z

    #@f
    .line 221
    const/4 v0, 0x0

    #@10
    sput-object v0, Lcom/android/internal/telephony/ServiceStateTracker;->ehrpd_ipv6_block_iface:Ljava/lang/String;

    #@12
    .line 230
    const/16 v0, 0x14

    #@14
    new-array v0, v0, [Ljava/lang/String;

    #@16
    const-string v3, "bf"

    #@18
    aput-object v3, v0, v2

    #@1a
    const-string v3, "ci"

    #@1c
    aput-object v3, v0, v1

    #@1e
    const/4 v1, 0x2

    #@1f
    const-string v3, "eh"

    #@21
    aput-object v3, v0, v1

    #@23
    const/4 v1, 0x3

    #@24
    const-string v3, "fo"

    #@26
    aput-object v3, v0, v1

    #@28
    const/4 v1, 0x4

    #@29
    const-string v3, "gb"

    #@2b
    aput-object v3, v0, v1

    #@2d
    const/4 v1, 0x5

    #@2e
    const-string v3, "gh"

    #@30
    aput-object v3, v0, v1

    #@32
    const/4 v1, 0x6

    #@33
    const-string v3, "gm"

    #@35
    aput-object v3, v0, v1

    #@37
    const/4 v1, 0x7

    #@38
    const-string v3, "gn"

    #@3a
    aput-object v3, v0, v1

    #@3c
    const/16 v1, 0x8

    #@3e
    const-string v3, "gw"

    #@40
    aput-object v3, v0, v1

    #@42
    const/16 v1, 0x9

    #@44
    const-string v3, "ie"

    #@46
    aput-object v3, v0, v1

    #@48
    const/16 v1, 0xa

    #@4a
    const-string v3, "lr"

    #@4c
    aput-object v3, v0, v1

    #@4e
    const/16 v1, 0xb

    #@50
    const-string v3, "is"

    #@52
    aput-object v3, v0, v1

    #@54
    const/16 v1, 0xc

    #@56
    const-string v3, "ma"

    #@58
    aput-object v3, v0, v1

    #@5a
    const/16 v1, 0xd

    #@5c
    const-string v3, "ml"

    #@5e
    aput-object v3, v0, v1

    #@60
    const/16 v1, 0xe

    #@62
    const-string v3, "mr"

    #@64
    aput-object v3, v0, v1

    #@66
    const/16 v1, 0xf

    #@68
    const-string v3, "pt"

    #@6a
    aput-object v3, v0, v1

    #@6c
    const/16 v1, 0x10

    #@6e
    const-string v3, "sl"

    #@70
    aput-object v3, v0, v1

    #@72
    const/16 v1, 0x11

    #@74
    const-string v3, "sn"

    #@76
    aput-object v3, v0, v1

    #@78
    const/16 v1, 0x12

    #@7a
    const-string v3, "st"

    #@7c
    aput-object v3, v0, v1

    #@7e
    const/16 v1, 0x13

    #@80
    const-string v3, "tg"

    #@82
    aput-object v3, v0, v1

    #@84
    sput-object v0, Lcom/android/internal/telephony/ServiceStateTracker;->GMT_COUNTRY_CODES:[Ljava/lang/String;

    #@86
    .line 855
    sput-boolean v2, Lcom/android/internal/telephony/ServiceStateTracker;->mSavedNeedFixZone:Z

    #@88
    .line 856
    sput v2, Lcom/android/internal/telephony/ServiceStateTracker;->mSavedZoneOffset:I

    #@8a
    .line 857
    sput-boolean v2, Lcom/android/internal/telephony/ServiceStateTracker;->mSavedZoneDst:Z

    #@8c
    .line 858
    const-wide/16 v0, 0x0

    #@8e
    sput-wide v0, Lcom/android/internal/telephony/ServiceStateTracker;->mSavedZoneTime:J

    #@90
    return-void

    #@91
    :cond_91
    move v0, v2

    #@92
    .line 130
    goto/16 :goto_b
.end method

.method protected constructor <init>(Lcom/android/internal/telephony/PhoneBase;Lcom/android/internal/telephony/CommandsInterface;Landroid/telephony/CellInfo;)V
    .registers 7
    .parameter "phoneBase"
    .parameter "ci"
    .parameter "cellInfo"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 268
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    .line 63
    iput-object v2, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@7
    .line 64
    iput-object v2, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mUiccApplcation:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@9
    .line 65
    iput-object v2, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mIccRecords:Lcom/android/internal/telephony/uicc/IccRecords;

    #@b
    .line 69
    new-instance v0, Landroid/telephony/ServiceState;

    #@d
    invoke-direct {v0}, Landroid/telephony/ServiceState;-><init>()V

    #@10
    iput-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@12
    .line 70
    new-instance v0, Landroid/telephony/ServiceState;

    #@14
    invoke-direct {v0}, Landroid/telephony/ServiceState;-><init>()V

    #@17
    iput-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@19
    .line 72
    iput-object v2, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mLastCellInfo:Landroid/telephony/CellInfo;

    #@1b
    .line 78
    new-instance v0, Landroid/telephony/SignalStrength;

    #@1d
    invoke-direct {v0}, Landroid/telephony/SignalStrength;-><init>()V

    #@20
    iput-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mSignalStrength:Landroid/telephony/SignalStrength;

    #@22
    .line 81
    new-instance v0, Lcom/android/internal/telephony/RestrictedState;

    #@24
    invoke-direct {v0}, Lcom/android/internal/telephony/RestrictedState;-><init>()V

    #@27
    iput-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mRestrictedState:Lcom/android/internal/telephony/RestrictedState;

    #@29
    .line 100
    iput v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mRilRadioTechnology:I

    #@2b
    .line 101
    iput v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mNewRilRadioTechnology:I

    #@2d
    .line 108
    iput-boolean v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->dontPollSignalStrength:Z

    #@2f
    .line 110
    new-instance v0, Landroid/os/RegistrantList;

    #@31
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@34
    iput-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mRoamingOnRegistrants:Landroid/os/RegistrantList;

    #@36
    .line 111
    new-instance v0, Landroid/os/RegistrantList;

    #@38
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@3b
    iput-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mRoamingOffRegistrants:Landroid/os/RegistrantList;

    #@3d
    .line 112
    new-instance v0, Landroid/os/RegistrantList;

    #@3f
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@42
    iput-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mAttachedRegistrants:Landroid/os/RegistrantList;

    #@44
    .line 113
    new-instance v0, Landroid/os/RegistrantList;

    #@46
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@49
    iput-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mDetachedRegistrants:Landroid/os/RegistrantList;

    #@4b
    .line 114
    new-instance v0, Landroid/os/RegistrantList;

    #@4d
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@50
    iput-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mNetworkAttachedRegistrants:Landroid/os/RegistrantList;

    #@52
    .line 115
    new-instance v0, Landroid/os/RegistrantList;

    #@54
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@57
    iput-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mPsRestrictEnabledRegistrants:Landroid/os/RegistrantList;

    #@59
    .line 116
    new-instance v0, Landroid/os/RegistrantList;

    #@5b
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@5e
    iput-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mPsRestrictDisabledRegistrants:Landroid/os/RegistrantList;

    #@60
    .line 118
    new-instance v0, Landroid/os/RegistrantList;

    #@62
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@65
    iput-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mLockStateRegistrants:Landroid/os/RegistrantList;

    #@67
    .line 121
    new-instance v0, Landroid/os/RegistrantList;

    #@69
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@6c
    iput-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mNoServiceChangedRegistrants:Landroid/os/RegistrantList;

    #@6e
    .line 125
    iput-boolean v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mPendingRadioPowerOffAfterDataOff:Z

    #@70
    .line 126
    iput v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mPendingRadioPowerOffAfterDataOffTag:I

    #@72
    .line 215
    const/4 v0, 0x4

    #@73
    new-array v0, v0, [Ljava/lang/String;

    #@75
    iput-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mlteInfo:[Ljava/lang/String;

    #@77
    .line 216
    const/4 v0, 0x2

    #@78
    new-array v0, v0, [Ljava/lang/String;

    #@7a
    iput-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mEhrpdInfo:[Ljava/lang/String;

    #@7c
    .line 260
    iput-boolean v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->IMSRegiOnOff:Z

    #@7e
    .line 261
    iput-boolean v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->alarmSwitch:Z

    #@80
    .line 262
    iput-object v2, p0, Lcom/android/internal/telephony/ServiceStateTracker;->filter:Landroid/content/IntentFilter;

    #@82
    .line 263
    iput-object v2, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mRadioOffIntent:Landroid/app/PendingIntent;

    #@84
    .line 265
    iput-boolean v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->poweroffdelayneed:Z

    #@86
    .line 285
    iput-object v2, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mLastSignalStrength:Landroid/telephony/SignalStrength;

    #@88
    .line 850
    iput-boolean v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mRequestedByPhone:Z

    #@8a
    .line 269
    iput-object p1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mPhoneBase:Lcom/android/internal/telephony/PhoneBase;

    #@8c
    .line 270
    iput-object p3, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mCellInfo:Landroid/telephony/CellInfo;

    #@8e
    .line 271
    iput-object p2, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@90
    .line 272
    invoke-static {}, Lcom/android/internal/telephony/uicc/UiccController;->getInstance()Lcom/android/internal/telephony/uicc/UiccController;

    #@93
    move-result-object v0

    #@94
    iput-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@96
    .line 273
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    #@98
    const/16 v1, 0x2a

    #@9a
    invoke-virtual {v0, p0, v1, v2}, Lcom/android/internal/telephony/uicc/UiccController;->registerForIccChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@9d
    .line 274
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@9f
    const/16 v1, 0xc

    #@a1
    invoke-interface {v0, p0, v1, v2}, Lcom/android/internal/telephony/CommandsInterface;->setOnSignalStrengthUpdate(Landroid/os/Handler;ILjava/lang/Object;)V

    #@a4
    .line 275
    return-void
.end method

.method protected static getSavedNeedFixZone()Z
    .registers 1

    #@0
    .prologue
    .line 864
    sget-boolean v0, Lcom/android/internal/telephony/ServiceStateTracker;->mSavedNeedFixZone:Z

    #@2
    return v0
.end method

.method protected static getSavedZoneDst()Z
    .registers 1

    #@0
    .prologue
    .line 876
    sget-boolean v0, Lcom/android/internal/telephony/ServiceStateTracker;->mSavedZoneDst:Z

    #@2
    return v0
.end method

.method protected static getSavedZoneOffset()I
    .registers 1

    #@0
    .prologue
    .line 870
    sget v0, Lcom/android/internal/telephony/ServiceStateTracker;->mSavedZoneOffset:I

    #@2
    return v0
.end method

.method protected static getSavedZoneTime()J
    .registers 2

    #@0
    .prologue
    .line 882
    sget-wide v0, Lcom/android/internal/telephony/ServiceStateTracker;->mSavedZoneTime:J

    #@2
    return-wide v0
.end method

.method protected static setSavedNeedFixZone(Z)V
    .registers 1
    .parameter "needFixZone"

    #@0
    .prologue
    .line 861
    sput-boolean p0, Lcom/android/internal/telephony/ServiceStateTracker;->mSavedNeedFixZone:Z

    #@2
    .line 862
    return-void
.end method

.method protected static setSavedZoneDst(Z)V
    .registers 1
    .parameter "zoneDst"

    #@0
    .prologue
    .line 873
    sput-boolean p0, Lcom/android/internal/telephony/ServiceStateTracker;->mSavedZoneDst:Z

    #@2
    .line 874
    return-void
.end method

.method protected static setSavedZoneOffset(I)V
    .registers 1
    .parameter "zoneOffset"

    #@0
    .prologue
    .line 867
    sput p0, Lcom/android/internal/telephony/ServiceStateTracker;->mSavedZoneOffset:I

    #@2
    .line 868
    return-void
.end method

.method protected static setSavedZoneTime(J)V
    .registers 2
    .parameter "zoneTime"

    #@0
    .prologue
    .line 879
    sput-wide p0, Lcom/android/internal/telephony/ServiceStateTracker;->mSavedZoneTime:J

    #@2
    .line 880
    return-void
.end method


# virtual methods
.method protected cancelPollState()V
    .registers 2

    #@0
    .prologue
    .line 736
    const/4 v0, 0x1

    #@1
    new-array v0, v0, [I

    #@3
    iput-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->pollingContext:[I

    #@5
    .line 737
    return-void
.end method

.method protected checkCorrectThread()V
    .registers 3

    #@0
    .prologue
    .line 843
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p0}, Lcom/android/internal/telephony/ServiceStateTracker;->getLooper()Landroid/os/Looper;

    #@7
    move-result-object v1

    #@8
    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    #@b
    move-result-object v1

    #@c
    if-eq v0, v1, :cond_16

    #@e
    .line 844
    new-instance v0, Ljava/lang/RuntimeException;

    #@10
    const-string v1, "ServiceStateTracker must be used from within one thread"

    #@12
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@15
    throw v0

    #@16
    .line 847
    :cond_16
    return-void
.end method

.method public disableLocationUpdates()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 414
    iput-boolean v2, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mWantContinuousLocationUpdates:Z

    #@3
    .line 415
    iget-boolean v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mWantSingleLocationUpdate:Z

    #@5
    if-nez v0, :cond_11

    #@7
    iget-boolean v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mWantContinuousLocationUpdates:Z

    #@9
    if-nez v0, :cond_11

    #@b
    .line 416
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@d
    const/4 v1, 0x0

    #@e
    invoke-interface {v0, v2, v1}, Lcom/android/internal/telephony/CommandsInterface;->setLocationUpdates(ZLandroid/os/Message;)V

    #@11
    .line 418
    :cond_11
    return-void
.end method

.method protected disableSingleLocationUpdate()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 407
    iput-boolean v2, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mWantSingleLocationUpdate:Z

    #@3
    .line 408
    iget-boolean v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mWantSingleLocationUpdate:Z

    #@5
    if-nez v0, :cond_11

    #@7
    iget-boolean v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mWantContinuousLocationUpdates:Z

    #@9
    if-nez v0, :cond_11

    #@b
    .line 409
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@d
    const/4 v1, 0x0

    #@e
    invoke-interface {v0, v2, v1}, Lcom/android/internal/telephony/CommandsInterface;->setLocationUpdates(ZLandroid/os/Message;)V

    #@11
    .line 411
    :cond_11
    return-void
.end method

.method public dispose()V
    .registers 2

    #@0
    .prologue
    .line 278
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unSetOnSignalStrengthUpdate(Landroid/os/Handler;)V

    #@5
    .line 279
    return-void
.end method

.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 6
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 820
    const-string v0, "ServiceStateTracker:"

    #@2
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@5
    .line 821
    new-instance v0, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v1, " ss="

    #@c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v0

    #@10
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@12
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v0

    #@16
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v0

    #@1a
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1d
    .line 822
    new-instance v0, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    const-string v1, " newSS="

    #@24
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v0

    #@28
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->newSS:Landroid/telephony/ServiceState;

    #@2a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v0

    #@2e
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v0

    #@32
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@35
    .line 823
    new-instance v0, Ljava/lang/StringBuilder;

    #@37
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@3a
    const-string v1, " mCellInfo="

    #@3c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v0

    #@40
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mCellInfo:Landroid/telephony/CellInfo;

    #@42
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v0

    #@46
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@49
    move-result-object v0

    #@4a
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@4d
    .line 824
    new-instance v0, Ljava/lang/StringBuilder;

    #@4f
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@52
    const-string v1, " mRestrictedState="

    #@54
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v0

    #@58
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mRestrictedState:Lcom/android/internal/telephony/RestrictedState;

    #@5a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v0

    #@5e
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@61
    move-result-object v0

    #@62
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@65
    .line 825
    new-instance v0, Ljava/lang/StringBuilder;

    #@67
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@6a
    const-string v1, " pollingContext="

    #@6c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v0

    #@70
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->pollingContext:[I

    #@72
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v0

    #@76
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@79
    move-result-object v0

    #@7a
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@7d
    .line 826
    new-instance v0, Ljava/lang/StringBuilder;

    #@7f
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@82
    const-string v1, " mDesiredPowerState="

    #@84
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v0

    #@88
    iget-boolean v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mDesiredPowerState:Z

    #@8a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@8d
    move-result-object v0

    #@8e
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@91
    move-result-object v0

    #@92
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@95
    .line 827
    new-instance v0, Ljava/lang/StringBuilder;

    #@97
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@9a
    const-string v1, " mRilRadioTechnology="

    #@9c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v0

    #@a0
    iget v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mRilRadioTechnology:I

    #@a2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v0

    #@a6
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a9
    move-result-object v0

    #@aa
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@ad
    .line 828
    new-instance v0, Ljava/lang/StringBuilder;

    #@af
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@b2
    const-string v1, " mNewRilRadioTechnology="

    #@b4
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b7
    move-result-object v0

    #@b8
    iget v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mNewRilRadioTechnology:I

    #@ba
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v0

    #@be
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c1
    move-result-object v0

    #@c2
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@c5
    .line 829
    new-instance v0, Ljava/lang/StringBuilder;

    #@c7
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@ca
    const-string v1, " dontPollSignalStrength="

    #@cc
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cf
    move-result-object v0

    #@d0
    iget-boolean v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->dontPollSignalStrength:Z

    #@d2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v0

    #@d6
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d9
    move-result-object v0

    #@da
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@dd
    .line 830
    new-instance v0, Ljava/lang/StringBuilder;

    #@df
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@e2
    const-string v1, " mPendingRadioPowerOffAfterDataOff="

    #@e4
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e7
    move-result-object v0

    #@e8
    iget-boolean v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mPendingRadioPowerOffAfterDataOff:Z

    #@ea
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@ed
    move-result-object v0

    #@ee
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f1
    move-result-object v0

    #@f2
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@f5
    .line 831
    new-instance v0, Ljava/lang/StringBuilder;

    #@f7
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@fa
    const-string v1, " mPendingRadioPowerOffAfterDataOffTag="

    #@fc
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ff
    move-result-object v0

    #@100
    iget v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mPendingRadioPowerOffAfterDataOffTag:I

    #@102
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@105
    move-result-object v0

    #@106
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@109
    move-result-object v0

    #@10a
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@10d
    .line 832
    return-void
.end method

.method public enableLocationUpdates()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 401
    iget-boolean v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mWantSingleLocationUpdate:Z

    #@3
    if-nez v0, :cond_9

    #@5
    iget-boolean v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mWantContinuousLocationUpdates:Z

    #@7
    if-eqz v0, :cond_a

    #@9
    .line 404
    :cond_9
    :goto_9
    return-void

    #@a
    .line 402
    :cond_a
    iput-boolean v2, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mWantContinuousLocationUpdates:Z

    #@c
    .line 403
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@e
    const/16 v1, 0x12

    #@10
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/ServiceStateTracker;->obtainMessage(I)Landroid/os/Message;

    #@13
    move-result-object v1

    #@14
    invoke-interface {v0, v2, v1}, Lcom/android/internal/telephony/CommandsInterface;->setLocationUpdates(ZLandroid/os/Message;)V

    #@17
    goto :goto_9
.end method

.method public enableSingleLocationUpdate()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 395
    iget-boolean v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mWantSingleLocationUpdate:Z

    #@3
    if-nez v0, :cond_9

    #@5
    iget-boolean v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mWantContinuousLocationUpdates:Z

    #@7
    if-eqz v0, :cond_a

    #@9
    .line 398
    :cond_9
    :goto_9
    return-void

    #@a
    .line 396
    :cond_a
    iput-boolean v2, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mWantSingleLocationUpdate:Z

    #@c
    .line 397
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@e
    const/16 v1, 0x12

    #@10
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/ServiceStateTracker;->obtainMessage(I)Landroid/os/Message;

    #@13
    move-result-object v1

    #@14
    invoke-interface {v0, v2, v1}, Lcom/android/internal/telephony/CommandsInterface;->setLocationUpdates(ZLandroid/os/Message;)V

    #@17
    goto :goto_9
.end method

.method public getAllCellInfo()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/telephony/CellInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 807
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public abstract getCurrentDataConnectionState()I
.end method

.method public getDesiredPowerState()Z
    .registers 2

    #@0
    .prologue
    .line 282
    iget-boolean v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mDesiredPowerState:Z

    #@2
    return v0
.end method

.method public getEhrpdInfo()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 724
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mEhrpdInfo:[Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getLteInfo()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 720
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mlteInfo:[Ljava/lang/String;

    #@2
    return-object v0
.end method

.method protected abstract getPhone()Lcom/android/internal/telephony/Phone;
.end method

.method public getSignalStrength()Landroid/telephony/SignalStrength;
    .registers 3

    #@0
    .prologue
    .line 814
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mCellInfo:Landroid/telephony/CellInfo;

    #@2
    monitor-enter v1

    #@3
    .line 815
    :try_start_3
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mSignalStrength:Landroid/telephony/SignalStrength;

    #@5
    monitor-exit v1

    #@6
    return-object v0

    #@7
    .line 816
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method public get_internetpdn_ipv6_blocked_by_ip6table()Z
    .registers 2

    #@0
    .prologue
    .line 466
    sget-boolean v0, Lcom/android/internal/telephony/ServiceStateTracker;->is_ehrpd_internetpdn_ipv6_blocked_by_ip6tables:Z

    #@2
    return v0
.end method

.method public get_internetpdn_ipv6_blocked_iface()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 470
    sget-object v0, Lcom/android/internal/telephony/ServiceStateTracker;->ehrpd_ipv6_block_iface:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 4
    .parameter "msg"

    #@0
    .prologue
    .line 422
    iget v0, p1, Landroid/os/Message;->what:I

    #@2
    sparse-switch v0, :sswitch_data_68

    #@5
    .line 443
    new-instance v0, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v1, "Unhandled message with number: "

    #@c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v0

    #@10
    iget v1, p1, Landroid/os/Message;->what:I

    #@12
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    move-result-object v0

    #@16
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v0

    #@1a
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/ServiceStateTracker;->log(Ljava/lang/String;)V

    #@1d
    .line 446
    :goto_1d
    return-void

    #@1e
    .line 424
    :sswitch_1e
    monitor-enter p0

    #@1f
    .line 425
    :try_start_1f
    iget-boolean v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mPendingRadioPowerOffAfterDataOff:Z

    #@21
    if-eqz v0, :cond_3f

    #@23
    iget v0, p1, Landroid/os/Message;->arg1:I

    #@25
    iget v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mPendingRadioPowerOffAfterDataOffTag:I

    #@27
    if-ne v0, v1, :cond_3f

    #@29
    .line 427
    const-string v0, "EVENT_SET_RADIO_OFF, turn radio off now."

    #@2b
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/ServiceStateTracker;->log(Ljava/lang/String;)V

    #@2e
    .line 428
    invoke-virtual {p0}, Lcom/android/internal/telephony/ServiceStateTracker;->hangupAndPowerOff()V

    #@31
    .line 429
    iget v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mPendingRadioPowerOffAfterDataOffTag:I

    #@33
    add-int/lit8 v0, v0, 0x1

    #@35
    iput v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mPendingRadioPowerOffAfterDataOffTag:I

    #@37
    .line 430
    const/4 v0, 0x0

    #@38
    iput-boolean v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mPendingRadioPowerOffAfterDataOff:Z

    #@3a
    .line 435
    :goto_3a
    monitor-exit p0

    #@3b
    goto :goto_1d

    #@3c
    :catchall_3c
    move-exception v0

    #@3d
    monitor-exit p0
    :try_end_3e
    .catchall {:try_start_1f .. :try_end_3e} :catchall_3c

    #@3e
    throw v0

    #@3f
    .line 432
    :cond_3f
    :try_start_3f
    new-instance v0, Ljava/lang/StringBuilder;

    #@41
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@44
    const-string v1, "EVENT_SET_RADIO_OFF is stale arg1="

    #@46
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v0

    #@4a
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@4c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v0

    #@50
    const-string v1, "!= tag="

    #@52
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v0

    #@56
    iget v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mPendingRadioPowerOffAfterDataOffTag:I

    #@58
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v0

    #@5c
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5f
    move-result-object v0

    #@60
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/ServiceStateTracker;->log(Ljava/lang/String;)V
    :try_end_63
    .catchall {:try_start_3f .. :try_end_63} :catchall_3c

    #@63
    goto :goto_3a

    #@64
    .line 439
    :sswitch_64
    invoke-virtual {p0}, Lcom/android/internal/telephony/ServiceStateTracker;->onUpdateIccAvailability()V

    #@67
    goto :goto_1d

    #@68
    .line 422
    :sswitch_data_68
    .sparse-switch
        0x26 -> :sswitch_1e
        0x2a -> :sswitch_64
    .end sparse-switch
.end method

.method protected abstract handlePollStateResult(ILandroid/os/AsyncResult;)V
.end method

.method protected abstract hangupAndPowerOff()V
.end method

.method public abstract isConcurrentVoiceAndDataAllowed()Z
.end method

.method public isIccIdChanged()Z
    .registers 3

    #@0
    .prologue
    .line 592
    const-string v0, "persist.radio.iccid-changed"

    #@2
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    const-string v1, "0"

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v0

    #@c
    if-eqz v0, :cond_10

    #@e
    .line 593
    const/4 v0, 0x0

    #@f
    .line 595
    :goto_f
    return v0

    #@10
    :cond_10
    const/4 v0, 0x1

    #@11
    goto :goto_f
.end method

.method protected abstract log(Ljava/lang/String;)V
.end method

.method protected abstract loge(Ljava/lang/String;)V
.end method

.method protected notifySignalStrength()Z
    .registers 6

    #@0
    .prologue
    .line 287
    const/4 v1, 0x0

    #@1
    .line 288
    .local v1, notified:Z
    iget-object v3, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mCellInfo:Landroid/telephony/CellInfo;

    #@3
    monitor-enter v3

    #@4
    .line 289
    :try_start_4
    iget-object v2, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mSignalStrength:Landroid/telephony/SignalStrength;

    #@6
    iget-object v4, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mLastSignalStrength:Landroid/telephony/SignalStrength;

    #@8
    invoke-virtual {v2, v4}, Landroid/telephony/SignalStrength;->equals(Ljava/lang/Object;)Z
    :try_end_b
    .catchall {:try_start_4 .. :try_end_b} :catchall_34

    #@b
    move-result v2

    #@c
    if-nez v2, :cond_14

    #@e
    .line 291
    :try_start_e
    iget-object v2, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mPhoneBase:Lcom/android/internal/telephony/PhoneBase;

    #@10
    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneBase;->notifySignalStrength()V
    :try_end_13
    .catchall {:try_start_e .. :try_end_13} :catchall_34
    .catch Ljava/lang/NullPointerException; {:try_start_e .. :try_end_13} :catch_16

    #@13
    .line 292
    const/4 v1, 0x1

    #@14
    .line 298
    :cond_14
    :goto_14
    :try_start_14
    monitor-exit v3

    #@15
    .line 299
    return v1

    #@16
    .line 293
    :catch_16
    move-exception v0

    #@17
    .line 294
    .local v0, ex:Ljava/lang/NullPointerException;
    new-instance v2, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v4, "updateSignalStrength() Phone already destroyed: "

    #@1e
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v2

    #@22
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v2

    #@26
    const-string v4, "SignalStrength not notified"

    #@28
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v2

    #@2c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v2

    #@30
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/ServiceStateTracker;->loge(Ljava/lang/String;)V

    #@33
    goto :goto_14

    #@34
    .line 298
    .end local v0           #ex:Ljava/lang/NullPointerException;
    :catchall_34
    move-exception v2

    #@35
    monitor-exit v3
    :try_end_36
    .catchall {:try_start_14 .. :try_end_36} :catchall_34

    #@36
    throw v2
.end method

.method protected onSignalStrengthResult(Landroid/os/AsyncResult;Z)Z
    .registers 7
    .parameter "ar"
    .parameter "isGsm"

    #@0
    .prologue
    const v3, 0x7fffffff

    #@3
    .line 669
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mSignalStrength:Landroid/telephony/SignalStrength;

    #@5
    .line 674
    .local v0, oldSignalStrength:Landroid/telephony/SignalStrength;
    iget-object v1, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@7
    if-nez v1, :cond_a5

    #@9
    iget-object v1, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@b
    if-eqz v1, :cond_a5

    #@d
    .line 675
    iget-object v1, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@f
    check-cast v1, Landroid/telephony/SignalStrength;

    #@11
    iput-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mSignalStrength:Landroid/telephony/SignalStrength;

    #@13
    .line 676
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mSignalStrength:Landroid/telephony/SignalStrength;

    #@15
    invoke-virtual {v1}, Landroid/telephony/SignalStrength;->validateInput()V

    #@18
    .line 677
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mSignalStrength:Landroid/telephony/SignalStrength;

    #@1a
    invoke-virtual {v1, p2}, Landroid/telephony/SignalStrength;->setGsm(Z)V

    #@1d
    .line 684
    :goto_1d
    if-eqz p2, :cond_3e

    #@1f
    .line 685
    new-instance v1, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v2, "onSignalStrengthResult() = "

    #@26
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v1

    #@2a
    iget v2, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mRilRadioTechnology:I

    #@2c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v1

    #@30
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v1

    #@34
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/ServiceStateTracker;->log(Ljava/lang/String;)V

    #@37
    .line 686
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mSignalStrength:Landroid/telephony/SignalStrength;

    #@39
    iget v2, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mRilRadioTechnology:I

    #@3b
    invoke-virtual {v1, v2}, Landroid/telephony/SignalStrength;->setRadioTechnology(I)V

    #@3e
    .line 692
    :cond_3e
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mSignalStrength:Landroid/telephony/SignalStrength;

    #@40
    invoke-static {}, Landroid/telephony/ServiceState;->getLgeRssiData()Lcom/android/internal/telephony/LgeRssiData;

    #@43
    move-result-object v2

    #@44
    invoke-virtual {v1, v2}, Landroid/telephony/SignalStrength;->setLgeRssiData(Lcom/android/internal/telephony/LgeRssiData;)V

    #@47
    .line 696
    new-instance v1, Ljava/lang/StringBuilder;

    #@49
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@4c
    const-string v2, "set ss feature feature = "

    #@4e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v1

    #@52
    iget-object v2, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@54
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@57
    move-result-object v2

    #@58
    iget v2, v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_SIGNAL_STRENTH_TYPE:I

    #@5a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v1

    #@5e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@61
    move-result-object v1

    #@62
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/ServiceStateTracker;->log(Ljava/lang/String;)V

    #@65
    .line 697
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mSignalStrength:Landroid/telephony/SignalStrength;

    #@67
    iget-object v2, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@69
    invoke-interface {v2}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@6c
    move-result-object v2

    #@6d
    iget v2, v2, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_UIAPP_SIGNAL_STRENTH_TYPE:I

    #@6f
    invoke-virtual {v1, v2}, Landroid/telephony/SignalStrength;->setfeature(I)V

    #@72
    .line 708
    const-string v1, "JP"

    #@74
    const-string v2, "DCM"

    #@76
    invoke-static {v1, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@79
    move-result v1

    #@7a
    if-eqz v1, :cond_c6

    #@7c
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@7e
    invoke-virtual {v1}, Landroid/telephony/ServiceState;->getState()I

    #@81
    move-result v1

    #@82
    if-nez v1, :cond_c6

    #@84
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mSignalStrength:Landroid/telephony/SignalStrength;

    #@86
    invoke-virtual {v1}, Landroid/telephony/SignalStrength;->getGsmSignalStrength()I

    #@89
    move-result v1

    #@8a
    const/16 v2, 0x63

    #@8c
    if-ne v1, v2, :cond_c6

    #@8e
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mSignalStrength:Landroid/telephony/SignalStrength;

    #@90
    invoke-virtual {v1}, Landroid/telephony/SignalStrength;->getLteRsrp()I

    #@93
    move-result v1

    #@94
    if-ne v1, v3, :cond_c6

    #@96
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mSignalStrength:Landroid/telephony/SignalStrength;

    #@98
    invoke-virtual {v1}, Landroid/telephony/SignalStrength;->getLteRsrq()I

    #@9b
    move-result v1

    #@9c
    if-ne v1, v3, :cond_c6

    #@9e
    .line 710
    const-string v1, "NONET_SUPP - Skip notifying signal strength"

    #@a0
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/ServiceStateTracker;->log(Ljava/lang/String;)V

    #@a3
    .line 711
    const/4 v1, 0x0

    #@a4
    .line 715
    :goto_a4
    return v1

    #@a5
    .line 679
    :cond_a5
    new-instance v1, Ljava/lang/StringBuilder;

    #@a7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@aa
    const-string v2, "onSignalStrengthResult() Exception from RIL : "

    #@ac
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@af
    move-result-object v1

    #@b0
    iget-object v2, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@b2
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v1

    #@b6
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b9
    move-result-object v1

    #@ba
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/ServiceStateTracker;->log(Ljava/lang/String;)V

    #@bd
    .line 680
    new-instance v1, Landroid/telephony/SignalStrength;

    #@bf
    invoke-direct {v1, p2}, Landroid/telephony/SignalStrength;-><init>(Z)V

    #@c2
    iput-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mSignalStrength:Landroid/telephony/SignalStrength;

    #@c4
    goto/16 :goto_1d

    #@c6
    .line 715
    :cond_c6
    invoke-virtual {p0}, Lcom/android/internal/telephony/ServiceStateTracker;->notifySignalStrength()Z

    #@c9
    move-result v1

    #@ca
    goto :goto_a4
.end method

.method protected abstract onUpdateIccAvailability()V
.end method

.method public powerOffRadioSafely(Lcom/android/internal/telephony/DataConnectionTracker;)V
    .registers 5
    .parameter "dcTracker"

    #@0
    .prologue
    .line 605
    monitor-enter p0

    #@1
    .line 606
    :try_start_1
    iget-boolean v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mPendingRadioPowerOffAfterDataOff:Z

    #@3
    if-nez v1, :cond_17

    #@5
    .line 611
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@7
    invoke-interface {v1}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@a
    move-result-object v1

    #@b
    iget-boolean v1, v1, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_AIRPLANEMODE_DETACH:Z

    #@d
    if-eqz v1, :cond_19

    #@f
    .line 612
    const-string v1, "[LGE_DATA] turn off radio right away."

    #@11
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/ServiceStateTracker;->log(Ljava/lang/String;)V

    #@14
    .line 613
    invoke-virtual {p0}, Lcom/android/internal/telephony/ServiceStateTracker;->hangupAndPowerOff()V

    #@17
    .line 641
    :cond_17
    :goto_17
    monitor-exit p0

    #@18
    .line 642
    return-void

    #@19
    .line 616
    :cond_19
    invoke-virtual {p1}, Lcom/android/internal/telephony/DataConnectionTracker;->isDisconnected()Z

    #@1c
    move-result v1

    #@1d
    if-eqz v1, :cond_30

    #@1f
    .line 618
    const-string v1, "radioTurnedOff"

    #@21
    invoke-virtual {p1, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->cleanUpAllConnections(Ljava/lang/String;)V

    #@24
    .line 619
    const-string v1, "Data disconnected, turn off radio right away."

    #@26
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/ServiceStateTracker;->log(Ljava/lang/String;)V

    #@29
    .line 620
    invoke-virtual {p0}, Lcom/android/internal/telephony/ServiceStateTracker;->hangupAndPowerOff()V

    #@2c
    goto :goto_17

    #@2d
    .line 641
    :catchall_2d
    move-exception v1

    #@2e
    monitor-exit p0
    :try_end_2f
    .catchall {:try_start_1 .. :try_end_2f} :catchall_2d

    #@2f
    throw v1

    #@30
    .line 622
    :cond_30
    :try_start_30
    const-string v1, "radioTurnedOff"

    #@32
    invoke-virtual {p1, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->cleanUpAllConnections(Ljava/lang/String;)V

    #@35
    .line 623
    invoke-static {p0}, Landroid/os/Message;->obtain(Landroid/os/Handler;)Landroid/os/Message;

    #@38
    move-result-object v0

    #@39
    .line 624
    .local v0, msg:Landroid/os/Message;
    const/16 v1, 0x26

    #@3b
    iput v1, v0, Landroid/os/Message;->what:I

    #@3d
    .line 625
    iget v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mPendingRadioPowerOffAfterDataOffTag:I

    #@3f
    add-int/lit8 v1, v1, 0x1

    #@41
    iput v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mPendingRadioPowerOffAfterDataOffTag:I

    #@43
    iput v1, v0, Landroid/os/Message;->arg1:I

    #@45
    .line 627
    invoke-static {}, Lcom/android/internal/telephony/TelephonyUtils;->isIPPhoneSupported()Z

    #@48
    move-result v1

    #@49
    if-eqz v1, :cond_5c

    #@4b
    const-wide/16 v1, 0x1388

    #@4d
    invoke-virtual {p0, v0, v1, v2}, Lcom/android/internal/telephony/ServiceStateTracker;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@50
    move-result v1

    #@51
    if-eqz v1, :cond_5c

    #@53
    .line 628
    const-string v1, "TMUS_ver: Wait upto 5s for data to disconnect, then turn off radio."

    #@55
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/ServiceStateTracker;->log(Ljava/lang/String;)V

    #@58
    .line 629
    const/4 v1, 0x1

    #@59
    iput-boolean v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mPendingRadioPowerOffAfterDataOff:Z

    #@5b
    goto :goto_17

    #@5c
    .line 632
    :cond_5c
    const-wide/16 v1, 0x7530

    #@5e
    invoke-virtual {p0, v0, v1, v2}, Lcom/android/internal/telephony/ServiceStateTracker;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@61
    move-result v1

    #@62
    if-eqz v1, :cond_6d

    #@64
    .line 633
    const-string v1, "Wait upto 30s for data to disconnect, then turn off radio."

    #@66
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/ServiceStateTracker;->log(Ljava/lang/String;)V

    #@69
    .line 634
    const/4 v1, 0x1

    #@6a
    iput-boolean v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mPendingRadioPowerOffAfterDataOff:Z

    #@6c
    goto :goto_17

    #@6d
    .line 636
    :cond_6d
    const-string v1, "Cannot send delayed Msg, turn off radio right away."

    #@6f
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/ServiceStateTracker;->log(Ljava/lang/String;)V

    #@72
    .line 637
    invoke-virtual {p0}, Lcom/android/internal/telephony/ServiceStateTracker;->hangupAndPowerOff()V
    :try_end_75
    .catchall {:try_start_30 .. :try_end_75} :catchall_2d

    #@75
    goto :goto_17
.end method

.method public processPendingRadioPowerOffAfterDataOff()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 650
    monitor-enter p0

    #@2
    .line 651
    :try_start_2
    iget-boolean v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mPendingRadioPowerOffAfterDataOff:Z

    #@4
    if-eqz v1, :cond_1a

    #@6
    .line 652
    const-string v0, "Process pending request to turn radio off."

    #@8
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/ServiceStateTracker;->log(Ljava/lang/String;)V

    #@b
    .line 653
    iget v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mPendingRadioPowerOffAfterDataOffTag:I

    #@d
    add-int/lit8 v0, v0, 0x1

    #@f
    iput v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mPendingRadioPowerOffAfterDataOffTag:I

    #@11
    .line 654
    invoke-virtual {p0}, Lcom/android/internal/telephony/ServiceStateTracker;->hangupAndPowerOff()V

    #@14
    .line 655
    const/4 v0, 0x0

    #@15
    iput-boolean v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mPendingRadioPowerOffAfterDataOff:Z

    #@17
    .line 656
    const/4 v0, 0x1

    #@18
    monitor-exit p0

    #@19
    .line 658
    :goto_19
    return v0

    #@1a
    :cond_1a
    monitor-exit p0

    #@1b
    goto :goto_19

    #@1c
    .line 659
    :catchall_1c
    move-exception v0

    #@1d
    monitor-exit p0
    :try_end_1e
    .catchall {:try_start_2 .. :try_end_1e} :catchall_1c

    #@1e
    throw v0
.end method

.method public reRegisterNetwork(Landroid/os/Message;)V
    .registers 4
    .parameter "onComplete"

    #@0
    .prologue
    .line 364
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    const/16 v1, 0x13

    #@4
    invoke-virtual {p0, v1, p1}, Lcom/android/internal/telephony/ServiceStateTracker;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@7
    move-result-object v1

    #@8
    invoke-interface {v0, v1}, Lcom/android/internal/telephony/CommandsInterface;->getPreferredNetworkType(Landroid/os/Message;)V

    #@b
    .line 366
    return-void
.end method

.method public registerForDataConnectionAttached(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 484
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 485
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mAttachedRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 487
    invoke-virtual {p0}, Lcom/android/internal/telephony/ServiceStateTracker;->getCurrentDataConnectionState()I

    #@d
    move-result v1

    #@e
    if-nez v1, :cond_13

    #@10
    .line 488
    invoke-virtual {v0}, Landroid/os/Registrant;->notifyRegistrant()V

    #@13
    .line 490
    :cond_13
    return-void
.end method

.method public registerForDataConnectionDetached(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 502
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 503
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mDetachedRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 505
    invoke-virtual {p0}, Lcom/android/internal/telephony/ServiceStateTracker;->getCurrentDataConnectionState()I

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 506
    invoke-virtual {v0}, Landroid/os/Registrant;->notifyRegistrant()V

    #@13
    .line 508
    :cond_13
    return-void
.end method

.method public registerForLockStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 346
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 347
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mLockStateRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 348
    return-void
.end method

.method public registerForNetworkAttached(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 520
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 522
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mNetworkAttachedRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 523
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@c
    invoke-virtual {v1}, Landroid/telephony/ServiceState;->getState()I

    #@f
    move-result v1

    #@10
    if-nez v1, :cond_15

    #@12
    .line 524
    invoke-virtual {v0}, Landroid/os/Registrant;->notifyRegistrant()V

    #@15
    .line 526
    :cond_15
    return-void
.end method

.method public registerForNoServiceChanged(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 571
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 572
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mNoServiceChangedRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 573
    return-void
.end method

.method public registerForPsRestrictedDisabled(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 557
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 558
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mPsRestrictDisabledRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 560
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mRestrictedState:Lcom/android/internal/telephony/RestrictedState;

    #@c
    invoke-virtual {v1}, Lcom/android/internal/telephony/RestrictedState;->isPsRestricted()Z

    #@f
    move-result v1

    #@10
    if-eqz v1, :cond_15

    #@12
    .line 561
    invoke-virtual {v0}, Landroid/os/Registrant;->notifyRegistrant()V

    #@15
    .line 563
    :cond_15
    return-void
.end method

.method public registerForPsRestrictedEnabled(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 538
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 539
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mPsRestrictEnabledRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 541
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mRestrictedState:Lcom/android/internal/telephony/RestrictedState;

    #@c
    invoke-virtual {v1}, Lcom/android/internal/telephony/RestrictedState;->isPsRestricted()Z

    #@f
    move-result v1

    #@10
    if-eqz v1, :cond_15

    #@12
    .line 542
    invoke-virtual {v0}, Landroid/os/Registrant;->notifyRegistrant()V

    #@15
    .line 544
    :cond_15
    return-void
.end method

.method public registerForRoamingOff(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 332
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 333
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mRoamingOffRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 335
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@c
    invoke-virtual {v1}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@f
    move-result v1

    #@10
    if-nez v1, :cond_15

    #@12
    .line 336
    invoke-virtual {v0}, Landroid/os/Registrant;->notifyRegistrant()V

    #@15
    .line 338
    :cond_15
    return-void
.end method

.method public registerForRoamingOn(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 311
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 312
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mRoamingOnRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 314
    iget-object v1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->ss:Landroid/telephony/ServiceState;

    #@c
    invoke-virtual {v1}, Landroid/telephony/ServiceState;->getRoaming()Z

    #@f
    move-result v1

    #@10
    if-eqz v1, :cond_15

    #@12
    .line 315
    invoke-virtual {v0}, Landroid/os/Registrant;->notifyRegistrant()V

    #@15
    .line 317
    :cond_15
    return-void
.end method

.method public sendNitzEvent(Landroid/os/AsyncResult;)V
    .registers 3
    .parameter "nitzInfo"

    #@0
    .prologue
    .line 885
    const/16 v0, 0xb

    #@2
    invoke-virtual {p0, v0, p1}, Lcom/android/internal/telephony/ServiceStateTracker;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/ServiceStateTracker;->sendMessage(Landroid/os/Message;)Z

    #@9
    .line 886
    return-void
.end method

.method public abstract setIMSRegistate(Z)V
.end method

.method protected abstract setPowerStateToDesired()V
.end method

.method public setRadioPower(Z)V
    .registers 3
    .parameter "power"

    #@0
    .prologue
    .line 371
    iget-boolean v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mDesiredPowerState:Z

    #@2
    iput-boolean v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mPrevDesiredPowerState:Z

    #@4
    .line 372
    const/4 v0, 0x1

    #@5
    iput-boolean v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mRequestedByPhone:Z

    #@7
    .line 376
    iput-boolean p1, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mDesiredPowerState:Z

    #@9
    .line 378
    invoke-virtual {p0}, Lcom/android/internal/telephony/ServiceStateTracker;->setPowerStateToDesired()V

    #@c
    .line 379
    return-void
.end method

.method public abstract set_internetpdn_ipv6_blocked_by_ip6table(Z)V
.end method

.method public abstract set_internetpdn_ipv6_blocked_iface(Ljava/lang/String;)V
.end method

.method protected shouldFixTimeZoneNow(Lcom/android/internal/telephony/PhoneBase;Ljava/lang/String;Ljava/lang/String;Z)Z
    .registers 16
    .parameter "phoneBase"
    .parameter "operatorNumeric"
    .parameter "prevOperatorNumeric"
    .parameter "needToFixTimeZone"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v8, 0x0

    #@2
    .line 758
    const/4 v9, 0x0

    #@3
    const/4 v10, 0x3

    #@4
    :try_start_4
    invoke-virtual {p2, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@7
    move-result-object v9

    #@8
    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_b} :catch_a1

    #@b
    move-result v4

    #@c
    .line 771
    .local v4, mcc:I
    const/4 v9, 0x0

    #@d
    const/4 v10, 0x3

    #@e
    :try_start_e
    invoke-virtual {p3, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@11
    move-result-object v9

    #@12
    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_15} :catch_c0

    #@15
    move-result v5

    #@16
    .line 777
    .local v5, prevMcc:I
    :goto_16
    const/4 v3, 0x0

    #@17
    .line 778
    .local v3, iccCardExist:Z
    iget-object v9, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mUiccApplcation:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@19
    if-eqz v9, :cond_26

    #@1b
    .line 779
    iget-object v9, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mUiccApplcation:Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@1d
    invoke-virtual {v9}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getState()Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;

    #@20
    move-result-object v9

    #@21
    sget-object v10, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;->APPSTATE_UNKNOWN:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppState;

    #@23
    if-eq v9, v10, :cond_c5

    #@25
    move v3, v7

    #@26
    .line 783
    :cond_26
    :goto_26
    if-eqz v3, :cond_2a

    #@28
    if-ne v4, v5, :cond_2c

    #@2a
    :cond_2a
    if-eqz p4, :cond_c8

    #@2c
    :cond_2c
    move v6, v7

    #@2d
    .line 786
    .local v6, retVal:Z
    :goto_2d
    const-string v9, "US"

    #@2f
    const-string v10, "SPR"

    #@31
    invoke-static {v9, v10}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isOperatorCountry(Ljava/lang/String;Ljava/lang/String;)Z

    #@34
    move-result v9

    #@35
    if-eqz v9, :cond_3c

    #@37
    .line 787
    if-ne v4, v5, :cond_3b

    #@39
    if-eqz p4, :cond_cb

    #@3b
    :cond_3b
    move v6, v7

    #@3c
    .line 792
    :cond_3c
    :goto_3c
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@3f
    move-result-wide v0

    #@40
    .line 793
    .local v0, ctm:J
    new-instance v7, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    const-string v8, "shouldFixTimeZoneNow: retVal="

    #@47
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v7

    #@4b
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v7

    #@4f
    const-string v8, " iccCardExist="

    #@51
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v7

    #@55
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@58
    move-result-object v7

    #@59
    const-string v8, " operatorNumeric="

    #@5b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v7

    #@5f
    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v7

    #@63
    const-string v8, " mcc="

    #@65
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v7

    #@69
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v7

    #@6d
    const-string v8, " prevOperatorNumeric="

    #@6f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v7

    #@73
    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v7

    #@77
    const-string v8, " prevMcc="

    #@79
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v7

    #@7d
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@80
    move-result-object v7

    #@81
    const-string v8, " needToFixTimeZone="

    #@83
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v7

    #@87
    invoke-virtual {v7, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v7

    #@8b
    const-string v8, " ltod="

    #@8d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v7

    #@91
    invoke-static {v0, v1}, Landroid/util/TimeUtils;->logTimeOfDay(J)Ljava/lang/String;

    #@94
    move-result-object v8

    #@95
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@98
    move-result-object v7

    #@99
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9c
    move-result-object v7

    #@9d
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/ServiceStateTracker;->log(Ljava/lang/String;)V

    #@a0
    .line 800
    .end local v0           #ctm:J
    .end local v3           #iccCardExist:Z
    .end local v4           #mcc:I
    .end local v5           #prevMcc:I
    .end local v6           #retVal:Z
    :goto_a0
    return v6

    #@a1
    .line 759
    :catch_a1
    move-exception v2

    #@a2
    .line 761
    .local v2, e:Ljava/lang/Exception;
    new-instance v7, Ljava/lang/StringBuilder;

    #@a4
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@a7
    const-string v9, "shouldFixTimeZoneNow: no mcc, operatorNumeric="

    #@a9
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v7

    #@ad
    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v7

    #@b1
    const-string v9, " retVal=false"

    #@b3
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b6
    move-result-object v7

    #@b7
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ba
    move-result-object v7

    #@bb
    invoke-virtual {p0, v7}, Lcom/android/internal/telephony/ServiceStateTracker;->log(Ljava/lang/String;)V

    #@be
    move v6, v8

    #@bf
    .line 764
    goto :goto_a0

    #@c0
    .line 772
    .end local v2           #e:Ljava/lang/Exception;
    .restart local v4       #mcc:I
    :catch_c0
    move-exception v2

    #@c1
    .line 773
    .restart local v2       #e:Ljava/lang/Exception;
    add-int/lit8 v5, v4, 0x1

    #@c3
    .restart local v5       #prevMcc:I
    goto/16 :goto_16

    #@c5
    .end local v2           #e:Ljava/lang/Exception;
    .restart local v3       #iccCardExist:Z
    :cond_c5
    move v3, v8

    #@c6
    .line 779
    goto/16 :goto_26

    #@c8
    :cond_c8
    move v6, v8

    #@c9
    .line 783
    goto/16 :goto_2d

    #@cb
    .restart local v6       #retVal:Z
    :cond_cb
    move v6, v8

    #@cc
    .line 787
    goto/16 :goto_3c
.end method

.method public unregisterForDataConnectionAttached(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 492
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mAttachedRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 493
    return-void
.end method

.method public unregisterForDataConnectionDetached(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 510
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mDetachedRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 511
    return-void
.end method

.method public unregisterForLockStateChanged(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 351
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mLockStateRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 352
    return-void
.end method

.method public unregisterForNetworkAttached(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 528
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mNetworkAttachedRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 529
    return-void
.end method

.method public unregisterForNoServiceChanged(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 576
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mNoServiceChangedRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 577
    return-void
.end method

.method public unregisterForPsRestrictedDisabled(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 566
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mPsRestrictDisabledRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 567
    return-void
.end method

.method public unregisterForPsRestrictedEnabled(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 547
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mPsRestrictEnabledRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 548
    return-void
.end method

.method public unregisterForRoamingOff(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 341
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mRoamingOffRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 342
    return-void
.end method

.method public unregisterForRoamingOn(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 320
    iget-object v0, p0, Lcom/android/internal/telephony/ServiceStateTracker;->mRoamingOnRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 321
    return-void
.end method

.method protected abstract updateSpnDisplay()V
.end method
