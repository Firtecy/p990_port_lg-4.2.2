.class public Lcom/android/internal/telephony/uicc/UiccCard;
.super Ljava/lang/Object;
.source "UiccCard.java"


# static fields
.field protected static final DBG:Z = true

.field private static final EVENT_CARD_ADDED:I = 0xe

.field private static final EVENT_CARD_REMOVED:I = 0xd

.field private static final EVENT_PHONE_REBOOT:I = 0xf

.field protected static final LOG_TAG:Ljava/lang/String; = "RIL_UiccCard"


# instance fields
.field private mAbsentRegistrants:Landroid/os/RegistrantList;

.field protected mCardState:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

.field protected mCatService:Lcom/android/internal/telephony/cat/CatService;

.field private mCdmaSubscriptionAppIndex:I

.field protected mCi:Lcom/android/internal/telephony/CommandsInterface;

.field protected mContext:Landroid/content/Context;

.field private mDestroyed:Z

.field private mGsmUmtsSubscriptionAppIndex:I

.field protected mHandler:Landroid/os/Handler;

.field private mImsSubscriptionAppIndex:I

.field private mLastRadioState:Lcom/android/internal/telephony/CommandsInterface$RadioState;

.field private final mLock:Ljava/lang/Object;

.field mTimeoutTimer:Ljava/util/Timer;

.field protected mUiccApplications:[Lcom/android/internal/telephony/uicc/UiccCardApplication;

.field private mUniversalPinState:Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;


# direct methods
.method protected constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 106
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 77
    new-instance v0, Ljava/lang/Object;

    #@5
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mLock:Ljava/lang/Object;

    #@a
    .line 83
    const/16 v0, 0x8

    #@c
    new-array v0, v0, [Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@e
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mUiccApplications:[Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@10
    .line 88
    const/4 v0, 0x0

    #@11
    iput-boolean v0, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mDestroyed:Z

    #@13
    .line 89
    sget-object v0, Lcom/android/internal/telephony/CommandsInterface$RadioState;->RADIO_UNAVAILABLE:Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@15
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mLastRadioState:Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@17
    .line 91
    new-instance v0, Landroid/os/RegistrantList;

    #@19
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@1c
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mAbsentRegistrants:Landroid/os/RegistrantList;

    #@1e
    .line 461
    new-instance v0, Lcom/android/internal/telephony/uicc/UiccCard$1;

    #@20
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/uicc/UiccCard$1;-><init>(Lcom/android/internal/telephony/uicc/UiccCard;)V

    #@23
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mHandler:Landroid/os/Handler;

    #@25
    .line 107
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;Lcom/android/internal/telephony/uicc/IccCardStatus;)V
    .registers 5
    .parameter "c"
    .parameter "ci"
    .parameter "ics"

    #@0
    .prologue
    .line 100
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 77
    new-instance v0, Ljava/lang/Object;

    #@5
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mLock:Ljava/lang/Object;

    #@a
    .line 83
    const/16 v0, 0x8

    #@c
    new-array v0, v0, [Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@e
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mUiccApplications:[Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@10
    .line 88
    const/4 v0, 0x0

    #@11
    iput-boolean v0, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mDestroyed:Z

    #@13
    .line 89
    sget-object v0, Lcom/android/internal/telephony/CommandsInterface$RadioState;->RADIO_UNAVAILABLE:Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@15
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mLastRadioState:Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@17
    .line 91
    new-instance v0, Landroid/os/RegistrantList;

    #@19
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@1c
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mAbsentRegistrants:Landroid/os/RegistrantList;

    #@1e
    .line 461
    new-instance v0, Lcom/android/internal/telephony/uicc/UiccCard$1;

    #@20
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/uicc/UiccCard$1;-><init>(Lcom/android/internal/telephony/uicc/UiccCard;)V

    #@23
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mHandler:Landroid/os/Handler;

    #@25
    .line 101
    const-string v0, "Creating"

    #@27
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/uicc/UiccCard;->log(Ljava/lang/String;)V

    #@2a
    .line 102
    iget-object v0, p3, Lcom/android/internal/telephony/uicc/IccCardStatus;->mCardState:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    #@2c
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mCardState:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    #@2e
    .line 103
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/internal/telephony/uicc/UiccCard;->update(Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;Lcom/android/internal/telephony/uicc/IccCardStatus;)V

    #@31
    .line 104
    return-void
.end method

.method static synthetic access$000(Lcom/android/internal/telephony/uicc/UiccCard;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mDestroyed:Z

    #@2
    return v0
.end method

.method static synthetic access$100(Lcom/android/internal/telephony/uicc/UiccCard;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/uicc/UiccCard;->loge(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$200(Lcom/android/internal/telephony/uicc/UiccCard;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/uicc/UiccCard;->onIccSwap(Z)V

    #@3
    return-void
.end method

.method static synthetic access$300(Lcom/android/internal/telephony/uicc/UiccCard;JLandroid/app/ProgressDialog;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 73
    invoke-direct {p0, p1, p2, p3}, Lcom/android/internal/telephony/uicc/UiccCard;->registerRebootTimer(JLandroid/app/ProgressDialog;)V

    #@3
    return-void
.end method

.method private cancelRebootTimer()V
    .registers 2

    #@0
    .prologue
    .line 621
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mTimeoutTimer:Ljava/util/Timer;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 622
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mTimeoutTimer:Ljava/util/Timer;

    #@6
    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    #@9
    .line 623
    :cond_9
    return-void
.end method

.method private checkIndex(ILcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;)I
    .registers 7
    .parameter "index"
    .parameter "expectedAppType"
    .parameter "altExpectedAppType"

    #@0
    .prologue
    const/4 v0, -0x1

    #@1
    .line 257
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mUiccApplications:[Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@3
    if-eqz v1, :cond_a

    #@5
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mUiccApplications:[Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@7
    array-length v1, v1

    #@8
    if-lt p1, v1, :cond_28

    #@a
    .line 258
    :cond_a
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v2, "App index "

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    const-string v2, " is invalid since there are no applications"

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/uicc/UiccCard;->loge(Ljava/lang/String;)V

    #@26
    move p1, v0

    #@27
    .line 275
    .end local p1
    :cond_27
    :goto_27
    return p1

    #@28
    .line 262
    .restart local p1
    :cond_28
    if-gez p1, :cond_2c

    #@2a
    move p1, v0

    #@2b
    .line 264
    goto :goto_27

    #@2c
    .line 267
    :cond_2c
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mUiccApplications:[Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@2e
    aget-object v1, v1, p1

    #@30
    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getType()Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;

    #@33
    move-result-object v1

    #@34
    if-eq v1, p2, :cond_27

    #@36
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mUiccApplications:[Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@38
    aget-object v1, v1, p1

    #@3a
    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getType()Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;

    #@3d
    move-result-object v1

    #@3e
    if-eq v1, p3, :cond_27

    #@40
    .line 269
    new-instance v1, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    const-string v2, "App index "

    #@47
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v1

    #@4b
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v1

    #@4f
    const-string v2, " is invalid since it\'s not "

    #@51
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v1

    #@55
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v1

    #@59
    const-string v2, " and not "

    #@5b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v1

    #@5f
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v1

    #@63
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@66
    move-result-object v1

    #@67
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/uicc/UiccCard;->loge(Ljava/lang/String;)V

    #@6a
    move p1, v0

    #@6b
    .line 271
    goto :goto_27
.end method

.method private loge(Ljava/lang/String;)V
    .registers 3
    .parameter "msg"

    #@0
    .prologue
    .line 606
    const-string v0, "RIL_UiccCard"

    #@2
    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 607
    return-void
.end method

.method private onIccSwap(Z)V
    .registers 4
    .parameter "isAdded"

    #@0
    .prologue
    .line 302
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "Empty Method onIccSwap :isAdded( "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    const-string v1, ")"

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/uicc/UiccCard;->loge(Ljava/lang/String;)V

    #@1c
    .line 303
    return-void
.end method

.method private registerRebootTimer(JLandroid/app/ProgressDialog;)V
    .registers 7
    .parameter "delay"
    .parameter "dialog"

    #@0
    .prologue
    .line 610
    move-object v0, p3

    #@1
    .line 612
    .local v0, progressDialog:Landroid/app/ProgressDialog;
    new-instance v1, Ljava/util/Timer;

    #@3
    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    #@6
    iput-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mTimeoutTimer:Ljava/util/Timer;

    #@8
    .line 613
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mTimeoutTimer:Ljava/util/Timer;

    #@a
    new-instance v2, Lcom/android/internal/telephony/uicc/UiccCard$2;

    #@c
    invoke-direct {v2, p0, v0}, Lcom/android/internal/telephony/uicc/UiccCard$2;-><init>(Lcom/android/internal/telephony/uicc/UiccCard;Landroid/app/ProgressDialog;)V

    #@f
    invoke-virtual {v1, v2, p1, p2}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    #@12
    .line 618
    return-void
.end method

.method private sanitizeApplicationIndexes()V
    .registers 4

    #@0
    .prologue
    .line 248
    iget v0, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mGsmUmtsSubscriptionAppIndex:I

    #@2
    sget-object v1, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;->APPTYPE_SIM:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;

    #@4
    sget-object v2, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;->APPTYPE_USIM:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;

    #@6
    invoke-direct {p0, v0, v1, v2}, Lcom/android/internal/telephony/uicc/UiccCard;->checkIndex(ILcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;)I

    #@9
    move-result v0

    #@a
    iput v0, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mGsmUmtsSubscriptionAppIndex:I

    #@c
    .line 250
    iget v0, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mCdmaSubscriptionAppIndex:I

    #@e
    sget-object v1, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;->APPTYPE_RUIM:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;

    #@10
    sget-object v2, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;->APPTYPE_CSIM:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;

    #@12
    invoke-direct {p0, v0, v1, v2}, Lcom/android/internal/telephony/uicc/UiccCard;->checkIndex(ILcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;)I

    #@15
    move-result v0

    #@16
    iput v0, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mCdmaSubscriptionAppIndex:I

    #@18
    .line 252
    iget v0, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mImsSubscriptionAppIndex:I

    #@1a
    sget-object v1, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;->APPTYPE_ISIM:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;

    #@1c
    const/4 v2, 0x0

    #@1d
    invoke-direct {p0, v0, v1, v2}, Lcom/android/internal/telephony/uicc/UiccCard;->checkIndex(ILcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;)I

    #@20
    move-result v0

    #@21
    iput v0, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mImsSubscriptionAppIndex:I

    #@23
    .line 254
    return-void
.end method


# virtual methods
.method protected createAndUpdateCatService()V
    .registers 3

    #@0
    .prologue
    .line 226
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mUiccApplications:[Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@2
    array-length v0, v0

    #@3
    if-lez v0, :cond_1b

    #@5
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mUiccApplications:[Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@7
    const/4 v1, 0x0

    #@8
    aget-object v0, v0, v1

    #@a
    if-eqz v0, :cond_1b

    #@c
    .line 227
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mCatService:Lcom/android/internal/telephony/cat/CatService;

    #@e
    if-nez v0, :cond_1a

    #@10
    .line 229
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@12
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mContext:Landroid/content/Context;

    #@14
    invoke-static {v0, v1, p0}, Lcom/android/internal/telephony/cat/CatService;->getInstance(Lcom/android/internal/telephony/CommandsInterface;Landroid/content/Context;Lcom/android/internal/telephony/uicc/UiccCard;)Lcom/android/internal/telephony/cat/CatService;

    #@17
    move-result-object v0

    #@18
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mCatService:Lcom/android/internal/telephony/cat/CatService;

    #@1a
    .line 236
    :cond_1a
    :goto_1a
    return-void

    #@1b
    .line 231
    :cond_1b
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mCatService:Lcom/android/internal/telephony/cat/CatService;

    #@1d
    if-eqz v0, :cond_24

    #@1f
    .line 232
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mCatService:Lcom/android/internal/telephony/cat/CatService;

    #@21
    invoke-virtual {v0}, Lcom/android/internal/telephony/cat/CatService;->dispose()V

    #@24
    .line 234
    :cond_24
    const/4 v0, 0x0

    #@25
    iput-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mCatService:Lcom/android/internal/telephony/cat/CatService;

    #@27
    goto :goto_1a
.end method

.method public dispose()V
    .registers 7

    #@0
    .prologue
    .line 110
    iget-object v5, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v5

    #@3
    .line 111
    :try_start_3
    const-string v4, "Disposing card"

    #@5
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/uicc/UiccCard;->log(Ljava/lang/String;)V

    #@8
    .line 112
    iget-object v4, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mCatService:Lcom/android/internal/telephony/cat/CatService;

    #@a
    if-eqz v4, :cond_11

    #@c
    iget-object v4, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mCatService:Lcom/android/internal/telephony/cat/CatService;

    #@e
    invoke-virtual {v4}, Lcom/android/internal/telephony/cat/CatService;->dispose()V

    #@11
    .line 113
    :cond_11
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mUiccApplications:[Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@13
    .local v1, arr$:[Lcom/android/internal/telephony/uicc/UiccCardApplication;
    array-length v3, v1

    #@14
    .local v3, len$:I
    const/4 v2, 0x0

    #@15
    .local v2, i$:I
    :goto_15
    if-ge v2, v3, :cond_21

    #@17
    aget-object v0, v1, v2

    #@19
    .line 114
    .local v0, app:Lcom/android/internal/telephony/uicc/UiccCardApplication;
    if-eqz v0, :cond_1e

    #@1b
    .line 115
    invoke-virtual {v0}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->dispose()V

    #@1e
    .line 113
    :cond_1e
    add-int/lit8 v2, v2, 0x1

    #@20
    goto :goto_15

    #@21
    .line 118
    .end local v0           #app:Lcom/android/internal/telephony/uicc/UiccCardApplication;
    :cond_21
    const/4 v4, 0x0

    #@22
    iput-object v4, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mCatService:Lcom/android/internal/telephony/cat/CatService;

    #@24
    .line 119
    const/4 v4, 0x0

    #@25
    iput-object v4, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mUiccApplications:[Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@27
    .line 120
    monitor-exit v5

    #@28
    .line 121
    return-void

    #@29
    .line 120
    .end local v1           #arr$:[Lcom/android/internal/telephony/uicc/UiccCardApplication;
    .end local v2           #i$:I
    .end local v3           #len$:I
    :catchall_29
    move-exception v4

    #@2a
    monitor-exit v5
    :try_end_2b
    .catchall {:try_start_3 .. :try_end_2b} :catchall_29

    #@2b
    throw v4
.end method

.method protected finalize()V
    .registers 2

    #@0
    .prologue
    .line 239
    const-string v0, "UiccCard finalized"

    #@2
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/uicc/UiccCard;->log(Ljava/lang/String;)V

    #@5
    .line 240
    return-void
.end method

.method public getApplication(I)Lcom/android/internal/telephony/uicc/UiccCardApplication;
    .registers 5
    .parameter "family"

    #@0
    .prologue
    .line 550
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 551
    const/16 v0, 0x8

    #@5
    .line 552
    .local v0, index:I
    packed-switch p1, :pswitch_data_24

    #@8
    .line 574
    :goto_8
    if-ltz v0, :cond_1e

    #@a
    :try_start_a
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mUiccApplications:[Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@c
    array-length v1, v1

    #@d
    if-ge v0, v1, :cond_1e

    #@f
    .line 575
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mUiccApplications:[Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@11
    aget-object v1, v1, v0

    #@13
    monitor-exit v2

    #@14
    .line 577
    :goto_14
    return-object v1

    #@15
    .line 554
    :pswitch_15
    iget v0, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mGsmUmtsSubscriptionAppIndex:I

    #@17
    .line 555
    goto :goto_8

    #@18
    .line 557
    :pswitch_18
    iget v0, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mCdmaSubscriptionAppIndex:I

    #@1a
    .line 558
    goto :goto_8

    #@1b
    .line 560
    :pswitch_1b
    iget v0, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mImsSubscriptionAppIndex:I

    #@1d
    goto :goto_8

    #@1e
    .line 577
    :cond_1e
    const/4 v1, 0x0

    #@1f
    monitor-exit v2

    #@20
    goto :goto_14

    #@21
    .line 578
    :catchall_21
    move-exception v1

    #@22
    monitor-exit v2
    :try_end_23
    .catchall {:try_start_a .. :try_end_23} :catchall_21

    #@23
    throw v1

    #@24
    .line 552
    :pswitch_data_24
    .packed-switch 0x1
        :pswitch_15
        :pswitch_18
        :pswitch_1b
    .end packed-switch
.end method

.method public getApplicationIndex(I)Lcom/android/internal/telephony/uicc/UiccCardApplication;
    .registers 4
    .parameter "index"

    #@0
    .prologue
    .line 582
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 583
    if-ltz p1, :cond_10

    #@5
    :try_start_5
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mUiccApplications:[Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@7
    array-length v0, v0

    #@8
    if-ge p1, v0, :cond_10

    #@a
    .line 584
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mUiccApplications:[Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@c
    aget-object v0, v0, p1

    #@e
    monitor-exit v1

    #@f
    .line 586
    :goto_f
    return-object v0

    #@10
    :cond_10
    const/4 v0, 0x0

    #@11
    monitor-exit v1

    #@12
    goto :goto_f

    #@13
    .line 587
    :catchall_13
    move-exception v0

    #@14
    monitor-exit v1
    :try_end_15
    .catchall {:try_start_5 .. :try_end_15} :catchall_13

    #@15
    throw v0
.end method

.method public getCardState()Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;
    .registers 3

    #@0
    .prologue
    .line 538
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 539
    :try_start_3
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mCardState:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    #@5
    monitor-exit v1

    #@6
    return-object v0

    #@7
    .line 540
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method public getNumApplications()I
    .registers 6

    #@0
    .prologue
    .line 592
    const/4 v2, 0x0

    #@1
    .line 593
    .local v2, count:I
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mUiccApplications:[Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@3
    .local v1, arr$:[Lcom/android/internal/telephony/uicc/UiccCardApplication;
    array-length v4, v1

    #@4
    .local v4, len$:I
    const/4 v3, 0x0

    #@5
    .local v3, i$:I
    :goto_5
    if-ge v3, v4, :cond_10

    #@7
    aget-object v0, v1, v3

    #@9
    .line 594
    .local v0, a:Lcom/android/internal/telephony/uicc/UiccCardApplication;
    if-eqz v0, :cond_d

    #@b
    .line 595
    add-int/lit8 v2, v2, 0x1

    #@d
    .line 593
    :cond_d
    add-int/lit8 v3, v3, 0x1

    #@f
    goto :goto_5

    #@10
    .line 598
    .end local v0           #a:Lcom/android/internal/telephony/uicc/UiccCardApplication;
    :cond_10
    return v2
.end method

.method public getUniversalPinState()Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;
    .registers 3

    #@0
    .prologue
    .line 544
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 545
    :try_start_3
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mUniversalPinState:Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;

    #@5
    monitor-exit v1

    #@6
    return-object v0

    #@7
    .line 546
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method public isApplicationOnIcc(Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;)Z
    .registers 6
    .parameter "type"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 524
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mLock:Ljava/lang/Object;

    #@3
    monitor-enter v2

    #@4
    .line 526
    :try_start_4
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mUiccApplications:[Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@6
    if-nez v3, :cond_a

    #@8
    monitor-exit v2

    #@9
    .line 533
    :goto_9
    return v1

    #@a
    .line 528
    :cond_a
    const/4 v0, 0x0

    #@b
    .local v0, i:I
    :goto_b
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mUiccApplications:[Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@d
    array-length v3, v3

    #@e
    if-ge v0, v3, :cond_29

    #@10
    .line 529
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mUiccApplications:[Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@12
    aget-object v3, v3, v0

    #@14
    if-eqz v3, :cond_26

    #@16
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mUiccApplications:[Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@18
    aget-object v3, v3, v0

    #@1a
    invoke-virtual {v3}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getType()Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;

    #@1d
    move-result-object v3

    #@1e
    if-ne v3, p1, :cond_26

    #@20
    .line 530
    const/4 v1, 0x1

    #@21
    monitor-exit v2

    #@22
    goto :goto_9

    #@23
    .line 534
    .end local v0           #i:I
    :catchall_23
    move-exception v1

    #@24
    monitor-exit v2
    :try_end_25
    .catchall {:try_start_4 .. :try_end_25} :catchall_23

    #@25
    throw v1

    #@26
    .line 528
    .restart local v0       #i:I
    :cond_26
    add-int/lit8 v0, v0, 0x1

    #@28
    goto :goto_b

    #@29
    .line 533
    :cond_29
    :try_start_29
    monitor-exit v2
    :try_end_2a
    .catchall {:try_start_29 .. :try_end_2a} :catchall_23

    #@2a
    goto :goto_9
.end method

.method protected log(Ljava/lang/String;)V
    .registers 3
    .parameter "msg"

    #@0
    .prologue
    .line 602
    const-string v0, "RIL_UiccCard"

    #@2
    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 603
    return-void
.end method

.method public registerForAbsent(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 8
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 282
    iget-object v2, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 283
    :try_start_3
    new-instance v0, Landroid/os/Registrant;

    #@5
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@8
    .line 285
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mAbsentRegistrants:Landroid/os/RegistrantList;

    #@a
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@d
    .line 287
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mCardState:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    #@f
    sget-object v3, Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;->CARDSTATE_ABSENT:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    #@11
    if-ne v1, v3, :cond_16

    #@13
    .line 288
    invoke-virtual {v0}, Landroid/os/Registrant;->notifyRegistrant()V

    #@16
    .line 290
    :cond_16
    monitor-exit v2

    #@17
    .line 291
    return-void

    #@18
    .line 290
    .end local v0           #r:Landroid/os/Registrant;
    :catchall_18
    move-exception v1

    #@19
    monitor-exit v2
    :try_end_1a
    .catchall {:try_start_3 .. :try_end_1a} :catchall_18

    #@1a
    throw v1
.end method

.method public unregisterForAbsent(Landroid/os/Handler;)V
    .registers 4
    .parameter "h"

    #@0
    .prologue
    .line 294
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 295
    :try_start_3
    iget-object v0, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mAbsentRegistrants:Landroid/os/RegistrantList;

    #@5
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@8
    .line 296
    monitor-exit v1

    #@9
    .line 297
    return-void

    #@a
    .line 296
    :catchall_a
    move-exception v0

    #@b
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method

.method public update(Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;Lcom/android/internal/telephony/uicc/IccCardStatus;)V
    .registers 13
    .parameter "c"
    .parameter "ci"
    .parameter "ics"

    #@0
    .prologue
    .line 124
    iget-object v4, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v4

    #@3
    .line 125
    :try_start_3
    iget-boolean v3, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mDestroyed:Z

    #@5
    if-eqz v3, :cond_e

    #@7
    .line 126
    const-string v3, "Updated after destroyed! Fix me!"

    #@9
    invoke-direct {p0, v3}, Lcom/android/internal/telephony/uicc/UiccCard;->loge(Ljava/lang/String;)V

    #@c
    .line 127
    monitor-exit v4

    #@d
    .line 223
    :goto_d
    return-void

    #@e
    .line 129
    :cond_e
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mCardState:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    #@10
    .line 130
    .local v1, oldState:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;
    iget-object v3, p3, Lcom/android/internal/telephony/uicc/IccCardStatus;->mCardState:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    #@12
    iput-object v3, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mCardState:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    #@14
    .line 131
    iget-object v3, p3, Lcom/android/internal/telephony/uicc/IccCardStatus;->mUniversalPinState:Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;

    #@16
    iput-object v3, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mUniversalPinState:Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;

    #@18
    .line 132
    iget v3, p3, Lcom/android/internal/telephony/uicc/IccCardStatus;->mGsmUmtsSubscriptionAppIndex:I

    #@1a
    iput v3, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mGsmUmtsSubscriptionAppIndex:I

    #@1c
    .line 133
    iget v3, p3, Lcom/android/internal/telephony/uicc/IccCardStatus;->mCdmaSubscriptionAppIndex:I

    #@1e
    iput v3, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mCdmaSubscriptionAppIndex:I

    #@20
    .line 135
    iget v3, p3, Lcom/android/internal/telephony/uicc/IccCardStatus;->mImsSubscriptionAppIndex:I

    #@22
    iput v3, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mImsSubscriptionAppIndex:I

    #@24
    .line 136
    new-instance v3, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v5, "[ISIM] mImsSubscriptionAppIndex = "

    #@2b
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v3

    #@2f
    iget v5, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mImsSubscriptionAppIndex:I

    #@31
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@34
    move-result-object v3

    #@35
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v3

    #@39
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/uicc/UiccCard;->log(Ljava/lang/String;)V

    #@3c
    .line 138
    iput-object p1, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mContext:Landroid/content/Context;

    #@3e
    .line 139
    iput-object p2, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@40
    .line 141
    new-instance v3, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    iget-object v5, p3, Lcom/android/internal/telephony/uicc/IccCardStatus;->mApplications:[Lcom/android/internal/telephony/uicc/IccCardApplicationStatus;

    #@47
    array-length v5, v5

    #@48
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v3

    #@4c
    const-string v5, " applications"

    #@4e
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v3

    #@52
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@55
    move-result-object v3

    #@56
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/uicc/UiccCard;->log(Ljava/lang/String;)V

    #@59
    .line 142
    const/4 v0, 0x0

    #@5a
    .local v0, i:I
    :goto_5a
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mUiccApplications:[Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@5c
    array-length v3, v3

    #@5d
    if-ge v0, v3, :cond_a3

    #@5f
    .line 143
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mUiccApplications:[Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@61
    aget-object v3, v3, v0

    #@63
    if-nez v3, :cond_7e

    #@65
    .line 145
    iget-object v3, p3, Lcom/android/internal/telephony/uicc/IccCardStatus;->mApplications:[Lcom/android/internal/telephony/uicc/IccCardApplicationStatus;

    #@67
    array-length v3, v3

    #@68
    if-ge v0, v3, :cond_7b

    #@6a
    .line 146
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mUiccApplications:[Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@6c
    new-instance v5, Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@6e
    iget-object v6, p3, Lcom/android/internal/telephony/uicc/IccCardStatus;->mApplications:[Lcom/android/internal/telephony/uicc/IccCardApplicationStatus;

    #@70
    aget-object v6, v6, v0

    #@72
    iget-object v7, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mContext:Landroid/content/Context;

    #@74
    iget-object v8, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@76
    invoke-direct {v5, p0, v6, v7, v8}, Lcom/android/internal/telephony/uicc/UiccCardApplication;-><init>(Lcom/android/internal/telephony/uicc/UiccCard;Lcom/android/internal/telephony/uicc/IccCardApplicationStatus;Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;)V

    #@79
    aput-object v5, v3, v0

    #@7b
    .line 142
    :cond_7b
    :goto_7b
    add-int/lit8 v0, v0, 0x1

    #@7d
    goto :goto_5a

    #@7e
    .line 149
    :cond_7e
    iget-object v3, p3, Lcom/android/internal/telephony/uicc/IccCardStatus;->mApplications:[Lcom/android/internal/telephony/uicc/IccCardApplicationStatus;

    #@80
    array-length v3, v3

    #@81
    if-lt v0, v3, :cond_93

    #@83
    .line 151
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mUiccApplications:[Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@85
    aget-object v3, v3, v0

    #@87
    invoke-virtual {v3}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->dispose()V

    #@8a
    .line 152
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mUiccApplications:[Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@8c
    const/4 v5, 0x0

    #@8d
    aput-object v5, v3, v0

    #@8f
    goto :goto_7b

    #@90
    .line 222
    .end local v0           #i:I
    .end local v1           #oldState:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;
    :catchall_90
    move-exception v3

    #@91
    monitor-exit v4
    :try_end_92
    .catchall {:try_start_3 .. :try_end_92} :catchall_90

    #@92
    throw v3

    #@93
    .line 155
    .restart local v0       #i:I
    .restart local v1       #oldState:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;
    :cond_93
    :try_start_93
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mUiccApplications:[Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@95
    aget-object v3, v3, v0

    #@97
    iget-object v5, p3, Lcom/android/internal/telephony/uicc/IccCardStatus;->mApplications:[Lcom/android/internal/telephony/uicc/IccCardApplicationStatus;

    #@99
    aget-object v5, v5, v0

    #@9b
    iget-object v6, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mContext:Landroid/content/Context;

    #@9d
    iget-object v7, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@9f
    invoke-virtual {v3, v5, v6, v7}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->update(Lcom/android/internal/telephony/uicc/IccCardApplicationStatus;Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;)V

    #@a2
    goto :goto_7b

    #@a3
    .line 159
    :cond_a3
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/UiccCard;->createAndUpdateCatService()V

    #@a6
    .line 160
    invoke-direct {p0}, Lcom/android/internal/telephony/uicc/UiccCard;->sanitizeApplicationIndexes()V

    #@a9
    .line 162
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    #@ab
    invoke-interface {v3}, Lcom/android/internal/telephony/CommandsInterface;->getRadioState()Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@ae
    move-result-object v2

    #@af
    .line 163
    .local v2, radioState:Lcom/android/internal/telephony/CommandsInterface$RadioState;
    new-instance v3, Ljava/lang/StringBuilder;

    #@b1
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@b4
    const-string v5, "update: radioState="

    #@b6
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b9
    move-result-object v3

    #@ba
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v3

    #@be
    const-string v5, " mLastRadioState="

    #@c0
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v3

    #@c4
    iget-object v5, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mLastRadioState:Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@c6
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c9
    move-result-object v3

    #@ca
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cd
    move-result-object v3

    #@ce
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/uicc/UiccCard;->log(Ljava/lang/String;)V

    #@d1
    .line 167
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mCardState:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    #@d3
    sget-object v5, Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;->CARDSTATE_SIM_DETECT_INSERTED:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    #@d5
    if-ne v3, v5, :cond_100

    #@d7
    .line 169
    new-instance v3, Ljava/lang/StringBuilder;

    #@d9
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@dc
    const-string v5, "[lge_UICC] card detect inserted!  : "

    #@de
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e1
    move-result-object v3

    #@e2
    iget-object v5, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mCardState:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    #@e4
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@e7
    move-result-object v3

    #@e8
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@eb
    move-result-object v3

    #@ec
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/uicc/UiccCard;->log(Ljava/lang/String;)V

    #@ef
    .line 170
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mHandler:Landroid/os/Handler;

    #@f1
    iget-object v5, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mHandler:Landroid/os/Handler;

    #@f3
    const/16 v6, 0xe

    #@f5
    const/4 v7, 0x0

    #@f6
    invoke-virtual {v5, v6, v7}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@f9
    move-result-object v5

    #@fa
    invoke-virtual {v3, v5}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@fd
    .line 171
    monitor-exit v4

    #@fe
    goto/16 :goto_d

    #@100
    .line 176
    :cond_100
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mCardState:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    #@102
    sget-object v5, Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;->CARDSTATE_REMOVED:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    #@104
    if-eq v3, v5, :cond_10c

    #@106
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mCardState:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    #@108
    sget-object v5, Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;->CARDSTATE_ABSENT:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    #@10a
    if-ne v3, v5, :cond_139

    #@10c
    .line 179
    :cond_10c
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mCardState:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    #@10e
    sget-object v5, Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;->CARDSTATE_ABSENT:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    #@110
    if-ne v3, v5, :cond_13e

    #@112
    invoke-virtual {p0}, Lcom/android/internal/telephony/uicc/UiccCard;->getNumApplications()I

    #@115
    move-result v3

    #@116
    if-eqz v3, :cond_13e

    #@118
    .line 181
    const-string v3, "[LGE_UICC] When ME power off, mAbsentRegistrants didin\'t notify"

    #@11a
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/uicc/UiccCard;->log(Ljava/lang/String;)V

    #@11d
    .line 192
    :goto_11d
    sget-object v3, Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;->CARDSTATE_REMOVED:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    #@11f
    if-eq v1, v3, :cond_139

    #@121
    sget-object v3, Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;->CARDSTATE_ABSENT:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    #@123
    if-eq v1, v3, :cond_139

    #@125
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mCardState:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    #@127
    sget-object v5, Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;->CARDSTATE_REMOVED:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    #@129
    if-ne v3, v5, :cond_139

    #@12b
    .line 194
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mHandler:Landroid/os/Handler;

    #@12d
    iget-object v5, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mHandler:Landroid/os/Handler;

    #@12f
    const/16 v6, 0xd

    #@131
    const/4 v7, 0x0

    #@132
    invoke-virtual {v5, v6, v7}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@135
    move-result-object v5

    #@136
    invoke-virtual {v3, v5}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@139
    .line 221
    :cond_139
    iput-object v2, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mLastRadioState:Lcom/android/internal/telephony/CommandsInterface$RadioState;

    #@13b
    .line 222
    monitor-exit v4

    #@13c
    goto/16 :goto_d

    #@13e
    .line 186
    :cond_13e
    const-string v3, "[LGE_UICC] Notifying mAbsentRegistrants"

    #@140
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/uicc/UiccCard;->log(Ljava/lang/String;)V

    #@143
    .line 187
    iget-object v3, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mAbsentRegistrants:Landroid/os/RegistrantList;

    #@145
    invoke-virtual {v3}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@148
    .line 188
    new-instance v3, Ljava/lang/StringBuilder;

    #@14a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@14d
    const-string v5, "[LGE_UICC] card_state update : "

    #@14f
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@152
    move-result-object v3

    #@153
    iget-object v5, p0, Lcom/android/internal/telephony/uicc/UiccCard;->mCardState:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    #@155
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@158
    move-result-object v3

    #@159
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15c
    move-result-object v3

    #@15d
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/uicc/UiccCard;->log(Ljava/lang/String;)V
    :try_end_160
    .catchall {:try_start_93 .. :try_end_160} :catchall_90

    #@160
    goto :goto_11d
.end method
