.class public Lcom/android/internal/telephony/WapPushOverSms;
.super Ljava/lang/Object;
.source "WapPushOverSms.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/WapPushOverSms$WapPushConnection;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "WAP PUSH"

.field private static final TAG:Ljava/lang/String; = "WapPushOverSms"


# instance fields
.field private final BIND_RETRY_INTERVAL:I

.field private final WAKE_LOCK_TIMEOUT:I

.field private final mContext:Landroid/content/Context;

.field private mSmsDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

.field private mWapConn:Lcom/android/internal/telephony/WapPushOverSms$WapPushConnection;

.field private pduDecoder:Lcom/android/internal/telephony/WspTypeDecoder;


# direct methods
.method public constructor <init>(Lcom/android/internal/telephony/Phone;Lcom/android/internal/telephony/SMSDispatcher;)V
    .registers 5
    .parameter "phone"
    .parameter "smsDispatcher"

    #@0
    .prologue
    .line 132
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 59
    const/16 v0, 0x1388

    #@5
    iput v0, p0, Lcom/android/internal/telephony/WapPushOverSms;->WAKE_LOCK_TIMEOUT:I

    #@7
    .line 61
    const/16 v0, 0x3e8

    #@9
    iput v0, p0, Lcom/android/internal/telephony/WapPushOverSms;->BIND_RETRY_INTERVAL:I

    #@b
    .line 65
    const/4 v0, 0x0

    #@c
    iput-object v0, p0, Lcom/android/internal/telephony/WapPushOverSms;->mWapConn:Lcom/android/internal/telephony/WapPushOverSms$WapPushConnection;

    #@e
    .line 133
    iput-object p2, p0, Lcom/android/internal/telephony/WapPushOverSms;->mSmsDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@10
    .line 134
    invoke-interface {p1}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@13
    move-result-object v0

    #@14
    iput-object v0, p0, Lcom/android/internal/telephony/WapPushOverSms;->mContext:Landroid/content/Context;

    #@16
    .line 135
    new-instance v0, Lcom/android/internal/telephony/WapPushOverSms$WapPushConnection;

    #@18
    iget-object v1, p0, Lcom/android/internal/telephony/WapPushOverSms;->mContext:Landroid/content/Context;

    #@1a
    invoke-direct {v0, p0, v1}, Lcom/android/internal/telephony/WapPushOverSms$WapPushConnection;-><init>(Lcom/android/internal/telephony/WapPushOverSms;Landroid/content/Context;)V

    #@1d
    iput-object v0, p0, Lcom/android/internal/telephony/WapPushOverSms;->mWapConn:Lcom/android/internal/telephony/WapPushOverSms$WapPushConnection;

    #@1f
    .line 136
    iget-object v0, p0, Lcom/android/internal/telephony/WapPushOverSms;->mWapConn:Lcom/android/internal/telephony/WapPushOverSms$WapPushConnection;

    #@21
    invoke-virtual {v0}, Lcom/android/internal/telephony/WapPushOverSms$WapPushConnection;->bindWapPushManager()V

    #@24
    .line 137
    return-void
.end method

.method static synthetic access$000(Lcom/android/internal/telephony/WapPushOverSms;)Lcom/android/internal/telephony/WapPushOverSms$WapPushConnection;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 45
    iget-object v0, p0, Lcom/android/internal/telephony/WapPushOverSms;->mWapConn:Lcom/android/internal/telephony/WapPushOverSms$WapPushConnection;

    #@2
    return-object v0
.end method

.method private getValidWapPduIndex()I
    .registers 3

    #@0
    .prologue
    .line 142
    iget-object v0, p0, Lcom/android/internal/telephony/WapPushOverSms;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "support_sprint_lock_and_wipe"

    #@4
    invoke-static {v0, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_11

    #@a
    .line 143
    const-string v0, "dispatchWapPdu(),[SPR] get normal index"

    #@c
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@f
    .line 144
    const/4 v0, 0x4

    #@10
    .line 155
    :goto_10
    return v0

    #@11
    .line 148
    :cond_11
    iget-object v0, p0, Lcom/android/internal/telephony/WapPushOverSms;->mContext:Landroid/content/Context;

    #@13
    const-string v1, "kddi_cdma_wap_push"

    #@15
    invoke-static {v0, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@18
    move-result v0

    #@19
    const/4 v1, 0x1

    #@1a
    if-ne v0, v1, :cond_23

    #@1c
    .line 149
    const-string v0, "dispatchWapPdu(),[KDDI] get normal index"

    #@1e
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@21
    .line 150
    const/4 v0, 0x7

    #@22
    goto :goto_10

    #@23
    .line 154
    :cond_23
    const-string v0, "reparseValidWapPduIndex(), Received non-PUSH WAP PDU"

    #@25
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->w(Ljava/lang/String;)I

    #@28
    .line 155
    const/4 v0, -0x1

    #@29
    goto :goto_10
.end method


# virtual methods
.method public dispatchWapPdu([B)I
    .registers 3
    .parameter "pdu"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 424
    invoke-virtual {p0, p1, v0, v0}, Lcom/android/internal/telephony/WapPushOverSms;->dispatchWapPdu([BLjava/lang/String;Ljava/lang/String;)I

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public dispatchWapPdu([BLjava/lang/String;Ljava/lang/String;)I
    .registers 35
    .parameter "pdu"
    .parameter "serviceCenter"
    .parameter "originating"

    #@0
    .prologue
    .line 172
    const/16 v24, 0x0

    #@2
    .line 176
    .local v24, thisIsSpam:Z
    const/4 v12, 0x0

    #@3
    .line 177
    .local v12, index:I
    add-int/lit8 v13, v12, 0x1

    #@5
    .end local v12           #index:I
    .local v13, index:I
    aget-byte v28, p1, v12

    #@7
    move/from16 v0, v28

    #@9
    and-int/lit16 v0, v0, 0xff

    #@b
    move/from16 v25, v0

    #@d
    .line 178
    .local v25, transactionId:I
    add-int/lit8 v12, v13, 0x1

    #@f
    .end local v13           #index:I
    .restart local v12       #index:I
    aget-byte v28, p1, v13

    #@11
    move/from16 v0, v28

    #@13
    and-int/lit16 v0, v0, 0xff

    #@15
    move/from16 v18, v0

    #@17
    .line 179
    .local v18, pduType:I
    const/4 v10, 0x0

    #@18
    .line 181
    .local v10, headerLength:I
    new-instance v28, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v29, "dispatchWapPdu(), index = "

    #@1f
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v28

    #@23
    move-object/from16 v0, v28

    #@25
    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v28

    #@29
    const-string v29, " PDU Type = "

    #@2b
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v28

    #@2f
    move-object/from16 v0, v28

    #@31
    move/from16 v1, v18

    #@33
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@36
    move-result-object v28

    #@37
    const-string v29, " transactionID = "

    #@39
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v28

    #@3d
    move-object/from16 v0, v28

    #@3f
    move/from16 v1, v25

    #@41
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@44
    move-result-object v28

    #@45
    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v28

    #@49
    invoke-static/range {v28 .. v28}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->w(Ljava/lang/String;)I

    #@4c
    .line 182
    const/16 v28, 0x6

    #@4e
    move/from16 v0, v18

    #@50
    move/from16 v1, v28

    #@52
    if-eq v0, v1, :cond_109

    #@54
    const/16 v28, 0x7

    #@56
    move/from16 v0, v18

    #@58
    move/from16 v1, v28

    #@5a
    if-eq v0, v1, :cond_109

    #@5c
    .line 185
    move-object/from16 v0, p0

    #@5e
    iget-object v0, v0, Lcom/android/internal/telephony/WapPushOverSms;->mContext:Landroid/content/Context;

    #@60
    move-object/from16 v28, v0

    #@62
    const-string v29, "reparse_wap_push_index"

    #@64
    invoke-static/range {v28 .. v29}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@67
    move-result v28

    #@68
    if-eqz v28, :cond_106

    #@6a
    .line 187
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/telephony/WapPushOverSms;->getValidWapPduIndex()I

    #@6d
    move-result v12

    #@6e
    .line 188
    const/16 v28, -0x1

    #@70
    move/from16 v0, v28

    #@72
    if-eq v12, v0, :cond_e9

    #@74
    .line 190
    add-int/lit8 v13, v12, 0x1

    #@76
    .end local v12           #index:I
    .restart local v13       #index:I
    aget-byte v28, p1, v12

    #@78
    move/from16 v0, v28

    #@7a
    and-int/lit16 v0, v0, 0xff

    #@7c
    move/from16 v25, v0

    #@7e
    .line 191
    add-int/lit8 v12, v13, 0x1

    #@80
    .end local v13           #index:I
    .restart local v12       #index:I
    aget-byte v28, p1, v13

    #@82
    move/from16 v0, v28

    #@84
    and-int/lit16 v0, v0, 0xff

    #@86
    move/from16 v18, v0

    #@88
    .line 192
    new-instance v28, Ljava/lang/StringBuilder;

    #@8a
    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    #@8d
    const-string v29, "dispatchWapPdu(), index = "

    #@8f
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@92
    move-result-object v28

    #@93
    move-object/from16 v0, v28

    #@95
    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@98
    move-result-object v28

    #@99
    const-string v29, " PDU Type = "

    #@9b
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v28

    #@9f
    move-object/from16 v0, v28

    #@a1
    move/from16 v1, v18

    #@a3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v28

    #@a7
    const-string v29, " transactionID = "

    #@a9
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v28

    #@ad
    move-object/from16 v0, v28

    #@af
    move/from16 v1, v25

    #@b1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v28

    #@b5
    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b8
    move-result-object v28

    #@b9
    invoke-static/range {v28 .. v28}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->w(Ljava/lang/String;)I

    #@bc
    .line 195
    const/16 v28, 0x6

    #@be
    move/from16 v0, v18

    #@c0
    move/from16 v1, v28

    #@c2
    if-eq v0, v1, :cond_109

    #@c4
    const/16 v28, 0x7

    #@c6
    move/from16 v0, v18

    #@c8
    move/from16 v1, v28

    #@ca
    if-eq v0, v1, :cond_109

    #@cc
    .line 197
    new-instance v28, Ljava/lang/StringBuilder;

    #@ce
    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    #@d1
    const-string v29, "dispatchWapPdu(), Received non-PUSH WAP PDU. Type = "

    #@d3
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d6
    move-result-object v28

    #@d7
    move-object/from16 v0, v28

    #@d9
    move/from16 v1, v18

    #@db
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@de
    move-result-object v28

    #@df
    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e2
    move-result-object v28

    #@e3
    invoke-static/range {v28 .. v28}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->w(Ljava/lang/String;)I

    #@e6
    .line 198
    const/16 v28, 0x1

    #@e8
    .line 418
    :goto_e8
    return v28

    #@e9
    .line 204
    :cond_e9
    new-instance v28, Ljava/lang/StringBuilder;

    #@eb
    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    #@ee
    const-string v29, "dispatchWapPdu(), Received non-PUSH WAP PDU. Type = "

    #@f0
    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f3
    move-result-object v28

    #@f4
    move-object/from16 v0, v28

    #@f6
    move/from16 v1, v18

    #@f8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@fb
    move-result-object v28

    #@fc
    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ff
    move-result-object v28

    #@100
    invoke-static/range {v28 .. v28}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->w(Ljava/lang/String;)I

    #@103
    .line 205
    const/16 v28, 0x1

    #@105
    goto :goto_e8

    #@106
    .line 213
    :cond_106
    const/16 v28, 0x1

    #@108
    goto :goto_e8

    #@109
    .line 217
    :cond_109
    new-instance v28, Lcom/android/internal/telephony/WspTypeDecoder;

    #@10b
    move-object/from16 v0, v28

    #@10d
    move-object/from16 v1, p1

    #@10f
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/WspTypeDecoder;-><init>([B)V

    #@112
    move-object/from16 v0, v28

    #@114
    move-object/from16 v1, p0

    #@116
    iput-object v0, v1, Lcom/android/internal/telephony/WapPushOverSms;->pduDecoder:Lcom/android/internal/telephony/WspTypeDecoder;

    #@118
    .line 225
    move-object/from16 v0, p0

    #@11a
    iget-object v0, v0, Lcom/android/internal/telephony/WapPushOverSms;->pduDecoder:Lcom/android/internal/telephony/WspTypeDecoder;

    #@11c
    move-object/from16 v28, v0

    #@11e
    move-object/from16 v0, v28

    #@120
    invoke-virtual {v0, v12}, Lcom/android/internal/telephony/WspTypeDecoder;->decodeUintvarInteger(I)Z

    #@123
    move-result v28

    #@124
    if-nez v28, :cond_129

    #@126
    .line 227
    const/16 v28, 0x2

    #@128
    goto :goto_e8

    #@129
    .line 229
    :cond_129
    move-object/from16 v0, p0

    #@12b
    iget-object v0, v0, Lcom/android/internal/telephony/WapPushOverSms;->pduDecoder:Lcom/android/internal/telephony/WspTypeDecoder;

    #@12d
    move-object/from16 v28, v0

    #@12f
    invoke-virtual/range {v28 .. v28}, Lcom/android/internal/telephony/WspTypeDecoder;->getValue32()J

    #@132
    move-result-wide v28

    #@133
    move-wide/from16 v0, v28

    #@135
    long-to-int v10, v0

    #@136
    .line 230
    move-object/from16 v0, p0

    #@138
    iget-object v0, v0, Lcom/android/internal/telephony/WapPushOverSms;->pduDecoder:Lcom/android/internal/telephony/WspTypeDecoder;

    #@13a
    move-object/from16 v28, v0

    #@13c
    invoke-virtual/range {v28 .. v28}, Lcom/android/internal/telephony/WspTypeDecoder;->getDecodedDataLength()I

    #@13f
    move-result v28

    #@140
    add-int v12, v12, v28

    #@142
    .line 232
    move v11, v12

    #@143
    .line 246
    .local v11, headerStartIndex:I
    move-object/from16 v0, p0

    #@145
    iget-object v0, v0, Lcom/android/internal/telephony/WapPushOverSms;->pduDecoder:Lcom/android/internal/telephony/WspTypeDecoder;

    #@147
    move-object/from16 v28, v0

    #@149
    move-object/from16 v0, v28

    #@14b
    invoke-virtual {v0, v12}, Lcom/android/internal/telephony/WspTypeDecoder;->decodeContentType(I)Z

    #@14e
    move-result v28

    #@14f
    if-nez v28, :cond_154

    #@151
    .line 248
    const/16 v28, 0x2

    #@153
    goto :goto_e8

    #@154
    .line 251
    :cond_154
    move-object/from16 v0, p0

    #@156
    iget-object v0, v0, Lcom/android/internal/telephony/WapPushOverSms;->pduDecoder:Lcom/android/internal/telephony/WspTypeDecoder;

    #@158
    move-object/from16 v28, v0

    #@15a
    invoke-virtual/range {v28 .. v28}, Lcom/android/internal/telephony/WspTypeDecoder;->getValueString()Ljava/lang/String;

    #@15d
    move-result-object v17

    #@15e
    .line 252
    .local v17, mimeType:Ljava/lang/String;
    move-object/from16 v0, p0

    #@160
    iget-object v0, v0, Lcom/android/internal/telephony/WapPushOverSms;->pduDecoder:Lcom/android/internal/telephony/WspTypeDecoder;

    #@162
    move-object/from16 v28, v0

    #@164
    invoke-virtual/range {v28 .. v28}, Lcom/android/internal/telephony/WspTypeDecoder;->getValue32()J

    #@167
    move-result-wide v4

    #@168
    .line 253
    .local v4, binaryContentType:J
    move-object/from16 v0, p0

    #@16a
    iget-object v0, v0, Lcom/android/internal/telephony/WapPushOverSms;->pduDecoder:Lcom/android/internal/telephony/WspTypeDecoder;

    #@16c
    move-object/from16 v28, v0

    #@16e
    invoke-virtual/range {v28 .. v28}, Lcom/android/internal/telephony/WspTypeDecoder;->getDecodedDataLength()I

    #@171
    move-result v28

    #@172
    add-int v12, v12, v28

    #@174
    .line 256
    move-object/from16 v0, p0

    #@176
    iget-object v0, v0, Lcom/android/internal/telephony/WapPushOverSms;->mContext:Landroid/content/Context;

    #@178
    move-object/from16 v28, v0

    #@17a
    const-string v29, "support_sprint_lock_and_wipe"

    #@17c
    invoke-static/range {v28 .. v29}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@17f
    move-result v28

    #@180
    if-eqz v28, :cond_1e0

    #@182
    .line 257
    if-nez v17, :cond_1bd

    #@184
    .line 259
    move-object/from16 v0, p0

    #@186
    iget-object v0, v0, Lcom/android/internal/telephony/WapPushOverSms;->mContext:Landroid/content/Context;

    #@188
    move-object/from16 v28, v0

    #@18a
    invoke-virtual/range {v28 .. v28}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@18d
    move-result-object v28

    #@18e
    const-string v29, "OMADM_LOCK_STATE"

    #@190
    const/16 v30, 0x0

    #@192
    invoke-static/range {v28 .. v30}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@195
    move-result v8

    #@196
    .line 260
    .local v8, dmLockState:I
    const/16 v28, 0x1

    #@198
    move/from16 v0, v28

    #@19a
    if-ne v8, v0, :cond_1e0

    #@19c
    long-to-int v0, v4

    #@19d
    move/from16 v28, v0

    #@19f
    const/16 v29, 0x45

    #@1a1
    move/from16 v0, v28

    #@1a3
    move/from16 v1, v29

    #@1a5
    if-eq v0, v1, :cond_1e0

    #@1a7
    long-to-int v0, v4

    #@1a8
    move/from16 v28, v0

    #@1aa
    const/16 v29, 0x44

    #@1ac
    move/from16 v0, v28

    #@1ae
    move/from16 v1, v29

    #@1b0
    if-eq v0, v1, :cond_1e0

    #@1b2
    .line 263
    const-string v28, "WapPushOverSms"

    #@1b4
    const-string v29, "dispatchWapPdu: -Lock and Wipe - enabled -Block Inbound WAP push"

    #@1b6
    invoke-static/range {v28 .. v29}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1b9
    .line 264
    const/16 v28, 0x2

    #@1bb
    goto/16 :goto_e8

    #@1bd
    .line 268
    .end local v8           #dmLockState:I
    :cond_1bd
    move-object/from16 v0, p0

    #@1bf
    iget-object v0, v0, Lcom/android/internal/telephony/WapPushOverSms;->mContext:Landroid/content/Context;

    #@1c1
    move-object/from16 v28, v0

    #@1c3
    invoke-virtual/range {v28 .. v28}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1c6
    move-result-object v28

    #@1c7
    const-string v29, "OMADM_LOCK_STATE"

    #@1c9
    const/16 v30, 0x0

    #@1cb
    invoke-static/range {v28 .. v30}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@1ce
    move-result v8

    #@1cf
    .line 269
    .restart local v8       #dmLockState:I
    const/16 v28, 0x1

    #@1d1
    move/from16 v0, v28

    #@1d3
    if-ne v8, v0, :cond_1e0

    #@1d5
    .line 270
    const-string v28, "WapPushOverSms"

    #@1d7
    const-string v29, "dispatchWapPdu: -Lock and Wipe - enabled -Block Inbound WAP push"

    #@1d9
    invoke-static/range {v28 .. v29}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1dc
    .line 271
    const/16 v28, 0x2

    #@1de
    goto/16 :goto_e8

    #@1e0
    .line 277
    .end local v8           #dmLockState:I
    :cond_1e0
    new-array v9, v10, [B

    #@1e2
    .line 278
    .local v9, header:[B
    const/16 v28, 0x0

    #@1e4
    array-length v0, v9

    #@1e5
    move/from16 v29, v0

    #@1e7
    move-object/from16 v0, p1

    #@1e9
    move/from16 v1, v28

    #@1eb
    move/from16 v2, v29

    #@1ed
    invoke-static {v0, v11, v9, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@1f0
    .line 282
    if-eqz v17, :cond_2e1

    #@1f2
    const-string v28, "application/vnd.wap.coc"

    #@1f4
    move-object/from16 v0, v17

    #@1f6
    move-object/from16 v1, v28

    #@1f8
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1fb
    move-result v28

    #@1fc
    if-eqz v28, :cond_2e1

    #@1fe
    .line 283
    move-object/from16 v16, p1

    #@200
    .line 296
    .local v16, intentData:[B
    :goto_200
    move-object/from16 v0, p0

    #@202
    iget-object v0, v0, Lcom/android/internal/telephony/WapPushOverSms;->pduDecoder:Lcom/android/internal/telephony/WspTypeDecoder;

    #@204
    move-object/from16 v28, v0

    #@206
    add-int v29, v12, v10

    #@208
    add-int/lit8 v29, v29, -0x1

    #@20a
    move-object/from16 v0, v28

    #@20c
    move/from16 v1, v29

    #@20e
    invoke-virtual {v0, v12, v1}, Lcom/android/internal/telephony/WspTypeDecoder;->seekXWapApplicationId(II)Z

    #@211
    move-result v28

    #@212
    if-eqz v28, :cond_3a2

    #@214
    .line 297
    move-object/from16 v0, p0

    #@216
    iget-object v0, v0, Lcom/android/internal/telephony/WapPushOverSms;->pduDecoder:Lcom/android/internal/telephony/WspTypeDecoder;

    #@218
    move-object/from16 v28, v0

    #@21a
    invoke-virtual/range {v28 .. v28}, Lcom/android/internal/telephony/WspTypeDecoder;->getValue32()J

    #@21d
    move-result-wide v28

    #@21e
    move-wide/from16 v0, v28

    #@220
    long-to-int v12, v0

    #@221
    .line 298
    move-object/from16 v0, p0

    #@223
    iget-object v0, v0, Lcom/android/internal/telephony/WapPushOverSms;->pduDecoder:Lcom/android/internal/telephony/WspTypeDecoder;

    #@225
    move-object/from16 v28, v0

    #@227
    move-object/from16 v0, v28

    #@229
    invoke-virtual {v0, v12}, Lcom/android/internal/telephony/WspTypeDecoder;->decodeXWapApplicationId(I)Z

    #@22c
    .line 299
    move-object/from16 v0, p0

    #@22e
    iget-object v0, v0, Lcom/android/internal/telephony/WapPushOverSms;->pduDecoder:Lcom/android/internal/telephony/WspTypeDecoder;

    #@230
    move-object/from16 v28, v0

    #@232
    invoke-virtual/range {v28 .. v28}, Lcom/android/internal/telephony/WspTypeDecoder;->getValueString()Ljava/lang/String;

    #@235
    move-result-object v26

    #@236
    .line 300
    .local v26, wapAppId:Ljava/lang/String;
    if-nez v26, :cond_24b

    #@238
    .line 301
    move-object/from16 v0, p0

    #@23a
    iget-object v0, v0, Lcom/android/internal/telephony/WapPushOverSms;->pduDecoder:Lcom/android/internal/telephony/WspTypeDecoder;

    #@23c
    move-object/from16 v28, v0

    #@23e
    invoke-virtual/range {v28 .. v28}, Lcom/android/internal/telephony/WspTypeDecoder;->getValue32()J

    #@241
    move-result-wide v28

    #@242
    move-wide/from16 v0, v28

    #@244
    long-to-int v0, v0

    #@245
    move/from16 v28, v0

    #@247
    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@24a
    move-result-object v26

    #@24b
    .line 304
    :cond_24b
    if-nez v17, :cond_304

    #@24d
    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@250
    move-result-object v6

    #@251
    .line 309
    .local v6, contentType:Ljava/lang/String;
    :goto_251
    move-object/from16 v0, p0

    #@253
    iget-object v0, v0, Lcom/android/internal/telephony/WapPushOverSms;->mContext:Landroid/content/Context;

    #@255
    move-object/from16 v28, v0

    #@257
    const-string v29, "support_sprint_lock_and_wipe"

    #@259
    invoke-static/range {v28 .. v29}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@25c
    move-result v28

    #@25d
    if-eqz v28, :cond_276

    #@25f
    .line 310
    const-wide/16 v28, 0x45

    #@261
    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@264
    move-result-object v28

    #@265
    move-object/from16 v0, v28

    #@267
    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@26a
    move-result v28

    #@26b
    if-eqz v28, :cond_276

    #@26d
    .line 312
    const-string v28, "WapPushOverSms"

    #@26f
    const-string v29, "OMADM NIA - mimeType = application/vnd.syncml.notification"

    #@271
    invoke-static/range {v28 .. v29}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@274
    .line 313
    const-string v17, "application/vnd.syncml.notification"

    #@276
    .line 319
    :cond_276
    move-object/from16 v0, p0

    #@278
    iget-object v0, v0, Lcom/android/internal/telephony/WapPushOverSms;->mContext:Landroid/content/Context;

    #@27a
    move-object/from16 v28, v0

    #@27c
    const-string v29, "KRWapPushWithSpam"

    #@27e
    invoke-static/range {v28 .. v29}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@281
    move-result v28

    #@282
    const/16 v29, 0x1

    #@284
    move/from16 v0, v28

    #@286
    move/from16 v1, v29

    #@288
    if-ne v0, v1, :cond_2cd

    #@28a
    .line 320
    add-int v7, v11, v10

    #@28c
    .line 323
    .local v7, dataIndex:I
    move-object/from16 v0, p1

    #@28e
    array-length v0, v0

    #@28f
    move/from16 v28, v0

    #@291
    sub-int v28, v28, v7

    #@293
    move/from16 v0, v28

    #@295
    new-array v0, v0, [B

    #@297
    move-object/from16 v23, v0

    #@299
    .line 324
    .local v23, spamcheckdata:[B
    const/16 v28, 0x0

    #@29b
    move-object/from16 v0, v23

    #@29d
    array-length v0, v0

    #@29e
    move/from16 v29, v0

    #@2a0
    move-object/from16 v0, p1

    #@2a2
    move-object/from16 v1, v23

    #@2a4
    move/from16 v2, v28

    #@2a6
    move/from16 v3, v29

    #@2a8
    invoke-static {v0, v7, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@2ab
    .line 326
    move-object/from16 v0, p0

    #@2ad
    iget-object v0, v0, Lcom/android/internal/telephony/WapPushOverSms;->mContext:Landroid/content/Context;

    #@2af
    move-object/from16 v28, v0

    #@2b1
    move-object/from16 v0, v23

    #@2b3
    move-object/from16 v1, v28

    #@2b5
    invoke-static {v0, v1}, Lcom/android/internal/telephony/SmsKrMessageManager;->getOperatorSpamMessage([BLandroid/content/Context;)Lcom/android/internal/telephony/SmsOperatorBasicMessage;

    #@2b8
    move-result-object v22

    #@2b9
    .line 328
    .local v22, spamMessage:Lcom/android/internal/telephony/SmsOperatorBasicMessage;
    if-eqz v22, :cond_2cd

    #@2bb
    .line 329
    invoke-interface/range {v22 .. v22}, Lcom/android/internal/telephony/SmsOperatorBasicMessage;->getInformation()Landroid/os/Bundle;

    #@2be
    move-result-object v14

    #@2bf
    .line 330
    .local v14, info:Landroid/os/Bundle;
    if-eqz v14, :cond_2cd

    #@2c1
    .line 331
    const-string v28, "spam_result"

    #@2c3
    const/16 v29, 0x0

    #@2c5
    move-object/from16 v0, v28

    #@2c7
    move/from16 v1, v29

    #@2c9
    invoke-virtual {v14, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    #@2cc
    move-result v24

    #@2cd
    .line 338
    .end local v7           #dataIndex:I
    .end local v14           #info:Landroid/os/Bundle;
    .end local v22           #spamMessage:Lcom/android/internal/telephony/SmsOperatorBasicMessage;
    .end local v23           #spamcheckdata:[B
    :cond_2cd
    const/16 v21, 0x1

    #@2cf
    .line 339
    .local v21, processFurther:Z
    :try_start_2cf
    move-object/from16 v0, p0

    #@2d1
    iget-object v0, v0, Lcom/android/internal/telephony/WapPushOverSms;->mWapConn:Lcom/android/internal/telephony/WapPushOverSms$WapPushConnection;

    #@2d3
    move-object/from16 v28, v0

    #@2d5
    invoke-virtual/range {v28 .. v28}, Lcom/android/internal/telephony/WapPushOverSms$WapPushConnection;->getWapPushManager()Lcom/android/internal/telephony/IWapPushManager;
    :try_end_2d8
    .catch Landroid/os/RemoteException; {:try_start_2cf .. :try_end_2d8} :catch_372

    #@2d8
    move-result-object v27

    #@2d9
    .line 341
    .local v27, wapPushMan:Lcom/android/internal/telephony/IWapPushManager;
    if-nez v27, :cond_308

    #@2db
    .line 361
    :cond_2db
    :goto_2db
    if-nez v21, :cond_373

    #@2dd
    .line 362
    const/16 v28, 0x1

    #@2df
    goto/16 :goto_e8

    #@2e1
    .line 285
    .end local v6           #contentType:Ljava/lang/String;
    .end local v16           #intentData:[B
    .end local v21           #processFurther:Z
    .end local v26           #wapAppId:Ljava/lang/String;
    .end local v27           #wapPushMan:Lcom/android/internal/telephony/IWapPushManager;
    :cond_2e1
    add-int v7, v11, v10

    #@2e3
    .line 286
    .restart local v7       #dataIndex:I
    move-object/from16 v0, p1

    #@2e5
    array-length v0, v0

    #@2e6
    move/from16 v28, v0

    #@2e8
    sub-int v28, v28, v7

    #@2ea
    move/from16 v0, v28

    #@2ec
    new-array v0, v0, [B

    #@2ee
    move-object/from16 v16, v0

    #@2f0
    .line 287
    .restart local v16       #intentData:[B
    const/16 v28, 0x0

    #@2f2
    move-object/from16 v0, v16

    #@2f4
    array-length v0, v0

    #@2f5
    move/from16 v29, v0

    #@2f7
    move-object/from16 v0, p1

    #@2f9
    move-object/from16 v1, v16

    #@2fb
    move/from16 v2, v28

    #@2fd
    move/from16 v3, v29

    #@2ff
    invoke-static {v0, v7, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@302
    goto/16 :goto_200

    #@304
    .end local v7           #dataIndex:I
    .restart local v26       #wapAppId:Ljava/lang/String;
    :cond_304
    move-object/from16 v6, v17

    #@306
    .line 304
    goto/16 :goto_251

    #@308
    .line 344
    .restart local v6       #contentType:Ljava/lang/String;
    .restart local v21       #processFurther:Z
    .restart local v27       #wapPushMan:Lcom/android/internal/telephony/IWapPushManager;
    :cond_308
    :try_start_308
    new-instance v15, Landroid/content/Intent;

    #@30a
    invoke-direct {v15}, Landroid/content/Intent;-><init>()V

    #@30d
    .line 345
    .local v15, intent:Landroid/content/Intent;
    const-string v28, "transactionId"

    #@30f
    move-object/from16 v0, v28

    #@311
    move/from16 v1, v25

    #@313
    invoke-virtual {v15, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@316
    .line 346
    const-string v28, "pduType"

    #@318
    move-object/from16 v0, v28

    #@31a
    move/from16 v1, v18

    #@31c
    invoke-virtual {v15, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@31f
    .line 347
    const-string v28, "header"

    #@321
    move-object/from16 v0, v28

    #@323
    invoke-virtual {v15, v0, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    #@326
    .line 348
    const-string v28, "data"

    #@328
    move-object/from16 v0, v28

    #@32a
    move-object/from16 v1, v16

    #@32c
    invoke-virtual {v15, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    #@32f
    .line 349
    const-string v28, "contentTypeParameters"

    #@331
    move-object/from16 v0, p0

    #@333
    iget-object v0, v0, Lcom/android/internal/telephony/WapPushOverSms;->pduDecoder:Lcom/android/internal/telephony/WspTypeDecoder;

    #@335
    move-object/from16 v29, v0

    #@337
    invoke-virtual/range {v29 .. v29}, Lcom/android/internal/telephony/WspTypeDecoder;->getContentParameters()Ljava/util/HashMap;

    #@33a
    move-result-object v29

    #@33b
    move-object/from16 v0, v28

    #@33d
    move-object/from16 v1, v29

    #@33f
    invoke-virtual {v15, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    #@342
    .line 351
    const-string v28, "subscription"

    #@344
    move-object/from16 v0, p0

    #@346
    iget-object v0, v0, Lcom/android/internal/telephony/WapPushOverSms;->mSmsDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@348
    move-object/from16 v29, v0

    #@34a
    move-object/from16 v0, v29

    #@34c
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@34e
    move-object/from16 v29, v0

    #@350
    invoke-virtual/range {v29 .. v29}, Lcom/android/internal/telephony/PhoneBase;->getSubscription()I

    #@353
    move-result v29

    #@354
    move-object/from16 v0, v28

    #@356
    move/from16 v1, v29

    #@358
    invoke-virtual {v15, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@35b
    .line 354
    move-object/from16 v0, v27

    #@35d
    move-object/from16 v1, v26

    #@35f
    invoke-interface {v0, v1, v6, v15}, Lcom/android/internal/telephony/IWapPushManager;->processMessage(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)I
    :try_end_362
    .catch Landroid/os/RemoteException; {:try_start_308 .. :try_end_362} :catch_372

    #@362
    move-result v20

    #@363
    .line 356
    .local v20, procRet:I
    and-int/lit8 v28, v20, 0x1

    #@365
    if-lez v28, :cond_2db

    #@367
    const v28, 0x8000

    #@36a
    and-int v28, v28, v20

    #@36c
    if-nez v28, :cond_2db

    #@36e
    .line 358
    const/16 v21, 0x0

    #@370
    goto/16 :goto_2db

    #@372
    .line 364
    .end local v15           #intent:Landroid/content/Intent;
    .end local v20           #procRet:I
    .end local v27           #wapPushMan:Lcom/android/internal/telephony/IWapPushManager;
    :catch_372
    move-exception v28

    #@373
    .line 368
    :cond_373
    move-object/from16 v0, p0

    #@375
    iget-object v0, v0, Lcom/android/internal/telephony/WapPushOverSms;->mContext:Landroid/content/Context;

    #@377
    move-object/from16 v28, v0

    #@379
    const-string v29, "dcm_push_check_security"

    #@37b
    invoke-static/range {v28 .. v29}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@37e
    move-result v28

    #@37f
    if-eqz v28, :cond_3a2

    #@381
    .line 369
    const-string v28, "9"

    #@383
    move-object/from16 v0, v26

    #@385
    move-object/from16 v1, v28

    #@387
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@38a
    move-result v28

    #@38b
    if-eqz v28, :cond_397

    #@38d
    const-string v28, "application/vnd.wap.emn+wbxml"

    #@38f
    move-object/from16 v0, v28

    #@391
    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@394
    move-result v28

    #@395
    if-nez v28, :cond_3a2

    #@397
    .line 370
    :cond_397
    const-string v28, "WAP PUSH"

    #@399
    const-string v29, "not mopera U mail push"

    #@39b
    invoke-static/range {v28 .. v29}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@39e
    .line 371
    const/16 v28, 0x1

    #@3a0
    goto/16 :goto_e8

    #@3a2
    .line 378
    .end local v6           #contentType:Ljava/lang/String;
    .end local v21           #processFurther:Z
    .end local v26           #wapAppId:Ljava/lang/String;
    :cond_3a2
    if-nez v17, :cond_3a8

    #@3a4
    .line 380
    const/16 v28, 0x2

    #@3a6
    goto/16 :goto_e8

    #@3a8
    .line 385
    :cond_3a8
    const-string v28, "application/vnd.wap.mms-message"

    #@3aa
    move-object/from16 v0, v17

    #@3ac
    move-object/from16 v1, v28

    #@3ae
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3b1
    move-result v28

    #@3b2
    if-eqz v28, :cond_476

    #@3b4
    .line 386
    const-string v19, "android.permission.RECEIVE_MMS"

    #@3b6
    .line 391
    .local v19, permission:Ljava/lang/String;
    :goto_3b6
    new-instance v15, Landroid/content/Intent;

    #@3b8
    const-string v28, "android.provider.Telephony.WAP_PUSH_RECEIVED"

    #@3ba
    move-object/from16 v0, v28

    #@3bc
    invoke-direct {v15, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@3bf
    .line 393
    .restart local v15       #intent:Landroid/content/Intent;
    move-object/from16 v0, p0

    #@3c1
    iget-object v0, v0, Lcom/android/internal/telephony/WapPushOverSms;->mContext:Landroid/content/Context;

    #@3c3
    move-object/from16 v28, v0

    #@3c5
    const-string v29, "KRWapPushWithSpam"

    #@3c7
    invoke-static/range {v28 .. v29}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@3ca
    move-result v28

    #@3cb
    const/16 v29, 0x1

    #@3cd
    move/from16 v0, v28

    #@3cf
    move/from16 v1, v29

    #@3d1
    if-ne v0, v1, :cond_3e4

    #@3d3
    const/16 v28, 0x1

    #@3d5
    move/from16 v0, v24

    #@3d7
    move/from16 v1, v28

    #@3d9
    if-ne v0, v1, :cond_3e4

    #@3db
    .line 395
    new-instance v15, Landroid/content/Intent;

    #@3dd
    .end local v15           #intent:Landroid/content/Intent;
    const-string v28, "android.provider.Telephony.WAP_PUSH_SPAM_RECEIVED"

    #@3df
    move-object/from16 v0, v28

    #@3e1
    invoke-direct {v15, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@3e4
    .line 398
    .restart local v15       #intent:Landroid/content/Intent;
    :cond_3e4
    move-object/from16 v0, v17

    #@3e6
    invoke-virtual {v15, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    #@3e9
    .line 399
    const-string v28, "transactionId"

    #@3eb
    move-object/from16 v0, v28

    #@3ed
    move/from16 v1, v25

    #@3ef
    invoke-virtual {v15, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@3f2
    .line 400
    const-string v28, "pduType"

    #@3f4
    move-object/from16 v0, v28

    #@3f6
    move/from16 v1, v18

    #@3f8
    invoke-virtual {v15, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@3fb
    .line 401
    const-string v28, "header"

    #@3fd
    move-object/from16 v0, v28

    #@3ff
    invoke-virtual {v15, v0, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    #@402
    .line 402
    const-string v28, "data"

    #@404
    move-object/from16 v0, v28

    #@406
    move-object/from16 v1, v16

    #@408
    invoke-virtual {v15, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    #@40b
    .line 403
    const-string v28, "contentTypeParameters"

    #@40d
    move-object/from16 v0, p0

    #@40f
    iget-object v0, v0, Lcom/android/internal/telephony/WapPushOverSms;->pduDecoder:Lcom/android/internal/telephony/WspTypeDecoder;

    #@411
    move-object/from16 v29, v0

    #@413
    invoke-virtual/range {v29 .. v29}, Lcom/android/internal/telephony/WspTypeDecoder;->getContentParameters()Ljava/util/HashMap;

    #@416
    move-result-object v29

    #@417
    move-object/from16 v0, v28

    #@419
    move-object/from16 v1, v29

    #@41b
    invoke-virtual {v15, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    #@41e
    .line 404
    const-string v28, "subscription"

    #@420
    move-object/from16 v0, p0

    #@422
    iget-object v0, v0, Lcom/android/internal/telephony/WapPushOverSms;->mSmsDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@424
    move-object/from16 v29, v0

    #@426
    move-object/from16 v0, v29

    #@428
    iget-object v0, v0, Lcom/android/internal/telephony/SMSDispatcher;->mPhone:Lcom/android/internal/telephony/PhoneBase;

    #@42a
    move-object/from16 v29, v0

    #@42c
    invoke-virtual/range {v29 .. v29}, Lcom/android/internal/telephony/PhoneBase;->getSubscription()I

    #@42f
    move-result v29

    #@430
    move-object/from16 v0, v28

    #@432
    move/from16 v1, v29

    #@434
    invoke-virtual {v15, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@437
    .line 406
    sget-boolean v28, Lcom/lge/config/ConfigBuildFlags;->CAPP_WAPSERVICE:Z

    #@439
    if-eqz v28, :cond_465

    #@43b
    .line 408
    const-string v28, "application/vnd.wap.sic"

    #@43d
    move-object/from16 v0, v28

    #@43f
    move-object/from16 v1, v17

    #@441
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@444
    move-result v28

    #@445
    if-nez v28, :cond_453

    #@447
    const-string v28, "application/vnd.wap.slc"

    #@449
    move-object/from16 v0, v28

    #@44b
    move-object/from16 v1, v17

    #@44d
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@450
    move-result v28

    #@451
    if-eqz v28, :cond_465

    #@453
    .line 410
    :cond_453
    const-string v28, "smscAdd"

    #@455
    move-object/from16 v0, v28

    #@457
    move-object/from16 v1, p2

    #@459
    invoke-virtual {v15, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@45c
    .line 411
    const-string v28, "originAdd"

    #@45e
    move-object/from16 v0, v28

    #@460
    move-object/from16 v1, p3

    #@462
    invoke-virtual {v15, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@465
    .line 416
    :cond_465
    move-object/from16 v0, p0

    #@467
    iget-object v0, v0, Lcom/android/internal/telephony/WapPushOverSms;->mSmsDispatcher:Lcom/android/internal/telephony/SMSDispatcher;

    #@469
    move-object/from16 v28, v0

    #@46b
    move-object/from16 v0, v28

    #@46d
    move-object/from16 v1, v19

    #@46f
    invoke-virtual {v0, v15, v1}, Lcom/android/internal/telephony/SMSDispatcher;->dispatch(Landroid/content/Intent;Ljava/lang/String;)V

    #@472
    .line 418
    const/16 v28, -0x1

    #@474
    goto/16 :goto_e8

    #@476
    .line 388
    .end local v15           #intent:Landroid/content/Intent;
    .end local v19           #permission:Ljava/lang/String;
    :cond_476
    const-string v19, "android.permission.RECEIVE_WAP_PUSH"

    #@478
    .restart local v19       #permission:Ljava/lang/String;
    goto/16 :goto_3b6
.end method
