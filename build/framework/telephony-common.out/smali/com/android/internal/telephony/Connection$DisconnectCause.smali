.class public final enum Lcom/android/internal/telephony/Connection$DisconnectCause;
.super Ljava/lang/Enum;
.source "Connection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/Connection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DisconnectCause"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/internal/telephony/Connection$DisconnectCause;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum ACCESS_CLASS_BLOCKED:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum ACCESS_INFO_DISCARD:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum ACCESS_STRATUM_REJ_ABORT_RADIO_UNAVAILABLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum ACCESS_STRATUM_REJ_LOW_LEVEL_FAIL:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum ACCESS_STRATUM_REJ_LOW_LEVEL_FAIL_REDIAL_NOT_ALLOWED:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum ACCESS_STRATUM_REJ_LOW_LEVEL_IMMED_RETRY:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum ACCESS_STRATUM_REJ_RRC_CLOSE_SESSION_IND:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum ACCESS_STRATUM_REJ_RRC_OPEN_SESSION_FAILURE:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum ACCESS_STRATUM_REJ_RRC_REL_IND:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum ACCESS_STRATUM_REJ_RR_RANDOM_ACCESS_FAILURE:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum ACCESS_STRATUM_REJ_RR_REL_IND:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum BEARER_CAPABILITY_NOT_AUTHORIZED:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum BEARER_CAPABILITY_NOT_PRESENTLY_AVAILABLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum BEARER_SERVICE_NOT_IMPLEMENTED:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum BUSY:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum CALL_BARRED:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum CALL_REJECTED:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum CDMA_ACCESS_BLOCKED:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum CDMA_ACCESS_FAILURE:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum CDMA_DROP:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum CDMA_INTERCEPT:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum CDMA_LOCKED_UNTIL_POWER_CYCLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum CDMA_NOT_EMERGENCY:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum CDMA_PREEMPTED:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum CDMA_REORDER:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum CDMA_RETRY_ORDER:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum CDMA_SO_REJECT:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum CHANNEL_UNACCEPTABLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum CNM_MM_REL_PENDING:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum CONDITIONAL_IE_ERROR:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum CONGESTION:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum CS_RESTRICTED:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum CS_RESTRICTED_EMERGENCY:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum CS_RESTRICTED_NORMAL:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum DEST_OUT_OF_ORDER:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum DIAL_MODIFIED_TO_DIAL:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum DIAL_MODIFIED_TO_SS:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum DIAL_MODIFIED_TO_USSD:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum ERROR_UNSPECIFIED:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum FACILITY_REJECTED:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum FDN_BLOCKED:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum ICC_ERROR:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum INCOMING_CALLS_BARRED_CUG:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum INCOMING_MISSED:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum INCOMING_REJECTED:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum INCOMPATIBLE_DESTINATION:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum INFORMATION_ELEMENT_NOT_IMPLEMENTED:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum INTERWORKING:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum INVALID_CREDENTIALS:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum INVALID_MANDATORY_INFO:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum INVALID_NUMBER:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum INVALID_SIM:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum INVALID_TRANSACTION_ID_VALUE:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum INVALID_TRANSIT_NETWORK_SELECTION:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum INVALID_USER_DATA:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum LIMIT_EXCEEDED:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum LOCAL:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum LOST_SIGNAL:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum MESSAGE_NOT_COMPATIBLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum MESSAGE_TYPE_NON_COMPATIBLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum MESSAGE_TYPE_NOT_COMPATIBLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum MMI:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum MS_ACCESS_CLASS_BARRED:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum NETWORK_OUT_OF_ORDER:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum NON_SELECTED_USER_CLEAR:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum NORMAL:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum NORMAL_UNSPECIFIED:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum NOT_DISCONNECTED:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum NO_ANSWER:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum NO_CELL_AVAILABLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum NO_RESOURCES:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum NO_ROUTE_TO_DEST:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum NO_USER_RESPONDING:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum NUMBER_CHANGED:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum NUMBER_UNREACHABLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum ONLY_RESTRICTED_DIGIT_INFO_BEARER_CAPABILITY:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum OPERATOR_DETERMINED_BARRING:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum OUT_OF_NETWORK:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum OUT_OF_SERVICE:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum POWER_OFF:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum PREEMPTION:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum PROTOCOL_ERROR:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum QOS_UNAVAILABLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum RECOVER_ON_TIMER_EXPIRY:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum REQUESTED_CIRCUIT_NOT_AVAILABLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum REQUESTED_FACILITY_NOT_IMPLEMENTED:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum REQUESTED_FACILITY_NOT_SUBSCRIBED:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum RESOURCE_UNAVAILABLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum RESPONSE_TO_STA_ENQ:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum SEMANTICALLY_INCORRECT_MESSAGE:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum SERVER_ERROR:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum SERVER_UNREACHABLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum SERVICE_NOT_AVAILABLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum SERVICE_NOT_IMPLEMENTED:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum SWITCHING_EQUIP_CONGESTION:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum TEMPORARY_FAIL:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum TIMED_OUT:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum TIMER_T303_EXPIRED:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum TIMER_T3230_EXPIRED:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum UNOBTAINABLE_NUMBER:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum USER_NOT_MEMBER_CUG:Lcom/android/internal/telephony/Connection$DisconnectCause;

.field public static final enum WRONG_STATE:Lcom/android/internal/telephony/Connection$DisconnectCause;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 40
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@7
    const-string v1, "NOT_DISCONNECTED"

    #@9
    invoke-direct {v0, v1, v3}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@c
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->NOT_DISCONNECTED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@e
    .line 41
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@10
    const-string v1, "INCOMING_MISSED"

    #@12
    invoke-direct {v0, v1, v4}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@15
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->INCOMING_MISSED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@17
    .line 42
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@19
    const-string v1, "NORMAL"

    #@1b
    invoke-direct {v0, v1, v5}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@1e
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->NORMAL:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@20
    .line 43
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@22
    const-string v1, "LOCAL"

    #@24
    invoke-direct {v0, v1, v6}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@27
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->LOCAL:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@29
    .line 44
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@2b
    const-string v1, "BUSY"

    #@2d
    invoke-direct {v0, v1, v7}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@30
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->BUSY:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@32
    .line 45
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@34
    const-string v1, "CONGESTION"

    #@36
    const/4 v2, 0x5

    #@37
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@3a
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->CONGESTION:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@3c
    .line 46
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@3e
    const-string v1, "MMI"

    #@40
    const/4 v2, 0x6

    #@41
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@44
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->MMI:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@46
    .line 47
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@48
    const-string v1, "INVALID_NUMBER"

    #@4a
    const/4 v2, 0x7

    #@4b
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@4e
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->INVALID_NUMBER:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@50
    .line 48
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@52
    const-string v1, "NUMBER_UNREACHABLE"

    #@54
    const/16 v2, 0x8

    #@56
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@59
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->NUMBER_UNREACHABLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@5b
    .line 49
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@5d
    const-string v1, "SERVER_UNREACHABLE"

    #@5f
    const/16 v2, 0x9

    #@61
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@64
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->SERVER_UNREACHABLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@66
    .line 50
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@68
    const-string v1, "INVALID_CREDENTIALS"

    #@6a
    const/16 v2, 0xa

    #@6c
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@6f
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->INVALID_CREDENTIALS:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@71
    .line 51
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@73
    const-string v1, "OUT_OF_NETWORK"

    #@75
    const/16 v2, 0xb

    #@77
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@7a
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->OUT_OF_NETWORK:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@7c
    .line 52
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@7e
    const-string v1, "SERVER_ERROR"

    #@80
    const/16 v2, 0xc

    #@82
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@85
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->SERVER_ERROR:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@87
    .line 53
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@89
    const-string v1, "TIMED_OUT"

    #@8b
    const/16 v2, 0xd

    #@8d
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@90
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->TIMED_OUT:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@92
    .line 54
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@94
    const-string v1, "LOST_SIGNAL"

    #@96
    const/16 v2, 0xe

    #@98
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@9b
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->LOST_SIGNAL:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@9d
    .line 55
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@9f
    const-string v1, "LIMIT_EXCEEDED"

    #@a1
    const/16 v2, 0xf

    #@a3
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@a6
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->LIMIT_EXCEEDED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@a8
    .line 56
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@aa
    const-string v1, "INCOMING_REJECTED"

    #@ac
    const/16 v2, 0x10

    #@ae
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@b1
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->INCOMING_REJECTED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@b3
    .line 57
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@b5
    const-string v1, "POWER_OFF"

    #@b7
    const/16 v2, 0x11

    #@b9
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@bc
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->POWER_OFF:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@be
    .line 58
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@c0
    const-string v1, "OUT_OF_SERVICE"

    #@c2
    const/16 v2, 0x12

    #@c4
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@c7
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->OUT_OF_SERVICE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@c9
    .line 59
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@cb
    const-string v1, "ICC_ERROR"

    #@cd
    const/16 v2, 0x13

    #@cf
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@d2
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->ICC_ERROR:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@d4
    .line 60
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@d6
    const-string v1, "CALL_BARRED"

    #@d8
    const/16 v2, 0x14

    #@da
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@dd
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->CALL_BARRED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@df
    .line 61
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@e1
    const-string v1, "FDN_BLOCKED"

    #@e3
    const/16 v2, 0x15

    #@e5
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@e8
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->FDN_BLOCKED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@ea
    .line 62
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@ec
    const-string v1, "CS_RESTRICTED"

    #@ee
    const/16 v2, 0x16

    #@f0
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@f3
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->CS_RESTRICTED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@f5
    .line 63
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@f7
    const-string v1, "CS_RESTRICTED_NORMAL"

    #@f9
    const/16 v2, 0x17

    #@fb
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@fe
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->CS_RESTRICTED_NORMAL:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@100
    .line 64
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@102
    const-string v1, "CS_RESTRICTED_EMERGENCY"

    #@104
    const/16 v2, 0x18

    #@106
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@109
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->CS_RESTRICTED_EMERGENCY:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@10b
    .line 65
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@10d
    const-string v1, "UNOBTAINABLE_NUMBER"

    #@10f
    const/16 v2, 0x19

    #@111
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@114
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->UNOBTAINABLE_NUMBER:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@116
    .line 66
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@118
    const-string v1, "DIAL_MODIFIED_TO_USSD"

    #@11a
    const/16 v2, 0x1a

    #@11c
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@11f
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->DIAL_MODIFIED_TO_USSD:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@121
    .line 67
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@123
    const-string v1, "DIAL_MODIFIED_TO_SS"

    #@125
    const/16 v2, 0x1b

    #@127
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@12a
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->DIAL_MODIFIED_TO_SS:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@12c
    .line 68
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@12e
    const-string v1, "DIAL_MODIFIED_TO_DIAL"

    #@130
    const/16 v2, 0x1c

    #@132
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@135
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->DIAL_MODIFIED_TO_DIAL:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@137
    .line 69
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@139
    const-string v1, "CDMA_LOCKED_UNTIL_POWER_CYCLE"

    #@13b
    const/16 v2, 0x1d

    #@13d
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@140
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->CDMA_LOCKED_UNTIL_POWER_CYCLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@142
    .line 70
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@144
    const-string v1, "CDMA_DROP"

    #@146
    const/16 v2, 0x1e

    #@148
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@14b
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->CDMA_DROP:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@14d
    .line 71
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@14f
    const-string v1, "CDMA_INTERCEPT"

    #@151
    const/16 v2, 0x1f

    #@153
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@156
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->CDMA_INTERCEPT:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@158
    .line 72
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@15a
    const-string v1, "CDMA_REORDER"

    #@15c
    const/16 v2, 0x20

    #@15e
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@161
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->CDMA_REORDER:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@163
    .line 73
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@165
    const-string v1, "CDMA_SO_REJECT"

    #@167
    const/16 v2, 0x21

    #@169
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@16c
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->CDMA_SO_REJECT:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@16e
    .line 74
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@170
    const-string v1, "CDMA_RETRY_ORDER"

    #@172
    const/16 v2, 0x22

    #@174
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@177
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->CDMA_RETRY_ORDER:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@179
    .line 75
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@17b
    const-string v1, "CDMA_ACCESS_FAILURE"

    #@17d
    const/16 v2, 0x23

    #@17f
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@182
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->CDMA_ACCESS_FAILURE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@184
    .line 76
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@186
    const-string v1, "CDMA_PREEMPTED"

    #@188
    const/16 v2, 0x24

    #@18a
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@18d
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->CDMA_PREEMPTED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@18f
    .line 77
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@191
    const-string v1, "CDMA_NOT_EMERGENCY"

    #@193
    const/16 v2, 0x25

    #@195
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@198
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->CDMA_NOT_EMERGENCY:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@19a
    .line 78
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@19c
    const-string v1, "CDMA_ACCESS_BLOCKED"

    #@19e
    const/16 v2, 0x26

    #@1a0
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@1a3
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->CDMA_ACCESS_BLOCKED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@1a5
    .line 80
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@1a7
    const-string v1, "MS_ACCESS_CLASS_BARRED"

    #@1a9
    const/16 v2, 0x27

    #@1ab
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@1ae
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->MS_ACCESS_CLASS_BARRED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@1b0
    .line 82
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@1b2
    const-string v1, "ERROR_UNSPECIFIED"

    #@1b4
    const/16 v2, 0x28

    #@1b6
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@1b9
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->ERROR_UNSPECIFIED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@1bb
    .line 84
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@1bd
    const-string v1, "NO_ROUTE_TO_DEST"

    #@1bf
    const/16 v2, 0x29

    #@1c1
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@1c4
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->NO_ROUTE_TO_DEST:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@1c6
    .line 85
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@1c8
    const-string v1, "CHANNEL_UNACCEPTABLE"

    #@1ca
    const/16 v2, 0x2a

    #@1cc
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@1cf
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->CHANNEL_UNACCEPTABLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@1d1
    .line 86
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@1d3
    const-string v1, "OPERATOR_DETERMINED_BARRING"

    #@1d5
    const/16 v2, 0x2b

    #@1d7
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@1da
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->OPERATOR_DETERMINED_BARRING:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@1dc
    .line 87
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@1de
    const-string v1, "NO_USER_RESPONDING"

    #@1e0
    const/16 v2, 0x2c

    #@1e2
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@1e5
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->NO_USER_RESPONDING:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@1e7
    .line 88
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@1e9
    const-string v1, "NO_ANSWER"

    #@1eb
    const/16 v2, 0x2d

    #@1ed
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@1f0
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->NO_ANSWER:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@1f2
    .line 89
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@1f4
    const-string v1, "CALL_REJECTED"

    #@1f6
    const/16 v2, 0x2e

    #@1f8
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@1fb
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->CALL_REJECTED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@1fd
    .line 90
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@1ff
    const-string v1, "NUMBER_CHANGED"

    #@201
    const/16 v2, 0x2f

    #@203
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@206
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->NUMBER_CHANGED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@208
    .line 91
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@20a
    const-string v1, "PREEMPTION"

    #@20c
    const/16 v2, 0x30

    #@20e
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@211
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->PREEMPTION:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@213
    .line 92
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@215
    const-string v1, "NON_SELECTED_USER_CLEAR"

    #@217
    const/16 v2, 0x31

    #@219
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@21c
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->NON_SELECTED_USER_CLEAR:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@21e
    .line 93
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@220
    const-string v1, "DEST_OUT_OF_ORDER"

    #@222
    const/16 v2, 0x32

    #@224
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@227
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->DEST_OUT_OF_ORDER:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@229
    .line 94
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@22b
    const-string v1, "FACILITY_REJECTED"

    #@22d
    const/16 v2, 0x33

    #@22f
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@232
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->FACILITY_REJECTED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@234
    .line 95
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@236
    const-string v1, "RESPONSE_TO_STA_ENQ"

    #@238
    const/16 v2, 0x34

    #@23a
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@23d
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->RESPONSE_TO_STA_ENQ:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@23f
    .line 96
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@241
    const-string v1, "NORMAL_UNSPECIFIED"

    #@243
    const/16 v2, 0x35

    #@245
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@248
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->NORMAL_UNSPECIFIED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@24a
    .line 97
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@24c
    const-string v1, "NETWORK_OUT_OF_ORDER"

    #@24e
    const/16 v2, 0x36

    #@250
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@253
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->NETWORK_OUT_OF_ORDER:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@255
    .line 98
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@257
    const-string v1, "TEMPORARY_FAIL"

    #@259
    const/16 v2, 0x37

    #@25b
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@25e
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->TEMPORARY_FAIL:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@260
    .line 99
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@262
    const-string v1, "SWITCHING_EQUIP_CONGESTION"

    #@264
    const/16 v2, 0x38

    #@266
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@269
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->SWITCHING_EQUIP_CONGESTION:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@26b
    .line 100
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@26d
    const-string v1, "ACCESS_INFO_DISCARD"

    #@26f
    const/16 v2, 0x39

    #@271
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@274
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->ACCESS_INFO_DISCARD:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@276
    .line 101
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@278
    const-string v1, "REQUESTED_CIRCUIT_NOT_AVAILABLE"

    #@27a
    const/16 v2, 0x3a

    #@27c
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@27f
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->REQUESTED_CIRCUIT_NOT_AVAILABLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@281
    .line 102
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@283
    const-string v1, "RESOURCE_UNAVAILABLE"

    #@285
    const/16 v2, 0x3b

    #@287
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@28a
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->RESOURCE_UNAVAILABLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@28c
    .line 103
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@28e
    const-string v1, "QOS_UNAVAILABLE"

    #@290
    const/16 v2, 0x3c

    #@292
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@295
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->QOS_UNAVAILABLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@297
    .line 104
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@299
    const-string v1, "REQUESTED_FACILITY_NOT_SUBSCRIBED"

    #@29b
    const/16 v2, 0x3d

    #@29d
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@2a0
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->REQUESTED_FACILITY_NOT_SUBSCRIBED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@2a2
    .line 105
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@2a4
    const-string v1, "INCOMING_CALLS_BARRED_CUG"

    #@2a6
    const/16 v2, 0x3e

    #@2a8
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@2ab
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->INCOMING_CALLS_BARRED_CUG:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@2ad
    .line 106
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@2af
    const-string v1, "BEARER_CAPABILITY_NOT_AUTHORIZED"

    #@2b1
    const/16 v2, 0x3f

    #@2b3
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@2b6
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->BEARER_CAPABILITY_NOT_AUTHORIZED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@2b8
    .line 107
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@2ba
    const-string v1, "BEARER_CAPABILITY_NOT_PRESENTLY_AVAILABLE"

    #@2bc
    const/16 v2, 0x40

    #@2be
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@2c1
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->BEARER_CAPABILITY_NOT_PRESENTLY_AVAILABLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@2c3
    .line 108
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@2c5
    const-string v1, "SERVICE_NOT_AVAILABLE"

    #@2c7
    const/16 v2, 0x41

    #@2c9
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@2cc
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->SERVICE_NOT_AVAILABLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@2ce
    .line 109
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@2d0
    const-string v1, "BEARER_SERVICE_NOT_IMPLEMENTED"

    #@2d2
    const/16 v2, 0x42

    #@2d4
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@2d7
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->BEARER_SERVICE_NOT_IMPLEMENTED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@2d9
    .line 110
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@2db
    const-string v1, "REQUESTED_FACILITY_NOT_IMPLEMENTED"

    #@2dd
    const/16 v2, 0x43

    #@2df
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@2e2
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->REQUESTED_FACILITY_NOT_IMPLEMENTED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@2e4
    .line 111
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@2e6
    const-string v1, "ONLY_RESTRICTED_DIGIT_INFO_BEARER_CAPABILITY"

    #@2e8
    const/16 v2, 0x44

    #@2ea
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@2ed
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->ONLY_RESTRICTED_DIGIT_INFO_BEARER_CAPABILITY:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@2ef
    .line 112
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@2f1
    const-string v1, "SERVICE_NOT_IMPLEMENTED"

    #@2f3
    const/16 v2, 0x45

    #@2f5
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@2f8
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->SERVICE_NOT_IMPLEMENTED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@2fa
    .line 113
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@2fc
    const-string v1, "INVALID_TRANSACTION_ID_VALUE"

    #@2fe
    const/16 v2, 0x46

    #@300
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@303
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->INVALID_TRANSACTION_ID_VALUE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@305
    .line 114
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@307
    const-string v1, "USER_NOT_MEMBER_CUG"

    #@309
    const/16 v2, 0x47

    #@30b
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@30e
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->USER_NOT_MEMBER_CUG:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@310
    .line 115
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@312
    const-string v1, "INCOMPATIBLE_DESTINATION"

    #@314
    const/16 v2, 0x48

    #@316
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@319
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->INCOMPATIBLE_DESTINATION:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@31b
    .line 116
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@31d
    const-string v1, "INVALID_TRANSIT_NETWORK_SELECTION"

    #@31f
    const/16 v2, 0x49

    #@321
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@324
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->INVALID_TRANSIT_NETWORK_SELECTION:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@326
    .line 117
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@328
    const-string v1, "SEMANTICALLY_INCORRECT_MESSAGE"

    #@32a
    const/16 v2, 0x4a

    #@32c
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@32f
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->SEMANTICALLY_INCORRECT_MESSAGE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@331
    .line 118
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@333
    const-string v1, "INVALID_MANDATORY_INFO"

    #@335
    const/16 v2, 0x4b

    #@337
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@33a
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->INVALID_MANDATORY_INFO:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@33c
    .line 119
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@33e
    const-string v1, "MESSAGE_TYPE_NON_COMPATIBLE"

    #@340
    const/16 v2, 0x4c

    #@342
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@345
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->MESSAGE_TYPE_NON_COMPATIBLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@347
    .line 120
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@349
    const-string v1, "MESSAGE_TYPE_NOT_COMPATIBLE"

    #@34b
    const/16 v2, 0x4d

    #@34d
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@350
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->MESSAGE_TYPE_NOT_COMPATIBLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@352
    .line 121
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@354
    const-string v1, "INFORMATION_ELEMENT_NOT_IMPLEMENTED"

    #@356
    const/16 v2, 0x4e

    #@358
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@35b
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->INFORMATION_ELEMENT_NOT_IMPLEMENTED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@35d
    .line 122
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@35f
    const-string v1, "CONDITIONAL_IE_ERROR"

    #@361
    const/16 v2, 0x4f

    #@363
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@366
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->CONDITIONAL_IE_ERROR:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@368
    .line 123
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@36a
    const-string v1, "MESSAGE_NOT_COMPATIBLE"

    #@36c
    const/16 v2, 0x50

    #@36e
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@371
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->MESSAGE_NOT_COMPATIBLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@373
    .line 124
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@375
    const-string v1, "RECOVER_ON_TIMER_EXPIRY"

    #@377
    const/16 v2, 0x51

    #@379
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@37c
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->RECOVER_ON_TIMER_EXPIRY:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@37e
    .line 125
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@380
    const-string v1, "PROTOCOL_ERROR"

    #@382
    const/16 v2, 0x52

    #@384
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@387
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->PROTOCOL_ERROR:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@389
    .line 126
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@38b
    const-string v1, "INTERWORKING"

    #@38d
    const/16 v2, 0x53

    #@38f
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@392
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->INTERWORKING:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@394
    .line 129
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@396
    const-string v1, "INVALID_SIM"

    #@398
    const/16 v2, 0x54

    #@39a
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@39d
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->INVALID_SIM:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@39f
    .line 130
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@3a1
    const-string v1, "WRONG_STATE"

    #@3a3
    const/16 v2, 0x55

    #@3a5
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@3a8
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->WRONG_STATE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@3aa
    .line 131
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@3ac
    const-string v1, "ACCESS_CLASS_BLOCKED"

    #@3ae
    const/16 v2, 0x56

    #@3b0
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@3b3
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->ACCESS_CLASS_BLOCKED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@3b5
    .line 132
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@3b7
    const-string v1, "NO_RESOURCES"

    #@3b9
    const/16 v2, 0x57

    #@3bb
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@3be
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->NO_RESOURCES:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@3c0
    .line 133
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@3c2
    const-string v1, "INVALID_USER_DATA"

    #@3c4
    const/16 v2, 0x58

    #@3c6
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@3c9
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->INVALID_USER_DATA:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@3cb
    .line 134
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@3cd
    const-string v1, "TIMER_T3230_EXPIRED"

    #@3cf
    const/16 v2, 0x59

    #@3d1
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@3d4
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->TIMER_T3230_EXPIRED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@3d6
    .line 135
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@3d8
    const-string v1, "NO_CELL_AVAILABLE"

    #@3da
    const/16 v2, 0x5a

    #@3dc
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@3df
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->NO_CELL_AVAILABLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@3e1
    .line 136
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@3e3
    const-string v1, "TIMER_T303_EXPIRED"

    #@3e5
    const/16 v2, 0x5b

    #@3e7
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@3ea
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->TIMER_T303_EXPIRED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@3ec
    .line 137
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@3ee
    const-string v1, "CNM_MM_REL_PENDING"

    #@3f0
    const/16 v2, 0x5c

    #@3f2
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@3f5
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->CNM_MM_REL_PENDING:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@3f7
    .line 138
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@3f9
    const-string v1, "ACCESS_STRATUM_REJ_RR_REL_IND"

    #@3fb
    const/16 v2, 0x5d

    #@3fd
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@400
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->ACCESS_STRATUM_REJ_RR_REL_IND:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@402
    .line 139
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@404
    const-string v1, "ACCESS_STRATUM_REJ_RR_RANDOM_ACCESS_FAILURE"

    #@406
    const/16 v2, 0x5e

    #@408
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@40b
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->ACCESS_STRATUM_REJ_RR_RANDOM_ACCESS_FAILURE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@40d
    .line 140
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@40f
    const-string v1, "ACCESS_STRATUM_REJ_RRC_REL_IND"

    #@411
    const/16 v2, 0x5f

    #@413
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@416
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->ACCESS_STRATUM_REJ_RRC_REL_IND:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@418
    .line 141
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@41a
    const-string v1, "ACCESS_STRATUM_REJ_RRC_CLOSE_SESSION_IND"

    #@41c
    const/16 v2, 0x60

    #@41e
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@421
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->ACCESS_STRATUM_REJ_RRC_CLOSE_SESSION_IND:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@423
    .line 142
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@425
    const-string v1, "ACCESS_STRATUM_REJ_RRC_OPEN_SESSION_FAILURE"

    #@427
    const/16 v2, 0x61

    #@429
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@42c
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->ACCESS_STRATUM_REJ_RRC_OPEN_SESSION_FAILURE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@42e
    .line 143
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@430
    const-string v1, "ACCESS_STRATUM_REJ_LOW_LEVEL_FAIL"

    #@432
    const/16 v2, 0x62

    #@434
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@437
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->ACCESS_STRATUM_REJ_LOW_LEVEL_FAIL:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@439
    .line 144
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@43b
    const-string v1, "ACCESS_STRATUM_REJ_LOW_LEVEL_FAIL_REDIAL_NOT_ALLOWED"

    #@43d
    const/16 v2, 0x63

    #@43f
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@442
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->ACCESS_STRATUM_REJ_LOW_LEVEL_FAIL_REDIAL_NOT_ALLOWED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@444
    .line 145
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@446
    const-string v1, "ACCESS_STRATUM_REJ_LOW_LEVEL_IMMED_RETRY"

    #@448
    const/16 v2, 0x64

    #@44a
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@44d
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->ACCESS_STRATUM_REJ_LOW_LEVEL_IMMED_RETRY:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@44f
    .line 146
    new-instance v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@451
    const-string v1, "ACCESS_STRATUM_REJ_ABORT_RADIO_UNAVAILABLE"

    #@453
    const/16 v2, 0x65

    #@455
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/Connection$DisconnectCause;-><init>(Ljava/lang/String;I)V

    #@458
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->ACCESS_STRATUM_REJ_ABORT_RADIO_UNAVAILABLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@45a
    .line 39
    const/16 v0, 0x66

    #@45c
    new-array v0, v0, [Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@45e
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->NOT_DISCONNECTED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@460
    aput-object v1, v0, v3

    #@462
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->INCOMING_MISSED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@464
    aput-object v1, v0, v4

    #@466
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->NORMAL:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@468
    aput-object v1, v0, v5

    #@46a
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->LOCAL:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@46c
    aput-object v1, v0, v6

    #@46e
    sget-object v1, Lcom/android/internal/telephony/Connection$DisconnectCause;->BUSY:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@470
    aput-object v1, v0, v7

    #@472
    const/4 v1, 0x5

    #@473
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->CONGESTION:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@475
    aput-object v2, v0, v1

    #@477
    const/4 v1, 0x6

    #@478
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->MMI:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@47a
    aput-object v2, v0, v1

    #@47c
    const/4 v1, 0x7

    #@47d
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->INVALID_NUMBER:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@47f
    aput-object v2, v0, v1

    #@481
    const/16 v1, 0x8

    #@483
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->NUMBER_UNREACHABLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@485
    aput-object v2, v0, v1

    #@487
    const/16 v1, 0x9

    #@489
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->SERVER_UNREACHABLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@48b
    aput-object v2, v0, v1

    #@48d
    const/16 v1, 0xa

    #@48f
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->INVALID_CREDENTIALS:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@491
    aput-object v2, v0, v1

    #@493
    const/16 v1, 0xb

    #@495
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->OUT_OF_NETWORK:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@497
    aput-object v2, v0, v1

    #@499
    const/16 v1, 0xc

    #@49b
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->SERVER_ERROR:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@49d
    aput-object v2, v0, v1

    #@49f
    const/16 v1, 0xd

    #@4a1
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->TIMED_OUT:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@4a3
    aput-object v2, v0, v1

    #@4a5
    const/16 v1, 0xe

    #@4a7
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->LOST_SIGNAL:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@4a9
    aput-object v2, v0, v1

    #@4ab
    const/16 v1, 0xf

    #@4ad
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->LIMIT_EXCEEDED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@4af
    aput-object v2, v0, v1

    #@4b1
    const/16 v1, 0x10

    #@4b3
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->INCOMING_REJECTED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@4b5
    aput-object v2, v0, v1

    #@4b7
    const/16 v1, 0x11

    #@4b9
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->POWER_OFF:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@4bb
    aput-object v2, v0, v1

    #@4bd
    const/16 v1, 0x12

    #@4bf
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->OUT_OF_SERVICE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@4c1
    aput-object v2, v0, v1

    #@4c3
    const/16 v1, 0x13

    #@4c5
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->ICC_ERROR:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@4c7
    aput-object v2, v0, v1

    #@4c9
    const/16 v1, 0x14

    #@4cb
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->CALL_BARRED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@4cd
    aput-object v2, v0, v1

    #@4cf
    const/16 v1, 0x15

    #@4d1
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->FDN_BLOCKED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@4d3
    aput-object v2, v0, v1

    #@4d5
    const/16 v1, 0x16

    #@4d7
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->CS_RESTRICTED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@4d9
    aput-object v2, v0, v1

    #@4db
    const/16 v1, 0x17

    #@4dd
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->CS_RESTRICTED_NORMAL:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@4df
    aput-object v2, v0, v1

    #@4e1
    const/16 v1, 0x18

    #@4e3
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->CS_RESTRICTED_EMERGENCY:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@4e5
    aput-object v2, v0, v1

    #@4e7
    const/16 v1, 0x19

    #@4e9
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->UNOBTAINABLE_NUMBER:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@4eb
    aput-object v2, v0, v1

    #@4ed
    const/16 v1, 0x1a

    #@4ef
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->DIAL_MODIFIED_TO_USSD:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@4f1
    aput-object v2, v0, v1

    #@4f3
    const/16 v1, 0x1b

    #@4f5
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->DIAL_MODIFIED_TO_SS:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@4f7
    aput-object v2, v0, v1

    #@4f9
    const/16 v1, 0x1c

    #@4fb
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->DIAL_MODIFIED_TO_DIAL:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@4fd
    aput-object v2, v0, v1

    #@4ff
    const/16 v1, 0x1d

    #@501
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->CDMA_LOCKED_UNTIL_POWER_CYCLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@503
    aput-object v2, v0, v1

    #@505
    const/16 v1, 0x1e

    #@507
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->CDMA_DROP:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@509
    aput-object v2, v0, v1

    #@50b
    const/16 v1, 0x1f

    #@50d
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->CDMA_INTERCEPT:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@50f
    aput-object v2, v0, v1

    #@511
    const/16 v1, 0x20

    #@513
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->CDMA_REORDER:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@515
    aput-object v2, v0, v1

    #@517
    const/16 v1, 0x21

    #@519
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->CDMA_SO_REJECT:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@51b
    aput-object v2, v0, v1

    #@51d
    const/16 v1, 0x22

    #@51f
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->CDMA_RETRY_ORDER:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@521
    aput-object v2, v0, v1

    #@523
    const/16 v1, 0x23

    #@525
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->CDMA_ACCESS_FAILURE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@527
    aput-object v2, v0, v1

    #@529
    const/16 v1, 0x24

    #@52b
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->CDMA_PREEMPTED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@52d
    aput-object v2, v0, v1

    #@52f
    const/16 v1, 0x25

    #@531
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->CDMA_NOT_EMERGENCY:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@533
    aput-object v2, v0, v1

    #@535
    const/16 v1, 0x26

    #@537
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->CDMA_ACCESS_BLOCKED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@539
    aput-object v2, v0, v1

    #@53b
    const/16 v1, 0x27

    #@53d
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->MS_ACCESS_CLASS_BARRED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@53f
    aput-object v2, v0, v1

    #@541
    const/16 v1, 0x28

    #@543
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->ERROR_UNSPECIFIED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@545
    aput-object v2, v0, v1

    #@547
    const/16 v1, 0x29

    #@549
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->NO_ROUTE_TO_DEST:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@54b
    aput-object v2, v0, v1

    #@54d
    const/16 v1, 0x2a

    #@54f
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->CHANNEL_UNACCEPTABLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@551
    aput-object v2, v0, v1

    #@553
    const/16 v1, 0x2b

    #@555
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->OPERATOR_DETERMINED_BARRING:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@557
    aput-object v2, v0, v1

    #@559
    const/16 v1, 0x2c

    #@55b
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->NO_USER_RESPONDING:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@55d
    aput-object v2, v0, v1

    #@55f
    const/16 v1, 0x2d

    #@561
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->NO_ANSWER:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@563
    aput-object v2, v0, v1

    #@565
    const/16 v1, 0x2e

    #@567
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->CALL_REJECTED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@569
    aput-object v2, v0, v1

    #@56b
    const/16 v1, 0x2f

    #@56d
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->NUMBER_CHANGED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@56f
    aput-object v2, v0, v1

    #@571
    const/16 v1, 0x30

    #@573
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->PREEMPTION:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@575
    aput-object v2, v0, v1

    #@577
    const/16 v1, 0x31

    #@579
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->NON_SELECTED_USER_CLEAR:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@57b
    aput-object v2, v0, v1

    #@57d
    const/16 v1, 0x32

    #@57f
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->DEST_OUT_OF_ORDER:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@581
    aput-object v2, v0, v1

    #@583
    const/16 v1, 0x33

    #@585
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->FACILITY_REJECTED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@587
    aput-object v2, v0, v1

    #@589
    const/16 v1, 0x34

    #@58b
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->RESPONSE_TO_STA_ENQ:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@58d
    aput-object v2, v0, v1

    #@58f
    const/16 v1, 0x35

    #@591
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->NORMAL_UNSPECIFIED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@593
    aput-object v2, v0, v1

    #@595
    const/16 v1, 0x36

    #@597
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->NETWORK_OUT_OF_ORDER:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@599
    aput-object v2, v0, v1

    #@59b
    const/16 v1, 0x37

    #@59d
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->TEMPORARY_FAIL:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@59f
    aput-object v2, v0, v1

    #@5a1
    const/16 v1, 0x38

    #@5a3
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->SWITCHING_EQUIP_CONGESTION:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@5a5
    aput-object v2, v0, v1

    #@5a7
    const/16 v1, 0x39

    #@5a9
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->ACCESS_INFO_DISCARD:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@5ab
    aput-object v2, v0, v1

    #@5ad
    const/16 v1, 0x3a

    #@5af
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->REQUESTED_CIRCUIT_NOT_AVAILABLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@5b1
    aput-object v2, v0, v1

    #@5b3
    const/16 v1, 0x3b

    #@5b5
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->RESOURCE_UNAVAILABLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@5b7
    aput-object v2, v0, v1

    #@5b9
    const/16 v1, 0x3c

    #@5bb
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->QOS_UNAVAILABLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@5bd
    aput-object v2, v0, v1

    #@5bf
    const/16 v1, 0x3d

    #@5c1
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->REQUESTED_FACILITY_NOT_SUBSCRIBED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@5c3
    aput-object v2, v0, v1

    #@5c5
    const/16 v1, 0x3e

    #@5c7
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->INCOMING_CALLS_BARRED_CUG:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@5c9
    aput-object v2, v0, v1

    #@5cb
    const/16 v1, 0x3f

    #@5cd
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->BEARER_CAPABILITY_NOT_AUTHORIZED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@5cf
    aput-object v2, v0, v1

    #@5d1
    const/16 v1, 0x40

    #@5d3
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->BEARER_CAPABILITY_NOT_PRESENTLY_AVAILABLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@5d5
    aput-object v2, v0, v1

    #@5d7
    const/16 v1, 0x41

    #@5d9
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->SERVICE_NOT_AVAILABLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@5db
    aput-object v2, v0, v1

    #@5dd
    const/16 v1, 0x42

    #@5df
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->BEARER_SERVICE_NOT_IMPLEMENTED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@5e1
    aput-object v2, v0, v1

    #@5e3
    const/16 v1, 0x43

    #@5e5
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->REQUESTED_FACILITY_NOT_IMPLEMENTED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@5e7
    aput-object v2, v0, v1

    #@5e9
    const/16 v1, 0x44

    #@5eb
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->ONLY_RESTRICTED_DIGIT_INFO_BEARER_CAPABILITY:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@5ed
    aput-object v2, v0, v1

    #@5ef
    const/16 v1, 0x45

    #@5f1
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->SERVICE_NOT_IMPLEMENTED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@5f3
    aput-object v2, v0, v1

    #@5f5
    const/16 v1, 0x46

    #@5f7
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->INVALID_TRANSACTION_ID_VALUE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@5f9
    aput-object v2, v0, v1

    #@5fb
    const/16 v1, 0x47

    #@5fd
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->USER_NOT_MEMBER_CUG:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@5ff
    aput-object v2, v0, v1

    #@601
    const/16 v1, 0x48

    #@603
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->INCOMPATIBLE_DESTINATION:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@605
    aput-object v2, v0, v1

    #@607
    const/16 v1, 0x49

    #@609
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->INVALID_TRANSIT_NETWORK_SELECTION:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@60b
    aput-object v2, v0, v1

    #@60d
    const/16 v1, 0x4a

    #@60f
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->SEMANTICALLY_INCORRECT_MESSAGE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@611
    aput-object v2, v0, v1

    #@613
    const/16 v1, 0x4b

    #@615
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->INVALID_MANDATORY_INFO:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@617
    aput-object v2, v0, v1

    #@619
    const/16 v1, 0x4c

    #@61b
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->MESSAGE_TYPE_NON_COMPATIBLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@61d
    aput-object v2, v0, v1

    #@61f
    const/16 v1, 0x4d

    #@621
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->MESSAGE_TYPE_NOT_COMPATIBLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@623
    aput-object v2, v0, v1

    #@625
    const/16 v1, 0x4e

    #@627
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->INFORMATION_ELEMENT_NOT_IMPLEMENTED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@629
    aput-object v2, v0, v1

    #@62b
    const/16 v1, 0x4f

    #@62d
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->CONDITIONAL_IE_ERROR:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@62f
    aput-object v2, v0, v1

    #@631
    const/16 v1, 0x50

    #@633
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->MESSAGE_NOT_COMPATIBLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@635
    aput-object v2, v0, v1

    #@637
    const/16 v1, 0x51

    #@639
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->RECOVER_ON_TIMER_EXPIRY:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@63b
    aput-object v2, v0, v1

    #@63d
    const/16 v1, 0x52

    #@63f
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->PROTOCOL_ERROR:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@641
    aput-object v2, v0, v1

    #@643
    const/16 v1, 0x53

    #@645
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->INTERWORKING:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@647
    aput-object v2, v0, v1

    #@649
    const/16 v1, 0x54

    #@64b
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->INVALID_SIM:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@64d
    aput-object v2, v0, v1

    #@64f
    const/16 v1, 0x55

    #@651
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->WRONG_STATE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@653
    aput-object v2, v0, v1

    #@655
    const/16 v1, 0x56

    #@657
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->ACCESS_CLASS_BLOCKED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@659
    aput-object v2, v0, v1

    #@65b
    const/16 v1, 0x57

    #@65d
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->NO_RESOURCES:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@65f
    aput-object v2, v0, v1

    #@661
    const/16 v1, 0x58

    #@663
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->INVALID_USER_DATA:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@665
    aput-object v2, v0, v1

    #@667
    const/16 v1, 0x59

    #@669
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->TIMER_T3230_EXPIRED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@66b
    aput-object v2, v0, v1

    #@66d
    const/16 v1, 0x5a

    #@66f
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->NO_CELL_AVAILABLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@671
    aput-object v2, v0, v1

    #@673
    const/16 v1, 0x5b

    #@675
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->TIMER_T303_EXPIRED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@677
    aput-object v2, v0, v1

    #@679
    const/16 v1, 0x5c

    #@67b
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->CNM_MM_REL_PENDING:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@67d
    aput-object v2, v0, v1

    #@67f
    const/16 v1, 0x5d

    #@681
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->ACCESS_STRATUM_REJ_RR_REL_IND:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@683
    aput-object v2, v0, v1

    #@685
    const/16 v1, 0x5e

    #@687
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->ACCESS_STRATUM_REJ_RR_RANDOM_ACCESS_FAILURE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@689
    aput-object v2, v0, v1

    #@68b
    const/16 v1, 0x5f

    #@68d
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->ACCESS_STRATUM_REJ_RRC_REL_IND:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@68f
    aput-object v2, v0, v1

    #@691
    const/16 v1, 0x60

    #@693
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->ACCESS_STRATUM_REJ_RRC_CLOSE_SESSION_IND:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@695
    aput-object v2, v0, v1

    #@697
    const/16 v1, 0x61

    #@699
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->ACCESS_STRATUM_REJ_RRC_OPEN_SESSION_FAILURE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@69b
    aput-object v2, v0, v1

    #@69d
    const/16 v1, 0x62

    #@69f
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->ACCESS_STRATUM_REJ_LOW_LEVEL_FAIL:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@6a1
    aput-object v2, v0, v1

    #@6a3
    const/16 v1, 0x63

    #@6a5
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->ACCESS_STRATUM_REJ_LOW_LEVEL_FAIL_REDIAL_NOT_ALLOWED:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@6a7
    aput-object v2, v0, v1

    #@6a9
    const/16 v1, 0x64

    #@6ab
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->ACCESS_STRATUM_REJ_LOW_LEVEL_IMMED_RETRY:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@6ad
    aput-object v2, v0, v1

    #@6af
    const/16 v1, 0x65

    #@6b1
    sget-object v2, Lcom/android/internal/telephony/Connection$DisconnectCause;->ACCESS_STRATUM_REJ_ABORT_RADIO_UNAVAILABLE:Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@6b3
    aput-object v2, v0, v1

    #@6b5
    sput-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->$VALUES:[Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@6b7
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/internal/telephony/Connection$DisconnectCause;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 39
    const-class v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/android/internal/telephony/Connection$DisconnectCause;
    .registers 1

    #@0
    .prologue
    .line 39
    sget-object v0, Lcom/android/internal/telephony/Connection$DisconnectCause;->$VALUES:[Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@2
    invoke-virtual {v0}, [Lcom/android/internal/telephony/Connection$DisconnectCause;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/android/internal/telephony/Connection$DisconnectCause;

    #@8
    return-object v0
.end method
