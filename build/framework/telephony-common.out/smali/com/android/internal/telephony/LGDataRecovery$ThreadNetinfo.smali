.class public Lcom/android/internal/telephony/LGDataRecovery$ThreadNetinfo;
.super Ljava/lang/Thread;
.source "LGDataRecovery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/LGDataRecovery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ThreadNetinfo"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/LGDataRecovery;


# direct methods
.method public constructor <init>(Lcom/android/internal/telephony/LGDataRecovery;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1933
    iput-object p1, p0, Lcom/android/internal/telephony/LGDataRecovery$ThreadNetinfo;->this$0:Lcom/android/internal/telephony/LGDataRecovery;

    #@2
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public run()V
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 1938
    const-string v3, "juhwan. thread 1"

    #@3
    invoke-static {v3}, Lcom/android/internal/telephony/LGDataRecovery;->log(Ljava/lang/String;)V

    #@6
    .line 1940
    const-string v3, "********** DUMP NETWORK INFO **********"

    #@8
    const-string v4, "netinfo.log"

    #@a
    invoke-static {v3, v4}, Lcom/android/internal/telephony/LGDataRecovery;->access$400(Ljava/lang/String;Ljava/lang/String;)V

    #@d
    .line 1954
    const-string v3, "<----- proc/net/route(ipv6_route), converted address ----->"

    #@f
    const-string v4, "netinfo.log"

    #@11
    invoke-static {v3, v4}, Lcom/android/internal/telephony/LGDataRecovery;->access$400(Ljava/lang/String;Ljava/lang/String;)V

    #@14
    .line 1955
    iget-object v3, p0, Lcom/android/internal/telephony/LGDataRecovery$ThreadNetinfo;->this$0:Lcom/android/internal/telephony/LGDataRecovery;

    #@16
    invoke-static {v3}, Lcom/android/internal/telephony/LGDataRecovery;->access$500(Lcom/android/internal/telephony/LGDataRecovery;)V

    #@19
    .line 1957
    const-string v3, "<----- netcfg ----->"

    #@1b
    const-string v4, "netinfo.log"

    #@1d
    invoke-static {v3, v4}, Lcom/android/internal/telephony/LGDataRecovery;->access$400(Ljava/lang/String;Ljava/lang/String;)V

    #@20
    .line 1958
    iget-object v3, p0, Lcom/android/internal/telephony/LGDataRecovery$ThreadNetinfo;->this$0:Lcom/android/internal/telephony/LGDataRecovery;

    #@22
    const-string v4, "netcfg"

    #@24
    invoke-static {v3, v4}, Lcom/android/internal/telephony/LGDataRecovery;->access$600(Lcom/android/internal/telephony/LGDataRecovery;Ljava/lang/String;)Ljava/util/ArrayList;

    #@27
    move-result-object v3

    #@28
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@2b
    move-result-object v1

    #@2c
    .local v1, i$:Ljava/util/Iterator;
    :goto_2c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@2f
    move-result v3

    #@30
    if-eqz v3, :cond_3e

    #@32
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@35
    move-result-object v2

    #@36
    check-cast v2, Ljava/lang/String;

    #@38
    .line 1959
    .local v2, line:Ljava/lang/String;
    const-string v3, "netinfo.log"

    #@3a
    invoke-static {v2, v3, v6}, Lcom/android/internal/telephony/LGDataRecovery;->access$700(Ljava/lang/String;Ljava/lang/String;Z)V

    #@3d
    goto :goto_2c

    #@3e
    .line 1962
    .end local v2           #line:Ljava/lang/String;
    :cond_3e
    iget-object v3, p0, Lcom/android/internal/telephony/LGDataRecovery$ThreadNetinfo;->this$0:Lcom/android/internal/telephony/LGDataRecovery;

    #@40
    invoke-virtual {v3}, Lcom/android/internal/telephony/LGDataRecovery;->findDefaultConnInfo()Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;

    #@43
    move-result-object v0

    #@44
    .line 1963
    .local v0, conn:Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;
    if-eqz v0, :cond_97

    #@46
    .line 1964
    new-instance v3, Ljava/lang/StringBuilder;

    #@48
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@4b
    const-string v4, "<----- proc/net/xt_quota/"

    #@4d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v3

    #@51
    iget-object v4, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->iface:Ljava/lang/String;

    #@53
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v3

    #@57
    const-string v4, " ----->"

    #@59
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v3

    #@5d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@60
    move-result-object v3

    #@61
    const-string v4, "netinfo.log"

    #@63
    invoke-static {v3, v4}, Lcom/android/internal/telephony/LGDataRecovery;->access$400(Ljava/lang/String;Ljava/lang/String;)V

    #@66
    .line 1965
    iget-object v3, p0, Lcom/android/internal/telephony/LGDataRecovery$ThreadNetinfo;->this$0:Lcom/android/internal/telephony/LGDataRecovery;

    #@68
    new-instance v4, Ljava/lang/StringBuilder;

    #@6a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@6d
    const-string v5, "proc/net/xt_quota/"

    #@6f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v4

    #@73
    iget-object v5, v0, Lcom/android/internal/telephony/LGDataRecovery$ConnectionInfo;->iface:Ljava/lang/String;

    #@75
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v4

    #@79
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7c
    move-result-object v4

    #@7d
    invoke-virtual {v3, v4}, Lcom/android/internal/telephony/LGDataRecovery;->getFileSystemInfo(Ljava/lang/String;)Ljava/util/ArrayList;

    #@80
    move-result-object v3

    #@81
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@84
    move-result-object v1

    #@85
    :goto_85
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@88
    move-result v3

    #@89
    if-eqz v3, :cond_97

    #@8b
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@8e
    move-result-object v2

    #@8f
    check-cast v2, Ljava/lang/String;

    #@91
    .line 1967
    .restart local v2       #line:Ljava/lang/String;
    const-string v3, "netinfo.log"

    #@93
    invoke-static {v2, v3, v6}, Lcom/android/internal/telephony/LGDataRecovery;->access$700(Ljava/lang/String;Ljava/lang/String;Z)V

    #@96
    goto :goto_85

    #@97
    .line 1971
    .end local v2           #line:Ljava/lang/String;
    :cond_97
    const-string v3, "<----- proc/net/dev ----->"

    #@99
    const-string v4, "netinfo.log"

    #@9b
    invoke-static {v3, v4}, Lcom/android/internal/telephony/LGDataRecovery;->access$400(Ljava/lang/String;Ljava/lang/String;)V

    #@9e
    .line 1972
    iget-object v3, p0, Lcom/android/internal/telephony/LGDataRecovery$ThreadNetinfo;->this$0:Lcom/android/internal/telephony/LGDataRecovery;

    #@a0
    const-string v4, "proc/net/dev"

    #@a2
    invoke-virtual {v3, v4}, Lcom/android/internal/telephony/LGDataRecovery;->getFileSystemInfo(Ljava/lang/String;)Ljava/util/ArrayList;

    #@a5
    move-result-object v3

    #@a6
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@a9
    move-result-object v1

    #@aa
    :goto_aa
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@ad
    move-result v3

    #@ae
    if-eqz v3, :cond_bc

    #@b0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@b3
    move-result-object v2

    #@b4
    check-cast v2, Ljava/lang/String;

    #@b6
    .line 1973
    .restart local v2       #line:Ljava/lang/String;
    const-string v3, "netinfo.log"

    #@b8
    invoke-static {v2, v3, v6}, Lcom/android/internal/telephony/LGDataRecovery;->access$700(Ljava/lang/String;Ljava/lang/String;Z)V

    #@bb
    goto :goto_aa

    #@bc
    .line 1975
    .end local v2           #line:Ljava/lang/String;
    :cond_bc
    const-string v3, "\n"

    #@be
    const-string v4, "netinfo.log"

    #@c0
    invoke-static {v3, v4, v6}, Lcom/android/internal/telephony/LGDataRecovery;->access$700(Ljava/lang/String;Ljava/lang/String;Z)V

    #@c3
    .line 1976
    return-void
.end method
