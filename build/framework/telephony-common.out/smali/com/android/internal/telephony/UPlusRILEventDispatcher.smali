.class public final Lcom/android/internal/telephony/UPlusRILEventDispatcher;
.super Landroid/os/Handler;
.source "UPlusRILEventDispatcher.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/UPlusRILEventDispatcher$LockOrderIntentReceiver;
    }
.end annotation


# static fields
.field private static final ACTION_LG_NVITEM_RESET:Ljava/lang/String; = "android.intent.action.LG_NVITEM_RESET"

.field public static final CDMA_LOCK_ORDER:Ljava/lang/String; = "android.intent.action.CDMA_LOCK_ORDER"

.field public static final CDMA_MAINT_REQ:Ljava/lang/String; = "android.intent.action.CDMA_MAINT_REQ"

.field private static final CDMA_RIL_EVENT:Ljava/lang/String; = "android.intent.action.CDMA_RIL_EVENT"

.field static final CONGESTTION:I = 0x16

.field private static final EHRPD_LOCK_ORDER:Ljava/lang/String; = "android.intent.action.EHRPD_LOCK_ORDER"

.field static final EPS_SERVICES_AND_NON_EPS_SERVICES_NOT_ALLOWED:I = 0x8

.field static final EPS_SERVICES_NOT_ALLOWED:I = 0x7

.field static final EPS_SERVICES_NOT_ALLOWED_IN_THIS_PLMN:I = 0xe

.field static final ESM_FAILURE:I = 0x13

.field private static final EVENT_EMM_REJECT:I = 0x16

.field private static final EVENT_HDR_LOCK:I = 0x14

.field private static final EVENT_LGT_FACTORY_RESET:I = 0x13

.field private static final EVENT_LGT_OTA_SESSION_FAIL:I = 0x1

.field private static final EVENT_LGT_OTA_SESSION_SUCCESS:I = 0x2

.field private static final EVENT_LGT_ROAMING_UI_TEST_SET_DONE:I = 0xc

.field private static final EVENT_LGT_SID_CHANGED:I = 0x4

.field private static final EVENT_LGT_WPBX_CHANGED:I = 0x5

.field private static final EVENT_LOCK_STATE_CHANGED:I = 0x3

.field private static final EVENT_LTE_LOCK:I = 0x15

.field private static final EVENT_NET_ERR_DISP:I = 0xf

.field private static final EVENT_RIL_EVENT_DISPATCHER_BASE:I = 0x0

.field static final ILLEGAL_ME:I = 0x6

.field static final ILLEGAL_UE:I = 0x3

.field static final IMEI_NOT_ACCEPTED:I = 0x5

.field static final IMPLICITLY_DETACHED:I = 0xa

.field static final IMSI_NUKNOWN_IN_HSS:I = 0x2

.field static final INFORMATION_ELEMENTNON_EXISTANT_OR_NOT_IMPLEMENTED:I = 0x63

.field static final INVALID_MANDATORY_INFO:I = 0x60

.field public static final LGT_AUTH_LOCK:Ljava/lang/String; = "android.intent.action.LGT_AUTH_LOCK"

.field public static final LGT_HDR_NETWORK_ERROR:Ljava/lang/String; = "android.intent.action.LGT_HDR_NETWORK_ERROR"

.field public static final LGT_OTA_RES_NOTIF_FAIL:Ljava/lang/String; = "android.intent.action.LGT_OTA_RES_NOTIF_FAIL"

.field public static final LGT_OTA_RES_NOTIF_SAME:Ljava/lang/String; = "android.intent.action.LGT_OTA_RES_NOTIF_SAME"

.field public static final LGT_OTA_RES_NOTIF_UPDATE:Ljava/lang/String; = "android.intent.action.LGT_OTA_RES_NOTIF_UPDATE"

.field public static final LGT_SID_CHANGED:Ljava/lang/String; = "android.intent.action.LGT_SID_CHANGED"

.field public static final LGT_WPBX_MATCH:Ljava/lang/String; = "android.intent.action.LGT_WPBX_MATCH"

.field public static final LGT_WPBX_NOMATCH:Ljava/lang/String; = "android.intent.action.LGT_WPBX_NOMATCH"

.field private static final LOG_TAG:Ljava/lang/String; = "CALL_FRW"

.field static final LTE_AUTHENTICATION_REJECT:I = 0x54

.field private static final LTE_EMM_REJECT:Ljava/lang/String; = "android.intent.action.LTE_EMM_REJECT"

.field private static final LTE_LOCK_ORDER:Ljava/lang/String; = "android.intent.action.LTE_LOCK_ORDER"

.field private static final LTE_MISSING_PHONE:Ljava/lang/String; = "com.lge.intent.action.LTE_MISSING_PHONE"

.field static final MAC_FAILURE:I = 0x14

.field static final MESSAGE_TYPE_NONEXISTANT_OR_NOT_IMPLEMENTED:I = 0x61

.field static final MSC_TEMPORARILY_NOT_REACHABLE:I = 0x10

.field static final NETWORK_FAILURE:I = 0x11

.field static final NOT_AUTHORIZED_FOR_THIS_CSG:I = 0x19

.field static final NO_EPS_BEARER_CONTEXT_ACTIVATED:I = 0x28

.field static final NO_SUITABLE_CELLS_IN_TRACKING_AREA:I = 0xf

.field static final PLMN_NOT_ALLOWED:I = 0xb

.field private static final PROPERTY_SERVICE_PROVIDER:Ljava/lang/String; = "ro.telephony.service_provider"

.field static final PROTOCOL_ERROR_UNSPECIFIED:I = 0x6f

.field static final REJECTCAUSE_NOTIFICATION_ID:I = 0xc73b

.field static final ROAMING_NOT_ALLOWED_IN_THIS_TRACKING_AREA:I = 0xd

.field static final SEMANTICALLY_INCORRECT_MSG:I = 0x5f

.field private static final SERVICE_PROVIDER_LGT:Ljava/lang/String; = "LGT"

.field private static final SERVICE_PROVIDER_NONE:Ljava/lang/String; = "None"

.field static final TRACKING_AREA_NOT_ALLOWED:I = 0xc

.field static final UE_IDENTITY_CANNOT_BE_DERIVED_BY_THE_NERWORK:I = 0x9

.field private static rilEventDispatcher:Lcom/android/internal/telephony/UPlusRILEventDispatcher;


# instance fields
.field private curMessageRes:Ljava/lang/String;

.field private debugFilter:Z

.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private final mCm:Lcom/android/internal/telephony/CommandsInterface;

.field private final mContext:Landroid/content/Context;

.field private mEsmRejectNum:I

.field private mIsLGTHDRNetworkError:Z

.field private mIsLGTUnauthenticated:Z

.field private mIsLGTUnregister:Z

.field private mIsLTEAuthError:Z

.field private mIsLTEEMMReject:Z

.field private mLockOrderPopup:Landroid/app/AlertDialog;

.field private mLockOrderReceiver:Landroid/content/BroadcastReceiver;

.field private mRejectNotification:Landroid/app/Notification;

.field private mRejectNum:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 226
    const/4 v0, 0x0

    #@1
    sput-object v0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->rilEventDispatcher:Lcom/android/internal/telephony/UPlusRILEventDispatcher;

    #@3
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;)V
    .registers 8
    .parameter "ctx"
    .parameter "commandsInterface"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    const/4 v3, 0x0

    #@3
    .line 184
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@6
    .line 119
    iput-boolean v4, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->debugFilter:Z

    #@8
    .line 122
    iput-object v3, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mLockOrderReceiver:Landroid/content/BroadcastReceiver;

    #@a
    .line 128
    iput-object v3, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mLockOrderPopup:Landroid/app/AlertDialog;

    #@c
    .line 130
    iput-object v3, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->curMessageRes:Ljava/lang/String;

    #@e
    .line 133
    iput-boolean v1, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mIsLGTUnregister:Z

    #@10
    .line 134
    iput-boolean v1, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mIsLGTUnauthenticated:Z

    #@12
    .line 135
    iput-boolean v1, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mIsLGTHDRNetworkError:Z

    #@14
    .line 140
    iput-boolean v1, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mIsLTEAuthError:Z

    #@16
    .line 144
    iput-boolean v1, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mIsLTEEMMReject:Z

    #@18
    .line 145
    iput v1, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mRejectNum:I

    #@1a
    .line 146
    iput v1, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mEsmRejectNum:I

    #@1c
    .line 500
    new-instance v1, Lcom/android/internal/telephony/UPlusRILEventDispatcher$1;

    #@1e
    invoke-direct {v1, p0}, Lcom/android/internal/telephony/UPlusRILEventDispatcher$1;-><init>(Lcom/android/internal/telephony/UPlusRILEventDispatcher;)V

    #@21
    iput-object v1, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@23
    .line 185
    const-string v1, "UPlusRILEventDispatcher created"

    #@25
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@28
    .line 188
    iput-object p1, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mContext:Landroid/content/Context;

    #@2a
    .line 189
    iput-object p2, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@2c
    .line 191
    new-instance v0, Landroid/content/IntentFilter;

    #@2e
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@31
    .line 192
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.LGT_ROAMING_UI_TEST_LGT"

    #@33
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@36
    .line 193
    const-string v1, "android.intent.action.LGT_ROAMING_UI_TEST_KT"

    #@38
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@3b
    .line 194
    const-string v1, "android.intent.action.LGT_ROAMING_UI_TEST_JCDMA"

    #@3d
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@40
    .line 195
    const-string v1, "android.intent.action.LGT_ROAMING_UI_TEST_DCN"

    #@42
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@45
    .line 196
    const-string v1, "android.intent.action.LG_NVITEM_RESET"

    #@47
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@4a
    .line 197
    iget-object v1, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mContext:Landroid/content/Context;

    #@4c
    iget-object v2, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@4e
    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@51
    .line 199
    iget-object v1, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@53
    invoke-interface {v1, p0, v4, v3}, Lcom/android/internal/telephony/CommandsInterface;->registerForOtaSessionFail(Landroid/os/Handler;ILjava/lang/Object;)V

    #@56
    .line 200
    iget-object v1, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@58
    const/4 v2, 0x2

    #@59
    invoke-interface {v1, p0, v2, v3}, Lcom/android/internal/telephony/CommandsInterface;->registerForOtaSessionSuccess(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5c
    .line 201
    iget-object v1, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@5e
    const/4 v2, 0x3

    #@5f
    invoke-interface {v1, p0, v2, v3}, Lcom/android/internal/telephony/CommandsInterface;->registerForLockStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@62
    .line 202
    iget-object v1, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@64
    const/4 v2, 0x4

    #@65
    invoke-interface {v1, p0, v2, v3}, Lcom/android/internal/telephony/CommandsInterface;->registerForCdmaSidChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@68
    .line 203
    iget-object v1, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@6a
    const/4 v2, 0x5

    #@6b
    invoke-interface {v1, p0, v2, v3}, Lcom/android/internal/telephony/CommandsInterface;->registerForWpbxStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@6e
    .line 206
    iget-object v1, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@70
    const/16 v2, 0xf

    #@72
    invoke-interface {v1, p0, v2, v3}, Lcom/android/internal/telephony/CommandsInterface;->registerForNetworkErrorDisp(Landroid/os/Handler;ILjava/lang/Object;)V

    #@75
    .line 209
    iget-object v1, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@77
    const/16 v2, 0x14

    #@79
    invoke-interface {v1, p0, v2, v3}, Lcom/android/internal/telephony/CommandsInterface;->registerForHdrLock(Landroid/os/Handler;ILjava/lang/Object;)V

    #@7c
    .line 210
    iget-object v1, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@7e
    const/16 v2, 0x15

    #@80
    invoke-interface {v1, p0, v2, v3}, Lcom/android/internal/telephony/CommandsInterface;->registerForLteLock(Landroid/os/Handler;ILjava/lang/Object;)V

    #@83
    .line 211
    iget-object v1, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@85
    const/16 v2, 0x16

    #@87
    invoke-interface {v1, p0, v2, v3}, Lcom/android/internal/telephony/CommandsInterface;->registerForLteEmmReject(Landroid/os/Handler;ILjava/lang/Object;)V

    #@8a
    .line 215
    invoke-direct {p0}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->registerIntentReceivers()V

    #@8d
    .line 219
    const-string v1, "gsm.lge.lte_reject_cause"

    #@8f
    const-string v2, "0"

    #@91
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@94
    .line 220
    const-string v1, "set  : TelephonyProperties.PROPERTY_LTE_REJECT_CAUSE to 0"

    #@96
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@99
    .line 223
    return-void
.end method

.method static synthetic access$000(Lcom/android/internal/telephony/UPlusRILEventDispatcher;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->handleLGTRoamingUITest(I)V

    #@3
    return-void
.end method

.method static synthetic access$100(Lcom/android/internal/telephony/UPlusRILEventDispatcher;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 60
    invoke-direct {p0}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->handleNVItemReset()V

    #@3
    return-void
.end method

.method static synthetic access$1000(Lcom/android/internal/telephony/UPlusRILEventDispatcher;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 60
    invoke-direct {p0}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->setPopUp()V

    #@3
    return-void
.end method

.method static synthetic access$1202(Lcom/android/internal/telephony/UPlusRILEventDispatcher;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 60
    iput-object p1, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mLockOrderPopup:Landroid/app/AlertDialog;

    #@2
    return-object p1
.end method

.method static synthetic access$200(Lcom/android/internal/telephony/UPlusRILEventDispatcher;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mIsLGTUnregister:Z

    #@2
    return v0
.end method

.method static synthetic access$202(Lcom/android/internal/telephony/UPlusRILEventDispatcher;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mIsLGTUnregister:Z

    #@2
    return p1
.end method

.method static synthetic access$300(Lcom/android/internal/telephony/UPlusRILEventDispatcher;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mIsLGTUnauthenticated:Z

    #@2
    return v0
.end method

.method static synthetic access$302(Lcom/android/internal/telephony/UPlusRILEventDispatcher;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mIsLGTUnauthenticated:Z

    #@2
    return p1
.end method

.method static synthetic access$400(Lcom/android/internal/telephony/UPlusRILEventDispatcher;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mIsLGTHDRNetworkError:Z

    #@2
    return v0
.end method

.method static synthetic access$402(Lcom/android/internal/telephony/UPlusRILEventDispatcher;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mIsLGTHDRNetworkError:Z

    #@2
    return p1
.end method

.method static synthetic access$500(Lcom/android/internal/telephony/UPlusRILEventDispatcher;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mIsLTEAuthError:Z

    #@2
    return v0
.end method

.method static synthetic access$502(Lcom/android/internal/telephony/UPlusRILEventDispatcher;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mIsLTEAuthError:Z

    #@2
    return p1
.end method

.method static synthetic access$600(Lcom/android/internal/telephony/UPlusRILEventDispatcher;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mIsLTEEMMReject:Z

    #@2
    return v0
.end method

.method static synthetic access$602(Lcom/android/internal/telephony/UPlusRILEventDispatcher;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mIsLTEEMMReject:Z

    #@2
    return p1
.end method

.method static synthetic access$700(Lcom/android/internal/telephony/UPlusRILEventDispatcher;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget v0, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mRejectNum:I

    #@2
    return v0
.end method

.method static synthetic access$702(Lcom/android/internal/telephony/UPlusRILEventDispatcher;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 60
    iput p1, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mRejectNum:I

    #@2
    return p1
.end method

.method static synthetic access$802(Lcom/android/internal/telephony/UPlusRILEventDispatcher;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 60
    iput p1, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mEsmRejectNum:I

    #@2
    return p1
.end method

.method static synthetic access$900(Lcom/android/internal/telephony/UPlusRILEventDispatcher;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget-object v0, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method public static getUPlusRILEventDispatcher(Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;)Lcom/android/internal/telephony/UPlusRILEventDispatcher;
    .registers 5
    .parameter "ctx"
    .parameter "commandsInterface"

    #@0
    .prologue
    .line 231
    const-string v0, "CALL_FRW"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "getUPlusRILEventDispatcher : rilEventDispatcher="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    sget-object v2, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->rilEventDispatcher:Lcom/android/internal/telephony/UPlusRILEventDispatcher;

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    const-string v2, "ctx="

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    const-string v2, "commandsInterface="

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v1

    #@27
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v1

    #@2b
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    .line 235
    if-eqz p0, :cond_32

    #@30
    if-nez p1, :cond_34

    #@32
    :cond_32
    const/4 v0, 0x0

    #@33
    .line 241
    :goto_33
    return-object v0

    #@34
    .line 237
    :cond_34
    sget-object v0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->rilEventDispatcher:Lcom/android/internal/telephony/UPlusRILEventDispatcher;

    #@36
    if-nez v0, :cond_3f

    #@38
    .line 238
    new-instance v0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;

    #@3a
    invoke-direct {v0, p0, p1}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;-><init>(Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;)V

    #@3d
    sput-object v0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->rilEventDispatcher:Lcom/android/internal/telephony/UPlusRILEventDispatcher;

    #@3f
    .line 241
    :cond_3f
    sget-object v0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->rilEventDispatcher:Lcom/android/internal/telephony/UPlusRILEventDispatcher;

    #@41
    goto :goto_33
.end method

.method private handleHdrLock(Landroid/os/AsyncResult;)V
    .registers 6
    .parameter "ar"

    #@0
    .prologue
    .line 438
    const-string v2, "eHRPD Lock Order Received!"

    #@2
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@5
    .line 440
    iget-object v2, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@7
    if-eqz v2, :cond_f

    #@9
    .line 441
    const-string v2, "Err! eHRPD Lock order"

    #@b
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@e
    .line 452
    :cond_e
    :goto_e
    return-void

    #@f
    .line 443
    :cond_f
    iget-object v2, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@11
    check-cast v2, [I

    #@13
    move-object v1, v2

    #@14
    check-cast v1, [I

    #@16
    .line 446
    .local v1, ints:[I
    const/4 v2, 0x0

    #@17
    aget v2, v1, v2

    #@19
    const/4 v3, 0x1

    #@1a
    if-ne v2, v3, :cond_e

    #@1c
    .line 447
    const-string v2, "send intent EHRPD_LOCK_ORDER!"

    #@1e
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@21
    .line 448
    new-instance v0, Landroid/content/Intent;

    #@23
    const-string v2, "android.intent.action.EHRPD_LOCK_ORDER"

    #@25
    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@28
    .line 449
    .local v0, intent:Landroid/content/Intent;
    iget-object v2, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mContext:Landroid/content/Context;

    #@2a
    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@2d
    goto :goto_e
.end method

.method private handleLGTNetworkError(Landroid/os/AsyncResult;)V
    .registers 5
    .parameter "ar"

    #@0
    .prologue
    .line 418
    iget-object v1, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@2
    if-eqz v1, :cond_21

    #@4
    .line 419
    iget-boolean v1, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->debugFilter:Z

    #@6
    if-eqz v1, :cond_20

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "EVENT_NET_ERR_DISP Err"

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    iget-object v2, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@20
    .line 429
    :cond_20
    :goto_20
    return-void

    #@21
    .line 421
    :cond_21
    iget-object v1, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@23
    check-cast v1, [I

    #@25
    check-cast v1, [I

    #@27
    const/4 v2, 0x0

    #@28
    aget v1, v1, v2

    #@2a
    const/4 v2, 0x1

    #@2b
    if-ne v1, v2, :cond_43

    #@2d
    .line 422
    new-instance v0, Landroid/content/Intent;

    #@2f
    const-string v1, "android.intent.action.LGT_HDR_NETWORK_ERROR"

    #@31
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@34
    .line 423
    .local v0, intent:Landroid/content/Intent;
    iget-object v1, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mContext:Landroid/content/Context;

    #@36
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@39
    .line 424
    iget-boolean v1, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->debugFilter:Z

    #@3b
    if-eqz v1, :cond_20

    #@3d
    const-string v1, "intent LGT_HDR_NETWORK_ERROR send "

    #@3f
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@42
    goto :goto_20

    #@43
    .line 426
    .end local v0           #intent:Landroid/content/Intent;
    :cond_43
    iget-boolean v1, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->debugFilter:Z

    #@45
    if-eqz v1, :cond_20

    #@47
    const-string v1, "DO NOT send intent LGT_HDR_NETWORK_ERROR"

    #@49
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@4c
    goto :goto_20
.end method

.method private handleLGTRoamingUITest(I)V
    .registers 5
    .parameter "Value"

    #@0
    .prologue
    .line 412
    iget-object v0, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    const v1, 0x20030

    #@5
    const/16 v2, 0xc

    #@7
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->obtainMessage(I)Landroid/os/Message;

    #@a
    move-result-object v2

    #@b
    invoke-interface {v0, v1, p1, v2}, Lcom/android/internal/telephony/CommandsInterface;->setModemIntegerItem(IILandroid/os/Message;)V

    #@e
    .line 413
    return-void
.end method

.method private handleLgtOtaSessionFail()V
    .registers 3

    #@0
    .prologue
    .line 322
    const-string v1, "LGT OTA SESSION FAIL"

    #@2
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@5
    .line 325
    new-instance v0, Landroid/content/Intent;

    #@7
    const-string v1, "android.intent.action.LGT_OTA_RES_NOTIF_FAIL"

    #@9
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@c
    .line 326
    .local v0, intent:Landroid/content/Intent;
    iget-object v1, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mContext:Landroid/content/Context;

    #@e
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@11
    .line 327
    return-void
.end method

.method private handleLgtOtaSessionSuccess(Landroid/os/AsyncResult;)V
    .registers 7
    .parameter "ar"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 330
    iget-boolean v2, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->debugFilter:Z

    #@3
    if-eqz v2, :cond_a

    #@5
    const-string v2, "LGT OTA SESSION SUCCESS"

    #@7
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@a
    .line 332
    :cond_a
    iget-object v2, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@c
    if-eqz v2, :cond_14

    #@e
    .line 333
    const-string v2, "Err! CDMA Lock order"

    #@10
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@13
    .line 348
    :cond_13
    :goto_13
    return-void

    #@14
    .line 335
    :cond_14
    iget-object v2, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@16
    check-cast v2, [I

    #@18
    move-object v1, v2

    #@19
    check-cast v1, [I

    #@1b
    .line 338
    .local v1, ints:[I
    aget v2, v1, v4

    #@1d
    const/4 v3, 0x1

    #@1e
    if-ne v2, v3, :cond_32

    #@20
    .line 339
    const-string v2, "send intent LGT_OTA_RES_NOTIF_UPDATE!"

    #@22
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@25
    .line 340
    new-instance v0, Landroid/content/Intent;

    #@27
    const-string v2, "android.intent.action.LGT_OTA_RES_NOTIF_UPDATE"

    #@29
    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@2c
    .line 341
    .local v0, intent:Landroid/content/Intent;
    iget-object v2, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mContext:Landroid/content/Context;

    #@2e
    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@31
    goto :goto_13

    #@32
    .line 342
    .end local v0           #intent:Landroid/content/Intent;
    :cond_32
    aget v2, v1, v4

    #@34
    if-nez v2, :cond_13

    #@36
    .line 343
    const-string v2, "send intent LGT_OTA_RES_NOTIF_SAME!"

    #@38
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@3b
    .line 344
    new-instance v0, Landroid/content/Intent;

    #@3d
    const-string v2, "android.intent.action.LGT_OTA_RES_NOTIF_SAME"

    #@3f
    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@42
    .line 345
    .restart local v0       #intent:Landroid/content/Intent;
    iget-object v2, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mContext:Landroid/content/Context;

    #@44
    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@47
    goto :goto_13
.end method

.method private handleLgtSidChanged(Landroid/os/AsyncResult;)V
    .registers 7
    .parameter "ar"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 377
    iget-boolean v2, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->debugFilter:Z

    #@3
    if-eqz v2, :cond_a

    #@5
    const-string v2, "LGT ROAMING SID Changed!"

    #@7
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@a
    .line 379
    :cond_a
    iget-object v2, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@c
    if-eqz v2, :cond_14

    #@e
    .line 380
    const-string v2, "Err! LGT SID Changed"

    #@10
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@13
    .line 390
    :goto_13
    return-void

    #@14
    .line 382
    :cond_14
    iget-object v2, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@16
    check-cast v2, [I

    #@18
    move-object v1, v2

    #@19
    check-cast v1, [I

    #@1b
    .line 384
    .local v1, ints:[I
    iget-boolean v2, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->debugFilter:Z

    #@1d
    if-eqz v2, :cond_37

    #@1f
    new-instance v2, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v3, "send SID info : "

    #@26
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v2

    #@2a
    aget v3, v1, v4

    #@2c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v2

    #@30
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v2

    #@34
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@37
    .line 386
    :cond_37
    new-instance v0, Landroid/content/Intent;

    #@39
    const-string v2, "android.intent.action.LGT_SID_CHANGED"

    #@3b
    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@3e
    .line 387
    .local v0, intent:Landroid/content/Intent;
    const-string v2, "sid"

    #@40
    aget v3, v1, v4

    #@42
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@45
    .line 388
    iget-object v2, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mContext:Landroid/content/Context;

    #@47
    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@4a
    goto :goto_13
.end method

.method private handleLgtWpbxChanged(Landroid/os/AsyncResult;)V
    .registers 7
    .parameter "ar"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 393
    iget-boolean v2, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->debugFilter:Z

    #@3
    if-eqz v2, :cond_a

    #@5
    const-string v2, "LGT WPBX Match Changed!"

    #@7
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@a
    .line 395
    :cond_a
    iget-object v2, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@c
    if-eqz v2, :cond_14

    #@e
    .line 396
    const-string v2, "Err! LGT WPBX Match"

    #@10
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@13
    .line 409
    :cond_13
    :goto_13
    return-void

    #@14
    .line 398
    :cond_14
    iget-object v2, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@16
    check-cast v2, [I

    #@18
    move-object v1, v2

    #@19
    check-cast v1, [I

    #@1b
    .line 401
    .local v1, ints:[I
    aget v2, v1, v4

    #@1d
    const/4 v3, 0x1

    #@1e
    if-ne v2, v3, :cond_2d

    #@20
    .line 402
    new-instance v0, Landroid/content/Intent;

    #@22
    const-string v2, "android.intent.action.LGT_WPBX_MATCH"

    #@24
    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@27
    .line 403
    .local v0, intent:Landroid/content/Intent;
    iget-object v2, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mContext:Landroid/content/Context;

    #@29
    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@2c
    goto :goto_13

    #@2d
    .line 404
    .end local v0           #intent:Landroid/content/Intent;
    :cond_2d
    aget v2, v1, v4

    #@2f
    if-nez v2, :cond_13

    #@31
    .line 405
    new-instance v0, Landroid/content/Intent;

    #@33
    const-string v2, "android.intent.action.LGT_WPBX_NOMATCH"

    #@35
    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@38
    .line 406
    .restart local v0       #intent:Landroid/content/Intent;
    iget-object v2, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mContext:Landroid/content/Context;

    #@3a
    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@3d
    goto :goto_13
.end method

.method private handleLockStateChanged(Landroid/os/AsyncResult;)V
    .registers 7
    .parameter "ar"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    .line 351
    iget-boolean v2, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->debugFilter:Z

    #@4
    if-eqz v2, :cond_b

    #@6
    const-string v2, "CDMA Lock Order Received!"

    #@8
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@b
    .line 353
    :cond_b
    iget-object v2, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@d
    if-eqz v2, :cond_15

    #@f
    .line 354
    const-string v2, "Err! CDMA Lock order"

    #@11
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@14
    .line 374
    :cond_14
    :goto_14
    return-void

    #@15
    .line 356
    :cond_15
    iget-object v2, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@17
    check-cast v2, [I

    #@19
    move-object v1, v2

    #@1a
    check-cast v1, [I

    #@1c
    .line 359
    .local v1, ints:[I
    aget v2, v1, v4

    #@1e
    if-ne v2, v3, :cond_36

    #@20
    aget v2, v1, v3

    #@22
    if-ne v2, v3, :cond_36

    #@24
    .line 360
    const-string v2, "send intent CDMA_LOCK_ORDER!"

    #@26
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@29
    .line 361
    new-instance v0, Landroid/content/Intent;

    #@2b
    const-string v2, "android.intent.action.CDMA_LOCK_ORDER"

    #@2d
    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@30
    .line 362
    .local v0, intent:Landroid/content/Intent;
    iget-object v2, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mContext:Landroid/content/Context;

    #@32
    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@35
    goto :goto_14

    #@36
    .line 363
    .end local v0           #intent:Landroid/content/Intent;
    :cond_36
    aget v2, v1, v4

    #@38
    if-ne v2, v3, :cond_4c

    #@3a
    .line 364
    const-string v2, "send intent CDMA_MAINT_REQ!"

    #@3c
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@3f
    .line 365
    new-instance v0, Landroid/content/Intent;

    #@41
    const-string v2, "android.intent.action.CDMA_MAINT_REQ"

    #@43
    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@46
    .line 366
    .restart local v0       #intent:Landroid/content/Intent;
    iget-object v2, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mContext:Landroid/content/Context;

    #@48
    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@4b
    goto :goto_14

    #@4c
    .line 368
    .end local v0           #intent:Landroid/content/Intent;
    :cond_4c
    aget v2, v1, v3

    #@4e
    if-ne v2, v3, :cond_14

    #@50
    .line 369
    const-string v2, "send intent LGT_AUTH_LOCK!"

    #@52
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@55
    .line 370
    new-instance v0, Landroid/content/Intent;

    #@57
    const-string v2, "android.intent.action.LGT_AUTH_LOCK"

    #@59
    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@5c
    .line 371
    .restart local v0       #intent:Landroid/content/Intent;
    iget-object v2, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mContext:Landroid/content/Context;

    #@5e
    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@61
    goto :goto_14
.end method

.method private handleLteEmmReject(Landroid/os/AsyncResult;)V
    .registers 11
    .parameter "ar"

    #@0
    .prologue
    const/4 v8, 0x6

    #@1
    const/4 v7, 0x5

    #@2
    const/4 v6, 0x1

    #@3
    const/4 v5, 0x0

    #@4
    .line 473
    const-string v3, "LTE EMM REJECT Received!"

    #@6
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@9
    .line 475
    iget-object v3, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@b
    if-eqz v3, :cond_13

    #@d
    .line 476
    const-string v3, "Err! LTE EMM REJECT order"

    #@f
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@12
    .line 497
    :goto_12
    return-void

    #@13
    .line 478
    :cond_13
    iget-object v3, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@15
    check-cast v3, [I

    #@17
    move-object v1, v3

    #@18
    check-cast v1, [I

    #@1a
    .line 482
    .local v1, ints:[I
    const-string v3, "gsm.lge.lte_reject_cause"

    #@1c
    aget v4, v1, v5

    #@1e
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@21
    move-result-object v4

    #@22
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@25
    .line 483
    const-string v3, "gsm.lge.lte_esm_reject_cause"

    #@27
    aget v4, v1, v6

    #@29
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@2c
    move-result-object v4

    #@2d
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@30
    .line 487
    const-string v3, "persist.radio.last_ltereject"

    #@32
    invoke-static {v3, v5}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@35
    move-result v2

    #@36
    .line 488
    .local v2, lastLteRejectCause:I
    if-eq v2, v7, :cond_3a

    #@38
    if-ne v2, v8, :cond_42

    #@3a
    :cond_3a
    aget v3, v1, v5

    #@3c
    if-eq v3, v7, :cond_5c

    #@3e
    aget v3, v1, v5

    #@40
    if-eq v3, v8, :cond_5c

    #@42
    .line 489
    :cond_42
    new-instance v0, Landroid/content/Intent;

    #@44
    const-string v3, "android.intent.action.LTE_EMM_REJECT"

    #@46
    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@49
    .line 490
    .local v0, intent:Landroid/content/Intent;
    const-string v3, "rejectCode"

    #@4b
    aget v4, v1, v5

    #@4d
    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@50
    .line 491
    const-string v3, "esmRejectCode"

    #@52
    aget v4, v1, v6

    #@54
    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@57
    .line 492
    iget-object v3, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mContext:Landroid/content/Context;

    #@59
    invoke-virtual {v3, v0}, Landroid/content/Context;->sendStickyBroadcast(Landroid/content/Intent;)V

    #@5c
    .line 494
    .end local v0           #intent:Landroid/content/Intent;
    :cond_5c
    const-string v3, "persist.radio.last_ltereject"

    #@5e
    aget v4, v1, v5

    #@60
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@63
    move-result-object v4

    #@64
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@67
    goto :goto_12
.end method

.method private handleLteLock(Landroid/os/AsyncResult;)V
    .registers 5
    .parameter "ar"

    #@0
    .prologue
    .line 455
    const-string v1, "LTE Lock Order Received!"

    #@2
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@5
    .line 457
    iget-object v1, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@7
    if-eqz v1, :cond_f

    #@9
    .line 458
    const-string v1, "Err! LTE Lock order"

    #@b
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@e
    .line 470
    :cond_e
    :goto_e
    return-void

    #@f
    .line 460
    :cond_f
    iget-object v1, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@11
    check-cast v1, [I

    #@13
    move-object v0, v1

    #@14
    check-cast v0, [I

    #@16
    .line 463
    .local v0, ints:[I
    const/4 v1, 0x0

    #@17
    aget v1, v0, v1

    #@19
    const/4 v2, 0x1

    #@1a
    if-ne v1, v2, :cond_e

    #@1c
    .line 464
    const-string v1, "send intent LTE_LOCK_ORDER!"

    #@1e
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@21
    goto :goto_e
.end method

.method private handleNVItemReset()V
    .registers 5

    #@0
    .prologue
    .line 433
    iget-object v0, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@2
    const v1, 0x20036

    #@5
    const/4 v2, 0x0

    #@6
    const/16 v3, 0x13

    #@8
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->obtainMessage(I)Landroid/os/Message;

    #@b
    move-result-object v3

    #@c
    invoke-interface {v0, v1, v2, v3}, Lcom/android/internal/telephony/CommandsInterface;->setModemIntegerItem(IILandroid/os/Message;)V

    #@f
    .line 434
    return-void
.end method

.method private isOtaActivity()Z
    .registers 2

    #@0
    .prologue
    .line 800
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method private registerIntentReceivers()V
    .registers 4

    #@0
    .prologue
    .line 595
    const-string v1, "registerIntentReceivers"

    #@2
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@5
    .line 598
    iget-object v1, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mLockOrderReceiver:Landroid/content/BroadcastReceiver;

    #@7
    if-nez v1, :cond_4f

    #@9
    .line 599
    new-instance v0, Landroid/content/IntentFilter;

    #@b
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@e
    .line 600
    .local v0, lockOrderfilter:Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.CDMA_LOCK_ORDER"

    #@10
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@13
    .line 601
    const-string v1, "android.intent.action.CDMA_MAINT_REQ"

    #@15
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@18
    .line 602
    const-string v1, "android.intent.action.LGT_AUTH_LOCK"

    #@1a
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1d
    .line 603
    const-string v1, "android.intent.action.LGT_HDR_NETWORK_ERROR"

    #@1f
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@22
    .line 606
    const-string v1, "android.intent.action.EHRPD_LOCK_ORDER"

    #@24
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@27
    .line 607
    const-string v1, "android.intent.action.LTE_LOCK_ORDER"

    #@29
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@2c
    .line 608
    const-string v1, "android.intent.action.EHRPD_AN_LOCK"

    #@2e
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@31
    .line 612
    const-string v1, "android.intent.action.LTE_EMM_REJECT"

    #@33
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@36
    .line 616
    const-string v1, "com.lge.intent.action.LTE_MISSING_PHONE"

    #@38
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@3b
    .line 617
    const-string v1, "android.intent.action.BOOT_COMPLETED"

    #@3d
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@40
    .line 619
    new-instance v1, Lcom/android/internal/telephony/UPlusRILEventDispatcher$LockOrderIntentReceiver;

    #@42
    const/4 v2, 0x0

    #@43
    invoke-direct {v1, p0, v2}, Lcom/android/internal/telephony/UPlusRILEventDispatcher$LockOrderIntentReceiver;-><init>(Lcom/android/internal/telephony/UPlusRILEventDispatcher;Lcom/android/internal/telephony/UPlusRILEventDispatcher$1;)V

    #@46
    iput-object v1, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mLockOrderReceiver:Landroid/content/BroadcastReceiver;

    #@48
    .line 620
    iget-object v1, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mContext:Landroid/content/Context;

    #@4a
    iget-object v2, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mLockOrderReceiver:Landroid/content/BroadcastReceiver;

    #@4c
    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@4f
    .line 622
    .end local v0           #lockOrderfilter:Landroid/content/IntentFilter;
    :cond_4f
    return-void
.end method

.method private setPopUp()V
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 646
    const-string v3, "setPopUp"

    #@4
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@7
    .line 648
    const-string v1, ""

    #@9
    .line 649
    .local v1, message:Ljava/lang/String;
    iget-boolean v3, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mIsLGTUnregister:Z

    #@b
    if-eqz v3, :cond_60

    #@d
    .line 650
    const-string v3, "lgt_unregister"

    #@f
    invoke-static {v3}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    .line 695
    :cond_13
    :goto_13
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@16
    move-result v3

    #@17
    if-nez v3, :cond_22

    #@19
    invoke-direct {p0}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->isOtaActivity()Z

    #@1c
    move-result v3

    #@1d
    if-nez v3, :cond_22

    #@1f
    .line 696
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->showPopUp(Ljava/lang/String;)V

    #@22
    .line 701
    :cond_22
    new-instance v3, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v4, "CDMA_RIL_EVENT++ "

    #@29
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v3

    #@2d
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v3

    #@31
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v3

    #@35
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@38
    .line 702
    new-instance v0, Landroid/content/Intent;

    #@3a
    const-string v3, "android.intent.action.CDMA_RIL_EVENT"

    #@3c
    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@3f
    .line 703
    .local v0, intent:Landroid/content/Intent;
    const-string v3, "Event_Type"

    #@41
    invoke-virtual {v0, v3, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@44
    .line 704
    iget-object v3, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mContext:Landroid/content/Context;

    #@46
    invoke-virtual {v3, v0}, Landroid/content/Context;->sendStickyBroadcast(Landroid/content/Intent;)V

    #@49
    .line 705
    new-instance v3, Ljava/lang/StringBuilder;

    #@4b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@4e
    const-string v4, "CDMA_RIL_EVENT-- : "

    #@50
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v3

    #@54
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v3

    #@58
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5b
    move-result-object v3

    #@5c
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@5f
    .line 708
    return-void

    #@60
    .line 651
    .end local v0           #intent:Landroid/content/Intent;
    :cond_60
    iget-boolean v3, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mIsLGTUnauthenticated:Z

    #@62
    if-eqz v3, :cond_6b

    #@64
    .line 652
    const-string v3, "lgt_unauthenticated"

    #@66
    invoke-static {v3}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@69
    move-result-object v1

    #@6a
    goto :goto_13

    #@6b
    .line 653
    :cond_6b
    iget-boolean v3, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mIsLGTHDRNetworkError:Z

    #@6d
    if-eqz v3, :cond_76

    #@6f
    .line 654
    const-string v3, "lgt_hdr_network_error"

    #@71
    invoke-static {v3}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@74
    move-result-object v1

    #@75
    goto :goto_13

    #@76
    .line 657
    :cond_76
    iget-boolean v3, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mIsLTEAuthError:Z

    #@78
    if-eqz v3, :cond_81

    #@7a
    .line 658
    const-string v3, "lgt_hdr_network_error"

    #@7c
    invoke-static {v3}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@7f
    move-result-object v1

    #@80
    goto :goto_13

    #@81
    .line 663
    :cond_81
    iget-boolean v3, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mIsLTEEMMReject:Z

    #@83
    if-eqz v3, :cond_13

    #@85
    .line 667
    iget v3, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mRejectNum:I

    #@87
    iget v4, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mEsmRejectNum:I

    #@89
    invoke-static {v3, v4}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->toLteRejectCauseString(II)Ljava/lang/String;

    #@8c
    move-result-object v1

    #@8d
    .line 671
    iget v3, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mRejectNum:I

    #@8f
    const/4 v4, 0x5

    #@90
    if-eq v3, v4, :cond_97

    #@92
    iget v3, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mRejectNum:I

    #@94
    const/4 v4, 0x6

    #@95
    if-ne v3, v4, :cond_13

    #@97
    .line 673
    :cond_97
    iget-object v3, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mContext:Landroid/content/Context;

    #@99
    invoke-static {v3, v1, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@9c
    move-result-object v2

    #@9d
    .line 674
    .local v2, toast:Landroid/widget/Toast;
    const/16 v3, 0x50

    #@9f
    invoke-virtual {v2, v3, v5, v5}, Landroid/widget/Toast;->setGravity(III)V

    #@a2
    .line 675
    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    #@a5
    .line 677
    new-instance v0, Landroid/content/Intent;

    #@a7
    const-string v3, "com.android.phone.EmergencyDialer.DIAL"

    #@a9
    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@ac
    .line 678
    .restart local v0       #intent:Landroid/content/Intent;
    const-string v3, "rejectCode"

    #@ae
    iget v4, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mRejectNum:I

    #@b0
    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@b3
    .line 680
    if-eqz v0, :cond_bf

    #@b5
    .line 681
    const/high16 v3, 0x1080

    #@b7
    invoke-virtual {v0, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@ba
    .line 682
    iget-object v3, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mContext:Landroid/content/Context;

    #@bc
    invoke-virtual {v3, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@bf
    .line 684
    :cond_bf
    const-string v1, ""

    #@c1
    .line 685
    iput-boolean v5, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mIsLTEEMMReject:Z

    #@c3
    iput-boolean v5, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mIsLTEAuthError:Z

    #@c5
    iput-boolean v5, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mIsLGTHDRNetworkError:Z

    #@c7
    iput-boolean v5, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mIsLGTUnauthenticated:Z

    #@c9
    iput-boolean v5, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mIsLGTUnregister:Z

    #@cb
    .line 686
    iput v5, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mRejectNum:I

    #@cd
    goto/16 :goto_13
.end method

.method private showPopUp(Ljava/lang/String;)V
    .registers 9
    .parameter "message"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 714
    invoke-static {}, Lcom/android/internal/telephony/TelephonyUtils;->isFactoryMode()Z

    #@4
    move-result v2

    #@5
    if-eqz v2, :cond_8

    #@7
    .line 773
    :cond_7
    :goto_7
    return-void

    #@8
    .line 716
    :cond_8
    new-instance v2, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v3, "showPopUp / message : "

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    const-string v3, ", mLockOrderPopup : "

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    iget-object v3, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mLockOrderPopup:Landroid/app/AlertDialog;

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    const-string v3, ", mRejectNum : "

    #@25
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v2

    #@29
    iget v3, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mRejectNum:I

    #@2b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@36
    .line 719
    iget-object v2, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mLockOrderPopup:Landroid/app/AlertDialog;

    #@38
    if-eqz v2, :cond_4a

    #@3a
    .line 721
    iget-boolean v2, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->debugFilter:Z

    #@3c
    if-eqz v2, :cond_43

    #@3e
    const-string v2, "New messageRes close previous popup"

    #@40
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@43
    .line 722
    :cond_43
    iget-object v2, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mLockOrderPopup:Landroid/app/AlertDialog;

    #@45
    invoke-virtual {v2}, Landroid/app/AlertDialog;->dismiss()V

    #@48
    .line 723
    iput-object v6, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mLockOrderPopup:Landroid/app/AlertDialog;

    #@4a
    .line 729
    :cond_4a
    iget-object v2, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mLockOrderPopup:Landroid/app/AlertDialog;

    #@4c
    if-nez v2, :cond_7

    #@4e
    .line 730
    new-instance v0, Landroid/app/AlertDialog$Builder;

    #@50
    iget-object v2, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mContext:Landroid/content/Context;

    #@52
    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    #@55
    move-result-object v2

    #@56
    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    #@59
    .line 732
    .local v0, ad:Landroid/app/AlertDialog$Builder;
    const-string v2, "showPopUp / new AlertDialog.Builder"

    #@5b
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@5e
    .line 734
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@61
    .line 736
    const-string v2, "showPopUp / setMessage"

    #@63
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@66
    .line 738
    const-string v2, "telephony_dialog_ok_button"

    #@68
    invoke-static {v2}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@6b
    move-result-object v2

    #@6c
    new-instance v3, Lcom/android/internal/telephony/UPlusRILEventDispatcher$2;

    #@6e
    invoke-direct {v3, p0}, Lcom/android/internal/telephony/UPlusRILEventDispatcher$2;-><init>(Lcom/android/internal/telephony/UPlusRILEventDispatcher;)V

    #@71
    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@74
    .line 749
    const-string v2, "showPopUp / setNeutralButton"

    #@76
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@79
    .line 751
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    #@7c
    move-result-object v2

    #@7d
    iput-object v2, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mLockOrderPopup:Landroid/app/AlertDialog;

    #@7f
    .line 752
    iget-object v2, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mLockOrderPopup:Landroid/app/AlertDialog;

    #@81
    invoke-virtual {v2}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    #@84
    move-result-object v2

    #@85
    const/16 v3, 0x7d8

    #@87
    invoke-virtual {v2, v3}, Landroid/view/Window;->setType(I)V

    #@8a
    .line 756
    if-eqz p1, :cond_7

    #@8c
    .line 757
    iget-object v2, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mContext:Landroid/content/Context;

    #@8e
    const-string v3, "notification"

    #@90
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@93
    move-result-object v1

    #@94
    check-cast v1, Landroid/app/NotificationManager;

    #@96
    .line 758
    .local v1, notificationManager:Landroid/app/NotificationManager;
    new-instance v2, Landroid/app/Notification$BigTextStyle;

    #@98
    new-instance v3, Landroid/app/Notification$Builder;

    #@9a
    iget-object v4, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mContext:Landroid/content/Context;

    #@9c
    invoke-virtual {v4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    #@9f
    move-result-object v4

    #@a0
    invoke-direct {v3, v4}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    #@a3
    const-string v4, "UPLUS_ROAMING_FAIL_NOTIFICATION_TITLE"

    #@a5
    invoke-static {v4}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@a8
    move-result-object v4

    #@a9
    invoke-virtual {v3, v4}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    #@ac
    move-result-object v3

    #@ad
    const v4, 0x108008a

    #@b0
    invoke-virtual {v3, v4}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    #@b3
    move-result-object v3

    #@b4
    const-wide/16 v4, 0x0

    #@b6
    invoke-virtual {v3, v4, v5}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    #@b9
    move-result-object v3

    #@ba
    invoke-direct {v2, v3}, Landroid/app/Notification$BigTextStyle;-><init>(Landroid/app/Notification$Builder;)V

    #@bd
    invoke-virtual {v2, p1}, Landroid/app/Notification$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    #@c0
    move-result-object v2

    #@c1
    invoke-virtual {v2}, Landroid/app/Notification$BigTextStyle;->build()Landroid/app/Notification;

    #@c4
    move-result-object v2

    #@c5
    iput-object v2, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mRejectNotification:Landroid/app/Notification;

    #@c7
    .line 764
    iget-object v2, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mRejectNotification:Landroid/app/Notification;

    #@c9
    const/16 v3, 0x20

    #@cb
    iput v3, v2, Landroid/app/Notification;->flags:I

    #@cd
    .line 765
    iget-object v2, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mRejectNotification:Landroid/app/Notification;

    #@cf
    iget-object v3, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mContext:Landroid/content/Context;

    #@d1
    const-string v4, "UPLUS_ROAMING_FAIL_NOTIFICATION_TITLE"

    #@d3
    invoke-static {v4}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@d6
    move-result-object v4

    #@d7
    invoke-virtual {v2, v3, v4, p1, v6}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    #@da
    .line 766
    const v2, 0xc73b

    #@dd
    iget-object v3, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mRejectNotification:Landroid/app/Notification;

    #@df
    invoke-virtual {v1, v2, v3}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    #@e2
    .line 768
    new-instance v2, Ljava/lang/StringBuilder;

    #@e4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@e7
    const-string v3, "reject cause notification msg : "

    #@e9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ec
    move-result-object v2

    #@ed
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f0
    move-result-object v2

    #@f1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f4
    move-result-object v2

    #@f5
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@f8
    goto/16 :goto_7
.end method

.method public static toLteRejectCauseString(II)Ljava/lang/String;
    .registers 5
    .parameter "rejectNum"
    .parameter "esmRejectNum"

    #@0
    .prologue
    .line 807
    if-gtz p0, :cond_4

    #@2
    const/4 v0, 0x0

    #@3
    .line 840
    :goto_3
    return-object v0

    #@4
    .line 810
    :cond_4
    sparse-switch p0, :sswitch_data_64

    #@7
    .line 836
    new-instance v1, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v2, "lgu_lteemmreject"

    #@e
    invoke-static {v2}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    const-string v2, "("

    #@18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    const-string v2, ")"

    #@22
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v1

    #@26
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v0

    #@2a
    .local v0, message:Ljava/lang/String;
    goto :goto_3

    #@2b
    .line 820
    .end local v0           #message:Ljava/lang/String;
    :sswitch_2b
    const-string v0, ""

    #@2d
    .line 821
    .restart local v0       #message:Ljava/lang/String;
    goto :goto_3

    #@2e
    .line 824
    .end local v0           #message:Ljava/lang/String;
    :sswitch_2e
    const/16 v1, 0x8

    #@30
    if-ne p1, v1, :cond_39

    #@32
    .line 825
    const-string v1, "lgu_lteemmreject_19_8"

    #@34
    invoke-static {v1}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@37
    move-result-object v0

    #@38
    .restart local v0       #message:Ljava/lang/String;
    goto :goto_3

    #@39
    .line 827
    .end local v0           #message:Ljava/lang/String;
    :cond_39
    new-instance v1, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string v2, "lgu_lteemmreject"

    #@40
    invoke-static {v2}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@43
    move-result-object v2

    #@44
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v1

    #@48
    const-string v2, "(19-"

    #@4a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v1

    #@4e
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@51
    move-result-object v1

    #@52
    const-string v2, ")"

    #@54
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v1

    #@58
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5b
    move-result-object v0

    #@5c
    .line 829
    .restart local v0       #message:Ljava/lang/String;
    goto :goto_3

    #@5d
    .line 832
    .end local v0           #message:Ljava/lang/String;
    :sswitch_5d
    const-string v1, "lgt_unauthenticated"

    #@5f
    invoke-static {v1}, Lcom/android/internal/telephony/TelephonyUtils;->getTelephonyString(Ljava/lang/String;)Ljava/lang/String;

    #@62
    move-result-object v0

    #@63
    .line 833
    .restart local v0       #message:Ljava/lang/String;
    goto :goto_3

    #@64
    .line 810
    :sswitch_data_64
    .sparse-switch
        0xa -> :sswitch_2b
        0xc -> :sswitch_2b
        0xf -> :sswitch_2b
        0x13 -> :sswitch_2e
        0x16 -> :sswitch_2b
        0x54 -> :sswitch_5d
        0x5f -> :sswitch_2b
        0x60 -> :sswitch_2b
        0x61 -> :sswitch_2b
        0x63 -> :sswitch_2b
    .end sparse-switch
.end method


# virtual methods
.method public dispose()V
    .registers 2

    #@0
    .prologue
    .line 625
    const-string v0, "dispose"

    #@2
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@5
    .line 643
    return-void
.end method

.method protected finalize()V
    .registers 3

    #@0
    .prologue
    .line 846
    const-string v0, "UPlusRILEventDispatcher finalized"

    #@2
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@5
    .line 848
    iget-object v0, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mContext:Landroid/content/Context;

    #@7
    iget-object v1, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@9
    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@c
    .line 849
    iget-object v0, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mContext:Landroid/content/Context;

    #@e
    iget-object v1, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mLockOrderReceiver:Landroid/content/BroadcastReceiver;

    #@10
    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@13
    .line 851
    iget-object v0, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@15
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForOtaSessionFail(Landroid/os/Handler;)V

    #@18
    .line 852
    iget-object v0, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@1a
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForOtaSessionSuccess(Landroid/os/Handler;)V

    #@1d
    .line 853
    iget-object v0, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@1f
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForLockStateChanged(Landroid/os/Handler;)V

    #@22
    .line 854
    iget-object v0, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@24
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForCdmaSidChanged(Landroid/os/Handler;)V

    #@27
    .line 855
    iget-object v0, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@29
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForWpbxStateChanged(Landroid/os/Handler;)V

    #@2c
    .line 856
    iget-object v0, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@2e
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForNetworkErrorDisp(Landroid/os/Handler;)V

    #@31
    .line 857
    iget-object v0, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@33
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForHdrLock(Landroid/os/Handler;)V

    #@36
    .line 858
    iget-object v0, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@38
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForLteLock(Landroid/os/Handler;)V

    #@3b
    .line 859
    iget-object v0, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->mCm:Lcom/android/internal/telephony/CommandsInterface;

    #@3d
    invoke-interface {v0, p0}, Lcom/android/internal/telephony/CommandsInterface;->unregisterForLteEmmReject(Landroid/os/Handler;)V

    #@40
    .line 860
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 5
    .parameter "msg"

    #@0
    .prologue
    .line 249
    iget-boolean v1, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->debugFilter:Z

    #@2
    if-eqz v1, :cond_1c

    #@4
    .line 250
    new-instance v1, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v2, "New RIL Event Message Received : "

    #@b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    iget v2, p1, Landroid/os/Message;->what:I

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@1c
    .line 253
    :cond_1c
    iget v1, p1, Landroid/os/Message;->what:I

    #@1e
    packed-switch v1, :pswitch_data_194

    #@21
    .line 319
    :cond_21
    :goto_21
    :pswitch_21
    return-void

    #@22
    .line 255
    :pswitch_22
    iget-boolean v1, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->debugFilter:Z

    #@24
    if-eqz v1, :cond_3e

    #@26
    .line 256
    new-instance v1, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    const-string v2, "EVENT_LGT_OTA_SESSION_FAIL : "

    #@2d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v1

    #@31
    iget v2, p1, Landroid/os/Message;->what:I

    #@33
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@36
    move-result-object v1

    #@37
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v1

    #@3b
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@3e
    .line 257
    :cond_3e
    invoke-direct {p0}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->handleLgtOtaSessionFail()V

    #@41
    goto :goto_21

    #@42
    .line 261
    :pswitch_42
    iget-boolean v1, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->debugFilter:Z

    #@44
    if-eqz v1, :cond_5e

    #@46
    .line 262
    new-instance v1, Ljava/lang/StringBuilder;

    #@48
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@4b
    const-string v2, "EVENT_LGT_OTA_SESSION_SUCCESS : "

    #@4d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v1

    #@51
    iget v2, p1, Landroid/os/Message;->what:I

    #@53
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@56
    move-result-object v1

    #@57
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v1

    #@5b
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@5e
    .line 263
    :cond_5e
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@60
    check-cast v0, Landroid/os/AsyncResult;

    #@62
    .line 264
    .local v0, ar:Landroid/os/AsyncResult;
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->handleLgtOtaSessionSuccess(Landroid/os/AsyncResult;)V

    #@65
    goto :goto_21

    #@66
    .line 268
    .end local v0           #ar:Landroid/os/AsyncResult;
    :pswitch_66
    iget-boolean v1, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->debugFilter:Z

    #@68
    if-eqz v1, :cond_82

    #@6a
    .line 269
    new-instance v1, Ljava/lang/StringBuilder;

    #@6c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@6f
    const-string v2, "EVENT_LOCK_STATE_CHANGED : "

    #@71
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v1

    #@75
    iget v2, p1, Landroid/os/Message;->what:I

    #@77
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v1

    #@7b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7e
    move-result-object v1

    #@7f
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@82
    .line 270
    :cond_82
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@84
    check-cast v0, Landroid/os/AsyncResult;

    #@86
    .line 271
    .restart local v0       #ar:Landroid/os/AsyncResult;
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->handleLockStateChanged(Landroid/os/AsyncResult;)V

    #@89
    goto :goto_21

    #@8a
    .line 275
    .end local v0           #ar:Landroid/os/AsyncResult;
    :pswitch_8a
    iget-boolean v1, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->debugFilter:Z

    #@8c
    if-eqz v1, :cond_a6

    #@8e
    new-instance v1, Ljava/lang/StringBuilder;

    #@90
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@93
    const-string v2, "EVENT_LGT_SID_CHANGED : "

    #@95
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@98
    move-result-object v1

    #@99
    iget v2, p1, Landroid/os/Message;->what:I

    #@9b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v1

    #@9f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a2
    move-result-object v1

    #@a3
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@a6
    .line 276
    :cond_a6
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@a8
    check-cast v0, Landroid/os/AsyncResult;

    #@aa
    .line 277
    .restart local v0       #ar:Landroid/os/AsyncResult;
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->handleLgtSidChanged(Landroid/os/AsyncResult;)V

    #@ad
    goto/16 :goto_21

    #@af
    .line 281
    .end local v0           #ar:Landroid/os/AsyncResult;
    :pswitch_af
    iget-boolean v1, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->debugFilter:Z

    #@b1
    if-eqz v1, :cond_cb

    #@b3
    new-instance v1, Ljava/lang/StringBuilder;

    #@b5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b8
    const-string v2, "EVENT_LGT_WPBX_CHANGED : "

    #@ba
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v1

    #@be
    iget v2, p1, Landroid/os/Message;->what:I

    #@c0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v1

    #@c4
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c7
    move-result-object v1

    #@c8
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@cb
    .line 282
    :cond_cb
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@cd
    check-cast v0, Landroid/os/AsyncResult;

    #@cf
    .line 283
    .restart local v0       #ar:Landroid/os/AsyncResult;
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->handleLgtWpbxChanged(Landroid/os/AsyncResult;)V

    #@d2
    goto/16 :goto_21

    #@d4
    .line 287
    .end local v0           #ar:Landroid/os/AsyncResult;
    :pswitch_d4
    iget-boolean v1, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->debugFilter:Z

    #@d6
    if-eqz v1, :cond_21

    #@d8
    new-instance v1, Ljava/lang/StringBuilder;

    #@da
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@dd
    const-string v2, "EVENT_LGT_ROAMING_UI_TEST_SET_DONE : "

    #@df
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e2
    move-result-object v1

    #@e3
    iget v2, p1, Landroid/os/Message;->what:I

    #@e5
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e8
    move-result-object v1

    #@e9
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ec
    move-result-object v1

    #@ed
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@f0
    goto/16 :goto_21

    #@f2
    .line 292
    :pswitch_f2
    iget-boolean v1, p0, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->debugFilter:Z

    #@f4
    if-eqz v1, :cond_10e

    #@f6
    new-instance v1, Ljava/lang/StringBuilder;

    #@f8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@fb
    const-string v2, "EVENT_NET_ERR_DISP : "

    #@fd
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@100
    move-result-object v1

    #@101
    iget v2, p1, Landroid/os/Message;->what:I

    #@103
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@106
    move-result-object v1

    #@107
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10a
    move-result-object v1

    #@10b
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@10e
    .line 293
    :cond_10e
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@110
    check-cast v0, Landroid/os/AsyncResult;

    #@112
    .line 294
    .restart local v0       #ar:Landroid/os/AsyncResult;
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->handleLGTNetworkError(Landroid/os/AsyncResult;)V

    #@115
    goto/16 :goto_21

    #@117
    .line 297
    .end local v0           #ar:Landroid/os/AsyncResult;
    :pswitch_117
    new-instance v1, Ljava/lang/StringBuilder;

    #@119
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@11c
    const-string v2, "EVENT_LGT_FACTORY_RESET : "

    #@11e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@121
    move-result-object v1

    #@122
    iget v2, p1, Landroid/os/Message;->what:I

    #@124
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@127
    move-result-object v1

    #@128
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12b
    move-result-object v1

    #@12c
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@12f
    goto/16 :goto_21

    #@131
    .line 301
    :pswitch_131
    new-instance v1, Ljava/lang/StringBuilder;

    #@133
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@136
    const-string v2, "EVENT_HDR_LOCK : "

    #@138
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13b
    move-result-object v1

    #@13c
    iget v2, p1, Landroid/os/Message;->what:I

    #@13e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@141
    move-result-object v1

    #@142
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@145
    move-result-object v1

    #@146
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@149
    .line 302
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@14b
    check-cast v0, Landroid/os/AsyncResult;

    #@14d
    .line 303
    .restart local v0       #ar:Landroid/os/AsyncResult;
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->handleHdrLock(Landroid/os/AsyncResult;)V

    #@150
    goto/16 :goto_21

    #@152
    .line 306
    .end local v0           #ar:Landroid/os/AsyncResult;
    :pswitch_152
    new-instance v1, Ljava/lang/StringBuilder;

    #@154
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@157
    const-string v2, "EVENT_LTE_LOCK : "

    #@159
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15c
    move-result-object v1

    #@15d
    iget v2, p1, Landroid/os/Message;->what:I

    #@15f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@162
    move-result-object v1

    #@163
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@166
    move-result-object v1

    #@167
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@16a
    .line 307
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@16c
    check-cast v0, Landroid/os/AsyncResult;

    #@16e
    .line 308
    .restart local v0       #ar:Landroid/os/AsyncResult;
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->handleLteLock(Landroid/os/AsyncResult;)V

    #@171
    goto/16 :goto_21

    #@173
    .line 312
    .end local v0           #ar:Landroid/os/AsyncResult;
    :pswitch_173
    new-instance v1, Ljava/lang/StringBuilder;

    #@175
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@178
    const-string v2, "EVENT_EMM_REJECT : "

    #@17a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17d
    move-result-object v1

    #@17e
    iget v2, p1, Landroid/os/Message;->what:I

    #@180
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@183
    move-result-object v1

    #@184
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@187
    move-result-object v1

    #@188
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->log(Ljava/lang/String;)V

    #@18b
    .line 313
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@18d
    check-cast v0, Landroid/os/AsyncResult;

    #@18f
    .line 314
    .restart local v0       #ar:Landroid/os/AsyncResult;
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/UPlusRILEventDispatcher;->handleLteEmmReject(Landroid/os/AsyncResult;)V

    #@192
    goto/16 :goto_21

    #@194
    .line 253
    :pswitch_data_194
    .packed-switch 0x1
        :pswitch_22
        :pswitch_42
        :pswitch_66
        :pswitch_8a
        :pswitch_af
        :pswitch_21
        :pswitch_21
        :pswitch_21
        :pswitch_21
        :pswitch_21
        :pswitch_21
        :pswitch_d4
        :pswitch_21
        :pswitch_21
        :pswitch_f2
        :pswitch_21
        :pswitch_21
        :pswitch_21
        :pswitch_117
        :pswitch_131
        :pswitch_152
        :pswitch_173
    .end packed-switch
.end method

.method protected log(Ljava/lang/String;)V
    .registers 5
    .parameter "s"

    #@0
    .prologue
    .line 863
    const-string v0, "CALL_FRW"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[UPlusRILEventDispatcher] "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 864
    return-void
.end method
