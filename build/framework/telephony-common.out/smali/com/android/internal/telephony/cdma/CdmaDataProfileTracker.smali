.class public final Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;
.super Landroid/os/Handler;
.source "CdmaDataProfileTracker.java"


# static fields
.field private static final EVENT_GET_DATA_CALL_PROFILE_DONE:I = 0x1

.field private static final EVENT_LOAD_PROFILES:I = 0x2

.field private static final EVENT_READ_MODEM_PROFILES:I

.field private static mDefaultApnTypes:[Ljava/lang/String;

.field private static mSupportedApnTypes:[Ljava/lang/String;


# instance fields
.field protected final LOG_TAG:Ljava/lang/String;

.field public SPR_GsmGlobalAPN:Ljava/lang/String;

.field public SPR_GsmGlobalAPN_TEST:Ljava/lang/String;

.field protected mActiveDp:Lcom/android/internal/telephony/DataProfile;

.field private mCdmaSsm:Lcom/android/internal/telephony/cdma/CdmaSubscriptionSourceManager;

.field private mDataProfilesList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/telephony/DataProfile;",
            ">;"
        }
    .end annotation
.end field

.field private mIsOmhEnabled:Z

.field private mModemDataProfileRegistrants:Landroid/os/RegistrantList;

.field mOmhDataProfilesList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/telephony/DataProfile;",
            ">;"
        }
    .end annotation
.end field

.field private mOmhReadProfileContext:I

.field private mOmhReadProfileCount:I

.field mOmhServicePriorityMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mPhone:Lcom/android/internal/telephony/cdma/CDMAPhone;

.field mTempOmhDataProfilesList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/telephony/DataProfile;",
            ">;"
        }
    .end annotation
.end field

.field private myfeatureset:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/android/internal/telephony/cdma/CDMAPhone;)V
    .registers 10
    .parameter "phone"

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x1

    #@3
    const/4 v4, 0x2

    #@4
    const/4 v3, 0x0

    #@5
    .line 153
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@8
    .line 81
    const-string v0, "CDMA"

    #@a
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->LOG_TAG:Ljava/lang/String;

    #@c
    .line 89
    new-instance v0, Ljava/util/ArrayList;

    #@e
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@11
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mDataProfilesList:Ljava/util/ArrayList;

    #@13
    .line 120
    iput v3, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mOmhReadProfileContext:I

    #@15
    .line 125
    iput v3, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mOmhReadProfileCount:I

    #@17
    .line 127
    const-string v0, "persist.omh.enabled"

    #@19
    invoke-static {v0, v3}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@1c
    move-result v0

    #@1d
    iput-boolean v0, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mIsOmhEnabled:Z

    #@1f
    .line 131
    new-instance v0, Ljava/util/ArrayList;

    #@21
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@24
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mOmhDataProfilesList:Ljava/util/ArrayList;

    #@26
    .line 134
    new-instance v0, Ljava/util/ArrayList;

    #@28
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@2b
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mTempOmhDataProfilesList:Ljava/util/ArrayList;

    #@2d
    .line 140
    new-instance v0, Landroid/os/RegistrantList;

    #@2f
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@32
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mModemDataProfileRegistrants:Landroid/os/RegistrantList;

    #@34
    .line 147
    const-string v0, "cinet.spcs"

    #@36
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->SPR_GsmGlobalAPN:Ljava/lang/String;

    #@38
    .line 148
    const-string v0, "gsmtest"

    #@3a
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->SPR_GsmGlobalAPN_TEST:Ljava/lang/String;

    #@3c
    .line 154
    iput-object p1, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mPhone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@3e
    .line 155
    invoke-virtual {p1}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@41
    move-result-object v0

    #@42
    iget-object v1, p1, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@44
    const/4 v2, 0x0

    #@45
    invoke-static {v0, v1, p0, v4, v2}, Lcom/android/internal/telephony/cdma/CdmaSubscriptionSourceManager;->getInstance(Landroid/content/Context;Lcom/android/internal/telephony/CommandsInterface;Landroid/os/Handler;ILjava/lang/Object;)Lcom/android/internal/telephony/cdma/CdmaSubscriptionSourceManager;

    #@48
    move-result-object v0

    #@49
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mCdmaSsm:Lcom/android/internal/telephony/cdma/CdmaSubscriptionSourceManager;

    #@4b
    .line 158
    new-instance v0, Ljava/util/HashMap;

    #@4d
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@50
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mOmhServicePriorityMap:Ljava/util/HashMap;

    #@52
    .line 159
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mPhone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@54
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@56
    invoke-interface {v0}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@59
    move-result-object v0

    #@5a
    iget-object v0, v0, Lcom/android/internal/telephony/LGfeature;->myfeatureset:Ljava/lang/String;

    #@5c
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->myfeatureset:Ljava/lang/String;

    #@5e
    .line 162
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mPhone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@60
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@62
    invoke-interface {v0}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@65
    move-result-object v0

    #@66
    iget-boolean v0, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_DISABLE_ON_LEGACY_CDMA_VZW:Z

    #@68
    if-eqz v0, :cond_e0

    #@6a
    .line 164
    const/16 v0, 0x8

    #@6c
    new-array v0, v0, [Ljava/lang/String;

    #@6e
    const-string v1, "default"

    #@70
    aput-object v1, v0, v3

    #@72
    const-string v1, "mms"

    #@74
    aput-object v1, v0, v5

    #@76
    const-string v1, "supl"

    #@78
    aput-object v1, v0, v4

    #@7a
    const-string v1, "dun"

    #@7c
    aput-object v1, v0, v6

    #@7e
    const-string v1, "hipri"

    #@80
    aput-object v1, v0, v7

    #@82
    const/4 v1, 0x5

    #@83
    const-string v2, "fota"

    #@85
    aput-object v2, v0, v1

    #@87
    const/4 v1, 0x6

    #@88
    const-string v2, "vzwapp"

    #@8a
    aput-object v2, v0, v1

    #@8c
    const/4 v1, 0x7

    #@8d
    const-string v2, "cbs"

    #@8f
    aput-object v2, v0, v1

    #@91
    sput-object v0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mSupportedApnTypes:[Ljava/lang/String;

    #@93
    .line 201
    :goto_93
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->myfeatureset:Ljava/lang/String;

    #@95
    const-string v1, "VZWBASE"

    #@97
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9a
    move-result v0

    #@9b
    if-eqz v0, :cond_140

    #@9d
    .line 202
    const/4 v0, 0x7

    #@9e
    new-array v0, v0, [Ljava/lang/String;

    #@a0
    const-string v1, "default"

    #@a2
    aput-object v1, v0, v3

    #@a4
    const-string v1, "mms"

    #@a6
    aput-object v1, v0, v5

    #@a8
    const-string v1, "supl"

    #@aa
    aput-object v1, v0, v4

    #@ac
    const-string v1, "hipri"

    #@ae
    aput-object v1, v0, v6

    #@b0
    const-string v1, "fota"

    #@b2
    aput-object v1, v0, v7

    #@b4
    const/4 v1, 0x5

    #@b5
    const-string v2, "vzwapp"

    #@b7
    aput-object v2, v0, v1

    #@b9
    const/4 v1, 0x6

    #@ba
    const-string v2, "cbs"

    #@bc
    aput-object v2, v0, v1

    #@be
    sput-object v0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mDefaultApnTypes:[Ljava/lang/String;

    #@c0
    .line 246
    :goto_c0
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->obtainMessage(I)Landroid/os/Message;

    #@c3
    move-result-object v0

    #@c4
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->sendMessage(Landroid/os/Message;)Z

    #@c7
    .line 248
    new-instance v0, Ljava/lang/StringBuilder;

    #@c9
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@cc
    const-string v1, "SUPPORT_OMH: "

    #@ce
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d1
    move-result-object v0

    #@d2
    iget-boolean v1, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mIsOmhEnabled:Z

    #@d4
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@d7
    move-result-object v0

    #@d8
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@db
    move-result-object v0

    #@dc
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->log(Ljava/lang/String;)V

    #@df
    .line 249
    return-void

    #@e0
    .line 175
    :cond_e0
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mPhone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@e2
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@e4
    invoke-interface {v0}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@e7
    sget-boolean v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_IMS_DISABLE_ON_LEGACY_CDMA_KDDI:Z

    #@e9
    if-eqz v0, :cond_10f

    #@eb
    .line 177
    const/4 v0, 0x7

    #@ec
    new-array v0, v0, [Ljava/lang/String;

    #@ee
    const-string v1, "default"

    #@f0
    aput-object v1, v0, v3

    #@f2
    const-string v1, "mms"

    #@f4
    aput-object v1, v0, v5

    #@f6
    const-string v1, "supl"

    #@f8
    aput-object v1, v0, v4

    #@fa
    const-string v1, "dun"

    #@fc
    aput-object v1, v0, v6

    #@fe
    const-string v1, "hipri"

    #@100
    aput-object v1, v0, v7

    #@102
    const/4 v1, 0x5

    #@103
    const-string v2, "fota"

    #@105
    aput-object v2, v0, v1

    #@107
    const/4 v1, 0x6

    #@108
    const-string v2, "cbs"

    #@10a
    aput-object v2, v0, v1

    #@10c
    sput-object v0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mSupportedApnTypes:[Ljava/lang/String;

    #@10e
    goto :goto_93

    #@10f
    .line 189
    :cond_10f
    const/16 v0, 0x9

    #@111
    new-array v0, v0, [Ljava/lang/String;

    #@113
    const-string v1, "default"

    #@115
    aput-object v1, v0, v3

    #@117
    const-string v1, "mms"

    #@119
    aput-object v1, v0, v5

    #@11b
    const-string v1, "supl"

    #@11d
    aput-object v1, v0, v4

    #@11f
    const-string v1, "dun"

    #@121
    aput-object v1, v0, v6

    #@123
    const-string v1, "hipri"

    #@125
    aput-object v1, v0, v7

    #@127
    const/4 v1, 0x5

    #@128
    const-string v2, "fota"

    #@12a
    aput-object v2, v0, v1

    #@12c
    const/4 v1, 0x6

    #@12d
    const-string v2, "ims"

    #@12f
    aput-object v2, v0, v1

    #@131
    const/4 v1, 0x7

    #@132
    const-string v2, "vzwapp"

    #@134
    aput-object v2, v0, v1

    #@136
    const/16 v1, 0x8

    #@138
    const-string v2, "cbs"

    #@13a
    aput-object v2, v0, v1

    #@13c
    sput-object v0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mSupportedApnTypes:[Ljava/lang/String;

    #@13e
    goto/16 :goto_93

    #@140
    .line 211
    :cond_140
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->myfeatureset:Ljava/lang/String;

    #@142
    const-string v1, "SPCSBASE"

    #@144
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@147
    move-result v0

    #@148
    if-eqz v0, :cond_16f

    #@14a
    .line 213
    const/4 v0, 0x7

    #@14b
    new-array v0, v0, [Ljava/lang/String;

    #@14d
    const-string v1, "default"

    #@14f
    aput-object v1, v0, v3

    #@151
    const-string v1, "mms"

    #@153
    aput-object v1, v0, v5

    #@155
    const-string v1, "supl"

    #@157
    aput-object v1, v0, v4

    #@159
    const-string v1, "hipri"

    #@15b
    aput-object v1, v0, v6

    #@15d
    const-string v1, "fota"

    #@15f
    aput-object v1, v0, v7

    #@161
    const/4 v1, 0x5

    #@162
    const-string v2, "dun"

    #@164
    aput-object v2, v0, v1

    #@166
    const/4 v1, 0x6

    #@167
    const-string v2, "cbs"

    #@169
    aput-object v2, v0, v1

    #@16b
    sput-object v0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mDefaultApnTypes:[Ljava/lang/String;

    #@16d
    goto/16 :goto_c0

    #@16f
    .line 223
    :cond_16f
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->myfeatureset:Ljava/lang/String;

    #@171
    const-string v1, "KDDIBASE"

    #@173
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@176
    move-result v0

    #@177
    if-eqz v0, :cond_19e

    #@179
    .line 224
    const/4 v0, 0x7

    #@17a
    new-array v0, v0, [Ljava/lang/String;

    #@17c
    const-string v1, "default"

    #@17e
    aput-object v1, v0, v3

    #@180
    const-string v1, "mms"

    #@182
    aput-object v1, v0, v5

    #@184
    const-string v1, "supl"

    #@186
    aput-object v1, v0, v4

    #@188
    const-string v1, "hipri"

    #@18a
    aput-object v1, v0, v6

    #@18c
    const-string v1, "fota"

    #@18e
    aput-object v1, v0, v7

    #@190
    const/4 v1, 0x5

    #@191
    const-string v2, "dun"

    #@193
    aput-object v2, v0, v1

    #@195
    const/4 v1, 0x6

    #@196
    const-string v2, "cbs"

    #@198
    aput-object v2, v0, v1

    #@19a
    sput-object v0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mDefaultApnTypes:[Ljava/lang/String;

    #@19c
    goto/16 :goto_c0

    #@19e
    .line 235
    :cond_19e
    const/4 v0, 0x7

    #@19f
    new-array v0, v0, [Ljava/lang/String;

    #@1a1
    const-string v1, "default"

    #@1a3
    aput-object v1, v0, v3

    #@1a5
    const-string v1, "mms"

    #@1a7
    aput-object v1, v0, v5

    #@1a9
    const-string v1, "supl"

    #@1ab
    aput-object v1, v0, v4

    #@1ad
    const-string v1, "hipri"

    #@1af
    aput-object v1, v0, v6

    #@1b1
    const-string v1, "fota"

    #@1b3
    aput-object v1, v0, v7

    #@1b5
    const/4 v1, 0x5

    #@1b6
    const-string v2, "ims"

    #@1b8
    aput-object v2, v0, v1

    #@1ba
    const/4 v1, 0x6

    #@1bb
    const-string v2, "cbs"

    #@1bd
    aput-object v2, v0, v1

    #@1bf
    sput-object v0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mDefaultApnTypes:[Ljava/lang/String;

    #@1c1
    goto/16 :goto_c0
.end method

.method private addServiceTypeToUnSpecified()V
    .registers 9

    #@0
    .prologue
    .line 748
    sget-object v1, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mSupportedApnTypes:[Ljava/lang/String;

    #@2
    .local v1, arr$:[Ljava/lang/String;
    array-length v5, v1

    #@3
    .local v5, len$:I
    const/4 v3, 0x0

    #@4
    .local v3, i$:I
    move v4, v3

    #@5
    .end local v3           #i$:I
    .local v4, i$:I
    :goto_5
    if-ge v4, v5, :cond_55

    #@7
    aget-object v0, v1, v4

    #@9
    .line 749
    .local v0, apntype:Ljava/lang/String;
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mOmhServicePriorityMap:Ljava/util/HashMap;

    #@b
    invoke-virtual {v6, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@e
    move-result v6

    #@f
    if-nez v6, :cond_51

    #@11
    .line 754
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mTempOmhDataProfilesList:Ljava/util/ArrayList;

    #@13
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@16
    move-result-object v3

    #@17
    .end local v4           #i$:I
    .local v3, i$:Ljava/util/Iterator;
    :cond_17
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@1a
    move-result v6

    #@1b
    if-eqz v6, :cond_51

    #@1d
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@20
    move-result-object v2

    #@21
    check-cast v2, Lcom/android/internal/telephony/DataProfile;

    #@23
    .local v2, dp:Lcom/android/internal/telephony/DataProfile;
    move-object v6, v2

    #@24
    .line 755
    check-cast v6, Lcom/android/internal/telephony/cdma/DataProfileOmh;

    #@26
    invoke-virtual {v6}, Lcom/android/internal/telephony/cdma/DataProfileOmh;->getDataProfileTypeModem()Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;

    #@29
    move-result-object v6

    #@2a
    sget-object v7, Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;->PROFILE_TYPE_UNSPECIFIED:Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;

    #@2c
    if-ne v6, v7, :cond_17

    #@2e
    .line 757
    check-cast v2, Lcom/android/internal/telephony/cdma/DataProfileOmh;

    #@30
    .end local v2           #dp:Lcom/android/internal/telephony/DataProfile;
    invoke-static {v0}, Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;->getDataProfileTypeModem(Ljava/lang/String;)Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;

    #@33
    move-result-object v6

    #@34
    invoke-virtual {v2, v6}, Lcom/android/internal/telephony/cdma/DataProfileOmh;->addServiceType(Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;)V

    #@37
    .line 759
    new-instance v6, Ljava/lang/StringBuilder;

    #@39
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@3c
    const-string v7, "OMH: Service Type added to UNSPECIFIED is : "

    #@3e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v6

    #@42
    invoke-static {v0}, Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;->getDataProfileTypeModem(Ljava/lang/String;)Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;

    #@45
    move-result-object v7

    #@46
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v6

    #@4a
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v6

    #@4e
    invoke-virtual {p0, v6}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->log(Ljava/lang/String;)V

    #@51
    .line 748
    .end local v3           #i$:Ljava/util/Iterator;
    :cond_51
    add-int/lit8 v3, v4, 0x1

    #@53
    .local v3, i$:I
    move v4, v3

    #@54
    .end local v3           #i$:I
    .restart local v4       #i$:I
    goto :goto_5

    #@55
    .line 766
    .end local v0           #apntype:Ljava/lang/String;
    :cond_55
    return-void
.end method

.method private createApnListonCDMA(Landroid/database/Cursor;)Ljava/util/ArrayList;
    .registers 23
    .parameter "cursor"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/telephony/DataProfile;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 513
    new-instance v20, Ljava/util/ArrayList;

    #@2
    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 514
    .local v20, result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/DataProfile;>;"
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToFirst()Z

    #@8
    move-result v3

    #@9
    if-eqz v3, :cond_49

    #@b
    .line 516
    :cond_b
    const-string v3, "type"

    #@d
    move-object/from16 v0, p1

    #@f
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@12
    move-result v3

    #@13
    move-object/from16 v0, p1

    #@15
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@18
    move-result-object v3

    #@19
    move-object/from16 v0, p0

    #@1b
    invoke-direct {v0, v3}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->parseTypes(Ljava/lang/String;)[Ljava/lang/String;

    #@1e
    move-result-object v15

    #@1f
    .line 520
    .local v15, types:[Ljava/lang/String;
    const/4 v3, 0x0

    #@20
    aget-object v3, v15, v3

    #@22
    const-string v4, "dummy"

    #@24
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@27
    move-result v3

    #@28
    if-nez v3, :cond_35

    #@2a
    const/4 v3, 0x0

    #@2b
    aget-object v3, v15, v3

    #@2d
    const-string v4, "Dummy"

    #@2f
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@32
    move-result v3

    #@33
    if-eqz v3, :cond_51

    #@35
    :cond_35
    move-object/from16 v0, p0

    #@37
    iget-object v3, v0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mPhone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@39
    iget-object v3, v3, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@3b
    invoke-interface {v3}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@3e
    move-result-object v3

    #@3f
    iget-boolean v3, v3, Lcom/android/internal/telephony/LGfeature;->DoNotReadDummyAPN:Z

    #@41
    if-eqz v3, :cond_51

    #@43
    .line 590
    :goto_43
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z

    #@46
    move-result v3

    #@47
    if-nez v3, :cond_b

    #@49
    .line 592
    .end local v15           #types:[Ljava/lang/String;
    :cond_49
    const-string v3, "!!Load APN DB in CDMA DCT"

    #@4b
    move-object/from16 v0, p0

    #@4d
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->log(Ljava/lang/String;)V

    #@50
    .line 593
    return-object v20

    #@51
    .line 526
    .restart local v15       #types:[Ljava/lang/String;
    :cond_51
    const-string v16, "IP"

    #@53
    .line 527
    .local v16, protocol:Ljava/lang/String;
    const-string v17, "IP"

    #@55
    .line 529
    .local v17, Rprotocol:Ljava/lang/String;
    move-object/from16 v0, p0

    #@57
    iget-object v3, v0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mPhone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@59
    iget-object v3, v3, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@5b
    invoke-interface {v3}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@5e
    move-result-object v3

    #@5f
    iget-boolean v3, v3, Lcom/android/internal/telephony/LGfeature;->SUPPORT_IPV6:Z

    #@61
    if-eqz v3, :cond_7f

    #@63
    .line 531
    const-string v3, "protocol"

    #@65
    move-object/from16 v0, p1

    #@67
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@6a
    move-result v3

    #@6b
    move-object/from16 v0, p1

    #@6d
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@70
    move-result-object v16

    #@71
    .line 532
    const-string v3, "roaming_protocol"

    #@73
    move-object/from16 v0, p1

    #@75
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@78
    move-result v3

    #@79
    move-object/from16 v0, p1

    #@7b
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@7e
    move-result-object v17

    #@7f
    .line 535
    :cond_7f
    new-instance v2, Lcom/android/internal/telephony/ApnSetting;

    #@81
    const-string v3, "_id"

    #@83
    move-object/from16 v0, p1

    #@85
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@88
    move-result v3

    #@89
    move-object/from16 v0, p1

    #@8b
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    #@8e
    move-result v3

    #@8f
    const-string v4, "numeric"

    #@91
    move-object/from16 v0, p1

    #@93
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@96
    move-result v4

    #@97
    move-object/from16 v0, p1

    #@99
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@9c
    move-result-object v4

    #@9d
    const-string v5, "name"

    #@9f
    move-object/from16 v0, p1

    #@a1
    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@a4
    move-result v5

    #@a5
    move-object/from16 v0, p1

    #@a7
    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@aa
    move-result-object v5

    #@ab
    const-string v6, "apn"

    #@ad
    move-object/from16 v0, p1

    #@af
    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@b2
    move-result v6

    #@b3
    move-object/from16 v0, p1

    #@b5
    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@b8
    move-result-object v6

    #@b9
    const-string v7, "proxy"

    #@bb
    move-object/from16 v0, p1

    #@bd
    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@c0
    move-result v7

    #@c1
    move-object/from16 v0, p1

    #@c3
    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@c6
    move-result-object v7

    #@c7
    invoke-static {v7}, Landroid/net/NetworkUtils;->trimV4AddrZeros(Ljava/lang/String;)Ljava/lang/String;

    #@ca
    move-result-object v7

    #@cb
    const-string v8, "port"

    #@cd
    move-object/from16 v0, p1

    #@cf
    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@d2
    move-result v8

    #@d3
    move-object/from16 v0, p1

    #@d5
    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@d8
    move-result-object v8

    #@d9
    const-string v9, "mmsc"

    #@db
    move-object/from16 v0, p1

    #@dd
    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@e0
    move-result v9

    #@e1
    move-object/from16 v0, p1

    #@e3
    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@e6
    move-result-object v9

    #@e7
    invoke-static {v9}, Landroid/net/NetworkUtils;->trimV4AddrZeros(Ljava/lang/String;)Ljava/lang/String;

    #@ea
    move-result-object v9

    #@eb
    const-string v10, "mmsproxy"

    #@ed
    move-object/from16 v0, p1

    #@ef
    invoke-interface {v0, v10}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@f2
    move-result v10

    #@f3
    move-object/from16 v0, p1

    #@f5
    invoke-interface {v0, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@f8
    move-result-object v10

    #@f9
    invoke-static {v10}, Landroid/net/NetworkUtils;->trimV4AddrZeros(Ljava/lang/String;)Ljava/lang/String;

    #@fc
    move-result-object v10

    #@fd
    const-string v11, "mmsport"

    #@ff
    move-object/from16 v0, p1

    #@101
    invoke-interface {v0, v11}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@104
    move-result v11

    #@105
    move-object/from16 v0, p1

    #@107
    invoke-interface {v0, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@10a
    move-result-object v11

    #@10b
    const-string v12, "user"

    #@10d
    move-object/from16 v0, p1

    #@10f
    invoke-interface {v0, v12}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@112
    move-result v12

    #@113
    move-object/from16 v0, p1

    #@115
    invoke-interface {v0, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@118
    move-result-object v12

    #@119
    const-string v13, "password"

    #@11b
    move-object/from16 v0, p1

    #@11d
    invoke-interface {v0, v13}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@120
    move-result v13

    #@121
    move-object/from16 v0, p1

    #@123
    invoke-interface {v0, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@126
    move-result-object v13

    #@127
    const-string v14, "authtype"

    #@129
    move-object/from16 v0, p1

    #@12b
    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@12e
    move-result v14

    #@12f
    move-object/from16 v0, p1

    #@131
    invoke-interface {v0, v14}, Landroid/database/Cursor;->getInt(I)I

    #@134
    move-result v14

    #@135
    const-string v18, "carrier_enabled"

    #@137
    move-object/from16 v0, p1

    #@139
    move-object/from16 v1, v18

    #@13b
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@13e
    move-result v18

    #@13f
    move-object/from16 v0, p1

    #@141
    move/from16 v1, v18

    #@143
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    #@146
    move-result v18

    #@147
    const/16 v19, 0x1

    #@149
    move/from16 v0, v18

    #@14b
    move/from16 v1, v19

    #@14d
    if-ne v0, v1, :cond_1ac

    #@14f
    const/16 v18, 0x1

    #@151
    :goto_151
    const-string v19, "bearer"

    #@153
    move-object/from16 v0, p1

    #@155
    move-object/from16 v1, v19

    #@157
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@15a
    move-result v19

    #@15b
    move-object/from16 v0, p1

    #@15d
    move/from16 v1, v19

    #@15f
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    #@162
    move-result v19

    #@163
    invoke-direct/range {v2 .. v19}, Lcom/android/internal/telephony/ApnSetting;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)V

    #@166
    .line 562
    .local v2, apn:Lcom/android/internal/telephony/ApnSetting;
    move-object/from16 v0, p0

    #@168
    iget-object v3, v0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mPhone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@16a
    iget-object v3, v3, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@16c
    invoke-interface {v3}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@16f
    move-result-object v3

    #@170
    iget-boolean v3, v3, Lcom/android/internal/telephony/LGfeature;->LGP_GSM_GLOBAL_PREFERED_APN_SPR:Z

    #@172
    if-eqz v3, :cond_1e3

    #@174
    .line 564
    const/4 v3, 0x0

    #@175
    aget-object v3, v15, v3

    #@177
    const-string v4, "default"

    #@179
    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@17c
    move-result v3

    #@17d
    if-eqz v3, :cond_1e3

    #@17f
    .line 566
    move-object/from16 v0, p0

    #@181
    iget-object v3, v0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mPhone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@183
    iget-object v3, v3, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@185
    iget-object v3, v3, Lcom/android/internal/telephony/DataConnectionTracker;->apnSelectionHdlr:Lcom/android/internal/telephony/ApnSelectionHandler;

    #@187
    invoke-virtual {v3, v2}, Lcom/android/internal/telephony/ApnSelectionHandler;->findAllOperatorApnID(Lcom/android/internal/telephony/ApnSetting;)V

    #@18a
    .line 568
    iget v3, v2, Lcom/android/internal/telephony/DataProfile;->bearer:I

    #@18c
    if-eqz v3, :cond_1af

    #@18e
    iget v3, v2, Lcom/android/internal/telephony/DataProfile;->bearer:I

    #@190
    const/16 v4, 0xd

    #@192
    if-eq v3, v4, :cond_1af

    #@194
    iget v3, v2, Lcom/android/internal/telephony/DataProfile;->bearer:I

    #@196
    const/16 v4, 0xe

    #@198
    if-eq v3, v4, :cond_1af

    #@19a
    .line 570
    move-object/from16 v0, p0

    #@19c
    iget-object v3, v0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mPhone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@19e
    iget-object v3, v3, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@1a0
    const/4 v4, 0x0

    #@1a1
    iput-object v4, v3, Lcom/android/internal/telephony/DataConnectionTracker;->mRomaingPreferredApn:Lcom/android/internal/telephony/DataProfile;

    #@1a3
    .line 571
    const-string v3, "mRomaingPreferredApn set null "

    #@1a5
    move-object/from16 v0, p0

    #@1a7
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->log(Ljava/lang/String;)V

    #@1aa
    goto/16 :goto_43

    #@1ac
    .line 535
    .end local v2           #apn:Lcom/android/internal/telephony/ApnSetting;
    :cond_1ac
    const/16 v18, 0x0

    #@1ae
    goto :goto_151

    #@1af
    .line 579
    .restart local v2       #apn:Lcom/android/internal/telephony/ApnSetting;
    :cond_1af
    move-object/from16 v0, p0

    #@1b1
    iget-object v3, v0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mPhone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@1b3
    iget-object v3, v3, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@1b5
    iput-object v2, v3, Lcom/android/internal/telephony/DataConnectionTracker;->mDomesticPreferredApn:Lcom/android/internal/telephony/DataProfile;

    #@1b7
    .line 580
    move-object/from16 v0, p0

    #@1b9
    iget-object v3, v0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mPhone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@1bb
    iget-object v3, v3, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@1bd
    iput-object v2, v3, Lcom/android/internal/telephony/DataConnectionTracker;->mPreferredApn:Lcom/android/internal/telephony/DataProfile;

    #@1bf
    .line 581
    new-instance v3, Ljava/lang/StringBuilder;

    #@1c1
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1c4
    const-string v4, "mDomesticPreferredApn :: "

    #@1c6
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c9
    move-result-object v3

    #@1ca
    move-object/from16 v0, p0

    #@1cc
    iget-object v4, v0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mPhone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@1ce
    iget-object v4, v4, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@1d0
    iget-object v4, v4, Lcom/android/internal/telephony/DataConnectionTracker;->mDomesticPreferredApn:Lcom/android/internal/telephony/DataProfile;

    #@1d2
    invoke-virtual {v4}, Lcom/android/internal/telephony/DataProfile;->toString()Ljava/lang/String;

    #@1d5
    move-result-object v4

    #@1d6
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d9
    move-result-object v3

    #@1da
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1dd
    move-result-object v3

    #@1de
    move-object/from16 v0, p0

    #@1e0
    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->log(Ljava/lang/String;)V

    #@1e3
    .line 587
    :cond_1e3
    move-object/from16 v0, v20

    #@1e5
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1e8
    goto/16 :goto_43
.end method

.method private createDefaultDataProfiles()V
    .registers 15

    #@0
    .prologue
    const/4 v6, 0x3

    #@1
    const/4 v13, 0x1

    #@2
    const/4 v12, 0x0

    #@3
    const/4 v2, 0x0

    #@4
    .line 279
    const-string v1, "Creating default profiles..."

    #@6
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->log(Ljava/lang/String;)V

    #@9
    .line 280
    const-string v1, "persist.telephony.cdma.protocol"

    #@b
    const-string v3, "IP"

    #@d
    invoke-static {v1, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@10
    move-result-object v8

    #@11
    .line 282
    .local v8, ipProto:Ljava/lang/String;
    const-string v1, "persist.telephony.cdma.rproto"

    #@13
    const-string v3, "IP"

    #@15
    invoke-static {v1, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@18
    move-result-object v9

    #@19
    .line 285
    .local v9, roamingIpProto:Ljava/lang/String;
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mPhone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@1b
    iget-object v1, v1, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@1d
    instance-of v1, v1, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;

    #@1f
    if-eqz v1, :cond_7c

    #@21
    .line 287
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mPhone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@23
    iget-object v11, v1, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@25
    .line 290
    .local v11, dct:Lcom/android/internal/telephony/DataConnectionTracker;
    new-instance v0, Lcom/android/internal/telephony/cdma/DataProfileCdma;

    #@27
    const-string v1, "default"

    #@29
    invoke-virtual {v11, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->apnTypeToId(Ljava/lang/String;)I

    #@2c
    move-result v1

    #@2d
    sget-object v7, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mDefaultApnTypes:[Ljava/lang/String;

    #@2f
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mPhone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@31
    invoke-virtual {v3}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getServiceState()Landroid/telephony/ServiceState;

    #@34
    move-result-object v3

    #@35
    invoke-virtual {v3}, Landroid/telephony/ServiceState;->getRadioTechnology()I

    #@38
    move-result v10

    #@39
    move-object v3, v2

    #@3a
    move-object v4, v2

    #@3b
    move-object v5, v2

    #@3c
    invoke-direct/range {v0 .. v10}, Lcom/android/internal/telephony/cdma/DataProfileCdma;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    #@3f
    .line 295
    .local v0, dp:Lcom/android/internal/telephony/cdma/DataProfileCdma;
    invoke-virtual {v0, v12}, Lcom/android/internal/telephony/cdma/DataProfileCdma;->setProfileId(I)V

    #@42
    .line 296
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mDataProfilesList:Ljava/util/ArrayList;

    #@44
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@47
    .line 298
    new-array v7, v13, [Ljava/lang/String;

    #@49
    const-string v1, "dun"

    #@4b
    aput-object v1, v7, v12

    #@4d
    .line 300
    .local v7, types:[Ljava/lang/String;
    new-instance v0, Lcom/android/internal/telephony/cdma/DataProfileCdma;

    #@4f
    .end local v0           #dp:Lcom/android/internal/telephony/cdma/DataProfileCdma;
    const-string v1, "dun"

    #@51
    invoke-virtual {v11, v1}, Lcom/android/internal/telephony/DataConnectionTracker;->apnTypeToId(Ljava/lang/String;)I

    #@54
    move-result v1

    #@55
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mPhone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@57
    invoke-virtual {v3}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getServiceState()Landroid/telephony/ServiceState;

    #@5a
    move-result-object v3

    #@5b
    invoke-virtual {v3}, Landroid/telephony/ServiceState;->getRadioTechnology()I

    #@5e
    move-result v10

    #@5f
    move-object v3, v2

    #@60
    move-object v4, v2

    #@61
    move-object v5, v2

    #@62
    invoke-direct/range {v0 .. v10}, Lcom/android/internal/telephony/cdma/DataProfileCdma;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    #@65
    .line 305
    .restart local v0       #dp:Lcom/android/internal/telephony/cdma/DataProfileCdma;
    invoke-virtual {v0, v13}, Lcom/android/internal/telephony/cdma/DataProfileCdma;->setProfileId(I)V

    #@68
    .line 306
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mDataProfilesList:Ljava/util/ArrayList;

    #@6a
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@6d
    .line 309
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mPhone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@6f
    iget-object v1, v1, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@71
    invoke-interface {v1}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@74
    move-result-object v1

    #@75
    iget-boolean v1, v1, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_APN_APNSYNC_ON_CDMA:Z

    #@77
    if-eqz v1, :cond_7c

    #@79
    .line 310
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->readDataProfileFromDb()V

    #@7c
    .line 315
    .end local v0           #dp:Lcom/android/internal/telephony/cdma/DataProfileCdma;
    .end local v7           #types:[Ljava/lang/String;
    .end local v11           #dct:Lcom/android/internal/telephony/DataConnectionTracker;
    :cond_7c
    return-void
.end method

.method private getDuplicateProfile(Lcom/android/internal/telephony/DataProfile;)Lcom/android/internal/telephony/cdma/DataProfileOmh;
    .registers 6
    .parameter "dp"

    #@0
    .prologue
    .line 717
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mTempOmhDataProfilesList:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v1

    #@6
    .local v1, i$:Ljava/util/Iterator;
    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_25

    #@c
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Lcom/android/internal/telephony/DataProfile;

    #@12
    .local v0, dataProfile:Lcom/android/internal/telephony/DataProfile;
    move-object v2, p1

    #@13
    .line 718
    check-cast v2, Lcom/android/internal/telephony/cdma/DataProfileOmh;

    #@15
    invoke-virtual {v2}, Lcom/android/internal/telephony/cdma/DataProfileOmh;->getProfileId()I

    #@18
    move-result v3

    #@19
    move-object v2, v0

    #@1a
    check-cast v2, Lcom/android/internal/telephony/cdma/DataProfileOmh;

    #@1c
    invoke-virtual {v2}, Lcom/android/internal/telephony/cdma/DataProfileOmh;->getProfileId()I

    #@1f
    move-result v2

    #@20
    if-ne v3, v2, :cond_6

    #@22
    .line 720
    check-cast v0, Lcom/android/internal/telephony/cdma/DataProfileOmh;

    #@24
    .line 723
    .end local v0           #dataProfile:Lcom/android/internal/telephony/DataProfile;
    :goto_24
    return-object v0

    #@25
    :cond_25
    const/4 v0, 0x0

    #@26
    goto :goto_24
.end method

.method private getOperatorNumeric()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 322
    const/4 v1, 0x0

    #@1
    .line 324
    .local v1, result:Ljava/lang/String;
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mPhone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@3
    iget-object v2, v2, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@5
    instance-of v2, v2, Lcom/android/internal/telephony/cdma/CdmaDataConnectionTracker;

    #@7
    if-eqz v2, :cond_32

    #@9
    .line 325
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mPhone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@b
    iget-object v0, v2, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@d
    .line 326
    .local v0, dct:Lcom/android/internal/telephony/DataConnectionTracker;
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mCdmaSsm:Lcom/android/internal/telephony/cdma/CdmaSubscriptionSourceManager;

    #@f
    invoke-virtual {v2}, Lcom/android/internal/telephony/cdma/CdmaSubscriptionSourceManager;->getCdmaSubscriptionSource()I

    #@12
    move-result v2

    #@13
    const/4 v3, 0x1

    #@14
    if-ne v2, v3, :cond_33

    #@16
    .line 328
    sget-object v2, Lcom/android/internal/telephony/cdma/CDMAPhone;->PROPERTY_CDMA_HOME_OPERATOR_NUMERIC:Ljava/lang/String;

    #@18
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    .line 329
    new-instance v2, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v3, "operatorNumeric for NV "

    #@23
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v2

    #@27
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->log(Ljava/lang/String;)V

    #@32
    .line 338
    .end local v0           #dct:Lcom/android/internal/telephony/DataConnectionTracker;
    :cond_32
    :goto_32
    return-object v1

    #@33
    .line 330
    .restart local v0       #dct:Lcom/android/internal/telephony/DataConnectionTracker;
    :cond_33
    invoke-virtual {v0}, Lcom/android/internal/telephony/DataConnectionTracker;->getIccRecords()Lcom/android/internal/telephony/uicc/IccRecords;

    #@36
    move-result-object v2

    #@37
    if-eqz v2, :cond_58

    #@39
    .line 331
    invoke-virtual {v0}, Lcom/android/internal/telephony/DataConnectionTracker;->getIccRecords()Lcom/android/internal/telephony/uicc/IccRecords;

    #@3c
    move-result-object v2

    #@3d
    invoke-virtual {v2}, Lcom/android/internal/telephony/uicc/IccRecords;->getOperatorNumeric()Ljava/lang/String;

    #@40
    move-result-object v1

    #@41
    .line 332
    new-instance v2, Ljava/lang/StringBuilder;

    #@43
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@46
    const-string v3, "operatorNumeric for icc "

    #@48
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v2

    #@4c
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v2

    #@50
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@53
    move-result-object v2

    #@54
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->log(Ljava/lang/String;)V

    #@57
    goto :goto_32

    #@58
    .line 334
    :cond_58
    const-string v2, "IccRecords == null -> operatorNumeric = null"

    #@5a
    invoke-virtual {p0, v2}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->log(Ljava/lang/String;)V

    #@5d
    goto :goto_32
.end method

.method private omhListGetArbitratedPriority(Ljava/util/ArrayList;Ljava/lang/String;)I
    .registers 8
    .parameter
    .parameter "serviceType"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/telephony/DataProfile;",
            ">;",
            "Ljava/lang/String;",
            ")I"
        }
    .end annotation

    #@0
    .prologue
    .line 775
    .local p1, dataProfileListModem:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/DataProfile;>;"
    const/4 v2, 0x0

    #@1
    .line 777
    .local v2, profile:Lcom/android/internal/telephony/DataProfile;
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@4
    move-result-object v1

    #@5
    .local v1, i$:Ljava/util/Iterator;
    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@8
    move-result v3

    #@9
    if-eqz v3, :cond_4c

    #@b
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@e
    move-result-object v0

    #@f
    check-cast v0, Lcom/android/internal/telephony/DataProfile;

    #@11
    .local v0, dp:Lcom/android/internal/telephony/DataProfile;
    move-object v3, v0

    #@12
    .line 778
    check-cast v3, Lcom/android/internal/telephony/cdma/DataProfileOmh;

    #@14
    invoke-virtual {v3}, Lcom/android/internal/telephony/cdma/DataProfileOmh;->isValidPriority()Z

    #@17
    move-result v3

    #@18
    if-nez v3, :cond_20

    #@1a
    .line 779
    const-string v3, "[OMH] Invalid priority... skipping"

    #@1c
    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->log(Ljava/lang/String;)V

    #@1f
    goto :goto_5

    #@20
    .line 783
    :cond_20
    if-nez v2, :cond_24

    #@22
    .line 784
    move-object v2, v0

    #@23
    goto :goto_5

    #@24
    .line 786
    :cond_24
    const-string v3, "supl"

    #@26
    if-ne p2, v3, :cond_3a

    #@28
    move-object v3, v0

    #@29
    .line 788
    check-cast v3, Lcom/android/internal/telephony/cdma/DataProfileOmh;

    #@2b
    move-object v4, v2

    #@2c
    check-cast v4, Lcom/android/internal/telephony/cdma/DataProfileOmh;

    #@2e
    invoke-virtual {v4}, Lcom/android/internal/telephony/cdma/DataProfileOmh;->getPriority()I

    #@31
    move-result v4

    #@32
    invoke-virtual {v3, v4}, Lcom/android/internal/telephony/cdma/DataProfileOmh;->isPriorityLower(I)Z

    #@35
    move-result v3

    #@36
    if-eqz v3, :cond_39

    #@38
    move-object v2, v0

    #@39
    :cond_39
    goto :goto_5

    #@3a
    :cond_3a
    move-object v3, v0

    #@3b
    .line 792
    check-cast v3, Lcom/android/internal/telephony/cdma/DataProfileOmh;

    #@3d
    move-object v4, v2

    #@3e
    check-cast v4, Lcom/android/internal/telephony/cdma/DataProfileOmh;

    #@40
    invoke-virtual {v4}, Lcom/android/internal/telephony/cdma/DataProfileOmh;->getPriority()I

    #@43
    move-result v4

    #@44
    invoke-virtual {v3, v4}, Lcom/android/internal/telephony/cdma/DataProfileOmh;->isPriorityHigher(I)Z

    #@47
    move-result v3

    #@48
    if-eqz v3, :cond_4b

    #@4a
    move-object v2, v0

    #@4b
    :cond_4b
    goto :goto_5

    #@4c
    .line 797
    .end local v0           #dp:Lcom/android/internal/telephony/DataProfile;
    :cond_4c
    check-cast v2, Lcom/android/internal/telephony/cdma/DataProfileOmh;

    #@4e
    .end local v2           #profile:Lcom/android/internal/telephony/DataProfile;
    invoke-virtual {v2}, Lcom/android/internal/telephony/cdma/DataProfileOmh;->getPriority()I

    #@51
    move-result v3

    #@52
    return v3
.end method

.method private onGetDataCallProfileDone(Landroid/os/AsyncResult;I)V
    .registers 11
    .parameter "ar"
    .parameter "context"

    #@0
    .prologue
    .line 642
    iget-object v6, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@2
    if-eqz v6, :cond_1d

    #@4
    .line 643
    new-instance v6, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v7, "OMH: Exception in onGetDataCallProfileDone:"

    #@b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v6

    #@f
    iget-object v7, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@11
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v6

    #@15
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v6

    #@19
    invoke-virtual {p0, v6}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->log(Ljava/lang/String;)V

    #@1c
    .line 709
    :cond_1c
    :goto_1c
    return-void

    #@1d
    .line 647
    :cond_1d
    iget v6, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mOmhReadProfileContext:I

    #@1f
    if-ne p2, v6, :cond_1c

    #@21
    .line 654
    new-instance v0, Ljava/util/ArrayList;

    #@23
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@26
    .line 655
    .local v0, dataProfileListModem:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/DataProfile;>;"
    iget-object v0, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@28
    .end local v0           #dataProfileListModem:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/DataProfile;>;"
    check-cast v0, Ljava/util/ArrayList;

    #@2a
    .line 657
    .restart local v0       #dataProfileListModem:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/DataProfile;>;"
    iget-object v3, p1, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    #@2c
    check-cast v3, Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;

    #@2e
    .line 659
    .local v3, modemProfile:Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;
    iget v6, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mOmhReadProfileCount:I

    #@30
    add-int/lit8 v6, v6, -0x1

    #@32
    iput v6, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mOmhReadProfileCount:I

    #@34
    .line 661
    if-eqz v0, :cond_ba

    #@36
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@39
    move-result v6

    #@3a
    if-lez v6, :cond_ba

    #@3c
    .line 665
    invoke-virtual {v3}, Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;->getDataServiceType()Ljava/lang/String;

    #@3f
    move-result-object v5

    #@40
    .line 667
    .local v5, serviceType:Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    const-string v7, "OMH: # profiles returned from modem:"

    #@47
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v6

    #@4b
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@4e
    move-result v7

    #@4f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@52
    move-result-object v6

    #@53
    const-string v7, " for "

    #@55
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v6

    #@59
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v6

    #@5d
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@60
    move-result-object v6

    #@61
    invoke-virtual {p0, v6}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->log(Ljava/lang/String;)V

    #@64
    .line 670
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mOmhServicePriorityMap:Ljava/util/HashMap;

    #@66
    invoke-direct {p0, v0, v5}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->omhListGetArbitratedPriority(Ljava/util/ArrayList;Ljava/lang/String;)I

    #@69
    move-result v7

    #@6a
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@6d
    move-result-object v7

    #@6e
    invoke-virtual {v6, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@71
    .line 673
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@74
    move-result-object v2

    #@75
    .local v2, i$:Ljava/util/Iterator;
    :goto_75
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@78
    move-result v6

    #@79
    if-eqz v6, :cond_ba

    #@7b
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@7e
    move-result-object v1

    #@7f
    check-cast v1, Lcom/android/internal/telephony/DataProfile;

    #@81
    .local v1, dp:Lcom/android/internal/telephony/DataProfile;
    move-object v6, v1

    #@82
    .line 676
    check-cast v6, Lcom/android/internal/telephony/cdma/DataProfileOmh;

    #@84
    invoke-virtual {v6, v3}, Lcom/android/internal/telephony/cdma/DataProfileOmh;->setDataProfileTypeModem(Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;)V

    #@87
    .line 682
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->getDuplicateProfile(Lcom/android/internal/telephony/DataProfile;)Lcom/android/internal/telephony/cdma/DataProfileOmh;

    #@8a
    move-result-object v4

    #@8b
    .line 683
    .local v4, omhDuplicatedp:Lcom/android/internal/telephony/cdma/DataProfileOmh;
    if-nez v4, :cond_9c

    #@8d
    .line 684
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mTempOmhDataProfilesList:Ljava/util/ArrayList;

    #@8f
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@92
    .line 685
    check-cast v1, Lcom/android/internal/telephony/cdma/DataProfileOmh;

    #@94
    .end local v1           #dp:Lcom/android/internal/telephony/DataProfile;
    invoke-static {v5}, Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;->getDataProfileTypeModem(Ljava/lang/String;)Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;

    #@97
    move-result-object v6

    #@98
    invoke-virtual {v1, v6}, Lcom/android/internal/telephony/cdma/DataProfileOmh;->addServiceType(Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;)V

    #@9b
    goto :goto_75

    #@9c
    .line 694
    .restart local v1       #dp:Lcom/android/internal/telephony/DataProfile;
    :cond_9c
    new-instance v6, Ljava/lang/StringBuilder;

    #@9e
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@a1
    const-string v7, "OMH: Duplicate Profile "

    #@a3
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v6

    #@a7
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v6

    #@ab
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ae
    move-result-object v6

    #@af
    invoke-virtual {p0, v6}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->log(Ljava/lang/String;)V

    #@b2
    .line 695
    invoke-static {v5}, Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;->getDataProfileTypeModem(Ljava/lang/String;)Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;

    #@b5
    move-result-object v6

    #@b6
    invoke-virtual {v4, v6}, Lcom/android/internal/telephony/cdma/DataProfileOmh;->addServiceType(Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;)V

    #@b9
    goto :goto_75

    #@ba
    .line 702
    .end local v1           #dp:Lcom/android/internal/telephony/DataProfile;
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v4           #omhDuplicatedp:Lcom/android/internal/telephony/cdma/DataProfileOmh;
    .end local v5           #serviceType:Ljava/lang/String;
    :cond_ba
    iget v6, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mOmhReadProfileCount:I

    #@bc
    if-nez v6, :cond_1c

    #@be
    .line 703
    const-string v6, "OMH: Modem omh profile read complete."

    #@c0
    invoke-virtual {p0, v6}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->log(Ljava/lang/String;)V

    #@c3
    .line 704
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->addServiceTypeToUnSpecified()V

    #@c6
    .line 705
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mDataProfilesList:Ljava/util/ArrayList;

    #@c8
    iget-object v7, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mTempOmhDataProfilesList:Ljava/util/ArrayList;

    #@ca
    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    #@cd
    .line 706
    iget-object v6, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mModemDataProfileRegistrants:Landroid/os/RegistrantList;

    #@cf
    invoke-virtual {v6}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@d2
    goto/16 :goto_1c
.end method

.method private onReadDataProfilesFromModem()V
    .registers 10

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 615
    const-string v4, "OMH: onReadDataProfilesFromModem()"

    #@3
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->log(Ljava/lang/String;)V

    #@6
    .line 616
    iget v4, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mOmhReadProfileContext:I

    #@8
    add-int/lit8 v4, v4, 0x1

    #@a
    iput v4, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mOmhReadProfileContext:I

    #@c
    .line 618
    iput v8, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mOmhReadProfileCount:I

    #@e
    .line 620
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mOmhDataProfilesList:Ljava/util/ArrayList;

    #@10
    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    #@13
    .line 621
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mTempOmhDataProfilesList:Ljava/util/ArrayList;

    #@15
    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    #@18
    .line 622
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mOmhServicePriorityMap:Ljava/util/HashMap;

    #@1a
    invoke-virtual {v4}, Ljava/util/HashMap;->clear()V

    #@1d
    .line 625
    invoke-static {}, Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;->values()[Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;

    #@20
    move-result-object v0

    #@21
    .local v0, arr$:[Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;
    array-length v2, v0

    #@22
    .local v2, len$:I
    const/4 v1, 0x0

    #@23
    .local v1, i$:I
    :goto_23
    if-ge v1, v2, :cond_5c

    #@25
    aget-object v3, v0, v1

    #@27
    .line 626
    .local v3, p:Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;
    new-instance v4, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const-string v5, "OMH: Reading profiles for:"

    #@2e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v4

    #@32
    invoke-virtual {v3}, Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;->getid()I

    #@35
    move-result v5

    #@36
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@39
    move-result-object v4

    #@3a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d
    move-result-object v4

    #@3e
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->log(Ljava/lang/String;)V

    #@41
    .line 627
    iget v4, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mOmhReadProfileCount:I

    #@43
    add-int/lit8 v4, v4, 0x1

    #@45
    iput v4, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mOmhReadProfileCount:I

    #@47
    .line 628
    iget-object v4, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mPhone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@49
    iget-object v4, v4, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@4b
    invoke-virtual {v3}, Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;->getid()I

    #@4e
    move-result v5

    #@4f
    const/4 v6, 0x1

    #@50
    iget v7, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mOmhReadProfileContext:I

    #@52
    invoke-virtual {p0, v6, v7, v8, v3}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@55
    move-result-object v6

    #@56
    invoke-interface {v4, v5, v6}, Lcom/android/internal/telephony/CommandsInterface;->getDataCallProfile(ILandroid/os/Message;)V

    #@59
    .line 625
    add-int/lit8 v1, v1, 0x1

    #@5b
    goto :goto_23

    #@5c
    .line 635
    .end local v3           #p:Lcom/android/internal/telephony/cdma/DataProfileOmh$DataProfileTypeModem;
    :cond_5c
    return-void
.end method

.method private parseTypes(Ljava/lang/String;)[Ljava/lang/String;
    .registers 5
    .parameter "types"

    #@0
    .prologue
    .line 400
    if-eqz p1, :cond_a

    #@2
    const-string v1, ""

    #@4
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_13

    #@a
    .line 401
    :cond_a
    const/4 v1, 0x1

    #@b
    new-array v0, v1, [Ljava/lang/String;

    #@d
    .line 402
    .local v0, result:[Ljava/lang/String;
    const/4 v1, 0x0

    #@e
    const-string v2, "*"

    #@10
    aput-object v2, v0, v1

    #@12
    .line 406
    :goto_12
    return-object v0

    #@13
    .line 404
    .end local v0           #result:[Ljava/lang/String;
    :cond_13
    const-string v1, ","

    #@15
    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    .restart local v0       #result:[Ljava/lang/String;
    goto :goto_12
.end method

.method private populateDataProfilesList(Landroid/database/Cursor;)V
    .registers 13
    .parameter "cursor"

    #@0
    .prologue
    .line 371
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    #@3
    move-result v1

    #@4
    if-eqz v1, :cond_7e

    #@6
    .line 373
    :cond_6
    const-string v1, "type"

    #@8
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@b
    move-result v1

    #@c
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@f
    move-result-object v1

    #@10
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->parseTypes(Ljava/lang/String;)[Ljava/lang/String;

    #@13
    move-result-object v7

    #@14
    .line 375
    .local v7, types:[Ljava/lang/String;
    new-instance v0, Lcom/android/internal/telephony/cdma/DataProfileCdma;

    #@16
    const-string v1, "_id"

    #@18
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@1b
    move-result v1

    #@1c
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    #@1f
    move-result v1

    #@20
    const-string v2, "numeric"

    #@22
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@25
    move-result v2

    #@26
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@29
    move-result-object v2

    #@2a
    const-string v3, "apn"

    #@2c
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@2f
    move-result v3

    #@30
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@33
    move-result-object v3

    #@34
    const-string v4, "user"

    #@36
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@39
    move-result v4

    #@3a
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@3d
    move-result-object v4

    #@3e
    const-string v5, "password"

    #@40
    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@43
    move-result v5

    #@44
    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@47
    move-result-object v5

    #@48
    const-string v6, "authtype"

    #@4a
    invoke-interface {p1, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@4d
    move-result v6

    #@4e
    invoke-interface {p1, v6}, Landroid/database/Cursor;->getInt(I)I

    #@51
    move-result v6

    #@52
    const-string v8, "protocol"

    #@54
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@57
    move-result v8

    #@58
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@5b
    move-result-object v8

    #@5c
    const-string v9, "roaming_protocol"

    #@5e
    invoke-interface {p1, v9}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@61
    move-result v9

    #@62
    invoke-interface {p1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@65
    move-result-object v9

    #@66
    const-string v10, "bearer"

    #@68
    invoke-interface {p1, v10}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@6b
    move-result v10

    #@6c
    invoke-interface {p1, v10}, Landroid/database/Cursor;->getInt(I)I

    #@6f
    move-result v10

    #@70
    invoke-direct/range {v0 .. v10}, Lcom/android/internal/telephony/cdma/DataProfileCdma;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    #@73
    .line 387
    .local v0, nai:Lcom/android/internal/telephony/cdma/DataProfileCdma;
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mDataProfilesList:Ljava/util/ArrayList;

    #@75
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@78
    .line 389
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    #@7b
    move-result v1

    #@7c
    if-nez v1, :cond_6

    #@7e
    .line 391
    .end local v0           #nai:Lcom/android/internal/telephony/cdma/DataProfileCdma;
    .end local v7           #types:[Ljava/lang/String;
    :cond_7e
    return-void
.end method

.method private readNaiListFromDatabase()V
    .registers 9

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 346
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->getOperatorNumeric()Ljava/lang/String;

    #@4
    move-result-object v7

    #@5
    .line 347
    .local v7, operator:Ljava/lang/String;
    if-eqz v7, :cond_e

    #@7
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    #@a
    move-result v0

    #@b
    const/4 v1, 0x2

    #@c
    if-ge v0, v1, :cond_14

    #@e
    .line 348
    :cond_e
    const-string v0, "operatorNumeric invalid. Won\'t read database"

    #@10
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->loge(Ljava/lang/String;)V

    #@13
    .line 368
    :cond_13
    :goto_13
    return-void

    #@14
    .line 352
    :cond_14
    new-instance v0, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v1, "Loading data profiles for operator = "

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v0

    #@1f
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v0

    #@27
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->log(Ljava/lang/String;)V

    #@2a
    .line 353
    new-instance v0, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v1, "numeric = \'"

    #@31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v0

    #@35
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v0

    #@39
    const-string v1, "\'"

    #@3b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v0

    #@3f
    const-string v1, " and profile_type = \'nai\'"

    #@41
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v0

    #@45
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v3

    #@49
    .line 356
    .local v3, selection:Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    #@4b
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@4e
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v0

    #@52
    const-string v1, " and carrier_enabled = 1"

    #@54
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v0

    #@58
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5b
    move-result-object v3

    #@5c
    .line 357
    new-instance v0, Ljava/lang/StringBuilder;

    #@5e
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@61
    const-string v1, "readNaiListFromDatabase: selection="

    #@63
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v0

    #@67
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v0

    #@6b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6e
    move-result-object v0

    #@6f
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->log(Ljava/lang/String;)V

    #@72
    .line 359
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mPhone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@74
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@77
    move-result-object v0

    #@78
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7b
    move-result-object v0

    #@7c
    sget-object v1, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    #@7e
    move-object v4, v2

    #@7f
    move-object v5, v2

    #@80
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@83
    move-result-object v6

    #@84
    .line 362
    .local v6, cursor:Landroid/database/Cursor;
    if-eqz v6, :cond_13

    #@86
    .line 363
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    #@89
    move-result v0

    #@8a
    if-lez v0, :cond_8f

    #@8c
    .line 364
    invoke-direct {p0, v6}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->populateDataProfilesList(Landroid/database/Cursor;)V

    #@8f
    .line 366
    :cond_8f
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@92
    goto :goto_13
.end method

.method private setActiveDpToDefault()V
    .registers 2

    #@0
    .prologue
    .line 318
    const-string v0, "default"

    #@2
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->getDataProfile(Ljava/lang/String;)Lcom/android/internal/telephony/DataProfile;

    #@5
    move-result-object v0

    #@6
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mActiveDp:Lcom/android/internal/telephony/DataProfile;

    #@8
    .line 319
    return-void
.end method


# virtual methods
.method public clearActiveDataProfile()V
    .registers 2

    #@0
    .prologue
    .line 801
    const/4 v0, 0x0

    #@1
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mActiveDp:Lcom/android/internal/telephony/DataProfile;

    #@3
    .line 802
    return-void
.end method

.method public dispose()V
    .registers 1

    #@0
    .prologue
    .line 410
    return-void
.end method

.method protected finalize()V
    .registers 3

    #@0
    .prologue
    .line 413
    const-string v0, "CDMA"

    #@2
    const-string v1, "CdmaDataProfileTracker finalized"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 414
    return-void
.end method

.method protected getActiveApnTypes()[Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 823
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mActiveDp:Lcom/android/internal/telephony/DataProfile;

    #@2
    if-eqz v1, :cond_b

    #@4
    .line 824
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mActiveDp:Lcom/android/internal/telephony/DataProfile;

    #@6
    invoke-virtual {v1}, Lcom/android/internal/telephony/DataProfile;->getServiceTypes()[Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    .line 829
    .local v0, result:[Ljava/lang/String;
    :goto_a
    return-object v0

    #@b
    .line 826
    .end local v0           #result:[Ljava/lang/String;
    :cond_b
    const/4 v1, 0x1

    #@c
    new-array v0, v1, [Ljava/lang/String;

    #@e
    .line 827
    .restart local v0       #result:[Ljava/lang/String;
    const/4 v1, 0x0

    #@f
    const-string v2, "default"

    #@11
    aput-object v2, v0, v1

    #@13
    goto :goto_a
.end method

.method public getDataProfile(Ljava/lang/String;)Lcom/android/internal/telephony/DataProfile;
    .registers 7
    .parameter "serviceType"

    #@0
    .prologue
    .line 727
    const/4 v2, 0x0

    #@1
    .line 730
    .local v2, profile:Lcom/android/internal/telephony/DataProfile;
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mDataProfilesList:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@6
    move-result-object v1

    #@7
    .local v1, i$:Ljava/util/Iterator;
    :cond_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@a
    move-result v3

    #@b
    if-eqz v3, :cond_26

    #@d
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@10
    move-result-object v0

    #@11
    check-cast v0, Lcom/android/internal/telephony/DataProfile;

    #@13
    .line 731
    .local v0, dp:Lcom/android/internal/telephony/DataProfile;
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/DataProfile;->canHandleType(Ljava/lang/String;)Z

    #@16
    move-result v3

    #@17
    if-eqz v3, :cond_7

    #@19
    .line 732
    iget-boolean v3, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mIsOmhEnabled:Z

    #@1b
    if-eqz v3, :cond_25

    #@1d
    invoke-virtual {v0}, Lcom/android/internal/telephony/DataProfile;->getDataProfileType()Lcom/android/internal/telephony/DataProfile$DataProfileType;

    #@20
    move-result-object v3

    #@21
    sget-object v4, Lcom/android/internal/telephony/DataProfile$DataProfileType;->PROFILE_TYPE_OMH:Lcom/android/internal/telephony/DataProfile$DataProfileType;

    #@23
    if-ne v3, v4, :cond_7

    #@25
    .line 737
    :cond_25
    move-object v2, v0

    #@26
    .line 741
    .end local v0           #dp:Lcom/android/internal/telephony/DataProfile;
    :cond_26
    return-object v2
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 4
    .parameter "msg"

    #@0
    .prologue
    .line 427
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mPhone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@2
    iget-boolean v0, v0, Lcom/android/internal/telephony/PhoneBase;->mIsTheCurrentActivePhone:Z

    #@4
    if-nez v0, :cond_e

    #@6
    .line 428
    const-string v0, "CDMA"

    #@8
    const-string v1, "Ignore CDMA msgs since CDMA phone is inactive"

    #@a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 449
    :goto_d
    return-void

    #@e
    .line 432
    :cond_e
    iget v0, p1, Landroid/os/Message;->what:I

    #@10
    packed-switch v0, :pswitch_data_2a

    #@13
    .line 446
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    #@16
    goto :goto_d

    #@17
    .line 434
    :pswitch_17
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->loadProfiles()V

    #@1a
    goto :goto_d

    #@1b
    .line 437
    :pswitch_1b
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->onReadDataProfilesFromModem()V

    #@1e
    goto :goto_d

    #@1f
    .line 441
    :pswitch_1f
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@21
    check-cast v0, Landroid/os/AsyncResult;

    #@23
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@25
    invoke-direct {p0, v0, v1}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->onGetDataCallProfileDone(Landroid/os/AsyncResult;I)V

    #@28
    goto :goto_d

    #@29
    .line 432
    nop

    #@2a
    :pswitch_data_2a
    .packed-switch 0x0
        :pswitch_1b
        :pswitch_1f
        :pswitch_17
    .end packed-switch
.end method

.method public isApnTypeActive(Ljava/lang/String;)Z
    .registers 3
    .parameter "type"

    #@0
    .prologue
    .line 805
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mActiveDp:Lcom/android/internal/telephony/DataProfile;

    #@2
    if-eqz v0, :cond_e

    #@4
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mActiveDp:Lcom/android/internal/telephony/DataProfile;

    #@6
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/DataProfile;->canHandleType(Ljava/lang/String;)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method protected isApnTypeAvailable(Ljava/lang/String;)Z
    .registers 7
    .parameter "type"

    #@0
    .prologue
    .line 813
    sget-object v0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mSupportedApnTypes:[Ljava/lang/String;

    #@2
    .local v0, arr$:[Ljava/lang/String;
    array-length v2, v0

    #@3
    .local v2, len$:I
    const/4 v1, 0x0

    #@4
    .local v1, i$:I
    :goto_4
    if-ge v1, v2, :cond_13

    #@6
    aget-object v3, v0, v1

    #@8
    .line 814
    .local v3, s:Ljava/lang/String;
    invoke-static {p1, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    #@b
    move-result v4

    #@c
    if-eqz v4, :cond_10

    #@e
    .line 815
    const/4 v4, 0x1

    #@f
    .line 818
    .end local v3           #s:Ljava/lang/String;
    :goto_f
    return v4

    #@10
    .line 813
    .restart local v3       #s:Ljava/lang/String;
    :cond_10
    add-int/lit8 v1, v1, 0x1

    #@12
    goto :goto_4

    #@13
    .line 818
    .end local v3           #s:Ljava/lang/String;
    :cond_13
    const/4 v4, 0x0

    #@14
    goto :goto_f
.end method

.method public isOmhEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 809
    iget-boolean v0, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mIsOmhEnabled:Z

    #@2
    return v0
.end method

.method loadProfiles()V
    .registers 3

    #@0
    .prologue
    .line 255
    const-string v0, "loadProfiles..."

    #@2
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->log(Ljava/lang/String;)V

    #@5
    .line 256
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mDataProfilesList:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    #@a
    .line 258
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->readNaiListFromDatabase()V

    #@d
    .line 260
    new-instance v0, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v1, "Got "

    #@14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v0

    #@18
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mDataProfilesList:Ljava/util/ArrayList;

    #@1a
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@1d
    move-result v1

    #@1e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@21
    move-result-object v0

    #@22
    const-string v1, " profiles from database"

    #@24
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v0

    #@28
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v0

    #@2c
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->log(Ljava/lang/String;)V

    #@2f
    .line 262
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mDataProfilesList:Ljava/util/ArrayList;

    #@31
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@34
    move-result v0

    #@35
    if-nez v0, :cond_3a

    #@37
    .line 264
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->createDefaultDataProfiles()V

    #@3a
    .line 268
    :cond_3a
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->setActiveDpToDefault()V

    #@3d
    .line 271
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->readDataProfilesFromModem()Z

    #@40
    .line 272
    return-void
.end method

.method protected log(Ljava/lang/String;)V
    .registers 5
    .parameter "s"

    #@0
    .prologue
    .line 833
    const-string v0, "CDMA"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[CdmaDataProfileTracker] "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 834
    return-void
.end method

.method protected loge(Ljava/lang/String;)V
    .registers 5
    .parameter "s"

    #@0
    .prologue
    .line 837
    const-string v0, "CDMA"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[CdmaDataProfileTracker] "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 838
    return-void
.end method

.method public readDataProfileFromDb()V
    .registers 13

    #@0
    .prologue
    const/4 v11, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 453
    const/4 v7, 0x0

    #@3
    .line 458
    .local v7, operator:Ljava/lang/String;
    invoke-static {}, Lcom/android/internal/telephony/uicc/UiccController;->getInstance()Lcom/android/internal/telephony/uicc/UiccController;

    #@6
    move-result-object v9

    #@7
    .line 459
    .local v9, uiccController:Lcom/android/internal/telephony/uicc/UiccController;
    if-eqz v9, :cond_2a

    #@9
    .line 460
    invoke-virtual {v9, v11}, Lcom/android/internal/telephony/uicc/UiccController;->getUiccCardApplication(I)Lcom/android/internal/telephony/uicc/UiccCardApplication;

    #@c
    move-result-object v10

    #@d
    .line 462
    .local v10, usimUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;
    if-eqz v10, :cond_1b

    #@f
    .line 463
    invoke-virtual {v10}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getIccRecords()Lcom/android/internal/telephony/uicc/IccRecords;

    #@12
    move-result-object v8

    #@13
    check-cast v8, Lcom/android/internal/telephony/uicc/SIMRecords;

    #@15
    .line 465
    .local v8, uSimRecords:Lcom/android/internal/telephony/uicc/SIMRecords;
    if-eqz v8, :cond_1b

    #@17
    .line 466
    invoke-virtual {v8}, Lcom/android/internal/telephony/uicc/SIMRecords;->getOperatorNumeric()Ljava/lang/String;

    #@1a
    move-result-object v7

    #@1b
    .line 474
    .end local v8           #uSimRecords:Lcom/android/internal/telephony/uicc/SIMRecords;
    .end local v10           #usimUiccApplication:Lcom/android/internal/telephony/uicc/UiccCardApplication;
    :cond_1b
    :goto_1b
    if-eqz v7, :cond_24

    #@1d
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    #@20
    move-result v0

    #@21
    const/4 v1, 0x2

    #@22
    if-ge v0, v1, :cond_30

    #@24
    .line 475
    :cond_24
    const-string v0, "operatorNumeric invalid. Won\'t read database"

    #@26
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->log(Ljava/lang/String;)V

    #@29
    .line 509
    :goto_29
    return-void

    #@2a
    .line 471
    :cond_2a
    const-string v0, "UiccController invalid, fail to read operator"

    #@2c
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->log(Ljava/lang/String;)V

    #@2f
    goto :goto_1b

    #@30
    .line 479
    :cond_30
    new-instance v0, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    const-string v1, "Loading data profiles for operator = "

    #@37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v0

    #@3b
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v0

    #@3f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v0

    #@43
    invoke-virtual {p0, v0}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->log(Ljava/lang/String;)V

    #@46
    .line 481
    new-instance v0, Ljava/lang/StringBuilder;

    #@48
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@4b
    const-string v1, "numeric = \'"

    #@4d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v0

    #@51
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v0

    #@55
    const-string v1, "\'"

    #@57
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v0

    #@5b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v3

    #@5f
    .line 482
    .local v3, selection:Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    #@61
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@64
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v0

    #@68
    const-string v1, " and carrier_enabled = 1"

    #@6a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v0

    #@6e
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@71
    move-result-object v3

    #@72
    .line 484
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mPhone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@74
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CDMAPhone;->getContext()Landroid/content/Context;

    #@77
    move-result-object v0

    #@78
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7b
    move-result-object v0

    #@7c
    sget-object v1, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    #@7e
    move-object v4, v2

    #@7f
    move-object v5, v2

    #@80
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@83
    move-result-object v6

    #@84
    .line 488
    .local v6, cursor:Landroid/database/Cursor;
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mPhone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@86
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@88
    new-instance v1, Ljava/util/ArrayList;

    #@8a
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@8d
    iput-object v1, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mAllApns:Ljava/util/ArrayList;

    #@8f
    .line 490
    if-eqz v6, :cond_a4

    #@91
    .line 491
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    #@94
    move-result v0

    #@95
    if-lez v0, :cond_a1

    #@97
    .line 492
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mPhone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@99
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@9b
    invoke-direct {p0, v6}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->createApnListonCDMA(Landroid/database/Cursor;)Ljava/util/ArrayList;

    #@9e
    move-result-object v1

    #@9f
    iput-object v1, v0, Lcom/android/internal/telephony/DataConnectionTracker;->mAllApns:Ljava/util/ArrayList;

    #@a1
    .line 494
    :cond_a1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@a4
    .line 498
    :cond_a4
    const-string v0, "311480"

    #@a6
    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a9
    move-result v0

    #@aa
    if-nez v0, :cond_bc

    #@ac
    const-string v0, "00101"

    #@ae
    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b1
    move-result v0

    #@b2
    if-nez v0, :cond_bc

    #@b4
    const-string v0, "001010"

    #@b6
    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b9
    move-result v0

    #@ba
    if-eqz v0, :cond_e1

    #@bc
    :cond_bc
    const-string v0, "VZWBASE"

    #@be
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mPhone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@c0
    iget-object v1, v1, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@c2
    invoke-interface {v1}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@c5
    move-result-object v1

    #@c6
    iget-object v1, v1, Lcom/android/internal/telephony/LGfeature;->myfeatureset:Ljava/lang/String;

    #@c8
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@cb
    move-result v0

    #@cc
    if-eqz v0, :cond_e1

    #@ce
    .line 500
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mPhone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@d0
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@d2
    invoke-interface {v0}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@d5
    move-result-object v0

    #@d6
    iput-boolean v11, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_BLOCK_DATA_CALL_WHEN_ADMIN_PDN_DSIABLED_VZW:Z

    #@d8
    .line 508
    :goto_d8
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mPhone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@da
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    #@dc
    invoke-virtual {v0}, Lcom/android/internal/telephony/DataConnectionTracker;->sendPdnTable()V

    #@df
    goto/16 :goto_29

    #@e1
    .line 504
    :cond_e1
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mPhone:Lcom/android/internal/telephony/cdma/CDMAPhone;

    #@e3
    iget-object v0, v0, Lcom/android/internal/telephony/PhoneBase;->mCM:Lcom/android/internal/telephony/CommandsInterface;

    #@e5
    invoke-interface {v0}, Lcom/android/internal/telephony/CommandsInterface;->getLGfeatures()Lcom/android/internal/telephony/LGfeature;

    #@e8
    move-result-object v0

    #@e9
    const/4 v1, 0x0

    #@ea
    iput-boolean v1, v0, Lcom/android/internal/telephony/LGfeature;->LGP_DATA_DATACONNECTION_BLOCK_DATA_CALL_WHEN_ADMIN_PDN_DSIABLED_VZW:Z

    #@ec
    goto :goto_d8
.end method

.method public readDataProfilesFromModem()Z
    .registers 3

    #@0
    .prologue
    .line 601
    const/4 v0, 0x0

    #@1
    .line 602
    .local v0, retVal:Z
    iget-boolean v1, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mIsOmhEnabled:Z

    #@3
    if-eqz v1, :cond_f

    #@5
    .line 603
    const/4 v1, 0x0

    #@6
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->obtainMessage(I)Landroid/os/Message;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->sendMessage(Landroid/os/Message;)Z

    #@d
    .line 604
    const/4 v0, 0x1

    #@e
    .line 608
    :goto_e
    return v0

    #@f
    .line 606
    :cond_f
    const-string v1, "OMH is disabled, ignoring request!"

    #@11
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->log(Ljava/lang/String;)V

    #@14
    goto :goto_e
.end method

.method public registerForModemProfileReady(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 417
    new-instance v0, Landroid/os/Registrant;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 418
    .local v0, r:Landroid/os/Registrant;
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mModemDataProfileRegistrants:Landroid/os/RegistrantList;

    #@7
    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 419
    return-void
.end method

.method public unregisterForModemProfileReady(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 422
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/CdmaDataProfileTracker;->mModemDataProfileRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 423
    return-void
.end method
