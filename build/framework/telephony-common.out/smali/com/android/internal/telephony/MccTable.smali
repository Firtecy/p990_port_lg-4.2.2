.class public final Lcom/android/internal/telephony/MccTable;
.super Ljava/lang/Object;
.source "MccTable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/MccTable$MccEntry;
    }
.end annotation


# static fields
.field static final LOG_TAG:Ljava/lang/String; = "MccTable"

.field static table:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/telephony/MccTable$MccEntry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/16 v7, 0xf0

    #@2
    const/4 v6, 0x3

    #@3
    const/4 v5, 0x2

    #@4
    .line 351
    new-instance v0, Ljava/util/ArrayList;

    #@6
    invoke-direct {v0, v7}, Ljava/util/ArrayList;-><init>(I)V

    #@9
    sput-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@b
    .line 369
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@d
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@f
    const/16 v2, 0xca

    #@11
    const-string v3, "gr"

    #@13
    const-string v4, "el"

    #@15
    invoke-direct {v1, v2, v3, v5, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@18
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1b
    .line 370
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@1d
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@1f
    const/16 v2, 0xcc

    #@21
    const-string v3, "nl"

    #@23
    const-string v4, "nl"

    #@25
    invoke-direct {v1, v2, v3, v5, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@28
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2b
    .line 371
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@2d
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@2f
    const/16 v2, 0xce

    #@31
    const-string v3, "be"

    #@33
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@36
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@39
    .line 372
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@3b
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@3d
    const/16 v2, 0xd0

    #@3f
    const-string v3, "fr"

    #@41
    const-string v4, "fr"

    #@43
    invoke-direct {v1, v2, v3, v5, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@46
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@49
    .line 373
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@4b
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@4d
    const/16 v2, 0xd4

    #@4f
    const-string v3, "mc"

    #@51
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@54
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@57
    .line 374
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@59
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@5b
    const/16 v2, 0xd5

    #@5d
    const-string v3, "ad"

    #@5f
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@62
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@65
    .line 375
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@67
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@69
    const/16 v2, 0xd6

    #@6b
    const-string v3, "es"

    #@6d
    const-string v4, "es"

    #@6f
    invoke-direct {v1, v2, v3, v5, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@72
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@75
    .line 376
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@77
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@79
    const/16 v2, 0xd8

    #@7b
    const-string v3, "hu"

    #@7d
    const-string v4, "hu"

    #@7f
    invoke-direct {v1, v2, v3, v5, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@82
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@85
    .line 377
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@87
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@89
    const/16 v2, 0xda

    #@8b
    const-string v3, "ba"

    #@8d
    const-string v4, "bs"

    #@8f
    invoke-direct {v1, v2, v3, v5, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@92
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@95
    .line 378
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@97
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@99
    const/16 v2, 0xdb

    #@9b
    const-string v3, "hr"

    #@9d
    const-string v4, "hr"

    #@9f
    invoke-direct {v1, v2, v3, v5, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@a2
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@a5
    .line 379
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@a7
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@a9
    const/16 v2, 0xdc

    #@ab
    const-string v3, "rs"

    #@ad
    const-string v4, "sr"

    #@af
    invoke-direct {v1, v2, v3, v5, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@b2
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@b5
    .line 380
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@b7
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@b9
    const/16 v2, 0xde

    #@bb
    const-string v3, "it"

    #@bd
    const-string v4, "it"

    #@bf
    invoke-direct {v1, v2, v3, v5, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@c2
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@c5
    .line 381
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@c7
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@c9
    const/16 v2, 0xe1

    #@cb
    const-string v3, "va"

    #@cd
    const-string v4, "it"

    #@cf
    invoke-direct {v1, v2, v3, v5, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@d2
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@d5
    .line 382
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@d7
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@d9
    const/16 v2, 0xe2

    #@db
    const-string v3, "ro"

    #@dd
    const-string v4, "ro"

    #@df
    invoke-direct {v1, v2, v3, v5, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@e2
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@e5
    .line 383
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@e7
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@e9
    const/16 v2, 0xe4

    #@eb
    const-string v3, "ch"

    #@ed
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@f0
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@f3
    .line 384
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@f5
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@f7
    const/16 v2, 0xe6

    #@f9
    const-string v3, "cz"

    #@fb
    const-string v4, "cs"

    #@fd
    invoke-direct {v1, v2, v3, v5, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@100
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@103
    .line 385
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@105
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@107
    const/16 v2, 0xe7

    #@109
    const-string v3, "sk"

    #@10b
    const-string v4, "sk"

    #@10d
    invoke-direct {v1, v2, v3, v5, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@110
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@113
    .line 386
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@115
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@117
    const/16 v2, 0xe8

    #@119
    const-string v3, "at"

    #@11b
    const-string v4, "de"

    #@11d
    invoke-direct {v1, v2, v3, v5, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@120
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@123
    .line 387
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@125
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@127
    const/16 v2, 0xea

    #@129
    const-string v3, "gb"

    #@12b
    const-string v4, "en"

    #@12d
    invoke-direct {v1, v2, v3, v5, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@130
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@133
    .line 388
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@135
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@137
    const/16 v2, 0xeb

    #@139
    const-string v3, "gb"

    #@13b
    const-string v4, "en"

    #@13d
    invoke-direct {v1, v2, v3, v5, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@140
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@143
    .line 389
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@145
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@147
    const/16 v2, 0xee

    #@149
    const-string v3, "dk"

    #@14b
    const-string v4, "da"

    #@14d
    invoke-direct {v1, v2, v3, v5, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@150
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@153
    .line 390
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@155
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@157
    const-string v2, "se"

    #@159
    const-string v3, "sv"

    #@15b
    invoke-direct {v1, v7, v2, v5, v3}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@15e
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@161
    .line 391
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@163
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@165
    const/16 v2, 0xf2

    #@167
    const-string v3, "no"

    #@169
    const-string v4, "nb"

    #@16b
    invoke-direct {v1, v2, v3, v5, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@16e
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@171
    .line 392
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@173
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@175
    const/16 v2, 0xf4

    #@177
    const-string v3, "fi"

    #@179
    const-string v4, "fi"

    #@17b
    invoke-direct {v1, v2, v3, v5, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@17e
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@181
    .line 393
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@183
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@185
    const/16 v2, 0xf6

    #@187
    const-string v3, "lt"

    #@189
    const-string v4, "lt"

    #@18b
    invoke-direct {v1, v2, v3, v5, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@18e
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@191
    .line 394
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@193
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@195
    const/16 v2, 0xf7

    #@197
    const-string v3, "lv"

    #@199
    const-string v4, "lv"

    #@19b
    invoke-direct {v1, v2, v3, v5, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@19e
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1a1
    .line 395
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@1a3
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@1a5
    const/16 v2, 0xf8

    #@1a7
    const-string v3, "ee"

    #@1a9
    const-string v4, "et"

    #@1ab
    invoke-direct {v1, v2, v3, v5, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@1ae
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1b1
    .line 396
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@1b3
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@1b5
    const/16 v2, 0xfa

    #@1b7
    const-string v3, "ru"

    #@1b9
    const-string v4, "ru"

    #@1bb
    invoke-direct {v1, v2, v3, v5, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@1be
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1c1
    .line 397
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@1c3
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@1c5
    const/16 v2, 0xff

    #@1c7
    const-string v3, "ua"

    #@1c9
    const-string v4, "uk"

    #@1cb
    invoke-direct {v1, v2, v3, v5, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@1ce
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1d1
    .line 398
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@1d3
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@1d5
    const/16 v2, 0x101

    #@1d7
    const-string v3, "by"

    #@1d9
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@1dc
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1df
    .line 399
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@1e1
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@1e3
    const/16 v2, 0x103

    #@1e5
    const-string v3, "md"

    #@1e7
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@1ea
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1ed
    .line 400
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@1ef
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@1f1
    const/16 v2, 0x104

    #@1f3
    const-string v3, "pl"

    #@1f5
    const-string v4, "pl"

    #@1f7
    invoke-direct {v1, v2, v3, v5, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@1fa
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1fd
    .line 401
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@1ff
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@201
    const/16 v2, 0x106

    #@203
    const-string v3, "de"

    #@205
    const-string v4, "de"

    #@207
    invoke-direct {v1, v2, v3, v5, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@20a
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@20d
    .line 402
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@20f
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@211
    const/16 v2, 0x10a

    #@213
    const-string v3, "gi"

    #@215
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@218
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@21b
    .line 403
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@21d
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@21f
    const/16 v2, 0x10c

    #@221
    const-string v3, "pt"

    #@223
    const-string v4, "pt"

    #@225
    invoke-direct {v1, v2, v3, v5, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@228
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@22b
    .line 404
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@22d
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@22f
    const/16 v2, 0x10e

    #@231
    const-string v3, "lu"

    #@233
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@236
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@239
    .line 405
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@23b
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@23d
    const/16 v2, 0x110

    #@23f
    const-string v3, "ie"

    #@241
    const-string v4, "en"

    #@243
    invoke-direct {v1, v2, v3, v5, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@246
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@249
    .line 406
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@24b
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@24d
    const/16 v2, 0x112

    #@24f
    const-string v3, "is"

    #@251
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@254
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@257
    .line 407
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@259
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@25b
    const/16 v2, 0x114

    #@25d
    const-string v3, "al"

    #@25f
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@262
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@265
    .line 408
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@267
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@269
    const/16 v2, 0x116

    #@26b
    const-string v3, "mt"

    #@26d
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@270
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@273
    .line 409
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@275
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@277
    const/16 v2, 0x118

    #@279
    const-string v3, "cy"

    #@27b
    const-string v4, "el"

    #@27d
    invoke-direct {v1, v2, v3, v5, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@280
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@283
    .line 410
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@285
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@287
    const/16 v2, 0x11a

    #@289
    const-string v3, "ge"

    #@28b
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@28e
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@291
    .line 411
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@293
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@295
    const/16 v2, 0x11b

    #@297
    const-string v3, "am"

    #@299
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@29c
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@29f
    .line 412
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@2a1
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@2a3
    const/16 v2, 0x11c

    #@2a5
    const-string v3, "bg"

    #@2a7
    const-string v4, "bg"

    #@2a9
    invoke-direct {v1, v2, v3, v5, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@2ac
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2af
    .line 413
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@2b1
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@2b3
    const/16 v2, 0x11e

    #@2b5
    const-string v3, "tr"

    #@2b7
    const-string v4, "tr"

    #@2b9
    invoke-direct {v1, v2, v3, v5, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@2bc
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2bf
    .line 414
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@2c1
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@2c3
    const/16 v2, 0x120

    #@2c5
    const-string v3, "fo"

    #@2c7
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@2ca
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2cd
    .line 415
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@2cf
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@2d1
    const/16 v2, 0x121

    #@2d3
    const-string v3, "ge"

    #@2d5
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@2d8
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2db
    .line 416
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@2dd
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@2df
    const/16 v2, 0x122

    #@2e1
    const-string v3, "gl"

    #@2e3
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@2e6
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2e9
    .line 417
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@2eb
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@2ed
    const/16 v2, 0x124

    #@2ef
    const-string v3, "sm"

    #@2f1
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@2f4
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2f7
    .line 418
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@2f9
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@2fb
    const/16 v2, 0x125

    #@2fd
    const-string v3, "si"

    #@2ff
    const-string v4, "sl"

    #@301
    invoke-direct {v1, v2, v3, v5, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@304
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@307
    .line 420
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@309
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@30b
    const/16 v2, 0x126

    #@30d
    const-string v3, "mk"

    #@30f
    const-string v4, "mk"

    #@311
    invoke-direct {v1, v2, v3, v5, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@314
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@317
    .line 422
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@319
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@31b
    const/16 v2, 0x127

    #@31d
    const-string v3, "li"

    #@31f
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@322
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@325
    .line 423
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@327
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@329
    const/16 v2, 0x129

    #@32b
    const-string v3, "me"

    #@32d
    const-string v4, "sr"

    #@32f
    invoke-direct {v1, v2, v3, v5, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@332
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@335
    .line 424
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@337
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@339
    const/16 v2, 0x12e

    #@33b
    const-string v3, "ca"

    #@33d
    const-string v4, ""

    #@33f
    invoke-direct {v1, v2, v3, v6, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@342
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@345
    .line 425
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@347
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@349
    const/16 v2, 0x134

    #@34b
    const-string v3, "pm"

    #@34d
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@350
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@353
    .line 426
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@355
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@357
    const/16 v2, 0x136

    #@359
    const-string v3, "us"

    #@35b
    const-string v4, "en"

    #@35d
    invoke-direct {v1, v2, v3, v6, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@360
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@363
    .line 427
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@365
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@367
    const/16 v2, 0x137

    #@369
    const-string v3, "us"

    #@36b
    const-string v4, "en"

    #@36d
    invoke-direct {v1, v2, v3, v6, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@370
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@373
    .line 428
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@375
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@377
    const/16 v2, 0x138

    #@379
    const-string v3, "us"

    #@37b
    const-string v4, "en"

    #@37d
    invoke-direct {v1, v2, v3, v6, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@380
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@383
    .line 429
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@385
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@387
    const/16 v2, 0x139

    #@389
    const-string v3, "us"

    #@38b
    const-string v4, "en"

    #@38d
    invoke-direct {v1, v2, v3, v6, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@390
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@393
    .line 430
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@395
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@397
    const/16 v2, 0x13a

    #@399
    const-string v3, "us"

    #@39b
    const-string v4, "en"

    #@39d
    invoke-direct {v1, v2, v3, v6, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@3a0
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@3a3
    .line 431
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@3a5
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@3a7
    const/16 v2, 0x13b

    #@3a9
    const-string v3, "us"

    #@3ab
    const-string v4, "en"

    #@3ad
    invoke-direct {v1, v2, v3, v6, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@3b0
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@3b3
    .line 432
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@3b5
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@3b7
    const/16 v2, 0x13c

    #@3b9
    const-string v3, "us"

    #@3bb
    const-string v4, "en"

    #@3bd
    invoke-direct {v1, v2, v3, v6, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@3c0
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@3c3
    .line 433
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@3c5
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@3c7
    const/16 v2, 0x14a

    #@3c9
    const-string v3, "pr"

    #@3cb
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@3ce
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@3d1
    .line 434
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@3d3
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@3d5
    const/16 v2, 0x14c

    #@3d7
    const-string v3, "vi"

    #@3d9
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@3dc
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@3df
    .line 435
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@3e1
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@3e3
    const/16 v2, 0x14e

    #@3e5
    const-string v3, "mx"

    #@3e7
    invoke-direct {v1, v2, v3, v6}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@3ea
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@3ed
    .line 436
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@3ef
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@3f1
    const/16 v2, 0x152

    #@3f3
    const-string v3, "jm"

    #@3f5
    invoke-direct {v1, v2, v3, v6}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@3f8
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@3fb
    .line 437
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@3fd
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@3ff
    const/16 v2, 0x154

    #@401
    const-string v3, "gp"

    #@403
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@406
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@409
    .line 438
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@40b
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@40d
    const/16 v2, 0x156

    #@40f
    const-string v3, "bb"

    #@411
    invoke-direct {v1, v2, v3, v6}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@414
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@417
    .line 439
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@419
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@41b
    const/16 v2, 0x158

    #@41d
    const-string v3, "ag"

    #@41f
    invoke-direct {v1, v2, v3, v6}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@422
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@425
    .line 440
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@427
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@429
    const/16 v2, 0x15a

    #@42b
    const-string v3, "ky"

    #@42d
    invoke-direct {v1, v2, v3, v6}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@430
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@433
    .line 441
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@435
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@437
    const/16 v2, 0x15c

    #@439
    const-string v3, "vg"

    #@43b
    invoke-direct {v1, v2, v3, v6}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@43e
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@441
    .line 442
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@443
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@445
    const/16 v2, 0x15e

    #@447
    const-string v3, "bm"

    #@449
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@44c
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@44f
    .line 443
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@451
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@453
    const/16 v2, 0x160

    #@455
    const-string v3, "gd"

    #@457
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@45a
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@45d
    .line 444
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@45f
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@461
    const/16 v2, 0x162

    #@463
    const-string v3, "ms"

    #@465
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@468
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@46b
    .line 445
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@46d
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@46f
    const/16 v2, 0x164

    #@471
    const-string v3, "kn"

    #@473
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@476
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@479
    .line 446
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@47b
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@47d
    const/16 v2, 0x166

    #@47f
    const-string v3, "lc"

    #@481
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@484
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@487
    .line 447
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@489
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@48b
    const/16 v2, 0x168

    #@48d
    const-string v3, "vc"

    #@48f
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@492
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@495
    .line 448
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@497
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@499
    const/16 v2, 0x16a

    #@49b
    const-string v3, "ai"

    #@49d
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@4a0
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@4a3
    .line 449
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@4a5
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@4a7
    const/16 v2, 0x16b

    #@4a9
    const-string v3, "aw"

    #@4ab
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@4ae
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@4b1
    .line 450
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@4b3
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@4b5
    const/16 v2, 0x16c

    #@4b7
    const-string v3, "bs"

    #@4b9
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@4bc
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@4bf
    .line 451
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@4c1
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@4c3
    const/16 v2, 0x16d

    #@4c5
    const-string v3, "ai"

    #@4c7
    invoke-direct {v1, v2, v3, v6}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@4ca
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@4cd
    .line 452
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@4cf
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@4d1
    const/16 v2, 0x16e

    #@4d3
    const-string v3, "dm"

    #@4d5
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@4d8
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@4db
    .line 453
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@4dd
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@4df
    const/16 v2, 0x170

    #@4e1
    const-string v3, "cu"

    #@4e3
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@4e6
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@4e9
    .line 454
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@4eb
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@4ed
    const/16 v2, 0x172

    #@4ef
    const-string v3, "do"

    #@4f1
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@4f4
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@4f7
    .line 455
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@4f9
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@4fb
    const/16 v2, 0x174

    #@4fd
    const-string v3, "ht"

    #@4ff
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@502
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@505
    .line 456
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@507
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@509
    const/16 v2, 0x176

    #@50b
    const-string v3, "tt"

    #@50d
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@510
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@513
    .line 457
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@515
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@517
    const/16 v2, 0x178

    #@519
    const-string v3, "tc"

    #@51b
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@51e
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@521
    .line 458
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@523
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@525
    const/16 v2, 0x190

    #@527
    const-string v3, "az"

    #@529
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@52c
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@52f
    .line 459
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@531
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@533
    const/16 v2, 0x191

    #@535
    const-string v3, "kz"

    #@537
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@53a
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@53d
    .line 460
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@53f
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@541
    const/16 v2, 0x192

    #@543
    const-string v3, "bt"

    #@545
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@548
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@54b
    .line 461
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@54d
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@54f
    const/16 v2, 0x194

    #@551
    const-string v3, "in"

    #@553
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@556
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@559
    .line 462
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@55b
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@55d
    const/16 v2, 0x195

    #@55f
    const-string v3, "in"

    #@561
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@564
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@567
    .line 463
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@569
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@56b
    const/16 v2, 0x19a

    #@56d
    const-string v3, "pk"

    #@56f
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@572
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@575
    .line 464
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@577
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@579
    const/16 v2, 0x19c

    #@57b
    const-string v3, "af"

    #@57d
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@580
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@583
    .line 465
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@585
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@587
    const/16 v2, 0x19d

    #@589
    const-string v3, "lk"

    #@58b
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@58e
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@591
    .line 466
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@593
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@595
    const/16 v2, 0x19e

    #@597
    const-string v3, "mm"

    #@599
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@59c
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@59f
    .line 467
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@5a1
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@5a3
    const/16 v2, 0x19f

    #@5a5
    const-string v3, "lb"

    #@5a7
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@5aa
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@5ad
    .line 468
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@5af
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@5b1
    const/16 v2, 0x1a0

    #@5b3
    const-string v3, "jo"

    #@5b5
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@5b8
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@5bb
    .line 469
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@5bd
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@5bf
    const/16 v2, 0x1a1

    #@5c1
    const-string v3, "sy"

    #@5c3
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@5c6
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@5c9
    .line 470
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@5cb
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@5cd
    const/16 v2, 0x1a2

    #@5cf
    const-string v3, "iq"

    #@5d1
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@5d4
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@5d7
    .line 471
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@5d9
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@5db
    const/16 v2, 0x1a3

    #@5dd
    const-string v3, "kw"

    #@5df
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@5e2
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@5e5
    .line 472
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@5e7
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@5e9
    const/16 v2, 0x1a4

    #@5eb
    const-string v3, "sa"

    #@5ed
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@5f0
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@5f3
    .line 473
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@5f5
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@5f7
    const/16 v2, 0x1a5

    #@5f9
    const-string v3, "ye"

    #@5fb
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@5fe
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@601
    .line 474
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@603
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@605
    const/16 v2, 0x1a6

    #@607
    const-string v3, "om"

    #@609
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@60c
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@60f
    .line 475
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@611
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@613
    const/16 v2, 0x1a7

    #@615
    const-string v3, "ps"

    #@617
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@61a
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@61d
    .line 476
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@61f
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@621
    const/16 v2, 0x1a8

    #@623
    const-string v3, "ae"

    #@625
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@628
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@62b
    .line 477
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@62d
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@62f
    const/16 v2, 0x1a9

    #@631
    const-string v3, "il"

    #@633
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@636
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@639
    .line 478
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@63b
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@63d
    const/16 v2, 0x1aa

    #@63f
    const-string v3, "bh"

    #@641
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@644
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@647
    .line 479
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@649
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@64b
    const/16 v2, 0x1ab

    #@64d
    const-string v3, "qa"

    #@64f
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@652
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@655
    .line 480
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@657
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@659
    const/16 v2, 0x1ac

    #@65b
    const-string v3, "mn"

    #@65d
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@660
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@663
    .line 481
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@665
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@667
    const/16 v2, 0x1ad

    #@669
    const-string v3, "np"

    #@66b
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@66e
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@671
    .line 482
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@673
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@675
    const/16 v2, 0x1ae

    #@677
    const-string v3, "ae"

    #@679
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@67c
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@67f
    .line 483
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@681
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@683
    const/16 v2, 0x1af

    #@685
    const-string v3, "ae"

    #@687
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@68a
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@68d
    .line 484
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@68f
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@691
    const/16 v2, 0x1b0

    #@693
    const-string v3, "ir"

    #@695
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@698
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@69b
    .line 485
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@69d
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@69f
    const/16 v2, 0x1b2

    #@6a1
    const-string v3, "uz"

    #@6a3
    const-string v4, "ru"

    #@6a5
    invoke-direct {v1, v2, v3, v5, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@6a8
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@6ab
    .line 486
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@6ad
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@6af
    const/16 v2, 0x1b4

    #@6b1
    const-string v3, "tj"

    #@6b3
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@6b6
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@6b9
    .line 487
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@6bb
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@6bd
    const/16 v2, 0x1b5

    #@6bf
    const-string v3, "kg"

    #@6c1
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@6c4
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@6c7
    .line 488
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@6c9
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@6cb
    const/16 v2, 0x1b6

    #@6cd
    const-string v3, "tm"

    #@6cf
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@6d2
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@6d5
    .line 489
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@6d7
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@6d9
    const/16 v2, 0x1b8

    #@6db
    const-string v3, "jp"

    #@6dd
    const-string v4, "ja"

    #@6df
    invoke-direct {v1, v2, v3, v5, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@6e2
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@6e5
    .line 490
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@6e7
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@6e9
    const/16 v2, 0x1b9

    #@6eb
    const-string v3, "jp"

    #@6ed
    const-string v4, "ja"

    #@6ef
    invoke-direct {v1, v2, v3, v5, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@6f2
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@6f5
    .line 491
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@6f7
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@6f9
    const/16 v2, 0x1c2

    #@6fb
    const-string v3, "kr"

    #@6fd
    const-string v4, "ko"

    #@6ff
    invoke-direct {v1, v2, v3, v5, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@702
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@705
    .line 492
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@707
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@709
    const/16 v2, 0x1c4

    #@70b
    const-string v3, "vn"

    #@70d
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@710
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@713
    .line 493
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@715
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@717
    const/16 v2, 0x1c6

    #@719
    const-string v3, "hk"

    #@71b
    const-string v4, "zh"

    #@71d
    invoke-direct {v1, v2, v3, v5, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@720
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@723
    .line 494
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@725
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@727
    const/16 v2, 0x1c7

    #@729
    const-string v3, "mo"

    #@72b
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@72e
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@731
    .line 495
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@733
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@735
    const/16 v2, 0x1c8

    #@737
    const-string v3, "kh"

    #@739
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@73c
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@73f
    .line 496
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@741
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@743
    const/16 v2, 0x1c9

    #@745
    const-string v3, "la"

    #@747
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@74a
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@74d
    .line 497
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@74f
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@751
    const/16 v2, 0x1cc

    #@753
    const-string v3, "cn"

    #@755
    const-string v4, "zh"

    #@757
    invoke-direct {v1, v2, v3, v5, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@75a
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@75d
    .line 498
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@75f
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@761
    const/16 v2, 0x1cd

    #@763
    const-string v3, "cn"

    #@765
    const-string v4, "zh"

    #@767
    invoke-direct {v1, v2, v3, v5, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@76a
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@76d
    .line 499
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@76f
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@771
    const/16 v2, 0x1d2

    #@773
    const-string v3, "tw"

    #@775
    const-string v4, "zh"

    #@777
    invoke-direct {v1, v2, v3, v5, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@77a
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@77d
    .line 500
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@77f
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@781
    const/16 v2, 0x1d3

    #@783
    const-string v3, "kp"

    #@785
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@788
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@78b
    .line 501
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@78d
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@78f
    const/16 v2, 0x1d6

    #@791
    const-string v3, "bd"

    #@793
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@796
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@799
    .line 502
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@79b
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@79d
    const/16 v2, 0x1d8

    #@79f
    const-string v3, "mv"

    #@7a1
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@7a4
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@7a7
    .line 503
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@7a9
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@7ab
    const/16 v2, 0x1f6

    #@7ad
    const-string v3, "my"

    #@7af
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@7b2
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@7b5
    .line 504
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@7b7
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@7b9
    const/16 v2, 0x1f9

    #@7bb
    const-string v3, "au"

    #@7bd
    const-string v4, "en"

    #@7bf
    invoke-direct {v1, v2, v3, v5, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@7c2
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@7c5
    .line 505
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@7c7
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@7c9
    const/16 v2, 0x1fe

    #@7cb
    const-string v3, "id"

    #@7cd
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@7d0
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@7d3
    .line 506
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@7d5
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@7d7
    const/16 v2, 0x202

    #@7d9
    const-string v3, "tl"

    #@7db
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@7de
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@7e1
    .line 507
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@7e3
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@7e5
    const/16 v2, 0x203

    #@7e7
    const-string v3, "ph"

    #@7e9
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@7ec
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@7ef
    .line 508
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@7f1
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@7f3
    const/16 v2, 0x208

    #@7f5
    const-string v3, "th"

    #@7f7
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@7fa
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@7fd
    .line 509
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@7ff
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@801
    const/16 v2, 0x20d

    #@803
    const-string v3, "sg"

    #@805
    const-string v4, "en"

    #@807
    invoke-direct {v1, v2, v3, v5, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@80a
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@80d
    .line 510
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@80f
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@811
    const/16 v2, 0x210

    #@813
    const-string v3, "bn"

    #@815
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@818
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@81b
    .line 511
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@81d
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@81f
    const/16 v2, 0x212

    #@821
    const-string v3, "nz"

    #@823
    const-string v4, "en"

    #@825
    invoke-direct {v1, v2, v3, v5, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@828
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@82b
    .line 512
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@82d
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@82f
    const/16 v2, 0x216

    #@831
    const-string v3, "mp"

    #@833
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@836
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@839
    .line 513
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@83b
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@83d
    const/16 v2, 0x217

    #@83f
    const-string v3, "gu"

    #@841
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@844
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@847
    .line 514
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@849
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@84b
    const/16 v2, 0x218

    #@84d
    const-string v3, "nr"

    #@84f
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@852
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@855
    .line 515
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@857
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@859
    const/16 v2, 0x219

    #@85b
    const-string v3, "pg"

    #@85d
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@860
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@863
    .line 516
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@865
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@867
    const/16 v2, 0x21b

    #@869
    const-string v3, "to"

    #@86b
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@86e
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@871
    .line 517
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@873
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@875
    const/16 v2, 0x21c

    #@877
    const-string v3, "sb"

    #@879
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@87c
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@87f
    .line 518
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@881
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@883
    const/16 v2, 0x21d

    #@885
    const-string v3, "vu"

    #@887
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@88a
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@88d
    .line 519
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@88f
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@891
    const/16 v2, 0x21e

    #@893
    const-string v3, "fj"

    #@895
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@898
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@89b
    .line 520
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@89d
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@89f
    const/16 v2, 0x21f

    #@8a1
    const-string v3, "wf"

    #@8a3
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@8a6
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@8a9
    .line 521
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@8ab
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@8ad
    const/16 v2, 0x220

    #@8af
    const-string v3, "as"

    #@8b1
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@8b4
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@8b7
    .line 522
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@8b9
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@8bb
    const/16 v2, 0x221

    #@8bd
    const-string v3, "ki"

    #@8bf
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@8c2
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@8c5
    .line 523
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@8c7
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@8c9
    const/16 v2, 0x222

    #@8cb
    const-string v3, "nc"

    #@8cd
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@8d0
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@8d3
    .line 524
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@8d5
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@8d7
    const/16 v2, 0x223

    #@8d9
    const-string v3, "pf"

    #@8db
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@8de
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@8e1
    .line 525
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@8e3
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@8e5
    const/16 v2, 0x224

    #@8e7
    const-string v3, "ck"

    #@8e9
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@8ec
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@8ef
    .line 526
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@8f1
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@8f3
    const/16 v2, 0x225

    #@8f5
    const-string v3, "ws"

    #@8f7
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@8fa
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@8fd
    .line 527
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@8ff
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@901
    const/16 v2, 0x226

    #@903
    const-string v3, "fm"

    #@905
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@908
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@90b
    .line 528
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@90d
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@90f
    const/16 v2, 0x227

    #@911
    const-string v3, "mh"

    #@913
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@916
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@919
    .line 529
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@91b
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@91d
    const/16 v2, 0x228

    #@91f
    const-string v3, "pw"

    #@921
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@924
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@927
    .line 530
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@929
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@92b
    const/16 v2, 0x25a

    #@92d
    const-string v3, "eg"

    #@92f
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@932
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@935
    .line 531
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@937
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@939
    const/16 v2, 0x25b

    #@93b
    const-string v3, "dz"

    #@93d
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@940
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@943
    .line 532
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@945
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@947
    const/16 v2, 0x25c

    #@949
    const-string v3, "ma"

    #@94b
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@94e
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@951
    .line 533
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@953
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@955
    const/16 v2, 0x25d

    #@957
    const-string v3, "tn"

    #@959
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@95c
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@95f
    .line 534
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@961
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@963
    const/16 v2, 0x25e

    #@965
    const-string v3, "ly"

    #@967
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@96a
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@96d
    .line 535
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@96f
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@971
    const/16 v2, 0x25f

    #@973
    const-string v3, "gm"

    #@975
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@978
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@97b
    .line 536
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@97d
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@97f
    const/16 v2, 0x260

    #@981
    const-string v3, "sn"

    #@983
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@986
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@989
    .line 537
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@98b
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@98d
    const/16 v2, 0x261

    #@98f
    const-string v3, "mr"

    #@991
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@994
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@997
    .line 538
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@999
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@99b
    const/16 v2, 0x262

    #@99d
    const-string v3, "ml"

    #@99f
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@9a2
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@9a5
    .line 539
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@9a7
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@9a9
    const/16 v2, 0x263

    #@9ab
    const-string v3, "gn"

    #@9ad
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@9b0
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@9b3
    .line 540
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@9b5
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@9b7
    const/16 v2, 0x264

    #@9b9
    const-string v3, "ci"

    #@9bb
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@9be
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@9c1
    .line 541
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@9c3
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@9c5
    const/16 v2, 0x265

    #@9c7
    const-string v3, "bf"

    #@9c9
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@9cc
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@9cf
    .line 542
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@9d1
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@9d3
    const/16 v2, 0x266

    #@9d5
    const-string v3, "ne"

    #@9d7
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@9da
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@9dd
    .line 543
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@9df
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@9e1
    const/16 v2, 0x267

    #@9e3
    const-string v3, "tg"

    #@9e5
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@9e8
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@9eb
    .line 544
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@9ed
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@9ef
    const/16 v2, 0x268

    #@9f1
    const-string v3, "bj"

    #@9f3
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@9f6
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@9f9
    .line 545
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@9fb
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@9fd
    const/16 v2, 0x269

    #@9ff
    const-string v3, "mu"

    #@a01
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@a04
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@a07
    .line 546
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@a09
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@a0b
    const/16 v2, 0x26a

    #@a0d
    const-string v3, "lr"

    #@a0f
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@a12
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@a15
    .line 547
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@a17
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@a19
    const/16 v2, 0x26b

    #@a1b
    const-string v3, "sl"

    #@a1d
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@a20
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@a23
    .line 548
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@a25
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@a27
    const/16 v2, 0x26c

    #@a29
    const-string v3, "gh"

    #@a2b
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@a2e
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@a31
    .line 549
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@a33
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@a35
    const/16 v2, 0x26d

    #@a37
    const-string v3, "ng"

    #@a39
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@a3c
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@a3f
    .line 550
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@a41
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@a43
    const/16 v2, 0x26e

    #@a45
    const-string v3, "td"

    #@a47
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@a4a
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@a4d
    .line 551
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@a4f
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@a51
    const/16 v2, 0x26f

    #@a53
    const-string v3, "cf"

    #@a55
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@a58
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@a5b
    .line 552
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@a5d
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@a5f
    const/16 v2, 0x270

    #@a61
    const-string v3, "cm"

    #@a63
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@a66
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@a69
    .line 553
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@a6b
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@a6d
    const/16 v2, 0x271

    #@a6f
    const-string v3, "cv"

    #@a71
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@a74
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@a77
    .line 554
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@a79
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@a7b
    const/16 v2, 0x272

    #@a7d
    const-string v3, "st"

    #@a7f
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@a82
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@a85
    .line 555
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@a87
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@a89
    const/16 v2, 0x273

    #@a8b
    const-string v3, "gq"

    #@a8d
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@a90
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@a93
    .line 556
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@a95
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@a97
    const/16 v2, 0x274

    #@a99
    const-string v3, "ga"

    #@a9b
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@a9e
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@aa1
    .line 557
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@aa3
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@aa5
    const/16 v2, 0x275

    #@aa7
    const-string v3, "cg"

    #@aa9
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@aac
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@aaf
    .line 558
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@ab1
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@ab3
    const/16 v2, 0x276

    #@ab5
    const-string v3, "cg"

    #@ab7
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@aba
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@abd
    .line 559
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@abf
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@ac1
    const/16 v2, 0x277

    #@ac3
    const-string v3, "ao"

    #@ac5
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@ac8
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@acb
    .line 560
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@acd
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@acf
    const/16 v2, 0x278

    #@ad1
    const-string v3, "gw"

    #@ad3
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@ad6
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@ad9
    .line 561
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@adb
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@add
    const/16 v2, 0x279

    #@adf
    const-string v3, "sc"

    #@ae1
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@ae4
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@ae7
    .line 562
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@ae9
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@aeb
    const/16 v2, 0x27a

    #@aed
    const-string v3, "sd"

    #@aef
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@af2
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@af5
    .line 563
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@af7
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@af9
    const/16 v2, 0x27b

    #@afb
    const-string v3, "rw"

    #@afd
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@b00
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@b03
    .line 564
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@b05
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@b07
    const/16 v2, 0x27c

    #@b09
    const-string v3, "et"

    #@b0b
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@b0e
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@b11
    .line 565
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@b13
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@b15
    const/16 v2, 0x27d

    #@b17
    const-string v3, "so"

    #@b19
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@b1c
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@b1f
    .line 566
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@b21
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@b23
    const/16 v2, 0x27e

    #@b25
    const-string v3, "dj"

    #@b27
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@b2a
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@b2d
    .line 567
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@b2f
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@b31
    const/16 v2, 0x27f

    #@b33
    const-string v3, "ke"

    #@b35
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@b38
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@b3b
    .line 568
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@b3d
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@b3f
    const/16 v2, 0x280

    #@b41
    const-string v3, "tz"

    #@b43
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@b46
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@b49
    .line 569
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@b4b
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@b4d
    const/16 v2, 0x281

    #@b4f
    const-string v3, "ug"

    #@b51
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@b54
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@b57
    .line 570
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@b59
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@b5b
    const/16 v2, 0x282

    #@b5d
    const-string v3, "bi"

    #@b5f
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@b62
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@b65
    .line 571
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@b67
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@b69
    const/16 v2, 0x283

    #@b6b
    const-string v3, "mz"

    #@b6d
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@b70
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@b73
    .line 572
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@b75
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@b77
    const/16 v2, 0x285

    #@b79
    const-string v3, "zm"

    #@b7b
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@b7e
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@b81
    .line 573
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@b83
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@b85
    const/16 v2, 0x286

    #@b87
    const-string v3, "mg"

    #@b89
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@b8c
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@b8f
    .line 574
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@b91
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@b93
    const/16 v2, 0x287

    #@b95
    const-string v3, "re"

    #@b97
    const-string v4, "fr"

    #@b99
    invoke-direct {v1, v2, v3, v5, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@b9c
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@b9f
    .line 575
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@ba1
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@ba3
    const/16 v2, 0x288

    #@ba5
    const-string v3, "zw"

    #@ba7
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@baa
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@bad
    .line 576
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@baf
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@bb1
    const/16 v2, 0x289

    #@bb3
    const-string v3, "na"

    #@bb5
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@bb8
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@bbb
    .line 577
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@bbd
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@bbf
    const/16 v2, 0x28a

    #@bc1
    const-string v3, "mw"

    #@bc3
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@bc6
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@bc9
    .line 578
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@bcb
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@bcd
    const/16 v2, 0x28b

    #@bcf
    const-string v3, "ls"

    #@bd1
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@bd4
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@bd7
    .line 579
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@bd9
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@bdb
    const/16 v2, 0x28c

    #@bdd
    const-string v3, "bw"

    #@bdf
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@be2
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@be5
    .line 580
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@be7
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@be9
    const/16 v2, 0x28d

    #@beb
    const-string v3, "sz"

    #@bed
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@bf0
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@bf3
    .line 581
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@bf5
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@bf7
    const/16 v2, 0x28e

    #@bf9
    const-string v3, "km"

    #@bfb
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@bfe
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@c01
    .line 582
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@c03
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@c05
    const/16 v2, 0x28f

    #@c07
    const-string v3, "za"

    #@c09
    const-string v4, "en"

    #@c0b
    invoke-direct {v1, v2, v3, v5, v4}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    #@c0e
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@c11
    .line 583
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@c13
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@c15
    const/16 v2, 0x291

    #@c17
    const-string v3, "er"

    #@c19
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@c1c
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@c1f
    .line 584
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@c21
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@c23
    const/16 v2, 0x2be

    #@c25
    const-string v3, "bz"

    #@c27
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@c2a
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@c2d
    .line 585
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@c2f
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@c31
    const/16 v2, 0x2c0

    #@c33
    const-string v3, "gt"

    #@c35
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@c38
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@c3b
    .line 586
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@c3d
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@c3f
    const/16 v2, 0x2c2

    #@c41
    const-string v3, "sv"

    #@c43
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@c46
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@c49
    .line 587
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@c4b
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@c4d
    const/16 v2, 0x2c4

    #@c4f
    const-string v3, "hn"

    #@c51
    invoke-direct {v1, v2, v3, v6}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@c54
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@c57
    .line 588
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@c59
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@c5b
    const/16 v2, 0x2c6

    #@c5d
    const-string v3, "ni"

    #@c5f
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@c62
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@c65
    .line 589
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@c67
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@c69
    const/16 v2, 0x2c8

    #@c6b
    const-string v3, "cr"

    #@c6d
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@c70
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@c73
    .line 590
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@c75
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@c77
    const/16 v2, 0x2ca

    #@c79
    const-string v3, "pa"

    #@c7b
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@c7e
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@c81
    .line 591
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@c83
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@c85
    const/16 v2, 0x2cc

    #@c87
    const-string v3, "pe"

    #@c89
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@c8c
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@c8f
    .line 592
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@c91
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@c93
    const/16 v2, 0x2d2

    #@c95
    const-string v3, "ar"

    #@c97
    invoke-direct {v1, v2, v3, v6}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@c9a
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@c9d
    .line 593
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@c9f
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@ca1
    const/16 v2, 0x2d4

    #@ca3
    const-string v3, "br"

    #@ca5
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@ca8
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@cab
    .line 594
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@cad
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@caf
    const/16 v2, 0x2da

    #@cb1
    const-string v3, "cl"

    #@cb3
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@cb6
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@cb9
    .line 595
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@cbb
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@cbd
    const/16 v2, 0x2dc

    #@cbf
    const-string v3, "co"

    #@cc1
    invoke-direct {v1, v2, v3, v6}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@cc4
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@cc7
    .line 596
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@cc9
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@ccb
    const/16 v2, 0x2de

    #@ccd
    const-string v3, "ve"

    #@ccf
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@cd2
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@cd5
    .line 597
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@cd7
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@cd9
    const/16 v2, 0x2e0

    #@cdb
    const-string v3, "bo"

    #@cdd
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@ce0
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@ce3
    .line 598
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@ce5
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@ce7
    const/16 v2, 0x2e2

    #@ce9
    const-string v3, "gy"

    #@ceb
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@cee
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@cf1
    .line 599
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@cf3
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@cf5
    const/16 v2, 0x2e4

    #@cf7
    const-string v3, "ec"

    #@cf9
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@cfc
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@cff
    .line 600
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@d01
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@d03
    const/16 v2, 0x2e6

    #@d05
    const-string v3, "gf"

    #@d07
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@d0a
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@d0d
    .line 601
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@d0f
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@d11
    const/16 v2, 0x2e8

    #@d13
    const-string v3, "py"

    #@d15
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@d18
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@d1b
    .line 602
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@d1d
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@d1f
    const/16 v2, 0x2ea

    #@d21
    const-string v3, "sr"

    #@d23
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@d26
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@d29
    .line 603
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@d2b
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@d2d
    const/16 v2, 0x2ec

    #@d2f
    const-string v3, "uy"

    #@d31
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@d34
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@d37
    .line 604
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@d39
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@d3b
    const/16 v2, 0x2ee

    #@d3d
    const-string v3, "fk"

    #@d3f
    invoke-direct {v1, v2, v3, v5}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@d42
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@d45
    .line 607
    sget-object v0, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@d47
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    #@d4a
    .line 608
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 47
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 53
    return-void
.end method

.method public static countryCodeForMcc(I)Ljava/lang/String;
    .registers 3
    .parameter "mcc"

    #@0
    .prologue
    .line 130
    invoke-static {p0}, Lcom/android/internal/telephony/MccTable;->entryForMcc(I)Lcom/android/internal/telephony/MccTable$MccEntry;

    #@3
    move-result-object v0

    #@4
    .line 132
    .local v0, entry:Lcom/android/internal/telephony/MccTable$MccEntry;
    if-nez v0, :cond_9

    #@6
    .line 133
    const-string v1, ""

    #@8
    .line 135
    :goto_8
    return-object v1

    #@9
    :cond_9
    iget-object v1, v0, Lcom/android/internal/telephony/MccTable$MccEntry;->iso:Ljava/lang/String;

    #@b
    goto :goto_8
.end method

.method public static defaultLanguageForMcc(I)Ljava/lang/String;
    .registers 3
    .parameter "mcc"

    #@0
    .prologue
    .line 147
    invoke-static {p0}, Lcom/android/internal/telephony/MccTable;->entryForMcc(I)Lcom/android/internal/telephony/MccTable$MccEntry;

    #@3
    move-result-object v0

    #@4
    .line 149
    .local v0, entry:Lcom/android/internal/telephony/MccTable$MccEntry;
    if-nez v0, :cond_8

    #@6
    .line 150
    const/4 v1, 0x0

    #@7
    .line 152
    :goto_7
    return-object v1

    #@8
    :cond_8
    iget-object v1, v0, Lcom/android/internal/telephony/MccTable$MccEntry;->language:Ljava/lang/String;

    #@a
    goto :goto_7
.end method

.method public static defaultTimeZoneForMcc(I)Ljava/lang/String;
    .registers 7
    .parameter "mcc"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 104
    invoke-static {p0}, Lcom/android/internal/telephony/MccTable;->entryForMcc(I)Lcom/android/internal/telephony/MccTable$MccEntry;

    #@4
    move-result-object v0

    #@5
    .line 105
    .local v0, entry:Lcom/android/internal/telephony/MccTable$MccEntry;
    if-eqz v0, :cond_b

    #@7
    iget-object v4, v0, Lcom/android/internal/telephony/MccTable$MccEntry;->iso:Ljava/lang/String;

    #@9
    if-nez v4, :cond_c

    #@b
    .line 116
    :cond_b
    :goto_b
    return-object v3

    #@c
    .line 109
    :cond_c
    iget-object v4, v0, Lcom/android/internal/telephony/MccTable$MccEntry;->language:Ljava/lang/String;

    #@e
    if-nez v4, :cond_22

    #@10
    .line 110
    new-instance v1, Ljava/util/Locale;

    #@12
    iget-object v4, v0, Lcom/android/internal/telephony/MccTable$MccEntry;->iso:Ljava/lang/String;

    #@14
    invoke-direct {v1, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    #@17
    .line 114
    .local v1, locale:Ljava/util/Locale;
    :goto_17
    invoke-static {v1}, Llibcore/icu/TimeZones;->forLocale(Ljava/util/Locale;)[Ljava/lang/String;

    #@1a
    move-result-object v2

    #@1b
    .line 115
    .local v2, tz:[Ljava/lang/String;
    array-length v4, v2

    #@1c
    if-eqz v4, :cond_b

    #@1e
    .line 116
    const/4 v3, 0x0

    #@1f
    aget-object v3, v2, v3

    #@21
    goto :goto_b

    #@22
    .line 112
    .end local v1           #locale:Ljava/util/Locale;
    .end local v2           #tz:[Ljava/lang/String;
    :cond_22
    new-instance v1, Ljava/util/Locale;

    #@24
    iget-object v4, v0, Lcom/android/internal/telephony/MccTable$MccEntry;->language:Ljava/lang/String;

    #@26
    iget-object v5, v0, Lcom/android/internal/telephony/MccTable$MccEntry;->iso:Ljava/lang/String;

    #@28
    invoke-direct {v1, v4, v5}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@2b
    .restart local v1       #locale:Ljava/util/Locale;
    goto :goto_17
.end method

.method private static entryForMcc(I)Lcom/android/internal/telephony/MccTable$MccEntry;
    .registers 5
    .parameter "mcc"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 85
    new-instance v1, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@3
    const/4 v3, 0x0

    #@4
    invoke-direct {v1, p0, v2, v3}, Lcom/android/internal/telephony/MccTable$MccEntry;-><init>(ILjava/lang/String;I)V

    #@7
    .line 87
    .local v1, m:Lcom/android/internal/telephony/MccTable$MccEntry;
    sget-object v3, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@9
    invoke-static {v3, v1}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;)I

    #@c
    move-result v0

    #@d
    .line 89
    .local v0, index:I
    if-gez v0, :cond_10

    #@f
    .line 92
    :goto_f
    return-object v2

    #@10
    :cond_10
    sget-object v2, Lcom/android/internal/telephony/MccTable;->table:Ljava/util/ArrayList;

    #@12
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@15
    move-result-object v2

    #@16
    check-cast v2, Lcom/android/internal/telephony/MccTable$MccEntry;

    #@18
    goto :goto_f
.end method

.method private static setLocaleFromMccIfNeeded(Landroid/content/Context;I)V
    .registers 7
    .parameter "context"
    .parameter "mcc"

    #@0
    .prologue
    .line 304
    invoke-static {}, Landroid/telephony/TelephonyManager;->getLteOnCdmaModeStatic()I

    #@3
    move-result v2

    #@4
    const/4 v3, 0x1

    #@5
    if-ne v2, v3, :cond_8

    #@7
    .line 319
    :cond_7
    :goto_7
    return-void

    #@8
    .line 310
    :cond_8
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getCountry()Ljava/lang/String;

    #@b
    move-result-object v2

    #@c
    const-string v3, "KR"

    #@e
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11
    move-result v2

    #@12
    if-nez v2, :cond_7

    #@14
    .line 314
    invoke-static {p1}, Lcom/android/internal/telephony/MccTable;->defaultLanguageForMcc(I)Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    .line 315
    .local v1, language:Ljava/lang/String;
    invoke-static {p1}, Lcom/android/internal/telephony/MccTable;->countryCodeForMcc(I)Ljava/lang/String;

    #@1b
    move-result-object v0

    #@1c
    .line 317
    .local v0, country:Ljava/lang/String;
    const-string v2, "MccTable"

    #@1e
    new-instance v3, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v4, "locale set to "

    #@25
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v3

    #@29
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v3

    #@2d
    const-string v4, "_"

    #@2f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v3

    #@33
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v3

    #@37
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v3

    #@3b
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    .line 318
    invoke-static {p0, v1, v0}, Lcom/android/internal/telephony/MccTable;->setSystemLocale(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    #@41
    goto :goto_7
.end method

.method public static setSystemLocale(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .registers 15
    .parameter "context"
    .parameter "language"
    .parameter "country"

    #@0
    .prologue
    const/4 v11, 0x5

    #@1
    .line 235
    const-string v8, "persist.sys.language"

    #@3
    invoke-static {v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v6

    #@7
    .line 236
    .local v6, l:Ljava/lang/String;
    const-string v8, "persist.sys.country"

    #@9
    invoke-static {v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@c
    move-result-object v3

    #@d
    .line 238
    .local v3, c:Ljava/lang/String;
    if-nez p1, :cond_10

    #@f
    .line 277
    :cond_f
    :goto_f
    return-void

    #@10
    .line 241
    :cond_10
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@13
    move-result-object p1

    #@14
    .line 242
    if-nez p2, :cond_18

    #@16
    .line 243
    const-string p2, ""

    #@18
    .line 245
    :cond_18
    invoke-virtual {p2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    #@1b
    move-result-object p2

    #@1c
    .line 247
    if-eqz v6, :cond_24

    #@1e
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    #@21
    move-result v8

    #@22
    if-nez v8, :cond_f

    #@24
    :cond_24
    if-eqz v3, :cond_2c

    #@26
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@29
    move-result v8

    #@2a
    if-nez v8, :cond_f

    #@2c
    .line 250
    :cond_2c
    :try_start_2c
    invoke-virtual {p0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    #@2f
    move-result-object v8

    #@30
    invoke-virtual {v8}, Landroid/content/res/AssetManager;->getLocales()[Ljava/lang/String;

    #@33
    move-result-object v7

    #@34
    .line 251
    .local v7, locales:[Ljava/lang/String;
    array-length v0, v7

    #@35
    .line 252
    .local v0, N:I
    const/4 v2, 0x0

    #@36
    .line 253
    .local v2, bestMatch:Ljava/lang/String;
    const/4 v5, 0x0

    #@37
    .local v5, i:I
    :goto_37
    if-ge v5, v0, :cond_63

    #@39
    .line 255
    aget-object v8, v7, v5

    #@3b
    if-eqz v8, :cond_8d

    #@3d
    aget-object v8, v7, v5

    #@3f
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    #@42
    move-result v8

    #@43
    if-lt v8, v11, :cond_8d

    #@45
    aget-object v8, v7, v5

    #@47
    const/4 v9, 0x0

    #@48
    const/4 v10, 0x2

    #@49
    invoke-virtual {v8, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@4c
    move-result-object v8

    #@4d
    invoke-virtual {v8, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@50
    move-result v8

    #@51
    if-eqz v8, :cond_8d

    #@53
    .line 257
    aget-object v8, v7, v5

    #@55
    const/4 v9, 0x3

    #@56
    const/4 v10, 0x5

    #@57
    invoke-virtual {v8, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@5a
    move-result-object v8

    #@5b
    invoke-virtual {v8, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5e
    move-result v8

    #@5f
    if-eqz v8, :cond_89

    #@61
    .line 258
    aget-object v2, v7, v5

    #@63
    .line 265
    :cond_63
    if-eqz v2, :cond_f

    #@65
    .line 266
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@68
    move-result-object v1

    #@69
    .line 267
    .local v1, am:Landroid/app/IActivityManager;
    invoke-interface {v1}, Landroid/app/IActivityManager;->getConfiguration()Landroid/content/res/Configuration;

    #@6c
    move-result-object v4

    #@6d
    .line 268
    .local v4, config:Landroid/content/res/Configuration;
    new-instance v8, Ljava/util/Locale;

    #@6f
    const/4 v9, 0x0

    #@70
    const/4 v10, 0x2

    #@71
    invoke-virtual {v2, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@74
    move-result-object v9

    #@75
    const/4 v10, 0x3

    #@76
    const/4 v11, 0x5

    #@77
    invoke-virtual {v2, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@7a
    move-result-object v10

    #@7b
    invoke-direct {v8, v9, v10}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@7e
    iput-object v8, v4, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@80
    .line 270
    const/4 v8, 0x1

    #@81
    iput-boolean v8, v4, Landroid/content/res/Configuration;->userSetLocale:Z

    #@83
    .line 271
    invoke-interface {v1, v4}, Landroid/app/IActivityManager;->updateConfiguration(Landroid/content/res/Configuration;)V

    #@86
    goto :goto_f

    #@87
    .line 273
    .end local v0           #N:I
    .end local v1           #am:Landroid/app/IActivityManager;
    .end local v2           #bestMatch:Ljava/lang/String;
    .end local v4           #config:Landroid/content/res/Configuration;
    .end local v5           #i:I
    .end local v7           #locales:[Ljava/lang/String;
    :catch_87
    move-exception v8

    #@88
    goto :goto_f

    #@89
    .line 260
    .restart local v0       #N:I
    .restart local v2       #bestMatch:Ljava/lang/String;
    .restart local v5       #i:I
    .restart local v7       #locales:[Ljava/lang/String;
    :cond_89
    if-nez v2, :cond_8d

    #@8b
    .line 261
    aget-object v2, v7, v5
    :try_end_8d
    .catch Ljava/lang/Exception; {:try_start_2c .. :try_end_8d} :catch_87

    #@8d
    .line 253
    :cond_8d
    add-int/lit8 v5, v5, 0x1

    #@8f
    goto :goto_37
.end method

.method private static setTimezoneFromMccIfNeeded(Landroid/content/Context;I)V
    .registers 8
    .parameter "context"
    .parameter "mcc"

    #@0
    .prologue
    .line 285
    const-string v3, "persist.sys.timezone"

    #@2
    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    .line 286
    .local v1, timezone:Ljava/lang/String;
    if-eqz v1, :cond_e

    #@8
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@b
    move-result v3

    #@c
    if-nez v3, :cond_3d

    #@e
    .line 287
    :cond_e
    invoke-static {p1}, Lcom/android/internal/telephony/MccTable;->defaultTimeZoneForMcc(I)Ljava/lang/String;

    #@11
    move-result-object v2

    #@12
    .line 288
    .local v2, zoneId:Ljava/lang/String;
    if-eqz v2, :cond_3d

    #@14
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@17
    move-result v3

    #@18
    if-lez v3, :cond_3d

    #@1a
    .line 290
    const-string v3, "alarm"

    #@1c
    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@1f
    move-result-object v0

    #@20
    check-cast v0, Landroid/app/AlarmManager;

    #@22
    .line 292
    .local v0, alarm:Landroid/app/AlarmManager;
    invoke-virtual {v0, v2}, Landroid/app/AlarmManager;->setTimeZone(Ljava/lang/String;)V

    #@25
    .line 293
    const-string v3, "MccTable"

    #@27
    new-instance v4, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const-string v5, "timezone set to "

    #@2e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v4

    #@32
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v4

    #@36
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v4

    #@3a
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3d
    .line 296
    .end local v0           #alarm:Landroid/app/AlarmManager;
    .end local v2           #zoneId:Ljava/lang/String;
    :cond_3d
    return-void
.end method

.method private static setWifiCountryCodeFromMcc(Landroid/content/Context;I)V
    .registers 8
    .parameter "context"
    .parameter "mcc"

    #@0
    .prologue
    .line 328
    invoke-static {p1}, Lcom/android/internal/telephony/MccTable;->countryCodeForMcc(I)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 329
    .local v0, country:Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    #@7
    move-result v3

    #@8
    if-nez v3, :cond_3a

    #@a
    .line 332
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@d
    move-result-object v3

    #@e
    const-string v4, "wifi_country_code"

    #@10
    invoke-static {v3, v4}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    .line 335
    .local v1, strCurrentDBCountryCode:Ljava/lang/String;
    if-eqz v1, :cond_3b

    #@16
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    #@19
    move-result v3

    #@1a
    if-nez v3, :cond_3b

    #@1c
    .line 336
    invoke-virtual {v1, v0}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    #@1f
    move-result v3

    #@20
    if-nez v3, :cond_3b

    #@22
    .line 337
    const-string v3, "MccTable"

    #@24
    new-instance v4, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v5, "Skip - WIFI_COUNTRY_CODE set to "

    #@2b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v4

    #@2f
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v4

    #@33
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v4

    #@37
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 348
    .end local v1           #strCurrentDBCountryCode:Ljava/lang/String;
    :cond_3a
    :goto_3a
    return-void

    #@3b
    .line 343
    .restart local v1       #strCurrentDBCountryCode:Ljava/lang/String;
    :cond_3b
    const-string v3, "MccTable"

    #@3d
    new-instance v4, Ljava/lang/StringBuilder;

    #@3f
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@42
    const-string v5, "WIFI_COUNTRY_CODE set to "

    #@44
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v4

    #@48
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v4

    #@4c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4f
    move-result-object v4

    #@50
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@53
    .line 344
    const-string v3, "wifi"

    #@55
    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@58
    move-result-object v2

    #@59
    check-cast v2, Landroid/net/wifi/WifiManager;

    #@5b
    .line 346
    .local v2, wM:Landroid/net/wifi/WifiManager;
    const/4 v3, 0x1

    #@5c
    invoke-virtual {v2, v0, v3}, Landroid/net/wifi/WifiManager;->setCountryCode(Ljava/lang/String;Z)V

    #@5f
    goto :goto_3a
.end method

.method public static smallestDigitsMccForMnc(I)I
    .registers 3
    .parameter "mcc"

    #@0
    .prologue
    .line 166
    invoke-static {p0}, Lcom/android/internal/telephony/MccTable;->entryForMcc(I)Lcom/android/internal/telephony/MccTable$MccEntry;

    #@3
    move-result-object v0

    #@4
    .line 168
    .local v0, entry:Lcom/android/internal/telephony/MccTable$MccEntry;
    if-nez v0, :cond_8

    #@6
    .line 169
    const/4 v1, 0x2

    #@7
    .line 171
    :goto_7
    return v1

    #@8
    :cond_8
    iget v1, v0, Lcom/android/internal/telephony/MccTable$MccEntry;->smallestDigitsMnc:I

    #@a
    goto :goto_7
.end method

.method public static updateMccMncConfiguration(Landroid/content/Context;Ljava/lang/String;)V
    .registers 9
    .parameter "context"
    .parameter "mccmnc"

    #@0
    .prologue
    .line 182
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@3
    move-result v4

    #@4
    if-nez v4, :cond_7c

    #@6
    .line 186
    const/4 v4, 0x0

    #@7
    const/4 v5, 0x3

    #@8
    :try_start_8
    invoke-virtual {p1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@b
    move-result-object v4

    #@c
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@f
    move-result v2

    #@10
    .line 187
    .local v2, mcc:I
    const/4 v4, 0x3

    #@11
    invoke-virtual {p1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@14
    move-result-object v4

    #@15
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_18
    .catch Ljava/lang/NumberFormatException; {:try_start_8 .. :try_end_18} :catch_7d

    #@18
    move-result v3

    #@19
    .line 193
    .local v3, mnc:I
    const-string v4, "MccTable"

    #@1b
    new-instance v5, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v6, "updateMccMncConfiguration: mcc="

    #@22
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v5

    #@26
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@29
    move-result-object v5

    #@2a
    const-string v6, ", mnc="

    #@2c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v5

    #@30
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@33
    move-result-object v5

    #@34
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v5

    #@38
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 195
    if-eqz v2, :cond_46

    #@3d
    .line 196
    invoke-static {p0, v2}, Lcom/android/internal/telephony/MccTable;->setTimezoneFromMccIfNeeded(Landroid/content/Context;I)V

    #@40
    .line 197
    invoke-static {p0, v2}, Lcom/android/internal/telephony/MccTable;->setLocaleFromMccIfNeeded(Landroid/content/Context;I)V

    #@43
    .line 198
    invoke-static {p0, v2}, Lcom/android/internal/telephony/MccTable;->setWifiCountryCodeFromMcc(Landroid/content/Context;I)V

    #@46
    .line 201
    :cond_46
    :try_start_46
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@49
    move-result-object v4

    #@4a
    invoke-interface {v4}, Landroid/app/IActivityManager;->getConfiguration()Landroid/content/res/Configuration;

    #@4d
    move-result-object v0

    #@4e
    .line 204
    .local v0, config:Landroid/content/res/Configuration;
    const/4 v4, 0x0

    #@4f
    const-string v5, "support_assisted_dialing"

    #@51
    invoke-static {v4, v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@54
    move-result v4

    #@55
    if-nez v4, :cond_60

    #@57
    const/4 v4, 0x0

    #@58
    const-string v5, "support_smart_dialing"

    #@5a
    invoke-static {v4, v5}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@5d
    move-result v4

    #@5e
    if-eqz v4, :cond_6d

    #@60
    .line 208
    :cond_60
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@63
    move-result-object v4

    #@64
    const-string v5, "assist_dial_ota_mcc"

    #@66
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@69
    move-result-object v6

    #@6a
    invoke-static {v4, v5, v6}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    #@6d
    .line 213
    :cond_6d
    if-eqz v2, :cond_71

    #@6f
    .line 214
    iput v2, v0, Landroid/content/res/Configuration;->mcc:I

    #@71
    .line 216
    :cond_71
    if-eqz v3, :cond_75

    #@73
    .line 217
    iput v3, v0, Landroid/content/res/Configuration;->mnc:I

    #@75
    .line 219
    :cond_75
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@78
    move-result-object v4

    #@79
    invoke-interface {v4, v0}, Landroid/app/IActivityManager;->updateConfiguration(Landroid/content/res/Configuration;)V
    :try_end_7c
    .catch Landroid/os/RemoteException; {:try_start_46 .. :try_end_7c} :catch_86

    #@7c
    .line 224
    .end local v0           #config:Landroid/content/res/Configuration;
    .end local v2           #mcc:I
    .end local v3           #mnc:I
    :cond_7c
    :goto_7c
    return-void

    #@7d
    .line 188
    :catch_7d
    move-exception v1

    #@7e
    .line 189
    .local v1, e:Ljava/lang/NumberFormatException;
    const-string v4, "MccTable"

    #@80
    const-string v5, "Error parsing IMSI"

    #@82
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@85
    goto :goto_7c

    #@86
    .line 220
    .end local v1           #e:Ljava/lang/NumberFormatException;
    .restart local v2       #mcc:I
    .restart local v3       #mnc:I
    :catch_86
    move-exception v1

    #@87
    .line 221
    .local v1, e:Landroid/os/RemoteException;
    const-string v4, "MccTable"

    #@89
    const-string v5, "Can\'t update configuration"

    #@8b
    invoke-static {v4, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@8e
    goto :goto_7c
.end method
