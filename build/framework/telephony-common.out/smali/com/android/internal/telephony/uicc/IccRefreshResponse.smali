.class public Lcom/android/internal/telephony/uicc/IccRefreshResponse;
.super Ljava/lang/Object;
.source "IccRefreshResponse.java"


# static fields
.field public static final REFRESH_RESULT_APP_INIT:I = 0x4

.field public static final REFRESH_RESULT_FILE_UPDATE:I = 0x0

.field public static final REFRESH_RESULT_INIT:I = 0x1

.field public static final REFRESH_RESULT_OTA:I = 0x3

.field public static final REFRESH_RESULT_RESET:I = 0x2


# instance fields
.field public aid:Ljava/lang/String;

.field public efId:I

.field public refreshResult:I


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 26
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static refreshResultFromRIL(I)I
    .registers 4
    .parameter "refreshResult"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/ATParseEx;
        }
    .end annotation

    #@0
    .prologue
    .line 52
    packed-switch p0, :pswitch_data_24

    #@3
    .line 61
    :pswitch_3
    new-instance v0, Lcom/android/internal/telephony/ATParseEx;

    #@5
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v2, "Sim Refresh response is Unknown "

    #@c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    invoke-direct {v0, v1}, Lcom/android/internal/telephony/ATParseEx;-><init>(Ljava/lang/String;)V

    #@1b
    throw v0

    #@1c
    .line 54
    :pswitch_1c
    const/4 v0, 0x0

    #@1d
    .line 58
    :goto_1d
    return v0

    #@1e
    .line 55
    :pswitch_1e
    const/4 v0, 0x1

    #@1f
    goto :goto_1d

    #@20
    .line 56
    :pswitch_20
    const/4 v0, 0x2

    #@21
    goto :goto_1d

    #@22
    .line 58
    :pswitch_22
    const/4 v0, 0x4

    #@23
    goto :goto_1d

    #@24
    .line 52
    :pswitch_data_24
    .packed-switch 0x0
        :pswitch_1c
        :pswitch_1e
        :pswitch_20
        :pswitch_3
        :pswitch_22
    .end packed-switch
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 67
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "{"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget v1, p0, Lcom/android/internal/telephony/uicc/IccRefreshResponse;->refreshResult:I

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, ", "

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget-object v1, p0, Lcom/android/internal/telephony/uicc/IccRefreshResponse;->aid:Ljava/lang/String;

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, ", "

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    iget v1, p0, Lcom/android/internal/telephony/uicc/IccRefreshResponse;->efId:I

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    const-string v1, "}"

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v0

    #@33
    return-object v0
.end method
