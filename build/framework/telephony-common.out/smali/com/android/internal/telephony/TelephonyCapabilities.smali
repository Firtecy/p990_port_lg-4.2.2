.class public Lcom/android/internal/telephony/TelephonyCapabilities;
.super Ljava/lang/Object;
.source "TelephonyCapabilities.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "TelephonyCapabilities"


# direct methods
.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 34
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 35
    return-void
.end method

.method public static canDistinguishDialingAndConnected(I)Z
    .registers 2
    .parameter "phoneType"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 213
    if-ne p0, v0, :cond_4

    #@3
    :goto_3
    return v0

    #@4
    :cond_4
    const/4 v0, 0x0

    #@5
    goto :goto_3
.end method

.method public static getDeviceIdLabel(Lcom/android/internal/telephony/Phone;)I
    .registers 4
    .parameter "phone"

    #@0
    .prologue
    .line 114
    invoke-interface {p0}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    #@3
    move-result v0

    #@4
    const/4 v1, 0x1

    #@5
    if-ne v0, v1, :cond_b

    #@7
    .line 115
    const v0, 0x10400a3

    #@a
    .line 121
    :goto_a
    return v0

    #@b
    .line 116
    :cond_b
    invoke-interface {p0}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    #@e
    move-result v0

    #@f
    const/4 v1, 0x2

    #@10
    if-ne v0, v1, :cond_16

    #@12
    .line 117
    const v0, 0x10400a4

    #@15
    goto :goto_a

    #@16
    .line 119
    :cond_16
    const-string v0, "TelephonyCapabilities"

    #@18
    new-instance v1, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v2, "getDeviceIdLabel: no known label for phone "

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    invoke-interface {p0}, Lcom/android/internal/telephony/Phone;->getPhoneName()Ljava/lang/String;

    #@26
    move-result-object v2

    #@27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v1

    #@2b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v1

    #@2f
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 121
    const/4 v0, 0x0

    #@33
    goto :goto_a
.end method

.method public static supportsAdn(I)Z
    .registers 3
    .parameter "phoneType"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 201
    if-eq p0, v0, :cond_6

    #@3
    const/4 v1, 0x2

    #@4
    if-ne p0, v1, :cond_7

    #@6
    :cond_6
    :goto_6
    return v0

    #@7
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method

.method public static supportsAnswerAndHold(Lcom/android/internal/telephony/Phone;)Z
    .registers 4
    .parameter "phone"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 179
    invoke-interface {p0}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    #@4
    move-result v1

    #@5
    if-eq v1, v0, :cond_15

    #@7
    invoke-interface {p0}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    #@a
    move-result v1

    #@b
    const/4 v2, 0x3

    #@c
    if-eq v1, v2, :cond_15

    #@e
    invoke-interface {p0}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    #@11
    move-result v1

    #@12
    const/4 v2, 0x4

    #@13
    if-ne v1, v2, :cond_16

    #@15
    :cond_15
    :goto_15
    return v0

    #@16
    :cond_16
    const/4 v0, 0x0

    #@17
    goto :goto_15
.end method

.method public static supportsCallModify(Lcom/android/internal/telephony/Phone;)Z
    .registers 3
    .parameter "phone"

    #@0
    .prologue
    .line 146
    invoke-interface {p0}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    #@3
    move-result v0

    #@4
    const/4 v1, 0x4

    #@5
    if-ne v0, v1, :cond_9

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public static supportsConferenceCallManagement(Lcom/android/internal/telephony/Phone;)Z
    .registers 4
    .parameter "phone"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 136
    invoke-interface {p0}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    #@4
    move-result v1

    #@5
    if-eq v1, v0, :cond_15

    #@7
    invoke-interface {p0}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    #@a
    move-result v1

    #@b
    const/4 v2, 0x3

    #@c
    if-eq v1, v2, :cond_15

    #@e
    invoke-interface {p0}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    #@11
    move-result v1

    #@12
    const/4 v2, 0x4

    #@13
    if-ne v1, v2, :cond_16

    #@15
    :cond_15
    :goto_15
    return v0

    #@16
    :cond_16
    const/4 v0, 0x0

    #@17
    goto :goto_15
.end method

.method public static supportsEcm(Lcom/android/internal/telephony/Phone;)Z
    .registers 6
    .parameter "phone"

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    const/4 v0, 0x0

    #@2
    const/4 v1, 0x1

    #@3
    .line 53
    invoke-interface {p0}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@6
    move-result-object v2

    #@7
    const-string v3, "support_emergency_callback_mode_for_gsm"

    #@9
    invoke-static {v2, v3}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@c
    move-result v2

    #@d
    if-eqz v2, :cond_1d

    #@f
    .line 54
    invoke-interface {p0}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    #@12
    move-result v2

    #@13
    if-eq v2, v1, :cond_1b

    #@15
    invoke-interface {p0}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    #@18
    move-result v2

    #@19
    if-ne v2, v4, :cond_1c

    #@1b
    :cond_1b
    move v0, v1

    #@1c
    .line 59
    :cond_1c
    :goto_1c
    return v0

    #@1d
    :cond_1d
    invoke-interface {p0}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    #@20
    move-result v2

    #@21
    if-ne v2, v4, :cond_25

    #@23
    :goto_23
    move v0, v1

    #@24
    goto :goto_1c

    #@25
    :cond_25
    move v1, v0

    #@26
    goto :goto_23
.end method

.method public static supportsHoldAndUnhold(Lcom/android/internal/telephony/Phone;)Z
    .registers 4
    .parameter "phone"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 159
    invoke-interface {p0}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    #@4
    move-result v1

    #@5
    if-eq v1, v0, :cond_15

    #@7
    invoke-interface {p0}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    #@a
    move-result v1

    #@b
    const/4 v2, 0x3

    #@c
    if-eq v1, v2, :cond_15

    #@e
    invoke-interface {p0}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    #@11
    move-result v1

    #@12
    const/4 v2, 0x4

    #@13
    if-ne v1, v2, :cond_16

    #@15
    :cond_15
    :goto_15
    return v0

    #@16
    :cond_16
    const/4 v0, 0x0

    #@17
    goto :goto_15
.end method

.method public static supportsNetworkSelection(Lcom/android/internal/telephony/Phone;)Z
    .registers 3
    .parameter "phone"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 102
    invoke-interface {p0}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    #@4
    move-result v1

    #@5
    if-ne v1, v0, :cond_8

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public static supportsOtasp(Lcom/android/internal/telephony/Phone;)Z
    .registers 3
    .parameter "phone"

    #@0
    .prologue
    .line 80
    invoke-interface {p0}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    #@3
    move-result v0

    #@4
    const/4 v1, 0x2

    #@5
    if-ne v0, v1, :cond_9

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public static supportsVoiceMessageCount(Lcom/android/internal/telephony/Phone;)Z
    .registers 3
    .parameter "phone"

    #@0
    .prologue
    .line 90
    invoke-interface {p0}, Lcom/android/internal/telephony/Phone;->getVoiceMessageCount()I

    #@3
    move-result v0

    #@4
    const/4 v1, -0x1

    #@5
    if-eq v0, v1, :cond_9

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method
