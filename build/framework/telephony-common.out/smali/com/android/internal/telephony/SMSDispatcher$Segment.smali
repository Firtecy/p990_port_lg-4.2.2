.class Lcom/android/internal/telephony/SMSDispatcher$Segment;
.super Ljava/lang/Object;
.source "SMSDispatcher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/SMSDispatcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Segment"
.end annotation


# static fields
.field public static final ADDRESS:Ljava/lang/String; = "address"

.field public static final COUNTER_LENGTH:I = 0xc

.field public static final EXPIRATION_TIME:I = 0x493e0

.field public static final ID:Ljava/lang/String; = "_id"

.field public static final MAX_COUNT:I = 0xa

.field public static final MIN_COUNT:I = 0x2

.field public static final PDU:Ljava/lang/String; = "pdu"

.field public static final PROJECTION:[Ljava/lang/String; = null

.field public static final SEQUENCE:Ljava/lang/String; = "sequence"

.field public static final STATE_BRACE1:I = 0x1

.field public static final STATE_BRACE2:I = 0xb

.field public static final STATE_DONE:I = 0x15

.field public static final STATE_F:I = 0x10

.field public static final STATE_INVALID:I = -0x1

.field public static final STATE_NUMBER1:I = 0x4

.field public static final STATE_NUMBER2:I = 0x9

.field public static final STATE_NUMBER3:I = 0xd

.field public static final STATE_NUMBER4:I = 0x13

.field public static final STATE_O:I = 0xf

.field public static final STATE_SLASH:I = 0x6

.field public static final STATE_SLASH2:I = 0x16

.field public static final STATE_SPACE1:I = 0x2

.field public static final STATE_SPACE2:I = 0x5

.field public static final STATE_SPACE3:I = 0x7

.field public static final STATE_SPACE4:I = 0xa

.field public static final STATE_SPACE5:I = 0xe

.field public static final STATE_SPACE6:I = 0x11

.field public static final STATE_SPACE7:I = 0x14

.field public static final STATE_START:I = 0x0

.field public static final STATE_ZERO1:I = 0x3

.field public static final STATE_ZERO2:I = 0x8

.field public static final STATE_ZERO3:I = 0xc

.field public static final STATE_ZERO4:I = 0x12

.field public static final TAG:Ljava/lang/String; = "SMS Segmentation"

.field public static final TIME:Ljava/lang/String; = "time"

.field public static final TOTAL_COUNT:Ljava/lang/String; = "totalCount"


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 4080
    const/4 v0, 0x6

    #@1
    new-array v0, v0, [Ljava/lang/String;

    #@3
    const/4 v1, 0x0

    #@4
    const-string v2, "_id"

    #@6
    aput-object v2, v0, v1

    #@8
    const/4 v1, 0x1

    #@9
    const-string v2, "time"

    #@b
    aput-object v2, v0, v1

    #@d
    const/4 v1, 0x2

    #@e
    const-string v2, "sequence"

    #@10
    aput-object v2, v0, v1

    #@12
    const/4 v1, 0x3

    #@13
    const-string v2, "totalCount"

    #@15
    aput-object v2, v0, v1

    #@17
    const/4 v1, 0x4

    #@18
    const-string v2, "address"

    #@1a
    aput-object v2, v0, v1

    #@1c
    const/4 v1, 0x5

    #@1d
    const-string v2, "pdu"

    #@1f
    aput-object v2, v0, v1

    #@21
    sput-object v0, Lcom/android/internal/telephony/SMSDispatcher$Segment;->PROJECTION:[Ljava/lang/String;

    #@23
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 4070
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method
