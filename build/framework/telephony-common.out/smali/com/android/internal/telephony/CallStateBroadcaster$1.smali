.class final Lcom/android/internal/telephony/CallStateBroadcaster$1;
.super Ljava/util/HashMap;
.source "CallStateBroadcaster.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/CallStateBroadcaster;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 3

    #@0
    .prologue
    .line 87
    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    #@3
    .line 88
    sget-object v0, Lcom/android/internal/telephony/Call$State;->IDLE:Lcom/android/internal/telephony/Call$State;

    #@5
    invoke-virtual {v0}, Lcom/android/internal/telephony/Call$State;->toString()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    const-string v1, "IDLE"

    #@b
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/CallStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@e
    .line 89
    sget-object v0, Lcom/android/internal/telephony/Call$State;->ACTIVE:Lcom/android/internal/telephony/Call$State;

    #@10
    invoke-virtual {v0}, Lcom/android/internal/telephony/Call$State;->toString()Ljava/lang/String;

    #@13
    move-result-object v0

    #@14
    const-string v1, "CONNECTED"

    #@16
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/CallStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@19
    .line 90
    sget-object v0, Lcom/android/internal/telephony/Call$State;->HOLDING:Lcom/android/internal/telephony/Call$State;

    #@1b
    invoke-virtual {v0}, Lcom/android/internal/telephony/Call$State;->toString()Ljava/lang/String;

    #@1e
    move-result-object v0

    #@1f
    const-string v1, "HELD"

    #@21
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/CallStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@24
    .line 91
    sget-object v0, Lcom/android/internal/telephony/Call$State;->DIALING:Lcom/android/internal/telephony/Call$State;

    #@26
    invoke-virtual {v0}, Lcom/android/internal/telephony/Call$State;->toString()Ljava/lang/String;

    #@29
    move-result-object v0

    #@2a
    const-string v1, "ATTEMPTING"

    #@2c
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/CallStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2f
    .line 92
    sget-object v0, Lcom/android/internal/telephony/Call$State;->ALERTING:Lcom/android/internal/telephony/Call$State;

    #@31
    invoke-virtual {v0}, Lcom/android/internal/telephony/Call$State;->toString()Ljava/lang/String;

    #@34
    move-result-object v0

    #@35
    const-string v1, "ESTABLISHED"

    #@37
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/CallStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3a
    .line 93
    sget-object v0, Lcom/android/internal/telephony/Call$State;->INCOMING:Lcom/android/internal/telephony/Call$State;

    #@3c
    invoke-virtual {v0}, Lcom/android/internal/telephony/Call$State;->toString()Ljava/lang/String;

    #@3f
    move-result-object v0

    #@40
    const-string v1, "ATTEMPTING"

    #@42
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/CallStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@45
    .line 94
    sget-object v0, Lcom/android/internal/telephony/Call$State;->WAITING:Lcom/android/internal/telephony/Call$State;

    #@47
    invoke-virtual {v0}, Lcom/android/internal/telephony/Call$State;->toString()Ljava/lang/String;

    #@4a
    move-result-object v0

    #@4b
    const-string v1, "ESTABLISHED"

    #@4d
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/CallStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@50
    .line 95
    sget-object v0, Lcom/android/internal/telephony/Call$State;->DISCONNECTED:Lcom/android/internal/telephony/Call$State;

    #@52
    invoke-virtual {v0}, Lcom/android/internal/telephony/Call$State;->toString()Ljava/lang/String;

    #@55
    move-result-object v0

    #@56
    const-string v1, "FAILED"

    #@58
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/CallStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5b
    .line 96
    sget-object v0, Lcom/android/internal/telephony/Call$State;->DISCONNECTING:Lcom/android/internal/telephony/Call$State;

    #@5d
    invoke-virtual {v0}, Lcom/android/internal/telephony/Call$State;->toString()Ljava/lang/String;

    #@60
    move-result-object v0

    #@61
    const-string v1, "DISCONNECTING"

    #@63
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/telephony/CallStateBroadcaster$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@66
    .line 97
    return-void
.end method
