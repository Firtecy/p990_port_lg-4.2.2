.class public Lcom/android/internal/telephony/CallStateBroadcaster;
.super Ljava/lang/Object;
.source "CallStateBroadcaster.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/CallStateBroadcaster$InstanceLock;
    }
.end annotation


# static fields
.field private static final ACTION_DETAILED_CALL_STATE:Ljava/lang/String; = "diagandroid.phone.detailedCallState"

.field private static final CALL_STATE_ENDED:Ljava/lang/String; = "ENDED"

#the value of this static final field might be set in the static constructor
.field static final DEBUG:Z = false

.field private static final EXTRA_CALL_CODE:Ljava/lang/String; = "CallCode"

.field private static final EXTRA_CALL_NUMBER:Ljava/lang/String; = "CallNumber"

.field private static final EXTRA_CALL_STATE:Ljava/lang/String; = "CallState"

.field private static final PERMISSION_RECEIVE_DETAILED_CALL_STATE:Ljava/lang/String; = "diagandroid.phone.receiveDetailedCallState"

.field static final TAG:Ljava/lang/String; = "CallStateBroadcaster"

.field private static sInstance:Lcom/android/internal/telephony/CallStateBroadcaster;

.field private static sStatusCodePerCall:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final sStatusCodes:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 19
    const-string v0, "ro.debuggable"

    #@2
    const-string v1, "0"

    #@4
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    const-string v1, "1"

    #@a
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_1e

    #@10
    const/4 v0, 0x1

    #@11
    :goto_11
    sput-boolean v0, Lcom/android/internal/telephony/CallStateBroadcaster;->DEBUG:Z

    #@13
    .line 85
    const/4 v0, 0x0

    #@14
    sput-object v0, Lcom/android/internal/telephony/CallStateBroadcaster;->sStatusCodePerCall:Ljava/util/HashMap;

    #@16
    .line 87
    new-instance v0, Lcom/android/internal/telephony/CallStateBroadcaster$1;

    #@18
    invoke-direct {v0}, Lcom/android/internal/telephony/CallStateBroadcaster$1;-><init>()V

    #@1b
    sput-object v0, Lcom/android/internal/telephony/CallStateBroadcaster;->sStatusCodes:Ljava/util/HashMap;

    #@1d
    return-void

    #@1e
    .line 19
    :cond_1e
    const/4 v0, 0x0

    #@1f
    goto :goto_11
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 108
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 109
    iput-object p1, p0, Lcom/android/internal/telephony/CallStateBroadcaster;->mContext:Landroid/content/Context;

    #@5
    .line 110
    new-instance v0, Ljava/util/HashMap;

    #@7
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@a
    sput-object v0, Lcom/android/internal/telephony/CallStateBroadcaster;->sStatusCodePerCall:Ljava/util/HashMap;

    #@c
    .line 111
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Lcom/android/internal/telephony/CallStateBroadcaster$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/CallStateBroadcaster;-><init>(Landroid/content/Context;)V

    #@3
    return-void
.end method

.method private Broadcast(Landroid/content/Intent;)V
    .registers 4
    .parameter "intent"

    #@0
    .prologue
    .line 161
    iget-object v0, p0, Lcom/android/internal/telephony/CallStateBroadcaster;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "diagandroid.phone.receiveDetailedCallState"

    #@4
    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    #@7
    .line 162
    return-void
.end method

.method private static CreateIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .registers 4
    .parameter "callState"
    .parameter "number"

    #@0
    .prologue
    .line 100
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "diagandroid.phone.detailedCallState"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 101
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "CallState"

    #@9
    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@c
    .line 102
    const-string v1, "CallNumber"

    #@e
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@11
    .line 103
    return-object v0
.end method

.method private static GetStatus(I)Ljava/lang/String;
    .registers 4
    .parameter "id"

    #@0
    .prologue
    .line 153
    sget-boolean v0, Lcom/android/internal/telephony/CallStateBroadcaster;->DEBUG:Z

    #@2
    if-eqz v0, :cond_1c

    #@4
    const-string v0, "CallStateBroadcaster"

    #@6
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v2, "[EXTENSION] GetStatus - CIQ ID : "

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 154
    :cond_1c
    sget-object v0, Lcom/android/internal/telephony/CallStateBroadcaster;->sStatusCodePerCall:Ljava/util/HashMap;

    #@1e
    if-eqz v0, :cond_2d

    #@20
    .line 155
    sget-object v0, Lcom/android/internal/telephony/CallStateBroadcaster;->sStatusCodePerCall:Ljava/util/HashMap;

    #@22
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@25
    move-result-object v1

    #@26
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@29
    move-result-object v0

    #@2a
    check-cast v0, Ljava/lang/String;

    #@2c
    .line 157
    :goto_2c
    return-object v0

    #@2d
    :cond_2d
    const/4 v0, 0x0

    #@2e
    goto :goto_2c
.end method

.method public static SendCallDisconnected(ILjava/lang/String;I)V
    .registers 5
    .parameter "id"
    .parameter "number"
    .parameter "cause"

    #@0
    .prologue
    .line 31
    sget-object v0, Lcom/android/internal/telephony/CallStateBroadcaster;->sInstance:Lcom/android/internal/telephony/CallStateBroadcaster;

    #@2
    if-eqz v0, :cond_d

    #@4
    .line 32
    sget-object v0, Lcom/android/internal/telephony/CallStateBroadcaster;->sInstance:Lcom/android/internal/telephony/CallStateBroadcaster;

    #@6
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@9
    move-result-object v1

    #@a
    invoke-direct {v0, p0, p1, v1}, Lcom/android/internal/telephony/CallStateBroadcaster;->SendCallDisconnected(ILjava/lang/String;Ljava/lang/String;)V

    #@d
    .line 34
    :cond_d
    return-void
.end method

.method private SendCallDisconnected(ILjava/lang/String;Ljava/lang/String;)V
    .registers 9
    .parameter "id"
    .parameter "number"
    .parameter "cause"

    #@0
    .prologue
    .line 126
    if-eqz p3, :cond_62

    #@2
    .line 127
    invoke-static {p1}, Lcom/android/internal/telephony/CallStateBroadcaster;->GetStatus(I)Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    .line 128
    .local v1, preStatus:Ljava/lang/String;
    if-nez v1, :cond_2f

    #@8
    .line 129
    sget-boolean v2, Lcom/android/internal/telephony/CallStateBroadcaster;->DEBUG:Z

    #@a
    if-eqz v2, :cond_2a

    #@c
    const-string v2, "CallStateBroadcaster"

    #@e
    new-instance v3, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v4, "[EXTENSION] CIQ ID : "

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    const-string v4, ", preStatus is null"

    #@1f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v3

    #@27
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 130
    :cond_2a
    const-string v2, "ATTEMPTING"

    #@2c
    invoke-direct {p0, p1, p2, v2}, Lcom/android/internal/telephony/CallStateBroadcaster;->SendCallStatus(ILjava/lang/String;Ljava/lang/String;)V

    #@2f
    .line 132
    :cond_2f
    const-string v2, "ENDED"

    #@31
    invoke-static {v2, p2}, Lcom/android/internal/telephony/CallStateBroadcaster;->CreateIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@34
    move-result-object v0

    #@35
    .line 133
    .local v0, intent:Landroid/content/Intent;
    const-string v2, "CallCode"

    #@37
    invoke-virtual {v0, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@3a
    .line 134
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/CallStateBroadcaster;->Broadcast(Landroid/content/Intent;)V

    #@3d
    .line 135
    sget-boolean v2, Lcom/android/internal/telephony/CallStateBroadcaster;->DEBUG:Z

    #@3f
    if-eqz v2, :cond_59

    #@41
    .line 136
    const-string v2, "CallStateBroadcaster"

    #@43
    new-instance v3, Ljava/lang/StringBuilder;

    #@45
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@48
    const-string v4, "[EXTENSION]SendCallDisconnected : status = CALL_STATE_ENDED  cause = "

    #@4a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v3

    #@4e
    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v3

    #@52
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@55
    move-result-object v3

    #@56
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@59
    .line 138
    :cond_59
    sget-object v2, Lcom/android/internal/telephony/CallStateBroadcaster;->sStatusCodePerCall:Ljava/util/HashMap;

    #@5b
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5e
    move-result-object v3

    #@5f
    invoke-virtual {v2, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@62
    .line 140
    .end local v0           #intent:Landroid/content/Intent;
    .end local v1           #preStatus:Ljava/lang/String;
    :cond_62
    return-void
.end method

.method public static SendCallStatus(ILjava/lang/String;Lcom/android/internal/telephony/Call$State;)V
    .registers 7
    .parameter "id"
    .parameter "number"
    .parameter "status"

    #@0
    .prologue
    .line 21
    sget-boolean v1, Lcom/android/internal/telephony/CallStateBroadcaster;->DEBUG:Z

    #@2
    if-eqz v1, :cond_3c

    #@4
    .line 22
    const-string v1, "CallStateBroadcaster"

    #@6
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v3, "[EXTENSION]SendCallStatus : sInstance = "

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    sget-object v3, Lcom/android/internal/telephony/CallStateBroadcaster;->sInstance:Lcom/android/internal/telephony/CallStateBroadcaster;

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    const-string v3, ", CIQ ID : "

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    const-string v3, ", number "

    #@23
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v2

    #@27
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    const-string v3, ", status = "

    #@2d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v2

    #@31
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v2

    #@35
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v2

    #@39
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3c
    .line 24
    :cond_3c
    sget-object v1, Lcom/android/internal/telephony/CallStateBroadcaster;->sInstance:Lcom/android/internal/telephony/CallStateBroadcaster;

    #@3e
    if-eqz v1, :cond_55

    #@40
    if-eqz p1, :cond_55

    #@42
    if-eqz p2, :cond_55

    #@44
    .line 25
    sget-object v1, Lcom/android/internal/telephony/CallStateBroadcaster;->sStatusCodes:Ljava/util/HashMap;

    #@46
    invoke-virtual {p2}, Lcom/android/internal/telephony/Call$State;->toString()Ljava/lang/String;

    #@49
    move-result-object v2

    #@4a
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@4d
    move-result-object v0

    #@4e
    check-cast v0, Ljava/lang/String;

    #@50
    .line 26
    .local v0, statusCode:Ljava/lang/String;
    sget-object v1, Lcom/android/internal/telephony/CallStateBroadcaster;->sInstance:Lcom/android/internal/telephony/CallStateBroadcaster;

    #@52
    invoke-direct {v1, p0, p1, v0}, Lcom/android/internal/telephony/CallStateBroadcaster;->SendCallStatus(ILjava/lang/String;Ljava/lang/String;)V

    #@55
    .line 28
    .end local v0           #statusCode:Ljava/lang/String;
    :cond_55
    return-void
.end method

.method private SendCallStatus(ILjava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter "id"
    .parameter "number"
    .parameter "statusString"

    #@0
    .prologue
    .line 114
    if-eqz p3, :cond_32

    #@2
    .line 115
    invoke-static {p3, p2}, Lcom/android/internal/telephony/CallStateBroadcaster;->CreateIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@5
    move-result-object v0

    #@6
    .line 116
    .local v0, intent:Landroid/content/Intent;
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/CallStateBroadcaster;->Broadcast(Landroid/content/Intent;)V

    #@9
    .line 118
    sget-boolean v1, Lcom/android/internal/telephony/CallStateBroadcaster;->DEBUG:Z

    #@b
    if-eqz v1, :cond_2f

    #@d
    .line 119
    const-string v1, "CallStateBroadcaster"

    #@f
    new-instance v2, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v3, "[EXTENSION]SendCallStatus :  CIQ ID = "

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    const-string v3, ", status = "

    #@20
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v2

    #@28
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v2

    #@2c
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 121
    :cond_2f
    invoke-static {p1, p3}, Lcom/android/internal/telephony/CallStateBroadcaster;->SetStatus(ILjava/lang/String;)V

    #@32
    .line 123
    .end local v0           #intent:Landroid/content/Intent;
    :cond_32
    return-void
.end method

.method private static SetStatus(ILjava/lang/String;)V
    .registers 6
    .parameter "id"
    .parameter "status"

    #@0
    .prologue
    .line 143
    sget-boolean v1, Lcom/android/internal/telephony/CallStateBroadcaster;->DEBUG:Z

    #@2
    if-eqz v1, :cond_26

    #@4
    const-string v1, "CallStateBroadcaster"

    #@6
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v3, "[EXTENSION] SetStatus - CIQ ID : "

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    const-string v3, ", status : "

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 144
    :cond_26
    sget-object v1, Lcom/android/internal/telephony/CallStateBroadcaster;->sStatusCodePerCall:Ljava/util/HashMap;

    #@28
    if-eqz v1, :cond_47

    #@2a
    .line 145
    sget-object v1, Lcom/android/internal/telephony/CallStateBroadcaster;->sStatusCodePerCall:Ljava/util/HashMap;

    #@2c
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2f
    move-result-object v2

    #@30
    invoke-virtual {v1, v2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@33
    move-result-object v0

    #@34
    check-cast v0, Ljava/lang/String;

    #@36
    .line 146
    .local v0, result:Ljava/lang/String;
    if-nez v0, :cond_47

    #@38
    const-string v1, "ATTEMPTING"

    #@3a
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3d
    move-result v1

    #@3e
    if-nez v1, :cond_47

    #@40
    .line 147
    const-string v1, "CallStateBroadcaster"

    #@42
    const-string v2, "[EXTENSION] Call state is wrong"

    #@44
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@47
    .line 149
    .end local v0           #result:Ljava/lang/String;
    :cond_47
    return-void
.end method

.method static synthetic access$002(Lcom/android/internal/telephony/CallStateBroadcaster;)Lcom/android/internal/telephony/CallStateBroadcaster;
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 17
    sput-object p0, Lcom/android/internal/telephony/CallStateBroadcaster;->sInstance:Lcom/android/internal/telephony/CallStateBroadcaster;

    #@2
    return-object p0
.end method
