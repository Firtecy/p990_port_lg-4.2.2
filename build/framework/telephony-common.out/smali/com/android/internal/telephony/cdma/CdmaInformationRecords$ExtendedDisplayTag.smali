.class public final enum Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;
.super Ljava/lang/Enum;
.source "CdmaInformationRecords.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/cdma/CdmaInformationRecords;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ExtendedDisplayTag"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

.field public static final enum X_DISPLAY_TAG_ACCUMULATED_DIGITS:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

.field public static final enum X_DISPLAY_TAG_BLANK:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

.field public static final enum X_DISPLAY_TAG_CALLED_ADDRESS:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

.field public static final enum X_DISPLAY_TAG_CALLED_PARTY_NAME:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

.field public static final enum X_DISPLAY_TAG_CALLING_ADDRESS:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

.field public static final enum X_DISPLAY_TAG_CALLING_PARTY_NAME:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

.field public static final enum X_DISPLAY_TAG_CALL_APPEARANCE_ID:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

.field public static final enum X_DISPLAY_TAG_CAUSE:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

.field public static final enum X_DISPLAY_TAG_CONNECTED_NAME:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

.field public static final enum X_DISPLAY_TAG_CONNECTED_NUMBER:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

.field public static final enum X_DISPLAY_TAG_CONTINUATION:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

.field public static final enum X_DISPLAY_TAG_DATETIME:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

.field public static final enum X_DISPLAY_TAG_FEATURE_ADDRESS:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

.field public static final enum X_DISPLAY_TAG_INBAND:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

.field public static final enum X_DISPLAY_TAG_NOTIFICATION_INDICATOR:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

.field public static final enum X_DISPLAY_TAG_ORIGINAL_CALLED_NAME:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

.field public static final enum X_DISPLAY_TAG_ORIGINAL_CALLED_NUMBER:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

.field public static final enum X_DISPLAY_TAG_ORIGINATING_RESTRICTIONS:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

.field public static final enum X_DISPLAY_TAG_PROGRESS_INDICATOR:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

.field public static final enum X_DISPLAY_TAG_PROMPT:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

.field public static final enum X_DISPLAY_TAG_REASON:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

.field public static final enum X_DISPLAY_TAG_REDIRECTING_NAME:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

.field public static final enum X_DISPLAY_TAG_REDIRECTING_NUMBER:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

.field public static final enum X_DISPLAY_TAG_REDIRECTION_NAME:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

.field public static final enum X_DISPLAY_TAG_REDIRECTION_NUMBER:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

.field public static final enum X_DISPLAY_TAG_SKIP:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

.field public static final enum X_DISPLAY_TAG_STATUS:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

.field public static final enum X_DISPLAY_TAG_TEXT:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;


# instance fields
.field private final mValue:B


# direct methods
.method static constructor <clinit>()V
    .registers 9

    #@0
    .prologue
    const/4 v8, 0x4

    #@1
    const/4 v7, 0x3

    #@2
    const/4 v6, 0x2

    #@3
    const/4 v5, 0x1

    #@4
    const/4 v4, 0x0

    #@5
    .line 202
    new-instance v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@7
    const-string v1, "X_DISPLAY_TAG_BLANK"

    #@9
    const/16 v2, -0x80

    #@b
    invoke-direct {v0, v1, v4, v2}, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;-><init>(Ljava/lang/String;IB)V

    #@e
    sput-object v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_BLANK:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@10
    .line 203
    new-instance v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@12
    const-string v1, "X_DISPLAY_TAG_SKIP"

    #@14
    const/16 v2, -0x7f

    #@16
    invoke-direct {v0, v1, v5, v2}, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;-><init>(Ljava/lang/String;IB)V

    #@19
    sput-object v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_SKIP:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@1b
    .line 204
    new-instance v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@1d
    const-string v1, "X_DISPLAY_TAG_CONTINUATION"

    #@1f
    const/16 v2, -0x7e

    #@21
    invoke-direct {v0, v1, v6, v2}, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;-><init>(Ljava/lang/String;IB)V

    #@24
    sput-object v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_CONTINUATION:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@26
    .line 205
    new-instance v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@28
    const-string v1, "X_DISPLAY_TAG_CALLED_ADDRESS"

    #@2a
    const/16 v2, -0x7d

    #@2c
    invoke-direct {v0, v1, v7, v2}, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;-><init>(Ljava/lang/String;IB)V

    #@2f
    sput-object v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_CALLED_ADDRESS:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@31
    .line 206
    new-instance v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@33
    const-string v1, "X_DISPLAY_TAG_CAUSE"

    #@35
    const/16 v2, -0x7c

    #@37
    invoke-direct {v0, v1, v8, v2}, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;-><init>(Ljava/lang/String;IB)V

    #@3a
    sput-object v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_CAUSE:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@3c
    .line 207
    new-instance v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@3e
    const-string v1, "X_DISPLAY_TAG_PROGRESS_INDICATOR"

    #@40
    const/4 v2, 0x5

    #@41
    const/16 v3, -0x7b

    #@43
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;-><init>(Ljava/lang/String;IB)V

    #@46
    sput-object v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_PROGRESS_INDICATOR:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@48
    .line 208
    new-instance v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@4a
    const-string v1, "X_DISPLAY_TAG_NOTIFICATION_INDICATOR"

    #@4c
    const/4 v2, 0x6

    #@4d
    const/16 v3, -0x7a

    #@4f
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;-><init>(Ljava/lang/String;IB)V

    #@52
    sput-object v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_NOTIFICATION_INDICATOR:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@54
    .line 209
    new-instance v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@56
    const-string v1, "X_DISPLAY_TAG_PROMPT"

    #@58
    const/4 v2, 0x7

    #@59
    const/16 v3, -0x79

    #@5b
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;-><init>(Ljava/lang/String;IB)V

    #@5e
    sput-object v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_PROMPT:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@60
    .line 210
    new-instance v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@62
    const-string v1, "X_DISPLAY_TAG_ACCUMULATED_DIGITS"

    #@64
    const/16 v2, 0x8

    #@66
    const/16 v3, -0x78

    #@68
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;-><init>(Ljava/lang/String;IB)V

    #@6b
    sput-object v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_ACCUMULATED_DIGITS:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@6d
    .line 211
    new-instance v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@6f
    const-string v1, "X_DISPLAY_TAG_STATUS"

    #@71
    const/16 v2, 0x9

    #@73
    const/16 v3, -0x77

    #@75
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;-><init>(Ljava/lang/String;IB)V

    #@78
    sput-object v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_STATUS:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@7a
    .line 212
    new-instance v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@7c
    const-string v1, "X_DISPLAY_TAG_INBAND"

    #@7e
    const/16 v2, 0xa

    #@80
    const/16 v3, -0x76

    #@82
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;-><init>(Ljava/lang/String;IB)V

    #@85
    sput-object v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_INBAND:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@87
    .line 213
    new-instance v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@89
    const-string v1, "X_DISPLAY_TAG_CALLING_ADDRESS"

    #@8b
    const/16 v2, 0xb

    #@8d
    const/16 v3, -0x75

    #@8f
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;-><init>(Ljava/lang/String;IB)V

    #@92
    sput-object v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_CALLING_ADDRESS:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@94
    .line 214
    new-instance v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@96
    const-string v1, "X_DISPLAY_TAG_REASON"

    #@98
    const/16 v2, 0xc

    #@9a
    const/16 v3, -0x74

    #@9c
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;-><init>(Ljava/lang/String;IB)V

    #@9f
    sput-object v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_REASON:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@a1
    .line 215
    new-instance v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@a3
    const-string v1, "X_DISPLAY_TAG_CALLING_PARTY_NAME"

    #@a5
    const/16 v2, 0xd

    #@a7
    const/16 v3, -0x73

    #@a9
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;-><init>(Ljava/lang/String;IB)V

    #@ac
    sput-object v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_CALLING_PARTY_NAME:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@ae
    .line 216
    new-instance v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@b0
    const-string v1, "X_DISPLAY_TAG_CALLED_PARTY_NAME"

    #@b2
    const/16 v2, 0xe

    #@b4
    const/16 v3, -0x72

    #@b6
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;-><init>(Ljava/lang/String;IB)V

    #@b9
    sput-object v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_CALLED_PARTY_NAME:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@bb
    .line 217
    new-instance v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@bd
    const-string v1, "X_DISPLAY_TAG_ORIGINAL_CALLED_NAME"

    #@bf
    const/16 v2, 0xf

    #@c1
    const/16 v3, -0x71

    #@c3
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;-><init>(Ljava/lang/String;IB)V

    #@c6
    sput-object v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_ORIGINAL_CALLED_NAME:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@c8
    .line 218
    new-instance v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@ca
    const-string v1, "X_DISPLAY_TAG_REDIRECTING_NAME"

    #@cc
    const/16 v2, 0x10

    #@ce
    const/16 v3, -0x70

    #@d0
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;-><init>(Ljava/lang/String;IB)V

    #@d3
    sput-object v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_REDIRECTING_NAME:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@d5
    .line 219
    new-instance v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@d7
    const-string v1, "X_DISPLAY_TAG_CONNECTED_NAME"

    #@d9
    const/16 v2, 0x11

    #@db
    const/16 v3, -0x6f

    #@dd
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;-><init>(Ljava/lang/String;IB)V

    #@e0
    sput-object v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_CONNECTED_NAME:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@e2
    .line 220
    new-instance v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@e4
    const-string v1, "X_DISPLAY_TAG_ORIGINATING_RESTRICTIONS"

    #@e6
    const/16 v2, 0x12

    #@e8
    const/16 v3, -0x6e

    #@ea
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;-><init>(Ljava/lang/String;IB)V

    #@ed
    sput-object v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_ORIGINATING_RESTRICTIONS:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@ef
    .line 221
    new-instance v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@f1
    const-string v1, "X_DISPLAY_TAG_DATETIME"

    #@f3
    const/16 v2, 0x13

    #@f5
    const/16 v3, -0x6d

    #@f7
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;-><init>(Ljava/lang/String;IB)V

    #@fa
    sput-object v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_DATETIME:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@fc
    .line 222
    new-instance v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@fe
    const-string v1, "X_DISPLAY_TAG_CALL_APPEARANCE_ID"

    #@100
    const/16 v2, 0x14

    #@102
    const/16 v3, -0x6c

    #@104
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;-><init>(Ljava/lang/String;IB)V

    #@107
    sput-object v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_CALL_APPEARANCE_ID:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@109
    .line 223
    new-instance v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@10b
    const-string v1, "X_DISPLAY_TAG_FEATURE_ADDRESS"

    #@10d
    const/16 v2, 0x15

    #@10f
    const/16 v3, -0x6b

    #@111
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;-><init>(Ljava/lang/String;IB)V

    #@114
    sput-object v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_FEATURE_ADDRESS:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@116
    .line 224
    new-instance v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@118
    const-string v1, "X_DISPLAY_TAG_REDIRECTION_NAME"

    #@11a
    const/16 v2, 0x16

    #@11c
    const/16 v3, -0x6a

    #@11e
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;-><init>(Ljava/lang/String;IB)V

    #@121
    sput-object v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_REDIRECTION_NAME:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@123
    .line 225
    new-instance v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@125
    const-string v1, "X_DISPLAY_TAG_REDIRECTION_NUMBER"

    #@127
    const/16 v2, 0x17

    #@129
    const/16 v3, -0x69

    #@12b
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;-><init>(Ljava/lang/String;IB)V

    #@12e
    sput-object v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_REDIRECTION_NUMBER:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@130
    .line 226
    new-instance v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@132
    const-string v1, "X_DISPLAY_TAG_REDIRECTING_NUMBER"

    #@134
    const/16 v2, 0x18

    #@136
    const/16 v3, -0x68

    #@138
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;-><init>(Ljava/lang/String;IB)V

    #@13b
    sput-object v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_REDIRECTING_NUMBER:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@13d
    .line 227
    new-instance v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@13f
    const-string v1, "X_DISPLAY_TAG_ORIGINAL_CALLED_NUMBER"

    #@141
    const/16 v2, 0x19

    #@143
    const/16 v3, -0x67

    #@145
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;-><init>(Ljava/lang/String;IB)V

    #@148
    sput-object v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_ORIGINAL_CALLED_NUMBER:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@14a
    .line 228
    new-instance v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@14c
    const-string v1, "X_DISPLAY_TAG_CONNECTED_NUMBER"

    #@14e
    const/16 v2, 0x1a

    #@150
    const/16 v3, -0x66

    #@152
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;-><init>(Ljava/lang/String;IB)V

    #@155
    sput-object v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_CONNECTED_NUMBER:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@157
    .line 229
    new-instance v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@159
    const-string v1, "X_DISPLAY_TAG_TEXT"

    #@15b
    const/16 v2, 0x1b

    #@15d
    const/16 v3, -0x62

    #@15f
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;-><init>(Ljava/lang/String;IB)V

    #@162
    sput-object v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_TEXT:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@164
    .line 201
    const/16 v0, 0x1c

    #@166
    new-array v0, v0, [Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@168
    sget-object v1, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_BLANK:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@16a
    aput-object v1, v0, v4

    #@16c
    sget-object v1, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_SKIP:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@16e
    aput-object v1, v0, v5

    #@170
    sget-object v1, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_CONTINUATION:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@172
    aput-object v1, v0, v6

    #@174
    sget-object v1, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_CALLED_ADDRESS:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@176
    aput-object v1, v0, v7

    #@178
    sget-object v1, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_CAUSE:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@17a
    aput-object v1, v0, v8

    #@17c
    const/4 v1, 0x5

    #@17d
    sget-object v2, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_PROGRESS_INDICATOR:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@17f
    aput-object v2, v0, v1

    #@181
    const/4 v1, 0x6

    #@182
    sget-object v2, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_NOTIFICATION_INDICATOR:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@184
    aput-object v2, v0, v1

    #@186
    const/4 v1, 0x7

    #@187
    sget-object v2, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_PROMPT:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@189
    aput-object v2, v0, v1

    #@18b
    const/16 v1, 0x8

    #@18d
    sget-object v2, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_ACCUMULATED_DIGITS:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@18f
    aput-object v2, v0, v1

    #@191
    const/16 v1, 0x9

    #@193
    sget-object v2, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_STATUS:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@195
    aput-object v2, v0, v1

    #@197
    const/16 v1, 0xa

    #@199
    sget-object v2, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_INBAND:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@19b
    aput-object v2, v0, v1

    #@19d
    const/16 v1, 0xb

    #@19f
    sget-object v2, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_CALLING_ADDRESS:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@1a1
    aput-object v2, v0, v1

    #@1a3
    const/16 v1, 0xc

    #@1a5
    sget-object v2, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_REASON:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@1a7
    aput-object v2, v0, v1

    #@1a9
    const/16 v1, 0xd

    #@1ab
    sget-object v2, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_CALLING_PARTY_NAME:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@1ad
    aput-object v2, v0, v1

    #@1af
    const/16 v1, 0xe

    #@1b1
    sget-object v2, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_CALLED_PARTY_NAME:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@1b3
    aput-object v2, v0, v1

    #@1b5
    const/16 v1, 0xf

    #@1b7
    sget-object v2, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_ORIGINAL_CALLED_NAME:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@1b9
    aput-object v2, v0, v1

    #@1bb
    const/16 v1, 0x10

    #@1bd
    sget-object v2, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_REDIRECTING_NAME:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@1bf
    aput-object v2, v0, v1

    #@1c1
    const/16 v1, 0x11

    #@1c3
    sget-object v2, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_CONNECTED_NAME:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@1c5
    aput-object v2, v0, v1

    #@1c7
    const/16 v1, 0x12

    #@1c9
    sget-object v2, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_ORIGINATING_RESTRICTIONS:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@1cb
    aput-object v2, v0, v1

    #@1cd
    const/16 v1, 0x13

    #@1cf
    sget-object v2, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_DATETIME:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@1d1
    aput-object v2, v0, v1

    #@1d3
    const/16 v1, 0x14

    #@1d5
    sget-object v2, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_CALL_APPEARANCE_ID:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@1d7
    aput-object v2, v0, v1

    #@1d9
    const/16 v1, 0x15

    #@1db
    sget-object v2, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_FEATURE_ADDRESS:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@1dd
    aput-object v2, v0, v1

    #@1df
    const/16 v1, 0x16

    #@1e1
    sget-object v2, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_REDIRECTION_NAME:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@1e3
    aput-object v2, v0, v1

    #@1e5
    const/16 v1, 0x17

    #@1e7
    sget-object v2, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_REDIRECTION_NUMBER:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@1e9
    aput-object v2, v0, v1

    #@1eb
    const/16 v1, 0x18

    #@1ed
    sget-object v2, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_REDIRECTING_NUMBER:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@1ef
    aput-object v2, v0, v1

    #@1f1
    const/16 v1, 0x19

    #@1f3
    sget-object v2, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_ORIGINAL_CALLED_NUMBER:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@1f5
    aput-object v2, v0, v1

    #@1f7
    const/16 v1, 0x1a

    #@1f9
    sget-object v2, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_CONNECTED_NUMBER:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@1fb
    aput-object v2, v0, v1

    #@1fd
    const/16 v1, 0x1b

    #@1ff
    sget-object v2, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->X_DISPLAY_TAG_TEXT:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@201
    aput-object v2, v0, v1

    #@203
    sput-object v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->$VALUES:[Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@205
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IB)V
    .registers 4
    .parameter
    .parameter
    .parameter "value"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(B)V"
        }
    .end annotation

    #@0
    .prologue
    .line 233
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    .line 234
    iput-byte p3, p0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->mValue:B

    #@5
    .line 235
    return-void
.end method

.method public static fromByte(B)Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;
    .registers 9
    .parameter "value"

    #@0
    .prologue
    .line 253
    const-string v5, "DisplayTag"

    #@2
    new-instance v6, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v7, "DisplayTag.fromByte("

    #@9
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v6

    #@d
    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v6

    #@11
    const-string v7, ")"

    #@13
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v6

    #@17
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v6

    #@1b
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 254
    const/4 v3, 0x0

    #@1f
    .line 255
    .local v3, ret:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;
    invoke-static {}, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->values()[Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@22
    move-result-object v0

    #@23
    .local v0, arr$:[Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;
    array-length v2, v0

    #@24
    .local v2, len$:I
    const/4 v1, 0x0

    #@25
    .local v1, i$:I
    :goto_25
    if-ge v1, v2, :cond_31

    #@27
    aget-object v4, v0, v1

    #@29
    .line 256
    .local v4, tag:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;
    iget-byte v5, v4, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->mValue:B

    #@2b
    if-ne v5, p0, :cond_2e

    #@2d
    .line 257
    move-object v3, v4

    #@2e
    .line 255
    :cond_2e
    add-int/lit8 v1, v1, 0x1

    #@30
    goto :goto_25

    #@31
    .line 259
    .end local v4           #tag:Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;
    :cond_31
    const-string v5, "DisplayTag"

    #@33
    new-instance v6, Ljava/lang/StringBuilder;

    #@35
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    const-string v7, "Tag for byte "

    #@3a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v6

    #@3e
    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@41
    move-result-object v6

    #@42
    const-string v7, ": "

    #@44
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v6

    #@48
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v6

    #@4c
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4f
    move-result-object v6

    #@50
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@53
    .line 260
    return-object v3
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 201
    const-class v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;
    .registers 1

    #@0
    .prologue
    .line 201
    sget-object v0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->$VALUES:[Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@2
    invoke-virtual {v0}, [Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;

    #@8
    return-object v0
.end method


# virtual methods
.method public asIndex()I
    .registers 5

    #@0
    .prologue
    .line 243
    iget-byte v1, p0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->mValue:B

    #@2
    and-int/lit16 v1, v1, 0xff

    #@4
    and-int/lit16 v0, v1, -0x81

    #@6
    .line 244
    .local v0, ret:I
    const-string v1, "DisplayTag"

    #@8
    new-instance v2, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->toString()Ljava/lang/String;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    const-string v3, " as index: "

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 245
    return v0
.end method

.method public value()B
    .registers 2

    #@0
    .prologue
    .line 236
    iget-byte v0, p0, Lcom/android/internal/telephony/cdma/CdmaInformationRecords$ExtendedDisplayTag;->mValue:B

    #@2
    return v0
.end method
