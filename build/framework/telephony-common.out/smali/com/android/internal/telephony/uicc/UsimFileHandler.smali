.class public final Lcom/android/internal/telephony/uicc/UsimFileHandler;
.super Lcom/android/internal/telephony/uicc/IccFileHandler;
.source "UsimFileHandler.java"

# interfaces
.implements Lcom/android/internal/telephony/uicc/IccConstants;


# static fields
.field static final LOG_TAG:Ljava/lang/String; = "RIL_UsimFH"


# direct methods
.method public constructor <init>(Lcom/android/internal/telephony/uicc/UiccCardApplication;Ljava/lang/String;Lcom/android/internal/telephony/CommandsInterface;)V
    .registers 4
    .parameter "app"
    .parameter "aid"
    .parameter "ci"

    #@0
    .prologue
    .line 32
    invoke-direct {p0, p1, p2, p3}, Lcom/android/internal/telephony/uicc/IccFileHandler;-><init>(Lcom/android/internal/telephony/uicc/UiccCardApplication;Ljava/lang/String;Lcom/android/internal/telephony/CommandsInterface;)V

    #@3
    .line 33
    return-void
.end method


# virtual methods
.method protected getEFPath(I)Ljava/lang/String;
    .registers 5
    .parameter "efid"

    #@0
    .prologue
    .line 37
    sparse-switch p1, :sswitch_data_2e

    #@3
    .line 100
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@6
    move-result-object v1

    #@7
    const-string v2, "KT"

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c
    move-result v1

    #@d
    if-eqz v1, :cond_12

    #@f
    .line 101
    sparse-switch p1, :sswitch_data_d4

    #@12
    .line 121
    :cond_12
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/uicc/UsimFileHandler;->getCommonIccEFPath(I)Ljava/lang/String;

    #@15
    move-result-object v0

    #@16
    .line 122
    .local v0, path:Ljava/lang/String;
    if-nez v0, :cond_1a

    #@18
    .line 126
    const-string v0, "3F007F105F3A"

    #@1a
    .line 128
    .end local v0           #path:Ljava/lang/String;
    :cond_1a
    :goto_1a
    return-object v0

    #@1b
    .line 83
    :sswitch_1b
    const-string v0, "3F007FFF"

    #@1d
    goto :goto_1a

    #@1e
    .line 87
    :sswitch_1e
    const-string v0, "3F007F105F3A"

    #@20
    goto :goto_1a

    #@21
    .line 90
    :sswitch_21
    const-string v0, "3F007F25"

    #@23
    goto :goto_1a

    #@24
    .line 95
    :sswitch_24
    const-string v0, "3F007FFF5F3D"

    #@26
    goto :goto_1a

    #@27
    .line 108
    :sswitch_27
    const-string v0, "3F007FFF5F3F"

    #@29
    goto :goto_1a

    #@2a
    .line 115
    :sswitch_2a
    const-string v0, "3F007FFF5F50"

    #@2c
    goto :goto_1a

    #@2d
    .line 37
    nop

    #@2e
    :sswitch_data_2e
    .sparse-switch
        0x2f24 -> :sswitch_1b
        0x4f22 -> :sswitch_24
        0x4f2c -> :sswitch_21
        0x4f30 -> :sswitch_1e
        0x4f55 -> :sswitch_24
        0x6f07 -> :sswitch_1b
        0x6f11 -> :sswitch_1b
        0x6f13 -> :sswitch_1b
        0x6f14 -> :sswitch_1b
        0x6f15 -> :sswitch_1b
        0x6f16 -> :sswitch_1b
        0x6f17 -> :sswitch_1b
        0x6f18 -> :sswitch_1b
        0x6f37 -> :sswitch_1b
        0x6f38 -> :sswitch_1b
        0x6f3b -> :sswitch_1b
        0x6f3c -> :sswitch_1b
        0x6f3e -> :sswitch_1b
        0x6f40 -> :sswitch_1b
        0x6f42 -> :sswitch_1b
        0x6f43 -> :sswitch_1b
        0x6f46 -> :sswitch_1b
        0x6f49 -> :sswitch_1b
        0x6f4b -> :sswitch_1b
        0x6f4e -> :sswitch_1b
        0x6f56 -> :sswitch_1b
        0x6f60 -> :sswitch_1b
        0x6f61 -> :sswitch_1b
        0x6f62 -> :sswitch_1b
        0x6f73 -> :sswitch_1b
        0x6f7b -> :sswitch_1b
        0x6f7e -> :sswitch_1b
        0x6fad -> :sswitch_1b
        0x6fc5 -> :sswitch_1b
        0x6fc6 -> :sswitch_1b
        0x6fc7 -> :sswitch_1b
        0x6fc8 -> :sswitch_1b
        0x6fc9 -> :sswitch_1b
        0x6fca -> :sswitch_1b
        0x6fcb -> :sswitch_1b
        0x6fcd -> :sswitch_1b
    .end sparse-switch

    #@d4
    .line 101
    :sswitch_data_d4
    .sparse-switch
        0x4f02 -> :sswitch_27
        0x4f03 -> :sswitch_27
        0x4f04 -> :sswitch_27
        0x4f07 -> :sswitch_27
        0x4f09 -> :sswitch_27
        0x4f81 -> :sswitch_2a
        0x4f82 -> :sswitch_2a
        0x4f83 -> :sswitch_2a
        0x4f84 -> :sswitch_2a
        0x4f85 -> :sswitch_2a
        0x4f86 -> :sswitch_2a
    .end sparse-switch
.end method

.method protected logd(Ljava/lang/String;)V
    .registers 3
    .parameter "msg"

    #@0
    .prologue
    .line 133
    const-string v0, "RIL_UsimFH"

    #@2
    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 134
    return-void
.end method

.method protected loge(Ljava/lang/String;)V
    .registers 3
    .parameter "msg"

    #@0
    .prologue
    .line 138
    const-string v0, "RIL_UsimFH"

    #@2
    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 139
    return-void
.end method
