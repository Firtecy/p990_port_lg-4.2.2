.class public final Lcom/android/internal/telephony/CallManager;
.super Ljava/lang/Object;
.source "CallManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/CallManager$2;
    }
.end annotation


# static fields
.field private static final DBG:Z = true

.field private static final EVENT_CALL_WAITING:I = 0x6c

.field private static final EVENT_CDMA_OTA_STATUS_CHANGE:I = 0x6f

.field private static final EVENT_CHECK_RADIO_STATE:I = 0xca

.field private static final EVENT_CIPHERING_NOTIFY:I = 0x7d

.field private static final EVENT_DISCONNECT:I = 0x64

.field private static final EVENT_DISPLAY_INFO:I = 0x6d

.field private static final EVENT_ECM_TIMER_RESET:I = 0x73

.field private static final EVENT_INCOMING_RING:I = 0x68

.field private static final EVENT_IN_CALL_VOICE_PRIVACY_OFF:I = 0x6b

.field private static final EVENT_IN_CALL_VOICE_PRIVACY_ON:I = 0x6a

.field private static final EVENT_MMI_COMPLETE:I = 0x72

.field private static final EVENT_MMI_INITIATE:I = 0x71

.field private static final EVENT_NEW_RINGING_CONNECTION:I = 0x66

.field private static final EVENT_POST_DIAL_CHARACTER:I = 0x77

.field private static final EVENT_PRECISE_CALL_STATE_CHANGED:I = 0x65

.field private static final EVENT_RADIO_ON:I = 0xcb

.field private static final EVENT_RESEND_INCALL_MUTE:I = 0x70

.field private static final EVENT_RINGBACK_TONE:I = 0x69

.field private static final EVENT_SERVICE_STATE_CHANGED:I = 0x76

.field private static final EVENT_SIGNAL_INFO:I = 0x6e

.field private static final EVENT_STATE_REDIRECTING_NUMBERINFO:I = 0x79

.field private static final EVENT_SUBSCRIPTION_INFO_READY:I = 0x74

.field private static final EVENT_SUPP_SERVICE_FAILED:I = 0x75

.field private static final EVENT_SUPP_SERVICE_NOTIFY:I = 0x78

.field private static final EVENT_TRY_EMERGENCY_CALL:I = 0xc9

.field private static final EVENT_UNKNOWN_CONNECTION:I = 0x67

.field private static final INSTANCE:Lcom/android/internal/telephony/CallManager; = null

.field private static final LOCAL_MODEM:I = 0x0

.field private static final LOG_TAG:Ljava/lang/String; = "CallManager"

.field private static final MAX_IN_CALL_AUDIO_MODE_BIT:I = 0x7

.field private static final MODE2DDESCRIPTION:[Ljava/lang/String; = null

.field private static final PROPERTY_BASEBAND:Ljava/lang/String; = "ro.baseband"

.field private static final PROPERTY_VOICE_MODEM_INDEX:Ljava/lang/String; = "persist.radio.voice.modem.index"

.field private static final REMOTE_MODEM:I = 0x1

.field private static final SGLTE:Ljava/lang/String; = "sglte"

.field private static final SGLTE_TYPE2:Ljava/lang/String; = "sglte2"

.field private static final VDBG:Z

.field public static isStartDtmf:Z

.field private static mIPPhoneProxy:Lcom/movial/ipphone/IPPhoneProxy;

.field public static mIgnoreDisconnect:Z

.field public static mIgnoreOutOfServiceArea:Z

.field private static mIsKeepingFakeCall:Z

.field private static mIsNormalPowerOnReq:Z

.field private static mIsUsingIPPhone:Z

.field private static mPowerStateCheckCount:I

.field private static mSkipRegistration:Z

.field private static syncObj:Ljava/lang/Object;


# instance fields
.field private final emptyConnections:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/telephony/Connection;",
            ">;"
        }
    .end annotation
.end field

.field private final mBackgroundCalls:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/telephony/Call;",
            ">;"
        }
    .end annotation
.end field

.field private mBaseband:Ljava/lang/String;

.field protected final mCallWaitingRegistrants:Landroid/os/RegistrantList;

.field protected final mCdmaOtaStatusChangeRegistrants:Landroid/os/RegistrantList;

.field protected final mCipheringNotificationRegistrants:Landroid/os/RegistrantList;

.field private mDefaultPhone:Lcom/android/internal/telephony/Phone;

.field protected final mDisconnectRegistrants:Landroid/os/RegistrantList;

.field protected final mDisplayInfoRegistrants:Landroid/os/RegistrantList;

.field protected final mEcmTimerResetRegistrants:Landroid/os/RegistrantList;

.field private final mForegroundCalls:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/telephony/Call;",
            ">;"
        }
    .end annotation
.end field

.field private mHandler:Landroid/os/Handler;

.field protected final mInCallVoicePrivacyOffRegistrants:Landroid/os/RegistrantList;

.field protected final mInCallVoicePrivacyOnRegistrants:Landroid/os/RegistrantList;

.field protected final mIncomingRingRegistrants:Landroid/os/RegistrantList;

.field private mIsIMSECCCalling:Z

.field protected final mMmiCompleteRegistrants:Landroid/os/RegistrantList;

.field protected final mMmiInitiateRegistrants:Landroid/os/RegistrantList;

.field protected final mMmiRegistrants:Landroid/os/RegistrantList;

.field protected final mNewRingingConnectionRegistrants:Landroid/os/RegistrantList;

.field private final mPhones:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/telephony/Phone;",
            ">;"
        }
    .end annotation
.end field

.field protected final mPostDialCharacterRegistrants:Landroid/os/RegistrantList;

.field protected final mPreciseCallStateRegistrants:Landroid/os/RegistrantList;

.field protected final mRedirectedNumberInfoRegistrants:Landroid/os/RegistrantList;

.field protected final mResendIncallMuteRegistrants:Landroid/os/RegistrantList;

.field protected final mRingbackToneRegistrants:Landroid/os/RegistrantList;

.field private final mRingingCalls:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/telephony/Call;",
            ">;"
        }
    .end annotation
.end field

.field protected final mServiceStateChangedRegistrants:Landroid/os/RegistrantList;

.field protected final mSignalInfoRegistrants:Landroid/os/RegistrantList;

.field private mSpeedUpAudioForMtCall:Z

.field protected final mSubscriptionInfoReadyRegistrants:Landroid/os/RegistrantList;

.field protected final mSuppServiceFailedRegistrants:Landroid/os/RegistrantList;

.field protected final mSuppServiceNotificationRegistrants:Landroid/os/RegistrantList;

.field protected final mUnknownConnectionRegistrants:Landroid/os/RegistrantList;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 121
    const/16 v0, 0xa

    #@4
    new-array v0, v0, [Ljava/lang/String;

    #@6
    const-string v1, "CS_ACTIVE"

    #@8
    aput-object v1, v0, v3

    #@a
    const-string v1, "CS_HOLD"

    #@c
    aput-object v1, v0, v4

    #@e
    const/4 v1, 0x2

    #@f
    const-string v2, "<invalid-2>"

    #@11
    aput-object v2, v0, v1

    #@13
    const/4 v1, 0x3

    #@14
    const-string v2, "<invalid-3>"

    #@16
    aput-object v2, v0, v1

    #@18
    const/4 v1, 0x4

    #@19
    const-string v2, "IMS_ACTIVE"

    #@1b
    aput-object v2, v0, v1

    #@1d
    const/4 v1, 0x5

    #@1e
    const-string v2, "IMS_HOLD"

    #@20
    aput-object v2, v0, v1

    #@22
    const/4 v1, 0x6

    #@23
    const-string v2, "<invalid-6>"

    #@25
    aput-object v2, v0, v1

    #@27
    const/4 v1, 0x7

    #@28
    const-string v2, "<invalid-7>"

    #@2a
    aput-object v2, v0, v1

    #@2c
    const/16 v1, 0x8

    #@2e
    const-string v2, "CS_ACTIVE_SESSION2"

    #@30
    aput-object v2, v0, v1

    #@32
    const/16 v1, 0x9

    #@34
    const-string v2, "CS_HOLD_SESSION2"

    #@36
    aput-object v2, v0, v1

    #@38
    sput-object v0, Lcom/android/internal/telephony/CallManager;->MODE2DDESCRIPTION:[Ljava/lang/String;

    #@3a
    .line 135
    new-instance v0, Lcom/android/internal/telephony/CallManager;

    #@3c
    invoke-direct {v0}, Lcom/android/internal/telephony/CallManager;-><init>()V

    #@3f
    sput-object v0, Lcom/android/internal/telephony/CallManager;->INSTANCE:Lcom/android/internal/telephony/CallManager;

    #@41
    .line 157
    sput-boolean v3, Lcom/android/internal/telephony/CallManager;->isStartDtmf:Z

    #@43
    .line 2591
    const/4 v0, 0x0

    #@44
    sput-object v0, Lcom/android/internal/telephony/CallManager;->mIPPhoneProxy:Lcom/movial/ipphone/IPPhoneProxy;

    #@46
    .line 2592
    sput-boolean v3, Lcom/android/internal/telephony/CallManager;->mIsUsingIPPhone:Z

    #@48
    .line 2594
    invoke-static {}, Lcom/android/internal/telephony/TelephonyUtils;->isIPPhoneSupported()Z

    #@4b
    move-result v0

    #@4c
    sput-boolean v0, Lcom/android/internal/telephony/CallManager;->mIsUsingIPPhone:Z

    #@4e
    .line 2619
    sput-boolean v4, Lcom/android/internal/telephony/CallManager;->mIsNormalPowerOnReq:Z

    #@50
    .line 2620
    sput-boolean v4, Lcom/android/internal/telephony/CallManager;->mSkipRegistration:Z

    #@52
    .line 2636
    sput-boolean v3, Lcom/android/internal/telephony/CallManager;->mIsKeepingFakeCall:Z

    #@54
    .line 2637
    new-instance v0, Ljava/lang/Object;

    #@56
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@59
    sput-object v0, Lcom/android/internal/telephony/CallManager;->syncObj:Ljava/lang/Object;

    #@5b
    .line 2659
    sput v3, Lcom/android/internal/telephony/CallManager;->mPowerStateCheckCount:I

    #@5d
    .line 2672
    sput-boolean v3, Lcom/android/internal/telephony/CallManager;->mIgnoreDisconnect:Z

    #@5f
    .line 2673
    sput-boolean v3, Lcom/android/internal/telephony/CallManager;->mIgnoreOutOfServiceArea:Z

    #@61
    return-void
.end method

.method private constructor <init>()V
    .registers 3

    #@0
    .prologue
    .line 240
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 150
    new-instance v0, Ljava/util/ArrayList;

    #@5
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/internal/telephony/CallManager;->emptyConnections:Ljava/util/ArrayList;

    #@a
    .line 155
    const/4 v0, 0x0

    #@b
    iput-boolean v0, p0, Lcom/android/internal/telephony/CallManager;->mSpeedUpAudioForMtCall:Z

    #@d
    .line 161
    const-string v0, "ro.baseband"

    #@f
    const-string v1, "msm"

    #@11
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    iput-object v0, p0, Lcom/android/internal/telephony/CallManager;->mBaseband:Ljava/lang/String;

    #@17
    .line 164
    new-instance v0, Landroid/os/RegistrantList;

    #@19
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@1c
    iput-object v0, p0, Lcom/android/internal/telephony/CallManager;->mPreciseCallStateRegistrants:Landroid/os/RegistrantList;

    #@1e
    .line 167
    new-instance v0, Landroid/os/RegistrantList;

    #@20
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@23
    iput-object v0, p0, Lcom/android/internal/telephony/CallManager;->mNewRingingConnectionRegistrants:Landroid/os/RegistrantList;

    #@25
    .line 170
    new-instance v0, Landroid/os/RegistrantList;

    #@27
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@2a
    iput-object v0, p0, Lcom/android/internal/telephony/CallManager;->mIncomingRingRegistrants:Landroid/os/RegistrantList;

    #@2c
    .line 173
    new-instance v0, Landroid/os/RegistrantList;

    #@2e
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@31
    iput-object v0, p0, Lcom/android/internal/telephony/CallManager;->mDisconnectRegistrants:Landroid/os/RegistrantList;

    #@33
    .line 176
    new-instance v0, Landroid/os/RegistrantList;

    #@35
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@38
    iput-object v0, p0, Lcom/android/internal/telephony/CallManager;->mMmiRegistrants:Landroid/os/RegistrantList;

    #@3a
    .line 179
    new-instance v0, Landroid/os/RegistrantList;

    #@3c
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@3f
    iput-object v0, p0, Lcom/android/internal/telephony/CallManager;->mUnknownConnectionRegistrants:Landroid/os/RegistrantList;

    #@41
    .line 182
    new-instance v0, Landroid/os/RegistrantList;

    #@43
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@46
    iput-object v0, p0, Lcom/android/internal/telephony/CallManager;->mRingbackToneRegistrants:Landroid/os/RegistrantList;

    #@48
    .line 185
    new-instance v0, Landroid/os/RegistrantList;

    #@4a
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@4d
    iput-object v0, p0, Lcom/android/internal/telephony/CallManager;->mInCallVoicePrivacyOnRegistrants:Landroid/os/RegistrantList;

    #@4f
    .line 188
    new-instance v0, Landroid/os/RegistrantList;

    #@51
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@54
    iput-object v0, p0, Lcom/android/internal/telephony/CallManager;->mInCallVoicePrivacyOffRegistrants:Landroid/os/RegistrantList;

    #@56
    .line 191
    new-instance v0, Landroid/os/RegistrantList;

    #@58
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@5b
    iput-object v0, p0, Lcom/android/internal/telephony/CallManager;->mCallWaitingRegistrants:Landroid/os/RegistrantList;

    #@5d
    .line 194
    new-instance v0, Landroid/os/RegistrantList;

    #@5f
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@62
    iput-object v0, p0, Lcom/android/internal/telephony/CallManager;->mDisplayInfoRegistrants:Landroid/os/RegistrantList;

    #@64
    .line 197
    new-instance v0, Landroid/os/RegistrantList;

    #@66
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@69
    iput-object v0, p0, Lcom/android/internal/telephony/CallManager;->mSignalInfoRegistrants:Landroid/os/RegistrantList;

    #@6b
    .line 200
    new-instance v0, Landroid/os/RegistrantList;

    #@6d
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@70
    iput-object v0, p0, Lcom/android/internal/telephony/CallManager;->mCdmaOtaStatusChangeRegistrants:Landroid/os/RegistrantList;

    #@72
    .line 203
    new-instance v0, Landroid/os/RegistrantList;

    #@74
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@77
    iput-object v0, p0, Lcom/android/internal/telephony/CallManager;->mResendIncallMuteRegistrants:Landroid/os/RegistrantList;

    #@79
    .line 206
    new-instance v0, Landroid/os/RegistrantList;

    #@7b
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@7e
    iput-object v0, p0, Lcom/android/internal/telephony/CallManager;->mMmiInitiateRegistrants:Landroid/os/RegistrantList;

    #@80
    .line 209
    new-instance v0, Landroid/os/RegistrantList;

    #@82
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@85
    iput-object v0, p0, Lcom/android/internal/telephony/CallManager;->mMmiCompleteRegistrants:Landroid/os/RegistrantList;

    #@87
    .line 212
    new-instance v0, Landroid/os/RegistrantList;

    #@89
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@8c
    iput-object v0, p0, Lcom/android/internal/telephony/CallManager;->mEcmTimerResetRegistrants:Landroid/os/RegistrantList;

    #@8e
    .line 215
    new-instance v0, Landroid/os/RegistrantList;

    #@90
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@93
    iput-object v0, p0, Lcom/android/internal/telephony/CallManager;->mSubscriptionInfoReadyRegistrants:Landroid/os/RegistrantList;

    #@95
    .line 218
    new-instance v0, Landroid/os/RegistrantList;

    #@97
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@9a
    iput-object v0, p0, Lcom/android/internal/telephony/CallManager;->mSuppServiceFailedRegistrants:Landroid/os/RegistrantList;

    #@9c
    .line 221
    new-instance v0, Landroid/os/RegistrantList;

    #@9e
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@a1
    iput-object v0, p0, Lcom/android/internal/telephony/CallManager;->mSuppServiceNotificationRegistrants:Landroid/os/RegistrantList;

    #@a3
    .line 224
    new-instance v0, Landroid/os/RegistrantList;

    #@a5
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@a8
    iput-object v0, p0, Lcom/android/internal/telephony/CallManager;->mServiceStateChangedRegistrants:Landroid/os/RegistrantList;

    #@aa
    .line 227
    new-instance v0, Landroid/os/RegistrantList;

    #@ac
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@af
    iput-object v0, p0, Lcom/android/internal/telephony/CallManager;->mPostDialCharacterRegistrants:Landroid/os/RegistrantList;

    #@b1
    .line 231
    new-instance v0, Landroid/os/RegistrantList;

    #@b3
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@b6
    iput-object v0, p0, Lcom/android/internal/telephony/CallManager;->mRedirectedNumberInfoRegistrants:Landroid/os/RegistrantList;

    #@b8
    .line 236
    new-instance v0, Landroid/os/RegistrantList;

    #@ba
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@bd
    iput-object v0, p0, Lcom/android/internal/telephony/CallManager;->mCipheringNotificationRegistrants:Landroid/os/RegistrantList;

    #@bf
    .line 2358
    new-instance v0, Lcom/android/internal/telephony/CallManager$1;

    #@c1
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/CallManager$1;-><init>(Lcom/android/internal/telephony/CallManager;)V

    #@c4
    iput-object v0, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@c6
    .line 241
    new-instance v0, Ljava/util/ArrayList;

    #@c8
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@cb
    iput-object v0, p0, Lcom/android/internal/telephony/CallManager;->mPhones:Ljava/util/ArrayList;

    #@cd
    .line 242
    new-instance v0, Ljava/util/ArrayList;

    #@cf
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@d2
    iput-object v0, p0, Lcom/android/internal/telephony/CallManager;->mRingingCalls:Ljava/util/ArrayList;

    #@d4
    .line 243
    new-instance v0, Ljava/util/ArrayList;

    #@d6
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@d9
    iput-object v0, p0, Lcom/android/internal/telephony/CallManager;->mBackgroundCalls:Ljava/util/ArrayList;

    #@db
    .line 244
    new-instance v0, Ljava/util/ArrayList;

    #@dd
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@e0
    iput-object v0, p0, Lcom/android/internal/telephony/CallManager;->mForegroundCalls:Ljava/util/ArrayList;

    #@e2
    .line 245
    const/4 v0, 0x0

    #@e3
    iput-object v0, p0, Lcom/android/internal/telephony/CallManager;->mDefaultPhone:Lcom/android/internal/telephony/Phone;

    #@e5
    .line 246
    return-void
.end method

.method static synthetic access$000(Lcom/android/internal/telephony/CallManager;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 75
    invoke-direct {p0}, Lcom/android/internal/telephony/CallManager;->hasMoreThanOneRingingCall()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$100()Z
    .registers 1

    #@0
    .prologue
    .line 75
    sget-boolean v0, Lcom/android/internal/telephony/CallManager;->mIsUsingIPPhone:Z

    #@2
    return v0
.end method

.method static synthetic access$200()Lcom/movial/ipphone/IPPhoneProxy;
    .registers 1

    #@0
    .prologue
    .line 75
    sget-object v0, Lcom/android/internal/telephony/CallManager;->mIPPhoneProxy:Lcom/movial/ipphone/IPPhoneProxy;

    #@2
    return-object v0
.end method

.method static synthetic access$302(Lcom/android/internal/telephony/CallManager;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 75
    iput-boolean p1, p0, Lcom/android/internal/telephony/CallManager;->mIsIMSECCCalling:Z

    #@2
    return p1
.end method

.method static synthetic access$400(Lcom/android/internal/telephony/CallManager;)Lcom/android/internal/telephony/Phone;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 75
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mDefaultPhone:Lcom/android/internal/telephony/Phone;

    #@2
    return-object v0
.end method

.method static synthetic access$500()I
    .registers 1

    #@0
    .prologue
    .line 75
    sget v0, Lcom/android/internal/telephony/CallManager;->mPowerStateCheckCount:I

    #@2
    return v0
.end method

.method static synthetic access$502(I)I
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 75
    sput p0, Lcom/android/internal/telephony/CallManager;->mPowerStateCheckCount:I

    #@2
    return p0
.end method

.method static synthetic access$508()I
    .registers 2

    #@0
    .prologue
    .line 75
    sget v0, Lcom/android/internal/telephony/CallManager;->mPowerStateCheckCount:I

    #@2
    add-int/lit8 v1, v0, 0x1

    #@4
    sput v1, Lcom/android/internal/telephony/CallManager;->mPowerStateCheckCount:I

    #@6
    return v0
.end method

.method static synthetic access$602(Z)Z
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 75
    sput-boolean p0, Lcom/android/internal/telephony/CallManager;->mIsNormalPowerOnReq:Z

    #@2
    return p0
.end method

.method static synthetic access$702(Z)Z
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 75
    sput-boolean p0, Lcom/android/internal/telephony/CallManager;->mSkipRegistration:Z

    #@2
    return p0
.end method

.method private canDial(Lcom/android/internal/telephony/Phone;)Z
    .registers 13
    .parameter "phone"

    #@0
    .prologue
    const/4 v10, 0x3

    #@1
    const/4 v8, 0x0

    #@2
    const/4 v7, 0x1

    #@3
    .line 1315
    invoke-interface {p1}, Lcom/android/internal/telephony/Phone;->getServiceState()Landroid/telephony/ServiceState;

    #@6
    move-result-object v9

    #@7
    invoke-virtual {v9}, Landroid/telephony/ServiceState;->getState()I

    #@a
    move-result v6

    #@b
    .line 1316
    .local v6, serviceState:I
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->hasActiveRingingCall()Z

    #@e
    move-result v4

    #@f
    .line 1317
    .local v4, hasRingingCall:Z
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->hasActiveFgCall()Z

    #@12
    move-result v2

    #@13
    .line 1318
    .local v2, hasActiveCall:Z
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->hasActiveBgCall()Z

    #@16
    move-result v3

    #@17
    .line 1319
    .local v3, hasHoldingCall:Z
    if-eqz v2, :cond_84

    #@19
    if-eqz v3, :cond_84

    #@1b
    move v0, v7

    #@1c
    .line 1320
    .local v0, allLinesTaken:Z
    :goto_1c
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getActiveFgCallState()Lcom/android/internal/telephony/Call$State;

    #@1f
    move-result-object v1

    #@20
    .line 1332
    .local v1, fgCallState:Lcom/android/internal/telephony/Call$State;
    invoke-interface {p1}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    #@23
    move-result v9

    #@24
    if-ne v9, v7, :cond_88

    #@26
    .line 1333
    if-eq v6, v10, :cond_86

    #@28
    if-nez v4, :cond_86

    #@2a
    sget-object v9, Lcom/android/internal/telephony/Call$State;->ACTIVE:Lcom/android/internal/telephony/Call$State;

    #@2c
    if-eq v1, v9, :cond_36

    #@2e
    sget-object v9, Lcom/android/internal/telephony/Call$State;->IDLE:Lcom/android/internal/telephony/Call$State;

    #@30
    if-eq v1, v9, :cond_36

    #@32
    sget-object v9, Lcom/android/internal/telephony/Call$State;->DISCONNECTED:Lcom/android/internal/telephony/Call$State;

    #@34
    if-ne v1, v9, :cond_86

    #@36
    :cond_36
    move v5, v7

    #@37
    .line 1346
    .local v5, result:Z
    :goto_37
    if-nez v5, :cond_83

    #@39
    .line 1347
    const-string v7, "CallManager"

    #@3b
    new-instance v8, Ljava/lang/StringBuilder;

    #@3d
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@40
    const-string v9, "canDial serviceState="

    #@42
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v8

    #@46
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@49
    move-result-object v8

    #@4a
    const-string v9, " hasRingingCall="

    #@4c
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v8

    #@50
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@53
    move-result-object v8

    #@54
    const-string v9, " hasActiveCall="

    #@56
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v8

    #@5a
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v8

    #@5e
    const-string v9, " hasHoldingCall="

    #@60
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v8

    #@64
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@67
    move-result-object v8

    #@68
    const-string v9, " allLinesTaken="

    #@6a
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v8

    #@6e
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@71
    move-result-object v8

    #@72
    const-string v9, " fgCallState="

    #@74
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v8

    #@78
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v8

    #@7c
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7f
    move-result-object v8

    #@80
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@83
    .line 1354
    :cond_83
    return v5

    #@84
    .end local v0           #allLinesTaken:Z
    .end local v1           #fgCallState:Lcom/android/internal/telephony/Call$State;
    .end local v5           #result:Z
    :cond_84
    move v0, v8

    #@85
    .line 1319
    goto :goto_1c

    #@86
    .restart local v0       #allLinesTaken:Z
    .restart local v1       #fgCallState:Lcom/android/internal/telephony/Call$State;
    :cond_86
    move v5, v8

    #@87
    .line 1333
    goto :goto_37

    #@88
    .line 1339
    :cond_88
    if-eq v6, v10, :cond_9c

    #@8a
    if-nez v4, :cond_9c

    #@8c
    if-nez v0, :cond_9c

    #@8e
    sget-object v9, Lcom/android/internal/telephony/Call$State;->ACTIVE:Lcom/android/internal/telephony/Call$State;

    #@90
    if-eq v1, v9, :cond_9a

    #@92
    sget-object v9, Lcom/android/internal/telephony/Call$State;->IDLE:Lcom/android/internal/telephony/Call$State;

    #@94
    if-eq v1, v9, :cond_9a

    #@96
    sget-object v9, Lcom/android/internal/telephony/Call$State;->DISCONNECTED:Lcom/android/internal/telephony/Call$State;

    #@98
    if-ne v1, v9, :cond_9c

    #@9a
    :cond_9a
    move v5, v7

    #@9b
    .restart local v5       #result:Z
    :goto_9b
    goto :goto_37

    #@9c
    .end local v5           #result:Z
    :cond_9c
    move v5, v8

    #@9d
    goto :goto_9b
.end method

.method public static checkNoCoverageCase(Lcom/android/internal/telephony/CommandsInterface;)V
    .registers 2
    .parameter "cm"

    #@0
    .prologue
    .line 2662
    sget-object v0, Lcom/android/internal/telephony/CallManager;->INSTANCE:Lcom/android/internal/telephony/CallManager;

    #@2
    invoke-direct {v0, p0}, Lcom/android/internal/telephony/CallManager;->checkRadioState(Lcom/android/internal/telephony/CommandsInterface;)V

    #@5
    .line 2663
    return-void
.end method

.method private checkRadioState(Lcom/android/internal/telephony/CommandsInterface;)V
    .registers 6
    .parameter "cm"

    #@0
    .prologue
    .line 2666
    const/4 v1, 0x0

    #@1
    sput v1, Lcom/android/internal/telephony/CallManager;->mPowerStateCheckCount:I

    #@3
    .line 2667
    iget-object v1, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@5
    const/16 v2, 0xca

    #@7
    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@a
    move-result-object v0

    #@b
    .line 2668
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@d
    const-wide/16 v2, 0x1f4

    #@f
    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@12
    .line 2669
    return-void
.end method

.method private getContext()Landroid/content/Context;
    .registers 3

    #@0
    .prologue
    .line 778
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    #@3
    move-result-object v0

    #@4
    .line 779
    .local v0, defaultPhone:Lcom/android/internal/telephony/Phone;
    if-nez v0, :cond_8

    #@6
    const/4 v1, 0x0

    #@7
    :goto_7
    return-object v1

    #@8
    :cond_8
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@b
    move-result-object v1

    #@c
    goto :goto_7
.end method

.method private getFirstActiveCall(Ljava/util/ArrayList;)Lcom/android/internal/telephony/Call;
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/telephony/Call;",
            ">;)",
            "Lcom/android/internal/telephony/Call;"
        }
    .end annotation

    #@0
    .prologue
    .line 2299
    .local p1, calls:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/Call;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@3
    move-result-object v1

    #@4
    .local v1, i$:Ljava/util/Iterator;
    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@7
    move-result v2

    #@8
    if-eqz v2, :cond_17

    #@a
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@d
    move-result-object v0

    #@e
    check-cast v0, Lcom/android/internal/telephony/Call;

    #@10
    .line 2300
    .local v0, call:Lcom/android/internal/telephony/Call;
    invoke-virtual {v0}, Lcom/android/internal/telephony/Call;->isIdle()Z

    #@13
    move-result v2

    #@14
    if-nez v2, :cond_4

    #@16
    .line 2304
    .end local v0           #call:Lcom/android/internal/telephony/Call;
    :goto_16
    return-object v0

    #@17
    :cond_17
    const/4 v0, 0x0

    #@18
    goto :goto_16
.end method

.method private getFirstCallOfState(Ljava/util/ArrayList;Lcom/android/internal/telephony/Call$State;)Lcom/android/internal/telephony/Call;
    .registers 6
    .parameter
    .parameter "state"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/telephony/Call;",
            ">;",
            "Lcom/android/internal/telephony/Call$State;",
            ")",
            "Lcom/android/internal/telephony/Call;"
        }
    .end annotation

    #@0
    .prologue
    .line 2311
    .local p1, calls:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/internal/telephony/Call;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@3
    move-result-object v1

    #@4
    .local v1, i$:Ljava/util/Iterator;
    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@7
    move-result v2

    #@8
    if-eqz v2, :cond_17

    #@a
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@d
    move-result-object v0

    #@e
    check-cast v0, Lcom/android/internal/telephony/Call;

    #@10
    .line 2312
    .local v0, call:Lcom/android/internal/telephony/Call;
    invoke-virtual {v0}, Lcom/android/internal/telephony/Call;->getState()Lcom/android/internal/telephony/Call$State;

    #@13
    move-result-object v2

    #@14
    if-ne v2, p2, :cond_4

    #@16
    .line 2316
    .end local v0           #call:Lcom/android/internal/telephony/Call;
    :goto_16
    return-object v0

    #@17
    :cond_17
    const/4 v0, 0x0

    #@18
    goto :goto_16
.end method

.method private getFirstNonIdleCall(Ljava/util/List;)Lcom/android/internal/telephony/Call;
    .registers 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/Call;",
            ">;)",
            "Lcom/android/internal/telephony/Call;"
        }
    .end annotation

    #@0
    .prologue
    .line 2165
    .local p1, calls:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/telephony/Call;>;"
    const/4 v2, 0x0

    #@1
    .line 2166
    .local v2, result:Lcom/android/internal/telephony/Call;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@4
    move-result-object v1

    #@5
    .local v1, i$:Ljava/util/Iterator;
    :cond_5
    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@8
    move-result v3

    #@9
    if-eqz v3, :cond_24

    #@b
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@e
    move-result-object v0

    #@f
    check-cast v0, Lcom/android/internal/telephony/Call;

    #@11
    .line 2167
    .local v0, call:Lcom/android/internal/telephony/Call;
    invoke-virtual {v0}, Lcom/android/internal/telephony/Call;->isIdle()Z

    #@14
    move-result v3

    #@15
    if-nez v3, :cond_18

    #@17
    .line 2173
    .end local v0           #call:Lcom/android/internal/telephony/Call;
    :goto_17
    return-object v0

    #@18
    .line 2169
    .restart local v0       #call:Lcom/android/internal/telephony/Call;
    :cond_18
    invoke-virtual {v0}, Lcom/android/internal/telephony/Call;->getState()Lcom/android/internal/telephony/Call$State;

    #@1b
    move-result-object v3

    #@1c
    sget-object v4, Lcom/android/internal/telephony/Call$State;->IDLE:Lcom/android/internal/telephony/Call$State;

    #@1e
    if-eq v3, v4, :cond_5

    #@20
    .line 2170
    if-nez v2, :cond_5

    #@22
    move-object v2, v0

    #@23
    goto :goto_5

    #@24
    .end local v0           #call:Lcom/android/internal/telephony/Call;
    :cond_24
    move-object v0, v2

    #@25
    .line 2173
    goto :goto_17
.end method

.method public static getInstance()Lcom/android/internal/telephony/CallManager;
    .registers 1

    #@0
    .prologue
    .line 253
    sget-object v0, Lcom/android/internal/telephony/CallManager;->INSTANCE:Lcom/android/internal/telephony/CallManager;

    #@2
    return-object v0
.end method

.method private static getPhoneBase(Lcom/android/internal/telephony/Phone;)Lcom/android/internal/telephony/Phone;
    .registers 2
    .parameter "phone"

    #@0
    .prologue
    .line 266
    sget-boolean v0, Lcom/android/internal/telephony/CallManager;->mIsUsingIPPhone:Z

    #@2
    if-eqz v0, :cond_5

    #@4
    .line 278
    .end local p0
    :cond_4
    :goto_4
    return-object p0

    #@5
    .line 273
    .restart local p0
    :cond_5
    instance-of v0, p0, Lcom/android/internal/telephony/PhoneProxy;

    #@7
    if-eqz v0, :cond_4

    #@9
    .line 274
    invoke-interface {p0}, Lcom/android/internal/telephony/Phone;->getForegroundCall()Lcom/android/internal/telephony/Call;

    #@c
    move-result-object v0

    #@d
    invoke-virtual {v0}, Lcom/android/internal/telephony/Call;->getPhone()Lcom/android/internal/telephony/Phone;

    #@10
    move-result-object p0

    #@11
    goto :goto_4
.end method

.method public static getSkipRegistration()Z
    .registers 1

    #@0
    .prologue
    .line 2627
    sget-boolean v0, Lcom/android/internal/telephony/CallManager;->mSkipRegistration:Z

    #@2
    return v0
.end method

.method public static hangupFakeCall()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 2653
    invoke-static {v1}, Lcom/android/internal/telephony/CallManager;->isKeepingFakeCall(Z)Z

    #@4
    move-result v0

    #@5
    .line 2654
    .local v0, curStatus:Z
    sget-object v2, Lcom/android/internal/telephony/CallManager;->mIPPhoneProxy:Lcom/movial/ipphone/IPPhoneProxy;

    #@7
    if-nez v0, :cond_d

    #@9
    :goto_9
    invoke-virtual {v2, v1}, Lcom/movial/ipphone/IPPhoneProxy;->hangupFakeCall(Z)V

    #@c
    .line 2655
    return-void

    #@d
    .line 2654
    :cond_d
    const/4 v1, 0x0

    #@e
    goto :goto_9
.end method

.method private hasActiveCall(Lcom/android/internal/telephony/Phone;)Z
    .registers 7
    .parameter "phone"

    #@0
    .prologue
    .line 538
    const/4 v1, 0x0

    #@1
    .line 539
    .local v1, ret:Z
    iget-object v2, p0, Lcom/android/internal/telephony/CallManager;->mForegroundCalls:Ljava/util/ArrayList;

    #@3
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/CallManager;->getFirstActiveCall(Ljava/util/ArrayList;)Lcom/android/internal/telephony/Call;

    #@6
    move-result-object v0

    #@7
    .line 540
    .local v0, call:Lcom/android/internal/telephony/Call;
    if-eqz v0, :cond_37

    #@9
    invoke-virtual {v0}, Lcom/android/internal/telephony/Call;->getPhone()Lcom/android/internal/telephony/Phone;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@10
    move-result v2

    #@11
    if-eqz v2, :cond_37

    #@13
    const/4 v1, 0x1

    #@14
    .line 541
    :goto_14
    const-string v2, "CallManager"

    #@16
    new-instance v3, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v4, "hasActiveCall("

    #@1d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v3

    #@25
    const-string v4, "): "

    #@27
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v3

    #@2f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v3

    #@33
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    .line 542
    return v1

    #@37
    .line 540
    :cond_37
    const/4 v1, 0x0

    #@38
    goto :goto_14
.end method

.method private hasHoldingCall(Lcom/android/internal/telephony/Phone;)Z
    .registers 8
    .parameter "phone"

    #@0
    .prologue
    .line 546
    const/4 v2, 0x0

    #@1
    .line 548
    .local v2, ret:Z
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getBackgroundCalls()Ljava/util/List;

    #@4
    move-result-object v3

    #@5
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@8
    move-result-object v1

    #@9
    .local v1, i$:Ljava/util/Iterator;
    :cond_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@c
    move-result v3

    #@d
    if-eqz v3, :cond_26

    #@f
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@12
    move-result-object v0

    #@13
    check-cast v0, Lcom/android/internal/telephony/Call;

    #@15
    .line 549
    .local v0, call:Lcom/android/internal/telephony/Call;
    invoke-virtual {v0}, Lcom/android/internal/telephony/Call;->getPhone()Lcom/android/internal/telephony/Phone;

    #@18
    move-result-object v3

    #@19
    invoke-virtual {v3, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@1c
    move-result v3

    #@1d
    if-eqz v3, :cond_9

    #@1f
    .line 550
    invoke-virtual {v0}, Lcom/android/internal/telephony/Call;->isIdle()Z

    #@22
    move-result v3

    #@23
    if-nez v3, :cond_49

    #@25
    const/4 v2, 0x1

    #@26
    .line 554
    .end local v0           #call:Lcom/android/internal/telephony/Call;
    :cond_26
    :goto_26
    const-string v3, "CallManager"

    #@28
    new-instance v4, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v5, "hasHoldingCall("

    #@2f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v4

    #@33
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v4

    #@37
    const-string v5, "): "

    #@39
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v4

    #@3d
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@40
    move-result-object v4

    #@41
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v4

    #@45
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    .line 555
    return v2

    #@49
    .line 550
    .restart local v0       #call:Lcom/android/internal/telephony/Call;
    :cond_49
    const/4 v2, 0x0

    #@4a
    goto :goto_26
.end method

.method private hasMoreThanOneRingingCall()Z
    .registers 6

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 2321
    const/4 v1, 0x0

    #@2
    .line 2322
    .local v1, count:I
    iget-object v4, p0, Lcom/android/internal/telephony/CallManager;->mRingingCalls:Ljava/util/ArrayList;

    #@4
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@7
    move-result-object v2

    #@8
    .local v2, i$:Ljava/util/Iterator;
    :cond_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@b
    move-result v4

    #@c
    if-eqz v4, :cond_23

    #@e
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@11
    move-result-object v0

    #@12
    check-cast v0, Lcom/android/internal/telephony/Call;

    #@14
    .line 2323
    .local v0, call:Lcom/android/internal/telephony/Call;
    invoke-virtual {v0}, Lcom/android/internal/telephony/Call;->getState()Lcom/android/internal/telephony/Call$State;

    #@17
    move-result-object v4

    #@18
    invoke-virtual {v4}, Lcom/android/internal/telephony/Call$State;->isRinging()Z

    #@1b
    move-result v4

    #@1c
    if-eqz v4, :cond_8

    #@1e
    .line 2324
    add-int/lit8 v1, v1, 0x1

    #@20
    if-le v1, v3, :cond_8

    #@22
    .line 2327
    .end local v0           #call:Lcom/android/internal/telephony/Call;
    :goto_22
    return v3

    #@23
    :cond_23
    const/4 v3, 0x0

    #@24
    goto :goto_22
.end method

.method private inCallAudioModeForPhone(Lcom/android/internal/telephony/Phone;)I
    .registers 12
    .parameter "phone"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    .line 559
    const/4 v3, 0x0

    #@2
    .line 560
    .local v3, ret:I
    const/4 v4, 0x0

    #@3
    .line 561
    .local v4, sub:I
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/CallManager;->hasActiveCall(Lcom/android/internal/telephony/Phone;)Z

    #@6
    move-result v0

    #@7
    .line 562
    .local v0, hasActiveCall:Z
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/CallManager;->hasHoldingCall(Lcom/android/internal/telephony/Phone;)Z

    #@a
    move-result v1

    #@b
    .line 563
    .local v1, hasHoldingCall:Z
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getFgPhone()Lcom/android/internal/telephony/Phone;

    #@e
    move-result-object v7

    #@f
    invoke-virtual {v7, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@12
    move-result v2

    #@13
    .line 564
    .local v2, isFgPhone:Z
    const-string v7, "persist.radio.voice.modem.index"

    #@15
    const/4 v8, 0x0

    #@16
    invoke-static {v7, v8}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@19
    move-result v5

    #@1a
    .line 567
    .local v5, voiceModemIndex:I
    const-string v7, "CallManager"

    #@1c
    new-instance v8, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v9, "inCallAudioModeForPhone( "

    #@23
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v8

    #@27
    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v8

    #@2b
    const-string v9, " ): phoneState: "

    #@2d
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v8

    #@31
    invoke-interface {p1}, Lcom/android/internal/telephony/Phone;->getState()Lcom/android/internal/telephony/PhoneConstants$State;

    #@34
    move-result-object v9

    #@35
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v8

    #@39
    const-string v9, " hasActiveCall: "

    #@3b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v8

    #@3f
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@42
    move-result-object v8

    #@43
    const-string v9, " hasHoldingCall: "

    #@45
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v8

    #@49
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v8

    #@4d
    const-string v9, " isFgPhone: "

    #@4f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v8

    #@53
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@56
    move-result-object v8

    #@57
    const-string v9, " voiceModemIndex: "

    #@59
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v8

    #@5d
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@60
    move-result-object v8

    #@61
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v8

    #@65
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@68
    .line 572
    invoke-interface {p1}, Lcom/android/internal/telephony/Phone;->getSubscription()I

    #@6b
    move-result v4

    #@6c
    .line 573
    invoke-interface {p1}, Lcom/android/internal/telephony/Phone;->getState()Lcom/android/internal/telephony/PhoneConstants$State;

    #@6f
    move-result-object v7

    #@70
    sget-object v8, Lcom/android/internal/telephony/PhoneConstants$State;->OFFHOOK:Lcom/android/internal/telephony/PhoneConstants$State;

    #@72
    if-ne v7, v8, :cond_83

    #@74
    .line 574
    if-eqz v2, :cond_96

    #@76
    if-eqz v0, :cond_96

    #@78
    .line 575
    invoke-interface {p1}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    #@7b
    move-result v7

    #@7c
    packed-switch v7, :pswitch_data_b6

    #@7f
    .line 583
    if-eqz v5, :cond_8f

    #@81
    .line 584
    const/16 v3, 0x100

    #@83
    .line 609
    :cond_83
    :goto_83
    return v3

    #@84
    .line 577
    :pswitch_84
    const-string v6, "CallManager"

    #@86
    const-string v7, "inCallAudioModeForPhone is meaningless for SIP"

    #@88
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@8b
    goto :goto_83

    #@8c
    .line 580
    :pswitch_8c
    const/16 v3, 0x10

    #@8e
    .line 581
    goto :goto_83

    #@8f
    .line 586
    :cond_8f
    if-ne v4, v6, :cond_94

    #@91
    const/16 v3, 0x100

    #@93
    :goto_93
    goto :goto_83

    #@94
    :cond_94
    move v3, v6

    #@95
    goto :goto_93

    #@96
    .line 590
    :cond_96
    if-eqz v1, :cond_83

    #@98
    .line 591
    invoke-interface {p1}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    #@9b
    move-result v7

    #@9c
    packed-switch v7, :pswitch_data_be

    #@9f
    .line 599
    if-eqz v5, :cond_af

    #@a1
    .line 600
    const/16 v3, 0x200

    #@a3
    goto :goto_83

    #@a4
    .line 593
    :pswitch_a4
    const-string v6, "CallManager"

    #@a6
    const-string v7, "inCallAudioModeForPhone is meaningless for SIP"

    #@a8
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@ab
    goto :goto_83

    #@ac
    .line 596
    :pswitch_ac
    const/16 v3, 0x20

    #@ae
    .line 597
    goto :goto_83

    #@af
    .line 602
    :cond_af
    if-ne v4, v6, :cond_b4

    #@b1
    const/16 v3, 0x200

    #@b3
    :goto_b3
    goto :goto_83

    #@b4
    :cond_b4
    const/4 v3, 0x2

    #@b5
    goto :goto_b3

    #@b6
    .line 575
    :pswitch_data_b6
    .packed-switch 0x3
        :pswitch_84
        :pswitch_8c
    .end packed-switch

    #@be
    .line 591
    :pswitch_data_be
    .packed-switch 0x3
        :pswitch_a4
        :pswitch_ac
    .end packed-switch
.end method

.method private inCallModeToString(I)Ljava/lang/String;
    .registers 7
    .parameter "inCallMode"

    #@0
    .prologue
    .line 614
    const-string v2, "["

    #@2
    .line 615
    .local v2, ret:Ljava/lang/String;
    const/4 v0, 0x0

    #@3
    .line 617
    .local v0, addOper:Z
    const/4 v1, 0x7

    #@4
    .local v1, i:I
    :goto_4
    if-ltz v1, :cond_39

    #@6
    .line 618
    const/4 v3, 0x1

    #@7
    shl-int/2addr v3, v1

    #@8
    and-int/2addr v3, p1

    #@9
    if-eqz v3, :cond_36

    #@b
    .line 619
    if-eqz v0, :cond_20

    #@d
    new-instance v3, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v3

    #@16
    const-string v4, "|"

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v2

    #@20
    .line 620
    :cond_20
    new-instance v3, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v3

    #@29
    sget-object v4, Lcom/android/internal/telephony/CallManager;->MODE2DDESCRIPTION:[Ljava/lang/String;

    #@2b
    aget-object v4, v4, v1

    #@2d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v3

    #@31
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v2

    #@35
    .line 621
    const/4 v0, 0x1

    #@36
    .line 617
    :cond_36
    add-int/lit8 v1, v1, -0x1

    #@38
    goto :goto_4

    #@39
    .line 624
    :cond_39
    new-instance v3, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v3

    #@42
    const-string v4, "]"

    #@44
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v3

    #@48
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v2

    #@4c
    .line 625
    return-object v2
.end method

.method public static isCallOnImsEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 310
    const-string v0, "persist.radio.calls.on.ims"

    #@2
    const/4 v1, 0x0

    #@3
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@6
    move-result v0

    #@7
    return v0
.end method

.method private isInCallModeActive(I)Z
    .registers 9
    .parameter "inCallMode"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 525
    const/4 v1, 0x0

    #@3
    .line 527
    .local v1, ret:Z
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getFgPhone()Lcom/android/internal/telephony/Phone;

    #@6
    move-result-object v0

    #@7
    .line 528
    .local v0, fg:Lcom/android/internal/telephony/Phone;
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    #@a
    move-result v5

    #@b
    const/4 v6, 0x3

    #@c
    if-ne v5, v6, :cond_16

    #@e
    move v2, v3

    #@f
    .line 530
    .local v2, sipactive:Z
    :goto_f
    and-int/lit16 v5, p1, 0x333

    #@11
    if-eqz v5, :cond_18

    #@13
    if-nez v2, :cond_18

    #@15
    :goto_15
    return v3

    #@16
    .end local v2           #sipactive:Z
    :cond_16
    move v2, v4

    #@17
    .line 528
    goto :goto_f

    #@18
    .restart local v2       #sipactive:Z
    :cond_18
    move v3, v4

    #@19
    .line 530
    goto :goto_15
.end method

.method public static isKeepingFakeCall(Z)Z
    .registers 4
    .parameter "reset"

    #@0
    .prologue
    .line 2640
    const/4 v0, 0x0

    #@1
    .line 2641
    .local v0, prev:Z
    sget-object v2, Lcom/android/internal/telephony/CallManager;->syncObj:Ljava/lang/Object;

    #@3
    monitor-enter v2

    #@4
    .line 2642
    :try_start_4
    sget-boolean v0, Lcom/android/internal/telephony/CallManager;->mIsKeepingFakeCall:Z

    #@6
    .line 2643
    if-eqz p0, :cond_b

    #@8
    const/4 v1, 0x0

    #@9
    sput-boolean v1, Lcom/android/internal/telephony/CallManager;->mIsKeepingFakeCall:Z

    #@b
    .line 2644
    :cond_b
    monitor-exit v2

    #@c
    .line 2645
    return v0

    #@d
    .line 2644
    :catchall_d
    move-exception v1

    #@e
    monitor-exit v2
    :try_end_f
    .catchall {:try_start_4 .. :try_end_f} :catchall_d

    #@f
    throw v1
.end method

.method public static isNormalPowerOnReq()Z
    .registers 1

    #@0
    .prologue
    .line 2623
    sget-boolean v0, Lcom/android/internal/telephony/CallManager;->mIsNormalPowerOnReq:Z

    #@2
    return v0
.end method

.method public static isSamePhone(Lcom/android/internal/telephony/Phone;Lcom/android/internal/telephony/Phone;)Z
    .registers 4
    .parameter "p1"
    .parameter "p2"

    #@0
    .prologue
    .line 303
    invoke-static {p0}, Lcom/android/internal/telephony/CallManager;->getPhoneBase(Lcom/android/internal/telephony/Phone;)Lcom/android/internal/telephony/Phone;

    #@3
    move-result-object v0

    #@4
    invoke-static {p1}, Lcom/android/internal/telephony/CallManager;->getPhoneBase(Lcom/android/internal/telephony/Phone;)Lcom/android/internal/telephony/Phone;

    #@7
    move-result-object v1

    #@8
    if-ne v0, v1, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method private phoneAudioModeForSipPhone(ILcom/android/internal/telephony/sip/SipPhone;)I
    .registers 6
    .parameter "mode"
    .parameter "phone"

    #@0
    .prologue
    .line 514
    const/4 v0, 0x0

    #@1
    .line 516
    .local v0, ret:I
    invoke-virtual {p2}, Lcom/android/internal/telephony/sip/SipPhone;->getState()Lcom/android/internal/telephony/PhoneConstants$State;

    #@4
    move-result-object v1

    #@5
    sget-object v2, Lcom/android/internal/telephony/PhoneConstants$State;->OFFHOOK:Lcom/android/internal/telephony/PhoneConstants$State;

    #@7
    if-eq v1, v2, :cond_c

    #@9
    const/4 v1, 0x3

    #@a
    if-ne p1, v1, :cond_d

    #@c
    .line 518
    :cond_c
    const/4 v0, 0x3

    #@d
    .line 521
    :cond_d
    return v0
.end method

.method private registerForPhoneStates(Lcom/android/internal/telephony/Phone;)V
    .registers 8
    .parameter "phone"

    #@0
    .prologue
    const/16 v5, 0x73

    #@2
    const/4 v4, 0x2

    #@3
    const/4 v3, 0x1

    #@4
    const/4 v2, 0x0

    #@5
    .line 784
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@7
    const/16 v1, 0x65

    #@9
    invoke-interface {p1, v0, v1, v2}, Lcom/android/internal/telephony/Phone;->registerForPreciseCallStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@c
    .line 785
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@e
    const/16 v1, 0x64

    #@10
    invoke-interface {p1, v0, v1, v2}, Lcom/android/internal/telephony/Phone;->registerForDisconnect(Landroid/os/Handler;ILjava/lang/Object;)V

    #@13
    .line 786
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@15
    const/16 v1, 0x66

    #@17
    invoke-interface {p1, v0, v1, v2}, Lcom/android/internal/telephony/Phone;->registerForNewRingingConnection(Landroid/os/Handler;ILjava/lang/Object;)V

    #@1a
    .line 787
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@1c
    const/16 v1, 0x67

    #@1e
    invoke-interface {p1, v0, v1, v2}, Lcom/android/internal/telephony/Phone;->registerForUnknownConnection(Landroid/os/Handler;ILjava/lang/Object;)V

    #@21
    .line 788
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@23
    const/16 v1, 0x68

    #@25
    invoke-interface {p1, v0, v1, v2}, Lcom/android/internal/telephony/Phone;->registerForIncomingRing(Landroid/os/Handler;ILjava/lang/Object;)V

    #@28
    .line 789
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@2a
    const/16 v1, 0x69

    #@2c
    invoke-interface {p1, v0, v1, v2}, Lcom/android/internal/telephony/Phone;->registerForRingbackTone(Landroid/os/Handler;ILjava/lang/Object;)V

    #@2f
    .line 790
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@31
    const/16 v1, 0x6a

    #@33
    invoke-interface {p1, v0, v1, v2}, Lcom/android/internal/telephony/Phone;->registerForInCallVoicePrivacyOn(Landroid/os/Handler;ILjava/lang/Object;)V

    #@36
    .line 791
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@38
    const/16 v1, 0x6b

    #@3a
    invoke-interface {p1, v0, v1, v2}, Lcom/android/internal/telephony/Phone;->registerForInCallVoicePrivacyOff(Landroid/os/Handler;ILjava/lang/Object;)V

    #@3d
    .line 792
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@3f
    const/16 v1, 0x6d

    #@41
    invoke-interface {p1, v0, v1, v2}, Lcom/android/internal/telephony/Phone;->registerForDisplayInfo(Landroid/os/Handler;ILjava/lang/Object;)V

    #@44
    .line 793
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@46
    const/16 v1, 0x6e

    #@48
    invoke-interface {p1, v0, v1, v2}, Lcom/android/internal/telephony/Phone;->registerForSignalInfo(Landroid/os/Handler;ILjava/lang/Object;)V

    #@4b
    .line 794
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@4d
    const/16 v1, 0x70

    #@4f
    invoke-interface {p1, v0, v1, v2}, Lcom/android/internal/telephony/Phone;->registerForResendIncallMute(Landroid/os/Handler;ILjava/lang/Object;)V

    #@52
    .line 795
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@54
    const/16 v1, 0x71

    #@56
    invoke-interface {p1, v0, v1, p1}, Lcom/android/internal/telephony/Phone;->registerForMmiInitiate(Landroid/os/Handler;ILjava/lang/Object;)V

    #@59
    .line 796
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@5b
    const/16 v1, 0x72

    #@5d
    invoke-interface {p1, v0, v1, p1}, Lcom/android/internal/telephony/Phone;->registerForMmiComplete(Landroid/os/Handler;ILjava/lang/Object;)V

    #@60
    .line 797
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@62
    const/16 v1, 0x75

    #@64
    invoke-interface {p1, v0, v1, v2}, Lcom/android/internal/telephony/Phone;->registerForSuppServiceFailed(Landroid/os/Handler;ILjava/lang/Object;)V

    #@67
    .line 798
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@69
    const/16 v1, 0x76

    #@6b
    invoke-interface {p1, v0, v1, v2}, Lcom/android/internal/telephony/Phone;->registerForServiceStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@6e
    .line 799
    invoke-interface {p1}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    #@71
    move-result v0

    #@72
    if-eq v0, v3, :cond_7b

    #@74
    invoke-interface {p1}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    #@77
    move-result v0

    #@78
    const/4 v1, 0x4

    #@79
    if-ne v0, v1, :cond_93

    #@7b
    .line 801
    :cond_7b
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@7d
    const/16 v1, 0x78

    #@7f
    invoke-interface {p1, v0, v1, v2}, Lcom/android/internal/telephony/Phone;->registerForSuppServiceNotification(Landroid/os/Handler;ILjava/lang/Object;)V

    #@82
    .line 804
    invoke-interface {p1}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@85
    move-result-object v0

    #@86
    const-string v1, "support_emergency_callback_mode_for_gsm"

    #@88
    invoke-static {v0, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@8b
    move-result v0

    #@8c
    if-eqz v0, :cond_93

    #@8e
    .line 805
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@90
    invoke-interface {p1, v0, v5, v2}, Lcom/android/internal/telephony/Phone;->registerForEcmTimerReset(Landroid/os/Handler;ILjava/lang/Object;)V

    #@93
    .line 811
    :cond_93
    invoke-interface {p1}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    #@96
    move-result v0

    #@97
    if-eq v0, v3, :cond_9f

    #@99
    invoke-interface {p1}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    #@9c
    move-result v0

    #@9d
    if-ne v0, v4, :cond_a6

    #@9f
    .line 813
    :cond_9f
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@a1
    const/16 v1, 0x77

    #@a3
    invoke-interface {p1, v0, v1, v2}, Lcom/android/internal/telephony/Phone;->setOnPostDialCharacter(Landroid/os/Handler;ILjava/lang/Object;)V

    #@a6
    .line 817
    :cond_a6
    invoke-interface {p1}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    #@a9
    move-result v0

    #@aa
    if-ne v0, v4, :cond_cd

    #@ac
    .line 818
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@ae
    const/16 v1, 0x6f

    #@b0
    invoke-interface {p1, v0, v1, v2}, Lcom/android/internal/telephony/Phone;->registerForCdmaOtaStatusChange(Landroid/os/Handler;ILjava/lang/Object;)V

    #@b3
    .line 819
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@b5
    const/16 v1, 0x74

    #@b7
    invoke-interface {p1, v0, v1, v2}, Lcom/android/internal/telephony/Phone;->registerForSubscriptionInfoReady(Landroid/os/Handler;ILjava/lang/Object;)V

    #@ba
    .line 820
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@bc
    const/16 v1, 0x6c

    #@be
    invoke-interface {p1, v0, v1, v2}, Lcom/android/internal/telephony/Phone;->registerForCallWaiting(Landroid/os/Handler;ILjava/lang/Object;)V

    #@c1
    .line 821
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@c3
    invoke-interface {p1, v0, v5, v2}, Lcom/android/internal/telephony/Phone;->registerForEcmTimerReset(Landroid/os/Handler;ILjava/lang/Object;)V

    #@c6
    .line 823
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@c8
    const/16 v1, 0x79

    #@ca
    invoke-interface {p1, v0, v1, v2}, Lcom/android/internal/telephony/Phone;->registerForRedirectedNumberInfo(Landroid/os/Handler;ILjava/lang/Object;)V

    #@cd
    .line 827
    :cond_cd
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@cf
    const/16 v1, 0x7d

    #@d1
    invoke-interface {p1, v0, v1, v2}, Lcom/android/internal/telephony/Phone;->registerLGECipheringInd(Landroid/os/Handler;ILjava/lang/Object;)V

    #@d4
    .line 829
    return-void
.end method

.method public static setKeepingFakeCall()V
    .registers 1

    #@0
    .prologue
    .line 2649
    const/4 v0, 0x1

    #@1
    sput-boolean v0, Lcom/android/internal/telephony/CallManager;->mIsKeepingFakeCall:Z

    #@3
    .line 2650
    return-void
.end method

.method public static setSkipRegistration(Z)V
    .registers 1
    .parameter "skip"

    #@0
    .prologue
    .line 2631
    sput-boolean p0, Lcom/android/internal/telephony/CallManager;->mSkipRegistration:Z

    #@2
    .line 2632
    return-void
.end method

.method private unregisterForPhoneStates(Lcom/android/internal/telephony/Phone;)V
    .registers 7
    .parameter "phone"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v3, 0x2

    #@2
    const/4 v2, 0x1

    #@3
    .line 833
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@5
    invoke-interface {p1, v0}, Lcom/android/internal/telephony/Phone;->unregisterForPreciseCallStateChanged(Landroid/os/Handler;)V

    #@8
    .line 834
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@a
    invoke-interface {p1, v0}, Lcom/android/internal/telephony/Phone;->unregisterForDisconnect(Landroid/os/Handler;)V

    #@d
    .line 835
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@f
    invoke-interface {p1, v0}, Lcom/android/internal/telephony/Phone;->unregisterForNewRingingConnection(Landroid/os/Handler;)V

    #@12
    .line 836
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@14
    invoke-interface {p1, v0}, Lcom/android/internal/telephony/Phone;->unregisterForUnknownConnection(Landroid/os/Handler;)V

    #@17
    .line 837
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@19
    invoke-interface {p1, v0}, Lcom/android/internal/telephony/Phone;->unregisterForIncomingRing(Landroid/os/Handler;)V

    #@1c
    .line 838
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@1e
    invoke-interface {p1, v0}, Lcom/android/internal/telephony/Phone;->unregisterForRingbackTone(Landroid/os/Handler;)V

    #@21
    .line 839
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@23
    invoke-interface {p1, v0}, Lcom/android/internal/telephony/Phone;->unregisterForInCallVoicePrivacyOn(Landroid/os/Handler;)V

    #@26
    .line 840
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@28
    invoke-interface {p1, v0}, Lcom/android/internal/telephony/Phone;->unregisterForInCallVoicePrivacyOff(Landroid/os/Handler;)V

    #@2b
    .line 841
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@2d
    invoke-interface {p1, v0}, Lcom/android/internal/telephony/Phone;->unregisterForDisplayInfo(Landroid/os/Handler;)V

    #@30
    .line 842
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@32
    invoke-interface {p1, v0}, Lcom/android/internal/telephony/Phone;->unregisterForSignalInfo(Landroid/os/Handler;)V

    #@35
    .line 843
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@37
    invoke-interface {p1, v0}, Lcom/android/internal/telephony/Phone;->unregisterForResendIncallMute(Landroid/os/Handler;)V

    #@3a
    .line 844
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@3c
    invoke-interface {p1, v0}, Lcom/android/internal/telephony/Phone;->unregisterForMmiInitiate(Landroid/os/Handler;)V

    #@3f
    .line 845
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@41
    invoke-interface {p1, v0}, Lcom/android/internal/telephony/Phone;->unregisterForMmiComplete(Landroid/os/Handler;)V

    #@44
    .line 846
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@46
    invoke-interface {p1, v0}, Lcom/android/internal/telephony/Phone;->unregisterForSuppServiceFailed(Landroid/os/Handler;)V

    #@49
    .line 847
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@4b
    invoke-interface {p1, v0}, Lcom/android/internal/telephony/Phone;->unregisterForSuppServiceNotification(Landroid/os/Handler;)V

    #@4e
    .line 848
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@50
    invoke-interface {p1, v0}, Lcom/android/internal/telephony/Phone;->unregisterForServiceStateChanged(Landroid/os/Handler;)V

    #@53
    .line 851
    invoke-interface {p1}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    #@56
    move-result v0

    #@57
    if-ne v0, v2, :cond_6a

    #@59
    .line 852
    invoke-interface {p1}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    #@5c
    move-result-object v0

    #@5d
    const-string v1, "support_emergency_callback_mode_for_gsm"

    #@5f
    invoke-static {v0, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@62
    move-result v0

    #@63
    if-eqz v0, :cond_6a

    #@65
    .line 853
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@67
    invoke-interface {p1, v0}, Lcom/android/internal/telephony/Phone;->unregisterForEcmTimerReset(Landroid/os/Handler;)V

    #@6a
    .line 859
    :cond_6a
    invoke-interface {p1}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    #@6d
    move-result v0

    #@6e
    if-eq v0, v2, :cond_76

    #@70
    invoke-interface {p1}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    #@73
    move-result v0

    #@74
    if-ne v0, v3, :cond_7b

    #@76
    .line 861
    :cond_76
    const/16 v0, 0x77

    #@78
    invoke-interface {p1, v4, v0, v4}, Lcom/android/internal/telephony/Phone;->setOnPostDialCharacter(Landroid/os/Handler;ILjava/lang/Object;)V

    #@7b
    .line 865
    :cond_7b
    invoke-interface {p1}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    #@7e
    move-result v0

    #@7f
    if-ne v0, v3, :cond_95

    #@81
    .line 866
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@83
    invoke-interface {p1, v0}, Lcom/android/internal/telephony/Phone;->unregisterForCdmaOtaStatusChange(Landroid/os/Handler;)V

    #@86
    .line 867
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@88
    invoke-interface {p1, v0}, Lcom/android/internal/telephony/Phone;->unregisterForSubscriptionInfoReady(Landroid/os/Handler;)V

    #@8b
    .line 868
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@8d
    invoke-interface {p1, v0}, Lcom/android/internal/telephony/Phone;->unregisterForCallWaiting(Landroid/os/Handler;)V

    #@90
    .line 869
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@92
    invoke-interface {p1, v0}, Lcom/android/internal/telephony/Phone;->unregisterForEcmTimerReset(Landroid/os/Handler;)V

    #@95
    .line 872
    :cond_95
    invoke-interface {p1}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    #@98
    move-result v0

    #@99
    const/4 v1, 0x4

    #@9a
    if-ne v0, v1, :cond_a6

    #@9c
    .line 873
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@9e
    invoke-interface {p1, v0}, Lcom/android/internal/telephony/Phone;->unregisterForEcmTimerReset(Landroid/os/Handler;)V

    #@a1
    .line 875
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@a3
    invoke-interface {p1, v0}, Lcom/android/internal/telephony/Phone;->unregisterForRedirectedNumberInfo(Landroid/os/Handler;)V

    #@a6
    .line 879
    :cond_a6
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@a8
    invoke-interface {p1, v0}, Lcom/android/internal/telephony/Phone;->unregisterLGECipheringInd(Landroid/os/Handler;)V

    #@ab
    .line 881
    return-void
.end method

.method private updateAudioFocus(Landroid/media/AudioManager;)V
    .registers 4
    .parameter "audioManager"

    #@0
    .prologue
    .line 762
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->hasActiveRingingCall()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_c

    #@6
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->hasActiveFgCall()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_11

    #@c
    .line 765
    :cond_c
    const/4 v0, 0x0

    #@d
    const/4 v1, 0x2

    #@e
    invoke-virtual {p1, v0, v1}, Landroid/media/AudioManager;->requestAudioFocusForCall(II)V

    #@11
    .line 775
    :cond_11
    return-void
.end method


# virtual methods
.method public acceptCall(Lcom/android/internal/telephony/Call;)V
    .registers 3
    .parameter "ringingCall"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 897
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0}, Lcom/android/internal/telephony/CallManager;->acceptCall(Lcom/android/internal/telephony/Call;I)V

    #@4
    .line 898
    return-void
.end method

.method public acceptCall(Lcom/android/internal/telephony/Call;I)V
    .registers 15
    .parameter "ringingCall"
    .parameter "callType"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v10, 0x3

    #@2
    const/4 v11, 0x2

    #@3
    const/4 v7, 0x1

    #@4
    .line 915
    invoke-virtual {p1}, Lcom/android/internal/telephony/Call;->getPhone()Lcom/android/internal/telephony/Phone;

    #@7
    move-result-object v5

    #@8
    .line 923
    .local v5, ringingPhone:Lcom/android/internal/telephony/Phone;
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->hasActiveFgCall()Z

    #@b
    move-result v8

    #@c
    if-eqz v8, :cond_2f

    #@e
    .line 924
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getActiveFgCall()Lcom/android/internal/telephony/Call;

    #@11
    move-result-object v8

    #@12
    invoke-virtual {v8}, Lcom/android/internal/telephony/Call;->getPhone()Lcom/android/internal/telephony/Phone;

    #@15
    move-result-object v0

    #@16
    .line 925
    .local v0, activePhone:Lcom/android/internal/telephony/Phone;
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getBackgroundCall()Lcom/android/internal/telephony/Call;

    #@19
    move-result-object v8

    #@1a
    invoke-virtual {v8}, Lcom/android/internal/telephony/Call;->isIdle()Z

    #@1d
    move-result v8

    #@1e
    if-nez v8, :cond_47

    #@20
    move v4, v7

    #@21
    .line 926
    .local v4, hasBgCall:Z
    :goto_21
    if-ne v0, v5, :cond_24

    #@23
    move v6, v7

    #@24
    .line 932
    .local v6, sameChannel:Z
    :cond_24
    if-eqz v6, :cond_49

    #@26
    if-eqz v4, :cond_49

    #@28
    .line 933
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getActiveFgCall()Lcom/android/internal/telephony/Call;

    #@2b
    move-result-object v8

    #@2c
    invoke-virtual {v8}, Lcom/android/internal/telephony/Call;->hangup()V

    #@2f
    .line 941
    .end local v0           #activePhone:Lcom/android/internal/telephony/Phone;
    .end local v4           #hasBgCall:Z
    .end local v6           #sameChannel:Z
    :cond_2f
    :goto_2f
    invoke-direct {p0}, Lcom/android/internal/telephony/CallManager;->getContext()Landroid/content/Context;

    #@32
    move-result-object v2

    #@33
    .line 942
    .local v2, context:Landroid/content/Context;
    if-nez v2, :cond_5d

    #@35
    .line 943
    const-string v7, "CallManager"

    #@37
    const-string v8, "Speedup Audio Path enhancement: Context is null"

    #@39
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3c
    .line 970
    :cond_3c
    :goto_3c
    invoke-interface {v5}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    #@3f
    move-result v7

    #@40
    const/4 v8, 0x4

    #@41
    if-ne v7, v8, :cond_dc

    #@43
    .line 971
    invoke-interface {v5, p2}, Lcom/android/internal/telephony/Phone;->acceptCall(I)V

    #@46
    .line 981
    :goto_46
    return-void

    #@47
    .end local v2           #context:Landroid/content/Context;
    .restart local v0       #activePhone:Lcom/android/internal/telephony/Phone;
    :cond_47
    move v4, v6

    #@48
    .line 925
    goto :goto_21

    #@49
    .line 934
    .restart local v4       #hasBgCall:Z
    .restart local v6       #sameChannel:Z
    :cond_49
    if-nez v6, :cond_51

    #@4b
    if-nez v4, :cond_51

    #@4d
    .line 935
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->switchHoldingAndActive()V

    #@50
    goto :goto_2f

    #@51
    .line 936
    :cond_51
    if-nez v6, :cond_2f

    #@53
    if-eqz v4, :cond_2f

    #@55
    .line 937
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getActiveFgCall()Lcom/android/internal/telephony/Call;

    #@58
    move-result-object v8

    #@59
    invoke-virtual {v8}, Lcom/android/internal/telephony/Call;->hangup()V

    #@5c
    goto :goto_2f

    #@5d
    .line 944
    .end local v0           #activePhone:Lcom/android/internal/telephony/Phone;
    .end local v4           #hasBgCall:Z
    .end local v6           #sameChannel:Z
    .restart local v2       #context:Landroid/content/Context;
    :cond_5d
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@60
    move-result-object v8

    #@61
    const v9, 0x111004d

    #@64
    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@67
    move-result v8

    #@68
    if-eqz v8, :cond_3c

    #@6a
    .line 946
    const-string v8, "CallManager"

    #@6c
    const-string v9, "Speedup Audio Path enhancement"

    #@6e
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@71
    .line 947
    const-string v8, "audio"

    #@73
    invoke-virtual {v2, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@76
    move-result-object v1

    #@77
    check-cast v1, Landroid/media/AudioManager;

    #@79
    .line 949
    .local v1, audioManager:Landroid/media/AudioManager;
    invoke-virtual {v1}, Landroid/media/AudioManager;->getMode()I

    #@7c
    move-result v3

    #@7d
    .line 950
    .local v3, currMode:I
    if-eq v3, v11, :cond_af

    #@7f
    instance-of v8, v5, Lcom/android/internal/telephony/sip/SipPhone;

    #@81
    if-nez v8, :cond_af

    #@83
    instance-of v8, v5, Lcom/movial/ipphone/IPPhone;

    #@85
    if-nez v8, :cond_af

    #@87
    .line 953
    const-string v8, "CallManager"

    #@89
    new-instance v9, Ljava/lang/StringBuilder;

    #@8b
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@8e
    const-string v10, "setAudioMode Setting audio mode from "

    #@90
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@93
    move-result-object v9

    #@94
    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@97
    move-result-object v9

    #@98
    const-string v10, " to "

    #@9a
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v9

    #@9e
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v9

    #@a2
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a5
    move-result-object v9

    #@a6
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a9
    .line 955
    invoke-virtual {v1, v11}, Landroid/media/AudioManager;->setMode(I)V

    #@ac
    .line 956
    iput-boolean v7, p0, Lcom/android/internal/telephony/CallManager;->mSpeedUpAudioForMtCall:Z

    #@ae
    goto :goto_3c

    #@af
    .line 961
    :cond_af
    if-eq v3, v10, :cond_3c

    #@b1
    instance-of v7, v5, Lcom/movial/ipphone/IPPhone;

    #@b3
    if-eqz v7, :cond_3c

    #@b5
    .line 963
    const-string v7, "CallManager"

    #@b7
    new-instance v8, Ljava/lang/StringBuilder;

    #@b9
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@bc
    const-string v9, "[LGE_WFC] wifi calling accept, setAudioMode "

    #@be
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c1
    move-result-object v8

    #@c2
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c5
    move-result-object v8

    #@c6
    const-string v9, " to "

    #@c8
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cb
    move-result-object v8

    #@cc
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@cf
    move-result-object v8

    #@d0
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d3
    move-result-object v8

    #@d4
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d7
    .line 965
    invoke-virtual {v1, v10}, Landroid/media/AudioManager;->setMode(I)V

    #@da
    goto/16 :goto_3c

    #@dc
    .line 973
    .end local v1           #audioManager:Landroid/media/AudioManager;
    .end local v3           #currMode:I
    :cond_dc
    invoke-interface {v5}, Lcom/android/internal/telephony/Phone;->acceptCall()V

    #@df
    goto/16 :goto_46
.end method

.method public canConference(Lcom/android/internal/telephony/Call;)Z
    .registers 7
    .parameter "heldCall"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1100
    const/4 v0, 0x0

    #@2
    .line 1101
    .local v0, activePhone:Lcom/android/internal/telephony/Phone;
    const/4 v1, 0x0

    #@3
    .line 1103
    .local v1, heldPhone:Lcom/android/internal/telephony/Phone;
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->hasActiveFgCall()Z

    #@6
    move-result v3

    #@7
    if-eqz v3, :cond_11

    #@9
    .line 1104
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getActiveFgCall()Lcom/android/internal/telephony/Call;

    #@c
    move-result-object v3

    #@d
    invoke-virtual {v3}, Lcom/android/internal/telephony/Call;->getPhone()Lcom/android/internal/telephony/Phone;

    #@10
    move-result-object v0

    #@11
    .line 1107
    :cond_11
    if-eqz p1, :cond_1b

    #@13
    .line 1108
    invoke-virtual {p1}, Lcom/android/internal/telephony/Call;->getPhone()Lcom/android/internal/telephony/Phone;

    #@16
    move-result-object v1

    #@17
    .line 1117
    if-eqz v0, :cond_1b

    #@19
    if-nez v1, :cond_1c

    #@1b
    .line 1126
    :cond_1b
    :goto_1b
    return v2

    #@1c
    .line 1120
    :cond_1c
    sget-boolean v3, Lcom/android/internal/telephony/CallManager;->mIsUsingIPPhone:Z

    #@1e
    if-eqz v3, :cond_36

    #@20
    .line 1121
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@23
    move-result-object v3

    #@24
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@27
    move-result-object v4

    #@28
    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@2b
    move-result v3

    #@2c
    if-eqz v3, :cond_1b

    #@2e
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->canConference()Z

    #@31
    move-result v3

    #@32
    if-eqz v3, :cond_1b

    #@34
    const/4 v2, 0x1

    #@35
    goto :goto_1b

    #@36
    .line 1124
    :cond_36
    if-eqz v0, :cond_1b

    #@38
    if-eqz v1, :cond_1b

    #@3a
    .line 1126
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@3d
    move-result-object v2

    #@3e
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@41
    move-result-object v3

    #@42
    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@45
    move-result v2

    #@46
    goto :goto_1b
.end method

.method public canTransfer(Lcom/android/internal/telephony/Call;)Z
    .registers 5
    .parameter "heldCall"

    #@0
    .prologue
    .line 1363
    const/4 v0, 0x0

    #@1
    .line 1364
    .local v0, activePhone:Lcom/android/internal/telephony/Phone;
    const/4 v1, 0x0

    #@2
    .line 1366
    .local v1, heldPhone:Lcom/android/internal/telephony/Phone;
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->hasActiveFgCall()Z

    #@5
    move-result v2

    #@6
    if-eqz v2, :cond_10

    #@8
    .line 1367
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getActiveFgCall()Lcom/android/internal/telephony/Call;

    #@b
    move-result-object v2

    #@c
    invoke-virtual {v2}, Lcom/android/internal/telephony/Call;->getPhone()Lcom/android/internal/telephony/Phone;

    #@f
    move-result-object v0

    #@10
    .line 1370
    :cond_10
    if-eqz p1, :cond_16

    #@12
    .line 1371
    invoke-virtual {p1}, Lcom/android/internal/telephony/Call;->getPhone()Lcom/android/internal/telephony/Phone;

    #@15
    move-result-object v1

    #@16
    .line 1374
    :cond_16
    if-ne v1, v0, :cond_20

    #@18
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->canTransfer()Z

    #@1b
    move-result v2

    #@1c
    if-eqz v2, :cond_20

    #@1e
    const/4 v2, 0x1

    #@1f
    :goto_1f
    return v2

    #@20
    :cond_20
    const/4 v2, 0x0

    #@21
    goto :goto_1f
.end method

.method public clearDisconnected()V
    .registers 4

    #@0
    .prologue
    .line 1299
    iget-object v2, p0, Lcom/android/internal/telephony/CallManager;->mPhones:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v0

    #@6
    .local v0, i$:Ljava/util/Iterator;
    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_16

    #@c
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    check-cast v1, Lcom/android/internal/telephony/Phone;

    #@12
    .line 1300
    .local v1, phone:Lcom/android/internal/telephony/Phone;
    invoke-interface {v1}, Lcom/android/internal/telephony/Phone;->clearDisconnected()V

    #@15
    goto :goto_6

    #@16
    .line 1302
    .end local v1           #phone:Lcom/android/internal/telephony/Phone;
    :cond_16
    return-void
.end method

.method public conference(Lcom/android/internal/telephony/Call;)V
    .registers 5
    .parameter "heldCall"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1148
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getFgPhone()Lcom/android/internal/telephony/Phone;

    #@3
    move-result-object v0

    #@4
    .line 1149
    .local v0, fgPhone:Lcom/android/internal/telephony/Phone;
    instance-of v1, v0, Lcom/android/internal/telephony/sip/SipPhone;

    #@6
    if-eqz v1, :cond_e

    #@8
    .line 1150
    check-cast v0, Lcom/android/internal/telephony/sip/SipPhone;

    #@a
    .end local v0           #fgPhone:Lcom/android/internal/telephony/Phone;
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/sip/SipPhone;->conference(Lcom/android/internal/telephony/Call;)V

    #@d
    .line 1162
    :goto_d
    return-void

    #@e
    .line 1151
    .restart local v0       #fgPhone:Lcom/android/internal/telephony/Phone;
    :cond_e
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/CallManager;->canConference(Lcom/android/internal/telephony/Call;)Z

    #@11
    move-result v1

    #@12
    if-eqz v1, :cond_18

    #@14
    .line 1152
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->conference()V

    #@17
    goto :goto_d

    #@18
    .line 1154
    :cond_18
    new-instance v1, Lcom/android/internal/telephony/CallStateException;

    #@1a
    const-string v2, "Can\'t conference foreground and selected background call"

    #@1c
    invoke-direct {v1, v2}, Lcom/android/internal/telephony/CallStateException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v1
.end method

.method public dial(Lcom/android/internal/telephony/Phone;Ljava/lang/String;)Lcom/android/internal/telephony/Connection;
    .registers 5
    .parameter "phone"
    .parameter "dialString"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1175
    const/4 v0, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/android/internal/telephony/CallManager;->dial(Lcom/android/internal/telephony/Phone;Ljava/lang/String;I[Ljava/lang/String;)Lcom/android/internal/telephony/Connection;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public dial(Lcom/android/internal/telephony/Phone;Ljava/lang/String;I[Ljava/lang/String;)Lcom/android/internal/telephony/Connection;
    .registers 14
    .parameter "phone"
    .parameter "dialString"
    .parameter "callType"
    .parameter "extras"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 1195
    invoke-static {p1}, Lcom/android/internal/telephony/CallManager;->getPhoneBase(Lcom/android/internal/telephony/Phone;)Lcom/android/internal/telephony/Phone;

    #@5
    move-result-object v1

    #@6
    .line 1203
    .local v1, basePhone:Lcom/android/internal/telephony/Phone;
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->hasActiveFgCall()Z

    #@9
    move-result v6

    #@a
    if-eqz v6, :cond_65

    #@c
    .line 1204
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getActiveFgCall()Lcom/android/internal/telephony/Call;

    #@f
    move-result-object v6

    #@10
    invoke-virtual {v6}, Lcom/android/internal/telephony/Call;->getPhone()Lcom/android/internal/telephony/Phone;

    #@13
    move-result-object v0

    #@14
    .line 1205
    .local v0, activePhone:Lcom/android/internal/telephony/Phone;
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getBackgroundCall()Lcom/android/internal/telephony/Call;

    #@17
    move-result-object v6

    #@18
    invoke-virtual {v6}, Lcom/android/internal/telephony/Call;->isIdle()Z

    #@1b
    move-result v6

    #@1c
    if-nez v6, :cond_71

    #@1e
    move v2, v4

    #@1f
    .line 1208
    .local v2, hasBgCall:Z
    :goto_1f
    const-string v6, "CallManager"

    #@21
    new-instance v7, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    const-string v8, "hasBgCall: "

    #@28
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v7

    #@2c
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v7

    #@30
    const-string v8, " sameChannel:"

    #@32
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v7

    #@36
    if-ne v0, v1, :cond_73

    #@38
    :goto_38
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v4

    #@3c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v4

    #@40
    invoke-static {v6, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@43
    .line 1211
    sget-boolean v4, Lcom/android/internal/telephony/CallManager;->mIsUsingIPPhone:Z

    #@45
    if-eqz v4, :cond_53

    #@47
    instance-of v4, v1, Lcom/movial/ipphone/IPPhoneProxy;

    #@49
    if-eqz v4, :cond_53

    #@4b
    .line 1212
    invoke-interface {v1}, Lcom/android/internal/telephony/Phone;->getForegroundCall()Lcom/android/internal/telephony/Call;

    #@4e
    move-result-object v4

    #@4f
    invoke-virtual {v4}, Lcom/android/internal/telephony/Call;->getPhone()Lcom/android/internal/telephony/Phone;

    #@52
    move-result-object v1

    #@53
    .line 1216
    :cond_53
    if-eq v0, v1, :cond_65

    #@55
    .line 1217
    if-eqz v2, :cond_75

    #@57
    .line 1218
    const-string v4, "CallManager"

    #@59
    const-string v5, "Hangup"

    #@5b
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5e
    .line 1219
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getActiveFgCall()Lcom/android/internal/telephony/Call;

    #@61
    move-result-object v4

    #@62
    invoke-virtual {v4}, Lcom/android/internal/telephony/Call;->hangup()V

    #@65
    .line 1227
    .end local v0           #activePhone:Lcom/android/internal/telephony/Phone;
    .end local v2           #hasBgCall:Z
    :cond_65
    :goto_65
    invoke-interface {p1}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    #@68
    move-result v4

    #@69
    const/4 v5, 0x4

    #@6a
    if-ne v4, v5, :cond_80

    #@6c
    .line 1228
    invoke-interface {v1, p2, p3, p4}, Lcom/android/internal/telephony/Phone;->dial(Ljava/lang/String;I[Ljava/lang/String;)Lcom/android/internal/telephony/Connection;

    #@6f
    move-result-object v3

    #@70
    .line 1238
    .local v3, result:Lcom/android/internal/telephony/Connection;
    :goto_70
    return-object v3

    #@71
    .end local v3           #result:Lcom/android/internal/telephony/Connection;
    .restart local v0       #activePhone:Lcom/android/internal/telephony/Phone;
    :cond_71
    move v2, v5

    #@72
    .line 1205
    goto :goto_1f

    #@73
    .restart local v2       #hasBgCall:Z
    :cond_73
    move v4, v5

    #@74
    .line 1208
    goto :goto_38

    #@75
    .line 1221
    :cond_75
    const-string v4, "CallManager"

    #@77
    const-string v5, "Switch"

    #@79
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7c
    .line 1222
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->switchHoldingAndActive()V

    #@7f
    goto :goto_65

    #@80
    .line 1230
    .end local v0           #activePhone:Lcom/android/internal/telephony/Phone;
    .end local v2           #hasBgCall:Z
    :cond_80
    invoke-interface {v1, p2}, Lcom/android/internal/telephony/Phone;->dial(Ljava/lang/String;)Lcom/android/internal/telephony/Connection;

    #@83
    move-result-object v3

    #@84
    .restart local v3       #result:Lcom/android/internal/telephony/Connection;
    goto :goto_70
.end method

.method public dial(Lcom/android/internal/telephony/Phone;Ljava/lang/String;Lcom/android/internal/telephony/UUSInfo;)Lcom/android/internal/telephony/Connection;
    .registers 5
    .parameter "phone"
    .parameter "dialString"
    .parameter "uusInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1252
    invoke-interface {p1, p2, p3}, Lcom/android/internal/telephony/Phone;->dial(Ljava/lang/String;Lcom/android/internal/telephony/UUSInfo;)Lcom/android/internal/telephony/Connection;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public dial(Lcom/android/internal/telephony/Phone;Ljava/lang/String;Z)Lcom/android/internal/telephony/Connection;
    .registers 13
    .parameter "phone"
    .parameter "dialString"
    .parameter "subAddress"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 1257
    invoke-static {p1}, Lcom/android/internal/telephony/CallManager;->getPhoneBase(Lcom/android/internal/telephony/Phone;)Lcom/android/internal/telephony/Phone;

    #@5
    move-result-object v1

    #@6
    .line 1265
    .local v1, basePhone:Lcom/android/internal/telephony/Phone;
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->hasActiveFgCall()Z

    #@9
    move-result v6

    #@a
    if-eqz v6, :cond_55

    #@c
    .line 1266
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getActiveFgCall()Lcom/android/internal/telephony/Call;

    #@f
    move-result-object v6

    #@10
    invoke-virtual {v6}, Lcom/android/internal/telephony/Call;->getPhone()Lcom/android/internal/telephony/Phone;

    #@13
    move-result-object v0

    #@14
    .line 1267
    .local v0, activePhone:Lcom/android/internal/telephony/Phone;
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getBackgroundCall()Lcom/android/internal/telephony/Call;

    #@17
    move-result-object v6

    #@18
    invoke-virtual {v6}, Lcom/android/internal/telephony/Call;->isIdle()Z

    #@1b
    move-result v6

    #@1c
    if-nez v6, :cond_5a

    #@1e
    move v2, v4

    #@1f
    .line 1270
    .local v2, hasBgCall:Z
    :goto_1f
    const-string v6, "CallManager"

    #@21
    new-instance v7, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    const-string v8, "hasBgCall: "

    #@28
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v7

    #@2c
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v7

    #@30
    const-string v8, " sameChannel:"

    #@32
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v7

    #@36
    if-ne v0, v1, :cond_5c

    #@38
    :goto_38
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v4

    #@3c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v4

    #@40
    invoke-static {v6, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@43
    .line 1273
    if-eq v0, v1, :cond_55

    #@45
    .line 1274
    if-eqz v2, :cond_5e

    #@47
    .line 1275
    const-string v4, "CallManager"

    #@49
    const-string v5, "Hangup"

    #@4b
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    .line 1276
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getActiveFgCall()Lcom/android/internal/telephony/Call;

    #@51
    move-result-object v4

    #@52
    invoke-virtual {v4}, Lcom/android/internal/telephony/Call;->hangup()V

    #@55
    .line 1284
    .end local v0           #activePhone:Lcom/android/internal/telephony/Phone;
    .end local v2           #hasBgCall:Z
    :cond_55
    :goto_55
    invoke-interface {v1, p2, p3}, Lcom/android/internal/telephony/Phone;->dial(Ljava/lang/String;Z)Lcom/android/internal/telephony/Connection;

    #@58
    move-result-object v3

    #@59
    .line 1291
    .local v3, result:Lcom/android/internal/telephony/Connection;
    return-object v3

    #@5a
    .end local v3           #result:Lcom/android/internal/telephony/Connection;
    .restart local v0       #activePhone:Lcom/android/internal/telephony/Phone;
    :cond_5a
    move v2, v5

    #@5b
    .line 1267
    goto :goto_1f

    #@5c
    .restart local v2       #hasBgCall:Z
    :cond_5c
    move v4, v5

    #@5d
    .line 1270
    goto :goto_38

    #@5e
    .line 1278
    :cond_5e
    const-string v4, "CallManager"

    #@60
    const-string v5, "Switch"

    #@62
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@65
    .line 1279
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->switchHoldingAndActive()V

    #@68
    goto :goto_55
.end method

.method public explicitCallTransfer(Lcom/android/internal/telephony/Call;)V
    .registers 3
    .parameter "heldCall"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1395
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/CallManager;->canTransfer(Lcom/android/internal/telephony/Call;)Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_d

    #@6
    .line 1396
    invoke-virtual {p1}, Lcom/android/internal/telephony/Call;->getPhone()Lcom/android/internal/telephony/Phone;

    #@9
    move-result-object v0

    #@a
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->explicitCallTransfer()V

    #@d
    .line 1404
    :cond_d
    return-void
.end method

.method public getActiveFgCall()Lcom/android/internal/telephony/Call;
    .registers 3

    #@0
    .prologue
    .line 2146
    sget-boolean v1, Lcom/android/internal/telephony/CallManager;->mIsUsingIPPhone:Z

    #@2
    if-eqz v1, :cond_e

    #@4
    .line 2147
    const/4 v1, 0x0

    #@5
    invoke-static {v1}, Lcom/android/internal/telephony/CallManager;->isKeepingFakeCall(Z)Z

    #@8
    move-result v1

    #@9
    if-eqz v1, :cond_e

    #@b
    .line 2148
    sget-object v0, Lcom/android/internal/telephony/gsm/GsmCallTracker;->mFakeCall:Lcom/android/internal/telephony/gsm/GsmCall;

    #@d
    .line 2159
    :cond_d
    :goto_d
    return-object v0

    #@e
    .line 2153
    :cond_e
    iget-object v1, p0, Lcom/android/internal/telephony/CallManager;->mForegroundCalls:Ljava/util/ArrayList;

    #@10
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/CallManager;->getFirstNonIdleCall(Ljava/util/List;)Lcom/android/internal/telephony/Call;

    #@13
    move-result-object v0

    #@14
    .line 2154
    .local v0, call:Lcom/android/internal/telephony/Call;
    if-nez v0, :cond_d

    #@16
    .line 2155
    iget-object v1, p0, Lcom/android/internal/telephony/CallManager;->mDefaultPhone:Lcom/android/internal/telephony/Phone;

    #@18
    if-nez v1, :cond_1c

    #@1a
    const/4 v0, 0x0

    #@1b
    :goto_1b
    goto :goto_d

    #@1c
    :cond_1c
    iget-object v1, p0, Lcom/android/internal/telephony/CallManager;->mDefaultPhone:Lcom/android/internal/telephony/Phone;

    #@1e
    invoke-interface {v1}, Lcom/android/internal/telephony/Phone;->getForegroundCall()Lcom/android/internal/telephony/Call;

    #@21
    move-result-object v0

    #@22
    goto :goto_1b
.end method

.method public getActiveFgCallState()Lcom/android/internal/telephony/Call$State;
    .registers 3

    #@0
    .prologue
    .line 2228
    sget-boolean v1, Lcom/android/internal/telephony/CallManager;->mIsUsingIPPhone:Z

    #@2
    if-eqz v1, :cond_e

    #@4
    .line 2229
    const/4 v1, 0x0

    #@5
    invoke-static {v1}, Lcom/android/internal/telephony/CallManager;->isKeepingFakeCall(Z)Z

    #@8
    move-result v1

    #@9
    if-eqz v1, :cond_e

    #@b
    .line 2230
    sget-object v1, Lcom/android/internal/telephony/Call$State;->DIALING:Lcom/android/internal/telephony/Call$State;

    #@d
    .line 2242
    :goto_d
    return-object v1

    #@e
    .line 2236
    :cond_e
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getActiveFgCall()Lcom/android/internal/telephony/Call;

    #@11
    move-result-object v0

    #@12
    .line 2238
    .local v0, fgCall:Lcom/android/internal/telephony/Call;
    if-eqz v0, :cond_19

    #@14
    .line 2239
    invoke-virtual {v0}, Lcom/android/internal/telephony/Call;->getState()Lcom/android/internal/telephony/Call$State;

    #@17
    move-result-object v1

    #@18
    goto :goto_d

    #@19
    .line 2242
    :cond_19
    sget-object v1, Lcom/android/internal/telephony/Call$State;->IDLE:Lcom/android/internal/telephony/Call$State;

    #@1b
    goto :goto_d
.end method

.method public getAllPhones()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/Phone;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 319
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mPhones:Ljava/util/ArrayList;

    #@2
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getBackgroundCalls()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/Call;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 2099
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mBackgroundCalls:Ljava/util/ArrayList;

    #@2
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getBgCallConnections()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/Connection;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 2262
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getFirstActiveBgCall()Lcom/android/internal/telephony/Call;

    #@3
    move-result-object v0

    #@4
    .line 2263
    .local v0, bgCall:Lcom/android/internal/telephony/Call;
    if-eqz v0, :cond_b

    #@6
    .line 2264
    invoke-virtual {v0}, Lcom/android/internal/telephony/Call;->getConnections()Ljava/util/List;

    #@9
    move-result-object v1

    #@a
    .line 2266
    :goto_a
    return-object v1

    #@b
    :cond_b
    iget-object v1, p0, Lcom/android/internal/telephony/CallManager;->emptyConnections:Ljava/util/ArrayList;

    #@d
    goto :goto_a
.end method

.method public getBgPhone()Lcom/android/internal/telephony/Phone;
    .registers 2

    #@0
    .prologue
    .line 478
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getFirstActiveBgCall()Lcom/android/internal/telephony/Call;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Lcom/android/internal/telephony/Call;->getPhone()Lcom/android/internal/telephony/Phone;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public getDefaultPhone()Lcom/android/internal/telephony/Phone;
    .registers 2

    #@0
    .prologue
    .line 464
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mDefaultPhone:Lcom/android/internal/telephony/Phone;

    #@2
    return-object v0
.end method

.method public getFgCallConnections()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/Connection;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 2250
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getActiveFgCall()Lcom/android/internal/telephony/Call;

    #@3
    move-result-object v0

    #@4
    .line 2251
    .local v0, fgCall:Lcom/android/internal/telephony/Call;
    if-eqz v0, :cond_b

    #@6
    .line 2252
    invoke-virtual {v0}, Lcom/android/internal/telephony/Call;->getConnections()Ljava/util/List;

    #@9
    move-result-object v1

    #@a
    .line 2254
    :goto_a
    return-object v1

    #@b
    :cond_b
    iget-object v1, p0, Lcom/android/internal/telephony/CallManager;->emptyConnections:Ljava/util/ArrayList;

    #@d
    goto :goto_a
.end method

.method public getFgCallLatestConnection()Lcom/android/internal/telephony/Connection;
    .registers 3

    #@0
    .prologue
    .line 2274
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getActiveFgCall()Lcom/android/internal/telephony/Call;

    #@3
    move-result-object v0

    #@4
    .line 2275
    .local v0, fgCall:Lcom/android/internal/telephony/Call;
    if-eqz v0, :cond_b

    #@6
    .line 2276
    invoke-virtual {v0}, Lcom/android/internal/telephony/Call;->getLatestConnection()Lcom/android/internal/telephony/Connection;

    #@9
    move-result-object v1

    #@a
    .line 2278
    :goto_a
    return-object v1

    #@b
    :cond_b
    const/4 v1, 0x0

    #@c
    goto :goto_a
.end method

.method public getFgPhone()Lcom/android/internal/telephony/Phone;
    .registers 2

    #@0
    .prologue
    .line 471
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getActiveFgCall()Lcom/android/internal/telephony/Call;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Lcom/android/internal/telephony/Call;->getPhone()Lcom/android/internal/telephony/Phone;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public getFirstActiveBgCall()Lcom/android/internal/telephony/Call;
    .registers 3

    #@0
    .prologue
    .line 2190
    iget-object v1, p0, Lcom/android/internal/telephony/CallManager;->mBackgroundCalls:Ljava/util/ArrayList;

    #@2
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/CallManager;->getFirstNonIdleCall(Ljava/util/List;)Lcom/android/internal/telephony/Call;

    #@5
    move-result-object v0

    #@6
    .line 2191
    .local v0, call:Lcom/android/internal/telephony/Call;
    if-nez v0, :cond_d

    #@8
    .line 2192
    iget-object v1, p0, Lcom/android/internal/telephony/CallManager;->mDefaultPhone:Lcom/android/internal/telephony/Phone;

    #@a
    if-nez v1, :cond_e

    #@c
    const/4 v0, 0x0

    #@d
    .line 2196
    :cond_d
    :goto_d
    return-object v0

    #@e
    .line 2192
    :cond_e
    iget-object v1, p0, Lcom/android/internal/telephony/CallManager;->mDefaultPhone:Lcom/android/internal/telephony/Phone;

    #@10
    invoke-interface {v1}, Lcom/android/internal/telephony/Phone;->getBackgroundCall()Lcom/android/internal/telephony/Call;

    #@13
    move-result-object v0

    #@14
    goto :goto_d
.end method

.method public getFirstActiveRingingCall()Lcom/android/internal/telephony/Call;
    .registers 3

    #@0
    .prologue
    .line 2213
    iget-object v1, p0, Lcom/android/internal/telephony/CallManager;->mRingingCalls:Ljava/util/ArrayList;

    #@2
    invoke-direct {p0, v1}, Lcom/android/internal/telephony/CallManager;->getFirstNonIdleCall(Ljava/util/List;)Lcom/android/internal/telephony/Call;

    #@5
    move-result-object v0

    #@6
    .line 2214
    .local v0, call:Lcom/android/internal/telephony/Call;
    if-nez v0, :cond_d

    #@8
    .line 2215
    iget-object v1, p0, Lcom/android/internal/telephony/CallManager;->mDefaultPhone:Lcom/android/internal/telephony/Phone;

    #@a
    if-nez v1, :cond_e

    #@c
    const/4 v0, 0x0

    #@d
    .line 2219
    :cond_d
    :goto_d
    return-object v0

    #@e
    .line 2215
    :cond_e
    iget-object v1, p0, Lcom/android/internal/telephony/CallManager;->mDefaultPhone:Lcom/android/internal/telephony/Phone;

    #@10
    invoke-interface {v1}, Lcom/android/internal/telephony/Phone;->getRingingCall()Lcom/android/internal/telephony/Call;

    #@13
    move-result-object v0

    #@14
    goto :goto_d
.end method

.method public getForegroundCalls()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/Call;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 2092
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mForegroundCalls:Ljava/util/ArrayList;

    #@2
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getIsIMSECCSetup()Z
    .registers 2

    #@0
    .prologue
    .line 2613
    sget-boolean v0, Lcom/android/internal/telephony/CallManager;->mIsUsingIPPhone:Z

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x0

    #@5
    .line 2615
    :goto_5
    return v0

    #@6
    :cond_6
    iget-boolean v0, p0, Lcom/android/internal/telephony/CallManager;->mIsIMSECCCalling:Z

    #@8
    goto :goto_5
.end method

.method public getMute()Z
    .registers 2

    #@0
    .prologue
    .line 1468
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->hasActiveFgCall()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_13

    #@6
    .line 1469
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getActiveFgCall()Lcom/android/internal/telephony/Call;

    #@9
    move-result-object v0

    #@a
    invoke-virtual {v0}, Lcom/android/internal/telephony/Call;->getPhone()Lcom/android/internal/telephony/Phone;

    #@d
    move-result-object v0

    #@e
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getMute()Z

    #@11
    move-result v0

    #@12
    .line 1473
    :goto_12
    return v0

    #@13
    .line 1470
    :cond_13
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->hasActiveBgCall()Z

    #@16
    move-result v0

    #@17
    if-eqz v0, :cond_26

    #@19
    .line 1471
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getFirstActiveBgCall()Lcom/android/internal/telephony/Call;

    #@1c
    move-result-object v0

    #@1d
    invoke-virtual {v0}, Lcom/android/internal/telephony/Call;->getPhone()Lcom/android/internal/telephony/Phone;

    #@20
    move-result-object v0

    #@21
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getMute()Z

    #@24
    move-result v0

    #@25
    goto :goto_12

    #@26
    .line 1473
    :cond_26
    const/4 v0, 0x0

    #@27
    goto :goto_12
.end method

.method public getPendingMmiCodes(Lcom/android/internal/telephony/Phone;)Ljava/util/List;
    .registers 4
    .parameter "phone"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/internal/telephony/Phone;",
            ")",
            "Ljava/util/List",
            "<+",
            "Lcom/android/internal/telephony/MmiCode;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1416
    const-string v0, "CallManager"

    #@2
    const-string v1, "getPendingMmiCodes not implemented"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1417
    const/4 v0, 0x0

    #@8
    return-object v0
.end method

.method public getPhoneInCall()Lcom/android/internal/telephony/Phone;
    .registers 3

    #@0
    .prologue
    .line 492
    const/4 v0, 0x0

    #@1
    .line 493
    .local v0, phone:Lcom/android/internal/telephony/Phone;
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getFirstActiveRingingCall()Lcom/android/internal/telephony/Call;

    #@4
    move-result-object v1

    #@5
    invoke-virtual {v1}, Lcom/android/internal/telephony/Call;->isIdle()Z

    #@8
    move-result v1

    #@9
    if-nez v1, :cond_14

    #@b
    .line 494
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getFirstActiveRingingCall()Lcom/android/internal/telephony/Call;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v1}, Lcom/android/internal/telephony/Call;->getPhone()Lcom/android/internal/telephony/Phone;

    #@12
    move-result-object v0

    #@13
    .line 501
    :goto_13
    return-object v0

    #@14
    .line 495
    :cond_14
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getActiveFgCall()Lcom/android/internal/telephony/Call;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1}, Lcom/android/internal/telephony/Call;->isIdle()Z

    #@1b
    move-result v1

    #@1c
    if-nez v1, :cond_27

    #@1e
    .line 496
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getActiveFgCall()Lcom/android/internal/telephony/Call;

    #@21
    move-result-object v1

    #@22
    invoke-virtual {v1}, Lcom/android/internal/telephony/Call;->getPhone()Lcom/android/internal/telephony/Phone;

    #@25
    move-result-object v0

    #@26
    goto :goto_13

    #@27
    .line 499
    :cond_27
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getFirstActiveBgCall()Lcom/android/internal/telephony/Call;

    #@2a
    move-result-object v1

    #@2b
    invoke-virtual {v1}, Lcom/android/internal/telephony/Call;->getPhone()Lcom/android/internal/telephony/Phone;

    #@2e
    move-result-object v0

    #@2f
    goto :goto_13
.end method

.method public getRingingCalls()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/Call;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 2085
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mRingingCalls:Ljava/util/ArrayList;

    #@2
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getRingingPhone()Lcom/android/internal/telephony/Phone;
    .registers 2

    #@0
    .prologue
    .line 485
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getFirstActiveRingingCall()Lcom/android/internal/telephony/Call;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Lcom/android/internal/telephony/Call;->getPhone()Lcom/android/internal/telephony/Phone;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public getServiceState()I
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x3

    #@1
    const/4 v5, 0x2

    #@2
    .line 359
    const/4 v2, 0x1

    #@3
    .line 361
    .local v2, resultState:I
    iget-object v4, p0, Lcom/android/internal/telephony/CallManager;->mPhones:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@8
    move-result-object v0

    #@9
    .local v0, i$:Ljava/util/Iterator;
    :cond_9
    :goto_9
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@c
    move-result v4

    #@d
    if-eqz v4, :cond_20

    #@f
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@12
    move-result-object v1

    #@13
    check-cast v1, Lcom/android/internal/telephony/Phone;

    #@15
    .line 362
    .local v1, phone:Lcom/android/internal/telephony/Phone;
    invoke-interface {v1}, Lcom/android/internal/telephony/Phone;->getServiceState()Landroid/telephony/ServiceState;

    #@18
    move-result-object v4

    #@19
    invoke-virtual {v4}, Landroid/telephony/ServiceState;->getState()I

    #@1c
    move-result v3

    #@1d
    .line 363
    .local v3, serviceState:I
    if-nez v3, :cond_21

    #@1f
    .line 365
    move v2, v3

    #@20
    .line 380
    .end local v1           #phone:Lcom/android/internal/telephony/Phone;
    .end local v3           #serviceState:I
    :cond_20
    return v2

    #@21
    .line 367
    .restart local v1       #phone:Lcom/android/internal/telephony/Phone;
    .restart local v3       #serviceState:I
    :cond_21
    const/4 v4, 0x1

    #@22
    if-ne v3, v4, :cond_2a

    #@24
    .line 370
    if-eq v2, v5, :cond_28

    #@26
    if-ne v2, v6, :cond_9

    #@28
    .line 372
    :cond_28
    move v2, v3

    #@29
    goto :goto_9

    #@2a
    .line 374
    :cond_2a
    if-ne v3, v5, :cond_9

    #@2c
    .line 375
    if-ne v2, v6, :cond_9

    #@2e
    .line 376
    move v2, v3

    #@2f
    goto :goto_9
.end method

.method public getState()Lcom/android/internal/telephony/PhoneConstants$State;
    .registers 6

    #@0
    .prologue
    .line 330
    sget-boolean v3, Lcom/android/internal/telephony/CallManager;->mIsUsingIPPhone:Z

    #@2
    if-eqz v3, :cond_e

    #@4
    .line 331
    const/4 v3, 0x0

    #@5
    invoke-static {v3}, Lcom/android/internal/telephony/CallManager;->isKeepingFakeCall(Z)Z

    #@8
    move-result v3

    #@9
    if-eqz v3, :cond_e

    #@b
    .line 332
    sget-object v2, Lcom/android/internal/telephony/PhoneConstants$State;->OFFHOOK:Lcom/android/internal/telephony/PhoneConstants$State;

    #@d
    .line 345
    :cond_d
    return-object v2

    #@e
    .line 336
    :cond_e
    sget-object v2, Lcom/android/internal/telephony/PhoneConstants$State;->IDLE:Lcom/android/internal/telephony/PhoneConstants$State;

    #@10
    .line 338
    .local v2, s:Lcom/android/internal/telephony/PhoneConstants$State;
    iget-object v3, p0, Lcom/android/internal/telephony/CallManager;->mPhones:Ljava/util/ArrayList;

    #@12
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@15
    move-result-object v0

    #@16
    .local v0, i$:Ljava/util/Iterator;
    :cond_16
    :goto_16
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@19
    move-result v3

    #@1a
    if-eqz v3, :cond_d

    #@1c
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1f
    move-result-object v1

    #@20
    check-cast v1, Lcom/android/internal/telephony/Phone;

    #@22
    .line 339
    .local v1, phone:Lcom/android/internal/telephony/Phone;
    invoke-interface {v1}, Lcom/android/internal/telephony/Phone;->getState()Lcom/android/internal/telephony/PhoneConstants$State;

    #@25
    move-result-object v3

    #@26
    sget-object v4, Lcom/android/internal/telephony/PhoneConstants$State;->RINGING:Lcom/android/internal/telephony/PhoneConstants$State;

    #@28
    if-ne v3, v4, :cond_2d

    #@2a
    .line 340
    sget-object v2, Lcom/android/internal/telephony/PhoneConstants$State;->RINGING:Lcom/android/internal/telephony/PhoneConstants$State;

    #@2c
    goto :goto_16

    #@2d
    .line 341
    :cond_2d
    invoke-interface {v1}, Lcom/android/internal/telephony/Phone;->getState()Lcom/android/internal/telephony/PhoneConstants$State;

    #@30
    move-result-object v3

    #@31
    sget-object v4, Lcom/android/internal/telephony/PhoneConstants$State;->OFFHOOK:Lcom/android/internal/telephony/PhoneConstants$State;

    #@33
    if-ne v3, v4, :cond_16

    #@35
    .line 342
    sget-object v3, Lcom/android/internal/telephony/PhoneConstants$State;->IDLE:Lcom/android/internal/telephony/PhoneConstants$State;

    #@37
    if-ne v2, v3, :cond_16

    #@39
    sget-object v2, Lcom/android/internal/telephony/PhoneConstants$State;->OFFHOOK:Lcom/android/internal/telephony/PhoneConstants$State;

    #@3b
    goto :goto_16
.end method

.method public hangupForegroundResumeBackground(Lcom/android/internal/telephony/Call;)V
    .registers 5
    .parameter "heldCall"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1066
    const/4 v1, 0x0

    #@1
    .line 1067
    .local v1, foregroundPhone:Lcom/android/internal/telephony/Phone;
    const/4 v0, 0x0

    #@2
    .line 1074
    .local v0, backgroundPhone:Lcom/android/internal/telephony/Phone;
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->hasActiveFgCall()Z

    #@5
    move-result v2

    #@6
    if-eqz v2, :cond_1b

    #@8
    .line 1075
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getFgPhone()Lcom/android/internal/telephony/Phone;

    #@b
    move-result-object v1

    #@c
    .line 1076
    if-eqz p1, :cond_1b

    #@e
    .line 1077
    invoke-virtual {p1}, Lcom/android/internal/telephony/Call;->getPhone()Lcom/android/internal/telephony/Phone;

    #@11
    move-result-object v0

    #@12
    .line 1078
    if-ne v1, v0, :cond_1c

    #@14
    .line 1079
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getActiveFgCall()Lcom/android/internal/telephony/Call;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {v2}, Lcom/android/internal/telephony/Call;->hangup()V

    #@1b
    .line 1092
    :cond_1b
    :goto_1b
    return-void

    #@1c
    .line 1082
    :cond_1c
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getActiveFgCall()Lcom/android/internal/telephony/Call;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2}, Lcom/android/internal/telephony/Call;->hangup()V

    #@23
    .line 1083
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/CallManager;->switchHoldingAndActive(Lcom/android/internal/telephony/Call;)V

    #@26
    goto :goto_1b
.end method

.method public hasActiveBgCall()Z
    .registers 2

    #@0
    .prologue
    .line 2122
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mBackgroundCalls:Ljava/util/ArrayList;

    #@2
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/CallManager;->getFirstActiveCall(Ljava/util/ArrayList;)Lcom/android/internal/telephony/Call;

    #@5
    move-result-object v0

    #@6
    if-eqz v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public hasActiveFgCall()Z
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 2107
    sget-boolean v2, Lcom/android/internal/telephony/CallManager;->mIsUsingIPPhone:Z

    #@4
    if-eqz v2, :cond_d

    #@6
    .line 2108
    invoke-static {v1}, Lcom/android/internal/telephony/CallManager;->isKeepingFakeCall(Z)Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_d

    #@c
    .line 2113
    :cond_c
    :goto_c
    return v0

    #@d
    :cond_d
    iget-object v2, p0, Lcom/android/internal/telephony/CallManager;->mForegroundCalls:Ljava/util/ArrayList;

    #@f
    invoke-direct {p0, v2}, Lcom/android/internal/telephony/CallManager;->getFirstActiveCall(Ljava/util/ArrayList;)Lcom/android/internal/telephony/Call;

    #@12
    move-result-object v2

    #@13
    if-nez v2, :cond_c

    #@15
    move v0, v1

    #@16
    goto :goto_c
.end method

.method public hasActiveRingingCall()Z
    .registers 2

    #@0
    .prologue
    .line 2130
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mRingingCalls:Ljava/util/ArrayList;

    #@2
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/CallManager;->getFirstActiveCall(Ljava/util/ArrayList;)Lcom/android/internal/telephony/Call;

    #@5
    move-result-object v0

    #@6
    if-eqz v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public hasDisconnectedBgCall()Z
    .registers 3

    #@0
    .prologue
    .line 2292
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mBackgroundCalls:Ljava/util/ArrayList;

    #@2
    sget-object v1, Lcom/android/internal/telephony/Call$State;->DISCONNECTED:Lcom/android/internal/telephony/Call$State;

    #@4
    invoke-direct {p0, v0, v1}, Lcom/android/internal/telephony/CallManager;->getFirstCallOfState(Ljava/util/ArrayList;Lcom/android/internal/telephony/Call$State;)Lcom/android/internal/telephony/Call;

    #@7
    move-result-object v0

    #@8
    if-eqz v0, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method public hasDisconnectedFgCall()Z
    .registers 3

    #@0
    .prologue
    .line 2285
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mForegroundCalls:Ljava/util/ArrayList;

    #@2
    sget-object v1, Lcom/android/internal/telephony/Call$State;->DISCONNECTED:Lcom/android/internal/telephony/Call$State;

    #@4
    invoke-direct {p0, v0, v1}, Lcom/android/internal/telephony/CallManager;->getFirstCallOfState(Ljava/util/ArrayList;Lcom/android/internal/telephony/Call$State;)Lcom/android/internal/telephony/Call;

    #@7
    move-result-object v0

    #@8
    if-eqz v0, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method public isImsPhoneActive()Z
    .registers 5

    #@0
    .prologue
    .line 2335
    iget-object v2, p0, Lcom/android/internal/telephony/CallManager;->mPhones:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v0

    #@6
    .local v0, i$:Ljava/util/Iterator;
    :cond_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_23

    #@c
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    check-cast v1, Lcom/android/internal/telephony/Phone;

    #@12
    .line 2336
    .local v1, phone:Lcom/android/internal/telephony/Phone;
    invoke-interface {v1}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    #@15
    move-result v2

    #@16
    const/4 v3, 0x4

    #@17
    if-ne v2, v3, :cond_6

    #@19
    invoke-interface {v1}, Lcom/android/internal/telephony/Phone;->getState()Lcom/android/internal/telephony/PhoneConstants$State;

    #@1c
    move-result-object v2

    #@1d
    sget-object v3, Lcom/android/internal/telephony/PhoneConstants$State;->IDLE:Lcom/android/internal/telephony/PhoneConstants$State;

    #@1f
    if-eq v2, v3, :cond_6

    #@21
    .line 2338
    const/4 v2, 0x1

    #@22
    .line 2341
    .end local v1           #phone:Lcom/android/internal/telephony/Phone;
    :goto_22
    return v2

    #@23
    :cond_23
    const/4 v2, 0x0

    #@24
    goto :goto_22
.end method

.method public isImsPhoneIdle()Z
    .registers 5

    #@0
    .prologue
    .line 2349
    iget-object v2, p0, Lcom/android/internal/telephony/CallManager;->mPhones:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v0

    #@6
    .local v0, i$:Ljava/util/Iterator;
    :cond_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_23

    #@c
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    check-cast v1, Lcom/android/internal/telephony/Phone;

    #@12
    .line 2350
    .local v1, phone:Lcom/android/internal/telephony/Phone;
    invoke-interface {v1}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    #@15
    move-result v2

    #@16
    const/4 v3, 0x4

    #@17
    if-ne v2, v3, :cond_6

    #@19
    invoke-interface {v1}, Lcom/android/internal/telephony/Phone;->getState()Lcom/android/internal/telephony/PhoneConstants$State;

    #@1c
    move-result-object v2

    #@1d
    sget-object v3, Lcom/android/internal/telephony/PhoneConstants$State;->IDLE:Lcom/android/internal/telephony/PhoneConstants$State;

    #@1f
    if-ne v2, v3, :cond_6

    #@21
    .line 2352
    const/4 v2, 0x1

    #@22
    .line 2355
    .end local v1           #phone:Lcom/android/internal/telephony/Phone;
    :goto_22
    return v2

    #@23
    :cond_23
    const/4 v2, 0x0

    #@24
    goto :goto_22
.end method

.method public registerForCallWaiting(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1916
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mCallWaitingRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/os/RegistrantList;->addUnique(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1917
    return-void
.end method

.method public registerForCdmaOtaStatusChange(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1983
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mCdmaOtaStatusChangeRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/os/RegistrantList;->addUnique(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1984
    return-void
.end method

.method public registerForCipheringNotification(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 2064
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mCipheringNotificationRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/os/RegistrantList;->addUnique(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 2065
    return-void
.end method

.method public registerForDisconnect(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1617
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mDisconnectRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/os/RegistrantList;->addUnique(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1618
    return-void
.end method

.method public registerForDisplayInfo(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1962
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mDisplayInfoRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/os/RegistrantList;->addUnique(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1963
    return-void
.end method

.method public registerForEcmTimerReset(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1797
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mEcmTimerResetRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/os/RegistrantList;->addUnique(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1798
    return-void
.end method

.method public registerForInCallVoicePrivacyOff(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1896
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mInCallVoicePrivacyOffRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/os/RegistrantList;->addUnique(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1897
    return-void
.end method

.method public registerForInCallVoicePrivacyOn(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1876
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mInCallVoicePrivacyOnRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/os/RegistrantList;->addUnique(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1877
    return-void
.end method

.method public registerForIncomingRing(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1699
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mIncomingRingRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/os/RegistrantList;->addUnique(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1700
    return-void
.end method

.method public registerForMmiComplete(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1779
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mMmiCompleteRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/os/RegistrantList;->addUnique(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1780
    return-void
.end method

.method public registerForMmiInitiate(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1759
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mMmiInitiateRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/os/RegistrantList;->addUnique(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1760
    return-void
.end method

.method public registerForNewRingingConnection(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1678
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mNewRingingConnectionRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/os/RegistrantList;->addUnique(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1679
    return-void
.end method

.method public registerForPostDialCharacter(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 2045
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mPostDialCharacterRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/os/RegistrantList;->addUnique(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 2046
    return-void
.end method

.method public registerForPreciseCallStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1638
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mPreciseCallStateRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/os/RegistrantList;->addUnique(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1639
    return-void
.end method

.method public registerForRedirectedNumberInfo(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 2054
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mRedirectedNumberInfoRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/os/RegistrantList;->addUnique(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 2055
    return-void
.end method

.method public registerForResendIncallMute(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1737
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mResendIncallMuteRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/os/RegistrantList;->addUnique(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1738
    return-void
.end method

.method public registerForRingbackTone(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1721
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mRingbackToneRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/os/RegistrantList;->addUnique(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1722
    return-void
.end method

.method public registerForServiceStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1814
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mServiceStateChangedRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/os/RegistrantList;->addUnique(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1815
    return-void
.end method

.method public registerForSignalInfo(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1939
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mSignalInfoRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/os/RegistrantList;->addUnique(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1940
    return-void
.end method

.method public registerForSubscriptionInfoReady(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 2001
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mSubscriptionInfoReadyRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/os/RegistrantList;->addUnique(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 2002
    return-void
.end method

.method public registerForSuppServiceFailed(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1834
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mSuppServiceFailedRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/os/RegistrantList;->addUnique(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1835
    return-void
.end method

.method public registerForSuppServiceNotification(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1856
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mSuppServiceNotificationRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/os/RegistrantList;->addUnique(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1857
    return-void
.end method

.method public registerForUnknownConnection(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 1654
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mUnknownConnectionRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/os/RegistrantList;->addUnique(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 1655
    return-void
.end method

.method public registerPhone(Lcom/android/internal/telephony/Phone;)Z
    .registers 8
    .parameter "phone"

    #@0
    .prologue
    .line 389
    invoke-static {p1}, Lcom/android/internal/telephony/CallManager;->getPhoneBase(Lcom/android/internal/telephony/Phone;)Lcom/android/internal/telephony/Phone;

    #@3
    move-result-object v0

    #@4
    .line 391
    .local v0, basePhone:Lcom/android/internal/telephony/Phone;
    if-eqz v0, :cond_9b

    #@6
    iget-object v2, p0, Lcom/android/internal/telephony/CallManager;->mPhones:Ljava/util/ArrayList;

    #@8
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@b
    move-result v2

    #@c
    if-nez v2, :cond_9b

    #@e
    .line 394
    const-string v2, "CallManager"

    #@10
    new-instance v3, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v4, "registerPhone("

    #@17
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    invoke-interface {p1}, Lcom/android/internal/telephony/Phone;->getPhoneName()Ljava/lang/String;

    #@1e
    move-result-object v4

    #@1f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    const-string v4, " "

    #@25
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v3

    #@29
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v3

    #@2d
    const-string v4, ")"

    #@2f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v3

    #@33
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v3

    #@37
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 398
    iget-object v2, p0, Lcom/android/internal/telephony/CallManager;->mPhones:Ljava/util/ArrayList;

    #@3c
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    #@3f
    move-result v2

    #@40
    if-eqz v2, :cond_44

    #@42
    .line 399
    iput-object v0, p0, Lcom/android/internal/telephony/CallManager;->mDefaultPhone:Lcom/android/internal/telephony/Phone;

    #@44
    .line 401
    :cond_44
    iget-object v2, p0, Lcom/android/internal/telephony/CallManager;->mPhones:Ljava/util/ArrayList;

    #@46
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@49
    .line 402
    iget-object v2, p0, Lcom/android/internal/telephony/CallManager;->mRingingCalls:Ljava/util/ArrayList;

    #@4b
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getRingingCall()Lcom/android/internal/telephony/Call;

    #@4e
    move-result-object v3

    #@4f
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@52
    .line 403
    iget-object v2, p0, Lcom/android/internal/telephony/CallManager;->mBackgroundCalls:Ljava/util/ArrayList;

    #@54
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getBackgroundCall()Lcom/android/internal/telephony/Call;

    #@57
    move-result-object v3

    #@58
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@5b
    .line 404
    iget-object v2, p0, Lcom/android/internal/telephony/CallManager;->mForegroundCalls:Ljava/util/ArrayList;

    #@5d
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getForegroundCall()Lcom/android/internal/telephony/Call;

    #@60
    move-result-object v3

    #@61
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@64
    .line 407
    sget-boolean v2, Lcom/android/internal/telephony/CallManager;->mIsUsingIPPhone:Z

    #@66
    if-eqz v2, :cond_96

    #@68
    instance-of v2, v0, Lcom/movial/ipphone/IPPhoneProxy;

    #@6a
    if-eqz v2, :cond_96

    #@6c
    move-object v1, v0

    #@6d
    .line 408
    check-cast v1, Lcom/movial/ipphone/IPPhoneProxy;

    #@6f
    .line 409
    .local v1, proxy:Lcom/movial/ipphone/IPPhoneProxy;
    iget-object v2, p0, Lcom/android/internal/telephony/CallManager;->mRingingCalls:Ljava/util/ArrayList;

    #@71
    invoke-virtual {v1}, Lcom/movial/ipphone/IPPhoneProxy;->getImsRingingCall()Lcom/android/internal/telephony/Call;

    #@74
    move-result-object v3

    #@75
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@78
    .line 410
    iget-object v2, p0, Lcom/android/internal/telephony/CallManager;->mBackgroundCalls:Ljava/util/ArrayList;

    #@7a
    invoke-virtual {v1}, Lcom/movial/ipphone/IPPhoneProxy;->getImsBackgroundCall()Lcom/android/internal/telephony/Call;

    #@7d
    move-result-object v3

    #@7e
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@81
    .line 411
    iget-object v2, p0, Lcom/android/internal/telephony/CallManager;->mForegroundCalls:Ljava/util/ArrayList;

    #@83
    invoke-virtual {v1}, Lcom/movial/ipphone/IPPhoneProxy;->getImsForegroundCall()Lcom/android/internal/telephony/Call;

    #@86
    move-result-object v3

    #@87
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@8a
    .line 412
    sput-object v1, Lcom/android/internal/telephony/CallManager;->mIPPhoneProxy:Lcom/movial/ipphone/IPPhoneProxy;

    #@8c
    .line 413
    sget-object v2, Lcom/android/internal/telephony/CallManager;->mIPPhoneProxy:Lcom/movial/ipphone/IPPhoneProxy;

    #@8e
    iget-object v3, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@90
    const/16 v4, 0xcb

    #@92
    const/4 v5, 0x0

    #@93
    invoke-virtual {v2, v3, v4, v5}, Lcom/movial/ipphone/IPPhoneProxy;->registerForOn(Landroid/os/Handler;ILjava/lang/Object;)V

    #@96
    .line 416
    .end local v1           #proxy:Lcom/movial/ipphone/IPPhoneProxy;
    :cond_96
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/CallManager;->registerForPhoneStates(Lcom/android/internal/telephony/Phone;)V

    #@99
    .line 417
    const/4 v2, 0x1

    #@9a
    .line 419
    :goto_9a
    return v2

    #@9b
    :cond_9b
    const/4 v2, 0x0

    #@9c
    goto :goto_9a
.end method

.method public rejectCall(Lcom/android/internal/telephony/Call;)V
    .registers 3
    .parameter "ringingCall"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 998
    invoke-virtual {p1}, Lcom/android/internal/telephony/Call;->getPhone()Lcom/android/internal/telephony/Phone;

    #@3
    move-result-object v0

    #@4
    .line 1000
    .local v0, ringingPhone:Lcom/android/internal/telephony/Phone;
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->rejectCall()V

    #@7
    .line 1006
    return-void
.end method

.method public sendBurstDtmf(Ljava/lang/String;IILandroid/os/Message;)Z
    .registers 6
    .parameter "dtmfString"
    .parameter "on"
    .parameter "off"
    .parameter "onComplete"

    #@0
    .prologue
    .line 1599
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->hasActiveFgCall()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_13

    #@6
    .line 1600
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getActiveFgCall()Lcom/android/internal/telephony/Call;

    #@9
    move-result-object v0

    #@a
    invoke-virtual {v0}, Lcom/android/internal/telephony/Call;->getPhone()Lcom/android/internal/telephony/Phone;

    #@d
    move-result-object v0

    #@e
    invoke-interface {v0, p1, p2, p3, p4}, Lcom/android/internal/telephony/Phone;->sendBurstDtmf(Ljava/lang/String;IILandroid/os/Message;)V

    #@11
    .line 1601
    const/4 v0, 0x1

    #@12
    .line 1603
    :goto_12
    return v0

    #@13
    :cond_13
    const/4 v0, 0x0

    #@14
    goto :goto_12
.end method

.method public sendDtmf(C)Z
    .registers 4
    .parameter "c"

    #@0
    .prologue
    .line 1504
    const/4 v0, 0x0

    #@1
    .line 1511
    .local v0, result:Z
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->hasActiveFgCall()Z

    #@4
    move-result v1

    #@5
    if-eqz v1, :cond_13

    #@7
    .line 1512
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getActiveFgCall()Lcom/android/internal/telephony/Call;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v1}, Lcom/android/internal/telephony/Call;->getPhone()Lcom/android/internal/telephony/Phone;

    #@e
    move-result-object v1

    #@f
    invoke-interface {v1, p1}, Lcom/android/internal/telephony/Phone;->sendDtmf(C)V

    #@12
    .line 1513
    const/4 v0, 0x1

    #@13
    .line 1520
    :cond_13
    return v0
.end method

.method public sendUssdResponse(Lcom/android/internal/telephony/Phone;Ljava/lang/String;)Z
    .registers 5
    .parameter "phone"
    .parameter "ussdMessge"

    #@0
    .prologue
    .line 1429
    const-string v0, "CallManager"

    #@2
    const-string v1, "sendUssdResponse not implemented"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1430
    const/4 v0, 0x0

    #@8
    return v0
.end method

.method public setAudioAndInCallMode()V
    .registers 10

    #@0
    .prologue
    .line 629
    invoke-direct {p0}, Lcom/android/internal/telephony/CallManager;->getContext()Landroid/content/Context;

    #@3
    move-result-object v1

    #@4
    .line 630
    .local v1, context:Landroid/content/Context;
    if-nez v1, :cond_7

    #@6
    .line 670
    :cond_6
    :goto_6
    return-void

    #@7
    .line 631
    :cond_7
    const-string v6, "audio"

    #@9
    invoke-virtual {v1, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@c
    move-result-object v0

    #@d
    check-cast v0, Landroid/media/AudioManager;

    #@f
    .line 634
    .local v0, audioManager:Landroid/media/AudioManager;
    const/4 v4, 0x0

    #@10
    .line 635
    .local v4, mode:I
    const/4 v3, 0x0

    #@11
    .line 637
    .local v3, inCallMode:I
    const-string v6, "CallManager"

    #@13
    new-instance v7, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v8, "setAudioAndInCall state: "

    #@1a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v7

    #@1e
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getState()Lcom/android/internal/telephony/PhoneConstants$State;

    #@21
    move-result-object v8

    #@22
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v7

    #@26
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v7

    #@2a
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 638
    sget-object v6, Lcom/android/internal/telephony/CallManager$2;->$SwitchMap$com$android$internal$telephony$PhoneConstants$State:[I

    #@2f
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getState()Lcom/android/internal/telephony/PhoneConstants$State;

    #@32
    move-result-object v7

    #@33
    invoke-virtual {v7}, Lcom/android/internal/telephony/PhoneConstants$State;->ordinal()I

    #@36
    move-result v7

    #@37
    aget v6, v6, v7

    #@39
    packed-switch v6, :pswitch_data_d8

    #@3c
    .line 658
    :cond_3c
    :goto_3c
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/CallManager;->updateAudioFocus(Landroid/media/AudioManager;)V

    #@3f
    .line 660
    const-string v6, "CallManager"

    #@41
    new-instance v7, Ljava/lang/StringBuilder;

    #@43
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@46
    const-string v8, "setAudioAndInCallMode inCallMode = "

    #@48
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v7

    #@4c
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v7

    #@50
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@53
    move-result-object v7

    #@54
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@57
    .line 661
    invoke-direct {p0, v3}, Lcom/android/internal/telephony/CallManager;->isInCallModeActive(I)Z

    #@5a
    move-result v6

    #@5b
    if-eqz v6, :cond_af

    #@5d
    .line 662
    const-string v6, "CallManager"

    #@5f
    new-instance v7, Ljava/lang/StringBuilder;

    #@61
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@64
    const-string v8, "Calling setInCallMode("

    #@66
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v7

    #@6a
    invoke-direct {p0, v3}, Lcom/android/internal/telephony/CallManager;->inCallModeToString(I)Ljava/lang/String;

    #@6d
    move-result-object v8

    #@6e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v7

    #@72
    const-string v8, ")"

    #@74
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v7

    #@78
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7b
    move-result-object v7

    #@7c
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7f
    .line 663
    invoke-virtual {v0}, Landroid/media/AudioManager;->getInCallMode()I

    #@82
    move-result v6

    #@83
    if-eq v3, v6, :cond_6

    #@85
    .line 664
    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->setInCallMode(I)V

    #@88
    goto/16 :goto_6

    #@8a
    .line 640
    :pswitch_8a
    const/4 v4, 0x1

    #@8b
    .line 641
    goto :goto_3c

    #@8c
    .line 646
    :pswitch_8c
    iget-object v6, p0, Lcom/android/internal/telephony/CallManager;->mPhones:Ljava/util/ArrayList;

    #@8e
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@91
    move-result-object v2

    #@92
    .local v2, i$:Ljava/util/Iterator;
    :goto_92
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@95
    move-result v6

    #@96
    if-eqz v6, :cond_3c

    #@98
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@9b
    move-result-object v5

    #@9c
    check-cast v5, Lcom/android/internal/telephony/Phone;

    #@9e
    .line 647
    .local v5, phone:Lcom/android/internal/telephony/Phone;
    instance-of v6, v5, Lcom/android/internal/telephony/sip/SipPhone;

    #@a0
    if-eqz v6, :cond_a9

    #@a2
    .line 649
    check-cast v5, Lcom/android/internal/telephony/sip/SipPhone;

    #@a4
    .end local v5           #phone:Lcom/android/internal/telephony/Phone;
    invoke-direct {p0, v4, v5}, Lcom/android/internal/telephony/CallManager;->phoneAudioModeForSipPhone(ILcom/android/internal/telephony/sip/SipPhone;)I

    #@a7
    move-result v4

    #@a8
    goto :goto_92

    #@a9
    .line 651
    .restart local v5       #phone:Lcom/android/internal/telephony/Phone;
    :cond_a9
    invoke-direct {p0, v5}, Lcom/android/internal/telephony/CallManager;->inCallAudioModeForPhone(Lcom/android/internal/telephony/Phone;)I

    #@ac
    move-result v6

    #@ad
    or-int/2addr v3, v6

    #@ae
    goto :goto_92

    #@af
    .line 667
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v5           #phone:Lcom/android/internal/telephony/Phone;
    :cond_af
    const-string v6, "CallManager"

    #@b1
    new-instance v7, Ljava/lang/StringBuilder;

    #@b3
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@b6
    const-string v8, "Calling setMode( "

    #@b8
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bb
    move-result-object v7

    #@bc
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v7

    #@c0
    const-string v8, ")"

    #@c2
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c5
    move-result-object v7

    #@c6
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c9
    move-result-object v7

    #@ca
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@cd
    .line 668
    invoke-virtual {v0}, Landroid/media/AudioManager;->getMode()I

    #@d0
    move-result v6

    #@d1
    if-eq v6, v4, :cond_6

    #@d3
    invoke-virtual {v0, v4}, Landroid/media/AudioManager;->setMode(I)V

    #@d6
    goto/16 :goto_6

    #@d8
    .line 638
    :pswitch_data_d8
    .packed-switch 0x1
        :pswitch_8a
        :pswitch_8c
    .end packed-switch
.end method

.method public setAudioMode()V
    .registers 13

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v11, 0x2

    #@2
    const/4 v6, 0x0

    #@3
    .line 673
    const-string v8, "persist.radio.calls.on.ims"

    #@5
    invoke-static {v8, v6}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@8
    move-result v8

    #@9
    if-nez v8, :cond_29

    #@b
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@e
    move-result-object v8

    #@f
    invoke-virtual {v8}, Landroid/telephony/MSimTelephonyManager;->isMultiSimEnabled()Z

    #@12
    move-result v8

    #@13
    if-nez v8, :cond_29

    #@15
    iget-object v8, p0, Lcom/android/internal/telephony/CallManager;->mBaseband:Ljava/lang/String;

    #@17
    const-string v9, "sglte"

    #@19
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1c
    move-result v8

    #@1d
    if-nez v8, :cond_29

    #@1f
    iget-object v8, p0, Lcom/android/internal/telephony/CallManager;->mBaseband:Ljava/lang/String;

    #@21
    const-string v9, "sglte2"

    #@23
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@26
    move-result v8

    #@27
    if-eqz v8, :cond_54

    #@29
    :cond_29
    move v5, v7

    #@2a
    .line 678
    .local v5, useInCallMode:Z
    :goto_2a
    const-string v8, "CallManager"

    #@2c
    new-instance v9, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v10, "setAudioMode useInCallMode = "

    #@33
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v9

    #@37
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v9

    #@3b
    const-string v10, ", Baseband = "

    #@3d
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v9

    #@41
    iget-object v10, p0, Lcom/android/internal/telephony/CallManager;->mBaseband:Ljava/lang/String;

    #@43
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v9

    #@47
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v9

    #@4b
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    .line 680
    if-eqz v5, :cond_56

    #@50
    .line 681
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->setAudioAndInCallMode()V

    #@53
    .line 759
    :cond_53
    :goto_53
    return-void

    #@54
    .end local v5           #useInCallMode:Z
    :cond_54
    move v5, v6

    #@55
    .line 673
    goto :goto_2a

    #@56
    .line 684
    .restart local v5       #useInCallMode:Z
    :cond_56
    invoke-direct {p0}, Lcom/android/internal/telephony/CallManager;->getContext()Landroid/content/Context;

    #@59
    move-result-object v1

    #@5a
    .line 685
    .local v1, context:Landroid/content/Context;
    if-eqz v1, :cond_53

    #@5c
    .line 686
    const-string v8, "audio"

    #@5e
    invoke-virtual {v1, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@61
    move-result-object v0

    #@62
    check-cast v0, Landroid/media/AudioManager;

    #@64
    .line 691
    .local v0, audioManager:Landroid/media/AudioManager;
    sget-object v8, Lcom/android/internal/telephony/CallManager$2;->$SwitchMap$com$android$internal$telephony$PhoneConstants$State:[I

    #@66
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getState()Lcom/android/internal/telephony/PhoneConstants$State;

    #@69
    move-result-object v9

    #@6a
    invoke-virtual {v9}, Lcom/android/internal/telephony/PhoneConstants$State;->ordinal()I

    #@6d
    move-result v9

    #@6e
    aget v8, v8, v9

    #@70
    packed-switch v8, :pswitch_data_de

    #@73
    goto :goto_53

    #@74
    .line 693
    :pswitch_74
    invoke-virtual {v0}, Landroid/media/AudioManager;->getMode()I

    #@77
    move-result v2

    #@78
    .line 694
    .local v2, curAudioMode:I
    if-eq v2, v7, :cond_8a

    #@7a
    .line 696
    invoke-virtual {v0, v11}, Landroid/media/AudioManager;->getStreamVolume(I)I

    #@7d
    move-result v6

    #@7e
    if-lez v6, :cond_83

    #@80
    .line 698
    invoke-virtual {v0, v11, v11}, Landroid/media/AudioManager;->requestAudioFocusForCall(II)V

    #@83
    .line 701
    :cond_83
    iget-boolean v6, p0, Lcom/android/internal/telephony/CallManager;->mSpeedUpAudioForMtCall:Z

    #@85
    if-nez v6, :cond_8a

    #@87
    .line 702
    invoke-virtual {v0, v7}, Landroid/media/AudioManager;->setMode(I)V

    #@8a
    .line 706
    :cond_8a
    iget-boolean v6, p0, Lcom/android/internal/telephony/CallManager;->mSpeedUpAudioForMtCall:Z

    #@8c
    if-eqz v6, :cond_53

    #@8e
    if-eq v2, v11, :cond_53

    #@90
    .line 707
    invoke-virtual {v0, v11}, Landroid/media/AudioManager;->setMode(I)V

    #@93
    goto :goto_53

    #@94
    .line 711
    .end local v2           #curAudioMode:I
    :pswitch_94
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getFgPhone()Lcom/android/internal/telephony/Phone;

    #@97
    move-result-object v4

    #@98
    .line 712
    .local v4, offhookPhone:Lcom/android/internal/telephony/Phone;
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getActiveFgCallState()Lcom/android/internal/telephony/Call$State;

    #@9b
    move-result-object v7

    #@9c
    sget-object v8, Lcom/android/internal/telephony/Call$State;->IDLE:Lcom/android/internal/telephony/Call$State;

    #@9e
    if-ne v7, v8, :cond_a4

    #@a0
    .line 715
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getBgPhone()Lcom/android/internal/telephony/Phone;

    #@a3
    move-result-object v4

    #@a4
    .line 718
    :cond_a4
    const/4 v3, 0x2

    #@a5
    .line 719
    .local v3, newAudioMode:I
    instance-of v7, v4, Lcom/android/internal/telephony/sip/SipPhone;

    #@a7
    if-eqz v7, :cond_c4

    #@a9
    .line 720
    const-string v7, "CallManager"

    #@ab
    const-string v8, "setAudioMode Set audio mode for SIP call!"

    #@ad
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b0
    .line 722
    const/4 v3, 0x3

    #@b1
    .line 739
    :cond_b1
    :goto_b1
    invoke-virtual {v0}, Landroid/media/AudioManager;->getMode()I

    #@b4
    move-result v7

    #@b5
    if-ne v7, v3, :cond_bb

    #@b7
    iget-boolean v7, p0, Lcom/android/internal/telephony/CallManager;->mSpeedUpAudioForMtCall:Z

    #@b9
    if-eqz v7, :cond_c1

    #@bb
    .line 742
    :cond_bb
    invoke-virtual {v0, v6, v11}, Landroid/media/AudioManager;->requestAudioFocusForCall(II)V

    #@be
    .line 744
    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->setMode(I)V

    #@c1
    .line 747
    :cond_c1
    iput-boolean v6, p0, Lcom/android/internal/telephony/CallManager;->mSpeedUpAudioForMtCall:Z

    #@c3
    goto :goto_53

    #@c4
    .line 725
    :cond_c4
    sget-boolean v7, Lcom/android/internal/telephony/CallManager;->mIsUsingIPPhone:Z

    #@c6
    if-eqz v7, :cond_b1

    #@c8
    .line 726
    instance-of v7, v4, Lcom/movial/ipphone/IPPhone;

    #@ca
    if-eqz v7, :cond_b1

    #@cc
    .line 727
    const/4 v3, 0x3

    #@cd
    goto :goto_b1

    #@ce
    .line 750
    .end local v3           #newAudioMode:I
    .end local v4           #offhookPhone:Lcom/android/internal/telephony/Phone;
    :pswitch_ce
    invoke-virtual {v0}, Landroid/media/AudioManager;->getMode()I

    #@d1
    move-result v7

    #@d2
    if-eqz v7, :cond_da

    #@d4
    .line 751
    invoke-virtual {v0, v6}, Landroid/media/AudioManager;->setMode(I)V

    #@d7
    .line 754
    invoke-virtual {v0}, Landroid/media/AudioManager;->abandonAudioFocusForCall()V

    #@da
    .line 756
    :cond_da
    iput-boolean v6, p0, Lcom/android/internal/telephony/CallManager;->mSpeedUpAudioForMtCall:Z

    #@dc
    goto/16 :goto_53

    #@de
    .line 691
    :pswitch_data_de
    .packed-switch 0x1
        :pswitch_74
        :pswitch_94
        :pswitch_ce
    .end packed-switch
.end method

.method public setEchoSuppressionEnabled(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 1485
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->hasActiveFgCall()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_11

    #@6
    .line 1486
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getActiveFgCall()Lcom/android/internal/telephony/Call;

    #@9
    move-result-object v0

    #@a
    invoke-virtual {v0}, Lcom/android/internal/telephony/Call;->getPhone()Lcom/android/internal/telephony/Phone;

    #@d
    move-result-object v0

    #@e
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->setEchoSuppressionEnabled(Z)V

    #@11
    .line 1493
    :cond_11
    return-void
.end method

.method public setIsIMSECCSetup(Z)V
    .registers 6
    .parameter "sending"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 2601
    sget-boolean v0, Lcom/android/internal/telephony/CallManager;->mIsUsingIPPhone:Z

    #@3
    if-nez v0, :cond_6

    #@5
    .line 2610
    :cond_5
    :goto_5
    return-void

    #@6
    .line 2603
    :cond_6
    const-string v0, "CallManager"

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "RETRY_ECC Set flag ECCRETRY "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 2604
    iput-boolean p1, p0, Lcom/android/internal/telephony/CallManager;->mIsIMSECCCalling:Z

    #@20
    .line 2605
    iget-boolean v0, p0, Lcom/android/internal/telephony/CallManager;->mIsIMSECCCalling:Z

    #@22
    if-eqz v0, :cond_5

    #@24
    .line 2606
    sput-boolean v3, Lcom/android/internal/telephony/CallManager;->mIsNormalPowerOnReq:Z

    #@26
    .line 2607
    sput-boolean v3, Lcom/android/internal/telephony/CallManager;->mSkipRegistration:Z

    #@28
    .line 2608
    const/4 v0, 0x1

    #@29
    sput-boolean v0, Lcom/android/internal/telephony/CallManager;->mIsKeepingFakeCall:Z

    #@2b
    goto :goto_5
.end method

.method public setMute(Z)V
    .registers 3
    .parameter "muted"

    #@0
    .prologue
    .line 1448
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->hasActiveFgCall()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_11

    #@6
    .line 1449
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getActiveFgCall()Lcom/android/internal/telephony/Call;

    #@9
    move-result-object v0

    #@a
    invoke-virtual {v0}, Lcom/android/internal/telephony/Call;->getPhone()Lcom/android/internal/telephony/Phone;

    #@d
    move-result-object v0

    #@e
    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->setMute(Z)V

    #@11
    .line 1456
    :cond_11
    return-void
.end method

.method public startDtmf(C)Z
    .registers 4
    .parameter "c"

    #@0
    .prologue
    .line 1533
    const/4 v0, 0x0

    #@1
    .line 1540
    .local v0, result:Z
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->hasActiveFgCall()Z

    #@4
    move-result v1

    #@5
    if-eqz v1, :cond_1d

    #@7
    .line 1542
    sget-boolean v1, Lcom/android/internal/telephony/CallManager;->isStartDtmf:Z

    #@9
    if-eqz v1, :cond_e

    #@b
    .line 1543
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->stopDtmf()V

    #@e
    .line 1545
    :cond_e
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getActiveFgCall()Lcom/android/internal/telephony/Call;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1}, Lcom/android/internal/telephony/Call;->getPhone()Lcom/android/internal/telephony/Phone;

    #@15
    move-result-object v1

    #@16
    invoke-interface {v1, p1}, Lcom/android/internal/telephony/Phone;->startDtmf(C)V

    #@19
    .line 1546
    const/4 v1, 0x1

    #@1a
    sput-boolean v1, Lcom/android/internal/telephony/CallManager;->isStartDtmf:Z

    #@1c
    .line 1547
    const/4 v0, 0x1

    #@1d
    .line 1558
    :cond_1d
    return v0
.end method

.method public stopDtmf()V
    .registers 2

    #@0
    .prologue
    .line 1571
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->hasActiveFgCall()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_10

    #@6
    .line 1572
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getFgPhone()Lcom/android/internal/telephony/Phone;

    #@9
    move-result-object v0

    #@a
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->stopDtmf()V

    #@d
    .line 1574
    const/4 v0, 0x0

    #@e
    sput-boolean v0, Lcom/android/internal/telephony/CallManager;->isStartDtmf:Z

    #@10
    .line 1582
    :cond_10
    return-void
.end method

.method public switchHoldingAndActive(Lcom/android/internal/telephony/Call;)V
    .registers 5
    .parameter "heldCall"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    #@0
    .prologue
    .line 1027
    const/4 v0, 0x0

    #@1
    .line 1028
    .local v0, activePhone:Lcom/android/internal/telephony/Phone;
    const/4 v1, 0x0

    #@2
    .line 1035
    .local v1, heldPhone:Lcom/android/internal/telephony/Phone;
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->hasActiveFgCall()Z

    #@5
    move-result v2

    #@6
    if-eqz v2, :cond_10

    #@8
    .line 1036
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getActiveFgCall()Lcom/android/internal/telephony/Call;

    #@b
    move-result-object v2

    #@c
    invoke-virtual {v2}, Lcom/android/internal/telephony/Call;->getPhone()Lcom/android/internal/telephony/Phone;

    #@f
    move-result-object v0

    #@10
    .line 1039
    :cond_10
    if-eqz p1, :cond_16

    #@12
    .line 1040
    invoke-virtual {p1}, Lcom/android/internal/telephony/Call;->getPhone()Lcom/android/internal/telephony/Phone;

    #@15
    move-result-object v1

    #@16
    .line 1043
    :cond_16
    if-eqz v0, :cond_1b

    #@18
    .line 1044
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->switchHoldingAndActive()V

    #@1b
    .line 1047
    :cond_1b
    if-eqz v1, :cond_22

    #@1d
    if-eq v1, v0, :cond_22

    #@1f
    .line 1048
    invoke-interface {v1}, Lcom/android/internal/telephony/Phone;->switchHoldingAndActive()V

    #@22
    .line 1055
    :cond_22
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 7

    #@0
    .prologue
    .line 2555
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 2557
    .local v0, b:Ljava/lang/StringBuilder;
    const-string v4, "CallManager {"

    #@7
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    .line 2558
    new-instance v4, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v5, "\nstate = "

    #@11
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v4

    #@15
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getState()Lcom/android/internal/telephony/PhoneConstants$State;

    #@18
    move-result-object v5

    #@19
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v4

    #@1d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v4

    #@21
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    .line 2559
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getActiveFgCall()Lcom/android/internal/telephony/Call;

    #@27
    move-result-object v1

    #@28
    .line 2560
    .local v1, call:Lcom/android/internal/telephony/Call;
    new-instance v4, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v5, "\n- Foreground: "

    #@2f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v4

    #@33
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getActiveFgCallState()Lcom/android/internal/telephony/Call$State;

    #@36
    move-result-object v5

    #@37
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v4

    #@3b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v4

    #@3f
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    .line 2561
    new-instance v4, Ljava/lang/StringBuilder;

    #@44
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    const-string v5, " from "

    #@49
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v4

    #@4d
    invoke-virtual {v1}, Lcom/android/internal/telephony/Call;->getPhone()Lcom/android/internal/telephony/Phone;

    #@50
    move-result-object v5

    #@51
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v4

    #@55
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@58
    move-result-object v4

    #@59
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    .line 2562
    const-string v4, "\n  Conn: "

    #@5e
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v4

    #@62
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getFgCallConnections()Ljava/util/List;

    #@65
    move-result-object v5

    #@66
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@69
    .line 2563
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getFirstActiveBgCall()Lcom/android/internal/telephony/Call;

    #@6c
    move-result-object v1

    #@6d
    .line 2564
    new-instance v4, Ljava/lang/StringBuilder;

    #@6f
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@72
    const-string v5, "\n- Background: "

    #@74
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v4

    #@78
    invoke-virtual {v1}, Lcom/android/internal/telephony/Call;->getState()Lcom/android/internal/telephony/Call$State;

    #@7b
    move-result-object v5

    #@7c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v4

    #@80
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@83
    move-result-object v4

    #@84
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    .line 2565
    new-instance v4, Ljava/lang/StringBuilder;

    #@89
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@8c
    const-string v5, " from "

    #@8e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    move-result-object v4

    #@92
    invoke-virtual {v1}, Lcom/android/internal/telephony/Call;->getPhone()Lcom/android/internal/telephony/Phone;

    #@95
    move-result-object v5

    #@96
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@99
    move-result-object v4

    #@9a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9d
    move-result-object v4

    #@9e
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a1
    .line 2566
    const-string v4, "\n  Conn: "

    #@a3
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v4

    #@a7
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getBgCallConnections()Ljava/util/List;

    #@aa
    move-result-object v5

    #@ab
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@ae
    .line 2567
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getFirstActiveRingingCall()Lcom/android/internal/telephony/Call;

    #@b1
    move-result-object v1

    #@b2
    .line 2568
    new-instance v4, Ljava/lang/StringBuilder;

    #@b4
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@b7
    const-string v5, "\n- Ringing: "

    #@b9
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bc
    move-result-object v4

    #@bd
    invoke-virtual {v1}, Lcom/android/internal/telephony/Call;->getState()Lcom/android/internal/telephony/Call$State;

    #@c0
    move-result-object v5

    #@c1
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c4
    move-result-object v4

    #@c5
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c8
    move-result-object v4

    #@c9
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cc
    .line 2569
    new-instance v4, Ljava/lang/StringBuilder;

    #@ce
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@d1
    const-string v5, " from "

    #@d3
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d6
    move-result-object v4

    #@d7
    invoke-virtual {v1}, Lcom/android/internal/telephony/Call;->getPhone()Lcom/android/internal/telephony/Phone;

    #@da
    move-result-object v5

    #@db
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@de
    move-result-object v4

    #@df
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e2
    move-result-object v4

    #@e3
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e6
    .line 2571
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallManager;->getAllPhones()Ljava/util/List;

    #@e9
    move-result-object v4

    #@ea
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@ed
    move-result-object v2

    #@ee
    .local v2, i$:Ljava/util/Iterator;
    :cond_ee
    :goto_ee
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@f1
    move-result v4

    #@f2
    if-eqz v4, :cond_156

    #@f4
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f7
    move-result-object v3

    #@f8
    check-cast v3, Lcom/android/internal/telephony/Phone;

    #@fa
    .line 2572
    .local v3, phone:Lcom/android/internal/telephony/Phone;
    if-eqz v3, :cond_ee

    #@fc
    .line 2573
    new-instance v4, Ljava/lang/StringBuilder;

    #@fe
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@101
    const-string v5, "\nPhone: "

    #@103
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@106
    move-result-object v4

    #@107
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10a
    move-result-object v4

    #@10b
    const-string v5, ", name = "

    #@10d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@110
    move-result-object v4

    #@111
    invoke-interface {v3}, Lcom/android/internal/telephony/Phone;->getPhoneName()Ljava/lang/String;

    #@114
    move-result-object v5

    #@115
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@118
    move-result-object v4

    #@119
    const-string v5, ", state = "

    #@11b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11e
    move-result-object v4

    #@11f
    invoke-interface {v3}, Lcom/android/internal/telephony/Phone;->getState()Lcom/android/internal/telephony/PhoneConstants$State;

    #@122
    move-result-object v5

    #@123
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@126
    move-result-object v4

    #@127
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12a
    move-result-object v4

    #@12b
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12e
    .line 2575
    invoke-interface {v3}, Lcom/android/internal/telephony/Phone;->getForegroundCall()Lcom/android/internal/telephony/Call;

    #@131
    move-result-object v1

    #@132
    .line 2576
    const-string v4, "\n- Foreground: "

    #@134
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@137
    move-result-object v4

    #@138
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@13b
    .line 2577
    invoke-interface {v3}, Lcom/android/internal/telephony/Phone;->getBackgroundCall()Lcom/android/internal/telephony/Call;

    #@13e
    move-result-object v1

    #@13f
    .line 2578
    const-string v4, " Background: "

    #@141
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@144
    move-result-object v4

    #@145
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@148
    .line 2579
    invoke-interface {v3}, Lcom/android/internal/telephony/Phone;->getRingingCall()Lcom/android/internal/telephony/Call;

    #@14b
    move-result-object v1

    #@14c
    .line 2580
    const-string v4, " Ringing: "

    #@14e
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@151
    move-result-object v4

    #@152
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@155
    goto :goto_ee

    #@156
    .line 2583
    .end local v3           #phone:Lcom/android/internal/telephony/Phone;
    :cond_156
    const-string v4, "\n}"

    #@158
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15b
    .line 2584
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15e
    move-result-object v4

    #@15f
    return-object v4
.end method

.method public unregisterForCallWaiting(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1924
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mCallWaitingRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 1925
    return-void
.end method

.method public unregisterForCdmaOtaStatusChange(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1991
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mCdmaOtaStatusChangeRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 1992
    return-void
.end method

.method public unregisterForCipheringNotification(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 2068
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mCipheringNotificationRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 2069
    return-void
.end method

.method public unregisterForDisconnect(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1625
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mDisconnectRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 1626
    return-void
.end method

.method public unregisterForDisplayInfo(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1972
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mDisplayInfoRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 1973
    return-void
.end method

.method public unregisterForEcmTimerReset(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1805
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mEcmTimerResetRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 1806
    return-void
.end method

.method public unregisterForInCallVoicePrivacyOff(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1905
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mInCallVoicePrivacyOffRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 1906
    return-void
.end method

.method public unregisterForInCallVoicePrivacyOn(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1885
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mInCallVoicePrivacyOnRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 1886
    return-void
.end method

.method public unregisterForIncomingRing(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1708
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mIncomingRingRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 1709
    return-void
.end method

.method public unregisterForMmiComplete(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1787
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mMmiCompleteRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 1788
    return-void
.end method

.method public unregisterForMmiInitiate(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1767
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mMmiInitiateRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 1768
    return-void
.end method

.method public unregisterForNewRingingConnection(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1687
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mNewRingingConnectionRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 1688
    return-void
.end method

.method public unregisterForPostDialCharacter(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 2049
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mPostDialCharacterRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 2050
    return-void
.end method

.method public unregisterForPreciseCallStateChanged(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1646
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mPreciseCallStateRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 1647
    return-void
.end method

.method public unregisterForRedirectedNumberInfo(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 2058
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mRedirectedNumberInfoRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 2059
    return-void
.end method

.method public unregisterForResendIncallMute(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1744
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mResendIncallMuteRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 1745
    return-void
.end method

.method public unregisterForRingbackTone(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1729
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mRingbackToneRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 1730
    return-void
.end method

.method public unregisterForServiceStateChanged(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1822
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mServiceStateChangedRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 1823
    return-void
.end method

.method public unregisterForSignalInfo(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1949
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mSignalInfoRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 1950
    return-void
.end method

.method public unregisterForSubscriptionInfoReady(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 2009
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mSubscriptionInfoReadyRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 2010
    return-void
.end method

.method public unregisterForSuppServiceFailed(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1844
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mSuppServiceFailedRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 1845
    return-void
.end method

.method public unregisterForSuppServiceNotification(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1865
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mSuppServiceNotificationRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 1866
    return-void
.end method

.method public unregisterForUnknownConnection(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 1661
    iget-object v0, p0, Lcom/android/internal/telephony/CallManager;->mUnknownConnectionRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 1662
    return-void
.end method

.method public unregisterPhone(Lcom/android/internal/telephony/Phone;)V
    .registers 6
    .parameter "phone"

    #@0
    .prologue
    .line 427
    invoke-static {p1}, Lcom/android/internal/telephony/CallManager;->getPhoneBase(Lcom/android/internal/telephony/Phone;)Lcom/android/internal/telephony/Phone;

    #@3
    move-result-object v0

    #@4
    .line 429
    .local v0, basePhone:Lcom/android/internal/telephony/Phone;
    if-eqz v0, :cond_7b

    #@6
    iget-object v1, p0, Lcom/android/internal/telephony/CallManager;->mPhones:Ljava/util/ArrayList;

    #@8
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@b
    move-result v1

    #@c
    if-eqz v1, :cond_7b

    #@e
    .line 432
    const-string v1, "CallManager"

    #@10
    new-instance v2, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v3, "unregisterPhone("

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-interface {p1}, Lcom/android/internal/telephony/Phone;->getPhoneName()Ljava/lang/String;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    const-string v3, " "

    #@25
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v2

    #@29
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v2

    #@2d
    const-string v3, ")"

    #@2f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v2

    #@37
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 436
    iget-object v1, p0, Lcom/android/internal/telephony/CallManager;->mPhones:Ljava/util/ArrayList;

    #@3c
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@3f
    .line 437
    iget-object v1, p0, Lcom/android/internal/telephony/CallManager;->mRingingCalls:Ljava/util/ArrayList;

    #@41
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getRingingCall()Lcom/android/internal/telephony/Call;

    #@44
    move-result-object v2

    #@45
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@48
    .line 438
    iget-object v1, p0, Lcom/android/internal/telephony/CallManager;->mBackgroundCalls:Ljava/util/ArrayList;

    #@4a
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getBackgroundCall()Lcom/android/internal/telephony/Call;

    #@4d
    move-result-object v2

    #@4e
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@51
    .line 439
    iget-object v1, p0, Lcom/android/internal/telephony/CallManager;->mForegroundCalls:Ljava/util/ArrayList;

    #@53
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getForegroundCall()Lcom/android/internal/telephony/Call;

    #@56
    move-result-object v2

    #@57
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@5a
    .line 441
    sget-boolean v1, Lcom/android/internal/telephony/CallManager;->mIsUsingIPPhone:Z

    #@5c
    if-eqz v1, :cond_69

    #@5e
    .line 442
    instance-of v1, v0, Lcom/movial/ipphone/IPPhoneProxy;

    #@60
    if-eqz v1, :cond_69

    #@62
    .line 443
    sget-object v1, Lcom/android/internal/telephony/CallManager;->mIPPhoneProxy:Lcom/movial/ipphone/IPPhoneProxy;

    #@64
    iget-object v2, p0, Lcom/android/internal/telephony/CallManager;->mHandler:Landroid/os/Handler;

    #@66
    invoke-virtual {v1, v2}, Lcom/movial/ipphone/IPPhoneProxy;->unregisterForOn(Landroid/os/Handler;)V

    #@69
    .line 449
    :cond_69
    invoke-direct {p0, v0}, Lcom/android/internal/telephony/CallManager;->unregisterForPhoneStates(Lcom/android/internal/telephony/Phone;)V

    #@6c
    .line 450
    iget-object v1, p0, Lcom/android/internal/telephony/CallManager;->mDefaultPhone:Lcom/android/internal/telephony/Phone;

    #@6e
    if-ne v0, v1, :cond_7b

    #@70
    .line 451
    iget-object v1, p0, Lcom/android/internal/telephony/CallManager;->mPhones:Ljava/util/ArrayList;

    #@72
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    #@75
    move-result v1

    #@76
    if-eqz v1, :cond_7c

    #@78
    .line 452
    const/4 v1, 0x0

    #@79
    iput-object v1, p0, Lcom/android/internal/telephony/CallManager;->mDefaultPhone:Lcom/android/internal/telephony/Phone;

    #@7b
    .line 458
    :cond_7b
    :goto_7b
    return-void

    #@7c
    .line 454
    :cond_7c
    iget-object v1, p0, Lcom/android/internal/telephony/CallManager;->mPhones:Ljava/util/ArrayList;

    #@7e
    const/4 v2, 0x0

    #@7f
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@82
    move-result-object v1

    #@83
    check-cast v1, Lcom/android/internal/telephony/Phone;

    #@85
    iput-object v1, p0, Lcom/android/internal/telephony/CallManager;->mDefaultPhone:Lcom/android/internal/telephony/Phone;

    #@87
    goto :goto_7b
.end method
