.class Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimSpnLoaded;
.super Ljava/lang/Object;
.source "RuimRecords.java"

# interfaces
.implements Lcom/android/internal/telephony/uicc/IccRecords$IccRecordLoaded;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/uicc/RuimRecords;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EfCsimSpnLoaded"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/uicc/RuimRecords;


# direct methods
.method private constructor <init>(Lcom/android/internal/telephony/uicc/RuimRecords;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 359
    iput-object p1, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimSpnLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/internal/telephony/uicc/RuimRecords;Lcom/android/internal/telephony/uicc/RuimRecords$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 359
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimSpnLoaded;-><init>(Lcom/android/internal/telephony/uicc/RuimRecords;)V

    #@3
    return-void
.end method


# virtual methods
.method public getEfName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 361
    const-string v0, "EF_CSIM_SPN"

    #@2
    return-object v0
.end method

.method public onRecordLoaded(Landroid/os/AsyncResult;)V
    .registers 14
    .parameter "ar"

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    const/16 v4, 0x20

    #@3
    const/4 v9, 0x0

    #@4
    .line 365
    iget-object v7, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@6
    check-cast v7, [B

    #@8
    move-object v0, v7

    #@9
    check-cast v0, [B

    #@b
    .line 366
    .local v0, data:[B
    iget-object v7, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimSpnLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@d
    new-instance v10, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v11, "CSIM_SPN="

    #@14
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v10

    #@18
    invoke-static {v0}, Lcom/android/internal/telephony/uicc/IccUtils;->bytesToHexString([B)Ljava/lang/String;

    #@1b
    move-result-object v11

    #@1c
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v10

    #@20
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v10

    #@24
    invoke-virtual {v7, v10}, Lcom/android/internal/telephony/uicc/RuimRecords;->log(Ljava/lang/String;)V

    #@27
    .line 370
    iget-object v10, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimSpnLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@29
    aget-byte v7, v0, v9

    #@2b
    and-int/lit8 v7, v7, 0x1

    #@2d
    if-eqz v7, :cond_5a

    #@2f
    move v7, v8

    #@30
    :goto_30
    iput-boolean v7, v10, Lcom/android/internal/telephony/uicc/RuimRecords;->mCsimSpnDisplayCondition:Z

    #@32
    .line 372
    aget-byte v2, v0, v8

    #@34
    .line 373
    .local v2, encoding:I
    const/4 v7, 0x2

    #@35
    aget-byte v3, v0, v7

    #@37
    .line 374
    .local v3, language:I
    new-array v6, v4, [B

    #@39
    .line 375
    .local v6, spnData:[B
    array-length v7, v0

    #@3a
    add-int/lit8 v7, v7, -0x3

    #@3c
    if-ge v7, v4, :cond_41

    #@3e
    array-length v7, v0

    #@3f
    add-int/lit8 v4, v7, -0x3

    #@41
    .line 376
    .local v4, len:I
    :cond_41
    const/4 v7, 0x3

    #@42
    invoke-static {v0, v7, v6, v9, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@45
    .line 379
    const/4 v5, 0x0

    #@46
    .local v5, numBytes:I
    :goto_46
    array-length v7, v6

    #@47
    if-ge v5, v7, :cond_51

    #@49
    .line 380
    aget-byte v7, v6, v5

    #@4b
    and-int/lit16 v7, v7, 0xff

    #@4d
    const/16 v8, 0xff

    #@4f
    if-ne v7, v8, :cond_5c

    #@51
    .line 383
    :cond_51
    if-nez v5, :cond_5f

    #@53
    .line 384
    iget-object v7, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimSpnLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@55
    const-string v8, ""

    #@57
    iput-object v8, v7, Lcom/android/internal/telephony/uicc/IccRecords;->spn:Ljava/lang/String;

    #@59
    .line 422
    :goto_59
    return-void

    #@5a
    .end local v2           #encoding:I
    .end local v3           #language:I
    .end local v4           #len:I
    .end local v5           #numBytes:I
    .end local v6           #spnData:[B
    :cond_5a
    move v7, v9

    #@5b
    .line 370
    goto :goto_30

    #@5c
    .line 379
    .restart local v2       #encoding:I
    .restart local v3       #language:I
    .restart local v4       #len:I
    .restart local v5       #numBytes:I
    .restart local v6       #spnData:[B
    :cond_5c
    add-int/lit8 v5, v5, 0x1

    #@5e
    goto :goto_46

    #@5f
    .line 388
    :cond_5f
    packed-switch v2, :pswitch_data_124

    #@62
    .line 414
    :pswitch_62
    :try_start_62
    iget-object v7, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimSpnLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@64
    const-string v8, "SPN encoding not supported"

    #@66
    invoke-virtual {v7, v8}, Lcom/android/internal/telephony/uicc/RuimRecords;->log(Ljava/lang/String;)V
    :try_end_69
    .catch Ljava/lang/Exception; {:try_start_62 .. :try_end_69} :catch_b8

    #@69
    .line 419
    :goto_69
    iget-object v7, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimSpnLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@6b
    new-instance v8, Ljava/lang/StringBuilder;

    #@6d
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@70
    const-string v9, "spn="

    #@72
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v8

    #@76
    iget-object v9, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimSpnLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@78
    iget-object v9, v9, Lcom/android/internal/telephony/uicc/IccRecords;->spn:Ljava/lang/String;

    #@7a
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v8

    #@7e
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@81
    move-result-object v8

    #@82
    invoke-virtual {v7, v8}, Lcom/android/internal/telephony/uicc/RuimRecords;->log(Ljava/lang/String;)V

    #@85
    .line 420
    iget-object v7, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimSpnLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@87
    new-instance v8, Ljava/lang/StringBuilder;

    #@89
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@8c
    const-string v9, "spnCondition="

    #@8e
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    move-result-object v8

    #@92
    iget-object v9, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimSpnLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@94
    iget-boolean v9, v9, Lcom/android/internal/telephony/uicc/RuimRecords;->mCsimSpnDisplayCondition:Z

    #@96
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@99
    move-result-object v8

    #@9a
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9d
    move-result-object v8

    #@9e
    invoke-virtual {v7, v8}, Lcom/android/internal/telephony/uicc/RuimRecords;->log(Ljava/lang/String;)V

    #@a1
    .line 421
    const-string v7, "gsm.sim.operator.alpha"

    #@a3
    iget-object v8, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimSpnLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@a5
    iget-object v8, v8, Lcom/android/internal/telephony/uicc/IccRecords;->spn:Ljava/lang/String;

    #@a7
    invoke-static {v7, v8}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@aa
    goto :goto_59

    #@ab
    .line 391
    :pswitch_ab
    :try_start_ab
    iget-object v7, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimSpnLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@ad
    new-instance v8, Ljava/lang/String;

    #@af
    const/4 v9, 0x0

    #@b0
    const-string v10, "ISO-8859-1"

    #@b2
    invoke-direct {v8, v6, v9, v5, v10}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    #@b5
    iput-object v8, v7, Lcom/android/internal/telephony/uicc/IccRecords;->spn:Ljava/lang/String;
    :try_end_b7
    .catch Ljava/lang/Exception; {:try_start_ab .. :try_end_b7} :catch_b8

    #@b7
    goto :goto_69

    #@b8
    .line 416
    :catch_b8
    move-exception v1

    #@b9
    .line 417
    .local v1, e:Ljava/lang/Exception;
    iget-object v7, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimSpnLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@bb
    new-instance v8, Ljava/lang/StringBuilder;

    #@bd
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@c0
    const-string v9, "spn decode error: "

    #@c2
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c5
    move-result-object v8

    #@c6
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c9
    move-result-object v8

    #@ca
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cd
    move-result-object v8

    #@ce
    invoke-virtual {v7, v8}, Lcom/android/internal/telephony/uicc/RuimRecords;->log(Ljava/lang/String;)V

    #@d1
    goto :goto_69

    #@d2
    .line 395
    .end local v1           #e:Ljava/lang/Exception;
    :pswitch_d2
    :try_start_d2
    iget-object v7, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimSpnLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@d4
    const/4 v8, 0x0

    #@d5
    mul-int/lit8 v9, v5, 0x8

    #@d7
    div-int/lit8 v9, v9, 0x7

    #@d9
    invoke-static {v6, v8, v9}, Lcom/android/internal/telephony/GsmAlphabet;->gsm7BitPackedToString([BII)Ljava/lang/String;

    #@dc
    move-result-object v8

    #@dd
    iput-object v8, v7, Lcom/android/internal/telephony/uicc/IccRecords;->spn:Ljava/lang/String;

    #@df
    goto :goto_69

    #@e0
    .line 401
    :pswitch_e0
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@e3
    move-result-object v7

    #@e4
    const-string v8, "VZW"

    #@e6
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e9
    move-result v7

    #@ea
    if-nez v7, :cond_f8

    #@ec
    invoke-static {}, Lcom/android/internal/telephony/uicc/LGEIccUtils;->getOperator()Ljava/lang/String;

    #@ef
    move-result-object v7

    #@f0
    const-string v8, "SPR"

    #@f2
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f5
    move-result v7

    #@f6
    if-eqz v7, :cond_107

    #@f8
    .line 402
    :cond_f8
    iget-object v7, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimSpnLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@fa
    const/4 v8, 0x0

    #@fb
    mul-int/lit8 v9, v5, 0x8

    #@fd
    div-int/lit8 v9, v9, 0x7

    #@ff
    invoke-static {v6, v8, v9}, Lcom/android/internal/telephony/GsmAlphabet;->gsm7BitPackedToString([BII)Ljava/lang/String;

    #@102
    move-result-object v8

    #@103
    iput-object v8, v7, Lcom/android/internal/telephony/uicc/IccRecords;->spn:Ljava/lang/String;

    #@105
    goto/16 :goto_69

    #@107
    .line 407
    :cond_107
    iget-object v7, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimSpnLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@109
    new-instance v8, Ljava/lang/String;

    #@10b
    const/4 v9, 0x0

    #@10c
    const-string v10, "US-ASCII"

    #@10e
    invoke-direct {v8, v6, v9, v5, v10}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    #@111
    iput-object v8, v7, Lcom/android/internal/telephony/uicc/IccRecords;->spn:Ljava/lang/String;

    #@113
    goto/16 :goto_69

    #@115
    .line 411
    :pswitch_115
    iget-object v7, p0, Lcom/android/internal/telephony/uicc/RuimRecords$EfCsimSpnLoaded;->this$0:Lcom/android/internal/telephony/uicc/RuimRecords;

    #@117
    new-instance v8, Ljava/lang/String;

    #@119
    const/4 v9, 0x0

    #@11a
    const-string v10, "utf-16"

    #@11c
    invoke-direct {v8, v6, v9, v5, v10}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    #@11f
    iput-object v8, v7, Lcom/android/internal/telephony/uicc/IccRecords;->spn:Ljava/lang/String;
    :try_end_121
    .catch Ljava/lang/Exception; {:try_start_d2 .. :try_end_121} :catch_b8

    #@121
    goto/16 :goto_69

    #@123
    .line 388
    nop

    #@124
    :pswitch_data_124
    .packed-switch 0x0
        :pswitch_ab
        :pswitch_62
        :pswitch_e0
        :pswitch_d2
        :pswitch_115
        :pswitch_62
        :pswitch_62
        :pswitch_62
        :pswitch_ab
        :pswitch_d2
    .end packed-switch
.end method
