.class public final enum Lcom/android/internal/telephony/DataConnectionManager$FunctionName;
.super Ljava/lang/Enum;
.source "DataConnectionManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/DataConnectionManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "FunctionName"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/internal/telephony/DataConnectionManager$FunctionName;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

.field public static final enum KT_KAF:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

.field public static final enum MMS_MOBILE_OFF:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

.field public static final enum WHEN_DATA_DISALLOWED:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

.field public static final enum callingSetMobileDataEnabled:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

.field public static final enum debugFileWrite:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

.field public static final enum functionForPacketList:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

.field public static final enum getAlreadyAppUsedPacket:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

.field public static final enum getBlockPacketMenuProcess:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

.field public static final enum getDataNetworkMode:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

.field public static final enum getRouteList_debug:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

.field public static final enum handleSKT_QA:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

.field public static final enum setAlreadyAppUsedPacket:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

.field public static final enum setBlockPacketMenuProcess:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 96
    new-instance v0, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@7
    const-string v1, "getBlockPacketMenuProcess"

    #@9
    invoke-direct {v0, v1, v3}, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;-><init>(Ljava/lang/String;I)V

    #@c
    sput-object v0, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->getBlockPacketMenuProcess:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@e
    .line 97
    new-instance v0, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@10
    const-string v1, "getAlreadyAppUsedPacket"

    #@12
    invoke-direct {v0, v1, v4}, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;-><init>(Ljava/lang/String;I)V

    #@15
    sput-object v0, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->getAlreadyAppUsedPacket:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@17
    .line 98
    new-instance v0, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@19
    const-string v1, "getDataNetworkMode"

    #@1b
    invoke-direct {v0, v1, v5}, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;-><init>(Ljava/lang/String;I)V

    #@1e
    sput-object v0, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->getDataNetworkMode:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@20
    .line 100
    new-instance v0, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@22
    const-string v1, "KT_KAF"

    #@24
    invoke-direct {v0, v1, v6}, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;-><init>(Ljava/lang/String;I)V

    #@27
    sput-object v0, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->KT_KAF:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@29
    .line 103
    new-instance v0, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@2b
    const-string v1, "setBlockPacketMenuProcess"

    #@2d
    invoke-direct {v0, v1, v7}, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;-><init>(Ljava/lang/String;I)V

    #@30
    sput-object v0, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->setBlockPacketMenuProcess:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@32
    .line 104
    new-instance v0, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@34
    const-string v1, "setAlreadyAppUsedPacket"

    #@36
    const/4 v2, 0x5

    #@37
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;-><init>(Ljava/lang/String;I)V

    #@3a
    sput-object v0, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->setAlreadyAppUsedPacket:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@3c
    .line 105
    new-instance v0, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@3e
    const-string v1, "functionForPacketList"

    #@40
    const/4 v2, 0x6

    #@41
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;-><init>(Ljava/lang/String;I)V

    #@44
    sput-object v0, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->functionForPacketList:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@46
    .line 106
    new-instance v0, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@48
    const-string v1, "getRouteList_debug"

    #@4a
    const/4 v2, 0x7

    #@4b
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;-><init>(Ljava/lang/String;I)V

    #@4e
    sput-object v0, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->getRouteList_debug:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@50
    .line 107
    new-instance v0, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@52
    const-string v1, "handleSKT_QA"

    #@54
    const/16 v2, 0x8

    #@56
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;-><init>(Ljava/lang/String;I)V

    #@59
    sput-object v0, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->handleSKT_QA:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@5b
    .line 108
    new-instance v0, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@5d
    const-string v1, "debugFileWrite"

    #@5f
    const/16 v2, 0x9

    #@61
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;-><init>(Ljava/lang/String;I)V

    #@64
    sput-object v0, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->debugFileWrite:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@66
    .line 109
    new-instance v0, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@68
    const-string v1, "callingSetMobileDataEnabled"

    #@6a
    const/16 v2, 0xa

    #@6c
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;-><init>(Ljava/lang/String;I)V

    #@6f
    sput-object v0, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->callingSetMobileDataEnabled:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@71
    .line 111
    new-instance v0, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@73
    const-string v1, "MMS_MOBILE_OFF"

    #@75
    const/16 v2, 0xb

    #@77
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;-><init>(Ljava/lang/String;I)V

    #@7a
    sput-object v0, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->MMS_MOBILE_OFF:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@7c
    .line 114
    new-instance v0, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@7e
    const-string v1, "WHEN_DATA_DISALLOWED"

    #@80
    const/16 v2, 0xc

    #@82
    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;-><init>(Ljava/lang/String;I)V

    #@85
    sput-object v0, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->WHEN_DATA_DISALLOWED:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@87
    .line 94
    const/16 v0, 0xd

    #@89
    new-array v0, v0, [Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@8b
    sget-object v1, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->getBlockPacketMenuProcess:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@8d
    aput-object v1, v0, v3

    #@8f
    sget-object v1, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->getAlreadyAppUsedPacket:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@91
    aput-object v1, v0, v4

    #@93
    sget-object v1, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->getDataNetworkMode:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@95
    aput-object v1, v0, v5

    #@97
    sget-object v1, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->KT_KAF:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@99
    aput-object v1, v0, v6

    #@9b
    sget-object v1, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->setBlockPacketMenuProcess:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@9d
    aput-object v1, v0, v7

    #@9f
    const/4 v1, 0x5

    #@a0
    sget-object v2, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->setAlreadyAppUsedPacket:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@a2
    aput-object v2, v0, v1

    #@a4
    const/4 v1, 0x6

    #@a5
    sget-object v2, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->functionForPacketList:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@a7
    aput-object v2, v0, v1

    #@a9
    const/4 v1, 0x7

    #@aa
    sget-object v2, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->getRouteList_debug:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@ac
    aput-object v2, v0, v1

    #@ae
    const/16 v1, 0x8

    #@b0
    sget-object v2, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->handleSKT_QA:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@b2
    aput-object v2, v0, v1

    #@b4
    const/16 v1, 0x9

    #@b6
    sget-object v2, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->debugFileWrite:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@b8
    aput-object v2, v0, v1

    #@ba
    const/16 v1, 0xa

    #@bc
    sget-object v2, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->callingSetMobileDataEnabled:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@be
    aput-object v2, v0, v1

    #@c0
    const/16 v1, 0xb

    #@c2
    sget-object v2, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->MMS_MOBILE_OFF:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@c4
    aput-object v2, v0, v1

    #@c6
    const/16 v1, 0xc

    #@c8
    sget-object v2, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->WHEN_DATA_DISALLOWED:Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@ca
    aput-object v2, v0, v1

    #@cc
    sput-object v0, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->$VALUES:[Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@ce
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 94
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/internal/telephony/DataConnectionManager$FunctionName;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 94
    const-class v0, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/android/internal/telephony/DataConnectionManager$FunctionName;
    .registers 1

    #@0
    .prologue
    .line 94
    sget-object v0, Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->$VALUES:[Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@2
    invoke-virtual {v0}, [Lcom/android/internal/telephony/DataConnectionManager$FunctionName;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/android/internal/telephony/DataConnectionManager$FunctionName;

    #@8
    return-object v0
.end method
