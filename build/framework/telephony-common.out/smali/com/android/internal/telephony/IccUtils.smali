.class public Lcom/android/internal/telephony/IccUtils;
.super Ljava/lang/Object;
.source "IccUtils.java"


# static fields
.field static final LOG_TAG:Ljava/lang/String; = "IccUtils"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 33
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static adnStringFieldToString([BII)Ljava/lang/String;
    .registers 16
    .parameter "data"
    .parameter "offset"
    .parameter "length"

    #@0
    .prologue
    .line 179
    if-nez p2, :cond_5

    #@2
    .line 180
    const-string v10, ""

    #@4
    .line 265
    :goto_4
    return-object v10

    #@5
    .line 182
    :cond_5
    const/4 v10, 0x1

    #@6
    if-lt p2, v10, :cond_44

    #@8
    .line 183
    aget-byte v10, p0, p1

    #@a
    const/16 v11, -0x80

    #@c
    if-ne v10, v11, :cond_44

    #@e
    .line 184
    add-int/lit8 v10, p2, -0x1

    #@10
    div-int/lit8 v9, v10, 0x2

    #@12
    .line 185
    .local v9, ucslen:I
    const/4 v7, 0x0

    #@13
    .line 188
    .local v7, ret:Ljava/lang/String;
    :try_start_13
    new-instance v8, Ljava/lang/String;

    #@15
    add-int/lit8 v10, p1, 0x1

    #@17
    mul-int/lit8 v11, v9, 0x2

    #@19
    const-string v12, "utf-16be"

    #@1b
    invoke-direct {v8, p0, v10, v11, v12}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V
    :try_end_1e
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_13 .. :try_end_1e} :catch_35

    #@1e
    .end local v7           #ret:Ljava/lang/String;
    .local v8, ret:Ljava/lang/String;
    move-object v7, v8

    #@1f
    .line 194
    .end local v8           #ret:Ljava/lang/String;
    .restart local v7       #ret:Ljava/lang/String;
    :goto_1f
    if-eqz v7, :cond_44

    #@21
    .line 197
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    #@24
    move-result v9

    #@25
    .line 198
    :goto_25
    if-lez v9, :cond_3e

    #@27
    add-int/lit8 v10, v9, -0x1

    #@29
    invoke-virtual {v7, v10}, Ljava/lang/String;->charAt(I)C

    #@2c
    move-result v10

    #@2d
    const v11, 0xffff

    #@30
    if-ne v10, v11, :cond_3e

    #@32
    .line 199
    add-int/lit8 v9, v9, -0x1

    #@34
    goto :goto_25

    #@35
    .line 189
    :catch_35
    move-exception v3

    #@36
    .line 190
    .local v3, ex:Ljava/io/UnsupportedEncodingException;
    const-string v10, "IccUtils"

    #@38
    const-string v11, "implausible UnsupportedEncodingException"

    #@3a
    invoke-static {v10, v11, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@3d
    goto :goto_1f

    #@3e
    .line 201
    .end local v3           #ex:Ljava/io/UnsupportedEncodingException;
    :cond_3e
    const/4 v10, 0x0

    #@3f
    invoke-virtual {v7, v10, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@42
    move-result-object v10

    #@43
    goto :goto_4

    #@44
    .line 206
    .end local v7           #ret:Ljava/lang/String;
    .end local v9           #ucslen:I
    :cond_44
    const/4 v4, 0x0

    #@45
    .line 207
    .local v4, isucs2:Z
    const/4 v0, 0x0

    #@46
    .line 208
    .local v0, base:C
    const/4 v5, 0x0

    #@47
    .line 210
    .local v5, len:I
    const/4 v10, 0x3

    #@48
    if-lt p2, v10, :cond_8e

    #@4a
    aget-byte v10, p0, p1

    #@4c
    const/16 v11, -0x7f

    #@4e
    if-ne v10, v11, :cond_8e

    #@50
    .line 211
    add-int/lit8 v10, p1, 0x1

    #@52
    aget-byte v10, p0, v10

    #@54
    and-int/lit16 v5, v10, 0xff

    #@56
    .line 212
    add-int/lit8 v10, p2, -0x3

    #@58
    if-le v5, v10, :cond_5c

    #@5a
    .line 213
    add-int/lit8 v5, p2, -0x3

    #@5c
    .line 215
    :cond_5c
    add-int/lit8 v10, p1, 0x2

    #@5e
    aget-byte v10, p0, v10

    #@60
    and-int/lit16 v10, v10, 0xff

    #@62
    shl-int/lit8 v10, v10, 0x7

    #@64
    int-to-char v0, v10

    #@65
    .line 216
    add-int/lit8 p1, p1, 0x3

    #@67
    .line 217
    const/4 v4, 0x1

    #@68
    .line 229
    :cond_68
    :goto_68
    if-eqz v4, :cond_c7

    #@6a
    .line 230
    new-instance v7, Ljava/lang/StringBuilder;

    #@6c
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@6f
    .line 232
    .local v7, ret:Ljava/lang/StringBuilder;
    :goto_6f
    if-lez v5, :cond_c1

    #@71
    .line 235
    aget-byte v10, p0, p1

    #@73
    if-gez v10, :cond_82

    #@75
    .line 236
    aget-byte v10, p0, p1

    #@77
    and-int/lit8 v10, v10, 0x7f

    #@79
    add-int/2addr v10, v0

    #@7a
    int-to-char v10, v10

    #@7b
    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@7e
    .line 237
    add-int/lit8 p1, p1, 0x1

    #@80
    .line 238
    add-int/lit8 v5, v5, -0x1

    #@82
    .line 243
    :cond_82
    const/4 v1, 0x0

    #@83
    .line 244
    .local v1, count:I
    :goto_83
    if-ge v1, v5, :cond_b7

    #@85
    add-int v10, p1, v1

    #@87
    aget-byte v10, p0, v10

    #@89
    if-ltz v10, :cond_b7

    #@8b
    .line 245
    add-int/lit8 v1, v1, 0x1

    #@8d
    goto :goto_83

    #@8e
    .line 218
    .end local v1           #count:I
    .end local v7           #ret:Ljava/lang/StringBuilder;
    :cond_8e
    const/4 v10, 0x4

    #@8f
    if-lt p2, v10, :cond_68

    #@91
    aget-byte v10, p0, p1

    #@93
    const/16 v11, -0x7e

    #@95
    if-ne v10, v11, :cond_68

    #@97
    .line 219
    add-int/lit8 v10, p1, 0x1

    #@99
    aget-byte v10, p0, v10

    #@9b
    and-int/lit16 v5, v10, 0xff

    #@9d
    .line 220
    add-int/lit8 v10, p2, -0x4

    #@9f
    if-le v5, v10, :cond_a3

    #@a1
    .line 221
    add-int/lit8 v5, p2, -0x4

    #@a3
    .line 223
    :cond_a3
    add-int/lit8 v10, p1, 0x2

    #@a5
    aget-byte v10, p0, v10

    #@a7
    and-int/lit16 v10, v10, 0xff

    #@a9
    shl-int/lit8 v10, v10, 0x8

    #@ab
    add-int/lit8 v11, p1, 0x3

    #@ad
    aget-byte v11, p0, v11

    #@af
    and-int/lit16 v11, v11, 0xff

    #@b1
    or-int/2addr v10, v11

    #@b2
    int-to-char v0, v10

    #@b3
    .line 225
    add-int/lit8 p1, p1, 0x4

    #@b5
    .line 226
    const/4 v4, 0x1

    #@b6
    goto :goto_68

    #@b7
    .line 247
    .restart local v1       #count:I
    .restart local v7       #ret:Ljava/lang/StringBuilder;
    :cond_b7
    invoke-static {p0, p1, v1}, Lcom/android/internal/telephony/GsmAlphabet;->gsm8BitUnpackedToString([BII)Ljava/lang/String;

    #@ba
    move-result-object v10

    #@bb
    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@be
    .line 250
    add-int/2addr p1, v1

    #@bf
    .line 251
    sub-int/2addr v5, v1

    #@c0
    .line 252
    goto :goto_6f

    #@c1
    .line 254
    .end local v1           #count:I
    :cond_c1
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c4
    move-result-object v10

    #@c5
    goto/16 :goto_4

    #@c7
    .line 257
    .end local v7           #ret:Ljava/lang/StringBuilder;
    :cond_c7
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@ca
    move-result-object v6

    #@cb
    .line 258
    .local v6, resource:Landroid/content/res/Resources;
    const-string v2, ""

    #@cd
    .line 260
    .local v2, defaultCharset:Ljava/lang/String;
    const v10, 0x104003c

    #@d0
    :try_start_d0
    invoke-virtual {v6, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
    :try_end_d3
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_d0 .. :try_end_d3} :catch_de

    #@d3
    move-result-object v2

    #@d4
    .line 265
    :goto_d4
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@d7
    move-result-object v10

    #@d8
    invoke-static {p0, p1, p2, v10}, Lcom/android/internal/telephony/GsmAlphabet;->gsm8BitUnpackedToString([BIILjava/lang/String;)Ljava/lang/String;

    #@db
    move-result-object v10

    #@dc
    goto/16 :goto_4

    #@de
    .line 262
    :catch_de
    move-exception v10

    #@df
    goto :goto_d4
.end method

.method public static bcdToString([BII)Ljava/lang/String;
    .registers 8
    .parameter "data"
    .parameter "offset"
    .parameter "length"

    #@0
    .prologue
    const/16 v4, 0x9

    #@2
    .line 46
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    mul-int/lit8 v3, p2, 0x2

    #@6
    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    #@9
    .line 48
    .local v1, ret:Ljava/lang/StringBuilder;
    move v0, p1

    #@a
    .local v0, i:I
    :goto_a
    add-int v3, p1, p2

    #@c
    if-ge v0, v3, :cond_14

    #@e
    .line 52
    aget-byte v3, p0, v0

    #@10
    and-int/lit8 v2, v3, 0xf

    #@12
    .line 53
    .local v2, v:I
    if-le v2, v4, :cond_19

    #@14
    .line 63
    .end local v2           #v:I
    :cond_14
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v3

    #@18
    return-object v3

    #@19
    .line 54
    .restart local v2       #v:I
    :cond_19
    add-int/lit8 v3, v2, 0x30

    #@1b
    int-to-char v3, v3

    #@1c
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@1f
    .line 56
    aget-byte v3, p0, v0

    #@21
    shr-int/lit8 v3, v3, 0x4

    #@23
    and-int/lit8 v2, v3, 0xf

    #@25
    .line 58
    const/16 v3, 0xf

    #@27
    if-ne v2, v3, :cond_2c

    #@29
    .line 48
    :goto_29
    add-int/lit8 v0, v0, 0x1

    #@2b
    goto :goto_a

    #@2c
    .line 59
    :cond_2c
    if-gt v2, v4, :cond_14

    #@2e
    .line 60
    add-int/lit8 v3, v2, 0x30

    #@30
    int-to-char v3, v3

    #@31
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@34
    goto :goto_29
.end method

.method private static bitToRGB(I)I
    .registers 2
    .parameter "bit"

    #@0
    .prologue
    .line 418
    const/4 v0, 0x1

    #@1
    if-ne p0, v0, :cond_5

    #@3
    .line 419
    const/4 v0, -0x1

    #@4
    .line 421
    :goto_4
    return v0

    #@5
    :cond_5
    const/high16 v0, -0x100

    #@7
    goto :goto_4
.end method

.method public static bytesToHexString([B)Ljava/lang/String;
    .registers 5
    .parameter "bytes"

    #@0
    .prologue
    .line 315
    if-nez p0, :cond_4

    #@2
    const/4 v3, 0x0

    #@3
    .line 331
    :goto_3
    return-object v3

    #@4
    .line 317
    :cond_4
    new-instance v2, Ljava/lang/StringBuilder;

    #@6
    array-length v3, p0

    #@7
    mul-int/lit8 v3, v3, 0x2

    #@9
    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    #@c
    .line 319
    .local v2, ret:Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    #@d
    .local v1, i:I
    :goto_d
    array-length v3, p0

    #@e
    if-ge v1, v3, :cond_2f

    #@10
    .line 322
    aget-byte v3, p0, v1

    #@12
    shr-int/lit8 v3, v3, 0x4

    #@14
    and-int/lit8 v0, v3, 0xf

    #@16
    .line 324
    .local v0, b:I
    const-string v3, "0123456789abcdef"

    #@18
    invoke-virtual {v3, v0}, Ljava/lang/String;->charAt(I)C

    #@1b
    move-result v3

    #@1c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@1f
    .line 326
    aget-byte v3, p0, v1

    #@21
    and-int/lit8 v0, v3, 0xf

    #@23
    .line 328
    const-string v3, "0123456789abcdef"

    #@25
    invoke-virtual {v3, v0}, Ljava/lang/String;->charAt(I)C

    #@28
    move-result v3

    #@29
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@2c
    .line 319
    add-int/lit8 v1, v1, 0x1

    #@2e
    goto :goto_d

    #@2f
    .line 331
    .end local v0           #b:I
    :cond_2f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v3

    #@33
    goto :goto_3
.end method

.method public static cdmaBcdByteToInt(B)I
    .registers 4
    .parameter "b"

    #@0
    .prologue
    .line 128
    const/4 v0, 0x0

    #@1
    .line 131
    .local v0, ret:I
    and-int/lit16 v1, p0, 0xf0

    #@3
    const/16 v2, 0x90

    #@5
    if-gt v1, v2, :cond_d

    #@7
    .line 132
    shr-int/lit8 v1, p0, 0x4

    #@9
    and-int/lit8 v1, v1, 0xf

    #@b
    mul-int/lit8 v0, v1, 0xa

    #@d
    .line 135
    :cond_d
    and-int/lit8 v1, p0, 0xf

    #@f
    const/16 v2, 0x9

    #@11
    if-gt v1, v2, :cond_16

    #@13
    .line 136
    and-int/lit8 v1, p0, 0xf

    #@15
    add-int/2addr v0, v1

    #@16
    .line 139
    :cond_16
    return v0
.end method

.method public static cdmaBcdToString([BII)Ljava/lang/String;
    .registers 9
    .parameter "data"
    .parameter "offset"
    .parameter "length"

    #@0
    .prologue
    const/16 v5, 0x9

    #@2
    .line 71
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2, p2}, Ljava/lang/StringBuilder;-><init>(I)V

    #@7
    .line 73
    .local v2, ret:Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    #@8
    .line 74
    .local v0, count:I
    move v1, p1

    #@9
    .local v1, i:I
    :goto_9
    if-ge v0, p2, :cond_1c

    #@b
    .line 76
    aget-byte v4, p0, v1

    #@d
    and-int/lit8 v3, v4, 0xf

    #@f
    .line 77
    .local v3, v:I
    if-le v3, v5, :cond_12

    #@11
    const/4 v3, 0x0

    #@12
    .line 78
    :cond_12
    add-int/lit8 v4, v3, 0x30

    #@14
    int-to-char v4, v4

    #@15
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@18
    .line 80
    add-int/lit8 v0, v0, 0x1

    #@1a
    if-ne v0, p2, :cond_21

    #@1c
    .line 87
    .end local v3           #v:I
    :cond_1c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v4

    #@20
    return-object v4

    #@21
    .line 82
    .restart local v3       #v:I
    :cond_21
    aget-byte v4, p0, v1

    #@23
    shr-int/lit8 v4, v4, 0x4

    #@25
    and-int/lit8 v3, v4, 0xf

    #@27
    .line 83
    if-le v3, v5, :cond_2a

    #@29
    const/4 v3, 0x0

    #@2a
    .line 84
    :cond_2a
    add-int/lit8 v4, v3, 0x30

    #@2c
    int-to-char v4, v4

    #@2d
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@30
    .line 85
    add-int/lit8 v0, v0, 0x1

    #@32
    .line 74
    add-int/lit8 v1, v1, 0x1

    #@34
    goto :goto_9
.end method

.method private static getCLUT([BII)[I
    .registers 12
    .parameter "rawData"
    .parameter "offset"
    .parameter "number"

    #@0
    .prologue
    .line 513
    if-nez p0, :cond_4

    #@2
    .line 514
    const/4 v4, 0x0

    #@3
    .line 528
    :cond_3
    return-object v4

    #@4
    .line 517
    :cond_4
    new-array v4, p2, [I

    #@6
    .line 518
    .local v4, result:[I
    mul-int/lit8 v7, p2, 0x3

    #@8
    add-int v3, p1, v7

    #@a
    .line 519
    .local v3, endIndex:I
    move v5, p1

    #@b
    .line 520
    .local v5, valueIndex:I
    const/4 v1, 0x0

    #@c
    .line 521
    .local v1, colorIndex:I
    const/high16 v0, -0x100

    #@e
    .line 523
    .local v0, alpha:I
    :goto_e
    add-int/lit8 v2, v1, 0x1

    #@10
    .end local v1           #colorIndex:I
    .local v2, colorIndex:I
    add-int/lit8 v6, v5, 0x1

    #@12
    .end local v5           #valueIndex:I
    .local v6, valueIndex:I
    aget-byte v7, p0, v5

    #@14
    and-int/lit16 v7, v7, 0xff

    #@16
    shl-int/lit8 v7, v7, 0x10

    #@18
    or-int/2addr v7, v0

    #@19
    add-int/lit8 v5, v6, 0x1

    #@1b
    .end local v6           #valueIndex:I
    .restart local v5       #valueIndex:I
    aget-byte v8, p0, v6

    #@1d
    and-int/lit16 v8, v8, 0xff

    #@1f
    shl-int/lit8 v8, v8, 0x8

    #@21
    or-int/2addr v7, v8

    #@22
    add-int/lit8 v6, v5, 0x1

    #@24
    .end local v5           #valueIndex:I
    .restart local v6       #valueIndex:I
    aget-byte v8, p0, v5

    #@26
    and-int/lit16 v8, v8, 0xff

    #@28
    or-int/2addr v7, v8

    #@29
    aput v7, v4, v1

    #@2b
    .line 527
    if-ge v6, v3, :cond_3

    #@2d
    move v1, v2

    #@2e
    .end local v2           #colorIndex:I
    .restart local v1       #colorIndex:I
    move v5, v6

    #@2f
    .end local v6           #valueIndex:I
    .restart local v5       #valueIndex:I
    goto :goto_e
.end method

.method public static gsmBcdByteToInt(B)I
    .registers 4
    .parameter "b"

    #@0
    .prologue
    .line 106
    const/4 v0, 0x0

    #@1
    .line 109
    .local v0, ret:I
    and-int/lit16 v1, p0, 0xf0

    #@3
    const/16 v2, 0x90

    #@5
    if-gt v1, v2, :cond_b

    #@7
    .line 110
    shr-int/lit8 v1, p0, 0x4

    #@9
    and-int/lit8 v0, v1, 0xf

    #@b
    .line 113
    :cond_b
    and-int/lit8 v1, p0, 0xf

    #@d
    const/16 v2, 0x9

    #@f
    if-gt v1, v2, :cond_16

    #@11
    .line 114
    and-int/lit8 v1, p0, 0xf

    #@13
    mul-int/lit8 v1, v1, 0xa

    #@15
    add-int/2addr v0, v1

    #@16
    .line 117
    :cond_16
    return v0
.end method

.method static hexCharToInt(C)I
    .registers 4
    .parameter "c"

    #@0
    .prologue
    .line 270
    const/16 v0, 0x30

    #@2
    if-lt p0, v0, :cond_b

    #@4
    const/16 v0, 0x39

    #@6
    if-gt p0, v0, :cond_b

    #@8
    add-int/lit8 v0, p0, -0x30

    #@a
    .line 272
    :goto_a
    return v0

    #@b
    .line 271
    :cond_b
    const/16 v0, 0x41

    #@d
    if-lt p0, v0, :cond_18

    #@f
    const/16 v0, 0x46

    #@11
    if-gt p0, v0, :cond_18

    #@13
    add-int/lit8 v0, p0, -0x41

    #@15
    add-int/lit8 v0, v0, 0xa

    #@17
    goto :goto_a

    #@18
    .line 272
    :cond_18
    const/16 v0, 0x61

    #@1a
    if-lt p0, v0, :cond_25

    #@1c
    const/16 v0, 0x66

    #@1e
    if-gt p0, v0, :cond_25

    #@20
    add-int/lit8 v0, p0, -0x61

    #@22
    add-int/lit8 v0, v0, 0xa

    #@24
    goto :goto_a

    #@25
    .line 274
    :cond_25
    new-instance v0, Ljava/lang/RuntimeException;

    #@27
    new-instance v1, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const-string v2, "invalid hex char \'"

    #@2e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v1

    #@32
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@35
    move-result-object v1

    #@36
    const-string v2, "\'"

    #@38
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v1

    #@3c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v1

    #@40
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@43
    throw v0
.end method

.method public static hexStringToBytes(Ljava/lang/String;)[B
    .registers 7
    .parameter "s"

    #@0
    .prologue
    .line 291
    if-nez p0, :cond_4

    #@2
    const/4 v1, 0x0

    #@3
    .line 302
    :cond_3
    return-object v1

    #@4
    .line 293
    :cond_4
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@7
    move-result v2

    #@8
    .line 295
    .local v2, sz:I
    div-int/lit8 v3, v2, 0x2

    #@a
    new-array v1, v3, [B

    #@c
    .line 297
    .local v1, ret:[B
    const/4 v0, 0x0

    #@d
    .local v0, i:I
    :goto_d
    if-ge v0, v2, :cond_3

    #@f
    .line 298
    div-int/lit8 v3, v0, 0x2

    #@11
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    #@14
    move-result v4

    #@15
    invoke-static {v4}, Lcom/android/internal/telephony/IccUtils;->hexCharToInt(C)I

    #@18
    move-result v4

    #@19
    shl-int/lit8 v4, v4, 0x4

    #@1b
    add-int/lit8 v5, v0, 0x1

    #@1d
    invoke-virtual {p0, v5}, Ljava/lang/String;->charAt(I)C

    #@20
    move-result v5

    #@21
    invoke-static {v5}, Lcom/android/internal/telephony/IccUtils;->hexCharToInt(C)I

    #@24
    move-result v5

    #@25
    or-int/2addr v4, v5

    #@26
    int-to-byte v4, v4

    #@27
    aput-byte v4, v1, v3

    #@29
    .line 297
    add-int/lit8 v0, v0, 0x2

    #@2b
    goto :goto_d
.end method

.method private static mapTo2OrderBitColor([BII[II)[I
    .registers 16
    .parameter "data"
    .parameter "valueIndex"
    .parameter "length"
    .parameter "colorArray"
    .parameter "bits"

    #@0
    .prologue
    const/16 v10, 0x8

    #@2
    .line 463
    rem-int v9, v10, p4

    #@4
    if-eqz v9, :cond_12

    #@6
    .line 464
    const-string v9, "IccUtils"

    #@8
    const-string v10, "not event number of color"

    #@a
    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 465
    invoke-static {p0, p1, p2, p3, p4}, Lcom/android/internal/telephony/IccUtils;->mapToNon2OrderBitColor([BII[II)[I

    #@10
    move-result-object v2

    #@11
    .line 496
    :goto_11
    return-object v2

    #@12
    .line 469
    :cond_12
    const/4 v0, 0x1

    #@13
    .line 470
    .local v0, mask:I
    packed-switch p4, :pswitch_data_48

    #@16
    .line 485
    :goto_16
    :pswitch_16
    new-array v2, p2, [I

    #@18
    .line 486
    .local v2, resultArray:[I
    const/4 v3, 0x0

    #@19
    .line 487
    .local v3, resultIndex:I
    div-int v5, v10, p4

    #@1b
    .local v5, run:I
    move v8, p1

    #@1c
    .line 488
    .end local p1
    .local v8, valueIndex:I
    :goto_1c
    if-ge v3, p2, :cond_46

    #@1e
    .line 489
    add-int/lit8 p1, v8, 0x1

    #@20
    .end local v8           #valueIndex:I
    .restart local p1
    aget-byte v7, p0, v8

    #@22
    .line 490
    .local v7, tempByte:B
    const/4 v6, 0x0

    #@23
    .local v6, runIndex:I
    move v4, v3

    #@24
    .end local v3           #resultIndex:I
    .local v4, resultIndex:I
    :goto_24
    if-ge v6, v5, :cond_43

    #@26
    .line 491
    sub-int v9, v5, v6

    #@28
    add-int/lit8 v1, v9, -0x1

    #@2a
    .line 492
    .local v1, offset:I
    add-int/lit8 v3, v4, 0x1

    #@2c
    .end local v4           #resultIndex:I
    .restart local v3       #resultIndex:I
    mul-int v9, v1, p4

    #@2e
    shr-int v9, v7, v9

    #@30
    and-int/2addr v9, v0

    #@31
    aget v9, p3, v9

    #@33
    aput v9, v2, v4

    #@35
    .line 490
    add-int/lit8 v6, v6, 0x1

    #@37
    move v4, v3

    #@38
    .end local v3           #resultIndex:I
    .restart local v4       #resultIndex:I
    goto :goto_24

    #@39
    .line 472
    .end local v1           #offset:I
    .end local v2           #resultArray:[I
    .end local v4           #resultIndex:I
    .end local v5           #run:I
    .end local v6           #runIndex:I
    .end local v7           #tempByte:B
    :pswitch_39
    const/4 v0, 0x1

    #@3a
    .line 473
    goto :goto_16

    #@3b
    .line 475
    :pswitch_3b
    const/4 v0, 0x3

    #@3c
    .line 476
    goto :goto_16

    #@3d
    .line 478
    :pswitch_3d
    const/16 v0, 0xf

    #@3f
    .line 479
    goto :goto_16

    #@40
    .line 481
    :pswitch_40
    const/16 v0, 0xff

    #@42
    goto :goto_16

    #@43
    .restart local v2       #resultArray:[I
    .restart local v4       #resultIndex:I
    .restart local v5       #run:I
    .restart local v6       #runIndex:I
    .restart local v7       #tempByte:B
    :cond_43
    move v3, v4

    #@44
    .end local v4           #resultIndex:I
    .restart local v3       #resultIndex:I
    move v8, p1

    #@45
    .line 495
    .end local p1
    .restart local v8       #valueIndex:I
    goto :goto_1c

    #@46
    .end local v6           #runIndex:I
    .end local v7           #tempByte:B
    :cond_46
    move p1, v8

    #@47
    .line 496
    .end local v8           #valueIndex:I
    .restart local p1
    goto :goto_11

    #@48
    .line 470
    :pswitch_data_48
    .packed-switch 0x1
        :pswitch_39
        :pswitch_3b
        :pswitch_16
        :pswitch_3d
        :pswitch_16
        :pswitch_16
        :pswitch_16
        :pswitch_40
    .end packed-switch
.end method

.method private static mapToNon2OrderBitColor([BII[II)[I
    .registers 8
    .parameter "data"
    .parameter "valueIndex"
    .parameter "length"
    .parameter "colorArray"
    .parameter "bits"

    #@0
    .prologue
    .line 501
    const/16 v1, 0x8

    #@2
    rem-int/2addr v1, p4

    #@3
    if-nez v1, :cond_11

    #@5
    .line 502
    const-string v1, "IccUtils"

    #@7
    const-string v2, "not odd number of color"

    #@9
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 503
    invoke-static {p0, p1, p2, p3, p4}, Lcom/android/internal/telephony/IccUtils;->mapTo2OrderBitColor([BII[II)[I

    #@f
    move-result-object v0

    #@10
    .line 509
    :goto_10
    return-object v0

    #@11
    .line 507
    :cond_11
    new-array v0, p2, [I

    #@13
    .line 509
    .local v0, resultArray:[I
    goto :goto_10
.end method

.method public static networkNameToString([BII)Ljava/lang/String;
    .registers 10
    .parameter "data"
    .parameter "offset"
    .parameter "length"

    #@0
    .prologue
    .line 344
    aget-byte v4, p0, p1

    #@2
    and-int/lit16 v4, v4, 0x80

    #@4
    const/16 v5, 0x80

    #@6
    if-ne v4, v5, :cond_b

    #@8
    const/4 v4, 0x1

    #@9
    if-ge p2, v4, :cond_e

    #@b
    .line 345
    :cond_b
    const-string v2, ""

    #@d
    .line 382
    :cond_d
    :goto_d
    return-object v2

    #@e
    .line 348
    :cond_e
    aget-byte v4, p0, p1

    #@10
    ushr-int/lit8 v4, v4, 0x4

    #@12
    and-int/lit8 v4, v4, 0x7

    #@14
    packed-switch v4, :pswitch_data_4a

    #@17
    .line 369
    const-string v2, ""

    #@19
    .line 377
    .local v2, ret:Ljava/lang/String;
    :goto_19
    aget-byte v4, p0, p1

    #@1b
    and-int/lit8 v4, v4, 0x40

    #@1d
    if-eqz v4, :cond_d

    #@1f
    goto :goto_d

    #@20
    .line 352
    .end local v2           #ret:Ljava/lang/String;
    :pswitch_20
    aget-byte v4, p0, p1

    #@22
    and-int/lit8 v3, v4, 0x7

    #@24
    .line 353
    .local v3, unusedBits:I
    add-int/lit8 v4, p2, -0x1

    #@26
    mul-int/lit8 v4, v4, 0x8

    #@28
    sub-int/2addr v4, v3

    #@29
    div-int/lit8 v0, v4, 0x7

    #@2b
    .line 354
    .local v0, countSeptets:I
    add-int/lit8 v4, p1, 0x1

    #@2d
    invoke-static {p0, v4, v0}, Lcom/android/internal/telephony/GsmAlphabet;->gsm7BitPackedToString([BII)Ljava/lang/String;

    #@30
    move-result-object v2

    #@31
    .line 355
    .restart local v2       #ret:Ljava/lang/String;
    goto :goto_19

    #@32
    .line 359
    .end local v0           #countSeptets:I
    .end local v2           #ret:Ljava/lang/String;
    .end local v3           #unusedBits:I
    :pswitch_32
    :try_start_32
    new-instance v2, Ljava/lang/String;

    #@34
    add-int/lit8 v4, p1, 0x1

    #@36
    add-int/lit8 v5, p2, -0x1

    #@38
    const-string v6, "utf-16"

    #@3a
    invoke-direct {v2, p0, v4, v5, v6}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V
    :try_end_3d
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_32 .. :try_end_3d} :catch_3e

    #@3d
    .restart local v2       #ret:Ljava/lang/String;
    goto :goto_19

    #@3e
    .line 361
    .end local v2           #ret:Ljava/lang/String;
    :catch_3e
    move-exception v1

    #@3f
    .line 362
    .local v1, ex:Ljava/io/UnsupportedEncodingException;
    const-string v2, ""

    #@41
    .line 363
    .restart local v2       #ret:Ljava/lang/String;
    const-string v4, "IccUtils"

    #@43
    const-string v5, "implausible UnsupportedEncodingException"

    #@45
    invoke-static {v4, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@48
    goto :goto_19

    #@49
    .line 348
    nop

    #@4a
    :pswitch_data_4a
    .packed-switch 0x0
        :pswitch_20
        :pswitch_32
    .end packed-switch
.end method

.method public static parseToBnW([BI)Landroid/graphics/Bitmap;
    .registers 15
    .parameter "data"
    .parameter "length"

    #@0
    .prologue
    .line 392
    const/4 v8, 0x0

    #@1
    .line 393
    .local v8, valueIndex:I
    add-int/lit8 v9, v8, 0x1

    #@3
    .end local v8           #valueIndex:I
    .local v9, valueIndex:I
    aget-byte v11, p0, v8

    #@5
    and-int/lit16 v10, v11, 0xff

    #@7
    .line 394
    .local v10, width:I
    add-int/lit8 v8, v9, 0x1

    #@9
    .end local v9           #valueIndex:I
    .restart local v8       #valueIndex:I
    aget-byte v11, p0, v9

    #@b
    and-int/lit16 v3, v11, 0xff

    #@d
    .line 395
    .local v3, height:I
    mul-int v4, v10, v3

    #@f
    .line 397
    .local v4, numOfPixels:I
    new-array v7, v4, [I

    #@11
    .line 399
    .local v7, pixels:[I
    const/4 v5, 0x0

    #@12
    .line 400
    .local v5, pixelIndex:I
    const/4 v0, 0x7

    #@13
    .line 401
    .local v0, bitIndex:I
    const/4 v2, 0x0

    #@14
    .local v2, currentByte:B
    move v6, v5

    #@15
    .end local v5           #pixelIndex:I
    .local v6, pixelIndex:I
    move v9, v8

    #@16
    .line 402
    .end local v8           #valueIndex:I
    .restart local v9       #valueIndex:I
    :goto_16
    if-ge v6, v4, :cond_33

    #@18
    .line 404
    rem-int/lit8 v11, v6, 0x8

    #@1a
    if-nez v11, :cond_43

    #@1c
    .line 405
    add-int/lit8 v8, v9, 0x1

    #@1e
    .end local v9           #valueIndex:I
    .restart local v8       #valueIndex:I
    aget-byte v2, p0, v9

    #@20
    .line 406
    const/4 v0, 0x7

    #@21
    .line 408
    :goto_21
    add-int/lit8 v5, v6, 0x1

    #@23
    .end local v6           #pixelIndex:I
    .restart local v5       #pixelIndex:I
    add-int/lit8 v1, v0, -0x1

    #@25
    .end local v0           #bitIndex:I
    .local v1, bitIndex:I
    shr-int v11, v2, v0

    #@27
    and-int/lit8 v11, v11, 0x1

    #@29
    invoke-static {v11}, Lcom/android/internal/telephony/IccUtils;->bitToRGB(I)I

    #@2c
    move-result v11

    #@2d
    aput v11, v7, v6

    #@2f
    move v0, v1

    #@30
    .end local v1           #bitIndex:I
    .restart local v0       #bitIndex:I
    move v6, v5

    #@31
    .end local v5           #pixelIndex:I
    .restart local v6       #pixelIndex:I
    move v9, v8

    #@32
    .end local v8           #valueIndex:I
    .restart local v9       #valueIndex:I
    goto :goto_16

    #@33
    .line 411
    :cond_33
    if-eq v6, v4, :cond_3c

    #@35
    .line 412
    const-string v11, "IccUtils"

    #@37
    const-string v12, "parse end and size error"

    #@39
    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3c
    .line 414
    :cond_3c
    sget-object v11, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    #@3e
    invoke-static {v7, v10, v3, v11}, Landroid/graphics/Bitmap;->createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    #@41
    move-result-object v11

    #@42
    return-object v11

    #@43
    :cond_43
    move v8, v9

    #@44
    .end local v9           #valueIndex:I
    .restart local v8       #valueIndex:I
    goto :goto_21
.end method

.method public static parseToRGB([BIZ)Landroid/graphics/Bitmap;
    .registers 14
    .parameter "data"
    .parameter "length"
    .parameter "transparency"

    #@0
    .prologue
    .line 435
    const/4 v6, 0x0

    #@1
    .line 436
    .local v6, valueIndex:I
    add-int/lit8 v7, v6, 0x1

    #@3
    .end local v6           #valueIndex:I
    .local v7, valueIndex:I
    aget-byte v9, p0, v6

    #@5
    and-int/lit16 v8, v9, 0xff

    #@7
    .line 437
    .local v8, width:I
    add-int/lit8 v6, v7, 0x1

    #@9
    .end local v7           #valueIndex:I
    .restart local v6       #valueIndex:I
    aget-byte v9, p0, v7

    #@b
    and-int/lit16 v4, v9, 0xff

    #@d
    .line 438
    .local v4, height:I
    add-int/lit8 v7, v6, 0x1

    #@f
    .end local v6           #valueIndex:I
    .restart local v7       #valueIndex:I
    aget-byte v9, p0, v6

    #@11
    and-int/lit16 v0, v9, 0xff

    #@13
    .line 439
    .local v0, bits:I
    add-int/lit8 v6, v7, 0x1

    #@15
    .end local v7           #valueIndex:I
    .restart local v6       #valueIndex:I
    aget-byte v9, p0, v7

    #@17
    and-int/lit16 v3, v9, 0xff

    #@19
    .line 440
    .local v3, colorNumber:I
    add-int/lit8 v7, v6, 0x1

    #@1b
    .end local v6           #valueIndex:I
    .restart local v7       #valueIndex:I
    aget-byte v9, p0, v6

    #@1d
    and-int/lit16 v9, v9, 0xff

    #@1f
    shl-int/lit8 v9, v9, 0x8

    #@21
    add-int/lit8 v6, v7, 0x1

    #@23
    .end local v7           #valueIndex:I
    .restart local v6       #valueIndex:I
    aget-byte v10, p0, v7

    #@25
    and-int/lit16 v10, v10, 0xff

    #@27
    or-int v1, v9, v10

    #@29
    .line 443
    .local v1, clutOffset:I
    invoke-static {p0, v1, v3}, Lcom/android/internal/telephony/IccUtils;->getCLUT([BII)[I

    #@2c
    move-result-object v2

    #@2d
    .line 444
    .local v2, colorIndexArray:[I
    const/4 v9, 0x1

    #@2e
    if-ne v9, p2, :cond_35

    #@30
    .line 445
    add-int/lit8 v9, v3, -0x1

    #@32
    const/4 v10, 0x0

    #@33
    aput v10, v2, v9

    #@35
    .line 448
    :cond_35
    const/4 v5, 0x0

    #@36
    .line 449
    .local v5, resultArray:[I
    const/16 v9, 0x8

    #@38
    rem-int/2addr v9, v0

    #@39
    if-nez v9, :cond_48

    #@3b
    .line 450
    mul-int v9, v8, v4

    #@3d
    invoke-static {p0, v6, v9, v2, v0}, Lcom/android/internal/telephony/IccUtils;->mapTo2OrderBitColor([BII[II)[I

    #@40
    move-result-object v5

    #@41
    .line 457
    :goto_41
    sget-object v9, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    #@43
    invoke-static {v5, v8, v4, v9}, Landroid/graphics/Bitmap;->createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    #@46
    move-result-object v9

    #@47
    return-object v9

    #@48
    .line 453
    :cond_48
    mul-int v9, v8, v4

    #@4a
    invoke-static {p0, v6, v9, v2, v0}, Lcom/android/internal/telephony/IccUtils;->mapToNon2OrderBitColor([BII[II)[I

    #@4d
    move-result-object v5

    #@4e
    goto :goto_41
.end method
