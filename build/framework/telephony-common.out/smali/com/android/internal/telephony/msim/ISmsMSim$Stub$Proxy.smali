.class Lcom/android/internal/telephony/msim/ISmsMSim$Stub$Proxy;
.super Ljava/lang/Object;
.source "ISmsMSim.java"

# interfaces
.implements Lcom/android/internal/telephony/msim/ISmsMSim;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/msim/ISmsMSim$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 247
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 248
    iput-object p1, p0, Lcom/android/internal/telephony/msim/ISmsMSim$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 249
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 252
    iget-object v0, p0, Lcom/android/internal/telephony/msim/ISmsMSim$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public copyMessageToIccEf(I[B[BI)Z
    .registers 11
    .parameter "status"
    .parameter "pdu"
    .parameter "smsc"
    .parameter "subscription"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 326
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 327
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 330
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "com.android.internal.telephony.msim.ISmsMSim"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 331
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 332
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeByteArray([B)V

    #@14
    .line 333
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeByteArray([B)V

    #@17
    .line 334
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@1a
    .line 335
    iget-object v3, p0, Lcom/android/internal/telephony/msim/ISmsMSim$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1c
    const/4 v4, 0x3

    #@1d
    const/4 v5, 0x0

    #@1e
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@21
    .line 336
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@24
    .line 337
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_27
    .catchall {:try_start_9 .. :try_end_27} :catchall_32

    #@27
    move-result v3

    #@28
    if-eqz v3, :cond_2b

    #@2a
    const/4 v2, 0x1

    #@2b
    .line 340
    .local v2, _result:Z
    :cond_2b
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 341
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@31
    .line 343
    return v2

    #@32
    .line 340
    .end local v2           #_result:Z
    :catchall_32
    move-exception v3

    #@33
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@36
    .line 341
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@39
    .line 340
    throw v3
.end method

.method public disableCellBroadcast(II)Z
    .registers 9
    .parameter "messageIdentifier"
    .parameter "subscription"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 542
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 543
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 546
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "com.android.internal.telephony.msim.ISmsMSim"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 547
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 548
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 549
    iget-object v3, p0, Lcom/android/internal/telephony/msim/ISmsMSim$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@16
    const/16 v4, 0x8

    #@18
    const/4 v5, 0x0

    #@19
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1c
    .line 550
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1f
    .line 551
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_22
    .catchall {:try_start_9 .. :try_end_22} :catchall_2d

    #@22
    move-result v3

    #@23
    if-eqz v3, :cond_26

    #@25
    const/4 v2, 0x1

    #@26
    .line 554
    .local v2, _result:Z
    :cond_26
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 555
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 557
    return v2

    #@2d
    .line 554
    .end local v2           #_result:Z
    :catchall_2d
    move-exception v3

    #@2e
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@31
    .line 555
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@34
    .line 554
    throw v3
.end method

.method public disableCellBroadcastRange(III)Z
    .registers 10
    .parameter "startMessageId"
    .parameter "endMessageId"
    .parameter "subscription"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 607
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 608
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 611
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "com.android.internal.telephony.msim.ISmsMSim"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 612
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 613
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 614
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@17
    .line 615
    iget-object v3, p0, Lcom/android/internal/telephony/msim/ISmsMSim$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/16 v4, 0xa

    #@1b
    const/4 v5, 0x0

    #@1c
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1f
    .line 616
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@22
    .line 617
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_25
    .catchall {:try_start_9 .. :try_end_25} :catchall_30

    #@25
    move-result v3

    #@26
    if-eqz v3, :cond_29

    #@28
    const/4 v2, 0x1

    #@29
    .line 620
    .local v2, _result:Z
    :cond_29
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 621
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2f
    .line 623
    return v2

    #@30
    .line 620
    .end local v2           #_result:Z
    :catchall_30
    move-exception v3

    #@31
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@34
    .line 621
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@37
    .line 620
    throw v3
.end method

.method public enableCellBroadcast(II)Z
    .registers 9
    .parameter "messageIdentifier"
    .parameter "subscription"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 511
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 512
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 515
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "com.android.internal.telephony.msim.ISmsMSim"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 516
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 517
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 518
    iget-object v3, p0, Lcom/android/internal/telephony/msim/ISmsMSim$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@16
    const/4 v4, 0x7

    #@17
    const/4 v5, 0x0

    #@18
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1b
    .line 519
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1e
    .line 520
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_21
    .catchall {:try_start_9 .. :try_end_21} :catchall_2c

    #@21
    move-result v3

    #@22
    if-eqz v3, :cond_25

    #@24
    const/4 v2, 0x1

    #@25
    .line 523
    .local v2, _result:Z
    :cond_25
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 524
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 526
    return v2

    #@2c
    .line 523
    .end local v2           #_result:Z
    :catchall_2c
    move-exception v3

    #@2d
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@30
    .line 524
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@33
    .line 523
    throw v3
.end method

.method public enableCellBroadcastRange(III)Z
    .registers 10
    .parameter "startMessageId"
    .parameter "endMessageId"
    .parameter "subscription"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 574
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 575
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 578
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "com.android.internal.telephony.msim.ISmsMSim"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 579
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 580
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 581
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@17
    .line 582
    iget-object v3, p0, Lcom/android/internal/telephony/msim/ISmsMSim$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/16 v4, 0x9

    #@1b
    const/4 v5, 0x0

    #@1c
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1f
    .line 583
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@22
    .line 584
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_25
    .catchall {:try_start_9 .. :try_end_25} :catchall_30

    #@25
    move-result v3

    #@26
    if-eqz v3, :cond_29

    #@28
    const/4 v2, 0x1

    #@29
    .line 587
    .local v2, _result:Z
    :cond_29
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 588
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2f
    .line 590
    return v2

    #@30
    .line 587
    .end local v2           #_result:Z
    :catchall_30
    move-exception v3

    #@31
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@34
    .line 588
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@37
    .line 587
    throw v3
.end method

.method public getAllMessagesFromIccEf(I)Ljava/util/List;
    .registers 8
    .parameter "subscription"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/SmsRawData;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 265
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 266
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 269
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.android.internal.telephony.msim.ISmsMSim"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 270
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 271
    iget-object v3, p0, Lcom/android/internal/telephony/msim/ISmsMSim$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v4, 0x1

    #@13
    const/4 v5, 0x0

    #@14
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@17
    .line 272
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1a
    .line 273
    sget-object v3, Lcom/android/internal/telephony/SmsRawData;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1c
    invoke-virtual {v1, v3}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;
    :try_end_1f
    .catchall {:try_start_8 .. :try_end_1f} :catchall_27

    #@1f
    move-result-object v2

    #@20
    .line 276
    .local v2, _result:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/telephony/SmsRawData;>;"
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 277
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 279
    return-object v2

    #@27
    .line 276
    .end local v2           #_result:Ljava/util/List;,"Ljava/util/List<Lcom/android/internal/telephony/SmsRawData;>;"
    :catchall_27
    move-exception v3

    #@28
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 277
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 276
    throw v3
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 256
    const-string v0, "com.android.internal.telephony.msim.ISmsMSim"

    #@2
    return-object v0
.end method

.method public getPreferredSmsSubscription()I
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 631
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 632
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 635
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.android.internal.telephony.msim.ISmsMSim"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 636
    iget-object v3, p0, Lcom/android/internal/telephony/msim/ISmsMSim$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0xb

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 637
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 638
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1b
    .catchall {:try_start_8 .. :try_end_1b} :catchall_23

    #@1b
    move-result v2

    #@1c
    .line 641
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1f
    .line 642
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 644
    return v2

    #@23
    .line 641
    .end local v2           #_result:I
    :catchall_23
    move-exception v3

    #@24
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 642
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 641
    throw v3
.end method

.method public sendData(Ljava/lang/String;Ljava/lang/String;I[BLandroid/app/PendingIntent;Landroid/app/PendingIntent;I)V
    .registers 13
    .parameter "destAddr"
    .parameter "scAddr"
    .parameter "destPort"
    .parameter "data"
    .parameter "sentIntent"
    .parameter "deliveryIntent"
    .parameter "subscription"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 369
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 370
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 372
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.android.internal.telephony.msim.ISmsMSim"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 373
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 374
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@13
    .line 375
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 376
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeByteArray([B)V

    #@19
    .line 377
    if-eqz p5, :cond_41

    #@1b
    .line 378
    const/4 v2, 0x1

    #@1c
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@1f
    .line 379
    const/4 v2, 0x0

    #@20
    invoke-virtual {p5, v0, v2}, Landroid/app/PendingIntent;->writeToParcel(Landroid/os/Parcel;I)V

    #@23
    .line 384
    :goto_23
    if-eqz p6, :cond_4e

    #@25
    .line 385
    const/4 v2, 0x1

    #@26
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@29
    .line 386
    const/4 v2, 0x0

    #@2a
    invoke-virtual {p6, v0, v2}, Landroid/app/PendingIntent;->writeToParcel(Landroid/os/Parcel;I)V

    #@2d
    .line 391
    :goto_2d
    invoke-virtual {v0, p7}, Landroid/os/Parcel;->writeInt(I)V

    #@30
    .line 392
    iget-object v2, p0, Lcom/android/internal/telephony/msim/ISmsMSim$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@32
    const/4 v3, 0x4

    #@33
    const/4 v4, 0x0

    #@34
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@37
    .line 393
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_3a
    .catchall {:try_start_8 .. :try_end_3a} :catchall_46

    #@3a
    .line 396
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3d
    .line 397
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@40
    .line 399
    return-void

    #@41
    .line 382
    :cond_41
    const/4 v2, 0x0

    #@42
    :try_start_42
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_45
    .catchall {:try_start_42 .. :try_end_45} :catchall_46

    #@45
    goto :goto_23

    #@46
    .line 396
    :catchall_46
    move-exception v2

    #@47
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@4a
    .line 397
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@4d
    .line 396
    throw v2

    #@4e
    .line 389
    :cond_4e
    const/4 v2, 0x0

    #@4f
    :try_start_4f
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_52
    .catchall {:try_start_4f .. :try_end_52} :catchall_46

    #@52
    goto :goto_2d
.end method

.method public sendMultipartText(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;I)V
    .registers 12
    .parameter "destinationAddress"
    .parameter "scAddress"
    .parameter
    .parameter
    .parameter
    .parameter "subscription"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/app/PendingIntent;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/app/PendingIntent;",
            ">;I)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 479
    .local p3, parts:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .local p4, sentIntents:Ljava/util/List;,"Ljava/util/List<Landroid/app/PendingIntent;>;"
    .local p5, deliveryIntents:Ljava/util/List;,"Ljava/util/List<Landroid/app/PendingIntent;>;"
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 480
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 482
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.android.internal.telephony.msim.ISmsMSim"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 483
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 484
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@13
    .line 485
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    #@16
    .line 486
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@19
    .line 487
    invoke-virtual {v0, p5}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@1c
    .line 488
    invoke-virtual {v0, p6}, Landroid/os/Parcel;->writeInt(I)V

    #@1f
    .line 489
    iget-object v2, p0, Lcom/android/internal/telephony/msim/ISmsMSim$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@21
    const/4 v3, 0x6

    #@22
    const/4 v4, 0x0

    #@23
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@26
    .line 490
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_29
    .catchall {:try_start_8 .. :try_end_29} :catchall_30

    #@29
    .line 493
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 494
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2f
    .line 496
    return-void

    #@30
    .line 493
    :catchall_30
    move-exception v2

    #@31
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@34
    .line 494
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@37
    .line 493
    throw v2
.end method

.method public sendText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;I)V
    .registers 12
    .parameter "destAddr"
    .parameter "scAddr"
    .parameter "text"
    .parameter "sentIntent"
    .parameter "deliveryIntent"
    .parameter "subscription"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 424
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 425
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 427
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.android.internal.telephony.msim.ISmsMSim"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 428
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 429
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@13
    .line 430
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@16
    .line 431
    if-eqz p4, :cond_3e

    #@18
    .line 432
    const/4 v2, 0x1

    #@19
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@1c
    .line 433
    const/4 v2, 0x0

    #@1d
    invoke-virtual {p4, v0, v2}, Landroid/app/PendingIntent;->writeToParcel(Landroid/os/Parcel;I)V

    #@20
    .line 438
    :goto_20
    if-eqz p5, :cond_4b

    #@22
    .line 439
    const/4 v2, 0x1

    #@23
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@26
    .line 440
    const/4 v2, 0x0

    #@27
    invoke-virtual {p5, v0, v2}, Landroid/app/PendingIntent;->writeToParcel(Landroid/os/Parcel;I)V

    #@2a
    .line 445
    :goto_2a
    invoke-virtual {v0, p6}, Landroid/os/Parcel;->writeInt(I)V

    #@2d
    .line 446
    iget-object v2, p0, Lcom/android/internal/telephony/msim/ISmsMSim$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2f
    const/4 v3, 0x5

    #@30
    const/4 v4, 0x0

    #@31
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@34
    .line 447
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_37
    .catchall {:try_start_8 .. :try_end_37} :catchall_43

    #@37
    .line 450
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3a
    .line 451
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3d
    .line 453
    return-void

    #@3e
    .line 436
    :cond_3e
    const/4 v2, 0x0

    #@3f
    :try_start_3f
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_42
    .catchall {:try_start_3f .. :try_end_42} :catchall_43

    #@42
    goto :goto_20

    #@43
    .line 450
    :catchall_43
    move-exception v2

    #@44
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@47
    .line 451
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@4a
    .line 450
    throw v2

    #@4b
    .line 443
    :cond_4b
    const/4 v2, 0x0

    #@4c
    :try_start_4c
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_4f
    .catchall {:try_start_4c .. :try_end_4f} :catchall_43

    #@4f
    goto :goto_2a
.end method

.method public updateMessageOnIccEf(II[BI)Z
    .registers 11
    .parameter "messageIndex"
    .parameter "newStatus"
    .parameter "pdu"
    .parameter "subscription"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 295
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 296
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 299
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "com.android.internal.telephony.msim.ISmsMSim"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 300
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 301
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 302
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeByteArray([B)V

    #@17
    .line 303
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@1a
    .line 304
    iget-object v3, p0, Lcom/android/internal/telephony/msim/ISmsMSim$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1c
    const/4 v4, 0x2

    #@1d
    const/4 v5, 0x0

    #@1e
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@21
    .line 305
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@24
    .line 306
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_27
    .catchall {:try_start_9 .. :try_end_27} :catchall_32

    #@27
    move-result v3

    #@28
    if-eqz v3, :cond_2b

    #@2a
    const/4 v2, 0x1

    #@2b
    .line 309
    .local v2, _result:Z
    :cond_2b
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 310
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@31
    .line 312
    return v2

    #@32
    .line 309
    .end local v2           #_result:Z
    :catchall_32
    move-exception v3

    #@33
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@36
    .line 310
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@39
    .line 309
    throw v3
.end method
