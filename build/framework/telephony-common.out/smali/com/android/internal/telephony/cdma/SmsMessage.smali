.class public Lcom/android/internal/telephony/cdma/SmsMessage;
.super Lcom/android/internal/telephony/SmsMessageBase;
.source "SmsMessage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;
    }
.end annotation


# static fields
.field private static final BEARER_DATA:B = 0x8t

.field private static final BEARER_REPLY_OPTION:B = 0x6t

.field private static final CAUSE_CODES:B = 0x7t

.field private static final DESTINATION_ADDRESS:B = 0x4t

.field private static final DESTINATION_SUB_ADDRESS:B = 0x5t

.field private static final LOGGABLE_TAG:Ljava/lang/String; = "CDMA:SMS"

.field static final LOG_TAG:Ljava/lang/String; = "CDMA"

.field private static final ORIGINATING_ADDRESS:B = 0x2t

.field private static final ORIGINATING_SUB_ADDRESS:B = 0x3t

.field private static final RETURN_ACK:I = 0x1

.field private static final RETURN_NO_ACK:I = 0x0

.field private static final SERVICE_CATEGORY:B = 0x1t

.field private static final TELESERVICE_IDENTIFIER:B

.field private static mDeliverPriority:I

.field private static mDisplayMode:I

.field private static mPrivacyInd:I

.field private static mSubmitIsRoaming:Z

.field private static mSubmitPriority:I

.field private static timeSmsOnCSim:J


# instance fields
.field private mBearerData:Lcom/android/internal/telephony/cdma/sms/BearerData;

.field private mEnvelope:Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;

.field private status:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 125
    sput v2, Lcom/android/internal/telephony/cdma/SmsMessage;->mSubmitPriority:I

    #@3
    .line 126
    sput v2, Lcom/android/internal/telephony/cdma/SmsMessage;->mDeliverPriority:I

    #@5
    .line 130
    sput v2, Lcom/android/internal/telephony/cdma/SmsMessage;->mPrivacyInd:I

    #@7
    .line 142
    const/4 v0, 0x1

    #@8
    sput v0, Lcom/android/internal/telephony/cdma/SmsMessage;->mDisplayMode:I

    #@a
    .line 147
    const-wide/16 v0, 0x0

    #@c
    sput-wide v0, Lcom/android/internal/telephony/cdma/SmsMessage;->timeSmsOnCSim:J

    #@e
    .line 151
    sput-boolean v2, Lcom/android/internal/telephony/cdma/SmsMessage;->mSubmitIsRoaming:Z

    #@10
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 95
    invoke-direct {p0}, Lcom/android/internal/telephony/SmsMessageBase;-><init>()V

    #@3
    .line 154
    return-void
.end method

.method public static calculateLGLength(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    .registers 3
    .parameter "messageBody"
    .parameter "use7bitOnly"

    #@0
    .prologue
    .line 1167
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0, p1}, Lcom/android/internal/telephony/cdma/sms/BearerData;->calcTextEncodingDetails(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public static calculateLength(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    .registers 3
    .parameter "messageBody"
    .parameter "use7bitOnly"

    #@0
    .prologue
    .line 1153
    invoke-static {p0, p1}, Lcom/android/internal/telephony/cdma/sms/BearerData;->calcTextEncodingDetails(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public static calculateLengthEx(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;
    .registers 3
    .parameter "messageBody"
    .parameter "use7bitOnly"

    #@0
    .prologue
    .line 1174
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0, p1}, Lcom/android/internal/telephony/cdma/sms/BearerData;->calcTextEncodingDetailsEx(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/GsmAlphabet$TextEncodingDetails;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method private convertDtmfToAscii(B)B
    .registers 3
    .parameter "dtmfDigit"

    #@0
    .prologue
    .line 1957
    packed-switch p1, :pswitch_data_36

    #@3
    .line 1975
    const/16 v0, 0x20

    #@5
    .line 1979
    .local v0, asciiDigit:B
    :goto_5
    return v0

    #@6
    .line 1958
    .end local v0           #asciiDigit:B
    :pswitch_6
    const/16 v0, 0x44

    #@8
    .restart local v0       #asciiDigit:B
    goto :goto_5

    #@9
    .line 1959
    .end local v0           #asciiDigit:B
    :pswitch_9
    const/16 v0, 0x31

    #@b
    .restart local v0       #asciiDigit:B
    goto :goto_5

    #@c
    .line 1960
    .end local v0           #asciiDigit:B
    :pswitch_c
    const/16 v0, 0x32

    #@e
    .restart local v0       #asciiDigit:B
    goto :goto_5

    #@f
    .line 1961
    .end local v0           #asciiDigit:B
    :pswitch_f
    const/16 v0, 0x33

    #@11
    .restart local v0       #asciiDigit:B
    goto :goto_5

    #@12
    .line 1962
    .end local v0           #asciiDigit:B
    :pswitch_12
    const/16 v0, 0x34

    #@14
    .restart local v0       #asciiDigit:B
    goto :goto_5

    #@15
    .line 1963
    .end local v0           #asciiDigit:B
    :pswitch_15
    const/16 v0, 0x35

    #@17
    .restart local v0       #asciiDigit:B
    goto :goto_5

    #@18
    .line 1964
    .end local v0           #asciiDigit:B
    :pswitch_18
    const/16 v0, 0x36

    #@1a
    .restart local v0       #asciiDigit:B
    goto :goto_5

    #@1b
    .line 1965
    .end local v0           #asciiDigit:B
    :pswitch_1b
    const/16 v0, 0x37

    #@1d
    .restart local v0       #asciiDigit:B
    goto :goto_5

    #@1e
    .line 1966
    .end local v0           #asciiDigit:B
    :pswitch_1e
    const/16 v0, 0x38

    #@20
    .restart local v0       #asciiDigit:B
    goto :goto_5

    #@21
    .line 1967
    .end local v0           #asciiDigit:B
    :pswitch_21
    const/16 v0, 0x39

    #@23
    .restart local v0       #asciiDigit:B
    goto :goto_5

    #@24
    .line 1968
    .end local v0           #asciiDigit:B
    :pswitch_24
    const/16 v0, 0x30

    #@26
    .restart local v0       #asciiDigit:B
    goto :goto_5

    #@27
    .line 1969
    .end local v0           #asciiDigit:B
    :pswitch_27
    const/16 v0, 0x2a

    #@29
    .restart local v0       #asciiDigit:B
    goto :goto_5

    #@2a
    .line 1970
    .end local v0           #asciiDigit:B
    :pswitch_2a
    const/16 v0, 0x23

    #@2c
    .restart local v0       #asciiDigit:B
    goto :goto_5

    #@2d
    .line 1971
    .end local v0           #asciiDigit:B
    :pswitch_2d
    const/16 v0, 0x41

    #@2f
    .restart local v0       #asciiDigit:B
    goto :goto_5

    #@30
    .line 1972
    .end local v0           #asciiDigit:B
    :pswitch_30
    const/16 v0, 0x42

    #@32
    .restart local v0       #asciiDigit:B
    goto :goto_5

    #@33
    .line 1973
    .end local v0           #asciiDigit:B
    :pswitch_33
    const/16 v0, 0x43

    #@35
    .restart local v0       #asciiDigit:B
    goto :goto_5

    #@36
    .line 1957
    :pswitch_data_36
    .packed-switch 0x0
        :pswitch_6
        :pswitch_9
        :pswitch_c
        :pswitch_f
        :pswitch_12
        :pswitch_15
        :pswitch_18
        :pswitch_1b
        :pswitch_1e
        :pswitch_21
        :pswitch_24
        :pswitch_27
        :pswitch_2a
        :pswitch_2d
        :pswitch_30
        :pswitch_33
    .end packed-switch
.end method

.method public static createFromEfRecord(I[B)Lcom/android/internal/telephony/cdma/SmsMessage;
    .registers 14
    .parameter "index"
    .parameter "data"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 299
    :try_start_1
    new-instance v3, Lcom/android/internal/telephony/cdma/SmsMessage;

    #@3
    invoke-direct {v3}, Lcom/android/internal/telephony/cdma/SmsMessage;-><init>()V

    #@6
    .line 301
    .local v3, msg:Lcom/android/internal/telephony/cdma/SmsMessage;
    iput p0, v3, Lcom/android/internal/telephony/SmsMessageBase;->indexOnIcc:I

    #@8
    .line 307
    const/4 v9, 0x0

    #@9
    aget-byte v9, p1, v9

    #@b
    and-int/lit8 v7, v9, 0x7

    #@d
    .line 308
    .local v7, statusOnSim:I
    array-length v0, p1

    #@e
    .line 309
    .local v0, dataLength:I
    new-instance v9, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v10, "createFromEfRecord(), data total length = "

    #@15
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v9

    #@19
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v9

    #@1d
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v9

    #@21
    invoke-static {v9}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@24
    .line 310
    new-instance v9, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v10, "createFromEfRecord(), statusOnSim = "

    #@2b
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v9

    #@2f
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v9

    #@33
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v9

    #@37
    invoke-static {v9}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@3a
    .line 313
    const/4 v9, 0x0

    #@3b
    aget-byte v9, p1, v9

    #@3d
    and-int/lit8 v9, v9, 0x1

    #@3f
    if-nez v9, :cond_4a

    #@41
    .line 314
    const-string v9, "CDMA"

    #@43
    const-string v10, "SMS parsing failed: Trying to parse a free record"

    #@45
    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    move-object v3, v8

    #@49
    .line 353
    .end local v0           #dataLength:I
    .end local v3           #msg:Lcom/android/internal/telephony/cdma/SmsMessage;
    .end local v7           #statusOnSim:I
    :goto_49
    return-object v3

    #@4a
    .line 318
    .restart local v0       #dataLength:I
    .restart local v3       #msg:Lcom/android/internal/telephony/cdma/SmsMessage;
    .restart local v7       #statusOnSim:I
    :cond_4a
    const/4 v9, 0x0

    #@4b
    const-string v10, "control_uicc_storage"

    #@4d
    invoke-static {v9, v10}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@50
    move-result v9

    #@51
    if-eqz v9, :cond_a5

    #@53
    .line 319
    const/4 v9, 0x0

    #@54
    aget-byte v9, p1, v9

    #@56
    and-int/lit8 v9, v9, 0x7

    #@58
    const/4 v10, 0x7

    #@59
    if-ne v9, v10, :cond_9d

    #@5b
    .line 320
    const/4 v9, 0x5

    #@5c
    iput v9, v3, Lcom/android/internal/telephony/SmsMessageBase;->statusOnIcc:I

    #@5e
    .line 333
    :goto_5e
    const/4 v9, 0x1

    #@5f
    aget-byte v9, p1, v9

    #@61
    shr-int/lit8 v9, v9, 0x4

    #@63
    and-int/lit8 v2, v9, 0xf

    #@65
    .line 334
    .local v2, firstNumber:I
    const/4 v9, 0x1

    #@66
    aget-byte v9, p1, v9

    #@68
    and-int/lit8 v5, v9, 0xf

    #@6a
    .line 336
    .local v5, secondNumber:I
    mul-int/lit8 v9, v2, 0x10

    #@6c
    add-int v6, v9, v5

    #@6e
    .line 338
    .local v6, size:I
    new-instance v9, Ljava/lang/StringBuilder;

    #@70
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@73
    const-string v10, "createFromEfRecord(), 3GPP2 MSG_LEN = "

    #@75
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v9

    #@79
    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v9

    #@7d
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@80
    move-result-object v9

    #@81
    invoke-static {v9}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@84
    .line 343
    add-int/lit8 v9, v0, -0x2

    #@86
    new-array v4, v9, [B

    #@88
    .line 345
    .local v4, pdu:[B
    const/4 v9, 0x2

    #@89
    const/4 v10, 0x0

    #@8a
    add-int/lit8 v11, v0, -0x2

    #@8c
    invoke-static {p1, v9, v4, v10, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@8f
    .line 349
    invoke-direct {v3, v4}, Lcom/android/internal/telephony/cdma/SmsMessage;->parsePduFromEfRecord([B)V
    :try_end_92
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_92} :catch_93

    #@92
    goto :goto_49

    #@93
    .line 351
    .end local v0           #dataLength:I
    .end local v2           #firstNumber:I
    .end local v3           #msg:Lcom/android/internal/telephony/cdma/SmsMessage;
    .end local v4           #pdu:[B
    .end local v5           #secondNumber:I
    .end local v6           #size:I
    .end local v7           #statusOnSim:I
    :catch_93
    move-exception v1

    #@94
    .line 352
    .local v1, ex:Ljava/lang/RuntimeException;
    const-string v9, "CDMA"

    #@96
    const-string v10, "SMS PDU parsing failed: "

    #@98
    invoke-static {v9, v10, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@9b
    move-object v3, v8

    #@9c
    .line 353
    goto :goto_49

    #@9d
    .line 322
    .end local v1           #ex:Ljava/lang/RuntimeException;
    .restart local v0       #dataLength:I
    .restart local v3       #msg:Lcom/android/internal/telephony/cdma/SmsMessage;
    .restart local v7       #statusOnSim:I
    :cond_9d
    const/4 v9, 0x0

    #@9e
    :try_start_9e
    aget-byte v9, p1, v9

    #@a0
    and-int/lit8 v9, v9, 0x7

    #@a2
    iput v9, v3, Lcom/android/internal/telephony/SmsMessageBase;->statusOnIcc:I

    #@a4
    goto :goto_5e

    #@a5
    .line 326
    :cond_a5
    const/4 v9, 0x0

    #@a6
    aget-byte v9, p1, v9

    #@a8
    and-int/lit8 v9, v9, 0x7

    #@aa
    iput v9, v3, Lcom/android/internal/telephony/SmsMessageBase;->statusOnIcc:I
    :try_end_ac
    .catch Ljava/lang/RuntimeException; {:try_start_9e .. :try_end_ac} :catch_93

    #@ac
    goto :goto_5e
.end method

.method public static createFromPdu([B)Lcom/android/internal/telephony/cdma/SmsMessage;
    .registers 7
    .parameter "pdu"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 162
    new-instance v2, Lcom/android/internal/telephony/cdma/SmsMessage;

    #@3
    invoke-direct {v2}, Lcom/android/internal/telephony/cdma/SmsMessage;-><init>()V

    #@6
    .line 165
    .local v2, msg:Lcom/android/internal/telephony/cdma/SmsMessage;
    :try_start_6
    invoke-direct {v2, p0}, Lcom/android/internal/telephony/cdma/SmsMessage;->parsePdu([B)V
    :try_end_9
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_9} :catch_a
    .catch Ljava/lang/OutOfMemoryError; {:try_start_6 .. :try_end_9} :catch_14

    #@9
    .line 172
    .end local v2           #msg:Lcom/android/internal/telephony/cdma/SmsMessage;
    :goto_9
    return-object v2

    #@a
    .line 167
    .restart local v2       #msg:Lcom/android/internal/telephony/cdma/SmsMessage;
    :catch_a
    move-exception v1

    #@b
    .line 168
    .local v1, ex:Ljava/lang/RuntimeException;
    const-string v4, "CDMA"

    #@d
    const-string v5, "SMS PDU parsing failed: "

    #@f
    invoke-static {v4, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@12
    move-object v2, v3

    #@13
    .line 169
    goto :goto_9

    #@14
    .line 170
    .end local v1           #ex:Ljava/lang/RuntimeException;
    :catch_14
    move-exception v0

    #@15
    .line 171
    .local v0, e:Ljava/lang/OutOfMemoryError;
    const-string v4, "CDMA"

    #@17
    const-string v5, "SMS PDU parsing failed with out of memory: "

    #@19
    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1c
    move-object v2, v3

    #@1d
    .line 172
    goto :goto_9
.end method

.method private createPdu()V
    .registers 9

    #@0
    .prologue
    .line 1910
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mEnvelope:Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;

    #@2
    .line 1911
    .local v3, env:Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;
    iget-object v0, v3, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->origAddress:Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;

    #@4
    .line 1912
    .local v0, addr:Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    #@6
    const/16 v5, 0x64

    #@8
    invoke-direct {v1, v5}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    #@b
    .line 1913
    .local v1, baos:Ljava/io/ByteArrayOutputStream;
    new-instance v2, Ljava/io/DataOutputStream;

    #@d
    new-instance v5, Ljava/io/BufferedOutputStream;

    #@f
    invoke-direct {v5, v1}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    #@12
    invoke-direct {v2, v5}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    #@15
    .line 1916
    .local v2, dos:Ljava/io/DataOutputStream;
    :try_start_15
    iget v5, v3, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->messageType:I

    #@17
    invoke-virtual {v2, v5}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@1a
    .line 1917
    iget v5, v3, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->teleService:I

    #@1c
    invoke-virtual {v2, v5}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@1f
    .line 1918
    iget v5, v3, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->serviceCategory:I

    #@21
    invoke-virtual {v2, v5}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@24
    .line 1920
    iget v5, v0, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->digitMode:I

    #@26
    invoke-virtual {v2, v5}, Ljava/io/DataOutputStream;->writeByte(I)V

    #@29
    .line 1921
    iget v5, v0, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberMode:I

    #@2b
    invoke-virtual {v2, v5}, Ljava/io/DataOutputStream;->writeByte(I)V

    #@2e
    .line 1922
    iget v5, v0, Lcom/android/internal/telephony/SmsAddress;->ton:I

    #@30
    invoke-virtual {v2, v5}, Ljava/io/DataOutputStream;->writeByte(I)V

    #@33
    .line 1923
    iget v5, v0, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberPlan:I

    #@35
    invoke-virtual {v2, v5}, Ljava/io/DataOutputStream;->writeByte(I)V

    #@38
    .line 1924
    iget v5, v0, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberOfDigits:I

    #@3a
    invoke-virtual {v2, v5}, Ljava/io/DataOutputStream;->writeByte(I)V

    #@3d
    .line 1925
    iget-object v5, v0, Lcom/android/internal/telephony/SmsAddress;->origBytes:[B

    #@3f
    const/4 v6, 0x0

    #@40
    iget-object v7, v0, Lcom/android/internal/telephony/SmsAddress;->origBytes:[B

    #@42
    array-length v7, v7

    #@43
    invoke-virtual {v2, v5, v6, v7}, Ljava/io/DataOutputStream;->write([BII)V

    #@46
    .line 1927
    iget v5, v3, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->bearerReply:I

    #@48
    invoke-virtual {v2, v5}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@4b
    .line 1929
    iget-byte v5, v3, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->replySeqNo:B

    #@4d
    invoke-virtual {v2, v5}, Ljava/io/DataOutputStream;->writeByte(I)V

    #@50
    .line 1930
    iget-byte v5, v3, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->errorClass:B

    #@52
    invoke-virtual {v2, v5}, Ljava/io/DataOutputStream;->writeByte(I)V

    #@55
    .line 1931
    iget-byte v5, v3, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->causeCode:B

    #@57
    invoke-virtual {v2, v5}, Ljava/io/DataOutputStream;->writeByte(I)V

    #@5a
    .line 1933
    iget-object v5, v3, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->bearerData:[B

    #@5c
    array-length v5, v5

    #@5d
    invoke-virtual {v2, v5}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@60
    .line 1934
    iget-object v5, v3, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->bearerData:[B

    #@62
    const/4 v6, 0x0

    #@63
    iget-object v7, v3, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->bearerData:[B

    #@65
    array-length v7, v7

    #@66
    invoke-virtual {v2, v5, v6, v7}, Ljava/io/DataOutputStream;->write([BII)V

    #@69
    .line 1935
    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V

    #@6c
    .line 1945
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@6f
    move-result-object v5

    #@70
    iput-object v5, p0, Lcom/android/internal/telephony/SmsMessageBase;->mPdu:[B
    :try_end_72
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_72} :catch_73

    #@72
    .line 1949
    :goto_72
    return-void

    #@73
    .line 1946
    :catch_73
    move-exception v4

    #@74
    .line 1947
    .local v4, ex:Ljava/io/IOException;
    const-string v5, "CDMA"

    #@76
    new-instance v6, Ljava/lang/StringBuilder;

    #@78
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@7b
    const-string v7, "createPdu: conversion from object to byte array failed: "

    #@7d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v6

    #@81
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v6

    #@85
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@88
    move-result-object v6

    #@89
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@8c
    goto :goto_72
.end method

.method private static getCdmaDeliverPduSCTS(J)[B
    .registers 15
    .parameter "msgtime"

    #@0
    .prologue
    .line 794
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    #@2
    const/16 v11, 0x64

    #@4
    invoke-direct {v0, v11}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    #@7
    .line 795
    .local v0, byteoutput:Ljava/io/ByteArrayOutputStream;
    new-instance v2, Ljava/io/DataOutputStream;

    #@9
    invoke-direct {v2, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    #@c
    .line 798
    .local v2, dosbyteoutput:Ljava/io/DataOutputStream;
    new-instance v11, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v12, "getCdmaDeliverPduSCTS(), [SMS_VZW_UICC] 2. TimeZone.getDefault().getID() = ("

    #@13
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v11

    #@17
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    #@1a
    move-result-object v12

    #@1b
    invoke-virtual {v12}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    #@1e
    move-result-object v12

    #@1f
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v11

    #@23
    const-string v12, ")"

    #@25
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v11

    #@29
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v11

    #@2d
    invoke-static {v11}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@30
    .line 799
    new-instance v7, Landroid/text/format/Time;

    #@32
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    #@35
    move-result-object v11

    #@36
    invoke-virtual {v11}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    #@39
    move-result-object v11

    #@3a
    invoke-direct {v7, v11}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    #@3d
    .line 801
    .local v7, sctstime:Landroid/text/format/Time;
    invoke-virtual {v7, p0, p1}, Landroid/text/format/Time;->set(J)V

    #@40
    .line 803
    iget v11, v7, Landroid/text/format/Time;->year:I

    #@42
    const/16 v12, 0x7d0

    #@44
    if-lt v11, v12, :cond_85

    #@46
    iget v11, v7, Landroid/text/format/Time;->year:I

    #@48
    add-int/lit16 v9, v11, -0x7d0

    #@4a
    .line 804
    .local v9, year:I
    :goto_4a
    iget v11, v7, Landroid/text/format/Time;->month:I

    #@4c
    add-int/lit8 v5, v11, 0x1

    #@4e
    .line 806
    .local v5, month:I
    invoke-static {v9}, Lcom/android/internal/telephony/uicc/IccUtils;->cdmaIntTobcdByte(I)B

    #@51
    move-result v10

    #@52
    .line 807
    .local v10, yearByte:B
    invoke-static {v5}, Lcom/android/internal/telephony/uicc/IccUtils;->cdmaIntTobcdByte(I)B

    #@55
    move-result v6

    #@56
    .line 808
    .local v6, monthByte:B
    iget v11, v7, Landroid/text/format/Time;->monthDay:I

    #@58
    invoke-static {v11}, Lcom/android/internal/telephony/uicc/IccUtils;->cdmaIntTobcdByte(I)B

    #@5b
    move-result v1

    #@5c
    .line 809
    .local v1, dayByte:B
    iget v11, v7, Landroid/text/format/Time;->hour:I

    #@5e
    invoke-static {v11}, Lcom/android/internal/telephony/uicc/IccUtils;->cdmaIntTobcdByte(I)B

    #@61
    move-result v3

    #@62
    .line 810
    .local v3, hourByte:B
    iget v11, v7, Landroid/text/format/Time;->minute:I

    #@64
    invoke-static {v11}, Lcom/android/internal/telephony/uicc/IccUtils;->cdmaIntTobcdByte(I)B

    #@67
    move-result v4

    #@68
    .line 811
    .local v4, minuteByte:B
    iget v11, v7, Landroid/text/format/Time;->second:I

    #@6a
    invoke-static {v11}, Lcom/android/internal/telephony/uicc/IccUtils;->cdmaIntTobcdByte(I)B

    #@6d
    move-result v8

    #@6e
    .line 814
    .local v8, secondByte:B
    invoke-virtual {v0, v10}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@71
    .line 815
    invoke-virtual {v0, v6}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@74
    .line 816
    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@77
    .line 817
    invoke-virtual {v0, v3}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@7a
    .line 818
    invoke-virtual {v0, v4}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@7d
    .line 819
    invoke-virtual {v0, v8}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@80
    .line 821
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@83
    move-result-object v11

    #@84
    return-object v11

    #@85
    .line 803
    .end local v1           #dayByte:B
    .end local v3           #hourByte:B
    .end local v4           #minuteByte:B
    .end local v5           #month:I
    .end local v6           #monthByte:B
    .end local v8           #secondByte:B
    .end local v9           #year:I
    .end local v10           #yearByte:B
    :cond_85
    iget v11, v7, Landroid/text/format/Time;->year:I

    #@87
    add-int/lit16 v9, v11, -0x76c

    #@89
    goto :goto_4a
.end method

.method public static getDeliverPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJ[BI)Lcom/android/internal/telephony/gsm/SmsMessage$DeliverPdu;
    .registers 20
    .parameter "scAddress"
    .parameter "originatorAddress"
    .parameter "message"
    .parameter "statusReportRequested"
    .parameter "time"
    .parameter "header"
    .parameter "encodingtype"

    #@0
    .prologue
    .line 583
    if-eqz p2, :cond_4

    #@2
    if-nez p1, :cond_6

    #@4
    .line 584
    :cond_4
    const/4 v5, 0x0

    #@5
    .line 691
    :goto_5
    return-object v5

    #@6
    .line 587
    :cond_6
    new-instance v5, Lcom/android/internal/telephony/gsm/SmsMessage$DeliverPdu;

    #@8
    invoke-direct {v5}, Lcom/android/internal/telephony/gsm/SmsMessage$DeliverPdu;-><init>()V

    #@b
    .line 590
    .local v5, ret:Lcom/android/internal/telephony/gsm/SmsMessage$DeliverPdu;
    if-eqz p6, :cond_2e

    #@d
    const/16 v9, 0x40

    #@f
    :goto_f
    or-int/lit8 v9, v9, 0x0

    #@11
    int-to-byte v4, v9

    #@12
    .line 592
    .local v4, mtiByte:B
    invoke-static {p0, p1, v4, p3, v5}, Lcom/android/internal/telephony/cdma/SmsMessage;->getDeliverPduHead(Ljava/lang/String;Ljava/lang/String;BZLcom/android/internal/telephony/gsm/SmsMessage$DeliverPdu;)Ljava/io/ByteArrayOutputStream;

    #@15
    move-result-object v2

    #@16
    .line 595
    .local v2, bo:Ljava/io/ByteArrayOutputStream;
    const/4 v9, 0x1

    #@17
    move/from16 v0, p7

    #@19
    if-ne v0, v9, :cond_8c

    #@1b
    .line 601
    const/4 v9, 0x0

    #@1c
    const/4 v10, 0x0

    #@1d
    :try_start_1d
    move-object/from16 v0, p6

    #@1f
    invoke-static {p2, v0, v9, v10}, Lcom/android/internal/telephony/GsmAlphabet;->stringToGsm7BitPackedWithHeader(Ljava/lang/String;[BII)[B

    #@22
    move-result-object v8

    #@23
    .line 603
    .local v8, userData:[B
    const/4 v9, 0x0

    #@24
    aget-byte v9, v8, v9

    #@26
    and-int/lit16 v9, v9, 0xff

    #@28
    const/16 v10, 0xa0

    #@2a
    if-le v9, v10, :cond_30

    #@2c
    .line 605
    const/4 v5, 0x0

    #@2d
    goto :goto_5

    #@2e
    .line 590
    .end local v2           #bo:Ljava/io/ByteArrayOutputStream;
    .end local v4           #mtiByte:B
    .end local v8           #userData:[B
    :cond_2e
    const/4 v9, 0x0

    #@2f
    goto :goto_f

    #@30
    .line 610
    .restart local v2       #bo:Ljava/io/ByteArrayOutputStream;
    .restart local v4       #mtiByte:B
    .restart local v8       #userData:[B
    :cond_30
    const/4 v9, 0x0

    #@31
    invoke-virtual {v2, v9}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@34
    .line 612
    move-wide/from16 v0, p4

    #@36
    invoke-static {v0, v1, v2}, Lcom/android/internal/telephony/cdma/SmsMessage;->getDeliverPduSCTS(JLjava/io/ByteArrayOutputStream;)Ljava/io/ByteArrayOutputStream;

    #@39
    .line 614
    const/4 v9, 0x0

    #@3a
    array-length v10, v8

    #@3b
    invoke-virtual {v2, v8, v9, v10}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_3e
    .catch Lcom/android/internal/telephony/EncodeException; {:try_start_1d .. :try_end_3e} :catch_45

    #@3e
    .line 690
    :goto_3e
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@41
    move-result-object v9

    #@42
    iput-object v9, v5, Lcom/android/internal/telephony/SmsMessageBase$DeliverPduBase;->encodedMessage:[B

    #@44
    goto :goto_5

    #@45
    .line 615
    .end local v8           #userData:[B
    :catch_45
    move-exception v3

    #@46
    .line 621
    .local v3, ex:Lcom/android/internal/telephony/EncodeException;
    :try_start_46
    const-string v9, "utf-16be"

    #@48
    invoke-virtual {p2, v9}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_4b
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_46 .. :try_end_4b} :catch_6e

    #@4b
    move-result-object v6

    #@4c
    .line 627
    .local v6, textPart:[B
    if-eqz p6, :cond_76

    #@4e
    .line 628
    move-object/from16 v0, p6

    #@50
    array-length v9, v0

    #@51
    array-length v10, v6

    #@52
    add-int/2addr v9, v10

    #@53
    new-array v8, v9, [B

    #@55
    .line 629
    .restart local v8       #userData:[B
    const/4 v9, 0x0

    #@56
    const/4 v10, 0x0

    #@57
    move-object/from16 v0, p6

    #@59
    array-length v11, v0

    #@5a
    move-object/from16 v0, p6

    #@5c
    invoke-static {v0, v9, v8, v10, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@5f
    .line 630
    const/4 v9, 0x0

    #@60
    move-object/from16 v0, p6

    #@62
    array-length v10, v0

    #@63
    array-length v11, v6

    #@64
    invoke-static {v6, v9, v8, v10, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@67
    .line 635
    :goto_67
    array-length v9, v8

    #@68
    const/16 v10, 0x8c

    #@6a
    if-le v9, v10, :cond_78

    #@6c
    .line 637
    const/4 v5, 0x0

    #@6d
    goto :goto_5

    #@6e
    .line 622
    .end local v6           #textPart:[B
    .end local v8           #userData:[B
    :catch_6e
    move-exception v7

    #@6f
    .line 623
    .local v7, uex:Ljava/io/UnsupportedEncodingException;
    const-string v9, "getDeliverPdu(), Implausible UnsupportedEncodingException"

    #@71
    invoke-static {v9}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@74
    .line 624
    const/4 v5, 0x0

    #@75
    goto :goto_5

    #@76
    .line 632
    .end local v7           #uex:Ljava/io/UnsupportedEncodingException;
    .restart local v6       #textPart:[B
    :cond_76
    move-object v8, v6

    #@77
    .restart local v8       #userData:[B
    goto :goto_67

    #@78
    .line 642
    :cond_78
    const/16 v9, 0xb

    #@7a
    invoke-virtual {v2, v9}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@7d
    .line 645
    move-wide/from16 v0, p4

    #@7f
    invoke-static {v0, v1, v2}, Lcom/android/internal/telephony/cdma/SmsMessage;->getDeliverPduSCTS(JLjava/io/ByteArrayOutputStream;)Ljava/io/ByteArrayOutputStream;

    #@82
    .line 648
    array-length v9, v8

    #@83
    invoke-virtual {v2, v9}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@86
    .line 650
    const/4 v9, 0x0

    #@87
    array-length v10, v8

    #@88
    invoke-virtual {v2, v8, v9, v10}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    #@8b
    goto :goto_3e

    #@8c
    .line 658
    .end local v3           #ex:Lcom/android/internal/telephony/EncodeException;
    .end local v6           #textPart:[B
    .end local v8           #userData:[B
    :cond_8c
    :try_start_8c
    const-string v9, "utf-16be"

    #@8e
    invoke-virtual {p2, v9}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_91
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_8c .. :try_end_91} :catch_b5

    #@91
    move-result-object v6

    #@92
    .line 664
    .restart local v6       #textPart:[B
    if-eqz p6, :cond_be

    #@94
    .line 665
    move-object/from16 v0, p6

    #@96
    array-length v9, v0

    #@97
    array-length v10, v6

    #@98
    add-int/2addr v9, v10

    #@99
    new-array v8, v9, [B

    #@9b
    .line 666
    .restart local v8       #userData:[B
    const/4 v9, 0x0

    #@9c
    const/4 v10, 0x0

    #@9d
    move-object/from16 v0, p6

    #@9f
    array-length v11, v0

    #@a0
    move-object/from16 v0, p6

    #@a2
    invoke-static {v0, v9, v8, v10, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@a5
    .line 667
    const/4 v9, 0x0

    #@a6
    move-object/from16 v0, p6

    #@a8
    array-length v10, v0

    #@a9
    array-length v11, v6

    #@aa
    invoke-static {v6, v9, v8, v10, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@ad
    .line 672
    :goto_ad
    array-length v9, v8

    #@ae
    const/16 v10, 0x8c

    #@b0
    if-le v9, v10, :cond_c0

    #@b2
    .line 674
    const/4 v5, 0x0

    #@b3
    goto/16 :goto_5

    #@b5
    .line 659
    .end local v6           #textPart:[B
    .end local v8           #userData:[B
    :catch_b5
    move-exception v7

    #@b6
    .line 660
    .restart local v7       #uex:Ljava/io/UnsupportedEncodingException;
    const-string v9, "getDeliverPdu(), Implausible UnsupportedEncodingException"

    #@b8
    invoke-static {v9}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@bb
    .line 661
    const/4 v5, 0x0

    #@bc
    goto/16 :goto_5

    #@be
    .line 669
    .end local v7           #uex:Ljava/io/UnsupportedEncodingException;
    .restart local v6       #textPart:[B
    :cond_be
    move-object v8, v6

    #@bf
    .restart local v8       #userData:[B
    goto :goto_ad

    #@c0
    .line 679
    :cond_c0
    const/16 v9, 0xb

    #@c2
    invoke-virtual {v2, v9}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@c5
    .line 682
    move-wide/from16 v0, p4

    #@c7
    invoke-static {v0, v1, v2}, Lcom/android/internal/telephony/cdma/SmsMessage;->getDeliverPduSCTS(JLjava/io/ByteArrayOutputStream;)Ljava/io/ByteArrayOutputStream;

    #@ca
    .line 685
    array-length v9, v8

    #@cb
    invoke-virtual {v2, v9}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@ce
    .line 687
    const/4 v9, 0x0

    #@cf
    array-length v10, v8

    #@d0
    invoke-virtual {v2, v8, v9, v10}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    #@d3
    goto/16 :goto_3e
.end method

.method private static getDeliverPduHead(Ljava/lang/String;Ljava/lang/String;BZLcom/android/internal/telephony/gsm/SmsMessage$DeliverPdu;)Ljava/io/ByteArrayOutputStream;
    .registers 11
    .parameter "scAddress"
    .parameter "originatorAddress"
    .parameter "mtiByte"
    .parameter "statusReportRequested"
    .parameter "ret"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 709
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    #@3
    const/16 v2, 0xb4

    #@5
    invoke-direct {v0, v2}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    #@8
    .line 712
    .local v0, bo:Ljava/io/ByteArrayOutputStream;
    if-nez p0, :cond_39

    #@a
    .line 713
    const/4 v2, 0x0

    #@b
    iput-object v2, p4, Lcom/android/internal/telephony/SmsMessageBase$DeliverPduBase;->encodedScAddress:[B

    #@d
    .line 719
    :goto_d
    if-eqz p3, :cond_12

    #@f
    .line 721
    or-int/lit8 v2, p2, 0x20

    #@11
    int-to-byte p2, v2

    #@12
    .line 723
    :cond_12
    invoke-virtual {v0, p2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@15
    .line 726
    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->networkPortionToCalledPartyBCD(Ljava/lang/String;)[B

    #@18
    move-result-object v1

    #@19
    .line 730
    .local v1, oaBytes:[B
    if-eqz v1, :cond_38

    #@1b
    .line 731
    array-length v2, v1

    #@1c
    add-int/lit8 v2, v2, -0x1

    #@1e
    mul-int/lit8 v4, v2, 0x2

    #@20
    array-length v2, v1

    #@21
    add-int/lit8 v2, v2, -0x1

    #@23
    aget-byte v2, v1, v2

    #@25
    and-int/lit16 v2, v2, 0xf0

    #@27
    const/16 v5, 0xf0

    #@29
    if-ne v2, v5, :cond_40

    #@2b
    const/4 v2, 0x1

    #@2c
    :goto_2c
    sub-int v2, v4, v2

    #@2e
    invoke-virtual {v0, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@31
    .line 735
    array-length v2, v1

    #@32
    invoke-virtual {v0, v1, v3, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    #@35
    .line 738
    invoke-virtual {v0, v3}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@38
    .line 740
    :cond_38
    return-object v0

    #@39
    .line 715
    .end local v1           #oaBytes:[B
    :cond_39
    invoke-static {p0}, Landroid/telephony/PhoneNumberUtils;->networkPortionToCalledPartyBCDWithLength(Ljava/lang/String;)[B

    #@3c
    move-result-object v2

    #@3d
    iput-object v2, p4, Lcom/android/internal/telephony/SmsMessageBase$DeliverPduBase;->encodedScAddress:[B

    #@3f
    goto :goto_d

    #@40
    .restart local v1       #oaBytes:[B
    :cond_40
    move v2, v3

    #@41
    .line 731
    goto :goto_2c
.end method

.method private static getDeliverPduSCTS(JLjava/io/ByteArrayOutputStream;)Ljava/io/ByteArrayOutputStream;
    .registers 14
    .parameter "msgtime"
    .parameter "byteoutput"

    #@0
    .prologue
    .line 755
    new-instance v9, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v10, "getDeliverPduSCTS(), [SMS_VZW_UICC] 1. TimeZone.getDefault().getID() = ("

    #@7
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v9

    #@b
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    #@e
    move-result-object v10

    #@f
    invoke-virtual {v10}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    #@12
    move-result-object v10

    #@13
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v9

    #@17
    const-string v10, ")"

    #@19
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v9

    #@1d
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v9

    #@21
    invoke-static {v9}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@24
    .line 756
    new-instance v5, Landroid/text/format/Time;

    #@26
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    #@29
    move-result-object v9

    #@2a
    invoke-virtual {v9}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    #@2d
    move-result-object v9

    #@2e
    invoke-direct {v5, v9}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    #@31
    .line 758
    .local v5, sctstime:Landroid/text/format/Time;
    invoke-virtual {v5, p0, p1}, Landroid/text/format/Time;->set(J)V

    #@34
    .line 760
    iget v9, v5, Landroid/text/format/Time;->year:I

    #@36
    const/16 v10, 0x7d0

    #@38
    if-lt v9, v10, :cond_79

    #@3a
    iget v9, v5, Landroid/text/format/Time;->year:I

    #@3c
    add-int/lit16 v7, v9, -0x7d0

    #@3e
    .line 761
    .local v7, year:I
    :goto_3e
    iget v9, v5, Landroid/text/format/Time;->month:I

    #@40
    add-int/lit8 v3, v9, 0x1

    #@42
    .line 765
    .local v3, month:I
    invoke-static {v7}, Lcom/android/internal/telephony/uicc/IccUtils;->gsmIntTobcdByte(I)B

    #@45
    move-result v8

    #@46
    .line 766
    .local v8, yearByte:B
    invoke-static {v3}, Lcom/android/internal/telephony/uicc/IccUtils;->gsmIntTobcdByte(I)B

    #@49
    move-result v4

    #@4a
    .line 767
    .local v4, monthByte:B
    iget v9, v5, Landroid/text/format/Time;->monthDay:I

    #@4c
    invoke-static {v9}, Lcom/android/internal/telephony/uicc/IccUtils;->gsmIntTobcdByte(I)B

    #@4f
    move-result v0

    #@50
    .line 768
    .local v0, dayByte:B
    iget v9, v5, Landroid/text/format/Time;->hour:I

    #@52
    invoke-static {v9}, Lcom/android/internal/telephony/uicc/IccUtils;->gsmIntTobcdByte(I)B

    #@55
    move-result v1

    #@56
    .line 769
    .local v1, hourByte:B
    iget v9, v5, Landroid/text/format/Time;->minute:I

    #@58
    invoke-static {v9}, Lcom/android/internal/telephony/uicc/IccUtils;->gsmIntTobcdByte(I)B

    #@5b
    move-result v2

    #@5c
    .line 770
    .local v2, minuteByte:B
    iget v9, v5, Landroid/text/format/Time;->second:I

    #@5e
    invoke-static {v9}, Lcom/android/internal/telephony/uicc/IccUtils;->gsmIntTobcdByte(I)B

    #@61
    move-result v6

    #@62
    .line 774
    .local v6, secondByte:B
    invoke-virtual {p2, v8}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@65
    .line 775
    invoke-virtual {p2, v4}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@68
    .line 776
    invoke-virtual {p2, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@6b
    .line 777
    invoke-virtual {p2, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@6e
    .line 778
    invoke-virtual {p2, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@71
    .line 779
    invoke-virtual {p2, v6}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@74
    .line 780
    const/4 v9, 0x0

    #@75
    invoke-virtual {p2, v9}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@78
    .line 782
    return-object p2

    #@79
    .line 760
    .end local v0           #dayByte:B
    .end local v1           #hourByte:B
    .end local v2           #minuteByte:B
    .end local v3           #month:I
    .end local v4           #monthByte:B
    .end local v6           #secondByte:B
    .end local v7           #year:I
    .end local v8           #yearByte:B
    :cond_79
    iget v9, v5, Landroid/text/format/Time;->year:I

    #@7b
    add-int/lit16 v7, v9, -0x76c

    #@7d
    goto :goto_3e
.end method

.method public static getDomainNotiPdu(Ljava/lang/String;Ljava/lang/String;[BZLcom/android/internal/telephony/SmsHeader;)Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;
    .registers 8
    .parameter "scAddr"
    .parameter "destAddr"
    .parameter "data"
    .parameter "statusReportRequested"
    .parameter "smsHeader"

    #@0
    .prologue
    .line 2173
    if-eqz p2, :cond_4

    #@2
    if-nez p1, :cond_b

    #@4
    .line 2174
    :cond_4
    const-string v1, "getDomainNotiPdu(), [KDDI][DAN] No data or No destination Address."

    #@6
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@9
    .line 2175
    const/4 v1, 0x0

    #@a
    .line 2184
    :goto_a
    return-object v1

    #@b
    .line 2177
    :cond_b
    new-instance v0, Lcom/android/internal/telephony/cdma/sms/UserData;

    #@d
    invoke-direct {v0}, Lcom/android/internal/telephony/cdma/sms/UserData;-><init>()V

    #@10
    .line 2178
    .local v0, uData:Lcom/android/internal/telephony/cdma/sms/UserData;
    iput-object p4, v0, Lcom/android/internal/telephony/cdma/sms/UserData;->userDataHeader:Lcom/android/internal/telephony/SmsHeader;

    #@12
    .line 2179
    const/4 v1, 0x0

    #@13
    iput v1, v0, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncoding:I

    #@15
    .line 2180
    const/4 v1, 0x1

    #@16
    iput-boolean v1, v0, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncodingSet:Z

    #@18
    .line 2181
    iput-object p2, v0, Lcom/android/internal/telephony/cdma/sms/UserData;->payload:[B

    #@1a
    .line 2182
    invoke-static {p2}, Lcom/android/internal/util/HexDump;->toHexString([B)Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    iput-object v1, v0, Lcom/android/internal/telephony/cdma/sms/UserData;->payloadStr:Ljava/lang/String;

    #@20
    .line 2183
    new-instance v1, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v2, "getDomainNotiPdu(), [KDDI][DAN]userData: "

    #@27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v1

    #@2b
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/sms/UserData;->toString()Ljava/lang/String;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v1

    #@33
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v1

    #@37
    invoke-static {v1}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@3a
    .line 2184
    invoke-static {p1, p3, v0}, Lcom/android/internal/telephony/cdma/SmsMessage;->privateGetDomainNotiPdu(Ljava/lang/String;ZLcom/android/internal/telephony/cdma/sms/UserData;)Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;

    #@3d
    move-result-object v1

    #@3e
    goto :goto_a
.end method

.method public static getMessagePrivacyInd()I
    .registers 1

    #@0
    .prologue
    .line 1605
    sget v0, Lcom/android/internal/telephony/cdma/SmsMessage;->mPrivacyInd:I

    #@2
    return v0
.end method

.method static declared-synchronized getNextMessageId()I
    .registers 6

    #@0
    .prologue
    .line 1622
    const-class v3, Lcom/android/internal/telephony/cdma/SmsMessage;

    #@2
    monitor-enter v3

    #@3
    :try_start_3
    const-string v2, "persist.radio.cdma.msgid"

    #@5
    const/4 v4, 0x1

    #@6
    invoke-static {v2, v4}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@9
    move-result v0

    #@a
    .line 1623
    .local v0, msgId:I
    const v2, 0xffff

    #@d
    rem-int v2, v0, v2

    #@f
    add-int/lit8 v2, v2, 0x1

    #@11
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    .line 1624
    .local v1, nextMsgId:Ljava/lang/String;
    const-string v2, "persist.radio.cdma.msgid"

    #@17
    invoke-static {v2, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@1a
    .line 1625
    const-string v2, "CDMA:SMS"

    #@1c
    const/4 v4, 0x2

    #@1d
    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@20
    move-result v2

    #@21
    if-eqz v2, :cond_59

    #@23
    .line 1626
    const-string v2, "CDMA"

    #@25
    new-instance v4, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    const-string v5, "next persist.radio.cdma.msgid = "

    #@2c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v4

    #@30
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v4

    #@34
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v4

    #@38
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 1627
    const-string v2, "CDMA"

    #@3d
    new-instance v4, Ljava/lang/StringBuilder;

    #@3f
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@42
    const-string v5, "readback gets "

    #@44
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v4

    #@48
    const-string v5, "persist.radio.cdma.msgid"

    #@4a
    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@4d
    move-result-object v5

    #@4e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v4

    #@52
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@55
    move-result-object v4

    #@56
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_59
    .catchall {:try_start_3 .. :try_end_59} :catchall_5b

    #@59
    .line 1630
    :cond_59
    monitor-exit v3

    #@5a
    return v0

    #@5b
    .line 1622
    .end local v1           #nextMsgId:Ljava/lang/String;
    :catchall_5b
    move-exception v2

    #@5c
    monitor-exit v3

    #@5d
    throw v2
.end method

.method public static getSmsIsRoaming()Z
    .registers 2

    #@0
    .prologue
    .line 2133
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "getSmsIsRoaming(), mSubmitIsRoaming = "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    sget-boolean v1, Lcom/android/internal/telephony/cdma/SmsMessage;->mSubmitIsRoaming:Z

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@18
    .line 2134
    sget-boolean v0, Lcom/android/internal/telephony/cdma/SmsMessage;->mSubmitIsRoaming:Z

    #@1a
    return v0
.end method

.method public static getSmsPriority()I
    .registers 1

    #@0
    .prologue
    .line 2041
    sget v0, Lcom/android/internal/telephony/cdma/SmsMessage;->mDeliverPriority:I

    #@2
    return v0
.end method

.method public static getSubmitPdu(Ljava/lang/String;Lcom/android/internal/telephony/cdma/sms/UserData;Z)Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;
    .registers 4
    .parameter "destAddr"
    .parameter "userData"
    .parameter "statusReportRequested"

    #@0
    .prologue
    .line 480
    invoke-static {p0, p2, p1}, Lcom/android/internal/telephony/cdma/SmsMessage;->privateGetSubmitPdu(Ljava/lang/String;ZLcom/android/internal/telephony/cdma/sms/UserData;)Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public static getSubmitPdu(Ljava/lang/String;Lcom/android/internal/telephony/cdma/sms/UserData;ZLjava/lang/String;)Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;
    .registers 5
    .parameter "destAddr"
    .parameter "userData"
    .parameter "statusReportRequested"
    .parameter "cbAddress"

    #@0
    .prologue
    .line 565
    invoke-static {p0, p2, p1, p3}, Lcom/android/internal/telephony/cdma/SmsMessage;->privateGetSubmitPdu(Ljava/lang/String;ZLcom/android/internal/telephony/cdma/sms/UserData;Ljava/lang/String;)Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public static getSubmitPdu(Ljava/lang/String;Ljava/lang/String;I[BZ)Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;
    .registers 9
    .parameter "scAddr"
    .parameter "destAddr"
    .parameter "destPort"
    .parameter "data"
    .parameter "statusReportRequested"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 451
    new-instance v0, Lcom/android/internal/telephony/SmsHeader$PortAddrs;

    #@3
    invoke-direct {v0}, Lcom/android/internal/telephony/SmsHeader$PortAddrs;-><init>()V

    #@6
    .line 452
    .local v0, portAddrs:Lcom/android/internal/telephony/SmsHeader$PortAddrs;
    iput p2, v0, Lcom/android/internal/telephony/SmsHeader$PortAddrs;->destPort:I

    #@8
    .line 453
    iput v3, v0, Lcom/android/internal/telephony/SmsHeader$PortAddrs;->origPort:I

    #@a
    .line 454
    iput-boolean v3, v0, Lcom/android/internal/telephony/SmsHeader$PortAddrs;->areEightBits:Z

    #@c
    .line 456
    new-instance v1, Lcom/android/internal/telephony/SmsHeader;

    #@e
    invoke-direct {v1}, Lcom/android/internal/telephony/SmsHeader;-><init>()V

    #@11
    .line 457
    .local v1, smsHeader:Lcom/android/internal/telephony/SmsHeader;
    iput-object v0, v1, Lcom/android/internal/telephony/SmsHeader;->portAddrs:Lcom/android/internal/telephony/SmsHeader$PortAddrs;

    #@13
    .line 459
    new-instance v2, Lcom/android/internal/telephony/cdma/sms/UserData;

    #@15
    invoke-direct {v2}, Lcom/android/internal/telephony/cdma/sms/UserData;-><init>()V

    #@18
    .line 460
    .local v2, uData:Lcom/android/internal/telephony/cdma/sms/UserData;
    iput-object v1, v2, Lcom/android/internal/telephony/cdma/sms/UserData;->userDataHeader:Lcom/android/internal/telephony/SmsHeader;

    #@1a
    .line 461
    iput v3, v2, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncoding:I

    #@1c
    .line 462
    const/4 v3, 0x1

    #@1d
    iput-boolean v3, v2, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncodingSet:Z

    #@1f
    .line 463
    iput-object p3, v2, Lcom/android/internal/telephony/cdma/sms/UserData;->payload:[B

    #@21
    .line 465
    invoke-static {p1, p4, v2}, Lcom/android/internal/telephony/cdma/SmsMessage;->privateGetSubmitPdu(Ljava/lang/String;ZLcom/android/internal/telephony/cdma/sms/UserData;)Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;

    #@24
    move-result-object v3

    #@25
    return-object v3
.end method

.method public static getSubmitPdu(Ljava/lang/String;Ljava/lang/String;I[BZLjava/lang/String;)Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;
    .registers 10
    .parameter "scAddr"
    .parameter "destAddr"
    .parameter "destPort"
    .parameter "data"
    .parameter "statusReportRequested"
    .parameter "cbAddress"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 537
    new-instance v0, Lcom/android/internal/telephony/SmsHeader$PortAddrs;

    #@3
    invoke-direct {v0}, Lcom/android/internal/telephony/SmsHeader$PortAddrs;-><init>()V

    #@6
    .line 538
    .local v0, portAddrs:Lcom/android/internal/telephony/SmsHeader$PortAddrs;
    iput p2, v0, Lcom/android/internal/telephony/SmsHeader$PortAddrs;->destPort:I

    #@8
    .line 539
    iput v3, v0, Lcom/android/internal/telephony/SmsHeader$PortAddrs;->origPort:I

    #@a
    .line 540
    iput-boolean v3, v0, Lcom/android/internal/telephony/SmsHeader$PortAddrs;->areEightBits:Z

    #@c
    .line 542
    new-instance v1, Lcom/android/internal/telephony/SmsHeader;

    #@e
    invoke-direct {v1}, Lcom/android/internal/telephony/SmsHeader;-><init>()V

    #@11
    .line 543
    .local v1, smsHeader:Lcom/android/internal/telephony/SmsHeader;
    iput-object v0, v1, Lcom/android/internal/telephony/SmsHeader;->portAddrs:Lcom/android/internal/telephony/SmsHeader$PortAddrs;

    #@13
    .line 545
    new-instance v2, Lcom/android/internal/telephony/cdma/sms/UserData;

    #@15
    invoke-direct {v2}, Lcom/android/internal/telephony/cdma/sms/UserData;-><init>()V

    #@18
    .line 546
    .local v2, uData:Lcom/android/internal/telephony/cdma/sms/UserData;
    iput-object v1, v2, Lcom/android/internal/telephony/cdma/sms/UserData;->userDataHeader:Lcom/android/internal/telephony/SmsHeader;

    #@1a
    .line 547
    iput v3, v2, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncoding:I

    #@1c
    .line 548
    const/4 v3, 0x1

    #@1d
    iput-boolean v3, v2, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncodingSet:Z

    #@1f
    .line 549
    iput-object p3, v2, Lcom/android/internal/telephony/cdma/sms/UserData;->payload:[B

    #@21
    .line 551
    invoke-static {p1, p4, v2, p5}, Lcom/android/internal/telephony/cdma/SmsMessage;->privateGetSubmitPdu(Ljava/lang/String;ZLcom/android/internal/telephony/cdma/sms/UserData;Ljava/lang/String;)Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;

    #@24
    move-result-object v3

    #@25
    return-object v3
.end method

.method public static getSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/android/internal/telephony/SmsHeader;)Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;
    .registers 7
    .parameter "scAddr"
    .parameter "destAddr"
    .parameter "message"
    .parameter "statusReportRequested"
    .parameter "smsHeader"

    #@0
    .prologue
    .line 419
    if-eqz p2, :cond_4

    #@2
    if-nez p1, :cond_6

    #@4
    .line 420
    :cond_4
    const/4 v1, 0x0

    #@5
    .line 426
    :goto_5
    return-object v1

    #@6
    .line 423
    :cond_6
    new-instance v0, Lcom/android/internal/telephony/cdma/sms/UserData;

    #@8
    invoke-direct {v0}, Lcom/android/internal/telephony/cdma/sms/UserData;-><init>()V

    #@b
    .line 424
    .local v0, uData:Lcom/android/internal/telephony/cdma/sms/UserData;
    iput-object p2, v0, Lcom/android/internal/telephony/cdma/sms/UserData;->payloadStr:Ljava/lang/String;

    #@d
    .line 425
    iput-object p4, v0, Lcom/android/internal/telephony/cdma/sms/UserData;->userDataHeader:Lcom/android/internal/telephony/SmsHeader;

    #@f
    .line 426
    invoke-static {p1, p3, v0}, Lcom/android/internal/telephony/cdma/SmsMessage;->privateGetSubmitPdu(Ljava/lang/String;ZLcom/android/internal/telephony/cdma/sms/UserData;)Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;

    #@12
    move-result-object v1

    #@13
    goto :goto_5
.end method

.method public static getSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/android/internal/telephony/SmsHeader;Ljava/lang/String;)Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;
    .registers 8
    .parameter "scAddr"
    .parameter "destAddr"
    .parameter "message"
    .parameter "statusReportRequested"
    .parameter "smsHeader"
    .parameter "cbAddress"

    #@0
    .prologue
    .line 505
    if-eqz p2, :cond_4

    #@2
    if-nez p1, :cond_6

    #@4
    .line 506
    :cond_4
    const/4 v1, 0x0

    #@5
    .line 512
    :goto_5
    return-object v1

    #@6
    .line 509
    :cond_6
    new-instance v0, Lcom/android/internal/telephony/cdma/sms/UserData;

    #@8
    invoke-direct {v0}, Lcom/android/internal/telephony/cdma/sms/UserData;-><init>()V

    #@b
    .line 510
    .local v0, uData:Lcom/android/internal/telephony/cdma/sms/UserData;
    iput-object p2, v0, Lcom/android/internal/telephony/cdma/sms/UserData;->payloadStr:Ljava/lang/String;

    #@d
    .line 511
    iput-object p4, v0, Lcom/android/internal/telephony/cdma/sms/UserData;->userDataHeader:Lcom/android/internal/telephony/SmsHeader;

    #@f
    .line 512
    invoke-static {p1, p3, v0, p5}, Lcom/android/internal/telephony/cdma/SmsMessage;->privateGetSubmitPdu(Ljava/lang/String;ZLcom/android/internal/telephony/cdma/sms/UserData;Ljava/lang/String;)Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;

    #@12
    move-result-object v1

    #@13
    goto :goto_5
.end method

.method public static getTPLayerLengthForPDU(Ljava/lang/String;)I
    .registers 3
    .parameter "pdu"

    #@0
    .prologue
    .line 384
    const-string v0, "CDMA"

    #@2
    const-string v1, "getTPLayerLengthForPDU: is not supported in CDMA mode."

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 385
    const/4 v0, 0x0

    #@8
    return v0
.end method

.method public static getTimeforSMSonCSim()J
    .registers 2

    #@0
    .prologue
    .line 376
    sget-wide v0, Lcom/android/internal/telephony/cdma/SmsMessage;->timeSmsOnCSim:J

    #@2
    return-wide v0
.end method

.method public static isKSC5601Encoding()Z
    .registers 3

    #@0
    .prologue
    .line 2161
    const/4 v1, 0x0

    #@1
    const-string v2, "KSC5601EncodingUSCDMA"

    #@3
    invoke-static {v1, v2}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@6
    move-result v1

    #@7
    if-nez v1, :cond_b

    #@9
    .line 2162
    const/4 v1, 0x0

    #@a
    .line 2167
    .local v0, smsState:Lcom/android/internal/telephony/cdma/CdmaSmsStateQeTest;
    :goto_a
    return v1

    #@b
    .line 2165
    .end local v0           #smsState:Lcom/android/internal/telephony/cdma/CdmaSmsStateQeTest;
    :cond_b
    new-instance v0, Lcom/android/internal/telephony/cdma/CdmaSmsStateQeTest;

    #@d
    invoke-direct {v0}, Lcom/android/internal/telephony/cdma/CdmaSmsStateQeTest;-><init>()V

    #@10
    .line 2167
    .restart local v0       #smsState:Lcom/android/internal/telephony/cdma/CdmaSmsStateQeTest;
    invoke-virtual {v0}, Lcom/android/internal/telephony/cdma/CdmaSmsStateQeTest;->isTestInKorea()Z

    #@13
    move-result v1

    #@14
    goto :goto_a
.end method

.method public static newFromParcel(Landroid/os/Parcel;)Lcom/android/internal/telephony/cdma/SmsMessage;
    .registers 15
    .parameter "p"

    #@0
    .prologue
    const/16 v13, 0x2b

    #@2
    const/4 v12, 0x1

    #@3
    const/4 v11, 0x0

    #@4
    .line 182
    new-instance v7, Lcom/android/internal/telephony/cdma/SmsMessage;

    #@6
    invoke-direct {v7}, Lcom/android/internal/telephony/cdma/SmsMessage;-><init>()V

    #@9
    .line 183
    .local v7, msg:Lcom/android/internal/telephony/cdma/SmsMessage;
    new-instance v5, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;

    #@b
    invoke-direct {v5}, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;-><init>()V

    #@e
    .line 184
    .local v5, env:Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;
    new-instance v0, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;

    #@10
    invoke-direct {v0}, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;-><init>()V

    #@13
    .line 185
    .local v0, addr:Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;
    new-instance v8, Lcom/android/internal/telephony/cdma/sms/CdmaSmsSubaddress;

    #@15
    invoke-direct {v8}, Lcom/android/internal/telephony/cdma/sms/CdmaSmsSubaddress;-><init>()V

    #@18
    .line 192
    .local v8, subaddr:Lcom/android/internal/telephony/cdma/sms/CdmaSmsSubaddress;
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@1b
    move-result v10

    #@1c
    iput v10, v5, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->teleService:I

    #@1e
    .line 194
    invoke-virtual {p0}, Landroid/os/Parcel;->readByte()B

    #@21
    move-result v10

    #@22
    if-eqz v10, :cond_6b

    #@24
    .line 195
    iput v12, v5, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->messageType:I

    #@26
    .line 205
    :goto_26
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@29
    move-result v10

    #@2a
    iput v10, v5, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->serviceCategory:I

    #@2c
    .line 208
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@2f
    move-result v1

    #@30
    .line 209
    .local v1, addressDigitMode:I
    and-int/lit16 v10, v1, 0xff

    #@32
    int-to-byte v10, v10

    #@33
    iput v10, v0, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->digitMode:I

    #@35
    .line 210
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@38
    move-result v10

    #@39
    and-int/lit16 v10, v10, 0xff

    #@3b
    int-to-byte v10, v10

    #@3c
    iput v10, v0, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberMode:I

    #@3e
    .line 211
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@41
    move-result v10

    #@42
    iput v10, v0, Lcom/android/internal/telephony/SmsAddress;->ton:I

    #@44
    .line 212
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@47
    move-result v10

    #@48
    and-int/lit16 v10, v10, 0xff

    #@4a
    int-to-byte v10, v10

    #@4b
    iput v10, v0, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberPlan:I

    #@4d
    .line 213
    invoke-virtual {p0}, Landroid/os/Parcel;->readByte()B

    #@50
    move-result v2

    #@51
    .line 214
    .local v2, count:B
    iput v2, v0, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberOfDigits:I

    #@53
    .line 215
    new-array v4, v2, [B

    #@55
    .line 217
    .local v4, data:[B
    const/4 v6, 0x0

    #@56
    .local v6, index:I
    :goto_56
    if-ge v6, v2, :cond_76

    #@58
    .line 218
    invoke-virtual {p0}, Landroid/os/Parcel;->readByte()B

    #@5b
    move-result v10

    #@5c
    aput-byte v10, v4, v6

    #@5e
    .line 221
    if-nez v1, :cond_68

    #@60
    .line 222
    aget-byte v10, v4, v6

    #@62
    invoke-direct {v7, v10}, Lcom/android/internal/telephony/cdma/SmsMessage;->convertDtmfToAscii(B)B

    #@65
    move-result v10

    #@66
    aput-byte v10, v4, v6

    #@68
    .line 217
    :cond_68
    add-int/lit8 v6, v6, 0x1

    #@6a
    goto :goto_56

    #@6b
    .line 198
    .end local v1           #addressDigitMode:I
    .end local v2           #count:B
    .end local v4           #data:[B
    .end local v6           #index:I
    :cond_6b
    iget v10, v5, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->teleService:I

    #@6d
    if-nez v10, :cond_73

    #@6f
    .line 200
    const/4 v10, 0x2

    #@70
    iput v10, v5, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->messageType:I

    #@72
    goto :goto_26

    #@73
    .line 202
    :cond_73
    iput v11, v5, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->messageType:I

    #@75
    goto :goto_26

    #@76
    .line 226
    .restart local v1       #addressDigitMode:I
    .restart local v2       #count:B
    .restart local v4       #data:[B
    .restart local v6       #index:I
    :cond_76
    iput-object v4, v0, Lcom/android/internal/telephony/SmsAddress;->origBytes:[B

    #@78
    .line 228
    iget v10, v0, Lcom/android/internal/telephony/SmsAddress;->ton:I

    #@7a
    if-ne v10, v12, :cond_91

    #@7c
    .line 229
    aget-byte v10, v4, v11

    #@7e
    if-eq v10, v13, :cond_91

    #@80
    .line 230
    iget v10, v0, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberOfDigits:I

    #@82
    add-int/lit8 v10, v10, 0x1

    #@84
    iput v10, v0, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberOfDigits:I

    #@86
    .line 231
    iget v10, v0, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberOfDigits:I

    #@88
    new-array v9, v10, [B

    #@8a
    .line 232
    .local v9, tmpData:[B
    aput-byte v13, v9, v11

    #@8c
    .line 233
    invoke-static {v4, v11, v9, v12, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@8f
    .line 234
    iput-object v9, v0, Lcom/android/internal/telephony/SmsAddress;->origBytes:[B

    #@91
    .line 237
    .end local v9           #tmpData:[B
    :cond_91
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@94
    move-result v10

    #@95
    iput v10, v8, Lcom/android/internal/telephony/cdma/sms/CdmaSmsSubaddress;->type:I

    #@97
    .line 238
    invoke-virtual {p0}, Landroid/os/Parcel;->readByte()B

    #@9a
    move-result v10

    #@9b
    iput-byte v10, v8, Lcom/android/internal/telephony/cdma/sms/CdmaSmsSubaddress;->odd:B

    #@9d
    .line 239
    invoke-virtual {p0}, Landroid/os/Parcel;->readByte()B

    #@a0
    move-result v2

    #@a1
    .line 241
    if-gez v2, :cond_a4

    #@a3
    .line 242
    const/4 v2, 0x0

    #@a4
    .line 247
    :cond_a4
    new-array v4, v2, [B

    #@a6
    .line 249
    const/4 v6, 0x0

    #@a7
    :goto_a7
    if-ge v6, v2, :cond_b2

    #@a9
    .line 250
    invoke-virtual {p0}, Landroid/os/Parcel;->readByte()B

    #@ac
    move-result v10

    #@ad
    aput-byte v10, v4, v6

    #@af
    .line 249
    add-int/lit8 v6, v6, 0x1

    #@b1
    goto :goto_a7

    #@b2
    .line 253
    :cond_b2
    iput-object v4, v8, Lcom/android/internal/telephony/cdma/sms/CdmaSmsSubaddress;->origBytes:[B

    #@b4
    .line 263
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@b7
    move-result v3

    #@b8
    .line 264
    .local v3, countInt:I
    if-gez v3, :cond_bb

    #@ba
    .line 265
    const/4 v3, 0x0

    #@bb
    .line 268
    :cond_bb
    new-array v4, v3, [B

    #@bd
    .line 269
    const/4 v6, 0x0

    #@be
    :goto_be
    if-ge v6, v3, :cond_c9

    #@c0
    .line 270
    invoke-virtual {p0}, Landroid/os/Parcel;->readByte()B

    #@c3
    move-result v10

    #@c4
    aput-byte v10, v4, v6

    #@c6
    .line 269
    add-int/lit8 v6, v6, 0x1

    #@c8
    goto :goto_be

    #@c9
    .line 273
    :cond_c9
    iput-object v4, v5, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->bearerData:[B

    #@cb
    .line 276
    iput-object v0, v5, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->origAddress:Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;

    #@cd
    .line 277
    iput-object v8, v5, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->origSubaddress:Lcom/android/internal/telephony/cdma/sms/CdmaSmsSubaddress;

    #@cf
    .line 278
    iput-object v0, v7, Lcom/android/internal/telephony/SmsMessageBase;->originatingAddress:Lcom/android/internal/telephony/SmsAddress;

    #@d1
    .line 279
    iput-object v5, v7, Lcom/android/internal/telephony/cdma/SmsMessage;->mEnvelope:Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;

    #@d3
    .line 282
    invoke-direct {v7}, Lcom/android/internal/telephony/cdma/SmsMessage;->createPdu()V

    #@d6
    .line 284
    return-object v7
.end method

.method private parsePdu([B)V
    .registers 12
    .parameter "pdu"

    #@0
    .prologue
    .line 1218
    new-instance v1, Ljava/io/ByteArrayInputStream;

    #@2
    invoke-direct {v1, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    #@5
    .line 1219
    .local v1, bais:Ljava/io/ByteArrayInputStream;
    new-instance v3, Ljava/io/DataInputStream;

    #@7
    invoke-direct {v3, v1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    #@a
    .line 1222
    .local v3, dis:Ljava/io/DataInputStream;
    new-instance v4, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;

    #@c
    invoke-direct {v4}, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;-><init>()V

    #@f
    .line 1223
    .local v4, env:Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;
    new-instance v0, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;

    #@11
    invoke-direct {v0}, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;-><init>()V

    #@14
    .line 1226
    .local v0, addr:Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;
    :try_start_14
    invoke-virtual {v3}, Ljava/io/DataInputStream;->readInt()I

    #@17
    move-result v7

    #@18
    iput v7, v4, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->messageType:I

    #@1a
    .line 1227
    invoke-virtual {v3}, Ljava/io/DataInputStream;->readInt()I

    #@1d
    move-result v7

    #@1e
    iput v7, v4, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->teleService:I

    #@20
    .line 1228
    invoke-virtual {v3}, Ljava/io/DataInputStream;->readInt()I

    #@23
    move-result v7

    #@24
    iput v7, v4, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->serviceCategory:I

    #@26
    .line 1230
    invoke-virtual {v3}, Ljava/io/DataInputStream;->readByte()B

    #@29
    move-result v7

    #@2a
    iput v7, v0, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->digitMode:I

    #@2c
    .line 1231
    invoke-virtual {v3}, Ljava/io/DataInputStream;->readByte()B

    #@2f
    move-result v7

    #@30
    iput v7, v0, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberMode:I

    #@32
    .line 1232
    invoke-virtual {v3}, Ljava/io/DataInputStream;->readByte()B

    #@35
    move-result v7

    #@36
    iput v7, v0, Lcom/android/internal/telephony/SmsAddress;->ton:I

    #@38
    .line 1233
    invoke-virtual {v3}, Ljava/io/DataInputStream;->readByte()B

    #@3b
    move-result v7

    #@3c
    iput v7, v0, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberPlan:I

    #@3e
    .line 1235
    invoke-virtual {v3}, Ljava/io/DataInputStream;->readByte()B

    #@41
    move-result v6

    #@42
    .line 1236
    .local v6, length:B
    iput v6, v0, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberOfDigits:I

    #@44
    .line 1239
    array-length v7, p1

    #@45
    if-le v6, v7, :cond_85

    #@47
    .line 1240
    new-instance v7, Ljava/lang/RuntimeException;

    #@49
    new-instance v8, Ljava/lang/StringBuilder;

    #@4b
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@4e
    const-string v9, "createFromPdu: Invalid pdu, addr.numberOfDigits "

    #@50
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v8

    #@54
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@57
    move-result-object v8

    #@58
    const-string v9, " > pdu len "

    #@5a
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v8

    #@5e
    array-length v9, p1

    #@5f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@62
    move-result-object v8

    #@63
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@66
    move-result-object v8

    #@67
    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@6a
    throw v7
    :try_end_6b
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_6b} :catch_6b

    #@6b
    .line 1264
    .end local v6           #length:B
    :catch_6b
    move-exception v5

    #@6c
    .line 1265
    .local v5, ex:Ljava/io/IOException;
    new-instance v7, Ljava/lang/RuntimeException;

    #@6e
    new-instance v8, Ljava/lang/StringBuilder;

    #@70
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@73
    const-string v9, "createFromPdu: conversion from byte array to object failed: "

    #@75
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v8

    #@79
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v8

    #@7d
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@80
    move-result-object v8

    #@81
    invoke-direct {v7, v8, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@84
    throw v7

    #@85
    .line 1244
    .end local v5           #ex:Ljava/io/IOException;
    .restart local v6       #length:B
    :cond_85
    :try_start_85
    new-array v7, v6, [B

    #@87
    iput-object v7, v0, Lcom/android/internal/telephony/SmsAddress;->origBytes:[B

    #@89
    .line 1245
    iget-object v7, v0, Lcom/android/internal/telephony/SmsAddress;->origBytes:[B

    #@8b
    const/4 v8, 0x0

    #@8c
    invoke-virtual {v3, v7, v8, v6}, Ljava/io/DataInputStream;->read([BII)I

    #@8f
    .line 1247
    invoke-virtual {v3}, Ljava/io/DataInputStream;->readInt()I

    #@92
    move-result v7

    #@93
    iput v7, v4, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->bearerReply:I

    #@95
    .line 1249
    invoke-virtual {v3}, Ljava/io/DataInputStream;->readByte()B

    #@98
    move-result v7

    #@99
    iput-byte v7, v4, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->replySeqNo:B

    #@9b
    .line 1250
    invoke-virtual {v3}, Ljava/io/DataInputStream;->readByte()B

    #@9e
    move-result v7

    #@9f
    iput-byte v7, v4, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->errorClass:B

    #@a1
    .line 1251
    invoke-virtual {v3}, Ljava/io/DataInputStream;->readByte()B

    #@a4
    move-result v7

    #@a5
    iput-byte v7, v4, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->causeCode:B

    #@a7
    .line 1254
    invoke-virtual {v3}, Ljava/io/DataInputStream;->readInt()I

    #@aa
    move-result v2

    #@ab
    .line 1256
    .local v2, bearerDataLength:I
    array-length v7, p1

    #@ac
    if-le v2, v7, :cond_d2

    #@ae
    .line 1257
    new-instance v7, Ljava/lang/RuntimeException;

    #@b0
    new-instance v8, Ljava/lang/StringBuilder;

    #@b2
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@b5
    const-string v9, "createFromPdu: Invalid pdu, bearerDataLength "

    #@b7
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v8

    #@bb
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@be
    move-result-object v8

    #@bf
    const-string v9, " > pdu len "

    #@c1
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c4
    move-result-object v8

    #@c5
    array-length v9, p1

    #@c6
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c9
    move-result-object v8

    #@ca
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cd
    move-result-object v8

    #@ce
    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@d1
    throw v7

    #@d2
    .line 1261
    :cond_d2
    new-array v7, v2, [B

    #@d4
    iput-object v7, v4, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->bearerData:[B

    #@d6
    .line 1262
    iget-object v7, v4, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->bearerData:[B

    #@d8
    const/4 v8, 0x0

    #@d9
    invoke-virtual {v3, v7, v8, v2}, Ljava/io/DataInputStream;->read([BII)I

    #@dc
    .line 1263
    invoke-virtual {v3}, Ljava/io/DataInputStream;->close()V
    :try_end_df
    .catch Ljava/io/IOException; {:try_start_85 .. :try_end_df} :catch_6b

    #@df
    .line 1270
    iput-object v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->originatingAddress:Lcom/android/internal/telephony/SmsAddress;

    #@e1
    .line 1271
    iput-object v0, v4, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->origAddress:Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;

    #@e3
    .line 1272
    iput-object v4, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mEnvelope:Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;

    #@e5
    .line 1273
    iput-object p1, p0, Lcom/android/internal/telephony/SmsMessageBase;->mPdu:[B

    #@e7
    .line 1275
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/SmsMessage;->parseSms()V

    #@ea
    .line 1276
    return-void
.end method

.method private parsePduFromEfRecord([B)V
    .registers 29
    .parameter "pdu"

    #@0
    .prologue
    .line 1282
    new-instance v5, Ljava/io/ByteArrayInputStream;

    #@2
    move-object/from16 v0, p1

    #@4
    invoke-direct {v5, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    #@7
    .line 1283
    .local v5, bais:Ljava/io/ByteArrayInputStream;
    new-instance v8, Ljava/io/DataInputStream;

    #@9
    invoke-direct {v8, v5}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    #@c
    .line 1284
    .local v8, dis:Ljava/io/DataInputStream;
    new-instance v9, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;

    #@e
    invoke-direct {v9}, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;-><init>()V

    #@11
    .line 1285
    .local v9, env:Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;
    new-instance v2, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;

    #@13
    invoke-direct {v2}, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;-><init>()V

    #@16
    .line 1286
    .local v2, addr:Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;
    new-instance v20, Lcom/android/internal/telephony/cdma/sms/CdmaSmsSubaddress;

    #@18
    invoke-direct/range {v20 .. v20}, Lcom/android/internal/telephony/cdma/sms/CdmaSmsSubaddress;-><init>()V

    #@1b
    .line 1289
    .local v20, subAddr:Lcom/android/internal/telephony/cdma/sms/CdmaSmsSubaddress;
    :try_start_1b
    invoke-virtual {v8}, Ljava/io/DataInputStream;->readByte()B

    #@1e
    move-result v24

    #@1f
    move/from16 v0, v24

    #@21
    iput v0, v9, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->messageType:I

    #@23
    .line 1291
    :cond_23
    :goto_23
    invoke-virtual {v8}, Ljava/io/DataInputStream;->available()I

    #@26
    move-result v24

    #@27
    if-lez v24, :cond_346

    #@29
    .line 1292
    invoke-virtual {v8}, Ljava/io/DataInputStream;->readByte()B

    #@2c
    move-result v16

    #@2d
    .line 1295
    .local v16, parameterId:I
    invoke-virtual {v8}, Ljava/io/DataInputStream;->readByte()B

    #@30
    move-result v14

    #@31
    .line 1296
    .local v14, paramLen:B
    shr-int/lit8 v24, v14, 0x4

    #@33
    and-int/lit8 v11, v24, 0xf

    #@35
    .line 1297
    .local v11, firstNumber:I
    and-int/lit8 v19, v14, 0xf

    #@37
    .line 1298
    .local v19, secondNumber:I
    mul-int/lit8 v24, v11, 0x10

    #@39
    add-int v17, v24, v19

    #@3b
    .line 1300
    .local v17, parameterLen:I
    new-instance v24, Ljava/lang/StringBuilder;

    #@3d
    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    #@40
    const-string v25, "parsePduFromEfRecord(), parameterLen = "

    #@42
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v24

    #@46
    move-object/from16 v0, v24

    #@48
    move/from16 v1, v17

    #@4a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v24

    #@4e
    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@51
    move-result-object v24

    #@52
    invoke-static/range {v24 .. v24}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@55
    .line 1301
    move/from16 v0, v17

    #@57
    new-array v15, v0, [B

    #@59
    .line 1303
    .local v15, parameterData:[B
    packed-switch v16, :pswitch_data_34e

    #@5c
    .line 1414
    new-instance v24, Ljava/lang/Exception;

    #@5e
    new-instance v25, Ljava/lang/StringBuilder;

    #@60
    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    #@63
    const-string v26, "unsupported parameterId ("

    #@65
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v25

    #@69
    move-object/from16 v0, v25

    #@6b
    move/from16 v1, v16

    #@6d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@70
    move-result-object v25

    #@71
    const-string v26, ")"

    #@73
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v25

    #@77
    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7a
    move-result-object v25

    #@7b
    invoke-direct/range {v24 .. v25}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    #@7e
    throw v24
    :try_end_7f
    .catch Ljava/lang/Exception; {:try_start_1b .. :try_end_7f} :catch_7f

    #@7f
    .line 1419
    .end local v11           #firstNumber:I
    .end local v14           #paramLen:B
    .end local v15           #parameterData:[B
    .end local v16           #parameterId:I
    .end local v17           #parameterLen:I
    .end local v19           #secondNumber:I
    :catch_7f
    move-exception v10

    #@80
    .line 1420
    .local v10, ex:Ljava/lang/Exception;
    const-string v24, "CDMA"

    #@82
    new-instance v25, Ljava/lang/StringBuilder;

    #@84
    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    #@87
    const-string v26, "parsePduFromEfRecord: conversion from pdu to SmsMessage failed"

    #@89
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v25

    #@8d
    move-object/from16 v0, v25

    #@8f
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@92
    move-result-object v25

    #@93
    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@96
    move-result-object v25

    #@97
    invoke-static/range {v24 .. v25}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@9a
    .line 1424
    .end local v10           #ex:Ljava/lang/Exception;
    :goto_9a
    move-object/from16 v0, p0

    #@9c
    iput-object v2, v0, Lcom/android/internal/telephony/SmsMessageBase;->originatingAddress:Lcom/android/internal/telephony/SmsAddress;

    #@9e
    .line 1425
    iput-object v2, v9, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->origAddress:Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;

    #@a0
    .line 1426
    move-object/from16 v0, v20

    #@a2
    iput-object v0, v9, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->origSubaddress:Lcom/android/internal/telephony/cdma/sms/CdmaSmsSubaddress;

    #@a4
    .line 1427
    move-object/from16 v0, p0

    #@a6
    iput-object v9, v0, Lcom/android/internal/telephony/cdma/SmsMessage;->mEnvelope:Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;

    #@a8
    .line 1428
    move-object/from16 v0, p1

    #@aa
    move-object/from16 v1, p0

    #@ac
    iput-object v0, v1, Lcom/android/internal/telephony/SmsMessageBase;->mPdu:[B

    #@ae
    .line 1430
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/telephony/cdma/SmsMessage;->parseSms()V

    #@b1
    .line 1431
    return-void

    #@b2
    .line 1310
    .restart local v11       #firstNumber:I
    .restart local v14       #paramLen:B
    .restart local v15       #parameterData:[B
    .restart local v16       #parameterId:I
    .restart local v17       #parameterLen:I
    .restart local v19       #secondNumber:I
    :pswitch_b2
    :try_start_b2
    invoke-virtual {v8}, Ljava/io/DataInputStream;->readUnsignedShort()I

    #@b5
    move-result v24

    #@b6
    move/from16 v0, v24

    #@b8
    iput v0, v9, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->teleService:I

    #@ba
    .line 1311
    const-string v24, "CDMA"

    #@bc
    new-instance v25, Ljava/lang/StringBuilder;

    #@be
    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    #@c1
    const-string v26, "teleservice = "

    #@c3
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v25

    #@c7
    iget v0, v9, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->teleService:I

    #@c9
    move/from16 v26, v0

    #@cb
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v25

    #@cf
    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d2
    move-result-object v25

    #@d3
    invoke-static/range {v24 .. v25}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@d6
    goto/16 :goto_23

    #@d8
    .line 1318
    :pswitch_d8
    invoke-virtual {v8}, Ljava/io/DataInputStream;->readUnsignedShort()I

    #@db
    move-result v24

    #@dc
    move/from16 v0, v24

    #@de
    iput v0, v9, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->serviceCategory:I

    #@e0
    goto/16 :goto_23

    #@e2
    .line 1322
    :pswitch_e2
    const/16 v24, 0x0

    #@e4
    move/from16 v0, v24

    #@e6
    move/from16 v1, v17

    #@e8
    invoke-virtual {v8, v15, v0, v1}, Ljava/io/DataInputStream;->read([BII)I

    #@eb
    .line 1323
    new-instance v3, Lcom/android/internal/util/BitwiseInputStream;

    #@ed
    invoke-direct {v3, v15}, Lcom/android/internal/util/BitwiseInputStream;-><init>([B)V

    #@f0
    .line 1324
    .local v3, addrBis:Lcom/android/internal/util/BitwiseInputStream;
    const/16 v24, 0x1

    #@f2
    move/from16 v0, v24

    #@f4
    invoke-virtual {v3, v0}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@f7
    move-result v24

    #@f8
    move/from16 v0, v24

    #@fa
    iput v0, v2, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->digitMode:I

    #@fc
    .line 1325
    const/16 v24, 0x1

    #@fe
    move/from16 v0, v24

    #@100
    invoke-virtual {v3, v0}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@103
    move-result v24

    #@104
    move/from16 v0, v24

    #@106
    iput v0, v2, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberMode:I

    #@108
    .line 1326
    const/4 v13, 0x0

    #@109
    .line 1327
    .local v13, numberType:I
    iget v0, v2, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->digitMode:I

    #@10b
    move/from16 v24, v0

    #@10d
    const/16 v25, 0x1

    #@10f
    move/from16 v0, v24

    #@111
    move/from16 v1, v25

    #@113
    if-ne v0, v1, :cond_131

    #@115
    .line 1328
    const/16 v24, 0x3

    #@117
    move/from16 v0, v24

    #@119
    invoke-virtual {v3, v0}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@11c
    move-result v13

    #@11d
    .line 1329
    iput v13, v2, Lcom/android/internal/telephony/SmsAddress;->ton:I

    #@11f
    .line 1331
    iget v0, v2, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberMode:I

    #@121
    move/from16 v24, v0

    #@123
    if-nez v24, :cond_131

    #@125
    .line 1332
    const/16 v24, 0x4

    #@127
    move/from16 v0, v24

    #@129
    invoke-virtual {v3, v0}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@12c
    move-result v24

    #@12d
    move/from16 v0, v24

    #@12f
    iput v0, v2, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberPlan:I

    #@131
    .line 1335
    :cond_131
    const/16 v24, 0x8

    #@133
    move/from16 v0, v24

    #@135
    invoke-virtual {v3, v0}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@138
    move-result v24

    #@139
    move/from16 v0, v24

    #@13b
    iput v0, v2, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberOfDigits:I

    #@13d
    .line 1337
    iget v0, v2, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberOfDigits:I

    #@13f
    move/from16 v24, v0

    #@141
    move/from16 v0, v24

    #@143
    new-array v7, v0, [B

    #@145
    .line 1338
    .local v7, data:[B
    const/4 v4, 0x0

    #@146
    .line 1340
    .local v4, b:B
    iget v0, v2, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->digitMode:I

    #@148
    move/from16 v24, v0

    #@14a
    if-nez v24, :cond_16d

    #@14c
    .line 1342
    const/4 v12, 0x0

    #@14d
    .local v12, index:I
    :goto_14d
    iget v0, v2, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberOfDigits:I

    #@14f
    move/from16 v24, v0

    #@151
    move/from16 v0, v24

    #@153
    if-ge v12, v0, :cond_234

    #@155
    .line 1343
    const/16 v24, 0x4

    #@157
    move/from16 v0, v24

    #@159
    invoke-virtual {v3, v0}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@15c
    move-result v24

    #@15d
    and-int/lit8 v24, v24, 0xf

    #@15f
    move/from16 v0, v24

    #@161
    int-to-byte v4, v0

    #@162
    .line 1346
    move-object/from16 v0, p0

    #@164
    invoke-direct {v0, v4}, Lcom/android/internal/telephony/cdma/SmsMessage;->convertDtmfToAscii(B)B

    #@167
    move-result v24

    #@168
    aput-byte v24, v7, v12

    #@16a
    .line 1342
    add-int/lit8 v12, v12, 0x1

    #@16c
    goto :goto_14d

    #@16d
    .line 1348
    .end local v12           #index:I
    :cond_16d
    iget v0, v2, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->digitMode:I

    #@16f
    move/from16 v24, v0

    #@171
    const/16 v25, 0x1

    #@173
    move/from16 v0, v24

    #@175
    move/from16 v1, v25

    #@177
    if-ne v0, v1, :cond_25c

    #@179
    .line 1349
    iget v0, v2, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberMode:I

    #@17b
    move/from16 v24, v0

    #@17d
    if-nez v24, :cond_19e

    #@17f
    .line 1350
    const/4 v12, 0x0

    #@180
    .restart local v12       #index:I
    :goto_180
    iget v0, v2, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberOfDigits:I

    #@182
    move/from16 v24, v0

    #@184
    move/from16 v0, v24

    #@186
    if-ge v12, v0, :cond_234

    #@188
    .line 1351
    const/16 v24, 0x8

    #@18a
    move/from16 v0, v24

    #@18c
    invoke-virtual {v3, v0}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@18f
    move-result v24

    #@190
    move/from16 v0, v24

    #@192
    and-int/lit16 v0, v0, 0xff

    #@194
    move/from16 v24, v0

    #@196
    move/from16 v0, v24

    #@198
    int-to-byte v4, v0

    #@199
    .line 1352
    aput-byte v4, v7, v12

    #@19b
    .line 1350
    add-int/lit8 v12, v12, 0x1

    #@19d
    goto :goto_180

    #@19e
    .line 1355
    .end local v12           #index:I
    :cond_19e
    iget v0, v2, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberMode:I

    #@1a0
    move/from16 v24, v0

    #@1a2
    const/16 v25, 0x1

    #@1a4
    move/from16 v0, v24

    #@1a6
    move/from16 v1, v25

    #@1a8
    if-ne v0, v1, :cond_254

    #@1aa
    .line 1358
    const-string v24, "parsePduFromEfRecord(), addr.numberMode= NUMBER_MODE_DATA_NETWORK"

    #@1ac
    invoke-static/range {v24 .. v24}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@1af
    .line 1359
    const/16 v24, 0x2

    #@1b1
    move/from16 v0, v24

    #@1b3
    if-ne v13, v0, :cond_22d

    #@1b5
    .line 1360
    const-string v24, "parsePduFromEfRecord(), addr.ton: Originating Addr is email id"

    #@1b7
    invoke-static/range {v24 .. v24}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@1ba
    .line 1361
    const/4 v12, 0x0

    #@1bb
    .restart local v12       #index:I
    :goto_1bb
    iget v0, v2, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberOfDigits:I

    #@1bd
    move/from16 v24, v0

    #@1bf
    move/from16 v0, v24

    #@1c1
    if-ge v12, v0, :cond_234

    #@1c3
    .line 1362
    const/16 v24, 0x8

    #@1c5
    move/from16 v0, v24

    #@1c7
    invoke-virtual {v3, v0}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@1ca
    move-result v24

    #@1cb
    move/from16 v0, v24

    #@1cd
    and-int/lit16 v0, v0, 0xff

    #@1cf
    move/from16 v24, v0

    #@1d1
    move/from16 v0, v24

    #@1d3
    int-to-byte v4, v0

    #@1d4
    .line 1363
    aput-byte v4, v7, v12

    #@1d6
    .line 1364
    new-instance v24, Ljava/lang/StringBuilder;

    #@1d8
    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    #@1db
    const-string v25, "parsePduFromEfRecord(), index = "

    #@1dd
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e0
    move-result-object v24

    #@1e1
    move-object/from16 v0, v24

    #@1e3
    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e6
    move-result-object v24

    #@1e7
    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ea
    move-result-object v24

    #@1eb
    invoke-static/range {v24 .. v24}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@1ee
    .line 1365
    new-instance v24, Ljava/lang/StringBuilder;

    #@1f0
    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    #@1f3
    const-string v25, "parsePduFromEfRecord(), b = "

    #@1f5
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f8
    move-result-object v24

    #@1f9
    move-object/from16 v0, v24

    #@1fb
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1fe
    move-result-object v24

    #@1ff
    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@202
    move-result-object v24

    #@203
    invoke-static/range {v24 .. v24}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@206
    .line 1366
    new-instance v24, Ljava/lang/StringBuilder;

    #@208
    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    #@20b
    const-string v25, "parsePduFromEfRecord(), data["

    #@20d
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@210
    move-result-object v24

    #@211
    move-object/from16 v0, v24

    #@213
    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@216
    move-result-object v24

    #@217
    const-string v25, "] = "

    #@219
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21c
    move-result-object v24

    #@21d
    aget-byte v25, v7, v12

    #@21f
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@222
    move-result-object v24

    #@223
    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@226
    move-result-object v24

    #@227
    invoke-static/range {v24 .. v24}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@22a
    .line 1361
    add-int/lit8 v12, v12, 0x1

    #@22c
    goto :goto_1bb

    #@22d
    .line 1370
    .end local v12           #index:I
    :cond_22d
    const-string v24, "CDMA"

    #@22f
    const-string v25, "TODO: Originating Addr is data network address"

    #@231
    invoke-static/range {v24 .. v25}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@234
    .line 1378
    :cond_234
    :goto_234
    iput-object v7, v2, Lcom/android/internal/telephony/SmsAddress;->origBytes:[B

    #@236
    .line 1379
    const-string v24, "CDMA"

    #@238
    new-instance v25, Ljava/lang/StringBuilder;

    #@23a
    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    #@23d
    const-string v26, "Originating Addr="

    #@23f
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@242
    move-result-object v25

    #@243
    invoke-virtual {v2}, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->toString()Ljava/lang/String;

    #@246
    move-result-object v26

    #@247
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24a
    move-result-object v25

    #@24b
    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24e
    move-result-object v25

    #@24f
    invoke-static/range {v24 .. v25}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@252
    goto/16 :goto_23

    #@254
    .line 1373
    :cond_254
    const-string v24, "CDMA"

    #@256
    const-string v25, "Originating Addr is of incorrect type"

    #@258
    invoke-static/range {v24 .. v25}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@25b
    goto :goto_234

    #@25c
    .line 1376
    :cond_25c
    const-string v24, "CDMA"

    #@25e
    const-string v25, "Incorrect Digit mode"

    #@260
    invoke-static/range {v24 .. v25}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@263
    goto :goto_234

    #@264
    .line 1383
    .end local v3           #addrBis:Lcom/android/internal/util/BitwiseInputStream;
    .end local v4           #b:B
    .end local v7           #data:[B
    .end local v13           #numberType:I
    :pswitch_264
    const/16 v24, 0x0

    #@266
    move/from16 v0, v24

    #@268
    move/from16 v1, v17

    #@26a
    invoke-virtual {v8, v15, v0, v1}, Ljava/io/DataInputStream;->read([BII)I

    #@26d
    .line 1384
    new-instance v21, Lcom/android/internal/util/BitwiseInputStream;

    #@26f
    move-object/from16 v0, v21

    #@271
    invoke-direct {v0, v15}, Lcom/android/internal/util/BitwiseInputStream;-><init>([B)V

    #@274
    .line 1385
    .local v21, subAddrBis:Lcom/android/internal/util/BitwiseInputStream;
    const/16 v24, 0x3

    #@276
    move-object/from16 v0, v21

    #@278
    move/from16 v1, v24

    #@27a
    invoke-virtual {v0, v1}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@27d
    move-result v24

    #@27e
    move/from16 v0, v24

    #@280
    move-object/from16 v1, v20

    #@282
    iput v0, v1, Lcom/android/internal/telephony/cdma/sms/CdmaSmsSubaddress;->type:I

    #@284
    .line 1386
    const/16 v24, 0x1

    #@286
    move-object/from16 v0, v21

    #@288
    move/from16 v1, v24

    #@28a
    invoke-virtual {v0, v1}, Lcom/android/internal/util/BitwiseInputStream;->readByteArray(I)[B

    #@28d
    move-result-object v24

    #@28e
    const/16 v25, 0x0

    #@290
    aget-byte v24, v24, v25

    #@292
    move/from16 v0, v24

    #@294
    move-object/from16 v1, v20

    #@296
    iput-byte v0, v1, Lcom/android/internal/telephony/cdma/sms/CdmaSmsSubaddress;->odd:B

    #@298
    .line 1387
    const/16 v24, 0x8

    #@29a
    move-object/from16 v0, v21

    #@29c
    move/from16 v1, v24

    #@29e
    invoke-virtual {v0, v1}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@2a1
    move-result v22

    #@2a2
    .line 1388
    .local v22, subAddrLen:I
    move/from16 v0, v22

    #@2a4
    new-array v0, v0, [B

    #@2a6
    move-object/from16 v23, v0

    #@2a8
    .line 1389
    .local v23, subdata:[B
    const/4 v12, 0x0

    #@2a9
    .restart local v12       #index:I
    :goto_2a9
    move/from16 v0, v22

    #@2ab
    if-ge v12, v0, :cond_2cb

    #@2ad
    .line 1390
    const/16 v24, 0x4

    #@2af
    move-object/from16 v0, v21

    #@2b1
    move/from16 v1, v24

    #@2b3
    invoke-virtual {v0, v1}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@2b6
    move-result v24

    #@2b7
    move/from16 v0, v24

    #@2b9
    and-int/lit16 v0, v0, 0xff

    #@2bb
    move/from16 v24, v0

    #@2bd
    move/from16 v0, v24

    #@2bf
    int-to-byte v4, v0

    #@2c0
    .line 1392
    .restart local v4       #b:B
    move-object/from16 v0, p0

    #@2c2
    invoke-direct {v0, v4}, Lcom/android/internal/telephony/cdma/SmsMessage;->convertDtmfToAscii(B)B

    #@2c5
    move-result v24

    #@2c6
    aput-byte v24, v23, v12

    #@2c8
    .line 1389
    add-int/lit8 v12, v12, 0x1

    #@2ca
    goto :goto_2a9

    #@2cb
    .line 1394
    .end local v4           #b:B
    :cond_2cb
    move-object/from16 v0, v23

    #@2cd
    move-object/from16 v1, v20

    #@2cf
    iput-object v0, v1, Lcom/android/internal/telephony/cdma/sms/CdmaSmsSubaddress;->origBytes:[B

    #@2d1
    goto/16 :goto_23

    #@2d3
    .line 1397
    .end local v12           #index:I
    .end local v21           #subAddrBis:Lcom/android/internal/util/BitwiseInputStream;
    .end local v22           #subAddrLen:I
    .end local v23           #subdata:[B
    :pswitch_2d3
    const/16 v24, 0x0

    #@2d5
    move/from16 v0, v24

    #@2d7
    move/from16 v1, v17

    #@2d9
    invoke-virtual {v8, v15, v0, v1}, Ljava/io/DataInputStream;->read([BII)I

    #@2dc
    .line 1398
    new-instance v18, Lcom/android/internal/util/BitwiseInputStream;

    #@2de
    move-object/from16 v0, v18

    #@2e0
    invoke-direct {v0, v15}, Lcom/android/internal/util/BitwiseInputStream;-><init>([B)V

    #@2e3
    .line 1399
    .local v18, replyOptBis:Lcom/android/internal/util/BitwiseInputStream;
    const/16 v24, 0x6

    #@2e5
    move-object/from16 v0, v18

    #@2e7
    move/from16 v1, v24

    #@2e9
    invoke-virtual {v0, v1}, Lcom/android/internal/util/BitwiseInputStream;->read(I)I

    #@2ec
    move-result v24

    #@2ed
    move/from16 v0, v24

    #@2ef
    iput v0, v9, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->bearerReply:I

    #@2f1
    goto/16 :goto_23

    #@2f3
    .line 1402
    .end local v18           #replyOptBis:Lcom/android/internal/util/BitwiseInputStream;
    :pswitch_2f3
    const/16 v24, 0x0

    #@2f5
    move/from16 v0, v24

    #@2f7
    move/from16 v1, v17

    #@2f9
    invoke-virtual {v8, v15, v0, v1}, Ljava/io/DataInputStream;->read([BII)I

    #@2fc
    .line 1403
    new-instance v6, Lcom/android/internal/util/BitwiseInputStream;

    #@2fe
    invoke-direct {v6, v15}, Lcom/android/internal/util/BitwiseInputStream;-><init>([B)V

    #@301
    .line 1404
    .local v6, ccBis:Lcom/android/internal/util/BitwiseInputStream;
    const/16 v24, 0x6

    #@303
    move/from16 v0, v24

    #@305
    invoke-virtual {v6, v0}, Lcom/android/internal/util/BitwiseInputStream;->readByteArray(I)[B

    #@308
    move-result-object v24

    #@309
    const/16 v25, 0x0

    #@30b
    aget-byte v24, v24, v25

    #@30d
    move/from16 v0, v24

    #@30f
    iput-byte v0, v9, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->replySeqNo:B

    #@311
    .line 1405
    const/16 v24, 0x2

    #@313
    move/from16 v0, v24

    #@315
    invoke-virtual {v6, v0}, Lcom/android/internal/util/BitwiseInputStream;->readByteArray(I)[B

    #@318
    move-result-object v24

    #@319
    const/16 v25, 0x0

    #@31b
    aget-byte v24, v24, v25

    #@31d
    move/from16 v0, v24

    #@31f
    iput-byte v0, v9, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->errorClass:B

    #@321
    .line 1406
    iget-byte v0, v9, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->errorClass:B

    #@323
    move/from16 v24, v0

    #@325
    if-eqz v24, :cond_23

    #@327
    .line 1407
    const/16 v24, 0x8

    #@329
    move/from16 v0, v24

    #@32b
    invoke-virtual {v6, v0}, Lcom/android/internal/util/BitwiseInputStream;->readByteArray(I)[B

    #@32e
    move-result-object v24

    #@32f
    const/16 v25, 0x0

    #@331
    aget-byte v24, v24, v25

    #@333
    move/from16 v0, v24

    #@335
    iput-byte v0, v9, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->causeCode:B

    #@337
    goto/16 :goto_23

    #@339
    .line 1410
    .end local v6           #ccBis:Lcom/android/internal/util/BitwiseInputStream;
    :pswitch_339
    const/16 v24, 0x0

    #@33b
    move/from16 v0, v24

    #@33d
    move/from16 v1, v17

    #@33f
    invoke-virtual {v8, v15, v0, v1}, Ljava/io/DataInputStream;->read([BII)I

    #@342
    .line 1411
    iput-object v15, v9, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->bearerData:[B

    #@344
    goto/16 :goto_23

    #@346
    .line 1417
    .end local v11           #firstNumber:I
    .end local v14           #paramLen:B
    .end local v15           #parameterData:[B
    .end local v16           #parameterId:I
    .end local v17           #parameterLen:I
    .end local v19           #secondNumber:I
    :cond_346
    invoke-virtual {v5}, Ljava/io/ByteArrayInputStream;->close()V

    #@349
    .line 1418
    invoke-virtual {v8}, Ljava/io/DataInputStream;->close()V
    :try_end_34c
    .catch Ljava/lang/Exception; {:try_start_b2 .. :try_end_34c} :catch_7f

    #@34c
    goto/16 :goto_9a

    #@34e
    .line 1303
    :pswitch_data_34e
    .packed-switch 0x0
        :pswitch_b2
        :pswitch_d8
        :pswitch_e2
        :pswitch_264
        :pswitch_e2
        :pswitch_264
        :pswitch_2d3
        :pswitch_2f3
        :pswitch_339
    .end packed-switch
.end method

.method private static privateGetDomainNotiPdu(Ljava/lang/String;ZLcom/android/internal/telephony/cdma/sms/UserData;)Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;
    .registers 16
    .parameter "destAddrStr"
    .parameter "statusReportRequested"
    .parameter "userData"

    #@0
    .prologue
    const/4 v10, 0x2

    #@1
    const/4 v12, 0x1

    #@2
    const/4 v8, 0x0

    #@3
    const/4 v11, 0x0

    #@4
    .line 2187
    invoke-static {p0}, Landroid/telephony/PhoneNumberUtils;->cdmaCheckAndProcessPlusCode(Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v9

    #@8
    invoke-static {v9}, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->parse(Ljava/lang/String;)Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;

    #@b
    move-result-object v2

    #@c
    .line 2190
    .local v2, destAddr:Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;
    new-instance v1, Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@e
    invoke-direct {v1}, Lcom/android/internal/telephony/cdma/sms/BearerData;-><init>()V

    #@11
    .line 2191
    .local v1, bearerData:Lcom/android/internal/telephony/cdma/sms/BearerData;
    iput v10, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->messageType:I

    #@13
    .line 2193
    invoke-static {}, Lcom/android/internal/telephony/cdma/SmsMessage;->getNextMessageId()I

    #@16
    move-result v9

    #@17
    iput v9, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->messageId:I

    #@19
    .line 2194
    iput-boolean p1, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->deliveryAckReq:Z

    #@1b
    .line 2195
    iput-boolean v11, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->userAckReq:Z

    #@1d
    .line 2196
    iput-boolean v11, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->readAckReq:Z

    #@1f
    .line 2198
    iput-boolean v12, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->priorityIndicatorSet:Z

    #@21
    .line 2199
    iput v10, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->priority:I

    #@23
    .line 2200
    iput-object p2, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@25
    .line 2201
    invoke-static {v1}, Lcom/android/internal/telephony/cdma/sms/BearerData;->encode(Lcom/android/internal/telephony/cdma/sms/BearerData;)[B

    #@28
    move-result-object v4

    #@29
    .line 2203
    .local v4, encodedBearerData:[B
    new-instance v9, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v10, "privateGetDomainNotiPdu(), [KDDI][DAN]BearerData(str): "

    #@30
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v9

    #@34
    invoke-virtual {v1}, Lcom/android/internal/telephony/cdma/sms/BearerData;->toString()Ljava/lang/String;

    #@37
    move-result-object v10

    #@38
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v9

    #@3c
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v9

    #@40
    invoke-static {v9}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@43
    .line 2204
    new-instance v9, Ljava/lang/StringBuilder;

    #@45
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@48
    const-string v10, "privateGetDomainNotiPdu(), [KDDI][DAN]BearerData(bin) : "

    #@4a
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v9

    #@4e
    invoke-static {v4}, Lcom/android/internal/util/HexDump;->toHexString([B)Ljava/lang/String;

    #@51
    move-result-object v10

    #@52
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v9

    #@56
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v9

    #@5a
    invoke-static {v9}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@5d
    .line 2206
    if-nez v4, :cond_61

    #@5f
    move-object v7, v8

    #@60
    .line 2243
    :goto_60
    return-object v7

    #@61
    .line 2209
    :cond_61
    new-instance v5, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;

    #@63
    invoke-direct {v5}, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;-><init>()V

    #@66
    .line 2210
    .local v5, envelope:Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;
    iput v11, v5, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->messageType:I

    #@68
    .line 2211
    const/16 v9, 0x1092

    #@6a
    iput v9, v5, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->teleService:I

    #@6c
    .line 2212
    iput-object v2, v5, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->destAddress:Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;

    #@6e
    .line 2213
    iput v12, v5, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->bearerReply:I

    #@70
    .line 2214
    iput-object v4, v5, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->bearerData:[B

    #@72
    .line 2216
    :try_start_72
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    #@74
    const/16 v9, 0x64

    #@76
    invoke-direct {v0, v9}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    #@79
    .line 2217
    .local v0, baos:Ljava/io/ByteArrayOutputStream;
    new-instance v3, Ljava/io/DataOutputStream;

    #@7b
    invoke-direct {v3, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    #@7e
    .line 2218
    .local v3, dos:Ljava/io/DataOutputStream;
    iget v9, v5, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->teleService:I

    #@80
    invoke-virtual {v3, v9}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@83
    .line 2219
    const/4 v9, 0x0

    #@84
    invoke-virtual {v3, v9}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@87
    .line 2220
    const/4 v9, 0x0

    #@88
    invoke-virtual {v3, v9}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@8b
    .line 2221
    iget v9, v2, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->digitMode:I

    #@8d
    invoke-virtual {v3, v9}, Ljava/io/DataOutputStream;->write(I)V

    #@90
    .line 2222
    iget v9, v2, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberMode:I

    #@92
    invoke-virtual {v3, v9}, Ljava/io/DataOutputStream;->write(I)V

    #@95
    .line 2223
    iget v9, v2, Lcom/android/internal/telephony/SmsAddress;->ton:I

    #@97
    invoke-virtual {v3, v9}, Ljava/io/DataOutputStream;->write(I)V

    #@9a
    .line 2224
    iget v9, v2, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberPlan:I

    #@9c
    invoke-virtual {v3, v9}, Ljava/io/DataOutputStream;->write(I)V

    #@9f
    .line 2225
    iget v9, v2, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberOfDigits:I

    #@a1
    invoke-virtual {v3, v9}, Ljava/io/DataOutputStream;->write(I)V

    #@a4
    .line 2226
    iget-object v9, v2, Lcom/android/internal/telephony/SmsAddress;->origBytes:[B

    #@a6
    const/4 v10, 0x0

    #@a7
    iget-object v11, v2, Lcom/android/internal/telephony/SmsAddress;->origBytes:[B

    #@a9
    array-length v11, v11

    #@aa
    invoke-virtual {v3, v9, v10, v11}, Ljava/io/DataOutputStream;->write([BII)V

    #@ad
    .line 2228
    const/4 v9, 0x0

    #@ae
    invoke-virtual {v3, v9}, Ljava/io/DataOutputStream;->write(I)V

    #@b1
    .line 2229
    const/4 v9, 0x0

    #@b2
    invoke-virtual {v3, v9}, Ljava/io/DataOutputStream;->write(I)V

    #@b5
    .line 2230
    const/4 v9, 0x0

    #@b6
    invoke-virtual {v3, v9}, Ljava/io/DataOutputStream;->write(I)V

    #@b9
    .line 2231
    array-length v9, v4

    #@ba
    invoke-virtual {v3, v9}, Ljava/io/DataOutputStream;->write(I)V

    #@bd
    .line 2232
    const/4 v9, 0x0

    #@be
    array-length v10, v4

    #@bf
    invoke-virtual {v3, v4, v9, v10}, Ljava/io/DataOutputStream;->write([BII)V

    #@c2
    .line 2233
    invoke-virtual {v3}, Ljava/io/DataOutputStream;->close()V

    #@c5
    .line 2235
    new-instance v7, Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;

    #@c7
    invoke-direct {v7}, Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;-><init>()V

    #@ca
    .line 2236
    .local v7, pdu:Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@cd
    move-result-object v9

    #@ce
    iput-object v9, v7, Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;->encodedMessage:[B

    #@d0
    .line 2237
    new-instance v9, Ljava/lang/StringBuilder;

    #@d2
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@d5
    const-string v10, "privateGetDomainNotiPdu(), [KDDI][DAN] encodeMessage : "

    #@d7
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@da
    move-result-object v9

    #@db
    iget-object v10, v7, Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;->encodedMessage:[B

    #@dd
    invoke-static {v10}, Lcom/android/internal/util/HexDump;->toHexString([B)Ljava/lang/String;

    #@e0
    move-result-object v10

    #@e1
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e4
    move-result-object v9

    #@e5
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e8
    move-result-object v9

    #@e9
    invoke-static {v9}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@ec
    .line 2238
    const/4 v9, 0x0

    #@ed
    iput-object v9, v7, Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;->encodedScAddress:[B
    :try_end_ef
    .catch Ljava/io/IOException; {:try_start_72 .. :try_end_ef} :catch_f1

    #@ef
    goto/16 :goto_60

    #@f1
    .line 2240
    .end local v0           #baos:Ljava/io/ByteArrayOutputStream;
    .end local v3           #dos:Ljava/io/DataOutputStream;
    .end local v7           #pdu:Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;
    :catch_f1
    move-exception v6

    #@f2
    .line 2241
    .local v6, ex:Ljava/io/IOException;
    new-instance v9, Ljava/lang/StringBuilder;

    #@f4
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@f7
    const-string v10, "privateGetDomainNotiPdu(), [KDDI][DAN]creating SubmitPdu failed: "

    #@f9
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fc
    move-result-object v9

    #@fd
    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@100
    move-result-object v9

    #@101
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@104
    move-result-object v9

    #@105
    invoke-static {v9}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@108
    move-object v7, v8

    #@109
    .line 2243
    goto/16 :goto_60
.end method

.method private static privateGetSubmitPdu(Ljava/lang/String;ZLcom/android/internal/telephony/cdma/sms/UserData;)Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;
    .registers 15
    .parameter "destAddrStr"
    .parameter "statusReportRequested"
    .parameter "userData"

    #@0
    .prologue
    .line 1654
    invoke-static {p0}, Landroid/telephony/PhoneNumberUtils;->cdmaCheckAndProcessPlusCode(Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v9

    #@4
    invoke-static {v9}, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->parse(Ljava/lang/String;)Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;

    #@7
    move-result-object v2

    #@8
    .line 1656
    .local v2, destAddr:Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;
    if-nez v2, :cond_c

    #@a
    const/4 v7, 0x0

    #@b
    .line 1773
    :goto_b
    return-object v7

    #@c
    .line 1658
    :cond_c
    new-instance v1, Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@e
    invoke-direct {v1}, Lcom/android/internal/telephony/cdma/sms/BearerData;-><init>()V

    #@11
    .line 1659
    .local v1, bearerData:Lcom/android/internal/telephony/cdma/sms/BearerData;
    const/4 v9, 0x2

    #@12
    iput v9, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->messageType:I

    #@14
    .line 1661
    invoke-static {}, Lcom/android/internal/telephony/cdma/SmsMessage;->getNextMessageId()I

    #@17
    move-result v9

    #@18
    iput v9, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->messageId:I

    #@1a
    .line 1663
    iput-boolean p1, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->deliveryAckReq:Z

    #@1c
    .line 1664
    const/4 v9, 0x0

    #@1d
    iput-boolean v9, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->userAckReq:Z

    #@1f
    .line 1665
    const/4 v9, 0x0

    #@20
    iput-boolean v9, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->readAckReq:Z

    #@22
    .line 1666
    const/4 v9, 0x0

    #@23
    iput-boolean v9, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->reportReq:Z

    #@25
    .line 1669
    const/4 v9, 0x0

    #@26
    const-string v10, "cdma_priority_indicator"

    #@28
    invoke-static {v9, v10}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@2b
    move-result v9

    #@2c
    if-eqz v9, :cond_3a

    #@2e
    .line 1670
    sget v9, Lcom/android/internal/telephony/cdma/SmsMessage;->mSubmitPriority:I

    #@30
    const/4 v10, 0x2

    #@31
    if-ne v9, v10, :cond_3a

    #@33
    .line 1671
    const/4 v9, 0x1

    #@34
    iput-boolean v9, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->priorityIndicatorSet:Z

    #@36
    .line 1672
    sget v9, Lcom/android/internal/telephony/cdma/SmsMessage;->mSubmitPriority:I

    #@38
    iput v9, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->priority:I

    #@3a
    .line 1678
    :cond_3a
    invoke-static {}, Lcom/android/internal/telephony/cdma/SmsMessage;->isKSC5601Encoding()Z

    #@3d
    move-result v9

    #@3e
    if-eqz v9, :cond_53

    #@40
    .line 1679
    const-string v9, "privateGetSubmitPdu(), KSC5601 Encoding = true"

    #@42
    invoke-static {v9}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@45
    .line 1681
    const/4 v9, 0x1

    #@46
    iput-boolean v9, p2, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncodingSet:Z

    #@48
    .line 1682
    const/16 v9, 0x10

    #@4a
    iput v9, p2, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncoding:I

    #@4c
    .line 1684
    const/4 v9, 0x1

    #@4d
    iput-boolean v9, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->languageIndicatorSet:Z

    #@4f
    .line 1685
    const/16 v9, 0x40

    #@51
    iput v9, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->language:I

    #@53
    .line 1690
    :cond_53
    const/4 v9, 0x0

    #@54
    const-string v10, "lgu_userdata_encoding"

    #@56
    invoke-static {v9, v10}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@59
    move-result v9

    #@5a
    if-eqz v9, :cond_b1

    #@5c
    .line 1691
    const/4 v9, 0x1

    #@5d
    iput-boolean v9, p2, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncodingSet:Z

    #@5f
    .line 1692
    const/16 v9, 0x10

    #@61
    iput v9, p2, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncoding:I

    #@63
    .line 1694
    const/4 v9, 0x1

    #@64
    iput-boolean v9, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->languageIndicatorSet:Z

    #@66
    .line 1695
    const/16 v9, 0x40

    #@68
    iput v9, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->language:I

    #@6a
    .line 1697
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@6d
    move-result-object v9

    #@6e
    invoke-virtual {v9}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    #@71
    move-result-object v9

    #@72
    if-nez v9, :cond_bc

    #@74
    .line 1698
    const/4 v9, 0x0

    #@75
    iput-object v9, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->callbackNumber:Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;

    #@77
    .line 1704
    :goto_77
    const/4 v9, 0x1

    #@78
    iput-boolean v9, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->priorityIndicatorSet:Z

    #@7a
    .line 1705
    const/4 v9, 0x0

    #@7b
    iput v9, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->priority:I

    #@7d
    .line 1709
    new-instance v9, Ljava/lang/StringBuilder;

    #@7f
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@82
    const-string v10, "privateGetSubmitPdu(), "

    #@84
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v9

    #@88
    invoke-virtual {v1}, Lcom/android/internal/telephony/cdma/sms/BearerData;->toString()Ljava/lang/String;

    #@8b
    move-result-object v10

    #@8c
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v9

    #@90
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@93
    move-result-object v9

    #@94
    invoke-static {v9}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@97
    .line 1710
    new-instance v9, Ljava/lang/StringBuilder;

    #@99
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@9c
    const-string v10, "privateGetSubmitPdu(), "

    #@9e
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v9

    #@a2
    invoke-virtual {v2}, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->toString()Ljava/lang/String;

    #@a5
    move-result-object v10

    #@a6
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v9

    #@aa
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ad
    move-result-object v9

    #@ae
    invoke-static {v9}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->i(Ljava/lang/String;)I

    #@b1
    .line 1714
    :cond_b1
    iput-object p2, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@b3
    .line 1716
    invoke-static {v1}, Lcom/android/internal/telephony/cdma/sms/BearerData;->encode(Lcom/android/internal/telephony/cdma/sms/BearerData;)[B

    #@b6
    move-result-object v4

    #@b7
    .line 1717
    .local v4, encodedBearerData:[B
    if-nez v4, :cond_d3

    #@b9
    const/4 v7, 0x0

    #@ba
    goto/16 :goto_b

    #@bc
    .line 1700
    .end local v4           #encodedBearerData:[B
    :cond_bc
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@bf
    move-result-object v9

    #@c0
    invoke-virtual {v9}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    #@c3
    move-result-object v9

    #@c4
    const-string v10, "+82"

    #@c6
    const-string v11, "0"

    #@c8
    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    #@cb
    move-result-object v9

    #@cc
    invoke-static {v9}, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->parse(Ljava/lang/String;)Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;

    #@cf
    move-result-object v9

    #@d0
    iput-object v9, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->callbackNumber:Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;

    #@d2
    goto :goto_77

    #@d3
    .line 1720
    .restart local v4       #encodedBearerData:[B
    :cond_d3
    invoke-static {}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isUserMode()Z

    #@d6
    move-result v9

    #@d7
    if-nez v9, :cond_113

    #@d9
    .line 1721
    const-string v9, "CDMA"

    #@db
    new-instance v10, Ljava/lang/StringBuilder;

    #@dd
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@e0
    const-string v11, "privateGetSubmitPdu() - MO (encoded) BearerData = "

    #@e2
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e5
    move-result-object v10

    #@e6
    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@e9
    move-result-object v10

    #@ea
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ed
    move-result-object v10

    #@ee
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f1
    .line 1722
    const-string v9, "CDMA"

    #@f3
    new-instance v10, Ljava/lang/StringBuilder;

    #@f5
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@f8
    const-string v11, "privateGetSubmitPdu() - MO raw BearerData = \'"

    #@fa
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fd
    move-result-object v10

    #@fe
    invoke-static {v4}, Lcom/android/internal/util/HexDump;->toHexString([B)Ljava/lang/String;

    #@101
    move-result-object v11

    #@102
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@105
    move-result-object v10

    #@106
    const-string v11, "\'"

    #@108
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10b
    move-result-object v10

    #@10c
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10f
    move-result-object v10

    #@110
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@113
    .line 1726
    :cond_113
    iget-boolean v9, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->hasUserDataHeader:Z

    #@115
    if-eqz v9, :cond_1a9

    #@117
    const/16 v8, 0x1005

    #@119
    .line 1730
    .local v8, teleservice:I
    :goto_119
    new-instance v5, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;

    #@11b
    invoke-direct {v5}, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;-><init>()V

    #@11e
    .line 1731
    .local v5, envelope:Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;
    const/4 v9, 0x0

    #@11f
    iput v9, v5, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->messageType:I

    #@121
    .line 1732
    iput v8, v5, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->teleService:I

    #@123
    .line 1733
    iput-object v2, v5, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->destAddress:Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;

    #@125
    .line 1734
    const/4 v9, 0x1

    #@126
    iput v9, v5, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->bearerReply:I

    #@128
    .line 1735
    iput-object v4, v5, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->bearerData:[B

    #@12a
    .line 1747
    :try_start_12a
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    #@12c
    const/16 v9, 0x64

    #@12e
    invoke-direct {v0, v9}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    #@131
    .line 1748
    .local v0, baos:Ljava/io/ByteArrayOutputStream;
    new-instance v3, Ljava/io/DataOutputStream;

    #@133
    invoke-direct {v3, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    #@136
    .line 1749
    .local v3, dos:Ljava/io/DataOutputStream;
    iget v9, v5, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->teleService:I

    #@138
    invoke-virtual {v3, v9}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@13b
    .line 1750
    const/4 v9, 0x0

    #@13c
    invoke-virtual {v3, v9}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@13f
    .line 1751
    const/4 v9, 0x0

    #@140
    invoke-virtual {v3, v9}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@143
    .line 1752
    iget v9, v2, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->digitMode:I

    #@145
    invoke-virtual {v3, v9}, Ljava/io/DataOutputStream;->write(I)V

    #@148
    .line 1753
    iget v9, v2, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberMode:I

    #@14a
    invoke-virtual {v3, v9}, Ljava/io/DataOutputStream;->write(I)V

    #@14d
    .line 1754
    iget v9, v2, Lcom/android/internal/telephony/SmsAddress;->ton:I

    #@14f
    invoke-virtual {v3, v9}, Ljava/io/DataOutputStream;->write(I)V

    #@152
    .line 1755
    iget v9, v2, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberPlan:I

    #@154
    invoke-virtual {v3, v9}, Ljava/io/DataOutputStream;->write(I)V

    #@157
    .line 1756
    iget v9, v2, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberOfDigits:I

    #@159
    invoke-virtual {v3, v9}, Ljava/io/DataOutputStream;->write(I)V

    #@15c
    .line 1757
    iget-object v9, v2, Lcom/android/internal/telephony/SmsAddress;->origBytes:[B

    #@15e
    const/4 v10, 0x0

    #@15f
    iget-object v11, v2, Lcom/android/internal/telephony/SmsAddress;->origBytes:[B

    #@161
    array-length v11, v11

    #@162
    invoke-virtual {v3, v9, v10, v11}, Ljava/io/DataOutputStream;->write([BII)V

    #@165
    .line 1759
    const/4 v9, 0x0

    #@166
    invoke-virtual {v3, v9}, Ljava/io/DataOutputStream;->write(I)V

    #@169
    .line 1760
    const/4 v9, 0x0

    #@16a
    invoke-virtual {v3, v9}, Ljava/io/DataOutputStream;->write(I)V

    #@16d
    .line 1761
    const/4 v9, 0x0

    #@16e
    invoke-virtual {v3, v9}, Ljava/io/DataOutputStream;->write(I)V

    #@171
    .line 1762
    array-length v9, v4

    #@172
    invoke-virtual {v3, v9}, Ljava/io/DataOutputStream;->write(I)V

    #@175
    .line 1763
    const/4 v9, 0x0

    #@176
    array-length v10, v4

    #@177
    invoke-virtual {v3, v4, v9, v10}, Ljava/io/DataOutputStream;->write([BII)V

    #@17a
    .line 1764
    invoke-virtual {v3}, Ljava/io/DataOutputStream;->close()V

    #@17d
    .line 1766
    new-instance v7, Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;

    #@17f
    invoke-direct {v7}, Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;-><init>()V

    #@182
    .line 1767
    .local v7, pdu:Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@185
    move-result-object v9

    #@186
    iput-object v9, v7, Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;->encodedMessage:[B

    #@188
    .line 1768
    const/4 v9, 0x0

    #@189
    iput-object v9, v7, Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;->encodedScAddress:[B
    :try_end_18b
    .catch Ljava/io/IOException; {:try_start_12a .. :try_end_18b} :catch_18d

    #@18b
    goto/16 :goto_b

    #@18d
    .line 1770
    .end local v0           #baos:Ljava/io/ByteArrayOutputStream;
    .end local v3           #dos:Ljava/io/DataOutputStream;
    .end local v7           #pdu:Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;
    :catch_18d
    move-exception v6

    #@18e
    .line 1771
    .local v6, ex:Ljava/io/IOException;
    const-string v9, "CDMA"

    #@190
    new-instance v10, Ljava/lang/StringBuilder;

    #@192
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@195
    const-string v11, "creating SubmitPdu failed: "

    #@197
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19a
    move-result-object v10

    #@19b
    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19e
    move-result-object v10

    #@19f
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a2
    move-result-object v10

    #@1a3
    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1a6
    .line 1773
    const/4 v7, 0x0

    #@1a7
    goto/16 :goto_b

    #@1a9
    .line 1726
    .end local v5           #envelope:Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;
    .end local v6           #ex:Ljava/io/IOException;
    .end local v8           #teleservice:I
    :cond_1a9
    const/16 v8, 0x1002

    #@1ab
    goto/16 :goto_119
.end method

.method private static privateGetSubmitPdu(Ljava/lang/String;ZLcom/android/internal/telephony/cdma/sms/UserData;Ljava/lang/String;)Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;
    .registers 18
    .parameter "destAddrStr"
    .parameter "statusReportRequested"
    .parameter "userData"
    .parameter "cbAddress"

    #@0
    .prologue
    .line 1794
    invoke-static {p0}, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->parse(Ljava/lang/String;)Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;

    #@3
    move-result-object v4

    #@4
    .line 1795
    .local v4, destAddr:Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;
    invoke-static/range {p3 .. p3}, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->parse(Ljava/lang/String;)Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;

    #@7
    move-result-object v3

    #@8
    .line 1798
    .local v3, cbAddrStr:Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;
    invoke-static {}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isUserMode()Z

    #@b
    move-result v11

    #@c
    if-nez v11, :cond_70

    #@e
    .line 1799
    const-string v11, "CDMA"

    #@10
    new-instance v12, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v13, "privateGetSubmitPdu() - raw destAddrStr : "

    #@17
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v12

    #@1b
    invoke-virtual {v12, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v12

    #@1f
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v12

    #@23
    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 1800
    const-string v11, "CDMA"

    #@28
    new-instance v12, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v13, "privateGetSubmitPdu() - raw cbAddress : "

    #@2f
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v12

    #@33
    move-object/from16 v0, p3

    #@35
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v12

    #@39
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v12

    #@3d
    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    .line 1801
    const-string v11, "CDMA"

    #@42
    new-instance v12, Ljava/lang/StringBuilder;

    #@44
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    const-string v13, "privateGetSubmitPdu() - parse destAddr = "

    #@49
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v12

    #@4d
    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v12

    #@51
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v12

    #@55
    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@58
    .line 1802
    const-string v11, "CDMA"

    #@5a
    new-instance v12, Ljava/lang/StringBuilder;

    #@5c
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@5f
    const-string v13, "privateGetSubmitPdu() - parse cbAddrStr: "

    #@61
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v12

    #@65
    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v12

    #@69
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6c
    move-result-object v12

    #@6d
    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@70
    .line 1806
    :cond_70
    if-nez v4, :cond_74

    #@72
    const/4 v9, 0x0

    #@73
    .line 1901
    :goto_73
    return-object v9

    #@74
    .line 1807
    :cond_74
    if-nez v3, :cond_78

    #@76
    const/4 v9, 0x0

    #@77
    goto :goto_73

    #@78
    .line 1809
    :cond_78
    new-instance v2, Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@7a
    invoke-direct {v2}, Lcom/android/internal/telephony/cdma/sms/BearerData;-><init>()V

    #@7d
    .line 1811
    .local v2, bearerData:Lcom/android/internal/telephony/cdma/sms/BearerData;
    const/4 v11, 0x2

    #@7e
    iput v11, v2, Lcom/android/internal/telephony/cdma/sms/BearerData;->messageType:I

    #@80
    .line 1812
    invoke-static {}, Lcom/android/internal/telephony/cdma/SmsMessage;->getNextMessageId()I

    #@83
    move-result v11

    #@84
    iput v11, v2, Lcom/android/internal/telephony/cdma/sms/BearerData;->messageId:I

    #@86
    .line 1814
    iput-boolean p1, v2, Lcom/android/internal/telephony/cdma/sms/BearerData;->deliveryAckReq:Z

    #@88
    .line 1815
    const/4 v11, 0x0

    #@89
    iput-boolean v11, v2, Lcom/android/internal/telephony/cdma/sms/BearerData;->userAckReq:Z

    #@8b
    .line 1816
    const/4 v11, 0x0

    #@8c
    iput-boolean v11, v2, Lcom/android/internal/telephony/cdma/sms/BearerData;->readAckReq:Z

    #@8e
    .line 1817
    const/4 v11, 0x0

    #@8f
    iput-boolean v11, v2, Lcom/android/internal/telephony/cdma/sms/BearerData;->reportReq:Z

    #@91
    .line 1820
    const/4 v11, 0x0

    #@92
    const-string v12, "cdma_priority_indicator"

    #@94
    invoke-static {v11, v12}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@97
    move-result v11

    #@98
    if-eqz v11, :cond_a6

    #@9a
    .line 1821
    sget v11, Lcom/android/internal/telephony/cdma/SmsMessage;->mSubmitPriority:I

    #@9c
    const/4 v12, 0x2

    #@9d
    if-ne v11, v12, :cond_a6

    #@9f
    .line 1822
    const/4 v11, 0x1

    #@a0
    iput-boolean v11, v2, Lcom/android/internal/telephony/cdma/sms/BearerData;->priorityIndicatorSet:Z

    #@a2
    .line 1823
    sget v11, Lcom/android/internal/telephony/cdma/SmsMessage;->mSubmitPriority:I

    #@a4
    iput v11, v2, Lcom/android/internal/telephony/cdma/sms/BearerData;->priority:I

    #@a6
    .line 1829
    :cond_a6
    invoke-static {}, Lcom/android/internal/telephony/cdma/SmsMessage;->isKSC5601Encoding()Z

    #@a9
    move-result v11

    #@aa
    if-eqz v11, :cond_c5

    #@ac
    .line 1830
    const-string v11, "CDMA"

    #@ae
    const-string v12, "privateGetSubmitPdu() with Callback Number - KSC5601 Encoding : true"

    #@b0
    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b3
    .line 1832
    const/4 v11, 0x1

    #@b4
    move-object/from16 v0, p2

    #@b6
    iput-boolean v11, v0, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncodingSet:Z

    #@b8
    .line 1833
    const/16 v11, 0x10

    #@ba
    move-object/from16 v0, p2

    #@bc
    iput v11, v0, Lcom/android/internal/telephony/cdma/sms/UserData;->msgEncoding:I

    #@be
    .line 1835
    const/4 v11, 0x1

    #@bf
    iput-boolean v11, v2, Lcom/android/internal/telephony/cdma/sms/BearerData;->languageIndicatorSet:Z

    #@c1
    .line 1836
    const/16 v11, 0x40

    #@c3
    iput v11, v2, Lcom/android/internal/telephony/cdma/sms/BearerData;->language:I

    #@c5
    .line 1840
    :cond_c5
    move-object/from16 v0, p2

    #@c7
    iput-object v0, v2, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@c9
    .line 1841
    move-object/from16 v0, p2

    #@cb
    iget-object v11, v0, Lcom/android/internal/telephony/cdma/sms/UserData;->userDataHeader:Lcom/android/internal/telephony/SmsHeader;

    #@cd
    if-eqz v11, :cond_dc

    #@cf
    const/4 v11, 0x1

    #@d0
    :goto_d0
    iput-boolean v11, v2, Lcom/android/internal/telephony/cdma/sms/BearerData;->hasUserDataHeader:Z

    #@d2
    .line 1843
    iput-object v3, v2, Lcom/android/internal/telephony/cdma/sms/BearerData;->callbackNumber:Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;

    #@d4
    .line 1845
    invoke-static {v2}, Lcom/android/internal/telephony/cdma/sms/BearerData;->encode(Lcom/android/internal/telephony/cdma/sms/BearerData;)[B

    #@d7
    move-result-object v6

    #@d8
    .line 1846
    .local v6, encodedBearerData:[B
    if-nez v6, :cond_de

    #@da
    const/4 v9, 0x0

    #@db
    goto :goto_73

    #@dc
    .line 1841
    .end local v6           #encodedBearerData:[B
    :cond_dc
    const/4 v11, 0x0

    #@dd
    goto :goto_d0

    #@de
    .line 1849
    .restart local v6       #encodedBearerData:[B
    :cond_de
    invoke-static {}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isUserMode()Z

    #@e1
    move-result v11

    #@e2
    if-nez v11, :cond_11a

    #@e4
    .line 1850
    new-instance v11, Ljava/lang/StringBuilder;

    #@e6
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@e9
    const-string v12, "privateGetSubmitPdu(), MO (encoded) BearerData = "

    #@eb
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ee
    move-result-object v11

    #@ef
    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@f2
    move-result-object v11

    #@f3
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f6
    move-result-object v11

    #@f7
    invoke-static {v11}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@fa
    .line 1851
    new-instance v11, Ljava/lang/StringBuilder;

    #@fc
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@ff
    const-string v12, "privateGetSubmitPdu(), MO raw BearerData = \'"

    #@101
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@104
    move-result-object v11

    #@105
    invoke-static {v6}, Lcom/android/internal/util/HexDump;->toHexString([B)Ljava/lang/String;

    #@108
    move-result-object v12

    #@109
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10c
    move-result-object v11

    #@10d
    const-string v12, "\'"

    #@10f
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@112
    move-result-object v11

    #@113
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@116
    move-result-object v11

    #@117
    invoke-static {v11}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@11a
    .line 1855
    :cond_11a
    iget-boolean v11, v2, Lcom/android/internal/telephony/cdma/sms/BearerData;->hasUserDataHeader:Z

    #@11c
    if-eqz v11, :cond_1ae

    #@11e
    const/16 v10, 0x1005

    #@120
    .line 1858
    .local v10, teleservice:I
    :goto_120
    new-instance v7, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;

    #@122
    invoke-direct {v7}, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;-><init>()V

    #@125
    .line 1859
    .local v7, envelope:Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;
    const/4 v11, 0x0

    #@126
    iput v11, v7, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->messageType:I

    #@128
    .line 1860
    iput v10, v7, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->teleService:I

    #@12a
    .line 1861
    iput-object v4, v7, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->destAddress:Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;

    #@12c
    .line 1862
    const/4 v11, 0x1

    #@12d
    iput v11, v7, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->bearerReply:I

    #@12f
    .line 1863
    iput-object v6, v7, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->bearerData:[B

    #@131
    .line 1875
    :try_start_131
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    #@133
    const/16 v11, 0x64

    #@135
    invoke-direct {v1, v11}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    #@138
    .line 1876
    .local v1, baos:Ljava/io/ByteArrayOutputStream;
    new-instance v5, Ljava/io/DataOutputStream;

    #@13a
    invoke-direct {v5, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    #@13d
    .line 1877
    .local v5, dos:Ljava/io/DataOutputStream;
    iget v11, v7, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->teleService:I

    #@13f
    invoke-virtual {v5, v11}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@142
    .line 1878
    const/4 v11, 0x0

    #@143
    invoke-virtual {v5, v11}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@146
    .line 1879
    const/4 v11, 0x0

    #@147
    invoke-virtual {v5, v11}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@14a
    .line 1880
    iget v11, v4, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->digitMode:I

    #@14c
    invoke-virtual {v5, v11}, Ljava/io/DataOutputStream;->write(I)V

    #@14f
    .line 1881
    iget v11, v4, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberMode:I

    #@151
    invoke-virtual {v5, v11}, Ljava/io/DataOutputStream;->write(I)V

    #@154
    .line 1882
    iget v11, v4, Lcom/android/internal/telephony/SmsAddress;->ton:I

    #@156
    invoke-virtual {v5, v11}, Ljava/io/DataOutputStream;->write(I)V

    #@159
    .line 1883
    iget v11, v4, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberPlan:I

    #@15b
    invoke-virtual {v5, v11}, Ljava/io/DataOutputStream;->write(I)V

    #@15e
    .line 1884
    iget v11, v4, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberOfDigits:I

    #@160
    invoke-virtual {v5, v11}, Ljava/io/DataOutputStream;->write(I)V

    #@163
    .line 1885
    iget-object v11, v4, Lcom/android/internal/telephony/SmsAddress;->origBytes:[B

    #@165
    const/4 v12, 0x0

    #@166
    iget-object v13, v4, Lcom/android/internal/telephony/SmsAddress;->origBytes:[B

    #@168
    array-length v13, v13

    #@169
    invoke-virtual {v5, v11, v12, v13}, Ljava/io/DataOutputStream;->write([BII)V

    #@16c
    .line 1887
    const/4 v11, 0x0

    #@16d
    invoke-virtual {v5, v11}, Ljava/io/DataOutputStream;->write(I)V

    #@170
    .line 1888
    const/4 v11, 0x0

    #@171
    invoke-virtual {v5, v11}, Ljava/io/DataOutputStream;->write(I)V

    #@174
    .line 1889
    const/4 v11, 0x0

    #@175
    invoke-virtual {v5, v11}, Ljava/io/DataOutputStream;->write(I)V

    #@178
    .line 1890
    array-length v11, v6

    #@179
    invoke-virtual {v5, v11}, Ljava/io/DataOutputStream;->write(I)V

    #@17c
    .line 1891
    const/4 v11, 0x0

    #@17d
    array-length v12, v6

    #@17e
    invoke-virtual {v5, v6, v11, v12}, Ljava/io/DataOutputStream;->write([BII)V

    #@181
    .line 1892
    invoke-virtual {v5}, Ljava/io/DataOutputStream;->close()V

    #@184
    .line 1894
    new-instance v9, Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;

    #@186
    invoke-direct {v9}, Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;-><init>()V

    #@189
    .line 1895
    .local v9, pdu:Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@18c
    move-result-object v11

    #@18d
    iput-object v11, v9, Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;->encodedMessage:[B

    #@18f
    .line 1896
    const/4 v11, 0x0

    #@190
    iput-object v11, v9, Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;->encodedScAddress:[B
    :try_end_192
    .catch Ljava/io/IOException; {:try_start_131 .. :try_end_192} :catch_194

    #@192
    goto/16 :goto_73

    #@194
    .line 1898
    .end local v1           #baos:Ljava/io/ByteArrayOutputStream;
    .end local v5           #dos:Ljava/io/DataOutputStream;
    .end local v9           #pdu:Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;
    :catch_194
    move-exception v8

    #@195
    .line 1899
    .local v8, ex:Ljava/io/IOException;
    new-instance v11, Ljava/lang/StringBuilder;

    #@197
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@19a
    const-string v12, "privateGetSubmitPdu(), creating SubmitPdu failed: "

    #@19c
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19f
    move-result-object v11

    #@1a0
    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a3
    move-result-object v11

    #@1a4
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a7
    move-result-object v11

    #@1a8
    invoke-static {v11}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->e(Ljava/lang/String;)I

    #@1ab
    .line 1901
    const/4 v9, 0x0

    #@1ac
    goto/16 :goto_73

    #@1ae
    .line 1855
    .end local v7           #envelope:Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;
    .end local v8           #ex:Ljava/io/IOException;
    .end local v10           #teleservice:I
    :cond_1ae
    const/16 v10, 0x1002

    #@1b0
    goto/16 :goto_120
.end method

.method public static ruimGetDeliverPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/android/internal/telephony/SmsHeader;J)Lcom/android/internal/telephony/gsm/SmsMessage$DeliverPdu;
    .registers 9
    .parameter "scAddr"
    .parameter "destAddr"
    .parameter "message"
    .parameter "statusReportRequested"
    .parameter "smsHeader"
    .parameter "msgTime"

    #@0
    .prologue
    .line 962
    if-eqz p2, :cond_4

    #@2
    if-nez p1, :cond_6

    #@4
    .line 963
    :cond_4
    const/4 v1, 0x0

    #@5
    .line 969
    :goto_5
    return-object v1

    #@6
    .line 966
    :cond_6
    new-instance v0, Lcom/android/internal/telephony/cdma/sms/UserData;

    #@8
    invoke-direct {v0}, Lcom/android/internal/telephony/cdma/sms/UserData;-><init>()V

    #@b
    .line 967
    .local v0, uData:Lcom/android/internal/telephony/cdma/sms/UserData;
    iput-object p2, v0, Lcom/android/internal/telephony/cdma/sms/UserData;->payloadStr:Ljava/lang/String;

    #@d
    .line 968
    iput-object p4, v0, Lcom/android/internal/telephony/cdma/sms/UserData;->userDataHeader:Lcom/android/internal/telephony/SmsHeader;

    #@f
    .line 969
    invoke-static {p1, p3, v0, p5, p6}, Lcom/android/internal/telephony/cdma/SmsMessage;->ruimPrivateGetDeliverPdu(Ljava/lang/String;ZLcom/android/internal/telephony/cdma/sms/UserData;J)Lcom/android/internal/telephony/gsm/SmsMessage$DeliverPdu;

    #@12
    move-result-object v1

    #@13
    goto :goto_5
.end method

.method public static ruimGetSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/android/internal/telephony/SmsHeader;)Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;
    .registers 7
    .parameter "scAddr"
    .parameter "destAddr"
    .parameter "message"
    .parameter "statusReportRequested"
    .parameter "smsHeader"

    #@0
    .prologue
    .line 847
    if-eqz p2, :cond_4

    #@2
    if-nez p1, :cond_6

    #@4
    .line 848
    :cond_4
    const/4 v1, 0x0

    #@5
    .line 855
    :goto_5
    return-object v1

    #@6
    .line 851
    :cond_6
    new-instance v0, Lcom/android/internal/telephony/cdma/sms/UserData;

    #@8
    invoke-direct {v0}, Lcom/android/internal/telephony/cdma/sms/UserData;-><init>()V

    #@b
    .line 852
    .local v0, uData:Lcom/android/internal/telephony/cdma/sms/UserData;
    iput-object p2, v0, Lcom/android/internal/telephony/cdma/sms/UserData;->payloadStr:Ljava/lang/String;

    #@d
    .line 853
    iput-object p4, v0, Lcom/android/internal/telephony/cdma/sms/UserData;->userDataHeader:Lcom/android/internal/telephony/SmsHeader;

    #@f
    .line 855
    invoke-static {p1, p3, v0}, Lcom/android/internal/telephony/cdma/SmsMessage;->ruimPrivateGetSubmitPdu(Ljava/lang/String;ZLcom/android/internal/telephony/cdma/sms/UserData;)Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;

    #@12
    move-result-object v1

    #@13
    goto :goto_5
.end method

.method private static ruimPrivateGetDeliverPdu(Ljava/lang/String;ZLcom/android/internal/telephony/cdma/sms/UserData;J)Lcom/android/internal/telephony/gsm/SmsMessage$DeliverPdu;
    .registers 17
    .parameter "destAddrStr"
    .parameter "statusReportRequested"
    .parameter "userData"
    .parameter "msgTime"

    #@0
    .prologue
    .line 982
    invoke-static {p0}, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->parse(Ljava/lang/String;)Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;

    #@3
    move-result-object v2

    #@4
    .line 983
    .local v2, destAddr:Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;
    if-nez v2, :cond_8

    #@6
    const/4 v7, 0x0

    #@7
    .line 1053
    :goto_7
    return-object v7

    #@8
    .line 985
    :cond_8
    new-instance v1, Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@a
    invoke-direct {v1}, Lcom/android/internal/telephony/cdma/sms/BearerData;-><init>()V

    #@d
    .line 986
    .local v1, bearerData:Lcom/android/internal/telephony/cdma/sms/BearerData;
    const/4 v9, 0x1

    #@e
    iput v9, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->messageType:I

    #@10
    .line 988
    invoke-static {}, Lcom/android/internal/telephony/cdma/SmsMessage;->getNextMessageId()I

    #@13
    move-result v9

    #@14
    iput v9, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->messageId:I

    #@16
    .line 990
    iput-boolean p1, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->deliveryAckReq:Z

    #@18
    .line 991
    const/4 v9, 0x0

    #@19
    iput-boolean v9, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->userAckReq:Z

    #@1b
    .line 992
    const/4 v9, 0x0

    #@1c
    iput-boolean v9, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->readAckReq:Z

    #@1e
    .line 993
    const/4 v9, 0x0

    #@1f
    iput-boolean v9, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->reportReq:Z

    #@21
    .line 1000
    iput-object p2, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@23
    .line 1002
    iget-object v9, p2, Lcom/android/internal/telephony/cdma/sms/UserData;->userDataHeader:Lcom/android/internal/telephony/SmsHeader;

    #@25
    if-eqz v9, :cond_48

    #@27
    const/4 v9, 0x1

    #@28
    :goto_28
    iput-boolean v9, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->hasUserDataHeader:Z

    #@2a
    .line 1003
    const-wide/16 v9, 0x0

    #@2c
    cmp-long v9, p3, v9

    #@2e
    if-eqz v9, :cond_40

    #@30
    invoke-static/range {p3 .. p4}, Lcom/android/internal/telephony/cdma/SmsMessage;->getCdmaDeliverPduSCTS(J)[B

    #@33
    move-result-object v9

    #@34
    if-eqz v9, :cond_40

    #@36
    .line 1004
    invoke-static/range {p3 .. p4}, Lcom/android/internal/telephony/cdma/SmsMessage;->getCdmaDeliverPduSCTS(J)[B

    #@39
    move-result-object v6

    #@3a
    .line 1005
    .local v6, msgTimes:[B
    invoke-static {v6}, Lcom/android/internal/telephony/cdma/sms/BearerData$TimeStamp;->encodefromByteArray([B)Lcom/android/internal/telephony/cdma/sms/BearerData$TimeStamp;

    #@3d
    move-result-object v9

    #@3e
    iput-object v9, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->msgCenterTimeStamp:Lcom/android/internal/telephony/cdma/sms/BearerData$TimeStamp;

    #@40
    .line 1008
    .end local v6           #msgTimes:[B
    :cond_40
    invoke-static {v1}, Lcom/android/internal/telephony/cdma/sms/BearerData;->encode(Lcom/android/internal/telephony/cdma/sms/BearerData;)[B

    #@43
    move-result-object v4

    #@44
    .line 1010
    .local v4, encodedBearerData:[B
    if-nez v4, :cond_4a

    #@46
    const/4 v7, 0x0

    #@47
    goto :goto_7

    #@48
    .line 1002
    .end local v4           #encodedBearerData:[B
    :cond_48
    const/4 v9, 0x0

    #@49
    goto :goto_28

    #@4a
    .line 1012
    .restart local v4       #encodedBearerData:[B
    :cond_4a
    iget-boolean v9, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->hasUserDataHeader:Z

    #@4c
    if-eqz v9, :cond_cd

    #@4e
    const/16 v8, 0x1005

    #@50
    .line 1019
    .local v8, teleservice:I
    :goto_50
    :try_start_50
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    #@52
    const/16 v9, 0x64

    #@54
    invoke-direct {v0, v9}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    #@57
    .line 1020
    .local v0, baos:Ljava/io/ByteArrayOutputStream;
    new-instance v3, Ljava/io/DataOutputStream;

    #@59
    invoke-direct {v3, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    #@5c
    .line 1023
    .local v3, dos:Ljava/io/DataOutputStream;
    invoke-virtual {v3, v8}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@5f
    .line 1025
    const/4 v9, 0x0

    #@60
    invoke-virtual {v3, v9}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@63
    .line 1026
    const/4 v9, 0x0

    #@64
    invoke-virtual {v3, v9}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@67
    .line 1028
    iget v9, v2, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->digitMode:I

    #@69
    invoke-virtual {v3, v9}, Ljava/io/DataOutputStream;->write(I)V

    #@6c
    .line 1029
    iget v9, v2, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberMode:I

    #@6e
    invoke-virtual {v3, v9}, Ljava/io/DataOutputStream;->write(I)V

    #@71
    .line 1030
    iget v9, v2, Lcom/android/internal/telephony/SmsAddress;->ton:I

    #@73
    invoke-virtual {v3, v9}, Ljava/io/DataOutputStream;->write(I)V

    #@76
    .line 1031
    iget v9, v2, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberPlan:I

    #@78
    invoke-virtual {v3, v9}, Ljava/io/DataOutputStream;->write(I)V

    #@7b
    .line 1032
    iget v9, v2, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberOfDigits:I

    #@7d
    invoke-virtual {v3, v9}, Ljava/io/DataOutputStream;->write(I)V

    #@80
    .line 1033
    iget-object v9, v2, Lcom/android/internal/telephony/SmsAddress;->origBytes:[B

    #@82
    const/4 v10, 0x0

    #@83
    iget-object v11, v2, Lcom/android/internal/telephony/SmsAddress;->origBytes:[B

    #@85
    array-length v11, v11

    #@86
    invoke-virtual {v3, v9, v10, v11}, Ljava/io/DataOutputStream;->write([BII)V

    #@89
    .line 1036
    const/4 v9, 0x0

    #@8a
    invoke-virtual {v3, v9}, Ljava/io/DataOutputStream;->write(I)V

    #@8d
    .line 1037
    const/4 v9, 0x0

    #@8e
    invoke-virtual {v3, v9}, Ljava/io/DataOutputStream;->write(I)V

    #@91
    .line 1038
    const/4 v9, 0x0

    #@92
    invoke-virtual {v3, v9}, Ljava/io/DataOutputStream;->write(I)V

    #@95
    .line 1040
    array-length v9, v4

    #@96
    invoke-virtual {v3, v9}, Ljava/io/DataOutputStream;->write(I)V

    #@99
    .line 1041
    const/4 v9, 0x0

    #@9a
    array-length v10, v4

    #@9b
    invoke-virtual {v3, v4, v9, v10}, Ljava/io/DataOutputStream;->write([BII)V

    #@9e
    .line 1043
    invoke-virtual {v3}, Ljava/io/DataOutputStream;->close()V

    #@a1
    .line 1045
    new-instance v7, Lcom/android/internal/telephony/gsm/SmsMessage$DeliverPdu;

    #@a3
    invoke-direct {v7}, Lcom/android/internal/telephony/gsm/SmsMessage$DeliverPdu;-><init>()V

    #@a6
    .line 1046
    .local v7, pdu:Lcom/android/internal/telephony/gsm/SmsMessage$DeliverPdu;
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@a9
    move-result-object v9

    #@aa
    iput-object v9, v7, Lcom/android/internal/telephony/SmsMessageBase$DeliverPduBase;->encodedMessage:[B

    #@ac
    .line 1047
    const/4 v9, 0x0

    #@ad
    iput-object v9, v7, Lcom/android/internal/telephony/SmsMessageBase$DeliverPduBase;->encodedScAddress:[B
    :try_end_af
    .catch Ljava/io/IOException; {:try_start_50 .. :try_end_af} :catch_b1

    #@af
    goto/16 :goto_7

    #@b1
    .line 1050
    .end local v0           #baos:Ljava/io/ByteArrayOutputStream;
    .end local v3           #dos:Ljava/io/DataOutputStream;
    .end local v7           #pdu:Lcom/android/internal/telephony/gsm/SmsMessage$DeliverPdu;
    :catch_b1
    move-exception v5

    #@b2
    .line 1051
    .local v5, ex:Ljava/io/IOException;
    const-string v9, "CDMA"

    #@b4
    new-instance v10, Ljava/lang/StringBuilder;

    #@b6
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@b9
    const-string v11, "creating SubmitPdu failed: "

    #@bb
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@be
    move-result-object v10

    #@bf
    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c2
    move-result-object v10

    #@c3
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c6
    move-result-object v10

    #@c7
    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@ca
    .line 1053
    const/4 v7, 0x0

    #@cb
    goto/16 :goto_7

    #@cd
    .line 1012
    .end local v5           #ex:Ljava/io/IOException;
    .end local v8           #teleservice:I
    :cond_cd
    const/16 v8, 0x1002

    #@cf
    goto :goto_50
.end method

.method private static ruimPrivateGetSubmitPdu(Ljava/lang/String;ZLcom/android/internal/telephony/cdma/sms/UserData;)Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;
    .registers 15
    .parameter "destAddrStr"
    .parameter "statusReportRequested"
    .parameter "userData"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    const/4 v8, 0x0

    #@2
    .line 870
    invoke-static {p0}, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->parse(Ljava/lang/String;)Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;

    #@5
    move-result-object v2

    #@6
    .line 872
    .local v2, destAddr:Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;
    if-nez v2, :cond_a

    #@8
    move-object v6, v9

    #@9
    .line 936
    :goto_9
    return-object v6

    #@a
    .line 874
    :cond_a
    new-instance v1, Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@c
    invoke-direct {v1}, Lcom/android/internal/telephony/cdma/sms/BearerData;-><init>()V

    #@f
    .line 875
    .local v1, bearerData:Lcom/android/internal/telephony/cdma/sms/BearerData;
    const/4 v10, 0x2

    #@10
    iput v10, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->messageType:I

    #@12
    .line 877
    invoke-static {}, Lcom/android/internal/telephony/cdma/SmsMessage;->getNextMessageId()I

    #@15
    move-result v10

    #@16
    iput v10, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->messageId:I

    #@18
    .line 879
    iput-boolean p1, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->deliveryAckReq:Z

    #@1a
    .line 880
    iput-boolean v8, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->userAckReq:Z

    #@1c
    .line 881
    iput-boolean v8, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->readAckReq:Z

    #@1e
    .line 882
    iput-boolean v8, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->reportReq:Z

    #@20
    .line 889
    iput-object p2, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@22
    .line 892
    iget-object v10, p2, Lcom/android/internal/telephony/cdma/sms/UserData;->userDataHeader:Lcom/android/internal/telephony/SmsHeader;

    #@24
    if-eqz v10, :cond_27

    #@26
    const/4 v8, 0x1

    #@27
    :cond_27
    iput-boolean v8, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->hasUserDataHeader:Z

    #@29
    .line 893
    invoke-static {v1}, Lcom/android/internal/telephony/cdma/sms/BearerData;->encode(Lcom/android/internal/telephony/cdma/sms/BearerData;)[B

    #@2c
    move-result-object v4

    #@2d
    .line 894
    .local v4, encodedBearerData:[B
    if-nez v4, :cond_31

    #@2f
    move-object v6, v9

    #@30
    goto :goto_9

    #@31
    .line 896
    :cond_31
    iget-boolean v8, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->hasUserDataHeader:Z

    #@33
    if-eqz v8, :cond_b4

    #@35
    const/16 v7, 0x1005

    #@37
    .line 903
    .local v7, teleservice:I
    :goto_37
    :try_start_37
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    #@39
    const/16 v8, 0x64

    #@3b
    invoke-direct {v0, v8}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    #@3e
    .line 904
    .local v0, baos:Ljava/io/ByteArrayOutputStream;
    new-instance v3, Ljava/io/DataOutputStream;

    #@40
    invoke-direct {v3, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    #@43
    .line 908
    .local v3, dos:Ljava/io/DataOutputStream;
    invoke-virtual {v3, v7}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@46
    .line 909
    const/4 v8, 0x0

    #@47
    invoke-virtual {v3, v8}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@4a
    .line 910
    const/4 v8, 0x0

    #@4b
    invoke-virtual {v3, v8}, Ljava/io/DataOutputStream;->writeInt(I)V

    #@4e
    .line 912
    iget v8, v2, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->digitMode:I

    #@50
    invoke-virtual {v3, v8}, Ljava/io/DataOutputStream;->write(I)V

    #@53
    .line 913
    iget v8, v2, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberMode:I

    #@55
    invoke-virtual {v3, v8}, Ljava/io/DataOutputStream;->write(I)V

    #@58
    .line 914
    iget v8, v2, Lcom/android/internal/telephony/SmsAddress;->ton:I

    #@5a
    invoke-virtual {v3, v8}, Ljava/io/DataOutputStream;->write(I)V

    #@5d
    .line 915
    iget v8, v2, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberPlan:I

    #@5f
    invoke-virtual {v3, v8}, Ljava/io/DataOutputStream;->write(I)V

    #@62
    .line 916
    iget v8, v2, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberOfDigits:I

    #@64
    invoke-virtual {v3, v8}, Ljava/io/DataOutputStream;->write(I)V

    #@67
    .line 917
    iget-object v8, v2, Lcom/android/internal/telephony/SmsAddress;->origBytes:[B

    #@69
    const/4 v10, 0x0

    #@6a
    iget-object v11, v2, Lcom/android/internal/telephony/SmsAddress;->origBytes:[B

    #@6c
    array-length v11, v11

    #@6d
    invoke-virtual {v3, v8, v10, v11}, Ljava/io/DataOutputStream;->write([BII)V

    #@70
    .line 920
    const/4 v8, 0x0

    #@71
    invoke-virtual {v3, v8}, Ljava/io/DataOutputStream;->write(I)V

    #@74
    .line 921
    const/4 v8, 0x0

    #@75
    invoke-virtual {v3, v8}, Ljava/io/DataOutputStream;->write(I)V

    #@78
    .line 922
    const/4 v8, 0x0

    #@79
    invoke-virtual {v3, v8}, Ljava/io/DataOutputStream;->write(I)V

    #@7c
    .line 924
    array-length v8, v4

    #@7d
    invoke-virtual {v3, v8}, Ljava/io/DataOutputStream;->write(I)V

    #@80
    .line 925
    const/4 v8, 0x0

    #@81
    array-length v10, v4

    #@82
    invoke-virtual {v3, v4, v8, v10}, Ljava/io/DataOutputStream;->write([BII)V

    #@85
    .line 927
    invoke-virtual {v3}, Ljava/io/DataOutputStream;->close()V

    #@88
    .line 928
    new-instance v6, Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;

    #@8a
    invoke-direct {v6}, Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;-><init>()V

    #@8d
    .line 929
    .local v6, pdu:Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@90
    move-result-object v8

    #@91
    iput-object v8, v6, Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;->encodedMessage:[B

    #@93
    .line 930
    const/4 v8, 0x0

    #@94
    iput-object v8, v6, Lcom/android/internal/telephony/SmsMessageBase$SubmitPduBase;->encodedScAddress:[B
    :try_end_96
    .catch Ljava/io/IOException; {:try_start_37 .. :try_end_96} :catch_98

    #@96
    goto/16 :goto_9

    #@98
    .line 933
    .end local v0           #baos:Ljava/io/ByteArrayOutputStream;
    .end local v3           #dos:Ljava/io/DataOutputStream;
    .end local v6           #pdu:Lcom/android/internal/telephony/cdma/SmsMessage$SubmitPdu;
    :catch_98
    move-exception v5

    #@99
    .line 934
    .local v5, ex:Ljava/io/IOException;
    const-string v8, "CDMA"

    #@9b
    new-instance v10, Ljava/lang/StringBuilder;

    #@9d
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@a0
    const-string v11, "creating SubmitPdu failed: "

    #@a2
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v10

    #@a6
    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v10

    #@aa
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ad
    move-result-object v10

    #@ae
    invoke-static {v8, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@b1
    move-object v6, v9

    #@b2
    .line 936
    goto/16 :goto_9

    #@b4
    .line 896
    .end local v5           #ex:Ljava/io/IOException;
    .end local v7           #teleservice:I
    :cond_b4
    const/16 v7, 0x1002

    #@b6
    goto :goto_37
.end method

.method public static setSmsIsRoaming(Z)V
    .registers 3
    .parameter "isRoaming"

    #@0
    .prologue
    .line 2143
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "setSmsIsRoaming(), isRoaming = "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@16
    .line 2145
    sput-boolean p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mSubmitIsRoaming:Z

    #@18
    .line 2146
    return-void
.end method

.method public static setSmsPriority(I)V
    .registers 3
    .parameter "smsPriority"

    #@0
    .prologue
    .line 2051
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "setSmsPriority(), smsPriority = "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-static {v0}, Lcom/android/internal/telephony/lgeautoprofiling/LGSmsLog;->d(Ljava/lang/String;)I

    #@16
    .line 2052
    sput p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mSubmitPriority:I

    #@18
    .line 2053
    return-void
.end method

.method public static setTimeforSMSonCSIM(J)V
    .registers 2
    .parameter "timemillisec"

    #@0
    .prologue
    .line 366
    sput-wide p0, Lcom/android/internal/telephony/cdma/SmsMessage;->timeSmsOnCSim:J

    #@2
    .line 367
    return-void
.end method


# virtual methods
.method public getBearData()Lcom/android/internal/telephony/cdma/sms/BearerData;
    .registers 2

    #@0
    .prologue
    .line 1438
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mBearerData:Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@2
    return-object v0
.end method

.method public getBearerData()Lcom/android/internal/telephony/cdma/sms/BearerData;
    .registers 2

    #@0
    .prologue
    .line 2057
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mBearerData:Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@2
    return-object v0
.end method

.method getIncomingSmsFingerprint()[B
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 2000
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    #@3
    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    #@6
    .line 2002
    .local v0, output:Ljava/io/ByteArrayOutputStream;
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mEnvelope:Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;

    #@8
    iget v1, v1, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->teleService:I

    #@a
    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    #@d
    .line 2003
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mEnvelope:Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;

    #@f
    iget-object v1, v1, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->origAddress:Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;

    #@11
    iget-object v1, v1, Lcom/android/internal/telephony/SmsAddress;->origBytes:[B

    #@13
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mEnvelope:Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;

    #@15
    iget-object v2, v2, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->origAddress:Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;

    #@17
    iget-object v2, v2, Lcom/android/internal/telephony/SmsAddress;->origBytes:[B

    #@19
    array-length v2, v2

    #@1a
    invoke-virtual {v0, v1, v3, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    #@1d
    .line 2004
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mEnvelope:Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;

    #@1f
    iget-object v1, v1, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->bearerData:[B

    #@21
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mEnvelope:Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;

    #@23
    iget-object v2, v2, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->bearerData:[B

    #@25
    array-length v2, v2

    #@26
    invoke-virtual {v0, v1, v3, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    #@29
    .line 2005
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mEnvelope:Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;

    #@2b
    iget-object v1, v1, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->origSubaddress:Lcom/android/internal/telephony/cdma/sms/CdmaSmsSubaddress;

    #@2d
    iget-object v1, v1, Lcom/android/internal/telephony/cdma/sms/CdmaSmsSubaddress;->origBytes:[B

    #@2f
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mEnvelope:Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;

    #@31
    iget-object v2, v2, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->origSubaddress:Lcom/android/internal/telephony/cdma/sms/CdmaSmsSubaddress;

    #@33
    iget-object v2, v2, Lcom/android/internal/telephony/cdma/sms/CdmaSmsSubaddress;->origBytes:[B

    #@35
    array-length v2, v2

    #@36
    invoke-virtual {v0, v1, v3, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    #@39
    .line 2008
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@3c
    move-result-object v1

    #@3d
    return-object v1
.end method

.method public getMessageClass()Lcom/android/internal/telephony/SmsConstants$MessageClass;
    .registers 2

    #@0
    .prologue
    .line 1596
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mBearerData:Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@2
    iget v0, v0, Lcom/android/internal/telephony/cdma/sms/BearerData;->displayMode:I

    #@4
    if-nez v0, :cond_9

    #@6
    .line 1597
    sget-object v0, Lcom/android/internal/telephony/SmsConstants$MessageClass;->CLASS_0:Lcom/android/internal/telephony/SmsConstants$MessageClass;

    #@8
    .line 1599
    :goto_8
    return-object v0

    #@9
    :cond_9
    sget-object v0, Lcom/android/internal/telephony/SmsConstants$MessageClass;->UNKNOWN:Lcom/android/internal/telephony/SmsConstants$MessageClass;

    #@b
    goto :goto_8
.end method

.method getMessageType()I
    .registers 2

    #@0
    .prologue
    .line 1204
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mEnvelope:Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;

    #@2
    iget v0, v0, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->serviceCategory:I

    #@4
    if-eqz v0, :cond_8

    #@6
    .line 1205
    const/4 v0, 0x1

    #@7
    .line 1207
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public getNumFields()I
    .registers 2

    #@0
    .prologue
    .line 2065
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mBearerData:Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@2
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@4
    iget v0, v0, Lcom/android/internal/telephony/cdma/sms/UserData;->numFields:I

    #@6
    return v0
.end method

.method public getNumOfVoicemails()I
    .registers 2

    #@0
    .prologue
    .line 1989
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mBearerData:Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@2
    iget v0, v0, Lcom/android/internal/telephony/cdma/sms/BearerData;->numberOfMessages:I

    #@4
    return v0
.end method

.method public getOriginalAddress()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 2098
    invoke-super {p0}, Lcom/android/internal/telephony/SmsMessageBase;->getOriginatingAddress()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 2099
    .local v0, originatingAddress:Ljava/lang/String;
    if-nez v0, :cond_8

    #@6
    .line 2100
    const-string v0, ""

    #@8
    .line 2102
    .end local v0           #originatingAddress:Ljava/lang/String;
    :cond_8
    return-object v0
.end method

.method public getOriginatingAddress()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 2082
    const/4 v0, 0x0

    #@1
    const-string v1, "kr_address_spec"

    #@3
    invoke-static {v0, v1}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_22

    #@9
    .line 2083
    iget-object v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->cbAddress:Lcom/android/internal/telephony/SmsAddress;

    #@b
    if-eqz v0, :cond_1f

    #@d
    .line 2084
    iget-object v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->cbAddress:Lcom/android/internal/telephony/SmsAddress;

    #@f
    invoke-virtual {v0}, Lcom/android/internal/telephony/SmsAddress;->getAddressString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    if-nez v0, :cond_18

    #@15
    const-string v0, ""

    #@17
    .line 2088
    :goto_17
    return-object v0

    #@18
    .line 2084
    :cond_18
    iget-object v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->cbAddress:Lcom/android/internal/telephony/SmsAddress;

    #@1a
    invoke-virtual {v0}, Lcom/android/internal/telephony/SmsAddress;->getAddressString()Ljava/lang/String;

    #@1d
    move-result-object v0

    #@1e
    goto :goto_17

    #@1f
    .line 2086
    :cond_1f
    const-string v0, ""

    #@21
    goto :goto_17

    #@22
    .line 2088
    :cond_22
    invoke-super {p0}, Lcom/android/internal/telephony/SmsMessageBase;->getOriginatingAddress()Ljava/lang/String;

    #@25
    move-result-object v0

    #@26
    goto :goto_17
.end method

.method public getProtocolIdentifier()I
    .registers 3

    #@0
    .prologue
    .line 1062
    const-string v0, "CDMA"

    #@2
    const-string v1, "getProtocolIdentifier: is not supported in CDMA mode."

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1064
    const/4 v0, 0x0

    #@8
    return v0
.end method

.method getSmsCbProgramData()Ljava/util/ArrayList;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/telephony/cdma/CdmaSmsCbProgramData;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 2016
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mBearerData:Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@2
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/sms/BearerData;->serviceCategoryProgramData:Ljava/util/ArrayList;

    #@4
    return-object v0
.end method

.method public getSmsDisplayMode()I
    .registers 2

    #@0
    .prologue
    .line 2026
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mBearerData:Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 2027
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mBearerData:Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@6
    iget v0, v0, Lcom/android/internal/telephony/cdma/sms/BearerData;->displayMode:I

    #@8
    .line 2029
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x1

    #@a
    goto :goto_8
.end method

.method public getSmsEnvelope()Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;
    .registers 2

    #@0
    .prologue
    .line 2061
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mEnvelope:Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;

    #@2
    return-object v0
.end method

.method public getStatus()I
    .registers 2

    #@0
    .prologue
    .line 1128
    iget v0, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->status:I

    #@2
    shl-int/lit8 v0, v0, 0x10

    #@4
    return v0
.end method

.method public getTeleService()I
    .registers 2

    #@0
    .prologue
    .line 1190
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mEnvelope:Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;

    #@2
    iget v0, v0, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->teleService:I

    #@4
    return v0
.end method

.method public getTeleServiceId()I
    .registers 2

    #@0
    .prologue
    .line 2069
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mEnvelope:Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;

    #@2
    iget v0, v0, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->teleService:I

    #@4
    return v0
.end method

.method public isCphsMwiMessage()Z
    .registers 3

    #@0
    .prologue
    .line 1080
    const-string v0, "CDMA"

    #@2
    const-string v1, "isCphsMwiMessage: is not supported in CDMA mode."

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1081
    const/4 v0, 0x0

    #@8
    return v0
.end method

.method public isMWIClearMessage()Z
    .registers 2

    #@0
    .prologue
    .line 1088
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mBearerData:Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@2
    if-eqz v0, :cond_c

    #@4
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mBearerData:Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@6
    iget v0, v0, Lcom/android/internal/telephony/cdma/sms/BearerData;->numberOfMessages:I

    #@8
    if-nez v0, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method public isMWISetMessage()Z
    .registers 2

    #@0
    .prologue
    .line 1095
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mBearerData:Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@2
    if-eqz v0, :cond_c

    #@4
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mBearerData:Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@6
    iget v0, v0, Lcom/android/internal/telephony/cdma/sms/BearerData;->numberOfMessages:I

    #@8
    if-lez v0, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method public isMwiDontStore()Z
    .registers 2

    #@0
    .prologue
    .line 1102
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mBearerData:Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@2
    if-eqz v0, :cond_12

    #@4
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mBearerData:Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@6
    iget v0, v0, Lcom/android/internal/telephony/cdma/sms/BearerData;->numberOfMessages:I

    #@8
    if-lez v0, :cond_12

    #@a
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mBearerData:Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@c
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@e
    if-nez v0, :cond_12

    #@10
    const/4 v0, 0x1

    #@11
    :goto_11
    return v0

    #@12
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_11
.end method

.method public isMwiUrgentMessage()Z
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 1114
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mBearerData:Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@3
    if-eqz v1, :cond_20

    #@5
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mBearerData:Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@7
    iget-boolean v1, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->priorityIndicatorSet:Z

    #@9
    if-eqz v1, :cond_20

    #@b
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mBearerData:Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@d
    iget v1, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->priority:I

    #@f
    if-eq v1, v0, :cond_1f

    #@11
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mBearerData:Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@13
    iget v1, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->priority:I

    #@15
    const/4 v2, 0x2

    #@16
    if-eq v1, v2, :cond_1f

    #@18
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mBearerData:Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@1a
    iget v1, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->priority:I

    #@1c
    const/4 v2, 0x3

    #@1d
    if-ne v1, v2, :cond_20

    #@1f
    :cond_1f
    :goto_1f
    return v0

    #@20
    :cond_20
    const/4 v0, 0x0

    #@21
    goto :goto_1f
.end method

.method public isReplace()Z
    .registers 3

    #@0
    .prologue
    .line 1071
    const-string v0, "CDMA"

    #@2
    const-string v1, "isReplace: is not supported in CDMA mode."

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1072
    const/4 v0, 0x0

    #@8
    return v0
.end method

.method public isReplyPathPresent()Z
    .registers 3

    #@0
    .prologue
    .line 1140
    const-string v0, "CDMA"

    #@2
    const-string v1, "isReplyPathPresent: is not supported in CDMA mode."

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1141
    const/4 v0, 0x0

    #@8
    return v0
.end method

.method public isStatusReportMessage()Z
    .registers 3

    #@0
    .prologue
    .line 1133
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mBearerData:Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@2
    iget v0, v0, Lcom/android/internal/telephony/cdma/sms/BearerData;->messageType:I

    #@4
    const/4 v1, 0x4

    #@5
    if-ne v0, v1, :cond_9

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method parseBroadcastSms()Landroid/telephony/SmsCbMessage;
    .registers 14

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    const/4 v1, 0x2

    #@2
    .line 1572
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mEnvelope:Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;

    #@4
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->bearerData:[B

    #@6
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mEnvelope:Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;

    #@8
    iget v2, v2, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->serviceCategory:I

    #@a
    invoke-static {v0, v2}, Lcom/android/internal/telephony/cdma/sms/BearerData;->decode([BI)Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@d
    move-result-object v11

    #@e
    .line 1573
    .local v11, bData:Lcom/android/internal/telephony/cdma/sms/BearerData;
    if-nez v11, :cond_18

    #@10
    .line 1574
    const-string v0, "CDMA"

    #@12
    const-string v1, "BearerData.decode() returned null"

    #@14
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@17
    .line 1585
    :goto_17
    return-object v9

    #@18
    .line 1578
    :cond_18
    const-string v0, "CDMA:SMS"

    #@1a
    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@1d
    move-result v0

    #@1e
    if-eqz v0, :cond_40

    #@20
    .line 1579
    const-string v0, "CDMA"

    #@22
    new-instance v2, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v3, "MT raw BearerData = "

    #@29
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v2

    #@2d
    iget-object v3, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mEnvelope:Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;

    #@2f
    iget-object v3, v3, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->bearerData:[B

    #@31
    invoke-static {v3}, Lcom/android/internal/util/HexDump;->toHexString([B)Ljava/lang/String;

    #@34
    move-result-object v3

    #@35
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v2

    #@39
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v2

    #@3d
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    .line 1582
    :cond_40
    const-string v0, "gsm.operator.numeric"

    #@42
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@45
    move-result-object v12

    #@46
    .line 1583
    .local v12, plmn:Ljava/lang/String;
    new-instance v4, Landroid/telephony/SmsCbLocation;

    #@48
    invoke-direct {v4, v12}, Landroid/telephony/SmsCbLocation;-><init>(Ljava/lang/String;)V

    #@4b
    .line 1585
    .local v4, location:Landroid/telephony/SmsCbLocation;
    new-instance v0, Landroid/telephony/SmsCbMessage;

    #@4d
    const/4 v2, 0x1

    #@4e
    iget v3, v11, Lcom/android/internal/telephony/cdma/sms/BearerData;->messageId:I

    #@50
    iget-object v5, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mEnvelope:Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;

    #@52
    iget v5, v5, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->serviceCategory:I

    #@54
    invoke-virtual {v11}, Lcom/android/internal/telephony/cdma/sms/BearerData;->getLanguage()Ljava/lang/String;

    #@57
    move-result-object v6

    #@58
    iget-object v7, v11, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@5a
    iget-object v7, v7, Lcom/android/internal/telephony/cdma/sms/UserData;->payloadStr:Ljava/lang/String;

    #@5c
    iget v8, v11, Lcom/android/internal/telephony/cdma/sms/BearerData;->priority:I

    #@5e
    iget-object v10, v11, Lcom/android/internal/telephony/cdma/sms/BearerData;->cmasWarningInfo:Landroid/telephony/SmsCbCmasInfo;

    #@60
    invoke-direct/range {v0 .. v10}, Landroid/telephony/SmsCbMessage;-><init>(IIILandroid/telephony/SmsCbLocation;ILjava/lang/String;Ljava/lang/String;ILandroid/telephony/SmsCbEtwsInfo;Landroid/telephony/SmsCbCmasInfo;)V

    #@63
    move-object v9, v0

    #@64
    goto :goto_17
.end method

.method protected parseSms()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v3, 0x0

    #@2
    .line 1448
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mEnvelope:Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;

    #@4
    iget v0, v0, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->teleService:I

    #@6
    const/high16 v1, 0x4

    #@8
    if-ne v0, v1, :cond_24

    #@a
    .line 1449
    new-instance v0, Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@c
    invoke-direct {v0}, Lcom/android/internal/telephony/cdma/sms/BearerData;-><init>()V

    #@f
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mBearerData:Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@11
    .line 1450
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mEnvelope:Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;

    #@13
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->bearerData:[B

    #@15
    if-eqz v0, :cond_23

    #@17
    .line 1451
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mBearerData:Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@19
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mEnvelope:Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;

    #@1b
    iget-object v1, v1, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->bearerData:[B

    #@1d
    aget-byte v1, v1, v4

    #@1f
    and-int/lit16 v1, v1, 0xff

    #@21
    iput v1, v0, Lcom/android/internal/telephony/cdma/sms/BearerData;->numberOfMessages:I

    #@23
    .line 1566
    :cond_23
    :goto_23
    return-void

    #@24
    .line 1459
    :cond_24
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mEnvelope:Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;

    #@26
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->bearerData:[B

    #@28
    invoke-static {v0}, Lcom/android/internal/telephony/cdma/sms/BearerData;->decode([B)Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@2b
    move-result-object v0

    #@2c
    iput-object v0, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mBearerData:Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@2e
    .line 1460
    const-string v0, "CDMA:SMS"

    #@30
    const/4 v1, 0x2

    #@31
    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@34
    move-result v0

    #@35
    if-eqz v0, :cond_77

    #@37
    .line 1461
    const-string v0, "CDMA"

    #@39
    new-instance v1, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string v2, "MT raw BearerData = \'"

    #@40
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v1

    #@44
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mEnvelope:Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;

    #@46
    iget-object v2, v2, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->bearerData:[B

    #@48
    invoke-static {v2}, Lcom/android/internal/util/HexDump;->toHexString([B)Ljava/lang/String;

    #@4b
    move-result-object v2

    #@4c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v1

    #@50
    const-string v2, "\'"

    #@52
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v1

    #@56
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v1

    #@5a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5d
    .line 1463
    const-string v0, "CDMA"

    #@5f
    new-instance v1, Ljava/lang/StringBuilder;

    #@61
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@64
    const-string v2, "MT (decoded) BearerData = "

    #@66
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v1

    #@6a
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mBearerData:Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@6c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v1

    #@70
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@73
    move-result-object v1

    #@74
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@77
    .line 1465
    :cond_77
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mBearerData:Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@79
    iget v0, v0, Lcom/android/internal/telephony/cdma/sms/BearerData;->messageId:I

    #@7b
    iput v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->messageRef:I

    #@7d
    .line 1466
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mBearerData:Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@7f
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@81
    if-eqz v0, :cond_9b

    #@83
    .line 1467
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mBearerData:Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@85
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@87
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/sms/UserData;->payload:[B

    #@89
    iput-object v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->userData:[B

    #@8b
    .line 1468
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mBearerData:Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@8d
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@8f
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/sms/UserData;->userDataHeader:Lcom/android/internal/telephony/SmsHeader;

    #@91
    iput-object v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->userDataHeader:Lcom/android/internal/telephony/SmsHeader;

    #@93
    .line 1469
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mBearerData:Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@95
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@97
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/sms/UserData;->payloadStr:Ljava/lang/String;

    #@99
    iput-object v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->messageBody:Ljava/lang/String;

    #@9b
    .line 1473
    :cond_9b
    const-string v0, "kddi_check_orig_bytes"

    #@9d
    invoke-static {v3, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@a0
    move-result v0

    #@a1
    if-eqz v0, :cond_177

    #@a3
    .line 1475
    iget-object v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->originatingAddress:Lcom/android/internal/telephony/SmsAddress;

    #@a5
    if-eqz v0, :cond_ba

    #@a7
    iget-object v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->originatingAddress:Lcom/android/internal/telephony/SmsAddress;

    #@a9
    iget-object v0, v0, Lcom/android/internal/telephony/SmsAddress;->origBytes:[B

    #@ab
    if-eqz v0, :cond_ba

    #@ad
    .line 1476
    iget-object v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->originatingAddress:Lcom/android/internal/telephony/SmsAddress;

    #@af
    new-instance v1, Ljava/lang/String;

    #@b1
    iget-object v2, p0, Lcom/android/internal/telephony/SmsMessageBase;->originatingAddress:Lcom/android/internal/telephony/SmsAddress;

    #@b3
    iget-object v2, v2, Lcom/android/internal/telephony/SmsAddress;->origBytes:[B

    #@b5
    invoke-direct {v1, v2}, Ljava/lang/String;-><init>([B)V

    #@b8
    iput-object v1, v0, Lcom/android/internal/telephony/SmsAddress;->address:Ljava/lang/String;

    #@ba
    .line 1492
    :cond_ba
    :goto_ba
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mBearerData:Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@bc
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/sms/BearerData;->msgCenterTimeStamp:Lcom/android/internal/telephony/cdma/sms/BearerData$TimeStamp;

    #@be
    if-eqz v0, :cond_cb

    #@c0
    .line 1493
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mBearerData:Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@c2
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/sms/BearerData;->msgCenterTimeStamp:Lcom/android/internal/telephony/cdma/sms/BearerData$TimeStamp;

    #@c4
    const/4 v1, 0x1

    #@c5
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/cdma/sms/BearerData$TimeStamp;->toMillis(Z)J

    #@c8
    move-result-wide v0

    #@c9
    iput-wide v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->scTimeMillis:J

    #@cb
    .line 1499
    :cond_cb
    const-string v0, "cdma_priority_indicator"

    #@cd
    invoke-static {v3, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@d0
    move-result v0

    #@d1
    if-eqz v0, :cond_df

    #@d3
    .line 1500
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mBearerData:Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@d5
    iget-boolean v0, v0, Lcom/android/internal/telephony/cdma/sms/BearerData;->priorityIndicatorSet:Z

    #@d7
    if-eqz v0, :cond_18a

    #@d9
    .line 1501
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mBearerData:Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@db
    iget v0, v0, Lcom/android/internal/telephony/cdma/sms/BearerData;->priority:I

    #@dd
    sput v0, Lcom/android/internal/telephony/cdma/SmsMessage;->mDeliverPriority:I

    #@df
    .line 1509
    :cond_df
    :goto_df
    const-string v0, "cdma_privacy_indicator"

    #@e1
    invoke-static {v3, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@e4
    move-result v0

    #@e5
    if-eqz v0, :cond_f3

    #@e7
    .line 1510
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mBearerData:Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@e9
    iget-boolean v0, v0, Lcom/android/internal/telephony/cdma/sms/BearerData;->privacyIndicatorSet:Z

    #@eb
    if-eqz v0, :cond_18e

    #@ed
    .line 1511
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mBearerData:Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@ef
    iget v0, v0, Lcom/android/internal/telephony/cdma/sms/BearerData;->privacy:I

    #@f1
    sput v0, Lcom/android/internal/telephony/cdma/SmsMessage;->mPrivacyInd:I

    #@f3
    .line 1518
    :cond_f3
    :goto_f3
    const-string v0, "cdma_sms_display_mode"

    #@f5
    invoke-static {v3, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@f8
    move-result v0

    #@f9
    if-eqz v0, :cond_101

    #@fb
    .line 1519
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mBearerData:Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@fd
    iget v0, v0, Lcom/android/internal/telephony/cdma/sms/BearerData;->displayMode:I

    #@ff
    sput v0, Lcom/android/internal/telephony/cdma/SmsMessage;->mDisplayMode:I

    #@101
    .line 1524
    :cond_101
    const-string v0, "cdma_sms_callback"

    #@103
    invoke-static {v3, v0}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isSupportedFeature(Landroid/content/Context;Ljava/lang/String;)Z

    #@106
    move-result v0

    #@107
    if-eqz v0, :cond_115

    #@109
    .line 1525
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mBearerData:Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@10b
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/sms/BearerData;->callbackNumber:Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;

    #@10d
    if-eqz v0, :cond_115

    #@10f
    .line 1526
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mBearerData:Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@111
    iget-object v0, v0, Lcom/android/internal/telephony/cdma/sms/BearerData;->callbackNumber:Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;

    #@113
    iput-object v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->cbAddress:Lcom/android/internal/telephony/SmsAddress;

    #@115
    .line 1532
    :cond_115
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mBearerData:Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@117
    iget v0, v0, Lcom/android/internal/telephony/cdma/sms/BearerData;->messageType:I

    #@119
    const/4 v1, 0x4

    #@11a
    if-ne v0, v1, :cond_148

    #@11c
    .line 1540
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mBearerData:Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@11e
    iget-boolean v0, v0, Lcom/android/internal/telephony/cdma/sms/BearerData;->messageStatusSet:Z

    #@120
    if-nez v0, :cond_195

    #@122
    .line 1541
    const-string v1, "CDMA"

    #@124
    new-instance v0, Ljava/lang/StringBuilder;

    #@126
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@129
    const-string v2, "DELIVERY_ACK message without msgStatus ("

    #@12b
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12e
    move-result-object v2

    #@12f
    iget-object v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->userData:[B

    #@131
    if-nez v0, :cond_192

    #@133
    const-string v0, "also missing"

    #@135
    :goto_135
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@138
    move-result-object v0

    #@139
    const-string v2, " userData)."

    #@13b
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13e
    move-result-object v0

    #@13f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@142
    move-result-object v0

    #@143
    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@146
    .line 1544
    iput v4, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->status:I

    #@148
    .line 1559
    :cond_148
    :goto_148
    iget-object v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->messageBody:Ljava/lang/String;

    #@14a
    if-eqz v0, :cond_23

    #@14c
    .line 1560
    invoke-static {}, Lcom/android/internal/telephony/lgeautoprofiling/LgeAutoProfiling;->isUserMode()Z

    #@14f
    move-result v0

    #@150
    if-nez v0, :cond_172

    #@152
    .line 1561
    const-string v0, "CDMA"

    #@154
    new-instance v1, Ljava/lang/StringBuilder;

    #@156
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@159
    const-string v2, "parseSms() - SMS message body : \'"

    #@15b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15e
    move-result-object v1

    #@15f
    iget-object v2, p0, Lcom/android/internal/telephony/SmsMessageBase;->messageBody:Ljava/lang/String;

    #@161
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@164
    move-result-object v1

    #@165
    const-string v2, "\'"

    #@167
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16a
    move-result-object v1

    #@16b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16e
    move-result-object v1

    #@16f
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@172
    .line 1563
    :cond_172
    invoke-virtual {p0}, Lcom/android/internal/telephony/cdma/SmsMessage;->parseMessageBody()V

    #@175
    goto/16 :goto_23

    #@177
    .line 1483
    :cond_177
    iget-object v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->originatingAddress:Lcom/android/internal/telephony/SmsAddress;

    #@179
    if-eqz v0, :cond_ba

    #@17b
    .line 1484
    iget-object v0, p0, Lcom/android/internal/telephony/SmsMessageBase;->originatingAddress:Lcom/android/internal/telephony/SmsAddress;

    #@17d
    new-instance v1, Ljava/lang/String;

    #@17f
    iget-object v2, p0, Lcom/android/internal/telephony/SmsMessageBase;->originatingAddress:Lcom/android/internal/telephony/SmsAddress;

    #@181
    iget-object v2, v2, Lcom/android/internal/telephony/SmsAddress;->origBytes:[B

    #@183
    invoke-direct {v1, v2}, Ljava/lang/String;-><init>([B)V

    #@186
    iput-object v1, v0, Lcom/android/internal/telephony/SmsAddress;->address:Ljava/lang/String;

    #@188
    goto/16 :goto_ba

    #@18a
    .line 1503
    :cond_18a
    sput v4, Lcom/android/internal/telephony/cdma/SmsMessage;->mDeliverPriority:I

    #@18c
    goto/16 :goto_df

    #@18e
    .line 1513
    :cond_18e
    sput v4, Lcom/android/internal/telephony/cdma/SmsMessage;->mPrivacyInd:I

    #@190
    goto/16 :goto_f3

    #@192
    .line 1541
    :cond_192
    const-string v0, "does have"

    #@194
    goto :goto_135

    #@195
    .line 1546
    :cond_195
    iget-object v0, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mBearerData:Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@197
    iget v0, v0, Lcom/android/internal/telephony/cdma/sms/BearerData;->errorClass:I

    #@199
    shl-int/lit8 v0, v0, 0x8

    #@19b
    iput v0, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->status:I

    #@19d
    .line 1547
    iget v0, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->status:I

    #@19f
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mBearerData:Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@1a1
    iget v1, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->messageStatus:I

    #@1a3
    or-int/2addr v0, v1

    #@1a4
    iput v0, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->status:I

    #@1a6
    goto :goto_148
.end method

.method public replaceMessageBody(Ljava/lang/String;)Z
    .registers 6
    .parameter "newText"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 2108
    const/4 v0, 0x0

    #@2
    .line 2110
    .local v0, encodedBearerData:[B
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@5
    move-result v2

    #@6
    if-eqz v2, :cond_9

    #@8
    .line 2121
    :cond_8
    :goto_8
    return v1

    #@9
    .line 2113
    :cond_9
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mBearerData:Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@b
    iget-object v2, v2, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    #@d
    new-instance v3, Ljava/lang/String;

    #@f
    invoke-direct {v3, p1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    #@12
    iput-object v3, v2, Lcom/android/internal/telephony/cdma/sms/UserData;->payloadStr:Ljava/lang/String;

    #@14
    .line 2114
    iget-object v2, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mBearerData:Lcom/android/internal/telephony/cdma/sms/BearerData;

    #@16
    invoke-static {v2}, Lcom/android/internal/telephony/cdma/sms/BearerData;->encode(Lcom/android/internal/telephony/cdma/sms/BearerData;)[B

    #@19
    move-result-object v0

    #@1a
    .line 2116
    if-eqz v0, :cond_8

    #@1c
    .line 2119
    iget-object v1, p0, Lcom/android/internal/telephony/cdma/SmsMessage;->mEnvelope:Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;

    #@1e
    iput-object v0, v1, Lcom/android/internal/telephony/cdma/sms/SmsEnvelope;->bearerData:[B

    #@20
    .line 2120
    invoke-direct {p0}, Lcom/android/internal/telephony/cdma/SmsMessage;->createPdu()V

    #@23
    .line 2121
    const/4 v1, 0x1

    #@24
    goto :goto_8
.end method
