.class public Lcom/android/internal/telephony/cat/Input;
.super Ljava/lang/Object;
.source "Input.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/android/internal/telephony/cat/Input;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public defaultText:Ljava/lang/String;

.field public digitOnly:Z

.field public duration:Lcom/android/internal/telephony/cat/Duration;

.field public echo:Z

.field public helpAvailable:Z

.field public icon:Landroid/graphics/Bitmap;

.field public maxLen:I

.field public minLen:I

.field public packed:Z

.field public text:Ljava/lang/String;

.field public ucs2:Z

.field public yesNo:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 90
    new-instance v0, Lcom/android/internal/telephony/cat/Input$1;

    #@2
    invoke-direct {v0}, Lcom/android/internal/telephony/cat/Input$1;-><init>()V

    #@5
    sput-object v0, Lcom/android/internal/telephony/cat/Input;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method constructor <init>()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 41
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 42
    const-string v0, ""

    #@7
    iput-object v0, p0, Lcom/android/internal/telephony/cat/Input;->text:Ljava/lang/String;

    #@9
    .line 43
    iput-object v2, p0, Lcom/android/internal/telephony/cat/Input;->defaultText:Ljava/lang/String;

    #@b
    .line 44
    iput-object v2, p0, Lcom/android/internal/telephony/cat/Input;->icon:Landroid/graphics/Bitmap;

    #@d
    .line 45
    iput v1, p0, Lcom/android/internal/telephony/cat/Input;->minLen:I

    #@f
    .line 46
    const/4 v0, 0x1

    #@10
    iput v0, p0, Lcom/android/internal/telephony/cat/Input;->maxLen:I

    #@12
    .line 47
    iput-boolean v1, p0, Lcom/android/internal/telephony/cat/Input;->ucs2:Z

    #@14
    .line 48
    iput-boolean v1, p0, Lcom/android/internal/telephony/cat/Input;->packed:Z

    #@16
    .line 49
    iput-boolean v1, p0, Lcom/android/internal/telephony/cat/Input;->digitOnly:Z

    #@18
    .line 50
    iput-boolean v1, p0, Lcom/android/internal/telephony/cat/Input;->echo:Z

    #@1a
    .line 51
    iput-boolean v1, p0, Lcom/android/internal/telephony/cat/Input;->yesNo:Z

    #@1c
    .line 52
    iput-boolean v1, p0, Lcom/android/internal/telephony/cat/Input;->helpAvailable:Z

    #@1e
    .line 53
    iput-object v2, p0, Lcom/android/internal/telephony/cat/Input;->duration:Lcom/android/internal/telephony/cat/Duration;

    #@20
    .line 54
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 6
    .parameter "in"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x0

    #@2
    const/4 v1, 0x1

    #@3
    .line 56
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 57
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Lcom/android/internal/telephony/cat/Input;->text:Ljava/lang/String;

    #@c
    .line 58
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@f
    move-result-object v0

    #@10
    iput-object v0, p0, Lcom/android/internal/telephony/cat/Input;->defaultText:Ljava/lang/String;

    #@12
    .line 59
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    #@15
    move-result-object v0

    #@16
    check-cast v0, Landroid/graphics/Bitmap;

    #@18
    iput-object v0, p0, Lcom/android/internal/telephony/cat/Input;->icon:Landroid/graphics/Bitmap;

    #@1a
    .line 60
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@1d
    move-result v0

    #@1e
    iput v0, p0, Lcom/android/internal/telephony/cat/Input;->minLen:I

    #@20
    .line 61
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@23
    move-result v0

    #@24
    iput v0, p0, Lcom/android/internal/telephony/cat/Input;->maxLen:I

    #@26
    .line 62
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@29
    move-result v0

    #@2a
    if-ne v0, v1, :cond_64

    #@2c
    move v0, v1

    #@2d
    :goto_2d
    iput-boolean v0, p0, Lcom/android/internal/telephony/cat/Input;->ucs2:Z

    #@2f
    .line 63
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@32
    move-result v0

    #@33
    if-ne v0, v1, :cond_66

    #@35
    move v0, v1

    #@36
    :goto_36
    iput-boolean v0, p0, Lcom/android/internal/telephony/cat/Input;->packed:Z

    #@38
    .line 64
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3b
    move-result v0

    #@3c
    if-ne v0, v1, :cond_68

    #@3e
    move v0, v1

    #@3f
    :goto_3f
    iput-boolean v0, p0, Lcom/android/internal/telephony/cat/Input;->digitOnly:Z

    #@41
    .line 65
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@44
    move-result v0

    #@45
    if-ne v0, v1, :cond_6a

    #@47
    move v0, v1

    #@48
    :goto_48
    iput-boolean v0, p0, Lcom/android/internal/telephony/cat/Input;->echo:Z

    #@4a
    .line 66
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@4d
    move-result v0

    #@4e
    if-ne v0, v1, :cond_6c

    #@50
    move v0, v1

    #@51
    :goto_51
    iput-boolean v0, p0, Lcom/android/internal/telephony/cat/Input;->yesNo:Z

    #@53
    .line 67
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@56
    move-result v0

    #@57
    if-ne v0, v1, :cond_6e

    #@59
    :goto_59
    iput-boolean v1, p0, Lcom/android/internal/telephony/cat/Input;->helpAvailable:Z

    #@5b
    .line 68
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    #@5e
    move-result-object v0

    #@5f
    check-cast v0, Lcom/android/internal/telephony/cat/Duration;

    #@61
    iput-object v0, p0, Lcom/android/internal/telephony/cat/Input;->duration:Lcom/android/internal/telephony/cat/Duration;

    #@63
    .line 69
    return-void

    #@64
    :cond_64
    move v0, v2

    #@65
    .line 62
    goto :goto_2d

    #@66
    :cond_66
    move v0, v2

    #@67
    .line 63
    goto :goto_36

    #@68
    :cond_68
    move v0, v2

    #@69
    .line 64
    goto :goto_3f

    #@6a
    :cond_6a
    move v0, v2

    #@6b
    .line 65
    goto :goto_48

    #@6c
    :cond_6c
    move v0, v2

    #@6d
    .line 66
    goto :goto_51

    #@6e
    :cond_6e
    move v1, v2

    #@6f
    .line 67
    goto :goto_59
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/android/internal/telephony/cat/Input$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/cat/Input;-><init>(Landroid/os/Parcel;)V

    #@3
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 72
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method setIcon(Landroid/graphics/Bitmap;)Z
    .registers 3
    .parameter "Icon"

    #@0
    .prologue
    .line 100
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 6
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 76
    iget-object v0, p0, Lcom/android/internal/telephony/cat/Input;->text:Ljava/lang/String;

    #@4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@7
    .line 77
    iget-object v0, p0, Lcom/android/internal/telephony/cat/Input;->defaultText:Ljava/lang/String;

    #@9
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@c
    .line 78
    iget-object v0, p0, Lcom/android/internal/telephony/cat/Input;->icon:Landroid/graphics/Bitmap;

    #@e
    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    #@11
    .line 79
    iget v0, p0, Lcom/android/internal/telephony/cat/Input;->minLen:I

    #@13
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 80
    iget v0, p0, Lcom/android/internal/telephony/cat/Input;->maxLen:I

    #@18
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@1b
    .line 81
    iget-boolean v0, p0, Lcom/android/internal/telephony/cat/Input;->ucs2:Z

    #@1d
    if-eqz v0, :cond_50

    #@1f
    move v0, v1

    #@20
    :goto_20
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@23
    .line 82
    iget-boolean v0, p0, Lcom/android/internal/telephony/cat/Input;->packed:Z

    #@25
    if-eqz v0, :cond_52

    #@27
    move v0, v1

    #@28
    :goto_28
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@2b
    .line 83
    iget-boolean v0, p0, Lcom/android/internal/telephony/cat/Input;->digitOnly:Z

    #@2d
    if-eqz v0, :cond_54

    #@2f
    move v0, v1

    #@30
    :goto_30
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@33
    .line 84
    iget-boolean v0, p0, Lcom/android/internal/telephony/cat/Input;->echo:Z

    #@35
    if-eqz v0, :cond_56

    #@37
    move v0, v1

    #@38
    :goto_38
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@3b
    .line 85
    iget-boolean v0, p0, Lcom/android/internal/telephony/cat/Input;->yesNo:Z

    #@3d
    if-eqz v0, :cond_58

    #@3f
    move v0, v1

    #@40
    :goto_40
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@43
    .line 86
    iget-boolean v0, p0, Lcom/android/internal/telephony/cat/Input;->helpAvailable:Z

    #@45
    if-eqz v0, :cond_5a

    #@47
    :goto_47
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@4a
    .line 87
    iget-object v0, p0, Lcom/android/internal/telephony/cat/Input;->duration:Lcom/android/internal/telephony/cat/Duration;

    #@4c
    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    #@4f
    .line 88
    return-void

    #@50
    :cond_50
    move v0, v2

    #@51
    .line 81
    goto :goto_20

    #@52
    :cond_52
    move v0, v2

    #@53
    .line 82
    goto :goto_28

    #@54
    :cond_54
    move v0, v2

    #@55
    .line 83
    goto :goto_30

    #@56
    :cond_56
    move v0, v2

    #@57
    .line 84
    goto :goto_38

    #@58
    :cond_58
    move v0, v2

    #@59
    .line 85
    goto :goto_40

    #@5a
    :cond_5a
    move v1, v2

    #@5b
    .line 86
    goto :goto_47
.end method
