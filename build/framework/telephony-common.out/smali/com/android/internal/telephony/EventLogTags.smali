.class public Lcom/android/internal/telephony/EventLogTags;
.super Ljava/lang/Object;
.source "EventLogTags.java"


# static fields
.field public static final BAD_IP_ADDRESS:I = 0xc3c5

.field public static final CALL_DROP:I = 0xc3ba

.field public static final CDMA_DATA_DROP:I = 0xc3bf

.field public static final CDMA_DATA_SETUP_FAILED:I = 0xc3be

.field public static final CDMA_DATA_STATE_CHANGE:I = 0xc3c3

.field public static final CDMA_SERVICE_STATE_CHANGE:I = 0xc3c4

.field public static final DATA_NETWORK_REGISTRATION_FAIL:I = 0xc3bb

.field public static final DATA_NETWORK_STATUS_ON_RADIO_OFF:I = 0xc3bc

.field public static final DATA_STALL_RECOVERY_CLEANUP:I = 0xc3c7

.field public static final DATA_STALL_RECOVERY_GET_DATA_CALL_LIST:I = 0xc3c6

.field public static final DATA_STALL_RECOVERY_RADIO_RESTART:I = 0xc3c9

.field public static final DATA_STALL_RECOVERY_RADIO_RESTART_WITH_PROP:I = 0xc3ca

.field public static final DATA_STALL_RECOVERY_REREGISTER:I = 0xc3c8

.field public static final GSM_DATA_STATE_CHANGE:I = 0xc3c1

.field public static final GSM_RAT_SWITCHED:I = 0xc3c0

.field public static final GSM_SERVICE_STATE_CHANGE:I = 0xc3c2

.field public static final PDP_BAD_DNS_ADDRESS:I = 0xc3b4

.field public static final PDP_CONTEXT_RESET:I = 0xc3b7

.field public static final PDP_NETWORK_DROP:I = 0xc3bd

.field public static final PDP_RADIO_RESET:I = 0xc3b6

.field public static final PDP_RADIO_RESET_COUNTDOWN_TRIGGERED:I = 0xc3b5

.field public static final PDP_REREGISTER_NETWORK:I = 0xc3b8

.field public static final PDP_SETUP_FAIL:I = 0xc3b9

.field public static final SMS_DENIED_BY_USER:I = 0xc3cd

.field public static final SMS_SENT_BY_USER:I = 0xc3d0


# direct methods
.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 11
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static writeBadIpAddress(Ljava/lang/String;)V
    .registers 2
    .parameter "ipAddress"

    #@0
    .prologue
    .line 157
    const v0, 0xc3c5

    #@3
    invoke-static {v0, p0}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    #@6
    .line 158
    return-void
.end method

.method public static writeCallDrop(III)V
    .registers 7
    .parameter "cause"
    .parameter "cid"
    .parameter "networkType"

    #@0
    .prologue
    .line 113
    const v0, 0xc3ba

    #@3
    const/4 v1, 0x3

    #@4
    new-array v1, v1, [Ljava/lang/Object;

    #@6
    const/4 v2, 0x0

    #@7
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@a
    move-result-object v3

    #@b
    aput-object v3, v1, v2

    #@d
    const/4 v2, 0x1

    #@e
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@11
    move-result-object v3

    #@12
    aput-object v3, v1, v2

    #@14
    const/4 v2, 0x2

    #@15
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@18
    move-result-object v3

    #@19
    aput-object v3, v1, v2

    #@1b
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@1e
    .line 114
    return-void
.end method

.method public static writeCdmaDataDrop(II)V
    .registers 6
    .parameter "cid"
    .parameter "networkType"

    #@0
    .prologue
    .line 133
    const v0, 0xc3bf

    #@3
    const/4 v1, 0x2

    #@4
    new-array v1, v1, [Ljava/lang/Object;

    #@6
    const/4 v2, 0x0

    #@7
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@a
    move-result-object v3

    #@b
    aput-object v3, v1, v2

    #@d
    const/4 v2, 0x1

    #@e
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@11
    move-result-object v3

    #@12
    aput-object v3, v1, v2

    #@14
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@17
    .line 134
    return-void
.end method

.method public static writeCdmaDataSetupFailed(III)V
    .registers 7
    .parameter "cause"
    .parameter "cid"
    .parameter "networkType"

    #@0
    .prologue
    .line 129
    const v0, 0xc3be

    #@3
    const/4 v1, 0x3

    #@4
    new-array v1, v1, [Ljava/lang/Object;

    #@6
    const/4 v2, 0x0

    #@7
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@a
    move-result-object v3

    #@b
    aput-object v3, v1, v2

    #@d
    const/4 v2, 0x1

    #@e
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@11
    move-result-object v3

    #@12
    aput-object v3, v1, v2

    #@14
    const/4 v2, 0x2

    #@15
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@18
    move-result-object v3

    #@19
    aput-object v3, v1, v2

    #@1b
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@1e
    .line 130
    return-void
.end method

.method public static writeCdmaDataStateChange(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter "oldstate"
    .parameter "newstate"

    #@0
    .prologue
    .line 149
    const v0, 0xc3c3

    #@3
    const/4 v1, 0x2

    #@4
    new-array v1, v1, [Ljava/lang/Object;

    #@6
    const/4 v2, 0x0

    #@7
    aput-object p0, v1, v2

    #@9
    const/4 v2, 0x1

    #@a
    aput-object p1, v1, v2

    #@c
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@f
    .line 150
    return-void
.end method

.method public static writeCdmaServiceStateChange(IIII)V
    .registers 8
    .parameter "oldstate"
    .parameter "olddatastate"
    .parameter "newstate"
    .parameter "newdatastate"

    #@0
    .prologue
    .line 153
    const v0, 0xc3c4

    #@3
    const/4 v1, 0x4

    #@4
    new-array v1, v1, [Ljava/lang/Object;

    #@6
    const/4 v2, 0x0

    #@7
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@a
    move-result-object v3

    #@b
    aput-object v3, v1, v2

    #@d
    const/4 v2, 0x1

    #@e
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@11
    move-result-object v3

    #@12
    aput-object v3, v1, v2

    #@14
    const/4 v2, 0x2

    #@15
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@18
    move-result-object v3

    #@19
    aput-object v3, v1, v2

    #@1b
    const/4 v2, 0x3

    #@1c
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1f
    move-result-object v3

    #@20
    aput-object v3, v1, v2

    #@22
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@25
    .line 154
    return-void
.end method

.method public static writeDataNetworkRegistrationFail(II)V
    .registers 6
    .parameter "opNumeric"
    .parameter "cid"

    #@0
    .prologue
    .line 117
    const v0, 0xc3bb

    #@3
    const/4 v1, 0x2

    #@4
    new-array v1, v1, [Ljava/lang/Object;

    #@6
    const/4 v2, 0x0

    #@7
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@a
    move-result-object v3

    #@b
    aput-object v3, v1, v2

    #@d
    const/4 v2, 0x1

    #@e
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@11
    move-result-object v3

    #@12
    aput-object v3, v1, v2

    #@14
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@17
    .line 118
    return-void
.end method

.method public static writeDataNetworkStatusOnRadioOff(Ljava/lang/String;I)V
    .registers 6
    .parameter "dcState"
    .parameter "enable"

    #@0
    .prologue
    .line 121
    const v0, 0xc3bc

    #@3
    const/4 v1, 0x2

    #@4
    new-array v1, v1, [Ljava/lang/Object;

    #@6
    const/4 v2, 0x0

    #@7
    aput-object p0, v1, v2

    #@9
    const/4 v2, 0x1

    #@a
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@d
    move-result-object v3

    #@e
    aput-object v3, v1, v2

    #@10
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@13
    .line 122
    return-void
.end method

.method public static writeDataStallRecoveryCleanup(I)V
    .registers 2
    .parameter "outPacketCount"

    #@0
    .prologue
    .line 165
    const v0, 0xc3c7

    #@3
    invoke-static {v0, p0}, Landroid/util/EventLog;->writeEvent(II)I

    #@6
    .line 166
    return-void
.end method

.method public static writeDataStallRecoveryGetDataCallList(I)V
    .registers 2
    .parameter "outPacketCount"

    #@0
    .prologue
    .line 161
    const v0, 0xc3c6

    #@3
    invoke-static {v0, p0}, Landroid/util/EventLog;->writeEvent(II)I

    #@6
    .line 162
    return-void
.end method

.method public static writeDataStallRecoveryRadioRestart(I)V
    .registers 2
    .parameter "outPacketCount"

    #@0
    .prologue
    .line 173
    const v0, 0xc3c9

    #@3
    invoke-static {v0, p0}, Landroid/util/EventLog;->writeEvent(II)I

    #@6
    .line 174
    return-void
.end method

.method public static writeDataStallRecoveryRadioRestartWithProp(I)V
    .registers 2
    .parameter "outPacketCount"

    #@0
    .prologue
    .line 177
    const v0, 0xc3ca

    #@3
    invoke-static {v0, p0}, Landroid/util/EventLog;->writeEvent(II)I

    #@6
    .line 178
    return-void
.end method

.method public static writeDataStallRecoveryReregister(I)V
    .registers 2
    .parameter "outPacketCount"

    #@0
    .prologue
    .line 169
    const v0, 0xc3c8

    #@3
    invoke-static {v0, p0}, Landroid/util/EventLog;->writeEvent(II)I

    #@6
    .line 170
    return-void
.end method

.method public static writeGsmDataStateChange(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter "oldstate"
    .parameter "newstate"

    #@0
    .prologue
    .line 141
    const v0, 0xc3c1

    #@3
    const/4 v1, 0x2

    #@4
    new-array v1, v1, [Ljava/lang/Object;

    #@6
    const/4 v2, 0x0

    #@7
    aput-object p0, v1, v2

    #@9
    const/4 v2, 0x1

    #@a
    aput-object p1, v1, v2

    #@c
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@f
    .line 142
    return-void
.end method

.method public static writeGsmRatSwitched(III)V
    .registers 7
    .parameter "cid"
    .parameter "networkFrom"
    .parameter "networkTo"

    #@0
    .prologue
    .line 137
    const v0, 0xc3c0

    #@3
    const/4 v1, 0x3

    #@4
    new-array v1, v1, [Ljava/lang/Object;

    #@6
    const/4 v2, 0x0

    #@7
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@a
    move-result-object v3

    #@b
    aput-object v3, v1, v2

    #@d
    const/4 v2, 0x1

    #@e
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@11
    move-result-object v3

    #@12
    aput-object v3, v1, v2

    #@14
    const/4 v2, 0x2

    #@15
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@18
    move-result-object v3

    #@19
    aput-object v3, v1, v2

    #@1b
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@1e
    .line 138
    return-void
.end method

.method public static writeGsmServiceStateChange(IIII)V
    .registers 8
    .parameter "oldstate"
    .parameter "oldgprsstate"
    .parameter "newstate"
    .parameter "newgprsstate"

    #@0
    .prologue
    .line 145
    const v0, 0xc3c2

    #@3
    const/4 v1, 0x4

    #@4
    new-array v1, v1, [Ljava/lang/Object;

    #@6
    const/4 v2, 0x0

    #@7
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@a
    move-result-object v3

    #@b
    aput-object v3, v1, v2

    #@d
    const/4 v2, 0x1

    #@e
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@11
    move-result-object v3

    #@12
    aput-object v3, v1, v2

    #@14
    const/4 v2, 0x2

    #@15
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@18
    move-result-object v3

    #@19
    aput-object v3, v1, v2

    #@1b
    const/4 v2, 0x3

    #@1c
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1f
    move-result-object v3

    #@20
    aput-object v3, v1, v2

    #@22
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@25
    .line 146
    return-void
.end method

.method public static writePdpBadDnsAddress(Ljava/lang/String;)V
    .registers 2
    .parameter "dnsAddress"

    #@0
    .prologue
    .line 89
    const v0, 0xc3b4

    #@3
    invoke-static {v0, p0}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    #@6
    .line 90
    return-void
.end method

.method public static writePdpContextReset(I)V
    .registers 2
    .parameter "outPacketCount"

    #@0
    .prologue
    .line 101
    const v0, 0xc3b7

    #@3
    invoke-static {v0, p0}, Landroid/util/EventLog;->writeEvent(II)I

    #@6
    .line 102
    return-void
.end method

.method public static writePdpNetworkDrop(II)V
    .registers 6
    .parameter "cid"
    .parameter "networkType"

    #@0
    .prologue
    .line 125
    const v0, 0xc3bd

    #@3
    const/4 v1, 0x2

    #@4
    new-array v1, v1, [Ljava/lang/Object;

    #@6
    const/4 v2, 0x0

    #@7
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@a
    move-result-object v3

    #@b
    aput-object v3, v1, v2

    #@d
    const/4 v2, 0x1

    #@e
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@11
    move-result-object v3

    #@12
    aput-object v3, v1, v2

    #@14
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@17
    .line 126
    return-void
.end method

.method public static writePdpRadioReset(I)V
    .registers 2
    .parameter "outPacketCount"

    #@0
    .prologue
    .line 97
    const v0, 0xc3b6

    #@3
    invoke-static {v0, p0}, Landroid/util/EventLog;->writeEvent(II)I

    #@6
    .line 98
    return-void
.end method

.method public static writePdpRadioResetCountdownTriggered(I)V
    .registers 2
    .parameter "outPacketCount"

    #@0
    .prologue
    .line 93
    const v0, 0xc3b5

    #@3
    invoke-static {v0, p0}, Landroid/util/EventLog;->writeEvent(II)I

    #@6
    .line 94
    return-void
.end method

.method public static writePdpReregisterNetwork(I)V
    .registers 2
    .parameter "outPacketCount"

    #@0
    .prologue
    .line 105
    const v0, 0xc3b8

    #@3
    invoke-static {v0, p0}, Landroid/util/EventLog;->writeEvent(II)I

    #@6
    .line 106
    return-void
.end method

.method public static writePdpSetupFail(III)V
    .registers 7
    .parameter "cause"
    .parameter "cid"
    .parameter "networkType"

    #@0
    .prologue
    .line 109
    const v0, 0xc3b9

    #@3
    const/4 v1, 0x3

    #@4
    new-array v1, v1, [Ljava/lang/Object;

    #@6
    const/4 v2, 0x0

    #@7
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@a
    move-result-object v3

    #@b
    aput-object v3, v1, v2

    #@d
    const/4 v2, 0x1

    #@e
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@11
    move-result-object v3

    #@12
    aput-object v3, v1, v2

    #@14
    const/4 v2, 0x2

    #@15
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@18
    move-result-object v3

    #@19
    aput-object v3, v1, v2

    #@1b
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@1e
    .line 110
    return-void
.end method

.method public static writeSmsDeniedByUser(Ljava/lang/String;)V
    .registers 2
    .parameter "appSignature"

    #@0
    .prologue
    .line 181
    const v0, 0xc3cd

    #@3
    invoke-static {v0, p0}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    #@6
    .line 182
    return-void
.end method

.method public static writeSmsSentByUser(Ljava/lang/String;)V
    .registers 2
    .parameter "appSignature"

    #@0
    .prologue
    .line 185
    const v0, 0xc3d0

    #@3
    invoke-static {v0, p0}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    #@6
    .line 186
    return-void
.end method
